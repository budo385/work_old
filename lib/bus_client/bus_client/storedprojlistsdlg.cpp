#include "storedprojlistsdlg.h"
#include "gui_core/gui_core/macros.h"
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;

StoredProjListsDlg::StoredProjListsDlg(int nCurListID, QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);

	Status status;
	DbRecordSet recLists;
	_SERVER_CALL(StoredProjectLists->GetListNames(status, recLists /*,g_pClientManager->GetPersonID()*/)) //Project grid id
	_CHK_ERR(status);
	recLists.sort("BSPL_NAME");

	//Fill combo.
	int nRowCount = recLists.getRowCount();
	if(nRowCount > 0)
	{
		int nSelectIdx = 0;
		for(int i = 0; i < nRowCount; i++)
		{
			int ViewID = recLists.getDataRef(i, "BSPL_ID").toInt();
			QString strViewName = recLists.getDataRef(i, "BSPL_NAME").toString();
			ui.cboStoredProjLists->addItem(strViewName, ViewID);

			if(nCurListID == ViewID)
				nSelectIdx = i;
		}

		ui.cboStoredProjLists->setCurrentIndex(nSelectIdx);
	}
	else
		ui.btnOK->setEnabled(false);
}

StoredProjListsDlg::~StoredProjListsDlg()
{
}

void StoredProjListsDlg::on_btnCancel_clicked()
{
	done(0);
}

void StoredProjListsDlg::on_btnOK_clicked()
{
	done(1);
}

int StoredProjListsDlg::GetSelectedListID()
{
	int nIdx = ui.cboStoredProjLists->currentIndex();
	if(nIdx >= 0)
	{
		return ui.cboStoredProjLists->itemData(nIdx).toInt();
	}
	return -1; //no list selected
}

QString StoredProjListsDlg::GetSelectedName()
{
	return ui.cboStoredProjLists->currentText();
}
