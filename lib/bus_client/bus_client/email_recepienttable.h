#ifndef EMAIL_RECEPIENTTABLE_H
#define EMAIL_RECEPIENTTABLE_H

#include "mainentityselectioncontroller.h"
#include "common/common/dbrecordset.h"
#include <QtWidgets/QTableWidget>
#include <QCompleter>

class Email_RecepientTable : public QTableWidget
{
	friend class RecepientTable_WidgetRow;
	Q_OBJECT

public:
	Email_RecepientTable(QWidget * parent);
	~Email_RecepientTable();
	void	GetRecipients(DbRecordSet &recToRecordSet, DbRecordSet &recCCRecordSet, DbRecordSet &recBCCRecordSet);
	void	AppendContacts(DbRecordSet &recToRecordSet, DbRecordSet &recCCRecordSet, DbRecordSet &recBCCRecordSet);
	void	ClearAll();
	void	SetEditMode(bool bEdit=true);

private slots:
	void	OnCellClicked ( int row, int column );

private:
	void	OnSelectContact(int nRow);
	void	OnSelectContactEmail(int nRow);
	void	OnDelete(int nRow);
	void	RefreshDisplay();
	QString GetContactMail(int nContactID, int &nEMailID);
	bool	AddContact(DbRecordSet &rowRecepient);
	void	Widgets2Data();

	DbRecordSet						m_lstRecepients;
	DbRecordSet						m_lstTypes;
	bool							m_bEdit;
	MainEntitySelectionController	m_AllEmails;
	QCompleter						*m_pCompleter;
};



class RecepientTable_WidgetRow : public QWidget
{
	Q_OBJECT
public:
	RecepientTable_WidgetRow(Email_RecepientTable *parent,int nRow);
	~RecepientTable_WidgetRow();

private slots:
	void OnSelect();
	void OnSelectEmail();
	void OnRemove();

private:
	Email_RecepientTable *m_Parent;
	int m_nRow;
};

#endif // EMAIL_RECEPIENTTABLE_H
