#include "selection_recordselector.h"
#include "gui_core/gui_core/thememanager.h"
#define  STYLE_LABEL	  "QLabel {font-weight:600;font-style:normal;font-family:Arial; font-size:9pt; color:white}"

Selection_RecordSelector::Selection_RecordSelector(QWidget *parent)
	: QFrame(parent)
{
	ui.setupUi(this);

	m_pTitle = new QLabel;
	m_pTitle->setStyleSheet(STYLE_LABEL);
	//m_pTitle->setAlignment(Qt::AlignLeft);
	m_pTitle->setMaximumHeight(24);

	m_pRowSelector = new QLabel;
	m_pRowSelector->setStyleSheet(STYLE_LABEL);
	m_pRowSelector->setAlignment(Qt::AlignCenter);
	m_pRowSelector->setMaximumHeight(24);

	QSize sizeRowSelector(50, 24);
	//m_pRowSelector->setMinimumSize(sizeRowSelector);
	//m_pRowSelector->setMaximumSize(sizeRowSelector);

	QSize buttonSize(170, 24);
	m_btnAdd	=	new StyledPushButton(this,":Insert16_qcw.png",":Insert16_qcw.png","","");
	m_btnDel	=	new StyledPushButton(this,":Delete16_qcw.png",":Delete16_qcw.png","","");
	m_btnLeft	=	new StyledPushButton(this,":1leftarrow.png",":1leftarrow.png","","");
	m_btnRight	=	new StyledPushButton(this,":1rightarrow.png",":1rightarrow.png","","");
	m_btnAccept	=	new StyledPushButton(this,":Accept_Proposal16.png",":Accept_Proposal16.png","","");

	connect(m_btnAdd,SIGNAL(clicked()),this,SLOT(OnAdd()));
	connect(m_btnDel,SIGNAL(clicked()),this,SLOT(OnDelete()));
	connect(m_btnLeft,SIGNAL(clicked()),this,SLOT(OnLeft()));
	connect(m_btnRight,SIGNAL(clicked()),this,SLOT(OnRight()));
	connect(m_btnAccept,SIGNAL(clicked()),this,SLOT(OnAccept()));


	QHBoxLayout *box = new QHBoxLayout;
	box->addWidget(m_pTitle);
	box->addWidget(m_btnAdd);
	box->addWidget(m_btnDel);
	box->addWidget(m_btnLeft);
	box->addWidget(m_pRowSelector);
	box->addWidget(m_btnRight);
	box->addWidget(m_btnAccept);
	box->setSpacing(2);
	box->setContentsMargins(2,2,2,2);

	this->setLayout(box);

	//qDebug()<<this->objectName();
	QString strStyle="QFrame#frameRowSelector "+ThemeManager::GetCEMenuButtonStyle_Ex();
	this->setStyleSheet(strStyle);

}

Selection_RecordSelector::~Selection_RecordSelector()
{

}

void Selection_RecordSelector::SetEditMode(bool bEditMode)
{
	m_btnAdd->setEnabled(bEditMode);
	m_btnDel->setEnabled(bEditMode);
}

void Selection_RecordSelector::Initialize(DbRecordSet *pLstSource, QString strSelectorTitle,bool bPreventDelOfFirstRow)
{
	m_nCurrentRow=-1;
	if (pLstSource->getRowCount()>0)
		m_nCurrentRow=0;
	m_pLstSource=pLstSource;
	m_bPreventDelOfFirstRow=bPreventDelOfFirstRow;
	if (!strSelectorTitle.isEmpty())
		m_pTitle->setText(strSelectorTitle);
	else
		m_pTitle->setVisible(false);

	RefreshLabel();
}

void Selection_RecordSelector::Reset()
{
	m_nCurrentRow=-1;
	if (m_pLstSource->getRowCount()>0)
		m_nCurrentRow=0;
	RefreshLabel();
}

void Selection_RecordSelector::RefreshLabel()
{
	m_btnLeft->setEnabled(true);
	m_btnRight->setEnabled(true);

	QString strText=QString::number(m_nCurrentRow+1)+" / "+QString::number(m_pLstSource->getRowCount());
	m_pRowSelector->setText(strText);

	if (m_nCurrentRow==-1 ||( m_nCurrentRow==0 && m_bPreventDelOfFirstRow && m_pLstSource->getRowCount()==1))
		m_btnDel->setEnabled(false);
	else
		m_btnDel->setEnabled(true);
	if (m_pLstSource->getRowCount()==0 || m_pLstSource->getRowCount()==1)
	{
		m_btnLeft->setEnabled(false);
		m_btnRight->setEnabled(false);
	}
	if (m_nCurrentRow==0)
	{
		m_btnLeft->setEnabled(false);
	}
	else if (m_nCurrentRow==m_pLstSource->getRowCount()-1)
	{
		m_btnRight->setEnabled(false);
	}
}

void Selection_RecordSelector::OnAdd()
{
	int nPrevRow=m_nCurrentRow;
	m_pLstSource->addRow();
	m_nCurrentRow=m_pLstSource->getRowCount()-1;
	
	RefreshLabel();
	emit RowAdded(nPrevRow,m_pLstSource->getRowCount()-1);
	emit RowChanged(nPrevRow,m_nCurrentRow);
}
void Selection_RecordSelector::OnDelete()
{
	if (m_bPreventDelOfFirstRow && m_pLstSource->getRowCount()==1)
	{
		return;
	}
	m_pLstSource->deleteRow(m_nCurrentRow);
	int nRow=m_nCurrentRow;
	m_nCurrentRow--;
	if (m_nCurrentRow<0 && m_pLstSource->getRowCount()>0)
		m_nCurrentRow=0;

	RefreshLabel();
	emit RowDeleted(nRow);
	emit RowChanged(-1,m_nCurrentRow);

}
void Selection_RecordSelector::OnLeft()
{
	int nPrevRow=m_nCurrentRow;
	m_nCurrentRow--;
	if (m_nCurrentRow<0 && m_pLstSource->getRowCount()>0)
		m_nCurrentRow=0;
	
	RefreshLabel();
	emit RowChanged(nPrevRow,m_nCurrentRow);
}

void Selection_RecordSelector::OnRight()
{
	int nPrevRow=m_nCurrentRow;
	m_nCurrentRow++;
	if (m_nCurrentRow>=m_pLstSource->getRowCount()-1)
		m_nCurrentRow=m_pLstSource->getRowCount()-1;
	
	RefreshLabel();
	emit RowChanged(nPrevRow,m_nCurrentRow);
}

void Selection_RecordSelector::OnAccept()
{
	emit Accepted(m_nCurrentRow);
}

