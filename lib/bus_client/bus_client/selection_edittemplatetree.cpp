#include "bus_client/bus_client/selection_edittemplatetree.h"

Selection_EditTemplateTree::Selection_EditTemplateTree(QWidget *parent)
    : QDialog(parent)
{
	ui.setupUi(this);
}

Selection_EditTemplateTree::~Selection_EditTemplateTree()
{
}

void Selection_EditTemplateTree::SetData(DbRecordSet &data, QString strCodeField, QString strNameField)
{
	m_lstData = data;
	ui.listWidget->Initialize(&m_lstData, false, false, true);
	ui.listWidget->SetEditMode();

	DbRecordSet columns;
	ui.listWidget->AddColumnToSetup(columns, strCodeField,tr("Code"),120,true, "");
	ui.listWidget->AddColumnToSetup(columns, strNameField,tr("Name"),160,true, "");
	ui.listWidget->SetColumnSetup(columns);
	ui.listWidget->RefreshDisplay();
}

void Selection_EditTemplateTree::on_btnOK_clicked()
{
	accept();
}

void Selection_EditTemplateTree::on_btnCancel_clicked()
{
	reject();
}

void Selection_EditTemplateTree::on_btnRemove_clicked()
{
	m_lstData.deleteSelectedRows();
	ui.listWidget->RefreshDisplay();
}
