#include "simpleselectionwiztree.h"

#include <QHeaderView>
#include "gui_core/gui_core/thememanager.h"

SimpleSelectionWizTree::SimpleSelectionWizTree(QWidget *parent)
    : QTreeWidget(parent)
{
	//Set look and feel.
	header()->hide();
	setRootIsDecorated(false);
	setDragDropMode(QAbstractItemView::DropOnly);
	setStyleSheet(ThemeManager::GetTreeBkg());
	setSelectionMode(QAbstractItemView::ExtendedSelection);
}

SimpleSelectionWizTree::~SimpleSelectionWizTree()
{

}

bool SimpleSelectionWizTree::dropMimeData(QTreeWidgetItem *parent, int index, const QMimeData *data, Qt::DropAction action)
{
	emit DataDroped(parent, index, data, action);

	return true;
}
