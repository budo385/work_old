#ifndef DLG_ACCUSERRECORD_H
#define DLG_ACCUSERRECORD_H

#include <QtWidgets/QDialog>
#include "generatedfiles/ui_dlg_accuserrecord.h"
#include "bus_client/bus_client/mainentityselectioncontroller.h"


/*

	- Accepts extern UAR/GAR records, prepare for display
	
	UAR (not group dep):
	- extend recordset with CX fields and person data, refill it
	- if no rec with Person=NULL, add new one CUAR_PERSON_ID=NULL, Name=Others, always present at 1st row!
	
	UAR (group dep):
	- extend recordset with CX fields and person data and group/tree data, refill it
	- clear all previous records
	- add new one from all dep. groups users: refill CX, persona data and calc MAX RIGHTS
	- all chk boxes disabled, only is group enabled, -> group dep can be re-checked -> update on server

	GAR:
	- extend recordset with CX fields and group/tree data, refill it
	- shown in complex tree format: still to be desgined!!-> ver 2.0

	UAR GRID: (sort by pers code, group tree, group code)
	code, username, read chx, modify chx, full chx, is group dep chx, group code/name, tree name

	Parent :
	//CUAR_PARENT_ID if found and set, disable all, until ckbParentInherit is clear! If cleared: clear from all records
	//CUAR_PARENT_ID can only point to PROJECT UAR or GAR: child project UAR/GAR or child comm object UAR/GAR
	//if only one record has CUAR_PARENT_ID set, all other records must point to corresponding parent UAR/GAR records
	//CUAR_CHILD_INHERIT: only projects -> ckbChildInherit if set all subprojects inherit!!! (one time write)-> in WRITE_UAR_GAR (update/create for children)
	//CUAR_CHILD_COMM_INHERIT only projects -> ckbCommChildInherit if set all emails/voice,documents inherit (one time write) -> in WRITE_UAR_GAR (update/create for children)
	//if CUAR_GROUP_DEP=1 -> CUAR_PARENT_ID is NULL, as there is no point for that..as RIGHTS are calc from groups not copied from parent!!
	
	Operations:
	- ADD USER:	(from drop from cont or group):
	- set rec:id, pers:id, right = max, all flags clear, no group dep.
	- when group_dep is unchecked: clear group data, if checked -> algorithm for max group...!!!???
	
	- ADD GROUP: 
	- add in tree + subgroups+check boxes
	- add all UAR dep records. -> refresh ALL
	- when read, modify, del is changed-> recalc all user dep records -> algorithm for max group...!!!???

	- DEL USER: ok!
	- DEL GROUP: remove from tree, and UAR dep-> refresh ALL

	- COPY: !???

	- LOCKING: does not make any sense: as AR change must occur from editing of entity record (lock), all other sub changes on other entity records (if inherit) can not be avoided!

	- SELECTION CONTACTS: filter all that are not users!??
*/
class Dlg_AccUserRecord : public QDialog
{
	Q_OBJECT

public:
	Dlg_AccUserRecord(QWidget *parent = 0);
	~Dlg_AccUserRecord();

	void Initialize(Status &err, bool bReadOnly,QString strRecordDisplayName,int nLoggedPersonId,int nRecordId,int nTableId,DbRecordSet *lstUAR=NULL,DbRecordSet *lstGAR=NULL);
	void GetResult(DbRecordSet &lstUAR,DbRecordSet &lstGAR);

private slots:
	void on_btnCancel_clicked();
	void on_btnOK_clicked();
	void on_btnCopy_clicked();
	void on_btnAddSelected_UAR_clicked();
	void on_btnRemoveSelected_UAR_clicked();
	void on_btnAddSelected_GAR_clicked();
	void on_btnRemoveSelected_GAR_clicked();
	void on_btnAsignMySelf_clicked();
	void on_btnDeclareAsNonGroupDep_clicked();
	void on_btnAsignToSelected_clicked();
	void on_ckbRead_UAR_clicked();
	void on_ckbModify_UAR_clicked();
	void on_ckbFull_UAR_clicked();
	void on_ckbParentInherit_clicked();
	void OnTableDataDroped(int nDropType, DbRecordSet &lstDropValue, QDropEvent *event);
	void OnTreeDataDroped(int nDropType, DbRecordSet &lstDropValue, QDropEvent *event);
	void OnGroupTreeChanged();
	void OnCalcUARMaxRightForGroupDep(int nRow=-1);
	void OnGroupTreeUpdateAR();

private:
	Ui::Dlg_AccUserRecordClass ui;
	void GenerateNewUARFromContacts(DbRecordSet &lstContacts, DbRecordSet &lstUAR);
	void GenerateNewUARFromPersonID(QVariant varPersonID, DbRecordSet &lstUAR);
	void AddNewUARContacts(DbRecordSet &lstContacts);
	void AddNewGroups(DbRecordSet &lstGroups);
	bool CheckGUARRecords();
	

	int m_nRecordId;
	int m_nTableId;
	int m_nLoggedPersonId;
	int m_nParentRecordId;
	int m_nParentTableId;
	QString m_sParentName;

	DbRecordSet m_lstUAR;
	DbRecordSet m_lstGAR;
	DbRecordSet m_lstGroupUsers;
	MainEntitySelectionController	mPersonCache;

};

#endif // DLG_ACCUSERRECORD_H
