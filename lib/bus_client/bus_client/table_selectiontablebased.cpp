#include "table_selectiontablebased.h"
#include "common/common/entity_id_collection.h"

Table_SelectionTableBased::Table_SelectionTableBased(QWidget *parent)
:UniversalTableWidget(parent)
{
}


/*!
	Init table widget

	\param   nEntityID			- entity will be returend as drop type
	\param   pData				- source 
	\param   lstColumnSetup		- column setup data for UniversalTableWidget
*/
void Table_SelectionTableBased::Initialize(int nEntityID,DbRecordSet * pData,DbRecordSet *lstColumnSetup,bool bEnableDrag,bool bEnableMultiSelection)
{
	m_nEntityID=nEntityID;

	//set data source for table, set single selection:
	UniversalTableWidget::Initialize(pData,false,bEnableDrag,!bEnableMultiSelection);

	SetColumnSetup(*lstColumnSetup);

	//edit is false always:
	SetEditMode(false);

}


//create own context based on entity type:
void Table_SelectionTableBased::CreateContextMenuActions(QList<QAction*>& lstActions)
{

	QAction* pAction;
	QAction* SeparatorAct;

	pAction = new QAction(tr("&Reload"), this);
	pAction->setData(QVariant(true));	//means that is enabled only in edit mode
	connect(pAction, SIGNAL(triggered()), this, SLOT(SlotReload()));

	lstActions.append(pAction);
	this->addAction(pAction);


	//Add separator
	SeparatorAct = new QAction(this);
	SeparatorAct->setSeparator(true);
	lstActions.append(SeparatorAct);


	//SORT:
	pAction = new QAction(tr("S&ort"), this);
	pAction->setShortcut(tr("CTRL+S"));
	pAction->setIcon(QIcon(":rollingdice.png"));
	pAction->setData(QVariant(true));	//means that is always enabled
	connect(pAction, SIGNAL(triggered()), this, SLOT(SortDialog()));

	lstActions.append(pAction);
	this->addAction(pAction);

}
