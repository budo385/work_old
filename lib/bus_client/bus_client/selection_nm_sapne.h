#ifndef SELECTION_NM_SAPNE_H
#define SELECTION_NM_SAPNE_H


#include <QtWidgets/QFrame>
#include <QtWidgets/QMessageBox>
#ifndef WINCE
#include "generatedfiles/ui_selection_nm_sapne.h"
#else
#include "embedded_core/embedded_core/generatedfiles/ui_selection_nm_sapne.h"
#endif

#include "bus_client/bus_client/selectionpopup.h"
#include "common/common/observer_ptrn.h"
#include "bus_core/bus_core/mainentitycalculatedname.h"
#include "gui_core/gui_core/guidatamanipulator.h"
#include "bus_client/bus_client/mainentityselectioncontroller.h"
#include "bus_client/bus_client/selection_contactbutton.h"



/*!
	\class  Selection_NM_SAPNE
	\brief  1N assignment from selection dialog to one field inside FUI 
	\ingroup GUICore

	General Info:
	- set NM list, get NM list (init your way)
*/
class Selection_NM_SAPNE : public QFrame,public ObsrPtrn_Subject, public ObsrPtrn_Observer
{
	Q_OBJECT

public:
	Selection_NM_SAPNE(QWidget *parent = 0);
	virtual ~Selection_NM_SAPNE();

	enum Button
	{
		BUTTON_MODIFY,			
		BUTTON_ADD,	
		BUTTON_REMOVE,	
		BUTTON_SELECT,
		BUTTON_OPEN_NEW,		
		BUTTON_CONTACT_PIE		
	};
	void Initialize(int nEntityTypeID,QString strTitle="",bool bBlockNewPopUp=false);						
	void Initialize(int nEntityTypeID,QString strFKColumnName,int nTableViewID,DbRecordSet &lstColumnSetup,QString strTitle="",bool bBlockNewPopUp=false);						
	//void SetDisplayColumns(DbRecordSet &lstColumnSetup);	//sets display columns, if not set->only calc field is displayed						
	void SetList(DbRecordSet &lstNMData, int nPK_IDColIdx=-1);		//only ID field is relevant
	void GetList(DbRecordSet &lstNMData);					//data in entity format
	void Clear(bool bSkipNotifyObservers=false);
	void SetEditMode(bool bEditMode);
	QWidget* GetLastFUIOPen(){return m_pLastFUIOpen;};
	//for ACO (when opening new FUI, ACO must be preset)
	void SetActualOrganization(int nActualOrganizationID){m_nActualOrganizationID=nActualOrganizationID;}
	QWidget* GetButton(int nButton);
	//return main sel. controller:
	MainEntitySelectionController* GetSelectionController(){return m_dlgCachedPopUpSelector;}
	//to catch data change event from selector controller
	void updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail=0,const QVariant val=QVariant());
	void setEnabled ( bool );	//override
	void setDisabled ( bool );	//override
	bool blockSignals(bool b){blockObserverSignals(b); return QFrame::blockSignals(b);}
	void HideTitle(){ui.labelTitle->setVisible(false);};
signals:
	void OnAdd(DbRecordSet &lstData);
	void OnRemove(DbRecordSet &lstData);
private:
	Ui::Selection_NM_SAPNEClass ui;

protected:
	void SetEntityData(int nSelectedRecordID,DbRecordSet &rowSelected,bool bSkipNotifyObservers=false);
	virtual void CreatePieButton(){};
	int m_nEntityTypeID;
	bool m_bAutoAssigment;
	SelectionPopup m_dlgPopUp;
	MainEntitySelectionController *m_dlgCachedPopUpSelector;		//cached selector controller, 2nd time when open it will not go on server for data...
	QString m_strTitle;
	int m_nActualOrganizationID;
	//table view specific:
	int m_nTableDisplayViewID;
	int m_nFK_IDColIdx;
	QString m_strFKColumnName;
	QWidget *m_pLastFUIOpen;
	MainEntityCalculatedName m_NameFiller;
	DbRecordSet m_lstData;
	Selection_ContactButton * m_btnContactPie;

protected slots:
	virtual void on_btnModify_clicked();
	virtual void on_btnRemove_clicked();
	virtual void on_btnSelect_clicked();
	virtual void on_btnInsert_clicked();
	virtual void on_btnOpenNewWindow_clicked();
	virtual void OnItemClicked();

};


#endif // SELECTION_NM_SAPNE_H
