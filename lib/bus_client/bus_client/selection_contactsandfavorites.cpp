#include "selection_contactsandfavorites.h"
#include "gui_core/gui_core/mobilehelper.h"
#include "bus_client/bus_client/clientcontactmanager.h"
#include <QTabBar>
#include "db_core/db_core/dbsqltableview.h"
#include "common/common/entity_id_collection.h"
#include "bus_client/bus_client/selectionpopup.h"
#include "bus_interface/bus_interface/businessservicemanager.h"
#include "gui_core/gui_core/thememanager.h"

#include "clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;
#include "bus_client/bus_client/changemanager.h"
extern ChangeManager g_ChangeManager;	

Selection_ContactsAndFavorites::Selection_ContactsAndFavorites(QWidget *parent)
	: QFrame(parent)
{
	ui.setupUi(this);
	m_pTab= new QTabBar(this);
	m_pTab->addTab(tr("Contacts"));
	m_pTab->addTab(tr("Favorites"));

	m_pFrameContacts	= new Selection_Contacts;
	m_pFramePicFlow		= new QFrame;
	m_pStackedWidget	= new QStackedWidget;
	m_pStackedWidget->addWidget(m_pFrameContacts);
	m_pStackedWidget->addWidget(m_pFramePicFlow);

	m_pTab->setMaximumHeight(30);
	m_pTab->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Fixed);
	m_pStackedWidget->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);

	QVBoxLayout *vbox = new QVBoxLayout;
	vbox->addWidget(m_pTab);
	vbox->addWidget(m_pStackedWidget);
	vbox->setMargin(0);
	vbox->setSpacing(0);
	this->setLayout(vbox);

	connect(m_pTab,SIGNAL( currentChanged ( int)),this,SLOT(OnCurrentChanged ( int)));
	m_pStackedWidget->setCurrentIndex(0);

	m_recContactData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CONTACT_FULL));
	g_ChangeManager.registerObserver(this);
	m_CalcName.Initialize(ENTITY_BUS_CONTACT,m_recContactData);

	QStringList lstDummy;
	MobileHelper::PictureFlowWidget_Create(m_pFramePicFlow, &m_pPictureFlow, lstDummy, QSize(60,80), 0, 1/*favorites pic. flow*/);
	connect(m_pPictureFlow, SIGNAL(centerIndexChanged(int)), this, SLOT(OncenterIndexChanged(int)));
	connect(m_pPictureFlow, SIGNAL(centerIndexDoubleClicked(int)), this, SLOT(OncenterIndexDoubleClicked(int)));
	connect(m_pPictureFlow, SIGNAL(centerIndexSelectedForEditing(int)), this, SLOT(OncenterIndexSelectedForEditing(int)));
	connect(m_pPictureFlow, SIGNAL(AddItemButton_clicked()), this, SLOT(OnAddItemButton_clicked()));
	
	m_pPictureFlow->setStyleSheet(m_pPictureFlow->styleSheet()+" QWidget " +ThemeManager::GetBorderStyle());
	
	m_pAddFavorite = new QAction(tr("Add Favorite"),this);
	connect(m_pAddFavorite, SIGNAL(triggered()), this, SLOT(OnAddFav()));
	m_pRemoveFavorite = new QAction(tr("Remove Favorite"),this);
	connect(m_pRemoveFavorite, SIGNAL(triggered()), this, SLOT(OnRemoveFav()));
	m_pAddPicToFav = new QAction(tr("Add Picture"),this);
	connect(m_pAddPicToFav, SIGNAL(triggered()), this, SLOT(OnAddPicToFav()));
	m_pReload = new QAction(tr("Reload"),this);
	connect(m_pReload, SIGNAL(triggered()), this, SLOT(OnReload()));

	m_pTab->setStyleSheet(ThemeManager::GetTabBarStyle());
}

Selection_ContactsAndFavorites::~Selection_ContactsAndFavorites()
{
	g_ChangeManager.unregisterObserver(this);
}


void Selection_ContactsAndFavorites::OnCurrentChanged(int nIdx)
{
	m_pStackedWidget->setCurrentIndex(nIdx);
}
void Selection_ContactsAndFavorites::OncenterIndexChanged(int index)
{
	m_nCurrentCenterIndex = index;
	if (m_nCurrentCenterIndex>=0 && m_nCurrentCenterIndex<m_recContactData.getRowCount() && m_pStackedWidget->currentIndex()==1)
		emit FavoriteChanged(m_recContactData.getDataRef(m_nCurrentCenterIndex,"BCNT_ID").toInt());
}


void Selection_ContactsAndFavorites::ReloadData(int nDefaultFavID, bool bReloadFromServer)
{
	//Get favorites.
	Status status;
	ClientContactManager::GetFavoriteList(status, m_recContactData,!g_pClientManager->IsLogged(),bReloadFromServer);
	int nRowCount = m_recContactData.getRowCount();

	//Set current center slide - see the number of favorites, divide by 2 and take ceiling value - shortly take the middle one.
	if (nDefaultFavID==-1)
	{
		
		m_nCurrentCenterIndex = ceil((double)nRowCount/2)-1;
	}
	else
	{
		m_nCurrentCenterIndex=m_recContactData.find("BCNT_ID",nDefaultFavID,true);
	}
	if (m_nCurrentCenterIndex==-1 && m_recContactData.getRowCount()>0)
		m_nCurrentCenterIndex = ceil((double)nRowCount/2)-1;
	
	//Picture flow.
	m_pPictureFlow->blockSignals(true);
	QList<QAction*> lstActions;
	lstActions.append(m_pAddFavorite);
	lstActions.append(m_pRemoveFavorite);
	lstActions.append(m_pAddPicToFav);
	lstActions.append(m_pReload);
	MobileHelper::PictureFlowWidget_SetContactsPictures(m_pPictureFlow, m_recContactData, m_nCurrentCenterIndex,lstActions);
	m_pPictureFlow->blockSignals(false);

	//Actual contact frame.
	if(m_recContactData.getRowCount()==0)
		m_nCurrentCenterIndex=-1;

	if (nRowCount==0)
	{
		m_pRemoveFavorite->setEnabled(false);
		m_pAddPicToFav->setEnabled(false);
	}
	else
	{
		m_pRemoveFavorite->setEnabled(true);
		m_pAddPicToFav->setEnabled(true);
	}
	

}

void Selection_ContactsAndFavorites::SetSideBarVisible(bool bVisible)
{
	m_pTab->setVisible(bVisible);
	m_pTab->setCurrentIndex(0);
	m_pStackedWidget->setCurrentIndex(0);
	m_pFrameContacts->SetSideBarVisible(bVisible);
	//m_pFramePicFlow->setVisible(bVisible);
}
bool Selection_ContactsAndFavorites::IsSideBarVisible()
{
	return m_pFrameContacts->IsSideBarVisible() || m_pFramePicFlow->isVisible();
}


int	Selection_ContactsAndFavorites::GetCurrentFavoriteID()
{
	if (m_nCurrentCenterIndex>=0 && m_nCurrentCenterIndex<m_recContactData.getRowCount())
		return m_recContactData.getDataRef(m_nCurrentCenterIndex,"BCNT_ID").toInt();
	else
		return -1;
}

void Selection_ContactsAndFavorites::updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail,const QVariant val)
{

	if (pSubject==&g_ChangeManager)
	{
		if(nMsgCode==ChangeManager::GLOBAL_CONTACT_FAVORITE_ADD || nMsgCode==ChangeManager::GLOBAL_CONTACT_FAVORITE_REMOVE)
		{
			int nPrevFavID=GetCurrentFavoriteID();
			ReloadData(nPrevFavID,true);
			return;
		}
		if((nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_DELETED || nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_EDITED) && nMsgDetail==ENTITY_BUS_CONTACT)
		{
			DbRecordSet lstDelData=val.value<DbRecordSet>();
			if (lstDelData.getRowCount()>0 && lstDelData.getColumnIdx("BCNT_ID")>=0) 
			{
				int nDeletedID=lstDelData.getDataRef(0,"BCNT_ID").toInt();
				int nRow=m_recContactData.find("BCNT_ID",nDeletedID,true);
				if (nRow>=0)
				{
					int nPrevFav=GetCurrentFavoriteID();
					ReloadData(nPrevFav,true);
				}
				
			}
			return;
		}
	}

}

QString Selection_ContactsAndFavorites::GetCalculatedName(int nEntityRecordID)
{
	//if zero, return:
	if(nEntityRecordID<1)
		return "";

	if (m_pStackedWidget->currentIndex()==0)
		return m_pFrameContacts->GetCalculatedName(nEntityRecordID);
	else
	{
		int nRow=m_recContactData.find("BCNT_ID",nEntityRecordID,true);
		if (nRow<0)
			return "";
		else{
			DbRecordSet row = m_recContactData.getRow(nRow);
			return m_CalcName.GetCalculatedName(row,0);		//return calc name
		}
	}

	return "";
}

void Selection_ContactsAndFavorites::OnAddFav()
{

	//open pop up
	SelectionPopup dlgPopUp;
	dlgPopUp.Initialize(ENTITY_BUS_CONTACT);
	int nResult=dlgPopUp.OpenSelector();
	if(nResult)
	{
		DbRecordSet record;
		int nRecordID;
		dlgPopUp.GetSelectedData(nRecordID,record);
		if (nRecordID>0)
		{
			Status err;
			ClientContactManager::AddFavorite(err,nRecordID);
			_CHK_ERR(err);

			ReloadData(nRecordID);
			emit FavoriteChanged(nRecordID);
		}
	}

}
void Selection_ContactsAndFavorites::OnRemoveFav()
{

	int nContactID = GetCurrentFavoriteID();
	if (nContactID<=0)
		return;

	Status err;
	ClientContactManager::RemoveFavorite(err,nContactID);
	_CHK_ERR(err);
	ReloadData();

}

void Selection_ContactsAndFavorites::OnReload()
{
	int nPrev=GetCurrentFavoriteID();
	ReloadData(nPrev,true);
}

void Selection_ContactsAndFavorites::OnAddPicToFav()
{
	int nContactID=GetCurrentFavoriteID();
	if (nContactID>0)
	{
		Status err;
		ClientContactManager::AddPictureFavorite(err,nContactID);
		_CHK_ERR(err);
		ReloadData(nContactID); //get new pic
		g_ChangeManager.notifyObservers(ChangeManager::GLOBAL_CONTACT_FAVORITE_ADD_PICTURE,nContactID,QVariant(),this);
	}

}

void Selection_ContactsAndFavorites::OncenterIndexDoubleClicked(int index)
{
	notifyObservers(SelectorSignals::SELECTOR_ON_DOUBLE_CLICK,GetCurrentFavoriteID());
}


void Selection_ContactsAndFavorites::OncenterIndexSelectedForEditing(int index)
{
	if (m_nCurrentCenterIndex>=0 && m_nCurrentCenterIndex<m_recContactData.getRowCount() && m_pStackedWidget->currentIndex()==1)
		emit FavoriteChanged(m_recContactData.getDataRef(m_nCurrentCenterIndex,"BCNT_ID").toInt());
}


void Selection_ContactsAndFavorites::OnAddItemButton_clicked()
{
	OnAddFav();
}
bool Selection_ContactsAndFavorites::IsContactFrameVisible()
{
	return (bool)(m_pStackedWidget->currentIndex()==0);

}