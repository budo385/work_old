#ifndef ACCUSERRECORDSELECTOR_H
#define ACCUSERRECORDSELECTOR_H

#include <QWidget>
#include "generatedfiles/ui_accuserrecordselector.h"
#include "common/common/dbrecordset.h"

class AccUserRecordSelector : public QWidget
{
	Q_OBJECT

public:
	AccUserRecordSelector(QWidget *parent = 0);
	~AccUserRecordSelector();

	void Load(int nRecordId,int nTableID,DbRecordSet *lstUAR=NULL,DbRecordSet *lstGAR=NULL);
	void Save(int nRecordId,int nTableID,DbRecordSet *lstUAR=NULL,DbRecordSet *lstGAR=NULL);
	void CancelChanges();
	void Invalidate();
	void SetRecordDisplayName(QString strName){m_strRecordDisplayName=strName;};
	void HideLabel();
	void SetAccessDisplayLevel(int nAccDisplayLevel);
	void SetEditMode(bool bEdit=true);
	
signals:
	void UpdateRecordDisplayName(); //when dialog is about to open, fire this to rf record name

private slots:
	void on_comboBox_currentIndexChanged(int nIndex);
	void on_btnUAR_clicked();
private:

	Ui::AccUserRecordSelectorClass ui;
	
	QString m_strRecordDisplayName;
	int m_nRecordId;
	int m_nTableID;
	int m_nLoggedPersonID;
	DbRecordSet m_lstUAR;
	DbRecordSet m_lstGAR;
	//internal cache:
	int m_nRecordIdCache;
	int m_nTableIDCache;
	DbRecordSet m_lstUARCache;
	DbRecordSet m_lstGARCache;
	bool	m_bEdit;
};

#endif // ACCUSERRECORDSELECTOR_H
