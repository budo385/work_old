#include "httplookup.h"
#include <QtWidgets/QMessageBox>
#include "trans/trans/qhttp.h"
#include <QUrl>
#include <QTime>
#include <QDebug>
#include <QBuffer>
#include <QCoreApplication>
#include <QEventLoop>
#include <QProgressDialog>
#include "common/common/sleeper.h"
#include "bus_client/bus_client/zipselectordlg.h"

HttpLookup::HttpLookup()
{
}

HttpLookup::~HttpLookup()
{
}

bool HttpLookup::WebLookupZIPTown(QString &strZIP, QString &strTown, QString &strCountryCode)
{
	//initial check
	if(!strZIP.isEmpty() && !strTown.isEmpty()){
		QMessageBox::information(NULL, QObject::tr("Invalid Research"), QObject::tr("Neither ZIP nor Town fields are empty!"));
		return false;
	}
	if(strZIP.isEmpty() && strTown.isEmpty()){
		QMessageBox::information(NULL, QObject::tr("Invalid Research"), QObject::tr("Either ZIP or Town have to be entered to find the other!"));
		return false;
	}

	QApplication::setOverrideCursor(Qt::WaitCursor);

	//HTTP request
	//http://ws.geonames.org/postalCodeSearch?style=LONG&placename=Split
	//http://ws.geonames.org/postalCodeSearch?style=LONG&postalcode=21000
	QHttp *http = new QHttp("zafon.ws.geonames.org", QHttp::ConnectionModeHttp);
	QString strPage("/postalCodeSearch?style=FULL&"); //TOFIX FULL style?
	if(!strZIP.isEmpty())
		strPage += "postalcode=" + strZIP;
	else
		strPage += "placename=" + QUrl::toPercentEncoding(strTown);

	//progress dialog
	QSize size(300,100);
	QProgressDialog progress(QObject::tr("Contacting Web Service..."),QObject::tr("Cancel"),0,0,NULL);
	progress.setWindowTitle(QObject::tr("Operation In Progress"));
	progress.setMinimumSize(size);
	progress.setWindowModality(Qt::NonModal);
	progress.setMinimumDuration(300);//.3s before pops up

	QBuffer httpResponse;
	int nReqID = http->get(strPage /*QUrl::toPercentEncoding(strPage)*/, &httpResponse);

	//wait up to 6 seconds for the result
	QTime timer;
	timer.start();
	bool bResult = false;
	while(1){
		QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents|QEventLoop::WaitForMoreEvents, 10);
		progress.setValue(1);

		if(0 == http->currentId())	//request finished?
			break;
		if(timer.elapsed() > 6000)	//timeout elapsed?
			break;
	}
	if(http->error() != 0){
		qDebug() << "ZIP lookup error:" << http->errorString();	
	}
	else
		bResult = true;
	delete http;
	progress.close();

	QApplication::changeOverrideCursor(Qt::ArrowCursor);

	QString strData = QString::fromUtf8(httpResponse.data().data());
	if(strData.isEmpty())
		bResult = false;

	if(!bResult){
		QMessageBox::information(NULL, QObject::tr("Invalid Research"), QObject::tr("The ZIP/Town server could not be reached. Please try again later!"));
		return false;
	}

	qDebug() << "ZIP Lookup (" << timer.elapsed() << "ms elapsed) result:" << httpResponse.data();

	//Send XML to the picker dialog
	ZipSelectorDlg dlg(strData, strCountryCode);
	if(dlg.exec() > 0)
	{
		if(strZIP.isEmpty())
			strZIP = dlg.getSelectedZIP();
		else
			strTown = dlg.getSelectedTown();
		strCountryCode = dlg.getSelectedCountryCode();

		return true;
	}
	return false;
}