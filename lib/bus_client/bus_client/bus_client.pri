# ----------------------------------------------------
# This file is generated by the Qt Visual Studio Add-in.
# ------------------------------------------------------

# This is a reminder that you are using a generated .pro file.
# Remove it when you are finished editing this file.
message("You are running qmake on a generated .pro file. This may not work!")


HEADERS += ./clientapihelper.h \
    ./calendarreservationwidget.h \
    ./clientcontactmanager.h \
    ./clientdocumenthandler.h \
    ./clientdownloadmanager.h \
    ./clientinifile.h \
    ./clientoptionsandsettingsmanager.h \
    ./clientremindermanager.h \
    ./commgridviewhelper_client.h \
    ./commgridviewtablehelper_client.h \
    ./dlg_copyentity.h \
    ./dlg_downloadmanager.h \
    ./dlg_orderform.h \
    ./dlg_reminders.h \
    ./dlg_storagelocation.h \
    ./dlg_templatedb.h \
    ./dlg_trialpopup.h \
    ./documenthelper.h \
    ./generatecodedlg.h \
    ./httplookup.h \
    ./languagewarningdlg.h \
    ./messagedispatcher_client.h \
    ./servermessagehandlerset.h \
    ./storedprojlistsdlg.h \
    ./storedprojlistseditor.h \
    ./versionparser.h \
    ./zipselectordlg.h \
    ./customcollectionswnd.h \
    ./customselectionswnd.h \
    ./client_global_objects_create.h \
    ./changemanager.h \
    ./clientbackupmanager.h \
    ./clientimportexportmanager.h \
    ./clientmanagerext.h \
    ./modulelicenseclientcache.h \
    ./mainentityselectioncontroller.h \
    ./selection_actualorganization.h \
    ./selection_gridviews.h \
    ./selectionpopupfloat.h \
    ./selectorsignals.h \
    ./contactbuildall_searchfields.h \
    ./dlg_contactbuildall.h \
    ./dlg_contactbuildall_embedded.h \
    ./phoneselectordlg.h \
    ./selection_acp.h \
    ./selection_combobox.h \
    ./selection_contactbutton.h \
    ./selection_contacts.h \
    ./selection_contactsandfavorites.h \
    ./selection_edittemplatetree.h \
    ./selection_grouptree.h \
    ./selection_nm_sapne.h \
    ./selection_recordselector.h \
    ./selection_sapne.h \
    ./selection_schedulebutton.h \
    ./selection_tablebased.h \
    ./selection_treebased.h \
    ./selection_users.h \
    ./selectionpopup.h \
    ./table_selectioncontacts.h \
    ./table_selectiontablebased.h \
    ./toolbar_dateselector.h \
    ./tree_selectiongroup.h \
    ./accrightssetlistmodel.h \
    ./accrightstreemodel.h \
    ./roleaccrghtsdefmodel.h \
    ./roledeftreemodel.h \
    ./rolelistmodel.h \
    ./accrightsetdelegate.h \
    ./accuserrecordgrouptree.h \
    ./accuserrecordselector.h \
    ./accuserrecordusertable.h \
    ./dlg_accuserrecord.h \
    ./useraccessright_client.h \
    ./email_attachmenttable.h \
    ./email_recepienttable.h \
    ./emailhelper.h \
    ./emailselectordialog.h \
    ./simpleselectioncheckboxwizpage.h \
    ./simpleselectionwizfileselection.h \
    ./simpleselectionwizfileselectionexp.h \
    ./simpleselectionwizpage.h \
    ./simpleselectionwiztree.h
SOURCES += ./calendarreservationwidget.cpp \
    ./clientapihelper.cpp \
    ./clientcontactmanager.cpp \
    ./clientdocumenthandler.cpp \
    ./clientdownloadmanager.cpp \
    ./clientinifile.cpp \
    ./clientoptionsandsettingsmanager.cpp \
    ./clientremindermanager.cpp \
    ./commgridviewhelper_client.cpp \
    ./commgridviewtablehelper_client.cpp \
    ./customcollectionswnd.cpp \
    ./customselectionswnd.cpp \
    ./dlg_copyentity.cpp \
    ./dlg_downloadmanager.cpp \
    ./dlg_orderform.cpp \
    ./dlg_reminders.cpp \
    ./dlg_storagelocation.cpp \
    ./dlg_templatedb.cpp \
    ./dlg_trialpopup.cpp \
    ./documenthelper.cpp \
    ./generatecodedlg.cpp \
    ./httplookup.cpp \
    ./languagewarningdlg.cpp \
    ./messagedispatcher_client.cpp \
    ./storedprojlistsdlg.cpp \
    ./storedprojlistseditor.cpp \
    ./versionparser.cpp \
    ./changemanager.cpp \
    ./clientbackupmanager.cpp \
    ./clientimportexportmanager.cpp \
    ./clientmanagerext.cpp \
    ./modulelicenseclientcache.cpp \
    ./contactbuildall_searchfields.cpp \
    ./dlg_contactbuildall.cpp \
    ./dlg_contactbuildall_embedded.cpp \
    ./mainentityselectioncontroller.cpp \
    ./phoneselectordlg.cpp \
    ./selection_acp.cpp \
    ./selection_actualorganization.cpp \
    ./selection_combobox.cpp \
    ./selection_contactbutton.cpp \
    ./selection_contacts.cpp \
    ./selection_contactsandfavorites.cpp \
    ./selection_edittemplatetree.cpp \
    ./selection_gridviews.cpp \
    ./selection_grouptree.cpp \
    ./selection_nm_sapne.cpp \
    ./selection_recordselector.cpp \
    ./selection_sapne.cpp \
    ./selection_schedulebutton.cpp \
    ./selection_tablebased.cpp \
    ./selection_treebased.cpp \
    ./selection_users.cpp \
    ./selectionpopup.cpp \
    ./selectionpopupfloat.cpp \
    ./table_selectioncontacts.cpp \
    ./table_selectiontablebased.cpp \
    ./toolbar_dateselector.cpp \
    ./tree_selectiongroup.cpp \
    ./zipselectordlg.cpp \
    ./accrightsetdelegate.cpp \
    ./accrightssetlistmodel.cpp \
    ./accrightstreemodel.cpp \
    ./accuserrecordgrouptree.cpp \
    ./accuserrecordselector.cpp \
    ./accuserrecordusertable.cpp \
    ./dlg_accuserrecord.cpp \
    ./roleaccrghtsdefmodel.cpp \
    ./roledeftreemodel.cpp \
    ./rolelistmodel.cpp \
    ./useraccessright_client.cpp \
    ./email_attachmenttable.cpp \
    ./email_recepienttable.cpp \
    ./emailhelper.cpp \
    ./emailselectordialog.cpp \
    ./simpleselectioncheckboxwizpage.cpp \
    ./simpleselectionwizfileselection.cpp \
    ./simpleselectionwizfileselectionexp.cpp \
    ./simpleselectionwizpage.cpp \
    ./simpleselectionwiztree.cpp
FORMS += ./accuserrecordselector.ui \
    ./dlg_accuserrecord.ui \
    ./dlg_copyentity.ui \
    ./dlg_downloadmanager.ui \
    ./dlg_orderform.ui \
    ./dlg_reminders.ui \
    ./dlg_storagelocation.ui \
    ./dlg_templatedb.ui \
    ./dlg_trialpopup.ui \
    ./languagewarningdlg.ui \
    ./storedprojlistsdlg.ui \
    ./storedprojlistseditor.ui \
    ./customselectionswnd.ui \
    ./customcollectionswnd.ui \
    ./contactbuildall_searchfields.ui \
    ./dlg_contactbuildall.ui \
    ./dlg_contactbuildall_embedded.ui \
    ./emailselectordialog.ui \
    ./generatecodedlg.ui \
    ./phoneselectordlg.ui \
    ./selection_acp.ui \
    ./selection_contacts.ui \
    ./selection_contactsandfavorites.ui \
    ./selection_edittemplatetree.ui \
    ./selection_grouptree.ui \
    ./selection_nm_sapne.ui \
    ./selection_recordselector.ui \
    ./selection_sapne.ui \
    ./selection_tablebased.ui \
    ./selection_treebased.ui \
    ./selection_users.ui \
    ./selectionpopup.ui \
    ./toolbar_dateselector.ui \
    ./zipselectordlg.ui \
    ./simpleselectioncheckboxwizpage.ui \
    ./simpleselectionwizfileselection.ui \
    ./simpleselectionwizfileselectionexp.ui \
    ./simpleselectionwizpage.ui
