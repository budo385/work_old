#ifndef SELECTION_GRIDVIEWS_H
#define SELECTION_GRIDVIEWS_H

#include "bus_client/bus_client/selection_combobox.h"


class Selection_GridViews : public Selection_ComboBox
{
public:
	void RefreshDisplay(int nAction=REFRESH_RELOAD_ALL, int nRowInsideDataSource=-1);

private:
	
};

#endif // SELECTION_GRIDVIEWS_H
