#ifndef CLIENTDONWLOADMANAGER_H
#define CLIENTDONWLOADMANAGER_H

#include <QObject>
#include <QtWidgets/QSystemTrayIcon>
#include "bus_trans_client/bus_trans_client/userstoragehttpclient.h"
#include "dlg_downloadmanager.h"
#include "common/common/dbrecordset.h"

/*
	should be public:
*/
class ClientDownloadManager : public QObject
{
	Q_OBJECT

public:
	ClientDownloadManager(QObject *parent=NULL);
	~ClientDownloadManager();

	void Initialize(int nMaxActiveOperations=1);
	void AbortAllOperations();
	void OnThemeChanged();
	bool IsOperationInProgress(){return (bool)GetCurrentActiveOperations()!=0;}

	void ScheduleDownload(QString strFilePath, bool bIsBackup, bool bOverWrite=true);
	void ScheduleUpload(QString strFilePath, bool bIsBackup, bool bOverWrite=true);
	void ScheduleCheckOutDocument(int nDocumentID,QString strFilePath,bool bReadOnly,bool bAskForLocation=false,bool bSkipLock=false,bool bDoNotTrackCheckOutDoc=false,QString strDefaultDirPath="", int nRevisionID=-1);
	void ScheduleCheckInDocument(int nDocumentID,QString strFilePath,QString strTag="",bool bOverWrite=false,bool bSkipLock=false, int nAskForTag=-1, bool bDeleteDocumentWhenCancel=false,int nOpenDocumentAfterUpload_FUIMode=-1);

signals:
	void OperationCompleted(Status err,QString strFilePath);
	void CheckInOutCompleted(Status err,int nDocumentID,QString strFilePath,DbRecordSet recRev,DbRecordSet recCheckOutInfo);

public slots:
	void ClearAll();
	void OnShowDlg();
	void OnHideDlg();

private slots:
	void OnOpenFile(int nFileID);
	void OnOpenDirectory(int nFileID);
	void OnRemoveFile(int nFileID);
	void OnCancel(int nFileID);
	void OnOperationEnd(int nFileID, int nStatusID,QString strStatus);
	void OnSysMessageClicked();
	void OnSysMessageActivate(QSystemTrayIcon::ActivationReason);
	
private:
	int GetCurrentActiveOperations();
	void ProcessEndFile(int nFileID, Status err);
	void ProcessEndCheckInOut(int nFileID, Status err);
	void SysTrayNotifyUser(Status err,int nFileID);
	void ErrorHandlerOnCheckOut(Status err,int nFileID,int nDocumentID,QString strFile, bool bReadOnly,bool bSkipLock,DbRecordSet &recRev,DbRecordSet &recInfo, bool bSkipFileDelete=false);

	void ExecuteNextInQue();
	void ActivateAndPositionDlg();
	void CheckDlg();

	QMap<int,QThread*>					m_hshActiveDownloadThreads;
	QMap<int,DownloadWidget *>			m_hshActiveDownloadWidgets;
	Dlg_DownloadManager *m_Dlg;
	int m_nMaxActiveOperations;
	QSystemTrayIcon *m_sysTray;
	
};

#endif // CLIENTDONWLOADMANAGER_H
