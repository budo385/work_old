#include "clientapihelper.h"
#include <QString>
#include <QCoreApplication>
#include <QProgressDialog>
#include "common/common/csvimportfile.h"
#include "db_core/db_core/dbsqltableview.h"
#include "db_core/db_core/dbtableiddefinition.h"
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;
#include "common/common/logger.h"
extern Logger g_Logger;					//global logger

bool CsvProgress1(unsigned long nUserData)
{
	QProgressDialog *pDlg = (QProgressDialog *)nUserData;
	Q_ASSERT(NULL != pDlg);
	
	if(pDlg){
		pDlg->setValue(1);
		qApp->processEvents();
		if (pDlg->wasCanceled())
			return false;	// abort
	}
	return true;
}

//MR: simple replacement for MainEntitySelectionController for two field lookup (loading only those fields)
class SimpleTableCache {
public:
	SimpleTableCache(){};
	bool Load(const QString &strTable, const QString &strKeyField, const QString &strValueField, Status &pStatus);
	QVariant GetValue(const QVariant &key);

protected:
	DbRecordSet m_data;
};

bool SimpleTableCache::Load(const QString &strTable, const QString &strKeyField, const QString &strValueField, Status &pStatus)
{
	QString strQuery = QString("SELECT %1, %2 FROM %3").arg(strKeyField).arg(strValueField).arg(strTable);
	_SERVER_CALL(ClientSimpleORM->ExecuteSQL(pStatus, strQuery, m_data))
	if(pStatus.IsOK()){
		return true;
	}		
	return false;
}

QVariant SimpleTableCache::GetValue(const QVariant &key)
{
	int nRow = m_data.find(0, key, true);
	if(nRow >= 0){
		return m_data.getDataRef(nRow, 1);
	}
	return QVariant();
}

// rewrite of Service_BusImport::ProcessStoredProjectList_Import for client side (3rd version of the same code :( )
void ClientApiHelper::ProcessStoredProjectList_Import(Status &Ret_pStatus, QByteArray datHeader, QByteArray datDetail)
{
	CsvImportFile file;

	QSize size(300,100);
	QProgressDialog progress(QObject::tr("Loading data ..."),QObject::tr("Cancel"),0,0,NULL);
	progress.setWindowTitle(QObject::tr("Import In Progress"));
	progress.setMinimumSize(size);
	progress.setWindowModality(Qt::NonModal);
	progress.setMinimumDuration(1200);//2.4s before pops up
	progress.setCancelButton(NULL);

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Import Stored Project List - start"));

	//STEP 1: load files
	DbRecordSet lstHeaderData;
	file.LoadFromBlob(Ret_pStatus, datHeader, lstHeaderData, QStringList(), QStringList(), true, CsvProgress1, (unsigned long)&progress);
	if(!Ret_pStatus.IsOK()){
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Import Stored Project List - end (%1)").arg(Ret_pStatus.getErrorText()));
		return;
	}
	//lstHeaderData.Dump();

	int nColCnt = lstHeaderData.getColumnCount();
	if(nColCnt != 8){
		Ret_pStatus.setError(1, QString("Invalid column count in header file data (%1 instead of 8)").arg(nColCnt));
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Import Stored Project List - end (%1)").arg(Ret_pStatus.getErrorText()));
		return;
	}

	DbRecordSet lstDetailsData;
	file.LoadFromBlob(Ret_pStatus, datDetail, lstDetailsData, QStringList(), QStringList(), true, CsvProgress1, (unsigned long)&progress);
	if(!Ret_pStatus.IsOK()){
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Import Stored Project List - end1 (%1)").arg(Ret_pStatus.getErrorText()));
		return;
	}

	nColCnt = lstDetailsData.getColumnCount();
	if(nColCnt != 9){
		Ret_pStatus.setError(1, QString("Invalid column count in details file data (%1 instead of 9)").arg(nColCnt));
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Import Stored Project List - end (%1)").arg(Ret_pStatus.getErrorText()));
		return;
	}

	//STEP 2: write header data
	
	//SPL Main Data: AKA "header" file import is defined by MB like this:
	//PL_CODE			Code		Mostly, this is a name. Spaces etc. are allowed
	//PL_NAME						Mostly empty, unless the list was defined in Timesheet
	//PL_PERSON			Pers.No.	If a pers.no. is defined, only this user can see this SPL. It is hidden for all others.
	//PL_ABTEILUNG		Department	If a pers.no. is defined, only users from this department or subdepartments of it can see this SPL. It is hidden for all others.
	//PL_INKL_SEL		1: Is Selection
	//PL_SEL_KRIT		Selection criteria	XML format, to be converted to SQL when needed.
	//PL_SAMMELLISTE	1: Is Filter
	//PL_LST_SUBLISTS	List of sub-SPLs	XML format
	//-------------------------------------------------------------------------------
	//This input is to be stored into BUS_STORED_PROJECT_LIST as follows:
	//
	//BSPL_NAME -> trimmed(PL_CODE +" "+ PL_NAME)  /* PL_NAME je najcesce prazan */
	//BSPL_USEBY_PERSON_ID -> PL_PERSON (search BUS_PERSON by BPER_CODE = PL_PERSON) if not found then set NULL, but report error on console (qdebug)
	//BSPL_USEBY_DEPT_ID -> PL_ABTEILUNG (search BUS_DEPARTMENTS by BDEPT_CODE = PL_ABTEILUNG) if not found then set NULL
	//BSPL_IS_SELECTION -> PL_INKL_SEL
	//BSPL_SELECTION_DATA -> PL_SEL_KRIT
	//BSPL_IS_FILTER -> PL_SAMMELLISTE
	//BSPL_FILTER_DATA -> PL_LST_SUBLISTS
	
	DbRecordSet lstData;
	lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_STORED_PROJECT_LIST));

	SimpleTableCache PersonCache;
	PersonCache.Load("BUS_PERSON", "BPER_CODE", "BPER_ID", Ret_pStatus);

	SimpleTableCache DepartmentCache;
	DepartmentCache.Load("BUS_DEPARTMENTS", "BDEPT_CODE", "BDEPT_ID", Ret_pStatus);
	
	progress.setRange(0, lstHeaderData.getRowCount() + lstDetailsData.getRowCount());
	progress.setLabelText(QObject::tr("Importing data ..."));

	//get currently available lists to prevent duplicate names	
	DbRecordSet recLists;
	_SERVER_CALL(StoredProjectLists->GetListNames(Ret_pStatus, recLists));

	int nHdrRowCnt = lstHeaderData.getRowCount();
	int nRowCnt = lstHeaderData.getRowCount();
	int i;
	int nSkipped = 0;
	for(i=0; i<nRowCnt; i++)
	{
		progress.setValue(i);
		qApp->processEvents();

		//TOFIX set BSPL_OWNER to some value?
		QString strName = lstHeaderData.getDataRef(i, 0).toString();
		if(!lstHeaderData.getDataRef(i, 1).toString().isEmpty()){
			strName += " ";
			strName += lstHeaderData.getDataRef(i, 1).toString();
		}
		strName = strName.trimmed();	//trimmed(PL_CODE +" "+ PL_NAME)
		if(strName.isEmpty()){
			g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("ERROR: Skip importing header line: %1 (Empty code + name)").arg(i+1));
			qDebug() << "ERROR: Skip importing header line:" << i+1 << "(Empty code + name)";
			nSkipped ++;
			continue;
		}

		int nPersonID = -1;	//search BUS_PERSON by BPER_CODE = PL_PERSON
		QString strPersonCode = lstHeaderData.getDataRef(i, 2).toString();
		if(!strPersonCode.isEmpty()){
			nPersonID = PersonCache.GetValue(strPersonCode).toInt();
			if (nPersonID < 1){
				g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("ERROR: Could not find person id with code: [%1], header line: %2 (will import line)").arg(strPersonCode).arg(i+1));
				//qDebug() << QString("ERROR: Could not find person id with code: [%1], header line: %2").arg(strPersonCode).arg(i+1);
			}
		}
		int nDepartmentID = -1;
		QString strDepartmentCode = lstHeaderData.getDataRef(i, 3).toString();
		if(!strDepartmentCode.isEmpty()){
			nDepartmentID = DepartmentCache.GetValue(strDepartmentCode).toInt();
			if (nDepartmentID < 1){
				g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("ERROR: Could not find department id with code: [%1], header line: %2 (will import line)").arg(strDepartmentCode).arg(i+1));
				//qDebug() << QString("ERROR: Could not find department id with code: [%1], header line: %2").arg(strDepartmentCode).arg(i+1);
			}
		}

		lstData.addRow();
		int nIdx = i-nSkipped;

		//#2693: if project list with this name already exists, overwrite it (reusing the ID of existing list)
		int nRow = recLists.find("BSPL_NAME", strName, true);
		if(nRow >= 0){
			int nTreeID = recLists.getDataRef(nRow, "BSPL_ID").toInt();
			lstData.setData(nIdx, "BSPL_ID", nTreeID);

			//delete previous list content
			DbRecordSet lstDummy;
			QString strSql = QString("DELETE FROM BUS_STORED_PROJECT_LIST_DATA WHERE BSPLD_TREE_ID=%1").arg(nTreeID);
			_SERVER_CALL(ClientSimpleORM->ExecuteSQL(Ret_pStatus, strSql, lstDummy));
		}

		lstData.setData(nIdx, "BSPL_NAME",				strName);
		if (nPersonID > 0)			lstData.setData(nIdx, "BSPL_USEBY_PERSON_ID",	nPersonID);
		if (nDepartmentID > 0)		lstData.setData(nIdx, "BSPL_USEBY_DEPT_ID",	nDepartmentID);
		lstData.setData(nIdx, "BSPL_IS_SELECTION",		lstHeaderData.getDataRef(i, 4).toInt());
		lstData.setData(nIdx, "BSPL_SELECTION_DATA",	lstHeaderData.getDataRef(i, 5).toString());
		lstData.setData(nIdx, "BSPL_IS_FILTER",			lstHeaderData.getDataRef(i, 6).toInt());
		lstData.setData(nIdx, "BSPL_FILTER_DATA",		lstHeaderData.getDataRef(i, 7).toString());
	}

	//write data into the database
	QString pLockResourceID;
	int nQueryView = -1;
	int nSkipLastColumns = 0; 
	DbRecordSet lstForDelete;
	_SERVER_CALL(ClientSimpleORM->Write(Ret_pStatus, BUS_STORED_PROJECT_LIST, lstData, pLockResourceID, nQueryView, nSkipLastColumns, lstForDelete))
	if(!Ret_pStatus.IsOK()){
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Import Stored Project List - end (%1)").arg(Ret_pStatus.getErrorText()));
		return;
	}

	//STEP 3: write details data

	//SPL Details: AKA "details" file import is defined by MB like this:
	//PL_CODE			Code		Mostly, this is a name. Spaces etc. are allowed
	//PL_NAME						Mostly empty, unless the list was defined in Timesheet
	//PR_CODE			Project Code
	//PR_AKTIV			0=not active 1=active	Currently used to mark inactive projects in the tree
	//T_LIST_ELEM_STATE	0=no subelements 1=Collapsed (+) 2=Expanded (-)
	//PR_STUFE			Level					Starts with 0
	//PR_EX_UNTERPROJ	0=no subelements in DB	1=subelements exist in DB
	//T_MAIN_NODE		Project code of parent node
	//PR_STYLE			Styling					Used to display icons etc. depending on project type (not yet used in SPC-Q7)
	//----------------------------------------------------------------------------
	//These records are copied directly to BUS_STORED_PROJECT_LIST_DATA with following algorithm
	//PR_CODE -> BSPLD_PROJECT_ID (find BUSP_ID by PR_CODE in BUS_PROJECT table and set it here)
	//T_LIST_ELEM_STATE-> BSPLD_EXPANDED

	lstData.destroy();
	lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_STORED_PROJECT_LIST_DATA));

	SimpleTableCache ProjectListCache;
	ProjectListCache.Load("BUS_STORED_PROJECT_LIST", "BSPL_NAME", "BSPL_ID", Ret_pStatus);

	SimpleTableCache ProjectCache;
	ProjectCache.Load("BUS_PROJECTS", "BUSP_CODE", "BUSP_ID", Ret_pStatus);

	nSkipped = 0;
	nRowCnt = lstDetailsData.getRowCount();

	for(int z=0; z<nRowCnt; z++)
	{
		progress.setValue(nHdrRowCnt+z);
		qApp->processEvents();

		QString strName = lstDetailsData.getDataRef(z, 0).toString();	
		if(!lstDetailsData.getDataRef(z, 1).toString().isEmpty()){
				strName += " ";
				strName += lstDetailsData.getDataRef(z, 1).toString();
		}
		strName = strName.trimmed();	//trimmed(PL_CODE +" "+ PL_NAME)
		if(strName.isEmpty()){
			g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("ERROR: Skip importing detail line: %1 (Empty code + name)").arg(z+1));
			//qDebug() << "ERROR: Skip importing detail line:" << z+1 << "(Empty code + name)";
			nSkipped ++;
			continue;
		}

		int nTreeID = -1;	//search BUS_PERSON by BPER_CODE = PL_PERSON
		QString strProjListName = strName;
		if(!strProjListName.isEmpty()){
			nTreeID = ProjectListCache.GetValue(strProjListName).toInt();
		}
		if (nTreeID < 1){
			g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("ERROR: Skip importing detail line: %1 (project list not found using name: [%2])").arg(z+1).arg(strProjListName));
			//qDebug() << "ERROR: Skip importing detail line:" << z+1 << QString("(project list not found using name: [%1])").arg(strProjListName);
			nSkipped ++;
			continue;
		}

		int nProjectID = -1;
		QString strProjectCode = lstDetailsData.getDataRef(z, 2).toString();
		if(!strProjectCode.isEmpty()){
			nProjectID = ProjectCache.GetValue(strProjectCode).toInt();
		}
		if (nProjectID < 1){
			g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("ERROR: Skip importing detail line: %1 (project not found using code: [%2])").arg(z+1).arg(strProjectCode));
			//qDebug() << "ERROR: Skip importing detail line:" << z+1 << QString("(project not found using code: [%1])").arg(strProjectCode);
			nSkipped ++;
			continue;
		}

		//if this exact project already exists in that list, overwrite it
		DbRecordSet lstExisting;
		QString strQuery = QString("SELECT BSPLD_ID FROM BUS_STORED_PROJECT_LIST_DATA WHERE BSPLD_TREE_ID=%1 AND BSPLD_PROJECT_ID=%2").arg(nTreeID).arg(nProjectID);
		_SERVER_CALL(ClientSimpleORM->ExecuteSQL(Ret_pStatus, strQuery, lstExisting))
		if(!Ret_pStatus.IsOK())
		{
			QString strErr=Ret_pStatus.getErrorText();
			Ret_pStatus.setError(1, strErr);
			return;
		}
		int nID = -1;
		int nCnt = lstExisting.getRowCount();
		if(nCnt>0){
			nID = lstExisting.getDataRef(0, "BSPLD_ID").toInt();

			//if duplicates exist (there were no checks before in the code), delete all except the first item
			if(nCnt > 1){
				for(int i=1; i<nCnt; i++){
					int nRowID = lstExisting.getDataRef(i, "BSPLD_ID").toInt();
					QString strSql = QString("DELETE FROM BUS_STORED_PROJECT_LIST_DATA WHERE BSPLD_ID=%1").arg(nRowID);
					_SERVER_CALL(ClientSimpleORM->ExecuteSQL(Ret_pStatus, strSql, lstExisting));
				}
			}
		}

		lstData.addRow();
		
		int nIdx = lstData.getRowCount()-1;
		if(nID > 0) lstData.setData(nIdx, "BSPLD_ID", nID);		//overwrites existing item
		lstData.setData(nIdx, "BSPLD_TREE_ID",		nTreeID);
		lstData.setData(nIdx, "BSPLD_PROJECT_ID",	nProjectID);
		lstData.setData(nIdx, "BSPLD_EXPANDED",		lstDetailsData.getDataRef(z, 4).toInt());

		//B.T: breaks on large import:
		//go by chunks of 10000
		if (nIdx==10000 || z==nRowCnt-1)
		{
			qDebug()<<"Writing chunk to "<<z;
			//write data into the database
			QString pLockResourceID;
			int nQueryView = -1;
			int nSkipLastColumns = 0; 
			DbRecordSet lstForDelete;
			//lstData.Dump();
			_SERVER_CALL(ClientSimpleORM->Write(Ret_pStatus, BUS_STORED_PROJECT_LIST_DATA, lstData, pLockResourceID, nQueryView, nSkipLastColumns, lstForDelete))
			if(!Ret_pStatus.IsOK()){
				g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Import Stored Project List - end (%1)").arg(Ret_pStatus.getErrorText()));
				return;
			}

			lstData.clear();
		}
	}

	//any uncommitted row
	if(lstData.getRowCount() > 0){
		//write data into the database
		QString pLockResourceID;
		int nQueryView = -1;
		int nSkipLastColumns = 0; 
		DbRecordSet lstForDelete;
		_SERVER_CALL(ClientSimpleORM->Write(Ret_pStatus, BUS_STORED_PROJECT_LIST_DATA, lstData, pLockResourceID, nQueryView, nSkipLastColumns, lstForDelete))
		if(!Ret_pStatus.IsOK()){
			g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Import Stored Project List - end (%1)").arg(Ret_pStatus.getErrorText()));
			return;
		}
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Import Stored Project List - end"));
}
