#ifndef SELECTION_ACP_H
#define SELECTION_ACP_H


#include <QtWidgets/QFrame>
#ifndef WINCE
	#include "generatedfiles/ui_selection_acp.h"
#else
	#include "embedded_core/embedded_core/generatedfiles/ui_selection_acp.h"
#endif
#include "bus_client/bus_client/selectionpopup.h"
#include "common/common/observer_ptrn.h"
#include "gui_core/gui_core/guidatamanipulator.h"
#include "bus_client/bus_client/mainentityselectioncontroller.h"


/*!
	\class  Selection_ACP
	\brief  1N assignment: ACP: user enters new items in combobox
	\ingroup GUICore

	NOTE: Two modes of use:
	- provide ID field: new name is automatically save on server, new id is returnes
	- provide NAME field: new name is NOT saved on server, just filled inside extern record

*/
class Selection_ACP : public QFrame,public ObsrPtrn_Subject, public ObsrPtrn_Observer
{
	Q_OBJECT

public:
	Selection_ACP(QWidget *parent = 0);
	virtual ~Selection_ACP();


	enum Button
	{
		BUTTON_MODIFY,			
		BUTTON_ADD,	
		BUTTON_REMOVE,	
		BUTTON_SELECT		
	};

	//init
	void Initialize(int nEntityTypeID);																						//simple
	void Initialize(int nEntityTypeID,GuiDataManipulator *pDataManipulator,QString strColumnName, bool bColumnIsID=true);		//autofill from external data
	void Initialize(int nEntityTypeID,DbRecordSet *pDataSource,QString strColumnName, bool bColumnIsID=true);					//autofill from external data
	void SetFilterOnType(){m_bUseFilterOnType=true;};

	//main functions:
	void SetCurrentEntityRecord(int nEntityRecordID,bool bSkipNotifyObservers=false);			
	bool GetCurrentEntityRecord(int &nEntityRecordID,DbRecordSet &record);				//record as defined in selection controller (SELECT view for project, contacts,...)
	bool GetCurrentEntityRecord(int &nEntityRecordID);									//overloaded for convenience   
	QString GetCurrentDisplayName();													//directly gets display
	void SetCurrentDisplayName(QString strDisplayData,bool bSkipNotifyObservers=false);	//sets data from external source
	void Clear(bool bSkipNotifyObservers=false);


	//GUI functions:
	void blockSignals(bool b){blockObserverSignals(b);QFrame::blockSignals(b);}
	void SetEditMode(bool bEditMode);
	void SetSelectionButtonLabel(QString strSelButtonLabel);
	void DisableEntryText();
	QWidget* GetButton(int nButton);

	//only if external data provided
	void RefreshDisplay();	//data->gui
	void SetCurrentRow(unsigned int nRow){m_nCurrentRow=nRow;}; //set current row inside external data


	//return main sel. controller:
	MainEntitySelectionController* GetSelectionController(){return m_dlgCachedPopUpSelector;}

	//to catch data change event from selector controller
	void updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail=0,const QVariant val=QVariant());
	void setEnabled ( bool );	//override	
	void setDisabled ( bool );	//override
signals:
	void ComboTextChanged(const QString & text);
	void ComboTextEditFinish();

private:
	Ui::Selection_ACPClass ui;

	bool ACPSaveSelection();
	void ReloadComboBox(DbRecordSet *lstData);
	void SetEntityData(int nSelectedRecordID,DbRecordSet &rowSelected,bool bSkipNotifyObservers=false);
	void SetEntityData(QString strNewDisplayData,bool bSkipNotifyObservers=false);

	//current selection data is stored inside:
	DbRecordSet m_RowCurrentSelectedEntity;						//< list as datasource of combo


	//external data  (optional)
	GuiDataManipulator *m_pExternDataManipulator;		//< data source manager
	DbRecordSet *m_pExternData;
	int m_nIDColIdx;									//< id col in extern data
	int m_nNameColIdx;									//< ACP display column
	int	m_nCurrentRow;									//< row inside extern data source in which to change data

	int m_nEntityTypeID;
	SelectionPopup m_dlgPopUp;
	MainEntitySelectionController *m_dlgCachedPopUpSelector;		//cached selector controller, 2nd time when open it will not go on server for data...

	QString m_strHtmlTag;
	QString m_strEndHtmlTag;
	bool m_bRefreshFromExternalDataInProgress;
	bool m_bUseFilterOnType;


private slots:
		void on_cmbName_editTextChanged(const QString &text);
		void on_cmbName_currentIndexChanged(const QString &text);
		void onCmbNameLine_editingFinished();

		void on_btnRemove_clicked();
		void on_btnSelect_clicked();
};


#endif // SELECTION_ACP_H
