#ifndef SELECTION_GROUPTREE_H
#define SELECTION_GROUPTREE_H


#include <QtWidgets/QFrame>
#include <QMenu>
#include "common/common/status.h"
#ifdef WINCE
#include "embedded_core/embedded_core/generatedfiles/ui_selection_grouptree.h"
#else
#include "generatedfiles/ui_selection_grouptree.h"
#endif
#include "common/common/dbrecordset.h"
#include "bus_client/bus_client/mainentityselectioncontroller.h"



/*!
	\class  Selection_GroupTree
	\brief  Selection for group tree pattern: more tree's of hierarchical groups
	\ingroup GUICore

	Note: this selection does not use cache, nor reacts on cache changes..
	Infact, only reason to be inherited from MainEntitySelectionController is that is used in SAPNE pattern


*/
class Selection_GroupTree : public QFrame, public MainEntitySelectionController
{
    Q_OBJECT

public:
	
	enum GroupObserverSignals				
	{
		//GROUP_SELECTION_CHANGED=100,	
		GROUP_ADD_SELECTED=100,				
		GROUP_REMOVE_SELECTED,		
		GROUP_REPLACE_SELECTED,		
		GROUP_INTERSECT_SELECTED,		
		GROUP_ADD_WHOLE_LIST,		
		GROUP_REPLACE_WHOLE_LIST
	};


	//global signals: group deleted, group changed

    Selection_GroupTree(QWidget *parent = 0);
    ~Selection_GroupTree();

	void Initialize(int nEntityID,bool bSkipLoadingData=false,bool bIsFUISelectorWidget=true,bool bSingleClickIsSelection=true,bool bMultiSelection=false,bool bEnableDrag=true); 
	void SetActualListDataSource(DbRecordSet *pLstData);
	bool IsEditMode(){return m_bEditMode;};
	int GetCurrentTreeID();
	int GetCurrentGroupID(int &nRow);
	void RefreshDisplay(int nAction=REFRESH_RELOAD_ALL, int nPrimaryKeyValue=-1);
	UniversalTreeWidget* GetTreeWidget(){return ui.treeWidget;};
	virtual void updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail=0,const QVariant val=QVariant());
	QString GetTreeName(int nTreeID);

	//override: do not use cache, always server
	void GetSelection(int &nEntityRecordID, QString &strCode, QString &strName,DbRecordSet& lstEntityRecord);
	void GetSelection(DbRecordSet& lstEntityRecord);

	//QString GetCalculatedName(int nEntityRecordID); //from common columns assembles calculated name column (e.g. Contact: Org,LastName FirstName, Pers: LastName, First
	bool ReloadFromServer();		
	bool ReloadFromCache();	
	bool SetSelectionOnGroup(int GroupID, int nTreeID);
	bool blockSignals(bool b){blockObserverSignals(b); return QFrame::blockSignals(b);}

private:
	void	GetEntityName(int nEntityID,QString &strName, QString &strNamePlural);
	void	SaveGroupTreeIntoCache();
	QString GetGroupName(int nNodeID);
	void	SaveExpandedTreeState();
	void	LoadExpandedTreeState();

    Ui::Selection_GroupTreeClass ui;
	int m_nEntityID;
	bool m_bIsFUISelectorWidget;
	bool m_bSingleClickIsSelection;
	DbRecordSet m_lstTrees;
	DbRecordSet *m_pLstGroupContent;
	bool m_bEditMode;
	QString m_strLockedResourceTree;
	QList<QAction*> m_lstTreeActions;
	QMenu m_TreeMenu;
	QAction *m_pActExtCategory;
	QAction *m_pActImportSub;
	QAction *m_pActGroupLoad;
	int m_nExtCatIdx;
	int m_nDescIdx;
	int m_nLastGroupID;
	int m_nCurrTreeID;
	//int m_nGroupLoadOnStart;
	QHash< int,QList<int> > m_lstExpandedItems;

	void CheckDescription(int);
	void InvalidateGroupData();

	//cache:
	void LoadTree(bool bReloadFromServer=false);
	void LoadGroupTree(int nTreeID,bool bReloadFromServer=false);


private slots:
	
	//FINE GIRLIES:
	void ToggleEditMode();
	void on_cmbGroupTree_currentIndexChanged(int nIndex);
	void SlotSelectionChanged();
	void SlotSelectionChanged_DoubleClicked();
	void OnTreeDropEvent();

	//TREE
	void OnTreeAdd();
	void OnTreeRename();
	void OnTreeDelete();
	void OnTreeReload(int nCurrentIndex=-1);
	void OnTreeReloadFromServer();
	void OnItemExpanded(QTreeWidgetItem * item);
	void OnItemCollapsed(QTreeWidgetItem * item);

	//GROUPS
	void OnGroupItemAdd();
	void OnGroupItemRename();
	void OnGroupItemDelete();
	void OnTreeRefreshData();
	void OnTreeRefreshDataFromServer();
	void OnTreeExpandAll(); 
	void OnExtCategoryEdit();
	void OnGroupImport();

	void OnSetGroupLoad();
	void OnClearGroupLoad();


	//GROUP COTNENT
	void OnGroupAddSelected();
	void OnGroupRemoveSelected();
	void OnGroupReplaceSelected();
	void OnGroupIntersectSelected();
	void OnGroupAddWholeList();
	void OnGroupReplaceWholeList();
	

};




#endif // SELECTION_GROUPTREE_H

