#include "selection_actualorganization.h"
#include <QApplication>
#include "bus_client/bus_client/clientoptionsandsettingsmanager.h"
extern ClientOptionsAndSettingsManager *g_pSettings;
#include "common/common/cliententitycache.h"
extern ClientEntityCache g_ClientCache;				//global cache


Selection_ActualOrganization::Selection_ActualOrganization(QWidget *parent)
	: Selection_SAPNE(parent),m_plstData(NULL)
{
}

Selection_ActualOrganization::~Selection_ActualOrganization()
{

}

//fetch default org from user defaults, display it
void Selection_ActualOrganization::Initialize(DbRecordSet *pData,QString strIDCol)
{
	m_plstData=pData;
	m_strActualOrgID_ColName=strIDCol;

	//get aco:
	//DbRecordSet recordOrg;
	int nDefOrgID=g_pSettings->GetApplicationOption(APP_DEFAULT_ORGANIZATION_ID).toInt();
	Selection_SAPNE::Initialize(ENTITY_BUS_ORGANIZATION);
	SetSelectionButtonLabel(QApplication::translate("Selection_ActualOrganization","Actual Organization:"));
	GetButton(BUTTON_REMOVE)->setVisible(false);
	//if (recordOrg.getRowCount()>0)
	//{
	//	g_ClientCache.ModifyCache(ENTITY_BUS_ORGANIZATION,recordOrg,DataHelper::OP_EDIT,this,true); //skip notify to be sure...
	SetCurrentEntityRecord(nDefOrgID,false);
	//}
}


//return current ACO ID, if invalid -1
int Selection_ActualOrganization::GetCurrentActualOrganizationID()
{
	int nRecord=-1;
	GetCurrentEntityRecord(nRecord);
	return nRecord;
}


void Selection_ActualOrganization::SaveActualOrganizationID()
{
	int nOrg=GetCurrentActualOrganizationID();
	if (nOrg<=0)
	{
		QVariant varType(QVariant::Int); //NULL TYPE
		if(m_plstData!=NULL) m_plstData->setData(0,m_strActualOrgID_ColName,varType);
	}
	else
	{
		if(m_plstData!=NULL) m_plstData->setData(0,m_strActualOrgID_ColName,nOrg);
	}

	//m_recActualOrg.Dump();
	
}

QString Selection_ActualOrganization::GetCurrentActualOrganizationName()
{
	return m_dlgCachedPopUpSelector->GetCalculatedName(GetCurrentActualOrganizationID());
}