#ifndef SIMPLESELECTIONWIZTREE_H
#define SIMPLESELECTIONWIZTREE_H

#include <QTreeWidget>

class SimpleSelectionWizTree : public QTreeWidget
{
    Q_OBJECT

public:
    SimpleSelectionWizTree(QWidget *parent = 0);
    ~SimpleSelectionWizTree();

	bool dropMimeData(QTreeWidgetItem *parent, int index, const QMimeData *data, Qt::DropAction action);

private:

signals:
	bool DataDroped(QTreeWidgetItem *parent, int index, const QMimeData *data, Qt::DropAction action);
};

#endif // SIMPLESELECTIONWIZTREE_H
