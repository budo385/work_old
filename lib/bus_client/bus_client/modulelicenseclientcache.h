#ifndef SERVICE_MODULELICENSECLIENTCACHE_H
#define SERVICE_MODULELICENSECLIENTCACHE_H

#include "common/common/status.h"
#include "common/common/dbrecordset.h"
#include "bus_core/bus_core/fp_default.h"
/*!
    \class ModuleLicenseClientCache
    \brief Caches license info on a client side
    \ingroup Bus_Client

	Initialize it after login/or system startup
		
*/
class ModuleLicenseClientCache
{
public:
	ModuleLicenseClientCache();

	void Initialize(Status& Ret_pStatus,QString strModuleCode,QString strMLIID);
	void Invalidate(){m_bInitialised=false;}
	
	bool IsFPAvailable( int FPcode, int &Ret_nValue);
	bool IsFPAvailable( int FPcode);
	void GetFPList(DbRecordSet& RetOut_pList);
	void GetLicenseInfo(QString& RetOut_strUserName, int& RetOut_nLicenseID, QString &RetOut_strReportLine1, QString &RetOut_strReportLine2, QString &Ret_strCustomerID,int &Ret_nCustomSolutionID);
	
protected:
	bool m_bInitialised;
	DbRecordSet m_List;

	QString m_strModuleCode;
	QString m_strMLIID;
	QString m_strLicUser;
	int m_nLicID;
	QString m_strReportLine1;
	QString m_strReportLine2;
	QString m_strCustomerID;
	int m_nCustomSolutionID;
};

#endif //SERVICE_MODULELICENSECLIENTCACHE_H