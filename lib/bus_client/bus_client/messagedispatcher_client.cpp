#include "messagedispatcher_client.h"
#include "common/common/logger.h"
extern Logger g_Logger;


void MessageDispatcher_Client::NewMessage(int nMsgID)
{
	QReadLocker lock(&m_mutex);

	int nMsgNumber=m_lstMessages.find("MSG_ID",nMsgID,true);
	if (nMsgNumber<0)
		return;
	DbRecordSet rowMsg=m_lstMessages.getRow(nMsgNumber);

	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,rowMsg.getDataRef(0,"MSG_ID").toInt(),rowMsg.getDataRef(0,"MSG_TEXT").toString());
}
