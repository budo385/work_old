#include "bus_client/bus_client/selection_contactbutton.h"
#include <QContextMenuEvent>
#include "bus_client/bus_client/modulelicenseclientcache.h"
extern ModuleLicenseClientCache			g_FunctionPoint;			//global function point tester

#include "common/common/entity_id_collection.h"

Selection_ContactButton::Selection_ContactButton(QWidget *parent)
	: QToolButton(parent),m_pPieMenu(NULL)
{

	m_nEntityTypeID=ENTITY_BUS_CONTACT;
	//m_nEntityRecordID=-1;

	setIcon(QIcon(":PieMenu16.png"));
	setToolTip(tr("Communicate"));

	setAutoRaise(true);

	//build menu:
	m_pPieMenu = new QtPieMenu("Root menu", this, "Root menu");


	m_pPieMenu->insertItem(QIcon(":Email32.png"),tr("Email"),this,SLOT(OnSendMail()));
	m_pPieMenu->insertItem(QIcon(":Phone32.png"),tr("Phone Call"),this,SLOT(OnPhoneCall()));
	m_pPieMenu->insertItem(QIcon(":Document32.png"),tr("Document"),this,SLOT(OnDocument()));
	m_pPieMenu->insertItem(QIcon(":Earth32.png"),tr("Internet"),this,SLOT(OnInternet()));
	m_pPieMenu->insertItem(QIcon(":Comm_Chat32.png"),tr("Chat"),this,SLOT(OnChat()));
	m_pPieMenu->insertItem(QIcon(":Comm_SMS32.png"),tr("SMS"),this,SLOT(OnSMS()));

	m_pPieMenu->setOuterRadius(130);

	connect(this,SIGNAL(clicked()),this,SLOT(OnClick()));

	int nValue;
	if(!g_FunctionPoint.IsFPAvailable(FP_EMAILS_VIEW, nValue))
	{
		m_pPieMenu->setItemEnabled(false,0);
	}


}

Selection_ContactButton::~Selection_ContactButton()
{
	delete m_pPieMenu;

}

//if -1 
void Selection_ContactButton::SetEntityType(int nEntityType)
{
	m_nEntityTypeID=nEntityType;
}

void Selection_ContactButton::contextMenuEvent(QContextMenuEvent *event)
{
	if (m_nEntityTypeID==1)
		return;
	 m_pPieMenu->popup(event->globalPos());
}


void Selection_ContactButton::SetEntityRecordID(int nEntityRecordID)
{
	m_nEntityRecordID=nEntityRecordID;
	if (m_nEntityRecordID<=0)
	{
		setEnabled(false);
	}
	else
		if(!isEnabled())setEnabled(true);
}

void Selection_ContactButton::OnClick()
{
	if (m_nEntityTypeID==-1)
		return;

	//QPushButton::click();
	m_pPieMenu->popup(mapToGlobal(QPoint(15,15)));
}



/*
void Selection_ContactButton::OnSendMail()
{
	QPoint piePos=m_pPieMenu->mapToGlobal(QPoint(100,0));
	if (m_nEntityTypeID==ENTITY_BUS_CONTACT)
		CommunicationActions::sendEmail(m_nEntityRecordID,"",-1,&piePos);
	else
		CommunicationActions::sendEmail_Project(m_nEntityRecordID,"",&piePos);

}

void Selection_ContactButton::OnPhoneCall()
{
	if (m_nEntityTypeID==ENTITY_BUS_CONTACT)
		CommunicationActions::callPhone(m_nEntityRecordID);
	else
		CommunicationActions::callPhone_Project(m_nEntityRecordID);

}

void Selection_ContactButton::OnDocument()
{
	QPoint piePos=m_pPieMenu->mapToGlobal(QPoint(100,100));
	if (m_nEntityTypeID==ENTITY_BUS_CONTACT)
		CommunicationActions::sendDocument(m_nEntityRecordID,"",&piePos);
	else
		CommunicationActions::sendDocument_Project(m_nEntityRecordID,"",&piePos);

}

void Selection_ContactButton::OnInternet()
{
	if (m_nEntityTypeID==ENTITY_BUS_CONTACT)
		CommunicationActions::openWeb(m_nEntityRecordID);
	else
		CommunicationActions::openWeb_Project(m_nEntityRecordID);

}

void Selection_ContactButton::OnChat()
{
	if (m_nEntityTypeID==ENTITY_BUS_CONTACT)
		CommunicationActions::openChat(m_nEntityRecordID);
	else
		CommunicationActions::openChat_Project(m_nEntityRecordID);
}

void Selection_ContactButton::OnSMS()
{
	if (m_nEntityTypeID==ENTITY_BUS_CONTACT)
		CommunicationActions::openSMS(m_nEntityRecordID);
	else
		CommunicationActions::openSMS_Project(m_nEntityRecordID);
}
*/