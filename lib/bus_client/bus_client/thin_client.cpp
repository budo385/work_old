#include "thin_client.h"
#include "common/common/config.h"
#include "common/common/datahelper.h"
#include "common/common/logger.h"
extern Logger g_Logger;	
#include "bus_client/bus_client/loggeduserdata.h"
extern LoggedUserData g_LoggedUserData;
#include "bus_client/bus_client/useraccessright_client.h"
extern TestAccRights *g_AccessRight;
#include "bus_client/bus_client/modulelicenseclientcache.h"
extern ModuleLicenseClientCache g_FunctionPoint;
#include "bus_client/bus_client/clientoptionsandsettingsmanager.h"
extern ClientOptionsAndSettingsManager g_Settings;
#include "common/common/cliententitycache.h"
extern ClientEntityCache g_ClientCache;
#include "clientbackupmanager.h"
extern ClientBackupManager				g_BackupManager;			//database manager

Thin_Client::Thin_Client()
{

	m_pRpcConnection=NULL;
	m_IsAliveBkgTask=NULL;
	app=NULL;

}

Thin_Client::~Thin_Client()
{
	Status err;
	ClearLastFatalError();
	StopService(err);
	//LOGGER if error
}






/*!
	Creates BO set based on settings

	\param pStatus			- error
	\param pConnSettings	- connection settings
*/
void Thin_Client::StartService(Status &pStatus,HTTPClientConnectionSettings& pConnSettings)
{

	pStatus.setError(0);

	ClearLastFatalError();
	
	//g_Logger.SetLogLevel(m_INIFile.m_nLogLevel);
	//BT: added on thin client from thick:
	//----------------------------------------------
	//Start logger (based on settings: file-logger, db logger, no logging):
	//log level	0 - basic(system)+common, 1 -network, 2-sql+business, 3 - profiler, 4- full debug (time profile+all errors)
	//----------------------------------------------
	//g_Logger.UseAsGlobalLogger();
	if (m_INIFile.m_nLogLevel && m_INIFile.m_nLogTargetFile)
	{
		if (m_INIFile.m_nLogLevel==Logger::LOG_LEVEL_ALL_FLUSH)
			g_Logger.EnableFileLogging(DataHelper::GetApplicationHomeDir()+"/settings/client.log",0);
		else
			g_Logger.EnableFileLogging(DataHelper::GetApplicationHomeDir()+"/settings/client.log");
	}
		
	if (m_INIFile.m_nLogLevel && m_INIFile.m_nLogTargetConsole)
		g_Logger.EnableConsoleLogging();
	g_Logger.SetLogLevel(m_INIFile.m_nLogLevel);


	//if started stop it
	if(m_bServiceStarted) 
	{
		StopService(pStatus);
		if(!pStatus.IsOK()) return;
	}

	//clear if not already cleared
	//APP
	if(app!=NULL) delete app;
	app=NULL;

	//CONNECTION
	if(m_pRpcConnection!=NULL) delete m_pRpcConnection;
	m_pRpcConnection=NULL;



	Q_ASSERT(m_pRpcConnection==NULL); //must be null or something was wrong!!
	//make connection, init with parameters
	CreateRPCNetworkLayer(RPC_PROTOCOL_TYPE_XML_RPC);
	
	//connect signals for server msg:
	connect(m_pRpcConnection->GetSkeletonSet(),SIGNAL(Signal_ServerMsg(DbRecordSet)),this,SLOT(OnServerMessageRecieved(DbRecordSet)));

	//set settings:
	m_pRpcConnection->SetConnectionSettings(pConnSettings);


	m_bServiceStarted=true;
	g_LoggedUserData.SetConnectionName(pConnSettings.m_strCustomName);
	//m_strCurrentConnectionName=pConnSettings.m_strCustomName;

	connect(m_pRpcConnection->GetRpcHttpClient(),SIGNAL(SocketOperationInProgress(int,qint64,qint64)),this,SLOT(OnCommunicationInProgress(int,qint64,qint64)));
	connect(m_pRpcConnection,SIGNAL(RaiseFatalError(int)),this,SLOT(OnCommunicationFatalError(int)));

	g_BackupManager.SetClientMode(true);

}



/*!
	Stops service, destroy all objects, flags, resets data inside global objects

	\param pStatus			- error
	\param app				- app pointer to BO set

*/
void Thin_Client::StopService(Status &pStatus)
{
	pStatus.setError(0);
	if(!m_bServiceStarted) return;
	

	//clear globals:
	ProcessAfterLogout(pStatus);

	m_bIsLogged=false;

	//ALIVE
	if(m_IsAliveBkgTask!=NULL)
	{
		m_IsAliveBkgTask->Stop();
		delete m_IsAliveBkgTask;
		m_IsAliveBkgTask=NULL;
	}


	//only reset if not fatal (leave those bo's to live... until destruct or next start)
	if (GetLastFatalError()==0)
	{
		//APP
		if(app!=NULL) delete app;
		app=NULL;

		//CONNECTION
		if(m_pRpcConnection!=NULL) delete m_pRpcConnection;
		m_pRpcConnection=NULL;
	}


	//save settings after stopping:
	m_INIFile.Save();

	m_bServiceStarted=false;

}



/*!
	After success login, init global objects

	\param pStatus			- error
*/
void Thin_Client::ProcessAfterLogin(Status &pStatus)
{


	//load after login data:
	g_LoggedUserData.ReadUserData(pStatus);
	if(!pStatus.IsOK()) return;


	CheckServerMessagesAfterLogin();	//only for thin client!

	//set connection name
	//g_LoggedUserData.SetConnectionName(m_strCurrentConnectionName);

	//get user data:
	int nUserID=g_LoggedUserData.GetUserID();
	m_nIsAliveInterval=g_LoggedUserData.GetIsAliveInterval();

	DbRecordSet rowARSet;
	g_LoggedUserData.GetAccessRightData(rowARSet);

	//start isalive:
	m_IsAliveBkgTask = new BackgroundTaskExecutor(new IsAlive_Client,NULL,m_nIsAliveInterval*1000,true,true);
	m_IsAliveBkgTask->Start();

	

	//init AR:
	if(g_AccessRight)
	{
		dynamic_cast<UserAccessRight_Client*>(g_AccessRight)->SetUserID(nUserID);
		g_AccessRight->SetUserARRecordSet(nUserID,rowARSet);
	}

	//init FP:
	g_FunctionPoint.Initialize(pStatus,m_strModuleCode,m_strMLIID);
	if(!pStatus.IsOK())return;


	//check for THIN:
	int nValue;
	if(!g_FunctionPoint.IsFPAvailable(FP_THIN_CLIENT_ACCESS,nValue))
	{
		pStatus.setError(1,tr("Access not allowed from thin client!"));
		return;
	}

	//store username back
	m_INIFile.m_strLastUserName=g_LoggedUserData.GetLoggedUserName();
	m_INIFile.m_strConnectionName=g_LoggedUserData.GetConnectionName();



	QString strMessage=tr("Client successfully logged to: ")+m_pRpcConnection->GetRpcHttpClient()->GetConnectionSettings().m_strServerIP;
	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0,strMessage);
	
	g_LoggedUserData.SetConnectionIPAddress(m_pRpcConnection->GetRpcHttpClient()->GetConnectionSettings().m_strServerIP);
}





/*!
	Before Logout, stop IsAlive, destroy task

	\param pStatus			- error
*/
void Thin_Client::ProcessBeforeLogout(Status &pStatus)
{

	emit SignalProcessBeforeLogout();
	if(m_IsAliveBkgTask!=NULL)
	{
		m_IsAliveBkgTask->Stop();
		delete m_IsAliveBkgTask;
		m_IsAliveBkgTask=NULL;
	}


	g_Settings.SavePersonSettings(pStatus);
}


/*!
	After Logout, reset flags, and all cached data

	\param pStatus			- error
*/
void Thin_Client::ProcessAfterLogout(Status &pStatus)
{
	//reset AR:
	if(g_AccessRight)
	{
		g_AccessRight->DeleteUserARRecordSet(g_LoggedUserData.GetUserID());
		dynamic_cast<UserAccessRight_Client*>(g_AccessRight)->SetUserID(0);
	}

	//reset FP:
	g_FunctionPoint.Invalidate();

	//clear cache:
	g_ClientCache.PurgeCache();

	
	ClearUserData();



}

//disable access to network:
void Thin_Client::FatalErrorDetected(int)
{
	//if exists:
	if (m_IsAliveBkgTask) 
	{
		m_IsAliveBkgTask->Stop();  //stop isalive
		delete m_IsAliveBkgTask;
		m_IsAliveBkgTask=NULL;
	}

}

//only alive if not in error state or socket not in use (if in use, means isalive is not neccessary->skip)
void Thin_Client::IsAlive(Status &pStatus)
{
	if(GetLastFatalError()!=0)
		return;

	if (m_pRpcConnection==NULL || app == NULL)
		return;

	if (m_pRpcConnection->IsSocketInUse())
		return;

	app->CoreServices->IsAlive(pStatus);
}


void Thin_Client::AbortCommunicationOperation()
{
	if (m_pRpcConnection)
	{
		m_pRpcConnection->GetRpcHttpClient()->CancelOperation();
	}
}



void Thin_Client::CreateRPCNetworkLayer(int nRPC_TypeID)
{

	switch(nRPC_TypeID)
	{
	case RPC_PROTOCOL_TYPE_XML_RPC:
		{
			//make connection:
			XmlRpcHTTPConnectionHandler *pXmlRpcConnection= new XmlRpcHTTPConnectionHandler;
			//make stub collection	
			app= new XmlRpcStubClientSet(pXmlRpcConnection);
			m_pRpcConnection=pXmlRpcConnection; //store connection;
		}
		break;
	case RPC_PROTOCOL_TYPE_SOAP:
		{

		}
	    break;
	default:
	    break;
	}

}