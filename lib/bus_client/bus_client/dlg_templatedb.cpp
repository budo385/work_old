#include "dlg_templatedb.h"
#include "gui_core/gui_core/macros.h"
#include <QFileInfo>

#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;


Dlg_TemplateDb::Dlg_TemplateDb(QWidget *parent)
:QDialog(parent),m_pBackupManager(NULL)
{
	ui.setupUi(this);

}

bool Dlg_TemplateDb::Initialize(ClientBackupManager *pBackupManager)
{

	m_pBackupManager=pBackupManager;

	//load db's:
	Status err;
	_SERVER_CALL(ServerControl->LoadTemplateList(err,m_lstData))
	_CHK_ERR_RET_BOOL_ON_FAIL(err);


	//get template data:
	_SERVER_CALL(ServerControl->GetTemplateData(err,m_strTemplateName,m_nAskForTemplate,m_nStartStartUpWizard))
	_CHK_ERR_RET_BOOL_ON_FAIL(err);

	ui.txtTemplate->setText(m_strTemplateName);


	//add them:
	int nSize=m_lstData.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		QFileInfo info(m_lstData.getDataRef(i,0).toString());
		QString strTemplate=info.baseName();

		int k=0;
		k=strTemplate.lastIndexOf("_");
		k=strTemplate.lastIndexOf("_",k-1);
		k=strTemplate.lastIndexOf("_",k-1);
		if (k>0 && k<strTemplate.length())
		{
			strTemplate=strTemplate.left(k);
		}

		
		ui.cmbDb->addItem(strTemplate,m_lstData.getDataRef(i,0).toString());	
	}
	int nIndex=ui.cmbDb->findText(m_strTemplateName);
	ui.cmbDb->setCurrentIndex(nIndex);
	if (nIndex<0 && nSize>0)
	{
		ui.cmbDb->setCurrentIndex(0);
	}

	ui.cmbDb->setEditable(false);

	//if templates = 0, then skip, reset flag to 0
	if (m_lstData.getRowCount()==0)
	{
		on_btnCancel_clicked();
		return false;
	}

	return true;
}

Dlg_TemplateDb::~Dlg_TemplateDb()
{

}



void Dlg_TemplateDb::on_btnCancel_clicked()
{
	//reset ask flag:
	m_nAskForTemplate=0;
	Status err;
	_SERVER_CALL(ServerControl->SetTemplateData(err,m_strTemplateName,0,m_nStartStartUpWizard))
	_CHK_ERR_NO_RET(err);
	done(0);
}

void Dlg_TemplateDb::on_btnOK_clicked()
{
	//save template for restore...
	if (m_lstData.getDataRef(ui.cmbDb->currentIndex(),0).toString()!=m_strTemplateName && ui.cmbDb->currentIndex()!=-1)
	{
		//load db's:
		Status err;
		QString strRestoreFile;
		strRestoreFile=m_lstData.getDataRef(ui.cmbDb->currentIndex(),0).toString();
		_SERVER_CALL(ServerControl->RestoreFromTemplate(err,strRestoreFile))
		_CHK_ERR(err);
		done(1);
	}
}