#include "simpleselectionwizfileselection.h"
#include <QFileDialog>
#include "common/common/datahelper.h"
#include <QDir>
extern QString g_strLastDir_FileOpen;
SimpleSelectionWizFileSelection::SimpleSelectionWizFileSelection(int nWizardPageID, QString strPageTitle, QWidget *parent, int nPageEntityID /*= -1*/)
: WizardPage(nWizardPageID, strPageTitle)
{
	setMinimumSize(QSize(600,50));
	m_recResultRecordSet.addColumn(QVariant::String,"PATH");
	m_recResultRecordSet.addRow();
	m_recResultRecordSet.setData(0,0,"");
}

SimpleSelectionWizFileSelection::~SimpleSelectionWizFileSelection()
{

}


void SimpleSelectionWizFileSelection::Initialize()
{
	ui.setupUi(this);

	ui.txtPath->setFocus();
	ui.btnSelectPath->setToolTip(tr("Select directory"));
	ui.btnSelectPath->setIcon(QIcon(":SAP_Select.png"));
	m_bInitialized = true;
}


void SimpleSelectionWizFileSelection::on_btnSelectPath_clicked()
{
	QString strStartDir=ui.txtPath->text();

	if (strStartDir.isEmpty())
	{
		strStartDir=DataHelper::GetApplicationHomeDir();
	}

	//open file dialog:
	if (g_strLastDir_FileOpen.isEmpty())
		g_strLastDir_FileOpen=strStartDir;
	else
		strStartDir=g_strLastDir_FileOpen;
	QString strFile = QFileDialog::getSaveFileName(
		NULL,
		tr("Select File"),
		strStartDir,
		tr("Text file (*.txt)"));

	if(!strFile.isEmpty())
	{
		QFileInfo fileInfo(strFile);
		g_strLastDir_FileOpen=fileInfo.absolutePath();

		ui.txtPath->setText(QDir::toNativeSeparators(strFile));
		m_bComplete = true;
		emit completeStateChanged();
	}
}


void SimpleSelectionWizFileSelection::on_txtPath_textChanged( const QString &text )
{
	m_bComplete = true;
	emit completeStateChanged();
}


bool SimpleSelectionWizFileSelection::GetPageResult(DbRecordSet &RecordSet)
{
	if (!m_bInitialized)
		return false;
	m_recResultRecordSet.setData(0,0,ui.txtPath->text());
	RecordSet=m_recResultRecordSet;
	return true;

}

//after retry reset: add rows:
void SimpleSelectionWizFileSelection::resetPage()
{
	ui.txtPath->setText("");
}