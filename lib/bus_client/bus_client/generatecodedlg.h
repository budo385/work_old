#ifndef GENERATECODEDLG_H
#define GENERATECODEDLG_H

#include <QtWidgets/QWidget>
#include "./generatedfiles/ui_generatecodedlg.h"
#include "common/common/dbrecordset.h"

class generatecodedlg : public QDialog
{
    Q_OBJECT

public:
    generatecodedlg(QWidget *parent = 0);
    ~generatecodedlg();

	int	m_nEntityType;
	QString m_strFldPrefix;
	QString m_strParentCode;
	QString m_strProposedCode;
	QString m_strProposedName;

	void SetHierarhicalCodeData(DbRecordSet *pLstData){m_pLstData=pLstData;} //set this to avoid cache lookup
	void EnableNameInput();
	void EnableTemplateInput();
	void SetParentCode(QString strCode);
	void SetProposedCode(QString strCode);
	void RefreshData();
	
	bool IsUseTemplateSelected(){ return ui.chkUseTemplate->isVisible() && ui.chkUseTemplate->isChecked(); }

	int GetTemplateData(QString &strCode,QString &strName);

private:
    Ui::generatecodedlgClass ui;
	DbRecordSet *m_pLstData;
	DbRecordSet* GetCacheData();

private slots:
	void on_chkUseTemplate_stateChanged(int);
	void on_btnCancel_clicked();
	void on_btnOK_clicked();
	void on_btnSubLevel_clicked();
	void on_btnSameLevel_clicked();
	void on_DialogInitialized();
};

#endif // GENERATECODEDLG_H
