#include "bus_client/bus_client/selection_sapne.h"
#include "bus_core/bus_core/contacttypemanager.h"

//global message dispatcher
#include "bus_client/bus_client/changemanager.h"
extern ChangeManager g_ChangeManager;
#include "bus_core/bus_core/useraccessright.h"
extern UserAccessRight					*g_AccessRight;				//global access right tester


Selection_SAPNE::Selection_SAPNE(QWidget *parent)
: QFrame(parent),m_btnContactPie(NULL) //must be crreated by implementation class
{
	ui.setupUi(this);

	m_strHtmlTag="<html><head><meta name=\"qrichtext\" content=\"1\" /></head><body style=\" white-space: pre-wrap; font-family:MS Shell Dlg 2; font-size:8.25pt; font-weight:400; font-style:normal; text-decoration:none;\"><p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600; font-style:italic;\">";
	m_strEndHtmlTag="</span></p></body></html>";

	//set icons:
	ui.btnModify->setIcon(QIcon(":SAP_Modify.png"));
	ui.btnSelect->setIcon(QIcon(":Select16.png"));
	ui.btnRemove->setIcon(QIcon(":SAP_Clear.png"));
	ui.btnInsert->setIcon(QIcon(":handwrite.png"));
	ui.btnView->setIcon(QIcon(":SAP_Select.png"));

	ui.btnInsert->setVisible(false);
	
	//ui.btnModify->setToolTip(strComplexToolTip);
	ui.btnModify->setToolTip(tr("Modify/Create"));
	ui.btnSelect->setToolTip(tr("Select"));
	ui.btnRemove->setToolTip(tr("Remove Assignment"));
	ui.btnInsert->setToolTip(tr("Add"));
	ui.btnView->setToolTip(tr("View Details"));

	m_nCurrentRow=0;
	m_nIDColIdx=-1;
	m_pExternDataManipulator=NULL;
	m_pExternData=NULL;
	m_dlgCachedPopUpSelector=NULL;

	m_nActualOrganizationID=-1;
	m_bAutoAssigment=false;
	m_bRefreshFromExternalDataInProgress=false;

	g_ChangeManager.registerObserver(this); //if entity is deleted/edited, do this

	setFocusPolicy(Qt::StrongFocus);
	setFocusProxy(ui.btnSelect); //set on button
}

Selection_SAPNE::~Selection_SAPNE()
{
	//unregister observer:
	g_ChangeManager.unregisterObserver(this);
}





//Clear & Simple: init with entity type: use GetCurrentRecord & SetCurrentRecord
void Selection_SAPNE::Initialize(int nEntityTypeID)
{
	m_nEntityTypeID=nEntityTypeID;

	//init selector: data is not yet loaded from server, nor it will be until selection pop's up
	m_dlgPopUp.Initialize(nEntityTypeID);
	m_dlgCachedPopUpSelector=dynamic_cast<MainEntitySelectionController*>(m_dlgPopUp.GetSelectorWidget());
	Q_ASSERT(m_dlgCachedPopUpSelector!=NULL); //must be valid

	//current data is formatted as cache entity:
	m_RowCurrentSelectedEntity.copyDefinition(*m_dlgCachedPopUpSelector->GetDataSource());
	m_strCurrentDisplay="";

	//if contact or person, enable pie
	if (nEntityTypeID==ENTITY_BUS_CONTACT || nEntityTypeID==ENTITY_BUS_PERSON || nEntityTypeID==ENTITY_BUS_PROJECT)
	{
		CreatePieButton();
		if(m_btnContactPie)m_btnContactPie->SetEntityType(m_nEntityTypeID);
		if(m_btnContactPie)m_btnContactPie->setVisible(true);
		if (nEntityTypeID==ENTITY_BUS_PERSON)
			if(m_btnContactPie)m_btnContactPie->SetEntityType(ENTITY_BUS_CONTACT); //contact if person or contact
	}
	else
	{
		//if(m_btnContactPie)m_btnContactPie->setVisible(false);
		if(m_btnContactPie)m_btnContactPie->setEnabled(false);
		if(m_btnContactPie)m_btnContactPie->setIcon(QIcon());
	}

	if (nEntityTypeID==ENTITY_BUS_PROJECT || nEntityTypeID==ENTITY_BUS_CONTACT)
	{
		if (!g_AccessRight->TestAR(ASSIGN_CE_ENTITIES_TO_PROJECTS_AND_CONTACTS))
		ui.btnSelect->setEnabled(false);
	}

	//clear, but avoid to destroy data->let them call refreshdisplay:
	m_bRefreshFromExternalDataInProgress=true;
	Clear(true);
	m_bRefreshFromExternalDataInProgress=false;


	QWidget::setEnabled(m_dlgCachedPopUpSelector->IsEntityEnabled()); //FP

}

void Selection_SAPNE::setDisabled ( bool bEnable)
{
	if (m_dlgCachedPopUpSelector)
		if (!m_dlgCachedPopUpSelector->IsEntityEnabled())
			return;
	QWidget::setDisabled(bEnable);
}

void Selection_SAPNE::setEnabled ( bool bEnable)
{
	if (m_dlgCachedPopUpSelector)
		if (!m_dlgCachedPopUpSelector->IsEntityEnabled())
			return;
	QWidget::setEnabled(bEnable);
}

//ID is store inside field ID field of pExternData
//RefreshDisplay() will use ID from data
//if assignment is changed it will changed inside pExternData
void Selection_SAPNE::Initialize(int nEntityTypeID,GuiDataManipulator *pExternDataManipulator, QString strIDCol)
{
	m_pExternDataManipulator=pExternDataManipulator;
	Q_ASSERT(m_pExternDataManipulator);
	m_nIDColIdx=m_pExternDataManipulator->GetDataSource()->getColumnIdx(strIDCol);
	Q_ASSERT(m_nIDColIdx!=-1);
	
	Initialize(nEntityTypeID);
}

//ID is store inside field ID field of pExternData
//RefreshDisplay() will use ID from data
//if assignment is changed it will changed inside pExternData

void Selection_SAPNE::Initialize(int nEntityTypeID,DbRecordSet *pExternData, QString strIDCol)
{
	m_pExternData=pExternData;
	Q_ASSERT(m_pExternData);
	m_nIDColIdx=pExternData->getColumnIdx(strIDCol);
	Q_ASSERT(m_nIDColIdx!=-1);

	Initialize(nEntityTypeID);
}


//set label on selection button:
void Selection_SAPNE::SetSelectionButtonLabel(QString strSelButtonLabel)
{	
	QLabel *label=new QLabel;
	ui.hboxLayout->insertWidget(0,label);
	label->setText(" "+strSelButtonLabel);
	/*
	ui.btnSelect->setSizePolicy(QSizePolicy::Minimum,QSizePolicy::Fixed);
	ui.btnSelect->setMinimumWidth(20);
	ui.btnSelect->setMaximumWidth(16777215);
	ui.btnSelect->resize(20,ui.btnSelect->width()); 
	ui.btnSelect->setText(strSelButtonLabel);
	*/
}

void Selection_SAPNE::EnableInsertButton()
{
	ui.btnInsert->setVisible(true);
}


//show from data source in widgets, takes data from current row, does not emit change signal...
void Selection_SAPNE::RefreshDisplay()
{
	m_bRefreshFromExternalDataInProgress=true;

	if (m_pExternDataManipulator!=NULL)
	{
		if(m_pExternDataManipulator->GetDataSource()->getRowCount()>m_nCurrentRow)
			SetCurrentEntityRecord(m_pExternDataManipulator->GetDataSource()->getDataRef(m_nCurrentRow,m_nIDColIdx).toInt(),true);
		else
			Clear(true);

		m_bRefreshFromExternalDataInProgress=false;
		return;
	}

	if (m_pExternData!=NULL)
	{
		if(m_pExternData->getRowCount()>m_nCurrentRow)
			SetCurrentEntityRecord(m_pExternData->getDataRef(m_nCurrentRow,m_nIDColIdx).toInt(),true);
		else
			Clear(true);

		m_bRefreshFromExternalDataInProgress=false;
		return;
	}

	m_bRefreshFromExternalDataInProgress=false;
}




//--------------------------------------------------------------
//					EVENT HANDLERS
//--------------------------------------------------------------

//open selector, get selection, display & store inside data source
void Selection_SAPNE::on_btnSelect_clicked()
{
	Q_ASSERT(m_dlgCachedPopUpSelector); //test if initialized

	DbRecordSet empty;
	int nResult=m_dlgPopUp.OpenSelector();
	if(nResult)
	{
		DbRecordSet record;
		int nRecordID;
		m_dlgPopUp.GetSelectedData(nRecordID,record);
		SetEntityData(nRecordID,record);
	}

}

int Selection_SAPNE::OpenSelector()
{
	Q_ASSERT(m_dlgCachedPopUpSelector); //test if initialized

	DbRecordSet empty;
	int nResult=m_dlgPopUp.OpenSelector();
	if(nResult)
	{
		DbRecordSet record;
		int nRecordID;
		m_dlgPopUp.GetSelectedData(nRecordID,record);
		SetEntityData(nRecordID,record);
	}

	return nResult;
}


//remove all entries, store NULL inside datasource
void Selection_SAPNE::on_btnRemove_clicked()
{
	Q_ASSERT(m_dlgCachedPopUpSelector); //test if initialized
	Clear();
}



//open FUI in new window, set new ID if exists
void Selection_SAPNE::on_btnModify_clicked()
{
	Q_ASSERT(m_dlgCachedPopUpSelector); //test if initialized
	int nRecordID=-1;
	if (m_RowCurrentSelectedEntity.getRowCount()==1) //if something selected, get it
		nRecordID=m_RowCurrentSelectedEntity.getDataRef(0,m_dlgCachedPopUpSelector->m_nPrimaryKeyColumnIdx).toInt(); //get old ID if valid

	notifyObservers(SelectorSignals::SELECTOR_ON_EDIT,nRecordID,m_nActualOrganizationID);

	/*
	if (m_nEntityTypeID==ENTITY_BUS_CONTACT) //issue 1274
	{
		m_pLastFUIOpen=g_objFuiManager.OpenQCWWindow(NULL,nRecordID);
	}
	else
	{
		//open FUI, get window pointer
		int nFUI_ID=g_objFuiManager.FromEntityToMenuID(m_nEntityTypeID);
		int nNewFUI=-1;
		//if (nRecordID==-1)
		//	nNewFUI = g_objFuiManager.OpenFUI(nFUI_ID,true,false,FuiBase::MODE_INSERT,-1,true);
		//else
		nNewFUI = g_objFuiManager.OpenFUI(nFUI_ID,true,false,FuiBase::MODE_EDIT,nRecordID,true);
		m_pLastFUIOpen=g_objFuiManager.GetFUIWidget(nNewFUI);
	}


	if(m_nActualOrganizationID!=-1 && nRecordID==-1)
	{
		if(m_pLastFUIOpen)dynamic_cast<FuiBase*>(m_pLastFUIOpen)->SetActualOrganization(m_nActualOrganizationID);  //set org as defult
	}
	if(m_pLastFUIOpen)m_pLastFUIOpen->show();  //show FUI

	*/
}


//if click, open FUI, go insert, store Last FUI pointer, send signal 
void Selection_SAPNE::on_btnInsert_clicked()
{
	Q_ASSERT(m_dlgCachedPopUpSelector); //test if initialized;

	notifyObservers(SelectorSignals::SELECTOR_ON_INSERT,0,m_nActualOrganizationID);
	/*
	if (m_nEntityTypeID==ENTITY_BUS_CONTACT) //issue 1274
	{
		m_pLastFUIOpen=g_objFuiManager.OpenQCWWindow(NULL);
	}
	else
	{
		//open FUI, get window pointer
		int nFUI_ID=g_objFuiManager.FromEntityToMenuID(m_nEntityTypeID);
		int nNewFUI = g_objFuiManager.OpenFUI(nFUI_ID,true,false,FuiBase::MODE_INSERT,-1,true);
		m_pLastFUIOpen=g_objFuiManager.GetFUIWidget(nNewFUI);
	}
	
	if(m_nActualOrganizationID!=-1)
	{
		if(m_pLastFUIOpen)dynamic_cast<FuiBase*>(m_pLastFUIOpen)->SetActualOrganization(m_nActualOrganizationID);  //set org as defult
	}
	if(m_pLastFUIOpen)m_pLastFUIOpen->show();  //show FUI

	*/
	m_bAutoAssigment=true;
}







//reload data signal from selector: reload combo if needed, if selector changed data
void Selection_SAPNE::updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail,const QVariant data)
{
	if(nMsgDetail!=m_nEntityTypeID) return; //not our entity, exit
	if (pSubject!=&g_ChangeManager) return;

	//if EDIT mode, our FUI just inserted new id, assign back to FK link
	if(m_bAutoAssigment && nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_INSERTED)
	{
		int nPK=m_dlgCachedPopUpSelector->GetPrimaryKeyIdx();
		//extract new row:
		DbRecordSet record=data.value<DbRecordSet>();
		//record.Dump();
		if (record.getRowCount()!=1 || nPK==-1) //if row count doesn't match or ID col is not given, abort
			return;

		SetCurrentEntityRecord(record.getDataRef(0,nPK).toInt());
	}

	
	

	//IF FUI sends signal that entity data is changed:
	if (nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_EDITED ||nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_DELETED)
	{
		//CallBack to refresh displayed data:
		DbRecordSet lstNewData=data.value<DbRecordSet>();
		//if pk is ours?
		int nPkID=lstNewData.getColumnIdx(m_dlgCachedPopUpSelector->m_strTablePrefix+"_ID");	//try to get PK column for precise refresh
		if (nPkID!=-1 && lstNewData.getRowCount()>0)
		{
			int nID=lstNewData.getDataRef(nPkID,0).toInt();
			int nCurrentID=0;
			if (GetCurrentEntityRecord(nCurrentID))
			{
				if (nID==nCurrentID)
				{
					if(nMsgCode==ChangeManager::GLOBAL_REFRESH_ENTITY_EDITED )//refresh: either new data or delete
						SetCurrentEntityRecord(nCurrentID);						
					else
						Clear();
				}
			}
		}
	}

	//reload selected:
	if (nMsgCode==ChangeManager::GLOBAL_REFRESH_FORCE_ENTITY_SELECTORS_RELOAD)
	{
		int nCurrentID=0;
		if (GetCurrentEntityRecord(nCurrentID))
			SetCurrentEntityRecord(nCurrentID, true);
	}
	
}




//set this data on display and inside data source
void Selection_SAPNE::SetEntityData(int nRecordID, DbRecordSet &record,bool bSkipNotifyObservers)
{
	//first set new name on display & store record:
	m_strCurrentDisplay=m_dlgCachedPopUpSelector->GetCalculatedName(nRecordID);
	ui.labelText->setText(m_strHtmlTag+m_strCurrentDisplay+m_strEndHtmlTag);
	ui.labelText->setToolTip(ui.labelText->text());

	//store record inside our record:
	m_RowCurrentSelectedEntity=record;

	//if refrsh from data, skip backfire:
	if (!m_bRefreshFromExternalDataInProgress)
	{

		//if external, clean inside them
		if (m_pExternDataManipulator!=NULL)
		{
			if(m_pExternDataManipulator->GetDataSource()->getRowCount()>m_nCurrentRow)
				//m_pExternDataManipulator->GetDataSource()->setData(m_nCurrentRow,m_nIDColIdx,nRecordID);
				m_pExternDataManipulator->ChangeData(m_nCurrentRow,m_nIDColIdx,nRecordID);
		}
		if (m_pExternData!=NULL)
		{
			if(m_pExternData->getRowCount()>m_nCurrentRow)
				m_pExternData->setData(m_nCurrentRow,m_nIDColIdx,nRecordID);
		}
	}


	//if contact SAPNE, set to contact id:
	if (m_nEntityTypeID==ENTITY_BUS_CONTACT && record.getRowCount()==1)
	{
		int nContactID=record.getDataRef(0,"BCNT_ID").toInt();
		if(m_btnContactPie)m_btnContactPie->SetEntityRecordID(nContactID);
	}
	if (m_nEntityTypeID==ENTITY_BUS_PROJECT && record.getRowCount()==1)
	{
		int nContactID=record.getDataRef(0,"BUSP_ID").toInt();
		if(m_btnContactPie)m_btnContactPie->SetEntityRecordID(nContactID);
	}
	if (m_nEntityTypeID==ENTITY_BUS_PERSON && record.getRowCount()==1)
	{
		int nContactID=record.getDataRef(0,"BPER_CONTACT_ID").toInt();
		if(m_btnContactPie)m_btnContactPie->SetEntityRecordID(nContactID);
	}


	//notify all observers
	if(!bSkipNotifyObservers)
	{
		QVariant varData;
		qVariantSetValue(varData,record);
		notifyObservers(SelectorSignals::SELECTOR_SELECTION_CHANGED,nRecordID,varData);
		emit CurrentEntityRecordChanged(nRecordID);
	}

}




//sets SAPNE data for given entity record ID, data is lookup in cache then on server:
//data is set in datasource and on display
//signal can be omitted to avoid endless loops
void Selection_SAPNE::SetCurrentEntityRecord(int nEntityRecordID,bool bSkipNotifyObservers)
{
	DbRecordSet record;
	QString strCode,strName;

	Q_ASSERT(m_dlgCachedPopUpSelector!=NULL); //must be set
	bool bOK=m_dlgCachedPopUpSelector->GetEntityRecord(nEntityRecordID,record);
	if(bOK)
	{
		if (!this->isEnabled())
		{
			ui.labelText->setText("");
			ui.labelText->setToolTip("");
			this->setEnabled(true);
		}
		
		//record.Dump();
		m_RowCurrentSelectedEntity=record;
		SetEntityData(nEntityRecordID,record,bSkipNotifyObservers);
	}
	else
	{
		Clear(bSkipNotifyObservers); //if invalid id, clear all
		//UAR: blocked: go into UAR protect mode: disable all SAPNE
		if (nEntityRecordID>0)
		{
			ui.labelText->setText(tr("Access Denied"));
			ui.labelText->setToolTip(ui.labelText->text());
			this->setEnabled(false);
		}
	}

}




void Selection_SAPNE::SetEditMode(bool bEditMode)
{
	ui.btnSelect->setEnabled(bEditMode);
	ui.btnRemove->setEnabled(bEditMode);
	ui.btnInsert->setEnabled(bEditMode);

	m_bAutoAssigment=false; //block input for insert if changing edit mode
}






//if nothing selected, false will be returned
bool Selection_SAPNE::GetCurrentEntityRecord(int &nEntityRecordID,DbRecordSet &record)
{
	if (m_RowCurrentSelectedEntity.getRowCount()==0)
	{
		nEntityRecordID=-1;
		return false;
	}
	else
	{
		record=m_RowCurrentSelectedEntity;
		nEntityRecordID=m_RowCurrentSelectedEntity.getDataRef(0,m_dlgCachedPopUpSelector->m_nPrimaryKeyColumnIdx).toInt();
		return true;
	}
}

bool Selection_SAPNE::GetCurrentEntityRecord(int &nEntityRecordID)
{
	if (m_RowCurrentSelectedEntity.getRowCount()==0)
	{
		nEntityRecordID=-1;
		return false;
	}
	else
	{
		nEntityRecordID=m_RowCurrentSelectedEntity.getDataRef(0,m_dlgCachedPopUpSelector->m_nPrimaryKeyColumnIdx).toInt();
		return true;
	}
}



QString Selection_SAPNE::GetCurrentDisplayName()
{
	return m_strCurrentDisplay;
}



void Selection_SAPNE::Clear(bool bSkipNotifyObservers)
{
	Q_ASSERT(m_dlgCachedPopUpSelector); //test if initialized

	//clear GUI
	ui.labelText->setText("");
	ui.labelText->setToolTip("");
	if(m_btnContactPie)m_btnContactPie->SetEntityRecordID(0);


	//clear data:
	m_RowCurrentSelectedEntity.clear();
	QVariant empty(QVariant::Int);

	//if refrsh from data, skip backfire:
	if (!m_bRefreshFromExternalDataInProgress)
	{

		//if external, clean inside them
		if (m_pExternDataManipulator!=NULL)
		{
			if(m_pExternDataManipulator->GetDataSource()->getRowCount()>m_nCurrentRow)
				//m_pExternDataManipulator->GetDataSource()->setData(m_nCurrentRow,m_nIDColIdx,empty);
				m_pExternDataManipulator->ChangeData(m_nCurrentRow,m_nIDColIdx,empty);
		}

		if (m_pExternData!=NULL)
		{
			if(m_pExternData->getRowCount()>m_nCurrentRow)
				m_pExternData->setData(m_nCurrentRow,m_nIDColIdx,empty);
		}
	}


	//notify all observers
	if(!bSkipNotifyObservers)
	{

		QVariant varData;
		qVariantSetValue(varData,m_RowCurrentSelectedEntity);
		notifyObservers(SelectorSignals::SELECTOR_SELECTION_CHANGED,-1,varData); //invalid record:
		emit CurrentEntityRecordChanged(-1);
	}
}


QWidget* Selection_SAPNE::GetButton(int nButton)
{

	switch(nButton)
	{
	case BUTTON_MODIFY:
		return ui.btnModify;
		break;
	case BUTTON_SELECT:
		return ui.btnSelect;
		break;
	case BUTTON_REMOVE:
		return ui.btnRemove;
		break;
	case BUTTON_ADD:
		return ui.btnInsert;
		break;
	case BUTTON_CONTACT_PIE:
		return ui.btnInsert;
		break;
	case BUTTON_VIEW:
		return ui.btnView;
		break;
	default:
		return ui.btnSelect;
		break;

	}

	return NULL;

}



//issue: 1385: on view: open contact in normal FUI
//open FUI in new window, set new ID if exists
void Selection_SAPNE::on_btnView_clicked()
{
	Q_ASSERT(m_dlgCachedPopUpSelector); //test if initialized

	int nRecordID=-1;
	if (m_RowCurrentSelectedEntity.getRowCount()==1) //if something selected, get it
		nRecordID=m_RowCurrentSelectedEntity.getDataRef(0,m_dlgCachedPopUpSelector->m_nPrimaryKeyColumnIdx).toInt(); //get old ID if valid

	//issue 1516:
	if (nRecordID==-1)
	{
		QMessageBox::warning(this,tr("Warning"),tr("No Assignment Made!"));
		return;
	}
	
	notifyObservers(SelectorSignals::SELECTOR_ON_VIEW,nRecordID,m_nActualOrganizationID);

	/*
	//open FUI, get window pointer
	int nFUI_ID=g_objFuiManager.FromEntityToMenuID(m_nEntityTypeID);
	int nNewFUI=-1;
	//if (nRecordID==-1)
	//	nNewFUI = g_objFuiManager.OpenFUI(nFUI_ID,true,false,FuiBase::MODE_INSERT,-1,true);
	//else
	nNewFUI = g_objFuiManager.OpenFUI(nFUI_ID,true,false,FuiBase::MODE_READ,nRecordID,true);
	m_pLastFUIOpen=g_objFuiManager.GetFUIWidget(nNewFUI);

	//issue 1401: set tab to details
	if (m_nEntityTypeID==ENTITY_BUS_CONTACT) 
	{
		FUI_Contacts *pCont=dynamic_cast<FUI_Contacts*>(m_pLastFUIOpen);
		if(pCont)
		{
			pCont->SetCurrentTab(1); 
		}
	}
	


	if(m_nActualOrganizationID!=-1 && nRecordID==-1)
	{
		if(m_pLastFUIOpen)dynamic_cast<FuiBase*>(m_pLastFUIOpen)->SetActualOrganization(m_nActualOrganizationID);  //set org as defult
	}
	if(m_pLastFUIOpen)m_pLastFUIOpen->show();  //show FUI
	*/

}