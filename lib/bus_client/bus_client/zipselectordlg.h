#ifndef ZIPSELECTORDLG_H
#define ZIPSELECTORDLG_H

#include <QtWidgets/QDialog>
#include "ui_zipselectordlg.h"

class ZipSelectorDlg : public QDialog
{
	Q_OBJECT

public:
	ZipSelectorDlg(QString strXMLData, QString strCountryCode, QWidget *parent = 0);
	~ZipSelectorDlg();

	void AddLine(QString strZip, QString strTown, QString strCountry);
	QString getSelectedZIP();
	QString getSelectedTown();
	QString getSelectedCountryCode();


	QString m_strCountryCodeFilter;

private slots:
	void on_btnOK_clicked();
	void on_btnCancel_clicked();
	void on_itemDoubleClicked ( QTreeWidgetItem * item, int column );

private:
	Ui::ZipSelectorDlgClass ui;
};

#endif // ZIPSELECTORDLG_H
