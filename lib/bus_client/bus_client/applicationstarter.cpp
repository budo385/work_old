#ifndef  WINCE
#include "applicationstarter.h"


#include "common/common/config.h"
//#include <QThread>
#include <QCoreApplication>
#include <QApplication>
#include "client_global_objects_create.h"
#include "thin_client.h"
//
//#include "common/common/datahelper.h"

#ifndef SP_NO_THICK_CLIENT
	#include "thick_client.h"
#endif

/*
	Starts application, if error then aborts start, exits

	\param app						-	CoreApplication object
	\param pMainApplicationWindow	-	Main Window
	\param parent					-	QObject parent
*/
ApplicationStarter::ApplicationStarter(QObject *parent,bool bConsoleApp)
:QObject(parent)
{ 
	//reset globals:
	g_BoSet=NULL;
	g_AccessRight=NULL;

	Status err;
	//connect quit signals:
	//if(!bConsoleApp)
	//	QObject::connect(QApplication::instance(), SIGNAL(lastWindowClosed()), QApplication::instance(), SLOT(quit()));
	//QObject::connect(QCoreApplication::instance(), SIGNAL(aboutToQuit()), this, SLOT(deleteLater()));

	//directory name= app. name
	QDir dirApp(QCoreApplication::applicationDirPath());
	QCoreApplication::instance()->setApplicationName(dirApp.dirName());		//for settings

	m_bConsoleApp=bConsoleApp;		//
}
void ApplicationStarter::Go(Status &pStatus)
{
	//special AR object:
	g_AccessRight = new UserAccessRight_Client;

	//LOAD INI file & KEY
	bool bIsThickClient;
	QString strINIPath;
	ClientIniFile INIFile;
	LoadSettings(pStatus,g_IsThinClientMode,INIFile,strINIPath);
	if(!pStatus.IsOK())
		ExitApplication(pStatus);

	//make thin or thick client:
	#ifndef SP_NO_THICK_CLIENT
		if (!g_IsThinClientMode)
			g_BoSet= new Thick_Client;
		else
	#endif
			g_BoSet= new Thin_Client;

	g_BoSet->InitializeService(pStatus,INIFile);
	if(!pStatus.IsOK())
			ExitApplication(pStatus);

	g_BoSet->EnableCheckForUpdates("http://www.sokrates-communicator.com/version/");

	//ini theme, set last theme -> before login
	g_ThemeManager.OnStartUp();
}
ApplicationStarter::~ApplicationStarter()
{
	if(g_AccessRight!=NULL){ delete g_AccessRight;g_AccessRight=NULL;}
	if(g_BoSet!=NULL) {delete g_BoSet;g_BoSet=NULL;}
	//qDebug()<<"Imhere";
}


//if thick client, load also key file:
void ApplicationStarter::LoadSettings(Status &pStatus, int &bIsThinClient,ClientIniFile &INIFile,QString &strIniFilePath)
{
	QString strSettings=ClientManager::CheckSettingsDirectory(pStatus);
	if (!pStatus.IsOK()) return;

	bIsThinClient = INIFile.m_nIsThinClient;

	//load INI file:
	//------------------------------------------
	strIniFilePath= strSettings+"/client.ini";
	bool bOK=INIFile.Load(strIniFilePath);
	if(!bOK)
	{
		pStatus.setError(1,tr("INI file is missing,corrupted or has invalid values!"));
		return;
	}
}


void ApplicationStarter::ExitApplication(Status &pStatus)
{
	if (!m_bConsoleApp)
	{
		QMessageBox::warning(NULL, tr("Error Message"), pStatus.getErrorText());
		QApplication::closeAllWindows();
	}
	else
	{
		qDebug()<<pStatus.getErrorText();
		QCoreApplication::quit();
	}
	return;
}


#endif //WINCE