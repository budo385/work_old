#ifndef DOCUMENTHELPER_H
#define DOCUMENTHELPER_H

#include <QObject>
#include <QStringList>
#include <QPixmap>
#include <QHash>
#include <QFileSystemWatcher>
#include "common/common/dbrecordset.h"
#include "common/common/status.h"

#define MIN_FILE_SIZE_FOR_HTTP_TRANSFER 512000 //100Kb
#define MAX_TEMPLATE_SIZE 160*1048576		   //16MB

class DocumentHelper : public QObject
{
	Q_OBJECT

public:

	static void					DeleteDocument(Status &err,int nDocumentID,QString strLockRes="");
	static bool					SaveFileContent(QByteArray byteFileContent,QString strFilePath,bool bContentCompressed=false, bool bSetReadOnly=false);
	static QString				LoadFileContent(QByteArray &byteFileContent,QString strFilePath="",bool bContentCompressed=false,bool bAskForLocation=false,int *nSizeUnCompressed=NULL);
	static QString				GetDefaultDocumentDirectory();
	static QString				GetCheckOutDocumentSavePath(QString strFileName,bool bUseTempFolder=false);
	static QString				GetCheckOutDocumentDirectoryPath(bool bUseTempFolder=false);
	static QString				GetTempDirectoryForAutomaticStorage();
	static bool					OpenDocumentInExternalApp_Shell(int nApplicationID,QString strDocPath,QString strParameters,QString strAppParameters="");
	static bool					OpenDirectoryInExternalApp_Shell(QString strDocPath);
	static bool					StartWINCEProcess(QString strFile);
	static bool					OpenDocumentFromAttachment(QByteArray &contentAttachCompressed, QString strFileName);
	static QVariant				GetApplicationFromExtension(QString strFileName,bool bUseAsExtension=false);
	static void					GetDocTemplates(DbRecordSet &lstData,bool bReloadFromServer=false);
	static void					GetDocApplications(DbRecordSet &lstData,bool bReloadFromServer=false);
	static void					GetDocUserPaths(DbRecordSet &lstData,bool bReloadFromServer=false);
	static QString				GetApplicationName(int nApplicationID);
	static QString				GetApplicationPath(int nApplicationID, QString strApplicationName);
	static void					SaveUserApplicationPath(int nApplicationID, QString strApplicationPath);
	static QPixmap				GetApplicationIcon(int nApplicationID);
	static QHash<int, QPixmap>	GetDocumentTypeIconHash();
	static QPixmap				GetDocumentTypeIcon(int nDocumentTypeID);
	static bool					CreateDocumentRevisionFromPath(DbRecordSet &rowRevision,QString &strPath,bool bAskForPathConfirmation=false,int *nSizeUnCompressed=NULL, bool bSkipLoadContent=false);
	static bool					PackDocuments(DbRecordSet &lstDocuments,QStringList &lstZippedFiles, bool bSkipZip=false);
	static bool					CreateDocumentFromTemplate(DbRecordSet &rowTemplate,DbRecordSet &rowDocData,DbRecordSet &rowRevision,bool bAskForNewName=false,bool bNoFileApp=false);
	static bool					CreateLocalDocumentFromTemplate(DbRecordSet &rowTemplate,DbRecordSet &rowDocData,bool bNoFileApp=false);
	static bool					CreateInternetDocumentFromTemplate(DbRecordSet &rowTemplate,DbRecordSet &rowDocData,DbRecordSet &rowRevision,bool bAskForNewName=true);
	
	static QString				CheckOutDocument(int nDocumentID,QString strFilePath,bool bReadOnly,DbRecordSet &rowRevision, DbRecordSet &recCheckOutInfo,bool bAskForLocation=false,bool bSkipLock=false,bool bDoNotTrackCheckOutDoc=false,QString strDefaultDirPath="");
	static bool					CheckInDocument(int nDocumentID,QString strFilePath,DbRecordSet &recInsertedDocumentRevision,QString strTag="",bool bOverWrite=false,bool bSkipLock=false, int nAskForTag=-1);
	static QString				ResolvePathBeforeCheckOutDocument(Status &err,int nDocumentID,QString strFilePath,int &nFileSize,bool bAskForLocation=false,QString strDefaultDirPath="", int nRevisionID=-1);
	static bool					ResolveRevisionBeforeCheckInDocument(int nDocumentID,QString strFilePath,int &nFileSize,DbRecordSet &recInsertedDocumentRevision,QString strTag="",bool bSkipLock=false,int nAskForTag=-1, bool bSkipLoadContent=false);
	static bool					CheckFileBeforeCheckInDocument(int nDocumentID,QString &strFilePath,bool bSkipLock=false);

	static bool					CancelCheckOutDocument(int nDocumentID, bool bSkipLock=false);
	static void					CheckInIfPossible(DbRecordSet &lstDocuments);
	static void					ClearCheckOutDocumentAfterDrag(int nDocumentID,QString strFilePath);
	static void					GetCheckedOutDocs(DbRecordSet &lstData,bool bReloadFromServer=false);
	static bool					TestDocumentRevisionsForMaximumSize(DbRecordSet &lstRevisons);
	static bool					TestDocumentRevisionsForMaximumSize(QString strPath);
	static bool					TestDocumentTemplateRevisionsForMaximumSize(int nDocumentID);
	static void					TestCheckedOutDocsForModificiation(DbRecordSet &lstData,QList<bool> &lstChangedByUser);
	static void					DocumentGarbageCollector();
	static void					SetDocumentDropDefaults(DbRecordSet *lstContacts=NULL,DbRecordSet *lstProjects=NULL,DbRecordSet *rowQCWContact=NULL);
	static void					GetDocumentDropDefaults(DbRecordSet *lstContacts=NULL,DbRecordSet *lstProjects=NULL,DbRecordSet *rowQCWContact=NULL);
	static int					AskForDocumentType();
	static void					ClearData();
	static bool					OpenDocumentInExternalApp(DbRecordSet &rowDocument,bool bNoFileDocument=false,QString strDefaultDirPath="", int nCheckOutCopy=-1);
	static void					RegisterDocumentPaths(DbRecordSet &lstPaths,QList<QUrl> &lstSource, int nIsTemplate=0, QString strCategory="",bool bSkipWrite=false, DbRecordSet *plstWrite=NULL, DbRecordSet *plstRevisions=NULL, int nDefaultDocType=-1, int nOpenDocumentFUIMode=-1);
	static void					RegisterDocumentInternetAddresses(DbRecordSet &lstPaths, bool bRegisterAsTemplate=false, QString strCategory="");
	static void					RegisterApplicationPaths(DbRecordSet &lstPaths);
	static void					DWatcher_SetFileChanged(QString strFilePath); 
	static QFileSystemWatcher*	DWatcher();
	static void					RegisterApplicationsAndTemplatesFromRFFile(DbRecordSet lstApps,DbRecordSet lstTemplates, DbRecordSet lstInterfaces, QString strRFFileDirectory);

//private:
	static QString				GetNewTempDirName(int nCount);
	static void					RedefineDefaultsList();
	static void					RedefineFileWatcherList();
	static void					DWatcher_ClearState();
	static void					DWatcher_AddFilePath(int nDocument,QString strFilePath);
	static void					DWatcher_RemoveFilePath(int nDocument);
	static bool					DWatcher_IsFileChanged(int &nDocument,QString strFilePath); 
	static bool					DWatcher_SaveChanges(bool bSkipAsk=false);
	static bool					DWatcher_CheckForUnsavedDocuments();
	static QString				CopyFromRevision(int nDocumentID,DbRecordSet &rowRevision,bool bAskForNewName=false,QString strNewProposedName="");
	static QString				CopyFromPath(QString strPath, QString strCopyToPath="",bool bSilent=false,QString strNewProposedName="");
	static QString				GetNewDocumentProposedName(QString strTemplateName);
	static bool					WriteDocument(DbRecordSet &lstDocs,DbRecordSet &lstRevisions,int nOpenDocumentFUIMode=-1);
	static void					ApplicationInstallFinder(int nDocTemplateID);
	static void					DetermineDocType(DbRecordSet &lstPaths, QList<QUrl> &lstSource);
	static void					RegisterTemplatesFromRFFile(DbRecordSet &lstTemplates, QString strRFFileDirectory, int &nDone, int &nSkipped,bool bOverrideApplicationID=false);
	static void					RegisterApplicationsFromRFFile(DbRecordSet lstApps, QString strRFFileDirectory, int &nDone, int &nSkipped);
	static void					RegisterInterfacesFromRFFile(DbRecordSet &lstInterfaces, QString strRFFileDirectory, int &nDone, int &nSkipped);
	static QString				AskForRevisionTag(int nDocumentID,QString strPath, int nAskForTag);
	static bool					TestForOfficeTemplateFiles(QString strPath);
	
};

#endif // DOCUMENTHELPER_H
