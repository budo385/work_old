#ifndef CLIENT_API_HELPER_H
#define CLIENT_API_HELPER_H

#include "common/common/status.h"

class ClientApiHelper
{
public:
	static void ProcessStoredProjectList_Import(Status &Ret_pStatus, QByteArray datHeader, QByteArray datDetail);
	
};

#endif //CLIENT_API_HELPER_H