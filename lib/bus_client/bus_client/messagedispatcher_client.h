#ifndef MESSAGEDISPATCHER_CLIENT_H
#define MESSAGEDISPATCHER_CLIENT_H


#include "bus_core/bus_core/messagedispatcher.h"

class MessageDispatcher_Client : public MessageDispatcher
{
	Q_OBJECT


protected slots:
	void NewMessage(int nMsgNumber);
	
};

#endif // MESSAGEDISPATCHER_CLIENT_H
