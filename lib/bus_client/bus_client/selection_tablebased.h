#ifndef SELECTION_TABLEBASED_H
#define SELECTION_TABLEBASED_H

#include "./generatedfiles/ui_selection_tablebased.h"
#include "bus_client/bus_client/mainentityselectioncontroller.h"



/*!
	\class  Selection_TableBased
	\brief  Selection frame for selecting Main entities , list/table based
	\ingroup GUICore


	Use:
	- Add own entity data in initialize method
	- Call Initialize() to init to specific entity
	- register as observer to this object (dynamic cast to subject or mainselectioncontroller)
	- use GetSelection() to fetch selected data 
	- see interface MainEntitySelectionController for details
*/
class Selection_TableBased : public QFrame, public MainEntitySelectionController
{
    Q_OBJECT

public:
    Selection_TableBased(QWidget *parent = 0);
    ~Selection_TableBased();

	void Initialize(int nEntityID,bool bSkipLoadingData=false,bool bIsFUISelectorWidget=true,bool bSingleClickIsSelection=true,bool bMultiSelection=false,bool bEnableDrag=true); 
	void GetSelection(int &nEntityRecordID, QString &strCode, QString &strName,DbRecordSet& lstEntityRecord);
	void RefreshDisplay(int nAction=REFRESH_RELOAD_ALL, int nPrimaryKeyValue=-1);
	bool blockSignals(bool b){blockObserverSignals(b);return QFrame::blockSignals(b);}
	void SetFilter(QString strColumnName, int nColumnVal,int nPosition=-1);
	void ReloadData();
		

private slots:
	void SlotReloadFromServer();				
	void SlotSelectionChanged();
	void SlotSelectionChanged_DoubleClicked();


private:

    Ui::Selection_TableBasedClass ui;
	bool m_bIsFUISelectorWidget;
	bool m_bSingleClickIsSelection;

signals:
	void SelectionChanged();
};



#endif





