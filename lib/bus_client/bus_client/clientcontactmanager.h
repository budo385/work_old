#ifndef CLIENTCONTACTTYPEMANAGER_H
#define CLIENTCONTACTTYPEMANAGER_H

#include <QVariant>
#include "common/common/dbrecordset.h"
#include <QtWidgets/QProgressDialog>
#include "common/common/observer_ptrn.h"
#include "common/common/status.h"

#define MAX_CONTACT_FAVORITES 12

/*!
	\class  ClientContactManager
	\brief  Used on client side for contact operations, uses global cache for type search
	\ingroup Bus_Client

	Handles address formatting, types, etc...
	Use:
	- load types into global cache, then use static or as normal
*/
class ClientContactManager : public QObject
{
	Q_OBJECT

public:
	ClientContactManager();
	~ClientContactManager();

	enum FormatOps
	{
		FORMAT_USING_USERDEFAULTS,					//reads schemas from user defaults
		FORMAT_USING_STORED_FORMATSCHEMA,			//format schema using BCMA_FORMATSCHEMA_ID inside list of addresses
		FORMAT_USING_HARDCODE_DEFAULTSCHEMA			//format schema using hardcoded schema
	};

	enum ContactOperations
	{
		OP_ADD,
		OP_REMOVE,
		OP_REPLACE,
		OP_INTERSECT,

	};

	static bool FormatAddressWithDefaultSchema(DbRecordSet &lstAddresses, int nContactType,int nOperation, bool bSkipFormattingIfExists);
	static QVariant GetDefaultType(int nEntityTypeID);
	static QVariant GetUserTypeBasedOnSystemType(int nEntityTypeID, int nSystemTypeID);
	static QString GetTypeName(int TypeID);
	static bool GetTypeList(int TypeEntityID,DbRecordSet &lstData);
	static QString GetFirstLettersFromString(QString strString);
	static void GetFullContactGridSetup(DbRecordSet &lstData);
	static void GetBuildAllContactGridSetup(DbRecordSet &lstData);
	static void GetDefaultContactGridSetup(DbRecordSet &lstData);
	static void TranslateViewList(DbRecordSet &lstData);
	static void PrepareListsWithTypeName(DbRecordSet *pLstEmail,DbRecordSet *pLstPhone,DbRecordSet *pLstInternet,DbRecordSet *pLstAddress,DbRecordSet *pLstAddressTypes);
	static void AssignChildToParent(DbRecordSet &lstParent,QString strChildSubListCol,DbRecordSet &lstChilds,QString strFKCol,QString strParentIDCol="");
	void ConvertContactListFromImport(DbRecordSet &lstContactTxtFormat,DbRecordSet &lstContact, QString strOrganizationNameForced, QProgressDialog *pDlg = NULL);
	void PrepareContactListFromImport(DbRecordSet &lstContact, QProgressDialog *pDlg = NULL);
	static void CompareLists(DbRecordSet &lstContact1,DbRecordSet &lstContact2,DbRecordSet *lstContactMatch=NULL,DbRecordSet *lstContactNew=NULL, int nPKIdx1=0,int nPKIdx2=0);
	static bool CreateSubTypeAddressList(DbRecordSet &lstAddress,DbRecordSet &lstTypes,QString strSubTypeColName="LST_TYPES");
	static void LoadBigPictureList(Status &Ret_pStatus,DbRecordSet &lstData,QString strBigPicIDColName);
	static bool SelectGroupsForCopyContact(DbRecordSet &lstGroupsForCopy);
	static bool SelectPhoneNumberOfContact(DbRecordSet &lstPhones,QList<int> lstAllowedSystemPhoneTypes,QStringList &lstSelectedPhones);
	
	//VCARD:
	static DbRecordSet CreateContactsFromvCard(DbRecordSet &lstVcards);
	static DbRecordSet CreateContactsFromvCardUrl(QString strURL);
	static DbRecordSet CreateContactsFromvCardData(QString strvCard);
	void CreateFullContactFromvCardData(QString strvCard, bool bIsUrl, DbRecordSet &lstContact);
	void CreateFullContactFromvCardFile(DbRecordSet &lstVcards, DbRecordSet &lstContact);
	static void Read_vCard(const QString &strContents, DbRecordSet &lstResult);
	static void Write_vCard(const DbRecordSet &lstContents, QString &strResult);
	static bool LoadFileFromURL(const QString &strURL, QByteArray &strResult);

	//MB QCW contact parser:
	void Parse_Text(QString strText, DbRecordSet &lstContact);
	void Parse_TextForm(QString strText, DbRecordSet &lstContact);
	static void Parse_Addresses(DbRecordSet &lstContactLines,DbRecordSet &recAddress);
	static void Parse_Mail(QString strLine, QString &strEmail, QString &strDesc);
	static void Parse_Internet(QString strLine, QString &strUrl, QString &strDesc);
	static void Parse_Phone(QString strLine, QString &strPhone, QString &strDesc,int &nType);
	static int CountDigits(QString strLine);
	static void ExtractTownNames(QString strLine,QString &strTown,QString &strState,int &nZIP);
	static void ExtractNames(QString strLine1,QString strLine2,QString &strFirstName,QString &strLastName,QString &strMiddleName,QString &strCompanyName);
	static int FindLargestInteger(QString strLine);
	static void Parse_AddressForm(DbRecordSet &lstContactLines,DbRecordSet &recAddress);
	static void NotifyCacheAfterContactWrite(bool bIsInsert,DbRecordSet recNewData,ObsrPtrn_Observer *pSender=NULL);
	static void ReadContactDetails(Status &pStatus,DbRecordSet &lstSourceID,DbRecordSet &lstContactDetails);

	
	//FAVORITES (server calls):
	static void AddFavorite(Status &pStatus,int nContactID);
	static void RemoveFavorite(Status &pStatus,int nContactID);
	static void ChangeFavorite(Status &pStatus,DbRecordSet &rowData);
	static void AddPictureFavorite(Status &pStatus,int nContactID);
	//FAVORITES (offline & server calls):
	static void GetFavoriteList(Status &pStatus,DbRecordSet &lstFavorites, bool bOfflineMode=false, bool bReloadFromServer=false);
	static void GetContactData(Status &pStatus,int nContactID, DbRecordSet &rowData, bool bOfflineMode=false, bool bReloadFromServer=false);	
	//FAVORITES (offline calls, must be done before any call, or after connection down):
	static void Offline_SaveData(Status &pStatus);
	static void Offline_SaveContactList(Status &pStatus,DbRecordSet &lstContacts);
	static void Offline_LoadData(Status &pStatus);

private:
	static void DeleteOrphanFavorite(DbRecordSet lstAllFavs);

};

#endif // CLIENTCONTACTTYPEMANAGER_H
