#ifndef STOREDPROJLISTSDLG_H
#define STOREDPROJLISTSDLG_H

#include <QtWidgets/QDialog>
#include "ui_storedprojlistsdlg.h"

class StoredProjListsDlg : public QDialog
{
	Q_OBJECT

public:
	StoredProjListsDlg(int nCurListID = -1, QWidget *parent = 0);
	~StoredProjListsDlg();

	int GetSelectedListID();
	QString GetSelectedName();

private:
	Ui::StoredProjListsDlgClass ui;

private slots:
	void on_btnCancel_clicked();
	void on_btnOK_clicked();

};

#endif // STOREDPROJLISTSDLG_H
