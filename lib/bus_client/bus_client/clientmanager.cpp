#include "clientmanager.h"
#include "common/common/config.h"
#include "common/common/config_version_sc.h"
#include <QApplication>
#include <QWidget>
#include <QDesktopServices>
#include <QMessageBox>
#include "common/common/datahelper.h"
#include "bus_client/bus_client/mainentityselectioncontroller.h"
#include "common/common/entity_id_collection.h"

#include "dlg_trialpopup.h"
#include "gui_core/gui_core/timemessagebox.h"
#include "common/common/authenticator.h"
#include "trans/trans/httpreadersync.h"
#include <QCoreApplication>
#include <QDomDocument>
#include <QFileDialog>

extern int	g_IsThinClientMode;						//1 if thin, else thick

#include "modulelicenseclientcache.h"
extern ModuleLicenseClientCache			g_FunctionPoint;			//global function point tester
#include "loggeduserdata.h"
extern LoggedUserData					g_LoggedUserData;			//global cache for user defaults
#include "bus_client/bus_client/clientoptionsandsettingsmanager.h"
extern ClientOptionsAndSettingsManager g_Settings;

#include "common/common/logger.h"
extern Logger					g_Logger;
#include "clientbackupmanager.h"
extern ClientBackupManager				g_BackupManager;			//database manager

ClientManager::ClientManager()
:app(NULL),m_strModuleCode(""),m_strMLIID(""),m_bIsLogged(false),m_bServiceStarted(false),m_nLastFatalErrorCode(0),m_nStartUpWizard(0)
{
	m_strUpdateCheckURL="http://www.sokrates-communicator.com/version/";		//->setup.exe or version.txt
}

ClientManager::~ClientManager()
{

}



//logins, take care of all garbage collecting before
void ClientManager::Login(Status &pStatus,QString pStrUserName,QString pStrPassword)
{
	Q_ASSERT(app!=NULL); //must be set
	Q_ASSERT(!m_bIsLogged); //already logged

	pStatus.setError(0);

	g_LoggedUserData.SetLoggedUserName(pStrUserName);
	//m_strUserName=pStrUserName;	//save user name

	ProcessBeforeLogin(pStatus);
	if(!pStatus.IsOK()) return;

	//login:
	app->UserLogon->Login(pStatus,pStrUserName,pStrPassword,GetModuleCode(),GetMLIID());
	if(!pStatus.IsOK())
	{
		if (pStatus.getErrorCode()==StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH)
		{
			pStatus.setError(StatusCodeSet::ERR_SYSTEM_VERSION_MISMATCH);
			AskForUpdate("");
		}
		return;
	}

	//check APP version:
	QString strAppServerVersion,strAppServerDatabaseVersion,strTemplateInUse;
	int nAskForTemplate;
	QByteArray byteDemoKey;
	app->CoreServices->CheckVersion(pStatus,QString(APPLICATION_VERSION),strAppServerVersion,strAppServerDatabaseVersion,strTemplateInUse,nAskForTemplate,m_nStartUpWizard,byteDemoKey);
	if(!pStatus.IsOK())
	{
		Status err;
		app->UserLogon->Logout(err);
		if (pStatus.getErrorCode()==StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH)
		{
			pStatus.setError(StatusCodeSet::ERR_SYSTEM_VERSION_MISMATCH);
			AskForUpdate(strAppServerVersion);
		}
		return;
	}

	if (strAppServerVersion!=QString(APPLICATION_VERSION))
	{
		Status err;
		app->UserLogon->Logout(err);
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_VERSION_MISMATCH);
		AskForUpdate(strAppServerVersion);
		return;
	}


	//check DB:
	if (strAppServerDatabaseVersion!=QVariant(DATABASE_VERSION).toString())
	{
		app->UserLogon->Logout(pStatus);	//logout first:
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_VERSION_MISMATCH);
		//TEMPORARY: ask version:
		QString strMsg=tr("Database version do not match, notify administrator!");
		int nResult=QMessageBox::information(NULL,tr("Warning"),strMsg);
		return;
	}

	

	ProcessAfterLogin(pStatus); //inits global objects
	if(!pStatus.IsOK()) 
	{
		Status err;
		app->UserLogon->Logout(err);
		//Q_ASSERT(err.IsOK());//if error
		return;
	}

	//check for updates (only for THICK):issue 1099 + 1378
	if (!g_IsThinClientMode)
		if(g_Settings.GetPersonSetting(THICK_CHECK_FOR_UPDATES).toBool())
			if (CheckForUpdates()==2)
			{
				pStatus.setError(StatusCodeSet::ERR_SYSTEM_VERSION_MISMATCH);
				return;
			}


	//reset flag if logged:
	m_INIFile.m_nOrderAsk=0;	


	//--------------------------------------------------------
	// CHECK FOR END SUBSCRIPTION PERIOD: must be after login
	//--------------------------------------------------------
	if (!CheckSubscriptionPeriod())
	{
		Status err;
		app->UserLogon->Logout(err);
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_VERSION_MISMATCH);		//quit application
	}





	//-------------------------------------------------
	// CHECK FOR TEMPLATE DB: check for template database
	//-------------------------------------------------
	if (nAskForTemplate)
	{
		if(g_BackupManager.AskForTemplateDatabase())
		{
			//store username, right now!
			m_INIFile.m_strLastUserName=pStrUserName;
			m_INIFile.m_nHideLogin=1; //on next startup login is skiped!
			m_INIFile.Save();
			if (g_IsThinClientMode)
			{
				Status err;
				app->UserLogon->Logout(err);
				pStatus.setError(StatusCodeSet::ERR_SYSTEM_VERSION_MISMATCH);		//quit application
			}
			else
			{
				//restart if thick:
				Status err;
				app->UserLogon->Logout(err);
				pStatus.setError(StatusCodeSet::ERR_SYSTEM_SILENT_LOGIN_AGAIN);
			}
			return;
		}
	}



	//-------------------------------------------------
	// START UP WIZARD: after always reset FLAG
	//-------------------------------------------------
	//if wizard is executed: reload user data
	//if (nStartUpWizard)
	//{
	//	if(ProcessOnStartUpWizard(pStatus))
	//		return;
	//}




	int nPeriod=CheckForDemoPeriod(pStatus,byteDemoKey);
	if(!pStatus.IsOK()) 
	{
		Status err;
		app->UserLogon->Logout(err);
		//Q_ASSERT(err.IsOK());//if error
		return;
	}


	//-------------------------------------------------
	// CHECK FOR ORDER:
	//-------------------------------------------------
	if (nPeriod>=0 && nPeriod<=10) //only last 10 days check
	{
		Dlg_TrialPopUp Dlg;
		Dlg.Initialize(nPeriod,GetOrderFormURL(),g_IsThinClientMode);
		Dlg.exec();
		if (Dlg.GetState()!=0)
		{
			//QString strMsg=tr("Application will be restarted to complete registration procedure\n\n");
			//TimeMessageBox box(4,true,QMessageBox::Question,tr("Restart"),strMsg,QMessageBox::Ok);
			//box.exec();	//returns buttons->but if timeout then it will be undefined...
			if (!Dlg.GetLicensePath().isEmpty())
				m_INIFile.m_strKeyFilePath==Dlg.GetLicensePath();
			m_INIFile.m_strLastUserName=pStrUserName;
			m_INIFile.Save();
			Status err;
			app->UserLogon->Logout(err);
			pStatus.setError(StatusCodeSet::ERR_SYSTEM_VERSION_MISMATCH);
			return;
		}
	}








	m_bIsLogged=true;
}


//logins, take care of all garbage collecting after
void ClientManager::Logout(Status &pStatus)
{
	pStatus.setError(0);
	m_nStartUpWizard=0;	
	if(!m_bServiceStarted) return; //if service not started just exit
	if(!m_bIsLogged) return; //if not logged just exit


	Q_ASSERT(app!=NULL); //must be set

	ProcessBeforeLogout(pStatus);
	if(!pStatus.IsOK()) return;

	app->UserLogon->Logout(pStatus);
	if(!pStatus.IsOK()) return;


	m_bIsLogged=false;

	ProcessAfterLogout(pStatus);
	if(!pStatus.IsOK()) return;

}


void ClientManager::ClearUserData()
{
	//m_strUserName="";
	//m_strCurrentConnectionName="";
}

void ClientManager::HideCommunicationInProgress(bool bHide)
{
	if (bHide)
	{
		blockSignals(true);
	} 
	else
	{
		blockSignals(false);
	}

}

//fatal error occurred: it is dangerous to process this event while thread is working, 
//schedule event when event loop is returned, in mean time, block socket operations to prevent strange states
void ClientManager::OnCommunicationFatalError(int val)
{
	m_nLastFatalErrorCode=val;
	FatalErrorDetected(val);
	m_nGUICloseRetries=0;
	CloseAllModalWindows();			//now this is essential
	//emit CommunicationFatalError(m_nLastFatalErrorCode);
	//QTimer::singleShot(0,this,SLOT(OnCommunicationFatalErrorDelayNotification()));
}



//when event loop is non working: use this to notify others
void ClientManager::OnCommunicationFatalErrorDelayNotification()
{
	//if (m_nLastFatalErrorCode==0) return;
	emit CommunicationFatalError(m_nLastFatalErrorCode);
	//m_nLastFatalErrorCode=0;
}


//close all top windows
void ClientManager::CloseAllModalWindows()
{
	QWidget *pWidget=QApplication::activeModalWidget();
	while (pWidget)
	{
		pWidget->close();
		pWidget->deleteLater();		//queue for deletion
		QCoreApplication::instance()->processEvents();
		pWidget=QApplication::activeModalWidget();
	}

	
	if (m_nGUICloseRetries==GUI_CLOSE_WINDOW_RETIRES_AFTER_BROKEN_CONN)
	{
		QTimer::singleShot(0,this,SLOT(OnCommunicationFatalErrorDelayNotification()));
	}
	else
	{
		QTimer::singleShot(0,this,SLOT(CloseAllModalWindows()));
	}

	m_nGUICloseRetries++;
}

void ClientManager::GetDefaultConnectionParameters(QString &strUserName,QString &strConnection)
{
	strUserName=m_INIFile.m_strLastUserName;
	strConnection=m_INIFile.m_strConnectionName;
}



QString ClientManager::GetOrderFormURL()
{
	if (!m_INIFile.m_strOrderURL.isEmpty())
		return m_INIFile.m_strOrderURL;
	else
		return "http://www.sokrates-communicator.ch/";

}


//0 -expired, -1 no demo, else demo days left
int ClientManager::CheckForDemoPeriod(Status &pStatus,QByteArray key)
{

	if (!g_FunctionPoint.IsFPAvailable(FP_SHOW_ORDER_BUTTON))
		return -1;

	int nDemoPeriod;
	if (!g_FunctionPoint.IsFPAvailable(FP_EVALUATION_PERIOD,nDemoPeriod))
		return -1;

	
	//read key;
	QDateTime lastModif,datStart;
	int nSize,nLocked;
	int nDbVersion;
	QString strAppVersion;
	if(!Authenticator::Demo_ParseKey(key,strAppVersion,nDbVersion,lastModif,nSize,nLocked,datStart))
	{
		Authenticator::Demo_CreateKey(key,APPLICATION_VERSION,DATABASE_VERSION,lastModif,nSize,1,datStart);	//lock
		app->CoreServices->UpdateVersion(pStatus,key);
		//if (!pStatus.IsOK())
		return 0; //expired
	}

	if (nLocked!=0)
		return 0;

	if(nDbVersion!=DATABASE_VERSION)
	{
		Authenticator::Demo_CreateKey(key,APPLICATION_VERSION,DATABASE_VERSION,lastModif,nSize,1,datStart); //lock
		app->CoreServices->UpdateVersion(pStatus,key);
		//if (!pStatus.IsOK())
		return 0;
	}


	//ONLY for THICK client check size & date of sokrates.exe
	if (!g_IsThinClientMode)
	{
		//if application newer, update:
		bool bUpdate=false;
		if (strAppVersion!=QString(APPLICATION_VERSION))
		{
			//calc chk sum & last modif:
			QFileInfo sokrates(QCoreApplication::applicationDirPath()+"/sokrates.exe");
			if (sokrates.exists())
			{
				lastModif=sokrates.lastModified();
				nSize=sokrates.size();
			}
			Authenticator::Demo_CreateKey(key,APPLICATION_VERSION,DATABASE_VERSION,lastModif,nSize,0,datStart); 
			app->CoreServices->UpdateVersion(pStatus,key);
			if (!pStatus.IsOK())
				return -1;
		}
		else //check sums
		{
			//calc chk sum & last modif:
			QFileInfo sokrates(QCoreApplication::applicationDirPath()+"/sokrates.exe");
			if (sokrates.exists())
			{
				QDateTime curr_lastModif=sokrates.lastModified();
				int cur_nSize=sokrates.size();
				qDebug()<<lastModif;
				qDebug()<<curr_lastModif;

//to test last modified?
				//if (lastModif!=curr_lastModif || cur_nSize!=nSize)
				if (cur_nSize!=nSize)
				{
					Authenticator::Demo_CreateKey(key,APPLICATION_VERSION,DATABASE_VERSION,lastModif,nSize,1,datStart); //lock
					app->CoreServices->UpdateVersion(pStatus,key);
					//if (!pStatus.IsOK())
					return 0;
				}
			}
		}
	}


	//if empty
	if (datStart.isNull())
	{

		Authenticator::Demo_CreateKey(key,APPLICATION_VERSION,DATABASE_VERSION,lastModif,nSize,0,QDateTime::currentDateTime()); 
		app->CoreServices->UpdateVersion(pStatus,key);
		if (!pStatus.IsOK())
			return 0;
		else
			return nDemoPeriod;
	}


	//finally check date:
	int nPeriod=datStart.daysTo(QDateTime::currentDateTime());
	if (nPeriod<0)
	{		
		Authenticator::Demo_CreateKey(key,APPLICATION_VERSION,DATABASE_VERSION,lastModif,nSize,1,datStart); //lock
		app->CoreServices->UpdateVersion(pStatus,key);
		//if (!pStatus.IsOK())
		return 0;
	}

	//lock
	if (nDemoPeriod<nPeriod)
	{
		Authenticator::Demo_CreateKey(key,APPLICATION_VERSION,DATABASE_VERSION,lastModif,nSize,1,datStart); //lock
		app->CoreServices->UpdateVersion(pStatus,key);
		//if (!pStatus.IsOK())
		return 0;
	}

	int nLeft=nDemoPeriod-nPeriod;
	if(nLeft==0)nLeft=1; //last day, show hes got 1 day
	return nLeft;
}



void ClientManager::AskForUpdate(QString strServerActualVersion)
{
	
	QApplication::setOverrideCursor(QCursor(Qt::ArrowCursor)); //issue 1272

	double fVersionLatest;
	QString strUrl;
	QString strVersionID;
	GetAllVersionData(strVersionID,strUrl,fVersionLatest);

	//double fCurrentClient=QString(APPLICATION_VERSION).toDouble();
	double fCurrentClient=QString("20080142").toDouble();
	double fCurrentServer=strServerActualVersion.toDouble();
	if (strServerActualVersion.isEmpty())
	{
		fCurrentServer=fVersionLatest;
	}


	//ALG: if client < server && server != latest -> upgrade to server ver
	//ALG: if client < server && server = latest -> normal
	//ALG: if client > server -> ask for downgrade to server ver
	QString strMsg;
	bool bSetToServerVersion=false;
	if (fCurrentServer==fVersionLatest)
	{
			strMsg=tr("Your version %1 is not actual any more. Do you want to download and install an update to version %2 now?");
			strMsg=strMsg.arg(QString(APPLICATION_VERSION_ID));
			strMsg=strMsg.arg(strVersionID);
	}
	else //fCurrentServer<=fVersionLatest
	{
		if (fCurrentClient>fCurrentServer)
		{
			strMsg=tr("Your version %1 is newer than server version. Do you want to download and install a downgrade to the server version now?");
			strMsg=strMsg.arg(QString(APPLICATION_VERSION_ID));
			strMsg=strMsg.arg(strVersionID);
		}
		else
		{
			strMsg=tr("Your version %1 is not actual any more. Do you want to download and install an update to the version used by the server?");
			strMsg=strMsg.arg(QString(APPLICATION_VERSION_ID));
			strMsg=strMsg.arg(strVersionID);
		}
		bSetToServerVersion=true;
	}

		
	int nResult=QMessageBox::question(NULL,tr("Warning"),strMsg,tr("Yes"),tr("No"));
	if (nResult==0)
	{
		//->link
		if (bSetToServerVersion)
			QDesktopServices::openUrl(GetSetupExePathBasedOnVersion(strServerActualVersion));
		else
			QDesktopServices::openUrl(GetSetupExePath());
	}

	QApplication::restoreOverrideCursor(); //issue 1272
}

//read all version.xml...search installation by date
QString ClientManager::GetSetupExePathBasedOnDate(QDate datValidPeriod)
{
	//read from given address version.txt
	HttpReaderSync WebReader(6000);  //6sec..
	QByteArray strResult;
	WebReader.ReadWebContent(m_strUpdateCheckURL+"version.xml", &strResult);;

	int nStartPos=0;
	QDate dateTest;

	//parse XML:
	QDomDocument domDocument;
	if (!domDocument.setContent(strResult)) 
		return "";
	QDomNodeList ListOfElements	= domDocument.elementsByTagName("SOKRATES_Downloads");
	QDomNode Node = ListOfElements.at(0).firstChild();
	while(!Node.isNull()) 
	{
		QDomElement Element = Node.toElement();
		QString name = Element.tagName();
		if (name=="CommunicatorClient")
		{
			dateTest=QDate::fromString(Element.attribute("Date"),"dd.MM.yyyy");
			if (dateTest<datValidPeriod)
			{
				return Element.attribute("DownloadURL");
			}
		}
		Node = Node.nextSibling();
	}

	return "";
}



//read all version.xml...search installation by date
QString ClientManager::GetSetupExePathBasedOnVersion(QString strVersion)
{
	//read from given address version.txt
	HttpReaderSync WebReader(6000);  //6sec..
	QByteArray strResult;
	WebReader.ReadWebContent(m_strUpdateCheckURL+"version.xml", &strResult);;

	int nStartPos=0;
	QDate dateTest;
	QString strVer="";

	//parse XML:
	QDomDocument domDocument;
	if (!domDocument.setContent(strResult)) 
		return "";
	QDomNodeList ListOfElements	= domDocument.elementsByTagName("SOKRATES_Downloads");
	QDomNode Node = ListOfElements.at(0).firstChild();
	while(!Node.isNull()) 
	{
		QDomElement Element = Node.toElement();
		QString name = Element.tagName();
		if (name=="CommunicatorClient")
		{
			if (Element.attribute("VersionCounter")==strVersion)
			{
				return Element.attribute("DownloadURL");
			}
		}
		Node = Node.nextSibling();
	}

	return "";
}

QString ClientManager::GetSetupExePath()
{
	//read from given address version.txt
	HttpReaderSync WebReader(6000);  //6sec..
	QByteArray strResult;
	WebReader.ReadWebContent(m_strUpdateCheckURL+"version.xml", &strResult);;
	QString strAppServerVersion(strResult);

	int nPos1=strResult.indexOf("DownloadURL");
	nPos1=strResult.indexOf("\"",nPos1);
	int nPos2=strResult.indexOf("\"",nPos1+1);

	if (nPos1<=0 || nPos2<=0)
		return "";

	QString strUrl=strResult.mid(nPos1+1,nPos2-nPos1-1);
	return strUrl;
}

QString ClientManager::GetVersionID()
{

	//read from given address version.txt
	HttpReaderSync WebReader(6000);  //6sec..
	QByteArray strResult;
	WebReader.ReadWebContent(m_strUpdateCheckURL+"version.xml", &strResult);;

	QString strAppServerVersion(strResult);

	int nPos1=strResult.indexOf("VersionID");
	nPos1=strResult.indexOf("\"",nPos1);
	int nPos2=strResult.indexOf("\"",nPos1+1);

	if (nPos1<=0 || nPos2<=0)
		return "";

	QString strUrl=strResult.mid(nPos1+1,nPos2-nPos1-1);
	return strUrl;
}


double ClientManager::GetVersion()
{

	//read from given address version.txt
	HttpReaderSync WebReader(6000);  //6sec..
	QByteArray strResult;
	WebReader.ReadWebContent(m_strUpdateCheckURL+"version.xml", &strResult);;

	QString strAppServerVersion(strResult);

	int nPos1=strResult.indexOf("VersionCounter");
	nPos1=strResult.indexOf("\"",nPos1);
	int nPos2=strResult.indexOf("\"",nPos1+1);

	if (nPos1<=0 || nPos2<=0)
		return -1;

	QString strUrl=strResult.mid(nPos1+1,nPos2-nPos1-1);
	bool bOK=false;
	double val=strUrl.toDouble(&bOK);
	if (bOK)
		return val;
	else
		return -1;
}


void ClientManager::GetAllVersionData(QString &versionid, QString &path,double &version)
{

	//read from given address version.txt
	HttpReaderSync WebReader(6000);  //6sec..
	QByteArray strResult;
	WebReader.ReadWebContent(m_strUpdateCheckURL+"version.xml", &strResult);;
	QString strAppServerVersion(strResult);


	//path
	{
		int nPos1=strResult.indexOf("DownloadURL");
		nPos1=strResult.indexOf("\"",nPos1);
		int nPos2=strResult.indexOf("\"",nPos1+1);

		if (nPos1<=0 || nPos2<=0)
		{
			path="";
		}
		else
		{
			path=strResult.mid(nPos1+1,nPos2-nPos1-1);
		}

	}


	//versionid
	{

		int nPos1=strResult.indexOf("VersionID");
		nPos1=strResult.indexOf("\"",nPos1);
		int nPos2=strResult.indexOf("\"",nPos1+1);

		if (nPos1<=0 || nPos2<=0)
		{
			versionid="";
		}
		else
		{
			versionid=strResult.mid(nPos1+1,nPos2-nPos1-1);
		}
	}


	//version
	{

		int nPos1=strResult.indexOf("VersionCounter");
		nPos1=strResult.indexOf("\"",nPos1);
		int nPos2=strResult.indexOf("\"",nPos1+1);

		if (nPos1<=0 || nPos2<=0)
		{
			version=-1;
		}
		else
		{
			QString strUrl=strResult.mid(nPos1+1,nPos2-nPos1-1);
			bool bOK=false;
			version=strUrl.toDouble(&bOK);
			if (!bOK)
				version= -1;
		}
	}



}


bool ClientManager::CheckSubscriptionPeriod()
{
	return true;
 	QDate dateEnd=QDate(2007,8,8);
	QDate dateWarning=QDate(2007,8,4);

	if (!dateEnd.isValid())
		return true;

	//check registry:
	QString strCode=GetModuleCode();
	QSettings settingsTE("HKEY_LOCAL_MACHINE\\Software\\Helix Business Soft\\SOKRATES Communicator Team Edition\\Settings",	QSettings::NativeFormat);
	QSettings settingsPE("HKEY_LOCAL_MACHINE\\Software\\Helix Business Soft\\SOKRATES Communicator Personal Edition\\Settings",	QSettings::NativeFormat);
	QSettings settingsBE("HKEY_LOCAL_MACHINE\\Software\\Helix Business Soft\\SOKRATES Communicator Business Edition\\Settings",	QSettings::NativeFormat);

	QString strDateExpiredRegistry;
	if (strCode=="SC-TE")
	{
		strDateExpiredRegistry=settingsTE.value("Subscription End Date",QString("")).toString();
	}
	else if (strCode=="SC-PE")
	{
		strDateExpiredRegistry=settingsPE.value("Subscription End Date",QString("")).toString();
	}
	else if (strCode=="SC-BE")
	{
		strDateExpiredRegistry=settingsBE.value("Subscription End Date",QString("")).toString();
	}

	//if wrong then write:
	QString strDateEnd=dateEnd.toString("yyyyMMdd");
	if (strDateExpiredRegistry.isEmpty() || strDateExpiredRegistry!=strDateEnd);
	{
		if (strCode=="SC-TE")
		{
			settingsTE.setValue("Subscription End Date",strDateEnd);
		}
		else if (strCode=="SC-PE")
		{
			settingsPE.setValue("Subscription End Date",strDateEnd);
		}
		else if (strCode=="SC-BE")
		{
			settingsBE.setValue("Subscription End Date",strDateEnd);
		}
	}



	QDate dateBuild=QDate::fromString(APPLICATION_BUILD_DATE,"dd.MM.yyyy");
	//qDebug()<<dateBuild;


	if (QDate::currentDate()<dateWarning)
		return true;

	if (QDate::currentDate()<=dateEnd && QDate::currentDate()>dateWarning)
	{
		int nDaysLeft=dateWarning.daysTo(dateEnd);
		QString strMesg=tr("You have ")+QVariant(nDaysLeft).toString()+ tr(" days remaining before the update subscription expires. Please renew it to be able to download updates in the future!");
		QMessageBox::warning(NULL,tr("Warning"),strMesg);
		return true;
	}

	//check version:
	if (QDate::currentDate()>dateEnd)
	{
		if (dateBuild>dateEnd)
		{
			int nResult=QMessageBox::question(NULL,tr("Subscription period expired!"),tr("You can not use the new version because the update subscription period has expired. Order/renew your license to use this application!"),tr("  Renew update subscription "),tr("  Downgrade the un-licensed updated version to the previous, licensed one   "),tr(" Cancel "),0,2);
			if (nResult==0)
			{
				QString strUrl=GetOrderFormURL();
				QDesktopServices::openUrl(strUrl);
				return false;
			}
			else if (nResult==1)
			{
				//load version.xml, parse old valid version, download it
				QString strPath=GetSetupExePathBasedOnDate(dateEnd);
				if (strPath.isEmpty())
				{
					QMessageBox::critical(NULL,tr("Error"),tr("Could not fetch old version. Please try manually!"));
					return false;
				}
				else
				{
					QDesktopServices::openUrl(strPath);
					return false;
				}
			}
			else
				return false; //exit
		}
	}

	return true;
}


void ClientManager::OnServerMessageRecieved(DbRecordSet rowMsg)
{
	//open qmessage
	Status err;

	if (rowMsg.getRowCount()==0)
		return;

	err.setError(rowMsg.getDataRef(0,"MSG_CODE").toInt(),rowMsg.getDataRef(0,"MSG_TEXT").toString());

	switch(rowMsg.getDataRef(0,"MSG_TYPE").toInt())
	{
	case StatusCodeSet::TYPE_WARNING:
		{
			QMessageBox::warning(NULL,tr("New message"),err.getErrorText());
			g_Logger.logMessage(StatusCodeSet::TYPE_WARNING,rowMsg.getDataRef(0,"MSG_CODE").toInt(),rowMsg.getDataRef(0,"MSG_TEXT").toString());
		}
		break;
	case StatusCodeSet::TYPE_ERROR:
		{
			QMessageBox::critical(NULL,tr("New message"),err.getErrorText());
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,rowMsg.getDataRef(0,"MSG_CODE").toInt(),rowMsg.getDataRef(0,"MSG_TEXT").toString());
		}
	    break;
	default:

		QMessageBox::information(NULL,tr("New message"),err.getErrorText());
		g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,rowMsg.getDataRef(0,"MSG_CODE").toInt(),rowMsg.getDataRef(0,"MSG_TEXT").toString());
	    break;
	}
	

}

void ClientManager::CheckServerMessagesAfterLogin()
{
	DbRecordSet lstMsg;
	g_LoggedUserData.GetServerMsg(lstMsg);
	if (lstMsg.getRowCount()==0)
		return;


	int nSize=lstMsg.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		//dispatch messages:
		OnServerMessageRecieved(lstMsg.getRow(i));
	}

}

bool ClientManager::SaveIniFile()
{
	return m_INIFile.Save();
}



//settings are stored in application home, path
//if directory does not exists, first time, all files will be copied there
QString ClientManager::CheckSettingsDirectory(Status &pStatus)
{
	pStatus.setError(0);

	bool bCreated=false;
	QString strSettingsDir=DataHelper::GetApplicationHomeDir()+"/settings";
	QDir setDir(strSettingsDir);
	if (!setDir.exists())
	{
		QString strAppDir=DataHelper::GetApplicationHomeDir();
		QDir bcpDirCreate(strAppDir);
		if(bcpDirCreate.mkdir("settings"))
		{
			bCreated=true;
			strSettingsDir=strAppDir+"/settings";
		}
		else
		{
			pStatus.setError(1,tr("Settings directory:")+strAppDir+"/settings"+tr(" could not be created!"));
			return "";
		}
	}


	//if just created copy from template to that dir:
	//it will copy only missing files (coz installation now creates directory)
	DataHelper::CopyFolderContent(QCoreApplication::applicationDirPath()+"/settings",strSettingsDir);
	return strSettingsDir;
}


void ClientManager::InitializeService(Status &pStatus,ClientIniFile &ini)
{
	pStatus.setError(0);
	m_INIFile=ini;
	m_strModuleCode=m_INIFile.m_strModuleCode;
	m_strMLIID=m_INIFile.m_strMLIID;
}

QString ClientManager::GetKeyFileDlg()
{
	QString strStartDir=QDir::currentPath(); 
	QString strFilter="*.key";

	//open file dialog:
	QString strFile = QFileDialog::getOpenFileName(
		NULL,
		tr("Select KeyFile"),
		strStartDir,
		strFilter);

	return strFile;
}