#ifndef CLIENTOPTIONSANDSETTINGSMANAGER_H
#define CLIENTOPTIONSANDSETTINGSMANAGER_H

#include "bus_core/bus_core/optionsandsettingsmanager.h"
#include "common/common/status.h"

class ClientOptionsAndSettingsManager : public OptionsAndSettingsManager
{
public:
	ClientOptionsAndSettingsManager();
	~ClientOptionsAndSettingsManager();

	void LoadSettings(Status &status);
	void ReaLoadAvatarStartupSetting(Status &status);
	void LoadOptions(Status &status);
	void SaveApplicationOptions(Status &status, DbRecordSet *recDataOverwrite=NULL);
	void SavePersonSettings(Status &status, DbRecordSet *recDataOverwrite=NULL);

private:
	void SaveSettingsByRecordSet(Status &status, DbRecordSet &recSettings, bool bPersonSettings);
};

#endif // CLIENTOPTIONSANDSETTINGSMANAGER_H

