#ifndef LANGUAGEWARNINGDLG_H
#define LANGUAGEWARNINGDLG_H

#include <QtWidgets/QDialog>
#include "ui_languagewarningdlg.h"

class LanguageWarningDlg : public QDialog
{
	Q_OBJECT

public:
	LanguageWarningDlg(QString strUL, QString strDL, QWidget *parent = 0);
	~LanguageWarningDlg();

private slots:
	void on_btnOK_clicked();

private:
	Ui::LanguageWarningDlgClass ui;
};

#endif // LANGUAGEWARNINGDLG_H
