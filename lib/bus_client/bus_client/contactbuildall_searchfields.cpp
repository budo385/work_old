#include "contactbuildall_searchfields.h"

#include "bus_client/bus_client/clientcontactmanager.h"
#include <QStackedLayout>
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;
#include "bus_core/bus_core/globalconstants.h"
#include "db_core/db_core/dbtableiddefinition.h"

contactbuildall_searchfields::contactbuildall_searchfields(QWidget *parent)
	: QFrame(parent)
{
	ui.setupUi(this);

	//Build combo's:
	DbRecordSet lstFields;
	ClientContactManager::GetBuildAllContactGridSetup(lstFields);

	Status err;
	_SERVER_CALL(BusCustomFields->ReadFields(err,BUS_CM_CONTACT,m_lstCustomFlds));
	
	//_DUMP(lstFields);

	int nSize=lstFields.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		ui.cmbSearch1->addItem(lstFields.getDataRef(i,0).toString(),lstFields.getDataRef(i,1));
		ui.cmbSearch2->addItem(lstFields.getDataRef(i,0).toString(),lstFields.getDataRef(i,1));
	}
	nSize=m_lstCustomFlds.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		ui.cmbSearch1->addItem(m_lstCustomFlds.getDataRef(i,"BCF_NAME").toString(),m_lstCustomFlds.getDataRef(i,"BCF_ID"));
		ui.cmbSearch2->addItem(m_lstCustomFlds.getDataRef(i,"BCF_NAME").toString(),m_lstCustomFlds.getDataRef(i,"BCF_ID"));
	}
	
	//Connect.
	connect(ui.cmbSearch1,SIGNAL(currentIndexChanged(int)),this,SLOT(currentIndexChanged_column1(int)));
	connect(ui.cmbSearch2,SIGNAL(currentIndexChanged(int)),this,SLOT(currentIndexChanged_column2(int)));

	//set defaults:
	int nIndex1=lstFields.find(1,QString("BCNT_LASTNAME"),true);
	if (nIndex1!=-1)
		ui.cmbSearch1->setCurrentIndex(nIndex1);
	int nIndex2=lstFields.find(1,QString("BCNT_ORGANIZATIONNAME"),true);
	if (nIndex2!=-1)
		ui.cmbSearch2->setCurrentIndex(nIndex2);

	ui.cmbSearch1->setEditable(false);
	ui.cmbSearch1->setEditable(false);

	//Set date fields to today.
	connect(ui.dateEdit1Col1,SIGNAL(dateChanged(const QDate&)),this,SLOT(dateEdit1Col1_dateChanged(const QDate&)));
	ui.dateEdit1Col1->setDate(QDate::currentDate());

	connect(ui.dateEdit1Col2,SIGNAL(dateChanged(const QDate&)),this,SLOT(dateEdit1Col2_dateChanged(const QDate&)));
	ui.dateEdit1Col2->setDate(QDate::currentDate());

	//Zip fields.
	connect(ui.txtSearch1Col1,SIGNAL(textChanged(const QString&)),this,SLOT(txtSearch1Col1_textChanged(const QString&)));
	connect(ui.txtSearch1Col2,SIGNAL(textChanged(const QString&)),this,SLOT(txtSearch1Col2_textChanged(const QString&)));

	//Text fields.
	connect(ui.txtSearchCol1,SIGNAL(editingFinished()),this,SLOT(txtEdited()));
	connect(ui.txtSearchCol2,SIGNAL(editingFinished()),this,SLOT(txtEdited()));
}

contactbuildall_searchfields::~contactbuildall_searchfields()
{

}

// if nDataTypeCustomFld = -1 then is normal fld, else it is custom fld of that data type
void contactbuildall_searchfields::GetSearchData(QString &strColName1, int &nDataTypeCustomFld1, QString &strCol1Search1, QString &strCol1Search2, QString &strColName2, int &nDataTypeCustomFld2, QString &strCol2Search1, QString &strCol2Search2)
{
	nDataTypeCustomFld1=-1;
	nDataTypeCustomFld2=-1;

	//First search.
	strColName1=ui.cmbSearch1->itemData(ui.cmbSearch1->currentIndex()).toString();
	//test if Custom fld:
	bool bOK=false;
	bool bCustomFldDetected=false;
	int nCustomFldID=strColName1.toInt(&bOK);
	if (bOK)
	{
		int nRow=m_lstCustomFlds.find("BCF_ID",nCustomFldID,true,false,true,true);
		if (nRow>=0) //positive confirmation that custom fld is 
		{
			//strColName1=m_lstCustomFlds.getDataRef(nRow,"BCF_NAME").toString();
			nDataTypeCustomFld1=m_lstCustomFlds.getDataRef(nRow,"BCF_DATA_TYPE").toInt();

			if (nDataTypeCustomFld1==GlobalConstants::TYPE_CUSTOM_DATA_DATE)
			{
				strCol1Search1 = ui.dateEdit1Col1->date().toString(Qt::ISODate);
				strCol1Search2 = ui.dateEdit2Col1->date().toString(Qt::ISODate);
			}
			else
			{
				strCol1Search1 = ui.txtSearchCol1->text();
			}
			bCustomFldDetected=true;
		}
	}
	if (!bCustomFldDetected)
	{
		if(strColName1=="BCNT_BIRTHDAY" || strColName1=="BGCN_CMCA_VALID_FROM" || strColName1=="BGCN_CMCA_VALID_TO" || strColName1=="BCNT_DAT_CREATED" || strColName1=="BCNT_FOUNDATIONDATE")
		{
			strCol1Search1 = ui.dateEdit1Col1->date().toString(Qt::ISODate);
			strCol1Search2 = ui.dateEdit2Col1->date().toString(Qt::ISODate);
		}
		else if(strColName1=="BCMA_ZIP" || strColName1=="BCMD_DEBTORCODE" || strColName1=="BCMD_DEBTORACCOUNT")
		{
			strCol1Search1 = ui.txtSearch1Col1->text();
			strCol1Search2 = ui.txtSearch2Col1->text();
		}
		else
		{
			strCol1Search1 = ui.txtSearchCol1->text();
		}
	}



	strColName2=ui.cmbSearch2->itemData(ui.cmbSearch2->currentIndex()).toString();
	//test if Custom fld:
	bOK=false;
	bCustomFldDetected=false;
	nCustomFldID=strColName2.toInt(&bOK);
	if (bOK)
	{
		int nRow=m_lstCustomFlds.find("BCF_ID",nCustomFldID,true,false,true,true);
		if (nRow>=0) //positive confirmation that custom fld is 
		{
			//strColName2=m_lstCustomFlds.getDataRef(nRow,"BCF_NAME").toString();
			nDataTypeCustomFld2=m_lstCustomFlds.getDataRef(nRow,"BCF_DATA_TYPE").toInt();

			if (nDataTypeCustomFld2==GlobalConstants::TYPE_CUSTOM_DATA_DATE)
			{
				strCol2Search1 = ui.dateEdit1Col2->date().toString(Qt::ISODate);
				strCol2Search2 = ui.dateEdit2Col2->date().toString(Qt::ISODate);
			}
			else
			{
				strCol2Search1 = ui.txtSearchCol2->text();
			}
			bCustomFldDetected=true;
		}
	}
	if (!bCustomFldDetected)
	{
		//Second search.
		if(strColName2=="BCNT_BIRTHDAY" || strColName2=="BGCN_CMCA_VALID_FROM" || strColName2=="BGCN_CMCA_VALID_TO" || strColName2=="BCNT_DAT_CREATED" || strColName2=="BCNT_FOUNDATIONDATE")
		{
			strCol2Search1 = ui.dateEdit1Col2->date().toString(Qt::ISODate);
			strCol2Search2 = ui.dateEdit2Col2->date().toString(Qt::ISODate);
		}
		else if(strColName2=="BCMA_ZIP" || strColName2=="BCMD_DEBTORCODE" || strColName2=="BCMD_DEBTORACCOUNT")
		{
			strCol2Search1 = ui.txtSearch1Col2->text();
			strCol2Search2 = ui.txtSearch2Col2->text();
		}
		else
		{
			strCol2Search1 = ui.txtSearchCol2->text();
		}
	}
}

void contactbuildall_searchfields::currentIndexChanged_column1(int index)
{
	QString strColumnName=ui.cmbSearch1->itemData(index).toString();
	SetFilterFldBasedOnData(strColumnName,ui.stackedWidget1);
}

void contactbuildall_searchfields::currentIndexChanged_column2(int index)
{
	QString strColumnName=ui.cmbSearch2->itemData(index).toString();
	SetFilterFldBasedOnData(strColumnName,ui.stackedWidget2);
}

void contactbuildall_searchfields::dateEdit1Col1_dateChanged(const QDate &date)
{
	ui.dateEdit2Col1->setDate(date);
	emit contentChanged();
}

void contactbuildall_searchfields::dateEdit1Col2_dateChanged(const QDate &date)
{
	ui.dateEdit2Col2->setDate(date);
	emit contentChanged();
}

void contactbuildall_searchfields::txtSearch1Col1_textChanged(const QString& text)
{
	ui.txtSearch2Col1->setText(text);
	emit contentChanged();
}

void contactbuildall_searchfields::txtSearch1Col2_textChanged(const QString& text)
{
	ui.txtSearch2Col2->setText(text);
	emit contentChanged();
}

void contactbuildall_searchfields::txtEdited()
{
	emit contentChanged();
}


void contactbuildall_searchfields::SetFilterFldBasedOnData(QString strColumnName, QStackedWidget *pStacked)
{
	//test if Custom fld:
	bool bOK=false;
	int nCustomFldID=strColumnName.toInt(&bOK);
	if (bOK)
	{
		int nRow=m_lstCustomFlds.find("BCF_ID",nCustomFldID,true,false,true,true);
		if (nRow>=0) //positive confirmation that custom fld is 
		{
			int nDataType=m_lstCustomFlds.getDataRef(nRow,"BCF_DATA_TYPE").toInt();

			if (nDataType==GlobalConstants::TYPE_CUSTOM_DATA_DATE)
			{
				pStacked->setCurrentIndex(1); //two date flds
			}
			else
			{
				pStacked->setCurrentIndex(0); //one text fld
			}
			emit contentChanged();
			return;
		}
	}

	if(strColumnName=="BCNT_BIRTHDAY" || strColumnName=="BGCN_CMCA_VALID_FROM" || strColumnName=="BGCN_CMCA_VALID_TO" || strColumnName=="BCNT_DAT_CREATED" || strColumnName=="BCNT_FOUNDATIONDATE")
	{
		pStacked->setCurrentIndex(1); //two date flds
	}
	else if(strColumnName=="BCMA_ZIP" || strColumnName=="BCMD_DEBTORCODE" || strColumnName=="BCMD_DEBTORACCOUNT")
	{
		pStacked->setCurrentIndex(2); //from -to txt flds
	}
	else
	{
		pStacked->setCurrentIndex(0); //one text fld
	}

	emit contentChanged();

}

