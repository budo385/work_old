#include "languagewarningdlg.h"

LanguageWarningDlg::LanguageWarningDlg(QString strUL, QString strDL, QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);

	QString strULName;
	if(strUL == "en")
		strULName = tr("english");
	else
		strULName = tr("german");

	QString strDLName;
	if(strDL == "en")
		strDLName = tr("english");
	else
		strDLName = tr("german");

	ui.labelMsg->setText(QString(tr("Your preferred language \"%1\" is currently not the default language of the software.")).arg(strULName));
	ui.radContinue->setText(QString(tr("Continue with the actual language \"%1\"")).arg(strDLName));
	ui.radSwitch->setText(QString(tr("Switch your language \"%1\". You will have to login again.")).arg(strULName));

	ui.radContinue->setChecked(true);
}

LanguageWarningDlg::~LanguageWarningDlg()
{
}

void LanguageWarningDlg::on_btnOK_clicked()
{
	if(ui.radContinue->isChecked())
		done(0);
	else
		done(1);
}

