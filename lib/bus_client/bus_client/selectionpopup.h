#ifndef SELECTIONPOPUP_H
#define SELECTIONPOPUP_H

#include <QtWidgets/QDialog>
#include <QtWidgets/QMessageBox>
#ifndef WINCE
	#include "generatedfiles/ui_selectionpopup.h"
#else
	#include "embedded_core/embedded_core/generatedfiles/ui_selectionpopup.h"
#endif

#include "common/common/entity_id_collection.h"
#include "common/common/dbrecordset.h"
#include "common/common/observer_ptrn.h"


//filter:
#include "bus_core/bus_core/mainentityfilter.h"


/*!
	\class  SelectionPopup
	\brief  SelectionPopup for displaying data selectors
	\ingroup fuiCollection


	- Every SAPNE has one
	- they are initialized 'cold', data is empty
	- data can be filled by SAPNE lookup or cache signals
	- when opening selector, all data must be loaded (for projects, persons), only for some other types (contacts) data must be reloaded from cache every time, to propagate last state
	- once loaded, there is no need for cache/server lookups...
*/
class SelectionPopup : public QDialog, public ObsrPtrn_Observer
{
    Q_OBJECT

public:
    SelectionPopup(QWidget *parent = 0);
    ~SelectionPopup();

	//cached selector
	void Initialize(int nEntityID,bool bEnableMultiSelection=false);
	int OpenSelector();//MainEntityFilter FilterData);
	QFrame* GetSelectorWidget(){return m_pSelectorWidget;};
	void GetSelectedData(int &nSelectedRecordID, DbRecordSet &lstSelected);
	void updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail=0,const QVariant val=QVariant());
	static void ClearCache();
protected:
	QFrame *m_pSelectorWidget;
	int m_nSelectedRecordID;
	DbRecordSet m_lstSelected;
	QString m_strCode;
	QString m_strName;
	int m_nSelectorType;
	int m_nEntityID;

	//static QHash<int,DbRecordSet> s_hshCachedData; //< By entity ID cached data!


private:
    Ui::SelectionPopupClass ui;
	bool m_bSelectorLoaded;

private slots:
	void on_btnCancel_clicked();
	void on_btnOK_clicked();


};

#endif // SELECTIONPOPUP_H
