#include "accuserrecordusertable.h"

#define COL_CX_READ 2
#define COL_CX_MODIFY 3
#define COL_CX_FULL 4
#define COL_CX_GROUP_DEP 5

AccUserRecordUserTable::AccUserRecordUserTable(QWidget *parent)
	:UniversalTableWidgetEx(parent)
{
	connect(this,SIGNAL(SignalDataChanged(int , int ) ),this,SLOT(OnDataChanged(int , int ) ));
	connect(this,SIGNAL(SignalDataDroped(int,DbRecordSet &,QDropEvent *)),this,SLOT(OnDataDroped(int,DbRecordSet &,QDropEvent *)));
	CreateContextMenu();

	//test sort:
	/*
	DbRecordSet lstTemp;
	lstTemp.addColumn(QVariant::String,"X1");
	lstTemp.addColumn(QVariant::String,"X2");
	lstTemp.addColumn(QVariant::String,"X3");

	lstTemp.addRow(3);
	lstTemp.setData(0,0,"A");
	lstTemp.setData(1,0,"B");
	lstTemp.setData(2,0,"C");

	lstTemp.setData(0,1,"G");
	lstTemp.setData(1,1,"A");
	lstTemp.setData(2,1,"");
	*/
}

AccUserRecordUserTable::~AccUserRecordUserTable()
{

}


void AccUserRecordUserTable::RefreshDisplay(int nRow,bool bApplyLastSortModel)
{
	//sort by pers code, group tree, group code
	m_lstLastSort.clear();
	SortDataList lstSort;
	lstSort<<SortData(m_plstData->getColumnIdx("BGTR_NAME"),0);
	lstSort<<SortData(m_plstData->getColumnIdx("BGIT_CODE"),0);
	lstSort<<SortData(m_plstData->getColumnIdx("BPER_CODE"),0);
	m_plstData->sortMulti(lstSort);

	UniversalTableWidgetEx::RefreshDisplay(nRow,bApplyLastSortModel);

	this->blockSignals(true);
	//if group dep: disable rest
	//if Others: disable group dep:

	int nSize=m_plstData->getRowCount();
	for(int i=0;i<nSize;i++)
	{
		//re-enable all chkbox items
		EnableItem(true,i,COL_CX_READ);
		EnableItem(true,i,COL_CX_MODIFY);
		EnableItem(true,i,COL_CX_FULL);
		EnableItem(true,i,COL_CX_GROUP_DEP);

		if (m_plstData->getDataRef(i,"CUAR_PERSON_ID").toInt()==0)
		{
			EnableItem(false,i,COL_CX_GROUP_DEP);
		}
		if (m_plstData->getDataRef(i,"CUAR_GROUP_DEPENDENT").toInt()==1)
		{
			EnableItem(false,i,COL_CX_READ);
			EnableItem(false,i,COL_CX_MODIFY);
			EnableItem(false,i,COL_CX_FULL);
		}
	}

	this->blockSignals(false);
}

//for check boxes:
void AccUserRecordUserTable::EnableItem(bool bEnable,int nRow,int nCol)
{
	QTableWidgetItem *pItem=item(nRow,nCol); //try if item
	if (pItem)
	{
		Qt::ItemFlags flags=pItem->flags();
		if (bEnable)
		{
			flags = flags | Qt::ItemIsUserCheckable;
			//if (pItem->checkState()==Qt::PartiallyChecked);
			//	pItem->setCheckState(Qt::Checked);
		}
		else
		{
			flags = flags & ~Qt::ItemIsUserCheckable; //negate bit
			//if (pItem->checkState()==Qt::Checked);
			//	pItem->setCheckState(Qt::PartiallyChecked);
		}
		pItem->setFlags(flags);
	}
}


//if group dep: from 1->0, re-enable others 
//if group dep: from 0->1, ??? -> disable others, calc max right and group that belongs in!!
void AccUserRecordUserTable::OnDataChanged(int nRow, int nCol)
{
	if (nCol==COL_CX_GROUP_DEP)
	{
		UpdateCheckBoxStates(nRow);
		//if (m_plstData->getDataRef(nRow,"CUAR_GROUP_DEPENDENT").toInt()==1) //emit signal to refresh group data & right if group depedent
		emit CalcUARMaxRightForGroupDep(nRow);
		return;
	}
	if (nCol==COL_CX_FULL && m_plstData->getDataRef(nRow,"CX_FULL").toInt()==1) 
	{
		m_plstData->setData(nRow,"CX_READ",1);
		m_plstData->setData(nRow,"CX_MODIFY",1);
		RefreshDisplay(nRow);
	}
	if (nCol==COL_CX_MODIFY) 
	{
		if (m_plstData->getDataRef(nRow,"CX_MODIFY").toInt()==1)
		{
			m_plstData->setData(nRow,"CX_READ",1);
			m_plstData->setData(nRow,"CX_FULL",0);
		}
		else
			m_plstData->setData(nRow,"CX_FULL",0);
		RefreshDisplay(nRow);
	}
	if (nCol==COL_CX_READ) 
	{
		m_plstData->setData(nRow,"CX_MODIFY",0);
		m_plstData->setData(nRow,"CX_FULL",0);
		RefreshDisplay(nRow);
	}

}



void AccUserRecordUserTable::UpdateCheckBoxStates(int nRow)
{
	this->blockSignals(true);
	if (m_plstData->getDataRef(nRow,"CUAR_GROUP_DEPENDENT").toInt()==1)
	{
		EnableItem(false,nRow,COL_CX_READ);
		EnableItem(false,nRow,COL_CX_MODIFY);
		EnableItem(false,nRow,COL_CX_FULL);
	}
	else
	{
		EnableItem(true,nRow,COL_CX_READ);
		EnableItem(true,nRow,COL_CX_MODIFY);
		EnableItem(true,nRow,COL_CX_FULL);
	}
	this->blockSignals(false);
}



//create custom cntx menu at end of existing one:
void AccUserRecordUserTable::CreateContextMenu()
{
	QList<QAction*> lstActions;
	
	//Add separator
	/*
	QAction* SeparatorAct = new QAction(this);
	SeparatorAct->setSeparator(true);
	lstActions.prepend(SeparatorAct);
	*/

	//Insert new row:
	QAction* pAction = new QAction(tr("Add User"), this);
	connect(pAction, SIGNAL(triggered()), this, SIGNAL(Signal_AddUser()));
	lstActions.prepend(pAction);

	pAction = new QAction(tr("Remove Selected User(s)"), this);
	connect(pAction, SIGNAL(triggered()), this, SIGNAL(Signal_RemoveUser()));
	lstActions.prepend(pAction);

	pAction = new QAction(tr("Assign MySelf"), this);
	connect(pAction, SIGNAL(triggered()), this, SIGNAL(Signal_AssignMySelf()));
	lstActions.prepend(pAction);

	pAction = new QAction(tr("Remove Group Dependent Flag From Selected User(s)"), this);
	connect(pAction, SIGNAL(triggered()), this, SIGNAL(Signal_RemoveGroupDep()));
	lstActions.prepend(pAction);

	pAction = new QAction(tr("Assign Access Rights To All Selected User(s)"), this);
	connect(pAction, SIGNAL(triggered()), this, SIGNAL(Signal_AssignToAll()));
	lstActions.prepend(pAction);

	SetContextMenuActions(lstActions);
}