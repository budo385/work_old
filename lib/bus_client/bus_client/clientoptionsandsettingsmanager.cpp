#include "clientoptionsandsettingsmanager.h"
#include "db_core/db_core/dbsqltabledefinition.h"
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager	*g_pBoSet;	
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;


ClientOptionsAndSettingsManager::ClientOptionsAndSettingsManager()
{

}

ClientOptionsAndSettingsManager::~ClientOptionsAndSettingsManager()
{

}

//load from server:
void ClientOptionsAndSettingsManager::LoadOptions(Status &status)
{
	QWriteLocker lock(&m_lckOptions);
	QString strWhere = "WHERE BOUS_PERSON_ID IS NULL ";
	g_pBoSet->app->ClientSimpleORM->Read(status,BUS_OPT_SETTINGS,m_recOptions,strWhere);
	if (!status.IsOK()) return;
	TestOptions();
}

//load from server:
void ClientOptionsAndSettingsManager::LoadSettings(Status &status)
{
	QWriteLocker lock(&m_lckSettings);
	QString strWhere = "WHERE BOUS_PERSON_ID = " + QVariant(g_pClientManager->GetPersonID()).toString();
	g_pBoSet->app->ClientSimpleORM->Read(status,BUS_OPT_SETTINGS,m_recSettings,strWhere);
	if (!status.IsOK()) return;
	TestSettings();
	m_lstChangedSettingsID.clear();
}

void ClientOptionsAndSettingsManager::ReaLoadAvatarStartupSetting(Status &status)
{
	QWriteLocker lock(&m_lckSettings);
	QString strWhere = "WHERE BOUS_PERSON_ID = " + QVariant(g_pClientManager->GetPersonID()).toString();
	DbRecordSet lstData;
	g_pBoSet->app->ClientSimpleORM->Read(status,BUS_OPT_SETTINGS,lstData,strWhere);
	if (!status.IsOK()) return;

	int nRowAvatar = lstData.find("BOUS_SETTING_ID",AVATAR_STARTUP_LIST,true,false,true,true);
	if (nRowAvatar>=0)
	{
		DbRecordSet rowAvatarStartup=lstData.getRow(nRowAvatar);
		//rowAvatarStartup.Dump();
		nRowAvatar = m_recSettings.find("BOUS_SETTING_ID",AVATAR_STARTUP_LIST,true,false,true,true);
		if (nRowAvatar>=0)
		{
			m_recSettings.assignRow(nRowAvatar,rowAvatarStartup);
		}
	}
}

void ClientOptionsAndSettingsManager::SaveApplicationOptions(Status &status, DbRecordSet *recDataOverwrite)
{
	if (recDataOverwrite)
	{
		SaveSettingsByRecordSet(status, *recDataOverwrite,false); //save all
		//if (status.IsOK() && recDataOverwrite->getRowCount()>0) //reload settings:
		//{
		//	LoadOptions(status);
		//}
		return;
	}
	SaveSettingsByRecordSet(status, m_recOptions,false); //save all
}
void ClientOptionsAndSettingsManager::SavePersonSettings(Status &status, DbRecordSet *recDataOverwrite)
{
	status.setError(0);
	if (recDataOverwrite)
	{
		//m_recSettings=*recDataOverwrite;
		SaveSettingsByRecordSet(status, *recDataOverwrite,true);
		//if (status.IsOK() && recDataOverwrite->getRowCount()>0) //reload settings:
		//{
		//	LoadSettings(status);
		//}
	}

	if (m_lstChangedSettingsID.size()==0) //no need to save
		return;

	//Copy row from cache.
	m_recSettings.clearSelection();
	//Find changed rows.
	int nSize=m_lstChangedSettingsID.size();
	for(int i=0;i<nSize;i++)
	{
		m_recSettings.find("BOUS_SETTING_ID", m_lstChangedSettingsID.at(i), false, false, false,true);
	}

	

	DbRecordSet recForSave;
	recForSave.copyDefinition(m_recSettings);
	recForSave.merge(m_recSettings,true);

	if (recForSave.getRowCount()>0)
	{
		SaveSettingsByRecordSet(status, recForSave,true);
		if (status.IsOK())
			m_lstChangedSettingsID.clear();
	}

}


void ClientOptionsAndSettingsManager::SaveSettingsByRecordSet(Status &status, DbRecordSet &recSettings, bool bPersonSettings)
{
	int nRowCount = recSettings.getRowCount();
	recSettings.clearSelection();
	//recSettings.Dump();
	//CHECK RECORDS: FIRE ASSERT IF not match:
	//Delete rows that have user ID = NULL - if not settings (they always have person ID null).
#ifdef _DEBUG
	if (bPersonSettings)
	{
		for(int i = 0; i < nRowCount; ++i)
		{
			if (recSettings.getDataRef(i, "BOUS_PERSON_ID").isNull() || recSettings.getDataRef(i, "BOUS_PERSON_ID").toInt() == 0)
				recSettings.selectRow(i);
		}
		Q_ASSERT(recSettings.getSelectedCount()==0); //some person settings have pers id=0!!!
		recSettings.deleteSelectedRows();
	}
	//Delete options with BOUS_ID null. It simply can not happen - application options MUST be inserted in DB when deploying.
	else
	{
		for(int i = 0; i < nRowCount; ++i)
		{
			if (recSettings.getDataRef(i, "BOUS_ID").isNull())
				recSettings.selectRow(i);
		}
		Q_ASSERT(recSettings.getSelectedCount()==0); //some app options settings are not inserted in DB...
		recSettings.deleteSelectedRows();
	}
#endif


	//Write to DB - if settings use special method to check is some combination of user ID and settings already occupied.
	if (bPersonSettings)
		_SERVER_CALL(CoreServices->SavePersonalSettings(status, recSettings))
	else{
		QString pLockResourceID;
		int nQueryView = -1;
		int nSkipLastColumns = 0; 
		DbRecordSet lstForDelete;
		_SERVER_CALL(ClientSimpleORM->Write(status, BUS_OPT_SETTINGS, recSettings, pLockResourceID, nQueryView, nSkipLastColumns, lstForDelete))
	}

	_CHK_ERR(status);

	//reassign back to settings: (for ID):
	if (bPersonSettings)
	{
		int nSize=recSettings.getRowCount();
		for(int i=0;i<nSize;i++)
		{
			int nRow=m_recSettings.find("BOUS_SETTING_ID",recSettings.getDataRef(i,"BOUS_SETTING_ID").toInt(),true);
			if (nRow>=0)
			{
				DbRecordSet row = recSettings.getRow(i);
				m_recSettings.assignRow(nRow,row); //assign all row:
			}
		}
	}
	else
	{
		int nSize=recSettings.getRowCount();
		for(int i=0;i<nSize;i++)
		{
			int nRow=m_recOptions.find("BOUS_SETTING_ID",recSettings.getDataRef(i,"BOUS_SETTING_ID").toInt(),true);
			if (nRow>=0)
			{
				DbRecordSet row = recSettings.getRow(i);
				m_recOptions.assignRow(nRow,row); //assign all row:
			}
		}
	}


}

