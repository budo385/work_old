#ifndef SIMPLESELECTIONWIZFILESELECTIONEXP_H
#define SIMPLESELECTIONWIZFILESELECTIONEXP_H


#include "generatedfiles/ui_simpleselectionwizfileselectionexp.h"
#include "gui_core/gui_core/wizardpage.h"

class SimpleSelectionWizFileSelectionExp : public WizardPage
{
	Q_OBJECT

public:
	SimpleSelectionWizFileSelectionExp(int nWizardPageID, QString strPageTitle, QWidget *parent = 0, int nPageEntityID = -1);
	~SimpleSelectionWizFileSelectionExp();

	void Initialize();
	void resetPage();
	bool GetPageResult(DbRecordSet &RecordSet);

	bool m_bHideOutlookExpChk;

private:
	Ui::SimpleSelectionWizFileSelectionExpClass ui;

private slots:
	void on_btnSelectPath_clicked();
	void on_txtPath_textChanged( const QString & );
	void on_chkSokratesHdr_stateChanged(int nChecked);
	void on_chkColumnHdr_stateChanged(int nChecked);
	void on_chkOutlookExport_stateChanged(int nChecked);
};

#endif // SIMPLESELECTIONWIZFILESELECTION_H
