#ifndef DLG_CONTACTBUILDALL_EMBEDDED_H
#define DLG_CONTACTBUILDALL_EMBEDDED_H

#include <QtWidgets/QDialog>
#include "ui_dlg_contactbuildall_embedded.h"
#include "common/common/dbrecordset.h"
#include "common/common/observer_ptrn.h"
 
class Dlg_ContactBuildAll_Embedded : public QDialog, public ObsrPtrn_Observer
{
	Q_OBJECT

public:
	Dlg_ContactBuildAll_Embedded(QWidget *parent = 0);
	~Dlg_ContactBuildAll_Embedded();

	void GetResult(DbRecordSet &lstFilterData, int &nOperation,bool &bExcludeGroup);
	void updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail=0,const QVariant val=QVariant());

private:
	Ui::Dlg_ContactBuildAll_EmbeddedClass ui;
	DbRecordSet m_lstSAPNEGroup;

private slots:
	void on_btnCancel_clicked();
	void on_btnOK_clicked();
	void OnGroup2Selected();
	void OnGroup3Selected();
};

#endif // DLG_CONTACTBUILDALL_EMBEDDED_H
