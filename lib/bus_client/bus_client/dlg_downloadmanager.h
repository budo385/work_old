#ifndef DLG_DOWNLOADMANAGER_H
#define DLG_DOWNLOADMANAGER_H

#include <QtWidgets/QWidget>
#include <QProgressBar>
#include <QLabel>
#include "generatedfiles/ui_dlg_downloadmanager.h"
#include "gui_core/gui_core/styledpushbutton.h"
#include "common/common/status.h"

class DownloadWidget;

class Dlg_DownloadManager : public QWidget
{
	Q_OBJECT

public:

	Dlg_DownloadManager(QWidget *parent = 0);
	~Dlg_DownloadManager();

	DownloadWidget*		AddFileForDownLoad(QString strFilePath,QString strIcon="");
	DownloadWidget*		AddFileForUpload(QString strFilePath,QString strIcon="");
	void				RemoveFile(DownloadWidget* pWidget);

	void				OnThemeChanged();
signals:
	void				ClearAll();

private slots:
	void on_btnClearAll_clicked();

private:
	Ui::Dlg_DownloadManagerClass ui;
	int								m_nFileIDCounter;
	QWidget							*m_pParentWidget;
};


class DownloadWidget : public QFrame
{
	Q_OBJECT
	
public:
	enum StatusEnum
	{
		STATUS_WAITING,
		STATUS_DOWNLOAD,
		STATUS_UPLOAD,
		STATUS_FINISHED,
		STATUS_CANCEL,
		STATUS_ERROR
	};

	DownloadWidget(QWidget *parent,int nFileID, QString strFilePath,bool bIsDownLoad,QString strIcon="");
	~DownloadWidget();

	void SetStatus(int nStatus);

signals:
	void	SignalOpenFile(int nFileID);
	void	SignalRemoveFile(int nFileID);
	void	SignalCancel(int nFileID);
	void	SignalOperationEnd(int,int, QString);
	void	SignalOpenDirectory(int nFileID);

public slots:
	void	OnCommunicationInProgress(int,qint64,qint64);
	void	OnOperationEnded(int,QString);

private slots:
	void	OnOpenFile(){emit SignalOpenFile(m_nFileID);};
	void	OnRemoveFile(){emit SignalRemoveFile(m_nFileID);};
	void	OnCancel(){emit SignalCancel(m_nFileID);};
	void	OnOpenDirectory(){emit SignalOpenDirectory(m_nFileID);};
	

public:
	QString				m_strFilePath;
	int					m_nFileID;
	int					m_nStatus;
	QLabel				*m_pIcon;
	QLabel				*m_fileName;
	QLabel				*m_Status;
	QProgressBar		*m_ProgressBar;
	StyledPushButton	*m_pBtnCancel;
	StyledPushButton	*m_pBtnRemove;
	StyledPushButton	*m_pBtnOpen;
	StyledPushButton	*m_pBtnOpenDir;
	
	//file op data:
	bool				m_bIsDownLoadOperation; //true down, else upload
	bool				m_bIsBackup;
	Status				m_LastError;
	bool				m_bOverWrite;
	bool				m_bIsCheckInOut;

	//check in-out data:
	bool				m_bDeleteDocumentWhenCancel;	//when operation is either aborted or errored, delete document previously inserted in DB, without revision!
	int					m_nDocumentID;
	bool				m_bReadOnly;
	bool				m_bSkipLock;
	QString				m_strTag;
	bool				m_bDoNotTrackCheckOutDoc;
	int					m_nDocumentOriginalSize;
	int					m_nRevisionID; //-1 by default
};


#endif // DLG_DOWNLOADMANAGER_H
