#include "accuserrecordgrouptree.h"
#include "bus_core/bus_core/hierarchicalhelper.h"
#include "common/common/entity_id_collection.h"
#include "trans/trans/xmlutil.h"
#include "os_specific/os_specific/mime_types.h"
#include "bus_core/bus_core/accuarcore.h"
#include <QHeaderView>
#include <QApplication>
#include <QMimeData>
#include "gui_core/gui_core/thememanager.h"

#define COL_ID		"BGIT_ID"
#define COL_CODE	"BGIT_CODE"
#define COL_LEVEL	"BGIT_LEVEL"
#define COL_ICON	"BGIT_ICON"
#define COL_PARENT	"BGIT_PARENT"
#define COL_NAME	"BGIT_NAME"

AccUserRecordGroupTree::AccUserRecordGroupTree(QWidget *parent)
	: QTreeWidget(parent),m_pData(NULL)
{
	clear();
	setColumnCount(5);
	RefreshHeader();
	connect(this, SIGNAL(itemSelectionChanged()),this, SLOT(SelectionChanged()));
	connect(this, SIGNAL(itemChanged(QTreeWidgetItem *, int)), this, SLOT(OnItemChanged(QTreeWidgetItem *, int)));
	EnableDrop(true,ENTITY_BUS_GROUP_ITEMS);
	setStyleSheet(ThemeManager::GetTreeBkg());
}

AccUserRecordGroupTree::~AccUserRecordGroupTree()
{

}

void AccUserRecordGroupTree::Initialize(DbRecordSet *plstData)
{
	m_pData=plstData;
}
void AccUserRecordGroupTree::RefreshDisplay()
{
	if (!m_pData) return;

	setUpdatesEnabled(false);
	blockSignals(true);
	clear();
	RefreshHeader();

	int nLastNodeID=0; //remember last node, and expand to it
	if(m_pData->getSelectedCount()>0)
	{
		nLastNodeID=m_pData->getDataRef(m_pData->getSelectedRow(),COL_ID).toInt();
	}

	m_hshRowIDtoItem.clear();

	if (m_pData->getRowCount()>0)
	{
		//sort by: tree_name/group_code
		SortDataList lstSort;
		lstSort<<SortData(m_pData->getColumnIdx("BGTR_NAME"),0);
		lstSort<<SortData(m_pData->getColumnIdx("BGIT_CODE"),0);
		m_pData->sortMulti(lstSort);

		QFont strStyle1("Arial",10,QFont::Bold);
		strStyle1.setStyleStrategy(QFont::PreferAntialias);

		QString strLastTree=m_pData->getDataRef(0,"BGTR_NAME").toString();
		int nRowID=m_pData->getDataRef(0,"BGIT_ID").toInt();
		int nStartChildRow=0;

		QList<QTreeWidgetItem*> lstParents;
		int nSize=m_pData->getRowCount();
		for(int i=0;i<nSize;i++)
		{
			if (strLastTree!=m_pData->getDataRef(i,"BGTR_NAME").toString() || i==nSize-1)
			{
				QTreeWidgetItem *parent = new QTreeWidgetItem();
				//int nRowID = m_pData->getDataRef(i, COL_ID).toInt();
				parent->setData(0,Qt::DisplayRole,strLastTree); //->points to first row
				parent->setData(0,Qt::UserRole,nRowID);
				parent->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled); 
				parent->setFont(0,strStyle1);
				lstParents.append(parent);
				if (i!=nSize-1) //create childrens from start of tree nodes to end..
					CreateItemsFromRecordSet(parent,nStartChildRow,i-1);
				else
					CreateItemsFromRecordSet(parent,nStartChildRow,nSize-1); //last iteration
				nStartChildRow=i;
				strLastTree=m_pData->getDataRef(i,"BGTR_NAME").toString();
				nRowID=m_pData->getDataRef(i,"BGIT_ID").toInt();
			}
			else
				continue;
		}
		addTopLevelItems(lstParents);
	}

	if (nLastNodeID>0)
	{
		SelectItemByRowID(nLastNodeID);
		ExpandItemByRowID(nLastNodeID);
	}
	else
	{
		ExpandTree();
	}

	blockSignals(false);
	setUpdatesEnabled(true);
}



//creates or refreshes data from recordset on tree, nodes are created on parent, from startrow to endrow (inclusive)
void AccUserRecordGroupTree::CreateItemsFromRecordSet(QTreeWidgetItem *RootParentItem, int nStartRow, int nEndRow)
{
	QFont strStyle1("Arial",10,QFont::Bold);
	QFont strStyle2("Arial",10,QFont::DemiBold,true);
	QFont strStyle3("Arial",9,QFont::DemiBold,true);
	QFont strStyle4("Arial",8,QFont::DemiBold,true);
	QFont strStyle5("Arial",7,QFont::DemiBold,true);
	strStyle1.setStyleStrategy(QFont::PreferAntialias);
	strStyle2.setStyleStrategy(QFont::PreferAntialias);
	strStyle3.setStyleStrategy(QFont::PreferAntialias);
	strStyle4.setStyleStrategy(QFont::PreferAntialias);
	strStyle5.setStyleStrategy(QFont::PreferAntialias);
	QSize sizeCol(600,16);

	QTreeWidgetItem *ParentItem=RootParentItem;

	for(int i=nStartRow;i<=nEndRow;++i)
	{
		//create item or refresh display:
		QTreeWidgetItem *Item;
		int nRowID = m_pData->getDataRef(i, COL_ID).toInt();
		if (!m_hshRowIDtoItem.contains(nRowID))
		{
			Item = new QTreeWidgetItem;
			m_hshRowIDtoItem.insert(nRowID, Item);
		}
		else
			Item = m_hshRowIDtoItem.value(nRowID);
		QString strCode = m_pData->getDataRef(i, COL_CODE).toString();
		QString strName = m_pData->getDataRef(i, COL_NAME).toString();
		QString strIcon = m_pData->getDataRef(i, COL_ICON).toString();
		QString strDisplayData = strCode + " " + strName;
		Item->setData(0, Qt::UserRole,		nRowID);		//RowID.
		Item->setData(0, Qt::DisplayRole,	strDisplayData);//Display Code + Name.
		//if (!strIcon.isEmpty())
			//Item->setIcon(0, CreateIcon(strIcon));				//Set icon.

		Item->setCheckState(1,m_pData->getDataRef(i,"CX_READ").toInt()>0 ? Qt::Checked : Qt::Unchecked);
		Item->setCheckState(2,m_pData->getDataRef(i,"CX_MODIFY").toInt()>0 ? Qt::Checked : Qt::Unchecked);
		Item->setCheckState(3,m_pData->getDataRef(i,"CX_FULL").toInt()>0 ? Qt::Checked : Qt::Unchecked);
		Item->setCheckState(4,m_pData->getDataRef(i,"CX_PROPAGATE").toInt()>0 ? Qt::Checked : Qt::Unchecked);

		//select if needed:
		if (m_pData->isRowSelected(i))
		{
			Item->setSelected(true);
		}

		//BT: issue 1646: add different fonts for diff levels:
		/*
		Level 1: Arial 10pt bold
		Level 2: Arial 10pt regular
		Level 3: Arial 9pt regular
		Level 4: Arial 8pt regular
		Level 5...: Arial 7pt regular
		*/
		int nLevel = m_pData->getDataRef(i, COL_LEVEL).toInt();
		switch(nLevel)
		{
		case 0:
			Item->setFont(0,strStyle1);
			break;
		case 1:
			Item->setFont(0,strStyle2);
			break;
		case 2:
			Item->setFont(0,strStyle3);
			break;
		case 3:
			Item->setFont(0,strStyle4);
			break;
		default:
			Item->setFont(0,strStyle5);
			break;
		}
		Item->setSizeHint(0,sizeCol);
		Item->setToolTip(0,strDisplayData);

		//if parent null, try to find it:
		if (!ParentItem)
		{
			int nParentID=m_pData->getDataRef(i, COL_PARENT).toInt();
			if (nParentID>0)
			{
				ParentItem=m_hshRowIDtoItem.value(nParentID,NULL);
			}
		}
		//actually add item to tree:
		if (!ParentItem) 
			RootParentItem->addChild(Item);
		else
			ParentItem->addChild(Item);


		ParentItem=NULL;
	}


}


//does not touch previous selection, selects all children below parent with RowID including parent itself
//all children have parent code start
void AccUserRecordGroupTree::SelectInRecordSetByNodeID(int nNodeID)
{
	int nRow=m_pData->find(COL_ID,nNodeID,true); //this does not select/deselect any
	if (nRow!=-1)
	{
		m_pData->selectRow(nRow);
		QString strParentCode=m_pData->getDataRef(nRow,COL_CODE).toString();
		HierarchicalHelper::SelectChildren(strParentCode,*m_pData,COL_CODE,false,nRow);
	}
}

void AccUserRecordGroupTree::SelectItemByRowID(int nRowID)
{
	QTreeWidgetItem *item = m_hshRowIDtoItem.value(nRowID,NULL);
	if (!item) return;

	//select it (preserve previous selection):
	SelectInRecordSetByNodeID(nRowID); //issue 1009
	//m_pData->find(m_lstColumnIdx.at(COL_ID), nRowID); //select it, deselect others
	blockSignals(true);
	item->setSelected(true);
	blockSignals(false);
}

void AccUserRecordGroupTree::ExpandItemByRowID(int nRowID)
{
	QTreeWidgetItem *item = m_hshRowIDtoItem.value(nRowID,NULL);
	if (!item) return;
	setCurrentItem(item); //moves focus to the item and expands all parents to it!!! discovered by BT :)
}


void AccUserRecordGroupTree::SelectionChanged()
{
	//Selected items list.
	//m_hshSelectedState.clear();
	QList<QTreeWidgetItem*> items(selectedItems());
	m_pData->clearSelection();

	QListIterator<QTreeWidgetItem*> iter(items);
	while (iter.hasNext())
	{
		QTreeWidgetItem *item = iter.next();
		int RowID = item->data(0, Qt::UserRole).toInt();
		SelectInRecordSetByNodeID(RowID);
	}

	//QCoreApplication::processEvents(); //issue 1653: color row then fire signal...
	//emit SignalSelectionChanged();	//notify others
}

//--->check chekboxes: make changes in data.
void AccUserRecordGroupTree::OnItemChanged(QTreeWidgetItem *item, int column)
{
	if (column<1 || column>4)
		return;

	int nNodeID = item->data(0, Qt::UserRole).toInt();
	int nRow=m_pData->find(COL_ID,nNodeID,true); //this does not select/deselect any
	if (nRow==-1)
		return;

	blockSignals(true);

	if (item->parent())
	{
		if (column==1)
		{
			m_pData->setData(nRow,"CX_READ",(int)(item->checkState(1)==Qt::Checked));
			m_pData->setData(nRow,"CX_MODIFY",0);
			m_pData->setData(nRow,"CX_FULL",0);
		}
		if (column==2)
		{
			m_pData->setData(nRow,"CX_MODIFY",(int)(item->checkState(2)==Qt::Checked));
			m_pData->setData(nRow,"CX_READ",1);
			m_pData->setData(nRow,"CX_FULL",0);
		}
		if (column==3)
		{
			m_pData->setData(nRow,"CX_FULL",(int)(item->checkState(3)==Qt::Checked));
			m_pData->setData(nRow,"CX_READ",1);
			m_pData->setData(nRow,"CX_MODIFY",1);
		}
		if (column==4)
			m_pData->setData(nRow,"CX_PROPAGATE",(int)(item->checkState(4)==Qt::Checked));

		//refresh other items:
		QTreeWidgetItem *Item = m_hshRowIDtoItem.value(nNodeID,NULL); //rf on tree:
		if (!Item){blockSignals(false);return;}
		Item->setCheckState(1,m_pData->getDataRef(nRow,"CX_READ").toInt()>0 ? Qt::Checked : Qt::Unchecked);
		Item->setCheckState(2,m_pData->getDataRef(nRow,"CX_MODIFY").toInt()>0 ? Qt::Checked : Qt::Unchecked);
		Item->setCheckState(3,m_pData->getDataRef(nRow,"CX_FULL").toInt()>0 ? Qt::Checked : Qt::Unchecked);
		Item->setCheckState(4,m_pData->getDataRef(nRow,"CX_PROPAGATE").toInt()>0 ? Qt::Checked : Qt::Unchecked);
	}

	//if propagate: propagate to all children GAR's from nRow
	if (m_pData->getDataRef(nRow,"CX_PROPAGATE").toInt()>0)
	{
		QString strParentCode=m_pData->getDataRef(nRow,COL_CODE).toString();
		int nCodeSize=strParentCode.size();
		int nSize=m_pData->getRowCount();
		for(int i=nRow+1;i<nSize;++i)
		{
			int nRowID=m_pData->getDataRef(i,COL_ID).toInt();
			QString strChildCode=m_pData->getDataRef(i,COL_CODE).toString().left(nCodeSize);
			if (strChildCode!=strParentCode) //M.R. told that left is faster then indexOf
				break;
			m_pData->setData(i,"CX_READ",m_pData->getDataRef(nRow,"CX_READ").toInt());
			m_pData->setData(i,"CX_MODIFY",m_pData->getDataRef(nRow,"CX_MODIFY").toInt());
			m_pData->setData(i,"CX_FULL",m_pData->getDataRef(nRow,"CX_FULL").toInt());
			m_pData->setData(i,"CX_PROPAGATE",m_pData->getDataRef(nRow,"CX_PROPAGATE").toInt());
			//refresh other items:
			QTreeWidgetItem *Item = m_hshRowIDtoItem.value(nRowID,NULL); //rf on tree:
			if (!Item){blockSignals(false);return;}
			Item->setCheckState(1,m_pData->getDataRef(i,"CX_READ").toInt()>0 ? Qt::Checked : Qt::Unchecked);
			Item->setCheckState(2,m_pData->getDataRef(i,"CX_MODIFY").toInt()>0 ? Qt::Checked : Qt::Unchecked);
			Item->setCheckState(3,m_pData->getDataRef(i,"CX_FULL").toInt()>0 ? Qt::Checked : Qt::Unchecked);
			Item->setCheckState(4,m_pData->getDataRef(i,"CX_PROPAGATE").toInt()>0 ? Qt::Checked : Qt::Unchecked);
		}
	}

	blockSignals(false);

	AccUARCore::DisplayList2UAR(NULL,m_pData); //update list after change
	emit UpdateUARRights();
}

//delete selected, but if root node, delete all below, if parent->can be skipped
void AccUserRecordGroupTree::DeleteSelected()
{
	//if root selected: erase it:
	DbRecordSet lstSelected=m_pData->getSelectedRecordSet();
	int nSize=topLevelItemCount();
	for(int i=0;i<nSize;i++)
	{
		QTreeWidgetItem *itemRoot=topLevelItem(i);
		int nNodeID = itemRoot->data(0, Qt::UserRole).toInt();
		int nRow=lstSelected.find(COL_ID,nNodeID,true);
		if (nRow>=0)
		{
			QString strCode=lstSelected.getDataRef(nRow,COL_CODE).toString();
			HierarchicalHelper::SelectChildren(strCode,*m_pData,COL_CODE,false);
		}
	}

	m_pData->deleteSelectedRows();
	RefreshDisplay();
	emit DataChanged();
}


void AccUserRecordGroupTree::EnableDrop(bool bEnable,int nDropMimeType)
{
	setAcceptDrops(bEnable);
	m_nDropMimeType=nDropMimeType;
}



//if not edit mode: disable drop & drag
void AccUserRecordGroupTree::dropEvent(QDropEvent *event)
{
	const QMimeData *mime = event->mimeData();

	//------------------------------------------------
	// LIST of documents entities:
	//------------------------------------------------
	//internal/external
	if (mime->hasFormat(SOKRATES_MIME_LIST))
	{
		int nEntityID;
		DbRecordSet lstDropped;

		//import emails:
		QByteArray arData = mime->data(SOKRATES_MIME_LIST);
		if(arData.size() > 0)
		{
			lstDropped=XmlUtil::ConvertByteArray2RecordSet_Fast(arData);
		}
		QByteArray arDataType = mime->data(SOKRATES_MIME_DROP_TYPE);
		if(arDataType.size() > 0)
		{
			bool bOK;
			nEntityID=arDataType.toInt(&bOK);
			if (!bOK) nEntityID=-1;
		}
		//check if ok:
		if (m_nDropMimeType==-1) //if -1 always emit!!
			emit SignalDataDroped(nEntityID, lstDropped, event);
		else if (m_nDropMimeType!=-1 && m_nDropMimeType==nEntityID)
			emit SignalDataDroped(nEntityID, lstDropped, event);

		event->accept();
		return;
	}

	event->ignore();
	return;
}

Qt::DropActions AccUserRecordGroupTree::supportedDropActions() const 
{
	return  Qt::CopyAction | Qt::MoveAction | Qt::IgnoreAction | Qt::LinkAction | Qt::ActionMask | Qt::TargetMoveAction;
}
void AccUserRecordGroupTree::dragMoveEvent ( QDragMoveEvent * event )
{
	if (event->mimeData()->hasFormat(SOKRATES_MIME_LIST))
		event->accept();
	else
		event->ignore();
	return;
}
void AccUserRecordGroupTree::dragEnterEvent ( QDragEnterEvent * event )
{
	event->accept();
	return;
}


void AccUserRecordGroupTree::RefreshHeader()
{
	QFont strStyle1("Arial",8,QFont::Bold,true);
	strStyle1.setStyleStrategy(QFont::PreferAntialias);

	QTreeWidgetItem *header=headerItem();
	for(int i=0;i<5;i++)
		header->setFont(i,strStyle1);

	header->setData(0,Qt::DisplayRole,tr("Group"));
	header->setData(1,Qt::DisplayRole,tr("Read"));
	header->setData(2,Qt::DisplayRole,tr("Modify"));
	header->setData(3,Qt::DisplayRole,tr("Full"));
	header->setData(4,Qt::DisplayRole,tr("Subgroups Inherit Access Rights"));
	setColumnWidth(0,300);
	setColumnWidth(1,48); //read
	setColumnWidth(2,48); //modify
	setColumnWidth(3,48); //full
	setColumnWidth(4,48); //inherit
}


void AccUserRecordGroupTree::ExpandTree()
{
	//iterate through all items in hash of items and expand them: (? does we affect state?)
	blockSignals(true); //disable
	QApplication::setOverrideCursor(Qt::WaitCursor);
	QHashIterator<int, QTreeWidgetItem*> i(m_hshRowIDtoItem);
	while (i.hasNext()) 
	{
		i.next();
		i.value()->setExpanded(true);
	}
	QApplication::restoreOverrideCursor();

	int nSize=topLevelItemCount();
	for(int i=0;i<nSize;i++)
	{
		QTreeWidgetItem *itemRoot=topLevelItem(i);
		itemRoot->setExpanded(true);
	}

	blockSignals(false); //disable
}
