#ifndef SIMPLESELECTIONWIZPAGE_H
#define SIMPLESELECTIONWIZPAGE_H

#include <QHash>
#include <QTreeWidget>
#include <QHeaderView>
#include <QTreeWidgetItem>

#include "generatedfiles/ui_simpleselectionwizpage.h"
#include "gui_core/gui_core/wizardpage.h"
#include "common/common/dbrecordset.h"
#include "bus_client/bus_client/mainentityselectioncontroller.h"

class SimpleSelectionWizPage : public WizardPage
{
    Q_OBJECT

public:
    SimpleSelectionWizPage(int nWizardPageID, QString strPageTitle, int nPageEntityID = -1, QWidget *parent = 0, bool bShowNewBtn = false);
    ~SimpleSelectionWizPage();

	void Initialize();

	QFrame		*GetSelectorWidget(){	return m_pSelectorWidget;	}
	QFrame		*GetSelectedWidget(){	return m_pSelectedWidget;	}

private:
	Ui::SimpleSelectionWizPageClass ui;
	
	void EntityType();
	void InsertTableItems(DbRecordSet &DropedRecordSet, QTreeWidgetItem *parent, int index, bool bOnInitialize = false);
	void InsertTreeItems(DbRecordSet &DropedRecordSet, QTreeWidgetItem *parent, int index, bool bOnInitialize = false);
	void InsertContactItems(DbRecordSet &DropedRecordSet, QTreeWidgetItem *parent, int index, bool bOnInitialize = false);
	void AddRowToResultRecordSet(DbRecordSet &RecordSet);
	void RemoveTreeItemsForTreeEntity();

	int			m_nSelectorType;
	QFrame		*m_pSelectorWidget;
	QFrame		*m_pSelectedWidget;
	DbRecordSet m_recSelectTreeRecordSet;
	
	MainEntitySelectionController m_mainDataCacheSelector;		//initialized in constructor by entity

protected slots:
	void onSelectedTreeDroped(QTreeWidgetItem *parent, int index, const QMimeData *data, Qt::DropAction action);
	void on_AddSelected_pushButton_clicked(bool bExpanItems = false);
	void on_RemoveAll_pushButton_clicked();
	void on_RemoveSelected_pushButton_clicked();
	void on_AddAll_pushButton_clicked();
	void on_AddNew_pushButton_clicked();
	void CompleteChanged();
	void onItemSelectionChanged();
	void onSelectedTreeChanged();
};

#endif // SIMPLESELECTIONWIZPAGE_H
