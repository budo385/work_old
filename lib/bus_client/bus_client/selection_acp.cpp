#include "bus_client/bus_client/selection_acp.h"
#include "bus_core/bus_core/contacttypemanager.h"
#include "db_core/db_core/dbsqltabledefinition.h"
#include "db_core/db_core/dbsqltableview.h"
#include <QLineEdit>

//bo set
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;	


//global message dispatcher
#include "bus_client/bus_client/changemanager.h"
extern ChangeManager g_ChangeManager;


Selection_ACP::Selection_ACP(QWidget *parent)
: QFrame(parent),m_bUseFilterOnType(true)
{
	ui.setupUi(this);

	m_strHtmlTag="<html><head><meta name=\"qrichtext\" content=\"1\" /></head><body style=\" white-space: pre-wrap; font-family:MS Shell Dlg 2; font-size:8.25pt; font-weight:400; font-style:normal; text-decoration:none;\"><p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600; font-style:italic;\">";
	m_strEndHtmlTag="</span></p></body></html>";


	//set icons:
	ui.btnSelect->setIcon(QIcon(":Select16.png"));
	ui.btnRemove->setIcon(QIcon(":SAP_Clear.png"));

	ui.btnSelect->setToolTip(tr("Select"));
	ui.btnRemove->setToolTip(tr("Remove Assignment"));


	m_nCurrentRow=0;
	m_nIDColIdx=-1;
	m_nNameColIdx=-1;

	m_pExternDataManipulator=NULL;
	m_pExternData=NULL;
	m_dlgCachedPopUpSelector=NULL;
	m_bRefreshFromExternalDataInProgress=false;

	setFocusPolicy(Qt::StrongFocus);
	setFocusProxy(ui.cmbName);

}

Selection_ACP::~Selection_ACP()
{

}





//Clear & Simple: init with entity type: use GetCurrentRecord & SetCurrentRecord
//ACP specific: data will be loaded at initialize, here:
void Selection_ACP::Initialize(int nEntityTypeID)
{
	m_nEntityTypeID=nEntityTypeID;

	//init selector: data is not yet loaded from server, nor it will be until selection pop's up
	m_dlgPopUp.Initialize(nEntityTypeID);
	m_dlgCachedPopUpSelector=dynamic_cast<MainEntitySelectionController*>(m_dlgPopUp.GetSelectorWidget());
	Q_ASSERT(m_dlgCachedPopUpSelector!=NULL); //must be valid

	//Name column must be defined for APC patterns:
	Q_ASSERT(m_dlgCachedPopUpSelector->m_nNameColumnIdx!=-1); //name must be set

	m_dlgCachedPopUpSelector->registerObserver(this);	//for data change signal in controller

	ui.cmbName->setEditable(true);
	ui.cmbName->setAutoCompletion(true);

	//auto connect linedit control
	if (ui.cmbName->lineEdit())
		connect(ui.cmbName->lineEdit(),SIGNAL(editingFinished()),this,SLOT(onCmbNameLine_editingFinished()));


	//current data is formatted as cache entity:
	m_RowCurrentSelectedEntity.copyDefinition(*m_dlgCachedPopUpSelector->GetDataSource());

	//virtual call to load data at first start (it will back fire ACP combo reload)
	m_dlgCachedPopUpSelector->ReloadData(); 

	//clear:
	Clear(true);

	QWidget::setEnabled(m_dlgCachedPopUpSelector->IsEntityEnabled()); //FP
}

void Selection_ACP::setDisabled ( bool bEnable)
{
	if (m_dlgCachedPopUpSelector)
		if (!m_dlgCachedPopUpSelector->IsEntityEnabled())
			return;
	QWidget::setDisabled(bEnable);
}

void Selection_ACP::setEnabled ( bool bEnable)
{
	if (m_dlgCachedPopUpSelector)
		if (!m_dlgCachedPopUpSelector->IsEntityEnabled())
			return;
	QWidget::setEnabled(bEnable);
}
//ID is store inside field ID field of pExternData
//RefreshDisplay() will use ID from data
//if assignment is changed it will changed inside pExternData
void Selection_ACP::Initialize(int nEntityTypeID,GuiDataManipulator *pExternDataManipulator, QString strCol,bool bColumnIsID)
{
	m_pExternDataManipulator=pExternDataManipulator;
	Q_ASSERT(m_pExternDataManipulator);
	if (bColumnIsID)
	{
		m_nIDColIdx=m_pExternDataManipulator->GetDataSource()->getColumnIdx(strCol);
		Q_ASSERT(m_nIDColIdx!=-1);
	}
	else
	{
		m_nNameColIdx=m_pExternDataManipulator->GetDataSource()->getColumnIdx(strCol);
		Q_ASSERT(m_nNameColIdx!=-1);
	}

	Initialize(nEntityTypeID);
}

//ID is store inside field ID field of pExternData
//RefreshDisplay() will use ID from data
//if assignment is changed it will changed inside pExternData
void Selection_ACP::Initialize(int nEntityTypeID,DbRecordSet *pExternData, QString strCol, bool bColumnIsID)
{
	m_pExternData=pExternData;
	Q_ASSERT(m_pExternData);
	if (bColumnIsID)
	{
		m_nIDColIdx=m_pExternData->getColumnIdx(strCol);
		Q_ASSERT(m_nIDColIdx!=-1);
	}
	else
	{
		m_nNameColIdx=m_pExternData->getColumnIdx(strCol);
		Q_ASSERT(m_nNameColIdx!=-1);
	}

	Initialize(nEntityTypeID);
}





//set label on selection button:
void Selection_ACP::SetSelectionButtonLabel(QString strSelButtonLabel)
{
	ui.btnSelect->setSizePolicy(QSizePolicy::Minimum,QSizePolicy::Fixed);
	ui.btnSelect->setMinimumWidth(20);
	ui.btnSelect->setMaximumWidth(16777215);
	ui.btnSelect->resize(20,ui.btnSelect->width()); 
	ui.btnSelect->setText(strSelButtonLabel);
}


//show from data source in widgets, takes data from current row, does not emit change signal...
void Selection_ACP::RefreshDisplay()
{
	m_bRefreshFromExternalDataInProgress=true;

	if (m_pExternDataManipulator!=NULL)
	{
		if(m_pExternDataManipulator->GetDataSource()->getRowCount()>m_nCurrentRow)
			if (m_nIDColIdx!=-1)
				SetCurrentEntityRecord(m_pExternDataManipulator->GetDataSource()->getDataRef(m_nCurrentRow,m_nIDColIdx).toInt(),true);
			else
				SetCurrentDisplayName(m_pExternDataManipulator->GetDataSource()->getDataRef(m_nCurrentRow,m_nNameColIdx).toString(),true);
		else
			Clear(true);
		
		m_bRefreshFromExternalDataInProgress=false;
		return;
	}

	if (m_pExternData!=NULL)
	{
		if(m_pExternData->getRowCount()>m_nCurrentRow)
			if (m_nIDColIdx!=-1)
				SetCurrentEntityRecord(m_pExternData->getDataRef(m_nCurrentRow,m_nIDColIdx).toInt(),true);
			else
				SetCurrentDisplayName(m_pExternData->getDataRef(m_nCurrentRow,m_nNameColIdx).toString(),true);
		else
			Clear(true);
		
		m_bRefreshFromExternalDataInProgress=false;
		return;
	}
}





//--------------------------------------------------------------
//					EVENT HANDLERS
//--------------------------------------------------------------

//open selector, get selection, display & store inside data source
void Selection_ACP::on_btnSelect_clicked()
{
	Q_ASSERT(m_dlgCachedPopUpSelector); //test if initialized

	DbRecordSet empty;
	int nResult=m_dlgPopUp.OpenSelector();
	if(nResult)
	{
		DbRecordSet record;
		int nRecordID;
		m_dlgPopUp.GetSelectedData(nRecordID,record);
		SetEntityData(nRecordID,record);
	}

}


//remove all entries, store NULL inside datasource
void Selection_ACP::on_btnRemove_clicked()
{
	Q_ASSERT(m_dlgCachedPopUpSelector); //test if initialized
	Clear();
}


void Selection_ACP::on_cmbName_editTextChanged(const QString &text)
{
	Q_ASSERT(m_dlgCachedPopUpSelector); //test if initialized
	SetEntityData(text);

	if (!m_bUseFilterOnType)
	{
		return;
	}

	
	//filter list based on text entered:
	if (text.isEmpty())
	{
		DbRecordSet *lstData=m_dlgCachedPopUpSelector->GetDataSource();
		ReloadComboBox(lstData);
		emit ComboTextChanged(text);
		return;
	}

	int nSize=ui.cmbName->count();
	for(int i=nSize-1;i>=0;--i)
	{
		if (ui.cmbName->itemText(i).compare(text,Qt::CaseInsensitive)==0)
		{
			emit ComboTextChanged(text);
			return;
		}
	}


	//reset content:
	DbRecordSet *lstData=m_dlgCachedPopUpSelector->GetDataSource();
	ReloadComboBox(lstData);

	ui.cmbName->blockSignals(true);
	if (ui.cmbName->lineEdit())
		ui.cmbName->lineEdit()->blockSignals(true);

	nSize=ui.cmbName->count();
	for(int i=nSize-1;i>=0;--i)
	{
		if (ui.cmbName->itemText(i).indexOf(text,0,Qt::CaseInsensitive)!=0)
		{
			ui.cmbName->removeItem(i);
		}
	}

	ui.cmbName->setEditText(text);
	ui.cmbName->blockSignals(false);
	if (ui.cmbName->lineEdit())
		ui.cmbName->lineEdit()->blockSignals(false);
	emit ComboTextChanged(text);
}

void Selection_ACP::on_cmbName_currentIndexChanged(const QString &text)
{
	Q_ASSERT(m_dlgCachedPopUpSelector); //test if initialized
	SetEntityData(text);
}

//coz Combo doesnt properly handle event when autocompletion is on, catch events from linedit directly
void Selection_ACP::onCmbNameLine_editingFinished()
{
	Q_ASSERT(m_dlgCachedPopUpSelector); //test if initialized
	QString text;
	if (ui.cmbName->lineEdit())
		text=ui.cmbName->lineEdit()->text();
	SetEntityData(text);
	emit ComboTextEditFinish();

}






//reload data signal from selector: reload combo if needed, if selector changed data
void Selection_ACP::updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail,const QVariant data)
{
	if(pSubject==m_dlgCachedPopUpSelector && nMsgCode==SelectorSignals::SELECTOR_DATA_CHANGED)
	{
		Q_ASSERT(m_dlgCachedPopUpSelector!=NULL);
		DbRecordSet *lstData=m_dlgCachedPopUpSelector->GetDataSource();

		ReloadComboBox(lstData);
	}
}



//reload data for combo box, preserving current item if exist in list
void Selection_ACP::ReloadComboBox(DbRecordSet *lstData)
{

	//disconnect signals:
	ui.cmbName->blockSignals(true);
	if (ui.cmbName->lineEdit())
		ui.cmbName->lineEdit()->blockSignals(true);


	ui.cmbName->clear();

	int nColIdx=m_dlgCachedPopUpSelector->m_nNameColumnIdx;
	Q_ASSERT(nColIdx!=-1); //View for combo boxes in ACP mode must contain NAME column....

	//add data into combo:
	int nSize=lstData->getRowCount();
	for(int i=0;i<nSize;++i)
	{
		ui.cmbName->addItem(lstData->getDataRef(i,nColIdx).toString());
	}


	ui.cmbName->clearEditText();

	//connect signals back:
	ui.cmbName->blockSignals(false);
	if (ui.cmbName->lineEdit())
		ui.cmbName->lineEdit()->blockSignals(false);

}




//set this data on display and inside data source
void Selection_ACP::SetEntityData(int nRecordID, DbRecordSet &record,bool bSkipNotifyObservers)
{
	//first set new name on display & store record:
	QString strTextToDisplay;
	if (nRecordID!=-1)
	{
		strTextToDisplay=m_dlgCachedPopUpSelector->GetCalculatedName(nRecordID);
	}
	else if(m_dlgCachedPopUpSelector->m_nNameColumnIdx>=0 && record.getRowCount()>0)
	{
		strTextToDisplay=record.getDataRef(0,m_dlgCachedPopUpSelector->m_nNameColumnIdx).toString();
	}
	
	
	ui.cmbName->blockSignals(true);
	if (ui.cmbName->isEditable())
	{
		//set data on combo:
		ui.cmbName->clearEditText();
		ui.cmbName->setAutoCompletion(false); //prevent highlighting...
		ui.cmbName->setEditText(strTextToDisplay);
		ui.cmbName->setAutoCompletion(true);
	}
	else
	{
		int i=ui.cmbName->findText(strTextToDisplay);
		ui.cmbName->setCurrentIndex(i);
	}
	ui.cmbName->blockSignals(false);
	


	//store record inside our record:
	m_RowCurrentSelectedEntity=record;

	//if refrsh from data, skip backfire:
	if (!m_bRefreshFromExternalDataInProgress)
	{
		//if external, clean inside them
		if (m_pExternDataManipulator!=NULL)
		{
			if(m_pExternDataManipulator->GetDataSource()->getRowCount()>m_nCurrentRow && m_nNameColIdx!=-1)
				m_pExternDataManipulator->ChangeData(m_nCurrentRow,m_nNameColIdx,strTextToDisplay);
		}
		if (m_pExternData!=NULL)
		{
			if(m_pExternData->getRowCount()>m_nCurrentRow && m_nNameColIdx!=-1)
				m_pExternData->setData(m_nCurrentRow,m_nNameColIdx,strTextToDisplay);
		}
	}


	//notify all observers
	if(!bSkipNotifyObservers)
	{

		QVariant varData;
		qVariantSetValue(varData,record);
		notifyObservers(SelectorSignals::SELECTOR_SELECTION_CHANGED,nRecordID,varData);
	}

}



//set this data on display and inside data source
void Selection_ACP::SetEntityData(QString strNewDisplayData,bool bSkipNotifyObservers)
{
	if (strNewDisplayData!=ui.cmbName->currentText())
	{
		ui.cmbName->blockSignals(true);
		if (ui.cmbName->isEditable())
		{
			ui.cmbName->clearEditText();
			ui.cmbName->setAutoCompletion(false); //prevent highlighting...
			ui.cmbName->setEditText(strNewDisplayData);
			ui.cmbName->setAutoCompletion(true);
		}
		else
		{
			int i=ui.cmbName->findText(strNewDisplayData);
			ui.cmbName->setCurrentIndex(i);
		}
		ui.cmbName->blockSignals(false);
	}
	
	//store name inside our record:
	m_RowCurrentSelectedEntity.clear();
	m_RowCurrentSelectedEntity.addRow();
	m_RowCurrentSelectedEntity.setData(0,m_dlgCachedPopUpSelector->m_nNameColumnIdx,strNewDisplayData);

	//if refrsh from data, skip backfire:
	if (!m_bRefreshFromExternalDataInProgress)
	{
		//if external, clean inside them
		if (m_pExternDataManipulator!=NULL)
		{
			if(m_pExternDataManipulator->GetDataSource()->getRowCount()>m_nCurrentRow && m_nNameColIdx!=-1)
				m_pExternDataManipulator->ChangeData(m_nCurrentRow,m_nNameColIdx,strNewDisplayData);
		}
		if (m_pExternData!=NULL)
		{
			if(m_pExternData->getRowCount()>m_nCurrentRow && m_nNameColIdx!=-1)
				m_pExternData->setData(m_nCurrentRow,m_nNameColIdx,strNewDisplayData);
		}
	}
	



	//notify all observers
	if(!bSkipNotifyObservers)
	{

		QVariant varData;
		qVariantSetValue(varData,m_RowCurrentSelectedEntity);
		notifyObservers(SelectorSignals::SELECTOR_SELECTION_CHANGED,-1,varData);
	}

}



//sets SAPNE data for given entity record ID, data is lookup in cache then on server:
//data is set in datasource and on display
//signal can be omitted to avoid endless loops
void Selection_ACP::SetCurrentEntityRecord(int nEntityRecordID,bool bSkipNotifyObservers)
{
	DbRecordSet record;
	QString strCode,strName;

	Q_ASSERT(m_dlgCachedPopUpSelector!=NULL); //must be set
	bool bOK=m_dlgCachedPopUpSelector->GetEntityRecord(nEntityRecordID,record);
	if(bOK)
	{
		m_RowCurrentSelectedEntity=record;
		SetEntityData(nEntityRecordID,record,bSkipNotifyObservers);
	}
	else
	{
		Clear(bSkipNotifyObservers); //if invalid id, clear all
	}

}


void Selection_ACP::SetCurrentDisplayName(QString strDisplayData,bool bSkipNotifyObservers)
{
	SetEntityData(strDisplayData);
}



void Selection_ACP::SetEditMode(bool bEditMode)
{
	ui.btnSelect->setEnabled(bEditMode);
	ui.btnRemove->setEnabled(bEditMode);

	ui.cmbName->blockSignals(true); //B.T. 04.04.2012 issue 2611
	if (ui.cmbName->lineEdit())
		ui.cmbName->lineEdit()->blockSignals(true);
	ui.cmbName->setEnabled(bEditMode);
	ui.cmbName->blockSignals(false);
	if (ui.cmbName->lineEdit())
		ui.cmbName->lineEdit()->blockSignals(false);
}

void Selection_ACP::DisableEntryText()
{
	ui.cmbName->setEditable(false);
}



//if nothing selected, false will be returned
bool Selection_ACP::GetCurrentEntityRecord(int &nEntityRecordID,DbRecordSet &record)
{
	if (ui.cmbName->currentText().isEmpty())
	{
		nEntityRecordID=-1;
		return false;
	}
	else
	{
		//if not exists:
		if (m_RowCurrentSelectedEntity.getRowCount()!=1)
		{
			nEntityRecordID=-1;
			return false;
		}

		if (m_dlgCachedPopUpSelector->m_nPrimaryKeyColumnIdx<0)
		{
			nEntityRecordID=-1;
			return false;
		}

		//test current record, if newly added, try to save on server (ACP has ability only to add new entries)
		if (m_RowCurrentSelectedEntity.getDataRef(0,m_dlgCachedPopUpSelector->m_nPrimaryKeyColumnIdx).toInt()<=0)
		{
			if(!ACPSaveSelection())
			{
				nEntityRecordID=-1;
				return false;
			}
		}

		//valid record:
		record=m_RowCurrentSelectedEntity;
		nEntityRecordID=m_RowCurrentSelectedEntity.getDataRef(0,m_dlgCachedPopUpSelector->m_nPrimaryKeyColumnIdx).toInt();
		return true;
	}
}

bool Selection_ACP::GetCurrentEntityRecord(int &nEntityRecordID)
{
	DbRecordSet temp;
	return GetCurrentEntityRecord(nEntityRecordID,temp);
}




QString Selection_ACP::GetCurrentDisplayName()
{
	return ui.cmbName->currentText();
}



void Selection_ACP::Clear(bool bSkipNotifyObservers)
{
	
	Q_ASSERT(m_dlgCachedPopUpSelector); //test if initialized

	//clear GUI
	if (ui.cmbName->isEditable())
		ui.cmbName->clearEditText();		//this will fire events that will erase content automatically
	else
		ui.cmbName->setCurrentIndex(-1);

	//clear data:
	m_RowCurrentSelectedEntity.clear();
	QVariant empty(QVariant::Int);
	QVariant emptystr(QVariant::String);

	//if refrsh from data, skip backfire:
	if (!m_bRefreshFromExternalDataInProgress)
	{

		//if external, clean inside them
		if (m_pExternDataManipulator!=NULL)
		{
			if(m_pExternDataManipulator->GetDataSource()->getRowCount()>m_nCurrentRow && m_nIDColIdx!=-1)
				m_pExternDataManipulator->ChangeData(m_nCurrentRow,m_nIDColIdx,empty);

			if(m_pExternDataManipulator->GetDataSource()->getRowCount()>m_nCurrentRow && m_nNameColIdx!=-1)
				m_pExternDataManipulator->ChangeData(m_nCurrentRow,m_nNameColIdx,emptystr);
		}

		if (m_pExternData!=NULL)
		{
			if(m_pExternData->getRowCount()>m_nCurrentRow && m_nIDColIdx!=-1)
				m_pExternData->setData(m_nCurrentRow,m_nIDColIdx,empty);

			if(m_pExternData->getRowCount()>m_nCurrentRow && m_nNameColIdx!=-1)
				m_pExternData->setData(m_nCurrentRow,m_nNameColIdx,emptystr);
		}
	}


	//notify all observers
	if(!bSkipNotifyObservers)
	{

		QVariant varData;
		qVariantSetValue(varData,m_RowCurrentSelectedEntity);
		notifyObservers(SelectorSignals::SELECTOR_SELECTION_CHANGED,-1,varData); //invalid record:
	}
}


//when trying to fetch result, ask for save
bool Selection_ACP::ACPSaveSelection()
{
	//if (ui.cmbName->lineEdit())
	QString strCurrentText=ui.cmbName->currentText(); //lineEdit()->text();

	DbRecordSet *pList=m_dlgCachedPopUpSelector->GetDataSource();
	int nRow=pList->find(m_dlgCachedPopUpSelector->m_nNameColumnIdx,strCurrentText,true);
	if (nRow>=0)
	{
		m_RowCurrentSelectedEntity.clear();
		m_RowCurrentSelectedEntity.addRow();
		DbRecordSet row = pList->getRow(nRow);
		m_RowCurrentSelectedEntity.assignRow(0,row);
		return true;
	}

	switch(m_nEntityTypeID)
	{
	case ENTITY_BUS_CM_TYPES:
		{
			DbRecordSet row;
			row.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_TYPES));
			row.addRow();
			if (m_dlgCachedPopUpSelector)
			{
				DbRecordSet rowFilter= m_dlgCachedPopUpSelector->GetLocalFilter()->GetRecordSet();

				if (rowFilter.getRowCount()>0)
				{
					int nTypeSubType=rowFilter.getDataRef(0,"COLUMN_VALUE").toInt(); //TASK or PRESENCE TYPE

					row.setData(0,"BCMT_ENTITY_TYPE",nTypeSubType);
					row.setData(0,"BCMT_IS_DEFAULT",0);
					row.setData(0,"BCMT_TYPE_NAME",strCurrentText);

					Status err;
					QString pLockResourceID;
					int nQueryView = -1;
					int nSkipLastColumns = 0; 
					DbRecordSet lstForDelete;
					_SERVER_CALL(ClientSimpleORM->Write(err,BUS_CM_TYPES,row, pLockResourceID, nQueryView, nSkipLastColumns, lstForDelete))
						if(!err.IsOK())
						{
							QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
							return false;
						}
						m_RowCurrentSelectedEntity.assignRow(0,row);
						m_dlgCachedPopUpSelector->RefreshToGlobalCache(MainEntitySelectionController::CACHE_UPDATE_DATA_INSERTED,false,&row);
						return true;
				}
			}
		}
		break;
	
	case ENTITY_NMRX_ROLES:
		{
			//data is store in the filter, try to get it
			DbRecordSet filter=m_dlgCachedPopUpSelector->GetLocalFilter()->Serialize();
			if (filter.getColumnCount()==0 || filter.getRowCount()!=2)
				return false;
			

			QString strFind="BNRO_TABLE_1";
			nRow=filter.find("COLUMN_NAME",strFind,true);
			if (nRow==-1)
				return false;
			int nTable1=filter.getDataRef(nRow,"COLUMN_VALUE").toInt();

			strFind="BNRO_TABLE_2";
			nRow=filter.find("COLUMN_NAME",strFind,true);
			if (nRow==-1)
				return false;
			int nTable2=filter.getDataRef(nRow,"COLUMN_VALUE").toInt();

			DbRecordSet rowRole,rowForDel;
			rowRole.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_ROLE));
			rowRole.addRow();
			rowRole.setData(0,"BNRO_TABLE_1",nTable1);
			rowRole.setData(0,"BNRO_TABLE_2",nTable2);
			rowRole.setData(0,"BNRO_NAME",strCurrentText);
			rowRole.setData(0,"BNRO_ASSIGMENT_TEXT",strCurrentText);


			Status err;
			_SERVER_CALL(BusNMRX->WriteRole(err,rowRole,rowForDel,""))
			if(!err.IsOK())
			{
				QMessageBox::critical(NULL,tr("Error"),err.getErrorText(),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
				return false;
			}

			m_RowCurrentSelectedEntity.assignRow(0,rowRole);
			m_dlgCachedPopUpSelector->RefreshToGlobalCache(MainEntitySelectionController::CACHE_UPDATE_DATA_INSERTED,false,&rowRole);
			return true;
		}

	}
	//no save occured
	return false;

}


QWidget* Selection_ACP::GetButton(int nButton)
{

	switch(nButton)
	{
	//case BUTTON_MODIFY:
	//	return ui.btnModify;
		break;
	case BUTTON_SELECT:
		return ui.btnSelect;
		break;
	case BUTTON_REMOVE:
		return ui.btnRemove;
		break;
	//case BUTTON_ADD:
	//	return ui.btnInsert;
		break;
	default:
		Q_ASSERT(false);
		break;
	}

	return NULL;

}


