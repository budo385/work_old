#include "clientdocumenthandler.h"
#include "common/common/threadid.h"

#include "bus_client/bus_client/documenthelper.h"
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;
#include "bus_trans_client/bus_trans_client/businessservicemanager_thinclient.h"
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;

ClientDocumentHandler::ClientDocumentHandler(QObject *parent)
	: QObject(parent),m_nPhase(PHASE_NO_OP),m_bEnableHTTPUserStorageMode(false)
{

}

ClientDocumentHandler::~ClientDocumentHandler()
{
	//qDebug()<<"ClientDocumentHandler destroyed";
}

//m.o. : 
//1. path must be already resolved
//2. engage check out on server, no return info needed->store checked out document directly on server disk, wait for server! (ordinary client call in separate thread)
//3. when finished, only saved filename return
//4. engage http client download protocol....->as usual.hmmm..just reconnect signals
void ClientDocumentHandler::CheckOutDocument(Status& pStatus, int nDocumentID,QString strFilePath,bool bReadOnly,bool bSkipLock, int nRevisionID)
{
	if (!m_bEnableHTTPUserStorageMode)
	{
		g_pClientManager->StartThreadConnection(pStatus,this);
		if (!pStatus.IsOK())return;

		m_nPhase=PHASE_CHECK_OUT_BLOB2FILE_WRITE;
		g_pBoSet->app->BusDocuments->CheckOut(pStatus,nDocumentID,g_pClientManager->GetPersonID(),RecDocumentRevision,bReadOnly,RecLastCheckOutInfo,strFilePath,bSkipLock,nRevisionID);
		if (!pStatus.IsOK())
		{
			Status tmp_err; //don't bother with close err:
			g_pClientManager->CloseThreadConnection(tmp_err);
			m_nPhase=PHASE_NO_OP;
			return;
		}
		g_pClientManager->CloseThreadConnection(pStatus);

		//RecDocumentRevision.Dump();

		m_nPhase=PHASE_NO_OP;
		return;
	}

	//not possible in thick client
	BusinessServiceManager_ThinClient *pBo = dynamic_cast<BusinessServiceManager_ThinClient*>(g_pBoSet);
	if (pBo==NULL)
	{
		pStatus.setError(1,tr("Not available in the thick client"));
		return;
	}
	//IMPORTANT TO KEEP OTHER CONNECTION...for session to be alive
	g_pClientManager->StartThreadConnection(pStatus,this);
	if (!pStatus.IsOK())return;

	HTTPClientConnectionSettings settings;
	Authenticator auth;
	pBo->GetSettings(settings,auth);

	m_nPhase=PHASE_CHECK_OUT_BLOB2FILE_WRITE;

	g_pBoSet->app->BusDocuments->CheckOutToUserStorage(pStatus,nDocumentID,g_pClientManager->GetPersonID(),bReadOnly,RecDocumentRevision,RecLastCheckOutInfo,strFilePath,bSkipLock,nRevisionID);
	if (!pStatus.IsOK())
	{
		Status tmp_err; //don't bother with close err:
		g_pClientManager->CloseThreadConnection(tmp_err);
		m_nPhase=PHASE_NO_OP;
		return;
	}

	QFileInfo info(strFilePath);
	m_nPhase=PHASE_CHECK_OUT_DOWNLOAD;

	m_Httpclient = new UserStorageHTTPClient;
	m_Httpclient->SetConnectionSettings(settings,auth);
	connect(m_Httpclient,SIGNAL(SocketOperationInProgress(int,qint64,qint64)),this,SIGNAL(CommunicationInProgress(int,qint64,qint64)));
	m_Httpclient->DownLoadFile(pStatus,strFilePath,true);

	Status tmp_err; //don't bother with close err:
	g_pClientManager->CloseThreadConnection(tmp_err);

	m_nPhase=PHASE_NO_OP;
	m_Httpclient->deleteLater();
	m_Httpclient=NULL;
}

//m.o. : 
//1. path must be already resolved
//2. upload
//3. check in on server
void ClientDocumentHandler::CheckInDocument(Status& pStatus, int nDocumentID,QString strFilePath,bool bOverWrite,bool bSkipLock,QString strTag)
{
	//qDebug()<<"CheckInDocument, thread: "<<ThreadIdentificator::GetCurrentThreadID()<<" doc:" <<strFilePath;

	if (!m_bEnableHTTPUserStorageMode)
	{
		qDebug()<<"CheckInDocument, http mode";

		g_pClientManager->StartThreadConnection(pStatus,this);
		if (!pStatus.IsOK())return;

		QFileInfo info(strFilePath);

		m_nPhase=PHASE_CHECK_IN_FILE2BLOB_READ;
		g_pBoSet->app->BusDocuments->CheckIn(pStatus,nDocumentID,RecDocumentRevision,bOverWrite,RecLastCheckOutInfo,bSkipLock,info.size());
		if (!pStatus.IsOK())
		{
			Status tmp_err; //don't bother with close err:
			g_pClientManager->CloseThreadConnection(tmp_err);
			m_nPhase=PHASE_NO_OP;
			return;
		}
		g_pClientManager->CloseThreadConnection(pStatus);

		m_nPhase=PHASE_NO_OP;
		//issue 2674: delete temp file after upload:
		if (strFilePath.toLower().indexOf(DocumentHelper::GetDefaultDocumentDirectory().toLower())==0)
			QFile::remove(strFilePath);
		return;
	}

	//not possible in thick client
	BusinessServiceManager_ThinClient *pBo = dynamic_cast<BusinessServiceManager_ThinClient*>(g_pBoSet);
	if (pBo==NULL)
	{
		pStatus.setError(1,tr("Not available in the thick client"));
		return;
	}

	//IMPORTANT TO KEEP OTHER CONNECTION...for session to be alive
	g_pClientManager->StartThreadConnection(pStatus,this);
	if (!pStatus.IsOK())
	{
		return;
	}

	HTTPClientConnectionSettings settings;
	Authenticator auth;
	pBo->GetSettings(settings,auth);

	QFileInfo info(strFilePath);
	m_nPhase=PHASE_CHECK_IN_UPLOAD;

	m_Httpclient = new UserStorageHTTPClient;
	m_Httpclient->SetConnectionSettings(settings,auth);
	connect(m_Httpclient,SIGNAL(SocketOperationInProgress(int,qint64,qint64)),this,SIGNAL(CommunicationInProgress(int,qint64,qint64)));
	m_Httpclient->UpLoadFile(pStatus,strFilePath,true);
	if (!pStatus.IsOK())
	{
		Status tmp_err; //don't bother with close err:
		g_pClientManager->CloseThreadConnection(tmp_err);
		return;
	}

	m_nPhase=PHASE_NO_OP;
	m_Httpclient->deleteLater();
	


	m_nPhase=PHASE_CHECK_IN_FILE2BLOB_READ;

	g_pBoSet->app->BusDocuments->CheckInFromUserStorage(pStatus,nDocumentID,g_pClientManager->GetPersonID(),bOverWrite,RecDocumentRevision,RecLastCheckOutInfo,bSkipLock,strTag);
	if (!pStatus.IsOK())
	{
		Status err;
		m_Httpclient->ClearFromUserStorage(err,info.fileName()); //clear garbage
		g_pClientManager->CloseThreadConnection(err);
		m_nPhase=PHASE_NO_OP;
		m_Httpclient=NULL;
		return;
	}

	Status tmp_err; //don't bother with close err:
	g_pClientManager->CloseThreadConnection(tmp_err);

	m_nPhase=PHASE_NO_OP;
	m_Httpclient=NULL;

	//issue 2674: delete temp file after upload:
	if (strFilePath.toLower().indexOf(DocumentHelper::GetDefaultDocumentDirectory().toLower())==0)
		QFile::remove(strFilePath);
}

void ClientDocumentHandler::OnExecCheckOutDocument(int nDocumentID,QString strFilePath,bool bReadOnly,bool bSkipLock, int nRevisionID)
{
	Status err;
	CheckOutDocument(err,nDocumentID,strFilePath,bReadOnly,bSkipLock,nRevisionID);
	emit OperationEnded(err.getErrorCode(),err.getErrorTextRaw());
}
void ClientDocumentHandler::OnExecCheckInDocument(int nDocumentID,QString strFilePath,bool bOverWrite,bool bSkipLock,QString strTag)
{
	Status err;
	CheckInDocument(err,nDocumentID,strFilePath,bOverWrite,bSkipLock,strTag);
	emit OperationEnded(err.getErrorCode(),err.getErrorTextRaw());
}
void ClientDocumentHandler::OnCancelOperation()
{
	if (m_nPhase==PHASE_CHECK_OUT_BLOB2FILE_WRITE || m_nPhase==PHASE_CHECK_IN_FILE2BLOB_READ)
	{
		g_pBoSet->AbortDataTransfer();
	}	
	else if (m_nPhase==PHASE_CHECK_OUT_DOWNLOAD || m_nPhase==PHASE_CHECK_IN_UPLOAD)
	{
		if(m_Httpclient)m_Httpclient->CancelOperation();
	}
}



ClientDocumentHandlerThread::ClientDocumentHandlerThread()
: QThread(NULL),m_SlaveThread(NULL)
{

}

/*!
Destructor: stops socket thread
*/
ClientDocumentHandlerThread::~ClientDocumentHandlerThread()
{
	emit SignalDestroy();				//signal socket to init auto-destruct sequence
	wait(100);
	if(isRunning())
	{
		quit();		//stop thread
		wait();
	}
	m_SlaveThread=NULL;
}

void ClientDocumentHandlerThread::StartThread()
{
	m_ThreadSync.ThreadSetForWait();
	start(); //start thread
	m_ThreadSync.ThreadWait(); //wait until thread is started

}

void ClientDocumentHandlerThread::run()
{
	m_SlaveThread=new ClientDocumentHandler();

	connect(this,SIGNAL(SignalExecCheckOutDocument(int,QString,bool,bool,int)),m_SlaveThread,SLOT(OnExecCheckOutDocument(int,QString,bool,bool,int)),Qt::QueuedConnection);
	connect(this,SIGNAL(SignalExecCheckInDocument(int,QString,bool,bool,QString)),m_SlaveThread,SLOT(OnExecCheckInDocument(int,QString,bool,bool,QString)),Qt::QueuedConnection);
	connect(this,SIGNAL(SignalDestroy()),m_SlaveThread,SLOT(deleteLater()));
	connect(this,SIGNAL(SignalAbortOperation()),m_SlaveThread,SLOT(OnCancelOperation()));
	connect(m_SlaveThread,SIGNAL(OperationEnded(int, QString)),this,SIGNAL(SignalOperationEnded(int, QString)));	
	connect(m_SlaveThread,SIGNAL(CommunicationInProgress(int,qint64,qint64)),this,SIGNAL(SocketOperationInProgress(int,qint64,qint64)));

	m_ThreadSync.ThreadRelease();
	exec(); 
}

void ClientDocumentHandlerThread::ExecCheckOutDocument(int nDocumentID,QString strFilePath,bool bReadOnly,bool bSkipLock, int nRevisionID)
{
	emit SignalExecCheckOutDocument(nDocumentID,strFilePath,bReadOnly,bSkipLock,nRevisionID);
}
void ClientDocumentHandlerThread::ExecCheckInDocument(int nDocumentID,QString strFilePath,bool bOverWrite,bool bSkipLock,QString strTag)
{
	emit SignalExecCheckInDocument(nDocumentID,strFilePath,bOverWrite,bSkipLock,strTag);
}
void ClientDocumentHandlerThread::AbortOperation()
{
	emit SignalAbortOperation();
}

void ClientDocumentHandlerThread::GetLastDocumentInfo(DbRecordSet &recLastCheckOutInfo)
{
	if (m_SlaveThread)
		m_SlaveThread->GetLastDocumentInfo(recLastCheckOutInfo);
}
void ClientDocumentHandlerThread::GetDocumentRevision(DbRecordSet &rec)
{
	if (m_SlaveThread)
		m_SlaveThread->GetDocumentRevision(rec);
}
void ClientDocumentHandlerThread::SetDocumentRevision(DbRecordSet &rec)
{
	if (m_SlaveThread)
		m_SlaveThread->SetDocumentRevision(rec);
}
void ClientDocumentHandlerThread::EnableHTTPUserStorageMode(bool bEnable)
{
	if (m_SlaveThread)
		m_SlaveThread->EnableHTTPUserStorageMode(bEnable);
}
bool ClientDocumentHandlerThread::IsHTTPUserStorageModeEnabled()
{
	if (m_SlaveThread)
		return m_SlaveThread->IsHTTPUserStorageModeEnabled();

	return false;
}