#include "email_recepienttable.h"
#include "bus_core/bus_core/emailhelpercore.h"
#include "db_core/db_core/dbsqltableview.h"
#include "bus_client/bus_client/emailselectordialog.h"
#include "bus_client/bus_client/selectionpopup.h"
#include <QHeaderView>
#include "common/common/entity_id_collection.h"
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QHeaderView>
#include <QComboBox>
#include <QLineEdit>
#include <QToolButton>
#include "gui_core/gui_core/thememanager.h"
#include "gui_core/gui_core/macros.h"
#include "bus_client/bus_client/emailhelper.h"

#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;


Email_RecepientTable::Email_RecepientTable(QWidget *parent)
:QTableWidget(parent)
{
	m_bEdit=true;
	horizontalHeader()->hide();
	verticalHeader()->hide();
	verticalHeader()->setDefaultSectionSize(20);
	horizontalHeader()->setStretchLastSection(true);
	setColumnCount(3);
	setColumnWidth(0,60);
	setColumnWidth(1,50);
	setColumnWidth(2,200);

	m_lstRecepients.defineFromView(DbSqlTableView::getView(DbSqlTableView::MVIEW_EMAIL_RECIPIENTS));

	m_lstTypes.addColumn(QVariant::Int,"TYPE_ID");
	m_lstTypes.addColumn(QVariant::String,"TYPE_NAME");
	m_lstTypes.addRow();
	m_lstTypes.setData(0,"TYPE_ID",0);
	m_lstTypes.setData(0,"TYPE_NAME",tr("TO:"));
	m_lstTypes.addRow();
	m_lstTypes.setData(1,"TYPE_ID",1);
	m_lstTypes.setData(1,"TYPE_NAME",tr("CC:"));
	m_lstTypes.addRow();
	m_lstTypes.setData(2,"TYPE_ID",2);
	m_lstTypes.setData(2,"TYPE_NAME",tr("BCC:"));

	connect(this,SIGNAL(cellClicked(int, int)),this,SLOT(OnCellClicked(int,int)));

	QStringList strSource;
#ifndef WINCE
	//Mails:
	m_AllEmails.Initialize(ENITITY_ALL_EMAIL_ADDRESSES);
	m_AllEmails.ReloadData();

	
	int nSize=m_AllEmails.GetDataSource()->getRowCount();
	for(int i=0;i<nSize;i++)
	{
		strSource.append(m_AllEmails.GetDataSource()->getDataRef(i,0).toString());
	}
#endif
	m_pCompleter = new QCompleter(strSource,this);
	m_pCompleter->setCaseSensitivity(Qt::CaseInsensitive);
	m_pCompleter->setCompletionMode(QCompleter::PopupCompletion);

	//connect(this,SIGNAL(itemPressed(QTableWidgetItem *)),this,SLOT(OnitemPressed(QTableWidgetItem *)));
	//get all emails:

	setSelectionBehavior( QAbstractItemView::SelectRows);
	setSelectionMode(QAbstractItemView::NoSelection);
	setEditTriggers(QAbstractItemView::NoEditTriggers); //no edit by double click possible

	setStyleSheet(ThemeManager::GetTableViewBkg()+" QToolButton {background:transparent;}");
}

Email_RecepientTable::~Email_RecepientTable()
{
	delete m_pCompleter;
}


void Email_RecepientTable::GetRecipients(DbRecordSet &recToRecordSet, DbRecordSet &recCCRecordSet, DbRecordSet &recBCCRecordSet)
{
	Widgets2Data();

	//Destroy recordsets and fill them.
	recToRecordSet.destroy();
	recToRecordSet.defineFromView(DbSqlTableView::getView(DbSqlTableView::MVIEW_EMAIL_RECIPIENTS));
	recCCRecordSet.destroy();
	recCCRecordSet.defineFromView(DbSqlTableView::getView(DbSqlTableView::MVIEW_EMAIL_RECIPIENTS));
	recBCCRecordSet.destroy();
	recBCCRecordSet.defineFromView(DbSqlTableView::getView(DbSqlTableView::MVIEW_EMAIL_RECIPIENTS));


	m_lstRecepients.find("RECIPIENT_TYPE",0);
	recToRecordSet.merge(m_lstRecepients,true);
	recToRecordSet.clearSelection();
	int nSize=recToRecordSet.getRowCount();
	for(int i=0;i<nSize;i++)
	{
		if (recToRecordSet.getDataRef(i,"CONTACT_EMAIL").toString().isEmpty()) //clear empty fields
		{
			recToRecordSet.selectRow(i);
			continue;
		}
		recToRecordSet.setData(i,"CONTACT_EMAIL",EmailHelperCore::GetEmailFromName(recToRecordSet.getDataRef(i,"CONTACT_EMAIL").toString()));
	}
	recToRecordSet.deleteSelectedRows();

	m_lstRecepients.find("RECIPIENT_TYPE",1);
	recCCRecordSet.merge(m_lstRecepients,true);
	recCCRecordSet.clearSelection();
	nSize=recCCRecordSet.getRowCount();
	for(int i=0;i<nSize;i++)
	{
		if (recCCRecordSet.getDataRef(i,"CONTACT_EMAIL").toString().isEmpty()) //clear empty fields
		{
			recCCRecordSet.selectRow(i);
			continue;
		}
		recCCRecordSet.setData(i,"CONTACT_EMAIL",EmailHelperCore::GetEmailFromName(recCCRecordSet.getDataRef(i,"CONTACT_EMAIL").toString()));
	}
	recCCRecordSet.deleteSelectedRows();

	m_lstRecepients.find("RECIPIENT_TYPE",2);
	recBCCRecordSet.merge(m_lstRecepients,true);
	recBCCRecordSet.clearSelection();
	nSize=recBCCRecordSet.getRowCount();
	for(int i=0;i<nSize;i++)
	{ 
		if (recBCCRecordSet.getDataRef(i,"CONTACT_EMAIL").toString().isEmpty()) //clear empty fields
		{
			recBCCRecordSet.selectRow(i);
			continue;
		}
		recBCCRecordSet.setData(i,"CONTACT_EMAIL",EmailHelperCore::GetEmailFromName(recBCCRecordSet.getDataRef(i,"CONTACT_EMAIL").toString()));
	}
	recBCCRecordSet.deleteSelectedRows();


}

void Email_RecepientTable::AppendContacts(DbRecordSet &recToRecordSet, DbRecordSet &recCCRecordSet, DbRecordSet &recBCCRecordSet)
{
	Widgets2Data();

	recToRecordSet.setColValue(recToRecordSet.getColumnIdx("RECIPIENT_TYPE"),0);
	recCCRecordSet.setColValue(recToRecordSet.getColumnIdx("RECIPIENT_TYPE"),1);
	recBCCRecordSet.setColValue(recToRecordSet.getColumnIdx("RECIPIENT_TYPE"),2);
	m_lstRecepients.merge(recToRecordSet);
	m_lstRecepients.merge(recCCRecordSet);
	m_lstRecepients.merge(recBCCRecordSet);

	int nSize=m_lstRecepients.getRowCount();
	for(int i=0;i<nSize;i++)
	{
		if (m_lstRecepients.getDataRef(i,"CONTACT_NAME").toString().isEmpty())
			m_lstRecepients.setData(i,"CONTACT_NAME",EmailHelperCore::GetNameFromEmail(m_lstRecepients.getDataRef(i,"CONTACT_EMAIL").toString()));

		if (!m_lstRecepients.getDataRef(i,"CONTACT_NAME").toString().isEmpty() && !m_lstRecepients.getDataRef(i,"CONTACT_EMAIL").toString().isEmpty())
		{
			QString strMail=EmailHelperCore::GetFullEmailAddress(m_lstRecepients.getDataRef(i,"CONTACT_NAME").toString(),m_lstRecepients.getDataRef(i,"CONTACT_EMAIL").toString());
			m_lstRecepients.setData(i,"CONTACT_EMAIL",strMail);
		}
	}

	RefreshDisplay();
}

void Email_RecepientTable::ClearAll()
{
	m_lstRecepients.clear();
	RefreshDisplay();
}

void Email_RecepientTable::SetEditMode(bool bEdit)
{
	if (bEdit)
	{
		int nSize=rowCount();
		for(int i=0;i<nSize;i++)
		{
			QWidget *pWidget=cellWidget(i,0);
			if (pWidget)
				pWidget->setDisabled(false);
			pWidget=cellWidget(i,1);
			if (pWidget)
				pWidget->setDisabled(false);
			pWidget=cellWidget(i,2);
			if (pWidget)
				pWidget->setDisabled(false);
		}
	}
	else
	{	
		int nSize=rowCount();
		for(int i=0;i<nSize;i++)
		{
			QWidget *pWidget=cellWidget(i,0);
			if (pWidget)
				pWidget->setDisabled(true);
			pWidget=cellWidget(i,1);
			if (pWidget)
				pWidget->setDisabled(true);
			pWidget=cellWidget(i,2);
			if (pWidget)
				pWidget->setDisabled(true);
		}
	}

	m_bEdit=bEdit;

}

void Email_RecepientTable::OnSelectContact(int nRow)
{
	DbRecordSet rowData;
	if (AddContact(rowData))
	{
		Widgets2Data();
		int nType=m_lstRecepients.getDataRef(nRow,"RECIPIENT_TYPE").toInt();
		if (nType==-1)nType=0;
		m_lstRecepients.assignRow(nRow,rowData,true);
		m_lstRecepients.setData(nRow,"RECIPIENT_TYPE",nType);
		RefreshDisplay();
	}
	return;

}
void Email_RecepientTable::OnSelectContactEmail(int nRow)
{
	int nContactID=m_lstRecepients.getDataRef(nRow,"CONTACT_ID").toInt();
	int nEmailID;
	QString strMail=GetContactMail(nContactID,nEmailID);
	if (!strMail.isEmpty())
	{
		Widgets2Data();
		strMail=EmailHelperCore::GetFullEmailAddress(m_lstRecepients.getDataRef(nRow,"CONTACT_NAME").toString(),strMail);
		m_lstRecepients.setData(nRow,"CONTACT_EMAIL",strMail);
		m_lstRecepients.setData(nRow,"EMAIL_ID",nEmailID);
		RefreshDisplay();
	}

}
void Email_RecepientTable::OnDelete(int nRow)
{
	m_lstRecepients.deleteRow(nRow);
	RefreshDisplay();
}

void Email_RecepientTable::OnCellClicked ( int row, int column )
{
	if (!m_bEdit) return;

	if (row==m_lstRecepients.getRowCount())
	{
		Widgets2Data();
		m_lstRecepients.addRow();
		RefreshDisplay();
		if (column==2)
		{
			QTableWidgetItem *itemX=item(row,column);
			editItem(itemX);
			cellWidget(row,column)->setFocus();
		}
	}
}




QString Email_RecepientTable::GetContactMail(int nContactID, int &nEMailID)
{
	//Get contact emails.
	nEMailID=0;
	Status status;

	EmailSelectorDialog emailDialog(nContactID);
	if (emailDialog.exec())
	{
		QList<QString> lstData;
		lstData = emailDialog.getSelectedEmail();

		if (lstData.count()>0)
		{
			nEMailID= QVariant(lstData.takeFirst()).toInt();
			QString strEmail = lstData.takeFirst();
			return strEmail;
		}
	}
	return "";
}

//id + mail + name
bool Email_RecepientTable::AddContact(DbRecordSet &rowRecepient)
{
	rowRecepient.defineFromView(DbSqlTableView::getView(DbSqlTableView::MVIEW_EMAIL_RECIPIENTS));
	//open selector contact pop-up: single selection
	SelectionPopup dlgPopUp;
	dlgPopUp.Initialize(ENTITY_BUS_CONTACT);
	int nResult=dlgPopUp.OpenSelector();
	if(nResult)
	{
		DbRecordSet record;
		int nRecordID;
		dlgPopUp.GetSelectedData(nRecordID,record);
		if (nRecordID>0)
		{
			//from contact id get default mail..
			Status err;
			DbRecordSet recDefaults;
			_SERVER_CALL(BusContact->ReadContactDefaults(err,nRecordID,recDefaults));
			_CHK_ERR_RET_BOOL_ON_FAIL(err);
			if (recDefaults.getRowCount()>0)
			{
				if (!recDefaults.getDataRef(0,"BCME_ADDRESS").toString().isEmpty())
				{
					rowRecepient.addRow();
					rowRecepient.setData(0,"CONTACT_ID",nRecordID);
					QString strName=record.getDataRef(0,"BCNT_FIRSTNAME").toString()+" "+record.getDataRef(0,"BCNT_LASTNAME").toString();
					rowRecepient.setData(0,"CONTACT_NAME",strName);
					rowRecepient.setData(0,"CONTACT_EMAIL",EmailHelperCore::GetFullEmailAddress(strName,recDefaults.getDataRef(0,"BCME_ADDRESS").toString()));
					rowRecepient.setData(0,"RECIPIENT_TYPE",0);
					return true;
				}
			}
		}
	}

	return false;

}

void Email_RecepientTable::Widgets2Data()
{
	int nSize=m_lstRecepients.getRowCount();
	for(int i=0;i<nSize;i++)
	{
		QComboBox *pComboType = dynamic_cast<QComboBox *>(cellWidget(i,1));
		if (pComboType)
		{
			int nCurrIdx=pComboType->currentIndex();
			if (nCurrIdx>=0 && nCurrIdx<m_lstTypes.getRowCount())
				m_lstRecepients.setData(i,"RECIPIENT_TYPE",pComboType->itemData(nCurrIdx).toInt());
			else
				m_lstRecepients.setData(i,"RECIPIENT_TYPE",0); //def = 0
		}
		QLineEdit *pLnEmail = dynamic_cast<QLineEdit *>(cellWidget(i,2));
		if (pLnEmail)
		{
			QString strEmail = pLnEmail->text();
			EmailHelper::FixEmailInputLine(strEmail);
			m_lstRecepients.setData(i,"CONTACT_EMAIL", strEmail);
		}
	}
}

void Email_RecepientTable::RefreshDisplay()
{
	clear();

	int nSize=m_lstRecepients.getRowCount();
	setRowCount(nSize+4); //last 4 is empty
	int nSizeTotal=nSize+4;
	for(int i=0;i<nSizeTotal;i++)
	{
		if (i<nSize)
		{

			RecepientTable_WidgetRow *pWidget=dynamic_cast<RecepientTable_WidgetRow *>(cellWidget(i,0));
			if (!pWidget)
			{
				pWidget = new RecepientTable_WidgetRow(this,i);
				setCellWidget(i,0,pWidget);
			}
			pWidget->setVisible(true);

			QComboBox *pComboType = dynamic_cast<QComboBox *>(cellWidget(i,1));
			if (!pComboType)
			{
				pComboType = new QComboBox;
				pComboType->setEditable(false);
				//type:
				int nSize1=m_lstTypes.getRowCount();
				for(int k=0;k<nSize1;k++)
					pComboType->addItem(m_lstTypes.getDataRef(k,"TYPE_NAME").toString(),m_lstTypes.getDataRef(k,"TYPE_ID").toInt());
				setCellWidget(i,1,pComboType);
			}
			pComboType->setVisible(true);

			QLineEdit *pLnEmail = dynamic_cast<QLineEdit *>(cellWidget(i,2));
			if (!pLnEmail)
			{
				pLnEmail = new QLineEdit;
				pLnEmail->setStyleSheet("QLineEdit {color: white; background:transparent;}");
				pLnEmail->setCompleter(m_pCompleter);
				setCellWidget(i,2,pLnEmail);
			}
			pLnEmail->setVisible(true);

			//mail:
			pLnEmail->setText(m_lstRecepients.getDataRef(i,"CONTACT_EMAIL").toString());

			//type:
			int nCurrType=m_lstTypes.find("TYPE_ID",m_lstRecepients.getDataRef(i,"RECIPIENT_TYPE").toInt(),true);
			pComboType->setCurrentIndex(nCurrType);
		}

		else
		{

			QWidget *pWidget=cellWidget(i,0);
			if (pWidget)
				pWidget->setVisible(false);
			pWidget=cellWidget(i,1);
			if (pWidget)
				pWidget->setVisible(false);
			pWidget=cellWidget(i,2);
			if (pWidget)
				pWidget->setVisible(false);

		}
	}

	clearSelection();
	//if (currentRow()<0)
	//	setCurrentCell (0, 2, QItemSelectionModel::Select);
}










RecepientTable_WidgetRow::RecepientTable_WidgetRow(Email_RecepientTable *parent,int nRow)
{
	m_Parent=parent;
	m_nRow=nRow;

	//: select, open, delete
	//ckb zip
	QSize buttonSize(18,18);
	QToolButton *btnSelect = new QToolButton;
	QToolButton *btnDelete = new QToolButton;
	QToolButton *btnOpen = new QToolButton;
	btnSelect->setIcon(QIcon(":Select16.png"));
	btnDelete->setIcon(QIcon(":SAP_Clear.png"));
	btnOpen->setIcon(QIcon(":SAP_Select.png"));

	btnSelect->setToolTip(tr("Select Contact"));
	btnOpen->setToolTip(tr("Select Contact Email"));
	btnDelete->setToolTip(tr("Remove Recipient"));
	btnSelect->setAutoRaise(true);
	btnOpen->setAutoRaise(true);
	btnDelete->setAutoRaise(true);

	btnSelect->setMaximumSize(buttonSize);
	btnSelect->setMinimumSize(buttonSize);
	btnDelete->setMaximumSize(buttonSize);
	btnDelete->setMinimumSize(buttonSize);
	btnOpen->setMaximumSize(buttonSize);
	btnOpen->setMinimumSize(buttonSize);

	QHBoxLayout *hbox = new QHBoxLayout;
	hbox->addWidget(btnSelect);
	hbox->addWidget(btnDelete);
	hbox->addWidget(btnOpen);
	hbox->setSpacing(1);
	hbox->setContentsMargins(2,0,0,0);

	this->setLayout(hbox);

	connect(btnSelect,SIGNAL(clicked()),this,SLOT(OnSelect()));
	connect(btnDelete,SIGNAL(clicked()),this,SLOT(OnRemove()));
	connect(btnOpen,SIGNAL(clicked()),this,SLOT(OnSelectEmail()));

}

void RecepientTable_WidgetRow::OnSelect()
{
	//add attachment
	m_Parent->OnSelectContact(m_nRow);
}
void RecepientTable_WidgetRow::OnRemove()
{
	//remove
	m_Parent->OnDelete(m_nRow);
}
void RecepientTable_WidgetRow::OnSelectEmail()
{
	//open
	m_Parent->OnSelectContactEmail(m_nRow);
}


RecepientTable_WidgetRow::~RecepientTable_WidgetRow()
{


}

