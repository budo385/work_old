#include "clientdownloadmanager.h"
#include "documenthelper.h"
#include <QDesktopWidget>
#include <QApplication>
#include <QMenu>
#include <QAction>
#include "bus_client/bus_client/documenthelper.h"
#include "bus_client/bus_client/clientdocumenthandler.h"
#include "gui_core/gui_core/macros.h"
#include "common/common/threadid.h"

#include "bus_trans_client/bus_trans_client/businessservicemanager_thinclient.h"
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;
#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;
#include "bus_client/bus_client/changemanager.h"
extern ChangeManager g_ChangeManager;


typedef QMapIterator<int,DownloadWidget*> ActiveDownloadsIterator;

ClientDownloadManager::ClientDownloadManager(QObject *parent)
	: QObject(parent)
{
	m_Dlg = NULL;
	m_sysTray=NULL;
}

ClientDownloadManager::~ClientDownloadManager()
{
	if (m_Dlg)
	{
		AbortAllOperations();
		m_Dlg->hide();
		m_Dlg->deleteLater();
		m_sysTray->hide();
		m_sysTray->deleteLater();
		m_Dlg=NULL;
	}
}


void ClientDownloadManager::ScheduleDownload(QString strFilePath, bool bIsBackup, bool bOverWrite)
{
	CheckDlg();
	//not possible in thick client
	BusinessServiceManager_ThinClient *pBo = dynamic_cast<BusinessServiceManager_ThinClient*>(g_pBoSet);
	if (pBo==NULL)
		return;

	DownloadWidget *pWidget=m_Dlg->AddFileForDownLoad(strFilePath);
	UserStorageHTTPClientThread *client = new UserStorageHTTPClientThread;
	m_hshActiveDownloadThreads[pWidget->m_nFileID]=client;
	m_hshActiveDownloadWidgets[pWidget->m_nFileID]=pWidget;

	if (bIsBackup)
		pWidget->m_bIsBackup=true;
	if (bOverWrite)
		pWidget->m_bOverWrite=true;
	
	HTTPClientConnectionSettings settings;
	Authenticator auth;
	pBo->GetSettings(settings,auth);
	client->StartThread(settings,auth);

	connect(client,SIGNAL(SocketOperationInProgress(int,qint64,qint64)),pWidget,SLOT(OnCommunicationInProgress(int,qint64,qint64)));
	connect(client,SIGNAL(SignalOperationEnded(int,QString)),pWidget,SLOT(OnOperationEnded(int,QString)));

	connect(pWidget,SIGNAL(SignalOpenFile(int)),this,SLOT(OnOpenFile(int)),Qt::QueuedConnection);
	connect(pWidget,SIGNAL(SignalOpenDirectory(int)),this,SLOT(OnOpenDirectory(int)),Qt::QueuedConnection);
	connect(pWidget,SIGNAL(SignalRemoveFile(int)),this,SLOT(OnRemoveFile(int)),Qt::QueuedConnection);
	connect(pWidget,SIGNAL(SignalCancel(int)),this,SLOT(OnCancel(int)),Qt::QueuedConnection);
	connect(pWidget,SIGNAL(SignalOperationEnd(int,int,QString)),this,SLOT(OnOperationEnd(int,int,QString)),Qt::QueuedConnection);

	ExecuteNextInQue();

	if (!m_Dlg->isVisible())
	{
		ActivateAndPositionDlg();
	}

}

void ClientDownloadManager::ScheduleUpload(QString strFilePath, bool bIsBackup, bool bOverWrite)
{
	CheckDlg();

	//not possible in thick client
	BusinessServiceManager_ThinClient *pBo = dynamic_cast<BusinessServiceManager_ThinClient*>(g_pBoSet);
	if (pBo==NULL)
		return;

	DownloadWidget *pWidget=m_Dlg->AddFileForUpload(strFilePath);
	UserStorageHTTPClientThread *client = new UserStorageHTTPClientThread;
	m_hshActiveDownloadThreads[pWidget->m_nFileID]=client;
	m_hshActiveDownloadWidgets[pWidget->m_nFileID]=pWidget;

	if (bIsBackup)
		pWidget->m_bIsBackup=true;
	if (bOverWrite)
		pWidget->m_bOverWrite=true;


	HTTPClientConnectionSettings settings;
	Authenticator auth;
	pBo->GetSettings(settings,auth);
	client->StartThread(settings,auth);

	connect(client,SIGNAL(SocketOperationInProgress(int,qint64,qint64)),pWidget,SLOT(OnCommunicationInProgress(int,qint64,qint64)));
	connect(client,SIGNAL(SignalOperationEnded(int,QString)),pWidget,SLOT(OnOperationEnded(int,QString)));

	connect(pWidget,SIGNAL(SignalOpenFile(int)),this,SLOT(OnOpenFile(int)),Qt::QueuedConnection);
	connect(pWidget,SIGNAL(SignalOpenDirectory(int)),this,SLOT(OnOpenDirectory(int)),Qt::QueuedConnection);
	connect(pWidget,SIGNAL(SignalRemoveFile(int)),this,SLOT(OnRemoveFile(int)),Qt::QueuedConnection);
	connect(pWidget,SIGNAL(SignalCancel(int)),this,SLOT(OnCancel(int)),Qt::QueuedConnection);
	connect(pWidget,SIGNAL(SignalOperationEnd(int,int,QString)),this,SLOT(OnOperationEnd(int,int,QString)),Qt::QueuedConnection);

	ExecuteNextInQue();

	if (!m_Dlg->isVisible())
	{
		ActivateAndPositionDlg();
	}

}
void ClientDownloadManager::ScheduleCheckOutDocument(int nDocumentID,QString strFilePath,bool bReadOnly, bool bAskForLocation,bool bSkipLock,bool bDoNotTrackCheckOutDoc,QString strDefaultDirPath, int nRevisionID)
{
	CheckDlg();

	//resolve name & size
	Status err;
	int nSize=0;
	strFilePath=DocumentHelper::ResolvePathBeforeCheckOutDocument(err,nDocumentID,strFilePath,nSize,bAskForLocation,strDefaultDirPath,nRevisionID);
	_CHK_ERR(err);
	if (strFilePath.isEmpty()) //user abort
		return;

	DownloadWidget *pWidget=m_Dlg->AddFileForDownLoad(strFilePath);
	ClientDocumentHandlerThread *client = new ClientDocumentHandlerThread;
	m_hshActiveDownloadThreads[pWidget->m_nFileID]=client;
	m_hshActiveDownloadWidgets[pWidget->m_nFileID]=pWidget;
	pWidget->m_bIsCheckInOut=true;
	client->StartThread();
	if (nSize<MIN_FILE_SIZE_FOR_HTTP_TRANSFER)
		client->EnableHTTPUserStorageMode(false);
	else
		client->EnableHTTPUserStorageMode(true);

	pWidget->m_nDocumentID=nDocumentID;
	pWidget->m_bReadOnly=bReadOnly;
	pWidget->m_bSkipLock=bSkipLock;
	pWidget->m_bDoNotTrackCheckOutDoc=bDoNotTrackCheckOutDoc;
	pWidget->m_nDocumentOriginalSize=nSize;
	pWidget->m_nRevisionID=nRevisionID;

	connect(client,SIGNAL(SocketOperationInProgress(int,qint64,qint64)),pWidget,SLOT(OnCommunicationInProgress(int,qint64,qint64)));
	connect(client,SIGNAL(SignalOperationEnded(int,QString)),pWidget,SLOT(OnOperationEnded(int,QString)));
	connect(pWidget,SIGNAL(SignalOpenFile(int)),this,SLOT(OnOpenFile(int)),Qt::QueuedConnection);
	connect(pWidget,SIGNAL(SignalOpenDirectory(int)),this,SLOT(OnOpenDirectory(int)),Qt::QueuedConnection);
	connect(pWidget,SIGNAL(SignalRemoveFile(int)),this,SLOT(OnRemoveFile(int)),Qt::QueuedConnection);
	connect(pWidget,SIGNAL(SignalCancel(int)),this,SLOT(OnCancel(int)),Qt::QueuedConnection);
	connect(pWidget,SIGNAL(SignalOperationEnd(int,int,QString)),this,SLOT(OnOperationEnd(int,int,QString)),Qt::QueuedConnection);

	ExecuteNextInQue();

	if (!m_Dlg->isVisible())
	{
		ActivateAndPositionDlg();
	}
}
void ClientDownloadManager::ScheduleCheckInDocument(int nDocumentID,QString strFilePath,QString strTag,bool bOverWrite,bool bSkipLock, int nAskForTag, bool bDeleteDocumentWhenCancel,int nOpenDocumentAfterUpload_FUIMode)
{
	//check file before check in, if does not exist, offer to discard check in, if user manually find new location, strFilePath will contain new value
	if(!DocumentHelper::CheckFileBeforeCheckInDocument(nDocumentID,strFilePath,bSkipLock))
		return;

	//file must exist: if >1mb, chunked, else all in row...
	QFileInfo infoFile(strFilePath);
	if (!infoFile.exists())
		 return;
	bool bSkipLoadContent=true;
	int nSize=infoFile.size();
	if (nSize<MIN_FILE_SIZE_FOR_HTTP_TRANSFER)
		bSkipLoadContent=false;

	CheckDlg();

	DbRecordSet recInsertedDocumentRevision;
	if(!DocumentHelper::ResolveRevisionBeforeCheckInDocument(nDocumentID,strFilePath,nSize,recInsertedDocumentRevision,strTag,bSkipLock,nAskForTag,bSkipLoadContent))
		return;

	DownloadWidget *pWidget=m_Dlg->AddFileForUpload(strFilePath);
	ClientDocumentHandlerThread *client = new ClientDocumentHandlerThread;
	m_hshActiveDownloadThreads[pWidget->m_nFileID]=client;
	m_hshActiveDownloadWidgets[pWidget->m_nFileID]=pWidget;
	pWidget->m_bIsCheckInOut=true;
	client->StartThread();
	if (nSize<MIN_FILE_SIZE_FOR_HTTP_TRANSFER)
		client->EnableHTTPUserStorageMode(false);
	else
		client->EnableHTTPUserStorageMode(true);

	//_DUMP(recInsertedDocumentRevision)
	client->SetDocumentRevision(recInsertedDocumentRevision);

	pWidget->m_nDocumentID=nDocumentID;
	pWidget->m_bSkipLock=bSkipLock;
	pWidget->m_bOverWrite=bOverWrite;
	pWidget->m_bDeleteDocumentWhenCancel=bDeleteDocumentWhenCancel;

	connect(client,SIGNAL(SocketOperationInProgress(int,qint64,qint64)),pWidget,SLOT(OnCommunicationInProgress(int,qint64,qint64)));
	connect(client,SIGNAL(SignalOperationEnded(int,QString)),pWidget,SLOT(OnOperationEnded(int,QString)));
	connect(pWidget,SIGNAL(SignalOpenFile(int)),this,SLOT(OnOpenFile(int)),Qt::QueuedConnection);
	connect(pWidget,SIGNAL(SignalOpenDirectory(int)),this,SLOT(OnOpenDirectory(int)),Qt::QueuedConnection);
	connect(pWidget,SIGNAL(SignalRemoveFile(int)),this,SLOT(OnRemoveFile(int)),Qt::QueuedConnection);
	connect(pWidget,SIGNAL(SignalCancel(int)),this,SLOT(OnCancel(int)),Qt::QueuedConnection);
	connect(pWidget,SIGNAL(SignalOperationEnd(int,int,QString)),this,SLOT(OnOperationEnd(int,int,QString)),Qt::QueuedConnection);

	ExecuteNextInQue();

	if (!m_Dlg->isVisible())
	{
		ActivateAndPositionDlg();
	}
}

void ClientDownloadManager::OnThemeChanged()
{
	if (m_Dlg)
		m_Dlg->OnThemeChanged();
}

void ClientDownloadManager::Initialize(int nMaxActiveOperations) //called after user login
{
	CheckDlg();
	AbortAllOperations();
	m_nMaxActiveOperations=nMaxActiveOperations;

}
void ClientDownloadManager::AbortAllOperations()
{
	CheckDlg();
	m_Dlg->hide();
	m_sysTray->hide();
	QMap<int,DownloadWidget*> temp=m_hshActiveDownloadWidgets; //make local copy as hash is gonna be deleted
	ActiveDownloadsIterator i(temp);
	while (i.hasNext()) 
	{
		i.next();
		OnCancel(i.key());
		OnRemoveFile(i.key());
	}
}

void ClientDownloadManager::ClearAll()
{
	CheckDlg();
	QMap<int,DownloadWidget*> temp=m_hshActiveDownloadWidgets; //make local copy as hash is gonna be deleted
	ActiveDownloadsIterator i(temp);
	while (i.hasNext()) 
	{
		i.next();
		if (i.value()->m_nStatus==DownloadWidget::STATUS_DOWNLOAD || i.value()->m_nStatus==DownloadWidget::STATUS_UPLOAD)
			continue;
		OnCancel(i.key());
		OnRemoveFile(i.key());
	}
}

void ClientDownloadManager::OnOpenFile(int nFileID)
{
	QString strFile=m_hshActiveDownloadWidgets.value(nFileID)->m_strFilePath;
	if (!QFile::exists(strFile))return;

	DocumentHelper::OpenDocumentInExternalApp_Shell(0,strFile,"");
}
void ClientDownloadManager::OnOpenDirectory(int nFileID)
{
	QString strFile=m_hshActiveDownloadWidgets.value(nFileID)->m_strFilePath;
	DocumentHelper::OpenDirectoryInExternalApp_Shell(strFile);
}

void ClientDownloadManager::OnRemoveFile(int nFileID)
{
	if (m_hshActiveDownloadWidgets.value(nFileID)->m_nStatus==DownloadWidget::STATUS_DOWNLOAD || m_hshActiveDownloadWidgets.value(nFileID)->m_nStatus==DownloadWidget::STATUS_UPLOAD)
		return;

	//remove from widget:
	m_Dlg->RemoveFile(m_hshActiveDownloadWidgets.value(nFileID));

	//should be delete but anyways:
	if (m_hshActiveDownloadThreads.contains(nFileID))
	{
		if (m_hshActiveDownloadWidgets.value(nFileID)->m_bIsCheckInOut)
		{
			ClientDocumentHandlerThread *pThread=dynamic_cast<ClientDocumentHandlerThread *>(m_hshActiveDownloadThreads.value(nFileID,NULL));
			if (pThread)
			{
				pThread->blockSignals(true);
				pThread->deleteLater();
			}
		}
		else
		{
			UserStorageHTTPClientThread *pThread=dynamic_cast<UserStorageHTTPClientThread *>(m_hshActiveDownloadThreads.value(nFileID,NULL));
			if (pThread)
			{
				pThread->blockSignals(true);
				pThread->deleteLater();
			}
		}

		m_hshActiveDownloadThreads.remove(nFileID);
	}
	
	m_hshActiveDownloadWidgets.remove(nFileID);
}
void ClientDownloadManager::OnCancel(int nFileID)
{
	DownloadWidget *pWidget=m_hshActiveDownloadWidgets.value(nFileID,NULL);
	if (pWidget==NULL)
		return;

	//only active process can be canceled
	if (!(pWidget->m_nStatus==DownloadWidget::STATUS_DOWNLOAD || pWidget->m_nStatus==DownloadWidget::STATUS_UPLOAD))
	{
		return;
	}

	pWidget->SetStatus(DownloadWidget::STATUS_CANCEL);
	if (pWidget->m_bIsCheckInOut)
	{
		ClientDocumentHandlerThread *pThread=dynamic_cast<ClientDocumentHandlerThread *>(m_hshActiveDownloadThreads.value(nFileID,NULL));
		if (pThread)
		{
			disconnect(pThread,SIGNAL(SocketOperationInProgress(int,qint64,qint64)),pWidget,SLOT(OnCommunicationInProgress(int,qint64,qint64)));
			disconnect(pThread,SIGNAL(SignalOperationEnded(int,QString)),pWidget,SLOT(OnOperationEnded(int,QString)));
			pThread->AbortOperation();
		}
	}
	else
	{
		UserStorageHTTPClientThread *pThread=dynamic_cast<UserStorageHTTPClientThread *>(m_hshActiveDownloadThreads.value(nFileID,NULL));
		if (pThread)
		{
			disconnect(pThread,SIGNAL(SocketOperationInProgress(int,qint64,qint64)),pWidget,SLOT(OnCommunicationInProgress(int,qint64,qint64)));
			disconnect(pThread,SIGNAL(SignalOperationEnded(int,QString)),pWidget,SLOT(OnOperationEnded(int,QString)));

			pThread->AbortOperation(); //will fire operationend...
		}
	}

	//Status err;
	//err.setError(StatusCodeSet::ERR_HTTP_USER_OPERATION_ABORTED);
	OnOperationEnd(nFileID,StatusCodeSet::ERR_HTTP_USER_OPERATION_ABORTED,"");
}

void ClientDownloadManager::OnOperationEnd(int nFileID, int nStatusID,QString strStatus)
{
	Status err;
	err.setError(nStatusID,strStatus);

	if(m_hshActiveDownloadWidgets.value(nFileID)->m_bIsCheckInOut)
		ProcessEndCheckInOut(nFileID,err);
	else
		ProcessEndFile(nFileID,err);

	ExecuteNextInQue();
}

void ClientDownloadManager::ProcessEndFile(int nFileID, Status err)
{
	QString strFile=m_hshActiveDownloadWidgets.value(nFileID)->m_strFilePath;
	emit OperationCompleted(err,strFile);

	//kill socket thread:
	UserStorageHTTPClientThread *pThread=dynamic_cast<UserStorageHTTPClientThread *>(m_hshActiveDownloadThreads.value(nFileID,NULL));
	if (pThread)
	{
		pThread->blockSignals(true);
		pThread->deleteLater();
	}
	m_hshActiveDownloadThreads.remove(nFileID);

	if (err.getErrorCode()==StatusCodeSet::ERR_HTTP_USER_OPERATION_ABORTED) //do nothing..already done...
		return;

	SysTrayNotifyUser(err,nFileID);
	
}


int ClientDownloadManager::GetCurrentActiveOperations()
{
	int nOps=0;
	ActiveDownloadsIterator i(m_hshActiveDownloadWidgets);
	while (i.hasNext()) 
	{
		i.next();
		if (i.value()->m_nStatus==DownloadWidget::STATUS_DOWNLOAD || i.value()->m_nStatus==DownloadWidget::STATUS_UPLOAD)
			nOps++;
	}
	//qDebug()<<"current active ops"<<nOps;
	return nOps;
}

void ClientDownloadManager::ExecuteNextInQue()
{
	//start next in que if allowed:
	if (m_nMaxActiveOperations>GetCurrentActiveOperations())
	{
		ActiveDownloadsIterator i(m_hshActiveDownloadWidgets);
		while (i.hasNext()) 
		{
			i.next();
			if (i.value()->m_nStatus==DownloadWidget::STATUS_WAITING)
			{
				if (!i.value()->m_bIsCheckInOut)
				{
					UserStorageHTTPClientThread *client=dynamic_cast<UserStorageHTTPClientThread *>(m_hshActiveDownloadThreads.value(i.key(),NULL));
					if (client)
					{
						if (i.value()->m_bIsDownLoadOperation)
						{
							if (i.value()->m_bIsBackup)
								client->ExecDownLoadBackup(i.value()->m_strFilePath,i.value()->m_bOverWrite);
							else
								client->ExecDownLoadFile(i.value()->m_strFilePath,i.value()->m_bOverWrite);

							i.value()->SetStatus(DownloadWidget::STATUS_DOWNLOAD);
						}
						else
						{
							if (i.value()->m_bIsBackup)
								client->ExecUpLoadBackup(i.value()->m_strFilePath,i.value()->m_bOverWrite);
							else
								client->ExecUpLoadFile(i.value()->m_strFilePath,i.value()->m_bOverWrite);
							i.value()->SetStatus(DownloadWidget::STATUS_UPLOAD);
						}
					}
				}
				else
				{
					ClientDocumentHandlerThread *client=dynamic_cast<ClientDocumentHandlerThread *>(m_hshActiveDownloadThreads.value(i.key(),NULL));
					if (client)
					{
						if (i.value()->m_bIsDownLoadOperation)
						{
							client->ExecCheckOutDocument(i.value()->m_nDocumentID,i.value()->m_strFilePath,i.value()->m_bReadOnly,i.value()->m_bSkipLock,i.value()->m_nRevisionID);
							i.value()->SetStatus(DownloadWidget::STATUS_DOWNLOAD);
						}
						else
						{
							client->ExecCheckInDocument(i.value()->m_nDocumentID,i.value()->m_strFilePath,i.value()->m_bOverWrite,i.value()->m_bSkipLock,i.value()->m_strTag);
							i.value()->SetStatus(DownloadWidget::STATUS_UPLOAD);
						}
					}
				}
			}

			if (GetCurrentActiveOperations()>=m_nMaxActiveOperations)
			{
				return;
			}
		}
	}
}

void ClientDownloadManager::ActivateAndPositionDlg()
{
	QRect desktopRec=QApplication::desktop()->availableGeometry(QApplication::desktop()->screenNumber(m_Dlg));

	int nY=desktopRec.y()+desktopRec.height()-m_Dlg->height()-40;
	int nX=desktopRec.x()+desktopRec.width()-m_Dlg->width()-20;

	m_Dlg->move(nX,nY);
	m_Dlg->show();
	m_Dlg->activateWindow();

}
void ClientDownloadManager::OnShowDlg()
{
	if(m_Dlg)
		ActivateAndPositionDlg();

}
void ClientDownloadManager::OnHideDlg()
{
	if(m_Dlg)
		m_Dlg->hide();
}

void ClientDownloadManager::CheckDlg()
{
	if (m_Dlg==NULL)
	{
		m_Dlg = new Dlg_DownloadManager;
		m_Dlg->hide();
		m_Dlg->setWindowFlags(Qt::Window); //| Qt::WindowStaysOnTopHint);
		connect(m_Dlg,SIGNAL(ClearAll()),this,SLOT(ClearAll()));
		m_sysTray = new QSystemTrayIcon;
		m_sysTray->hide();

		QMenu *CnxtMenu = new QMenu;
		QAction* pAction = new QAction(tr("Show Current Download/Uploads Manager"), m_sysTray);
		connect(pAction, SIGNAL(triggered()), this, SLOT(OnShowDlg()));
		CnxtMenu->addAction(pAction);
		pAction = new QAction(tr("Hide Current Download/Uploads"), m_sysTray);
		connect(pAction, SIGNAL(triggered()), this, SLOT(OnHideDlg()));
		CnxtMenu->addAction(pAction);

		m_sysTray->setContextMenu(CnxtMenu);
		m_sysTray->setIcon(QIcon(":Earth16.png"));

		connect(m_sysTray,SIGNAL(messageClicked()),this,SLOT(OnSysMessageClicked()));
		connect(m_sysTray,SIGNAL(activated ( QSystemTrayIcon::ActivationReason)),this,SLOT(OnSysMessageActivate(QSystemTrayIcon::ActivationReason)));
	}
}

void ClientDownloadManager::OnSysMessageClicked()
{
	OnShowDlg();
	m_sysTray->hide();
}

void ClientDownloadManager::OnSysMessageActivate(QSystemTrayIcon::ActivationReason)
{
	OnSysMessageClicked();
}

void ClientDownloadManager::ProcessEndCheckInOut(int nFileID, Status err)
{
	QString strFile=m_hshActiveDownloadWidgets.value(nFileID)->m_strFilePath;
	bool	bCheckOut=m_hshActiveDownloadWidgets.value(nFileID)->m_bIsDownLoadOperation;
	int		nDocumentID=m_hshActiveDownloadWidgets.value(nFileID)->m_nDocumentID;
	bool	bReadOnly=m_hshActiveDownloadWidgets.value(nFileID)->m_bReadOnly;
	bool	bDoNotTrackCheckOutDoc=m_hshActiveDownloadWidgets.value(nFileID)->m_bDoNotTrackCheckOutDoc;
	bool	bSkipLock=m_hshActiveDownloadWidgets.value(nFileID)->m_bSkipLock;
	bool	bDeleteDocumentWhenCancel=m_hshActiveDownloadWidgets.value(nFileID)->m_bDeleteDocumentWhenCancel;
	int		nOriginalSize=m_hshActiveDownloadWidgets.value(nFileID)->m_nDocumentOriginalSize;

	DbRecordSet recRev,recInfo;
	bool bHTTPUserStorageModeEnabled=false;

	ClientDocumentHandlerThread *client=dynamic_cast<ClientDocumentHandlerThread *>(m_hshActiveDownloadThreads.value(nFileID,NULL));
	if (client)
	{
		client->GetLastDocumentInfo(recInfo);
		client->GetDocumentRevision(recRev);
		bHTTPUserStorageModeEnabled=client->IsHTTPUserStorageModeEnabled();
		client->blockSignals(true);
		client->deleteLater();
	}

	m_hshActiveDownloadThreads.remove(nFileID);

	if (bCheckOut)
	{
		//A: document is in recRev (bHTTPUserStorageModeEnabled=false) or saved onto disk..as is: content is either compressed or not
		if (!err.IsOK())
		{
			ErrorHandlerOnCheckOut(err,nFileID,nDocumentID,strFile,bReadOnly,bSkipLock,recRev,recInfo);
			return;
		}
		//issue 1436:
		//load apps, find right one, if flag is set then set file as read only
		bool bSetReadOnlyFile=false;
		if(bReadOnly) //only if read only, set flag only if document have set read only flag on check out copy
		{
			DbRecordSet lstApps;
			DocumentHelper::GetDocApplications(lstApps);
			int nAppID=recInfo.getDataRef(0,"BDMD_APPLICATION_ID").toInt();
			int nAppRow=lstApps.find("BDMA_ID",nAppID,true);
			if (nAppRow>=0)
			{
				if (lstApps.getDataRef(nAppRow,"BDMA_SET_READ_ONLY_FLAG").toInt()>0)
					bSetReadOnlyFile=true;
			}
		}
		//decompress if needed:
		if (!bHTTPUserStorageModeEnabled)
		{
			//recRev.Dump();
			if (recRev.getRowCount()>0)
			{
				//save content to desired location, if fails, revert
				if(!DocumentHelper::SaveFileContent(recRev.getDataRef(0,"BDMR_CONTENT").toByteArray(),strFile,recRev.getDataRef(0,"BDMR_IS_COMPRESSED").toInt(),bSetReadOnlyFile))
				{
					err.setError(1,QString(tr("Failed to write document %1")).arg(strFile));
					m_hshActiveDownloadWidgets.value(nFileID)->m_LastError=err;
					ErrorHandlerOnCheckOut(err,nFileID,nDocumentID,strFile,bReadOnly,bSkipLock,recRev,recInfo);				
					return;
				}
			}
			else //no revision:
			{
				err.setError(StatusCodeSet::ERR_BUS_DOC_REVISION_NOT_FOUND);
				m_hshActiveDownloadWidgets.value(nFileID)->m_LastError=err;
				ErrorHandlerOnCheckOut(err,nFileID,nDocumentID,strFile,bReadOnly,bSkipLock,recRev,recInfo);				
				return;
			}
	
		}
		else //file is already saved: to overcome from old system to new we must reload file in memory, decompress, save back...
		{
			/*
			if (recRev.getDataRef(0,"BDMR_IS_COMPRESSED").toInt())
			{
				//read all from strFile, compress, remove file, save back from memory...
				QFile file(strFile);
				if(!file.open(QIODevice::ReadOnly)) //lock acess
				{
					err.setError(1,QString(tr("Failed to write document %1")).arg(strFile));
					m_hshActiveDownloadWidgets.value(nFileID)->m_LastError=err;
					ErrorHandlerOnCheckOut(err,nFileID,nDocumentID,strFile,bReadOnly,bSkipLock,recRev,recInfo,true); //skip file delete---prob in use or else..!!!
					return;
				}
				QByteArray byteFileContent=file.readAll();
				byteFileContent=qUncompress(byteFileContent);
				file.close();
				if(!file.open(QIODevice::WriteOnly)) //lock acess
				{
					err.setError(1,QString(tr("Failed to write document %1")).arg(strFile));
					m_hshActiveDownloadWidgets.value(nFileID)->m_LastError=err;
					ErrorHandlerOnCheckOut(err,nFileID,nDocumentID,strFile,bReadOnly,bSkipLock,recRev,recInfo);
					return;
				}
				int nErr=file.write(byteFileContent);
				if (nErr==-1)
				{
					file.close();
					err.setError(1,QString(tr("Failed to write document %1")).arg(strFile));
					m_hshActiveDownloadWidgets.value(nFileID)->m_LastError=err;
					ErrorHandlerOnCheckOut(err,nFileID,nDocumentID,strFile,bReadOnly,bSkipLock,recRev,recInfo);
					return;
				}
				if (bSetReadOnlyFile)
				{
					file.setPermissions(QFile::ReadOwner | QFile::ExeOwner | QFile::ReadUser | QFile::ExeUser | QFile::ReadGroup | QFile::ExeGroup | QFile::ReadOther | QFile::ExeOther);
				}
				file.close();
			}
			else 
			*/
			if (bSetReadOnlyFile)
			{
				//just open file and set permissions:
				QFile file(strFile);
				if(!file.open(QIODevice::Append)) //lock acess
				{
					err.setError(1,QString(tr("Failed to write document %1")).arg(strFile));
					m_hshActiveDownloadWidgets.value(nFileID)->m_LastError=err;
					ErrorHandlerOnCheckOut(err,nFileID,nDocumentID,strFile,bReadOnly,bSkipLock,recRev,recInfo);
					return;
				}
				file.setPermissions(QFile::ReadOwner | QFile::ExeOwner | QFile::ReadUser | QFile::ExeUser | QFile::ReadGroup | QFile::ExeGroup | QFile::ReadOther | QFile::ExeOther);
				file.close();
			}
		}

		//CRC CHECKSUM:
		QFileInfo infoFile(strFile);
		if (infoFile.size()!=nOriginalSize && nOriginalSize>0)
		{
			err.setError(1,QString(tr("Failed to download document %1. CRC checksum failed.")).arg(strFile));
			m_hshActiveDownloadWidgets.value(nFileID)->m_LastError=err;
			ErrorHandlerOnCheckOut(err,nFileID,nDocumentID,strFile,bReadOnly,bSkipLock,recRev,recInfo);
			return;
		}
		//notify others:
		DbRecordSet lstDummy;
		DocumentHelper::GetCheckedOutDocs(lstDummy,true);
		if(!bReadOnly)
			g_ChangeManager.notifyObservers(ChangeManager::GLOBAL_CHECK_OUT_DOC,nDocumentID); 
		if (!bDoNotTrackCheckOutDoc)
			DocumentHelper::DWatcher_AddFilePath(nDocumentID,strFile);
	}
	else
	{
		//-----------------------------------------------
		//CHECK IN
		//-----------------------------------------------
		if (!err.IsOK() && bDeleteDocumentWhenCancel)
		{
			Status err;
			DocumentHelper::DeleteDocument(err,nDocumentID);
		}

		//A: document is in recRev (bHTTPUserStorageModeEnabled=false) or on d'disk..as is: content is either compressed or not, file is uploaded and chek in, test for err only.

		//check out //prepare for cancel:
		if (err.getErrorCode()==StatusCodeSet::ERR_HTTP_USER_OPERATION_ABORTED)
		{
			emit CheckInOutCompleted(err,nDocumentID,strFile,recRev,recInfo);
			return;
		}
		if (!err.IsOK())
		{
			emit CheckInOutCompleted(err,nDocumentID,strFile,recRev,recInfo);
			SysTrayNotifyUser(err,nFileID);
			return;
		}

		//notify others:
		DbRecordSet lstDummy;
		DocumentHelper::GetCheckedOutDocs(lstDummy,true);
		g_ChangeManager.notifyObservers(ChangeManager::GLOBAL_CHECK_IN_DOC,nDocumentID); 
		DocumentHelper::DWatcher_RemoveFilePath(nDocumentID);
	}


	emit CheckInOutCompleted(err,nDocumentID,strFile,recRev,recInfo);

	SysTrayNotifyUser(err,nFileID);
}


void ClientDownloadManager::SysTrayNotifyUser(Status err,int nFileID)
{
	m_sysTray->hide();
	if (err.getErrorCode()==StatusCodeSet::ERR_HTTP_USER_OPERATION_ABORTED) //already noted..
	{
		return;
	}

	if (err.IsOK())
	{
		m_hshActiveDownloadWidgets.value(nFileID)->SetStatus(DownloadWidget::STATUS_FINISHED);

		QString strMsg=tr("Upload of %1 complete");
		QString strTitle=tr("Upload complete");
		if (m_hshActiveDownloadWidgets.value(nFileID)->m_bIsDownLoadOperation)
		{
			strMsg=tr("Download of %1 complete");
			strTitle=tr("Download complete");
		}
		QFileInfo info(m_hshActiveDownloadWidgets.value(nFileID)->m_strFilePath);
		strMsg=strMsg.arg(info.fileName());
		m_sysTray->show();
		m_sysTray->setToolTip(strMsg);
		m_sysTray->showMessage(strTitle,strMsg);
		return;
	}
	
	QString strMsg=tr("Uploading of %1 failed: %2");
	QString strTitle=tr("Error Uploading File");
	if (m_hshActiveDownloadWidgets.value(nFileID)->m_bIsDownLoadOperation)
	{
		strMsg=tr("Downloading of %1 failed: %2");
		strTitle=tr("Error Downloading File");
	}

	m_hshActiveDownloadWidgets.value(nFileID)->SetStatus(DownloadWidget::STATUS_ERROR);

	QFileInfo info(m_hshActiveDownloadWidgets.value(nFileID)->m_strFilePath);
	strMsg=strMsg.arg(info.fileName()).arg(err.getErrorText());
	//QMessageBox::critical(NULL,strTitle,strMsg);
	m_sysTray->show();
	m_sysTray->setToolTip(strMsg);
	m_sysTray->showMessage(strTitle,strMsg,QSystemTrayIcon::Warning);
}

void ClientDownloadManager::ErrorHandlerOnCheckOut(Status err,int nFileID,int nDocumentID,QString strFile, bool bReadOnly,bool bSkipLock,DbRecordSet &recRev,DbRecordSet &recInfo, bool bSkipFileDelete)
{
	if (!bSkipFileDelete)
		QFile::remove(strFile);

	emit CheckInOutCompleted(err,nDocumentID,strFile,recRev,recInfo);
	SysTrayNotifyUser(err,nFileID);

	if (!bReadOnly)
	{
		Status status;
		_SERVER_CALL(BusDocuments->CancelCheckOut(status,nDocumentID,g_pClientManager->GetPersonID(),bSkipLock));
		_CHK_ERR_NO_RET(status);
	}
}