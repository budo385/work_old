#include "dlg_storagelocation.h"
#include "gui_core/gui_core/thememanager.h"
#include "gui_core/gui_core/gui_helper.h"


Dlg_StorageLocation::Dlg_StorageLocation(bool bStore,QString strDocName, QString strDocPath)
	: QDialog(0)
{
	ui.setupUi(this);
	setWindowTitle(tr("Document Management"));
	ui.txtName->setEnabled(false);
	ui.txtFileName->setEnabled(false);

	if (bStore)
	{
		this->setMaximumHeight(260);
		this->setMinimumHeight(260);
		resize(width(),260);
		
		ui.labelName->setVisible(false);
		ui.labelFile->setVisible(false);
		ui.txtName->setVisible(false);
		ui.txtFileName->setVisible(false);

		ui.txtTitle->setText(QString("<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\np, li { white-space: pre-wrap; }\n</style></head><body style=\" font-family:'Arial'; font-size:14pt; color:white; font-weight:bold; font-style:normal;\">\n<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">%1</p></body></html>").arg(tr("Choose Document Storage Location")));

		QString strTextButton= "<html><body style=\" font-family:Arial; text-decoration:none;\">\
							   <p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt; font-weight:800; font-style:normal; color:%1;\">%2</span></p>\
							   </body></html>";
		GUI_Helper::CreateStyledButton(ui.btnLocal,":Icon_Store_Local_Ref.png",strTextButton.arg("white").arg(tr("Save Reference(s) to Local File(s)")),10, ThemeManager::GetCEMenuButtonStyle(),10,false,48,48);
		GUI_Helper::CreateStyledButton(ui.btnInternet,":Icon_Store_Internet_DB.png",strTextButton.arg("white").arg(tr("Store File Centrally on Internet")),10, ThemeManager::GetCEMenuButtonStyle(),10,false,48,48);
		GUI_Helper::CreateStyledButton(ui.btnCancel,"",strTextButton.arg("white").arg(tr("Cancel")),0, ThemeManager::GetCEMenuButtonStyle(),0);
		ui.btnLocal->setToolTip(tr("Save a reference to a local file. The file itself keeps being stored locally where it is (on your computer or on your local server). A double-click on this file name in Communicator will open the original file. Local files can only be used inside your own network (LAN) and are not accessible from other locations."));
		ui.btnInternet->setToolTip(tr("Save a complete file in the central database of SOKRATES(R) Communicator, on the SOKRATES (R) Application Server. These files are available everywhere, even from other location over the internet."));
		ui.btnLocal->setWhatsThis(tr("Save a reference to a local file. The file itself keeps being stored locally where it is (on your computer or on your local server). A double-click on this file name in Communicator will open the original file. Local files can only be used inside your own network (LAN) and are not accessible from other locations."));
		ui.btnInternet->setWhatsThis(tr("Save a complete file in the central database of SOKRATES(R) Communicator, on the SOKRATES (R) Application Server. These files are available everywhere, even from other location over the internet."));
	}
	else
	{
		ui.txtName->setText(strDocName);
		QFileInfo info(strDocPath);
		ui.txtFileName->setText(info.fileName());
		ui.txtTitle->setText(QString("<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\np, li { white-space: pre-wrap; }\n</style></head><body style=\" font-family:'Arial'; font-size:14pt; color:white; font-weight:bold; font-style:normal;\">\n<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">%1</p></body></html>").arg(tr("Open Document")));

		QString strTextButton= "<html><body style=\" font-family:Arial; text-decoration:none;\">\
							   <p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt; font-weight:800; font-style:normal; color:%1;\">%2</span></p>\
							   </body></html>";
		GUI_Helper::CreateStyledButton(ui.btnLocal,":Icon_Checkout_Write.png",strTextButton.arg("white").arg(tr("Load Original Document And Modify")),10, ThemeManager::GetCEMenuButtonStyle(),10);
		GUI_Helper::CreateStyledButton(ui.btnInternet,":Icon_Checkout_Read.png",strTextButton.arg("white").arg(tr("Load Document Copy to View Only")),10, ThemeManager::GetCEMenuButtonStyle(),10);
		GUI_Helper::CreateStyledButton(ui.btnCancel,"",strTextButton.arg("white").arg(tr("Cancel")),0, ThemeManager::GetCEMenuButtonStyle(),0);

		
		ui.btnLocal->setToolTip(tr("Check out document"));
		ui.btnInternet->setToolTip(tr("Check out document as read only"));
		ui.btnLocal->setWhatsThis(tr("Check out document"));
		ui.btnInternet->setWhatsThis(tr("Check out document as read only"));
	
	}

	setStyleSheet("QDialog " + ThemeManager::GetMobileBkg() + ThemeManager::GetGlobalWidgetStyle());
}
Dlg_StorageLocation::~Dlg_StorageLocation()
{


}


void Dlg_StorageLocation::on_btnLocal_clicked()
{
	done(1);
}
void Dlg_StorageLocation::on_btnInternet_clicked()
{
	done(2);
}
void Dlg_StorageLocation::on_btnCancel_clicked()
{
	done(0);
}

