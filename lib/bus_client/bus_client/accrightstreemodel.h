#ifndef ACCRIGHTSTREEMODEL_H
#define ACCRIGHTSTREEMODEL_H

#include "gui_core/gui_core/dbrecordsetmodel.h"


/*!
	\class  AccRightsTreeModel
	\ingroup GUICore_ModelClasses
	\brief  RecordSet model. 

	Access rights tree model class.
*/
class AccRightsTreeModel : public DbRecordSetModel
{
public:
    AccRightsTreeModel(QObject *Parent = NULL);
    ~AccRightsTreeModel();
	virtual void AddDataToModel(DbRecordSet &PersonRolesRecordSet);
	void AddNewPerson(QString PersonName, QString PersonLastName, int PersonUserID);
	void AddNewUser(QString UserName, QString UserLastName);

	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
	Qt::ItemFlags flags(const QModelIndex &index) const;
	bool setData (const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);
	Qt::DropActions supportedDropActions() const;
	bool insertRows(int position, int rows, const QModelIndex &index = QModelIndex());
	bool removeRows(int position, int rows, const QModelIndex &index = QModelIndex());
	bool dropMimeData(const QMimeData * data, Qt::DropAction action, int row, 
						int column, const QModelIndex & parent);
	QStringList mimeTypes() const;

	//Access methods.
	void SetPersonFirstName(const QString &FirstName);
	void SetPersonLastName(const QString &LastName);
	void SetPersonUserID(const int nUserID);

protected:
	//Name and Last name.
	QString		m_strFirstName;		//< First name (user, person)
	QString		m_strLastName;		//< Last name (user, person)
	int			m_nPersonUserID;	//< Person user id.

	virtual void SetupModelData(DbRecordSetItem *Parent = NULL);
};

#endif // ACCRIGHTSTREEMODEL_H
