#ifndef ROLEDEFTREEMODEL_H
#define ROLEDEFTREEMODEL_H

#include <QHashIterator>
#include "common/common/status.h"
#include "gui_core/gui_core/dbrecordsetmodel.h"


/*!
	\class  RoleDefTreeModel
	\ingroup GUICore_ModelClasses
	\brief  Role definition tree model. 

	Model class for role definition, subclasses DbRecordSetModel.
*/
class RoleDefTreeModel : public DbRecordSetModel
{
public:
    RoleDefTreeModel(QObject *parent = NULL);
    ~RoleDefTreeModel();
	void InitializeModel(Status &pStatus);
	void AddDataToModel(int RoleID);

	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
	Qt::ItemFlags flags(const QModelIndex &index) const;
	bool setData (const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);
	Qt::DropActions supportedDropActions() const;
	bool insertRows(int position, int rows, const QModelIndex &index = QModelIndex());
	bool removeRows(int position, int rows, const QModelIndex &index = QModelIndex());
	bool dropMimeData(const QMimeData * data, Qt::DropAction action, int row, int column, const QModelIndex & parent);
	QStringList mimeTypes() const;
	void AddNewRole(QString RoleName, QString RoleDescription, int RoleType);

	//Access methods.
	void SetRoleName(const QString &RoleName);
	void SetRoleDescription(const QString &RoleDescription);
	void SetRoleType(const int nRoleType);

private:
	virtual void SetupModelData(DbRecordSetItem *Parent = NULL);

	QString		m_strRoleName;				//< Role name.
	QString		m_strRoleDescription;		//< Role description.
	int			m_nRoleType;				//< Role type (system, business).
};

#endif // ROLEDEFTREEMODEL_H

