#include "accuserrecordselector.h"
#include "dlg_accuserrecord.h"
#include "db_core/db_core/dbsqltableview.h"
#include "bus_core/bus_core/accuarcore.h"

#include "bus_client/bus_client/clientmanagerext.h"
extern ClientManagerExt *g_pClientManager;
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;
#include "bus_client/bus_client/modulelicenseclientcache.h"
extern ModuleLicenseClientCache g_FunctionPoint;

AccUserRecordSelector::AccUserRecordSelector(QWidget *parent)
	: QWidget(parent),m_bEdit(true)
{
	ui.setupUi(this);

	ui.comboBox->blockSignals(true);
	ui.comboBox->addItem(tr("Private"));
	ui.comboBox->addItem(tr("Others can Read"));
	ui.comboBox->addItem(tr("Others can Modify"));
	ui.comboBox->addItem(tr("Individual Rights"));
	ui.comboBox->blockSignals(false);
	ui.comboBox->setCurrentIndex(AccUARCore::ACC_OTHERS_CAN_MODIFY);

	m_nLoggedPersonID=g_pClientManager->GetPersonID();

	ui.btnUAR->setIcon(QIcon(":SAP_Modify.png"));
	ui.btnUAR->setToolTip(tr("Set Individual Access Rights"));

	m_lstUAR.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CORE_ACC_USER_REC));
	m_lstGAR.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CORE_ACC_GROUP_REC));
	Invalidate();

	//check FP:
	if(!g_FunctionPoint.IsFPAvailable(FP_PROFESSIONAL_ACCESS_RIGHTS)){
		if(!g_FunctionPoint.IsFPAvailable(FP_LIGHT_ACCESS_RIGHTS))
			this->setVisible(false);
		else
			ui.btnUAR->setVisible(false);
	}
	

}

AccUserRecordSelector::~AccUserRecordSelector()
{

}

void AccUserRecordSelector::SetAccessDisplayLevel(int nAccDisplayLevel)
{
	ui.comboBox->setCurrentIndex(nAccDisplayLevel);
}

void AccUserRecordSelector::HideLabel()
{
	ui.label->setVisible(false);
}

void AccUserRecordSelector::Load(int nRecordId,int nTableID,DbRecordSet *lstUAR,DbRecordSet *lstGAR)
{
	if (lstUAR == NULL || lstGAR == NULL )
	{
		Status err;
		_SERVER_CALL(AccessRights->ReadGUAR(err,nRecordId,nTableID,m_lstUAR,m_lstGAR))
		_CHK_ERR(err);
	}
	else
	{
		m_lstUAR=*lstUAR;
		m_lstGAR=*lstGAR;
	}

	m_nRecordId=nRecordId;
	m_nTableID=nTableID;
	m_nRecordIdCache=nRecordId;
	m_nTableIDCache=nTableID;
	m_lstUARCache=m_lstUAR;
	m_lstGARCache=m_lstGAR;

	int nIndex=AccUARCore::Record2DisplayLevel(m_nLoggedPersonID,m_lstUAR,m_lstGAR);
	ui.comboBox->setCurrentIndex(nIndex);
	if (!AccUARCore::IsUserAllowedToModifyUAR(m_nLoggedPersonID,m_lstUAR,m_lstGAR))
		ui.comboBox->setEnabled(false);
	else
		ui.comboBox->setEnabled(true);
}
//save data or just return records 
void AccUserRecordSelector::Save(int nRecordId,int nTableID,DbRecordSet *lstUAR,DbRecordSet *lstGAR)
{
	Q_ASSERT(nTableID!=-1);
	AccUARCore::DisplayLevel2Record(ui.comboBox->currentIndex(),m_nLoggedPersonID,nRecordId,nTableID,m_lstUAR,m_lstGAR);
	if (lstUAR == NULL || lstGAR == NULL )
	{
		Status err;
		_SERVER_CALL(AccessRights->WriteGUAR(err,nRecordId,nTableID,m_lstUAR,m_lstGAR))
		_CHK_ERR(err);
	}
	else
	{
		*lstUAR=m_lstUAR;
		*lstGAR=m_lstGAR;
	}

	//store data from last save:
	m_nRecordId=nRecordId;
	m_nTableID=nTableID;
	m_nRecordIdCache=nRecordId;
	m_nTableIDCache=nTableID;
	m_lstUARCache=m_lstUAR;
	m_lstGARCache=m_lstGAR;
}
void AccUserRecordSelector::Invalidate()
{
	m_nRecordId=-1;
	m_nTableID=-1;
	m_lstUAR.clear();
	m_lstGAR.clear();
	ui.comboBox->setCurrentIndex(AccUARCore::ACC_OTHERS_CAN_MODIFY);
	m_strRecordDisplayName.clear();
}

//revert to last load:
void AccUserRecordSelector::CancelChanges()
{
	m_nRecordId=m_nRecordIdCache;
	m_nTableID=m_nTableIDCache;
	Q_ASSERT(m_nTableID!=-1);
	m_lstUAR=m_lstUARCache;
	m_lstGAR=m_lstGARCache;
	int nIndex=AccUARCore::Record2DisplayLevel(m_nLoggedPersonID,m_lstUAR,m_lstGAR);
	ui.comboBox->setCurrentIndex(nIndex);
	m_strRecordDisplayName.clear();
}

void AccUserRecordSelector::on_comboBox_currentIndexChanged(int nIndex)
{
	if (nIndex!=AccUARCore::ACC_INDIVIDUAL)
		ui.btnUAR->setEnabled(false);
	else
		ui.btnUAR->setEnabled(true);
}
void AccUserRecordSelector::on_btnUAR_clicked()
{
	emit UpdateRecordDisplayName();
	Dlg_AccUserRecord Dlg;
	Status err;
	bool bReadOnly=!AccUARCore::IsUserAllowedToModifyUAR(m_nLoggedPersonID,m_lstUAR,m_lstGAR); //if not allowed to edit, set readonly=true;
	if (!m_bEdit)
		bReadOnly=true;

	Dlg.Initialize(err,bReadOnly,m_strRecordDisplayName,m_nLoggedPersonID,m_nRecordId,m_nTableID,&m_lstUAR,&m_lstGAR);
	_CHK_ERR(err);
	
	if(Dlg.exec())
		Dlg.GetResult(m_lstUAR,m_lstGAR);
}



void AccUserRecordSelector::SetEditMode(bool bEdit)
{
	m_bEdit=bEdit;
	ui.comboBox->setEnabled(bEdit);
}