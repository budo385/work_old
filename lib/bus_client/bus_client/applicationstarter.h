#ifndef  WINCE
#ifndef APPLICATIONSTARTER_H
#define APPLICATIONSTARTER_H


#include <QString>
#include <QObject>
#include "clientinifile.h"
#include "common/common/status.h"




/*!
	\class ApplicationStarter
	\brief StartUp object, inits global object based on client mode (thick/thin)
	\ingroup Bus_Client

	Based on thick or thin mode it will create global Business service, Thick or Thin client.
	Mode is defined in the common/common/config.h
*/

class ApplicationStarter : public QObject
{
        Q_OBJECT
public:
	ApplicationStarter(QObject *parent,bool bConsoleApp=false);
	~ApplicationStarter();

	void Go(Status &pStatus);
	void ExitApplication(Status &pStatus);

private:
	void LoadSettings(Status &pStatus, int &bIsThinClient,ClientIniFile &INIFile,QString &strIniFilePath);
	bool m_bConsoleApp;
};




#endif //APPLICATIONSTARTER_H

#endif //WINCE



