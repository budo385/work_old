#ifndef ACCRIGHTSSETLISTMODEL_H
#define ACCRIGHTSSETLISTMODEL_H

#include <QAbstractListModel>
#include <Qt>
#include <QtGlobal>
#include <QIcon>
#include <QColor>
#include <QFont>
#include <QPainter>
#include <QImageReader>
#include <QPersistentModelIndex>

#include "common/common/status.h"
#include "common/common/dbrecordset.h"
#include "gui_core/gui_core/dbrecordsetlistmodel.h"
#include "gui_core/gui_core/dbrecordsetitem.h"


/*!
	\class  AccRightsSetListModel
	\ingroup GUICore_ModelClasses
	\brief  RecordSet model. 

	Access rights list model class.
*/
class AccRightsSetListModel : public DbRecordSetListModel
{
public:
    AccRightsSetListModel(QObject *parent = NULL);
    ~AccRightsSetListModel();
	void InitializeModel(Status &pStatus);

	Qt::ItemFlags flags(const QModelIndex &index) const;
	Qt::DropActions supportedDropActions() const;
	QStringList mimeTypes() const;

private:
	void SetupModelData(DbRecordSetItem *Parent = NULL);
    
};

#endif // ACCRIGHTSSETLISTMODEL_H
