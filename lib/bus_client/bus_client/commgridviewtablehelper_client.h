#ifndef COMMGRIDVIEWTABLEHELPER_CLIENT_H
#define COMMGRIDVIEWTABLEHELPER_CLIENT_H

#include <QObject>
#include "bus_core/bus_core/commgridviewtablehelper.h"

class CommGridViewTableHelper_Client : public CommGridViewTableHelper
{
	Q_OBJECT

public:
	CommGridViewTableHelper_Client(QObject *parent=0);
	~CommGridViewTableHelper_Client();

	int GetLoggedPersonID();
	QString GetLoggedPersonInitials();
	void InitializeHelper();
private:
	
};

#endif // COMMGRIDVIEWTABLEHELPER_CLIENT_H
