#ifndef INTERFACE_CLIENTSIMPLEORM_H
#define INTERFACE_CLIENTSIMPLEORM_H


#include "common/common/status.h"
#include "common/common/dbrecordset.h"

/*!
\class Interface_ClientSimpleORM
\brief Client simple ORM.
\ingroup Bus_Interface_Collection
*/
class Interface_ClientSimpleORM
{
public:
	//virtual ~Interface_ClientSimpleORM(){};

	virtual void Write(Status &Ret_pStatus, int nTableID, DbRecordSet &RetOut_pLstForWrite, QString pLockResourceID, int nQueryView, int nSkipLastColumns, DbRecordSet &RetOut_pLstForDelete) = 0;
	virtual void WriteParentSubData(Status &Ret_pStatus, int nTableID, DbRecordSet &RetOut_pLstForWrite, int nParentId, QString strColParentName,QString pLockResourceID = "",int nQueryView = -1)=0;
	virtual void Read (Status &Ret_pStatus, int nTableID, DbRecordSet &Ret_pLstRead, QString strWhereClause = "", int nQueryView = -1, bool bDistinct = false) = 0;
	virtual void ReadAdv (Status &Ret_pStatus, DbRecordSet &RetOut_pLstRead, QString strJoinClause) = 0;
	virtual void ReadFromParentIDs(Status &Ret_pStatus, int nTableID,DbRecordSet &Ret_pLstRead, QString strColFK2ParentIDName, DbRecordSet &pLstOfID,QString strColIDName,QString strWhereClauseBefore="",QString strWhereClauseAfter="",int nQueryView=-1, bool bDistinct = false)= 0;
	virtual void Delete(Status &Ret_pStatus, int nTableID, DbRecordSet &pLstForDelete, QString pLockResourceID, DbRecordSet &Ret_pLstStatusRows, bool pBoolTransaction) = 0;
	virtual void Lock(Status &Ret_pStatus, int nTableID, DbRecordSet &pLstForLocking, QString &Ret_pResourceID, DbRecordSet &Ret_pLstStatusRows, bool bAllOrNothing = true) = 0;
	virtual void UnLock(Status &Ret_pStatus, int nTableID, bool &Ret_bUnLocked, QString &pResourceID) = 0;
	virtual void UnLockByRecordID(Status &Ret_pStatus, int nTableID, DbRecordSet &pLstForUnLocking, bool &Ret_bUnLocked) = 0;
	virtual void IsLocked(Status &Ret_pStatus, int nTableID, bool &Ret_bLocked, DbRecordSet &LockList) = 0;
	virtual void GetRowCount(Status &Ret_pStatus, int nTableID, QString strWhereClause, int &Ret_nCount) = 0;
	virtual void ExecuteSQL(Status &Ret_pStatus, QString strSQL, DbRecordSet &Ret_LstReturn)=0;
	virtual void WriteHierarchicalData(Status &Ret_pStatus, int nTableID,DbRecordSet &RetOut_pLstForWrite, QString pLockResourceID = "", QString strSQLWhereFilter="", int nQueryView = -1)=0;
	virtual void DeleteHierarchicalData(Status &Ret_pStatus, int nTableID,DbRecordSet &pLstForDelete, QString pLockResourceID = "", QString strSQLWhereFilter="")=0;

};

#endif // INTERFACE_CLIENTSIMPLEORM_H
