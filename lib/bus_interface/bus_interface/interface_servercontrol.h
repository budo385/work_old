#ifndef INTERFACE_SERVERCONTROL
#define INTERFACE_SERVERCONTROL


#include "common/common/status.h"
#include "common/common/dbrecordset.h"



/*!
	\class Interface_ServerControl
	\brief Interface for managing server (admin only)
	\ingroup Bus_Interface_Collection
*/

class Interface_ServerControl
{
public:
	virtual ~Interface_ServerControl(){};

	virtual void GetBackupData(Status &Ret_pStatus,int& Ret_nBackupFreq,QString& Ret_strBackupPath,QString& Ret_datBackupLastDate,QString& Ret_strRestoreOnNextStart,int& Ret_nBackupDay,QString& Ret_strBackupTime,int& Ret_nDeleteOldBackupsDay) = 0;
	virtual void SetBackupData(Status &Ret_pStatus,int nBackupFreq, QString strBackupPath, int nBackupDay,QString strBackupTime,int nDeleteOldBackupsDay) = 0;
	virtual void LoadBackupList(Status &Ret_pStatus,DbRecordSet &RetOut_Data)=0;
	virtual void Backup(Status &Ret_pStatus)=0;
	virtual void Restore(Status &Ret_pStatus,QString strRestoreFile)=0;

	virtual void LoadTemplateList(Status &Ret_pStatus,DbRecordSet &RetOut_Data)=0;
	virtual void RestoreFromTemplate(Status &Ret_pStatus,QString strRestoreFile)=0;
	virtual void SetTemplateData(Status &Ret_pStatus,QString strTemplateInUse,int nAskOnStartup, int nStartStartUpWizard)=0;
	virtual void GetTemplateData(Status &Ret_pStatus,QString &Ret_strTemplateInUse,int &Ret_nAskOnStartup,int &Ret_nStartStartUpWizard)=0;
	virtual void LoadBackupFile(Status &Ret_pStatus, QString strBackupName,QByteArray &Ret_bcpContent)=0;
	virtual void SaveBackupFile(Status &Ret_pStatus, QString strBackupName,QByteArray &Destroy_bcpContent)=0;
	virtual void DeleteBackupFile(Status &Ret_pStatus, QString strBackupName)=0;

	//import & export
	virtual void SaveImportExportSettings(Status &Ret_pStatus, DbRecordSet lstSettings)=0;
	virtual void LoadImportExportSettings(Status &Ret_pStatus, DbRecordSet &Ret_lstSettings)=0;
	virtual void StartProcessProject(Status &Ret_pStatus)=0; 
	virtual void StartProcessUser(Status &Ret_pStatus)=0; 
	virtual void StartProcessDebtor(Status &Ret_pStatus)=0; 
	virtual void StartProcessContact(Status &Ret_pStatus)=0; 
	virtual void StartProcessStoredProjectList(Status &Ret_pStatus)=0; 

	virtual void GetLog(Status &Ret_pStatus,QByteArray &Ret_logData,int &RetOut_nCurrentPosition,int &RetOut_nLogFileSizeOnStartOfChunkRead,int nChunkSize)=0;
	virtual void GetLogData(Status &Ret_pStatus, int &Ret_nLogLevel, int &Ret_nLogMaxSize, int &Ret_nBufferSize, int &Ret_nCurrentSize)=0;
	virtual void SetLogData(Status &Ret_pStatus, int nLogLevel, int nLogMaxSize, int nBufferSize)=0;

	virtual void GetAllActiveRemindersForUser(Status &Ret_pStatus,int nPersonID,DbRecordSet &Ret_Reminders)=0;
	virtual void DeleteReminders(Status &Ret_pStatus,DbRecordSet &lstReminders)=0;
	virtual void PostPoneReminders(Status &Ret_pStatus,DbRecordSet &lstReminders, int nMinutes)=0;

};



#endif //INTERFACE_SERVERCONTROL
