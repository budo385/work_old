#ifndef INTERFACE_BUS_ADDRESS_SCHEMAS_H
#define INTERFACE_BUS_ADDRESS_SCHEMAS_H



#include "common/common/status.h"
#include "common/common/dbrecordset.h"



/*!
	\class Interface_BusAddressSchemas
	\brief Interface for selecting addr schemas
	\ingroup Bus_Interface_Collection
*/

class Interface_BusAddressSchemas
{
public:
	virtual ~Interface_BusAddressSchemas(){};

	virtual void Write(Status &Ret_pStatus, DbRecordSet &RetOut_Data,QString strLockRes) = 0;
	virtual void Read(Status &Ret_pStatus, DbRecordSet &Ret_Data) = 0;
	virtual void Delete(Status &Ret_pStatus, int nRecordID) = 0;
	virtual void Lock(Status &Ret_pStatus, int nRecordID,QString &Ret_strLockRes) = 0;
	virtual void Unlock(Status &Ret_pStatus, QString strLockRes) = 0;

};



#endif //INTERFACE_BUS_ADDRESS_SCHEMAS_H
