#ifndef INTERFACE_BUSIMPORT_H
#define INTERFACE_BUSIMPORT_H


#include "common/common/status.h"
#include "common/common/dbrecordset.h"



/*!
	\class Interface_BusImport
	\brief Interface for importing business data
	\ingroup Bus_Interface_Collection
*/

class Interface_BusImport
{
public:
	virtual ~Interface_BusImport(){};

	virtual void ImportProjects(Status &Ret_pStatus, DbRecordSet &RetOut_Data,int nOperation) = 0;
	virtual void ImportPersons(Status &Ret_pStatus, DbRecordSet &RetOut_Data,int nOperation, bool bCreateUsers, bool bCreateContacts, int nRoleID) = 0;
	virtual void ImportContacts(Status &Ret_pStatus, DbRecordSet &RetOut_Data, DbRecordSet &RetOut_ErrRow, int nOperation, bool bCreateUsers) = 0;
	virtual void ImportUserContactRelationships(Status &Ret_pStatus, DbRecordSet &RetOut_Data) = 0;
	virtual void ImportContactContactRelationships(Status &Ret_pStatus, DbRecordSet &RetOut_Data) = 0;
	virtual void GetContactContactRelationships(Status &Ret_pStatus, DbRecordSet &RetOut_Data) = 0;
	virtual void ImportContactCustomData(Status &Ret_pStatus, QString strCustomFieldName, int nTableID, DbRecordSet &RetOut_Data)=0;
	
	//automated data exchange
	virtual void ProcessProjects_Import(Status &Ret_pStatus, QString strFileExt, QString strFilePrefix, QString strFileExtOfProcessed, QString strDirPath, DbRecordSet &Ret_ProcessedFiles, DbRecordSet &Ret_pErrors) = 0;
	virtual void ProcessProjects_Export(Status &Ret_pStatus, QString strFileExt, QString strFilePrefix, QString strFileExtOfProcessed, QString strDirPath, int nExportLasN, bool bExportAddDateTimeOnExportFile, DbRecordSet &Ret_ProcessedFiles, DbRecordSet &Ret_pErrors, DbRecordSet &ProjectsForExport, bool bClientSideExport, QByteArray &Ret_File, bool bUtf8) = 0;

	virtual void ProcessUsers_Import(Status &Ret_pStatus, QString strFileExt, QString strFilePrefix, QString strFileExtOfProcessed, QString strDirPath, DbRecordSet &Ret_ProcessedFiles, DbRecordSet &Ret_pErrors, int nRoleID) = 0;
	virtual void ProcessUsers_Export(Status &Ret_pStatus, QString strFileExt, QString strFilePrefix, QString strFileExtOfProcessed, QString strDirPath, int nExportLasN, bool bExportAddDateTimeOnExportFile, DbRecordSet &Ret_ProcessedFiles, DbRecordSet &Ret_pErrors, DbRecordSet &UsersForExport, bool bClientSideExport, QByteArray &Ret_File, bool bUtf8) = 0;

	//virtual void ProcessDebtors_Import(Status &Ret_pStatus, QString strFileExt, QString strFilePrefix, QString strFileExtOfProcessed, QString strDirPath, DbRecordSet &Ret_ProcessedFiles, DbRecordSet &Ret_pErrors) = 0;
	virtual void ProcessDebtors_Export(Status &Ret_pStatus, QString strFileExt, QString strFilePrefix, QString strFileExtOfProcessed, QString strDirPath, int nExportLasN, bool bExportAddDateTimeOnExportFile, DbRecordSet &Ret_ProcessedFiles, DbRecordSet &Ret_pErrors, int nAddressTypeFilter, bool bUtf8) = 0;

	virtual void ProcessContacts_Import(Status &Ret_pStatus, QString strFileExt, QString strFilePrefix, QString strFileExtOfProcessed, QString strDirPath, DbRecordSet &Ret_ProcessedFiles, DbRecordSet &Ret_pErrors) = 0;
	virtual void ProcessContacts_Export(Status &Ret_pStatus, QString strFileExt, QString strFilePrefix, QString strFileExtOfProcessed, QString strDirPath, int nExportLasN, bool bExportAddDateTimeOnExportFile, DbRecordSet &Ret_ProcessedFiles, DbRecordSet &Ret_pErrors, DbRecordSet &UsersForExport, bool bClientSideExport, QByteArray &Ret_File, bool bUtf8, bool bSokratesHdr, bool bColumnTitles, bool bOutlookExport, QString strLang, QByteArray datHedFile) = 0;
	virtual void ProcessContactContactRelations_Export(Status &Ret_pStatus, QString strFileExt, QString strFilePrefix, QString strFileExtOfProcessed, QString strDirPath, int nExportLasN, bool bExportAddDateTimeOnExportFile, DbRecordSet &Ret_ProcessedFiles, DbRecordSet &Ret_pErrors, DbRecordSet &UsersForExport, bool bClientSideExport, QByteArray &Ret_File, bool bUtf8, bool bSokratesHdr, bool bColumnTitles, bool bOutlookExport, QString strLang, QByteArray datHedFile) = 0;

	virtual void ProcessStoredProjectList_Import(Status &Ret_pStatus, QString strFileExt, QString strFilePrefix, QString strFileExtOfProcessed, QString strDirPath, DbRecordSet &Ret_ProcessedFiles, DbRecordSet &Ret_pErrors)=0;
};

#endif // INTERFACE_BUSIMPORT_H
