#ifndef INTERFACE_ACCESSRIGHTS_H
#define INTERFACE_ACCESSRIGHTS_H


#include "common/common/status.h"
#include "common/common/dbrecordset.h"

/*!
\class Interface_AccessRights
\brief Access Rights building class
\ingroup Bus_Interface_Collection


*/

class Interface_AccessRights
{
public:
	virtual ~Interface_AccessRights(){};

	//------------------------------------
	//			Person section
	//------------------------------------
	virtual void GetRoleList(Status &Ret_pStatus, DbRecordSet &Ret_pPersonRoleListRecordSet) = 0;
	virtual void GetPersons(Status &Ret_pStatus, DbRecordSet &Ret_pPersonsRecordSet) = 0;
	virtual void GetPersonRoleList(Status &Ret_pStatus, DbRecordSet &Ret_pPersonRoleListRecordSet, int PersonID) = 0;
	virtual void InsertNewRoleToPerson(Status &Ret_pStatus, int PersonID, int RoleID) = 0;
	virtual void DeleteRoleFromPerson(Status &Ret_pStatus, int PersonID, int RoleID) = 0;

	//------------------------------------
	//			User section
	//------------------------------------
	virtual void GetRoleListForUsers(Status &Ret_pStatus, DbRecordSet &Ret_pUserRoleListRecordSet) = 0;
	virtual void GetUsers(Status &Ret_pStatus, DbRecordSet &Ret_pUsersRecordSet) = 0;
	virtual void GetUserRoleList(Status &Ret_pStatus, DbRecordSet &Ret_pUserRoleListRecordSet, int UserID) = 0;
	virtual void InsertNewRoleToUser(Status &Ret_pStatus, int UserID, int RoleID) = 0;
	virtual void DeleteRoleFromUser(Status &Ret_pStatus, int UserID, int RoleID) = 0;

	virtual void GetUserAccRightsRecordSet(Status &Ret_pStatus, DbRecordSet &Ret_pUserAccRRecordSet, int UserID) = 0;

	//------------------------------------
	//			Role definition section
	//------------------------------------
	virtual void GetRoles(Status &Ret_pStatus, DbRecordSet &Ret_pUserRoleListRecordSet) = 0;
	virtual void GetBERoles(Status &Ret_pStatus, DbRecordSet &Ret_pRoleListRecordSet) = 0;
	virtual void GetTERoles(Status &Ret_pStatus, DbRecordSet &Ret_pRoleListRecordSet) = 0;
	virtual void GetRoleAccessRightSets(Status &Ret_pStatus, DbRecordSet &Ret_pAccessRightsListRecordSet, int RoleID) = 0;
	virtual void GetAccessRightsSets(Status &Ret_pStatus, DbRecordSet &Ret_pAccessRightsSetListRecordSet) = 0;
	virtual void DeleteRole(Status &Ret_pStatus, int RoleID) = 0;
	virtual void DeleteAccRSetFromRole(Status &Ret_pStatus, int RoleID, int AccRSetID) = 0;
	virtual void InsertNewAccRSetToRole(Status &Ret_pStatus, int RoleID, int AccRSetID) = 0;
	virtual void InsertNewRole(Status &Ret_pStatus, int &Ret_nNewRoleRowID, QString RoleName, QString RoleDescription, int RoleType) = 0;
	virtual void RenameRole(Status &Ret_pStatus, int RoleID, QString NewRoleName, QString NewRoleDescription) = 0;
	virtual void GetAccessRights(Status &Ret_pStatus, DbRecordSet &Ret_pAccessRightsRecordSet, int RoleID, int AccRSetID) = 0;
	virtual void SetAccessRightValue(Status &Ret_pStatus, int AccRightID, QString Value) = 0;
	virtual void GetDefaultRoles(Status &Ret_pStatus, int RoleType, DbRecordSet &Ret_pDefaultRoleRecordSet) = 0;
	virtual void ChangeAccessRights(Status &Ret_pStatus, DbRecordSet &ChangedRightsRecordSet) = 0;
	//------------------------------------
	//			UAR/GAR system
	//------------------------------------
	virtual void ReadGUAR(Status &Ret_pStatus, int nRecord, int nTableID,DbRecordSet &Ret_UAR, DbRecordSet &Ret_GAR)=0;
	virtual void WriteGUAR(Status &Ret_pStatus, int nRecord, int nTableID,DbRecordSet &RetOut_UAR, DbRecordSet &RetOut_GAR)=0;
	virtual void PrepareGUAR(Status &Ret_pStatus, int nRecord, int nTableID, DbRecordSet &RetOut_UAR, DbRecordSet &RetOut_GAR, DbRecordSet &Ret_lstGroupUsers,int &Ret_ParentRecord,int &Ret_ParentTableID,QString &Ret_ParentName)=0;
	virtual void TestUAR(Status &Ret_pStatus, int nTableID, int nPersonID, int nAccessLevel, DbRecordSet &lstTest_IDs, DbRecordSet &Ret_lstResult_IDs, bool bReturnAllowed=true)=0;
	virtual void UpdateProjectGUAR(Status &Ret_pStatus, DbRecordSet &lstProject_IDs)=0;
	virtual void UpdateGroupContactsGUAR(Status &Ret_pStatus, DbRecordSet &lstData)=0;
	virtual void UpdateProjectCommEntityGUAR(Status &Ret_pStatus, int nTableID, DbRecordSet &lstData)=0;

};

#endif // INTERFACE_ACCESSRIGHTS_H
