#ifndef INTERFACE_BUS_GROUP_TREE
#define INTERFACE_BUS_GROUP_TREE



#include "common/common/status.h"
#include "common/common/dbrecordset.h"


/*!
	\class Interface_BusGroupTree
	\brief Interface for managing Group Tree Pattern data
	\ingroup Bus_Interface_Collection
*/

class Interface_BusGroupTree
{
public:
	virtual ~Interface_BusGroupTree(){};

	//TREE OPERATIONS
	virtual void ReadTree(Status &Ret_pStatus, int nGroupEntityTypeID,DbRecordSet &Ret_Data) = 0;
	virtual void WriteTree(Status &Ret_pStatus, DbRecordSet &RetOut_Data,QString strLockRes) = 0;
	virtual void DeleteTree(Status &Ret_pStatus, int nTreeID) = 0;
	virtual void LockTree(Status &Ret_pStatus, int nTreeID,QString &Ret_strLockRes) = 0;
	virtual void UnLockTree(Status &Ret_pStatus, QString strLockRes) = 0;

	//ITEM-GROUP OPERATIONS (node)
	virtual void ReadGroupTree(Status &Ret_pStatus, int nTreeID,DbRecordSet &Ret_Data)=0;
	virtual void WriteGroupItem(Status &Ret_pStatus, int nTreeID,DbRecordSet &RetOut_Data)=0;
	virtual void DeleteGroupItem(Status &Ret_pStatus, int nTreeID,DbRecordSet &DataForDelete)=0;
	virtual void ChangeGroupContent(Status &Ret_pStatus, int nGroupEntityTypeID,int nGroupItemID,int nOperation,DbRecordSet &RetOut_GroupContent)=0;
	virtual void ReadGroupContent(Status &Ret_pStatus, int nGroupEntityTypeID,DbRecordSet &GroupItems,DbRecordSet &Ret_GroupContent)=0;

	//special purpose fast functions:
	virtual void WriteGroupItemData(Status &Ret_pStatus, int nGroupItemID, QString strExtCategory, QString strDescription)=0;
	virtual void WriteGroupContentData(Status &Ret_pStatus, int nGroupEntityTypeID, DbRecordSet &GroupItems)=0;
	virtual void RemoveGroupContentItem(Status &Ret_pStatus, int nGroupEntityTypeID,DbRecordSet &GroupItemsForRemove,DbRecordSet &Groups)=0;
	virtual void GetChildrenGroups(Status &Ret_pStatus, int nGroupParentID,DbRecordSet &Ret_ChildrenGroups)=0;

};



#endif //INTERFACE_BUS_GROUP_TREE
