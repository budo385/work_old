#ifndef INTERFACE_WEBPROJECTS_H
#define INTERFACE_WEBPROJECTS_H

#include "common/common/status.h"
#include "common/common/dbrecordset.h"

class Interface_WebProjects
{
public:
	virtual ~Interface_WebProjects(){};

	virtual void ReadProjectLists(Status &Ret_pStatus, DbRecordSet &Ret_Lists, int &Ret_nStartListID)=0;
	virtual void ReadProjectListData(Status &Ret_pStatus, int nListID,DbRecordSet &Ret_Data, QString strParentProjectCode="", int nLevelsDeep=-1)=0;
	virtual void ReadCommEntityCount(Status &Ret_pStatus, int nProjectID, int &Ret_nFileDocCount, int &Ret_nWebSiteCount, int &Ret_nNotesCount,int &Ret_nMailCount)=0;
	virtual void ReadProjectDocuments(Status &Ret_pStatus, int &Ret_nTotalCount,DbRecordSet &Ret_Documents,  int nProjectID,int nDocType=-1, int nFromN=-1, int nToN=-1)=0;
	virtual void ReadProjectEmails(Status &Ret_pStatus, int &Ret_nTotalCount, DbRecordSet &Ret_Emails, int nProjectID, int nFromN=-1, int nToN=-1)=0;

};

/*
//Full meta data for one function

	<ReadProjectLists>
		<URL>/project/list</URL>
		<HTTP_METHOD>GET</HTTP_METHOD>
		<REQUEST_XML_SCHEMA>contacts_search_request.xsd</REQUEST_XML_SCHEMA>
		<RESPONSE_XML_SCHEMA>contacts_search_response.xsd</RESPONSE_XML_SCHEMA>
		<SERVICE_DESC_XML>contacts_contacts.xml</SERVICE_DESC_XML>
		<RES_ID>nProjectID</RES_ID>
		<RES_PARENT_ID>nProjectID</RES_PARENT_ID>
		<Ret_nStartListID><DISPLAY_NAME>StartupListID</DISPLAY_NAME></Ret_nStartListID>
		<Ret_Documents>
			<DISPLAY_NAME>Documents</DISPLAY_NAME>
			<ROW_NAME>document</ROW_NAME>
			<ROW_URL_ATTR>GetWebServiceURL()+"/document"</ROW_URL_ATTR>
			<ROW_URL_ATTR_ID_FIELD>Document_ID</ROW_URL_ATTR_ID_FIELD>
		</Ret_Documents>
	</ReadProjectLists>



- URL and HTTP_METHOD are required, other are not
- to build meta data of existing objects, take interface_web*.h and corespondong rpcskeleton_web*.cpp file
- from rpcskeleton_web*.cpp in constructor you have information of service URL and method. e.g.: 
	mFunctList.insert("GET/contacts",&RpcSkeleton_WebContacts::ReadContacts);  ->means method=GET and url=/contacts
- REQUEST_XML_SCHEMA,   RESPONSE_XML_SCHEMA and SERVICE_DESC_XML ignore for now
- RES_ID and RES_PARENT_ID are input parameters, can be omitted if not required by function, 
	e.g. if you find that url method is something like this: GET/contact/[RES_ID]/addresses, it means that RES_ID is required and must be defined
	in in rpcskeleton in function search for use of int nResource_id,int nResource_parent_id parameters and in <RES_ID> or <RES_PARENT_ID> put name of parameter that is used
	e.g. if you find in function that nProjectID=nResource_id then you must declare <RES_ID>nProjectID</RES_ID>
	Same with RES_PARENT_ID...
- Return parameters are listed at the end of metadata description of the function. Return parameter can be omitted from metadata or it can contain:
			<DISPLAY_NAME> - name of parameter that will be sent back to client
			<ROW_NAME> -name of row if list (if parameter is not list then omit)
			<ROW_URL_ATTR> - attribute for each row of list (if parameter is not list then omit)
			<ROW_URL_ATTR_ID_FIELD> - attribute id for each row of list (if parameter is not list then omit)
			
	This info is only relvenat for AddParameter() function called for each return parameter
    Task: search each AddParameter() inside skeleton and if you find any parameters of AddParameter() create metadata for return paraneters,
    
    e.g. if you find: 
    rpc.msg_out->AddParameter(&Ret_Contacts,"Contacts","contact",GetWebServiceURL()+"/contact","Contact_ID");
    create metadata:
    <Ret_Contacts>
			<DISPLAY_NAME>Contacts</DISPLAY_NAME>
			<ROW_NAME>contact</ROW_NAME>
			<ROW_URL_ATTR>GetWebServiceURL()+"/contact"</ROW_URL_ATTR>
			<ROW_URL_ATTR_ID_FIELD>Contact_ID</ROW_URL_ATTR_ID_FIELD>
    </Ret_Contacts>
    
    e.g. if you find: 
    rpc.msg_out->AddParameter(&Ret_nStartListID,"StartupListID");
    create metadata:
    <Ret_nStartListID>
			<DISPLAY_NAME>Contacts</DISPLAY_NAME>
    </Ret_nStartListID>
	
*/



/*

<Web_service_meta_data>
	
	<ReadProjectLists>
		<URL>/project/list</URL>
		<HTTP_METHOD>GET</HTTP_METHOD>
		<Ret_nStartListID><DISPLAY_NAME>StartupListID</DISPLAY_NAME></Ret_nStartListID>
		<Ret_Lists>
			<DISPLAY_NAME>ProjectList</DISPLAY_NAME>
			<ROW_NAME>list</ROW_NAME>
		</Ret_Lists>
	</ReadProjectLists>

	<ReadProjectListData>
		<URL>/project/list/data</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
		<Ret_Data>
			<DISPLAY_NAME>Projects</DISPLAY_NAME>
			<ROW_NAME>project</ROW_NAME>
		</Ret_Data>
	</ReadProjectListData>

	<ReadCommEntityCount>
		<URL>/project/[RES_ID]/comm_entity/count</URL>
		<HTTP_METHOD>GET</HTTP_METHOD>
		<RES_ID>nProjectID</RES_ID>
		<Ret_nFileDocCount><DISPLAY_NAME>nFileDocCount</DISPLAY_NAME></Ret_nFileDocCount>
		<Ret_nWebSiteCount><DISPLAY_NAME>nWebSiteCount</DISPLAY_NAME></Ret_nWebSiteCount>
		<Ret_nNotesCount><DISPLAY_NAME>nNotesCount</DISPLAY_NAME></Ret_nNotesCount>
		<Ret_nMailCount><DISPLAY_NAME>nEmailCount</DISPLAY_NAME></Ret_nMailCount>
	</ReadCommEntityCount>

	<ReadProjectDocuments>
		<URL>/project/[RES_ID]/documents</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
		<RES_ID>nProjectID</RES_ID>
		<Ret_nTotalCount><DISPLAY_NAME>nTotalCount</DISPLAY_NAME></Ret_nTotalCount>
		<Ret_Documents>
			<DISPLAY_NAME>Documents</DISPLAY_NAME>
			<ROW_NAME>document</ROW_NAME>
			<ROW_URL_ATTR>GetWebServiceURL()+"/document"</ROW_URL_ATTR>
			<ROW_URL_ATTR_ID_FIELD>Document_ID</ROW_URL_ATTR_ID_FIELD>
		</Ret_Documents>
	</ReadProjectDocuments>

	<ReadProjectEmails>
		<URL>/project/[RES_ID]/emails</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
		<RES_ID>nProjectID</RES_ID>
		<Ret_nTotalCount><DISPLAY_NAME>nTotalCount</DISPLAY_NAME></Ret_nTotalCount>
		<Ret_Emails>
			<DISPLAY_NAME>Emails</DISPLAY_NAME>
			<ROW_NAME>email</ROW_NAME>
			<ROW_URL_ATTR>GetWebServiceURL()+"/email"</ROW_URL_ATTR>
			<ROW_URL_ATTR_ID_FIELD>Email_ID</ROW_URL_ATTR_ID_FIELD>
		</Ret_Emails>
	</ReadProjectEmails>
	
</Web_service_meta_data>
*/

#endif // INTERFACE_WEBPROJECTS_H
