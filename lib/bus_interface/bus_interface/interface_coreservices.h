#ifndef INTERFACE_CORESERVICE_H
#define INTERFACE_CORESERVICE_H


#include "common/common/status.h"
#include "common/common/dbrecordset.h"

/*!
	\class Interface_CoreServices
	\brief Collection of core / system services: LBO,etc...
	\ingroup Bus_Interface_Collection
*/
class Interface_CoreServices
{
public:

	virtual ~Interface_CoreServices(){};
	virtual void ReadLBOList(Status &Ret_pStatus, DbRecordSet &Ret_Data) = 0;
	virtual void CheckVersion(Status& Ret_Status,QString strClientVersion,QString &Ret_strServerVersion,QString& Ret_strServerDbVersion,QString& Ret_strTemplateInUse,int& Ret_nAskOnStartup,int& Ret_nStartStartUpWizard,QByteArray& Ret_byteVersion, int &Ret_nClientTimeZoneOffsetMinutes)=0;
	virtual void UpdateVersion(Status& Ret_Status,QByteArray byteVersion)=0;
	virtual void ReadUserSessionData(Status& Ret_Status,DbRecordSet &Ret_UserData, DbRecordSet &Ret_ARSet,DbRecordSet &Ret_ContactData, DbRecordSet &Ret_recSettings, DbRecordSet &Ret_recOptions, DbRecordSet &Ret_lstMessages)=0;
	virtual void GetUserSessions(Status& Ret_Status,int &Ret_nUserSessions)=0;
	virtual void SavePersonalSettings(Status &Ret_pStatus, DbRecordSet &RetOut_pLstForWrite) = 0;
	virtual void SavePersonalSettingsByPersonID(Status &Ret_pStatus, int nPersonID, DbRecordSet &RetOut_pLstForWrite) = 0;
	virtual void GetPersonalSettingsByPersonID(Status &Ret_pStatus, int nPersonID, DbRecordSet &RetOut_pLstForWrite) = 0;
};


#endif //INTERFACE_USERLOGON_H
