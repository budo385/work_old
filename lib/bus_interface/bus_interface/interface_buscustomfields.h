#ifndef INTERFACE_BUS_CUSTOM_FIELDS_H
#define INTERFACE_BUS_CUSTOM_FIELDS_H



#include "common/common/status.h"
#include "common/common/dbrecordset.h"



/*!
	\class Interface_BusCustomFields
	\brief Interface for custom fields
	\ingroup Bus_Interface_Collection
*/

class Interface_BusCustomFields
{
public:
	virtual ~Interface_BusCustomFields(){};

	virtual void Write(Status &Ret_pStatus, DbRecordSet &RetOut_Data,DbRecordSet &RetOut_Selections,QString strLockedRes)=0;
	virtual void Read(Status &Ret_pStatus, int nCustomFieldID, DbRecordSet &Ret_Data,DbRecordSet &Ret_Selections)=0;
	virtual void ReadFields(Status &Ret_pStatus, int nTableID, DbRecordSet &Ret_Data)=0;
	virtual void ReadUserCustomData(Status &Ret_pStatus, int nTableID, int nRecordID, DbRecordSet &Ret_Data)=0;
	virtual void WriteUserCustomData(Status &Ret_pStatus, int nTableID, int nRecordID, DbRecordSet &RetOut_Data)=0;
	virtual void ReadUserCustomDataForInsert(Status &Ret_pStatus,int nTableID, DbRecordSet &Ret_Data)=0;

};



#endif //INTERFACE_BUS_CUSTOM_FIELDS_H
