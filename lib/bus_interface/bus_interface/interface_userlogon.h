#ifndef INTERFACE_USERLOGON_H
#define INTERFACE_USERLOGON_H


#include "common/common/status.h"

/*!
    \class Interface_UserLogon
    \brief UserLogon class
    \ingroup Bus_Interface_Collection

	Has different interface for client & server, because it is used through client connectionhandler.
	Use overloaded functions, inherit as protected, reeanble only what u want.
		
*/

class Interface_UserLogon
{
public:

	virtual ~Interface_UserLogon(){};

	//client
	virtual void Login(Status& Ret_Status, QString strUsername, QString strPassword,QString strModuleCode, QString strMLIID="",QString strParentSession="", int nClientTimeZoneOffsetMinutes=0)=0;		
	virtual void Logout(Status& Ret_Status)=0;
	virtual void IsAlive(Status& Ret_Status)=0;
	//server:
	virtual void Login(Status& Ret_Status, QString& RetOut_strSessionID,QString strUsername, QByteArray strAuthToken,QByteArray strClientNonce,QString strModuleCode, QString strMLIID="", QString strParentSession="", bool bIsWebLogin=false, int nClientTimeZoneOffsetMinutes=0)=0;		
	//all
	virtual void ChangePassword(Status& Ret_Status,QString strUsername, QByteArray strAuthToken,QByteArray strClientNonce,QByteArray newPass,QString strNewUserName)=0;

};


#endif //INTERFACE_USERLOGON_H
