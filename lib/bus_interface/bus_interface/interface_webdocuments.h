#ifndef INTERFACE_WEBDOCUMENTS_H
#define INTERFACE_WEBDOCUMENTS_H

#include "common/common/status.h"
#include "common/common/dbrecordset.h"

class Interface_WebDocuments
{
public:
	virtual ~Interface_WebDocuments(){};

	virtual void ReadDocuments(Status &Ret_pStatus, DbRecordSet &Ret_Documents)=0;
	virtual void ReadDocumentsWithFilter(Status &Ret_pStatus, int &Ret_nTotalCount,DbRecordSet &Ret_Documents,  int nDocType=-1, int nFromN=-1, int nToN=-1)=0;

	//general api:
	virtual void SearchDocuments(Status &Ret_pStatus, int &Ret_nTotalCount,DbRecordSet &Ret_Documents,  int nDocType=-1, int nFromN=-1, int nToN=-1, int nContactID=-1, int nProjectID=-1,  int nOwnerUserID=-1,QString strSearchByName="", QDateTime datFromLastModified=QDateTime(), QDateTime datToLastModified=QDateTime(), QDateTime datFromCreated=QDateTime(), QDateTime datToCreated=QDateTime())=0;
	virtual void ReadDocumentDetails(Status &Ret_pStatus, int nDocumentID, DbRecordSet &Ret_Document)=0;
	virtual void ReadNoteDocument(Status &Ret_pStatus, int nDocumentID,QString &Ret_strDocumentURL)=0;
	virtual void CheckIn(Status &Ret_pStatus, int nDocumentID)=0;
	virtual void CheckOut(Status &Ret_pStatus, int nDocumentID,QString &Ret_strDocumentURL,bool bReadOnly=true)=0;
	virtual void CheckOutCopy(Status &Ret_pStatus, int nDocumentID,QString &Ret_strDocumentURL)=0;
};

/*
<Web_service_meta_data>
	
	<ReadDocuments>
		<URL>/document</URL>
		<HTTP_METHOD>GET</HTTP_METHOD>
		<Ret_Documents>
			<DISPLAY_NAME>Documents</DISPLAY_NAME>
			<ROW_NAME>document</ROW_NAME>
			<ROW_URL_ATTR>GetWebServiceURL()+"/document"</ROW_URL_ATTR>
			<ROW_URL_ATTR_ID_FIELD>Document_ID</ROW_URL_ATTR_ID_FIELD>
		</Ret_Documents>
	</ReadDocuments>
	
	<ReadDocumentsWithFilter>
		<URL>/document/filter</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
		<Ret_nTotalCount><DISPLAY_NAME>nTotalCount</DISPLAY_NAME></Ret_nTotalCount>
		<Ret_Documents>
			<DISPLAY_NAME>Documents</DISPLAY_NAME>
			<ROW_NAME>document</ROW_NAME>
			<ROW_URL_ATTR>GetWebServiceURL()+"/document"</ROW_URL_ATTR>
			<ROW_URL_ATTR_ID_FIELD>Document_ID</ROW_URL_ATTR_ID_FIELD>
		</Ret_Documents>
	</ReadDocumentsWithFilter>

	<SearchDocuments>
		<URL>/document/search</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
		<Ret_nTotalCount><DISPLAY_NAME>nTotalCount</DISPLAY_NAME></Ret_nTotalCount>
		<Ret_Documents>
			<DISPLAY_NAME>Documents</DISPLAY_NAME>
			<ROW_NAME>document</ROW_NAME>
			<ROW_URL_ATTR>GetWebServiceURL()+"/document"</ROW_URL_ATTR>
			<ROW_URL_ATTR_ID_FIELD>Document_ID</ROW_URL_ATTR_ID_FIELD>
		</Ret_Documents>
	</SearchDocuments>

	<ReadDocumentDetails>
		<URL>/document/[RES_ID]</URL>
		<HTTP_METHOD>GET</HTTP_METHOD>
		<RES_ID>nDocumentID</RES_ID>
		<Ret_Document>
			<DISPLAY_NAME>Document</DISPLAY_NAME>
			<ROW_NAME>document</ROW_NAME>
		</Ret_Document>
	</ReadDocumentDetails>
  
   <ReadNoteDocument>
		<URL>/document/[RES_ID]/note</URL>
		<HTTP_METHOD>GET</HTTP_METHOD>
		<RES_ID>nDocumentID</RES_ID>
		<Ret_strDocumentURL><DISPLAY_NAME>strDocumentURL</DISPLAY_NAME></Ret_strDocumentURL>
	</ReadNoteDocument>
	
	<CheckIn>
		<URL>/document/[RES_ID]/check_in</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
		<RES_ID>nDocumentID</RES_ID>
	</CheckIn>

	<CheckOut>
		<URL>/document/[RES_ID]/check_out</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
		<RES_ID>nDocumentID</RES_ID>
		<Ret_strDocumentURL><DISPLAY_NAME>strDocumentURL</DISPLAY_NAME></Ret_strDocumentURL>
	</CheckOut>

	<CheckOutCopy>
		<URL>/document/[RES_ID]/check_out_copy</URL>
		<HTTP_METHOD>GET</HTTP_METHOD>
		<RES_ID>nDocumentID</RES_ID>
		<Ret_strDocumentURL><DISPLAY_NAME>strDocumentURL</DISPLAY_NAME></Ret_strDocumentURL>
	</CheckOutCopy>

	
</Web_service_meta_data>
*/

#endif // INTERFACE_WEBDOCUMENTS_H