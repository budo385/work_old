#ifndef INTERFACE_WEBSERVER_H
#define INTERFACE_WEBSERVER_H

#include "common/common/status.h"
#include <QDateTime>

class Interface_WebServer
{
public:
	virtual ~Interface_WebServer(){};

	virtual void Login(Status& Ret_Status, QString& RetOut_strSessionID,int& Ret_nContactID,int& Ret_nPersonID,QString strUserName, QString strAuthToken,QString strClientNonce,int nProgCode=0, QString strProgVer="", QString strClientID="", QString strPlatform="",int nClientTimeZoneOffsetMinutes=0)=0;
	virtual void Logout(Status &Ret_Status,QString strSessionID)=0;
	virtual void ChangePassword(Status &Ret_Status,QString& RetOut_strSessionID, QString strUsername, QString strAuthToken,QString strClientNonce,QByteArray newPass, int nClientTimeZoneOffsetMinutes)=0;
	virtual void ReloadWebPages(Status &Ret_Status)=0;
	virtual void GetServerTime(Status &Ret_Status, QDateTime& Ret_datCurrentServerTime, QDateTime& Ret_datTimeNextTest, int& Ret_nActiveClientSocketCnt)=0;
	virtual void SetNextTestTime(Status &Ret_Status, QDateTime datSetTimeNextTest)=0;

};

/*
<Web_service_meta_data>
	<Login>
		<URL>/server/login</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
		<RetOut_strSessionID>
			<DISPLAY_NAME>Session</DISPLAY_NAME>
		</RetOut_strSessionID>
	</Login>

	<Logout>
		<URL>/server/logout</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
	</Logout>

	<ChangePassword>
		<URL>/server/changepassword</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
		<RetOut_strSessionID>
			<DISPLAY_NAME>Session</DISPLAY_NAME>
		</RetOut_strSessionID>
	</ChangePassword>

	<ReloadWebPages>
		<URL>/server/reloadwebpages</URL>
		<HTTP_METHOD>GET</HTTP_METHOD>
	</ReloadWebPages>

	<GetServerTime>
		<URL>/server/getservertime</URL>
		<HTTP_METHOD>GET</HTTP_METHOD>
	</GetServerTime>

	<SetNextTestTime>
		<URL>/server/setnexttesttime</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
	</SetNextTestTime>	

</Web_service_meta_data>
*/

#endif // INTERFACE_WEBSERVER_H
