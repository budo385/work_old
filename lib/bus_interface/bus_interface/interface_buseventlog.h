#ifndef INTERFACE_BUSEVENTLOG_H
#define INTERFACE_BUSEVENTLOG_H


#include "common/common/status.h"
#include "common/common/dbrecordset.h"

/*!
\class Interface_BusEventLog
\brief Business event log class
\ingroup Bus_Interface_Collection


*/

class Interface_BusEventLog
{
public:
	virtual ~Interface_BusEventLog(){};

	//Add an Event.
	virtual void AddEvent(Status &Ret_pStatus, int &Ret_EventID, int EventCode, QString EventText, int EventType, int EvCategoryID, QDateTime EventDateTime = QDateTime()) = 0;
	//Delete event.
	virtual void DeleteEventByEventID(Status &Ret_pStatus, int EventID) = 0; 
	virtual void DeleteEventByDateTime(Status &Ret_pStatus, QDateTime DateOlderThenGivenDateTime) = 0;
	//Get event index list.
	virtual void GetEventRecordsetByType(Status &Ret_pStatus, int EvType, DbRecordSet &Ret_EventRecordSet) = 0;
	virtual void GetEventRecordsetByCategory(Status &Ret_pStatus, int EvCategoryID, DbRecordSet &Ret_EventRecordSet) = 0;
	virtual void GetEventRecordsetByDateTime(Status &Ret_pStatus, QDateTime BeginTime, QDateTime EndTime, DbRecordSet &Ret_EventRecordSet) = 0;
};


#endif //INTERFACE_BUSEVENTLOG_H
