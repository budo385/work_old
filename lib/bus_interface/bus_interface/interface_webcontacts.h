#ifndef INTERFACE_WEBCONTACTS_H
#define INTERFACE_WEBCONTACTS_H

#include "common/common/status.h"
#include "common/common/dbrecordset.h"

class Interface_WebContacts
{
public:
	virtual ~Interface_WebContacts(){};

	virtual void ReadContacts(Status &Ret_pStatus, DbRecordSet &Ret_Contacts)=0; 
	virtual void ReadContactsWithFilter(Status &Ret_pStatus, int &Ret_nTotalCount,DbRecordSet &Ret_Contacts, int nFilter=-1,int nFromN=-1, int nToN=-1, QString strFromLetter="", QString strToLetter="")=0; 
	virtual void ReadContactDetails(Status &Ret_pStatus, int nContactID, DbRecordSet &Ret_ContactData,DbRecordSet &Ret_Emails,DbRecordSet &Ret_WebSites,DbRecordSet &Ret_Phones,DbRecordSet &Ret_Sms,DbRecordSet &Ret_Sykpe,DbRecordSet &Ret_Addresses) = 0;

	virtual void SearchContacts(Status &Ret_pStatus, int &Ret_nTotalCount, DbRecordSet &Ret_Contacts,int nSearchType, QString strField1_Name, QString strField1_Value, QString strField2_Name, QString strField2_Value, int nGroupID=-1,int nFromN=-1, int nToN=-1, QString strFromLetter="", QString strToLetter="") = 0;
	virtual void ReadFavorites(Status &Ret_pStatus, DbRecordSet &Ret_Favorites) = 0;
	virtual void ReadContactAddresses(Status &Ret_pStatus, int nContactID,DbRecordSet &Ret_Emails,DbRecordSet &Ret_WebSites,DbRecordSet &Ret_Phones,DbRecordSet &Ret_Sms,DbRecordSet &Ret_Sykpe,DbRecordSet &Ret_Addresses) = 0;

	virtual void ReadContactDocuments(Status &Ret_pStatus, int &Ret_nTotalCount,DbRecordSet &Ret_Documents,  int nContactID,int nDocType=-1, int nFromN=-1, int nToN=-1)=0;
	virtual void ReadContactEmails(Status &Ret_pStatus, int &Ret_nTotalCount, DbRecordSet &Ret_Emails, int nContactID, int nFromN=-1, int nToN=-1)=0;
	virtual void ReadCommEntityCount(Status &Ret_pStatus, int nContactID, int &Ret_nFileDocCount, int &Ret_nWebSiteCount, int &Ret_nNotesCount,int &Ret_nEmailCount)=0;

};

/*
<Web_service_meta_data>
	
	<ReadContacts>
		<URL>/contact</URL>
		<HTTP_METHOD>GET</HTTP_METHOD>
		<Ret_Contacts>
			<DISPLAY_NAME>Contacts</DISPLAY_NAME>
			<ROW_NAME>contact</ROW_NAME>
			<ROW_URL_ATTR>GetWebServiceURL()+"/contact"</ROW_URL_ATTR>
			<ROW_URL_ATTR_ID_FIELD>Contact_ID</ROW_URL_ATTR_ID_FIELD>
		</Ret_Contacts>
	</ReadContacts>

	<ReadContactsWithFilter>
		<URL>/contact/filter</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
		<Ret_nTotalCount>
			<DISPLAY_NAME>nTotalCount</DISPLAY_NAME>
		</Ret_nTotalCount>
		<Ret_Contacts>
			<DISPLAY_NAME>Contacts</DISPLAY_NAME>
			<ROW_NAME>contact</ROW_NAME>
			<ROW_URL_ATTR>GetWebServiceURL()+"/contact"</ROW_URL_ATTR>
			<ROW_URL_ATTR_ID_FIELD>Contact_ID</ROW_URL_ATTR_ID_FIELD>
		</Ret_Contacts>
	</ReadContactsWithFilter>

	<ReadContactDetails>
		<URL>/contact/[RES_ID]</URL>
		<HTTP_METHOD>GET</HTTP_METHOD>
		<RES_ID>nContactID</RES_ID>
		<Ret_ContactData>
			<DISPLAY_NAME>ContactData</DISPLAY_NAME>
			<ROW_NAME>contact</ROW_NAME>
			<ROW_URL_ATTR>GetWebServiceURL()+"/contact"</ROW_URL_ATTR>
			<ROW_URL_ATTR_ID_FIELD>Contact_ID</ROW_URL_ATTR_ID_FIELD>
		</Ret_ContactData>
		<Ret_Emails>
			<DISPLAY_NAME>Emails</DISPLAY_NAME>
			<ROW_NAME>email</ROW_NAME>
		</Ret_Emails>
		<Ret_WebSites>
			<DISPLAY_NAME>WebSites</DISPLAY_NAME>
			<ROW_NAME>website</ROW_NAME>
		</Ret_WebSites>
		<Ret_Phones>
			<DISPLAY_NAME>Phones</DISPLAY_NAME>
			<ROW_NAME>phone</ROW_NAME>
		</Ret_Phones>
		<Ret_Sms>
			<DISPLAY_NAME>Sms</DISPLAY_NAME>
			<ROW_NAME>sms</ROW_NAME>
		</Ret_Sms>
		<Ret_Skype>
			<DISPLAY_NAME>Skype</DISPLAY_NAME>
			<ROW_NAME>skype</ROW_NAME>
		</Ret_Skype>
		<Ret_Addresses>
			<DISPLAY_NAME>Addresses</DISPLAY_NAME>
			<ROW_NAME>address</ROW_NAME>
		</Ret_Addresses>
	</ReadContactDetails>

	<SearchContacts>
		<URL>/contact/search</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
		<Ret_nTotalCount>
			<DISPLAY_NAME>nTotalCount</DISPLAY_NAME>
		</Ret_nTotalCount>
		<Ret_Contacts>
			<DISPLAY_NAME>Contacts</DISPLAY_NAME>
			<ROW_NAME>contact</ROW_NAME>
			<ROW_URL_ATTR>GetWebServiceURL()+"/contact"</ROW_URL_ATTR>
			<ROW_URL_ATTR_ID_FIELD>Contact_ID</ROW_URL_ATTR_ID_FIELD>
		</Ret_Contacts>
	</SearchContacts>
	
	<ReadFavorites>
		<URL>/contact/favorites</URL>
		<HTTP_METHOD>GET</HTTP_METHOD>
		<Ret_Favorites>
			<DISPLAY_NAME>Favorites</DISPLAY_NAME>
			<ROW_NAME>contact</ROW_NAME>
			<ROW_URL_ATTR>GetWebServiceURL()+"/contact"</ROW_URL_ATTR>
			<ROW_URL_ATTR_ID_FIELD>Contact_ID</ROW_URL_ATTR_ID_FIELD>
		</Ret_Favorites>
	</ReadFavorites>
	
	<ReadContactAddresses>
		<URL>/contact/[RES_ID]/addresses</URL>
		<HTTP_METHOD>GET</HTTP_METHOD>
		<RES_ID>nContactID</RES_ID>
		<Ret_Emails>
			<DISPLAY_NAME>Emails</DISPLAY_NAME>
			<ROW_NAME>email</ROW_NAME>
		</Ret_Emails>
		<Ret_WebSites>
			<DISPLAY_NAME>WebSites</DISPLAY_NAME>
			<ROW_NAME>website</ROW_NAME>
		</Ret_WebSites>
		<Ret_Phones>
			<DISPLAY_NAME>Phones</DISPLAY_NAME>
			<ROW_NAME>phone</ROW_NAME>
		</Ret_Phones>
		<Ret_Sms>
			<DISPLAY_NAME>Sms</DISPLAY_NAME>
			<ROW_NAME>sms</ROW_NAME>
		</Ret_Sms>
		<Ret_Skype>
			<DISPLAY_NAME>Skype</DISPLAY_NAME>
			<ROW_NAME>skype</ROW_NAME>
		</Ret_Skype>
		<Ret_Addresses>
			<DISPLAY_NAME>Addresses</DISPLAY_NAME>
			<ROW_NAME>address</ROW_NAME>
		</Ret_Addresses>
	</ReadContactAddresses>

	<ReadContactDocuments>
		<URL>/contact/[RES_ID]/documents</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
		<RES_ID>nContactID</RES_ID>
		<Ret_nTotalCount><DISPLAY_NAME>nTotalCount</DISPLAY_NAME></Ret_nTotalCount>
		<Ret_Documents>
			<DISPLAY_NAME>Documents</DISPLAY_NAME>
			<ROW_NAME>document</ROW_NAME>
			<ROW_URL_ATTR>GetWebServiceURL()+"/document"</ROW_URL_ATTR>
			<ROW_URL_ATTR_ID_FIELD>Document_ID</ROW_URL_ATTR_ID_FIELD>
		</Ret_Documents>
	</ReadContactDocuments>
	
	<ReadContactEmails>
		<URL>/contact/[RES_ID]/emails</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
		<RES_ID>nContactID</RES_ID>
		<Ret_nTotalCount>
			<DISPLAY_NAME>nTotalCount</DISPLAY_NAME>
		</Ret_nTotalCount>
		<Ret_Emails>
			<DISPLAY_NAME>Emails</DISPLAY_NAME>
			<ROW_NAME>email</ROW_NAME>
			<ROW_URL_ATTR>GetWebServiceURL()+"/email"</ROW_URL_ATTR>
			<ROW_URL_ATTR_ID_FIELD>Email_ID</ROW_URL_ATTR_ID_FIELD>
		</Ret_Emails>
	</ReadContactEmails>

	<ReadCommEntityCount>
		<URL>/contact/[RES_ID]/comm_entity/count</URL>
		<HTTP_METHOD>GET</HTTP_METHOD>
		<RES_ID>nContactID</RES_ID>
		<Ret_nFileDocCount><DISPLAY_NAME>nFileDocCount</DISPLAY_NAME></Ret_nFileDocCount>
		<Ret_nWebSiteCount><DISPLAY_NAME>nWebSiteCount</DISPLAY_NAME></Ret_nWebSiteCount>
		<Ret_nNotesCount><DISPLAY_NAME>nNotesCount</DISPLAY_NAME></Ret_nNotesCount>
		<Ret_nEmailCount><DISPLAY_NAME>nEmailCount</DISPLAY_NAME></Ret_nEmailCount>
	</ReadCommEntityCount>

</Web_service_meta_data>
*/

#endif // INTERFACE_WEBCONTACTS_H
