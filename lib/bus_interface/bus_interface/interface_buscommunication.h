#ifndef INTERFACE_BUSCOMMUNICATION_H
#define INTERFACE_BUSCOMMUNICATION_H

#include "common/common/status.h"
#include "common/common/dbrecordset.h"

/*!
\class Interface_BusCommunication
\brief Class to communication workbench.
\ingroup Bus_Interface_Collection

*/
class Interface_BusCommunication
{
public:
	~Interface_BusCommunication(){};

	virtual void SaveCommFilterViews(Status &Ret_pStatus, int &Ret_nViewID, DbRecordSet &recView, DbRecordSet &recViewsData) = 0;
	virtual void GetDefaultDesktopFilterViews(Status &Ret_pStatus, int nLoggedPersonID, DbRecordSet &RetOut_recViews) = 0;
	virtual void GetCommFilterViews(Status &Ret_pStatus, DbRecordSet &RetOut_recViews, int nPersonID, int nGridType) = 0;
	virtual void GetCommFilterData(Status &Ret_pStatus, DbRecordSet &RetOut_recSortRecordSet, DbRecordSet &RetOut_recViewsData, DbRecordSet &RetOut_recViewsChBoxesData, int nViewID, int nLoggedPersonID) = 0;
	virtual void GetCommFilterViewDataAndViews(Status &Ret_pStatus, DbRecordSet &RetOut_recViewData, DbRecordSet &RetOut_recViews, int nViewID, int nGridType) = 0;

	virtual void GetGridViewForContact(Status &Ret_pStatus, int &RetOut_nViewID, int nContactID) = 0;
	virtual void GetGridViewForProject(Status &Ret_pStatus, int &RetOut_nViewID, int nProjectID) = 0;
	
	virtual void GetPersonCommData(Status &Ret_pStatus, bool &Ret_bRowsStripped, DbRecordSet &RetOut_recEmails, DbRecordSet &RetOut_recDocuments, DbRecordSet &RetOut_recVoiceCalls, int PersonID, DbRecordSet recFilter, bool bForceFilterByTaskType = false) = 0;
	virtual void GetContactCommData(Status &Ret_pStatus, DbRecordSet &RetOut_recEmails, DbRecordSet &RetOut_recDocuments, DbRecordSet &RetOut_recVoiceCalls, int ContactID, DbRecordSet recFilter) = 0;
	virtual void GetProjectCommData(Status &Ret_pStatus, DbRecordSet &RetOut_recEmails, DbRecordSet &RetOut_recDocuments, DbRecordSet &RetOut_recVoiceCalls, int nProjectID, DbRecordSet recFilter) = 0;
	virtual void GetCalendarCommData(Status &Ret_pStatus, DbRecordSet &RetOut_recEmails, DbRecordSet &RetOut_recDocuments, DbRecordSet &RetOut_recVoiceCalls, DbRecordSet recEntities, DbRecordSet recFilter) = 0;
	virtual void GetCalendarGridCommData(Status &Ret_pStatus, DbRecordSet &RetOut_recEmails, DbRecordSet &RetOut_recDocuments, DbRecordSet &RetOut_recVoiceCalls, DbRecordSet recEntities, DbRecordSet recFilter) = 0;

	virtual void GetCommGridDataByIDs(Status &Ret_pStatus, DbRecordSet &RetOut_recData, DbRecordSet recIDsRecordset, int nGridType, int nEntityType) = 0;
	
	virtual void GetComplexFilterData(Status &Ret_pStatus, DbRecordSet &RetOut_recFilterData, int nPersonID) = 0;
	
	virtual void GetContactFromEmail(Status &Ret_pStatus, DbRecordSet &RetOut_lstData) = 0;
	virtual void SetTaskIsDone(Status &Ret_pStatus, DbRecordSet &lstData) = 0;
	virtual void PostPoneTasks(Status &Ret_pStatus, DbRecordSet &lstData, QDateTime datDueDateTime, QDateTime datStartDateTime) = 0;
	virtual void SetEmailsRead(Status &Ret_pStatus, DbRecordSet &lstData) = 0;
	virtual void ChangeEntityAssigment(Status &Ret_pStatus, int nAssignedType,int nAssignedID, int nOperation, DbRecordSet &Data)=0;
	virtual void ReadCEMenuData(Status &Ret_pStatus, int nPersonID, DbRecordSet &Ret_DocTemplate, DbRecordSet &Ret_DocApps, DbRecordSet &Ret_DocCheckOut,DbRecordSet &Ret_EmailTemplate)=0;
	virtual void AssignCommDataToContact(Status &Ret_pStatus, DbRecordSet &Data, int nContactID)=0;
	virtual void AssignCommDataToProject(Status &Ret_pStatus, DbRecordSet &Data, int nProjectID)=0;

	virtual void CheckIfRecordExists(Status &Ret_pStatus, int nEntityType, int nRecordID, bool &Ret_bExists)=0;
};

#endif // INTERFACE_BUSCOMMUNICATION_H
