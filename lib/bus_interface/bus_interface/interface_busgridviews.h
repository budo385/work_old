#ifndef INTERFACE_BUSGRIDVIEWS_H
#define INTERFACE_BUSGRIDVIEWS_H

#include "common/common/status.h"
#include "common/common/dbrecordset.h"

/*!
	\class Interface_BusGridViews
	\brief Interface for selecting addr schemas
	\ingroup Bus_Interface_Collection
*/

class Interface_BusGridViews
{
public:
	virtual ~Interface_BusGridViews(){};

	virtual void Write(Status &Ret_pStatus, DbRecordSet &RetOut_Data,QString strLockRes,QString strOldView,int nGridID) = 0;
	virtual void Read(Status &Ret_pStatus, int nGridID,DbRecordSet &Ret_Data) = 0;
	virtual void Delete(Status &Ret_pStatus, QString strView,int nGridID) = 0;
	virtual void Lock(Status &Ret_pStatus, QString strView,QString &Ret_strLockRes,int nGridID) = 0;
	virtual void Unlock(Status &Ret_pStatus, QString strLockRes) = 0;

};


#endif // INTERFACE_BUSGRIDVIEWS_H
