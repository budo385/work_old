#ifndef INTERFACE_BusSms_H
#define INTERFACE_BusSms_H


#include "common/common/status.h"
#include "common/common/dbrecordset.h"

/*!
\class Interface_BusSms
\brief Class to handle sms events (Skype,TAPI, ...)
\ingroup Bus_Interface_Collection


*/

class Interface_BusSms
{
public:
	virtual ~Interface_BusSms(){};

	virtual void ReadSms(Status &Ret_pStatus, int nVoiceCallID,DbRecordSet &Ret_CallData, DbRecordSet &Ret_UAR,DbRecordSet &Ret_GAR)=0;
	virtual void WriteSms(Status &Ret_pStatus, DbRecordSet &RetOut_CallData, QString strLockRes,DbRecordSet &RetOut_ScheduleTask,DbRecordSet &RetOut_UAR,DbRecordSet &RetOut_GAR)=0;
	virtual void ListSmsMessages(Status &Ret_pStatus, DbRecordSet &RetOut_lstCallData, QDate &datDate, int nOwnerID) = 0;
	virtual void DeleteSmsMessages(Status &Ret_pStatus, DbRecordSet &RetOut_lstIDs) = 0;
};


#endif //INTERFACE_BusSms_H
