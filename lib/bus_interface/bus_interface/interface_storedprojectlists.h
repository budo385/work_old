#ifndef INTERFACE_StoredProjectLists_H
#define INTERFACE_StoredProjectLists_H

#include "common/common/status.h"
#include "common/common/dbrecordset.h"


/*!
	\class Interface_StoredProjectList
	\brief Interface for importing business data
	\ingroup Bus_Interface_Collection
*/

class Interface_StoredProjectLists
{
public:
	virtual ~Interface_StoredProjectLists(){};

	virtual void GetListNames(Status &Ret_pStatus, DbRecordSet &Ret_Data) = 0;
	virtual void DeleteList(Status &Ret_pStatus, int nListID) = 0;
	virtual void CreateList(Status &Ret_pStatus, QString strListName, DbRecordSet &lstData, int &RetOut_nListID, bool bIsSelection, QString strSelectionXML, bool bIsFilter, QString strFilterXML) = 0;
	virtual void GetListData(Status &Ret_pStatus, int nListID, DbRecordSet &Ret_lstData, bool &Ret_bIsSelection, QString &Ret_strSelectionXML, bool &Ret_bIsFilter, QString &Ret_strFilterXML, bool bGetDescOnly) = 0;
	virtual void SetListData(Status &Ret_pStatus, int nListID, DbRecordSet &lstData, bool bIsSelection, QString strSelectionXML, bool bIsFilter, QString strFilterXML) = 0;
	virtual void GetListDataLevelByLevel(Status &Ret_pStatus, int nListID, DbRecordSet &Ret_lstData, QString strParentProjectCode="", int nLevelsDeep=-1) =  0;
};

#endif // INTERFACE_StoredProjectList_H
