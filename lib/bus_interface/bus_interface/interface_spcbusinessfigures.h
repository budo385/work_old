#ifndef INTERFACE_SPCBUSINESSFIGURES_H
#define INTERFACE_SPCBUSINESSFIGURES_H

#include "common/common/status.h"
#include "common/common/dbrecordset.h"

class Interface_SpcBusinessFigures
{
public:
	virtual ~Interface_SpcBusinessFigures(){};

	virtual void Routine_275(Status& Ret_Status, DbRecordSet &Ret_lstData, QString strPersonNumber, QDate datFrom, QDate datTo, QString strProjCode="", int nIncl_Prov_Pr=0, int nIncl_Prov_Tsk=0)=0;
};

/*
<Web_service_meta_data>
	<Routine_275>
		<URL>/spc_businessfigures/routine_275</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
	</Routine_275>
</Web_service_meta_data>
*/

#endif // INTERFACE_WEBSERVER_H
