#ifndef INTERFACE_REPORTS_H
#define INTERFACE_REPORTS_H

#include "common/common/status.h"
#include "common/common/dbrecordset.h"

/*!
\class Interface_Reports
\brief Reports access to data class.
\ingroup Bus_Interface_Collection


*/

class Interface_Reports
{
public:
	virtual ~Interface_Reports(){};

	virtual void GetReportRecordSet(Status &Ret_pStatus, DbRecordSet &Ret_pReportRecordSet, int nViewID, QString strWhere) = 0;
    
};

#endif // INTERFACE_REPORTS_H
