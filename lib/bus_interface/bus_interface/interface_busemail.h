#ifndef INTERFACE_BUSEMAIL_H
#define INTERFACE_BUSEMAIL_H


#include "common/common/status.h"
#include "common/common/dbrecordset.h"

/*!
\class Interface_BusEmailCenter
\brief Class to handle emails
\ingroup Bus_Interface_Collection


*/

class Interface_BusEmail
{
public:
	virtual ~Interface_BusEmail(){};

	virtual void Read(Status &Ret_pStatus, int nEmailID, DbRecordSet &RetOut_Data, DbRecordSet &Ret_ContactLink,DbRecordSet &Ret_ScheduleTask,DbRecordSet &Ret_ContactEmails, DbRecordSet &Ret_Attachments,DbRecordSet &Ret_UAR,DbRecordSet &Ret_GAR )=0;
	virtual void Write(Status &Ret_pStatus, DbRecordSet &RetOut_Data, DbRecordSet &RetOut_ContactLink, DbRecordSet &RetOut_ScheduleTask, DbRecordSet &RetOut_Attachments,QString strLockRes,DbRecordSet &RetOut_UAR,DbRecordSet &RetOut_GAR)=0;
	virtual void WriteMultiple(Status &Ret_pStatus, DbRecordSet &RetOut_Data, DbRecordSet &RetOut_ContactLink, DbRecordSet &RetOut_ScheduleTask, DbRecordSet &RetOut_Attachments,QString strLockRes,bool bSkipExisting, int &RetOut_nSkippedOrReplacedCnt, bool bAddDupsAsNewRow)=0;
	virtual void WriteEmailLog(Status &Ret_pStatus, DbRecordSet &RetOut_Data, QString strLockRes)=0;
	virtual void List(Status &Ret_pStatus, DbRecordSet &RetOut_lstData, QDate &datDateFrom, QDate &datDateTo, int nOwnerID, int nFilterID) = 0;
	virtual void Delete(Status &Ret_pStatus, DbRecordSet &RetOut_lstIDs) = 0;
	virtual void UpdateEmailFromClient(Status &Ret_pStatus, DbRecordSet &Emails, DbRecordSet &Attachments)=0;
	virtual void SetEmailRead(Status &Ret_pStatus, int nCE_ID)=0;
	virtual void WriteAttachments(Status &Ret_pStatus,int nEmailID, DbRecordSet &RetOut_Attachments)=0;
	virtual void WriteEmailAttachmentsFromUserStorage(Status &Ret_pStatus,int nEmailID, DbRecordSet &Attachments)=0;
};


#endif //INTERFACE_BusEmail_H
