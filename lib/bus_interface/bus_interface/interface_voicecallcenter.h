#ifndef INTERFACE_VOICECALLCENTER_H
#define INTERFACE_VOICECALLCENTER_H


#include "common/common/status.h"
#include "common/common/dbrecordset.h"

/*!
\class Interface_VoiceCallCenter
\brief Class to handle voice call events (Skype,TAPI, ...)
\ingroup Bus_Interface_Collection


*/

class Interface_VoiceCallCenter
{
public:
	virtual ~Interface_VoiceCallCenter(){};

	virtual void ReadCall(Status &Ret_pStatus, int nVoiceCallID,DbRecordSet &Ret_CallData, DbRecordSet &Ret_UAR,DbRecordSet &Ret_GAR)=0;
	virtual void WriteCall(Status &Ret_pStatus, DbRecordSet &RetOut_CallData, QString strLockRes,DbRecordSet &RetOut_ScheduleTask,DbRecordSet &RetOut_UAR,DbRecordSet &RetOut_GAR)=0;
	virtual void ListCalls(Status &Ret_pStatus, DbRecordSet &RetOut_lstCallData, QDate &datDate, int nOwnerID) = 0;
	virtual void DeleteCalls(Status &Ret_pStatus, DbRecordSet &RetOut_lstIDs) = 0;
};


#endif //INTERFACE_VOICECALLCENTER_H
