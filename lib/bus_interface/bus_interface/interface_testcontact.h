#ifndef INTERFACE_TESTCONTACT_H
#define INTERFACE_TESTCONTACT_H

#include <QtCore>
#include "common/common/status.h"
#include "db_core/db_core/dbcore_connectionfaker.h"
#include "db_core/db_core/dbrecordset.h"



/*!
	\class Interface_TestContact
	\brief Bo Entity main interface
	\ingroup Bus_Interface_Collection
*/

class Interface_TestContact
{
public:
	virtual ~Interface_TestContact(){};

	virtual void LoadContacts(Status &Ret_pStatus, DbRecordSet &Ret_Data,SQLDBPointer* pDbConn = NULL) = 0;
};


#endif //INTERFACE_TESTCONTACT_H
