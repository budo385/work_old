#ifndef INTERFACE_COLLECTION_H
#define INTERFACE_COLLECTION_H


//-----------------------------------------------------------
//Holds header for all bussiness method interfaces
//-----------------------------------------------------------

#include "interface_userlogon.h"
#include "interface_modulelicense.h"
#include "interface_buseventlog.h"
#include "interface_accessrights.h"
#include "interface_mainentityselector.h"
#include "interface_coreservices.h"
#include "interface_buscontact.h"
#include "interface_reports.h"
#include "interface_voicecallcenter.h"
#include "interface_busaddressschemas.h"
#include "interface_clientsimpleorm.h"
#include "interface_busimport.h"
#include "interface_storedprojectlists.h"
#include "interface_busgridviews.h"
#include "interface_busnmrx.h"
#include "interface_busgrouptree.h"
#include "interface_busemail.h"
#include "interface_busdocuments.h"
#include "interface_buscommunication.h"
#include "interface_servercontrol.h"
#include "interface_busproject.h"
#include "interface_busperson.h"
#include "interface_bussms.h"
#include "interface_buscalendar.h"
#include "interface_webcontacts.h"
#include "interface_webdocuments.h"
#include "interface_webemails.h"
#include "interface_webserver.h"
#include "interface_webprojects.h"
#include "interface_webcalendars.h"
#include "interface_websessiondata.h"
#include "interface_webcommunication.h"
#include "interface_buscustomfields.h"
#include "interface_spcbusinessfigures.h"
#include "interface_spcsession.h"
#include "interface_webmwcloudservice.h"

#endif //INTERFACE_COLLECTION_H
