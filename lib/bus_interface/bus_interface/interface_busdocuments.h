#ifndef INTERFACE_BUSDOCUMENTS_H
#define INTERFACE_BUSDOCUMENTS_H


#include "common/common/status.h"
#include "common/common/dbrecordset.h"

/*!
	\class Interface_BusDocuments
	\brief Class to handle documents
	\ingroup Bus_Interface_Collection
*/

class Interface_BusDocuments
{
public:
	virtual ~Interface_BusDocuments(){};

	virtual void ReadUserPaths(Status &Ret_pStatus, int nPersonID,DbRecordSet &Ret_Data) = 0;
	virtual void ReadUserPathsForApplication(Status &Ret_pStatus, int nApplicationID,DbRecordSet &Ret_Data)=0;
	virtual void ReadTemplates(Status &Ret_pStatus, int nPersonID,DbRecordSet &Ret_Data) = 0;
	virtual void Write(Status &Ret_pStatus, DbRecordSet &RetOut_Data, DbRecordSet &RetOut_ProjectLink,DbRecordSet &RetOut_ContactLink,DbRecordSet &RetOut_ScheduleTask,DbRecordSet &RetOut_DocumentRevision,QString strLockRes,DbRecordSet &RetOut_UAR,DbRecordSet &RetOut_GAR)=0;
	virtual void Read(Status &Ret_pStatus, int nDocumentID, DbRecordSet &Ret_Data, DbRecordSet &Ret_ProjectLink,DbRecordSet &Ret_ContactLink,DbRecordSet &Ret_ScheduleTask,DbRecordSet &Ret_Revisions, DbRecordSet &Ret_CheckOutInfo,DbRecordSet &Ret_UAR,DbRecordSet &Ret_GAR)=0;
	virtual void DeleteDocument(Status &Ret_pStatus, int nDocumentID,QString strLockRes="")=0;
	virtual void WriteApplication(Status &Ret_pStatus, DbRecordSet &RetOut_Data,QString strLockRes)=0;
	virtual void ReadNoteDocument(Status &Ret_pStatus, int nDocumentID, QString &Ret_strData) =0;

	virtual void ReloadRevisions(Status &Ret_pStatus, int nDocumentID, DbRecordSet &Ret_CheckOutInfo, DbRecordSet &Ret_Revisions)=0;
	virtual void CheckIn(Status &Ret_pStatus, int nDocumentID, DbRecordSet &RetOut_Destroy_DocumentRevision,bool bOverWrite,DbRecordSet &Ret_CheckOutInfo, bool bSkipLock,int nSize)=0;
	virtual void CheckOut(Status &Ret_pStatus, int nDocumentID, int nCheckOutPersonID,DbRecordSet &Ret_DocumentRevision,bool bReadOnly,DbRecordSet &Ret_CheckOutInfo,QString strCheckOutPath,bool bSkipLock,int nRevisionID)=0;
	virtual void CheckOutToUserStorage(Status &Ret_pStatus, int nDocumentID, int nCheckOutPersonID,bool bReadOnly,DbRecordSet &Ret_DocumentRevision,DbRecordSet &Ret_CheckOutInfo,QString strCheckOutPath="",bool bSkipLock=false,int nRevisionID=-1)=0;
	virtual void CheckInFromUserStorage(Status &Ret_pStatus, int nDocumentID, int nCheckInPersonID,bool bOverWrite,DbRecordSet &Ret_DocumentRevision,DbRecordSet &Ret_CheckOutInfo, bool bSkipLock=false,QString strTag="", QString strSubDirectory="")=0;
	virtual void CheckInFromEmailAttachment(Status &Ret_pStatus, int nAttachmentID, int nOperation, int nProjectID, int nContactID,DbRecordSet &RetOut_DocumentIDs)=0;

	virtual void CancelCheckOut(Status &Ret_pStatus, int nDocumentID, int nCheckOutPersonID,bool bReadOnly)=0;
	virtual void CheckOutRevision(Status &Ret_pStatus, int nRevisionID, DbRecordSet &Ret_DocumentRevision)=0;
	virtual void UpdateRevision(Status &Ret_pStatus, int nRevisionID, QString strRevisionTag)=0;
	virtual void DeleteRevision(Status &Ret_pStatus, int nDocumentID,int nRevisionID, bool bLock)=0;
	virtual void IsCheckedOut(Status &Ret_pStatus, int nDocumentID, int &Ret_nCheckOutPersonID,QString &Ret_strCheckedOutPath)=0;
	virtual void GetDocumentFileNameAndSize(Status &Ret_pStatus, int nDocumentID, QString &Ret_strFileName, int &Ret_nSize,int nRevisionID=-1)=0;
	virtual void GetDocumentProjectAndContact(Status &Ret_pStatus, int nDocumentID, int &Ret_nProjectID,int &Ret_nContactID)=0;
	virtual void SetCheckOutPath(Status &Ret_pStatus, int nDocumentID, QString strPath)=0;
};


#endif //INTERFACE_BusEmail_H