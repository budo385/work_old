#ifndef INTERFACE_BUSPROJECT_H
#define INTERFACE_BUSPROJECT_H

#include "common/common/status.h"
#include "common/common/dbrecordset.h"


/*!
	\class Interface_BusProject
	\brief Interface for Projects
	\ingroup Bus_Interface_Collection
*/

class Interface_BusProject
{
public:
	virtual ~Interface_BusProject(){};
	virtual void ReadProjectDataSideBar(Status &Ret_pStatus, int nProjectID,DbRecordSet &Ret_Data,DbRecordSet &DataNMRXPersonFilter,DbRecordSet &Ret_DataNMRXPerson)=0;
	virtual void NewProjectsFromTemplate(Status &Ret_pStatus, DbRecordSet &RetOut_Data, bool bCopyCE)=0;
	virtual void ReadData(Status &Ret_pStatus, int nProjectID,DbRecordSet &Ret_Data,DbRecordSet &DataNMRXPersonFilter,DbRecordSet &Ret_DataNMRXPerson,DbRecordSet &Ret_UAR,DbRecordSet &Ret_GAR, DbRecordSet &Ret_CustomFlds)=0;
	virtual void WriteData(Status &Ret_pStatus,DbRecordSet &RetOut_Data, QString pLockResourceID, DbRecordSet &RetOut_UAR,DbRecordSet &RetOut_GAR, DbRecordSet &RetOut_CustomFlds)=0;
};

#endif // INTERFACE_BUSPROJECT_H