#ifndef INTERFACE_BUSNMRX_H
#define INTERFACE_BUSNMRX_H


#include "common/common/status.h"
#include "common/common/dbrecordset.h"


/*!
	\class Interface_BusNMRX
	\brief Interface for importing business data
	\ingroup Bus_Interface_Collection
*/

class Interface_BusNMRX
{
public:
	virtual ~Interface_BusNMRX(){};
	//NMRX:
	virtual void ReadOnePair(Status &Ret_pStatus, int nM_TableID,int nN_TableID, int nKey1,int nKey2,DbRecordSet &Ret_Data,int nX_TableID=-1) = 0;
	virtual void ReadMultiPairs(Status &Ret_pStatus, DbRecordSet &DataPairs, DbRecordSet &Ret_Data,int nX_TableID=-1) = 0;
	virtual void Write(Status &Ret_pStatus, DbRecordSet &RetOut_Data,QString strLockRes,int nX_TableID=-1) = 0;
	virtual void WriteSimple(Status &Ret_pStatus, DbRecordSet &RetOut_Data,QString strLockRes,int nX_TableID=-1) = 0;
	virtual void Delete(Status &Ret_pStatus, DbRecordSet &DataForDelete) = 0;
	virtual void Lock(Status &Ret_pStatus, int nNMRX_ID,QString &Ret_strLockRes) = 0;
	virtual void LockMulti(Status &Ret_pStatus, DbRecordSet lstLockData,QString &Ret_strLockRes) = 0;
	virtual void Unlock(Status &Ret_pStatus, QString strLockRes) = 0;
	virtual void WriteRelations(Status &Ret_pStatus, DbRecordSet lstRelations, int nTable1_ID, int nTable2_ID)=0;
	virtual void WriteCEContactLink(Status &Ret_pStatus, DbRecordSet lstCeIDs, DbRecordSet &RetOut_Data)=0;
	virtual void WriteCEProjectLink(Status &Ret_pStatus, DbRecordSet lstCeIDs, DbRecordSet &RetOut_Data)=0;
	virtual void CreateAssignmentData(Status &Ret_pStatus,DbRecordSet &Ret_lstNMRXData,int nMasterTableID,int nAssigmentTableID,DbRecordSet &lstIDForAssign, QString strDefaultRoleName,int nMasterTableRecordID,int nX_TableID, DbRecordSet &lstPersons,bool &Ret_bInserted)=0;

	//ROLE
	virtual void ReadRole(Status &Ret_pStatus, int nN_TableID,int nM_TableID, DbRecordSet &Ret_Data) = 0;
	virtual void WriteRole(Status &Ret_pStatus, DbRecordSet &RetOut_Data,DbRecordSet &DataForDelete,QString strLockRes) = 0;
	virtual void DeleteRole(Status &Ret_pStatus, DbRecordSet &DataForDelete) = 0;
	virtual void LockRole(Status &Ret_pStatus, int nN_TableID,int nM_TableID, QString &Ret_strLockRes) = 0;
	virtual void UnlockRole(Status &Ret_pStatus, QString strLockRes) = 0;
	virtual void CheckRole(Status &Ret_pStatus, int nN_TableID,int nM_TableID,QString strRoleName, DbRecordSet &Ret_Data, bool &Ret_bRoleInserted) = 0;
};

#endif // INTERFACE_BUSNMRX_H
