#ifndef INTERFACE_WEBCALENDARS_H
#define INTERFACE_WEBCALENDARS_H

#include "common/common/status.h"
#include "common/common/dbrecordset.h"

class Interface_WebCalendars
{
public:
	virtual ~Interface_WebCalendars(){};

	virtual void ReadCalendars(Status &Ret_pStatus, int &Ret_nTotalCount, DbRecordSet &Ret_Calendars, QDate datFrom, QDate datTo,QString &Ret_strFrom, QString &Ret_strTo) =0; 

};

/*

<Web_service_meta_data>
	
	<ReadCalendars>
		<URL>/calendar</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
		<Ret_nTotalCount>
			<DISPLAY_NAME>nTotalCount</DISPLAY_NAME>
		</Ret_nTotalCount>
		<Ret_Calendars>
			<DISPLAY_NAME>Calendars</DISPLAY_NAME>
			<ROW_NAME></ROW_NAME>
		</Ret_Calendars>
	</ReadCalendars>
	
</Web_service_meta_data>
*/


#endif // INTERFACE_WEBCALENDARS_H
