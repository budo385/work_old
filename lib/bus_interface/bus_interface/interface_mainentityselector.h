#ifndef MAIN_ENTITY_SELECTOR_H
#define MAIN_ENTITY_SELECTOR_H



#include "common/common/status.h"
#include "common/common/dbrecordset.h"


/*!
	\class Interface_MainEntitySelector
	\brief Interface for selecting all main data entities (persons, projects, etc)
	\ingroup Bus_Interface_Collection
*/

class Interface_MainEntitySelector
{
public:
	virtual ~Interface_MainEntitySelector(){};

	virtual void ReadData(Status &Ret_pStatus, int nEntityID, DbRecordSet Filter, DbRecordSet &Ret_Data) = 0;
	virtual void ReadDataBatch(Status &Ret_pStatus, DbRecordSet lstEntityIDs, DbRecordSet lstFilters, DbRecordSet &Ret_Data)=0;
	//special use by SAPNE patterns:
	virtual void ReadDataAll(Status &Ret_pStatus, int nEntityID,int nCountInClientCache, QDateTime datClientCacheTime, DbRecordSet &Ret_Data)=0;
	virtual void ReadDataOne(Status &Ret_pStatus, int nEntityID,DbRecordSet Filter, DbRecordSet &Ret_Data)=0;

};



#endif //MAIN_ENTITY_SELECTOR_H
