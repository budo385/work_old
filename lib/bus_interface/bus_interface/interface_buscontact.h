#ifndef CONTACT_SELECTOR_H
#define CONTACT_SELECTOR_H


#include "common/common/status.h"
#include "common/common/dbrecordset.h"

/*!
	\class Interface_BusContact
	\brief Interface for selecting contacts
	\ingroup Bus_Interface_Collection
*/

class Interface_BusContact
{
public:
	virtual ~Interface_BusContact(){};

	virtual void ReadShortContactList(Status &Ret_pStatus, DbRecordSet Filter, DbRecordSet &Ret_Data) = 0;
	virtual void ReadContactDataSideBar(Status &Ret_pStatus, int nContactID,DbRecordSet &Ret_Data,DbRecordSet &DataNMRXPersonFilter,DbRecordSet &Ret_DataNMRXPerson,DbRecordSet &DataNMRXProjectFilter,DbRecordSet &Ret_DataNMRXProject,bool bLoadPicture, DbRecordSet &Ret_Pictures)=0;
	virtual	void DeleteContacts(Status &Ret_pStatus, DbRecordSet &DataForDelete,bool bLock)=0;
	virtual void BatchReadContactData(Status &Ret_pStatus, int nContactID,DbRecordSet &Ret_Data,DbRecordSet &DataNMRXPersonFilter,DbRecordSet &Ret_DataNMRXPerson,DbRecordSet &DataNMRXProjectFilter,DbRecordSet &Ret_DataNMRXProject,int nSubDataFilter,DbRecordSet &Ret_UAR,DbRecordSet &Ret_GAR)=0;
	virtual	void ReadNMRXContactsFromContact(Status &Ret_pStatus, int nContactID,DbRecordSet &Ret_Data)=0;
	virtual void ReadNMRXContactsFromContact_Ext(Status &Ret_pStatus, int nContactID,DbRecordSet &Ret_NMRXData,DbRecordSet &Ret_ContactData)=0;
	virtual void ReadNMRXUsersFromContact_Ext(Status &Ret_pStatus, int nContactID,DbRecordSet &Ret_NMRXData,DbRecordSet &Ret_UserData)=0;
	virtual	void ReadNMRXContactsFromProject(Status &Ret_pStatus, int nProjectID,DbRecordSet &Ret_Data)=0;
	virtual	void ReadNMRXProjects(Status &Ret_pStatus, int nContactID,DbRecordSet &Ret_Data)=0;
	virtual void ReadContactDefaults(Status &Ret_pStatus, int nContactID, DbRecordSet &Ret_Data)=0;
	virtual void ReadContactReminderData(Status &Ret_pStatus, int nContactID, int nPersonID,QString strEmail, QString strMobilePhoneNumber)=0;
	virtual void ReadContactDataForVoiceCall(Status &Ret_pStatus, int nContactID, DbRecordSet &Ret_Phones,DbRecordSet &Ret_Pictures,DbRecordSet &Ret_NMRXContacts,DbRecordSet &Ret_NMRXProjects)=0;
	virtual void UpdateOrganization(Status &Ret_pStatus,QString strPrevOrg,QString strNewOrg)=0;
	virtual void AddContactEmail(Status &Ret_pStatus, DbRecordSet &RetOut_EmailData)=0;
	virtual void CreatePersonFromContact(Status &Ret_pStatus, DbRecordSet &lstContacts,int nOrgID, int &Ret_nPersonCreated)=0;
	
	//pic general
	virtual void ReadBigPicture(Status &Ret_pStatus, int nBigPicID,DbRecordSet &Ret_Data) = 0;
	virtual void WriteBigPicture(Status &Ret_pStatus, DbRecordSet &RetOut_Data) = 0;
	virtual void WriteEntityPictures(Status &Ret_pStatus, DbRecordSet &RetOut_Data, QString strBigPicIDColName, QString strPreviewPicColName)=0;
	virtual void ReadEntityPictures(Status &Ret_pStatus,DbRecordSet &Ret_Data,QString strBigPicIDColName)=0;
	//contact pic_
	virtual void ReadPreviewPicture(Status &Ret_pStatus, int nContactID,DbRecordSet &Ret_Data) = 0;
	virtual void AssignPictureContact(Status &Ret_pStatus, int nContactID, QByteArray &PreviewPicture,QByteArray &BigPicture, int &Ret_nBigPicID)=0;
	virtual void RemoveAssignedPictureContact(Status &Ret_pStatus, int nContactID)=0;
	//journal:
	virtual void ReadContactJournals(Status &Ret_pStatus, int nContactID, DbRecordSet &Ret_Data) = 0;
	virtual void WriteJournal(Status &Ret_pStatus, DbRecordSet &RetOut_Data,DbRecordSet &DataForDelete,QString strLockRes) = 0;
	virtual void DeleteJournal(Status &Ret_pStatus, DbRecordSet &DataForDelete,bool bLock) = 0;
	virtual void LockJournal(Status &Ret_pStatus, int nJournalID, QString &Ret_strLockRes) = 0;
	virtual void UnlockJournal(Status &Ret_pStatus, QString strLockRes) = 0;
	//Create person from contact (used in startup):
	virtual void CreateDefaultPerson(Status &Ret_pStatus,int nContactID)=0;
	virtual void CopyPerson2OrgContact(Status &Ret_pStatus,DbRecordSet Contacts,DbRecordSet lstGroups, DbRecordSet &Ret_Data)=0;
	virtual void FindDuplicates(Status &Ret_pStatus,DbRecordSet &Contacts,DbRecordSet &Ret_Data) =0;
	virtual void GetMaximumDebtorCode(Status &Ret_pStatus,QString &Ret_DebtorCode)=0;
	//generic read/write:
	virtual void ReadData(Status &Ret_pStatus, DbRecordSet Filter, DbRecordSet &Ret_Data,int nSubDataFilter=0) = 0;
	virtual void WriteData(Status &Ret_pStatus,DbRecordSet &RetOut_Data,int nSubDataFilter, QString pLockResourceID, DbRecordSet &RetOut_UAR,DbRecordSet &RetOut_GAR) = 0;
	virtual void WriteDataFromImport(Status &Ret_pStatus,DbRecordSet &RetOut_Data)=0;

	virtual void WriteGroupAssignments(Status &Ret_pStatus,DbRecordSet &RetOut_Data) = 0;
	virtual void ReadContactGroup(Status &Ret_pStatus, int nContactID, DbRecordSet &Ret_Data)=0;
	virtual void ReadContactsFromGroup(Status &Ret_pStatus, QString strUniqueGroupIdentifier,DbRecordSet &Ret_Data) = 0;
	virtual void ReadActiveContactsFromGroups(Status &Ret_pStatus, QString strGroupIDs,DbRecordSet &Ret_Data) = 0;

	//Phones:
	virtual void WritePhones(Status &Ret_pStatus,int nContactID,DbRecordSet &RetOut_Data) = 0;
	virtual void ReadContactPhones(Status &Ret_pStatus, int nContactID,DbRecordSet &Ret_Data, int nQueryRead=-1,DbRecordSet lstBySystemType=DbRecordSet())=0;

	//Address
	virtual void ReadContactAddress(Status &Ret_pStatus, DbRecordSet &lstOfContactIds, DbRecordSet &Ret_Data) = 0;
	virtual void WriteAddress(Status &Ret_pStatus,int nContactID, DbRecordSet &RetOut_Data) = 0;
	virtual void GetAllContactsOfOrganizationByAddress(Status &Ret_pStatus, int nContactIDOrg, DbRecordSet rowAddress,DbRecordSet &Ret_AddressIds) = 0;
	virtual void UpdateContactsOfOrganizationByAddress(Status &Ret_pStatus, DbRecordSet &lstAddressIds, DbRecordSet rowAddress) = 0;

	virtual void AssignDebtors2Project(Status &Ret_pStatus, int nProjectID, DbRecordSet &lstDebtors) = 0;

};



#endif //CONTACT_SELECTOR_H
