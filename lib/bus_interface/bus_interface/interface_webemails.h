#ifndef INTERFACE_WEBEMAILS_H
#define INTERFACE_WEBEMAILS_H

#include "common/common/status.h"
#include "common/common/dbrecordset.h"

class Interface_WebEmails
{
public:
	virtual ~Interface_WebEmails(){};

	virtual void ReadEmails(Status &Ret_pStatus, int &Ret_nTotalCount, DbRecordSet &Ret_Emails, int nFromN=-1, int nToN=-1)=0;
	virtual void SearchEmails(Status &Ret_pStatus, int &Ret_nTotalCount,DbRecordSet &Ret_Emails,  int nSortOrder=-1, int nFromN=-1, int nToN=-1, int nContactID=-1, int nProjectID=-1,  int nOwnerUserID=-1, QDate datFrom=QDate(), QDate datTo=QDate(), QString strEmailAcc="", QString strEmailSender="", int nMailBox=0)=0;
	virtual void ReadEmailDetails(Status &Ret_pStatus, int nEmailID, DbRecordSet &Ret_Email, DbRecordSet &Ret_Attachments)=0;
	virtual void GetAttachmentURL(Status &Ret_pStatus, int nAttachmentID, QString &Ret_strDocumentURL)=0;
	virtual void GetAttachmentUnzippedFiles(Status &Ret_pStatus, int nAttachmentID, DbRecordSet &Ret_AttachmentFiles)=0;
	virtual void ReadEmailAsPdf(Status &Ret_pStatus, int nEmailID,QString &Ret_strDocumentURL)=0;
	virtual void ReadEmailAsHtml(Status &Ret_pStatus, int nEmailID,QString &Ret_strDocumentURL)=0;
	virtual void ReadEmailAsPdfBinary(Status &Ret_pStatus, int nEmailID,QByteArray &Ret_byteData)=0;
	virtual void ReadEmailAsHtmlBinary(Status &Ret_pStatus, int nEmailID,QByteArray &Ret_byteData)=0;
	virtual void GetAttachmentBinary(Status &Ret_pStatus, int nAttachmentID, QByteArray &Ret_byteData)=0;

};

/*
<Web_service_meta_data>
	
	<ReadEmailDetails>
		<URL>/email/[RES_ID]</URL>
		<RES_ID>nEmailID</RES_ID>
		<HTTP_METHOD>GET</HTTP_METHOD>
		<Ret_Email>
			<DISPLAY_NAME>Emails</DISPLAY_NAME>
			<ROW_NAME>email</ROW_NAME>
		</Ret_Email>
		<Ret_Attachments>
			<DISPLAY_NAME>Attachment</DISPLAY_NAME>
			<ROW_NAME>document</ROW_NAME>
		</Ret_Attachments>
	</ReadEmailDetails>

	<ReadEmailAsPdf>
		<URL>/email/[RES_ID]/pdf</URL>
		<RES_ID>nEmailID</RES_ID>
		<HTTP_METHOD>GET</HTTP_METHOD>
		<Ret_strDocumentURL><DISPLAY_NAME>strDocumentURL</DISPLAY_NAME></Ret_strDocumentURL>
	</ReadEmailAsPdf>

	<ReadEmailAsHtml>
		<URL>/email/[RES_ID]/html</URL>
		<RES_ID>nEmailID</RES_ID>
		<HTTP_METHOD>GET</HTTP_METHOD>
		<Ret_strDocumentURL><DISPLAY_NAME>strDocumentURL</DISPLAY_NAME></Ret_strDocumentURL>
	</ReadEmailAsHtml>

	<ReadEmails>
		<URL>/email</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
		<Ret_nTotalCount><DISPLAY_NAME>nTotalCount</DISPLAY_NAME></Ret_nTotalCount>
		<Ret_Emails>
			<DISPLAY_NAME>Emails</DISPLAY_NAME>
			<ROW_NAME>email</ROW_NAME>
		</Ret_Emails>
	</ReadEmails>

	<SearchEmails>
		<URL>/email/search</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
		<Ret_nTotalCount><DISPLAY_NAME>nTotalCount</DISPLAY_NAME></Ret_nTotalCount>
		<Ret_Emails>
			<DISPLAY_NAME>Emails</DISPLAY_NAME>
			<ROW_NAME>email</ROW_NAME>
		</Ret_Emails>
	</SearchEmails>

	<GetAttachmentURL>
		<URL>/email/attachment/[RES_ID]</URL>
		<RES_ID>nAttachmentID</RES_ID>
		<HTTP_METHOD>GET</HTTP_METHOD>
		<Ret_strDocumentURL><DISPLAY_NAME>strDocumentURL</DISPLAY_NAME></Ret_strDocumentURL>
	</GetAttachmentURL>

	<GetAttachmentUnzippedFiles>
		<URL>/email/attachment/[RES_ID]/unzip</URL>
		<RES_ID>nAttachmentID</RES_ID>
		<HTTP_METHOD>GET</HTTP_METHOD>
		<Ret_AttachmentFiles>
			<DISPLAY_NAME>Attachment</DISPLAY_NAME>
			<ROW_NAME>document</ROW_NAME>
		</Ret_AttachmentFiles>
	</GetAttachmentUnzippedFiles>

	<GetAttachmentBinary>
		<URL>/email/attachment/[RES_ID]/binary</URL>
		<RES_ID>nAttachmentID</RES_ID>
		<HTTP_METHOD>GET</HTTP_METHOD>
	</GetAttachmentBinary>

	<ReadEmailAsPdfBinary>
		<URL>/email/[RES_ID]/pdf/binary</URL>
		<RES_ID>nEmailID</RES_ID>
		<HTTP_METHOD>GET</HTTP_METHOD>
	</ReadEmailAsPdfBinary>

	<ReadEmailAsHtmlBinary>
		<URL>/email/[RES_ID]/html/binary</URL>
		<RES_ID>nEmailID</RES_ID>
		<HTTP_METHOD>GET</HTTP_METHOD>
	</ReadEmailAsHtmlBinary>
	
</Web_service_meta_data>
*/

#endif // INTERFACE_WEBEMAILS_H