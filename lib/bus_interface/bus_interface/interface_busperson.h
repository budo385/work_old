#ifndef INTERFACE_BUSPERSON_H
#define INTERFACE_BUSPERSON_H

#include "common/common/status.h"
#include "common/common/dbrecordset.h"

class Interface_BusPerson
{
public:
	virtual ~Interface_BusPerson(){};

	virtual void ReadData(Status &Ret_pStatus, int nPersonID, DbRecordSet &Ret_Data,DbRecordSet &Ret_CoreUserData, DbRecordSet &Ret_RoleList)=0;
	virtual void WriteData(Status &Ret_pStatus, DbRecordSet &RetOut_Data, DbRecordSet &RetOut_CoreUserData, QString pLockResourceID="", int nPersonRoleId=-1)=0;
	virtual void GetPersonProjectListID(Status &Ret_pStatus, int nPersonID,int &Ret_nListID)=0;
};

#endif // INTERFACE_BUSPERSON_H