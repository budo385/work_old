#ifndef BUSINESSSERVICESETINTERFACE_H
#define BUSINESSSERVICESETINTERFACE_H


//Add all business object interface headers:
#include "interface_collection.h"


/*!
    \class Interface_BusinessServiceSet
    \brief Abstract class for defining interface for business methods
	\ingroup Bus_Interface

	Use:
	- declare interface for your business objcect (just interface methods as pure virtual)
	- include header here
	- add pointer of business object type, named same as business object itself (so it acn be referenced from outside g_BoSet->businessobject->method

*/

class Interface_BusinessServiceSet
{
public:

	virtual ~Interface_BusinessServiceSet(){};

	//Add all business object as pointers. NOTE: name must be same as business object itself
	Interface_UserLogon*			UserLogon;
	Interface_ModuleLicense*		ModuleLicense;
	Interface_BusEventLog*			BusEventLog;
	Interface_AccessRights*			AccessRights;
	Interface_MainEntitySelector*	MainEntitySelector;
	Interface_CoreServices*			CoreServices;
	Interface_BusContact*			BusContact;
	Interface_Reports*				Reports;
	Interface_VoiceCallCenter*		VoiceCallCenter;
	Interface_BusEmail*				BusEmail;
	Interface_BusAddressSchemas*	BusAddressSchemas;
	Interface_ClientSimpleORM*		ClientSimpleORM;
	Interface_BusImport*			BusImport;
	Interface_StoredProjectLists*	StoredProjectLists;
	Interface_BusGridViews*			BusGridViews;
	Interface_BusNMRX*				BusNMRX;	
	Interface_BusGroupTree*			BusGroupTree;	
	Interface_BusDocuments*			BusDocuments;
	Interface_BusCommunication*		BusCommunication;
	Interface_ServerControl*		ServerControl;
	Interface_BusProject*			BusProject;
	Interface_BusPerson*			BusPerson;
	Interface_BusSms*				BusSms;
	Interface_BusCalendar*			BusCalendar;
	Interface_WebContacts*			WebContacts;
	Interface_WebDocuments*			WebDocuments;
	Interface_WebEmails*			WebEmails;
	Interface_WebServer*			WebServer;
	Interface_WebProjects*			WebProjects;
	Interface_WebCalendars*			WebCalendars;
	Interface_WebSessionData*		WebSessionData;
	Interface_WebCommunication*		WebCommunication;
	Interface_BusCustomFields*		BusCustomFields;
	Interface_SpcSession*			SpcSession;
	Interface_SpcBusinessFigures*	SpcBusinessFigures;
	Interface_WebMWCloudService*	WebMWCloudService;
	
};



#endif //BUSINESSSERVICESETINTERFACE_H
