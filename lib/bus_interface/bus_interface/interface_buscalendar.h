#ifndef INTERFACE_BUSCALENDAR_H
#define INTERFACE_BUSCALENDAR_H

#include "common/common/status.h"
#include "common/common/dbrecordset.h"

class Interface_BusCalendar
{
public:
	virtual ~Interface_BusCalendar(){};

	virtual void SaveCalFilterView(Status &Ret_pStatus, int &Ret_nViewID, DbRecordSet recView, DbRecordSet recCalEntities, DbRecordSet recCalTypeIDs) = 0;
	virtual void GetCalFilterViews(Status &Ret_pStatus, DbRecordSet &RetOut_recViews, int nPersonID, int nGridType = 0) = 0;
	virtual void GetCalFilterViewsWithEntitesAndTypes(Status &Ret_pStatus, DbRecordSet &RetOut_recViews, DbRecordSet &RetOut_recCalEntities, DbRecordSet &RetOut_recCalTypeIDs, int nPersonID, int nGridType = 0) = 0;
	virtual void GetCalFilterView(Status &Ret_pStatus, DbRecordSet &RetOut_recViewsData, DbRecordSet &RetOut_recCalEntities, DbRecordSet &RetOut_recCalTypeIDs, int nViewID) = 0;
	virtual void DeleteCalFilterView(Status &Ret_pStatus, int nViewID) = 0;

	virtual void WriteEvent(Status &Ret_pStatus, DbRecordSet &RetOut_Data, DbRecordSet &RetOut_Parts,DbRecordSet &RetOut_ScheduleTask,QString strLockRes,DbRecordSet &RetOut_UAR,DbRecordSet &RetOut_GAR) = 0;
	virtual void ReadEvent(Status &Ret_pStatus, int nCalEventID, DbRecordSet &Ret_Data, DbRecordSet &Ret_Parts, DbRecordSet &Ret_ScheduleTask,DbRecordSet &Ret_UAR,DbRecordSet &Ret_GAR) = 0;
	virtual void ReadTemplates(Status &Ret_pStatus, DbRecordSet &Ret_Data) = 0;
	virtual void ReadOnePart(Status &Ret_pStatus, int nPartID,DbRecordSet &RetOut_RowPart,DbRecordSet &RetOut_RowActiveOption) = 0;
	virtual void ReadOneEvent(Status &Ret_pStatus, int nBCEV_ID, DbRecordSet &Ret_EventParts, DbRecordSet &Ret_Contacts, DbRecordSet &Ret_Resources, DbRecordSet &Ret_Projects, DbRecordSet &Ret_Breaks) = 0;
	virtual void ReadEventPartsCountForEntities(Status &Ret_pStatus, DbRecordSet &RetOut_recEntities, DbRecordSet recFilter, QDateTime datStartDate, QDateTime datEndDate) = 0;
	virtual void ReadEventParts(Status &Ret_pStatus, DbRecordSet recFilter, int nEntityType, int nEntityID, QDateTime datStartDate, QDateTime datEndDate, DbRecordSet &Ret_EventParts, DbRecordSet &Ret_Contacts, DbRecordSet &Ret_Resources, DbRecordSet &Ret_Projects, DbRecordSet &Ret_Breaks) = 0;
	virtual void ReadEventPartsForInsert(Status &Ret_pStatus, DbRecordSet recEntities, QDateTime datStartDate, QDateTime datEndDate, DbRecordSet &Ret_EventParts, DbRecordSet &Ret_Contacts, DbRecordSet &Ret_Resources, DbRecordSet &Ret_Projects, DbRecordSet &Ret_Breaks) = 0;
	virtual void ModifyEventDateRange(Status &Ret_pStatus, int nCalEventPartID, int nBCOL_ID, QDateTime datFrom, QDateTime datTo) = 0;
	virtual void DeleteEventPart(Status &Ret_pStatus, int nCalEventPartID, int nBCOL_ID, int nBCEV_ID) = 0;
	virtual void DeleteEventParts(Status &Ret_pStatus, DbRecordSet recDeleteItemsData) = 0;
	virtual void ReadReservation(Status &Ret_pStatus, int nEntityType, int nEntityID, QDateTime datStartDate, QDateTime datEndDate, DbRecordSet &Ret_Reservations) =0;
	virtual void ReadEventPartEntitesForOneEntity(Status &Ret_pStatus, DbRecordSet recFilter, int nEntityType, int nEntityID, QDateTime datStartDate, QDateTime datEndDate, DbRecordSet &Ret_Contacts, DbRecordSet &Ret_Resources, DbRecordSet &Ret_Projects) =0;
	virtual void ModifyEventPresence(Status &Ret_pStatus, QDateTime datFrom, QDateTime datTo, DbRecordSet recFilter, DbRecordSet lstAssignedEntities, int nPresenceTypeID) = 0;

	//Invite:
	virtual void SetInviteStatus(Status &Ret_pStatus, int nCalEventID,int nPartID,int nInviteID,int nContactId_ConfirmedBy, int nPersonID_ConfirmedBy, int nStatus,int nOwnerID=-1,bool bNotifyOwner=false)=0;
	virtual void SendInviteBySokrates(Status &Ret_pStatus, int nCalEventID,DbRecordSet &DataInvite)=0;
	virtual void PrepareInviteRecordsForSendByEmail(Status &Ret_pStatus, DbRecordSet &RetOut_DataInvite, int nDefaultEmailTemplateID)=0;
	virtual void WriteInviteRecordsAfterSend(Status &Ret_pStatus, DbRecordSet &DataInvite,bool bOnlyUpdateSentStatus=false)=0;
	virtual void WriteInviteReplyStatus(Status &Ret_pStatus, DbRecordSet &RetOut_DataInvite, int nTypeOfMessage=0)=0;

	//Synchronize
	virtual void WriteFromOutlook(Status &Ret_pStatus, DbRecordSet &RetOut_Data, bool bSkipExisting,DbRecordSet &Ret_lstStatuses)=0;
	virtual void ReadForOutlook(Status &Ret_pStatus, DbRecordSet &Ret_Data, QDate datFrom, QDate datTo=QDate())=0;

	virtual void GetEventPartContacts(Status &Ret_pStatus, DbRecordSet &recParts, DbRecordSet &Ret_Contacts)=0;
	virtual void GetEventPartResources(Status &Ret_pStatus, DbRecordSet &recParts, DbRecordSet &Ret_Resources)=0;
	virtual void GetEventPartProjects(Status &Ret_pStatus, DbRecordSet &recParts, DbRecordSet &Ret_Projects)=0;
	virtual void GetEventPartBreaks(Status &Ret_pStatus, DbRecordSet &recParts, DbRecordSet &Ret_Breaks)=0;

};



#endif // INTERFACE_BUSCALENDAR_H
