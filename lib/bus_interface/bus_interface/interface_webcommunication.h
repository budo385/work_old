#ifndef INTERFACE_WEBCOMMUNICATION_H
#define INTERFACE_WEBCOMMUNICATION_H

#include "common/common/status.h"
#include "common/common/dbrecordset.h"

class Interface_WebCommunication
{
public:
	virtual ~Interface_WebCommunication(){};

	virtual void ReadContactViews(Status &Ret_pStatus, DbRecordSet &Ret_Views)=0;
	virtual void ReadContactViewData(Status &Ret_pStatus, int nContactID, DbRecordSet &Ret_Data, int &Ret_nTotalCount,int nViewID, int nUseOneColumn, int nUseFromTo, QDateTime datFrom, QDateTime datTo, int nPrevNext, int nRangeSelectorSelector, int nMaxRecords,QString &Ret_strFrom, QString &Ret_strTo)=0;
	virtual void ReadUserViews(Status &Ret_pStatus, DbRecordSet &Ret_Views)=0;
	virtual void ReadUserViewData(Status &Ret_pStatus, int nUserID, DbRecordSet &Ret_Data, int &Ret_nTotalCount,int nViewID, int nUseOneColumn, int nUseFromTo, QDateTime datFrom, QDateTime datTo, int nPrevNext, int nRangeSelectorSelector, int nMaxRecords,QString &Ret_strFrom, QString &Ret_strTo)=0;
	virtual void ReadProjectViews(Status &Ret_pStatus, DbRecordSet &Ret_Views) = 0;
	virtual void ReadProjectViewData(Status &Ret_pStatus, int nProjectID, DbRecordSet &Ret_Data, int &Ret_nTotalCount,int nViewID, int nUseOneColumn, int nUseFromTo, QDateTime datFrom, QDateTime datTo, int nPrevNext, int nRangeSelectorSelector, int nMaxRecords,QString &Ret_strFrom, QString &Ret_strTo) =0;
	virtual void ReadCommEntityCount(Status &Ret_pStatus, int nUserID, int &Ret_nFileDocCount, int &Ret_nWebSiteCount, int &Ret_nNotesCount,int &Ret_nEmailCount)=0;
	virtual void ReadUserDocuments(Status &Ret_pStatus, int &Ret_nTotalCount,DbRecordSet &Ret_Documents, int nUserID,int nDocType=-1, int nFromN=-1, int nToN=-1)=0;
	virtual void ReadUserEmails(Status &Ret_pStatus, int &Ret_nTotalCount, DbRecordSet &Ret_Emails, int nUserID, int nFromN=-1, int nToN=-1, int nSortOrder=-1, QString strEmailAcc="",QString strEmailSender="", int nMailBox=0)=0;



};

/*
<Web_service_meta_data>
	
	<ReadContactViews>
		<URL>/communication/contact/views</URL>
		<HTTP_METHOD>GET</HTTP_METHOD>
		<Ret_Views>
			<DISPLAY_NAME>Views</DISPLAY_NAME>
			<ROW_NAME>view</ROW_NAME>
		</Ret_Views>
	</ReadContactViews>

	<ReadContactViewData>
		<URL>/communication/contact/views/data</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
		<Ret_nTotalCount><DISPLAY_NAME>nTotalCount</DISPLAY_NAME></Ret_nTotalCount>
		<Ret_strFrom><DISPLAY_NAME>Ret_strFrom</DISPLAY_NAME></Ret_strFrom>
		<Ret_strTo><DISPLAY_NAME>Ret_strTo</DISPLAY_NAME></Ret_strTo>
		<Ret_Data>
			<DISPLAY_NAME>Data</DISPLAY_NAME>
			<ROW_NAME>row_data</ROW_NAME>
		</Ret_Data>
	</ReadContactViewData>

	<ReadUserViews>
		<URL>/communication/user/views</URL>
		<HTTP_METHOD>GET</HTTP_METHOD>
		<Ret_Views>
		<DISPLAY_NAME>Views</DISPLAY_NAME>
		<ROW_NAME>view</ROW_NAME>
		</Ret_Views>
	</ReadUserViews>

	<ReadUserViewData>
		<URL>/communication/user/views/data</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
		<Ret_nTotalCount><DISPLAY_NAME>nTotalCount</DISPLAY_NAME></Ret_nTotalCount>
		<Ret_strFrom><DISPLAY_NAME>Ret_strFrom</DISPLAY_NAME></Ret_strFrom>
		<Ret_strTo><DISPLAY_NAME>Ret_strTo</DISPLAY_NAME></Ret_strTo>
		<Ret_Data>
		<DISPLAY_NAME>Data</DISPLAY_NAME>
		<ROW_NAME>row_data</ROW_NAME>
		</Ret_Data>
	</ReadUserViewData>

	<ReadProjectViews>
		<URL>/communication/project/views</URL>
		<HTTP_METHOD>GET</HTTP_METHOD>
		<Ret_Views>
		<DISPLAY_NAME>Views</DISPLAY_NAME>
		<ROW_NAME>view</ROW_NAME>
		</Ret_Views>
	</ReadProjectViews>

	<ReadProjectViewData>
		<URL>/communication/project/views/data</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
		<Ret_nTotalCount><DISPLAY_NAME>nTotalCount</DISPLAY_NAME></Ret_nTotalCount>
		<Ret_strFrom><DISPLAY_NAME>Ret_strFrom</DISPLAY_NAME></Ret_strFrom>
		<Ret_strTo><DISPLAY_NAME>Ret_strTo</DISPLAY_NAME></Ret_strTo>
		<Ret_Data>
		<DISPLAY_NAME>Data</DISPLAY_NAME>
		<ROW_NAME>row_data</ROW_NAME>
		</Ret_Data>
	</ReadProjectViewData>

	<ReadCommEntityCount>
		<URL>/communication/user/[RES_ID]/comm_entity/count</URL>
		<HTTP_METHOD>GET</HTTP_METHOD>
		<RES_ID>nUserID</RES_ID>
		<Ret_nFileDocCount><DISPLAY_NAME>nFileDocCount</DISPLAY_NAME></Ret_nFileDocCount>
		<Ret_nWebSiteCount><DISPLAY_NAME>nWebSiteCount</DISPLAY_NAME></Ret_nWebSiteCount>
		<Ret_nNotesCount><DISPLAY_NAME>nNotesCount</DISPLAY_NAME></Ret_nNotesCount>
		<Ret_nEmailCount><DISPLAY_NAME>nEmailCount</DISPLAY_NAME></Ret_nEmailCount>
	</ReadCommEntityCount>
	
	<ReadUserDocuments>
		<URL>/communication/user/[RES_ID]/documents</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
		<RES_ID>nUserID</RES_ID>
		<Ret_nTotalCount><DISPLAY_NAME>nTotalCount</DISPLAY_NAME></Ret_nTotalCount>
		<Ret_Documents>
		<DISPLAY_NAME>Documents</DISPLAY_NAME>
		<ROW_NAME>document</ROW_NAME>
		<ROW_URL_ATTR>GetWebServiceURL()+"/document"</ROW_URL_ATTR>
		<ROW_URL_ATTR_ID_FIELD>Document_ID</ROW_URL_ATTR_ID_FIELD>
		</Ret_Documents>
	</ReadUserDocuments>

	<ReadUserEmails>
		<URL>/communication/user/[RES_ID]/emails</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
		<RES_ID>nUserID</RES_ID>
		<Ret_nTotalCount>
		<DISPLAY_NAME>nTotalCount</DISPLAY_NAME>
		</Ret_nTotalCount>
		<Ret_Emails>
		<DISPLAY_NAME>Emails</DISPLAY_NAME>
		<ROW_NAME>email</ROW_NAME>
		<ROW_URL_ATTR>GetWebServiceURL()+"/email"</ROW_URL_ATTR>
		<ROW_URL_ATTR_ID_FIELD>Email_ID</ROW_URL_ATTR_ID_FIELD>
		</Ret_Emails>
	</ReadUserEmails>


</Web_service_meta_data>
*/

#endif // INTERFACE_WEBDOCUMENTS_H