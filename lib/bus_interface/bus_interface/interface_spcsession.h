#ifndef INTERFACE_SPCSESSION_H
#define INTERFACE_SPCSESSION_H

#include "common/common/status.h"

class Interface_SpcSession
{
public:
	virtual ~Interface_SpcSession(){};

	virtual void Login(Status& Ret_Status, QString& RetOut_strSessionID,QString strUserName, QString strAuthToken,QString strClientNonce,int nProgCode=0, QString strProgVer="", QString strClientID="", QString strPlatform="",int nClientTimeZoneOffsetMinutes=0)=0;
	virtual void Logout(Status &Ret_Status,QString strSessionID)=0;
};

/*
<Web_service_meta_data>
	<Login>
		<URL>/spc_session/login</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
		<RetOut_strSessionID>
			<DISPLAY_NAME>Session</DISPLAY_NAME>
		</RetOut_strSessionID>
	</Login>

	<Logout>
		<URL>/spc_session/logout</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
	</Logout>

</Web_service_meta_data>
*/

#endif // INTERFACE_SPCSESSION_H
