#ifndef INTERFACE_WEBMWCLOUDSERVICE_H
#define INTERFACE_WEBMWCLOUDSERVICE_H

#include "common/common/status.h"
#include "common/common/dbrecordset.h"

class Interface_WebMWCloudService
{
public:
	virtual ~Interface_WebMWCloudService(){};

	virtual void CreateUser(Status &Ret_pStatus, QString strUserEmail, QString strPass, QString strFirstName, QString strLastName)=0;
	virtual void ChangeUserPassword(Status &Ret_pStatus, QString strUserEmail, QString strPass)=0;
	virtual void EnableUserAccount(Status &Ret_pStatus, QString strUserEmail, bool bEnable)=0;
	virtual void DeleteUser(Status &Ret_pStatus, QString strUserEmail)=0;
	virtual void AddUserEmailAccountIn(Status &Ret_pStatus, QString strUsername, QString strConce, QString strAuthToken, QString strEmail, QString strLogin, QString strPassword, QString strServer, int nPort, bool nIsImap, int nConnectionType, bool bAccountActive, int nDeleteEmailsAfterNoDays)=0;
	virtual void AddUserEmailAccountOut(Status &Ret_pStatus, QString strUsername, QString strConce, QString strAuthToken, QString strEmail, QString strLogin, QString strPassword, QString strServer, int nPort, int nConnectionType, bool  bAuthenticationRequired)=0;
	virtual void RemoveUserEmailAccount(Status &Ret_pStatus, QString strUsername, QString strConce, QString strAuthToken, QString strEmail)=0;
	virtual void DeleteEmail(Status &Ret_pStatus,  QString strUsername, QString strConce, QString strAuthToken, int nEmailID)=0;
	virtual void DeleteMultipleEmails(Status &Ret_pStatus,  QString strUsername, QString strConce, QString strAuthToken, QString strEmailIDs)=0;
	virtual void MoveEmail(Status &Ret_pStatus, QString strUsername, QString strConce, QString strAuthToken, int nEmailID, int nMailbox, QString strDueDate, QString strReminderType)=0; 
	virtual void BlackListEmailAddress(Status &Ret_pStatus, QString strUsername, QString strConce, QString strAuthToken, QString strEmail, bool bActivateBlocking)=0;
	virtual void GetEmailAddressBlackList(Status &Ret_pStatus, QString strUsername, QString strConce, QString strAuthToken, QString &RetOut_strList)=0;
	virtual void StoreEmail(Status &Ret_pStatus, QString strUsername, QString strConce, QString strAuthToken, int nIsOutgoing,QString strFrom,QString strTo,QString strSubject, QString strMime,QString strFullFrom,QString strCC,QString strBCC)=0;
	virtual void GetUsersEmailCnt(Status &Ret_pStatus, DbRecordSet &Ret_lstData)=0;
	virtual void GetUnreadEmailsCount(Status &Ret_pStatus, QString strUsername, QString strConce, QString strAuthToken, int &Ret_nCount)=0;
	virtual void SendUserErrorPushMessage(Status &Ret_pStatus, QString strUsername, QString strEmailFrom, QString strErrorMsg)=0;
	virtual void RegisterPushNotificationID(Status &Ret_pStatus, QString strUsername, QString strConce, QString strAuthToken, QString strPushToken, int nOSType)=0;
	virtual void ActivatePushNotification(Status &Ret_pStatus, QString strUsername, QString strConce, QString strAuthToken, int nEnablePush)=0;
	virtual void SetNewEmailsPushNotifications(Status &Ret_pStatus, QString strUsername, QString strConce, QString strAuthToken, int nState, int nSilentMode, QString strContacts)=0;
	virtual void SetEmailAccountPushNotifications(Status &Ret_pStatus, QString strUsername, QString strConce, QString strAuthToken, QString strEmailAccount,int nEnablePush)=0;
	virtual void SendPushNotification(Status &Ret_pStatus, QString strUsername, QString strConce, QString strAuthToken, QString strMessage, int nOSType, QString strPushToken, QString strPushSoundName, int nBadgeNumber)=0;
};

/*
<Web_service_meta_data>
	
	<CreateUser>
		<URL>/mwcloudservice/createuser</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
	</CreateUser>
	
	<ChangeUserPassword>
	<URL>/mwcloudservice/changeuserpassword</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
	</ChangeUserPassword>


	<AddUserEmailAccountIn>
		<URL>/mwcloudservice/adduseremailaccountin</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
	</AddUserEmailAccountIn>

	<AddUserEmailAccountOut>
		<URL>/mwcloudservice/adduseremailaccountout</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
	</AddUserEmailAccountOut>


	<RemoveUserEmailAccount>
		<URL>/mwcloudservice/removeuseremailaccount</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
	</RemoveUserEmailAccount>

	<DeleteEmail>
		<URL>/mwcloudservice/deleteemail</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
	</DeleteEmail>

	<DeleteMultipleEmails>
		<URL>/mwcloudservice/deletemultipleemails</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
	</DeleteMultipleEmails>
	
	<BlackListEmailAddress>
		<URL>/mwcloudservice/blacklistemailaddress</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
	</BlackListEmailAddress>

	<GetEmailAddressBlackList>
		<URL>/mwcloudservice/getmailaddressblackliste</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
	</GetEmailAddressBlackList>

	<StoreEmail>
		<URL>/mwcloudservice/storeemail</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
	</StoreEmail>

	<GetUsersEmailCnt>
		<URL>/mwcloudservice/getusersemailcnt</URL>
		<HTTP_METHOD>GET</HTTP_METHOD>
	</GetUsersEmailCnt>

	<GetUnreadEmailsCount>
		<URL>/mwcloudservice/getunreademailscount</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
	</GetUnreadEmailsCount>

	<SendUserErrorPushMessage>
		<URL>/mwcloudservice/sendusererrorpushmessage</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
	</SendUserErrorPushMessage>

	<RegisterPushNotificationID>
		<URL>/mwcloudservice/registerpushnotificationid</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
	</RegisterPushNotificationID>

	<ActivatePushNotification>
		<URL>/mwcloudservice/activatepushnotification</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
	</ActivatePushNotification>

	<SetNewEmailsPushNotifications>
		<URL>/mwcloudservice/setnewemailspushnotifications</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
	</SetNewEmailsPushNotifications>

	<SetEmailAccountPushNotifications>
		<URL>/mwcloudservice/setemailaccountpushnotifications</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
	</SetEmailAccountPushNotifications>

	<SendPushNotification>
		<URL>/mwcloudservice/sendpushnotification</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
	</SendPushNotification>

	<MoveEmail>
		<URL>/mwcloudservice/moveemail</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
	</MoveEmail>

	<EnableUserAccount>
		<URL>/mwcloudservice/enableuseraccount</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
	</EnableUserAccount>
	
	<DeleteUser>
		<URL>/mwcloudservice/deleteuser</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
	</DeleteUser>
	
</Web_service_meta_data>
*/

#endif // INTERFACE_WEBMWCLOUDSERVICE_H