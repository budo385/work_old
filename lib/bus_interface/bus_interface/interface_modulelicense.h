#ifndef INTERFACE_MODULELICENSE_H
#define INTERFACE_MODULELICENSE_H

#include "common/common/status.h"
#include "common/common/dbrecordset.h"

/*!
    \class Interface_ModuleLicense
    \brief Interface class for a BO to query client's access rights (based on keyfile)
    \ingroup Bus_Interface_Collection
		
*/

class Interface_ModuleLicense
{
public:
	virtual ~Interface_ModuleLicense(){};

	//Remember: Status is always first and DbSqlConnection is DbSqlConnection is always last)
	virtual void GetAllModuleLicenseData(Status& Ret_pStatus, QString& strModuleCode, QString& strMLIID,DbRecordSet& RetOut_pList,QString& RetOut_strUserName, int& RetOut_nLicenseID, QString &RetOut_strReportLine1, QString &RetOut_strReportLine2, QString &Ret_strCustomerID,int &Ret_nCustomSolutionID)=0;
	virtual void GetFPList(Status& Ret_pStatus, QString& strModuleCode, QString& strMLIID, DbRecordSet& RetOut_pList) =0;
	virtual void GetLicenseInfo(Status& Ret_pStatus, QString& RetOut_strUserName, int& RetOut_nLicenseID, QString &RetOut_strReportLine1, QString &RetOut_strReportLine2, QString &Ret_strCustomerID,int &Ret_nCustomSolutionID) =0;
};


#endif //INTERFACE_MODULELICENSE_H
