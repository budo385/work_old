#ifndef INTERFACE_WEBSESSIONDATA_H
#define INTERFACE_WEBSESSIONDATA_H

#include "common/common/status.h"
#include "common/common/dbrecordset.h"

class Interface_WebSessionData
{
public:
	virtual ~Interface_WebSessionData(){};

	virtual void GetUserSessionData(Status &Ret_Status,DbRecordSet &Ret_ARSet,DbRecordSet &Ret_UserData, DbRecordSet &Ret_ContactData,  QString &Ret_strEmailCalConfAddress,  int &Ret_nIsAnestezistClient,  int &Ret_nCSID) = 0;

};

/*
<Web_service_meta_data>
	
	<GetUserSessionData>
		<URL>/sessiondata/user_opt_ar</URL>
		<HTTP_METHOD>GET</HTTP_METHOD>
		<Ret_ARSet>
			<DISPLAY_NAME>ARSet</DISPLAY_NAME>
			<ROW_NAME>row</ROW_NAME>
		</Ret_ARSet>
		<Ret_UserData>
			<DISPLAY_NAME>UserData</DISPLAY_NAME>
			<ROW_NAME>row</ROW_NAME>
		</Ret_UserData>
		<Ret_ContactData>
			<DISPLAY_NAME>ContactData</DISPLAY_NAME>
			<ROW_NAME>row</ROW_NAME>
		</Ret_ContactData>
		<Ret_strEmailCalConfAddress>
			<DISPLAY_NAME>strEmailCalConfAddress</DISPLAY_NAME>
		</Ret_strEmailCalConfAddress>
	</GetUserSessionData>

</Web_service_meta_data>
*/

#endif // INTERFACE_WEBSESSIONDATA_H