#ifndef BUSINESSSERVICEMANAGER_H
#define BUSINESSSERVICEMANAGER_H

#include "bus_interface/bus_interface/interface_businessserviceset.h"
#include "db_core/db_core/dbconnsettings.h"
#include "trans/trans/httpclientconnectionsettings.h"


#define _SERVER_CALL(funct) {if(g_pBoSet->app)g_pBoSet->app->funct;}

/*!
	\class BusinessServiceManager
	\brief Holds Business Set for accessing business calls
	\ingroup Bus_Client

	Use:
	See BusinessServiceManager_ThickClient and BusinessServiceSet_ThinClient

*/
class BusinessServiceManager
{
public:
	BusinessServiceManager();
	virtual ~BusinessServiceManager();
	
	Interface_BusinessServiceSet *app; //< this is main pointer for accessing BO server side object 
	virtual void StartServiceSet(Status &pStatus)=0;
	virtual void StopService(Status &pStatus, bool bFatalErrorDetected=false)=0;
	virtual void AbortDataTransfer()=0;
	bool IsServiceStarted(){return m_bServiceStarted;};
	virtual void SetSettings(HTTPClientConnectionSettings& pConnSettings){};
	virtual void SetSettings(DbConnectionSettings& DbConnSettings){};

protected:
	bool m_bServiceStarted;
};

#endif // BUSINESSSERVICEMANAGER_H
