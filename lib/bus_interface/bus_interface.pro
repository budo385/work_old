TEMPLATE	= lib
CONFIG		= qt warn_on staticlib
HEADERS		= bus_interface/interface_accessrights.h \
		  bus_interface/interface_boentity.h \
		  bus_interface/interface_buseventlog.h \
		  bus_interface/interface_businessserviceset.h \
		  bus_interface/interface_collection.h \
		  bus_interface/interface_mainentityselector.h \
		  bus_interface/interface_modulelicense.h \
		  bus_interface/interface_testbusinessobject.h \
		  bus_interface/interface_testcontact.h \
		  bus_interface/interface_treeview.h \
		  bus_interface/interface_userlogon.h
SOURCES		= bus_interface/test.cpp
INTERFACES	= 
TARGET		= bus_interface
