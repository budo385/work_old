// shnewhnd.cpp -- SmartHeap(tm) C++ new handler
// Professional Memory Management Library
//
// Copyright (C) 1991-2005 Compuware Corporation.
// All Rights Reserved.
//
// No part of this source code may be copied, modified or reproduced
// in any form without retaining the above copyright notice.
// This source code, or source code derived from it, may not be redistributed
// without express written permission of the author.
//

#include "smrtheap.hpp"

#ifdef _MSC_VER
#if UINT_MAX == 0xFFFFu
extern _PNHH mem_new_handler_msc_huge = 0;
#endif
#endif /* _MSC_VER */

#if (defined(__IBMCPP__) && __IBMCPP__ >= 300) && !defined(_AIX)
#define _new_handler __new_handler
#endif

MEM_STD_USING(new_handler);
extern new_handler _new_handler = 0;
// new_handler: note that MSC/C++ prototype for new handler differs from the
// protosed ANSI standard prototype for the new handler
#ifdef _MSC_VER
#if UINT_MAX == 0xFFFFu
_PNH _set_fnew_handler(_PNH handler)
{
   return _set_new_handler(handler);
}

_PNHH _set_hnew_handler(_PNHH handler)
{
   _PNHH old_handler = mem_new_handler_msc_huge;
   mem_new_handler_msc_huge = handler;
   return old_handler;
}
#endif /* 16-bit */

#if !(defined(_MFC_VER) && _MFC_VER >= 0x0300 && defined(_AFXDLL))

#if _MSC_VER < 900
// no need to override _set_new_handler for VC++ 2.0 or higher, since
// we can obtain a pointer to the CRT new-handler with _query_new_handler
extern "C" _PNH mem_new_handler_msc;

_PNH _set_new_handler(_PNH handler)
{
   _PNH old_handler = mem_new_handler_msc;
   mem_new_handler_msc = handler;
   return old_handler;
}
#endif
#endif /* !_AFXDLL */
#endif /* _MSC_VER */

#if (!defined(_MSC_VER) || _MSC_VER < 900) && !defined(_AIX43_Xxx)
                                        // AIX should be OK now with std::
// no need to override _set_new_handler for VC++ 2.0 or higher, since
// we can obtain a pointer to the CRT new-handler with _query_new_handler
MEM_STD_NAMESPACE_BEGIN
new_handler set_new_handler(new_handler handler) MEM_CPP_THROW2
{
   new_handler old_handler = _new_handler;
   _new_handler = handler;
   return old_handler;
}

#ifdef __GNUC__
// gcc ~bad_alloc is defined out-of-line
bad_alloc::~bad_alloc() MEM_CPP_THROW2 { }
#endif

MEM_STD_NAMESPACE_END
#endif

#if defined(__IBMCPP__) && __IBMCPP__ >= 300 && !defined(_AIX)
new_handler _set_mt_new_handler(new_handler handler)
{
	return MEM_STD_(set_new_handler)(handler);
}
#endif /* __IBMCPP__ */
