<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_CH">
<context>
    <name>Adm_ServerLog</name>
    <message>
        <location filename="adm_serverlog.cpp" line="19"/>
        <source>No Logging</source>
        <translation>Kein Logging</translation>
    </message>
    <message>
        <location filename="adm_serverlog.cpp" line="20"/>
        <source>Basic</source>
        <translation>Basic</translation>
    </message>
    <message>
        <location filename="adm_serverlog.cpp" line="21"/>
        <source>Advanced</source>
        <translation>Advanced</translation>
    </message>
    <message>
        <location filename="adm_serverlog.cpp" line="22"/>
        <source>Debug</source>
        <translation>Debug</translation>
    </message>
    <message>
        <location filename="adm_serverlog.cpp" line="23"/>
        <source>User Sessions</source>
        <translation>User-Sessions</translation>
    </message>
    <message>
        <location filename="adm_serverlog.cpp" line="28"/>
        <source>Current Day</source>
        <translation>Aktueller Tag</translation>
    </message>
    <message>
        <location filename="adm_serverlog.cpp" line="29"/>
        <source>Last 7 days</source>
        <translation>Letzte 7 Tage</translation>
    </message>
    <message>
        <location filename="adm_serverlog.cpp" line="30"/>
        <source>Last 30 days</source>
        <translation>Letzte 30 Tage</translation>
    </message>
    <message>
        <location filename="adm_serverlog.cpp" line="43"/>
        <source>Filter log by one or more message IDs. Message ID must be number or comma separates numbers e.g.: 300,201</source>
        <translation>Eine oder mehrere Nachrichten-IDs aus dem Log herausfiltern. Die IDs können durch Komma getrennt sein, z.B.: 300,201</translation>
    </message>
    <message>
        <location filename="adm_serverlog.cpp" line="44"/>
        <source>Maximum log file size. If exceeds this amount log is truncated by 20%.  Values (0-1000Mb).</source>
        <translation>Maximale Logdateigrösse. Wird diese überschritten, so wird das Log um 20% davon gekürzt. Werte (0-1000MB).</translation>
    </message>
    <message>
        <location filename="adm_serverlog.cpp" line="45"/>
        <source>Memory Buffer Cache Size before writing log into the file. Set to 0 for writing to disk without cache (can slower server). Values (0-4096Kb).</source>
        <translation>Cache-Buffergrösse, bei deren Überschreitung das Log ergänzt wird. 0 umgeht den Cache und schreibt direkt (langsamer). Werte (0-4096KB).</translation>
    </message>
    <message>
        <location filename="adm_serverlog.cpp" line="50"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location filename="adm_serverlog.cpp" line="51"/>
        <source>Msg ID</source>
        <translation>Msg ID</translation>
    </message>
    <message>
        <location filename="adm_serverlog.cpp" line="52"/>
        <source>Category</source>
        <translation>Ketegorie</translation>
    </message>
    <message>
        <location filename="adm_serverlog.cpp" line="53"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location filename="adm_serverlog.cpp" line="54"/>
        <source>Text</source>
        <translation>Text</translation>
    </message>
    <message>
        <location filename="adm_serverlog.cpp" line="252"/>
        <location filename="adm_serverlog.cpp" line="301"/>
        <location filename="adm_serverlog.cpp" line="307"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location filename="adm_serverlog.cpp" line="252"/>
        <source>Message ID format not recognized. Message ID must be number or comma separates numbers e.g.: 300,201</source>
        <translation>Format der Nachrichten-ID nicht erkannt. Die Nachrichten-ID muss eine Zahl sein oder mehrere durch Komma getrennte Zahlen, z.B.: 300,201</translation>
    </message>
    <message>
        <location filename="adm_serverlog.cpp" line="301"/>
        <source>Maximum log size can not be lower then 0 and greater then 1000Mb!</source>
        <translation>Maximale Log-Grösse darf nicht kleiner als 0 oder grösser als 1000MB sein!</translation>
    </message>
    <message>
        <location filename="adm_serverlog.cpp" line="307"/>
        <source>Maximum memory buffer size can not be lower then 0 and greater then 4096Kb!</source>
        <translation>Maximale Cache-Buffergrösse darf nicht kleiner als 0 oder grösser als 4096kB sein!</translation>
    </message>
    <message>
        <location filename="adm_serverlog.cpp" line="313"/>
        <source>Saved</source>
        <translation>Gespeichert</translation>
    </message>
    <message>
        <location filename="adm_serverlog.cpp" line="313"/>
        <source>Settings saved!</source>
        <translation>Einstellungen gespeichert!</translation>
    </message>
    <message>
        <location filename="adm_serverlog.cpp" line="437"/>
        <source>User</source>
        <translation>Benutzer</translation>
    </message>
    <message>
        <location filename="adm_serverlog.cpp" line="438"/>
        <source>User ID</source>
        <translation>Benutzer-ID</translation>
    </message>
    <message>
        <location filename="adm_serverlog.cpp" line="439"/>
        <source>IP</source>
        <translation>IP</translation>
    </message>
    <message>
        <location filename="adm_serverlog.cpp" line="440"/>
        <source>LogIn</source>
        <translation>Login</translation>
    </message>
    <message>
        <location filename="adm_serverlog.cpp" line="441"/>
        <source>LogOut</source>
        <translation>Logout</translation>
    </message>
    <message>
        <location filename="adm_serverlog.cpp" line="442"/>
        <source>Duration (min)</source>
        <translation>Dauer (Min.)</translation>
    </message>
</context>
<context>
    <name>Adm_ServerLogClass</name>
    <message>
        <location filename="adm_serverlog.ui" line="14"/>
        <source>Adm_ServerLog</source>
        <translation>Adm_ServerLog</translation>
    </message>
    <message>
        <location filename="adm_serverlog.ui" line="22"/>
        <source>Log Settings</source>
        <translation>Log-Einstellungen</translation>
    </message>
    <message>
        <location filename="adm_serverlog.ui" line="34"/>
        <source>Level:</source>
        <translation>Stufe:</translation>
    </message>
    <message>
        <location filename="adm_serverlog.ui" line="44"/>
        <source>Max Size (Mbytes):</source>
        <translation>Max. Grösse (MByte):</translation>
    </message>
    <message>
        <location filename="adm_serverlog.ui" line="54"/>
        <source>Memory Buffer Size (Kbytes):</source>
        <translation>Cache-Buffergrösse (kBytes):</translation>
    </message>
    <message>
        <location filename="adm_serverlog.ui" line="77"/>
        <source>Save</source>
        <translation>Speichern</translation>
    </message>
    <message>
        <location filename="adm_serverlog.ui" line="87"/>
        <source>Filter</source>
        <translation>Filter</translation>
    </message>
    <message>
        <location filename="adm_serverlog.ui" line="93"/>
        <source>Filter by Message ID:</source>
        <translation>Filtern nach Nachrichten-ID:</translation>
    </message>
    <message>
        <location filename="adm_serverlog.ui" line="103"/>
        <location filename="adm_serverlog.ui" line="120"/>
        <source>Apply</source>
        <translation>Anwenden</translation>
    </message>
    <message>
        <location filename="adm_serverlog.ui" line="110"/>
        <source>Show Report:</source>
        <translation>Bericht zeigen:</translation>
    </message>
    <message>
        <location filename="adm_serverlog.ui" line="140"/>
        <source>Clear Filter</source>
        <translation>Filter löschen</translation>
    </message>
    <message>
        <location filename="adm_serverlog.ui" line="180"/>
        <source>Show:</source>
        <translation>Anzeigen:</translation>
    </message>
    <message>
        <location filename="adm_serverlog.ui" line="213"/>
        <source>Refresh  Every </source>
        <translation>Refresh jeweils</translation>
    </message>
    <message>
        <location filename="adm_serverlog.ui" line="242"/>
        <source>minutes</source>
        <translation>Minuten</translation>
    </message>
    <message>
        <location filename="adm_serverlog.ui" line="265"/>
        <source>Current Log Size:</source>
        <translation>Aktuelle Log-Grösse:</translation>
    </message>
    <message>
        <location filename="adm_serverlog.ui" line="272"/>
        <source>210Mb</source>
        <translation>210MB</translation>
    </message>
    <message>
        <location filename="adm_serverlog.ui" line="296"/>
        <source>Refresh Now</source>
        <translation>Jetzt aktualisieren</translation>
    </message>
</context>
</TS>
