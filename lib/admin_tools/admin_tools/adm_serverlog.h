#ifndef ADM_SERVERLOG_H
#define ADM_SERVERLOG_H

#include <QWidget>
#include "generatedfiles/ui_adm_serverlog.h"
#include "common/common/dbrecordset.h"
#include "common/common/status.h"

class Adm_ServerLog : public QWidget
{
	Q_OBJECT

public:
	Adm_ServerLog(QWidget *parent = 0);
	~Adm_ServerLog();

	void Initialize();
	void Invalidate();


private slots:
	void on_btnRefreshNow_clicked();
	void on_btnApplyFilter_clicked();
	void on_btnApplyReport_clicked();
	void on_btnClearFilter_clicked();
	void on_ckbRefresh_clicked(bool bChecked);
	void on_btnSave_clicked();
	void on_txtRefreshMin_editingFinished();
protected:
	void timerEvent(QTimerEvent *event);

private:
	void ReadSettings();
	void SaveSettings();
	void ReadLog(Status &err, int nLastDaysPeriod=-1);
	void ParseRawData(QByteArray &data,DbRecordSet &lstData);
	void DefineLogList(DbRecordSet &lstData);
	bool CheckDate(DbRecordSet &lstData,QDateTime dateStop);
	bool ParseMsgIDFilter(QList<int> &lstIDs);
	void StartTimer();
	void ClearTimer();
	void ReadLogPrivate(Status &err, DbRecordSet &logRead, QDateTime stopDateTime, int &nLastSize);

	void Report_UserSessions();
	void DefineList_Report_UserSessions(DbRecordSet &lstData);

	Ui::Adm_ServerLogClass ui;

	QDateTime  m_datLastDate;
	int m_nLastDaysPeriod;
	DbRecordSet m_lstLog;
	DbRecordSet m_lstLogOriginal;

	int m_nLogLevel; 
	int m_nLogMaxSize;
	int m_nBufferSize;
	int m_nTimerID;

	DbRecordSet m_columns;
	bool m_bReportActive;
};

#endif // ADM_SERVERLOG_H
