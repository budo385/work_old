#include "adm_serverlog.h"
#include <QCoreApplication>
#include "gui_core/gui_core/macros.h"
#include "common/common/datahelper.h"

#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;	

#define LOG_CHUNK_SIZE 131072 //128Kb log

Adm_ServerLog::Adm_ServerLog(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
	m_datLastDate=QDateTime(); //invalid date
	m_nTimerID=-1;
	m_bReportActive=false;

	ui.cmbLogLevel->addItem(tr("No Logging"));
	ui.cmbLogLevel->addItem(tr("Basic"));
	ui.cmbLogLevel->addItem(tr("Advanced"));
	ui.cmbLogLevel->addItem(tr("Debug"));
	ui.cmbReport->addItem(tr("User Sessions"));

	ui.tableWidget->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);

	ui.cmbShowLast->setEditable(true);
	ui.cmbShowLast->addItem(tr("Current Day"),0);
	ui.cmbShowLast->addItem(tr("Last 7 days"),7);
	ui.cmbShowLast->addItem(tr("Last 30 days"),30);
	//ui.cmbShowLast->lineEdit()->setInputMask("999");
	ui.cmbShowLast->setInsertPolicy(QComboBox::InsertAtTop);

	ui.txtMaxSize->setInputMask("0000");
	ui.txtBufferSize->setInputMask("0000");
	ui.txtRefreshMin->setInputMask("000");
	ui.progressBar->setVisible(false);
	ui.ckbShowLast->setChecked(true);
	m_nLastDaysPeriod=0;
	ui.txtRefreshMin->setText("5");
	ui.ckbRefresh->setChecked(true);

	ui.txtFilterMsg->setToolTip(tr("Filter log by one or more message IDs. Message ID must be number or comma separates numbers e.g.: 300,201"));
	ui.txtMaxSize->setToolTip(tr("Maximum log file size. If exceeds this amount log is truncated by 20%.  Values (0-1000Mb)."));
	ui.txtBufferSize->setToolTip(tr("Memory Buffer Cache Size before writing log into the file. Set to 0 for writing to disk without cache (can slower server). Values (0-4096Kb)."));
	DefineLogList(m_lstLog);
	DefineLogList(m_lstLogOriginal);

	//setup columns
	UniversalTableWidgetEx::AddColumnToSetup(m_columns,"Date",tr("Date"),120,false, "",UniversalTableWidgetEx::COL_TYPE_TEXT,"");
	UniversalTableWidgetEx::AddColumnToSetup(m_columns,"ID",tr("Msg ID"),60,false, "",UniversalTableWidgetEx::COL_TYPE_TEXT,"");
	UniversalTableWidgetEx::AddColumnToSetup(m_columns,"Category",tr("Category"),80,false, "",UniversalTableWidgetEx::COL_TYPE_TEXT,"");
	UniversalTableWidgetEx::AddColumnToSetup(m_columns,"TypeName",tr("Type"),80,false, "",UniversalTableWidgetEx::COL_TYPE_TEXT,"");
	UniversalTableWidgetEx::AddColumnToSetup(m_columns,"Text",tr("Text"),300,false, "",UniversalTableWidgetEx::COL_TYPE_TEXT);
	ui.tableWidget->Initialize(&m_lstLog,&m_columns);
	ui.tableWidget->horizontalHeader()->setStretchLastSection(true);

}

Adm_ServerLog::~Adm_ServerLog()
{

}

void Adm_ServerLog::Initialize()
{
	ClearTimer();
	on_btnClearFilter_clicked();
	ui.cmbShowLast->setCurrentIndex(0);
	m_nLastDaysPeriod=0;
	m_datLastDate=QDateTime(); //invalid date
	m_lstLog.clear();
	m_lstLogOriginal.clear();
	ui.tableWidget->RefreshDisplay();
	ReadSettings();
	on_btnRefreshNow_clicked();
	if (ui.ckbRefresh->isChecked())
		StartTimer();
}

//nLastDaysPeriod = -1, all, 0 current day,>0 by day
void Adm_ServerLog::ReadLog(Status &err, int nLastDaysPeriod)
{
	//if period not changed, try to read only new entries
	bool bRefreshCurrent=false;
	if (m_nLastDaysPeriod==nLastDaysPeriod && m_datLastDate.isValid())
	{
		m_lstLog=m_lstLogOriginal;
		bRefreshCurrent=true;
	}
	else
	{
		//DefineLogList(m_lstLog);
		if (m_bReportActive)
			on_btnClearFilter_clicked();
	}
	
	QDateTime dateStop; //if invalid, then all
	if (nLastDaysPeriod>0)
		dateStop=QDateTime::currentDateTime().addDays(-nLastDaysPeriod); //last N days
	else if (nLastDaysPeriod==0)
		dateStop=QDateTime(QDate::currentDate(),QTime(0,0)); //from midnight

	int nLastSize=-1;
	if (bRefreshCurrent)
	{
		DbRecordSet lstNewRecords;
		ReadLogPrivate(err, lstNewRecords, m_datLastDate, nLastSize);
		if (!err.IsOK()) 
		{
			ui.progressBar->setVisible(false);
			return;
		}
		m_lstLog.merge(lstNewRecords);
	}
	else
	{
		ReadLogPrivate(err, m_lstLog, dateStop, nLastSize);
		if (!err.IsOK()) 
		{
			ui.progressBar->setVisible(false);
			return;
		}
	}

	m_lstLogOriginal=m_lstLog;
	if (m_lstLogOriginal.getRowCount()>0)
		m_datLastDate=m_lstLogOriginal.getDataRef(m_lstLogOriginal.getRowCount()-1,"Date").toDateTime();

	ui.labLogSize->setText(DataHelper::GetFormatedFileSize(nLastSize,3));
	ui.tableWidget->RefreshDisplay();
	ui.tableWidget->ScrollToLastItem(true);
	ui.progressBar->setValue(ui.progressBar->maximum());
	QCoreApplication::processEvents();
	ui.progressBar->setVisible(false);
}

void Adm_ServerLog::ParseRawData(QByteArray &data,DbRecordSet &lstData)
{
	int nLastPos=0;
	int nPos=data.indexOf("\n");

	DefineLogList(lstData);
	while (nPos>=0)
	{
		QStringList lstLine=QString(data.mid(nLastPos,nPos-nLastPos)).split("\t");
		nLastPos=nPos+1;
		nPos=data.indexOf("\n",nLastPos);
		if (lstLine.size()!=6)
			continue;
		lstData.addRow();
		int nRow=lstData.getRowCount()-1;
		lstData.setData(nRow,"Date",QDateTime::fromString(lstLine.at(0),"yyyy-MM-dd hh.mm.ss.zzz"));
		lstData.setData(nRow,"Type",QVariant(lstLine.at(1)).toInt());
		lstData.setData(nRow,"ID",QVariant(lstLine.at(2)).toInt());
		lstData.setData(nRow,"TypeName",lstLine.at(3));
		lstData.setData(nRow,"Category",lstLine.at(4));
		lstData.setData(nRow,"Text",lstLine.at(5).simplified());
		//qDebug()<<lstData.getDataRef(nRow,"Date").toDateTime().toString("yyyy-MM-dd hh.mm.ss.zzz");
	}
}


void Adm_ServerLog::DefineLogList(DbRecordSet &lstData)
{
	lstData.destroy();
	lstData.addColumn(QVariant::DateTime,"Date");
	lstData.addColumn(QVariant::Int,"Type");
	lstData.addColumn(QVariant::Int,"ID");
	lstData.addColumn(QVariant::String,"TypeName");
	lstData.addColumn(QVariant::String,"Category");
	lstData.addColumn(QVariant::String,"Text");
}



void Adm_ServerLog::on_btnRefreshNow_clicked()
{
	Status err;
	int nLastDays=-1;
	if (ui.ckbShowLast->isChecked())
	{
		bool bOK;
		nLastDays=ui.cmbShowLast->currentText().toInt(&bOK);
		if (!bOK)
		{
			if (ui.cmbShowLast->currentIndex()!=-1)
				nLastDays=ui.cmbShowLast->itemData(ui.cmbShowLast->currentIndex()).toInt();
		}
	}

	ReadLog(err,nLastDays);
	_CHK_ERR(err);
}


//true: all ok, false->date found, chunk is truncated, break operation
bool Adm_ServerLog::CheckDate(DbRecordSet &lstData,QDateTime dateStop)
{
	if (!dateStop.isValid())
		return true;
	if (lstData.getRowCount()==0)
		return true;

	if (lstData.getDataRef(0,"Date").toDateTime()<=dateStop)
	{
		int nSize=lstData.getRowCount()-1;
		for(int i=nSize;i>=0;i--)
		{
			if (lstData.getDataRef(i,"Date").toDateTime()<=dateStop)
				lstData.deleteRow(i);
		}
		return false;
	}
	return true;
}


bool Adm_ServerLog::ParseMsgIDFilter(QList<int> &lstIDs)
{
	QStringList strIDs;

	if (ui.txtFilterMsg->text().indexOf(",")>=0)
		strIDs=ui.txtFilterMsg->text().split(",",QString::SkipEmptyParts);
	else
		strIDs.append(ui.txtFilterMsg->text());

	int nSize=strIDs.size();
	for(int i=0;i<nSize;i++)
	{
		bool bOK;
		int nID=strIDs.at(i).toInt(&bOK);
		if (bOK)
		{
			lstIDs.append(nID);
		}
		else
			return false;
	}

	return true;
}


void Adm_ServerLog::on_btnApplyFilter_clicked()
{
	on_btnClearFilter_clicked(); //always clear
	
	QList<int> lstIds;
	if (!ParseMsgIDFilter(lstIds))
	{
		QMessageBox::warning(this,tr("Warning"),tr("Message ID format not recognized. Message ID must be number or comma separates numbers e.g.: 300,201"));
		return;
	}

	m_lstLog=m_lstLogOriginal;

	m_lstLog.clearSelection();
	int nSize=m_lstLog.getRowCount()-1;
	for(int i=nSize;i>=0;i--)
	{
		int nID=m_lstLog.getDataRef(i,"ID").toInt();
		if (lstIds.indexOf(nID)<0)
			m_lstLog.deleteRow(i);
	}
	ui.tableWidget->RefreshDisplay();
}

void Adm_ServerLog::on_btnClearFilter_clicked()
{
	m_lstLog=m_lstLogOriginal;
	if (m_bReportActive)
	{
		ui.tableWidget->SetColumnSetup(m_columns);
	}
	ui.tableWidget->RefreshDisplay();
	m_bReportActive=false;
}

void Adm_ServerLog::ReadSettings()
{
	Status err;
	int nCurrentSize;
	g_pBoSet->app->ServerControl->GetLogData(err,m_nLogLevel,m_nLogMaxSize,m_nBufferSize,nCurrentSize);
	_CHK_ERR(err);

	ui.labLogSize->setText(DataHelper::GetFormatedFileSize(nCurrentSize,3));

	ui.cmbLogLevel->setCurrentIndex(m_nLogLevel);
	ui.txtMaxSize->setText(QVariant(m_nLogMaxSize).toString());
	ui.txtBufferSize->setText(QVariant(m_nBufferSize).toString());
}

void Adm_ServerLog::SaveSettings()
{
	Status err;
	m_nLogLevel=ui.cmbLogLevel->currentIndex();
	m_nLogMaxSize=ui.txtMaxSize->text().toInt();
	if (m_nLogMaxSize<0 || m_nLogMaxSize>1000)
	{
		QMessageBox::warning(this,tr("Warning"),tr("Maximum log size can not be lower then 0 and greater then 1000Mb!"));
		return;
	}
	m_nBufferSize=ui.txtBufferSize->text().toInt();
	if (m_nBufferSize<0 || m_nBufferSize>4096)
	{
		QMessageBox::warning(this,tr("Warning"),tr("Maximum memory buffer size can not be lower then 0 and greater then 4096Kb!"));
		return;
	}
	g_pBoSet->app->ServerControl->SetLogData(err,m_nLogLevel,m_nLogMaxSize,m_nBufferSize);
	_CHK_ERR(err);

	QMessageBox::information(this,tr("Saved"),tr("Settings saved!"));
}

void Adm_ServerLog::timerEvent(QTimerEvent *event)
{
	if(event->timerId()==m_nTimerID)
	{
		ClearTimer();
		on_btnRefreshNow_clicked();
		StartTimer();
	}
}
void Adm_ServerLog::on_ckbRefresh_clicked(bool bChecked)
{
	if (bChecked)
		StartTimer();
	else
		ClearTimer();
}

void Adm_ServerLog::StartTimer()
{
	ClearTimer();
	int nPeriod=ui.txtRefreshMin->text().toInt();
	if (nPeriod<=0 || nPeriod>999)
	{
		nPeriod=5;
		ui.txtRefreshMin->setText("5");
	}
	m_nTimerID=startTimer(nPeriod*1000*60);	//try to save our asses if something goes wrong
}
void Adm_ServerLog::ClearTimer()
{
	if (m_nTimerID!=-1)
		killTimer(m_nTimerID);
	m_nTimerID=-1;
}


void Adm_ServerLog::on_btnSave_clicked()
{
	SaveSettings();
}

void Adm_ServerLog::on_txtRefreshMin_editingFinished()
{
	ClearTimer();
	if (ui.ckbRefresh->isChecked())
		StartTimer();
}


void Adm_ServerLog::Report_UserSessions()
{
	on_btnClearFilter_clicked();

	m_lstLog.find("ID",(int)StatusCodeSet::MSG_SYSTEM_CLIENT_LOGGED);
	m_lstLog.find("ID",(int)StatusCodeSet::MSG_SYSTEM_CLIENT_LOGOUT,false,false,false);

	DbRecordSet lstTemp;
	lstTemp.copyDefinition(m_lstLog);
	lstTemp.merge(m_lstLog,true);
	lstTemp.sort(0);

	DbRecordSet lstReport;
	DefineList_Report_UserSessions(lstReport);

	int nSize=lstTemp.getRowCount();
	for(int i=0;i<nSize;i++)
	{
		if (lstTemp.getDataRef(i,"ID").toInt()!=StatusCodeSet::MSG_SYSTEM_CLIENT_LOGGED)
			continue;
		
		QStringList strLogMsg;
		QString strFind;
		QDateTime datStart=lstTemp.getDataRef(i,"Date").toDateTime();
		QDateTime datEnd=QDateTime(); //invalid datetime
		strLogMsg=lstTemp.getDataRef(i,"Text").toString().split(",");
		if (strLogMsg.size()!=6)
			continue;
		strFind=strLogMsg.at(5);
		//find logout by session:
		for(int k=i;k<nSize;k++)
		{
			if (lstTemp.getDataRef(k,"ID").toInt()!=StatusCodeSet::MSG_SYSTEM_CLIENT_LOGOUT)
				continue;
			QStringList lstMsg=lstTemp.getDataRef(k,"Text").toString().split(",");
			if (lstMsg.size()!=6)
				continue;
			if (lstMsg.at(5)==strFind)
			{
				datEnd=lstTemp.getDataRef(k,"Date").toDateTime();
				break;
			}
		}

		QString strUser=strLogMsg.at(0);
		strUser=strUser.replace("User","",Qt::CaseInsensitive);
		strUser=strUser.replace("has logged","",Qt::CaseInsensitive).trimmed();
		QString strIP=strLogMsg.at(2);
		strIP=strIP.replace("ip:","",Qt::CaseInsensitive).trimmed();
		QString strX=strLogMsg.at(4);
		int nUserID=strX.replace("user_id:","",Qt::CaseInsensitive).trimmed().toInt();
		lstReport.addRow();
		int nRow=lstReport.getRowCount()-1;
		lstReport.setData(nRow,"User",strUser);
		lstReport.setData(nRow,"IP",strIP);
		lstReport.setData(nRow,"UserID",nUserID);
		lstReport.setData(nRow,"LogIn",datStart);
		if (datStart!=datEnd && datEnd.isValid())
		{
			int nSec=datStart.secsTo(datEnd);
			if (nSec>=0)
			{
				nSec=nSec/60;
				lstReport.setData(nRow,"LogOut",datEnd);
				lstReport.setData(nRow,"Duration",nSec);
			}
		}
	}

	m_lstLog=lstReport;

	DbRecordSet columns;
	UniversalTableWidgetEx::AddColumnToSetup(columns,"User",tr("User"),100,false, "",UniversalTableWidgetEx::COL_TYPE_TEXT,"");
	UniversalTableWidgetEx::AddColumnToSetup(columns,"UserID",tr("User ID"),80,false, "",UniversalTableWidgetEx::COL_TYPE_TEXT,"");
	UniversalTableWidgetEx::AddColumnToSetup(columns,"IP",tr("IP"),100,false, "",UniversalTableWidgetEx::COL_TYPE_TEXT,"");
	UniversalTableWidgetEx::AddColumnToSetup(columns,"LogIn",tr("LogIn"),120,false, "",UniversalTableWidgetEx::COL_TYPE_TEXT,"");
	UniversalTableWidgetEx::AddColumnToSetup(columns,"LogOut",tr("LogOut"),120,false, "",UniversalTableWidgetEx::COL_TYPE_TEXT);
	UniversalTableWidgetEx::AddColumnToSetup(columns,"Duration",tr("Duration (min)"),80,false, "",UniversalTableWidgetEx::COL_TYPE_TEXT);
	ui.tableWidget->SetColumnSetup(columns);
	ui.tableWidget->RefreshDisplay();
	//filter msg id=
	m_bReportActive=true;
}

void Adm_ServerLog::DefineList_Report_UserSessions(DbRecordSet &lstData)
{
	lstData.destroy();
	lstData.addColumn(QVariant::String,"User");
	lstData.addColumn(QVariant::Int,"UserID");
	lstData.addColumn(QVariant::String,"IP");
	lstData.addColumn(QVariant::DateTime,"LogIn");
	lstData.addColumn(QVariant::DateTime,"LogOut");
	lstData.addColumn(QVariant::Int,"Duration");
}

void Adm_ServerLog::on_btnApplyReport_clicked()
{
	Report_UserSessions();
}



//from end of log file to the >stopDateTime  or start of file if stopDateTime=invalid
void Adm_ServerLog::ReadLogPrivate(Status &err, DbRecordSet &logRead, QDateTime stopDateTime, int &nLastSize)
{
	DefineLogList(logRead);

	nLastSize=-1;
	int nLastCurrentPosition=-1;

	QByteArray byteLog;
	QByteArray byteChunk;
	DbRecordSet recChunk;

	ui.progressBar->setRange(0,9);
	ui.progressBar->setVisible(true);
	g_pBoSet->app->ServerControl->GetLog(err,byteChunk,nLastCurrentPosition,nLastSize,LOG_CHUNK_SIZE);
	if (!err.IsOK())
		return;
	ParseRawData(byteChunk,recChunk);

	//recChunk.Dump();
	if (!CheckDate(recChunk,stopDateTime))
	{
		logRead.merge(recChunk);
		return;
	}
	else
		logRead.merge(recChunk);

	int nParts=nLastSize/LOG_CHUNK_SIZE;
	ui.progressBar->setRange(0,nParts+2);
	ui.progressBar->setValue(1);
	QCoreApplication::processEvents();
	int nCurrentChunk=1;

	while (nLastCurrentPosition>0)
	{
		g_pBoSet->app->ServerControl->GetLog(err,byteChunk,nLastCurrentPosition,nLastSize,LOG_CHUNK_SIZE);
		if (!err.IsOK())
		{
			ui.progressBar->setVisible(false);
			return;
		}
		ParseRawData(byteChunk,recChunk);
		if (!CheckDate(recChunk,stopDateTime)) 
		{
			//logRead.Dump();
			logRead.prependList(recChunk);
			//logRead.Dump();
			break;
		}
		else
			logRead.prependList(recChunk);

		nCurrentChunk++;
		ui.progressBar->setValue(nCurrentChunk);
		QCoreApplication::processEvents();
	}
}
