#include "reportlineandpoint.h"

ReportLineAndPoint::ReportLineAndPoint(QPainter *Painter)
{
	//Painter.
	m_pPainter	  = Painter;

	//Default rectangle values.
	m_Pen		  = QPen(QColor(Qt::black));

	//Original painter values for later restoring.
	m_nDefaultPen = m_pPainter->pen();
}

ReportLineAndPoint::~ReportLineAndPoint()
{

}

/*!
Paint single point on given coordinates.

\param Point		- Start point position.
*/
void ReportLineAndPoint::PaintPoint(QPointF Point)
{
	//Set painter defaults.
	m_pPainter->setPen(m_Pen);

	//Paint point.
	m_pPainter->drawPoint(Point);
	RestoreDefaults();
}

/*!
Paint line point to point.

\param StartPoint	- Start point position.
\param EndPoint		- End point position.
*/
void ReportLineAndPoint::PaintLine(QPointF StartPoint, QPointF EndPoint)
{
	//Set painter defaults.
	m_pPainter->setPen(m_Pen);
	m_pPainter->setRenderHint(QPainter::Antialiasing);

	//Paint point.
	m_pPainter->drawLine(StartPoint, EndPoint);
	RestoreDefaults();
}

void ReportLineAndPoint::RestoreDefaults()
{
	//Restore painter.
	m_pPainter->setPen(m_nDefaultPen);

	//Restore local values.
	m_Pen		= QPen(QColor(Qt::black));
}
