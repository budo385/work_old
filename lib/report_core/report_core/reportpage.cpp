#include "reportpage.h"
//

ReportPage::ReportPage()
{
	m_nPageSize			= QPrinter::A4;
	m_BackgroundColor	= Qt::white;
	m_nOrientation		= QPrinter::Portrait;
	m_nLeftMargin		= 5;
	m_nRightMargin		= 5;
	m_nTopMargin		= 5;
	m_nBottomMargin		= 5;
	m_nPageNumber		= 0;
	m_bFirstPage		= true;

	//Before and after detail or instance height.
	m_dblBeforeDetailHeight = 0;
	m_dblAfterDetailHeight  = 0;
}

ReportPage::~ReportPage()
{
	m_hshVariableHash.clear();
	m_hshDetailRecordSetHash.clear();
	m_lstReportPrintingItemsOrderVector.clear();
	
	qDeleteAll(m_hshReportPrintingItems);
	m_hshReportPrintingItems.clear();

	qDeleteAll(m_hshPrintingElementHash);
	m_hshPrintingElementHash.clear();
}

void ReportPage::InitializeReport(QString DetaiId, DbRecordSet ReportRecordSet)
{
	m_hshDetailRecordSetHash.insert(DetaiId, ReportRecordSet);
}

bool ReportPage::SetWizardRecordSetList(QList<DbRecordSet> &lstPageRecordSet)
{
	m_lstPageRecordSet = lstPageRecordSet;

	return true;
}

void ReportPage::SendRecordSetValuesToStack(Detail *detail, int RecordRow)
{
	QString DetailId = detail->m_strItemID;

	int ColumnCount = m_hshDetailRecordSetHash.value(DetailId).getColumnCount();
	for (int i = 0; i < ColumnCount; ++i)
	{
		QString ColumnName;//, ColumnAlias, ColumnTable;
		int ColumnDataType;

		m_hshDetailRecordSetHash.value(DetailId).getColumn(i, ColumnDataType, ColumnName);//, ColumnAlias, ColumnTable);

		//Find recordset variables on stack and insert value - if field exists on stack.
		if (m_hshVariableHash.contains(ColumnName))
			m_hshVariableHash.insert(ColumnName, m_hshDetailRecordSetHash.value(DetailId).getDataRef(RecordRow, ColumnName));
	}
}
