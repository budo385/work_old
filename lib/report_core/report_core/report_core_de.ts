<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_CH">
<context>
    <name>RptPrinter</name>
    <message>
        <location filename="rptprinter.cpp" line="28"/>
        <source>File open error!</source>
        <translation>Fehler beim Öffnen der Datei!</translation>
    </message>
    <message>
        <location filename="rptprinter.cpp" line="28"/>
        <source>Unable to open report file</source>
        <translation>Auswertungsdatei kann nicht geöffnet werden</translation>
    </message>
</context>
</TS>
