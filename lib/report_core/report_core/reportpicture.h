#ifndef REPORTPICTURE_H
#define REPORTPICTURE_H

#include <QPainter>

/*!
\brief Report picture class.
\ingroup 

Report picture definition class - used to print pictures, images and pixmaps on report.

*/
class ReportPicture
{
public:
    ReportPicture(QPainter *Painter);
    ~ReportPicture();

	void PaintImage(QRectF target, QImage &image, QRectF source, Qt::ImageConversionFlags flags = Qt::AutoColor);
	void PaintPicture(QPointF Point, QPicture &picture);
	void PaintPixmap(QRectF target, QPixmap &pixmap);
	void PaintPixmapRect(QRectF target, QPixmap &pixmap, QRectF source);

private:
	QPainter *m_pPainter;				//< Pointer to report painter.
	QMatrix m_DefaultMatrix;			//< Painter initial (before entering this object) matrix.
};

#endif // REPORTPICTURE_H
