#include "reportitem.h"

ReportItem::ReportItem()
{
	m_bHideItem = false;
	m_nDetailRowPrinted = -1;
}

ReportItem::~ReportItem()
{

}

PagePrintArea::~PagePrintArea()
{
	qDeleteAll(m_lstPrintingElements);
	m_lstPrintingElements.clear();
	qDeleteAll(m_hshPrintingElements);
	m_hshPrintingElements.clear();
}

PagePrintingBlock::~PagePrintingBlock()
{
	qDeleteAll(m_lstReportItems);
	m_lstReportItems.clear();
}

Detail::Detail()
{
	InitalizeMembers();
}

Detail::~Detail()
{
	qDeleteAll(m_hshDetailGroupHash);
	m_hshDetailGroupHash.clear();
	m_lstDetailGroupVector.clear();
}

void Detail::InitalizeMembers()
{
	//This is from ReportItem() but it also must be set.
	m_nDetailRowPrinted = -1;
	
	//Set recordset to start from 0.
	m_nRecordRowNumber	= 0;
	m_nLastPrintedDetailRecord = -1;

	m_bFirstPass			= true;
	m_bLastPass				= false;
	m_bPageFirstPass		= true;
	m_bPageLastPass			= false;
	m_bDetailContainsDetail = false;

	//Initialize children too.
	foreach(ReportItem *item, m_lstReportItems)
		item->m_nDetailRowPrinted = -1;
}
