#ifndef REPORTPARSER_H
#define REPORTPARSER_H

#include <QDomDocument>
#include <QVariant>
#include <QFile>
#include <QTextStream>
#include <QDir>
#include <QtWidgets/QMessageBox>

#include "reportitem.h"
#include "reportpage.h"
#include "reportprintingelement.h"

class ReportParser
{
public:
    ReportParser(QString ReportFile,ReportPage* pReportPage);
    ~ReportParser();

	bool ParseFile();

	void ParseDetail(QDomElement Element, ReportItem *Parent);
	void ParseDetailBreak(QDomElement Element, ReportItem *Parent);
	void ParseHeaderFooter(QDomElement Element, ReportItem *Parent);

	void ParseInstance(QDomElement Element, ReportItem *Parent);

	void GetPrintAreaItems(QDomElement Element, ReportItem *Parent);
	QColor Convert2Color(QString ColorString);
	QFont GetLabelFont(QDomElement Element);
	int	GetAlignament(QDomElement Element);

	QString						m_strReportFile;		//< Report file (full path).
	QString						m_strReportPath;		//< Report full path.
	ReportPage					*m_pRepPage;			//< Pointer to report page.
};

#endif // REPORTPARSER_H
