#ifndef REPORTLABEL_H
#define REPORTLABEL_H

#include <QPainter>

/*!
\brief Report label class.
\ingroup 

Report label definition class - used to print text (labels and fields) on report.

*/
class ReportLabel
{
	//TODO add line spacing.
	//int		m_nLineSpacing;				//< Line spacing (for text wrap).
public:
    ReportLabel(QPainter *Painter);
    ~ReportLabel();

	void PaintSingleLine(QPointF Point, QString LabelText, int LabelLength = -1);
	void PaintSingleLine(QPointF Point, int RotationAngle, QString LabelText, int LabelLength = -1);
	void PaintMultiLine(QPointF Point, QString LabelText, int LabelLength, int LablelMaxLinesNumber, int nWrap);
	int	 PaintMultiLineNew(QPointF Point, QString LabelText, int LabelLength, int LablelMaxLinesNumber, int nWrap);
	int	 PaintMultiLineNewInMM(QPointF Point, QString LabelText, int LabelLength, int LablelMaxLinesNumber, int nWrap);

	int		m_nWrap;					//< Wrap text.
	int		m_nHAlignment;				//< Horizontal alignment.
	int		m_nVAlignment;				//< Vertical alignment.
	QFont	m_Font;						//< Label font.
	QPen	m_nPen;						//< Label pen.
	QBrush	m_BackgroundBrush;			//< Label background brush.
	
private:
	void RestoreDefaults();

	QPainter *m_pPainter;				//< Pointer to report painter.

	QFont	m_DefaultFont;				//< Painter initial (before entering this object) font.
	QPen	m_nDefaultPen;				//< Painter initial (before entering this object) pen.
	QBrush	m_DefaultBackgroundBrush;	//< Painter initial (before entering this object) background brush.
	QMatrix m_DefaultMatrix;			//< Painter initial (before entering this object) matrix.
};

#endif // REPORTLABEL_H
