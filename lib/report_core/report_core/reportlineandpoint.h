#ifndef REPORTLINEANDPOINT_H
#define REPORTLINEANDPOINT_H

#include <QPainter>

/*!
\brief Report line and point.
\ingroup 

Report line and point definition class - used to print lines and points on report.

*/
class ReportLineAndPoint
{
public:
    ReportLineAndPoint(QPainter *Painter);
    ~ReportLineAndPoint();

	void PaintPoint(QPointF Point);
	void PaintLine(QPointF StartPoint, QPointF EndPoint);

	QPen		m_Pen;					//< Line pen.

private:
	void RestoreDefaults();
    
	QPainter	*m_pPainter;			//< Pointer to report painter.
	QPen		m_nDefaultPen;			//< Painter initial (before entering this object) pen.
};

#endif // REPORTLINEANDPOINT_H
