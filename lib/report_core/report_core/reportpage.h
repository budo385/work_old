#ifndef REPORTPAGE_H
#define REPORTPAGE_H

//Insert macro (just for simplicity).
#define INS(x,y) m_hshVariableHash.insert(#x, y)
#define GET(x) m_hshVariableHash.value(#x)

#include "common/common/dbrecordset.h"
#include "reportitem.h"

#include <QObject>
#include <QHash>
#include <QVariant>
#include <QColor>
#include <QtPrintSupport/QPrinter>

class ReportPage : public QObject
{
public:
    ReportPage();
    ~ReportPage();

	//---------------------------------------------------------------------------------------------
	//								PAGE METHOD CALLS AND REPORT INITIALIZATION
	//---------------------------------------------------------------------------------------------
	void			InitializeReport(QString DetaiId, DbRecordSet ReportRecordSet);
	virtual bool	SetWizardRecordSetList(QList<DbRecordSet> &lstPageRecordSet);
	virtual void	ReportMethodCall(QString FunctionName){};

	//---------------------------------------------------------------------------------------------
	//										PAGE PROPERTIES
	//---------------------------------------------------------------------------------------------
	QString							 m_strReportID;							//< Report ID.
	bool							 m_bCallInterface;						//< Call interface bool. If true Printer will call report interface to fill detail recordsets (See departments report).
	int								 m_nPageSize;							//< Page format.
	int								 m_nOrientation;						//< Page orientation.
	int								 m_nWidth;								//< Page width.
	int								 m_nHeight;								//< Page height.
	int								 m_nResolution;							//< Page resolution.
	QColor							 m_BackgroundColor;						//< Page background color.
	int								 m_nLeftMargin;							//< Left margin.
	int								 m_nRightMargin;						//< Right margin.
	int								 m_nTopMargin;							//< Top margin.
	int								 m_nBottomMargin;						//< Bottom margin.
	int								 m_nPageNumber;							//< Page number.
	bool							 m_bHasMorePages;						//< Has more pages bool.
	bool							 m_bFirstPage;							//< First page bool - for calculating page header and footer.
	bool							 m_bLastPage;							//< Has more pages bool.
	QFont							 m_fntDefaultFont;						//< Default font.
	int								 m_fntDefaultFontLineSpacing;			//< Default line spacing (when changing default font size MUST change this too).
	int								 m_nLogDPIX;							//< Logical DPI of x axis.
	int								 m_nLogDPIY;							//< Logical DPI of y axis.

	//---------------------------------------------------------------------------------------------
	//										SOME REPORT UTILITIES
	//---------------------------------------------------------------------------------------------
	double							 m_dblBeforeDetailHeight;				//< Height of printing areas before detail.
	double							 m_dblAfterDetailHeight;				//< Height of printing areas after detail.
	QList<DbRecordSet>				 m_lstPageRecordSet;					//< Report wizard recordset list.

	//---------------------------------------------------------------------------------------------
	//										REPORT STACK
	//---------------------------------------------------------------------------------------------
	QHash<QString, QVariant>		 m_hshVariableHash;						//< Variable hash (fieldID, value).
	QHash<QString, DbRecordSet>		 m_hshDetailRecordSetHash;				//< Detail recordset hash.
	QHash<QString, int>				 m_hshDetailToTableIDHash;				//< Detail to it's table view hash.
	QHash<QString, int>				 m_hshDetailToTableViewHash;			//< Detail to it's table view hash.
	QHash<QString, QString>			 m_hshDetailToWhereConditionHash;		//< Detail to it's where condition hash.

	void SendRecordSetValuesToStack(Detail *detail, int RecordRow);

	//---------------------------------------------------------------------------------------------
	//										REPORT PRINTING ELEMENTS
	//---------------------------------------------------------------------------------------------
	QVector<QString>				 m_lstReportPrintingItemsOrderVector;	//< Report printing item ID order list (for printing in some order).
	QHash<QString, ReportItem*>		 m_hshReportPrintingItems;				//< Report item ID to pointer hash.
	QHash<QString, PrintingElement*> m_hshPrintingElementHash;				//< Printing element name to pointer hash.
};

#endif // REPORTPAGE_H
