#include "rptprinter.h"

RptPrinter::RptPrinter()
{
	m_nPrintOffsetY			= 0;
	m_nRptPageNumber		= 1;
	m_pReportDataInterface	= new ReportDataInterface();
}

RptPrinter::~RptPrinter()
{
	delete(m_pReportDataInterface);
	m_pReportPage			= NULL;
	m_pPaintDevice			= NULL;
}

bool RptPrinter::InitalizeReport(QString strReportFileFullPath, int nReportID, QString strReportName,ReportPage *pageData,QList<DbRecordSet> &lstPageRecordSet)
{
	m_nReportID=nReportID; //ReportRegistration::GetReportIDFromFile(ReportFile);
	//m_pReportPage=pageData;
	//ReportRegistration::GetReportPage(m_nReportID,&m_pReportPage);
	m_strReportName=strReportName; //ReportRegistration::GetReportName(m_nReportID);

	//Parse report definition (initialize the report with values from report definition XML).
	ReportParser parser(strReportFileFullPath, pageData);
	if (!parser.ParseFile())
	{
		QMessageBox::warning(NULL, tr("File open error!"), tr("Unable to open report file"));
		return false;
	}

	//Finally get pointer to report page.
	m_pReportPage = parser.m_pRepPage;

	//Give report his wizard pages and test if it will print.
	if (!m_pReportPage->SetWizardRecordSetList(lstPageRecordSet))
		return false;

	//Get detail data from interface (if true, else you must get detail recordsets from report).
	if(m_pReportPage->m_bCallInterface)
		m_pReportDataInterface->GetDetailRecordSet(m_pReportPage);

	return true;
}

void RptPrinter::PrintReportToPrinter(QPrinter *printer)
{
	//m_pPrinter = printer;
	bool hasMorePages = true;

	//Set orientation;
	printer->setPageSize((QPrinter::PageSize)m_pReportPage->m_nPageSize);
	printer->setOrientation((QPrinter::Orientation)m_pReportPage->m_nOrientation);

	//Begin with painter.
	m_Painter.begin(printer);

	//Set default values.
	SetDefaultValues();

	while (hasMorePages)
	{
		hasMorePages = printReportPageToPrinter(printer->logicalDpiX(),printer->logicalDpiY(),printer->physicalDpiX(),printer->physicalDpiY());
		//If has more pages then say it to printer.
		if (hasMorePages)
			printer->newPage();
	}

	m_Painter.end();
}

bool RptPrinter::printReportPageToPrinter(int nlogDpiX, int nlogDpiY, int nphysDpiX, int nphysDpiY)
{
	//New page bool and page top offset (top margin).
	m_bNewPage = false;
	m_nPrintOffsetY = m_pReportPage->m_nTopMargin;

	//Get device metrics.
	m_nLogDPIX = nlogDpiX;
	m_nLogDPIY = nlogDpiY;
	
	int nPhyDPIX = nphysDpiX;
	int nPhyDPIY = nphysDpiY;

	m_pReportPage->m_nLogDPIX = m_nLogDPIX;
	m_pReportPage->m_nLogDPIY = m_nLogDPIY;

	if (m_pReportPage->m_bFirstPage == true)
	{
		CalculatePageBeforeDetailHeight();
		CalculatePageAfterDetailHeight();
		//Calculate this spaces only on first page.
		m_pReportPage->m_bFirstPage = false;
	}

	//Get report printing items from report item order vector.
	QVectorIterator<QString> iter(m_pReportPage->m_lstReportPrintingItemsOrderVector);
	int cc = m_pReportPage->m_lstReportPrintingItemsOrderVector.count();
	while (iter.hasNext())
	{
		//Get item ID and then pointer to report printing item.
		QString ItemID = iter.peekNext();
		ReportItem *item = m_pReportPage->m_hshReportPrintingItems.value(ItemID);
		//Page header or footer.
		if (item->m_nItemType == ReportItem::PRINT_AREA)
			PrintPrintArea(item);
		else if (item->m_nItemType == ReportItem::FUNCTION_CALL)
			CallFunction(item);
		else if (item->m_nItemType == ReportItem::DETAIL && !m_bNewPage)
		{
			if (!PrintDetail(item))
				m_bNewPage = true;
		}
		else if (item->m_nItemType == ReportItem::WIDGET_DETAIL && !m_bNewPage)
		{
			if (!PrintWidgetDetail(item))
				m_bNewPage = true;
		}
		else if (item->m_nItemType == ReportItem::PRINT_INSTANCE && !m_bNewPage)
		{
			if (!PrintReportInstance(item))
				m_bNewPage = true;
		}
		else if (item->m_nItemType == ReportItem::PAGE_BREAK && !m_bNewPage)
			m_bNewPage = true;

		iter.next();
	}

	m_nRptPageNumber = m_pReportPage->m_nPageNumber++;

	return m_bNewPage;
}

bool RptPrinter::printReportPage(QPaintDevice *device)
{
	m_pPaintDevice = device;

	//New page bool and page top offset (top margin).
	m_bNewPage = false;
	m_nPrintOffsetY = m_pReportPage->m_nTopMargin;

	//Get device metrics.
	m_nLogDPIX = m_pPaintDevice->logicalDpiX();
	m_nLogDPIY = m_pPaintDevice->logicalDpiY();

	int nPhyDPIX = m_pPaintDevice->physicalDpiX();
	int nPhyDPIY = m_pPaintDevice->physicalDpiY();

	m_pReportPage->m_nLogDPIX = m_nLogDPIX;
	m_pReportPage->m_nLogDPIY = m_nLogDPIY;

	//Begin with painter.
	m_Painter.begin(m_pPaintDevice);

	//Set default values.
	SetDefaultValues();

	if (m_pReportPage->m_bFirstPage == true)
	{
		CalculatePageBeforeDetailHeight();
		CalculatePageAfterDetailHeight();
		//Calculate this spaces only on first page.
		m_pReportPage->m_bFirstPage = false;
	}

	//Get report printing items from report item order vector.
	QVectorIterator<QString> iter(m_pReportPage->m_lstReportPrintingItemsOrderVector);
	int cc = m_pReportPage->m_lstReportPrintingItemsOrderVector.count();
	while (iter.hasNext())
	{
		//Get item ID and then pointer to report printing item.
		QString ItemID = iter.peekNext();
		ReportItem *item = m_pReportPage->m_hshReportPrintingItems.value(ItemID);
		//Page header or footer.
		if (item->m_nItemType == ReportItem::PRINT_AREA)
			PrintPrintArea(item);
		else if (item->m_nItemType == ReportItem::FUNCTION_CALL)
			CallFunction(item);
		else if (item->m_nItemType == ReportItem::DETAIL && !m_bNewPage)
		{
			if (!PrintDetail(item))
				m_bNewPage = true;
		}
/*		else if (item->m_nItemType == ReportItem::WIDGET_DETAIL && !m_bNewPage)
		{
			if (!PrintWidgetDetail(item))
				m_bNewPage = true;
		}*/
		else if (item->m_nItemType == ReportItem::PRINT_INSTANCE && !m_bNewPage)
		{
			if (!PrintReportInstance(item))
				m_bNewPage = true;
		}
		else if (item->m_nItemType == ReportItem::PAGE_BREAK && !m_bNewPage)
			m_bNewPage = true;
		
		iter.next();
	}

	m_nRptPageNumber = m_pReportPage->m_nPageNumber++;
	m_Painter.end();

	return m_bNewPage;
}

bool RptPrinter::PrintReportInstance(ReportItem *Item)
{
	//Cast item to detail.
	PrintInstance *instance = static_cast<PrintInstance*>(Item);

	if (instance->m_bHideItem)
		return true;

	//Go through instance elements (only function calls and print areas).
	//For now handle only print elements. 
	QVector<ReportItem*> ElemList = instance->m_lstReportItems;
	QVectorIterator<ReportItem*> iter(ElemList);
	while (iter.hasNext())
	{
		ReportItem *item = iter.peekNext();

		if (item->m_nItemType == ReportItem::PRINT_AREA)
			PrintPrintArea(item);
		else if (item->m_nItemType == ReportItem::FUNCTION_CALL)
			CallFunction(item);

		//Initialize fields that have to be initialized in instance.
		UpdateFields(instance);
		InitializeFields(instance);
		
		//Set instance printing bool to false - don't print any more.
		instance->m_bHideItem= true;
		
		iter.next();
	}
	
	return true;
}

void RptPrinter::CallFunction(ReportItem *Item)
{
	QString functionName = Item->m_strItemID;
	m_pReportPage->ReportMethodCall(functionName);
}

bool RptPrinter::PrintDetail(ReportItem *Item)
{
	//Cast item to detail.
	Detail *detail = static_cast<Detail*>(Item);

	//See do we print it at all.
	if (detail->m_bHideItem)
		return true;

	detail->m_bPageFirstPass = true;

	int RowCount = m_pReportPage->m_hshDetailRecordSetHash.value(detail->m_strItemID).getRowCount();
	for (int i = detail->m_nRecordRowNumber; i < RowCount; ++i)
	{
		//*********************************************************************
		//			First do some checking and variable updating.
		//*********************************************************************
		//Set detail current record row number.
		detail->m_nRecordRowNumber = i;

		if (i == RowCount-1)
			detail->m_bLastPass = true;

		//If we can then go on and print.
		//Send recordset values to stack hash - before function calls.
		m_pReportPage->SendRecordSetValuesToStack(detail, i);

		//*********************************************************************
		//			Then print detail footer if it's time to do so.
		//*********************************************************************
		if (!PrintDetailGroupsFooter(detail, i))
			return false;

		//See can you print another detail area (will it fit on page).
		int lineHeight = m_pReportPage->m_fntDefaultFontLineSpacing;
		if ((detail->m_nHeight*lineHeight/m_nLogDPIY*25.4  + m_nPrintOffsetY/m_nLogDPIY*25.4 + m_pReportPage->m_dblAfterDetailHeight) 
			> (m_pReportPage->m_nHeight - m_pReportPage->m_nBottomMargin))
			return false;

		//Go through detail elements but only call detail function calls and print areas. 
		QVector<ReportItem*> ElemList = detail->m_lstReportItems;
		QVectorIterator<ReportItem*> iter(ElemList);
		while (iter.hasNext())
		{
			ReportItem *item = iter.peekNext();
			
			//If we already printed this detail or this item was already printed for this record - go to next.
			if (detail->m_nLastPrintedDetailRecord == i || item->m_nDetailRowPrinted == i)
			{
				iter.next();
				continue;
			}

			//*********************************************************************
			//				Print detail header if it's time to do so.
			//*********************************************************************
			if (!PrintDetailGroupsHeader(detail, i))
				return false;

			if (item->m_nItemType == ReportItem::DETAIL)
			{
				//Every time we print detail in detail we must reinitialize it's members.
				static_cast<Detail*>(item)->InitalizeMembers();
				PrintDetail(item);
				//if (!PrintDetail(item))
				//{
					//item->m_nDetailRowPrinted = i;
					//return false;
				//}
			}
			else if (item->m_nItemType == ReportItem::WIDGET_DETAIL)
			{
				if (!PrintWidgetDetail(item))
					return false;
			}
			else if (item->m_nItemType == ReportItem::PRINT_AREA)
				PrintPrintArea(item);
			else if (item->m_nItemType == ReportItem::FUNCTION_CALL)
			{
				CallFunction(item);
				//Set this item is printed for this row.
				item->m_nDetailRowPrinted = i;
				iter.next();
				continue;
			}
			else if (item->m_nItemType == ReportItem::PAGE_BREAK)
			{
				//Set this item is printed for this row.
				item->m_nDetailRowPrinted = i;
				//If this is the last record and break should not appear on last break.
				if ((i == RowCount-1) && !(static_cast<PageBreak*>(item)->m_bBreakOnLastDetail))
				{
					iter.next();
					continue;
				}
				else
					return false;
			}
			
			//Initialize fields that have to be initialized in detail.
			UpdateFields(detail);
			InitializeFields(detail);

			//If we go through detail for the first, next time it's not the first.
			detail->m_bPageFirstPass = false;
			if (i == 0)
				detail->m_bFirstPass = false;
			
			//Set this item is printed for this row.
			item->m_nDetailRowPrinted = i;

			iter.next();
		}

		//Set this record to be the last printed.
		detail->m_nLastPrintedDetailRecord = i;

		if (detail->m_bLastPass || detail->m_bPageLastPass)
			if (!PrintDetailGroupsFooterLastPass(detail))
				return false;
	}

	return true;
}

bool RptPrinter::PrintWidgetDetail(ReportItem *Item)
{
	//Cast item to detail.
	WidgetDetail *widget_detail = static_cast<WidgetDetail*>(Item);

	//See do we print it at all.
	if (widget_detail->m_bHideItem)
		return true;

	double dAlreadyPrintedWidgetHeight = widget_detail->m_dAlreadyPrintedWidgetHeight;

	bool bDetailPrintedCompletely = false;
	QRect  TargetRect, SourceRect;

	//Calculate printed widget height.
	double Xpos			= widget_detail->m_dX*m_nLogDPIX/25.4;
	double Ypos			= widget_detail->m_dY*m_nLogDPIY/25.4;
	double detailWidth	= widget_detail->m_dDetailWidth*m_nLogDPIX/25.4;
	double detailHeight = widget_detail->m_dDetailHeight*m_nLogDPIY/25.4;
	QPixmap pixPixmap(widget_detail->m_PrintingWidgetPixmap);

	//Calculate available space to print the detail.
	int nPageAvailableSpace = m_pReportPage->m_nHeight*m_nLogDPIY/25.4 - m_nPrintOffsetY - m_pReportPage->m_dblAfterDetailHeight*m_nLogDPIY/25.4 - m_pReportPage->m_nBottomMargin*m_nLogDPIY/25.4;
	
	//Set rectangles.
	TargetRect.setX(Xpos);
	TargetRect.setY(Ypos + m_nPrintOffsetY);
	TargetRect.setWidth(detailWidth);

	SourceRect.setX(0);
	SourceRect.setY(widget_detail->m_dAlreadyPrintedWidgetHeight);
	SourceRect.setWidth(pixPixmap.width());
	
	//If detail height is 0 then use as much hight as needed.
	if (detailHeight == 0)
	{
		int nPixmapHeight = pixPixmap.height() - dAlreadyPrintedWidgetHeight;
		if (nPageAvailableSpace >= nPixmapHeight)
		{
			TargetRect.setHeight(nPixmapHeight);
			SourceRect.setHeight(nPixmapHeight);
			bDetailPrintedCompletely = true;
			m_nPrintOffsetY += nPixmapHeight;
		}
		else
		{
			//Check is printing step set. If true then print on step.
			if(widget_detail->m_nPrintStep > 0)
			{
				int nPrintingAreaHeight = QVariant(floorf(nPageAvailableSpace/widget_detail->m_nPrintStep)*widget_detail->m_nPrintStep).toInt();
				TargetRect.setHeight(nPrintingAreaHeight);
				SourceRect.setHeight(nPrintingAreaHeight);
				dAlreadyPrintedWidgetHeight = dAlreadyPrintedWidgetHeight + nPrintingAreaHeight;
				bDetailPrintedCompletely = false;
				m_nPrintOffsetY += nPrintingAreaHeight;
			}
			else
			{
				TargetRect.setHeight(nPageAvailableSpace);
				SourceRect.setHeight(nPageAvailableSpace);
				dAlreadyPrintedWidgetHeight = dAlreadyPrintedWidgetHeight + nPageAvailableSpace;
				bDetailPrintedCompletely = false;
				m_nPrintOffsetY += nPageAvailableSpace;
			}
		}
	}
	else //For now not supported that height of this area can be something.
		Q_ASSERT(0);

	widget_detail->m_dAlreadyPrintedWidgetHeight = dAlreadyPrintedWidgetHeight;

	//Print pixmap.
	ReportPicture pixmap(&m_Painter);
	pixmap.PaintPixmapRect(TargetRect, pixPixmap, SourceRect);

	m_nPrintOffsetY += Ypos;
	
	return bDetailPrintedCompletely;
}

void RptPrinter::PrintPrintArea(ReportItem *Item)
{
	//Get print area pointer.
	PagePrintArea *print_area = static_cast<PagePrintArea*>(Item);

	//See do we print it at all.
	if (print_area->m_bHideItem)
		return;

	//Height of single line (calculated from default font).
	int lineHeight = m_pReportPage->m_fntDefaultFontLineSpacing;

	//Print area height, in lines (calculated from printing elements - for shrinking areas).
	int printAreaHeightInLines = 0;
	int nShrinkingMaxLines = 0;
	bool bShrink = print_area->m_bShrink;

	//Get print area height (in defined in lines).
	double PrintOffsetY = print_area->m_nHeight*lineHeight; 

	//Get print area printing elements and print them.
	QVector<PrintingElement*> ElemList = print_area ->m_lstPrintingElements;
	QVectorIterator<PrintingElement*> iter(ElemList);
	while (iter.hasNext())
	{
		PrintingElement *Element = iter.peekNext();

		//Check is this item hidden.
		if (Element->m_bHideElement)
		{
			iter.next();
			continue;
		}

		if (Element->m_nElementType == PrintingElement::POINT)
		{
			//Define point painter.
			ReportLineAndPoint point(&m_Painter);

			//Cast point element.
			PointElement* pointElem = static_cast<PointElement*>(Element);

			//Offset point and get values.
			double Xpos = pointElem ->m_dX*m_nLogDPIX/25.4;
			double Ypos = pointElem ->m_dY*m_nLogDPIY/25.4;
			//Set values.
			point.m_Pen = pointElem->m_Pen;

			//For shrinking areas calculate maximum line.
			if (bShrink)
			{
				int StartingLine = ceil(Ypos/lineHeight);
				if (StartingLine > printAreaHeightInLines)
					printAreaHeightInLines = StartingLine;
			}

			point.PaintPoint(QPointF(Xpos, Ypos + m_nPrintOffsetY));
		}
		else if (Element->m_nElementType == PrintingElement::LINE)
		{
			//Define line painter.
			ReportLineAndPoint Line(&m_Painter);

			//Cast line element.
			LineElement* lineElem = static_cast<LineElement*>(Element);

			double PrntOffsetY = print_area->m_nHeight*m_nLogDPIY/25.4;

			//Offset line and get values.
			double XStartpos = lineElem->m_dStartX*m_nLogDPIX/25.4;
			double XEndpos	 = lineElem->m_dEndX*m_nLogDPIX/25.4;
			
			double YStartpos = 0;
			double YEndpos	 = 0;
			if (lineElem->m_dStartY >= 0)
			{
				YStartpos = (lineElem->m_dStartY)*m_nLogDPIY/25.4;
				YEndpos	 = (lineElem->m_dEndY)*m_nLogDPIY/25.4;
			}
			else
			{
				//Print in the middle of line.
				YStartpos = lineElem->m_dLine*lineHeight;
				YEndpos = YStartpos;
			}

			//Set values.
			Line.m_Pen = lineElem->m_Pen;

			//For shrinking areas calculate maximum line.
			if (bShrink)
			{
				int StartingLine = ceil(YStartpos/lineHeight);
				if (StartingLine > printAreaHeightInLines)
					printAreaHeightInLines = StartingLine;
			}

			Line.PaintLine(QPointF(XStartpos, YStartpos + m_nPrintOffsetY), QPointF(XEndpos, YEndpos + m_nPrintOffsetY));
		}
		else if (Element->m_nElementType == PrintingElement::RECTANGLE)
		{
			//Define rectangle painter.
			ReportRectangle rectangle(&m_Painter);

			//Cast rectangle element.
			RectangleElement* rect = static_cast<RectangleElement*>(Element);

			//Offset rectangle and get values.
			double Xpos = rect->m_dX*m_nLogDPIX/25.4;
			double Ypos = rect->m_dY*m_nLogDPIY/25.4;
			double rectWidth  = rect->m_dWidth*m_nLogDPIX/25.4;
			double rectHeight = rect->m_dHeight*m_nLogDPIY/25.4;
			QRectF TargetRect(Xpos, Ypos + m_nPrintOffsetY, rectWidth, rectHeight);

			//Set values.
			rectangle.m_BackgroundBrush = rect->m_BackgroundBrush;
			rectangle.m_Pen = rect->m_Pen;

			//For shrinking areas calculate maximum line.
			if (bShrink)
			{
				int EndingLine = ceil((Ypos + rectHeight)/lineHeight);
				if (EndingLine > printAreaHeightInLines)
					printAreaHeightInLines = EndingLine;
			}

			rectangle.PaintRoundedRectangle(TargetRect, rect->m_nRotation, rect->m_nXRnd, rect->m_nYRnd);
		}
		else if (Element->m_nElementType == PrintingElement::PIXMAP)
		{
			//Define rectangle painter.
			ReportPicture pixmap(&m_Painter);

			//Cast rectangle element.
			PixmapElement* pix = static_cast<PixmapElement*>(Element);

			//Offset rectangle and get values.
			double Xpos		 = pix->m_dX*m_nLogDPIX/25.4;
			double Ypos		 = pix->m_dY*m_nLogDPIY/25.4;
			double pixWidth  = pix->m_dWidth*m_nLogDPIX/25.4;
			double pixHeight = pix->m_dHeight*m_nLogDPIY/25.4;
			QRectF TargetRect(Xpos, Ypos + m_nPrintOffsetY, pixWidth , pixHeight);

			//For shrinking areas calculate maximum line.
			if (bShrink)
			{
				int EndingLine = ceil((Ypos + pixHeight)/lineHeight);
				if (EndingLine > printAreaHeightInLines)
					printAreaHeightInLines = EndingLine;
			}

			//Get pixmap from somewhere.
			if (!pix->m_strDataField.isEmpty())
			{
				QByteArray byte = m_pReportPage->m_hshVariableHash.value(pix->m_strDataField).toByteArray();
				QPixmap pixPixmap;
				pixPixmap.loadFromData(byte);
				pixPixmap.scaled(pixWidth, pixHeight, Qt::KeepAspectRatio);
				pixmap.PaintPixmap(TargetRect, pixPixmap);
			}
			else
				pixmap.PaintPixmap(TargetRect, pix->m_Pixmap);
		}
		else if (Element->m_nElementType == PrintingElement::LABEL) /*SINGLE LINE ONLY, FOR NOW*/
		{
			LabelElement* label = static_cast<LabelElement*>(Element);
			//Calculate values.
			double Xpos = label->m_dX*m_nLogDPIX/25.4;
			
			double Ypos;
			if (label->m_dY >= 0)
				Ypos = label->m_dY*m_nLogDPIY/25.4;
			else
				Ypos = label->m_dLine*lineHeight;

			QPointF Position(Xpos, Ypos + m_nPrintOffsetY);
			ReportLabel lab(&m_Painter);

			//Get printing value from variable value stack.
			QString fieldText = QVariant(m_pReportPage->m_hshVariableHash.value(label->m_strElementID)).toString();

			//For shrinking areas calculate maximum line.
			if (bShrink && !fieldText.isEmpty())
			{
				int EndingLine = 0;
				if (label->m_dY >= 0)
					EndingLine = ceil(Ypos/lineHeight);
				else
					EndingLine = label->m_dLine;
				
				if (EndingLine > printAreaHeightInLines)
					printAreaHeightInLines = EndingLine;
			}

			//Set font.
			lab.m_Font = label->m_Font;
			lab.m_nHAlignment = label->m_nHAlignment;
			lab.m_BackgroundBrush = label->m_BackgroundBrush;

			//Don't print if text is empty.
			if (!fieldText.isEmpty())
				lab.PaintSingleLine(Position, label->m_nRotationAngle, fieldText, label->m_nLabelLength);
		}
		else if (Element->m_nElementType == PrintingElement::FIELD)
		{
			FieldElement* field = static_cast<FieldElement*>(Element);

			//Calculate values.
			double Xpos = field->m_dX*m_nLogDPIX/25.4;
			double Ypos;
			if (field->m_dY >= 0)
				Ypos = field->m_dY*m_nLogDPIY/25.4;
			else
				Ypos = field->m_dLine*lineHeight;

			QPointF Position(Xpos, Ypos + m_nPrintOffsetY);

			ReportLabel lab(&m_Painter);

			//Set font.
			lab.m_Font = field->m_Font;
			lab.m_nHAlignment = field->m_nHAlignment;

			//Get printing value from variable value stack.
			QString fieldText;
			if (field->m_strFieldType == "pageNumber")
				fieldText = QVariant(m_pReportPage->m_nPageNumber + 1).toString();
			else if (field->m_strFieldType == "Date")
				fieldText = QDate::currentDate().toString(Qt::LocalDate);
			else if (field->m_strFieldType == "DateField")
				fieldText = QVariant(m_pReportPage->m_hshVariableHash.value(field->m_strElementID)).toDate().toString(Qt::LocalDate);
			else if (field->m_strFieldType == "reportName")
				fieldText = QVariant(m_nReportID).toString() + " " + m_strReportName;
			else
				fieldText = QVariant(m_pReportPage->m_hshVariableHash.value(field->m_strElementID)).toString();


			//For shrinking areas calculate maximum line.
			if (bShrink && !fieldText.isEmpty())
			{
				int EndingLine = 0;
				if (field->m_dY >= 0)
					EndingLine = ceil(Ypos/lineHeight);
				else
					EndingLine = field->m_dLine;

				if (EndingLine > printAreaHeightInLines)
					printAreaHeightInLines = EndingLine;
			}

			//Don't print if text is empty.
			if (!fieldText.isEmpty())
				lab.PaintSingleLine(Position, field->m_nRotationAngle, fieldText, field->m_nLabelLength);
		}
		else if (Element->m_nElementType == PrintingElement::MULTILINE_FIELD)
		{
			FieldElement* field = static_cast<FieldElement*>(Element);

			//Calculate values.
			double Xpos = field->m_dX*m_nLogDPIX/25.4;
			double Ypos;
			if (field->m_dY >= 0)
				Ypos = field->m_dY*m_nLogDPIY/25.4;
			else
				Ypos = field->m_dLine*lineHeight;

			int widthInMM = 0;
			if (field->m_strWidthInMM == "1")	//This in fact is in pixels on screen.
				widthInMM = field->m_nLabelLength*m_nLogDPIY/25.4;

			QPointF Position(Xpos, Ypos + m_nPrintOffsetY);

			ReportLabel lab(&m_Painter);

			if (field->m_strDataType == "html")
				ParseHtmlText(field);

			//Get printing value from variable value stack.
			QString fieldText = QVariant(m_pReportPage->m_hshVariableHash.value(field->m_strElementID)).toString();

			//Set font.
			lab.m_Font = field->m_Font;
			lab.m_nHAlignment = field->m_nHAlignment;

			//Don't print if text is empty.
			int nLines = 0;
			if (!fieldText.isEmpty())
			{
				if (field->m_strWidthInMM == "1")
					nLines = lab.PaintMultiLineNewInMM(Position, fieldText, widthInMM, field->m_nMaxLineNumbers, field->m_nWrap);
				else
					nLines = lab.PaintMultiLineNew(Position, fieldText, field->m_nLabelLength, field->m_nMaxLineNumbers, field->m_nWrap);
			}

			//For shrinking areas calculate maximum line.
			if (bShrink && !fieldText.isEmpty())
			{
				int EndingLine = 0;
				if (field->m_dY >= 0)
					EndingLine = ceil(Ypos/lineHeight + nLines);
				else
					EndingLine = field->m_dLine + nLines;

				if (EndingLine > printAreaHeightInLines)
					printAreaHeightInLines = EndingLine;
			}
		}

		iter.next();
	}

	//Offset Y.
	if (bShrink)
		m_nPrintOffsetY += printAreaHeightInLines*lineHeight;
	else
		m_nPrintOffsetY += PrintOffsetY;
}

void RptPrinter::PrintHeaderFooter(ReportItem *Item)
{
	//Cast item to PagePrintingBlock.
	PagePrintingBlock *header = static_cast<PagePrintingBlock*>(Item);

	//See do we print it at all.
	if (header->m_bHideItem)
		return;

	//For now handle only print elements. 
	QVector<ReportItem*> ElemList = header->m_lstReportItems;
	QVectorIterator<ReportItem*> iter(ElemList);
	while (iter.hasNext())
	{
		ReportItem *Item = iter.peekNext();
		if (Item->m_nItemType == ReportItem::PRINT_AREA)
			PrintPrintArea(Item);
		else if (Item->m_nItemType == ReportItem::FUNCTION_CALL)
			CallFunction(Item);

		iter.next();
	}
}

bool RptPrinter::PrintDetailGroupsHeader(Detail *DetailItem, int CurrentRow)
{
	QVector<QString> DetailIDVector = DetailItem->m_lstDetailGroupVector;
	QVectorIterator<QString> iter(DetailIDVector);
	while (iter.hasNext())
	{
		//Get detail id.
		QString DetailID = iter.peekNext();

		//Check is it ready for printing.
		//Get detail group.
		DetailGroup *DetGroup = DetailItem->m_hshDetailGroupHash.value(DetailID);
		//Get break field and it's last recorded value.
		QString BreakFieldID = DetGroup->m_strBreakFieldID;
		QVariant BreakFieldValue = DetGroup->m_varBreakFieldValue;
		//Get printing element from report printing element hash (because we must get element name).
		//PrintingElement *print_element = m_pReportPage->m_hshPrintingElementHash.value(BreakFieldID);
		//Get breaking field name (m_strElementName) and then stack value.
		QVariant BreakFieldRecordSetValue = m_pReportPage->m_hshDetailRecordSetHash.value(DetailItem->m_strItemID).getDataRef(CurrentRow, BreakFieldID);

		//Get pointer to detail header item.
		PagePrintingBlock *header = DetGroup->m_pDetailGroupHeader;

		//See can you print another header area (will it fit on page).
		if ((header->m_nHeight + m_nPrintOffsetY/m_nLogDPIY*25.4 + m_pReportPage->m_dblAfterDetailHeight) 
			> (m_pReportPage->m_nHeight - m_pReportPage->m_nBottomMargin))
			return false;

		//If first pass on this page and header is a running header, print header.
		if (DetailItem->m_bPageFirstPass)
		{
			if (DetGroup->m_nBreakBehavior == 2)
				DetGroup->m_bPrintHeader = true;
		}

		//If first pass at all print header.
		if (DetailItem->m_bFirstPass)
			DetGroup->m_bPrintHeader = true;

		//If value not the same then print header.
		if (DetGroup->m_bPrintHeader)
		{
			//Print header.
			PrintHeaderFooter(header);	
			//Update value with new one.
			DetGroup->m_varBreakFieldValue = BreakFieldRecordSetValue;
			DetGroup->m_bPrintHeader = false;
		}

		iter.next();
	}

	return true;
}

bool RptPrinter::PrintDetailGroupsFooter(Detail *DetailItem, int CurrentRow)
{
	if (DetailItem->m_bFirstPass)
		return true;

	QVector<QString> DetailIDVector = DetailItem->m_lstDetailGroupVector;
	//Go from back 
	QVectorIterator<QString> iter(DetailIDVector);
	iter.toBack();
	while (iter.hasPrevious())
	{
		//Get detail id.
		QString DetailID = iter.peekPrevious();

		//Get detail group.
		DetailGroup *DetGroup = DetailItem->m_hshDetailGroupHash.value(DetailID);
		//Get break field and it's last recorded value.
		QString BreakFieldID = DetGroup->m_strBreakFieldID;
		QVariant BreakFieldValue = DetGroup->m_varBreakFieldValue;
		//Get printing element from report printing element hash (because we must get element name).
		//PrintingElement *print_element = m_pReportPage->m_hshPrintingElementHash.value(BreakFieldID);
		//Get breaking field name (m_strElementName) and then stack value.
		QVariant BreakFieldRecordSetValue = m_pReportPage->m_hshDetailRecordSetHash.value(DetailItem->m_strItemID).getDataRef(CurrentRow, BreakFieldID);

		//These two are just for value checking.
		int a = BreakFieldRecordSetValue.toInt();
		int b = BreakFieldValue.toInt();

		//Check has this footer already been printed.
		if (DetGroup->m_nLastPrintedFooterRecord == CurrentRow)
			return true;

		//Get pointer to detail footer item.
		PagePrintingBlock *footer = DetGroup->m_pDetailGroupFooter;

		//See can you print another footer area (will it fit on page).
		if ((footer->m_nHeight + m_nPrintOffsetY/m_nLogDPIY*25.4 + m_pReportPage->m_dblAfterDetailHeight) 
			> (m_pReportPage->m_nHeight - m_pReportPage->m_nBottomMargin))
			return false;

		//If value not the same then print footer and of course then header.
		if (BreakFieldValue != BreakFieldRecordSetValue)
		{
			PrintHeaderFooter(footer);
			//And update group fields to update.
			UpdateFields(DetGroup);
			DetGroup->m_bPrintHeader = true;
			DetGroup->m_nLastPrintedFooterRecord = CurrentRow;

			//Initialize sum fields for detail group area.
			InitializeFields(DetGroup);
		}

		iter.previous();
	}

	return true;
}

bool RptPrinter::PrintDetailGroupsFooterLastPass(Detail *DetailItem)
{
	QVector<QString> DetailIDVector = DetailItem->m_lstDetailGroupVector;
	QVectorIterator<QString> iter(DetailIDVector);
	iter.toBack();
	while (iter.hasPrevious())
	{
		//Get detail id.
		QString DetailID = iter.peekPrevious();
		//Get detail group.
		DetailGroup *DetGroup = DetailItem->m_hshDetailGroupHash.value(DetailID);
		//Get pointer to detail footer item.
		PagePrintingBlock *footer = DetGroup->m_pDetailGroupFooter;

		//See can you print another footer area (will it fit on page).
		if ((footer->m_nHeight + m_nPrintOffsetY/m_nLogDPIY*25.4 + m_pReportPage->m_dblAfterDetailHeight) 
			> (m_pReportPage->m_nHeight - m_pReportPage->m_nBottomMargin))
			return false;

		//Print and update group fields to update.
		PrintHeaderFooter(footer);
		UpdateFields(DetGroup);

		//Initialize sum fields for detail group area.
		InitializeFields(DetGroup);

		iter.previous();
	}
	
	return true;
}

void RptPrinter::InitializeFields(ReportItem *Item)
{
	PagePrintingBlock *printing_block = static_cast<PagePrintingBlock*>(Item);

	if (!printing_block->m_lstFieldsToInitialize.count())
		return;

	QList<QString> InitFieldList = printing_block->m_lstFieldsToInitialize;
	QListIterator<QString> iter(InitFieldList);
	while (iter.hasNext())
	{
		//Get field ID.
		QString FieldID = iter.peekNext();
		//Initialize value.
		m_pReportPage->m_hshVariableHash.insert(FieldID, QVariant());

		iter.next();
	}
}

void RptPrinter::UpdateFields(ReportItem *Item)
{
	PagePrintingBlock *printing_block = static_cast<PagePrintingBlock*>(Item);

	if (!printing_block->m_lstFieldsToUpdate.count())
		return;

	QList<QString> UpdateFieldList = printing_block->m_lstFieldsToUpdate;
	QListIterator<QString> iter(UpdateFieldList);
	while (iter.hasNext())
	{
		//Get field ID.
		QString FieldID = iter.peekNext();
		//Get field value.
		double FieldValue = m_pReportPage->m_hshVariableHash.value(FieldID).toDouble();
		//Get update field ID.
		QString ValueSourceFieldID = static_cast<FieldElement*>(m_pReportPage->m_hshPrintingElementHash.value(FieldID))->m_strValueSource;
		//Get value from update field.
		double ValueSourceFieldValue = m_pReportPage->m_hshVariableHash.value(ValueSourceFieldID).toDouble();

		if (m_pReportPage->m_hshPrintingElementHash.value(FieldID)->m_strFieldType == "sum_field")
		{
			FieldValue += ValueSourceFieldValue;
			m_pReportPage->m_hshVariableHash.insert(FieldID, QVariant(FieldValue));
		}
		else if (m_pReportPage->m_hshPrintingElementHash.value(FieldID)->m_strFieldType == "count")
		{
			FieldValue += m_pReportPage->m_hshPrintingElementHash.value(FieldID)->m_dblCountIncrement;
			m_pReportPage->m_hshVariableHash.insert(FieldID, QVariant(FieldValue));
		}

		iter.next();
	}
}

void RptPrinter::CalculatePageBeforeDetailHeight()
{
	QVectorIterator<QString> iter(m_pReportPage->m_lstReportPrintingItemsOrderVector);
	while (iter.hasNext())
	{
		//Get item id.
		QString ItemID = iter.peekNext();

		//Get report item.
		ReportItem *Item = static_cast<ReportItem*>(m_pReportPage->m_hshReportPrintingItems.value(ItemID));
		//If print area add it to member.
		if		(Item->m_nItemType == ReportItem::PRINT_AREA)
			m_pReportPage->m_dblBeforeDetailHeight += static_cast<PagePrintArea*>(Item)->m_nHeight;
		//If we have arrived to Detail return.
		else if (Item->m_nItemType == ReportItem::DETAIL)
			return;
		else if (Item->m_nItemType == ReportItem::PRINT_INSTANCE)
			return;
		else if (Item->m_nItemType == ReportItem::WORK_AREA)
			return;

		iter.next();
	}
}

void RptPrinter::CalculatePageAfterDetailHeight()
{
	bool StartHeightSum = false;

	QVectorIterator<QString> iter(m_pReportPage->m_lstReportPrintingItemsOrderVector);
	while (iter.hasNext())
	{
		//Get item id.
		QString ItemID = iter.peekNext();

		//Get report item.
		ReportItem *Item = static_cast<ReportItem*>(m_pReportPage->m_hshReportPrintingItems.value(ItemID));
		//If we have not arrived to Detail go to next.
		if		(!(Item->m_nItemType == ReportItem::DETAIL) && !StartHeightSum)
		{
			StartHeightSum = true;
			iter.next();
		}
		else if	(!(Item->m_nItemType == ReportItem::PRINT_INSTANCE) && !StartHeightSum)
		{
			StartHeightSum = true;
			iter.next();
		}
		//If print area add it to member.
		else if	((Item->m_nItemType == ReportItem::PRINT_AREA) && StartHeightSum)
			m_pReportPage->m_dblAfterDetailHeight += static_cast<PagePrintArea*>(Item)->m_nHeight;

		iter.next();
	}
}

QSize RptPrinter::GetReportPreviewPageSize()
{
	return QSize(m_pReportPage->m_nWidth*96/25.4, m_pReportPage->m_nHeight*96/25.4);
}

void RptPrinter::SetDefaultValues()
{
	//Set default font.
	m_Painter.setFont(m_pReportPage->m_fntDefaultFont);
}

void RptPrinter::ParseHtmlText(FieldElement* Field)
{
	//Get database text.
	QString fieldText = QVariant(m_pReportPage->m_hshVariableHash.value(Field->m_strElementID)).toString();

	QDomDocument domDocument;
	domDocument.setContent(fieldText);
	fieldText.clear();

	QDomNodeList ReportElement	= domDocument.elementsByTagName("body");
	QDomNode Node = ReportElement.at(0).firstChild();
	while(!Node.isNull()) 
	{
		QDomElement Element = Node.toElement();
		
		fieldText += Element.text();
		fieldText += "\n";

		Node = Node.nextSibling();
	}

	//Remove empty lines.
	QStringList stringList = fieldText.split("\n", QString::SkipEmptyParts);
	fieldText = stringList.join("\n");

	//Return formated text to stack.
	m_pReportPage->m_hshVariableHash.insert(Field->m_strElementID, fieldText);
}
