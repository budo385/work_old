#ifndef REPORTITEM_H
#define REPORTITEM_H

#include <QString>
#include <QVariant>
#include <QHash>
#include <QtWidgets/QWidget>

#include "reportprintingelement.h"

class ReportItem 
{
public:
    ReportItem();
    ~ReportItem();

	enum ItemType
	{
		FUNCTION_CALL,
		PRINT_AREA,
		DETAIL,
		WIDGET_DETAIL,
		DETAIL_GROUP,
		DETAIL_HEADER,
		DETAIL_FOOTER, 
		PRINT_INSTANCE,
		WORK_AREA,
		PAGE_BREAK			
		/*
		FUNCTION_CALL		= 0,
		PRINT_AREA			= 1,
		DETAIL				= 2,
		DETAIL_GROUP		= 3,
		DETAIL_HEADER		= 4,
		DETAIL_FOOTER		= 5, 
		PRINT_INSTANCE		= 6,
		WORK_AREA			= 7,
		PAGE_BREAK			= 8*/
	};

	QString m_strItemID;									//< Unique Item ID (identifier) - item hash key.
	int		m_nItemType;									//< Type of report printing item (Parser evaluator - from ItemType enum).
	bool	m_bHideItem;									//< Hide report item (don't print - for printing areas, ...).
	int		m_nDetailRowPrinted;							//< Detail last row printed. Used for checking is this item already printed in it's detail.
};

class PageBreak : public ReportItem
{
public:
	PageBreak(){
				m_nItemType = ReportItem::PAGE_BREAK;
				m_bBreakOnLastDetail = false;
				};

	bool	m_bBreakOnLastDetail;							//< Should it break on last detail record (if in detail).
};

class FunctionCall : public ReportItem
{
public:
	FunctionCall(){m_nItemType = ReportItem::FUNCTION_CALL;};
	~FunctionCall(){};
};

class PagePrintArea : public ReportItem
{
public:
	PagePrintArea(){};
	~PagePrintArea();

	double								m_nHeight;				//< Printing area height.
	bool								m_bShrink;				//< Shrink area.
	QVector<PrintingElement*>			m_lstPrintingElements;	//< Printing elements (labels, fields, ...) vector.
	QHash<QString, PrintingElement*>	m_hshPrintingElements;	//< Printing elements name to pointer hash.
};

class WorkArea : public PagePrintArea
{
public:
	WorkArea(){};
	~WorkArea(){};
};

class PagePrintingBlock : public ReportItem
{
public:
	PagePrintingBlock(){};
	~PagePrintingBlock();

	double				 m_nHeight;						//< Height of page printing block (used for header/footer in detail).
	QVector<ReportItem*> m_lstReportItems;				//< List of report items (print areas or function calls).
	QList<QString>		 m_lstFieldsToInitialize;		//< List of fields to initialize on each invocation.
	QList<QString>		 m_lstFieldsToUpdate;			//< List of fields to update on each invocation.
};

class DetailGroup : public PagePrintingBlock
{
public:
	DetailGroup()
	{
		m_varBreakFieldValue = QVariant();
		m_bPrintHeader		 = false;
		m_bRunningHeaderBehavior = false;
		m_bLocalHeaderBehavior = false;
		m_bOneDetailFooterBehavior = false;
	};
	~DetailGroup()
	{
		m_pDetailGroupHeader = NULL;
		m_pDetailGroupFooter = NULL;
	};

	enum BreakBehavior
	{
		EACH_USE				= 0,
		FIRST_USE_ONLY			= 1,
		FIRST_USE_AND_PAGE_TOP	= 2
	};

	enum HeaderBehavior
	{
		DEF_HEADER_BEHAVIOR		= 0,
		RUNNING_HEADER			= 1,
		LOCAL_HEADER_ONLY		= 2
	};

	enum FooterBehavior
	{
		DEF_FOOTER_BEHAVIOR		= 0,
		ONE_DETAIL_FOOTER		= 1
	};

	QString						m_strBreakFieldID;			//< Break field ID.
	
	int							m_nBreakBehavior;			//< Break behavior (from BreakBehavior enum).
	bool						m_bRunningHeaderBehavior;	//< Break header behavior - running header.
	bool						m_bLocalHeaderBehavior;		//< Break header behavior - local header.
	bool						m_bOneDetailFooterBehavior;	//< Break footer behavior - one detail footer.
	
	int							m_nLastPrintedFooterRecord;	//< Number of last printed footer record (for not printing footers two times when break is in last line of page).
	QVariant					m_varBreakFieldValue;		//< Break field value (used for checking state).
	bool						m_bPrintHeader;				//< Print header bool (used to signal header printing from footer).
	PagePrintingBlock			*m_pDetailGroupHeader;		//< Detail group header block.
	PagePrintingBlock			*m_pDetailGroupFooter;		//< Detail group footer block.
};

class Detail : public PagePrintingBlock
{
public:
	Detail();
	~Detail();

	int								m_nRecordRowNumber;			//< Current record row number.
	int								m_nLastPrintedDetailRecord;	//< Number of last printed detail record (for not printing details two times when break is in last line of page).
	bool							m_bFirstPass;				//< First pass through detail.
	bool							m_bLastPass;				//< Last pass through detail.
	bool							m_bPageFirstPass;			//< This page first pass through detail.
	bool							m_bPageLastPass;			//< This page last pass through detail.
	bool							m_bDetailContainsDetail;	//< This detail contains other details.
	QVector<QString>				m_lstDetailGroupVector;		//< Detail group ID's list (m_strItemID).
	QHash<QString, DetailGroup*>	m_hshDetailGroupHash;		//< Detail group ID to pointer hash (for accessing while printing).
	PagePrintingBlock				*m_pDetailHeader;			//< Detail header block.
	PagePrintingBlock				*m_pDetailFooter;			//< Detail footer block.

	void	InitalizeMembers();									//< Initialize members method. Used when printing detail from detail.
																//<	It must be reinitialize each time it is printed or the print areas won't print for records already printed.
																//< See ReportItem->m_nDetailRowPrinted in RptPrinter->DetailPrint();
};

class WidgetDetail : public PagePrintingBlock
{
public:
	WidgetDetail(){m_dAlreadyPrintedWidgetHeight = 0;};
	
	void Reset(){m_dAlreadyPrintedWidgetHeight = 0;};

	QPixmap							m_PrintingWidgetPixmap;
	double							m_dX;						//< Detail widget x starting position (in millimeters).
	double							m_dY;						//< Detail widget y starting position (in millimeters).
	double							m_dDetailWidth;				//< Detail widget width (in millimeters).
	double							m_dDetailHeight;			//< Detail widget height (if -1 then take space as much as needed).
	double							m_dAlreadyPrintedWidgetHeight;//< Already printed detail height (used from report printer class).
	int								m_nPrintStep;				//< Printing step. If not set (0) prints next page where it stopped on previous.
};

class PrintInstance : public PagePrintingBlock
{
public:
	PrintInstance(){};
	~PrintInstance(){};
};

#endif // REPORTITEM_H
