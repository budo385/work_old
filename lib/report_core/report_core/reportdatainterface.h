#ifndef REPORTDATAINTERFACE_H
#define REPORTDATAINTERFACE_H

#include "reportpage.h"

class ReportDataInterface
{
public:
    ReportDataInterface();
    ~ReportDataInterface();

	void GetDetailRecordSet(ReportPage *Report);

private:
    
};

#endif // REPORTDATAINTERFACE_H
