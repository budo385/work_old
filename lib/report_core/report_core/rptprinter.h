#ifndef RPTPRINTER_H
#define RPTPRINTER_H

#include <math.h>

#include <QPainter>
#include <QObject>
#include <QtWidgets/QMessageBox>

//Report printing elements.
#include "reportlineandpoint.h"
#include "reportrectangle.h"
#include "reportpicture.h"
#include "reportlabel.h"

//Report items and parser.
#include "reportpage.h"
#include "reportparser.h"


//Report interface.
#include "reportdatainterface.h"

class RptPrinter : public QObject
{
public:
    RptPrinter();
    ~RptPrinter();

	bool InitalizeReport(QString strReportFileFullPath, int nReportID, QString strReportName,ReportPage *pageData,QList<DbRecordSet> &lstPageRecordSet);

	void PrintReportToPrinter(QPrinter *printer);
	bool printReportPageToPrinter(int nlogDpiX, int nlogDpiY, int nphysDpiX, int nphysDpiY);

	bool printReportPage(QPaintDevice *device);

	void CallFunction(ReportItem *Item);
	void PrintPrintArea(ReportItem *Item);
	bool PrintDetail(ReportItem *Item);
	bool PrintWidgetDetail(ReportItem *Item);
	bool PrintReportInstance(ReportItem *Item);
	void PrintHeaderFooter(ReportItem *Item);
    
	bool PrintDetailGroupsHeader(Detail *DetailItem, int CurrentRow);
	bool PrintDetailGroupsFooter(Detail *DetailItem, int CurrentRow);
	bool PrintDetailGroupsFooterLastPass(Detail *DetailItem);

	void CalculatePageBeforeDetailHeight();
	void CalculatePageAfterDetailHeight();

	void InitializeFields(ReportItem *Item);					//< Initialize fields (sum, ...).
	void UpdateFields(ReportItem *Item);						//< Update fields (sum, ...).

	QSize GetReportPreviewPageSize();
	void  ParseHtmlText(FieldElement* Field);

	void SetDefaultValues();

	ReportDataInterface			*m_pReportDataInterface;		//< Report interface for geting data (from external world).
	int							m_nReportID;					//< ReportID.
	QString						m_strReportPath;				//< Report file path.
	ReportPage					*m_pReportPage;					//< Report page.
	QPainter					m_Painter;						//< Local painter.
	QPaintDevice				*m_pPaintDevice;				//< Pointer to paint device.
	//QPrinter					*m_pPrinter;					//< Pointer to printer.

	int							m_nLogDPIX;						//< Logical DPI of x axis.
	int							m_nLogDPIY;						//< Logical DPI of y axis.
	double						m_nPrintOffsetY;				//< Painting offset (current painting position).
	int							m_nRptPageNumber;				//< Current report page.
	bool						m_bNewPage;						//< New Page bool, if false prints page footer.
	QString						m_strReportName;					

};

#endif // RPTPRINTER_H
