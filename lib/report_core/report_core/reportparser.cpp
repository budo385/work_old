#include "reportparser.h"

ReportParser::ReportParser(QString ReportFile, ReportPage* pReportPage)
{
	m_strReportFile		= ReportFile;
	m_pRepPage	= pReportPage;
}

ReportParser::~ReportParser()
{
}

bool ReportParser::ParseFile()
{
	QDomDocument domDocument;
	QFile file(m_strReportFile);
	if (!file.open(QIODevice::ReadOnly))
		return false;
	if (!domDocument.setContent(&file)) {
		file.close();
		return false;
	}
	file.close();

	QDomNodeList ReportElement	= domDocument.elementsByTagName("report");
	
	//Get report ID (from report definition).
	int reportID =QVariant(ReportElement.at(0).toElement().attribute("reportID")).toInt();
	
	//Check does it exists in report hash or is it a simple report.
	//if (m_pReportPageHash->contains(reportID))
	//	m_pRepPage = m_pReportPageHash->value(reportID);
	//else
	//	m_pRepPage = m_pReportPageHash->value(0);

	//Now initialize report members.
	m_pRepPage->m_strReportID		= reportID;
	m_pRepPage->m_bCallInterface	= QVariant(ReportElement.at(0).toElement().attribute("callInterface")).toBool();
	m_pRepPage->m_nPageSize			= QVariant(ReportElement.at(0).toElement().attribute("pageSize")).toInt();
	m_pRepPage->m_nOrientation		= QVariant(ReportElement.at(0).toElement().attribute("pageOrientation")).toInt();
	m_pRepPage->m_nWidth			= QVariant(ReportElement.at(0).toElement().attribute("pageWidth")).toInt();
	m_pRepPage->m_nHeight			= QVariant(ReportElement.at(0).toElement().attribute("pageHeight")).toInt();
	m_pRepPage->m_nLeftMargin		= QVariant(ReportElement.at(0).toElement().attribute("leftMargin")).toInt();
	m_pRepPage->m_nRightMargin		= QVariant(ReportElement.at(0).toElement().attribute("rightMargin")).toInt();
	m_pRepPage->m_nTopMargin		= QVariant(ReportElement.at(0).toElement().attribute("topMargin")).toInt();
	m_pRepPage->m_nBottomMargin		= QVariant(ReportElement.at(0).toElement().attribute("bottomMargin")).toInt();

	QFont defaultFont;
	defaultFont.setFamily(ReportElement.at(0).toElement().attribute("defaultFont", "Helvetica"));
	defaultFont.setPointSize(QVariant(ReportElement.at(0).toElement().attribute("defaultFontSize", "8")).toInt());
	m_pRepPage->m_fntDefaultFont	= defaultFont;

	//Default line spacing.
	m_pRepPage->m_fntDefaultFontLineSpacing= QVariant(ReportElement.at(0).toElement().attribute("lineSpacing")).toInt();

	//Go through nodes and build report page.
	QDomNode Node = ReportElement.at(0).firstChild();
	while(!Node.isNull()) 
	{
		QDomElement Element = Node.toElement();
		
		QString name = Element.tagName();

		//See what are we up to.
		if (Element.tagName() == "function_call")
		{
			FunctionCall *function_call = new FunctionCall();
			function_call->m_strItemID = Element.attribute("reportItemID");
			function_call->m_nItemType = ReportItem::FUNCTION_CALL;
			//Add to report item order vector and hash.
			m_pRepPage->m_lstReportPrintingItemsOrderVector << function_call->m_strItemID;
			m_pRepPage->m_hshReportPrintingItems.insert(function_call->m_strItemID, function_call);
		}
		else if (Element.tagName() == "print_area")
		{
			PagePrintArea *print_area = new PagePrintArea();
			print_area->m_strItemID	  = Element.attribute("reportItemID");
			print_area->m_nItemType   = ReportItem::PRINT_AREA;
			print_area->m_nHeight	  = QVariant(Element.attribute("height")).toDouble();
			print_area->m_bShrink	  = QVariant(Element.attribute("shrink", "0")).toBool();
			print_area->m_bHideItem	  = QVariant(Element.attribute("hide", "0")).toBool();
			//Add to report item order vector and hash.
			m_pRepPage->m_lstReportPrintingItemsOrderVector << print_area->m_strItemID;
			m_pRepPage->m_hshReportPrintingItems.insert(print_area->m_strItemID, print_area);

			GetPrintAreaItems(Element, print_area);
		}
		else if (Element.tagName() == "detail")
		{
			Detail *detail = new Detail();
			detail->m_strItemID				= Element.attribute("reportItemID");
			//Get detail fetch data parameters if they exist.
			if (!Element.attribute("TableID").isEmpty())
				m_pRepPage->m_hshDetailToTableIDHash.insert(detail->m_strItemID, Element.attribute("TableID").toInt());
			if (!Element.attribute("TableView").isEmpty())
				m_pRepPage->m_hshDetailToTableViewHash.insert(detail->m_strItemID, Element.attribute("TableView").toInt());
			if (!Element.attribute("StrWhere").isEmpty())
				m_pRepPage->m_hshDetailToWhereConditionHash.insert(detail->m_strItemID, Element.attribute("StrWhere"));
			detail->m_nItemType				= ReportItem::DETAIL;
			detail->m_nHeight				= QVariant(Element.attribute("height")).toDouble();
			detail->m_bHideItem				= QVariant(Element.attribute("hide", "0")).toBool();
			detail->m_lstFieldsToInitialize = Element.attribute("initializeFields").split(";", QString::SkipEmptyParts);
			detail->m_lstFieldsToUpdate		= Element.attribute("updateFields").split(";", QString::SkipEmptyParts);
			//Add to report item order vector and hash.
			m_pRepPage->m_lstReportPrintingItemsOrderVector << detail->m_strItemID;
			m_pRepPage->m_hshReportPrintingItems.insert(detail->m_strItemID, detail);
			m_pRepPage->m_hshDetailRecordSetHash.insert(detail->m_strItemID, DbRecordSet());

			ParseDetail(Element, detail);
		}
/*		else if (Element.tagName() == "widget_detail")
		{
			WidgetDetail *widget_detail = new WidgetDetail();
			widget_detail->m_strItemID	= Element.attribute("reportItemID");
			widget_detail->m_nItemType	= ReportItem::WIDGET_DETAIL;
			widget_detail->m_dX			= QVariant(Element.attribute("x", "0")).toDouble();
			widget_detail->m_dY			= QVariant(Element.attribute("y", "0")).toDouble();
			widget_detail->m_bHideItem	= QVariant(Element.attribute("hide", "0")).toBool();
			widget_detail->m_dDetailWidth= QVariant(Element.attribute("widget_width")).toDouble();
			widget_detail->m_dDetailHeight= QVariant(Element.attribute("widget_height", "0")).toDouble();
			//Add to report item order vector and hash.
			m_pRepPage->m_lstReportPrintingItemsOrderVector << widget_detail->m_strItemID;
			m_pRepPage->m_hshReportPrintingItems.insert(widget_detail->m_strItemID, widget_detail);
		}
		*/
		else if (Element.tagName() == "instance")
		{
			PrintInstance *instance = new PrintInstance();
			instance->m_strItemID				= Element.attribute("reportItemID");
			instance->m_nItemType				= ReportItem::PRINT_INSTANCE;
			instance->m_nHeight					= QVariant(Element.attribute("height", "-1")).toDouble();
			instance->m_bHideItem				= QVariant(Element.attribute("hide", "0")).toBool();
			instance->m_lstFieldsToInitialize	= Element.attribute("initializeFields").split(";", QString::SkipEmptyParts);
			instance->m_lstFieldsToUpdate		= Element.attribute("updateFields").split(";", QString::SkipEmptyParts);
			//Add to report item order vector and hash.
			m_pRepPage->m_lstReportPrintingItemsOrderVector << instance->m_strItemID;
			m_pRepPage->m_hshReportPrintingItems.insert(instance->m_strItemID, instance);

			ParseInstance(Element, instance);
		}
		else if (Element.tagName() == "work_area")
		{
			WorkArea *work_area = new WorkArea();
			work_area->m_strItemID = Element.attribute("reportItemID");
			work_area->m_nItemType = ReportItem::WORK_AREA;
			work_area->m_nHeight = QVariant(Element.attribute("height")).toDouble();
			//Add to report item order vector and hash.
			m_pRepPage->m_lstReportPrintingItemsOrderVector << work_area->m_strItemID;
			m_pRepPage->m_hshReportPrintingItems.insert(work_area->m_strItemID, work_area);

			GetPrintAreaItems(Element, work_area);
		}
		else if (Element.tagName() == "page_break")
		{
			PageBreak *page_break = new PageBreak();
			page_break->m_strItemID = Element.attribute("reportItemID");
			page_break->m_nItemType = ReportItem::PAGE_BREAK;
			page_break->m_bBreakOnLastDetail = QVariant(Element.attribute("breakOnLastDetail", "false")).toBool();
			//Add to report item order vector and hash.
			m_pRepPage->m_lstReportPrintingItemsOrderVector << page_break->m_strItemID;
			m_pRepPage->m_hshReportPrintingItems.insert(page_break->m_strItemID, page_break);
		}

		Node = Node.nextSibling();
	}
	
	return true;
}

void ReportParser::GetPrintAreaItems(QDomElement ParentElement, ReportItem *Parent)
{
	//Get all printing elements and put them in a list.
	QList<QDomElement> ElementList;
	QDomNode node = ParentElement.firstChild();

	while(!node.isNull()) 
	{
		// try to convert the node to an element.
		QDomElement element = node.toElement();
		if(!element.isNull())
			ElementList << element;

		//Go to next.
		node = node.nextSibling();
	}

	//Then iterate and see what are.
	QListIterator<QDomElement> iter(ElementList);
	while (iter.hasNext())
	{
		//Peek next element.
		QDomElement elem = iter.peekNext();

		if (elem.tagName() == "point")
		{
			PointElement *NewPoint = new PointElement();
			NewPoint->m_strElementID = elem.attribute("fieldID");
			NewPoint->m_nElementType = PrintingElement::POINT;
			NewPoint->m_bHideElement = QVariant(elem.attribute("hide", "0")).toBool();
			NewPoint->m_dX			 = QVariant(elem.attribute("x")).toDouble();
			NewPoint->m_dY			 = QVariant(elem.attribute("y")).toDouble();
			NewPoint->m_Pen.setWidth(QVariant(elem.attribute("pointWidth", /*default value*/ "1")).toInt());
			NewPoint->m_Pen.setColor(Convert2Color(elem.attribute("pointColor", /*default value*/ "#000000")));

			//Add to parent printing list and hash.
			static_cast<PagePrintArea*>(Parent)->m_lstPrintingElements << NewPoint;
			static_cast<PagePrintArea*>(Parent)->m_hshPrintingElements.insert(NewPoint->m_strElementID, NewPoint);

			//Add to page printing item hash.
			m_pRepPage->m_hshPrintingElementHash.insert(NewPoint->m_strElementID, NewPoint);
			
			//Add to page printing item hash.
			m_pRepPage->m_hshPrintingElementHash.insert(NewPoint->m_strElementID, NewPoint);
		}
		else if (elem.tagName() == "line")
		{
			LineElement *NewLine = new LineElement();
			NewLine->m_strElementID = elem.attribute("fieldID");
			NewLine->m_nElementType = PrintingElement::LINE;
			NewLine->m_bHideElement = QVariant(elem.attribute("hide", "0")).toBool();
			NewLine->m_dStartX		= QVariant(elem.attribute("x1")).toDouble();
			NewLine->m_dStartY		= QVariant(elem.attribute("y1", "-1")).toDouble();
			NewLine->m_dEndX		= QVariant(elem.attribute("x2")).toDouble();
			NewLine->m_dEndY		= QVariant(elem.attribute("y2", "-1")).toDouble();
			NewLine->m_dLine		= QVariant(elem.attribute("line")).toDouble();
			NewLine->m_Pen.setWidth(QVariant(elem.attribute("lineWidth", /*default value*/ "1")).toInt());
			NewLine->m_Pen.setColor(Convert2Color(elem.attribute("lineColor", /*default value*/ "#000000")));

			//Add to parent printing list and hash.
			static_cast<PagePrintArea*>(Parent)->m_lstPrintingElements << NewLine;
			static_cast<PagePrintArea*>(Parent)->m_hshPrintingElements.insert(NewLine->m_strElementID, NewLine);

			//Add to page printing item hash.
			m_pRepPage->m_hshPrintingElementHash.insert(NewLine->m_strElementID, NewLine);
		}
		else if (elem.tagName() == "rectangle")
		{
			RectangleElement *NewRectangle = new RectangleElement();
			NewRectangle->m_strElementID = elem.attribute("fieldID");
			NewRectangle->m_nElementType = PrintingElement::RECTANGLE;
			NewRectangle->m_bHideElement = QVariant(elem.attribute("hide", "0")).toBool();
			NewRectangle->m_dX		= QVariant(elem.attribute("x")).toDouble();
			NewRectangle->m_dY		= QVariant(elem.attribute("y")).toDouble();
			NewRectangle->m_dWidth  = QVariant(elem.attribute("width")).toDouble();
			NewRectangle->m_dHeight = QVariant(elem.attribute("height")).toDouble();
			NewRectangle->m_nXRnd	= QVariant(elem.attribute("xRnd")).toDouble();
			NewRectangle->m_nYRnd	= QVariant(elem.attribute("yRnd")).toDouble();
			NewRectangle->m_Pen.setWidth(QVariant(elem.attribute("lineWidth", /*default value*/ "1")).toInt());
			NewRectangle->m_Pen.setColor(Convert2Color(elem.attribute("lineColor", /*default value*/ "#000000")));
			NewRectangle->m_nRotation = QVariant(elem.attribute("rotation", "0")).toInt();

			QBrush newBrush(Convert2Color(elem.attribute("backColor", /*default value*/ "#FFFFFF")));
			NewRectangle->m_BackgroundBrush = newBrush;

			//Add to parent printing list and hash.
			static_cast<PagePrintArea*>(Parent)->m_lstPrintingElements << NewRectangle;
			static_cast<PagePrintArea*>(Parent)->m_hshPrintingElements.insert(NewRectangle->m_strElementID, NewRectangle);

			//Add to page printing item hash.
			m_pRepPage->m_hshPrintingElementHash.insert(NewRectangle->m_strElementID, NewRectangle);
		}
		else if (elem.tagName() == "pixmap")
		{
			PixmapElement *NewPixmap = new PixmapElement();
			NewPixmap->m_strElementID= elem.attribute("fieldID");
			NewPixmap->m_nElementType= PrintingElement::PIXMAP;
			NewPixmap->m_bHideElement= QVariant(elem.attribute("hide", "0")).toBool();
			NewPixmap->m_dX			 = QVariant(elem.attribute("x")).toDouble();
			NewPixmap->m_dY			 = QVariant(elem.attribute("y")).toDouble();
			NewPixmap->m_dWidth		 = QVariant(elem.attribute("width")).toDouble();
			NewPixmap->m_dHeight	 = QVariant(elem.attribute("height")).toDouble();
			NewPixmap->m_strDataField= elem.attribute("DataField");
			NewPixmap->m_Pixmap		 = QPixmap(elem.attribute("pixmap"));

			//Add to parent printing list and hash.
			static_cast<PagePrintArea*>(Parent)->m_lstPrintingElements << NewPixmap;
			static_cast<PagePrintArea*>(Parent)->m_hshPrintingElements.insert(NewPixmap->m_strElementID, NewPixmap);

			//Add to page printing item hash.
			m_pRepPage->m_hshPrintingElementHash.insert(NewPixmap->m_strElementID, NewPixmap);
		}
		else if (elem.tagName() == "label")
		{
			LabelElement *NewLabel		= new LabelElement();
			NewLabel->m_strElementID	= elem.attribute("fieldID");
			NewLabel->m_nElementType	= PrintingElement::LABEL;
			NewLabel->m_bHideElement	= QVariant(elem.attribute("hide", "0")).toBool();
			NewLabel->m_dX				= QVariant(elem.attribute("x")).toDouble();
			NewLabel->m_dY				= QVariant(elem.attribute("y", "-1")).toDouble();
			NewLabel->m_dLine			= QVariant(elem.attribute("line")).toDouble();
			NewLabel->m_nLabelLength	= QVariant(elem.attribute("length", "-1")).toDouble();
			NewLabel->m_nRotationAngle	= QVariant(elem.attribute("rotation", "0")).toInt();
			NewLabel->m_strLabelText	= elem.text();
			NewLabel->m_Font			= GetLabelFont(elem);
			NewLabel->m_nHAlignment		= GetAlignament(elem);
			QBrush newBrush(Convert2Color(elem.attribute("backColor", /*default value*/ "#FFFFFF")));
			NewLabel->m_BackgroundBrush = newBrush;
			
			//Add to report stack.
			m_pRepPage->m_hshVariableHash.insert(NewLabel->m_strElementID, NewLabel->m_strLabelText);

			//Add to parent printing list and hash.
			static_cast<PagePrintArea*>(Parent)->m_lstPrintingElements << NewLabel;
			static_cast<PagePrintArea*>(Parent)->m_hshPrintingElements.insert(NewLabel->m_strElementID, NewLabel);

			//Add to page printing item hash.
			m_pRepPage->m_hshPrintingElementHash.insert(NewLabel->m_strElementID, NewLabel);
		}
		else if (elem.tagName() == "field")
		{
			FieldElement *NewField		= new FieldElement();
			NewField->m_strElementID	= elem.attribute("fieldID");
			NewField->m_nElementType	= PrintingElement::FIELD;
			NewField->m_bHideElement	= QVariant(elem.attribute("hide", "0")).toBool();
			NewField->m_strFieldType	= elem.attribute("fieldType");
			NewField->m_strDataType		= elem.attribute("type");
			NewField->m_dblCountIncrement= QVariant(elem.attribute("countIncrement")).toDouble();
			NewField->m_dX				= QVariant(elem.attribute("x")).toDouble();
			NewField->m_dY				= QVariant(elem.attribute("y", "-1")).toDouble();
			NewField->m_dLine			= QVariant(elem.attribute("line")).toDouble();
			NewField->m_nLabelLength	= QVariant(elem.attribute("length", "-1")).toInt();
			NewField->m_nRotationAngle	= QVariant(elem.attribute("rotation", "0")).toInt();
			NewField->m_Font			= GetLabelFont(elem);
			NewField->m_nHAlignment		= GetAlignament(elem);

			//Add to report stack.
			m_pRepPage->m_hshVariableHash.insert(NewField->m_strElementID, QVariant());

			//Add to parent printing list and hash.
			static_cast<PagePrintArea*>(Parent)->m_lstPrintingElements << NewField;
			static_cast<PagePrintArea*>(Parent)->m_hshPrintingElements.insert(NewField->m_strElementID, NewField);

			//Add to page printing item hash.
			m_pRepPage->m_hshPrintingElementHash.insert(NewField->m_strElementID, NewField);
		}
		else if (elem.tagName() == "multiLineField")
		{
			FieldElement *NewField		= new FieldElement();
			NewField->m_strElementID	= elem.attribute("fieldID");
			NewField->m_nElementType	= PrintingElement::MULTILINE_FIELD;
			NewField->m_bHideElement	= QVariant(elem.attribute("hide", "0")).toBool();
			NewField->m_strDataType		= elem.attribute("type");
			NewField->m_dX				= QVariant(elem.attribute("x")).toDouble();
			NewField->m_dY				= QVariant(elem.attribute("y", "-1")).toDouble();
			NewField->m_dLine			= QVariant(elem.attribute("line")).toDouble();
			NewField->m_nMaxLineNumbers	= QVariant(elem.attribute("maxLines")).toInt();
			NewField->m_nLabelLength	= QVariant(elem.attribute("length", "-1")).toInt();
			NewField->m_nRotationAngle	= QVariant(elem.attribute("rotation", "0")).toInt();
			NewField->m_nWrap			= QVariant(elem.attribute("wrap", "0")).toInt();
			NewField->m_Font			= GetLabelFont(elem);
			NewField->m_nHAlignment		= GetAlignament(elem);
			NewField->m_strWidthInMM	= elem.attribute("widthInMM", "0");
			
			//Add to report stack.
			m_pRepPage->m_hshVariableHash.insert(NewField->m_strElementID, QVariant());

			//Add to parent printing list and hash.
			static_cast<PagePrintArea*>(Parent)->m_lstPrintingElements << NewField;
			static_cast<PagePrintArea*>(Parent)->m_hshPrintingElements.insert(NewField->m_strElementID, NewField);

			//Add to page printing item hash.
			m_pRepPage->m_hshPrintingElementHash.insert(NewField->m_strElementID, NewField);
		}
		else if (elem.tagName() == "recordSetField")
		{
			FieldElement *NewField		= new FieldElement();
			NewField->m_strElementID	= elem.attribute("fieldID");
			NewField->m_nElementType	= PrintingElement::RECORD_FIELD;
			NewField->m_bHideElement	= QVariant(elem.attribute("hide", "0")).toBool();
			NewField->m_strDataType		= elem.attribute("type");
			NewField->m_dX				= QVariant(elem.attribute("x")).toDouble();
			NewField->m_dY				= QVariant(elem.attribute("y", "-1")).toDouble();
			NewField->m_dLine			= QVariant(elem.attribute("line")).toDouble();
			NewField->m_nLabelLength	= QVariant(elem.attribute("length", "-1")).toInt();
			NewField->m_nRotationAngle	= QVariant(elem.attribute("rotation", "0")).toInt();

			//Add to report stack recordset fields.
			m_pRepPage->m_hshVariableHash.insert(NewField->m_strElementID, 0);

			//Add to parent printing list.
			static_cast<PagePrintArea*>(Parent)->m_lstPrintingElements << NewField;
			static_cast<PagePrintArea*>(Parent)->m_hshPrintingElements.insert(NewField->m_strElementID, NewField);

			//Add to page printing item hash.
			m_pRepPage->m_hshPrintingElementHash.insert(NewField->m_strElementID, NewField);
		}
		else if (elem.tagName() == "sumField")
		{
			SumFieldElement *NewField	= new SumFieldElement();
			NewField->m_strElementID	= elem.attribute("fieldID");
			NewField->m_nElementType	= PrintingElement::SUM_FIELD;
			NewField->m_bHideElement	= QVariant(elem.attribute("hide", "0")).toBool();
			NewField->m_strDataType		= elem.attribute("type");
			NewField->m_strValueSource	= elem.attribute("sumFieldID");
			NewField->m_dX				= QVariant(elem.attribute("x")).toDouble();
			NewField->m_dY				= QVariant(elem.attribute("y", "-1")).toDouble();
			NewField->m_dLine			= QVariant(elem.attribute("line")).toDouble();
			NewField->m_nLabelLength	= QVariant(elem.attribute("length")).toInt();
			NewField->m_nRotationAngle	= QVariant(elem.attribute("rotation", "0")).toInt();

			//Add to report stack.
			m_pRepPage->m_hshVariableHash.insert(NewField->m_strElementID, QVariant());

			//Add to parent printing list.
			static_cast<PagePrintArea*>(Parent)->m_lstPrintingElements << NewField;
			static_cast<PagePrintArea*>(Parent)->m_hshPrintingElements.insert(NewField->m_strElementID, NewField);

			//Add to page printing item hash.
			m_pRepPage->m_hshPrintingElementHash.insert(NewField->m_strElementID, NewField);
		}

		//Go on next.
		iter.next();
	}
}

void ReportParser::ParseHeaderFooter(QDomElement Element, ReportItem *Parent)
{
	//Go through header or footer items (can be only function call or print area).
	QDomNode node = Element.firstChild();

	while(!node.isNull()) 
	{
		// try to convert the node to an element.
		QDomElement element = node.toElement();

		//If function call.
		if (element.tagName() == "function_call")
		{
			FunctionCall *function_call = new FunctionCall();
			function_call->m_strItemID = element.attribute("reportItemID");
			function_call->m_nItemType = ReportItem::FUNCTION_CALL;
			
			static_cast<PagePrintingBlock*>(Parent)->m_lstReportItems << function_call;
		}
		//Else if print area.
		else if (element.tagName() == "print_area")
		{
			PagePrintArea *print_area = new PagePrintArea();
			print_area->m_strItemID	= element.attribute("reportItemID");
			print_area->m_nItemType = ReportItem::PRINT_AREA;
			print_area->m_nHeight	= QVariant(element.attribute("height")).toDouble();
			print_area->m_bShrink	= QVariant(element.attribute("shrink", "0")).toBool();
			print_area->m_bHideItem	= QVariant(element.attribute("hide", "0")).toBool();

			//Add report item (this area) to parent item (detail header or footer).
			static_cast<PagePrintingBlock*>(Parent)->m_lstReportItems << print_area;
			
			GetPrintAreaItems(element, print_area);
		}

		//Go to next.
		node = node.nextSibling();
	}
}

void ReportParser::ParseDetail(QDomElement Element, ReportItem *Parent)
{
	//Go through detail items (can be function call, print area or detail break).
	QDomNode node = Element.firstChild();

	while(!node.isNull()) 
	{
		// try to convert the node to an element.
		QDomElement element = node.toElement();

		//If function call.
		if (element.tagName() == "function_call")
		{
			FunctionCall *function_call = new FunctionCall();
			function_call->m_strItemID = element.attribute("reportItemID");
			function_call->m_nItemType = ReportItem::FUNCTION_CALL;

			//Add to detail report item vector.
			static_cast<Detail*>(Parent)->m_lstReportItems << function_call;
		}
		else if (element.tagName() == "page_break")
		{
			PageBreak *page_break = new PageBreak();
			page_break->m_strItemID = element.attribute("reportItemID");
			page_break->m_nItemType = ReportItem::PAGE_BREAK;
			page_break->m_bBreakOnLastDetail = QVariant(element.attribute("breakOnLastDetail", "false")).toBool();

			//Add to detail report item vector.
			static_cast<Detail*>(Parent)->m_lstReportItems << page_break;
		}
		//Else if another detail in detail.
		else if (element.tagName() == "detail")
		{
			Detail *detail = new Detail();
			detail->m_strItemID				= element.attribute("reportItemID");
			//Get detail fetch data parameters if they exist.
			if (!element.attribute("TableID").isEmpty())
				m_pRepPage->m_hshDetailToTableIDHash.insert(detail->m_strItemID, element.attribute("TableID").toInt());
			if (!element.attribute("TableView").isEmpty())
				m_pRepPage->m_hshDetailToTableViewHash.insert(detail->m_strItemID, element.attribute("TableView").toInt());
			if (!element.attribute("StrWhere").isEmpty())
				m_pRepPage->m_hshDetailToWhereConditionHash.insert(detail->m_strItemID, element.attribute("StrWhere"));
			detail->m_nItemType				= ReportItem::DETAIL;
			detail->m_nHeight				= QVariant(element.attribute("height")).toDouble();
			detail->m_bHideItem				= QVariant(element.attribute("hide", "0")).toBool();
			detail->m_lstFieldsToInitialize = element.attribute("initializeFields").split(";", QString::SkipEmptyParts);
			detail->m_lstFieldsToUpdate		= element.attribute("updateFields").split(";", QString::SkipEmptyParts);
			detail->m_bDetailContainsDetail = true;
			
			//Add to report item order vector and hash.
			static_cast<Detail*>(Parent)->m_lstReportItems << detail;
			/* I think this is something we don't need here, but leave it just in case.
			m_pRepPage->m_lstReportPrintingItemsOrderVector << detail->m_strItemID;
			*/
			m_pRepPage->m_hshReportPrintingItems.insert(detail->m_strItemID, detail);
			m_pRepPage->m_hshDetailRecordSetHash.insert(detail->m_strItemID, DbRecordSet());
			ParseDetail(element, detail);
		}
		else if (element.tagName() == "widget_detail")
		{
			WidgetDetail *widget_detail = new WidgetDetail();
			widget_detail->m_strItemID	= element.attribute("reportItemID");
			widget_detail->m_nItemType	= ReportItem::WIDGET_DETAIL;
			widget_detail->m_dX			= QVariant(element.attribute("x", "0")).toDouble();
			widget_detail->m_dY			= QVariant(element.attribute("y", "0")).toDouble();
			widget_detail->m_bHideItem	= QVariant(element.attribute("hide", "0")).toBool();
			widget_detail->m_dDetailWidth= QVariant(element.attribute("widget_width")).toDouble();
			widget_detail->m_dDetailHeight= QVariant(element.attribute("widget_height", "0")).toDouble();
			widget_detail->m_nPrintStep	= QVariant(element.attribute("print_step", "0")).toInt();
			//Add to report item order vector and hash.
			static_cast<Detail*>(Parent)->m_lstReportItems << widget_detail;
			//m_pRepPage->m_lstReportPrintingItemsOrderVector << widget_detail->m_strItemID;
			m_pRepPage->m_hshReportPrintingItems.insert(widget_detail->m_strItemID, widget_detail);
		}
		//Else if print area.
		else if (element.tagName() == "print_area")
		{
			PagePrintArea *print_area = new PagePrintArea();
			print_area->m_strItemID	  = element.attribute("reportItemID");
			print_area->m_nItemType   = ReportItem::PRINT_AREA;
			print_area->m_nHeight	  = QVariant(element.attribute("height")).toDouble();
			print_area->m_bShrink	  = QVariant(element.attribute("shrink", "false")).toBool();
			print_area->m_bHideItem	  = QVariant(element.attribute("hide", "0")).toBool();

			//Add to detail report item vector.
			static_cast<Detail*>(Parent)->m_lstReportItems << print_area;
			m_pRepPage->m_hshReportPrintingItems.insert(print_area->m_strItemID, print_area);

			GetPrintAreaItems(element, print_area);
		}
		//Else if detail group area.
		else if (element.tagName() == "detailGroup")
		{
			//TODO add function call handling.
			DetailGroup *detail_group = new DetailGroup();
			QString ElemName = element.attribute("reportItemID");
			detail_group->m_strItemID = ElemName;
			detail_group->m_nItemType = ReportItem::DETAIL_GROUP;
			detail_group->m_strBreakFieldID = element.attribute("detailBreakFieldID");
			detail_group->m_bHideItem = QVariant(element.attribute("hide", "0")).toBool();
			
			//Header behaviour.
			detail_group->m_nBreakBehavior = QVariant(element.attribute("detailBreakBehaviour", "1")).toInt();				//Default value (First use only).
			detail_group->m_bRunningHeaderBehavior = QVariant(element.attribute("runningHeader")).toBool();					//Default value.
			detail_group->m_bLocalHeaderBehavior = QVariant(element.attribute("localHeader")).toBool();						//Default value.
			detail_group->m_bOneDetailFooterBehavior = QVariant(element.attribute("detailBreakFooterBehaviour", "0")).toInt();	//Default value.

			detail_group->m_lstFieldsToInitialize = element.attribute("initializeFields").split(";", QString::SkipEmptyParts);
			detail_group->m_lstFieldsToUpdate = element.attribute("updateFields").split(";", QString::SkipEmptyParts);

			//Add group to detail (detail vector and detail hash not to report items list - it's not directly printable).
			static_cast<Detail*>(Parent)->m_lstDetailGroupVector << ElemName;
			static_cast<Detail*>(Parent)->m_hshDetailGroupHash.insert(ElemName, detail_group);

			//Parse header.
			ParseDetailBreak(element, detail_group);
		}

		//Go to next.
		node = node.nextSibling();
	}
}

void ReportParser::ParseDetailBreak(QDomElement Element, ReportItem *Parent)
{
	//Go through detail items (can be function call, print area or detail break).
	QDomNode node = Element.firstChild();

	while(!node.isNull()) 
	{
		// try to convert the node to an element.
		QDomElement element = node.toElement();

		//If detail header area.
		if (element.tagName() == "detailHeader")
		{
			PagePrintingBlock *detail_header_area = new PagePrintingBlock();
			detail_header_area->m_strItemID = element.attribute("reportItemID");
			detail_header_area->m_nItemType = ReportItem::DETAIL_HEADER;
			detail_header_area->m_nHeight = QVariant(element.attribute("height")).toDouble();
			//Add header to its parent member and to printing block list .
			static_cast<DetailGroup*>(Parent)->m_pDetailGroupHeader = detail_header_area;
			static_cast<DetailGroup*>(Parent)->m_lstReportItems << detail_header_area;

			ParseHeaderFooter(element, detail_header_area);
		}
		//Else if detail footer area.
		else if (element.tagName() == "detailFooter")
		{
			PagePrintingBlock *detail_footer_area = new PagePrintingBlock();
			detail_footer_area->m_strItemID = element.attribute("reportItemID");
			detail_footer_area->m_nItemType = ReportItem::DETAIL_HEADER;
			detail_footer_area->m_nHeight = QVariant(element.attribute("height")).toDouble();
			//Add header to its parent member and to printing block list .
			static_cast<DetailGroup*>(Parent)->m_pDetailGroupFooter = detail_footer_area;
			static_cast<DetailGroup*>(Parent)->m_lstReportItems << detail_footer_area;

			ParseHeaderFooter(element, detail_footer_area);
		}
		
		//Go to next.
		node = node.nextSibling();
	}
}

void ReportParser::ParseInstance(QDomElement Element, ReportItem *Parent)
{
	//Go through instance items (can be a function call or print area).
	QDomNode node = Element.firstChild();

	while(!node.isNull()) 
	{
		// try to convert the node to an element.
		QDomElement element = node.toElement();

		//If function call.
		if (element.tagName() == "function_call")
		{
			FunctionCall *function_call = new FunctionCall();
			function_call->m_strItemID = element.attribute("reportItemID");
			function_call->m_nItemType = ReportItem::FUNCTION_CALL;

			//Add to detail report item vector.
			static_cast<PrintInstance*>(Parent)->m_lstReportItems << function_call;
		}
		//Else if print area.
		else if (element.tagName() == "print_area")
		{
			PagePrintArea *print_area = new PagePrintArea();
			print_area->m_strItemID = element.attribute("reportItemID");
			print_area->m_nItemType = ReportItem::PRINT_AREA;
			print_area->m_nHeight	= QVariant(element.attribute("height")).toDouble();
			print_area->m_bShrink	= QVariant(element.attribute("shrink", "0")).toBool();
			print_area->m_bHideItem = QVariant(element.attribute("hide", "0")).toBool();

			//Add to detail report item vector.
			static_cast<PrintInstance*>(Parent)->m_lstReportItems << print_area;

			GetPrintAreaItems(element, print_area);
		}

		//Go to next.
		node = node.nextSibling();
	}
}

QColor ReportParser::Convert2Color(QString ColorString)
{
	bool ok;
	return QColor(ColorString.mid(1,2).toInt(&ok, 16), ColorString.mid(3,2).toInt(&ok, 16), ColorString.mid(5,2).toInt(&ok, 16));
}

QFont ReportParser::GetLabelFont(QDomElement Element)
{
	QFont LabelFont;
	LabelFont.setFamily(Element.attribute("fontName", m_pRepPage->m_fntDefaultFont.family()));
	LabelFont.setPointSize(QVariant(Element.attribute("fontSize", QVariant(m_pRepPage->m_fntDefaultFont.pointSize()).toString())).toInt());
	LabelFont.setBold(QVariant(Element.attribute("fontBold", "false")).toBool());
	LabelFont.setItalic(QVariant(Element.attribute("fontItalic", "false")).toBool());
	LabelFont.setUnderline(QVariant(Element.attribute("fontUnderline", "false")).toBool());

	return LabelFont;
}

int	ReportParser::GetAlignament(QDomElement Element)
{
	if (Element.attribute("horizAlign") == "left")
		return Qt::AlignLeft;
	else if (Element.attribute("horizAlign") == "right")
		return Qt::AlignRight;
	else if (Element.attribute("horizAlign") == "center")
		return Qt::AlignHCenter;
	else if (Element.attribute("horizAlign") == "justify")
		return Qt::AlignJustify;
	else
		return Qt::AlignLeft;
}
