#ifndef REPORTRECTANGLE_H
#define REPORTRECTANGLE_H

#include <QPainter>

/*!
\brief Report rectangle.
\ingroup 

Report rectangle definition class - used to print rectangles on report.

*/
class ReportRectangle
{
public:
    ReportRectangle(QPainter *Painter);
    ~ReportRectangle();

	void PaintRectangle(QRectF Rect, int RotationAngle = 0);
	void PaintRoundedRectangle(QRectF Rect, int RotationAngle = 0, int XRnd = 0, int YRnd = 0);

	QPen	m_Pen;						//< Rectangle pen.
	QBrush	m_BackgroundBrush;			//< Rectangle background brush.

private:
	void RestoreDefaults();

	QPainter *m_pPainter;				//< Pointer to report painter.

	QPen	m_nDefaultPen;				//< Painter initial (before entering this object) pen.
	QBrush	m_DefaultBackgroundBrush;	//< Painter initial (before entering this object) background brush.
	QMatrix m_DefaultMatrix;			//< Painter initial (before entering this object) matrix.
};

#endif // REPORTRECTANGLE_H
