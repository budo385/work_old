#include "reportpicture.h"

ReportPicture::ReportPicture(QPainter *Painter)
{
	//Painter.
	m_pPainter		= Painter;

	//Original painter values for later restoring (if one day we want to rotate the picture).
	m_DefaultMatrix			 = m_pPainter->matrix();
}

ReportPicture::~ReportPicture()
{

}

/*!
Paint image in target rectangle from source rectangle with conversion flags.

\param target	- Target rectangle.
\param image	- Image.
\param source	- Source rectangle.
\param flags	- Conversion flags.
*/
void ReportPicture::PaintImage(QRectF target, QImage &image, QRectF source, Qt::ImageConversionFlags flags /*= Qt::AutoColor*/)
{
	m_pPainter->drawImage(target, image, source, flags);
}

/*!
Paint picture on point coordinates.

\param Point	- Point.
\param picture	- Picture.
*/
void ReportPicture::PaintPicture(QPointF Point, QPicture &picture)
{
	m_pPainter->drawPicture(Point, picture);
}

/*!
Paint pixmap on x, y coordinates with width give and height.

\param target	- Target rectangle.
\param pixmap	- Pixmap.
*/
void ReportPicture::PaintPixmap(QRectF target, QPixmap &pixmap)
{
	//m_pPainter->drawPixmap(target.x(), target.y(), target.width(), target.height(), pixmap, 0, 0, pixmap.width(), pixmap.height());
	m_pPainter->drawPixmap(target.x(), target.y(), pixmap);
}

void ReportPicture::PaintPixmapRect(QRectF target, QPixmap &pixmap, QRectF source)
{
	m_pPainter->drawPixmap(target, pixmap, source);
}
