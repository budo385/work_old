#include "reportlabel.h"
#include <QTextDocument>
#include <QTextBlock>
#include <QTextLayout>

ReportLabel::ReportLabel(QPainter *Painter)
{
	//Painter.
	m_pPainter		= Painter;

	//Default label values.
	m_nWrap					 = Qt::TextWordWrap;
	m_nHAlignment			 = Qt::AlignRight;
	m_nVAlignment			 = Qt::AlignBottom;
	m_BackgroundBrush		 = QBrush(QColor(Qt::white));
	m_nPen					 = QPen(QColor(Qt::black));
	m_Font					 = m_pPainter->font();
	
	//Original painter values for later restoring.
	m_DefaultFont			 = m_pPainter->font();
	m_nDefaultPen			 = m_pPainter->pen();
	m_DefaultBackgroundBrush = m_pPainter->brush();
	m_DefaultMatrix			 = m_pPainter->matrix();
}

ReportLabel::~ReportLabel()
{
	m_pPainter = NULL;
}

/*!
Paint single line label on given coordinates (starting from bottom left of bounding box).

\param Point		- Start point position.
\param LabelText	- Text to print.
\param LabelLength	- Length of label (if not set - take whole label).
*/
void ReportLabel::PaintSingleLine(QPointF Point, QString LabelText, int LabelLength /*= -1*/)
{
	//Set font (in case user set it manually).
	m_pPainter->setFont(m_Font);
	
	//Set width and height.
	QFontMetrics FMetr	= m_pPainter->fontMetrics();
	double Height		= FMetr.height();
	double Width;
	if (LabelLength == -1)
		Width = FMetr.width(LabelText);
	else
		Width = LabelLength*FMetr.width("O");

	//Set painter defaults.
	m_pPainter->setBrush(m_BackgroundBrush);
	m_pPainter->setPen(m_nPen);
	m_pPainter->setFont(m_Font);

	//Set flags (just like that).
	int Flags = (m_nWrap | m_nHAlignment | m_nVAlignment);

	//Translate starting point to bottom left.
	QMatrix Matrix;
	Matrix.translate(Point.x(), Point.y() - FMetr.ascent() - 1);
	m_pPainter->setMatrix(Matrix);

	//Paint (remove font height and add descent and line point so that it start).
	QRectF DrawRect(QPointF(0, 0), QSize(Width, Height));
	m_pPainter->drawText(DrawRect, Flags, LabelText);
	RestoreDefaults();
}

/*!
Paint single line label on given coordinates and rotate (starting from bottom left of bounding box).

\param Point		 - Start point position.
\param RotationAngle - Angle of rotation (in degrees).
\param LabelText	 - Text to print.
\param LabelLength	 - Length of label (if not set - take whole label).
*/
void ReportLabel::PaintSingleLine(QPointF Point, int RotationAngle, QString LabelText, int LabelLength /*= -1*/)
{
	//Set font (in case user set it manually).
	m_pPainter->setFont(m_Font);

	//Set width and height.
	QFontMetrics FMetr	= m_pPainter->fontMetrics();

	double Height		= FMetr.height();
	double Width;
	if (LabelLength == -1)
		Width = FMetr.width(LabelText);
	else
		Width = LabelLength*FMetr.width("O");

	//Set painter defaults.
	m_pPainter->setBrush(m_BackgroundBrush);
	m_pPainter->setPen(m_nPen);
	m_pPainter->setFont(m_Font);

	//Set flags.
	int Flags = (m_nWrap | m_nHAlignment | m_nVAlignment);

	//Set rotation.
	QMatrix Matrix;
	Matrix.translate(Point.x(), Point.y() - FMetr.ascent() - 1);
	Matrix.rotate(RotationAngle);
	m_pPainter->setMatrix(Matrix);
	m_pPainter->setRenderHint(QPainter::TextAntialiasing);

	//Paint (add descent and line point).
	QRectF DrawRect(QPointF(0, 0), QSize(Width, Height));
	m_pPainter->drawText(DrawRect, Flags, LabelText);
	RestoreDefaults();
}

/*!
Paint multi line label on given coordinates (starting from top left of bounding box).

\param Point				- Start point position.
\param LabelText			- Text to print.
\param LabelLength			- Length of label in characters.
\param LablelMaxLinesNumber - Max allowed number of lines.
*/
void ReportLabel::PaintMultiLine(QPointF Point, QString LabelText, int LabelLength, int LablelMaxLinesNumber, int nWrap)
{
	//Set font (in case user set it manually).
	m_pPainter->setFont(m_Font);

	//Set width and height.
	QFontMetrics FMetr	= m_pPainter->fontMetrics();
	double Height		= LablelMaxLinesNumber * FMetr.lineSpacing();
	double Width		= LabelLength*FMetr.maxWidth();

	//Set painter defaults.
	m_pPainter->setBrush(m_BackgroundBrush);
	m_pPainter->setPen(m_nPen);
	m_pPainter->setFont(m_Font);

	//Set top alignment so that it start paint in top left corner.
	m_nVAlignment = Qt::AlignTop;
	m_nHAlignment = Qt::AlignLeft;
	if (nWrap)
		m_nWrap	= Qt::TextWrapAnywhere;
	else
		m_nWrap	= Qt::TextWordWrap;


	//Set flags.
	int Flags = (m_nWrap | m_nHAlignment | m_nVAlignment);

	//Set translation.
	QMatrix Matrix;
	Matrix.translate(Point.x(), Point.y() - FMetr.ascent() - 1);
	m_pPainter->setMatrix(Matrix);

	//Paint (remove font height, descent and line point so that it start to paint on bottom left of first row).
	QRectF DrawRect(QPointF(0, 0), QSize(Width, Height));
	m_pPainter->drawText(DrawRect, Flags, LabelText);
	RestoreDefaults();
}

int ReportLabel::PaintMultiLineNew(QPointF Point, QString LabelText, int LabelLength, int LablelMaxLinesNumber, int nWrap)
{
	//Set font (in case user set it manually).
	m_pPainter->setFont(m_Font);

	QFontMetricsF FMetr	= m_pPainter->fontMetrics();
	double Height		= LablelMaxLinesNumber * FMetr.lineSpacing();
	double Width		= LabelLength*FMetr.maxWidth();
	
	//Set painter defaults.
	m_pPainter->setBrush(m_BackgroundBrush);
	m_pPainter->setPen(m_nPen);
	m_pPainter->setFont(m_Font);
	
	//Set translation.
	QMatrix Matrix;
	Matrix.translate(Point.x(), Point.y() - FMetr.ascent() - 1);
	m_pPainter->setMatrix(Matrix);
	
	//Create text document and draw it to painter.
	QTextDocument doc(LabelText);
	doc.setDefaultFont(m_Font);
	doc.setPageSize(QSizeF(Width, Height));
	//doc.drawContents(m_pPainter, QRectF(0, 0, Width, 50/*+FMetr.lineSpacing()-FMetr.descent()*/));	//This is so that exactly fit. Don't touch if OK.
	doc.drawContents(m_pPainter, QRectF(0, 0, Width, Height/*+FMetr.lineSpacing()-FMetr.descent()*/));	//This is so that exactly fit. Don't touch if OK.

	//Count lines.
	int nLineCount = 0;
	int nBlockCount = doc.blockCount();
	for (int i = 0; i < nBlockCount; ++i)
		nLineCount += doc.findBlock(i).layout()->lineCount();
	
	RestoreDefaults();
	
	if (nLineCount > LablelMaxLinesNumber)
		nLineCount = LablelMaxLinesNumber;

	return nLineCount;
}

int ReportLabel::PaintMultiLineNewInMM(QPointF Point, QString LabelText, int LabelLength /*in mm*/, int LablelMaxLinesNumber, int nWrap)
{
	//Set font (in case user set it manually).
	m_pPainter->setFont(m_Font);

	QFontMetricsF FMetr	= m_pPainter->fontMetrics();
	double Height		= LablelMaxLinesNumber * FMetr.lineSpacing();
	double Width		= LabelLength;

	//Set translation.
	QMatrix Matrix;
	Matrix.translate(Point.x(), Point.y() - FMetr.ascent() - 1);
	m_pPainter->setMatrix(Matrix);

	//Create text document and draw it to painter.
	QTextDocument doc(LabelText);
	doc.setDefaultFont(m_Font);
	doc.setPageSize(QSizeF(Width, Height));
	//doc.drawContents(m_pPainter, QRectF(0, 0, Width, 50/*+FMetr.lineSpacing()-FMetr.descent()*/));	//This is so that exactly fit. Don't touch if OK.
	doc.drawContents(m_pPainter, QRectF(0, 0, Width, Height/*+FMetr.lineSpacing()-FMetr.descent()*/));	//This is so that exactly fit. Don't touch if OK.

	//Count lines.
	int nLineCount = 0;
	int nBlockCount = doc.blockCount();
	for (int i = 0; i < nBlockCount; ++i)
		nLineCount += doc.findBlock(i).layout()->lineCount();

	RestoreDefaults();

	if (nLineCount > LablelMaxLinesNumber)
		nLineCount = LablelMaxLinesNumber;

	return nLineCount;
}

/*!
Restore default values to painter.

*/
void ReportLabel::RestoreDefaults()
{
	//Restore painter.
	m_pPainter->setFont(m_DefaultFont);
	m_pPainter->setPen(m_nDefaultPen);
	m_pPainter->setBackground(m_DefaultBackgroundBrush);
	m_pPainter->setMatrix(m_DefaultMatrix);

	//Restore local values.
	m_nWrap				= Qt::TextWordWrap;
	m_nHAlignment		= Qt::AlignLeft;
	m_nVAlignment		= Qt::AlignBottom;
	m_BackgroundBrush	= QBrush(QColor(Qt::white));
	m_nPen				= QPen(QColor(Qt::black));
	m_Font				= m_pPainter->font();

	//TODO add line spacing.
}
