#ifndef REPORTPRINTINGELEMENTS_H
#define REPORTPRINTINGELEMENTS_H

#include <QString>
#include <QPointF>
#include <QRectF>
#include <QFont>
#include <QPen>
#include <QBrush>
#include <QImage>
#include <QPicture>
#include <QPixmap>

class PrintingElement
{

public:
    PrintingElement();
    ~PrintingElement();

	enum ElementType
	{
		LABEL,
		POINT,
		LINE,
		IMAGE,
		PICTURE,
		PIXMAP,
		RECTANGLE,
		ROUND_RECT,
		FIELD,
		MULTILINE_FIELD,
		MULTILINE_FIELD_MM,
		SUM_FIELD,
		RECORD_FIELD
	};

	QString m_strElementID;									//<	Element ID.
	int		m_nElementType;									//< Element type (label, rectangle, image, ...).
	bool	m_bHideElement;									//< Hide element (print or not).
	QString m_strFieldType;									//< Sum, total, history, ... or just some ordinary field.
	double	m_dblCountIncrement;							//< Count increment for count fields.
};

class LabelElement : public PrintingElement
{
public:
	LabelElement(){m_nElementType = PrintingElement::LABEL;};
	~LabelElement(){};

	double  m_dX;											//< Label X position.
	double  m_dY;											//< Label Y position.
	double	m_dLine;										//< Label line (Y position) in print area.
	QString m_strLabelText;									//< Label text.
	int		m_nLabelLength;									//< Label length (number of characters).
	int		m_nRotationAngle;								//< Angle of rotation (in degrees).
	int		m_nMaxLineNumbers;								//< Maximum line number (for multi line labels). 
	int		m_nWrap;										//< Wrap text.
	int		m_nHAlignment;									//< Horizontal alignment.
	int		m_nVAlignment;									//< Vertical alignment.
	QFont	m_Font;											//< Label font.
	QPen	m_Pen;											//< Label pen.
	QBrush	m_BackgroundBrush;								//< Label background brush.
};

class PointElement : public PrintingElement
{
public:
	PointElement(){};
	~PointElement(){};

	double  m_dX;											//< Point X position.
	double  m_dY;											//< Point Y position.
	QPen	m_Pen;											//< Point pen.
};

class LineElement : public PrintingElement
{
public:
	LineElement(){};
	~LineElement(){};

	double  m_dStartX;										//< Line start X position.
	double  m_dStartY;										//< Line start Y position.
	double  m_dEndX;										//< Line end X position.
	double  m_dEndY;										//< Line end Y position.
	double	m_dLine;										//< Line (Y position) in print area (if defined).
	QPen	m_Pen;											//< Line pen.
};

class ImageElement : public PrintingElement
{
public:
	ImageElement(){};
	~ImageElement(){};

	QImage m_Image;											//< Image.
	QRectF m_TargetRectangle;								//< Target rectangle.
	QRectF m_SourceRectangle;								//< Source rectangle.
	int m_nImageConversionFlags;							//< Image conversion flags (for Image).
};

class PictureElement : public PrintingElement
{
public:
	PictureElement(){};
	~PictureElement(){};

	QPicture m_Picture;										//< Picture.
	QPointF m_DrawingPoint;									//< Picture start point.
};

class PixmapElement : public PrintingElement
{
public:
	PixmapElement(){};
	~PixmapElement(){};

	double  m_dX;											//< Pixmap target frame X position.
	double  m_dY;											//< Pixmap target frame Y position.
	double  m_dWidth;										//< Pixmap target frame width.
	double  m_dHeight;										//< Pixmap target frame height.
	QString m_strDataField;									//< Recordset data field.

	QPixmap m_Pixmap;										//< Pixmap.
};

class RectangleElement : public PrintingElement
{
public:
	RectangleElement(){};
	~RectangleElement(){};

	double  m_dX;											//< Rectangle X position.
	double  m_dY;											//< Rectangle Y position.
	double  m_nXRnd;										//< Rectangle X roundness - 0 is angled corners, 99 is maximum roundness.
	double  m_nYRnd;										//< Rectangle Y roundness - 0 is angled corners, 99 is maximum roundness.
	double  m_dWidth;										//< Rectangle width.
	double  m_dHeight;										//< Rectangle height.
	int		m_nMaxLines;									//< Maximum number of lines (for multi line label).
	int		m_nRotation;									//< Rectangle rotation.
	QPen	m_Pen;											//< Rectangle pen.
	QBrush	m_BackgroundBrush;								//< Rectangle background brush.
};

class RoundRectangleElement : public PrintingElement
{
public:
	RoundRectangleElement(){};
	~RoundRectangleElement(){};

	double  m_dX;											//< Rectangle X position.
	double  m_dY;											//< Rectangle Y position.
	double  m_dWidth;										//< Rectangle width.
	double  m_dHeight;										//< Rectangle height.
	int		m_nXRoundness;									//< X roundness.
	int		m_nYRoundness;									//< Y roundness.
	int		m_nRotation;									//< Rectangle rotation.
	QPen	m_Pen;											//< Rectangle pen.
	QBrush	m_BackgroundBrush;								//< Rectangle background brush.
};

class FieldElement : public LabelElement
{
public:
	FieldElement(){};
	~FieldElement(){};

	QString m_strDataType;									//< Field data type.
	QString m_strValueSource;								//< ID of field which values we are using.
	QString m_strInitializeIn;								//< Initialize value in (detail, group, ...).
	QString m_strUpdateIn;									//< Update value in (detail, group, ...).
	QString m_strWidthInMM;									//< Mark that the width is in millimeters (1 if true).
};

class SumFieldElement : public FieldElement
{
public:
	SumFieldElement(){};
	~SumFieldElement(){};
};

#endif // REPORTPRINTINGELEMENTS_H
