#include "reportrectangle.h"

ReportRectangle::ReportRectangle(QPainter *Painter)
{
	//Painter.
	m_pPainter		= Painter;

	//Default rectangle values.
	m_BackgroundBrush	= QBrush(QColor(Qt::white));
	m_Pen				= QPen(QColor(Qt::black));

	//Original painter values for later restoring.
	m_nDefaultPen			 = m_pPainter->pen();
	m_DefaultBackgroundBrush = m_pPainter->brush();
	m_DefaultMatrix			 = m_pPainter->matrix();
}

ReportRectangle::~ReportRectangle()
{
	m_pPainter = NULL;
}

/*!
Paint rectangle on given coordinates (starting from bottom left).

\param Xpos			 - X position.
\param Ypos			 - Y position.
\param RectWidth	 - Rectangle width.
\param RectHeight	 - Rectangle height.
\param RotationAngle - Rotation angle (in degrees).
*/
void ReportRectangle::PaintRectangle(QRectF Rect, int RotationAngle /*= 0*/)
{
	//Set painter defaults.
	m_pPainter->setBrush(m_BackgroundBrush);
	m_pPainter->setPen(m_Pen);

	//Set rotation.
	QMatrix Matrix;
	Matrix.translate(Rect.x(), Rect.y());
	Matrix.rotate(RotationAngle);
	m_pPainter->setMatrix(Matrix);
	m_pPainter->setRenderHint(QPainter::Antialiasing);

	//Paint rectangle - starting from bottom left.
	QRectF DrawRectangle(0, - Rect.height(), Rect.width(), Rect.height());
	m_pPainter->drawRect(DrawRectangle);
	RestoreDefaults();
}

/*!
Paint rounded rectangle on given coordinates (starting from bottom left).

\param Xpos			 - X position.
\param Ypos			 - Y position.
\param RectWidth	 - Rectangle width.
\param RectHeight	 - Rectangle height.
\param XRnd			 - Rectangle roundness - 0 is angled corners, 99 is maximum roundness.
\param YRnd			 - Rectangle roundness - 0 is angled corners, 99 is maximum roundness.
\param RotationAngle - Rotation angle (in degrees).
*/
void ReportRectangle::PaintRoundedRectangle(QRectF Rect, int XRnd /*= 25*/, int YRnd /*= 25*/, int RotationAngle /*= 0*/)
{
	//Set defaults.
	//Set painter defaults.
	m_pPainter->setBrush(m_BackgroundBrush);
	m_pPainter->setPen(m_Pen);

	//Set rotation.
	QMatrix Matrix;
	Matrix.translate(Rect.x(), Rect.y());
	Matrix.rotate(RotationAngle);
	m_pPainter->setMatrix(Matrix);
	m_pPainter->setRenderHint(QPainter::Antialiasing);

	//Paint rectangle - starting from bottom left.
	QRectF DrawRectangle(0, - Rect.height(), Rect.width(), Rect.height());
	m_pPainter->drawRoundRect(DrawRectangle, XRnd, YRnd);
	RestoreDefaults();
}

void ReportRectangle::RestoreDefaults()
{
	//Restore painter.
	m_pPainter->setPen(m_nDefaultPen);
	m_pPainter->setBackground(m_DefaultBackgroundBrush);
	m_pPainter->setMatrix(m_DefaultMatrix);

	//Restore local values.
	m_BackgroundBrush	= QBrush(QColor(Qt::white));
	m_Pen				= QPen(QColor(Qt::black));
}
