#ifndef _NAT_PROTOCOL_H
#define _NAT_PROTOCOL_H

typedef void (* FN_LOGGER)(int, const char *, unsigned long);

// NAT protocol types
typedef enum{
	NP_UPNP,
	NP_NATPMP
} tNatProtocols;	

// port forwarding status
typedef enum{
	PFS_PORT_MAPPING,
	PFS_PORT_MAPPED,
	PFS_PORT_UNMAPPING,
	PFS_PORT_UNMAPPED,
	PFS_PORT_ERROR
} tPFStatus;

class NatProtocol
{
public:
	NatProtocol(void);
	virtual ~NatProtocol(void);

	void RegisterLogger(FN_LOGGER pFn, unsigned long nData);
	
	void SetEnabled(bool bEnable);	//call this before initialize
	bool GetEnabled(){	return m_bEnabled; }
	int	 GetState(){	return m_nState; }

	virtual int GetType() = 0;

	virtual void Init(){};
	virtual void Uninit(){};
	virtual int ForwardPort(int nPort){	return -1; };
	virtual void StopForwardPort(int nPort){};
	
protected:
	void Log(int nMsg, const char *szFmt, ...);

protected:
	//logger
	FN_LOGGER		m_pFnLogger;
	unsigned long	m_nLogData;

	//protocol state
	bool	m_bEnabled;
	bool	m_bInitialised;
	bool	m_bHasDiscovered;
	int		m_nState;
	int		m_nPort;
	bool	m_bIsMapped;
};

#endif