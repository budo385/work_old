#include "nattraversal.h"
#include "NatNatpmp.h"
#include "NatUpnp.h"
#include <QHostAddress>

#ifndef max
#define max(x,y) (((x)>(y))?(x):(y))
#endif

NATTraversal::NATTraversal()
{
	m_pFnLogger = NULL;
	m_nLogData = 0;

	m_pUpnp = new NatUpnp();
	m_pNatpmp = new NatNatpmp();
}

NATTraversal::~NATTraversal()
{
	delete m_pUpnp;
	delete m_pNatpmp;
}

void NATTraversal::RegisterLogger(FN_LOGGER pFn, unsigned long nData)
{
	m_pFnLogger = pFn;
	m_nLogData = nData;

	m_pNatpmp->RegisterLogger(m_pFnLogger, nData);
	m_pUpnp->RegisterLogger(m_pFnLogger, nData);
}

void NATTraversal::Log(int nMsg, const char *szFmt, ...)
{
	//BT temporarly disabled: distracting me
	return;
	if(m_pFnLogger){
		char szBuffer[1024] ="";
		va_list ap;
		va_start(ap, szFmt);
		vsnprintf(szBuffer, sizeof(szBuffer), szFmt, ap);

		m_pFnLogger(nMsg, szBuffer, m_nLogData);
	}
}

void NATTraversal::EnableUPnP(bool bEnable)
{
	m_pUpnp->SetEnabled(bEnable);
}

void NATTraversal::EnableNatPmp(bool bEnable)
{
	m_pNatpmp->SetEnabled(bEnable);
}

void NATTraversal::Initialize()
{
	Log(0, ">>> Start Initialization" );

	m_pNatpmp->Init();
	m_pUpnp->Init();

	Log(0, "<<< End Initialization" );
}

void NATTraversal::Uninitialize()
{
	m_pNatpmp->Uninit();
	m_pUpnp->Uninit();
}

int NATTraversal::StartPortForwardRequest(int nPort)
{
	Log(0, ">>> Start Port Forwarding process" );

	m_pNatpmp->ForwardPort(nPort);
	m_pUpnp->ForwardPort(nPort);

	Log(0, "<<< End Start Port Forwarding process" );

	return GetGlobalStatus();
}

int NATTraversal::StopPortForwardRequest(int nPort)
{
	Log(0, ">>> Stop Port Forwarding process" );
	
	m_pNatpmp->StopForwardPort(nPort);
	m_pUpnp->StopForwardPort(nPort);

	Log(0, "<<< End Stop Port Forwarding process" );

	return GetGlobalStatus();
}

void NATTraversal::GetPublicIP(QString &strIP)
{
}

int	 NATTraversal::GetGlobalStatus()
{
    return max(m_pNatpmp->GetState(), m_pUpnp->GetState());
}

int	 NATTraversal::GetGlobalIsEnabled()
{
	return (m_pNatpmp->GetEnabled() || m_pUpnp->GetEnabled());
}

void NATTraversal::NatPulse(bool bCheckPort)
{
//	const tr_port port = s->session->peerPort;
//    const int isEnabled = s->isEnabled && !s->isShuttingDown;
/*
    int oldStatus = GetGlobalStatus( s );
    s->natpmpStatus = tr_natpmpPulse( s->natpmp, port, isEnabled );
    s->upnpStatus = tr_upnpPulse( s->upnp, port, isEnabled, doPortCheck );
    int newStatus = GetGlobalStatus( s );

    if( newStatus != oldStatus )
        Log( "State changed from \"%d\" to \"%d\"", oldStatus, newStatus ); 
		*/
}

// check if the connection can be established
bool NATTraversal::TestConnection(QHostAddress host, quint16 port, int timeout)
{
  QTcpSocket sock;
  sock.connectToHost(host, port);
  if (!sock.waitForConnected(timeout)) {
    return false;
  }
  sock.disconnectFromHost();
  return true;
}
 