#include "NatNatpmp.h"
#ifndef _WIN32
 #include <unistd.h>
 #include <arpa/inet.h>
#endif

#define LIFETIME_SECS 3600
#define COMMAND_WAIT_SECS 8

NatNatpmp::NatNatpmp()
{
	m_nInternalState = TR_NATPMP_DISCOVER;
}

NatNatpmp::~NatNatpmp()
{
}

void NatNatpmp::Init()
{
	//init NAT-PMP
	if(m_bEnabled){
		m_nInternalState = TR_NATPMP_DISCOVER;
		m_nPort = -1;
		m_natpmp.s = -1; // socket

		int val = initnatpmp( &m_natpmp );
		Log(0, "NATPMP: init (%d): %s", val, strnatpmperr(val) );
		val = sendpublicaddressrequest( &m_natpmp );
		Log(0, "NATPMP: sendpublicaddressrequest (%d bytes sent)", val);
		m_nInternalState = val < 0 ? TR_NATPMP_ERR : TR_NATPMP_RECV_PUB;
		m_bHasDiscovered = 1;
		m_commandTime = time(NULL) + COMMAND_WAIT_SECS;
	}
	else
		Log(0, "NATPMP: disabled");
}

void NatNatpmp::Uninit()
{
	if(m_bEnabled){
		//closenatpmp(&m_natpmp);

		if( m_natpmp.s >= 0 ){
		#ifdef WIN32
			closesocket(m_natpmp.s);
		#else
			close(m_natpmp.s);
		#endif
			m_natpmp.s = -1;
		}
	}
	m_nInternalState = TR_NATPMP_DISCOVER;
}

int NatNatpmp::ForwardPort(int nPort)
{
	if(m_bEnabled)
	{
		if( m_nInternalState == TR_NATPMP_DISCOVER ){
			Init();
		}

		if( ( m_nInternalState == TR_NATPMP_RECV_PUB ) && (time(NULL) >= m_commandTime) )
		{
			natpmpresp_t response;
			const int    val = readnatpmpresponseorretry( &m_natpmp, &response );
			Log(0, "NATPMP: readnatpmpresponseorretry (%d): %s", val, strnatpmperr(val));
			if( val >= 0 )
			{
				Log(0, "NATPMP: Found public address \"%s\"", inet_ntoa( response.pnu.publicaddress.addr ) );
				m_nInternalState = TR_NATPMP_IDLE;
			}
			else if( val != NATPMP_TRYAGAIN )
			{
				Log(0, "NATPMP: Error Finding public address");
				m_nInternalState = TR_NATPMP_ERR;
			}
		}

		if( ( m_nInternalState == TR_NATPMP_IDLE ) || ( m_nInternalState == TR_NATPMP_ERR ) )
		{
			if( m_bIsMapped && ( m_nPort != nPort ) )
				m_nInternalState = TR_NATPMP_SEND_UNMAP;
		}

		if( ( m_nInternalState == TR_NATPMP_SEND_UNMAP ) && (time(NULL) >= m_commandTime) )
		{
			StopForwardPort(m_nPort);
		}

		if( m_nInternalState == TR_NATPMP_RECV_UNMAP )
		{
			natpmpresp_t resp;
			const int    val = readnatpmpresponseorretry( &m_natpmp, &resp );
			Log(0, "NATPMP: readnatpmpresponseorretry (%d): %s", val, strnatpmperr(val));
			if( val >= 0 )
			{
				const int p = resp.pnu.newportmapping.privateport;
				Log(0, "NATPMP: no longer forwarding port %d", p);
				if( m_nPort == p )
				{
					m_nPort = -1;
					m_nInternalState = TR_NATPMP_IDLE;
					m_bIsMapped = 0;
				}
			}
			else if( val != NATPMP_TRYAGAIN )
			{
				m_nInternalState = TR_NATPMP_ERR;
			}
		}

		if( m_nInternalState == TR_NATPMP_IDLE )
		{
			if( !m_bIsMapped && m_bHasDiscovered )
				m_nInternalState = TR_NATPMP_SEND_MAP;

			else if( m_bIsMapped && time(NULL) >= m_renewTime )
				m_nInternalState = TR_NATPMP_SEND_MAP;
		}

		if( ( m_nInternalState == TR_NATPMP_SEND_MAP ) && (time(NULL) >= m_commandTime) )
		{
			//mapping
			const int val =
				sendnewportmappingrequest( &m_natpmp, NATPMP_PROTOCOL_TCP,
										   nPort,
										   nPort,
										   LIFETIME_SECS );
			Log(0, "NATPMP: sendnewportmappingrequest (%d bytes sent): %s", (val >= 0)? val : 0, (val < 0)? strnatpmperr(val) : "" );
			m_nInternalState = val < 0 ? TR_NATPMP_ERR : TR_NATPMP_RECV_MAP;
			m_commandTime = time(NULL) + COMMAND_WAIT_SECS;
		}

		if( m_nInternalState == TR_NATPMP_RECV_MAP )
		{
			natpmpresp_t resp;
			const int    val = readnatpmpresponseorretry( &m_natpmp, &resp );
			Log(0, "NATPMP: readnatpmpresponseorretry (%d): %s", val, strnatpmperr(val));
			if( val >= 0 )
			{
				m_nInternalState = TR_NATPMP_IDLE;
				m_bIsMapped = 1;
				m_renewTime = time(NULL) + LIFETIME_SECS;
				m_nPort = resp.pnu.newportmapping.privateport;
				Log(0, "NATPMP: Port %d forwarded successfully", m_nPort );
			}
			else if( val != NATPMP_TRYAGAIN )
			{
				Log(0, "NATPMP: Error forwarding port %d", m_nPort );
				m_nInternalState = TR_NATPMP_ERR;
			}
			else{
				Log(0, "NATPMP: 'try again' error");
			}
		}

		int ret;
		switch( m_nInternalState )
		{
			case TR_NATPMP_IDLE:
				ret = m_bIsMapped ? PFS_PORT_MAPPED : PFS_PORT_UNMAPPED; break;

			case TR_NATPMP_DISCOVER:
				ret = PFS_PORT_UNMAPPED; break;

			case TR_NATPMP_RECV_PUB:
			case TR_NATPMP_SEND_MAP:
			case TR_NATPMP_RECV_MAP:
				ret = PFS_PORT_MAPPING; break;

			case TR_NATPMP_SEND_UNMAP:
			case TR_NATPMP_RECV_UNMAP:
				ret = PFS_PORT_UNMAPPING; break;

			default:
				ret = PFS_PORT_ERROR; break;
		}
		
		m_nState = ret; 
	}

	return m_nState;
}
	
void NatNatpmp::StopForwardPort(int nPort)
{
	const int val =
		sendnewportmappingrequest( &m_natpmp, NATPMP_PROTOCOL_TCP,
		m_nPort, m_nPort,
		0 );

	Log(0, "NATPMP: sendnewportmappingrequest %d", val);
	m_nInternalState = val < 0 ? TR_NATPMP_ERR : TR_NATPMP_RECV_UNMAP;
	m_commandTime = time(NULL) + COMMAND_WAIT_SECS;

}
