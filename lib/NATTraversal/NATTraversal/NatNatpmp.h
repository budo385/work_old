#pragma once
#include "natprotocol.h"
extern "C" {
#include "libnatpmp/natpmp.h"
}

class NatNatpmp :
	public NatProtocol
{
	//internal state
	typedef enum
	{
		TR_NATPMP_IDLE,
		TR_NATPMP_ERR,
		TR_NATPMP_DISCOVER,
		TR_NATPMP_RECV_PUB,
		TR_NATPMP_SEND_MAP,
		TR_NATPMP_RECV_MAP,
		TR_NATPMP_SEND_UNMAP,
		TR_NATPMP_RECV_UNMAP
	}tInternalState;

public:
	NatNatpmp(void);
	~NatNatpmp(void);

	virtual int GetType(){	return NP_NATPMP; };
	virtual void Init();
	virtual void Uninit();
	virtual int ForwardPort(int nPort);
	virtual void StopForwardPort(int nPort);

protected:
	//NAT-PMP related fields
	natpmp_t m_natpmp;
	time_t  m_renewTime;
    time_t  m_commandTime; 
	int		m_nInternalState;
};
