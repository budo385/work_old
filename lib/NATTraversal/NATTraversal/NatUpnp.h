#pragma once
#include "natprotocol.h"
#include "miniupnpc/miniupnpc.h"
#include "miniupnpc/upnpcommands.h"

class NatUpnp :
	public NatProtocol
{
	//internal state
	typedef enum
	{
		TR_UPNP_IDLE,
		TR_UPNP_ERR,
		TR_UPNP_DISCOVER,
		TR_UPNP_MAP,
		TR_UPNP_UNMAP
	} tInternalState;

public:
	NatUpnp(void);
	virtual ~NatUpnp(void);

	virtual int GetType(){ return NP_UPNP; };
	virtual void Init();
	virtual void Uninit();
	virtual int ForwardPort(int nPort);
	virtual void StopForwardPort(int nPort);

protected:
	//UPnP specific fields
	struct UPNPUrls m_Urls;
	struct IGDdatas m_Data;	// "Internet Gateway Devices" data
	char    m_lanaddr[16];
	int		m_nInternalState;
};
