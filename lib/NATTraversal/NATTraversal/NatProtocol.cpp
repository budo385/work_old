#include "NatProtocol.h"
#include <stdio.h>
#include <stdarg.h>

NatProtocol::NatProtocol(void)
{
	m_bEnabled = true;	//default
	m_bInitialised	= false;
	m_bHasDiscovered = false;
	m_nState = -1;
	m_nPort = -1;
	m_bIsMapped = false;

	m_pFnLogger = NULL;
	m_nLogData = 0;
}

NatProtocol::~NatProtocol(void)
{
}

void NatProtocol::SetEnabled(bool bEnable)
{
	m_bEnabled = bEnable;
}

void NatProtocol::RegisterLogger(FN_LOGGER pFn, unsigned long nData)
{
	m_pFnLogger = pFn;
	m_nLogData = nData;
}

void NatProtocol::Log(int nMsg, const char *szFmt, ...)
{
	//BT temporarly disabled: distracting me
	return;

	if(m_pFnLogger){
		char szBuffer[1024] ="";
		va_list ap;
		va_start(ap, szFmt);
		vsnprintf(szBuffer, sizeof(szBuffer), szFmt, ap);

		m_pFnLogger(nMsg, szBuffer, m_nLogData);
	}
}
