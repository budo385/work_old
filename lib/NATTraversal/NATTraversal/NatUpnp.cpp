#include "NatUpnp.h"
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>

#define TR_NAME "PixFromHome"

NatUpnp::NatUpnp()
{
	m_nInternalState = TR_UPNP_DISCOVER;
}

NatUpnp::~NatUpnp()
{
}

void NatUpnp::Init()
{
	if(m_bEnabled){
		memset(&m_Urls, 0, sizeof(struct UPNPUrls));
		memset(&m_Data, 0, sizeof(struct IGDdatas));

		m_nInternalState = TR_UPNP_DISCOVER;

		Log(0, "UPNP: start upnpDiscover");

		//discover upnp devices
		errno = 0;
		struct UPNPDev *devlist = upnpDiscover(2000, NULL, NULL, 0);
		if(devlist == NULL)
		{
			Log(0, "UPNP: upnpDiscover failed (%d) : %s", errno, strerror(errno));
			m_nInternalState = TR_UPNP_ERR;
			return;	//UPNPControl::NoUPNPDevicesFound
		}

		//find gateway (router)
		errno = 0;
		int retval = UPNP_GetValidIGD(devlist, &m_Urls, &m_Data, m_lanaddr, sizeof(m_lanaddr));
		if (retval != 1 && retval != 2)
		{
			//no valid IGDs found
			m_nInternalState = TR_UPNP_ERR;
			Log(0, "UPNP: GetValidIGD failed (%d): %s", errno, strerror( errno ) );
			Log(0, "UPNP: If your router supports UPnP, please make sure UPnP is enabled!");
			return;	//UPNPControl::NoUPNPDevicesFound
		}
		else
		{
			Log(0, "UPNP: Found Internet Gateway Device \"%s\"", m_Urls.controlURL );
			Log(0, "UPNP: Local Address is \"%s\"", m_lanaddr);
			m_nInternalState = TR_UPNP_IDLE;
			m_bHasDiscovered = 1;
		}

		freeUPNPDevlist(devlist);
		m_nInternalState = TR_UPNP_IDLE;	//done discovery
		m_nPort = -1;
	}
	else
		Log(0, "UPNP: disabled");
}

void NatUpnp::Uninit()
{
	if(m_bEnabled){
		if(m_bHasDiscovered){
			FreeUPNPUrls(&m_Urls); 
			m_bHasDiscovered = false;
		}
		m_nInternalState = TR_UPNP_DISCOVER;
	}
}

int NatUpnp::ForwardPort(int nPort)
{
	if(m_bEnabled)
	{
		Log(0, "UPNP: Start port forwarding");

		if(TR_UPNP_ERR == m_nInternalState){
			Log(0, "UPNP: Error state detected");
			m_nState = PFS_PORT_ERROR;
			return m_nState;
		}

		if(m_nInternalState == TR_UPNP_DISCOVER){
			Init();
		}

		if( m_nInternalState == TR_UPNP_IDLE ){
			if( m_bIsMapped && ( m_nPort != nPort ) ){
				Log(0, "UPNP: Will need to unmap existing port");
				m_nInternalState = TR_UPNP_UNMAP;
			}
		}

		if( m_nInternalState == TR_UPNP_UNMAP)
		{
			char portStr[8];
			char intPort[8];
			char intClient[16];
			int i;

			sprintf( portStr, "%d", m_nPort );
			i = UPNP_GetSpecificPortMappingEntry( m_Urls.controlURL,
												  m_Data.first.servicetype, portStr,
												  "TCP", intClient, intPort );
			if( i != UPNPCOMMAND_SUCCESS )
			{
				Log(0, "UPNP: Port %d isn't forwarded", m_nPort);
				m_bIsMapped = false;
			}
			else
				Log(0, "UPNP: Read existing port mapping entry");
		}

		if( m_nInternalState == TR_UPNP_UNMAP){
			Log(0, "UPNP: Start unmaping existing port");
			StopForwardPort(m_nPort);
		}

		if( m_nInternalState == TR_UPNP_IDLE ){
			if( !m_bIsMapped )
				m_nInternalState = TR_UPNP_MAP;
		}

		if( m_nInternalState == TR_UPNP_MAP )
		{
			Log(0, "UPNP: Start mapping process");

			int err = -1;
			errno = 0;

			if( !m_Urls.controlURL || !m_Data.first.servicetype ){
				m_bIsMapped = 0;
				Log(0, "UPNP: No IGD data");
			}
			else
			{
				Log(0, "UPNP: Try adding the port mapping");

				char portStr[16];
				char desc[64];
				sprintf( portStr, "%d", nPort );
				sprintf( desc, "%s at %d", TR_NAME, nPort );
				err = UPNP_AddPortMapping( m_Urls.controlURL,
										   m_Data.first.servicetype,
										   portStr, portStr, m_lanaddr,
										   desc, "TCP", NULL );
				m_bIsMapped = !err;
			}
			Log(0, "UPNP: Port forwarding through \"%s\", service \"%s\".  (local address: %s:%d)", m_Urls.controlURL, m_Data.first.servicetype, m_lanaddr, nPort);
			if( m_bIsMapped )
			{
				Log(0, "UPNP: Port forwarding successful");
				m_nPort = nPort;
				m_nInternalState = TR_UPNP_IDLE;
			}
			else
			{
				Log(0, "UPNP: Port forwarding failed with error %d (errno %d - %s)", err, errno, strerror( errno ));
				Log(0, "UPNP: If your router supports UPnP, please make sure UPnP is enabled!" );
				m_nPort = -1;
				m_nInternalState = TR_UPNP_ERR;
			}
		}
		else
			Log(0, "UPNP: Skip mapping new port (state: %d)\n", m_nInternalState);

		int ret;
		switch( m_nInternalState )
		{
			case TR_UPNP_DISCOVER:
				ret = PFS_PORT_UNMAPPED; break;

			case TR_UPNP_MAP:
				ret = PFS_PORT_MAPPING; break;

			case TR_UPNP_UNMAP:
				ret = PFS_PORT_UNMAPPING; break;

			case TR_UPNP_IDLE:
				ret = m_bIsMapped ? PFS_PORT_MAPPED : PFS_PORT_UNMAPPED; break;

			default:
				ret = PFS_PORT_ERROR; break;
		}

		m_nState = ret;
	}

	return m_nState;
}

void NatUpnp::StopForwardPort(int nPort)
{
	if(TR_UPNP_ERR == m_nInternalState){
		Log(0, "UPNP: detected error state when doing stop forwarding!");
		m_nState = PFS_PORT_ERROR;
		return;
	}

	if(m_Urls.controlURL[0] == '\0'){
		Log(0, "UPNP: init was not done!");
		return;
	}
	char port_str[16];
	sprintf(port_str, "%d", nPort);
	int retval = UPNP_DeletePortMapping(m_Urls.controlURL, m_Data.first.servicetype, port_str, "TCP", NULL);
	Log(0, "UPNP: Stopped port %d forwarding through \"%s\", service \"%s\"", nPort, m_Urls.controlURL, m_Data.first.servicetype );
	m_bIsMapped = 0;
	m_nInternalState = TR_UPNP_IDLE;
	m_nPort = -1;

	//return (retval==UPNPCOMMAND_SUCCESS);
}
