#ifndef NATTRAVERSAL_H
#define NATTRAVERSAL_H

#include "NatProtocol.h"
#include <QString>
#include <QTcpSocket>

class NATTraversal
{
public:
	NATTraversal();
	~NATTraversal();

	void RegisterLogger(FN_LOGGER pFn, unsigned long nData);
	void Log(int nMsg, const char *szFmt, ...);

	//call these before initialize
	void EnableUPnP(bool bEnable);
	void EnableNatPmp(bool bEnable);
	
	void Initialize();
	void Uninitialize();
	int  StartPortForwardRequest(int nPort);
	int  StopPortForwardRequest(int nPort);

	//info
	int	 GetGlobalStatus();	//"shared" status
	int	 GetGlobalIsEnabled();	//"shared" status

	void GetPublicIP(QString &strIP);

	static bool TestConnection(QHostAddress host, quint16 port, int timeout);

protected:
	void NatPulse(bool bCheckPort);

private:
	//logger
	FN_LOGGER	m_pFnLogger;
	unsigned long	m_nLogData;

	//supported protocols
	NatProtocol *m_pUpnp;
	NatProtocol *m_pNatpmp;
};

#endif // NATTRAVERSAL_H

