quazip dl: http://quazip.sourceforge.net/index.html

WIN32:
1. extract zlib and quazip
2. copy zlib.h, zconf.h and all content from contrib/minizp into quazip/quazip/
3. repair in quazipfile (272) to return false
4. copy win32/build.bat to quazip/quazip and execute
5. quazip.lib is generated, include in the project, include headers


WINCE:
1. extract zlib and quazip
2. copy zlib.h, zconf.h and all content from contrib/minizp into quazip/quazip/
3. repair in quazipfile (272) to return false
4. find errno.h and copy in quazip/quazip/
5. copy crypt.h patched and timex.h/c to quazip/quazip/
6. copy quazip.pro to quazip/quazip/
7. copy winCE/build.bat to quazip/quazip and execute -> manually use nmake debug & release
8. quazip.lib is generated, include in the project, include headers 
9. if quazip release is big then edit Makefile.Release and remove -GL option from all CFLAGS..



  



