//#ifndef timex_h
//#define timex_h

#include <winbase.h>
	// see link http://blogs.msdn.com/cenet/archive/2006/04/29/time-h-on-windows-ce.aspx
	// there is no time() funct from time.h on WINCE!!!!

	#ifndef time_t
		#define time_t unsigned long
	#endif

	time_t time_ex( time_t *inTT );

//#endif //timex_h

