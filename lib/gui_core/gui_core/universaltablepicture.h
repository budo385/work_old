#ifndef UNIVERSALTABLEPICTURE_H
#define UNIVERSALTABLEPICTURE_H

#include <QLabel>
#include "universaltablecelldwidget.h"


/*!
	\class  UniversalTablePicture
	\ingroup GUICore_UniversalTable
	\brief  Widget used in UniversalTableWidget

	Use:
	- when adding column to UniversalTableWidget, mark column type=COL_TYPE_PICTURE (see MVIEW_GRID_COLUMN_SETUP)
	- by default, column (inside  UniversalTableWidget datasource list) must contain QByteArray (binary) for picture
	- picture is view only
*/
class UniversalTablePicture : public QLabel, public TableCellWidget
{

public:
    UniversalTablePicture(QWidget *parent);
    ~UniversalTablePicture();

	void SetData(int nRow, int nCol,QVariant &Data,QString strDataSourceFormat="",bool bIsEditable=true); //called by UniversalTableWidget when widget is created
	void RefreshData(QVariant &Data,bool bCopyContent=false);	//called by UniversalTableWidget in refresh method when widget data is only refreshed (data -> screen)
	void SetEditMode(bool bEdit=true);	//called by UniversalTableWidget for disabling/enabling user interaction with child widget
   
};

#endif // UNIVERSALTABLEPICTURE_H


