#ifndef LOGINDIALOGABSTRACT_H
#define LOGINDIALOGABSTRACT_H

#include "trans/trans/httpclientconnectionsettings.h"
#include "db_core/db_core/dbconnsettings.h"
#include "common/common/connectionslistabstract.h"
#include "db_core/db_core/dbconnectionslist.h"
#include "trans/trans/networkconnections.h"
#include <QtWidgets/QDialog>

class LoginDialogAbstract : public QDialog
{

	Q_OBJECT

public:
	LoginDialogAbstract(QWidget *parent);
	~LoginDialogAbstract();
	
	enum LoginDialogs
	{
		LOGIN_DLG_DEFAULT,
		LOGIN_DLG_SIDEBAR,
		LOGIN_DLG_EMBEDDED,
		LOGIN_DLG_WEB_AUTHENTICATION
	};

	
	virtual void Initialize(QString strSettingsFile, int nMode=0,bool bThinClient=false);
	virtual void RefreshConnectionsList() = 0;
	virtual void SetDefaults(QString strDefaultUserName="", QString strDefaultConnectionName="") = 0;
	virtual void SetPasswordFocus(){};

	HTTPClientConnectionSettings GetNetCurrentConnection();
	DbConnectionSettings GetDbCurrentConnection();

	QString m_strUser;
	QString m_strPass;


signals:
	void OnLoginClick();
	void OnCloseClick();

protected:
	void					keyPressEvent(QKeyEvent* event);
	int						EditSettings(int nConnectionIdx);
	ConnectionsListAbstract *GetConnList();

	NetworkConnections		m_objDataNet;
	DbConnectionsList		m_objDataDb;
	int						m_nConnIdx;
	int						m_nMode;		//0 default, 1- short view
	bool					m_bThinClient;	
};

#endif // LOGINDIALOGABSTRACT_H
