#ifndef HTMLTAGHIGHLIGHTER_H
#define HTMLTAGHIGHLIGHTER_H

#include <QSyntaxHighlighter>

class HtmlTagHighlighter : public QSyntaxHighlighter
{
public:
	HtmlTagHighlighter(QObject *parent);
	~HtmlTagHighlighter();

protected:
	void highlightBlock(const QString &text);
private:
	
};

#endif // HTMLTAGHIGHLIGHTER_H
