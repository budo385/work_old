#include "timemessagebox.h"
#include <QTimerEvent>
#include <QVariant>

TimeMessageBox::~TimeMessageBox()
{

}


TimeMessageBox::TimeMessageBox(int nTimeOut,bool bDisplayTimeOut, Icon icon, const QString & title, const QString & text, StandardButtons buttons, QWidget * parent, Qt::WindowFlags f)
:QMessageBox(icon,title,text,buttons,parent,f)
{

	m_nTimerID=-1;
	m_nTimeOut=nTimeOut;
	m_bDisplayTimeOut=bDisplayTimeOut;


}

int TimeMessageBox::exec ()
{

	if (m_bDisplayTimeOut)
		m_nTimerID=startTimer(1000);
	else
		m_nTimerID=startTimer(m_nTimeOut*1000);
	
	return QMessageBox::exec();
}



void TimeMessageBox::timerEvent(QTimerEvent *event)
{
	if(event->timerId()==m_nTimerID)
	{
		if (m_bDisplayTimeOut)
		{
			m_nTimeOut--;
			if (m_nTimeOut<0)
			{
				killTimer(m_nTimerID);
				m_nTimerID=-1;
				accept(); //set as accepted->close dlg
			}
			QString strMsg=tr("Dialog will be closed in: ");
			strMsg+=QVariant(m_nTimeOut).toString();
			strMsg+=tr(" seconds.");
			setInformativeText(strMsg);
		}
		else
		{
			killTimer(m_nTimerID);
			m_nTimerID=-1;
			accept(); //set as accepted->close dlg
		}
	}
}