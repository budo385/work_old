#include "multisortcolumn.h"

MultiSortColumn::MultiSortColumn(QWidget *parent)
    : QDialog(parent)
{
	ui.setupUi(this);

	this->setWindowTitle(tr("Configure Sort Order"));//set dialog title


}

MultiSortColumn::~MultiSortColumn()
{

}



/*!
	Initializes multisort window with all columns that are available for
	multicolumn sort:

	\param lstColumns		- list in MVIEW_GRID_COLUMN_SETUP format
	\param lstDataSource	- data source list
	
*/
void MultiSortColumn::SetColumnSetup(DbRecordSet &lstColumns)
{
	ui.frame->SetColumnSetup(lstColumns);
}

void MultiSortColumn::SetColumnSetup(DbRecordSet &lstColumns, DbRecordSet &lstColumnsForSort)
{
	ui.frame->SetColumnSetup(lstColumns, lstColumnsForSort);
}

DbRecordSet MultiSortColumn::GetResultRecordSet()
{
	return ui.frame->GetResultRecordSet();
};

void MultiSortColumn::on_btnOK_clicked()
{
	//ui.tableWidget->GetListForSorting(m_lstSortCols);
	done(1);
}

void MultiSortColumn::on_btnClose_clicked()
{
	done(0);
}

