#ifndef GUIFIELD_RICHEDIT_H
#define GUIFIELD_RICHEDIT_H

#include "guifield.h"
#include "richeditwidget.h"

class GuiField_RichEdit : public GuiField
{
	Q_OBJECT

public:
	GuiField_RichEdit(QString strFieldName,QWidget *pWidget,GuiDataManipulator* pDataManager,DbView *TableView=NULL, QLabel *pLabel=NULL);

	void RefreshDisplay(); //DATA SOURCE -> UI
	void UsePlainText(){m_bIsPlainText=true;}  //interprets content as plain text
	void setEnabled(bool bEnable);

private:
	RichEditWidget * m_pEditWidget;

	//bool m_bIsByteArray;
	bool m_bIsPlainText;
public slots:
	void FieldChanged(); //UI -> DATA SOURCE
	
};

#endif // GUIFIELD_RICHEDIT_H
