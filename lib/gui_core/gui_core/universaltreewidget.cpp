#include "universaltreewidget.h"
#include <QMenu>
#include <QApplication>
#include <QDrag>
#include "common/common/datahelper.h"
#include "db_core/db_core/dbsqltableview.h"
#include "os_specific/os_specific/mime_types.h"
#include "gui_core/gui_core/universaltabledelegate.h"
#include "trans/trans/xmlutil.h"
#include "thememanager.h"

UniversalTreeWidget::UniversalTreeWidget(QWidget *parent)
: QTreeWidget(parent), DragDropInterface(this),m_nIdxExpandedCol(-1),m_nIconSourceIdx(-1),m_nDefaultCheckState(Qt::Unchecked)
{
	m_nDragMode=DRAG_MODE_ONLY_SELECTED;

	m_bItemsUserCheckable = false;
	connect(this, SIGNAL(itemChanged(QTreeWidgetItem *, int)), this, SLOT(on_itemChanged(QTreeWidgetItem *, int)));
	connect(this, SIGNAL(itemExpanded(QTreeWidgetItem *)), this, SLOT(on_itemExpanded(QTreeWidgetItem *)));
	connect(this, SIGNAL(itemCollapsed(QTreeWidgetItem *)), this, SLOT(on_itemCollapsed(QTreeWidgetItem *)));
	//setUniformRowHeights(true); //all rows same height: improves perfomance...
	//issue 1646:
	header()->setStretchLastSection(false);
	setColumnWidth(0,600);
	setIndentation(18); 
	setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
	setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
	m_nColToolTipSourceIdx=-1;
	setUniformRowHeights(true);
	
	setStyleSheet(ThemeManager::GetTreeBkg());
}

UniversalTreeWidget::~UniversalTreeWidget()
{
}

/*!
	Initializes table for display. Moved out from constructor to enable connecting to virtual slots!

	\param recData			- data source (default GuiDataManipulator will be used)
	\param bEnableDrop		- true to enable drop
	\param bEnableDrag		- true to enable drag
	\param bSingleSelection	- true to allow only single row select
	\param bSelectRows		- if false, then every item is selected separately
	\param nVerticalRowSize	- vertical size of row

*/
void UniversalTreeWidget::Initialize(DbRecordSet *recData, bool bEnableDrop/*=false*/, bool bEnableDrag/*=false*/, 
									 bool bSingleSelection/*=false*/,bool bSelectRows/*=true*/,bool bIgnoreDropHandler)
{

	//Set pointer to data and create widget items.
	m_recData = recData;
	m_bIgnoreDropHandler=bIgnoreDropHandler;

	//recData->Dump();

	//Map mandatory columns:
	QString strPK,strPrefix;
	int nType;
	recData->getColumn(0,nType,strPK);
	int nIdx=strPK.indexOf("_");
	Q_ASSERT(nIdx!=-1); 
	strPrefix=strPK.left(nIdx);
	m_lstColumnIdx<<recData->getColumnIdx(strPrefix + "_ID");
	m_lstColumnIdx<<recData->getColumnIdx(strPrefix + "_CODE");
	m_lstColumnIdx<<recData->getColumnIdx(strPrefix + "_LEVEL");
	m_lstColumnIdx<<recData->getColumnIdx(strPrefix + "_PARENT");
	m_lstColumnIdx<<recData->getColumnIdx(strPrefix + "_HASCHILDREN");
	m_lstColumnIdx<<recData->getColumnIdx(strPrefix + "_ICON");
	m_lstColumnIdx<<recData->getColumnIdx(strPrefix + "_MAINPARENT_ID");
	m_lstColumnIdx<<recData->getColumnIdx(strPrefix + "_NAME");
	m_lstColumnIdx<<recData->getColumnIdx(strPrefix + "_STYLE");

	//qDebug()<<m_lstColumnIdx;


	connect(this, SIGNAL(itemSelectionChanged()),					this, SLOT(SelectionChanged()));

	//selection mode:
	if(bSelectRows) 
		this->setSelectionBehavior( QAbstractItemView::SelectRows);
	else
		this->setSelectionBehavior( QAbstractItemView::SelectItems);

	//if true then only one row can be selected at time:
	if(bSingleSelection) 
		this->setSelectionMode(QAbstractItemView::SingleSelection);
	else
		this->setSelectionMode(QAbstractItemView::ExtendedSelection);
	
	//Drag & Drop:
	setAcceptDrops(bEnableDrop);
	setDragEnabled(bEnableDrag);
	if(bEnableDrop) setDropIndicatorShown(true);

	//create context menu:
	CreateContextMenuActions(m_lstActions);

	//set all actions to false:
	SetEditMode(m_bEdit);


	//issue 1649
	DummyNoFocusDelegate* DeafultTableDelegate = new DummyNoFocusDelegate(this);
	setItemDelegate (DeafultTableDelegate);
}


/*!
	Constructs header, must be called for anything to see on tree. just call SetHeader(); for most usual ops (no header, 1 col).

	\param bHideHeader		- hide header
	\param nColumnCount		- no of cols to display
	\param lstColLabels		- list of labels 
	\param lstColWidths		- list of widths

*/
void UniversalTreeWidget::SetHeader(bool bHideHeader, int nColumnCount, const QStringList *lstColLabels, QList<int> *lstColWidths)
{
	//m_bHideHeader	= bHideHeader;
	//m_nColumnCount	= nColumnCount;
	//m_lstColLabels	= lstColLabels;
	//m_lstColWidths	= lstColWidths;

	setColumnCount(nColumnCount);
	QHeaderView *headerthis = header();
	headerthis->setSectionResizeMode(0, QHeaderView::ResizeToContents);
	//ui.treeView->header()->setSectionResizeMode(QHeaderView::ResizeToContents);
	if (bHideHeader)
	{
		header()->hide();
		return;
	}

	//set labels:
	
	if(lstColLabels!=NULL)
	{
		QStringList strList=*lstColLabels;
		//setHeaderLabels(strList);
		setHeaderLabels(*lstColLabels);
	}

	//set col width:
	if(lstColWidths!=NULL)
	{
		QHeaderView *header;
		header=this->header();

		int nSize=lstColWidths->size();
		for(int i=0;i<nSize;++i)
		{
			header->resizeSection(i,lstColWidths->at(i));
		}
	}

}









/*!
	Based on data source, refreshes contents of displayed items.
	Only can be called after:
	- edit node (name)
	- add node
	if:
	- edit code or delete, you must refresh whole tree!
*/
void UniversalTreeWidget::RefreshDisplay(int RowID /*= -1*/)
{

	setUpdatesEnabled(false);

//	blockSignals(true); //block all signals

	//Refresh whole display.
	if(RowID == -1)
	{
		//_START_TIMER(hash);
		m_hshRowIDtoItem.clear();			//hash clear is way too slow!
		m_hshItemRowIDtoCheckState.clear();
		//_STOP_TIMER(hash);
		//_START_TIMER(tree);
		clear();
		emit RebuildingTree();
		//_STOP_TIMER(tree);
		//_START_TIMER(build);
		CreateItemsFromRecordSet();
		//_STOP_TIMER(build);
	}
	//Refresh only selected row ID.
	else
	{
		int row = m_recData->find(m_lstColumnIdx.at(COL_ID), RowID, true);
		if (row!=-1)
			CreateItemsFromRecordSet(row);
	}


	setUpdatesEnabled(true);
//	blockSignals(false); //enable signals

	FixScrolling();
}


//creates or refreshes data from recordset on tree
void UniversalTreeWidget::CreateItemsFromRecordSet(int nRowStart, QTreeWidgetItem *ParentItem)
{
	/*
	QFont strStyle1("Arial",10,QFont::Bold);
	QFont strStyle2("Arial",10,QFont::DemiBold,true);
	QFont strStyle3("Arial",9,QFont::DemiBold,true);
	QFont strStyle4("Arial",8,QFont::DemiBold,true);
	QFont strStyle5("Arial",7,QFont::DemiBold,true);
	strStyle1.setStyleStrategy(QFont::PreferAntialias);
	strStyle2.setStyleStrategy(QFont::PreferAntialias);
	strStyle3.setStyleStrategy(QFont::PreferAntialias);
	//Issue #2009.
	//if(QSysInfo::windowsVersion()==QSysInfo::WV_VISTA)
	//{
		strStyle4.setStyleStrategy(QFont::PreferAntialias);
		strStyle5.setStyleStrategy(QFont::PreferAntialias);
	//}
	*/
	QSize sizeCol(600,16);
	
	int nSize=m_recData->getRowCount();
	if (nRowStart==-1)
		nRowStart=0;
	else
		nSize=nRowStart+1;
	
	for(int i=nRowStart;i<nSize;++i)
	{
		//create item or refresh display:
		QTreeWidgetItem *Item;
		int nRowID = m_recData->getDataRef(i, m_lstColumnIdx.at(COL_ID)).toInt();
		if (!m_hshRowIDtoItem.contains(nRowID))
		{
			Item = new QTreeWidgetItem;
			m_hshRowIDtoItem.insert(nRowID, Item);
		}
		else
			Item = m_hshRowIDtoItem.value(nRowID);
		QString strCode = m_recData->getDataRef(i, m_lstColumnIdx.at(COL_CODE)).toString();
		QString strName = m_recData->getDataRef(i, m_lstColumnIdx.at(COL_NAME)).toString();
		QString strDisplayData = strCode + " " + strName;
		Item->setData(0, Qt::UserRole,		nRowID);		//RowID.
		Item->setData(0, Qt::DisplayRole,	strDisplayData);//Display Code + Name.

		if (m_nIconSourceIdx>=0)
		{
			QByteArray bytePicture=m_recData->getDataRef(i,m_nIconSourceIdx).toByteArray();
			if (bytePicture.size()>0)
			{
				QPixmap pixMap;
				pixMap.loadFromData(bytePicture);
				Item->setIcon(0, QIcon(pixMap));
			}
		}
		else
		{
			QString strIcon = m_recData->getDataRef(i, m_lstColumnIdx.at(COL_ICON)).toString();
			if (!strIcon.isEmpty())
				Item->setIcon(0, CreateIcon(strIcon));				//Set icon.
		}


		//select if needed:
		if (m_recData->isRowSelected(i))
		{
			Item->setSelected(true);
		}

		//BT: issue 1646: add different fonts for diff levels:
		/*
		Level 1: Arial 10pt bold
		Level 2: Arial 10pt regular
		Level 3: Arial 9pt regular
		Level 4: Arial 8pt regular
		Level 5...: Arial 7pt regular
		*/
		int nLevel = m_recData->getDataRef(i, m_lstColumnIdx.at(COL_LEVEL)).toInt();
		/*
		switch(nLevel)
		{
		case 0:
			Item->setFont(0,strStyle1);
			break;
		case 1:
			Item->setFont(0,strStyle2);
			break;
		case 2:
			Item->setFont(0,strStyle3);
		    break;
		case 3:
			Item->setFont(0,strStyle4);
		    break;
		default:
			Item->setFont(0,strStyle5);
		    break;
		}
		*/
		Item->setSizeHint(0,sizeCol);
		//issue 1661: set node name on tooltip:
		if (m_nColToolTipSourceIdx<0)
			Item->setToolTip(0,strDisplayData);
		else
			Item->setToolTip(0,m_recData->getDataRef(i, m_nColToolTipSourceIdx).toString());

		//if parent null, try to find it:
		if (!ParentItem)
		{
			int nParentID=m_recData->getDataRef(i, m_lstColumnIdx.at(COL_PARENT)).toInt();
			if (nParentID>0)
			{
				ParentItem=m_hshRowIDtoItem.value(nParentID,NULL);
			}
		}
		//actually add item to tree:
		if (!ParentItem) 
			addTopLevelItem(Item);
		else
			ParentItem->addChild(Item);

		//If item is user checkable - add flag.
		if (m_bItemsUserCheckable)
		{
			blockSignals(true);
			Qt::ItemFlags defaulfFlags = Item->flags();
			Item->setFlags(defaulfFlags | Qt::ItemIsUserCheckable);
			Item->setCheckState(0, (Qt::CheckState)m_nDefaultCheckState);
			m_hshItemRowIDtoCheckState.insert(nRowID, m_nDefaultCheckState);
			blockSignals(false);
		}

		//Expand it if needed.
		//int nCoumnIdx = m_recData->getColumnIdx("BUSP_EXPANDED");
		if (m_nIdxExpandedCol >= 0)
		{
			bool bExpanded = m_recData->getDataRef(i, m_nIdxExpandedCol).toBool();
			Item->setExpanded(bExpanded);
		}

		ParentItem=NULL;
	}
}

void UniversalTreeWidget::SelectionChanged()
{
	//Selected items list.
	//m_hshSelectedState.clear();
	QList<QTreeWidgetItem*> items(selectedItems());
	m_recData->clearSelection();

	QListIterator<QTreeWidgetItem*> iter(items);
	while (iter.hasNext())
	{
		QTreeWidgetItem *item = iter.next();
		int RowID = item->data(0, Qt::UserRole).toInt();
		SelectInRecordSetByNodeID(RowID);
	}

	QCoreApplication::processEvents(); //issue 1653: color row then fire signal...
	emit SignalSelectionChanged();	//notify others
}



//does not touch previous selection, selects all children below parent with RowID including parent itself
//all children have parent code start
void UniversalTreeWidget::SelectInRecordSetByNodeID(int nNodeID)
{
	int nRow=m_recData->find(m_lstColumnIdx.at(COL_ID),nNodeID,true); //this does not select/deselect any
	//qDebug()<<m_recData->getSelectedCount();
	if (nRow!=-1)
	{
		m_recData->selectRow(nRow);
		QString strParentCode=m_recData->getDataRef(nRow,m_lstColumnIdx.at(COL_CODE)).toString();
		int nCodeSize=strParentCode.size();
		int nSize=m_recData->getRowCount();
		for(int i=nRow+1;i<nSize;++i)
		{
			QString strChildCode=m_recData->getDataRef(i,m_lstColumnIdx.at(COL_CODE)).toString().left(nCodeSize);
			if (strChildCode!=strParentCode) //M.R. told that left is faster then indexOf
				break;
			m_recData->selectRow(i);
		}
	}
}

void UniversalTreeWidget::SelectItemByRowID(int nRowID)
{
	QTreeWidgetItem *item = m_hshRowIDtoItem.value(nRowID,NULL);
	if (!item) return;

	//select it (preserve previous selection):
	SelectInRecordSetByNodeID(nRowID); //issue 1009
	//m_recData->find(m_lstColumnIdx.at(COL_ID), nRowID); //select it, deselect others
	blockSignals(true);
	item->setSelected(true);
	blockSignals(false);
}

void UniversalTreeWidget::ExpandItemByRowID(int nRowID, bool bFixScrolling /*= true*/)
{
	QTreeWidgetItem *item = m_hshRowIDtoItem.value(nRowID,NULL);
	if (!item) return;
	setCurrentItem(item); //moves focus to the item and expands all parents to it!!! discovered by BT :)

	if (bFixScrolling)
		FixScrolling();
}

void UniversalTreeWidget::ExpandItemByRowIDFromRecordSet(DbRecordSet &recData)
{
	setUpdatesEnabled(false);
	int nRowCount = recData.getRowCount();
	for (int i = 0; i < nRowCount; i++)
	{
		int nRowID = recData.getDataRef(i, m_lstColumnIdx.at(COL_ID)).toInt();
		ExpandItemByRowID(nRowID, false);
	}
	setUpdatesEnabled(true);
	update();
}

void UniversalTreeWidget::ExpandTree()
{

	//iterate through all items in hash of items and expand them: (? does we affect state?)
	blockSignals(true); //disable

	QApplication::setOverrideCursor(Qt::WaitCursor);

	QHashIterator<int, QTreeWidgetItem*> i(m_hshRowIDtoItem);
	while (i.hasNext()) 
	{
		i.next();
		i.value()->setExpanded(true);
	}

	QApplication::restoreOverrideCursor();
	
	blockSignals(false); //disable

	FixScrolling();
}

void UniversalTreeWidget::CollapseSelectedItems()
{
	QList<QTreeWidgetItem*> items(selectedItems());
	QListIterator<QTreeWidgetItem*> iter(items);
	while (iter.hasNext())
		collapseItem(iter.next());
}

void UniversalTreeWidget::ExpandSelectedItems()
{
	QList<QTreeWidgetItem*> items(selectedItems());
	QListIterator<QTreeWidgetItem*> iter(items);
	while (iter.hasNext())
		expandItem(iter.next());
}


QIcon UniversalTreeWidget::CreateIcon(QString IconString)
{
	if (IconString.isEmpty())
		return QIcon();

	//First separate icons.
	QStringList IconList = IconString.split(";");
	//Get Icon width.
	int IconWidth = IconList.count()*16;
	//If we have max icon size store it for later (drawing of tree view).
	if (IconWidth > m_sizeMaxIconSize.width())
		m_sizeMaxIconSize.setWidth(IconWidth);

	//TODO - add icon handling.
	return QIcon();
}


void UniversalTreeWidget::dropEvent(QDropEvent *event)
{
	if(!m_bEdit)
		return;
	
	DragDropInterface::dropEvent(event);
}


//why not merge entity data then refresh display?
void UniversalTreeWidget::DropHandler(QModelIndex index, int nDropType, DbRecordSet &DroppedValue,QDropEvent *event)
{
	//If not ignoring drop handler.
	if (!m_bIgnoreDropHandler)
	{
		//Check is there already an item with same RowID - it can not happen (at least for now).
		QList<int> ExistingRowList, DroppedRowIDList, DroppedRowList;
		DroppedValue.clearSelection();
		int count = DroppedValue.getRowCount();
		for (int i = 0; i < count; ++i)
		{
			int RowID = DroppedValue.getDataRef(i, 0).toInt();
			if (m_hshRowIDtoItem.contains(RowID))
				DroppedValue.selectRow(i);
		}
		DroppedValue.deleteSelectedRows();
		if (!DroppedValue.getRowCount())
			return;

		//copy data:
		if (!m_recData->getRowCount())
			m_recData->copyDefinition(DroppedValue);
		m_recData->merge(DroppedValue);
		SortList();
		RefreshDisplay();
		emit DataDropedAndProcessed();
	}
	else
		emit DataDroped(index, nDropType, DroppedValue, event);
}

void UniversalTreeWidget::dragEnterEvent ( QDragEnterEvent *event )
{
	if(!m_bEdit)
	{
		event->ignore();
		return;
	}
	
	DragDropInterface::dragEnterEvent(event);
}


//Record set always contains selected parent & all childrens
//To speed up copying whole recordset on big tree's, use only parent or access recordset directly

void UniversalTreeWidget::GetDropValue(DbRecordSet &values, bool bSelectItemChildren) const
{
	values.copyDefinition(*m_recData);
	if (bSelectItemChildren)
	{
		//select items always contain children:
		values.merge(*m_recData,true);
	}
	else
	{
		//get only actually selected
		values.merge(*m_recData,true);
		GetOnlyActuallySelectedNodes(values);
		//qDebug()<<values.getRowCount();

		/*
		int nFirstSelected=m_recData->getSelectedRow();
		if (nFirstSelected>=0)
		{
			values.merge(m_recData->getRow(nFirstSelected));
		}
		*/
	}

}

void UniversalTreeWidget::contextMenuEvent(QContextMenuEvent *event)
{
	QMenu ContextMenu(this);

	int nSize=m_lstActions.size();
	for(int i=0;i<nSize;++i)
		ContextMenu.addAction(m_lstActions.at(i));

	ContextMenu.exec(event->globalPos());
}

void UniversalTreeWidget::CreateContextMenuActions(QList<QAction*>& lstActions)
{
	QAction *action = new QAction(tr("&Expand Tree"), this);
	//connect(action, SIGNAL(triggered()), this, SLOT(ExpandSelectedItems()));
	connect(action, SIGNAL(triggered()), this, SLOT(ExpandTree()));
	m_lstActions.append(action);
	addAction(action);

	action = new QAction(tr("&Collapse"), this);
	connect(action, SIGNAL(triggered()), this, SLOT(CollapseSelectedItems()));
	m_lstActions.append(action);
	addAction(action);
}


void UniversalTreeWidget::SortList()
{
	m_recData->sort(m_lstColumnIdx.at(COL_CODE));
	
}

//If want items to be checkable call this with true BEFORE Initialize() method.
void UniversalTreeWidget::SetItemsUserCheckable(bool bCheckable, int nDefaultCheckState)
{
	m_bItemsUserCheckable = bCheckable;	
	m_nDefaultCheckState = nDefaultCheckState;
}

//Call to check items, call to after Initialize() - recCheckedItems is defined in MVIEW_CE_TYPES_FILTER_CHECKED_ITEMS
void UniversalTreeWidget::SetItemsChecked(DbRecordSet &recCheckedItems, bool bChecked /*= false*/)
{
	foreach(QTreeWidgetItem *item, m_hshRowIDtoItem)
	{
		if (!bChecked)
			item->setCheckState(0, Qt::Unchecked);
		else
			item->setCheckState(0, Qt::Checked);
	}

	int nRowCount = recCheckedItems.getRowCount();
	if (nRowCount)
	{
		for (int i = 0; i < nRowCount; ++i)
		{
			int nRowID = recCheckedItems.getDataRef(i, "CHECKED_ITEM_ID").toInt();
			if (!m_hshRowIDtoItem.contains(nRowID))
				continue;

			m_hshRowIDtoItem.value(nRowID)->setCheckState(0, (Qt::CheckState)recCheckedItems.getDataRef(i, "CHECKED_ITEM_STATE").toInt());
		}
	}
}

void UniversalTreeWidget::GetCheckedItems(DbRecordSet &recCheckedItems,bool bOnlyChecked)
{
	recCheckedItems.destroy();
	recCheckedItems.defineFromView(DbSqlTableView::getView(DbSqlTableView::MVIEW_CE_TYPES_FILTER_CHECKED_ITEMS));
	int nRowNumber = 0;
	foreach(QTreeWidgetItem* item, m_hshRowIDtoItem)
	{
		if (bOnlyChecked && item->checkState(0)!=Qt::Checked)
			continue;

		recCheckedItems.addRow();
		recCheckedItems.setData(nRowNumber, "CHECKED_ITEM_ID", item->data(0, Qt::UserRole).toInt());
		recCheckedItems.setData(nRowNumber, "CHECKED_ITEM_STATE", item->checkState(0));
		nRowNumber++;
	}
}

//Set child items checked or unchecked.
void UniversalTreeWidget::SetChildItemsCheckedState(QTreeWidgetItem *item, Qt::CheckState nCheckState, QList<QTreeWidgetItem*> &lstChangedCheckStateItems)
{
	int nChildItemsCount = item->childCount();
	for(int i =	0; i < nChildItemsCount; ++i)
	{
		QTreeWidgetItem *childItem = item->child(i);
		int nChildItemRowID = childItem->data(0, Qt::UserRole).toInt();
		
		if (!(m_hshItemRowIDtoCheckState.value(nChildItemRowID) == nCheckState))
		{
			childItem->setCheckState(0, nCheckState);
			//Insert new check state in check state list.
			m_hshItemRowIDtoCheckState.insert(nChildItemRowID, nCheckState);
			//Add this child to changed check state list.
			lstChangedCheckStateItems << childItem;
		}
		
		//Check child items children too.
		if (childItem->childCount() > 0)
			SetChildItemsCheckedState(childItem, nCheckState, lstChangedCheckStateItems);
	}
}

void UniversalTreeWidget::on_itemExpanded(QTreeWidgetItem *item)
{
	//Set item is expanded in recordset if that column exists.
	//int nCoumnIdx = m_recData->getColumnIdx("BUSP_EXPANDED");
	if (m_nIdxExpandedCol >= 0)
	{
		int nRowID = item->data(0, Qt::UserRole).toInt();
		int nRow = m_recData->find(m_lstColumnIdx.at(COL_ID), nRowID, true);
		m_recData->setData(nRow, m_nIdxExpandedCol, 1);
	}

	//Fix scrolling.
	FixScrolling();
}

void UniversalTreeWidget::on_itemCollapsed(QTreeWidgetItem *item)
{
	//Set item is colapsed in recordset if that column exists.
	//int nCoumnIdx = m_recData->getColumnIdx("BUSP_EXPANDED");
	if (m_nIdxExpandedCol >= 0)
	{
		int nRowID = item->data(0, Qt::UserRole).toInt();
		int nRow = m_recData->find(m_lstColumnIdx.at(COL_ID), nRowID, true);
		m_recData->setData(nRow, m_nIdxExpandedCol, 0);
	}
}

//For now used only in communication grid filter window for recursively checking an unchecking check boxes on items.
void UniversalTreeWidget::on_itemChanged(QTreeWidgetItem *item, int column)
{
	if (!m_bItemsUserCheckable)
		return;
	
	int nCheckState = item->checkState(0);
	int nRowID = item->data(0, Qt::UserRole).toInt();
	//Check is check state changed - if not return.
	if (nCheckState == m_hshItemRowIDtoCheckState.value(nRowID)){
		return;
	}
	else
	{
		QList<QTreeWidgetItem*>	lstChangedCheckStateItems;//< List of changed check state items.
		m_hshItemRowIDtoCheckState.insert(nRowID, nCheckState);
		
		lstChangedCheckStateItems << item;
		//If changed check (or uncheck) it's children too.
		this->blockSignals(true);
		SetChildItemsCheckedState(item, (Qt::CheckState)nCheckState, lstChangedCheckStateItems);
		this->blockSignals(false);

		emit ItemCheckStateChanged(lstChangedCheckStateItems);
		lstChangedCheckStateItems.count();
	}
}

//get item pointer by rowID, NULL if not exists
QTreeWidgetItem *UniversalTreeWidget::GetItemByRowID(int nRowID)
{
	return m_hshRowIDtoItem.value(nRowID,NULL);
}

//row ID from item, if not found : -1
int UniversalTreeWidget::GetRowIDByItem(QTreeWidgetItem *item)
{
	if (item)
		return item->data(0,Qt::UserRole).toInt();
	else
		return -1;
}

void UniversalTreeWidget::GetOnlyActuallySelectedNodes(DbRecordSet &lstSelectedItems) const
{
	//B.T: issue 815: do not include childrens in drag operations->only give selected ones:
	int nSize=lstSelectedItems.getRowCount();
	lstSelectedItems.clearSelection();
	for(int i=0;i<nSize;++i)
	{
		QTreeWidgetItem *item = m_hshRowIDtoItem.value(lstSelectedItems.getDataRef(i,m_lstColumnIdx.at(COL_ID)).toInt(),NULL);
		if (!item) 
			continue;

		if (item->isSelected())
			lstSelectedItems.selectRow(i);
	}
	lstSelectedItems.deleteUnSelectedRows();
}


//when drag: send selected items + type
QMimeData * UniversalTreeWidget::mimeData( const QList<QTreeWidgetItem *> items ) const
{
	QMimeData *mimeData=QTreeWidget::mimeData(items);
	if (mimeData)
	{
		DbRecordSet lstSelectedItems;
		GetDropValue(lstSelectedItems,true); 

		if (m_nDragMode==DRAG_MODE_ONLY_SELECTED)
		{
			//B.T: issue 815: do not include childrens in drag operations->only give selected ones:
			GetOnlyActuallySelectedNodes(lstSelectedItems);
		}
		else if (m_nDragMode==DRAG_MODE_ALL_CHILDREN_IF_COLLPASED)
		{
			QTreeWidgetItem *item =currentItem();
			if (item)
			{
				if (item->isExpanded())
					GetDropValue(lstSelectedItems); //only root node if expanded, else as is!!
			}
		}
		//if not both all all children is default



		if (lstSelectedItems.getRowCount()>0 && GetDropType()!=-1)
		{
			QByteArray byteListData=XmlUtil::ConvertRecordSet2ByteArray_Fast(lstSelectedItems);
			mimeData->setData(SOKRATES_MIME_LIST,byteListData);
			mimeData->setData(SOKRATES_MIME_DROP_TYPE,QVariant(GetDropType()).toString().toLatin1()); //pass unique type
		}
	}

	return mimeData;
}

//add custom mime types: selected list items + drop type
QStringList UniversalTreeWidget::mimeTypes() const
{
	QStringList lstDefault=QTreeWidget::mimeTypes();
	lstDefault<<SOKRATES_MIME_LIST;
	lstDefault<<SOKRATES_MIME_DROP_TYPE;

	return lstDefault;
}



/*!
Starts a drag by calling drag->start() using the given \a supportedActions.
*/
void UniversalTreeWidget::startDrag(Qt::DropActions supportedActions)
{
	//QTableWidget::startDrag(supportedActions);
	//return;

	//Q_D(QAbstractItemView);
	QModelIndexList indexes = selectedIndexes();
	if (indexes.count() > 0) 
	{
		QMimeData *data = model()->mimeData(indexes);
		if (!data)
			return;
		QRect rect;
		QPixmap pixmap(m_strDragIcon); //= d->renderToPixmap(indexes, &rect);
		QDrag *drag = new QDrag(this);
		drag->setPixmap(pixmap);
		drag->setMimeData(data);
		drag->setHotSpot(QPoint(-10,0)); //drag->pixmap().width()/2,0drag->pixmap().height()));

		if (drag->start(supportedActions) == Qt::MoveAction)
		{
			//------------------B.T. copied form void QAbstractItemViewPrivate::clearOrRemove()
			// we can't remove the rows so reset the items (i.e. the view is like a table)
			QModelIndexList list = selectedIndexes();
			for (int i=0; i < list.size(); ++i) 
			{
				QModelIndex index = list.at(i);
				QMap<int, QVariant> roles = model()->itemData(index);
				for (QMap<int, QVariant>::Iterator it = roles.begin(); it != roles.end(); ++it)
					it.value() = QVariant();
				model()->setItemData(index, roles);
			}
			//------------------B.T. copied form void QAbstractItemViewPrivate::clearOrRemove()
		}

	}
}

void UniversalTreeWidget::FixScrolling()
{
	//fix to enable proper horizontal scroll bar
	//header()->setStretchLastSection(false);
	header()->resizeSections(QHeaderView::ResizeToContents);
}
