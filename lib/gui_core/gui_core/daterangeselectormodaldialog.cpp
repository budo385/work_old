#include "daterangeselectormodaldialog.h"

#include "gui_core/gui_core/thememanager.h"
#include "gui_core/gui_core/gui_helper.h"
#include "common/common/datahelper.h"

#include <QHBoxLayout>
#include "daterangeselectordialog.h"

DateRangeSelectorModalDialog::DateRangeSelectorModalDialog(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);

	m_bFromCombo	 = false;
	m_bFromCalendar	 = false;

	//Set geometry.
	m_bSideBarMode = false;
	m_pSpacer = new QSpacerItem(0, 0, QSizePolicy::Expanding);

	ui.dateFrom->setReadOnly(true);
	ui.dateTo->setReadOnly(true);

	if (ThemeManager::GetViewMode() == ThemeManager::VIEW_SIDEBAR)
		m_bSideBarMode = true;

	if (m_bSideBarMode)
	{
		int nHeight = 18;
		ui.label_4->setMaximumHeight(nHeight);
		ui.dateFrom->setMaximumHeight(nHeight);
		ui.label_5->setMaximumHeight(nHeight);
		ui.dateTo->setMaximumHeight(nHeight);
		ui.cboRangeSize->setMaximumHeight(nHeight);
		ui.btnCurDateRange->setMaximumHeight(22);
		ui.btnPrev->setMaximumHeight(22);
		ui.btnNext->setMaximumHeight(22);

		setMinimumWidth(270);
		setMaximumWidth(270);
		setMinimumHeight(50);
		QHBoxLayout *horLayout = new QHBoxLayout();
		QGridLayout *gridLayout = new QGridLayout();
		horLayout->setSpacing(0);
		horLayout->setContentsMargins( 0, 0, 0, 0);
		gridLayout->setHorizontalSpacing(4);
		gridLayout->setVerticalSpacing(2);
		gridLayout->setContentsMargins( 0, 0, 0, 0);
		gridLayout->addWidget(ui.label_4, 0, 0);
		gridLayout->setAlignment(ui.label_4, Qt::AlignRight);
		gridLayout->addWidget(ui.dateFrom, 0, 1);
		gridLayout->addWidget(ui.label_5, 1, 0);
		gridLayout->setAlignment(ui.label_5, Qt::AlignRight);
		gridLayout->addWidget(ui.dateTo, 1, 1);
		horLayout->addItem(gridLayout);
		horLayout->addSpacing(10);
		horLayout->addWidget(ui.btnModalSelector);
		horLayout->addSpacing(10);
		horLayout->addWidget(ui.btnCurDateRange);
		horLayout->setStretchFactor(ui.btnCurDateRange, 0);
		horLayout->addWidget(ui.btnPrev);
		horLayout->setStretchFactor(ui.btnPrev, 0);
		horLayout->addWidget(ui.cboRangeSize);
		horLayout->setStretchFactor(ui.cboRangeSize, 5);
		//horLayout->setAlignment(ui.cboRangeSize, Qt::AlignHCenter);
		horLayout->addWidget(ui.btnNext);
		horLayout->setStretchFactor(ui.btnNext, 0);
		m_pSpacer->changeSize(1000,10, QSizePolicy::Expanding);
		horLayout->addSpacerItem(m_pSpacer);
		//horLayout->addStretch(1000);
		setLayout(horLayout);
	}
	else
	{
		QHBoxLayout *horLayout = new QHBoxLayout();
		horLayout->setSpacing(4);
		horLayout->addWidget(ui.label_4);
		horLayout->addWidget(ui.dateFrom);
		horLayout->addWidget(ui.label_5);
		horLayout->addWidget(ui.dateTo);
		horLayout->addWidget(ui.btnModalSelector);
		horLayout->addWidget(ui.btnCurDateRange);
		horLayout->addWidget(ui.btnPrev);
		horLayout->addWidget(ui.cboRangeSize);
		horLayout->addWidget(ui.btnNext);
		horLayout->setContentsMargins( 0, 0, 0, 0);
		m_pSpacer->changeSize(0,0, QSizePolicy::Expanding);
		horLayout->addSpacerItem(m_pSpacer);
		setLayout(horLayout);
	}

	//Set icons.
	ui.btnCurDateRange->setText("");
	ui.btnPrev->setText("");
	ui.btnNext->setText("");
	ui.btnModalSelector->setText("");
	ui.btnModalSelector->setIcon(QIcon(":Lupe24.png"));
	ui.btnCurDateRange->setIcon(QIcon(":2downarrow.png"));
	ui.btnPrev->setIcon(QIcon(":1leftarrow.png"));
	ui.btnNext->setIcon(QIcon(":1rightarrow.png"));
	ui.btnCurDateRange->setFlat(true);
	ui.btnPrev->setFlat(true);
	ui.btnNext->setFlat(true);
	ui.btnModalSelector->setFlat(true);

	//Setup range combo.
	GUI_Helper::GetDateRangeComboItems(ui.cboRangeSize);

	ui.dateFrom->setDate(QDate::currentDate());
	ui.dateTo->setDate(QDate::currentDate());

	connect(ui.cboRangeSize, SIGNAL(currentIndexChanged(int)), this, SLOT(OnComboChanged(int)));
}

//issue 2723-> remove year only from calendar..
void DateRangeSelectorModalDialog::DisableDateRange(int nDateRangeID)
{

	int nIdx=ui.cboRangeSize->findData(DataHelper::YEAR);
	if (nIdx>=0)
	{
		ui.cboRangeSize->removeItem(nIdx);
	}
}

DateRangeSelectorModalDialog::~DateRangeSelectorModalDialog()
{

}

void DateRangeSelectorModalDialog::Initialize(QDateTime &dateFrom, QDateTime &dateTo, int nRange, bool bFromCalendar /*= false*/)
{
	setupDateWidgets(dateFrom, dateTo, nRange);

	//When in calendar be a little wider.
	m_bFromCalendar = bFromCalendar;
	if (m_bFromCalendar)
	{
		ui.cboRangeSize->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
		m_pSpacer->changeSize(10,10, QSizePolicy::Expanding);
		setMinimumWidth(450);
		setMaximumWidth(450);
	}
}

void DateRangeSelectorModalDialog::setupDateWidgets(QDateTime &dateFrom, QDateTime &dateTo, int nRange)
{
	ui.dateFrom->blockSignals(true);
	ui.dateTo->blockSignals(true);
	ui.cboRangeSize->blockSignals(true);

	int nIndex = ui.cboRangeSize->findData(nRange);
	int nRangeIdx = ui.cboRangeSize->itemData(nIndex).toInt();

	ui.cboRangeSize->setCurrentIndex(nIndex);
	ui.dateFrom->setDate(dateFrom.date());
	ui.dateTo->setDate(dateTo.date());

	ui.dateFrom->blockSignals(false);
	ui.dateTo->blockSignals(false);
	ui.cboRangeSize->blockSignals(false);
}

void DateRangeSelectorModalDialog::on_btnPrev_clicked()
{
	QDate dateFrom	= ui.dateFrom->date();
	QDate dateTo	= ui.dateTo->date();
	int nIndex = ui.cboRangeSize->currentIndex();
	int nRangeIdx = ui.cboRangeSize->itemData(nIndex).toInt();

	DataHelper::DateRangeSelectorPreviousNextButtonClicked(dateFrom, dateTo, nRangeIdx, -1);

	ui.dateFrom->blockSignals(true);
	ui.dateTo->blockSignals(true);
	ui.dateFrom->setDate(dateFrom);
	ui.dateTo->setDate(dateTo);
	ui.dateFrom->blockSignals(false);
	ui.dateTo->blockSignals(false);

	QDateTime dtFrom = QDateTime(dateFrom, QTime(0,0,0));
	QDateTime dtTo = QDateTime(dateTo, QTime(23,59,59));

	emit onDateChanged(dtFrom, dtTo, nRangeIdx, false);
}

void DateRangeSelectorModalDialog::on_btnNext_clicked()
{
	QDate dateFrom	= ui.dateFrom->date();
	QDate dateTo	= ui.dateTo->date();
	int nIndex = ui.cboRangeSize->currentIndex();
	int nRangeIdx = ui.cboRangeSize->itemData(nIndex).toInt();
	
	DataHelper::DateRangeSelectorPreviousNextButtonClicked(dateFrom, dateTo, nRangeIdx, 1);
	
	ui.dateFrom->blockSignals(true);
	ui.dateTo->blockSignals(true);
	ui.dateFrom->setDate(dateFrom);
	ui.dateTo->setDate(dateTo);
	ui.dateFrom->blockSignals(false);
	ui.dateTo->blockSignals(false);

	QDateTime dtFrom = QDateTime(dateFrom, QTime(0,0,0));
	QDateTime dtTo = QDateTime(dateTo, QTime(23,59,59));

	emit onDateChanged(dtFrom, dtTo, nRangeIdx, false);
}

void DateRangeSelectorModalDialog::on_btnCurDateRange_clicked()
{
	QDate dateFrom	= ui.dateFrom->date();
	QDate dateTo	= ui.dateTo->date();
	int nIndex = ui.cboRangeSize->currentIndex();
	int nRangeIdx = ui.cboRangeSize->itemData(nIndex).toInt();

	DataHelper::DateRangeSelectorCurrentButtonClicked(dateFrom, dateTo, nRangeIdx);

	ui.dateFrom->blockSignals(true);
	ui.dateTo->blockSignals(true);
	ui.dateFrom->setDate(dateFrom);
	ui.dateTo->setDate(dateTo);
	ui.dateFrom->blockSignals(false);
	ui.dateTo->blockSignals(false);

	QDateTime dtFrom = QDateTime(dateFrom, QTime(0,0,0));
	QDateTime dtTo = QDateTime(dateTo, QTime(23,59,59));

	emit onDateChanged(dtFrom, dtTo, nRangeIdx, m_bFromCombo);
}

void DateRangeSelectorModalDialog::OnComboChanged(int nIdx)
{
	m_bFromCombo = true;
	on_btnCurDateRange_clicked();
}

void DateRangeSelectorModalDialog::on_btnModalSelector_clicked()
{
	QDateTime dateFrom	= ui.dateFrom->dateTime();
	QDateTime dateTo	= ui.dateTo->dateTime();
	int nIndex = ui.cboRangeSize->currentIndex();
	int nRangeIdx = ui.cboRangeSize->itemData(nIndex).toInt();
	DateRangeSelectorDialog dialog(dateFrom, dateTo, nRangeIdx);
	Qt::WindowFlags flags = dialog.windowFlags();
	Qt::WindowFlags helpFlag = Qt::WindowContextHelpButtonHint;
	flags = flags & (~helpFlag);
	dialog.setWindowFlags(flags);
	connect(&dialog, SIGNAL(OkSignal(QDate, QDate, int)), this, SLOT(onAccepted(QDate, QDate, int)));
	dialog.exec();
}

void DateRangeSelectorModalDialog::onAccepted(QDate dtFrom, QDate dtTo, int nRange)
{
	QDateTime dateFrom = QDateTime(dtFrom, QTime(0,0,0));
	QDateTime dateTo = QDateTime(dtTo, QTime(23,59,59));

	setupDateWidgets(dateFrom, dateTo, nRange);

	emit onDateChanged(dateFrom, dateTo, nRange, false);
}
