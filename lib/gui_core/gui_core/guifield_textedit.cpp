#include "guifield_textedit.h"




//------------------------------------------------------
//			MULTI LINE EDIT: can be connected to QByteArray or QString (looks up in type of column inside datasource)
//------------------------------------------------------

GuiField_TextEdit::GuiField_TextEdit(QString strFieldName,QWidget *pWidget,GuiDataManipulator* pDataManager,DbView *TableView, QLabel *pLabel)
:GuiField(strFieldName,pWidget,pDataManager,TableView,pLabel)
{
	//set as plain text
	m_bIsPlainText=false;

	//test column type in datasource:
	int nColumnType=pDataManager->GetDataSource()->getColumnType(pDataManager->GetDataSource()->getColumnIdx(strFieldName));
	Q_ASSERT(nColumnType==QVariant::String ); //column must be byte array or string
	//m_bIsByteArray=(nColumnType==QVariant::ByteArray);

	//register signal textChanged() ->ChangeData() if lineEdit:
	m_pEditWidget=dynamic_cast<QTextEdit*>(pWidget);

	Q_ASSERT(m_pEditWidget!=NULL);

	connect(m_pEditWidget,SIGNAL(textChanged()),this,SLOT(FieldChanged()));

}

/*!
	If edit line widget, then this slot is invoked when text changes
	NOTE: assert is fired if row is out of bound
*/
void GuiField_TextEdit::FieldChanged()
{
	QVariant value;
	/*
	if(m_bIsByteArray)
	{
		//QByteArray byteValue;
		QString byteValue;
		if(!m_bIsPlainText)
			byteValue = m_pEditWidget->toHtml();//.toLatin1().constData();			//stores multiline content as bytearray (default))
		else
			byteValue = m_pEditWidget->toPlainText();//.toLatin1().constData();	//stores multiline content as bytearray (default))

		value=byteValue;
	}
	else
	{
	*/
		QString strValue;
		if(!m_bIsPlainText)
			strValue= m_pEditWidget->toHtml();				//stores multiline content as string
		else
			strValue = m_pEditWidget->toPlainText();		//stores multiline content as string

		value=strValue;
	//}

	//if(ValidateUserInput(value))
		m_pDataManager->ChangeData(m_nCurrentRow,m_nIndex,value);
}



/*!
	If data is changed from data manipulator, refresh content
	If data source is empty, content will be cleared.
*/
void GuiField_TextEdit::RefreshDisplay()
{
	m_pEditWidget->blockSignals(true);

	//check if current row is out of bound, if so clear content;
	if(m_nCurrentRow<m_pDataManager->GetDataSource()->getRowCount()) 
	{
		QString strContent=m_pDataManager->GetDataSource()->getDataRef(m_nCurrentRow,m_nIndex).toString();
		if(!m_bIsPlainText)
			m_pEditWidget->setHtml(strContent);
		else
			m_pEditWidget->setPlainText(strContent);

		//move to start of doc (issue 1034)
		if (!strContent.isEmpty())
		{
			QTextCursor cur=m_pEditWidget->textCursor();
			cur.movePosition(QTextCursor::Start,QTextCursor::MoveAnchor);
			m_pEditWidget->setTextCursor(cur);
			m_pEditWidget->ensureCursorVisible();
		}
	}
	else
		m_pEditWidget->setHtml("");		

	m_pEditWidget->blockSignals(false);

}



void GuiField_TextEdit::setEnabled(bool bEnable)
{
	m_pEditWidget->setReadOnly(!bEnable);
};