#include "guifield.h"





/*!
	Constructor: sets label & tooltip for widget from given view.
	Stores index for later use of field inside given datasource.
	It can be initialized by GuiDataManipulator or by DbRecordSet as datasource

	\param strFieldName		- column name as defined in data source or in view
	\param pWidget			- widget used for displaying data 
	\param pDataManager		- data source
	\param TableView		- DbView containing label & tooltip
	\param pLabel			- label of widget, can be omitted (if you already set label on window)

*/
GuiField::GuiField(QString strFieldName,QWidget *pWidget,GuiDataManipulator* pDataManager,DbView *TableView, QLabel *pLabel)
{
	m_bUseInternalDataManipulator=false;
	Initalize(strFieldName,pWidget,pDataManager,TableView,pLabel);
}


GuiField::GuiField(QString strFieldName,QWidget *pWidget,DbRecordSet* pDataSource,DbView *TableView, QLabel *pLabel)
{
	m_pDataManager=new GuiDataManipulator(pDataSource);
	m_bUseInternalDataManipulator=true;
	Initalize(strFieldName,pWidget,m_pDataManager,TableView,pLabel);
}

GuiField::~GuiField()
{
	if(m_bUseInternalDataManipulator)delete m_pDataManager;
}

void GuiField::Initalize(QString strFieldName,QWidget *pWidget,GuiDataManipulator* pDataManager,DbView *TableView, QLabel *pLabel)
{

	m_nCurrentRow=0;
	m_nIndex=pDataManager->GetDataSource()->getColumnIdx(strFieldName);
	Q_ASSERT(m_nIndex >= 0);	//Db field must be valid one
	m_pWidget = pWidget;
	m_pDataManager=pDataManager;
}


/*

//call back: validates user input, if fails return old value back
bool GuiField::ValidateUserInput(QVariant& value)
{
	if(m_pValidator==NULL) return true; //if validator not set, always return true

	bool bOK=m_pValidator->ValidateUserInput(m_pWidget,m_nCurrentRow,m_nIndex,value);
	if(!bOK)
		RefreshDisplay(); //return back d' orignal state!

	return bOK;
}
*/