#ifndef PICTUREHELPER_H
#define PICTUREHELPER_H

#include <QPixmap>
#include "common/common/dbrecordset.h"
#include "common/common/status.h"
#include "gui_core/gui_core/guidatamanipulator.h"



/*!
	\class PictureHelper
	\brief Use as static, provides method to load picture from file or from clipboard
	\ingroup GUICore_CustomWidgets

*/
class PictureHelper 
{


public:
	
	static void ConvertPixMapToByteArray(QByteArray &bufPicture, QPixmap &bufPixMap, QString strFormat="PNG");
	static void ResizePixMapToPreview(QPixmap &bufPixMap);
	static void ResizePixMap(QPixmap &bufPixMap, int nWidth, int nHeight);

	static bool LoadPicture(QPixmap &bufPixMap,QString &strFormat);
	static bool SavePicture(QPixmap &bufPixMap);
	static bool PastePictureFromClipboard(QPixmap &bufPixMap);
	static bool CopyPictureToClipboard(QPixmap &bufPixMap);

	static DbRecordSet PreProcessSavePictureIfNeeded(Status &Ret_pStatus,GuiDataManipulator *pDataMan,DbRecordSet &lstPictureWrite,QString strFieldPictureId, QString strPicPreview,bool bIsPictureMandatory=true);
	static void PostProcessSavePictureIfNeeded(DbRecordSet &lstContactPictures,DbRecordSet &lstPictureWrite,QString strFieldPictureId);

	static bool	GetApplicationIcon(QString strAppPath,QPixmap &icon);
	
};

#endif // PICTUREHELPER_H


