#ifndef MULTISORTCOLUMNFRAME_H
#define MULTISORTCOLUMNFRAME_H

#include <QtWidgets/QFrame>
#include "ui_multisortcolumnframe.h"

#include "common/common/dbrecordset.h"

class MultiSortColumnFrame : public QFrame
{
	Q_OBJECT

public:
	MultiSortColumnFrame(QWidget *parent = 0);
	~MultiSortColumnFrame();

	void SetColumnSetup(DbRecordSet &lstColumns);
	void SetColumnSetup(DbRecordSet &lstColumns, DbRecordSet &lstColumnsForSort);
	void SetSortColumnSetup(DbRecordSet &lstColumnsForSort);
	DbRecordSet GetResultRecordSet(){return m_lstColumnsForSort;};

private:
	Ui::MultiSortColumnFrame ui;

	DbRecordSet m_lstColumnsForSort;

private slots:
	void on_btnMoveDown_clicked();
	void on_btnMoveUp_clicked();
	void on_btnAddColumn_clicked();
	void on_btnRemoveColumn_clicked();
};

#endif // MULTISORTCOLUMNFRAME_H
