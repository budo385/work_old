#include "richeditwidget.h"
#ifndef WINCE
	#include "richeditordlg.h"
	#include "htmltaghighlighter.h"
#endif
#include <QScrollBar>
#include <QApplication>
#include <QClipboard>
#include <QtWidgets/QDialog>
#include <QMenuBar>
#include <QToolBar>
#include <QComboBox>
#include <QTextEdit>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QActionGroup>
#include <QFontDatabase>
#include <QPushButton>
#include <QTextCharFormat>
#include <QTextCursor>
#include <QTextList>
#include <QColorDialog>
#include <QPushButton>
#include <QToolButton>
#include <QButtonGroup>
#include <QFileDialog>
#include <QtWidgets/QMessageBox>
#include <QTextCodec>
#include <QTextStream>
#include <QContextMenuEvent>
#include <QPrinter>
#include <QPrintDialog>
#include <QPainter>
#include <QAbstractTextDocumentLayout>
#include <QTextDocumentWriter>
#include "os_specific/os_specific/printmanager.h"
#include "thememanager.h"
#include "bus_client/bus_client/emailhelper.h"
#include <QXmlFormatter>
#include <QXmlQuery>
#include <QBuffer>

#include "common/common/logger.h"
extern Logger g_Logger;					//global logger

extern QString g_strLastDir_FileOpen;

RichEditWidget::RichEditWidget(bool bShowOpenInDialogButton /*= true*/, QWidget *parent /* = 0*/)
    : QFrame(parent)
{
	m_bFixHTMLContents = false;
	m_bEmitSignal = true;

	//Main layout.
	m_bShowOpenInDialogButton = bShowOpenInDialogButton;
#ifdef WINCE
	m_bShowOpenInDialogButton=false; //always false
#endif
	m_pMainLayout = new QVBoxLayout;
	setLayout(m_pMainLayout);

	m_bInverseStretchArrow = false;

	//Menu bar and text edit.
//	m_pTextEdit = new QTextEdit;
//	m_pTextEdit = new QTextBrowser();
	m_pTextEdit = new RichEditTextView();
	m_pTextEdit->setReadOnly(false);
	m_pTextEdit->setUndoRedoEnabled(true);
	m_pTextEdit->setOpenExternalLinks(true);
	m_pTextEdit->setAcceptRichText (true);

	QFont font("Arial", 11);
	int nsize = font.pointSize();
	//Issue #2009.
	//if(QSysInfo::windowsVersion()==QSysInfo::WV_VISTA)
		font.setStyleStrategy(QFont::PreferAntialias);
	m_pTextDocument = new QTextDocument;
	m_pTextEdit->setFont(font);
	m_pTextDocument->setDefaultFont(font);
	m_pTextEdit->setDocument(m_pTextDocument);
	m_pTextEdit->setCurrentFont(font);
	m_pTextEdit->setFontPointSize(11);

	qDebug() << "RighEditWidget:RighEditWidget set font size 11";

	//Search line edit and locals.
	m_pFindLineEdit = new QLineEdit();
	m_nFindFlags	= 0;

#ifndef WINCE
	//MP's old highlighter, see MR's below
	//HtmlTagHighlighter *htmlHighlighter = new HtmlTagHighlighter(m_pTextDocument);
#endif
	
	SetupButtons();
	setupActions();
	setupCombos();
#ifdef WINCE
	SetupDialog_WINCE();
#else
	m_pSearchDialog = NULL;
	SetupDialog();
#endif

	connect(m_pTextEdit, SIGNAL(currentCharFormatChanged(const QTextCharFormat &)), this, SLOT(currentCharFormatChanged(const QTextCharFormat &)));
	connect(m_pTextEdit, SIGNAL(cursorPositionChanged()),							this, SLOT(cursorPositionChanged()));
	connect(m_pTextEdit, SIGNAL(textChanged()),										this, SLOT(on_textChanged()));

	m_strFileName = QString();
	m_bTextIsHTML = false;
	m_bHTMLIsDisplayed = false;
	m_bEditOnly = true;
	m_bSmaller = true;
	m_pActionAlignLeft->trigger();
	m_lstPrintingHeaderData.clear();
	m_bEmbedPix	= true; //MB #2249
	m_pHtmlHighlighter = NULL;
	m_bInsideHTMLChange = false;
}

RichEditWidget::~RichEditWidget()
{
#ifndef WINCE
	m_pSearchDialog = NULL;
#endif

	//if setup destroy temp files
	int nCount = m_lstTempPixFiles.size();
	for(int i=0; i<nCount; i++)
		if(!QFile::remove(m_lstTempPixFiles[i]))
			Q_ASSERT(false);
}

void RichEditWidget::SetHtml(QString htmlString)
{
	if (m_strHtmlText.isEmpty() && htmlString.isEmpty()) //BT: fixed 25.02.2010, issue 2200: setting empty html, changes font size..
	{
		return;
	}

	//qDebug() << "RichEditWidget::SetHtml(" << htmlString << ")";
	if(m_bFixHTMLContents)
		PreprocessFixRtfEmail(htmlString);

	//if setup convert HTML from embedded pix format
	if(m_bEmbedPix){
		//
		int nCount = m_lstTempPixFiles.size();
		for(int i=0; i<nCount; i++)
			QFile::remove(m_lstTempPixFiles[i]);
			//if(!QFile::remove(m_lstTempPixFiles[i]))
			//	Q_ASSERT(false);

		EmailHelper::UnembedHtmlPix(htmlString, m_lstTempPixFiles);
		//qDebug() << "Temp embedded pix" << m_lstTempPixFiles;
		//qDebug() << "Unembedded html" << htmlString;
	}
	//qDebug() << "HTML modified:" << htmlString;

	m_strHtmlText = htmlString;
	m_pTextEdit->clear();
	m_pTextEdit->insertHtml(m_strHtmlText);
	m_pTextEdit->setFocus();

	if(!m_bInsideHTMLChange){
		//this fix combats the problems with MSOffice HTML
		//we get the HTML as modified by the widget itself and use this
		QString htmlStringWidget = GetHtml(true);
		//qDebug() << "HTML modified by the widget:" << htmlStringWidget;
		if(m_strHtmlText != htmlStringWidget){
			//if(m_bFixHTMLContents) HtmlFormatBeautifier(htmlStringWidget);
			m_strHtmlText = htmlStringWidget;
			m_pTextEdit->clear();
			m_pTextEdit->insertPlainText("aaa");
			m_pTextEdit->clear();
			m_pTextEdit->insertHtml(m_strHtmlText);
			m_pTextEdit->setFocus();
		}
		//QString htmlStringWidget1 = GetHtml(true);
	}

	m_bTextIsHTML = true;
	m_bHTMLIsDisplayed = false;
	m_pShowHTMLSourceButton->setDisabled(false);

	if(m_pHtmlHighlighter){
		delete m_pHtmlHighlighter;
		m_pHtmlHighlighter = NULL;
	}

	//Move cursor to start.
	m_pTextEdit->moveCursor(QTextCursor::Start);
}

QString RichEditWidget::GetHtml(bool bSkipPixEmbed)
{
	QString htmlString;
	if (m_bHTMLIsDisplayed)
		htmlString = m_pTextEdit->toPlainText();
	else
		htmlString = m_pTextEdit->toHtml();

	//if setup convert HTML back to embedded pix format
	if(m_bEmbedPix && !bSkipPixEmbed){
		QStringList lstResOrigFiles;
		EmailHelper::EmbedHtmlPix(htmlString, lstResOrigFiles);
	}

	return htmlString;
}

void RichEditWidget::RefreshHtmlHighlight()
{
	if(m_pHtmlHighlighter){
		delete m_pHtmlHighlighter;
		m_pHtmlHighlighter = NULL;
	}
	if(m_bHTMLIsDisplayed){
		m_pHtmlHighlighter = new HtmlHighlighter(m_pTextDocument);
	}
}

void RichEditWidget::SetPlainText(QString strPlainText)
{
	m_pTextEdit->clear();
	m_pTextEdit->insertPlainText(strPlainText);
	m_pTextEdit->setFocus();

#ifdef _DEBUG
	//QString strNewText = m_pTextEdit->toPlainText();
	//Q_ASSERT(strNewText == strPlainText);
#endif

	m_bTextIsHTML = false;
	m_bHTMLIsDisplayed = false;
	m_pShowHTMLSourceButton->setDisabled(true);
	RefreshHtmlHighlight();

	//Move cursor to start.
	m_pTextEdit->moveCursor(QTextCursor::Start);
}

void RichEditWidget::SetDocument(QTextDocument *pTextDocument)
{
	disconnectDocumnetFromSignals();
	m_pTextDocument = pTextDocument;
	m_pTextEdit->setDocument(m_pTextDocument);
	connectDocumnetToSignals(m_pTextDocument);
	//m_pTextEdit->verticalScrollBar()->setSliderPosition(m_pTextEdit->verticalScrollBar()->maximum());

	m_bTextIsHTML = true;
	m_bHTMLIsDisplayed = false;
	m_pShowHTMLSourceButton->setDisabled(false);
	RefreshHtmlHighlight();

	//Move cursor to start.
	m_pTextEdit->moveCursor(QTextCursor::Start);
}

QString RichEditWidget::GetPlainText()
{
	return m_pTextEdit->toPlainText();
}

//For journal:
//0 - contact name?, 1 - date, 2 - person.
//For documents:
//0 - name, 1 - contact, 2 - project, 3 - date, 4 - person.
//For voice calls:
//0 - Incoming ir Outgoing, 1 - strDate, 2 - strContact, 3 - strPhoneNumber, 4 - strOwner, 5 - strStartTime, 6 - strEndTime, 
//7 - strDurationTime, 8 - strProject.

void RichEditWidget::SetupPrintingHeader(QStringList lstPrintingHeaderData)
{
	m_lstPrintingHeaderData = lstPrintingHeaderData;
}

bool RichEditWidget::IsHTMLDisplayed()
{
	return m_bHTMLIsDisplayed;
}

void RichEditWidget::SwitchFromHTMLToNormalView()
{
	on_ShowHTMLSource();
}

void RichEditWidget::ShowVerticalStretchButton(bool bShow)
{
	m_pVerticalOpenClose->setHidden(!bShow);
}

void RichEditWidget::SetSmallBoolean(bool bSmall) //dirty just to satisfy Herzog layout changes.
{ 
//	m_bSmaller = bSmall;
//	m_bInverseStretchArrow = false;
	on_VerticalOpenClose();
}

//Issue #2097.
void RichEditWidget::SetInverseVerticalStretchButton()
{
	m_bInverseStretchArrow = true;
	m_pActionVerticalOpenClose->setIcon(QIcon(":Vertical_Open.png"));
//	if (m_bSmaller)
//		m_pActionVerticalOpenClose->setIcon(QIcon(":Vertical_Close.png"));
//	else
//		m_pActionVerticalOpenClose->setIcon(QIcon(":Vertical_Open.png"));
}

void RichEditWidget::SetupDialog()
{
	//Tool bar.
	QHBoxLayout *buttonLayout = new QHBoxLayout;

	buttonLayout->setSpacing(2);
	buttonLayout->setAlignment(Qt::AlignLeft);

	buttonLayout->addWidget((QWidget*)m_pOpenFileButton);
	//If opened from dialog show save file buttons.
	//Issue #2003 hide save file and add export to html instead.
	//Issue #960 add save button also if not opened in dialog.
	//if (!m_bShowOpenInDialogButton)
	//	buttonLayout->addWidget((QWidget*)m_pSaveFileButton);
	
	buttonLayout->addWidget((QWidget*)m_pExportToHtmlButton);
	buttonLayout->addWidget((QWidget*)m_pExportToPdfButton);
	buttonLayout->addWidget((QWidget*)m_pExportToOdfButton);
	buttonLayout->addWidget((QWidget*)m_pPrintButton);
	buttonLayout->addSpacing(3);

	buttonLayout->addWidget((QWidget*)m_pUndoButton);
	buttonLayout->addWidget((QWidget*)m_pRedoButton);
	buttonLayout->addSpacing(3);
	buttonLayout->addWidget((QWidget*)m_pCutButton);
	buttonLayout->addWidget((QWidget*)m_pCopyButton);
	buttonLayout->addWidget((QWidget*)m_pPasteButton);
	buttonLayout->addSpacing(3);
	buttonLayout->addWidget((QWidget*)m_pTextBoldButton);
	buttonLayout->addWidget((QWidget*)m_pTextItalicButton);
	buttonLayout->addWidget((QWidget*)m_pTextUnderlineButton);
	buttonLayout->addWidget((QWidget*)m_pStrikeThroughButton);
	buttonLayout->addSpacing(3);

	buttonLayout->addWidget((QWidget*)m_pAlignLeftButton);
	buttonLayout->addWidget((QWidget*)m_pAlignCenterButton);
	buttonLayout->addWidget((QWidget*)m_pAlignRightButton);
	buttonLayout->addWidget((QWidget*)m_pAlignJustifyButton);
	buttonLayout->addSpacing(3);

	buttonLayout->addWidget((QWidget*)m_pTextColorButton);
	buttonLayout->addWidget((QWidget*)m_pInsertPictureButton);
	
	//Issue #2003 hide open in dialog button
	//If opened from dialog don't show open dialog button.
	//	if (m_bShowOpenInDialogButton)
	//	{
	//		buttonLayout->addSpacing(3);
	//		buttonLayout->addWidget((QWidget*)m_pOpenInDialog);
	//	}
	//	else
	//	{
	//		buttonLayout->addSpacing(3);
	//		buttonLayout->addWidget((QWidget*)m_pShowHTMLSourceButton);
	//	}
	buttonLayout->addWidget((QWidget*)m_pShowHTMLSourceButton);
	
	buttonLayout->addStretch(7);
	
	if (m_bShowOpenInDialogButton)
		buttonLayout->addWidget((QWidget*)m_pVerticalOpenClose);

	//By default hide vertical stretc button.
	m_pVerticalOpenClose->setHidden(true);

	m_pMainLayout->addLayout(buttonLayout);

	//Combo boxes.
	QHBoxLayout *comboLayout = new QHBoxLayout;
	comboLayout->addWidget(m_pComboStyle);
	comboLayout->addWidget(m_pComboFont);
	comboLayout->addWidget(m_pComboSize);
	comboLayout->addWidget((QWidget*)m_pFindLineEdit);
	comboLayout->addWidget((QWidget*)m_pSearchButton);
	comboLayout->addWidget((QWidget*)m_pFindPreviousButton);
	comboLayout->addWidget((QWidget*)m_pSearchExtendedButton);
	comboLayout->addStretch(10);
	
	m_pMainLayout->addLayout(comboLayout);
	//Finally add text edit.
	m_pMainLayout->addWidget(m_pTextEdit);//, 10);
	m_pMainLayout->setMargin(5);
	m_pMainLayout->setSpacing(3);

	 //setTabOrder(m_pTextEdit, m_pComboStyle);
	 //setTabOrder(m_pComboStyle, m_pComboFont);
	 //setTabOrder(m_pComboFont, m_pComboSize);
}


void RichEditWidget::SetupDialog_WINCE()
{
	
	//Tool bar.
	QHBoxLayout *buttonLayout = new QHBoxLayout;
	buttonLayout->setSpacing(1);
	buttonLayout->setAlignment(Qt::AlignLeft);
	buttonLayout->addWidget((QWidget*)m_pOpenFileButton);
	//If opened from dialog show save file buttons.
	//Issue #960 add save button also if not opened in dialog.
	//if (!m_bShowOpenInDialogButton)
	//	buttonLayout->addWidget((QWidget*)m_pSaveFileButton);

	m_pPrintButton->setVisible(false);

	//buttonLayout->addSpacing(3);
	buttonLayout->addWidget((QWidget*)m_pUndoButton);
	buttonLayout->addWidget((QWidget*)m_pRedoButton);
	//buttonLayout->addSpacing(3);
	buttonLayout->addWidget((QWidget*)m_pCutButton);
	buttonLayout->addWidget((QWidget*)m_pCopyButton);
	buttonLayout->addWidget((QWidget*)m_pPasteButton);
	//buttonLayout->addSpacing(3);
	buttonLayout->addWidget((QWidget*)m_pTextBoldButton);
	buttonLayout->addWidget((QWidget*)m_pTextItalicButton);
	buttonLayout->addWidget((QWidget*)m_pTextUnderlineButton);
	buttonLayout->addWidget((QWidget*)m_pStrikeThroughButton);
	//buttonLayout->addSpacing(3);
	buttonLayout->addStretch(1);

	QHBoxLayout *buttonLayout1 = new QHBoxLayout;
	//buttonLayout1->setSpacing(1);
	buttonLayout1->setAlignment(Qt::AlignLeft);
	buttonLayout1->addWidget((QWidget*)m_pAlignLeftButton);
	buttonLayout1->addWidget((QWidget*)m_pAlignCenterButton);
	buttonLayout1->addWidget((QWidget*)m_pAlignRightButton);
	buttonLayout1->addWidget((QWidget*)m_pAlignJustifyButton);
	//buttonLayout1->addSpacing(3);
	buttonLayout1->addWidget((QWidget*)m_pTextColorButton);
	buttonLayout1->addWidget((QWidget*)m_pInsertPictureButton);
	buttonLayout1->addStretch(1);

	//If opened from dialog don't show open dialog button.


	m_pOpenInDialog->setVisible(false);
	m_pShowHTMLSourceButton->setVisible(false);

	//buttonLayout->addStretch(7);

	m_pMainLayout->addLayout(buttonLayout);
	m_pMainLayout->addLayout(buttonLayout1);

	//Combo boxes.
	QHBoxLayout *comboLayout = new QHBoxLayout;
	m_pComboStyle->setMaximumWidth(115);
	m_pComboFont->setMinimumWidth(90);
	m_pComboFont->setMaximumWidth(90);
	m_pComboStyle->setMaximumHeight(16);
	m_pComboFont->setMaximumHeight(16);
	m_pComboSize->setMaximumHeight(16);
	m_pComboStyle->setSizePolicy(QSizePolicy(QSizePolicy::Minimum,QSizePolicy::Fixed));
	m_pComboFont->setSizePolicy(QSizePolicy(QSizePolicy::Minimum,QSizePolicy::Fixed));

	comboLayout->setMargin(0);
	comboLayout->setSpacing(1);
	comboLayout->addWidget(m_pComboStyle);
	comboLayout->addWidget(m_pComboFont);
	comboLayout->addWidget(m_pComboSize);
	comboLayout->addStretch(1);
	
	m_pMainLayout->addLayout(comboLayout);
	//Finally add text edit.
	m_pMainLayout->addWidget(m_pTextEdit);//, 10);
	m_pMainLayout->setMargin(1);
	m_pMainLayout->setSpacing(1);

	setStyleSheet(ThemeManager::GetGlobalWidgetStyle()+" QFrame[class=\"RichEditWidget\"] "+ThemeManager::GetMobileSolidBkg_Dark());
}

void RichEditWidget::SetupButtons()
{
#ifdef WINCE
	QSize buttonSize(16, 16);
#else
	QSize buttonSize(24, 24);
#endif

	//Buttons.
	m_pOpenFileButton		= new QToolButton;
//	m_pSaveFileButton		= new QToolButton;
	m_pExportToHtmlButton	= new QToolButton;
	m_pExportToPdfButton	= new QToolButton;
	m_pExportToOdfButton	= new QToolButton;
	m_pTextBoldButton		= new QToolButton;
	m_pTextUnderlineButton	= new QToolButton;
	m_pTextItalicButton		= new QToolButton;
	m_pTextColorButton		= new QToolButton;
	m_pAlignLeftButton		= new QToolButton;
	m_pAlignCenterButton	= new QToolButton;
	m_pAlignRightButton		= new QToolButton;
	m_pAlignJustifyButton	= new QToolButton;
	m_pUndoButton			= new QToolButton;
	m_pRedoButton			= new QToolButton;
	m_pCutButton			= new QToolButton;
	m_pCopyButton			= new QToolButton;
	m_pPasteButton			= new QToolButton;
	m_pOpenInDialog			= new QToolButton;
	m_pPrintButton			= new QToolButton;
	m_pInsertPictureButton	= new QToolButton;
	m_pShowHTMLSourceButton = new QToolButton;
	m_pVerticalOpenClose	= new QToolButton;
	m_pSearchButton			= new QToolButton;
	m_pSearchExtendedButton	= new QToolButton;
	m_pFindPreviousButton	= new QToolButton;
	m_pStrikeThroughButton	= new QToolButton;
	
	m_pButtonGroup			= new QButtonGroup(this);
	m_pButtonGroup->setExclusive(true);
	m_pButtonGroup->addButton(m_pAlignLeftButton, 0);
	m_pButtonGroup->addButton(m_pAlignCenterButton, 1);
	m_pButtonGroup->addButton(m_pAlignRightButton, 2);
	m_pButtonGroup->addButton(m_pAlignJustifyButton, 3);

	m_pOpenFileButton		->setMaximumSize(buttonSize);
	//m_pSaveFileButton		->setMaximumSize(buttonSize);
	m_pExportToHtmlButton	->setMaximumSize(buttonSize);
	m_pExportToPdfButton	->setMaximumSize(buttonSize);
	m_pExportToOdfButton	->setMaximumSize(buttonSize);
	m_pTextBoldButton		->setMaximumSize(buttonSize);
	m_pTextUnderlineButton	->setMaximumSize(buttonSize);
	m_pTextItalicButton		->setMaximumSize(buttonSize);
	m_pTextColorButton		->setMaximumSize(buttonSize);
	m_pAlignLeftButton		->setMaximumSize(buttonSize);
	m_pAlignCenterButton	->setMaximumSize(buttonSize);
	m_pAlignRightButton		->setMaximumSize(buttonSize);
	m_pAlignJustifyButton	->setMaximumSize(buttonSize);
	m_pUndoButton			->setMaximumSize(buttonSize);
	m_pRedoButton			->setMaximumSize(buttonSize);
	m_pCutButton			->setMaximumSize(buttonSize);
	m_pCopyButton			->setMaximumSize(buttonSize);
	m_pPasteButton			->setMaximumSize(buttonSize);
	m_pOpenInDialog			->setMaximumSize(buttonSize);
	m_pPrintButton			->setMaximumSize(buttonSize);
	m_pInsertPictureButton	->setMaximumSize(buttonSize);
	m_pShowHTMLSourceButton ->setMaximumSize(buttonSize);
	m_pVerticalOpenClose	->setMaximumSize(buttonSize);
	m_pSearchButton			->setMaximumSize(buttonSize);
	m_pSearchExtendedButton	->setMaximumSize(buttonSize);
	m_pFindPreviousButton	->setMaximumSize(buttonSize);
	m_pStrikeThroughButton	->setMaximumSize(buttonSize);

	m_pOpenFileButton		->setAutoRaise(true);
	//m_pSaveFileButton		->setAutoRaise(true);
	m_pExportToHtmlButton	->setAutoRaise(true);
	m_pExportToPdfButton	->setAutoRaise(true);
	m_pExportToOdfButton	->setAutoRaise(true);
	m_pTextBoldButton		->setAutoRaise(true);
	m_pTextUnderlineButton	->setAutoRaise(true);
	m_pTextItalicButton		->setAutoRaise(true);
	m_pTextColorButton		->setAutoRaise(true);
	m_pAlignLeftButton		->setAutoRaise(true);
	m_pAlignCenterButton	->setAutoRaise(true);
	m_pAlignRightButton		->setAutoRaise(true);
	m_pAlignJustifyButton	->setAutoRaise(true);
	m_pUndoButton			->setAutoRaise(true);
	m_pRedoButton			->setAutoRaise(true);
	m_pCutButton			->setAutoRaise(true);
	m_pCopyButton			->setAutoRaise(true);
	m_pPasteButton			->setAutoRaise(true);
	m_pOpenInDialog			->setAutoRaise(true);
	m_pPrintButton			->setAutoRaise(true);
	m_pInsertPictureButton	->setAutoRaise(true);
	m_pShowHTMLSourceButton ->setAutoRaise(true);
	m_pShowHTMLSourceButton ->setCheckable(true);
	m_pVerticalOpenClose	->setAutoRaise(true);
	m_pSearchButton			->setAutoRaise(true);
	m_pSearchExtendedButton	->setAutoRaise(true);
	m_pFindPreviousButton	->setAutoRaise(true);
	m_pStrikeThroughButton	->setAutoRaise(true);
}

void RichEditWidget::setupActions()
{
	//Open.
	m_pActionOpenFile = new QAction(QIcon(":openfile.png"), tr("&Open"), this);
	m_pActionOpenFile->setShortcut(Qt::CTRL + Qt::Key_O);
	m_pOpenFileButton->setDefaultAction(m_pActionOpenFile);
	connect(m_pActionOpenFile,			SIGNAL(triggered()),			this,				SLOT(OnOpenFile()));
	//Save.
	m_pActionSaveFile = new QAction(QIcon(":filesave.png"), tr("&Save"), this);
	//m_pActionSaveFile->setShortcut(Qt::CTRL + Qt::Key_S);
	//m_pSaveFileButton->setDefaultAction(m_pActionSaveFile);
	//connect(m_pActionSaveFile,			SIGNAL(triggered()),			this,				SLOT(OnSaveFile()));
	//Export to Html.
	m_pActionExportToHtml = new QAction(QIcon(":Export_HTML16.png"), tr("&Export as HTML"), this);
	m_pActionExportToHtml->setShortcut(Qt::CTRL + Qt::Key_S);
	m_pExportToHtmlButton->setDefaultAction(m_pActionExportToHtml);
	connect(m_pActionExportToHtml,			SIGNAL(triggered()),			this,				SLOT(on_exportToHtmlButton()));
	//Export to Pdf.
	m_pActionExportToPdf = new QAction(QIcon(":Export_PDF16.png"), tr("Export as PDF"), this);
	m_pActionExportToPdf->setShortcut(Qt::CTRL + Qt::Key_X);
	m_pExportToPdfButton->setDefaultAction(m_pActionExportToPdf);
	connect(m_pActionExportToPdf,			SIGNAL(triggered()),			this,				SLOT(on_exportToPdfButton()));
	//Export to Odf.
	m_pActionExportToOdf = new QAction(QIcon(":Export_ODF16.png"), tr("Export as Open Document Format ODT"), this);
	//m_pActionExportToOdf->setShortcut(Qt::CTRL + Qt::Key_X);
	m_pExportToOdfButton->setDefaultAction(m_pActionExportToOdf);
	connect(m_pActionExportToOdf,			SIGNAL(triggered()),			this,				SLOT(on_exportToOdfButton()));
	//Print.
	m_pActionPrintFile = new QAction(QIcon(":print.png"), tr("&Print"), this);
	m_pActionPrintFile->setShortcut(Qt::CTRL + Qt::Key_P);
	m_pPrintButton->setDefaultAction(m_pActionPrintFile);
	connect(m_pActionPrintFile,			SIGNAL(triggered()),			this,				SLOT(OnPrintFile()));
	//Save As.
	m_pActionSaveFileAs = new QAction(QIcon(":filesave.png"), tr("&Save as..."), this);
	//connect(m_pActionSaveFileAs,		SIGNAL(triggered()),			this,				SLOT(OnSaveAsFile()));
	//Undo.
	m_pActionUndo = new QAction(QIcon(":editundo.png"), tr("&Undo"), this);
	m_pActionUndo->setShortcut(Qt::CTRL + Qt::Key_Z);
	m_pUndoButton->setDefaultAction(m_pActionUndo);
	//Redo.
	m_pActionRedo = new QAction(QIcon(":redo.png"), tr("&Redo"), this);
	m_pActionRedo->setShortcut(Qt::CTRL + Qt::Key_Y);
	m_pRedoButton->setDefaultAction(m_pActionRedo);
	//Cut.
	m_pActionCut = new QAction(QIcon(":cut.png"), tr("Cu&t"), this);
	m_pActionCut->setShortcut(Qt::CTRL + Qt::Key_X);
	m_pCutButton->setDefaultAction(m_pActionCut);
	connect(m_pTextEdit,				SIGNAL(copyAvailable(bool)),	m_pActionCut,		SLOT(setEnabled(bool)));
	connect(m_pActionCut,				SIGNAL(triggered()),			m_pTextEdit,		SLOT(cut()));
	//Copy.
	m_pActionCopy = new QAction(QIcon(":copy.png"), tr("&Copy"), this);
	m_pActionCopy->setShortcut(Qt::CTRL + Qt::Key_C);
	m_pCopyButton->setDefaultAction(m_pActionCopy);
	connect(m_pTextEdit,				SIGNAL(copyAvailable(bool)),	m_pActionCopy,		SLOT(setEnabled(bool)));
	connect(m_pActionCopy,				SIGNAL(triggered()),			m_pTextEdit,		SLOT(copy()));
	//Paste.
	m_pActionPaste = new QAction(QIcon(":paste.png"), tr("&Paste"), this);
	//m_pActionPaste->setShortcut(Qt::CTRL + Qt::Key_V);
	m_pActionPaste->setShortcut(QKeySequence::Paste);
	m_pActionPaste->setShortcutContext(Qt::WidgetShortcut);
	m_pPasteButton->setDefaultAction(m_pActionPaste);
	m_pTextEdit->addAction(m_pActionPaste);
	//m_pPasteButton->setShortcut(Qt::CTRL + Qt::Key_V);
	connect(QApplication::clipboard(),	SIGNAL(dataChanged()),			this,				SLOT(clipboardDataChanged()));
	connect(m_pActionPaste,				SIGNAL(triggered()),			this,				SLOT(on_paste()));

	((RichEditTextView *)m_pTextEdit)->m_pPasteAction = m_pActionPaste;

	//Connect document to signals.
	connectDocumnetToSignals(m_pTextDocument);
	
	//Bold.
	m_pActionTextBold = new QAction(QIcon(":Text_Bold.png"), tr("&Bold"), this);
	m_pActionTextBold->setShortcut(Qt::CTRL + Qt::Key_B);
	m_pActionTextBold->setCheckable(true);
	QFont bold;
	bold.setBold(true);
	m_pActionTextBold->setFont(bold);
	m_pActionTextBold->setCheckable(true);
	m_pTextBoldButton->setDefaultAction(m_pActionTextBold);
	connect(m_pActionTextBold,			SIGNAL(triggered()),			this,				SLOT(textBold()));

	//Italic.
	m_pActionTextItalic = new QAction(QIcon(":Text_Italic.png"), tr("&Italic"), this);
	m_pActionTextItalic->setShortcut(Qt::CTRL + Qt::Key_I);
	QFont italic;
	italic.setItalic(true);
	m_pActionTextItalic->setFont(italic);
	m_pActionTextItalic->setCheckable(true);
	m_pTextItalicButton->setDefaultAction(m_pActionTextItalic);
	connect(m_pActionTextItalic,		SIGNAL(triggered()),			this,				SLOT(textItalic()));

	//Underline.
	m_pActionTextUnderline = new QAction(QIcon(":Text_Underline.png"), tr("&Underline"), this);
	m_pActionTextUnderline->setShortcut(Qt::CTRL + Qt::Key_U);
	QFont underline;
	underline.setUnderline(true);
	m_pActionTextUnderline->setFont(underline);
	m_pActionTextUnderline->setCheckable(true);
	m_pTextUnderlineButton->setDefaultAction(m_pActionTextUnderline);
	connect(m_pActionTextUnderline,		SIGNAL(triggered()),			this,				SLOT(textUnderline()));

	//Alignment.
	m_pActionGroup = new QActionGroup(this);
	m_pActionGroup->setExclusive(true);
	m_pActionAlignLeft = new QAction(QIcon(":textleft.png"), tr("&Left"), m_pActionGroup);
	m_pActionAlignLeft->setShortcut(Qt::CTRL + Qt::Key_L);
	m_pActionAlignLeft->setCheckable(true);
	m_pAlignLeftButton->setDefaultAction(m_pActionAlignLeft);
	m_pActionAlignCenter = new QAction(QIcon(":textcenter.png"), tr("C&enter"), m_pActionGroup);
	m_pActionAlignCenter->setShortcut(Qt::CTRL + Qt::Key_E);
	m_pActionAlignCenter->setCheckable(true);
	m_pAlignCenterButton->setDefaultAction(m_pActionAlignCenter);
	m_pActionAlignRight = new QAction(QIcon(":textright.png"), tr("&Right"), m_pActionGroup);
	m_pActionAlignRight->setShortcut(Qt::CTRL + Qt::Key_R);
	m_pActionAlignRight->setCheckable(true);
	m_pAlignRightButton->setDefaultAction(m_pActionAlignRight);
	m_pActionAlignJustify = new QAction(QIcon(":textjustify.png"), tr("&Justify"), m_pActionGroup);
	m_pActionAlignJustify->setShortcut(Qt::CTRL + Qt::Key_J);
	m_pActionAlignJustify->setCheckable(true);
	m_pAlignJustifyButton->setDefaultAction(m_pActionAlignJustify);

	connect(m_pActionGroup,				SIGNAL(triggered(QAction *)),	this,					SLOT(textAlign(QAction *)));
	connect(m_pAlignLeftButton,			SIGNAL(clicked()),				this,					SLOT(textAlignLeft()));
	connect(m_pAlignCenterButton,		SIGNAL(clicked()),				this,					SLOT(textAlignCenter()));
	connect(m_pAlignRightButton,		SIGNAL(clicked()),				this,					SLOT(textAlignRight()));
	connect(m_pAlignJustifyButton,		SIGNAL(clicked()),				this,					SLOT(textAlignJustify()));

	//Color.
	QPixmap pix(16, 16);
	pix.fill(Qt::red);
	m_pActionTextColor = new QAction(pix, tr("Colo&r..."), this);
	m_pTextColorButton->setDefaultAction(m_pActionTextColor);
	connect(m_pActionTextColor, SIGNAL(triggered()), this, SLOT(textColor()));

	//Open in dialog button.
	m_pActionOpenInDialog = new QAction(QIcon(":SAP_Modify.png"), tr("&Open in dialog"), this);
	m_pOpenInDialog->setDefaultAction(m_pActionOpenInDialog);
	connect(m_pActionOpenInDialog,		SIGNAL(triggered()),			this,				SLOT(OpenInDialog()));

	//Insert Picture
	m_pActionInsertPicture = new QAction(QIcon(":Icon_Picture.png"), tr("Insert Picture"), this);
	m_pInsertPictureButton->setDefaultAction(m_pActionInsertPicture);
	connect(m_pActionInsertPicture,		SIGNAL(triggered()),			this,				SLOT(InsertPicture()));

	//Show html source.
	m_pActionShowHTMLSource = new QAction(QIcon(":Edit_HTML16.png"), tr("HTML Source Code View"), this);
	m_pShowHTMLSourceButton->setDefaultAction(m_pActionShowHTMLSource);
	connect(m_pActionShowHTMLSource,		SIGNAL(triggered()),			this,				SLOT(on_ShowHTMLSource()));

	//Vertically stretch or collapse.
	m_pActionVerticalOpenClose = new QAction(QIcon(":Vertical_Close.png"), tr("Larger"), this);
	m_pVerticalOpenClose->setDefaultAction(m_pActionVerticalOpenClose);
	connect(m_pActionVerticalOpenClose,		SIGNAL(triggered()),			this,				SLOT(on_VerticalOpenClose()));

	//Search button.
	m_pActionSearchButton = new QAction(QIcon(":Lupe16R.png"), tr("Search Next"), this);
	m_pSearchButton->setDefaultAction(m_pActionSearchButton);
	connect(m_pActionSearchButton,			SIGNAL(triggered()),			this,				SLOT(on_SearchButton()));

	//Extended search button.
	m_pActionSearchExtendedButton = new QAction(QIcon(":SearchExt16.png"), tr("Extended Search"), this);
	m_pSearchExtendedButton->setDefaultAction(m_pActionSearchExtendedButton);
	connect(m_pActionSearchExtendedButton,			SIGNAL(triggered()),			this,		SLOT(on_SearchExtendenButton()));

	//Find previous button.
	m_pActionFindPreviousButton = new QAction(QIcon(":Lupe16L.png"), tr("Search Previous"), this);
	m_pFindPreviousButton->setDefaultAction(m_pActionFindPreviousButton);
	connect(m_pActionFindPreviousButton,			SIGNAL(triggered()),			this,		SLOT(on_findPreviousButton()));
	
	//Strike through button.
	m_pActionStrikeThrough = new QAction(QIcon(":Text_Strikethrough.png"), tr("Strikethrough"), this);
	m_pStrikeThroughButton->setDefaultAction(m_pActionStrikeThrough);
	m_pActionStrikeThrough->setCheckable(true);
	connect(m_pActionStrikeThrough,			SIGNAL(triggered()),			this,		SLOT(on_StrikeThrough()));
}

void RichEditWidget::setupCombos()
{
	//Combos.
	m_pComboStyle = new QComboBox(this);
	m_pComboStyle->addItem("Standard");
	m_pComboStyle->addItem("Bullet List (Disc)");
	m_pComboStyle->addItem("Bullet List (Circle)");
	m_pComboStyle->addItem("Bullet List (Square)");
	m_pComboStyle->addItem("Ordered List (Decimal)");
	m_pComboStyle->addItem("Ordered List (Alpha lower)");
	m_pComboStyle->addItem("Ordered List (Alpha upper)");
	connect(m_pComboStyle, SIGNAL(activated(int)),	this, SLOT(textStyle(int)));

	m_pComboFont = new QComboBox(this);
	m_pComboFont->setEditable(true);
	QFontDatabase db;
	m_pComboFont->addItems(db.families());
	connect(m_pComboFont, SIGNAL(activated(const QString &)), this, SLOT(textFamily(const QString &)));

	int index = m_pComboFont->findText(m_pTextDocument->defaultFont().family());
	if (index<0 || index > m_pComboFont->count())		
		index=0;
	m_pComboFont->setCurrentIndex(index);
	m_pTextEdit->setCurrentFont(m_pTextDocument->defaultFont().family());

	m_pComboSize = new QComboBox(this);
	m_pComboSize->setObjectName("comboSize");
	m_pComboSize->setEditable(true);
	m_pComboSize->setSizePolicy(QSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed));
	m_pComboSize->setMinimumWidth(40);
	m_pComboSize->setMaximumWidth(40);

	foreach(int size, db.standardSizes())
		m_pComboSize->addItem(QString::number(size));

	connect(m_pComboSize, SIGNAL(activated(const QString &)), this, SLOT(textSize(const QString &)));
	int nsize = m_pTextDocument->defaultFont().pointSize();
	m_pComboSize->setCurrentIndex(m_pComboSize->findText(QString::number(nsize)));
	qDebug() << "RighEditWidget:setupCombos set font size " << nsize;
}

void RichEditWidget::OnOpenFile()
{

	QString FileName = QFileDialog::getOpenFileName(this, tr("Open File..."),
		g_strLastDir_FileOpen, tr("HTML-Files (*.htm *.html);;All Files (*)"));

	if (!FileName.isEmpty())
	{
		QFileInfo fileInfo(FileName);
		g_strLastDir_FileOpen=fileInfo.absolutePath();

		if (!QFile::exists(FileName))
			return;
		QFile file(FileName);
		if (!file.open(QFile::ReadOnly))
		{
			QMessageBox::information(this, tr("Open File Error!"), tr("Unable to open desired file!"));
			return;
		}

		QByteArray data = file.readAll();
		QTextCodec *codec = Qt::codecForHtml(data);
		QString str = codec->toUnicode(data);
		if (Qt::mightBeRichText(str)) {
			m_pTextEdit->setHtml(str);
		} else {
			str = QString::fromLocal8Bit(data);
			m_pTextEdit->setPlainText(str);
		}
	}
}

void RichEditWidget::OnSaveFile(QString strFormat)
{
	if (m_strFileName.isEmpty())
		return OnSaveAsFile(strFormat);

	SaveDoc(strFormat);
}

void RichEditWidget::OnPrintFile()
{
	emit on_PrintIcon_clicked();

	//Add header to print.
//	m_lstPrintingHeaderData << "Note title" << "Branimir Trumbic" << "Helix business soft" << "24.3.2007" << "Marko Perutovic";


	m_pTextEdit->setUpdatesEnabled(false);
	
	if (m_lstPrintingHeaderData.count() == 3)
		AddJournalPrintingHeader();
	if (m_lstPrintingHeaderData.count() == 5)
		AddNotePrintingHeader();
	if (m_lstPrintingHeaderData.count() == 9)
		AddPhoneCallPrintingHeader();

	PrintManager::PrintText(m_pTextEdit);

	//Remove printing header.
	if (m_lstPrintingHeaderData.count() == 3 || m_lstPrintingHeaderData.count() == 5 || m_lstPrintingHeaderData.count() == 9)
		m_pTextEdit->undo();

	m_pTextEdit->setUpdatesEnabled(true);
}

void RichEditWidget::OnSaveAsFile(QString strFormat)
{
	QString fn;
	if (strFormat == "html")
	{
		fn = QFileDialog::getSaveFileName(this, tr("Export as HTML..."),
		g_strLastDir_FileOpen, tr("HTML-Files (*.htm *.html);;"));
	}
	else
	{
		fn = QFileDialog::getSaveFileName(this, tr("Export as ODF..."),
		g_strLastDir_FileOpen, tr("ODF-Files (*.odf);;"));
	}
	
	if (!fn.isEmpty())
	{
		QFileInfo fileInfo(fn);
		g_strLastDir_FileOpen=fileInfo.absolutePath();
	}
	if (fn.isEmpty())
		return;
	
	m_strFileName = fn;
	
	return OnSaveFile(strFormat);
}

void RichEditWidget::focusSlot()
{
	m_pTextEdit->setFocus();
}

void RichEditWidget::textBold()
{
	m_pTextEdit->setFontWeight(m_pActionTextBold->isChecked() ? QFont::Bold : QFont::Normal);
}

void RichEditWidget::textUnderline()
{
	m_pTextEdit->setFontUnderline(m_pActionTextUnderline->isChecked());
}

void RichEditWidget::textItalic()
{
	m_pTextEdit->setFontItalic(m_pActionTextItalic->isChecked());
}

void RichEditWidget::textFamily(const QString &f)
{
	m_pTextEdit->setFontFamily(f);
	m_pTextEdit->setFocus();
}

void RichEditWidget::textSize(const QString &p)
{
	qDebug() << "RighEditWidget:textSize new font size " << p.toFloat();

	m_pTextEdit->setFontPointSize(p.toFloat());
	m_pTextEdit->setFocus();
}

void RichEditWidget::textStyle(int styleIndex)
{
	QTextCursor cursor = m_pTextEdit->textCursor();

	if (styleIndex != 0) {
		QTextListFormat::Style style = QTextListFormat::ListDisc;

		switch (styleIndex) {
			default:
			case 1:
				style = QTextListFormat::ListDisc;
				break;
			case 2:
				style = QTextListFormat::ListCircle;
				break;
			case 3:
				style = QTextListFormat::ListSquare;
				break;
			case 4:
				style = QTextListFormat::ListDecimal;
				break;
			case 5:
				style = QTextListFormat::ListLowerAlpha;
				break;
			case 6:
				style = QTextListFormat::ListUpperAlpha;
				break;
		}

		cursor.beginEditBlock();

		QTextBlockFormat blockFmt = cursor.blockFormat();

		QTextListFormat listFmt;

		if (cursor.currentList()) {
			listFmt = cursor.currentList()->format();
		} else {
			listFmt.setIndent(blockFmt.indent() + 1);
			blockFmt.setIndent(0);
			cursor.setBlockFormat(blockFmt);
		}

		listFmt.setStyle(style);

		cursor.createList(listFmt);

		cursor.endEditBlock();
	} else {
		// ####
		QTextBlockFormat bfmt;
		bfmt.setObjectIndex(-1);
		cursor.mergeBlockFormat(bfmt);
	}
	m_pTextEdit->setFocus();
}

void RichEditWidget::textColor()
{
	QColor col = QColorDialog::getColor(m_pTextEdit->textColor(), this);
	if (!col.isValid())
		return;
	m_pTextEdit->setTextColor(col);
	colorChanged(col);
}

void RichEditWidget::textAlign(QAction *a)
{
	if (a == m_pActionAlignLeft)
		m_pTextEdit->setAlignment(Qt::AlignLeft);
	else if (a == m_pActionAlignCenter)
		m_pTextEdit->setAlignment(Qt::AlignHCenter);
	else if (a == m_pActionAlignRight)
		m_pTextEdit->setAlignment(Qt::AlignRight);
	else if (a == m_pActionAlignJustify)
		m_pTextEdit->setAlignment(Qt::AlignJustify);
}

void RichEditWidget::textAlignLeft()
{
	textAlign(m_pActionAlignLeft);
}

void RichEditWidget::textAlignCenter()
{
	textAlign(m_pActionAlignCenter);
}

void RichEditWidget::textAlignRight()
{
	textAlign(m_pActionAlignRight);
}

void RichEditWidget::textAlignJustify()
{
	textAlign(m_pActionAlignJustify);
}

void RichEditWidget::clipboardDataChanged()
{
	bool bEnabled = !QApplication::clipboard()->text().isEmpty() ||	!QApplication::clipboard()->image().isNull();
	//qDebug() << "Enabled" << bEnabled;
	m_pActionPaste->setEnabled(bEnabled);
}

void RichEditWidget::fontChanged(const QFont &f)
{
	qDebug() << "RighEditWidget:fontChanged new font size " << f.pointSize();

	if(f.pointSize() == 9){
		int i=0;
	}

	m_pTextEdit->blockSignals(true);
	m_pComboFont->setCurrentIndex(m_pComboFont->findText(f.family()));
	//qDebug()<<QString::number(f.pointSize());
	m_pComboSize->setCurrentIndex(m_pComboSize->findText(QString::number(f.pointSize())));
	m_pActionTextBold->setChecked(f.bold());
	m_pActionTextItalic->setChecked(f.italic());
	m_pActionTextUnderline->setChecked(f.underline());
	m_pTextEdit->blockSignals(false);
}

void RichEditWidget::colorChanged(const QColor &c)
{
	QPixmap pix(16, 16);
	pix.fill(c);
	m_pActionTextColor->setIcon(pix);
}

void RichEditWidget::OpenInDialog()
{
	#ifndef WINCE
		m_strHtmlText = GetHtml();
		RichEditorDlg dialog;
		dialog.SetHtml(m_strHtmlText);
		dialog.SetEditMode(m_bEditOnly);

		if (dialog.exec())
		{
			//If html is shown, switch to normal mode and then get text.
			if (dialog.IsHTMLDisplayed())
				dialog.SwitchFromHTMLToNormalView();
			
			m_bInsideHTMLChange = true;
			SetHtml(dialog.GetHtml());
			m_bInsideHTMLChange = false;
		}
	#endif
}

//TRUE=edit, FALSE= view only
void RichEditWidget::SetEditMode(bool bEditOnly)
{
	m_bEditOnly = bEditOnly;

	m_pActionTextBold->setEnabled(m_bEditOnly);
	m_pActionTextUnderline->setEnabled(m_bEditOnly);
	m_pActionTextItalic->setEnabled(m_bEditOnly);
	m_pActionTextColor->setEnabled(m_bEditOnly);
	m_pActionAlignLeft->setEnabled(m_bEditOnly);
	m_pActionAlignCenter->setEnabled(m_bEditOnly);
	m_pActionAlignRight->setEnabled(m_bEditOnly);
	m_pActionAlignJustify->setEnabled(m_bEditOnly);
	m_pActionUndo->setEnabled(m_bEditOnly);
	m_pActionRedo->setEnabled(m_bEditOnly);
	m_pActionCut->setEnabled(m_bEditOnly);
	m_pActionCopy->setEnabled(m_bEditOnly);
	m_pActionPaste->setEnabled(m_bEditOnly);
	m_pActionOpenFile->setEnabled(m_bEditOnly);
	m_pActionSaveFile->setEnabled(m_bEditOnly);
	m_pActionSaveFileAs->setEnabled(m_bEditOnly);
	m_pActionInsertPicture->setEnabled(m_bEditOnly);

	m_pComboStyle->setEnabled(m_bEditOnly);
	m_pComboFont->setEnabled(m_bEditOnly);
	m_pComboSize->setEnabled(m_bEditOnly);

	//always enabled:
	if (!m_pTextEdit->isEnabled())
		m_pTextEdit->setEnabled(true);

	m_pTextEdit->setReadOnly(!m_bEditOnly);
}


void RichEditWidget::contextMenuEvent(QContextMenuEvent *event)
{
	QMenu cntxtMenu(this);

	cntxtMenu.addAction(m_pActionCut);
	cntxtMenu.addAction(m_pActionCopy);
	cntxtMenu.addAction(m_pActionPaste);
	cntxtMenu.addSeparator();
	cntxtMenu.addAction(m_pActionTextBold);
	cntxtMenu.addAction(m_pActionTextItalic);
	cntxtMenu.addAction(m_pActionTextUnderline);

	cntxtMenu.exec(event->globalPos());
}

void RichEditWidget::focusInEvent(QFocusEvent * event)
{
	m_pTextEdit->setFocus();
}

/*
void RichEditWidget::keyPressEvent(QKeyEvent *event)
{
	//qDebug() << "Key" << (int)event->key() << "modifiers" << event->modifiers();
	//qDebug() << "Text" << event->text();

	if (event->matches(QKeySequence::Find))
		on_SearchExtendenButton();
	QFrame::keyPressEvent(event);
}
*/

void RichEditWidget::OnTextChanged()
{
	m_pActionSaveFile->setEnabled(true);
	m_pActionSaveFileAs->setEnabled(true);
}

void RichEditWidget::currentCharFormatChanged(const QTextCharFormat &format)
{
	m_pTextEdit->blockSignals(true);

	if (m_pTextEdit->fontFamily()=="")
	{
		m_pTextEdit->setFontFamily(m_pComboFont->currentText());

		qDebug() << "RighEditWidget:currentCharFormatChanged new font size " << m_pComboSize->currentText().toFloat();
		m_pTextEdit->setFontPointSize(m_pComboSize->currentText().toFloat());
	}
	
	qDebug() << "RighEditWidget:currentCharFormatChanged fontChanged " << m_pTextEdit->currentFont().pointSize();
	fontChanged(m_pTextEdit->currentFont());
	colorChanged(format.foreground().color());

	m_pTextEdit->blockSignals(false);
}

void RichEditWidget::on_textChanged()
{
	emit textChanged();
}

void RichEditWidget::on_ShowHTMLSource()
{
	if (!m_bHTMLIsDisplayed)
	{
		//In case of strikeout, clear it.
		QTextCharFormat format = m_pTextEdit->currentCharFormat();
		if (format.fontStrikeOut())
		{
			format.setFontStrikeOut(false);
			m_pTextEdit->setCurrentCharFormat(format);
		}

		//show the HTML source view
		QString strText = GetHtml();
		if(m_bFixHTMLContents)
			HtmlFormatBeautifier(strText);

		m_pTextEdit->clear();
		//Set text size 10, black.
		m_pTextEdit->setFontPointSize(10);
		m_pTextEdit->setFontFamily("Arial");
		m_pTextEdit->setFontUnderline(false);
		m_pTextEdit->setFontItalic(false);
		m_pTextEdit->setFontWeight(QFont::Normal);
		QColor col(Qt::black);
		m_pTextEdit->setTextColor(col);
		colorChanged(col);
		m_pTextEdit->blockSignals(true);
		m_pTextEdit->insertPlainText(strText);
		m_pTextEdit->blockSignals(false);
		m_pTextEdit->setFocus();

		//BT: issue 2244
		m_pActionTextBold->setEnabled(false);
		m_pActionTextUnderline->setEnabled(false);
		m_pActionStrikeThrough->setEnabled(false);
		m_pActionTextItalic->setEnabled(false);
		m_pActionTextColor->setEnabled(false);
		m_pActionAlignLeft->setEnabled(false);
		m_pActionAlignCenter->setEnabled(false);
		m_pActionAlignRight->setEnabled(false);
		m_pActionAlignJustify->setEnabled(false);
		m_pActionUndo->setEnabled(false);
		m_pActionRedo->setEnabled(false);
		m_pActionCut->setEnabled(false);
		m_pActionPaste->setEnabled(false);
		m_pActionInsertPicture->setEnabled(false);
		m_pComboStyle->setEnabled(false);
		m_pComboFont->setEnabled(false);
		m_pComboSize->setEnabled(false);
		m_pStrikeThroughButton->setEnabled(false);
	}
	else
	{
		//BT: issue 2244
		m_pActionTextBold->setEnabled(true);
		m_pActionTextUnderline->setEnabled(true);
		m_pActionStrikeThrough->setEnabled(true);
		m_pActionTextItalic->setEnabled(true);
		m_pActionTextColor->setEnabled(true);
		m_pActionAlignLeft->setEnabled(true);
		m_pActionAlignCenter->setEnabled(true);
		m_pActionAlignRight->setEnabled(true);
		m_pActionAlignJustify->setEnabled(true);
		m_pActionUndo->setEnabled(true);
		m_pActionRedo->setEnabled(true);
		m_pActionCut->setEnabled(true);
		m_pActionPaste->setEnabled(true);
		m_pActionInsertPicture->setEnabled(true);
		m_pComboStyle->setEnabled(true);
		m_pComboFont->setEnabled(true);
		m_pComboSize->setEnabled(true);
		m_pStrikeThroughButton->setEnabled(true);

		//show back the HTML graphic view
		QString strText = GetPlainText();
		m_pTextEdit->blockSignals(true);
		m_pTextEdit->clear();
		m_bInsideHTMLChange = true;
		SetHtml(strText); //FIX: #2096 m_pTextEdit->insertHtml(strText);
		m_bInsideHTMLChange = false;
		m_pTextEdit->blockSignals(false);
		m_bHTMLIsDisplayed = true;	//will be changed below
		m_pTextEdit->setFocus();
	}

	m_bHTMLIsDisplayed = !m_bHTMLIsDisplayed;
	m_pShowHTMLSourceButton->setDown(m_bHTMLIsDisplayed);
	RefreshHtmlHighlight();

	if(m_bEmitSignal)
		emit EditViewSwitched();
}

void RichEditWidget::on_VerticalOpenClose(bool bEmitAndSetLocalVariable /*= true*/)
{
	if (m_bSmaller)
	{
		m_pActionVerticalOpenClose->setToolTip(tr("Smaller"));
		if (m_bInverseStretchArrow)
			m_pActionVerticalOpenClose->setIcon(QIcon(":Vertical_Close.png"));
		else
			m_pActionVerticalOpenClose->setIcon(QIcon(":Vertical_Open.png"));
	}
	else
	{
		m_pActionVerticalOpenClose->setToolTip(tr("Larger"));
		if (m_bInverseStretchArrow)
			m_pActionVerticalOpenClose->setIcon(QIcon(":Vertical_Open.png"));
		else
			m_pActionVerticalOpenClose->setIcon(QIcon(":Vertical_Close.png"));
	}


	if (bEmitAndSetLocalVariable)
	{
		m_bSmaller = !m_bSmaller;
		emit on_VerticalOpenClose_clicked(m_bSmaller);
	}
}

void RichEditWidget::on_SearchButton()
{
	SearchText(m_pFindLineEdit->text(), (QTextDocument::FindFlags)0, false);
}

void RichEditWidget::on_SearchExtendenButton()
{
#ifndef  WINCE
	if (!m_pSearchDialog) {
		m_pSearchDialog = new RichEditorSearchDialog(this);
		connect(m_pSearchDialog, SIGNAL(SearchText(QString, QTextDocument::FindFlags, bool)), this, SLOT(SearchText(QString, QTextDocument::FindFlags, bool)));
	}

	m_pSearchDialog->show();
	m_pSearchDialog->raise();
	m_pSearchDialog->activateWindow();
#endif
}
void RichEditWidget::on_findPreviousButton()
{
	SearchText(m_pFindLineEdit->text(), QTextDocument::FindBackward, false);
}

void RichEditWidget::on_exportToHtmlButton()
{
	OnSaveAsFile("html");
}

void RichEditWidget::on_exportToPdfButton()
{
	QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
	m_strFileName=QFileDialog::getSaveFileName(this, "Enter PDF file name", g_strLastDir_FileOpen, "PDF files (*.pdf)");
	if (m_strFileName.isNull())
	{
		QApplication::restoreOverrideCursor();
		return;
	}
	if (!m_strFileName.isEmpty())
	{
		QFileInfo fileInfo(m_strFileName);
		g_strLastDir_FileOpen=fileInfo.absolutePath();
	}

	//Check file extension.
	int extensionIndex = m_strFileName.lastIndexOf(".");
	QString extension = m_strFileName;
	extension.remove(0, extensionIndex);
	if (extension != ".pdf")
		m_strFileName.append(".pdf");

#ifndef WINCE
	//PRINT TO PDF.
	QPrinter *printer = new QPrinter();
	printer->setOutputFormat(QPrinter::PdfFormat);
	printer->setPageSize(QPrinter::A4);
	printer->setOrientation(QPrinter::Portrait);
	printer->setFullPage(true);
	printer->setOutputFileName(m_strFileName);	

	m_pTextDocument->print(printer);
#endif

	QApplication::restoreOverrideCursor();

}

void RichEditWidget::SaveToPdf(QString strFileName)
{
	//PRINT TO PDF.
	QPrinter *printer = new QPrinter();
	printer->setOutputFormat(QPrinter::PdfFormat);
	printer->setPageSize(QPrinter::A4);
	printer->setOrientation(QPrinter::Portrait);
	printer->setFullPage(true);
	printer->setOutputFileName(strFileName);	

	m_pTextDocument->print(printer);
}

void RichEditWidget::on_exportToOdfButton()
{
	OnSaveAsFile("odf");
}

void RichEditWidget::cursorPositionChanged()
{
	qDebug() << "RighEditWidget:cursorPositionChanged fontChanged " << m_pTextEdit->textCursor().charFormat().font() << m_pTextEdit->currentFont();
	fontChanged(m_pTextEdit->textCursor().charFormat().font());
	colorChanged(m_pTextEdit->textCursor().charFormat().foreground().color());
	alignmentChanged(m_pTextEdit->alignment());
}

void RichEditWidget::alignmentChanged(Qt::Alignment a)
{
	if (a & Qt::AlignLeft)
		m_pActionAlignLeft->setChecked(true);
	else if (a & Qt::AlignHCenter)
		m_pActionAlignCenter->setChecked(true);
	else if (a & Qt::AlignRight)
		m_pActionAlignRight->setChecked(true);
	else if (a & Qt::AlignJustify)
		m_pActionAlignJustify->setChecked(true);
}

void RichEditWidget::connectDocumnetToSignals(QTextDocument	*pTextDocument)
{
	connect(pTextDocument,			SIGNAL(undoAvailable(bool)),	m_pActionSaveFile,	SLOT(setEnabled(bool)));
	connect(pTextDocument,			SIGNAL(undoAvailable(bool)),	m_pActionSaveFileAs,SLOT(setEnabled(bool)));
	connect(pTextDocument,			SIGNAL(undoAvailable(bool)),	m_pActionUndo,		SLOT(setEnabled(bool)));
	connect(pTextDocument,			SIGNAL(redoAvailable(bool)),	m_pActionRedo,		SLOT(setEnabled(bool)));
	connect(m_pActionRedo,			SIGNAL(triggered()),			pTextDocument,		SLOT(redo()));
	connect(m_pActionUndo,			SIGNAL(triggered()),			pTextDocument,		SLOT(undo()));
}

void RichEditWidget::disconnectDocumnetFromSignals()
{
	disconnect(m_pTextDocument,			SIGNAL(undoAvailable(bool)),	m_pActionSaveFile,	SLOT(setEnabled(bool)));
	disconnect(m_pTextDocument,			SIGNAL(undoAvailable(bool)),	m_pActionSaveFileAs,SLOT(setEnabled(bool)));
	disconnect(m_pTextDocument,			SIGNAL(undoAvailable(bool)),	m_pActionUndo,		SLOT(setEnabled(bool)));
	disconnect(m_pTextDocument,			SIGNAL(redoAvailable(bool)),	m_pActionRedo,		SLOT(setEnabled(bool)));
	disconnect(m_pActionRedo,			SIGNAL(triggered()),			m_pTextDocument,	SLOT(redo()));
	disconnect(m_pActionUndo,			SIGNAL(triggered()),			m_pTextDocument,	SLOT(undo()));
}

void RichEditWidget::AddJournalPrintingHeader()
{
	//Char formats first.
	QTextCharFormat charFormatFirstRow;
	charFormatFirstRow.setFontFamily("Arial");
	charFormatFirstRow.setFontPointSize(14);
	charFormatFirstRow.setFontItalic(true);
	charFormatFirstRow.setTextOutline(QPen(QColor(85,0,255)));

	QTextCharFormat charFormatSecondRow;
	charFormatSecondRow.setFontFamily("Arial");
	charFormatSecondRow.setFontPointSize(8);
	charFormatSecondRow.setTextOutline(QPen(QColor(85,0,255)));
	
	//Text inserting.
	QTextCursor curs(m_pTextEdit->textCursor());
	curs.beginEditBlock();
	curs.movePosition(QTextCursor::Start);

	curs.insertBlock();
	curs.movePosition(QTextCursor::PreviousBlock);

	curs.insertText(tr("Journal Entry for "), charFormatFirstRow);

	//Insert contact.
	curs.insertText(m_lstPrintingHeaderData.value(0), charFormatFirstRow);
	
	curs.insertText("\n");
	
	//Check is date supplied.
	if (!m_lstPrintingHeaderData.value(1).isEmpty())
	{
		curs.insertText(tr("Date: "), charFormatSecondRow);
		curs.insertText(m_lstPrintingHeaderData.value(1), charFormatSecondRow);
		curs.insertText("\t\t");
	}

	//Check is person supplied.
	if (!m_lstPrintingHeaderData.value(2).isEmpty())
	{
		curs.insertText(tr("By: "), charFormatSecondRow);
		curs.insertText(m_lstPrintingHeaderData.value(2), charFormatSecondRow);
	}
	
	curs.insertText("\n");
	curs.endEditBlock();
}

void RichEditWidget::AddNotePrintingHeader()
{
	//Char formats first.
	QTextCharFormat charFormatFirstRow;
	charFormatFirstRow.setFontFamily("Arial");
	charFormatFirstRow.setFontPointSize(14);
	charFormatFirstRow.setFontItalic(true);
	charFormatFirstRow.setTextOutline(QPen(QColor(85,0,255)));

	QTextCharFormat charFormatSecondRow;
	charFormatSecondRow.setFontFamily("Arial");
	charFormatSecondRow.setFontPointSize(11);
	charFormatSecondRow.setTextOutline(QPen(QColor(85,0,255)));

	QTextCharFormat charFormatSecondRowBoldItalic;
	charFormatSecondRowBoldItalic.setFontFamily("Arial");
	charFormatSecondRowBoldItalic.setFontPointSize(11);
	charFormatSecondRowBoldItalic.setFontWeight(QFont::Bold);
	charFormatSecondRowBoldItalic.setFontItalic(true);
	charFormatSecondRowBoldItalic.setTextOutline(QPen(QColor(85,0,255)));
	
	QTextCharFormat charFormatLastRow;
	charFormatLastRow.setFontFamily("Arial");
	charFormatLastRow.setFontPointSize(8);
	charFormatLastRow.setTextOutline(QPen(QColor(85,0,255)));
	
	//Text inserting.
	QTextCursor curs(m_pTextEdit->textCursor());
	curs.beginEditBlock();
	curs.movePosition(QTextCursor::Start);

	curs.insertBlock();
	curs.movePosition(QTextCursor::PreviousBlock);

	curs.insertText(tr("Note: "), charFormatFirstRow);

	//Insert name.
	curs.insertText(m_lstPrintingHeaderData.value(0), charFormatFirstRow);
	curs.insertText("\n");
	
	//Insert contact.
	if (!m_lstPrintingHeaderData.value(1).isEmpty())
	{
		curs.insertText("Kontakt: ", charFormatSecondRowBoldItalic);
		curs.insertText(m_lstPrintingHeaderData.value(1), charFormatSecondRow);
		curs.insertText("\n");
	}
	
	//Insert project.
	if (!m_lstPrintingHeaderData.value(2).isEmpty())
	{
		curs.insertText("Project: ", charFormatSecondRowBoldItalic);
		curs.insertText(m_lstPrintingHeaderData.value(2), charFormatSecondRow);
		curs.insertText("\n");
	}

	//Check is date supplied.
	if (!m_lstPrintingHeaderData.value(3).isEmpty())
	{
		curs.insertText(tr("Date: "), charFormatLastRow);
		curs.insertText(m_lstPrintingHeaderData.value(3), charFormatLastRow);
		curs.insertText("\t\t");
	}

	//Check is person supplied.
	if (!m_lstPrintingHeaderData.value(4).isEmpty())
	{
		curs.insertText(tr("By: "), charFormatLastRow);
		curs.insertText(m_lstPrintingHeaderData.value(4), charFormatLastRow);
	}

	curs.insertText("\n");
	curs.endEditBlock();
}

//For voice calls:
//0 - Incoming or Outgoing, 1 - strDate, 2 - strContact, 3 - strPhoneNumber, 4 - strOwner, 5 - strStartTime, 6 - strEndTime, 
//7 - strDurationTime, 8 - strProject.
void RichEditWidget::AddPhoneCallPrintingHeader()
{
	//Char formats first.
	QTextCharFormat charFormatFirstRow;
	charFormatFirstRow.setFontFamily("Helvetica");
	charFormatFirstRow.setFontPointSize(17);

	QTextCharFormat charFormatSecondRowBoldItalic;
	charFormatSecondRowBoldItalic.setFontFamily("Helvetica");
	charFormatSecondRowBoldItalic.setFontPointSize(11);
	charFormatSecondRowBoldItalic.setFontItalic(true);
	charFormatSecondRowBoldItalic.setFontWeight(QFont::Bold);

	QTextCharFormat charFormatSecondRow;
	charFormatSecondRowBoldItalic.setFontFamily("Helvetica");
	charFormatSecondRowBoldItalic.setFontPointSize(11);

	//Text inserting.
	QTextCursor curs(m_pTextEdit->textCursor());
	curs.beginEditBlock();
	curs.movePosition(QTextCursor::Start);

	curs.insertBlock();
	curs.movePosition(QTextCursor::PreviousBlock);

	//Insert incoming or outgoing with date.
	curs.insertText(m_lstPrintingHeaderData.value(0), charFormatFirstRow);
	curs.insertText(" ", charFormatFirstRow);
	curs.insertText(m_lstPrintingHeaderData.value(1), charFormatFirstRow);
	curs.insertText("\n");

	//Insert contact.
	if (!m_lstPrintingHeaderData.value(2).isEmpty())
	{
		curs.insertText(tr("Contact: "), charFormatSecondRowBoldItalic);
		curs.insertText("\t");
		curs.insertText(m_lstPrintingHeaderData.value(2), charFormatSecondRow);
		curs.insertText("\n");
	}

	//Insert phone number.
	if (!m_lstPrintingHeaderData.value(3).isEmpty())
	{
		curs.insertText(tr("Number: "), charFormatSecondRowBoldItalic);
		curs.insertText("\t");
		curs.insertText(m_lstPrintingHeaderData.value(3), charFormatSecondRow);
		curs.insertText("\n");
	}
	
	//Insert owner.
	if (!m_lstPrintingHeaderData.value(4).isEmpty())
	{
		curs.insertText(tr("Owner: "), charFormatSecondRowBoldItalic);
		curs.insertText("\t");
		curs.insertText(m_lstPrintingHeaderData.value(4), charFormatSecondRow);
		curs.insertText("\n");
	}

	//Insert date, time, duration.
	if (!m_lstPrintingHeaderData.value(1).isEmpty() || !m_lstPrintingHeaderData.value(5).isEmpty() || !m_lstPrintingHeaderData.value(6).isEmpty() ||
		!m_lstPrintingHeaderData.value(7).isEmpty())
	{
		curs.insertText(tr("Time: "), charFormatSecondRowBoldItalic);
		curs.insertText("\t");
		//Insert time.
		if (!m_lstPrintingHeaderData.value(1).isEmpty())
		{
			curs.insertText(m_lstPrintingHeaderData.value(1), charFormatSecondRow);
			curs.insertText(" ", charFormatSecondRow);
		}

		//Insert start time.
		if (!m_lstPrintingHeaderData.value(5).isEmpty())
			curs.insertText(m_lstPrintingHeaderData.value(5), charFormatSecondRow);

		//Insert end time.
		if (!m_lstPrintingHeaderData.value(6).isEmpty())
		{
			curs.insertText("-", charFormatSecondRow);
			curs.insertText(m_lstPrintingHeaderData.value(6), charFormatSecondRow);
		}

		//Insert duration.
		if (!m_lstPrintingHeaderData.value(7).isEmpty())
		{
			curs.insertText(", ", charFormatSecondRow);
			curs.insertText(m_lstPrintingHeaderData.value(7), charFormatSecondRow);
		}
		curs.insertText("\n");
	}
	
	//Insert project.
	if (!m_lstPrintingHeaderData.value(8).isEmpty())
	{
		curs.insertText(tr("Project: "), charFormatSecondRowBoldItalic);
		curs.insertText("\t");
		curs.insertText(m_lstPrintingHeaderData.value(8), charFormatSecondRow);
		curs.insertText("\n");
	}
	
	curs.endEditBlock();
}

void RichEditWidget::SaveDoc(QString strFormat)
{
	QTextDocumentWriter writer(m_strFileName);
	writer.setFormat(strFormat.toLocal8Bit());
	if (!writer.write(m_pTextDocument))
	{
		QMessageBox::information(this, tr("Write Error!"), tr("Unable to write to desired file!"));
		return;
	}
	m_pTextEdit->document()->setModified(false);
}

void RichEditWidget::SearchText(QString strText, QTextDocument::FindFlags findFlags, bool bUseRegex)
{
	m_nFindFlags = findFlags;

	QTextCursor cursor;
	if (bUseRegex)
	{
		QRegExp regex(strText);
		cursor = m_pTextDocument->find(regex, m_pTextEdit->textCursor(), m_nFindFlags);
	}
	else
	{
		cursor = m_pTextDocument->find(strText, m_pTextEdit->textCursor(), m_nFindFlags);
	}
	if (cursor.isNull())
		m_pTextEdit->textCursor().clearSelection();
	else
		m_pTextEdit->setTextCursor(QTextCursor(cursor));
	
	m_pTextEdit->setFocus();
}

void RichEditWidget::InsertPicture()
{
	QString FileName = QFileDialog::getOpenFileName(this, tr("Open File..."),
		g_strLastDir_FileOpen, tr("Image Files (*.jpg *.png *.gif);;All Files (*)"));

	if (!FileName.isEmpty())
	{
		QFileInfo fileInfo(FileName);
		g_strLastDir_FileOpen=fileInfo.absolutePath();

		if (!QFile::exists(FileName))
			return;

		//
		QImage image;
		if(image.load(FileName))
		{
			/*
			QTextCursor curs(m_pTextEdit->textCursor());
			curs.beginEditBlock();
			curs.insertImage(image, FileName);
			//curs.insertImage(FileName);
			curs.endEditBlock();
			*/

			//#2169: MB requests all pics to be converted to JPEG
			PastePictureAsJpeg(image);

			g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Inserted picture from file [%1]").arg(FileName));
		}
		else
			QMessageBox::information(this, tr("Error"), tr("Failed to load picture from '%1'!").arg(FileName));
	}
}

void RichEditWidget::on_paste()
{
	//QMessageBox::information(NULL, "a", "on_paste");
	QString strFormatHTML("html");
	QString strFormatText("text");

	if(!QApplication::clipboard()->text(strFormatHTML).isEmpty()){
		//QMessageBox::information(NULL, "a", "paste html");

		QString strHtmlData = QApplication::clipboard()->text(strFormatHTML);
		QStringList lstResOrigFiles, lstTempFiles;
		EmailHelper::EmbedRemoteHtmlPix(strHtmlData, lstResOrigFiles, lstTempFiles);
		m_lstTempPixFiles += lstTempFiles;

		//QMessageBox::information(NULL, "a", strHtmlData);

		//paste the transformed HTML
		m_pTextEdit->insertHtml(strHtmlData);
		m_pTextEdit->setFocus();
	}
	else if(!QApplication::clipboard()->text(strFormatText).isEmpty()){
		//QMessageBox::information(NULL, "a", "paste text");

		m_pTextEdit->paste();
	}
	else if(!QApplication::clipboard()->image().isNull())
	{
		//QMessageBox::information(NULL, "a", "paste jpg");
		QImage img = QApplication::clipboard()->image();
		PastePictureAsJpeg(img);
	}
	else if(!QApplication::clipboard()->text().isEmpty()){
		//QMessageBox::information(NULL, "a", "paste text");
		m_pTextEdit->paste();
	}
	//else
	//	QMessageBox::information(NULL, "a", "no formats found");
}

void RichEditWidget::on_StrikeThrough()
{
	QTextCharFormat format = m_pTextEdit->currentCharFormat();
	format.setFontStrikeOut((m_pActionStrikeThrough->isChecked() ? true : false));
	m_pTextEdit->setCurrentCharFormat(format);
}

bool RichEditTextView::event ( QEvent * event )
{
    if (event->type() == QEvent::ShortcutOverride) {
        QKeyEvent *ke = (QKeyEvent *)event;
		if (ke == QKeySequence::Paste){
			//m_pPasteAction->setEnabled(!QApplication::clipboard()->text().isEmpty() ||	!QApplication::clipboard()->image().isNull());
			//m_pPasteAction->trigger();
			//qDebug() << "Event " << event->isAccepted();
            return false;
		}
    }
    return QTextBrowser::event(event);
}

void RichEditWidget::PastePictureAsJpeg(QImage &image)
{
	//issue #2429, convert transparent PNG to JPG with white bkg
	if(image.hasAlphaChannel()){
		//replace transparency with white
		QImage image2(image.size(), image.format());
		image2.fill(QColor(Qt::white).rgb());
		QPainter painter(&image2);
		painter.drawImage(0, 0, image);
		image = image2;
	}

	// save the image contents into the temp file
	QString strName = "SokratesImage.jpg";
	QFileInfo attInfo(strName);
	QString strTmpPath;
	//generate unique temp name
	int nTry = 0; bool bOK = false;
	while(nTry < 500 && !bOK){
		strTmpPath = QDir::tempPath();
		strTmpPath += "/";
		strTmpPath += attInfo.completeBaseName();
		strTmpPath += QVariant(nTry).toString();
		strTmpPath += ".";
		strTmpPath += attInfo.completeSuffix();
		nTry ++;
		bOK = !QFile::exists(strTmpPath);
	}
	if(!bOK){
		Q_ASSERT(false);	//could not create unused temp path
		return;
	}

	if(image.save(strTmpPath, "JPG"))
	{
		//paste the picture into the widget
		QTextCursor curs(m_pTextEdit->textCursor());
		curs.beginEditBlock();
		curs.insertImage(strTmpPath);
		curs.endEditBlock();

		m_lstTempPixFiles.push_back(strTmpPath);
	}
}

void RichEditWidget::HtmlFormatBeautifier(QString &strData)
{
	//FIX: this algorithm f*cks up the FixRtfEmail algorithm, so perform the FixRtfEmail action before it
	//return;
	PostprocessFixRtfEmail(strData);

/*
	//quick and dirty HTML formatter
	int nDepth = 0;
	int nLength = strData.length();
	for(int i=0; i<nLength; i++)
	{
		if('<' == strData.at(i))	//tag start
		{
			int nTagEnd = strData.indexOf('>', i+1);
			if(nTagEnd > 0)
			{
				bool bIsClosingTag = ((i < nLength-1 && strData.at(i+1) == '/') || strData.at(nTagEnd-1) == '/');

				//extract tag name and parameters
				QString strTagData = strData.mid(i+1, nTagEnd-i-1);
				QString strTagName = strTagData;
				int nEnd = strTagName.indexOf(' ');
				if(nEnd > 0)
					strTagName = strTagName.mid(0, nEnd);

				//TOFIX implement indentation
				//TOFIX only some tags increase indentation level

				//do we need to break to the new line
				if((bIsClosingTag || strTagName == "span") && nTagEnd < nLength-1)
				{
					if(strData.at(nTagEnd+1) != '\r' &&
					   strData.at(nTagEnd+1) != '\n')
					{
						strData.insert(nTagEnd+1, "\r\n");
						nLength += 2;
						i = nTagEnd+2;
					}
				}
				else
					i = nTagEnd;
			}
		}
	}
*/
}

void StripHtmlTagParameter(QString &strData, const char *szTagOpen, const char *szTagClose, const char *szParameter)
{
	QString strOpen(szTagOpen);
	strOpen.chop(1);				//unterminated tag to be searched ("<ul")	
	QString strParam(szParameter);	//"style=\""
	strParam += "=\"";

	int nStart = strData.indexOf(strOpen);
	while(nStart >= 0){
		int nEnd = strData.indexOf(szTagClose, nStart);
		if(nEnd >= 0){
			int nStyleStart = strData.indexOf(strParam, nStart);
			if(nStyleStart >= 0 && nStyleStart < nEnd){
				int nStyleEnd = strData.indexOf("\"", nStyleStart+strParam.length());
				strData.remove(nStyleStart, nStyleEnd+1-nStyleStart);
			}
		}
		nStart = strData.indexOf(strOpen, nStart+3);
	}
}


//QT's TextView "polutes" html tags (adds content that makes Outlook display ugly)
//convert the Qt dialect to something usable
void RichEditWidget::PostprocessFixRtfEmail(QString &strEmail)
{
	const char *szQtHeader = "<head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\np, li { white-space: pre-wrap; }\n</style>";
	const char *szFixedHeader = "<head><meta name=\"qrichtext\" content=\"1\"><style type=\"text/css\">\np, li, span, td, tr, div {font: Arial; white-space: pre-wrap; }\n</style></head>";
	strEmail.replace(szQtHeader, szFixedHeader);

	//2012.12.18. MB's request to fix table styling for client with some template email
	int nStart = strEmail.indexOf("<td");
	while(nStart >= 0){
		int nEnd = strEmail.indexOf(">", nStart);
		if(nEnd > 0){
			nStart += 3;	//point behind "<td"
			QString strContent = strEmail.mid(nStart, nEnd-nStart);
			int nStyleStartRel = strContent.indexOf("style=");
			if(nStyleStartRel < 0)
			{
				//insert missing style
				strEmail.insert(nStart, " style=\"font-family: Arial;\"");
				nStart += 10;
			}
			else
			{
				//extract exiting style
				int nQuoteIdx = strContent.indexOf("\"", nStyleStartRel);
				int nSingleQuoteIdx = strContent.indexOf("'", nStyleStartRel);
				QChar cDelimiter = '\"';
				int nStyleContentStart = nQuoteIdx;
				if(nSingleQuoteIdx>=0){
					if(nQuoteIdx < 0 || nSingleQuoteIdx < nQuoteIdx){
						cDelimiter = '\'';
						nStyleContentStart = nSingleQuoteIdx;
					}
				}
				int nStyleContentEnd = strContent.indexOf(cDelimiter, nStyleContentStart+1);
				QString strStyle = strContent.mid(nStyleContentStart+1, nStyleContentEnd-nStyleContentStart-1);

				//if font family is missing, add default one
				if(strStyle.indexOf("font-family") < 0)
				{
					strEmail.insert(nStart + nStyleContentStart + 1, "font-family: Arial; ");
				}
			}
		}
		else{
			Q_ASSERT(false);
			nStart += 3;
		}

		nStart = strEmail.indexOf("<td", nStart+3);
	}

	QString strBr = "<br />"; //"<p />"; //"<br>"; //"<br />"
	//first kill all BRs, assuming that widget does not use them
	//strEmail.replace(strBr, "");

	//MB: issue 1954
	strEmail.replace("<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"></p>", strBr);
	strEmail.replace("<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; color:#000000;\"></p>", strBr);
	strEmail.replace("<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:small; color:#000000;\"></p>", strBr);
	strEmail.replace("<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:small; color:#000000;\"> </span></p>", strBr);
	strEmail.replace("<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:10pt;\"> </span> </p>", strBr);
	//<p style="-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt; color:#000000;"></p>
	//<p style="*"></p>   ---> "<br />"
	//[^<>/] part matches anything except "<", ">" or "/"
	//QRegExp regex("<p style=\"[^<>]\"></p>");
	//strEmail.replace(regex, "<br />");

	nStart = strEmail.indexOf("<p style=");
	while(nStart >= 0){
		int nEnd = strEmail.indexOf("</p>", nStart);
		int nEndEmpty = strEmail.indexOf("></p>", nStart);
		if(nEndEmpty < nEnd && nEndEmpty > 0){
			int nEndStartTag = strEmail.indexOf(">", nStart);
			if(nEndStartTag == nEndEmpty)			//no tags in the middle
			{
				strEmail.remove(nStart, nEndEmpty-nStart+strlen("></p>"));
				strEmail.insert(nStart, strBr);
			}
		}
		nStart = strEmail.indexOf("<p style=", nStart+6);
	}

	strEmail.replace("<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:small; color:#000000;\"> </span></p>", "<br />");

	//MB: issue 472
	strEmail.replace(" font-size:small;", "");
	strEmail.replace(" margin-top:12px;", " margin-top:0px;");
	strEmail.replace(" margin-bottom:12px;", " margin-bottom:0px;");
	strEmail.replace(" \r", "<br>");
	//"�  " -> "<br>"

	//MB: issue 2393
	strEmail.replace("-qt-block-indent:0", "");

	strEmail.replace("<span style=\"\">�</span>", "<span style=\" font-size:small;\">&nbsp;</span>");

	ushort nQuote = 0x2019;	//"�"
	strEmail.replace(QChar(nQuote), '\'');		//FIX problems with quotes used instead of apostrophe, they get converted to ? by Outlook after editing in Sokrates

	//another chemistry, replace:
	//<p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; ; text-indent:0px;"><br /><br /><br /></p>
	//back to:
	//<br /><br /><br />
	QString strReplaceStr = "<br />"; //"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; text-indent:0px;\"><span style=\" font-size:small; color:#000000;\"> </span></p>\n";
	int nReplaceLen = strReplaceStr.length();
	int nBrLen = strlen("<br />");
	nStart = strEmail.indexOf("<p ");
	while(nStart >= 0){
		int nBrCnt = 0;
		int nEndStartTag = strEmail.indexOf(">", nStart);
		int nPos = nEndStartTag + 1;
		while(strEmail.mid(nPos, nBrLen) == strBr){
			nBrCnt ++;
			nPos += nBrLen;
		}
		int nEnd = strEmail.indexOf("</p>", nStart);
		if(nEnd < 0)
			break;
		if(nBrCnt > 0)
		{
			strEmail.remove(nStart, nEnd+strlen("</p>")-nStart);
			for(int i=0; i<nBrCnt; i++)
				strEmail.insert(nStart, strReplaceStr);	//strBr

			//search next
			nStart = strEmail.indexOf("<p style=", nStart+(nBrCnt*nReplaceLen));
		}
		else{
			//search next
			nStart = strEmail.indexOf("<p style=", nEnd+strlen("</p>"));
		}
	}

	//fix bulleted lists (Qt ones are not displayed in OE and Outlook when using direct send)
	StripHtmlTagParameter(strEmail, "<ul>", "</ul>", "style");
	StripHtmlTagParameter(strEmail, "<ol>", "</ol>", "style");
}

void RichEditWidget::PreprocessFixRtfEmail(QString &strEmail)
{
	//when streaming HTML back to QTextBrowser widget, modify HTML to use Qt's HTML dialect
	strEmail.replace("<br />", "<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; text-indent:0px;\"></p>");
	strEmail.replace("<span style=\"\"> </span>", "<span style=\" font-size:small;\">&nbsp;</span>");

	QString strBr = "<br />"; //"<p />"; //"<br>"; //"<br />"

	//another chemistry, replace:
	//<p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; ; text-indent:0px;"><br /><br /><br /></p>
	//back to:
	//<br /><br /><br />
	QString strReplaceStr = "<br />"; //"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; text-indent:0px;\"><span style=\" font-size:small; color:#000000;\"> </span></p>\n";
	int nReplaceLen = strReplaceStr.length();
	int nBrLen = strlen("<br />");
	int nStart = strEmail.indexOf("<p ");
	while(nStart >= 0){
		int nBrCnt = 0;
		int nEndStartTag = strEmail.indexOf(">", nStart);
		int nPos = nEndStartTag + 1;
		while(strEmail.mid(nPos, nBrLen) == strBr){
			nBrCnt ++;
			nPos += nBrLen;
		}
		int nEnd = strEmail.indexOf("</p>", nStart);
		if(nEnd < 0)
			break;
		if(nBrCnt > 0)
		{
			strEmail.remove(nStart, nEnd+strlen("</p>")-nStart);
			for(int i=0; i<nBrCnt; i++)
				strEmail.insert(nStart, strReplaceStr);	//strBr

			//search next
			nStart = strEmail.indexOf("<p style=", nStart+(nBrCnt*nReplaceLen));
		}
		else{
			//search next
			nStart = strEmail.indexOf("<p style=", nEnd+strlen("</p>"));
		}
	}
}
