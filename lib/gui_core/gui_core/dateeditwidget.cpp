#include "dateeditwidget.h"
#include "common/common/datahelper.h"
#include <QHBoxLayout>
#include <QPushButton>
#include "gui_core/gui_core/dateselectordlg.h"

DateEditWidget::DateEditWidget(QWidget *parent)
	: QWidget(parent)
{
	

	QSize size(21,21);
	QHBoxLayout *hbox= new QHBoxLayout;
	m_lineEdit= new QLineEdit;
	QPushButton* btnEditDate= new QPushButton;
	QSizePolicy policy(QSizePolicy::Expanding,QSizePolicy::Fixed);
	m_lineEdit->setSizePolicy(policy);
	m_lineEdit->setMaxLength(18);

	btnEditDate->setMaximumSize(size);
	btnEditDate->setMinimumSize(size);
	hbox->addWidget(m_lineEdit);
	hbox->addWidget(btnEditDate);
	hbox->setSpacing(0);
	hbox->setMargin(0);
	this->setLayout(hbox);

	btnEditDate->setIcon(QIcon(":DatePicker.png"));
	btnEditDate->setToolTip(tr("Select Date"));

	connect(btnEditDate,SIGNAL(clicked()),this,SLOT(OnDateEdit()));
	connect(m_lineEdit,SIGNAL(editingFinished()),this,SLOT(ValidateDate()));
}

DateEditWidget::~DateEditWidget()
{
}

/*!
	Validate if text is in date format. If not MsgBox, else emit DateChanged signal
*/
void DateEditWidget::ValidateDate()
{
	//if empty, ok, emit empty date (NULL)
	if(m_lineEdit->text().isEmpty()) 
	{
		emit EmitDateChanged(QDate()); //emit empty date
		return;
	}
	
	//try to convert:
	QDate datTemp=DataHelper::ParseDateString(m_lineEdit->text());

	//if failed, undo changes, fire error, else, emit OK
	if(datTemp.isNull())
	{
		QMessageBox::critical(NULL,tr("Error"),tr("Invalid Date!"),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
		m_lineEdit->undo();
	}
	else
		emit EmitDateChanged(datTemp);
}

//on click edit
void DateEditWidget::OnDateEdit()
{
	DateSelectorDlg Dlg;
	QDate dtSelected(getDate());
	Dlg.setDate(dtSelected);
	
	if (Dlg.exec())
	{
		dtSelected = Dlg.getDate();
		//qDebug()<<Dlg.getDate();
		setDate(dtSelected);
	}
}

QDate DateEditWidget::getDate()
{
	return DataHelper::ParseDateString(m_lineEdit->text());
}

void DateEditWidget::setDate(QDate &date)
{
	m_lineEdit->setText(date.toString(Qt::LocalDate));
	EmitDateChanged(date); //must refresh data
}