#ifndef DATERANGESELECTORMODALDIALOG_H
#define DATERANGESELECTORMODALDIALOG_H

#include <QWidget>
#include <QSpacerItem>
#include "generatedfiles/ui_daterangeselectormodaldialog.h"

class DateRangeSelectorModalDialog : public QWidget
{
	Q_OBJECT

public:
	DateRangeSelectorModalDialog(QWidget *parent = 0);
	~DateRangeSelectorModalDialog();

	void Initialize(QDateTime &dateFrom, QDateTime &dateTo, int nRange, bool bFromCalendar = false);
	void DisableDateRange(int nDateRangeID);
signals:
	void onDateChanged(QDateTime &dtFrom, QDateTime &dtTo, int nRange, bool bResizeColumns);

private:
	Ui::DateRangeSelectorModalDialog ui;

	void setupDateWidgets(QDateTime &dateFrom, QDateTime &dateTo, int nRange);
	void SendDateChangedSignal(QDate datFrom, QDate datTo);

	bool m_bSideBarMode;
	bool m_bFromCombo;
	bool m_bFromCalendar;
	QSpacerItem *m_pSpacer;

private slots:
	void on_btnPrev_clicked();
	void on_btnNext_clicked();
	void on_btnCurDateRange_clicked();
	void OnComboChanged(int nIdx);
	void on_btnModalSelector_clicked();

	void onAccepted(QDate dtFrom, QDate dtTo, int nRange);
};

#endif // DATERANGESELECTORMODALDIALOG_H
