#include "universaltablefindwidget.h"
#include <QLayout>
#include "gui_core/gui_core/thememanager.h"

UniversalTableFindWidget::UniversalTableFindWidget(QWidget *parent)
    : QWidget(parent)
{

}

UniversalTableFindWidget::~UniversalTableFindWidget()
{
	m_pTableWidget = NULL;
}

/*!
Initialize find widget with pointer to Universal table widget.

\param pTableWidget		  - Pointer to Universal table widget.
*/
void UniversalTableFindWidget::Initialize(QTableWidget *pTableWidget, bool bSideBarMode,QString strStyleSheet, int nSearchedColumn/*, DbRecordSet *pLstData, int nItemTextColIdx, int nOrganizationColIdx*/)
{
	//Set locals.
	m_pTableWidget	= pTableWidget;

	//Create combo box and put it in layout.
	m_pFindCombo	= new TableFindComboBox;
	m_pFindCombo->Initialize(m_pTableWidget,nSearchedColumn); //,pLstData,nItemTextColIdx,nOrganizationColIdx);

	//Find label.
	m_pFindLabel = new QLabel(tr("Find:"));

	//Setup layout.
	QFrame *frame=NULL;

	//issue 1662:
	//if (bSideBarMode)
	//{
		frame = new QFrame(this);
		frame->setFrameShape(QFrame::WinPanel);
		frame->setFrameShadow(QFrame::Sunken);
		frame->setStyleSheet(strStyleSheet+" "+ThemeManager::GetGlobalWidgetStyle());
	//}


	QHBoxLayout *layout = new QHBoxLayout;
	layout->addSpacing(2);
	layout->addWidget(m_pFindLabel, 0);
	layout->addWidget(m_pFindCombo, 10);
	layout->setContentsMargins(1,1,1,1);
	layout->setSpacing(2);

	if (frame)
	{
		frame->setLayout(layout);
		QHBoxLayout *layout2 = new QHBoxLayout;
		layout2->addWidget(frame);
		layout2->setContentsMargins(0,0,0,0);
		layout2->setSpacing(0);
		setLayout(layout2);
	}
	else
		setLayout(layout);

	//setStyleSheet(ThemeManager::GetGlobalWidgetStyle());
}
