#include "biginputdialog.h"

biginputdialog::biginputdialog(QString strTitle, QString strLabel, QWidget *parent,QString strDefaultValue)
    : QDialog(parent)
{
	ui.setupUi(this);

	setWindowTitle(strTitle);
	ui.txtLabel->setText(strLabel);
	ui.lineEdit->setText(strDefaultValue);

#ifdef WINCE
	setMaximumWidth(240);
#endif //WINCE
}

biginputdialog::~biginputdialog()
{
}

void biginputdialog::SetPasswordMode()
{
	ui.lineEdit->setEchoMode(QLineEdit::Password);
}

void biginputdialog::on_btnOK_clicked()
{
	m_strInput = ui.lineEdit->text();
	done(1);
}


void biginputdialog::on_btnCancel_clicked()
{
	done(0);
}