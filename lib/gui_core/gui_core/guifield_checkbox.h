#ifndef GUIFIELD_CHECKBOX_H
#define GUIFIELD_CHECKBOX_H

#include "guifield.h"
#include <QCheckBox>




/*!
	\class GuiField_CheckBox
	\brief Defines CheckBox
	\ingroup GUICore_GUIFields

	Connects to datasource (int or bool): 0 - unchecked, 1- checked
*/
class GuiField_CheckBox : public GuiField
{
	Q_OBJECT

public:
	GuiField_CheckBox(QString strFieldName,QWidget *pWidget,GuiDataManipulator* pDataManager,DbView *TableView=NULL, QLabel *pLabel=NULL);

	void RefreshDisplay(); //DATA SOURCE -> UI

private:
	QCheckBox* m_pChkBoxWidget;
	

public slots:
	void FieldChanged(); //UI -> DATA SOURCE

};

#endif // GUIFIELD_CHECKBOX_H


