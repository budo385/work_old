#include "multisortcolumnframe.h"
#include "multisorttable.h"
#include "multisortlist.h"
#include "common/common/entity_id_collection.h"

MultiSortColumnFrame::MultiSortColumnFrame(QWidget *parent)
	: QFrame(parent)
{
	ui.setupUi(this);
}

MultiSortColumnFrame::~MultiSortColumnFrame()
{

}

/*!
	Initializes multisort window with all columns that are available for
	multicolumn sort:

	\param lstColumns		- list in MVIEW_GRID_COLUMN_SETUP format
	\param lstDataSource	- data source list
	
*/
void MultiSortColumnFrame::SetColumnSetup(DbRecordSet &lstColumns)
{
	ui.listWidget->SetData(lstColumns);
	ui.tableWidget->Initialize(&m_lstColumnsForSort);
}

void MultiSortColumnFrame::SetColumnSetup(DbRecordSet &lstColumns, DbRecordSet &lstColumnsForSort)
{
	m_lstColumnsForSort = lstColumnsForSort;
	ui.listWidget->SetData(lstColumns);
	ui.tableWidget->InitializeForCommGrid(&m_lstColumnsForSort);
}

void MultiSortColumnFrame::SetSortColumnSetup(DbRecordSet &lstColumnsForSort)
{
	m_lstColumnsForSort = lstColumnsForSort;
	ui.tableWidget->InitializeForCommGrid(&m_lstColumnsForSort);
}

void MultiSortColumnFrame::on_btnRemoveColumn_clicked()
{
	ui.tableWidget->DeleteSelection();
}

void MultiSortColumnFrame::on_btnAddColumn_clicked()
{
	//from list widget copy selected items: only one:
	DbRecordSet list;
	ui.listWidget->GetDropValue(list);
	QModelIndex index;
	ui.tableWidget->DropHandler(index,MULTICOLUMNSORT,list,NULL);
}

void MultiSortColumnFrame::on_btnMoveUp_clicked()
{
	ui.tableWidget->MoveRowUp();
}

void MultiSortColumnFrame::on_btnMoveDown_clicked()
{
	ui.tableWidget->MoveRowDown();
}
