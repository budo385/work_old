#ifndef DBRECORDSETMODEL_H
#define DBRECORDSETMODEL_H

#include <QAbstractItemModel>
#include <QStringList>
#include <Qt>
#include <QtGlobal>
#include <QIcon>
#include <QColor>
#include <QFont>
#include <QPainter>
#include <QImageReader>
#include <QPersistentModelIndex>

#include "common/common/dbrecordset.h"
#include "dbrecordsetitem.h"

/*!
	\class  DbRecordSetModel
	\ingroup GUICore_ModelClasses
	\brief  RecordSet model. 

	Model class for views (mostly tree view).
*/
class DbRecordSetModel : public QAbstractItemModel
{
	Q_OBJECT

public:
	DbRecordSetModel(QObject *Parent = NULL);
    ~DbRecordSetModel();
	
	virtual void AddDataToModel(DbRecordSet &NewItemsRecordSet);
	virtual void InitializeModel(DbRecordSet *RecordSet = NULL);
	void ClearModel();
	virtual QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex ()) const;
	virtual QModelIndex parent(const QModelIndex &Parent = QModelIndex ()) const;
	int rowCount(const QModelIndex &parent = QModelIndex()) const;
	int GetLoadedChildrenCount(const QModelIndex &Item) const;
	int columnCount(const QModelIndex &parent = QModelIndex()) const;
	virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
	QVariant headerData(int section, Qt::Orientation orientation, int role) const;
	bool setHeaderData (int section, Qt::Orientation orientation, const QVariant &value, int role = Qt::EditRole);
	DbRecordSetItem *GetRootItem();
	QSize GetMaxIconSize();
	
	virtual Qt::ItemFlags flags(const QModelIndex &index) const;
	virtual bool setData ( const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);
	virtual Qt::DropActions supportedDropActions() const;
	virtual bool insertRows(int position, int rows, const QModelIndex &index = QModelIndex());
	virtual bool removeRows(int position, int rows, const QModelIndex &index = QModelIndex());
	void SetDropedItem(DbRecordSetItem *Item);
	void SetLeavedItem(DbRecordSetItem *Item);
	DbRecordSetItem *GetDropedItem();
	DbRecordSetItem *GetLeavedItem();
	virtual QStringList mimeTypes() const;
	QMultiHash<int, DbRecordSetItem*> *GetModelItems();

protected:
	virtual void SetupModelData(DbRecordSetItem *Parent = NULL);
	void SetRootItem();

protected:
	QMultiHash<int, DbRecordSetItem*> m_hshModelItems;	//< Model items hash.
	DbRecordSet		*m_pDbRecordSet;					//< Recordset.
	DbRecordSetItem *m_pRootItem;						//< Root item.
	DbRecordSetItem *m_pDropedItem;						//< Dropped item (dropped in).
	DbRecordSetItem *m_pLeavedItem;						//< Leaved item (dragged out).
	QSize			m_sizeMaxIconSize;					//< Max icon size (because of multiple icons).
};

#endif // DBRECORDSETMODEL_H

