#include "dateselectordlg.h"
#include "freedaysregister.h"
#include <QTextCharFormat>

DateSelectorDlg::DateSelectorDlg(QWidget *parent)
    : QDialog(parent)
{
	ui.setupUi(this);
	ui.calendarWidget->setFirstDayOfWeek(Qt::Monday);
	ui.calendarWidget->setVerticalHeaderFormat(QCalendarWidget::NoVerticalHeader);

	connect(ui.calendarWidget, SIGNAL(currentPageChanged(int,int)), this, SLOT(SetupMonthView()));
	connect(ui.calendarWidget, SIGNAL(selectionChanged()), this, SLOT(OnDateSelectionChanged()));
	SetupMonthView();
	OnDateSelectionChanged();
}

DateSelectorDlg::~DateSelectorDlg()
{
}

void DateSelectorDlg::on_btnOK_clicked()
{
	accept();
}

void DateSelectorDlg::on_btnCancel_clicked()
{
	reject();
}

void DateSelectorDlg::SetupMonthView()
{
	int nMonth = ui.calendarWidget->monthShown();
	int nYear  = ui.calendarWidget->yearShown();

	//calculate first and last month days
	QDate from(nYear, nMonth, 1);
	QDate to = from; to = to.addMonths(1); to = to.addDays(-1);

	//get a list of holidays for the current month period
	DbRecordSet lstHollidays;
	FreeDaysRegister reg;
	if(reg.GetFreeDays(from, to, lstHollidays))
	{
		// display each holiday date in red (same as weekend days)
		QTextCharFormat format;
		format.setForeground(Qt::red);

		int nCnt = lstHollidays.getRowCount();
		for(int i=0; i<nCnt; i++)
			ui.calendarWidget->setDateTextFormat(lstHollidays.getDataRef(i, "DATE").toDate(), format);
	}
}

void DateSelectorDlg::OnDateSelectionChanged()
{
	m_selectedDate = ui.calendarWidget->selectedDate();
}

void DateSelectorDlg::setDate(QDate &date)
{
	if (!date.isValid())
		date=QDate::currentDate();
	ui.calendarWidget->setSelectedDate(date);
	m_selectedDate = date;
}
