#include "multisortlist.h"
#include "common/common/entity_id_collection.h"

MultiSortList::MultiSortList(QWidget *parent)
	: QListWidget(parent)
{

	connect(this,SIGNAL(itemSelectionChanged()),this,SLOT(SelectionChanged()));
}

MultiSortList::~MultiSortList()
{

}

int MultiSortList::GetDropType() const
{return MULTICOLUMNSORT;}

void MultiSortList::SetData(DbRecordSet &data)
{
	m_lstData=data;

	setDragEnabled(true);
	setDropIndicatorShown(true);

	int nSize = m_lstData.getRowCount();
	for(int i=0;i<nSize;++i)
		if(m_lstData.getDataRef(i,2).toInt()<2) // COL_TYPE_CHECKBOX
			insertItem(i,m_lstData.getDataRef(i,0).toString());
		else
			m_lstData.selectRow(i);

	m_lstData.deleteSelectedRows();
}


void MultiSortList::GetDropValue(DbRecordSet &values, bool bSelectItemChildren ) const
{ 
	values.copyDefinition(m_lstData);
	values.merge(m_lstData,true);

}



void MultiSortList::SelectionChanged()
{

	//copy selection from items to list:
	int nSize =m_lstData.getRowCount();
	for(int nRow=0;nRow<nSize;++nRow)
		m_lstData.selectRow(nRow,isItemSelected(item(nRow)));

}

