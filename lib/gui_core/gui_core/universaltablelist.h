#ifndef UINVERSALTABLELIST_H
#define UINVERSALTABLELIST_H

#include <QListWidget>
#include "universaltablecelldwidget.h"
#include "common/common/dbrecordset.h"



/*!
	\class  UniversalTableList
	\ingroup GUICore_UniversalTable
	\brief  Widget used in UniversalTableWidget

	Use:
	- when adding column to UniversalTableWidget, mark column type=COL_TYPE_LIST (see MVIEW_GRID_COLUMN_SETUP)
	- by default, column (inside  UniversalTableWidget datasource list) must contain DbRecordSet (list)
	- Data that will be displayed in combo is defined by column datasource text (see MVIEW_GRID_COLUMN_SETUP)
	  format: "COLUMN_1<<'Separator'<<COLUMN_2<<COLUMN_3 ... where Separator is custom defined text printed as is
	- any user change (selection) is propagated trough UniversalTableWidget and stored in datasource DbRecordSet
*/

class UniversalTableList : public QListWidget, public TableCellWidget
{
	Q_OBJECT

public:
    UniversalTableList(QWidget *parent);
    ~UniversalTableList();



	void SetData(int nRow, int nCol,QVariant &Data,QString strDataSourceFormat="",bool bIsEditable=true); //called by UniversalTableWidget when widget is created
	void RefreshData(QVariant &Data,bool bCopyContent=false);	//called by UniversalTableWidget in refresh method when widget data is only refreshed (data -> screen)
	void SetEditMode(bool bEdit=true);	//called by UniversalTableWidget for disabling/enabling user interaction with child widget

private slots:

	void SelectionChanged();


    
};

#endif // UINVERSALTABLELIST_H


