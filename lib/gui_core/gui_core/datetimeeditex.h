#ifndef DATETIMEEDITEX_H
#define DATETIMEEDITEX_H

#include <QDateTimeEdit>
#include <QKeyEvent>


//Based on QDateTimeEdit, but supports empty values (NULL) is return in dateTime, date, time if empty
//If setDate has invalid date: it will show empty


class DateTimeEditEx : public QDateTimeEdit
{
	Q_OBJECT

public:
	DateTimeEditEx(QWidget *parent=NULL);
	~DateTimeEditEx();

	void stepBy ( int steps );

	//by default: it is used as Date Edit, to use as DateTime or Time use those functions below:
	void useAsDateTimeEdit(bool bNoSecondSection=false);
	void useAsTimeEdit();	
	void setReadOnly(bool bReadOnly);

	QDate date() const;
	QDateTime dateTime() const;
	QTime time() const;
signals:
	void focusOutSignal();

protected:
	void focusOutEvent ( QFocusEvent * event );
	void focusInEvent ( QFocusEvent * event );


public slots:
	void setDateTime ( const QDateTime & dateTime );
	void setDate ( const QDate & date );
	void setTime ( const QTime & time );
	void OnHighLightCurrentSection();

private slots:
	void OnTextChanged();
	void OnPopUpDateSelected( const QDate &);
	void OnTextEdited(const QString &);
	void SetInvalidDate();
	void focusFromLineEdit();

private:
	QString textFromDateTime ( const QDateTime & dateTime ) const;

	QValidator::State validate ( QString & input, int & pos ) const;
	void keyPressEvent ( QKeyEvent * event );

	

	QDateTime m_invalidDateTime;
	bool m_bSettingValueInProgress;
	
};

#endif // DATETIMEEDITEX_H
