#ifndef GUIFIELD_COMBOBOX_H
#define GUIFIELD_COMBOBOX_H

#include "guifield.h"
#include <QComboBox>
#include <QVariant>
#include <QWidget>
#include "common/common/dbrecordset.h"

/*!
	\class GuiField_ComboBox
	\brief Defines support for Radio Buttons
	\ingroup GUICore_GUIFields

	Connects to data source. Combo can display text, and other column data can be mapped to datasoruce.
	Combo can be editable (new values are automatically inserted into list)
*/
class GuiField_ComboBox : public GuiField
{
	Q_OBJECT

public:

	GuiField_ComboBox(QString strFieldName,QWidget *pWidget,DbRecordSet &lstComboValues,QString strComboListMapping_Col,
		QString strComboListDisplay_Col,bool bIsEditable,DbRecordSet* pData);


	void LoadList(DbRecordSet &lstComboValues);
	void RefreshDisplay(); //DATA SOURCE -> UI

private:
	DbRecordSet m_lstData;
	int m_nComboListMapping_Idx;
	int m_nComboListDisplay_Idx;
	QComboBox *m_pComboWidget;


public slots:
	void FieldChanged(int); //UI -> DATA SOURCE
	void on_cmbName_textChanged(const QString &text);
};


#endif // GUIFIELD_COMBOBOX_H
