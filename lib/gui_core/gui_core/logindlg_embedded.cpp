#include "logindlg_embedded.h"
#include <QInputDialog>
#include <QtWidgets/QMessageBox>
#include "gui_core/gui_core/thememanager.h"
#include "fui_embedded/fui_embedded/mobilehelper.h"
#include "gui_helper.h"
#include <QDesktopWidget>

LoginDlg_Embedded::LoginDlg_Embedded(QWidget *parent)
	:  LoginDialogAbstract(parent)
{
	ui.setupUi(this);

	//Dialog stylesheet.
	setStyleSheet("QDialog " + ThemeManager::GetMobileBkg() + ThemeManager::GetGlobalWidgetStyle());

	//HEADER:
	//Set left frame - name and date-time.
	QString styleLeftFrame = ThemeManager::GetMobileLeftHeaderBackground();
	QString styleMiddleFrame = ThemeManager::GetMobileCenterHeaderBackground();
	QString styleRightFrame = ThemeManager::GetMobileRightHeaderBackground();
	ui.left_frame->setStyleSheet(styleLeftFrame);
	ui.center_frame->setStyleSheet(styleMiddleFrame);
	ui.right_frame->setStyleSheet(styleRightFrame);

	//Sokrates label.
	QLabel *pSokratesLabel = new QLabel;
	pSokratesLabel->setText(QString("<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\np, li { white-space: pre-wrap; }\n</style></head><body style=\" font-family:'Arial'; font-size:14pt; color:white; font-weight:bold; font-style:normal;\">\n<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">SOKRATES<sup>")+QChar(174)+QString("</sup></p></body></html>"));

	//Username, password, connection labels.
	ui.user_label->setText(QString("<html><body style=\" font-family:Arial; text-decoration:none;\">\
								   <p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:10pt; font-weight:bold; color:white;\"><i>%1</i></span></p>\
								   </body></html>").arg(tr("User:")));
	ui.password_label->setText(QString("<html><body style=\" font-family:Arial; text-decoration:none;\">\
								   <p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:10pt; font-weight:bold; color:white;\"><i>%1</i></span></p>\
								   </body></html>").arg(tr("Password:")));
	ui.connection_label->setText(QString("<html><body style=\" font-family:Arial; text-decoration:none;\">\
								   <p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:10pt; font-weight:bold; color:white;\"><i>%1</i></span></p>\
								   </body></html>").arg(tr("Connection:")));

	//Left layout
	QHBoxLayout *leftHeaderLayout = new QHBoxLayout;
	leftHeaderLayout->setContentsMargins(0,0,0,0);
	leftHeaderLayout->addSpacing(10);
	leftHeaderLayout->addWidget(pSokratesLabel);
	leftHeaderLayout->addStretch(1);
	ui.left_frame->setLayout(leftHeaderLayout);

	//Right header layout.
	//Set right frame - main menu button.
	m_pCloseButton = new StyledPushButton(this, ":CloseIcon.png", ":CloseIcon.png", "");
	m_pCloseButton->setMinimumHeight(28);
	m_pCloseButton->setMaximumHeight(28);
	QHBoxLayout *rightHeaderLayout = new QHBoxLayout;
	rightHeaderLayout->setContentsMargins(0,0,0,0);
	rightHeaderLayout->addStretch(1);
	rightHeaderLayout->addWidget(m_pCloseButton);
	connect(m_pCloseButton, SIGNAL(clicked()), this, SLOT(OnCloseButton_clicked()));
	ui.right_frame->setLayout(rightHeaderLayout);

	//Three icons and background.
	QString	styleNew=ThemeManager::GetToolbarButtonStyle();
	m_pBtnComm		=	new StyledPushButton(this,":Desktop_G.png",":Desktop_C.png",styleNew);
	m_pBtnContact	=	new StyledPushButton(this,":Contacts_G.png",":Contacts_C.png",styleNew);
	m_pBtnProject	=	new StyledPushButton(this,":Projects_G.png",":Projects_C.png",styleNew);

	//MB always lite on on start:
	m_pBtnComm->setIcon(":Desktop_C.png");
	m_pBtnContact->setIcon(":Contacts_C.png");
	m_pBtnProject->setIcon(":Projects_C.png");
	
	//Tool Bar.
	QHBoxLayout *buttonLayout = new QHBoxLayout;
	//buttonLayout->setAlignment(Qt::AlignCenter);
	buttonLayout->addStretch(1);
	buttonLayout->addWidget(m_pBtnComm);
	buttonLayout->addStretch(1);
	buttonLayout->addWidget(m_pBtnContact);
	buttonLayout->addStretch(1);
	buttonLayout->addWidget(m_pBtnProject);
	buttonLayout->addStretch(1);
	buttonLayout->setContentsMargins(0,2,0,0);
	buttonLayout->setSpacing(0);
	
	
#ifdef WINCE
	if (MobileHelper::IsScreenInPortraitMode())
	{
		//Login button.
		GUI_Helper::CreateStyledButton(ui.btnLogin,"",QString("<html><b><font face:\"Arial\" size=\"20\" color=\"white\">%1 </font></b></html>").arg(tr("Login")), 0, ThemeManager::GetCEMenuButtonStyle(),0);

		ui.frameToolBar->setMinimumHeight(66);
		ui.frameToolBar->setMaximumHeight(66);
	}
	else
	{
		//Login button.
		GUI_Helper::CreateStyledButton(ui.btnLogin,"",QString("<html><b><font face:\"Arial\" size=\"12pt\" color=\"white\">%1 </font></b></html>").arg(tr("Login")),0, ThemeManager::GetCEMenuButtonStyle(),0);
		ui.btnLogin->setMinimumHeight(20);
		ui.btnLogin->setMaximumHeight(20);

		ui.frameToolBar->setMinimumHeight(50);
		ui.frameToolBar->setMaximumHeight(50);
	}
#endif

	ui.frameToolBar->setLayout(buttonLayout);
	ui.frameToolBar->setStyleSheet(ThemeManager::GetToolbarBkgStyle());
	
	ui.btnEditSettings->setIcon(QIcon(":SAP_Modify.png"));
	ui.btnEditSettings->setStyleSheet("QPushButton {background:transparent;}");

	//Show maximized.
	//showMaximized();
	//issue 1666:
	showFullScreen();
}

LoginDlg_Embedded::~LoginDlg_Embedded()
{

}

void LoginDlg_Embedded::SetDefaults(QString strDefaultUserName, QString strDefaultConnectionName)
{
	m_strUser=strDefaultUserName;
	ui.editUsername->setText(strDefaultUserName);
	m_nConnIdx=ui.cboConnection->findText(strDefaultConnectionName);
	if (m_nConnIdx!=-1)
		ui.cboConnection->setCurrentIndex(m_nConnIdx);

	if(!strDefaultUserName.isEmpty())
		ui.editPassword->setFocus();
}

void LoginDlg_Embedded::Initialize(QString strSettingsFile, int nMode,bool bThickClient)
{
	LoginDialogAbstract::Initialize(strSettingsFile,nMode,bThickClient);
	if (nMode)
	{
		//ui.btnClose->setVisible(false);
		//ui.picSplash->setVisible(false);
	}
}

void LoginDlg_Embedded::RefreshConnectionsList()
{
	ui.cboConnection->clear();

	if (m_bThinClient)
	{
		int nCnt = m_objDataNet.m_lstConnections.size();
		for(int i=0; i<nCnt; i++)
			ui.cboConnection->addItem(m_objDataNet.m_lstConnections[i].m_strCustomName);

	}
	else
	{
		int nCnt = m_objDataDb.m_lstConnections.size();
		for(int i=0; i<nCnt; i++)
			ui.cboConnection->addItem(m_objDataDb.m_lstConnections[i].m_strCustomName);
	}

}

void LoginDlg_Embedded::on_btnEditSettings_clicked()
{
	int nConnectionIdx = ui.cboConnection->currentIndex();
	int nConnID = EditSettings(nConnectionIdx);

	if (nConnID>=0)
		ui.cboConnection->setCurrentIndex(nConnID);
}

void LoginDlg_Embedded::on_btnLogin_clicked()
{
	//check if parameters are valid
	if(ui.editUsername->text().isEmpty())
	{
		QMessageBox::warning(this, tr("Error Message"), tr("Please fill user name!"));	
		return;
	}

	m_strUser = ui.editUsername->text();
	m_strPass = ui.editPassword->text();
	m_nConnIdx = ui.cboConnection->currentIndex();

	if(m_nConnIdx < 0)
	{
		QMessageBox::warning(this, tr("Error Message"), tr("Please define a connection!"));	
		return;
	}

	
	if (m_nMode)
		emit OnLoginClick();
	else
	{
		done(1);
		emit OnLoginClick();
	}
}


void LoginDlg_Embedded::SetPasswordFocus()
{
	if(!m_strUser.isEmpty())
		ui.editPassword->setFocus();

}

void LoginDlg_Embedded::OnCloseButton_clicked()
{
	qApp->quit();

	//done(0);
	//emit OnCloseClick();
}
