#include "dateselector.h"

DateSelector::DateSelector(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);

	m_pButtonGroup = new QButtonGroup(this);
	m_pButtonGroup->addButton(ui.ShowAll_radioButton, 0);
	m_pButtonGroup->addButton(ui.FromDate_radioButton, 1);
	m_pButtonGroup->addButton(ui.PerDate_radioButton, 2);
	m_pButtonGroup->addButton(ui.FromToDate_radioButton, 3);

	//Set default filter and connect filter changed to signal.
	ui.PerDate_radioButton->setChecked(true);
	connect(m_pButtonGroup, SIGNAL(buttonClicked(int)), this, SLOT(on_FilterChanged(int)));
	
	//Connect from date changed to signal and set current date.
	connect(ui.dateEdit, SIGNAL(dateChanged(QDate)), this, SLOT(on_EmitDateChanged(QDate)));
	ui.dateEdit->setCalendarPopup(true);
	ui.dateEdit->setDate(QDate::currentDate());
	
	//Connect from date changed to signal and set current date.
	ToDateEnable(false);
	ui.FromTodateEdit->setCalendarPopup(true);
	ui.FromTodateEdit->setDate(QDate::currentDate().addDays(1));
	connect(ui.FromTodateEdit, SIGNAL(dateChanged(QDate)), this, SLOT(on_EmitToDateChanged(QDate)));

	//Connect from-to date radio to date widget.
	connect(ui.FromToDate_radioButton, SIGNAL(toggled(bool)), this, SLOT(ToDateEnable(bool)));
}

DateSelector::~DateSelector()
{

}

QDate DateSelector::dateFrom()
{
	return ui.dateEdit->date();
}

QDate DateSelector::dateTo()
{
	return ui.FromTodateEdit->date();
}

void DateSelector::setDate(QDate date)
{
	ui.dateEdit->setDate(date);
}

int DateSelector::getFilter()
{
	return m_pButtonGroup->checkedId();
}

void DateSelector::ToDateEnable(bool bEnabled)
{
	ui.btnPrevMonthEmailLog_2->setEnabled(bEnabled);
	ui.btnPrevWeekEmailLog_2->setEnabled(bEnabled);
	ui.btnPrevDayEmailLog_2->setEnabled(bEnabled);
	ui.btnNextDayEmailLog_2->setEnabled(bEnabled);
	ui.btnNextWeekEmailLog_2->setEnabled(bEnabled);
	ui.btnNextMonthEmailLog_2->setEnabled(bEnabled);
	ui.FromTodateEdit->setEnabled(bEnabled);
	ui.label_15->setEnabled(bEnabled);
}

void DateSelector::on_EmitDateChanged(QDate date)
{
	emit EmitDateChanged(date);
}

void DateSelector::on_EmitToDateChanged(QDate date)
{
	emit EmitToDateChanged(date);
}

void DateSelector::on_FilterChanged(int id)
{
	emit FilterChanged(id);
}

void DateSelector::on_btnPrevMonthEmailLog_clicked()
{
	QDate date = ui.dateEdit->date();
	if(date.isValid())
		date = date.addMonths(-1);
	else
		date = QDate::currentDate();

	ui.dateEdit->setDate(date);
}

void DateSelector::on_btnPrevWeekEmailLog_clicked()
{
	QDate date = ui.dateEdit->date();
	if(date.isValid())
		date = date.addDays(-7);
	else
		date = QDate::currentDate();

	ui.dateEdit->setDate(date);
}

void DateSelector::on_btnPrevDayEmailLog_clicked()
{
	QDate date = ui.dateEdit->date();
	if(date.isValid())
		date = date.addDays(-1);
	else
		date = QDate::currentDate();

	ui.dateEdit->setDate(date);
}

void DateSelector::on_btnNextDayEmailLog_clicked()
{
	QDate date = ui.dateEdit->date();
	if(date.isValid())
		date = date.addDays(1);
	else
		date = QDate::currentDate();

	ui.dateEdit->setDate(date);
}

void DateSelector::on_btnNextWeekEmailLog_clicked()
{
	QDate date = ui.dateEdit->date();
	if(date.isValid())
		date = date.addDays(7);
	else
		date = QDate::currentDate();

	ui.dateEdit->setDate(date);
}

void DateSelector::on_btnNextMonthEmailLog_clicked()
{
	QDate date = ui.dateEdit->date();
	if(date.isValid())
		date = date.addMonths(1);
	else
		date = QDate::currentDate();

	ui.dateEdit->setDate(date);
}

void DateSelector::on_btnPrevMonthEmailLog_2_clicked()
{
	QDate date = ui.FromTodateEdit->date();
	if(date.isValid() && (ui.dateEdit->date() < ui.FromTodateEdit->date().addMonths(-1)))
		date = date.addMonths(-1);
	else
		date = QDate::currentDate().addDays(1);

	ui.FromTodateEdit->setDate(date);
}

void DateSelector::on_btnPrevWeekEmailLog_2_clicked()
{
	QDate date = ui.FromTodateEdit->date();
	if(date.isValid() && (ui.dateEdit->date() < ui.FromTodateEdit->date().addDays(-7)))
		date = date.addDays(-7);
	else
		date = QDate::currentDate().addDays(1);

	ui.FromTodateEdit->setDate(date);
}

void DateSelector::on_btnPrevDayEmailLog_2_clicked()
{
	QDate date = ui.FromTodateEdit->date();
	if(date.isValid() && (ui.dateEdit->date() < ui.FromTodateEdit->date().addDays(-1)))
		date = date.addDays(-1);
	else
		date = QDate::currentDate().addDays(1);

	ui.FromTodateEdit->setDate(date);
}

void DateSelector::on_btnNextDayEmailLog_2_clicked()
{
	QDate date = ui.FromTodateEdit->date();
	if(date.isValid())
		date = date.addDays(1);
	else
		date = QDate::currentDate().addDays(1);

	ui.FromTodateEdit->setDate(date);
}

void DateSelector::on_btnNextWeekEmailLog_2_clicked()
{
	QDate date = ui.FromTodateEdit->date();
	if(date.isValid())
		date = date.addDays(7);
	else
		date = QDate::currentDate().addDays(1);

	ui.FromTodateEdit->setDate(date);
}

void DateSelector::on_btnNextMonthEmailLog_2_clicked()
{
	QDate date = ui.FromTodateEdit->date();
	if(date.isValid())
		date = date.addMonths(1);
	else
		date = QDate::currentDate().addDays(1);

	ui.FromTodateEdit->setDate(date);
}
