#include "guifield_doublespinbox.h"




GuiField_DoubleSpinBox::GuiField_DoubleSpinBox(QString strFieldName,QWidget *pWidget,GuiDataManipulator* pDataManager,DbView *TableView, QLabel *pLabel)
:GuiField(strFieldName,pWidget,pDataManager,TableView,pLabel)
{
	//test column type in datasource:
	//Q_ASSERT(pDataManager->GetDataSource()->getColumnType(pDataManager->GetDataSource()->getColumnIdx(strFieldName))==QVariant::Double);

	m_nDataType=pDataManager->GetDataSource()->getColumnType(pDataManager->GetDataSource()->getColumnIdx(strFieldName));
	//register signal textChanged() -> ChangeData() if lineEdit:
	m_pEditWidget=dynamic_cast<QDoubleSpinBox*>(pWidget);
	Q_ASSERT(m_pEditWidget!=NULL);

	connect(m_pEditWidget,SIGNAL(valueChanged(double)),this,SLOT(FieldChanged()));

}

/*!
	This slot is invoked when date changes
	NOTE: assert is fired if row is out of bound
*/
void GuiField_DoubleSpinBox::FieldChanged()
{
	QVariant value=m_pEditWidget->value();
	if (m_nDataType==QVariant::Int)
	{
		int nValue=value.toInt();
		m_pDataManager->ChangeData(m_nCurrentRow,m_nIndex,value.toInt());
	}
	else
		m_pDataManager->ChangeData(m_nCurrentRow,m_nIndex,value);
}



/*!
	If data is changed from data manipulator, refresh content
	If data source is empty, content will be cleared.
*/
void GuiField_DoubleSpinBox::RefreshDisplay()
{

	m_pEditWidget->blockSignals(true);

	//check if current row is out of bound, if so clear content;
	if(m_nCurrentRow<m_pDataManager->GetDataSource()->getRowCount()) 
		m_pEditWidget->setValue(m_pDataManager->GetDataSource()->getDataRef(m_nCurrentRow,m_nIndex).toDouble());
	else
		m_pEditWidget->clear();		

	m_pEditWidget->blockSignals(false);

}


