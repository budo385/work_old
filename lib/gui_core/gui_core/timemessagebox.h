#ifndef TIMEMESSAGEBOX_H
#define TIMEMESSAGEBOX_H

#include <QtWidgets/QMessageBox>

class TimeMessageBox : public QMessageBox
{
	Q_OBJECT

public:
	
	TimeMessageBox(int nTimeOut,bool bDisplayTimeOut, Icon icon, const QString & title, const QString & text, StandardButtons buttons = NoButton, QWidget * parent = 0, Qt::WindowFlags f = Qt::Dialog | Qt::MSWindowsFixedSizeDialogHint );
	~TimeMessageBox();

public slots:
	int exec ();

protected:
	void timerEvent(QTimerEvent *event);

private:
	int m_nTimeOut;
	int m_nTimerID;
	bool m_bDisplayTimeOut;

	
};

#endif // TIMEMESSAGEBOX_H
