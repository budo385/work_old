#include "daterangeselectordialog.h"

#include "common/common/datahelper.h"
#include "gui_core/gui_core/gui_helper.h"

DateRangeSelectorDialog::DateRangeSelectorDialog(QDateTime &dateFrom, QDateTime &dateTo, int nRange, QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
	
	setWindowTitle(tr("Select Period"));

	//Set icons.
	ui.btnCurDateRange->setIcon(QIcon(":2downarrow.png"));
	ui.btnPrev->setIcon(QIcon(":1leftarrow.png"));
	ui.btnNext->setIcon(QIcon(":1rightarrow.png"));

	//Setup range combo.
	GUI_Helper::GetDateRangeComboItems(ui.cboRangeSize);

	//Set date time.
	ui.dateFrom->blockSignals(true);
	ui.dateTo->blockSignals(true);
	ui.cboRangeSize->blockSignals(true);

	int nIndex = ui.cboRangeSize->findData(nRange);
	int nRangeIdx = ui.cboRangeSize->itemData(nIndex).toInt();

	ui.cboRangeSize->setCurrentIndex(nIndex);
	ui.dateFrom->setDate(dateFrom.date());
	ui.dateTo->setDate(dateTo.date());

	ui.dateFrom->blockSignals(false);
	ui.dateTo->blockSignals(false);
	ui.cboRangeSize->blockSignals(false);

	connect(ui.dateFrom,SIGNAL(focusOutSignal()),this, SLOT(OnFromDateFocusOut()));
	connect(ui.dateTo,SIGNAL(focusOutSignal()),this, SLOT(OnToDateFocusOut()));
	connect(ui.cboRangeSize, SIGNAL(currentIndexChanged(int)), this, SLOT(OnComboChanged(int)));
}

DateRangeSelectorDialog::~DateRangeSelectorDialog()
{

}

void DateRangeSelectorDialog::on_btnPrev_clicked()
{
	QDate dateFrom	= ui.dateFrom->date();
	QDate dateTo	= ui.dateTo->date();
	int nIndex = ui.cboRangeSize->currentIndex();
	int nRangeIdx = ui.cboRangeSize->itemData(nIndex).toInt();

	DataHelper::DateRangeSelectorPreviousNextButtonClicked(dateFrom, dateTo, nRangeIdx, -1);

	ui.dateFrom->blockSignals(true);
	ui.dateTo->blockSignals(true);
	ui.dateFrom->setDate(dateFrom);
	ui.dateTo->setDate(dateTo);
	ui.dateFrom->blockSignals(false);
	ui.dateTo->blockSignals(false);

	QDateTime dtFrom = QDateTime(dateFrom, QTime(0,0,0));
	QDateTime dtTo = QDateTime(dateTo, QTime(23,59,59));
}

void DateRangeSelectorDialog::on_btnNext_clicked()
{
	QDate dateFrom	= ui.dateFrom->date();
	QDate dateTo	= ui.dateTo->date();
	int nIndex = ui.cboRangeSize->currentIndex();
	int nRangeIdx = ui.cboRangeSize->itemData(nIndex).toInt();

	DataHelper::DateRangeSelectorPreviousNextButtonClicked(dateFrom, dateTo, nRangeIdx, 1);

	ui.dateFrom->blockSignals(true);
	ui.dateTo->blockSignals(true);
	ui.dateFrom->setDate(dateFrom);
	ui.dateTo->setDate(dateTo);
	ui.dateFrom->blockSignals(false);
	ui.dateTo->blockSignals(false);
}

void DateRangeSelectorDialog::on_btnCurDateRange_clicked()
{
	QDate dateFrom	= ui.dateFrom->date();
	QDate dateTo	= ui.dateTo->date();
	int nIndex = ui.cboRangeSize->currentIndex();
	int nRangeIdx = ui.cboRangeSize->itemData(nIndex).toInt();

	DataHelper::DateRangeSelectorCurrentButtonClicked(dateFrom, dateTo, nRangeIdx);

	ui.dateFrom->blockSignals(true);
	ui.dateTo->blockSignals(true);
	ui.dateFrom->setDate(dateFrom);
	ui.dateTo->setDate(dateTo);
	ui.dateFrom->blockSignals(false);
	ui.dateTo->blockSignals(false);
}

void DateRangeSelectorDialog::OnComboChanged(int nIdx)
{
	on_btnCurDateRange_clicked();
}

void DateRangeSelectorDialog::on_okPushButton_clicked()
{
	QDate dateFrom	= ui.dateFrom->date();
	QDate dateTo	= ui.dateTo->date();
	int nIndex = ui.cboRangeSize->currentIndex();
	int nRangeIdx = ui.cboRangeSize->itemData(nIndex).toInt();

	emit OkSignal(dateFrom, dateTo, nRangeIdx);
	accept();
}

void DateRangeSelectorDialog::on_cancelPushButton_clicked()
{
	reject();
}

void DateRangeSelectorDialog::OnFromDateFocusOut()
{
	QDate dateFrom	= ui.dateFrom->date();
	QDate dateTo	= ui.dateTo->date();
	int nIndex = ui.cboRangeSize->currentIndex();
	int nRangeIdx = ui.cboRangeSize->itemData(nIndex).toInt();

	DataHelper::GetToFromDateInDateDialog(dateFrom, dateTo, nRangeIdx, true);

	ui.dateTo->blockSignals(true);
	ui.dateTo->setDate(dateTo);
	ui.dateTo->blockSignals(false);
}

void DateRangeSelectorDialog::OnToDateFocusOut()
{
	QDate dateFrom	= ui.dateFrom->date();
	QDate dateTo	= ui.dateTo->date();
	int nIndex = ui.cboRangeSize->currentIndex();
	int nRangeIdx = ui.cboRangeSize->itemData(nIndex).toInt();

	DataHelper::GetToFromDateInDateDialog(dateFrom, dateTo, nRangeIdx, false);

	ui.dateFrom->blockSignals(true);
	ui.dateFrom->setDate(dateFrom);
	ui.dateFrom->blockSignals(false);
}
