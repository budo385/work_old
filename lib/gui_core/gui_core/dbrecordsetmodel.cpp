#include "dbrecordsetmodel.h"

DbRecordSetModel::DbRecordSetModel(QObject *Parent /*= NULL*/)
								  : QAbstractItemModel(Parent)
{
	m_pDbRecordSet = new DbRecordSet();
	//m_pDbRecordSet = NULL;
}
DbRecordSetModel::~DbRecordSetModel()
{
		m_pDropedItem = NULL;
		if (m_pRootItem)
			delete(m_pRootItem);
		//delete(m_pDbRecordSet);
		qDeleteAll(m_hshModelItems);
}

/*!
Initialize model with given recordset.

\param RecordSet			- RecordSet.
*/
void DbRecordSetModel::InitializeModel(DbRecordSet *RecordSet)
{
	//Initialize.
	m_pDropedItem	  = NULL;
	m_pDbRecordSet	  = RecordSet;
	m_sizeMaxIconSize = QSize(16, 16);	//Default icon size.
	SetRootItem();
	SetupModelData();
}

/*!
Clear model.

*/
void DbRecordSetModel::ClearModel()
{
	m_pDbRecordSet=NULL;
	m_pDropedItem = NULL;
	delete(m_pRootItem);
	qDeleteAll(m_hshModelItems);
	m_hshModelItems.clear();
}

/*!
Return index at row, column, with some parent.

\param row			- row.
\param column		- column.
\param parent		- parent.
\return QModelIndex - index
*/
QModelIndex DbRecordSetModel::index(int row, int column, const QModelIndex &parent) const
{
	DbRecordSetItem *parentItem;

	if (!parent.isValid())
		parentItem = m_pRootItem;
	else
		parentItem = static_cast<DbRecordSetItem*>(parent.internalPointer());

	DbRecordSetItem *childItem = parentItem->child(row);
	
	QModelIndex Index = QModelIndex();

	if (childItem)
		Index = createIndex(row, column, childItem);

	return Index;
}
QModelIndex DbRecordSetModel::parent(const QModelIndex &index) const
{
	if (!index.isValid())
		return QModelIndex();

	DbRecordSetItem *childItem = static_cast<DbRecordSetItem*>(index.internalPointer());
	DbRecordSetItem *parentItem = childItem->parent();

	if (parentItem == m_pRootItem)
		return QModelIndex();

	return createIndex(parentItem->row(), 0, parentItem);
}
int DbRecordSetModel::rowCount(const QModelIndex &parent) const
{
	DbRecordSetItem *parentItem;

	if (!parent.isValid())
		parentItem = m_pRootItem;
	else
		parentItem = static_cast<DbRecordSetItem*>(parent.internalPointer());

	return parentItem->childCount();
}

/*!
Return loaded children count.

\param Item			- item.
\return int - number of loaded children.
*/
int DbRecordSetModel::GetLoadedChildrenCount(const QModelIndex &Item) const
{
	DbRecordSetItem *item = static_cast<DbRecordSetItem*>(Item.internalPointer());

	return item->GetLoadedChildrenCount();
}

int DbRecordSetModel::columnCount(const QModelIndex &parent) const
{
	if (parent.isValid())
		return static_cast<DbRecordSetItem*>(parent.internalPointer())->columnCount();
	else
		return m_pRootItem->columnCount();
}

/*!
Return data to display - customize for font, icon,...

\param index - index.
\param role	 - role.
\return QVariant - data.
*/
QVariant DbRecordSetModel::data(const QModelIndex &index, int role) const
{
	//If index not valid (root item), go out.
	if (!index.isValid())
		return QVariant();

	//Get item pointer for later.
	DbRecordSetItem *item = static_cast<DbRecordSetItem*>(index.internalPointer());

	//Icon setting.
	if (role == Qt::DecorationRole)
	{
		QStringList IconList = item->GetIcons();
		int IconCount = IconList.count();

		if (IconCount <= 1)
		{
			QString Icon_resource = IconList.value(0);
			Icon_resource.prepend(":");
			QIcon icon;
			icon.addFile(Icon_resource);
			return QVariant(icon);
		}
		else //Add multiple icons.
		{
			//Calculate new icon length (16 pixels * number of icons.)
			int IconWidth = IconCount*16;
			QSize IconSize(IconWidth, 16); 
			//Make pixmap of future icon.
			QPixmap IconPixmap(IconSize);
			//Fill with white (alpha 0-transparent).
			IconPixmap.fill(QColor(255,255,255,0));
			//Make painter.
			QPainter painter(&IconPixmap);

			for (int i = 0; i < IconCount; i++)
			{
				//Get icon from icon list.
				QString Icon_resource = IconList.value(i);
				//Make it visible to resources.
				Icon_resource.prepend(":");

				QPixmap CurentIcon(Icon_resource);
				
				painter.drawPixmap(i*16, 0,CurentIcon);
			}

			QIcon icon(IconPixmap);
			return QVariant(icon);
		}
	}
	//Font decoration.
	if (role == Qt::FontRole)
	{
		QFont font;
		font.setStyle(QFont::StyleItalic);
		font.setFamily("Helvetica");
		font.setBold(true);
		font.setPointSize(10);
		return QVariant(font);
	}
	//Checked or not.
	if (role == Qt::CheckStateRole)
	{
		return QVariant();
//		return QVariant(Qt::PartiallyChecked);
//		return QVariant(Qt::PartiallyChecked);
	}
	//Display role.
	if (role != Qt::DisplayRole)
	{
		return QVariant();
	}

	//Return data.
	return item->data(index.column());
}

QVariant DbRecordSetModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
		return m_pRootItem->data(section);

	return QVariant();
}

/*!
Set header data.

\param index - section(column or row).
\param orientation - orientation.
\param value	 - value.
\param role	 - role.
\return bool.
*/
bool DbRecordSetModel::setHeaderData ( int section, Qt::Orientation orientation, const QVariant &value, int role)
{
	if (orientation == Qt::Horizontal)
	{
		m_pRootItem->SetData(section, value);
		return true;
	}

	return true;
}

/*!
Get root item.

\return DbRecordSetItem *.
*/
DbRecordSetItem *DbRecordSetModel::GetRootItem()
{
	return m_pRootItem;
}

/*!
Set  root item.

*/
void DbRecordSetModel::SetRootItem()
{
	QList<QString> rootData;

	//int nDataType;
	QString strName, strAlias, strTable;
	//m_pDbRecordSet->getColumn(0, nDataType, strName, strAlias, strTable);

	rootData << QString("Header");
	//m_pDbRecordSet->getColumn(5, nDataType, strName, strAlias, strTable);
	//rootData << strAlias;

	m_pRootItem = new DbRecordSetItem(-1, rootData, QStringList(), 1);
}

/*!
Get max icon size.

\return QSize.
*/
QSize DbRecordSetModel::GetMaxIconSize()
{
	return m_sizeMaxIconSize;
}

Qt::ItemFlags DbRecordSetModel::flags(const QModelIndex &index) const
{
	if (!index.isValid())
		return Qt::ItemIsEnabled;

	Qt::ItemFlags DefaultFlags = QAbstractItemModel::flags(index)/* | Qt::ItemIsEditable */;

	if (index.parent().isValid())
		return Qt::ItemIsDragEnabled | DefaultFlags;
	else
		return Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled | DefaultFlags;
}
bool DbRecordSetModel::setData( const QModelIndex &index, const QVariant &value, int role)
{
	if (index.isValid() && role == Qt::EditRole) 
	{
		DbRecordSetItem *item = static_cast<DbRecordSetItem*>(index.internalPointer());
		item->SetData(index.column(), value);
		
		emit dataChanged(index, index);
		return true;
	}
	return false;
}

Qt::DropActions DbRecordSetModel::supportedDropActions() const
{
	return Qt::CopyAction | Qt::MoveAction;
	//return Qt::CopyAction;
}

bool DbRecordSetModel::insertRows(int position, int rows, const QModelIndex &parent)
{
	beginInsertRows(QModelIndex(), position, position+rows-1);

	endInsertRows();
	return true;
}
bool DbRecordSetModel::removeRows(int position, int rows, const QModelIndex &index)
{
	beginRemoveRows(QModelIndex(), position, position+rows-1);

	endRemoveRows();
	return true;
}

/*!
Setup model data from recordset.

\param Parent	 - Parent item.
*/
void DbRecordSetModel::SetupModelData(DbRecordSetItem *Parent /*= NULL*/)
{
	//Parents per column hash.
	QMultiHash<int, DbRecordSetItem*> Parents;		
	
	//Check are we building from scratch or just some node.
	if (Parent == NULL)
		Parents.insert(0, m_pRootItem);
	else
		Parents.insert(0, Parent);

	int row_count = m_pDbRecordSet->getRowCount();
	for (int row = 0; row < row_count; ++row)
	{
		//Set some data to display.
		QList<QString> data;
		QString Code = m_pDbRecordSet->getDataRef(row, 1).toString();
		QString Name = m_pDbRecordSet->getDataRef(row, 8).toString();
		QString DisplayData = Code + " " + Name;
		data << DisplayData;
		int RowId	 = m_pDbRecordSet->getDataRef(row, 0).toInt();
		int ParentId = m_pDbRecordSet->getDataRef(row, 3).toInt();
		int children = m_pDbRecordSet->getDataRef(row, 5).toInt();
		QString icon = m_pDbRecordSet->getDataRef(row, 6).toString();

		//First separate icons.
		QStringList IconList = icon.split(";");

		//Get Icon width.
		int IconWidth = IconList.count()*16;
		//If we have max icon size store it for later (drawing of tree view).
		if (IconWidth > m_sizeMaxIconSize.width())
			m_sizeMaxIconSize.setWidth(IconWidth);

		//Check if there is item parent. If not his parent is root item.
		if (!Parents.contains(ParentId))
			ParentId = 0;
		
		//Create new item.
		DbRecordSetItem *item = new DbRecordSetItem(RowId, data, IconList, children, Parents.value(ParentId));
		//Insert it in parents hash (key value is record id).
		Parents.insert(RowId, item);
		//Append item to it's parent.
		Parents.value(ParentId)->appendChild(item);
		//Finally append to model item list.
		m_hshModelItems.insert(RowId, item);
	}
	//Clear recordset.
	// m_pDbRecordSet->clear();
}

/*!
Add new data (items) to model.

\param NewItemsRecordSet	 - recordset with new data.
*/
void DbRecordSetModel::AddDataToModel(DbRecordSet &NewItemsRecordSet)
{
	int row_count = NewItemsRecordSet.getRowCount();
	for (int row = 0; row < row_count; ++row)
	{
		//Set some data to display.
		QList<QString> data;
		data << NewItemsRecordSet.getDataRef(row, 1).toString();
		int RowId	 = NewItemsRecordSet.getDataRef(row, 0).toInt();
		int ParentId = NewItemsRecordSet.getDataRef(row, 3).toInt();
		int children = NewItemsRecordSet.getDataRef(row, 5).toInt();
		QString icon = NewItemsRecordSet.getDataRef(row, 6).toString();

		//First separate icons.
		QStringList IconList = icon.split(";");

		//Get Icon width.
		int IconWidth = IconList.count()*16;
		//If we have max icon size store it for later (drawing of tree view).
		if (IconWidth > m_sizeMaxIconSize.width())
			m_sizeMaxIconSize.setWidth(IconWidth);

		//Check if there is item parent. If not his parent is root item.
		if (!m_hshModelItems.contains(ParentId))
			ParentId = 0;

		//Create new item.
		DbRecordSetItem *item = new DbRecordSetItem(RowId, data, IconList, children, m_hshModelItems.value(ParentId));
		//Insert it in parents hash (key value is record id).
		m_hshModelItems.insert(RowId, item);
		//Append item to it's parent.
		m_hshModelItems.value(ParentId)->appendChild(item);
	}
}

/*!
Set dropped item.

\param Item - dropped item.
*/
void DbRecordSetModel::SetDropedItem(DbRecordSetItem *Item)
{
	m_pDropedItem = Item;
}

/*!
Get dropped item.

\return DbRecordSetItem * - dropped item.
*/
DbRecordSetItem *DbRecordSetModel::GetDropedItem()
{
	return m_pDropedItem;
}

/*!
Set leaving (dragged out) item.

\param Item - dragged item.
*/
void DbRecordSetModel::SetLeavedItem(DbRecordSetItem *Item)
{
	m_pLeavedItem = Item;
}

/*!
Get leaving (dragged out) item.

\return DbRecordSetItem * - dragged item.
*/
DbRecordSetItem *DbRecordSetModel::GetLeavedItem()
{
	return m_pLeavedItem;
}

/*!
Set mime types.

\return QStringList - list of Mime types.
*/
QStringList DbRecordSetModel::mimeTypes () const
{
	QStringList types;
	types << "helix/dbrecordsetitem";
	return types;
}

/*!
Get hash with model items.

\return QMultiHash<int, DbRecordSetItem*> * - hash with items.
*/
QMultiHash<int, DbRecordSetItem*> *DbRecordSetModel::GetModelItems()
{
	return &m_hshModelItems;
}

