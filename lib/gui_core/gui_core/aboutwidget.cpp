#include "aboutwidget.h"
#include "common/common/config_version_sc.h"
#include "common/common/datahelper.h"
#include "bus_core/bus_core/customavailability.h"

#define HTML_START_F1	"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\np, li { white-space: pre-wrap; }\n</style></head><body style=\" font-family:'Arial'; font-size:12pt; font-weight:600; font-style:italic;\">\n<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">"
#define HTML_END_F1		"</p></body></html>"


AboutWidget::AboutWidget(QWidget *parent, QString strAppName, QString strModuleCode, QString strUserName, int nLicenseID, QString strReportLine1, QString strReportLine2)
	: QDialog(parent)
{
	ui.setupUi(this);


	QString strModuleName="<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\np, li { white-space: pre-wrap; }\n</style></head><body style=\" font-family:'Arial'; font-size:14pt; font-weight:600; font-style:normal;\">\n<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">%1</p></body></html>";
	strModuleName = strModuleName.arg(strAppName);
	ui.labelName->setText(strModuleName);

	ui.pixSplash->setPixmap(QPixmap(":splash.png"));
	
	QString strEdition;
	//set edition info
	if("SC-TE" == strModuleCode)
		strEdition=tr("TEAM EDITION");
	else if("SC-BE" ==strModuleCode)
		strEdition=tr("BUSINESS EDITION");
	else if("SC-PE" == strModuleCode)
		strEdition=tr("PERSONAL EDITION");
	else
		strEdition="";


	ui.labelEdition->setText(HTML_START_F1+strEdition+HTML_END_F1);
	//<html><head><meta name="qrichtext" content="1" /><style type="text/css">\np, li { white-space: pre-wrap; }\n</style></head><body style=" font-family:'Arial'; font-size:12pt; font-weight:600; font-style:normal;">\n<p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;">edition</p></body></html>

	//set build info
	QString strMsg;
	if (QString(APPLICATION_VERSION).right(3).toInt()>0 && QString(APPLICATION_VERSION).indexOf(".")>=0)
		strMsg.sprintf(tr("Version: %sP (%s %s)").toLatin1().constData(), APPLICATION_VERSION, DataHelper::GetCompileDate(), __TIME__); //this is private build: Marin wants P after version
	else
		strMsg.sprintf(tr("Version: %s (%s %s)").toLatin1().constData(), APPLICATION_VERSION, DataHelper::GetCompileDate(), __TIME__);
	ui.labelVersion->setText(strMsg);

	//if defined, show custom solution ID
	int nCSID = CustomAvailability::GetCustomSolutionID(); 
	if(nCSID > 0){
		ui.labelCSID->setText(QString("CSID: %1").arg(nCSID));
	}

	//show license info
	if(!strUserName.isEmpty())
		//ui.labelOwner->setText(strMsg);
		ui.labelOwner->setText(strReportLine1 + "\n" + strReportLine2);
	else{
		ui.labelOwner->setVisible(false);
		ui.label_4->setVisible(false);
	}
	if(nLicenseID > 0)
		ui.labelSerial->setText(QString("%1").arg(nLicenseID));
	else{
		ui.labelSerial->setVisible(false);
		ui.label_5->setVisible(false);
	}

	setWindowTitle(QString("SOKRATES")+QChar(174)+QString(" Communicator"));
}

AboutWidget::~AboutWidget()
{
}


