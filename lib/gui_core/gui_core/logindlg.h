#ifndef LoginDlg_H
#define LoginDlg_H

#ifdef WINCE
#include "ui_logindlg.h"
#else
#include "generatedfiles/ui_logindlg.h"
#endif
#include "logindialogabstract.h"

/*!
	\class  LoginDlg
	\brief  Login dialog for thin client mode
	\ingroup GUICore

	This dialog is used when logging in a system (both thin and thick mode), 
	it can also start a dialog with a list of all defined app. server (network) connections 
	or database connections (if in thick mode)
*/
class LoginDlg : public LoginDialogAbstract
{
    Q_OBJECT

public:
    LoginDlg(QWidget *parent = 0);
    ~LoginDlg();

	void Initialize(QString strSettingsFile, int nMode=0,bool bThickClient=false);
	void RefreshConnectionsList();
	void SetDefaults(QString strDefaultUserName="", QString strDefaultConnectionName="");
	void SetPasswordFocus();

private:
    Ui::LoginDlgClass ui;
	
private slots:
	void on_btnLogin_clicked();
	void on_btnEditSettings_clicked();
	void on_btnClose_clicked();
	void OnReject();


};

#endif // LoginDlg_H


