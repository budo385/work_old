#ifndef TREEWIDGET_H
#define TREEWIDGET_H

#include <QTreeWidget>
#include <QContextMenuEvent>
#include <QAction>



/*!
	\class TreeWidget
	\brief  ListWidget with context menu handler..
	\ingroup GUICore_CustomWidgets
*/

class TreeWidget : public QTreeWidget
{
	Q_OBJECT

public:
	TreeWidget(QWidget *parent);
	~TreeWidget();

	//------------------------------------------
	//		CNTX MENU
	//------------------------------------------


	void SetContextMenuActions(QList<QAction*>& lstActions){m_lstActions=lstActions;};		//set actions for cnxt menu (alternative approach for setting context menu)
	QList<QAction*>& GetContextMenuActions(){ return m_lstActions; };

protected:
	void contextMenuEvent(QContextMenuEvent *event);


private:

	QList<QAction*> m_lstActions;
	
};

#endif // TREEWIDGET_H
