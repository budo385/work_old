#include "pushbuttonwidget.h"
#include <QContextMenuEvent>
#include <QMenu>

PushButtonWidget::PushButtonWidget(QWidget *parent)
	: QPushButton(parent)
{

}

PushButtonWidget::~PushButtonWidget()
{

}


//--------------------------------------------------------------
//						CONTEXT MENU
//--------------------------------------------------------------


/*!
	Slot for opening context menu:
*/
void PushButtonWidget::contextMenuEvent(QContextMenuEvent *event)
{
	//event:
	if(m_lstActions.size()==0) 
	{
		event->ignore();	
		return;
	}

	QMenu CnxtMenu(this);

	int nSize=m_lstActions.size();
	for(int i=0;i<nSize;++i)
		CnxtMenu.addAction(m_lstActions.at(i));

	CnxtMenu.exec(event->globalPos());
}


void PushButtonWidget::enterEvent( QEvent * event )
{
	QPushButton::enterEvent(event);
	emit SignalMouseEntered();
}