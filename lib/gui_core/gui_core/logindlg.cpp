#include "logindlg.h"
#include <QInputDialog>
#include <QtWidgets/QMessageBox>
#include "thememanager.h"
#include "gui_helper.h"

LoginDlg::LoginDlg(QWidget *parent)
    :LoginDialogAbstract(parent)
{
	ui.setupUi(this);

	ui.picSplash->setPixmap(QPixmap(":splash.png"));

	//TOFIX couldn't set this through the designer
	ui.picSplash->setAlignment(Qt::AlignLeft|Qt::AlignTop);

	ui.btnEditSettings->setIcon(QIcon(":SAP_Modify.png"));
	ui.btnEditSettings->setStyleSheet("QPushButton {background:transparent;}");

	QString strTextButton= "<html><body style=\" font-family:Arial; text-decoration:none;\">\
								<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt; font-weight:800; font-style:normal; color:%1;\">%2</span></p>\
								</body></html>";

	GUI_Helper::CreateStyledButton(ui.btnLogin,"",strTextButton.arg("white").arg(tr("Login")),0, ThemeManager::GetCEMenuButtonStyle(),0);
	GUI_Helper::CreateStyledButton(ui.btnClose,"",strTextButton.arg("white").arg(tr("Close")),0, ThemeManager::GetCEMenuButtonStyle(),0);

	ui.txtTitle->setText(QString("<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\np, li { white-space: pre-wrap; }\n</style></head><body style=\" font-family:'Arial'; font-size:14pt; color:white; font-weight:bold; font-style:normal;\">\n<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">SOKRATES<sup>")+QChar(174)+QString("</sup> Communicator</p></body></html>"));

	//Username, password, connection labels.
	ui.user_label->setText(QString("<html><body style=\" font-family:Arial; text-decoration:none;\">\
								   <p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:10pt; font-weight:bold; color:white;\"><i>%1</i></span></p>\
								   </body></html>").arg(tr("User:")));
	ui.password_label->setText(QString("<html><body style=\" font-family:Arial; text-decoration:none;\">\
									   <p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:10pt; font-weight:bold; color:white;\"><i>%1</i></span></p>\
									   </body></html>").arg(tr("Password:")));
	ui.connection_label->setText(QString("<html><body style=\" font-family:Arial; text-decoration:none;\">\
										 <p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:10pt; font-weight:bold; color:white;\"><i>%1</i></span></p>\
										 </body></html>").arg(tr("Connection:")));


	//issue 1666:
	//setStyleSheet(ThemeManager::GetGlobalWidgetStyle());
	//Dialog stylesheet: issue 1778
	setStyleSheet("QDialog " + ThemeManager::GetMobileBkg() + ThemeManager::GetGlobalWidgetStyle());

	connect(this,SIGNAL(rejected()),this,SLOT(OnReject()));
}

void LoginDlg::SetDefaults(QString strDefaultUserName, QString strDefaultConnectionName)
{
	m_strUser=strDefaultUserName;
	ui.editUsername->setText(strDefaultUserName);
	m_nConnIdx=ui.cboConnection->findText(strDefaultConnectionName);
	if (m_nConnIdx!=-1)
		ui.cboConnection->setCurrentIndex(m_nConnIdx);

	if(!strDefaultUserName.isEmpty())
		ui.editPassword->setFocus();

}

LoginDlg::~LoginDlg()
{
}

void LoginDlg::Initialize(QString strSettingsFile, int nMode,bool bThickClient)
{
	LoginDialogAbstract::Initialize(strSettingsFile,nMode,bThickClient);

	if (nMode==LOGIN_DLG_WEB_AUTHENTICATION)
	{
		ui.btnEditSettings->setVisible(false);
		ui.cboConnection->setVisible(false);
		ui.connection_label->setVisible(false);
		ui.txtTitle->setVisible(false);
		ui.picSplash->setVisible(false);
	}
	else if (m_nMode)
	{
		ui.btnClose->setVisible(false);
		ui.picSplash->setVisible(false);
		ui.txtTitle->setVisible(false);
	}
}

void LoginDlg::RefreshConnectionsList()
{
	ui.cboConnection->clear();

	qSort(m_objDataNet.m_lstConnections); //B.T. issue 2471

	if (m_bThinClient)
	{
		int nCnt = m_objDataNet.m_lstConnections.size();
		for(int i=0; i<nCnt; i++)
			ui.cboConnection->addItem(m_objDataNet.m_lstConnections[i].m_strCustomName);

	}
	else
	{
		int nCnt = m_objDataDb.m_lstConnections.size();
		for(int i=0; i<nCnt; i++)
			ui.cboConnection->addItem(m_objDataDb.m_lstConnections[i].m_strCustomName);
	}

}

void LoginDlg::on_btnClose_clicked()
{
	blockSignals(true);
	done(0);
	blockSignals(false);
	emit OnCloseClick();
}

void LoginDlg::on_btnEditSettings_clicked()
{
	int nConnectionIdx = ui.cboConnection->currentIndex();
	int nConnID = EditSettings(nConnectionIdx);

	if (nConnID>=0)
		ui.cboConnection->setCurrentIndex(nConnID);
}

void LoginDlg::on_btnLogin_clicked()
{
	//check if parameters are valid
	if(ui.editUsername->text().isEmpty())
	{
		QMessageBox::warning(this, tr("Error Message"), tr("Please fill user name!"));	
		return;
	}

	m_strUser = ui.editUsername->text();
	m_strPass = ui.editPassword->text();
	m_nConnIdx = ui.cboConnection->currentIndex();

	if (m_nMode==LOGIN_DLG_WEB_AUTHENTICATION)
	{
		done(1);
		return;
	}
	

	if(m_nConnIdx < 0)
	{
		QMessageBox::warning(this, tr("Error Message"), tr("Please define a connection!"));	
		return;
	}

	if (m_nMode)
		emit OnLoginClick();
	else
	{
		done(1);
		emit OnLoginClick();
	}
}


void LoginDlg::SetPasswordFocus()
{
	if(!m_strUser.isEmpty())
		ui.editPassword->setFocus();
}

void LoginDlg::OnReject()
{
	emit OnCloseClick(); //Marko noticed: when ESC is pressed this is only way to detect closing of dlg
}