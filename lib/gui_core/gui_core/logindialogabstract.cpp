#include "logindialogabstract.h"

#include <QObject>
#include <QtWidgets/QMessageBox>
#include <QKeyEvent>

#ifndef WINCE
#include "networkconnectionsdlg.h"
#else
#include "networkconnectionsdlg_embedded.h"
#endif //WINCE

#include "dbconnsettingsgui.h"
#include "biginputdialog.h"
#include "common/common/datahelper.h"
#include "common/common/config.h"


LoginDialogAbstract::LoginDialogAbstract(QWidget *parent)
: QDialog(parent),m_nMode(0),m_nConnIdx(-1)
{

}

LoginDialogAbstract::~LoginDialogAbstract()
{

}

void LoginDialogAbstract::Initialize(QString strSettingsFile, int nMode,bool bThinClient)
{
	m_bThinClient=bThinClient;
	m_nMode=nMode;
	GetConnList()->Load(strSettingsFile,MASTER_PASSWORD);
}

int LoginDialogAbstract::EditSettings(int nConnectionIdx)
{
	int nReturnConnIndex = -1;
	if (m_bThinClient)
	{
		//ask for password if the one was set
		if(GetConnList()->IsUserPasswordSet())
		{
			QString strTitle(QObject::tr("User Settings Password"));
			QString strLabel(QObject::tr("Password:"));

			biginputdialog dlg(strTitle, strLabel, NULL, "");
			dlg.SetPasswordMode();
			int nRes = dlg.exec();
			if(nRes > 0)
			{
				QString text = dlg.m_strInput;
				if(!GetConnList()->MatchUserPassword(text)){
					QMessageBox::warning(NULL, QObject::tr("Error Message"), QObject::tr("Password does not match!"));
					return nReturnConnIndex;
				}
			}
			else
				return nReturnConnIndex;
		}

		//get currently selected connection
		int nIdx = nConnectionIdx;

		// Now display the GUI dialog
#ifndef WINCE
		NetworkConnectionsDlg dlg;
#else
		NetworkConnectionsDlg_Embedded dlg;
		dlg.showFullScreen();
#endif //WINCE
		dlg.SetConnections(m_objDataNet.m_lstConnections, nIdx);
		dlg.SetPassword(GetConnList()->IsUserPasswordSet(), 
			GetConnList()->GetPasswordHash());

		//display dialog	
		int nRes = dlg.exec();
		if(nRes > 0)
		{
			//save changes from the dialog
			m_objDataNet.m_lstConnections = dlg.GetConnections();
			RefreshConnectionsList();

			bool bUsePass = false;
			QString strHash;
			dlg.GetPassword(bUsePass, strHash);

			if(bUsePass)
				GetConnList()->SetUserPasswordHash(strHash);
			else
				GetConnList()->ClearUserPassword();

			//should we change the selection
			int nCurSel = dlg.GetCurSelection();
			
			if(nCurSel >= 0)
			{
				nReturnConnIndex = nCurSel;
				m_objDataNet.m_nCurSel = nCurSel;

				//issue 2517, coz of sorting indexes are mixed up, find by name:
				QString strConnName=dlg.GetConnections().at(nCurSel).m_strCustomName;

				int nSizeX=m_objDataNet.m_lstConnections.count();
				for (int i=0;i<nSizeX;i++)
				{
					if (m_objDataNet.m_lstConnections.at(i).m_strCustomName==strConnName)
					{
						nReturnConnIndex=i;
						m_objDataNet.m_nCurSel=i;
						break;
					}
				}
		
				
			}

			// Save 
			QString strIni = DataHelper::GetApplicationHomeDir();
			strIni += "/settings/network_connections.cfg";
			GetConnList()->Save(strIni, MASTER_PASSWORD);
		}
	}
	else	//Thick mode
	{
		//ask for password if the one was set
		if(GetConnList()->IsUserPasswordSet())
		{
			QString strTitle(QObject::tr("User Settings Password"));
			QString strLabel(QObject::tr("Password:"));

			biginputdialog dlg(strTitle, strLabel);
			dlg.SetPasswordMode();
			int nRes = dlg.exec();
			if(nRes > 0)
			{
				QString text = dlg.m_strInput;
				if(!GetConnList()->MatchUserPassword(text)){
					QMessageBox::warning(NULL, QObject::tr("Error Message"), QObject::tr("Password does not match!"));
					return nReturnConnIndex;
				}
			}
			else
				return nReturnConnIndex;
		}

		//get currently selected connection
		int nIdx = nConnectionIdx;

		// Now display the GUI dialog
		DbConnSettingsGUI dlg;
		dlg.SetConnections(m_objDataDb.m_lstConnections, nIdx);
		dlg.SetPassword(GetConnList()->IsUserPasswordSet(), 
			GetConnList()->GetPasswordHash());

		//display dialog	
		int nRes = dlg.exec();
		if(nRes > 0)
		{
			//save changes from the dialog
			m_objDataDb.m_lstConnections = dlg.GetConnections();
			RefreshConnectionsList();

			bool bUsePass = false;
			QString strHash;
			dlg.GetPassword(bUsePass, strHash);

			if(bUsePass)
				GetConnList()->SetUserPasswordHash(strHash);
			else
				GetConnList()->ClearUserPassword();

			//should we change the selection
			int nCurSel = dlg.GetCurSelection();
			if(nCurSel >= 0)
				nReturnConnIndex = nCurSel;

			// Save 
			QString strIni = DataHelper::GetApplicationHomeDir();
			strIni += "/settings/database_connections.cfg";
			GetConnList()->Save(strIni, MASTER_PASSWORD);
		}
	}

	return nReturnConnIndex;
}

HTTPClientConnectionSettings LoginDialogAbstract::GetNetCurrentConnection()
{
	if (m_nConnIdx<0)
	{
		if (m_objDataNet.m_lstConnections.size()>0)
		{
			return m_objDataNet.m_lstConnections[0];
		}
		else
		{
			HTTPClientConnectionSettings empty;
			return empty;
		}
	}
	else
		return m_objDataNet.m_lstConnections[m_nConnIdx];
}

DbConnectionSettings LoginDialogAbstract::GetDbCurrentConnection()
{
	if (m_nConnIdx<0)
	{
		if (m_objDataDb.m_lstConnections.size()>0)
		{
			return m_objDataDb.m_lstConnections[0];
		}
		else
		{
			DbConnectionSettings empty;
			return empty;
		}
	}
	else
		return m_objDataDb.m_lstConnections[m_nConnIdx];
}

ConnectionsListAbstract *LoginDialogAbstract::GetConnList()
{
	if (m_bThinClient)
		return &m_objDataNet;
	else
		return &m_objDataDb;
}

void LoginDialogAbstract::keyPressEvent(QKeyEvent* event)
{
	if (event->key() == Qt::Key_Escape && m_nMode==LOGIN_DLG_SIDEBAR)
	{
		event->ignore();
		return;
	}

	QDialog::keyPressEvent(event);
}




