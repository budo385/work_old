#ifndef BIGINPUTDIALOG_H
#define BIGINPUTDIALOG_H

#include <QWidget>
#include <QtWidgets/QDialog>
#include <QLineEdit>
#include "generatedfiles/ui_biginputdialog.h"
#include <QString>

class biginputdialog : public QDialog
{
    Q_OBJECT

public:
	biginputdialog(QString strTitle, QString strLabel/*, QLineEdit::echoMode mode = QLineEdit::Normal*/, QWidget *parent = 0,QString strDefaultValue="");
    ~biginputdialog();
	
	void SetPasswordMode();

	QString m_strInput;

private:
    Ui::biginputdialogClass ui;

private slots:
	void on_btnCancel_clicked();
	void on_btnOK_clicked();
};

#endif // BIGINPUTDIALOG_H
