#ifndef GUIFIELD_DATEEDITWIDGET_H
#define GUIFIELD_DATEEDITWIDGET_H

#include "guifield.h"
#include "dateeditwidget.h"



/*!
	\class GuiField_DateEditWidget
	\brief Defines GuiField_DateEditWidget
	\ingroup GUICore_GUIFields

	DateEditWidget uses simple LinEdit, used to support NULL values

*/
class GuiField_DateEditWidget : public GuiField
{
	Q_OBJECT

public:
	GuiField_DateEditWidget(QString strFieldName,QWidget *pWidget,GuiDataManipulator* pDataManager,DbView *TableView=NULL, QLabel *pLabel=NULL);

	void RefreshDisplay(); //DATA SOURCE -> UI


private:
	DateEditWidget *m_pEditWidget;

public slots:
		void FieldChanged(){}; //UI -> DATA SOURCE
		void FieldChanged(QDate); //UI -> DATA SOURCE

};

#endif // GUIFIELD_DATEEDIT_H


