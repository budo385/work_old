#include "dbrecordsetitem.h"

DbRecordSetItem::DbRecordSetItem()
{

}

DbRecordSetItem::DbRecordSetItem(const int RowID, const QList<QString> &data, const QStringList Icon, const int ItemState, DbRecordSetItem *parent)
{
	Q_ASSERT(RowID!=0);
	m_nItemRowID	= RowID;
	m_ItemData		= data;
	m_lstIcon		= Icon;
	m_nItemState	= ItemState;
	m_pParentItem	= parent;
}

DbRecordSetItem::~DbRecordSetItem()
{
	m_pParentItem = NULL;
}


DbRecordSetItem::DbRecordSetItem(const DbRecordSetItem &that)
{
	operator = (that);
}

void DbRecordSetItem::operator =(const DbRecordSetItem &that)
{
	m_nItemRowID	= that.m_nItemRowID;
	m_ItemData		= that.m_ItemData;
	m_lstIcon		= that.m_lstIcon;
	m_nItemState	= that.m_nItemState;
	m_pParentItem	= that.m_pParentItem;
	m_ChildItems	= that.m_ChildItems;
}



void DbRecordSetItem::ClearItem()
{
	m_pParentItem = NULL;
	m_ItemData.clear();
	m_ChildItems.clear();
}

void DbRecordSetItem::appendChild(DbRecordSetItem *item)
{
	//Q_ASSERT(!m_lstChildRowID.contains(item->GetRowId()));
	m_ChildItems.append(item);
	m_lstChildRowID.append(item->GetRowId());
}

DbRecordSetItem *DbRecordSetItem::child(int row)
{
	return m_ChildItems.value(row);
}

int DbRecordSetItem::childCount() const
{
	//If has children and not filled with them then return something.
	int ChildCount = m_ChildItems.count();
	if (!ChildCount && m_nItemState > 0)
		return 1;
	else
		return ChildCount;
}

int DbRecordSetItem::columnCount() const
{
	return m_ItemData.count();
}

QVariant DbRecordSetItem::data(int column) const
{
	return m_ItemData.value(column);
}

QStringList DbRecordSetItem::GetIcons()
{
	return m_lstIcon;
}

void DbRecordSetItem::SetData(const int column, const QVariant Value)
{
	Q_ASSERT(column < m_ItemData.size());
	m_ItemData.replace(column, Value.toString());
}

int DbRecordSetItem::GetRowId()
{
	return m_nItemRowID;
}

int DbRecordSetItem::GetItemState()
{
	return m_nItemState;
}

int DbRecordSetItem::row() const
{
	if (m_pParentItem)
		return m_pParentItem->m_ChildItems.indexOf(const_cast<DbRecordSetItem*>(this));

	return 0;
}

int DbRecordSetItem::GetLoadedChildrenCount()
{
	return m_ChildItems.count();
}

DbRecordSetItem *DbRecordSetItem::parent()
{
	return m_pParentItem;
}

void DbRecordSetItem::DeleteChild(DbRecordSetItem *child)
{
	Q_ASSERT(m_ChildItems.size() > m_ChildItems.indexOf(child));
	m_lstChildRowID.removeAll(child->GetRowId());
	m_ChildItems.remove(m_ChildItems.indexOf(child));

	Q_ASSERT(!m_lstChildRowID.contains(child->GetRowId()));
}
void DbRecordSetItem::DeleteChild(int Row)
{
	Q_ASSERT(m_ChildItems.size() > Row);

	m_lstChildRowID.removeAll(m_ChildItems.value(Row)->GetRowId());
	m_ChildItems.remove(Row);

	Q_ASSERT(!m_lstChildRowID.contains(m_ChildItems.value(Row)->GetRowId()));
}

void DbRecordSetItem::SetParent(DbRecordSetItem *ParentItem)
{
	Q_ASSERT(ParentItem);
	m_pParentItem = ParentItem;
}

QList<int> DbRecordSetItem::GetChildrenRowIDList()
{
	return m_lstChildRowID;
}

/*QVariant DbRecordSetItem::ItemValue()
{
	return m_ItemData.value(0);
}
QList<int> DbRecordSetItem::GetItemRecordsetRows()
{
	return m_nItemRecordSetRows;
}
int DbRecordSetItem::GetItemRecordsetColumn()
{
	return m_nColumn;
}
void DbRecordSetItem::AppendRow(int row)
{
	m_nItemRecordSetRows << row;
}
void DbRecordSetItem::DeleteParent()
{
	m_pParentItem = NULL;
}*/

