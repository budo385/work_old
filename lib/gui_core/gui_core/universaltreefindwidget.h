#ifndef UNIVERSALTREEFINDWIDGET_H
#define UNIVERSALTREEFINDWIDGET_H

#include <QWidget>
#include <QComboBox>
#include <QLabel>

#include "findcombobox.h"
#include "universaltreewidget.h"

/*!
\class UniversalTreeFindWidget
\brief Universal tree find widget.
\ingroup GUICore_CustomWidgets

Search widget. Implemented as widget for possible future implementation of 
search options.

*/
class UniversalTreeFindWidget : public QWidget
{
    Q_OBJECT

public:
    UniversalTreeFindWidget(QWidget *parent = 0);
    ~UniversalTreeFindWidget();

	void Initialize(UniversalTreeWidget *pTreeWidget, bool bSideBarMode=false,QString strStyleSheet="");
	FindComboBox			*m_pFindCombo;
private:
	
	UniversalTreeWidget		*m_pTreeWidget;
	QLabel					*m_pFindLabel;
};

#endif // UNIVERSALTREEFINDWIDGET_H
