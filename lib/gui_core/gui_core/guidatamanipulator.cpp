#include "guidatamanipulator.h"


//copy constr:
GuiDataManipulator::GuiDataManipulator(const GuiDataManipulator &that)
{
	operator =(that);
}


void GuiDataManipulator::operator =(const GuiDataManipulator &that)
{
	m_lstData						= that.m_lstData;
	m_nPrimaryKeyColumnIdx			= that.m_nPrimaryKeyColumnIdx;
	bTrackChanges					= that.bTrackChanges;
	DataCache						= that.DataCache;
	DataDeleted						= that.DataDeleted;
	DataChanged						= that.DataChanged;
}


/*!
	If primary key given, GuiDataManipulator will track changes

	\param lstData			- data source

*/
GuiDataManipulator::GuiDataManipulator(DbRecordSet *lstData)
{
	//store pointer to list
	m_lstData=lstData;

	//disable change tracking:
	m_nPrimaryKeyColumnIdx=-1;
	bTrackChanges=false;
}

/*!
	Enables tracking of changes, clears state of cache

	\param strPrimaryKey	- colum name of unique column inside lstData
*/
void GuiDataManipulator::EnableDataChangeTracking(QString strPrimaryKey)
{
	//define tracker lists:
	DataCache.copyDefinition(*m_lstData);
	DataDeleted.destroy();
	DataDeleted.addColumn(QVariant::Int,strPrimaryKey);
	DataChanged.destroy();
	DataChanged.addColumn(QVariant::Int,strPrimaryKey);

	//get pk index
	m_nPrimaryKeyColumnIdx= m_lstData->getColumnIdx(strPrimaryKey);
	Q_ASSERT(m_nPrimaryKeyColumnIdx!=-1);
	bTrackChanges=true;

}


/*!
	Inserts new empty row at given position or at end of list.
	Can duplicate existing row if rowFrom is given.

	\param nRowInsertAt - row number at which to insert new row (-1 for append to end of list)
	\param nRowFrom		- row number from which data will be copied/inserted at new row
*/
void GuiDataManipulator::InsertData(int nRowInsertAt,int nRowFrom)
{
	//insert from
	if(nRowFrom!=-1)
	{
		DbRecordSet Row=m_lstData->getRow(nRowFrom);
		if(nRowInsertAt!=-1)
		{
			m_lstData->insertRow(nRowInsertAt);
			m_lstData->assignRow(nRowInsertAt,Row);
		}
		else
		{
			m_lstData->addRow();
			m_lstData->assignRow(m_lstData->getRowCount()-1,Row);
		}
		return;
	}

	//insert to or append
	if(nRowInsertAt!=-1)
	{
		m_lstData->insertRow(nRowInsertAt);
	}
	else
	{
		m_lstData->addRow();
	}
	


}

/*!
	Deletes rows from list

	\param nRow			- row number to delete, if -1, whole table is deleted
	\param bSelected	- if true then first parameter is ignored, all selected are deleted
*/
void GuiDataManipulator::DeleteData(int nRow,bool bSelected)
{
	
	//remove whole table
	if(nRow==-1)
	{
		if(bTrackChanges)
		{
			DataDeleted.merge(*m_lstData);
			DataDeleted.find(m_nPrimaryKeyColumnIdx,0); //select all with pk=0
			DataDeleted.deleteSelectedRows();
			DataDeleted.removeDuplicates(0);
		}
		m_lstData->clear();
		return;
	}

	//remove selected
	if(bSelected)
	{
		if(bTrackChanges)
		{
			DataDeleted.merge(*m_lstData,true);
			DataDeleted.find(m_nPrimaryKeyColumnIdx,0); //select all with pk=0
			DataDeleted.deleteSelectedRows();
			DataDeleted.removeDuplicates(0);
		}
		m_lstData->deleteSelectedRows();
	}
	else
	{
		if(bTrackChanges)
		{
			int nID= m_lstData->getDataRef(nRow,m_nPrimaryKeyColumnIdx).toInt();
			if(nID!=0)
			{
				DataDeleted.addRow();
				DataDeleted.setData(DataDeleted.getRowCount()-1,0,nID);
			}
		}
		m_lstData->deleteRow(nRow);
	}


}

/*!
	Validates and stores new data into list. Returns false if validation fails.
	Re-implement this method to validate user input.

	\param nRow			- row 
	\param nCol			- column
	\param value		- new value entered by user
	\return				- true if validation is OK
*/
bool GuiDataManipulator::ChangeData(int nRow,int nCol, const QVariant &value)
{

	if(nRow>m_lstData->getRowCount()-1) return false; //this can happen in various forms, do not assert, just keep going...

	//m_lstData->DebugPrintInfo();

	//qDebug()<<value.toString();
	m_lstData->setData(nRow,nCol,value);

	if(bTrackChanges)
	{
		//keep track of changed data if PK!=0:
		int nID= m_lstData->getDataRef(nRow,m_nPrimaryKeyColumnIdx).toInt();
		if(nID!=0) //means that EDITing is in progress
		{
			//check if already exists:
			int nRowID=DataChanged.find(0,nID,true);
			if(nRowID==-1)//if not found add:
			{
				DataChanged.addRow();
				DataChanged.setData(DataChanged.getRowCount()-1,0,nID);
			}
		}
	}

	return true;
}


/*!
	Same as above: uses column name instead col no

	\param nRow				- row 
	\param strColumnName	- column
	\param value			- new value entered by user
	\return					- true if validation is OK
*/
bool GuiDataManipulator::ChangeData(int nRow,QString strColumnName, const QVariant &value)
{
	return ChangeData(nRow,m_lstData->getColumnIdx(strColumnName),value);
}



//set all rows to changed status:
void GuiDataManipulator::SetDirtyFlagOnAllRows()
{

	int nSize=m_lstData->getRowCount();
	for(int i=0;i<nSize;++i)
	{
		//keep track of changed data if PK!=0:
		int nID= m_lstData->getDataRef(i,m_nPrimaryKeyColumnIdx).toInt();
		if(nID!=0) //means that EDITing is in progress
		{
			//check if already exists:
			int j=DataChanged.find(0,nID,true);
			if(j==-1)//if not found add:
			{
				DataChanged.addRow();
				DataChanged.setData(DataChanged.getRowCount()-1,0,nID);
			}
		}


	}


}



//-----------------------------------------------------------
//					CHANGE TRACKER
//-----------------------------------------------------------


/*!
	Clears changes, stores current data to cache
*/
void GuiDataManipulator::ClearState()
{
	Q_ASSERT(bTrackChanges); //CANT BE USED IF NOT TRACKING ENABLED

	DataChanged.clear();
	DataDeleted.clear();
	DataCache=*m_lstData;    //cache is in lst def
}

/*!
	Clears changes, loads data from cache 
*/
void GuiDataManipulator::LoadFromCache()
{
	Q_ASSERT(bTrackChanges); //CANT BE USED IF NOT TRACKING ENABLED

	DataChanged.clear();
	DataDeleted.clear();
	*m_lstData=DataCache;
}

/*!
	Returns list (one column) with PK <>0 of deleted rows, since last ClearState or LoadFromCache
	
	\param lstDeleted - returned deleted list
*/
void GuiDataManipulator::GetDeletedData(DbRecordSet &lstDeleted)
{
	Q_ASSERT(bTrackChanges); //CANT BE USED IF NOT TRACKING ENABLED

	lstDeleted=DataDeleted;
}



/*!
	Returns list in data source format with all changed rows: inserted+cahnged.
	Note: selection in data source list is changed!
	
	\param lstWrite - all changed rows will be merge in this list (must be defined before)
*/
void GuiDataManipulator::GetWriteData(DbRecordSet &lstWrite)
{

	Q_ASSERT(bTrackChanges); //CANT BE USED IF NOT TRACKING ENABLED

	//CHANGED:
	int nID,nRowID;
	int nSize=m_lstData->getRowCount();
	m_lstData->clearSelection();
	for(int i=0;i<nSize;++i)
	{
		nID= m_lstData->getDataRef(i,m_nPrimaryKeyColumnIdx).toInt();
		if (nID==0) 
			m_lstData->selectRow(i);	//row inserted
		else
		{
			nRowID=DataChanged.find(0,nID,true);
			if(nRowID!=-1)m_lstData->selectRow(i);  //row changed
		}
	}

	lstWrite.merge(*m_lstData,true);	//only selected
}


/*!
	If data is changed, returns true
	\return		- true if something is changed
*/

bool GuiDataManipulator::IsChanged()
{
	Q_ASSERT(bTrackChanges); //CANT BE USED IF NOT TRACKING ENABLED

	if(DataDeleted.getRowCount()>0) return true;
	if(DataChanged.getRowCount()>0) return true;
	if(m_lstData->find(m_nPrimaryKeyColumnIdx,0,true)!=-1) return true;
	return false;
}


/*!
	If data in row is edited, returns true
	\return		- true if something is changed
*/
bool GuiDataManipulator::IsRowEdited(int nRow)
{
	//find value of PK inside datachanged
	if(DataChanged.find(0,m_lstData->getDataRef(nRow,m_nPrimaryKeyColumnIdx).toInt(),true)!=-1) 
		return true;
	else
		return false;
}


/*!
	If data in row is inserted, returns true
	\return		- true if row inserted
*/
bool GuiDataManipulator::IsRowInserted(int nRow)
{
	//if PK=0 then inserted
	if(m_lstData->getDataRef(nRow,m_nPrimaryKeyColumnIdx).toInt()==0)
		return true;
	else
		return false;

}


void GuiDataManipulator::SetDirtyFlag(int nRow)
{
	//keep track of changed data if PK!=0:
	int nID= m_lstData->getDataRef(nRow,m_nPrimaryKeyColumnIdx).toInt();
	if(nID!=0) //means that EDITing is in progress
	{
		//check if already exists:
		int i=DataChanged.find(0,nID,true);
		if(i==-1)//if not found add:
		{
			DataChanged.addRow();
			DataChanged.setData(DataChanged.getRowCount()-1,0,nID);
		}
	}

}