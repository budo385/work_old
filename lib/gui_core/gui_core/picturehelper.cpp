#include "picturehelper.h"
#include <QDebug>
#include <QtWidgets/QMessageBox>
#include <QBuffer>
#include <QFileDialog>
#include <QClipboard>
#include <QImageReader>
#include <QImageWriter>
#include <QApplication>
#include <QBitmap>
#include "common/common/datahelper.h"
#include "db_core/db_core/dbsqltableview.h"

extern QString g_strLastDir_FileOpen;
static QString strStartDir=DataHelper::GetMyDocumentsDir();

#ifdef _WIN32
	#define NOMINMAX
	#include <windows.h>
#endif


/*!
	Prompts user to pick picture from file

	\param bufPixMap	- return picture as pixmap
	\param strFormat	- format of picture
	\return				- false if user canceled or error occurred

*/
bool PictureHelper::LoadPicture(QPixmap &bufPixMap,QString &strFormat)
{

	//get list of supported images:
	QList<QByteArray> lstFormats = QImageReader::supportedImageFormats();
	QString strFilter ="Images (";
	for(int i=0;i<lstFormats.size();++i)
	{
		//if(i!=0)strFilter+=" ";
		strFilter+=" *."+lstFormats.at(i);
	}
	strFilter+=")";

	//if start dir empty then application path:
	if(strStartDir.isEmpty())strStartDir=QDir::currentPath(); 

	//open file dialog:
	if (g_strLastDir_FileOpen.isEmpty())
		g_strLastDir_FileOpen=strStartDir;
	else
		strStartDir=g_strLastDir_FileOpen;
	QString strFile = QFileDialog::getOpenFileName(
		NULL,
		QObject::tr("Choose picture"),
		strStartDir,
		strFilter);
	if (!strFile.isEmpty())
	{
		QFileInfo fileInfo(strFile);
		g_strLastDir_FileOpen=fileInfo.absolutePath();
	}

	//load picture into ByteArray
	if(!strFile.isEmpty())
	{
		QFileInfo picFile(strFile);
		strFormat=picFile.completeSuffix();

		bool bOK=bufPixMap.load(strFile);
		if(!bOK)
		{
			QMessageBox::critical(NULL,QObject::tr("Error"),QObject::tr("Picture can not be loaded!"),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
			return false;
		}

		strStartDir=picFile.absoluteDir().absolutePath(); //store back choosen dir
		return true;
	}
	else
		return false;

}





bool PictureHelper::SavePicture(QPixmap &bufPixMap)
{

	QByteArray bufPicture;

	//get list of supported images:
	QList<QByteArray> lstFormats = QImageWriter::supportedImageFormats();
	QString strFilter ="Images (";
	for(int i=0;i<lstFormats.size();++i)
	{
		//if(i!=0)strFilter+=" ";
		strFilter+=" *."+lstFormats.at(i);
	}
	strFilter+=")";

	//open file dialog:
	if (g_strLastDir_FileOpen.isEmpty())
		g_strLastDir_FileOpen=strStartDir;
	else
		strStartDir=g_strLastDir_FileOpen;
	QString strFile = QFileDialog::getSaveFileName(
		NULL,
		QObject::tr("Save Picture As"),
		strStartDir,
		strFilter);
	if (!strFile.isEmpty())
	{
		QFileInfo fileInfo(strFile);
		g_strLastDir_FileOpen=fileInfo.absolutePath();
	}

	//load picture into ByteArray
	if(!strFile.isEmpty())
	{

		QFileInfo picFile(strFile);
		QString strFormat=picFile.completeSuffix();

		QImageWriter pic_writer(strFile,strFormat.toLatin1());
		bool bOK=pic_writer.write(bufPixMap.toImage());
		if(!bOK)
		{
			QMessageBox::critical(NULL,QObject::tr("Error"),QObject::tr("Error while saving picture!"),QMessageBox::Ok,QMessageBox::NoButton,QMessageBox::NoButton);
			return false;
		}

		//qDebug()<<pic_writer.format();

		return true;
	}
	else
		return false;


}




/*!
	Check content of clipboards, tries to save to given variables

	\param bufPixMap	- return picture as pixmap
	\return				- false if user canceled or error occurred
*/
bool PictureHelper::PastePictureFromClipboard(QPixmap &bufPixMap)
{

	QClipboard *clipboard = QApplication::clipboard();
	bufPixMap = clipboard->pixmap(QClipboard::Clipboard);

	if (bufPixMap.isNull()) return false; //no picture in clipboard
	return true;
}



bool PictureHelper::CopyPictureToClipboard(QPixmap &bufPixMap)
{
	QClipboard *clipboard = QApplication::clipboard();
	clipboard->setPixmap(bufPixMap,QClipboard::Clipboard);
	return true;
}



//WARNING: all pics are converted into PNG format with this function......???
void PictureHelper::ConvertPixMapToByteArray(QByteArray &bufPicture, QPixmap &bufPixMap, QString strFormat)
{
	QBuffer buffer(&bufPicture);
	buffer.open(QIODevice::WriteOnly);		
	bool bOK=bufPixMap.save(&buffer, strFormat.toLatin1());			// writes pic into with default PNG format (who knows original format?)
	if (!bOK)
		bufPixMap.save(&buffer, "png");			// if failes with origin format, revert to known format
	return;

}



//scales picture to 80pix fixed by width, preserving aspect ratio
void PictureHelper::ResizePixMapToPreview(QPixmap &bufPixMap)
{
	int nW=bufPixMap.width();
	int nH=bufPixMap.height();

	double ratio1=0;
	if (nH!=0)
		ratio1=(double)nW/(double)nH;
	double ratio2=120.00/158.00;

	if (ratio1>ratio2)
		bufPixMap=bufPixMap.scaledToWidth(120,Qt::SmoothTransformation);
	else
		bufPixMap=bufPixMap.scaledToHeight(155,Qt::SmoothTransformation);

}

//scales picture , preserving aspect ratio 
void PictureHelper::ResizePixMap(QPixmap &bufPixMap, int nWidth, int nHeight)
{
	int nW=bufPixMap.width();
	int nH=bufPixMap.height();

	double ratio1=0;
	if (nH!=0)
		ratio1=(double)nW/(double)nH;
	double ratio2=(double)nWidth/(double)nHeight;

	if (ratio1>ratio2)
		bufPixMap=bufPixMap.scaledToWidth(nWidth,Qt::SmoothTransformation);
	else
		bufPixMap=bufPixMap.scaledToHeight(nHeight,Qt::SmoothTransformation);
}








//Put in BoEntity before save: saves all pictures 
//pData must have BPIC_PICTURE col
//When deleting, just set PIC_ID=0, trigger will do da job, this will only update existing or add new ones...
//Probably special routine for cleaning DB is needed in future (unlinked PICS)

DbRecordSet PictureHelper::PreProcessSavePictureIfNeeded(Status &Ret_pStatus,GuiDataManipulator *pDataMan,DbRecordSet &lstContactPictures,QString strFieldPictureId, QString strPicPreview,bool bIsPictureMandatory)
{

	
	//get data for write:
	DbRecordSet();
	DbRecordSet lstPictureWrite,lstPictureDelete;

	//writes picture data from picture entity (only images)
	lstPictureWrite.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_BIG_PICTURE));
	lstPictureDelete.copyDefinition(lstPictureWrite);
	lstContactPictures.copyDefinition(*(pDataMan->GetDataSource()));
	pDataMan->GetWriteData(lstContactPictures);

	//lstContactPictures.Dump();

	lstContactPictures.clearSelection();

	//copy pic data for save (edit or insert)
	//delete is automatic on DB (trigger)
	int nSize=lstContactPictures.getRowCount();
	for (int i =0;i<nSize;i++)
	{

		//check if picture exists: (both prev && full must be empty)
		if(lstContactPictures.getDataRef(i,"BPIC_PICTURE").toByteArray().size()==0 && lstContactPictures.getDataRef(i,strPicPreview).toByteArray().size()==0) 
		{
			if(bIsPictureMandatory)
			{
				Ret_pStatus.setError(StatusCodeSet::ERR_BUS_VALIDATION_FAILED,QObject::tr("Picture is mandatory field!"));	
				return DbRecordSet();	
			}
			else
			{
				//if pic is null & ID is valid, set it to zero, trigger on BUS_PERSON must automatically delete pics
				QVariant nullVar(QVariant::Int);
				lstContactPictures.setData(i,strFieldPictureId,nullVar);
				continue;
			}
		}
		
		//only write pic if loaded:
		if(lstContactPictures.getDataRef(i,"BPIC_PICTURE").toByteArray().size()!=0 )
		{
			lstPictureWrite.addRow();
			lstPictureWrite.setData(lstPictureWrite.getRowCount()-1,"BPIC_ID",lstContactPictures.getDataRef(i,strFieldPictureId));
			lstPictureWrite.setData(lstPictureWrite.getRowCount()-1,"BPIC_PICTURE",lstContactPictures.getDataRef(i,"BPIC_PICTURE"));
			lstContactPictures.selectRow(i); //tagg row for write
		}
		
	}


	return lstPictureWrite;

	/*
	//lstPictureWrite.Dump();

	//save and returns ID's back to list
	if(lstPictureWrite.getRowCount()>0)
	{
		//save pictures first, hmm little cranky...
		_SERVER_CALL(BusContact->WriteBigPicture(Ret_pStatus,lstPictureWrite))
		if(!Ret_pStatus.IsOK())	return;

		//copy back new ID's:
		int j=0;
		for (int i =0;i<nSize;i++)
		{
			if(lstContactPictures.isRowSelected(i))
			{
				lstContactPictures.setData(i,strFieldPictureId,lstPictureWrite.getDataRef(j,0));
				j++;
			}
		}
	}


	//set data back:
	lstContactPictures.removeColumn(lstContactPictures.getColumnIdx("BPIC_PICTURE"));
	*/

}



void PictureHelper::PostProcessSavePictureIfNeeded(DbRecordSet &lstContactPictures,DbRecordSet &lstPictureWrite,QString strFieldPictureId)
{
	int nSize=lstContactPictures.getRowCount();
	if(lstPictureWrite.getRowCount()>0)
	{
		//copy back new ID's:
		int j=0;
		for (int i =0;i<nSize;i++)
		{
			if(lstContactPictures.isRowSelected(i))
			{
				lstContactPictures.setData(i,strFieldPictureId,lstPictureWrite.getDataRef(j,0));
				j++;
			}
		}
	}


	//set data back:
	lstContactPictures.removeColumn(lstContactPictures.getColumnIdx("BPIC_PICTURE"));

}




//returns 1st large icon (48 or 32): modify api to fetch all other icons
bool PictureHelper::GetApplicationIcon(QString strAppPath,QPixmap &icon)
{
	
#if defined _WIN32 && !defined WINCE //only for WIN32

	return false;
	//B.T. disable for now for QT5....no functions avaliable...
	/*
	HBITMAP iconBmp;
	//LPCTSTR app=strAppPath.utf16();
	UINT nIconCnt;
	HICON* pLargeIcons = (HICON*)LocalAlloc( LPTR, 500*sizeof(HICON) );
	nIconCnt=ExtractIconEx((LPCTSTR)strAppPath.utf16(),0,pLargeIcons,NULL,500);
	HICON hIcon = pLargeIcons[0];
	if (hIcon)
	{
		ICONINFO iconInfo;
		GetIconInfo(hIcon, &iconInfo);
		DeleteObject(iconInfo.hbmMask);
		//DestroyIcon(hIcon);
		iconBmp=iconInfo.hbmColor;
		LocalFree((HLOCAL)pLargeIcons);

		icon=QPixmap::fromWinHBITMAP(iconBmp,QPixmap::PremultipliedAlpha);
		QPixmap iconAlpha=icon.alphaChannel();
		QPixmap black=iconAlpha;
		black.fill(QColor(0,0,0));
		if (iconAlpha.toImage()==black.toImage()) //alpha channel is invalid or not present: no visible pixel, use other funct, remove black...
		{
			icon=QPixmap::fromWinHBITMAP(iconBmp);
			QBitmap iconbmp=icon.createMaskFromColor(QColor(0,0,0));
			icon.setMask(iconbmp);
		}
		return true;
	}
	LocalFree((HLOCAL)pLargeIcons);
	return false;
	*/
#else
	return false;
#endif
	
}