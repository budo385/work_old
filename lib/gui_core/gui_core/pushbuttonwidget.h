#ifndef PUSHBUTTONWIDGET_H
#define PUSHBUTTONWIDGET_H

#include <QPushButton>

/*!
	\class PushButtonWidget
	\brief  PushButtonWidget with custom context menu
	\ingroup GUICore_CustomWidgets

	Set context menu with SetContextMenuActions
*/
class PushButtonWidget : public QPushButton
{
	Q_OBJECT

public:
    PushButtonWidget(QWidget *parent=0);
    ~PushButtonWidget();

	void SetContextMenuActions(QList<QAction*>& lstActions){m_lstActions=lstActions;};		//set actions for cnxt menu (alternative approach for setting context menu)
	QList<QAction*>& GetContextMenuActions(){ return m_lstActions; };

signals:
	void SignalMouseEntered();

private:

	void enterEvent ( QEvent * event );
	void contextMenuEvent(QContextMenuEvent *event);
	QList<QAction*> m_lstActions;
};

#endif // PUSHBUTTONWIDGET_H
