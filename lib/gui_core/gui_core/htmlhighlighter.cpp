#include "htmlhighlighter.h"

HtmlHighlighter::HtmlHighlighter(QTextDocument *document)
: QSyntaxHighlighter(document)
{
	//&nbsp; and similar
	QTextCharFormat entityFormat;
	entityFormat.setForeground(Qt::red);
	//entityFormat.setFontWeight(QFont::Bold);
	setFormatFor(Entity, entityFormat);

	//formatting for HTML tags
	QTextCharFormat tagFormat;
	tagFormat.setForeground(Qt::darkMagenta);	//QColor(192, 16, 112)
	tagFormat.setFontWeight(QFont::Bold);
	setFormatFor(Tag, tagFormat);

	//formatting for HTML tag parameter values
	QTextCharFormat tagParamValueFormat;
	tagParamValueFormat.setForeground(Qt::darkBlue);
	setFormatFor(TagParamValue, tagParamValueFormat);

	//formatting for HTML tag parameter names
	QTextCharFormat tagParamNameFormat;
	tagParamNameFormat.setForeground(Qt::red);
	tagParamNameFormat.setFontWeight(QFont::Bold);
	setFormatFor(TagParamName, tagParamNameFormat);
	
	//formatting when inside comment
	QTextCharFormat commentFormat;
	commentFormat.setForeground(Qt::darkGreen);
	commentFormat.setFontItalic(true);
	setFormatFor(Comment, commentFormat);
}

void HtmlHighlighter::setFormatFor(Construct construct, const QTextCharFormat &format)
{
	m_formats[construct] = format;
	rehighlight();
}

void HtmlHighlighter::highlightBlock(const QString &text)
{
	int state = previousBlockState();
	int len = text.length();
	int start = 0;
	int pos = 0;

	while (pos < len) {
		switch (state) {
		case NormalState:
		default:
			while (pos < len) {
				QChar ch = text.at(pos);
				if (ch == '<') {
					if (text.mid(pos, 4) == "<!--") {
						state = InComment;
					} else {
						state = InTag;
					}
					break;
				} else if (ch == '&') {
					start = pos;
					while (pos < len
						&& text.at(pos++) != ';')
						;
					setFormat(start, pos - start, m_formats[Entity]);
				} else {
					++pos;
				}
			}
			break;

		case InComment:
			start = pos;
			while (pos < len) {
				if (text.mid(pos, 3) == "-->") {
					pos += 3;
					state = NormalState;
					break;
				} else {
					++pos;
				}
			}
			setFormat(start, pos - start, m_formats[Comment]);
			break;

		case InTag:
			QList< QPair<int, int> > lstQuotedData;
			QList< QPair<int, int> > lstParamNames;
			bool bTagNameEnded=false;
			int startQuote = -1;
			int startName = -1;
			QChar quote = QChar::Null;
			start = pos;
			while (pos < len) {
				QChar ch = text.at(pos);
				if (quote.isNull()) {
					if (ch == '\'' || ch == '"') {
						//quote started
						quote = ch;
						startQuote = pos;
					} else if (ch == '>') {
						++pos;
						state = NormalState;
						startQuote = -1;
						break;
					}
					else if(ch == ' ') {
						bTagNameEnded = true;
					}
					else if(ch == '=') {
						if(startName > 0){
							//mark the parameter name for painting
							QPair<int, int> data;
							data.first = startName;
							data.second = pos - startName;
							if(data.second > 0)
								lstParamNames.append(data);
							startName = -1;
						}
					}
					else {
						if(bTagNameEnded && (startName == -1)){
							//detected the start
							startName = pos;
						}
					}

				} else if (ch == quote) {
					//quote ended, mark the quoted content
					QPair<int, int> datQuote;
					datQuote.first = startQuote+1;
					datQuote.second = pos - startQuote - 1;
					if(datQuote.second > 0)
						lstQuotedData.append(datQuote);

					quote = QChar::Null;
					startQuote = -1;
				}
				++pos;
			}
			setFormat(start, pos - start, m_formats[Tag]);
			
			//over-paint all quotes
			int nLen = lstQuotedData.length();
			int i;
			for(i=0; i<nLen; i++)
				setFormat(lstQuotedData[i].first, lstQuotedData[i].second, m_formats[TagParamValue]);
			//over-paint all param names
			nLen = lstParamNames.length();
			for(i=0; i<nLen; i++)
				setFormat(lstParamNames[i].first, lstParamNames[i].second, m_formats[TagParamName]);
			
		}
	}

	//paint "<",">" in blue
	int nStart = text.indexOf("<");
	while(nStart >= 0){
		setFormat(nStart, 1, m_formats[TagParamValue]);
		nStart = text.indexOf("<", nStart+1);
	}
	nStart = text.indexOf(">");
	while(nStart >= 0){
		setFormat(nStart, 1, m_formats[TagParamValue]);
		nStart = text.indexOf(">", nStart+1);
	}

	setCurrentBlockState(state);
}
