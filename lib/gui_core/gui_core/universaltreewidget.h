#ifndef UNIVERSALTREEWIDGET_H
#define UNIVERSALTREEWIDGET_H

#include <QTreeWidget>
#include <QHeaderView>

#include "common/common/dbrecordset.h"
#include "gui_core/gui_core/dragdropinterface.h"
//#include "gui_core/gui_core/universaltabledelegate.h"
//#include "gui_core/gui_core/guidatamanipulator.h"

/*!
\class  UniversalTreeWidget
\ingroup GUICore_UniversalTree
\brief  This class provides universal table view

Features:
- uses DbRecordSet as data source to display data or external GuiDataManipulator
- supports drag&drop, single or multirow select, default cotnext menu

Use:
- Initialize with datasource, MAKE SURE THAT THE FIRST COLUM IS TABLE ID (UNIQUE).
- Override SetItemData() method if not using tree view table structure. 
- First user role data MUST be RowID, second user role data MUST be ParentID.
- Enjoy using it. (util you find a bug).
*/
class UniversalTreeWidget : public QTreeWidget, public DragDropInterface
{
    Q_OBJECT


	enum MappedCols
	{
		COL_ID,
		COL_CODE,
		COL_LEVEL,
		COL_PARENT,
		COL_HASCHILDREN,
		COL_ICON,
		COL_MAINPARENT_ID,
		COL_NAME,
		COL_STYLE
	};

public:
	enum DragMode
	{
		DRAG_MODE_ONLY_SELECTED,
		DRAG_MODE_ALL_CHILDREN,
		DRAG_MODE_ALL_CHILDREN_IF_COLLPASED
	};

    UniversalTreeWidget(QWidget *parent = 0);
    ~UniversalTreeWidget();

	//Initialize (data source must be provided)
	void							Initialize(DbRecordSet *lstData, bool bEnableDrop = true, bool bEnableDrag = true, 
												bool bSingleSelection = false, bool bSelectRows = true,bool bIgnoreDropHandler=false);
	void							SetHeader(bool bHideHeader = true, int nColumnCount = 1, const QStringList *lstColLabels = NULL, QList<int> *lstColWidths = NULL);
	DbRecordSet*					GetDataSource(){return m_recData;};
	void							SetEditMode(bool bEdit = false){m_bEdit = bEdit;};
	void							SetDragMode(int nDragMode){m_nDragMode = nDragMode;};
	bool							IsEditMode(){return m_bEdit;};
	void							SetToolTipColumnDataSource(int nColIdx){m_nColToolTipSourceIdx=nColIdx;};
	void							SetIconColumnDataSource(int nColIdx){m_nIconSourceIdx=nColIdx;};
	void							EnableTrackExpandedState(QString strExpandedCol){m_nIdxExpandedCol=m_recData->getColumnIdx(strExpandedCol);};

	//Drag & drop.
	Qt::DropActions					supportedDropActions() const {return DragDropInterface::supportedDropActions();};
	void							dropEvent(QDropEvent *event);
	void							dragEnterEvent(QDragEnterEvent *event);
	virtual void					GetDropValue(DbRecordSet &values, bool bSelectItemChildren = false) const;
	virtual void					DropHandler(QModelIndex index, int nDropType, DbRecordSet &DroppedValue,QDropEvent *event);
	//for external fast setting of cnxt menu:
	void							SetContextMenuActions(QList<QAction*>& lstActions){m_lstActions=lstActions;};		//set actions for cnxt menu (alternative approach for setting context menu)
	QList<QAction*>&				GetContextMenuActions(){ return m_lstActions; };
	//visual effects:
	void							RefreshDisplay(int RowID = -1);
	void							SelectItemByRowID(int nRow);
	void							ExpandItemByRowID(int nRowID, bool bFixScrolling = true);
	void							ExpandItemByRowIDFromRecordSet(DbRecordSet &recData);
	//void							ExpandSelectedItems();
	
	QTreeWidgetItem *				GetItemByRowID(int nRowID);
	int 							GetRowIDByItem(QTreeWidgetItem *item);
	void							SortList();
	void							SetItemsUserCheckable(bool bCheckable, int nDefaultState=Qt::Unchecked);
	void							SetItemsChecked(DbRecordSet &recCheckedItems, bool bChecked = false);
	void							GetCheckedItems(DbRecordSet &recCheckedItems,bool bOnlyChecked=false);
	void							SetChildItemsCheckedState(QTreeWidgetItem *item, Qt::CheckState nCheckState, QList<QTreeWidgetItem*> &lstChangedCheckStateItems);

	void							FixScrolling();

private slots:
	void							on_itemChanged(QTreeWidgetItem *item, int column);
	void							on_itemExpanded(QTreeWidgetItem *item);
	void							on_itemCollapsed(QTreeWidgetItem *item);

public slots:
	virtual void					SelectionChanged();
	void							ExpandTree();
	void							ExpandSelectedItems();
	void							CollapseSelectedItems();

signals:
	void							SignalSelectionChanged();				//when selection is changed, use this
	void							DataDroped(QModelIndex index, int nDropType, DbRecordSet &DroppedValue, QDropEvent *event);
	void							DataDropedAndProcessed();
	void							ItemCheckStateChanged(QList<QTreeWidgetItem*> lstChangedCheckStateItems);
	void							RebuildingTree();


protected:

	//B.T:
	void							SelectInRecordSetByNodeID(int nNodeID);
	virtual void					CreateItemsFromRecordSet(int nRowStart=-1, QTreeWidgetItem *ParentItem = NULL);
	QIcon							CreateIcon(QString IconString);
	void							contextMenuEvent(QContextMenuEvent *event);
	void							GetOnlyActuallySelectedNodes(DbRecordSet &lstSelectedItems) const;
	virtual void					CreateContextMenuActions(QList<QAction*>& lstActions);
	virtual QMimeData * mimeData ( const QList<QTreeWidgetItem *> items ) const;
	virtual QStringList mimeTypes () const;
	virtual void startDrag(Qt::DropActions supportedActions);

	QHash<int, QTreeWidgetItem*>	m_hshRowIDtoItem;			//< Row ID to widget item pointer hash. Used for fast item access.
	QHash<int, int>					m_hshItemRowIDtoCheckState; //< Row ID to widget item pointer hash. Used for fast item access.
	DbRecordSet						*m_recData;					//< Pointer to data source recordset.
	bool							m_bEdit;					//< Is editable.
	QSize							m_sizeMaxIconSize;			//< Maximum icon size.
	QList<QAction*>					m_lstActions;				//< Context menu actions.
	QList<int>						m_lstColumnIdx;				//< mapping column indexes: this way TREE columns doesnt have to be ordered in specified way
	bool							m_bIgnoreDropHandler;		//< some flag
	bool							m_bItemsUserCheckable;		//< Are items user checkable or not (default false).
	int								m_nColToolTipSourceIdx;		//< By default NAME col is datasource!
	int								m_nDragMode;				//< 0-default, 1- node + all children, 2- if node collapsed then children else only node
	int								m_nIdxExpandedCol;			//< Nodes will be expanded by state of this field: 1 expanded, 0 colapsed, current state is saved here
	int								m_nIconSourceIdx;			//< if icon is provided as binary->point to this col and ignore _ICON 
	int								m_nDefaultCheckState;
	
};

#endif // UNIVERSALTREEWIDGET_H


