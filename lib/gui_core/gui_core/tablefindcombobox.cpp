#include "tablefindcombobox.h"

#include <QApplication>
#include <QDebug>


TableFindComboBox::TableFindComboBox(QWidget *parent)
: QComboBox(parent)
{
	connect(this, SIGNAL(currentIndexChanged(int)), this, SLOT(on_currentIndexChanged(int)));
	//connect(this, SIGNAL(highlighted(int)), this, SLOT(on_highlighted(int))); B.T. issue 1853, even without this user can press keydown or click on drop down..y u using this!???
	m_pLastFoundItem=NULL;
	setSizeAdjustPolicy(QComboBox::AdjustToMinimumContentsLength);
	m_nOrganizationColIdx = -1;
	m_nItemTextColIdx = -1;
	m_pLstData = NULL;
}

TableFindComboBox::~TableFindComboBox()
{
	m_pTableWidget = NULL;
}

/*!
Initialize find combo box with pointer to Universal tree widget.

\param pTreeWidget		  - Pointer to Universal tree widget.
*/
void TableFindComboBox::Initialize(QTableWidget *pTableWidget, int nSearchedColumn /* -1 means all */ /*, DbRecordSet *pLstData, int nItemTextColIdx, int nOrganizationColIdx*/)
{
	//issue 2556: only if organization is to be found:
	//m_nOrganizationColIdx = nOrganizationColIdx;
	//m_nItemTextColIdx = nItemTextColIdx;
	//m_pLstData = pLstData;

	m_pTableWidget = pTableWidget;

	//Set layout.
	setEditable(true);
	setCompleter(NULL);
	m_nSearchedColumn=nSearchedColumn;


	//Connect signals.
	connect(this, SIGNAL(editTextChanged(const QString&)), this, SLOT(on_ComboTextChanged(const QString&)));

	m_strDefaultStyleSheet = styleSheet();
}

void TableFindComboBox::focusOutEvent(QFocusEvent* event)
{
	//Don't act on popup and some user focus out reasons.
	if (event->reason() != Qt::PopupFocusReason &&
		event->reason() != Qt::OtherFocusReason)
	{
		m_pTableWidget->setFocus(Qt::MouseFocusReason);
		if (m_pLastFoundItem) //BT: to use QTableWidget directly
		{
			m_pTableWidget->blockSignals(true);
			m_pTableWidget->clearSelection();
			m_pTableWidget->blockSignals(false);
			m_pTableWidget->setCurrentItem(m_pLastFoundItem); //induce selection change
		}
	}

	QComboBox::focusOutEvent(event);
}

void TableFindComboBox::keyPressEvent(QKeyEvent* event)
{

	//If return or enter(numeric) pressed then send focus to tree and notify that selection is changed (load data).
	if(event->key() == Qt::Key_Return || event->key() == Qt::Key_Enter)
	{
		m_pTableWidget->setFocus(Qt::MouseFocusReason);
		if (m_pLastFoundItem) //BT: to use QTableWidget directly
		{
			m_pTableWidget->blockSignals(true);
			m_pTableWidget->clearSelection();
			m_pTableWidget->blockSignals(false);
			m_pTableWidget->setCurrentItem(m_pLastFoundItem); //induce selection change
			emit EnterPressed();
		}
		return;
	}
	//If key down, show combo popup.
	else if (event->key() == Qt::Key_Down)//|| event->key() == Qt::Key_PageDown || event->key() == Qt::Key_PageUp)
	{
		//blockSignals(true);
		showPopup();
		connect(view(), SIGNAL(pressed(const QModelIndex&)), this, SLOT(on_view_pressed(const QModelIndex&)));
		//blockSignals(false);
		return;
	}
	//If escape.
	else if (event->key() == Qt::Key_Escape)//|| event->key() == Qt::Key_PageDown || event->key() == Qt::Key_PageUp)
		m_pTableWidget->setFocus(Qt::MouseFocusReason);

	QComboBox::keyPressEvent(event);
}

void TableFindComboBox::on_ComboTextChanged(const QString &text)
{

	//QTime time;
	//time.start();

	//Fist block signals.
	m_pLastFoundItem=NULL;
	this->blockSignals(true);
	m_pTableWidget->blockSignals(true);
	m_pTableWidget->clearSelection();
	//Clear combo and items list.
	m_lstFoundItems_LastSearch = m_lstFoundItems; //save old search result: 2556
	clear();
	int i = 0;

	//B.T. improvajd: if entered text is subset of previous search text then use old found items-> can be lot of faster..
	if(text.indexOf(m_strLastFindString)==0 && !m_strLastFindString.isEmpty())
	{
		QList<QTableWidgetItem*> lstNew;
		int nSize=m_lstFoundItems.size();
		for(int k=0;k<nSize;k++)
		{
			if (m_nSearchedColumn!=-1 && m_lstFoundItems.at(k)->column()!=m_nSearchedColumn)
				continue;

			if (m_lstFoundItems.at(k)->text().indexOf(text,0,Qt::CaseInsensitive)>=0)
			{
				i++;
				lstNew.append(m_lstFoundItems.at(k));
				if (i < 30) 		//Insert only first 30 found items in popup (for speed).
				{
					QString itemText = m_lstFoundItems.at(k)->data(Qt::DisplayRole).toString().trimmed();
					int itemID		 = m_lstFoundItems.at(k)->data(Qt::UserRole).toInt();

					//issue 2556: add organization if exists:
					//SetOrganizationName(itemText,itemID);
					insertItem(i, itemText, itemID);
				}
			}
		}
		m_strLastFindString=text;
		m_lstFoundItems=lstNew;
	}
	else
	{
		m_strLastFindString=text;
		m_lstFoundItems.clear();

		QListIterator<QTableWidgetItem*> iter(m_pTableWidget->findItems(text, Qt::MatchContains | Qt::MatchRecursive));
		while (iter.hasNext())
		{
			i++;
			QTableWidgetItem *item = iter.next();

			//Append items to found list.
			m_lstFoundItems.append(item);

			//Insert only first 30 found items in popup (for speed).
			if (i < 30)
			{
				QString itemText = item->data(Qt::DisplayRole).toString().trimmed();
				int itemID		 = item->data(Qt::UserRole).toInt();

				//issue 2556: add organization if exists:
				//SetOrganizationName(itemText,itemID);
				insertItem(i, itemText, itemID);
			}
		}
	}


	//Set find combo background.
	if (i == 0)
	{
		setEditText(text);
		setStyleSheet("background: lightGray");
		QApplication::beep();
		this->blockSignals(false);
		m_pTableWidget->blockSignals(false);
		return;
	}
	else
		setStyleSheet(m_strDefaultStyleSheet);

	//Select and expand.
	m_nCurrentFoundItemNum = 0;
	m_pLastFoundItem=m_lstFoundItems.first();
	m_pTableWidget->setCurrentItem(m_lstFoundItems.first());

	//Set edit text and unblock signals.
	setEditText(text);
	m_pTableWidget->blockSignals(false);
	this->blockSignals(false);

}

void TableFindComboBox::on_currentIndexChanged(int index)
{
	//BT: was bug: when user selects item in combo no selection change occur, now it fires signal at once...
	//m_pTableWidget->blockSignals(true);
	m_pTableWidget->clearSelection();
	//m_pTableWidget->blockSignals(false);

	//Expand.
	if (m_lstFoundItems.value(index))
	{
		QTableWidgetItem *pSelected= m_lstFoundItems.value(index);
		m_lstFoundItems.value(index)->setSelected(true);
		m_pLastFoundItem=NULL; //m_lstFoundItems.value(index);
		m_pTableWidget->setCurrentItem(m_lstFoundItems.value(index));

		//issue 2556:
		if (m_lstFoundItems_LastSearch.size()>0)
		{
			m_lstFoundItems = m_lstFoundItems_LastSearch;
			blockSignals(true);
			clear();
			for (int i=0;i<m_lstFoundItems.size();i++)
			{
				QString itemText = m_lstFoundItems.at(i)->data(Qt::DisplayRole).toString().trimmed();
				int itemID		 = m_lstFoundItems.at(i)->data(Qt::UserRole).toInt();

				//issue 2556: add organization if exists:
				//SetOrganizationName(itemText,itemID);
				insertItem(i, itemText, itemID);
			}
			int nIdxOld = m_lstFoundItems.indexOf(pSelected);
			if (nIdxOld>=0 && nIdxOld<m_lstFoundItems.size())
			{
				setEditText(pSelected->data(Qt::DisplayRole).toString().trimmed());
			}
			blockSignals(false);
		}
	}
	
}

void TableFindComboBox::on_highlighted(int index)
{
	setCurrentIndex(index);
}

void TableFindComboBox::on_view_pressed(const QModelIndex &index)
{
	QKeyEvent event(QEvent::KeyRelease, Qt::Key_Return, Qt::NoModifier);
	keyPressEvent(&event);
}

//never worked: item id = 0, strItemText sometimes contains organization, sometimes not, and find does not work...
void TableFindComboBox::SetOrganizationName(QString &strItemText, int nItemID)
{
	//issue 2556: add organization if exists:
	if (m_nOrganizationColIdx>=0 && m_nItemTextColIdx>=0 && m_pLstData)
	{
		int nRow = m_pLstData->find(m_nItemTextColIdx,strItemText,true,false,false,true);
		if (nRow>=0)
		{
			QString strOrg = m_pLstData->getDataRef(nRow,m_nOrganizationColIdx).toString();
			if (!strOrg.isEmpty())
				strItemText = strItemText + " - "+ strOrg;
		}
	}
}