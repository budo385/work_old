#ifndef GUIFIELD_TEXTEDIT_H
#define GUIFIELD_TEXTEDIT_H

#include "guifield.h"
#include <QTextEdit>



/*!
	\class GuiField_TextEdit
	\brief Defines QTextEdit
	\ingroup GUICore_GUIFields

	Connects to datasource
*/
class GuiField_TextEdit : public GuiField
{
	Q_OBJECT

public:
	GuiField_TextEdit(QString strFieldName,QWidget *pWidget,GuiDataManipulator* pDataManager,DbView *TableView=NULL, QLabel *pLabel=NULL);

	void RefreshDisplay(); //DATA SOURCE -> UI
	void UsePlainText(){m_bIsPlainText=true;}  //interprets content as plain text
	void setEnabled(bool bEnable);

private:
	QTextEdit* m_pEditWidget;

	//bool m_bIsByteArray;
	bool m_bIsPlainText;

public slots:
	void FieldChanged(); //UI -> DATA SOURCE

};

#endif // GUIFIELD_TEXTEDIT_H


