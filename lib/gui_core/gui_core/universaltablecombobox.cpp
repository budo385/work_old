#include "universaltablecombobox.h"

UniversalTableComboBox::UniversalTableComboBox(QWidget *parent)
	: QComboBox(parent)
{
	setEditable(false);
	connect(this,SIGNAL(currentIndexChanged(int)),this,SLOT(SelectionChanged(int)));
}

UniversalTableComboBox::~UniversalTableComboBox()
{

}

/*!
	Called by UniversalTableWidget when widget is created

	\param nRow					- row number on table
	\param nCol					- column number on table
	\param Data					- sets data from UniversalTableWidget datasource, 
								  data is extracted from value using m_strDataSource
								  value must be DbRecordSet
	\param strDataSourceFormat	- string to format output in combobox, consist of fields inside Data
								  "<<" is separator:
								  'Field:'<<COL1<<COL2 (can contain literals enclosed in ')
	\param bIsEditable			- if false, widget is permamently disabled, no user input
*/
void UniversalTableComboBox::SetData(int nRow, int nCol,QVariant &Data,QString strDataSourceFormat,bool bIsEditable)
{

	//sets data:
	TableCellWidget::SetData(nRow,nCol,Data,strDataSourceFormat,bIsEditable);

	DbRecordSet lstData = Data.value<DbRecordSet>();
	
	//clear combo content:
	disconnect(this,SIGNAL(currentIndexChanged(int)),this,SLOT(SelectionChanged(int)));
	clear();

	//get list :format: "COLUMN_1<<'Separator'<<COLUMN_2<<COLUMN_3 ... where Separator is custom defined text printed as is
	QStringList lstDataSource=strDataSourceFormat.split("<<");
	Q_ASSERT_X(lstDataSource.size()!=0,"WidgetChild","Data Source not defined for combo box");
	int nSize =lstData.getRowCount();
	QString strItem;

	//add item into combo:
	for(int nRow=0;nRow<nSize;++nRow)
	{
		strItem="";
		for(int i=0;i<lstDataSource.size();++i)
		{
			if (lstDataSource.at(i).indexOf("'")!=-1) //if found replace '
			{
				QString strSeparator=lstDataSource.at(i);
				strItem+=strSeparator.replace("'","");
			}
			else
			{
				strItem+=lstData.getDataRef(nRow,lstDataSource.at(i)).toString();
			}
		}
		addItem(strItem);
	}
	

	//set current index:
	int nRowSelected=lstData.getSelectedRow();
	setCurrentIndex(nRowSelected);

	//set disabled:
	if(!bIsEditable)setEnabled(false);

	connect(this,SIGNAL(currentIndexChanged(int)),this,SLOT(SelectionChanged(int)));
}


/*!
	Called by UniversalTableWidget when widget already exist and data needs to be refreshed
	In combo box: selection is copied and set as current 
	If bCopyContent then operation will take some time to complete (copies whole content from datasorce into combo)
	\param value	- returns data set by SetData() (DbRecordSet), any user changes are stored in value
*/
void UniversalTableComboBox::RefreshData(QVariant &Data,bool bCopyContent)
{
	disconnect(this,SIGNAL(currentIndexChanged(int)),this,SLOT(SelectionChanged(int)));

	m_Data=Data; //assume that are same: only selection changed
	DbRecordSet lstData = Data.value<DbRecordSet>(); //extract


	if(bCopyContent)
	{
		//clear combo content:
		clear();

		//get list :format: "COLUMN_1<<'Separator'<<COLUMN_2<<COLUMN_3 ... where Separator is custom defined text printed as is
		QStringList lstDataSource=m_strDataSourceFormat.split("<<");
		Q_ASSERT_X(lstDataSource.size()!=0,"WidgetChild","Data Source not defined for combo box");
		int nSize =lstData.getRowCount();
		QString strItem;

		//add item into combo:
		for(int nRow=0;nRow<nSize;++nRow)
		{
			strItem="";
			for(int i=0;i<lstDataSource.size();++i)
			{
				if (lstDataSource.at(i).indexOf("'")!=-1) //if found replace '
				{
					QString strSeparator=lstDataSource.at(i);
					strItem+=strSeparator.replace("'","");
				}
				else
				{
					strItem+=lstData.getDataRef(nRow,lstDataSource.at(i)).toString();
				}
			}
			addItem(strItem);
		}


	}

	//set current index:
	int nRowSelected=lstData.getSelectedRow();
	setCurrentIndex(nRowSelected);


	connect(this,SIGNAL(currentIndexChanged(int)),this,SLOT(SelectionChanged(int)));

}


/*!
	Called by UniversalTableWidget for disabling/enabling user interaction with child widget
	\param bEdit	- true, control is clickable, false, control is disabled
*/
void UniversalTableComboBox::SetEditMode(bool bEdit)
{
	/*
	if(!bEdit)
		setFocusPolicy(Qt::NoFocus);
	else
		setFocusPolicy(Qt::StrongFocus);
	*/
	if(m_bEditable)setEnabled(bEdit);
}



void UniversalTableComboBox::SelectionChanged(int nIndex)
{
	DbRecordSet lstData = m_Data.value<DbRecordSet>(); //extract

	lstData.clearSelection();
	lstData.selectRow(nIndex);

	qVariantSetValue(m_Data, lstData);		//store back

	DataChanged();	//emit signal that data has changed
}


