#ifndef RICHEDITORDLG_H
#define RICHEDITORDLG_H

#include "richeditwidget.h"
#include <QtWidgets/QDialog>

class QMenuBar;
class QToolBar;
class QVBoxLayout;
class QHBoxLayout;
class QTextEdit;
class QComboBox;
class QTextCharFormat;
class QButtonGroup;

class RichEditorDlg : public QDialog
{
    Q_OBJECT

public:
	RichEditorDlg(QWidget *parent = 0);
    ~RichEditorDlg();

	void	SetHtml(QString strHtml);
	QString GetHtml();
	void	SetPlainText(QString strPlainText);
	void	SetDocument(QTextDocument *pTextDocument);
	QString GetPlainText();
	void	SetEditMode(bool bGoEdit);
	void	SetDialogTitle(QString strTitle);
	bool	IsHTMLDisplayed();
	void	SwitchFromHTMLToNormalView();

	RichEditWidget	*m_pRichEditWidget;

protected:
	QVBoxLayout		*m_pMainLayout;

	QMenuBar		*m_pMenuBar;
};

#endif // RICHEDITORDLG_H
