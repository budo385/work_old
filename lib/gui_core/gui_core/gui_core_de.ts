<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_CH">
<context>
    <name></name>
    <message>
        <source>Error Message</source>
        <translation type="obsolete">Fehlermeldung</translation>
    </message>
    <message>
        <source>Please fill user name!</source>
        <translation type="obsolete">Bitte Benutzernamen eingeben!</translation>
    </message>
    <message>
        <source>Please define a connection!</source>
        <translation type="obsolete">Bitte Verbindung definieren!</translation>
    </message>
</context>
<context>
    <name>@default</name>
    <message>
        <source>Error</source>
        <translation type="obsolete">Fehler</translation>
    </message>
</context>
<context>
    <name>AboutWidget</name>
    <message>
        <location filename="aboutwidget.cpp" line="+25"/>
        <source>TEAM EDITION</source>
        <translation>TEAM EDITION</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>BUSINESS EDITION</source>
        <translation>BUSINESS EDITION</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>PERSONAL EDITION</source>
        <translation>PERSONAL EDITION</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Version: %sP (%s %s)</source>
        <translation>Version: %sP (%s %s)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Version: %s (%s %s)</source>
        <translation>Version: %s (%s %s)</translation>
    </message>
</context>
<context>
    <name>AboutWidgetClass</name>
    <message>
        <location filename="aboutwidget.ui" line="+32"/>
        <source>SOKRATES® Communicator</source>
        <translation>SOKRATES® Communicator</translation>
    </message>
    <message>
        <location line="+113"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Arial&apos;; font-size:12pt; font-weight:600; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;edition&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Arial&apos;; font-size:12pt; font-weight:600; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Edition&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8pt; font-weight:600; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Your Personal Assistant in Today&apos;s World of Information&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8pt; font-weight:600; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Ihr persönlicher Assistent in der heutigen Welt der Informationen&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>version</source>
        <translation>Version</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Registered Owner:</source>
        <translation>Registrierter Benutzer:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>owner</source>
        <translation>Benutzer</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Serial Number:</source>
        <translation>Seriennummer:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>S/N</source>
        <translation>S/N</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Warning: This computer program is protected by copyright and international treaties.
Unauthorized reproduction or distribution of this program, or any portion of it, may result in severe civil and criminal penalties, and will be prosecuted to the maximum extent possible under the law.
</source>
        <translation>Warnung: Dieses Programm ist durch Copyright und internationales Recht geschützt.Nicht autorisierte Vervielfältigung oder Distribution dieses Programmes oder Teile davon wird zivil- und strafrechtlich verfolgt unter Anwendung sämtlicher Rechtsmittel.
</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Some icons copyright by Everaldo Coelho, see &lt;a href=&quot;http://www.everaldo.com/crystal/?action=license&quot;&gt;LGPL Licence&lt;/a&gt;.</source>
        <translation>Einzelne Icons unter Copyright von Everaldo Coelho, siehe &lt;a href=&quot;http://www.everaldo.com/crystal/?action=license&quot;&gt;LGPL Lizenz&lt;/a&gt;.</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>This program uses the GeoNames web service, see &lt;a href=&quot;http://www.geonames.org&quot;&gt;http://www.geonames.org&lt;/a&gt;.</source>
        <translation>Dieses Programm benützt den GeoNames Webdienst, siehe &lt;a href=&quot;http://www.geonames.org&quot;&gt;http://www.geonames.org&lt;/a&gt;.</translation>
    </message>
    <message>
        <location line="-20"/>
        <source>Copyright (C) 2006 - 2013 Helix Business Soft AG, Helix Business Soft d.o.o.</source>
        <translation>Copyright (C) 2006 - 2013 Helix Business Soft AG, Helix Business Soft d.o.o.</translation>
    </message>
    <message>
        <location line="-195"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ColorPickerPopup</name>
    <message>
        <source>Custom</source>
        <translation type="obsolete">Benutzerdefiniert</translation>
    </message>
</context>
<context>
    <name>DateEditWidget</name>
    <message>
        <location filename="dateeditwidget.cpp" line="+29"/>
        <source>Select Date</source>
        <translation>Auswahl Datum</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Invalid Date!</source>
        <translation>Ungültiges Datum!</translation>
    </message>
</context>
<context>
    <name>DateRangeSelectorDialog</name>
    <message>
        <location filename="daterangeselectordialog.cpp" line="+11"/>
        <source>Select Period</source>
        <translation>Zeitraum wählen</translation>
    </message>
    <message>
        <location filename="daterangeselectordialog.ui" line="+32"/>
        <source>DateRangeSelectorDialog</source>
        <translation></translation>
    </message>
    <message>
        <location line="+18"/>
        <source>From:</source>
        <translation>Von:</translation>
    </message>
    <message>
        <location line="+37"/>
        <location line="+132"/>
        <source>d.M.yyyy</source>
        <translation>d.M.yyyy</translation>
    </message>
    <message>
        <location line="-31"/>
        <source>To:</source>
        <translation>Bis:</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Ok</source>
        <translation>OK</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>DateRangeSelectorModalDialog</name>
    <message>
        <location filename="daterangeselectormodaldialog.ui" line="+20"/>
        <source>DateRangeSelectorModalDialog</source>
        <translation></translation>
    </message>
    <message>
        <location line="+24"/>
        <source>To:</source>
        <translation>Bis:</translation>
    </message>
    <message>
        <location line="+25"/>
        <location line="+78"/>
        <source>V</source>
        <translation>V</translation>
    </message>
    <message>
        <location line="-25"/>
        <source>&gt;</source>
        <translation>&gt;</translation>
    </message>
    <message>
        <location line="+72"/>
        <source>From:</source>
        <translation>Von:</translation>
    </message>
    <message>
        <location line="+47"/>
        <source>&lt;</source>
        <translation>&lt;</translation>
    </message>
</context>
<context>
    <name>DateSelectorClass</name>
    <message>
        <location filename="dateselector.ui" line="+13"/>
        <source>Select Date</source>
        <translation>Auswahl Datum</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Show &amp;All</source>
        <translation>&amp;Alles anzeigen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Show&gt;From &amp;Date</source>
        <translation>Anzeigen&gt;Ab &amp;Datum</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&amp;Per Date</source>
        <translation>&amp;Per Datum</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>From - &amp;To Date</source>
        <translation>Von - &amp;Bis Datum</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Date From:</source>
        <translation>Datum ab:</translation>
    </message>
    <message>
        <location line="+27"/>
        <location line="+182"/>
        <source>&lt;&lt;-</source>
        <translation>&lt;&lt;-</translation>
    </message>
    <message>
        <location line="-155"/>
        <location line="+182"/>
        <source>&lt;-</source>
        <translation>&lt;-</translation>
    </message>
    <message>
        <location line="-155"/>
        <location line="+182"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location line="-152"/>
        <location line="+182"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location line="-155"/>
        <location line="+182"/>
        <source>+&gt;</source>
        <translation>+&gt;</translation>
    </message>
    <message>
        <location line="-155"/>
        <location line="+182"/>
        <source>+&gt;&gt;</source>
        <translation>+&gt;&gt;</translation>
    </message>
    <message>
        <location line="-165"/>
        <source>Date To:</source>
        <translation>Datum bis:</translation>
    </message>
</context>
<context>
    <name>DateSelectorDlgClass</name>
    <message>
        <location filename="dateselectordlg.ui" line="+16"/>
        <source>Please Select a Date</source>
        <translation>Auswahl Datum</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>DbConnSettingsGUI</name>
    <message>
        <source>    Transactions supported.
</source>
        <translation type="obsolete">    Transaktionen unterstützt.
</translation>
    </message>
    <message>
        <source>    Transactions not supported.
</source>
        <translation type="obsolete">    Transaktionen nicht unterstützt.</translation>
    </message>
    <message>
        <source>    Query size supported.
</source>
        <translation type="obsolete">    Query-Grösse unterstützt.</translation>
    </message>
    <message>
        <source>    Query size not supported.
</source>
        <translation type="obsolete">    Query-Grösse nicht unterstützt.</translation>
    </message>
    <message>
        <source>    Unicode supported.
</source>
        <translation type="obsolete">    Unicode unterstützt.</translation>
    </message>
    <message>
        <source>    Unicode not supported.
</source>
        <translation type="obsolete">    Unicode nicht unterstützt.</translation>
    </message>
    <message>
        <source>    Prepared queries supported.
</source>
        <translation type="obsolete">    Prepared queries unterstützt.</translation>
    </message>
    <message>
        <source>    Prepared queries not supported.
</source>
        <translation type="obsolete">    Prepared queries nicht unterstützt.</translation>
    </message>
    <message>
        <source>    Last inserted Id supported.
</source>
        <translation type="obsolete">    Last inserted Id unterstützt.</translation>
    </message>
    <message>
        <source>    Last inserted Id not supported.
</source>
        <translation type="obsolete">    Last inserted Id nicht unterstützt.</translation>
    </message>
    <message>
        <source>    Batch operations supported.
</source>
        <translation type="obsolete">    Batch Operationen unterstützt.</translation>
    </message>
    <message>
        <source>    Batch operations not supported.
</source>
        <translation type="obsolete">    Batch Operationen nicht unterstützt.</translation>
    </message>
    <message>
        <source>Connection succeeded!

</source>
        <translation type="obsolete">Verbindung hergestellt!

</translation>
    </message>
    <message>
        <source>    Features:
</source>
        <translation type="obsolete">    Features:
</translation>
    </message>
    <message>
        <source>Connection test</source>
        <translation type="obsolete">Verbindungsprüfung</translation>
    </message>
    <message>
        <source>Unable to connect to database</source>
        <translation type="obsolete">Verbindung zur Datenbank konnte nicht hergestellt werden</translation>
    </message>
    <message>
        <location filename="dbconnsettingsgui.cpp" line="+239"/>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <location line="-94"/>
        <source>New Connection</source>
        <translation>Neue Verbindung</translation>
    </message>
    <message>
        <location line="+82"/>
        <source>User Settings Password</source>
        <translation>Passwort Benutzereinstellungen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>New Password:</source>
        <translation>Neues Passwort:</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Password must not be empty!</source>
        <translation>Passwort darf nicht leer sein!</translation>
    </message>
</context>
<context>
    <name>DbConnSettingsGUIClass</name>
    <message>
        <location filename="dbconnsettingsgui.ui" line="+42"/>
        <source>Connection Settings</source>
        <translation>Einstellungen Verbindung</translation>
    </message>
    <message>
        <location line="+23"/>
        <location line="+136"/>
        <source>Connection Name:</source>
        <translation>Verbindungsname:</translation>
    </message>
    <message>
        <location line="-95"/>
        <source>User:</source>
        <translation>Benutzer:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Port:</source>
        <translation>Port:</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Database Name:</source>
        <translation>Datenbankname:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Database Type:</source>
        <translation>Datenbanktyp:</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Connection Timeout:</source>
        <translation>Verbindungstimeout:</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>Host:</source>
        <translation>Host:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Password:</source>
        <translation>Passwort:</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Database Options:</source>
        <translation>Datenbankoptionen:</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>New</source>
        <translation>Neu</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Test Connection</source>
        <translation>Verbindung testen</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Use Password (Optional)</source>
        <translation>Passwort verwenden (optional)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Change Password</source>
        <translation>Passwort wechseln</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>Dlg_EnterPeriod</name>
    <message>
        <location filename="dlg_enterperiod.cpp" line="+7"/>
        <source>Enter Period</source>
        <translation>Zeitraum eingeben</translation>
    </message>
</context>
<context>
    <name>Dlg_EnterPeriodClass</name>
    <message>
        <location filename="dlg_enterperiod.ui" line="+13"/>
        <source>Enter Period</source>
        <translation>Eingabe Zeitperiode</translation>
    </message>
    <message>
        <location line="+72"/>
        <source>From:</source>
        <translation>Von:</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>To:</source>
        <translation>Bis:</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>Dlg_Picture</name>
    <message>
        <location filename="dlg_picture.cpp" line="+15"/>
        <location line="+14"/>
        <source>Picture Viewer</source>
        <translation>Bildbetrachter</translation>
    </message>
</context>
<context>
    <name>Dlg_PictureClass</name>
    <message>
        <location filename="dlg_picture.ui" line="+16"/>
        <source>Picture</source>
        <translation>Bild</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>FreeDaysRegister</name>
    <message>
        <location filename="freedaysregister.cpp" line="+67"/>
        <source>New Year</source>
        <translation>Neujahr</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>May 1st</source>
        <translation>1. Mai</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Christmas</source>
        <translation>Weihnachten</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>St. Stephen&apos;s Day</source>
        <translation>Stephanstag</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>August 1st</source>
        <translation>1. August</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Carnival Monday</source>
        <translation>Rosenmontag</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Ash Wednesday</source>
        <translation>Aschermittwoch</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Good Friday</source>
        <translation>Karfreitag</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Easter</source>
        <translation>Ostern</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Easter Monday</source>
        <translation>Ostermontag</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Ascension</source>
        <translation>Auffahrt</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Pentecost</source>
        <translation>Pfingsten</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Pentecost Monday</source>
        <translation>Pfingstmontag</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Corpus Christi</source>
        <translation>Fronleichnam</translation>
    </message>
</context>
<context>
    <name>GUI_Helper</name>
    <message>
        <location filename="gui_helper.cpp" line="+372"/>
        <source>Day</source>
        <translation>Tag</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>3 Days</source>
        <translation>3 Tage</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Business Days</source>
        <translation>Werktage</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Week</source>
        <translation>Woche</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>7 Days</source>
        <translation>7 Tage</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Two Weeks</source>
        <translation>Zwei Wochen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>14 Days</source>
        <translation>14 Tage</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Month</source>
        <translation>Monat</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Year</source>
        <translation>Jahr</translation>
    </message>
</context>
<context>
    <name>HideMenuManager</name>
    <message>
        <source>Disable Auto Hide</source>
        <translation type="obsolete">Automatisch Ausblenden deaktivieren</translation>
    </message>
    <message>
        <source>Auto Hide</source>
        <translation type="obsolete">Automatisch Ausblenden</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="obsolete">Schliessen</translation>
    </message>
</context>
<context>
    <name>ListWidget</name>
    <message>
        <location filename="listwidget.cpp" line="+16"/>
        <source>&amp;Delete Selection</source>
        <translation>Auswahl &amp;Löschen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Del</source>
        <translation>Del</translation>
    </message>
</context>
<context>
    <name>LoginController</name>
    <message>
        <source>Login In Progress</source>
        <translation type="obsolete">Login wird durchgeführt</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">Fehler</translation>
    </message>
    <message>
        <source>Error Message</source>
        <translation type="obsolete">Fehlermeldung</translation>
    </message>
    <message>
        <source>You have reached maximum number of login attempts!
The application will be closed now.</source>
        <translation type="obsolete">Maximale Anzahl Login-Versuche erreicht!
Die Anwendung wird geschlossen.</translation>
    </message>
</context>
<context>
    <name>LoginDlg</name>
    <message>
        <source>User Settings Password</source>
        <translation type="obsolete">Passwort Benutzereinstellungen</translation>
    </message>
    <message>
        <location filename="logindlg.cpp" line="+35"/>
        <source>Password:</source>
        <translation>Passwort:</translation>
    </message>
    <message>
        <location line="+95"/>
        <location line="+17"/>
        <source>Error Message</source>
        <translation>Fehlermeldung</translation>
    </message>
    <message>
        <source>Password does not match!</source>
        <translation type="obsolete">Passwort nicht korrekt!</translation>
    </message>
    <message>
        <location line="-17"/>
        <source>Please fill user name!</source>
        <translation>Bitte Benutzernamen eingeben!</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Please define a connection!</source>
        <translation>Bitte Verbindung definieren!</translation>
    </message>
    <message>
        <location line="-123"/>
        <source>Login</source>
        <translation>Login</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Close</source>
        <translation>Schliessen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>User:</source>
        <translation>Benutzer:</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Connection:</source>
        <translation>Verbindung:</translation>
    </message>
</context>
<context>
    <name>LoginDlgClass</name>
    <message>
        <location filename="logindlg.ui" line="+13"/>
        <source>Login</source>
        <translation>Login</translation>
    </message>
    <message>
        <source>&lt;h1&gt;&lt;b&gt;SOKRATES&lt;sup&gt;&amp;reg;&lt;/sup&gt;&amp;nbsp;Communicator&lt;/b&gt;&lt;/h1&gt;</source>
        <translation type="obsolete">&lt;h1&gt;&lt;b&gt;SOKRATES&lt;sup&gt;&amp;reg;&lt;/sup&gt;&amp;nbsp;Communicator&lt;/b&gt;&lt;/h1&gt;</translation>
    </message>
    <message>
        <location line="+76"/>
        <source>Login Password</source>
        <translation>Login-Passwort</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>User Name in a System</source>
        <translation>Benutzername in System</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Username:</source>
        <translation>Benutzername:</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Connection:</source>
        <translation>Verbindung:</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>List of defined Connections</source>
        <translation>Liste der definierten Verbindungen</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Password:</source>
        <translation>Passwort:</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="obsolete">Schliessen</translation>
    </message>
    <message>
        <location line="-94"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Arial&apos;; font-size:8pt; font-weight:600; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:18px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:xx-large;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Arial&apos;; font-size:8pt; font-weight:600; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:18px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:xx-large;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>LoginDlg_Embedded</name>
    <message>
        <location filename="logindlg_embedded.cpp" line="+180"/>
        <location line="+10"/>
        <source>Error Message</source>
        <translation>Fehlermeldung</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Please fill user name!</source>
        <translation>Bitte Benutzernamen eingeben!</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Please define a connection!</source>
        <translation>Bitte Verbindung definieren!</translation>
    </message>
    <message>
        <location line="-100"/>
        <location line="+8"/>
        <source>Login</source>
        <translation>Login</translation>
    </message>
    <message>
        <location line="-65"/>
        <source>User:</source>
        <translation>Benutzer:</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Password:</source>
        <translation>Passwort:</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Connection:</source>
        <translation>Verbindung:</translation>
    </message>
</context>
<context>
    <name>LoginDlg_EmbeddedClass</name>
    <message>
        <location filename="logindlg_embedded.ui" line="+13"/>
        <source>LoginDlg_Embedded</source>
        <translation>LoginDlg_Embedded</translation>
    </message>
    <message>
        <source>&lt;h1&gt;&lt;b&gt;SOKRATES&lt;sup&gt;&amp;reg;&lt;/sup&gt;&amp;nbsp;Communicator&lt;/b&gt;&lt;/h1&gt;</source>
        <translation type="obsolete">&lt;h1&gt;&lt;b&gt;SOKRATES&lt;sup&gt;&amp;reg;&lt;/sup&gt;&amp;nbsp;Communicator&lt;/b&gt;&lt;/h1&gt;</translation>
    </message>
    <message>
        <location line="+158"/>
        <source>Username:</source>
        <translation>Benutzername:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>User Name in a System</source>
        <translation>Benutzername in System</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Password:</source>
        <translation>Passwort:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Login Password</source>
        <translation>Login-Passwort</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Connection:</source>
        <translation>Verbindung:</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>List of defined Connections</source>
        <translation>Liste der definierten Verbindungen</translation>
    </message>
    <message>
        <source>Login</source>
        <translation type="obsolete">Login</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="obsolete">Schliessen</translation>
    </message>
</context>
<context>
    <name>MultiSortColumn</name>
    <message>
        <location filename="multisortcolumn.cpp" line="+8"/>
        <source>Configure Sort Order</source>
        <translation>Sortierreihenfolge konfigurieren</translation>
    </message>
</context>
<context>
    <name>MultiSortColumnClass</name>
    <message>
        <source>Multi Column Sorting</source>
        <translation type="obsolete">Mehrfachsortierung</translation>
    </message>
    <message>
        <source>^</source>
        <translation type="obsolete">^</translation>
    </message>
    <message>
        <source>?</source>
        <translation type="obsolete">ˇ</translation>
    </message>
    <message>
        <source>&gt;</source>
        <translation type="obsolete">&gt;</translation>
    </message>
    <message>
        <source>&lt;</source>
        <translation type="obsolete">&lt;</translation>
    </message>
    <message>
        <source>Available Columns</source>
        <translation type="obsolete">Verfügbare Spalten</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Abbrechen</translation>
    </message>
    <message>
        <source>Sorting Columns</source>
        <translation type="obsolete">Spaltensortierung</translation>
    </message>
</context>
<context>
    <name>MultiSortColumnFrame</name>
    <message>
        <location filename="multisortcolumnframe.ui" line="+14"/>
        <source>MultiSortColumnFrame</source>
        <translation>MultiSortColumnFrame</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Sorting Columns</source>
        <translation>Spaltensortierung</translation>
    </message>
    <message>
        <location line="+58"/>
        <source>^</source>
        <translation>^</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>&gt;</source>
        <translation>&gt;</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>&lt;</source>
        <translation>&lt;</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Available Columns</source>
        <translation>Verfügbare Spalten</translation>
    </message>
    <message>
        <location line="-87"/>
        <source>ˇ</source>
        <translation>ˇ</translation>
    </message>
</context>
<context>
    <name>MultiSortTable</name>
    <message>
        <location filename="multisorttable.cpp" line="+47"/>
        <source>&amp;Delete Selection</source>
        <translation>Auswahl &amp;Löschen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Del</source>
        <translation>Del</translation>
    </message>
    <message>
        <location line="+51"/>
        <source>Column Name</source>
        <translation>Spaltenname</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Ascending</source>
        <translation>Aufsteigend</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Descending</source>
        <translation>Absteigend</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Screen Width (px)</source>
        <translation>Breite auf Schirm (px)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Report Width (mm)</source>
        <translation>Breite auf Ausdruck (mm)</translation>
    </message>
</context>
<context>
    <name>NetworkConnectionsDlg</name>
    <message>
        <source>None</source>
        <translation type="obsolete">Keine</translation>
    </message>
    <message>
        <location filename="networkconnectionsdlg.cpp" line="+135"/>
        <source>New Connection</source>
        <translation>Neue Verbindung</translation>
    </message>
    <message>
        <location line="+80"/>
        <source>User Settings Password</source>
        <translation>Passwort Einstellungen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>New Password:</source>
        <translation>Neues Passwort:</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Password must not be empty!</source>
        <translation>Passwort darf nicht leer sein!</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <location line="-213"/>
        <source>Socks5 Proxy</source>
        <translation>Socks5 Proxy</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Http  Proxy</source>
        <translation>Http  Proxy</translation>
    </message>
</context>
<context>
    <name>NetworkConnectionsDlgClass</name>
    <message>
        <location filename="networkconnectionsdlg.ui" line="+16"/>
        <source>Network Connections</source>
        <translation>Netzverbindungen</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Connection</source>
        <translation>Verbindung</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Use Compression</source>
        <translation>Komprimierung verwenden</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Use SSL</source>
        <translation>SSL verwenden</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Database ID:</source>
        <translation>Datenbank-ID:</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Use Dynamic Fail Over</source>
        <translation>Dynamisches Fail-Over verwenden</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>IP Lookup URL:</source>
        <translation>IP Lookup URL:</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Enter custom connection name here</source>
        <translation>Benutzerdefinieren Verbindungsnamen eintragen</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+280"/>
        <source>Connection Name:</source>
        <translation>Verbindungsname:</translation>
    </message>
    <message>
        <location line="-270"/>
        <location line="+79"/>
        <source>Port:</source>
        <translation>Port:</translation>
    </message>
    <message>
        <location line="-39"/>
        <source>Application Server IP Address:</source>
        <translation>Application Server - IP Addresse:</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Proxy</source>
        <translation>Proxy</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Host ID:</source>
        <translation>Host ID:</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>User:</source>
        <translation>Benutzer:</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Password:</source>
        <translation>Passwort:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Proxy type:</source>
        <translation>Proxy Typ:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Use Proxy</source>
        <translation>Proxy verwenden</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>New</source>
        <translation>Neu</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>Use Password (Optional)</source>
        <translation>Passwort verwenden (optional)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Change Password</source>
        <translation>Passwort wechseln</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>NetworkConnectionsDlg_Embedded</name>
    <message>
        <location filename="networkconnectionsdlg_embedded.cpp" line="+14"/>
        <source>Socks5 Proxy</source>
        <translation>Socks5 Proxy</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Http  Proxy</source>
        <translation>Http  Proxy</translation>
    </message>
    <message>
        <location line="+119"/>
        <source>New Connection</source>
        <translation>Neue Verbindung</translation>
    </message>
    <message>
        <location line="+80"/>
        <source>User Settings Password</source>
        <translation>Passwort Benutzereinstellungen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>New Password:</source>
        <translation>Neues Passwort:</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Password must not be empty!</source>
        <translation>Passwort darf nicht leer sein!</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
</context>
<context>
    <name>NetworkConnectionsDlg_EmbeddedClass</name>
    <message>
        <location filename="networkconnectionsdlg_embedded.ui" line="+19"/>
        <source>NetworkConnectionsDlg_Embedded</source>
        <translation>NetworkConnectionsDlg_Embedded</translation>
    </message>
    <message>
        <location line="+68"/>
        <source>New</source>
        <translation>Neu</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location line="+20"/>
        <location line="+45"/>
        <source>Connection Name:</source>
        <translation>Verbindungsname:</translation>
    </message>
    <message>
        <location line="-12"/>
        <source>Connection</source>
        <translation>Verbindung</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Enter custom connection name here</source>
        <translation>Benutzerdefinieren Verbindungsnamen eintragen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Application Server IP Address:</source>
        <translation>Application Server - IP Addresse:</translation>
    </message>
    <message>
        <location line="+13"/>
        <location line="+101"/>
        <source>Port:</source>
        <translation>Port:</translation>
    </message>
    <message>
        <location line="-84"/>
        <source>Database ID:</source>
        <translation>Datenbank-ID:</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Use SSL</source>
        <translation>SSL verwenden</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Use Compression</source>
        <translation>Komprimierung verwenden</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Use Dynamic Fail Over</source>
        <translation>Dynamisches Fail-Over verwenden</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>IP Lookup URL:</source>
        <translation>IP Lookup URL:</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Proxy</source>
        <translation>Proxy</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Password:</source>
        <translation>Passwort:</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Proxy type:</source>
        <translation>Proxy Typ:</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>User:</source>
        <translation>Benutzer:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Use Proxy</source>
        <translation>Proxy verwenden</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Host ID:</source>
        <translation>Host ID:</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Use Password (Optional)</source>
        <translation>Passwort verwenden (optional)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Change Password</source>
        <translation>Passwort wechseln</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>PictureHelper</name>
    <message>
        <source>Choose picture</source>
        <translation type="obsolete">Bild wählen</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">Fehler</translation>
    </message>
    <message>
        <source>Picture can not be loaded!</source>
        <translation type="obsolete">Bild kann nicht geladen werden!</translation>
    </message>
    <message>
        <source>Save Picture As</source>
        <translation type="obsolete">Bild speichern als</translation>
    </message>
    <message>
        <source>Error while saving picture!</source>
        <translation type="obsolete">Fehler beim Speichern des Bildes!</translation>
    </message>
    <message>
        <source>Picture is mandatory field!</source>
        <translation type="obsolete">Bild ist erforderlich!</translation>
    </message>
</context>
<context>
    <name>PictureWidget</name>
    <message>
        <source>View</source>
        <translation type="obsolete">Ansicht</translation>
    </message>
    <message>
        <location filename="picturewidget.cpp" line="+469"/>
        <source>&amp;Load Picture From File</source>
        <translation>Bild aus &amp;Datei laden</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Save Picture To File</source>
        <translation>Bild in Datei &amp;speichern</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Erase Picture</source>
        <translation>Bild &amp;löschen</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Print</source>
        <translation>&amp;Drucken</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Paste Picture From Clipboard</source>
        <translation>Bild aus Zwischenablage einfügen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>CTRL+V</source>
        <translation>CTRL+V</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Copy Picture To Clipboard</source>
        <translation>Bild in Zwischenablage kopieren</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>CTRL+C</source>
        <translation>CTRL+C</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Zoom &amp;In (25%)</source>
        <translation>&amp;Einzoomen (25%)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Ctrl++</source>
        <translation>Ctrl++</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Zoom &amp;Out (25%)</source>
        <translation>&amp;Auszoomen (25%)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Ctrl+-</source>
        <translation>Ctrl+-</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Original Size</source>
        <translation>&amp;Originalgrösse</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Resize to Window Width</source>
        <translation>Auf &amp;Fensterbreite skalieren</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Ctrl+F</source>
        <translation>Ctrl+F</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Open picture in new window</source>
        <translation>Bild in neuem Fenster Ö&amp;ffnen</translation>
    </message>
    <message>
        <location line="+513"/>
        <source>Connect to </source>
        <translation>Verbinden mit</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Failed to open the file!</source>
        <translation type="obsolete">Datei kann nicht geöffnet werden!</translation>
    </message>
    <message>
        <source>Invalid file format header!</source>
        <translation type="obsolete">Dateiformat ungültig!</translation>
    </message>
    <message>
        <location filename="macros.h" line="+32"/>
        <location line="+8"/>
        <location line="+10"/>
        <location line="+8"/>
        <location filename="picturehelper.cpp" line="+72"/>
        <location line="+58"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <source>Dialog will be closed in: </source>
        <translation type="obsolete">Fenster wird geschlossen in: </translation>
    </message>
    <message>
        <source> seconds.</source>
        <translation type="obsolete"> Sekunden.</translation>
    </message>
    <message>
        <location filename="property_browser/qttreepropertybrowser.cpp" line="+184"/>
        <source>Property</source>
        <translation>Eigenschaft</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Value</source>
        <translation>Wert</translation>
    </message>
    <message>
        <source>Select Date</source>
        <translation type="obsolete">Auswahl Datum</translation>
    </message>
    <message>
        <source>Invalid Date!</source>
        <translation type="obsolete">Ungültiges Datum!</translation>
    </message>
    <message>
        <source>Picture Viewer</source>
        <translation type="obsolete">Bildbetrachter</translation>
    </message>
    <message>
        <source>Disable Auto Hide</source>
        <translation type="obsolete">Automatisch Ausblenden deaktivieren</translation>
    </message>
    <message>
        <source>Auto Hide</source>
        <translation type="obsolete">Automatisch Ausblenden</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="obsolete">Schliessen</translation>
    </message>
    <message>
        <source>Login In Progress</source>
        <translation type="obsolete">Login wird durchgeführt</translation>
    </message>
    <message>
        <location filename="logindialogabstract.cpp" line="+55"/>
        <location line="+85"/>
        <source>Error Message</source>
        <translation>Fehlermeldung</translation>
    </message>
    <message>
        <location line="-85"/>
        <location line="+85"/>
        <source>Password does not match!</source>
        <translation>Passwort nicht korrekt!</translation>
    </message>
    <message>
        <source>Please fill user name!</source>
        <translation type="obsolete">Bitte Benutzernamen eingeben!</translation>
    </message>
    <message>
        <source>Please define a connection!</source>
        <translation type="obsolete">Bitte Verbindung definieren!</translation>
    </message>
    <message>
        <source>Configure Sort Order</source>
        <translation type="obsolete">Sortierreihenfolge konfigurieren</translation>
    </message>
    <message>
        <location filename="picturehelper.cpp" line="-76"/>
        <source>Choose picture</source>
        <translation>Bild wählen</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Picture can not be loaded!</source>
        <translation>Bild kann nicht geladen werden!</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Save Picture As</source>
        <translation>Bild speichern als</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Error while saving picture!</source>
        <translation>Fehler beim Speichern des Bildes!</translation>
    </message>
    <message>
        <location line="+134"/>
        <source>Picture is mandatory field!</source>
        <translation>Bild ist erforderlich!</translation>
    </message>
    <message>
        <location filename="picturewidget.cpp" line="-761"/>
        <source>View</source>
        <translation>Ansicht</translation>
    </message>
    <message>
        <location filename="logindialogabstract.cpp" line="-95"/>
        <location line="+85"/>
        <source>User Settings Password</source>
        <translation>Passwort Benutzereinstellungen</translation>
    </message>
    <message>
        <location line="-84"/>
        <location line="+85"/>
        <source>Password:</source>
        <translation>Passwort:</translation>
    </message>
</context>
<context>
    <name>QtBoolPropertyManager</name>
    <message>
        <location filename="property_browser/qtpropertymanager.cpp" line="+978"/>
        <source>True</source>
        <translation>An</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>False</source>
        <translation>Aus</translation>
    </message>
</context>
<context>
    <name>QtColorPicker</name>
    <message>
        <source>Black</source>
        <translation type="obsolete">Schwarz</translation>
    </message>
    <message>
        <source>White</source>
        <translation type="obsolete">Weiss</translation>
    </message>
    <message>
        <source>Red</source>
        <translation type="obsolete">Rot</translation>
    </message>
    <message>
        <source>Dark red</source>
        <translation type="obsolete">Dunkelrot</translation>
    </message>
    <message>
        <source>Green</source>
        <translation type="obsolete">Grün</translation>
    </message>
    <message>
        <source>Dark green</source>
        <translation type="obsolete">Dunkelgrün</translation>
    </message>
    <message>
        <source>Blue</source>
        <translation type="obsolete">Blau</translation>
    </message>
    <message>
        <source>Dark blue</source>
        <translation type="obsolete">Dunkelblau</translation>
    </message>
    <message>
        <source>Cyan</source>
        <translation type="obsolete">Zyan</translation>
    </message>
    <message>
        <source>Dark cyan</source>
        <translation type="obsolete">Dunkelzyan</translation>
    </message>
    <message>
        <source>Magenta</source>
        <translation type="obsolete">Magenta</translation>
    </message>
    <message>
        <source>Dark magenta</source>
        <translation type="obsolete">Dunkelmagenta</translation>
    </message>
    <message>
        <source>Yellow</source>
        <translation type="obsolete">Gelb</translation>
    </message>
    <message>
        <source>Dark yellow</source>
        <translation type="obsolete">Dunkelgelb</translation>
    </message>
    <message>
        <source>Gray</source>
        <translation type="obsolete">Grau</translation>
    </message>
    <message>
        <source>Dark gray</source>
        <translation type="obsolete">Dunkelgrau</translation>
    </message>
    <message>
        <source>Light gray</source>
        <translation type="obsolete">Hellgrau</translation>
    </message>
    <message>
        <source>Custom</source>
        <translation type="obsolete">Benutzerdefiniert</translation>
    </message>
</context>
<context>
    <name>QtColorPropertyManager</name>
    <message>
        <location line="+3152"/>
        <source>[%1, %2, %3] (%4)</source>
        <translation>[%1, %2, %3] (%4)</translation>
    </message>
    <message>
        <location line="+64"/>
        <source>Red</source>
        <translation>Rot</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Green</source>
        <translation>Grün</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Blue</source>
        <translation>Blau</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Alpha</source>
        <translation>Alpha</translation>
    </message>
</context>
<context>
    <name>QtFontPropertyManager</name>
    <message>
        <location line="-426"/>
        <source>[%1, %2]</source>
        <translation>[%1, %2]</translation>
    </message>
    <message>
        <location line="+71"/>
        <source>Family</source>
        <translation>Familie</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Point Size</source>
        <translation>Punktgrösse</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Bold</source>
        <translation>Fett</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Italic</source>
        <translation>Schrägschrift</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Underline</source>
        <translation>Unterstrichen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Strikeout</source>
        <translation>Durchgestrichen</translation>
    </message>
</context>
<context>
    <name>QtPointPropertyManager</name>
    <message>
        <location line="-2178"/>
        <source>(%1, %2)</source>
        <translation>(%1, %2)</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>X</source>
        <translation>X</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Y</source>
        <translation>Y</translation>
    </message>
</context>
<context>
    <name>QtRectPropertyManager</name>
    <message>
        <location line="+695"/>
        <source>[(%1, %2), %3 x %4]</source>
        <translation>[(%1, %2), %3 x %4]</translation>
    </message>
    <message>
        <location line="+117"/>
        <source>X</source>
        <translation>X</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Y</source>
        <translation>Y</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Width</source>
        <translation>Breite</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Height</source>
        <translation>Höhe</translation>
    </message>
</context>
<context>
    <name>QtSizePolicyPropertyManager</name>
    <message>
        <location line="+812"/>
        <source>[%1, %2, %3, %4]</source>
        <translation>[%1, %2, %3, %4]</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>Horizontal Policy</source>
        <translation>Regel horizontal</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Vertical Policy</source>
        <translation>Regel vertikal</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Horizontal Stretch</source>
        <translation>Horizontal verbreitern</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Vertical Stretch</source>
        <translation>Vertikal verbreitern</translation>
    </message>
</context>
<context>
    <name>QtSizePropertyManager</name>
    <message>
        <location line="-1489"/>
        <source>%1 x %2</source>
        <translation>%1 x %2</translation>
    </message>
    <message>
        <location line="+199"/>
        <source>Width</source>
        <translation>Breite</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Height</source>
        <translation>Höhe</translation>
    </message>
</context>
<context>
    <name>QtTreePropertyBrowserPrivate</name>
    <message>
        <source>Property</source>
        <translation type="obsolete">Eigenschaft</translation>
    </message>
    <message>
        <source>Value</source>
        <translation type="obsolete">Wert</translation>
    </message>
</context>
<context>
    <name>RichEditWidget</name>
    <message>
        <location filename="richeditwidget.cpp" line="+587"/>
        <source>&amp;Open</source>
        <translation>Ö&amp;ffnen</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Save</source>
        <translation>&amp;Speichern</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Export as HTML</source>
        <translation>Als HTML &amp;exportieren</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Export as PDF</source>
        <translation>Als PDF exportieren</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>&amp;Save as...</source>
        <translation>Speichern &amp;unter...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Undo</source>
        <translation>&amp;Rückgängig</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Redo</source>
        <translation>&amp;Wiederholen</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Cu&amp;t</source>
        <translation>&amp;Ausschneiden</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Copy</source>
        <translation>&amp;Kopieren</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Paste</source>
        <translation>&amp;Einfügen</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>&amp;Bold</source>
        <translation>&amp;Fett</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;Italic</source>
        <translation>&amp;Schrägschrift</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>&amp;Underline</source>
        <translation>&amp;Unterstrichen</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>&amp;Left</source>
        <translation>&amp;Links</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>C&amp;enter</source>
        <translation>&amp;Zentriert</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Right</source>
        <translation>&amp;Rechts</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Justify</source>
        <translation>&amp;Blocksatz</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Colo&amp;r...</source>
        <translation>&amp;Farbe...</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Open in dialog</source>
        <translation>In &amp;Fenster öffnen</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Search Previous</source>
        <translation>Suche nach oben</translation>
    </message>
    <message>
        <location line="+55"/>
        <location line="+882"/>
        <source>Open File...</source>
        <translation>Datei öffnen...</translation>
    </message>
    <message>
        <location line="-881"/>
        <source>HTML-Files (*.htm *.html);;All Files (*)</source>
        <translation>HTML-Dateien (*.htm *.html);;Alle Dateien (*)</translation>
    </message>
    <message>
        <source>Save as...</source>
        <translation type="obsolete">Speichern unter...</translation>
    </message>
    <message>
        <location line="-194"/>
        <source>&amp;Print</source>
        <translation>&amp;Drucken</translation>
    </message>
    <message>
        <location line="+206"/>
        <source>Open File Error!</source>
        <translation>Fehler beim Öffnen der Datei!</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Unable to open desired file!</source>
        <translation>Die gewünschte Datei kann nicht geöffnet werden!</translation>
    </message>
    <message>
        <location line="+626"/>
        <source>Journal Entry for </source>
        <translation>Journaleintrag für </translation>
    </message>
    <message>
        <location line="-571"/>
        <source>Export as HTML...</source>
        <translation>Als HTML exportieren...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>HTML-Files (*.htm *.html);;</source>
        <translation>HTML-Dateien (*.htm *.html);;</translation>
    </message>
    <message>
        <location line="+580"/>
        <location line="+75"/>
        <source>Date: </source>
        <translation>Datum: </translation>
    </message>
    <message>
        <location line="-67"/>
        <location line="+75"/>
        <source>By: </source>
        <translation>Von: </translation>
    </message>
    <message>
        <location line="-33"/>
        <source>Note: </source>
        <translation>Notiz: </translation>
    </message>
    <message>
        <location line="+78"/>
        <source>Contact: </source>
        <translation>Kontakt: </translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Number: </source>
        <translation>Nummer: </translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Owner: </source>
        <translation>Besitzer: </translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Time: </source>
        <translation>Zeit: </translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Project: </source>
        <translation>Projekt: </translation>
    </message>
    <message>
        <location line="-917"/>
        <source>Insert Picture</source>
        <translation>Bild einfügen</translation>
    </message>
    <message>
        <location line="+963"/>
        <source>Image Files (*.jpg *.png *.gif);;All Files (*)</source>
        <translation>Bilddateien (*.jpg *.png *.gif);;Alle Dateien(*)</translation>
    </message>
    <message>
        <source>Modify html code for emails</source>
        <translation type="obsolete">Bearbeiten des html-Quelltextes von E-Mails</translation>
    </message>
    <message>
        <location line="-953"/>
        <location line="+548"/>
        <source>Larger</source>
        <translation>Grösser</translation>
    </message>
    <message>
        <location line="-543"/>
        <source>Search Next</source>
        <translation>Suche nach unten</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Extended Search</source>
        <translation>Erweiterte Suche</translation>
    </message>
    <message>
        <location line="+530"/>
        <source>Smaller</source>
        <translation>Kleiner</translation>
    </message>
    <message>
        <location line="-668"/>
        <source>Export as Open Document Format ODT</source>
        <translation>Als Open Document Format ODT exportieren</translation>
    </message>
    <message>
        <location line="+271"/>
        <source>Export as ODF...</source>
        <translation>Als ODF exportieren...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>ODF-Files (*.odf);;</source>
        <translation>ODF-Files (*.odf);;</translation>
    </message>
    <message>
        <location line="+778"/>
        <source>Write Error!</source>
        <translation>Schreibfehler!</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Unable to write to desired file!</source>
        <translation>Die Datei kann nicht geschrieben werden!</translation>
    </message>
    <message>
        <location line="-927"/>
        <source>HTML Source Code View</source>
        <translation>HTML-Quellcodeansicht</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Strikethrough</source>
        <translation>Durchgestrichen</translation>
    </message>
    <message>
        <location line="+961"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Failed to load picture from &apos;%1&apos;!</source>
        <translation>Bild &apos;%1&apos; kann nicht geladen werden!</translation>
    </message>
</context>
<context>
    <name>RichEditorDlg</name>
    <message>
        <location filename="richeditordlg.cpp" line="+32"/>
        <source>&amp;File</source>
        <translation>&amp;Datei</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Edit</source>
        <translation>&amp;Bearbeiten</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>&amp;Format</source>
        <translation>&amp;Format</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>SOKRATES Editor</source>
        <translation>SOKRATES Editor</translation>
    </message>
</context>
<context>
    <name>RichEditorDlgClass</name>
    <message>
        <location filename="richeditordlg.ui" line="+16"/>
        <source>Rich Editor Dialog</source>
        <translation>Editor</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>RichEditorSearchDialog</name>
    <message>
        <location filename="richeditorsearchdialog.cpp" line="+7"/>
        <source>Find Text</source>
        <translation>Text suchen</translation>
    </message>
</context>
<context>
    <name>RichEditorSearchDialogClass</name>
    <message>
        <location filename="richeditorsearchdialog.ui" line="+26"/>
        <source>RichEditorSearchDialog</source>
        <translation>RichEditorSearchDialog</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Find Text:</source>
        <translation>Text suchen:</translation>
    </message>
    <message>
        <location line="+10"/>
        <location line="+3"/>
        <source>Search Up</source>
        <translation>Aufwärts suchen</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+3"/>
        <source>Case Sensitivity</source>
        <translation>Gross-/Kleinschreibung beachten</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+3"/>
        <source>Match Whole Word</source>
        <translation>Nur ganze Wörter</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+3"/>
        <source>Find Next</source>
        <translation>Suche weiter</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Close</source>
        <translation>Schliessen</translation>
    </message>
    <message>
        <location line="+20"/>
        <location line="+3"/>
        <source>Regular Expression</source>
        <translation>Regulärer Ausdruck</translation>
    </message>
</context>
<context>
    <name>ScrollableMsgBoxClass</name>
    <message>
        <location filename="scrollablemsgbox.ui" line="+17"/>
        <source>ScrollableMsgBox</source>
        <translation>ScrollableMsgBox</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>TestConnResultClass</name>
    <message>
        <location filename="testconnresult.ui" line="+34"/>
        <source>Test Connection</source>
        <translation>Testverbindung</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Ok</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>TimeMessageBox</name>
    <message>
        <location filename="timemessagebox.cpp" line="+48"/>
        <source>Dialog will be closed in: </source>
        <translation>Fenster wird geschlossen in: </translation>
    </message>
    <message>
        <location line="+2"/>
        <source> seconds.</source>
        <translation> Sekunden.</translation>
    </message>
</context>
<context>
    <name>TwixTel</name>
    <message>
        <source>Failed to load twxapi32.dll!</source>
        <translation type="obsolete">Twxapi32.dll konnte nicht geladen werden!</translation>
    </message>
    <message>
        <source>Your version of twxapi32.dll does not export required methods!</source>
        <translation type="obsolete">Nicht unterstützte Version von twxapi32.dll (TwixTel-Schnittstelle)!</translation>
    </message>
    <message>
        <source>Failed initializing Twixtel engine!</source>
        <translation type="obsolete">Twixtel Schnittstelle konnte nicht initialisiert werden!</translation>
    </message>
</context>
<context>
    <name>UniversalTableFindWidget</name>
    <message>
        <location filename="universaltablefindwidget.cpp" line="+31"/>
        <source>Find:</source>
        <translation>Suchen:</translation>
    </message>
</context>
<context>
    <name>UniversalTableWidget</name>
    <message>
        <location filename="universaltablewidget.cpp" line="+1014"/>
        <source>&amp;Add New Entry</source>
        <translation>&amp;Neuen Eintrag hinzufügen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Ins</source>
        <translation>Ins</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>&amp;Delete Selection</source>
        <translation>&amp; Auswahl löschen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Del</source>
        <translation>Del</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Delete &amp;All</source>
        <translation>&amp;Alles löschen</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+66"/>
        <source>CTRL+S</source>
        <translation>CTRL+S</translation>
    </message>
    <message>
        <location line="-57"/>
        <source>Duplicate &amp;Selection</source>
        <translation>Auswahl &amp;duplizieren</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>CTRL+D</source>
        <translation>CTRL+D</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>&amp;Select All</source>
        <translation>&amp;Alles auswählen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>CTRL+A</source>
        <translation>CTRL+A</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>D&amp;eSelect All</source>
        <translation>Auswahl a&amp;ufheben</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>CTRL+E</source>
        <translation>CTRL+E</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>S&amp;ort</source>
        <translation>&amp;Sortieren</translation>
    </message>
</context>
<context>
    <name>UniversalTableWidgetEx</name>
    <message>
        <location filename="universaltablewidgetex.cpp" line="+1047"/>
        <source>&amp;Add New Entry</source>
        <translation>&amp;Neuen Eintrag hinzufügen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Ins</source>
        <translation>Ins</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>&amp;Delete Selection</source>
        <translation>Auswahl &amp;löschen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Del</source>
        <translation>Del</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Delete &amp;All</source>
        <translation>&amp;Alles löschen</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+38"/>
        <source>CTRL+S</source>
        <translation>CTRL+S</translation>
    </message>
    <message>
        <location line="-30"/>
        <source>Duplicate &amp;Selection</source>
        <translation>Auswahl &amp;duplizieren</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>CTRL+D</source>
        <translation>CTRL+D</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>&amp;Select All</source>
        <translation>&amp;Alles auswählen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>CTRL+A</source>
        <translation>CTRL+A</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>D&amp;eSelect All</source>
        <translation>Auswahl a&amp;ufheben</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>CTRL+E</source>
        <translation>CTRL+E</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>S&amp;ort</source>
        <translation>&amp;Sortieren</translation>
    </message>
</context>
<context>
    <name>UniversalTreeFindWidget</name>
    <message>
        <location filename="universaltreefindwidget.cpp" line="+30"/>
        <source>Find:</source>
        <translation>Suchen:</translation>
    </message>
</context>
<context>
    <name>UniversalTreeWidget</name>
    <message>
        <location filename="universaltreewidget.cpp" line="+589"/>
        <source>&amp;Expand Tree</source>
        <translation>&amp;Zweige öffnen</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Collapse</source>
        <translation>Zweige &amp;schliessen</translation>
    </message>
</context>
<context>
    <name>WizardBase</name>
    <message>
        <location filename="wizardbase.cpp" line="+18"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&lt; &amp;Back</source>
        <translation>&lt;&amp;Zurück</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Next &gt;</source>
        <translation>Weiter &gt;</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Finish</source>
        <translation>&amp;Beenden</translation>
    </message>
    <message>
        <location line="+236"/>
        <source>Confirmation</source>
        <translation>Bestätigen</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Try again?</source>
        <translation>Erneut versuchen?</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>No</source>
        <translation>Nein</translation>
    </message>
</context>
<context>
    <name>WizardPage</name>
    <message>
        <location filename="wizardpage.cpp" line="+15"/>
        <source>&amp;Finish</source>
        <translation>&amp;Fertigstellen</translation>
    </message>
</context>
<context>
    <name>biginputdialogClass</name>
    <message>
        <source>biginputdialog</source>
        <translation type="obsolete">biginputdialog</translation>
    </message>
    <message>
        <location filename="biginputdialog.ui" line="+36"/>
        <source>TextLabel</source>
        <translation>TextLabel</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location line="-58"/>
        <source>Enter Password</source>
        <translation>Eingabe Passwort</translation>
    </message>
</context>
<context>
    <name>generatecodedlg</name>
    <message>
        <source>Could not find parent node!</source>
        <translation type="obsolete">Übergeordnetes element nicht gefunden!</translation>
    </message>
    <message>
        <source>This code is already taken!
Please try another one.</source>
        <translation type="obsolete">Code schon verwendet!
Bitte neuen Code eingeben.</translation>
    </message>
</context>
<context>
    <name>generatecodedlgClass</name>
    <message>
        <source>Define New Hierarchical Code</source>
        <translation type="obsolete">Neuen hierarchischen Code eingeben</translation>
    </message>
    <message>
        <source>New Name:</source>
        <translation type="obsolete">Neuer Name:</translation>
    </message>
    <message>
        <source>Actual Code:</source>
        <translation type="obsolete">Aktueller Code:</translation>
    </message>
    <message>
        <source>New Code:</source>
        <translation type="obsolete">Neuer Code:</translation>
    </message>
    <message>
        <source>Use Template</source>
        <translation type="obsolete">Vorlage verwenden</translation>
    </message>
    <message>
        <source>Proposal Same Level</source>
        <translation type="obsolete">Vorschlag gleiche Stufe</translation>
    </message>
    <message>
        <source>Proposal Sub-Level</source>
        <translation type="obsolete">Vorschlag Unterstufe</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation type="obsolete">&amp;OK</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Abbrechen</translation>
    </message>
</context>
<context>
    <name>multisortClass</name>
    <message>
        <source>Multi Column Sorting</source>
        <translation type="obsolete">Mehrfachsortierung</translation>
    </message>
    <message>
        <source>^</source>
        <translation type="obsolete">^</translation>
    </message>
    <message>
        <source>?</source>
        <translation type="obsolete">ˇ</translation>
    </message>
    <message>
        <source>&gt;</source>
        <translation type="obsolete">&gt;</translation>
    </message>
    <message>
        <source>&lt;</source>
        <translation type="obsolete">&lt;</translation>
    </message>
    <message>
        <source>Available Columns</source>
        <translation type="obsolete">Verfügbare Spalten</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Abbrechen</translation>
    </message>
    <message>
        <source>Sorting Columns</source>
        <translation type="obsolete">Spaltensortierung</translation>
    </message>
</context>
<context>
    <name>multisortcolumnclass</name>
    <message>
        <location filename="multisortcolumn.ui" line="+14"/>
        <source>Multi Column Sorting</source>
        <translation>Mehrfachsortierung</translation>
    </message>
    <message>
        <location line="+55"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
</TS>
