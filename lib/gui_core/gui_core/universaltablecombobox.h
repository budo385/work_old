#ifndef UNIVERSALTABLECOMBOBOX_H
#define UNIVERSALTABLECOMBOBOX_H

#include <QComboBox>
#include "universaltablecelldwidget.h"
#include "common/common/dbrecordset.h"



/*!
	\class  UniversalTableComboBox
	\ingroup GUICore_UniversalTable
	\brief  Widget used in UniversalTableWidget

	Use:
	- when adding column to UniversalTableWidget, mark column type=COL_TYPE_COMBOBOX (see MVIEW_GRID_COLUMN_SETUP)
	- by default, column (inside  UniversalTableWidget datasource list) must contain DbRecordSet (list)
	- ComboBox is read only (only selection)
	- Data that will be displayed in combo is defined by column datasource text (see MVIEW_GRID_COLUMN_SETUP)
	  format: "COLUMN_1<<'Separator'<<COLUMN_2<<COLUMN_3 ... where Separator is custom defined text printed as is
	- any user change (selection) is propagated trough UniversalTableWidget and stored in datasource DbRecordSet
*/
class UniversalTableComboBox : public QComboBox, public TableCellWidget
{
	Q_OBJECT

public:
    UniversalTableComboBox(QWidget *parent);
    ~UniversalTableComboBox();

	void SetData(int nRow, int nCol,QVariant &Data,QString strDataSourceFormat="",bool bIsEditable=true); //called by UniversalTableWidget when widget is created
	//void SetData(int nRow, int nCol,QVariant &Data,QString m_strDataSourceFormat=""); //called by UniversalTableWidget when widget is created
	void RefreshData(QVariant &Data,bool bCopyContent=false);	//called by UniversalTableWidget in refresh method when widget data is only refreshed (data -> screen)
	void SetEditMode(bool bEdit=true);	//called by UniversalTableWidget for disabling/enabling user interaction with child widget

private slots:

	void SelectionChanged(int nIndex);

private:

	DbRecordSet m_lstData;

	



    
};

#endif // UNIVERSALTABLECOMBOBOX_H


