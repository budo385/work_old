#ifndef GUI_HELPER_H
#define GUI_HELPER_H

#include <QtCore>
#include <QWidget>
#include <QGridLayout>
#include <QLabel>
#include <QPixmap>
#include <QPushButton>
#include <QMouseEvent>
#include <QTabWidget>
#include <QComboBox>

class GUI_Helper : public QObject
{
	Q_OBJECT

public:

	enum COLOR
	{
		BTN_COLOR_GREEN,
		BTN_COLOR_GREEN_1,
		BTN_COLOR_BROWN,
		BTN_COLOR_PINK,
		BTN_COLOR_SAND,
		BTN_COLOR_YELLOW
	};

	static void SetButtonTextIcon(QPushButton *pWidget,QString strText,QString strIconPath="");
	static void SetButtonColor(QWidget *pWidget, QColor color);
	static void SetButtonTextColor(QWidget *pWidget, QColor color);
	static void SetWidgetFontWeight(QWidget *pWidget, int weight);
	static void AdjustFixedWidget(QWidget *pWidgetExpandning1,QWidget *pWidgetFixed, bool bHorizontal=true);
	static void CreateStyledButton(QPushButton *pWidget,QString strIconPath,QString strTextHtml, int nIdent,QString strStyle, int nSpacing=10,bool bUseButtonText=false, int nIconWidth=-1, int nIconHeight=-1);
	static void SetStyledButtonColorBkg(QPushButton *pWidget,int nColor, QString strFontColor="black");
	static QString GetFontStyleFromWidget(QPushButton *pWidget);
	static void SetTabFont(QTabWidget *pWidget ,QString strFont="");

	//window properties->personal settings
	static int GetMenuWidthForFUI(int nFUIType);
	static void SetMenuWidthForFUI(int nFUIType,int nWidth);
	static void ReadWindowProperties(QByteArray &byteVal);  //call on log in
	static void SaveWindowProperties(QByteArray &byteVal);  //call on log out/before
	static bool IsWidgetOutOfScreen(QWidget *pWidget);

	//Date selector and calendar time selector.
	static void GetDateRangeComboItems(QComboBox *cboRangeSize);

private:
	
};





#endif // GUI_HELPER_H


