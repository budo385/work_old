#include "texteditwidget.h"
#include "common/common/entity_id_collection.h"
#include <QTextCursor>
#include <QTextDocumentFragment>
#include <QUrl>
#include <QMimeData>

TextEditWidget::TextEditWidget(QWidget *parent)
	: QTextEdit(parent),DragDropInterface(this)
{
	AddAcceptableDropTypes(ENTITY_DROP_TYPE_TEXT);			//add text type as aceptable
	AddAcceptableDropTypes(ENTITY_DROP_TYPE_LIST_ITEM);		//add item as acceptable->first col/row mustr contain string
	m_bFileDropEnabled=false;
	m_bDropSignal=false;
	
}

TextEditWidget::~TextEditWidget()
{

}


int TextEditWidget::GetDropType() const
{
	return ENTITY_DROP_TYPE_TEXT;
}


//if not edit mode: disable drop & drag
void TextEditWidget::dropEvent(QDropEvent *event)
{
	if (m_bDropSignal)
	{
		QTextEdit::dropEvent(event);
		emit DataDroped();
		return;
	}

	if(isReadOnly()){return;}

	if (m_bFileDropEnabled)
	{
		const QMimeData *mime = event->mimeData();
		if (!mime->hasUrls()) return;

		QList<QUrl> lst =mime->urls();

		if (lst.size()>0)
		{
			//QString strPath= QUrl::fromPercentEncoding(lst.at(0).toEncoded());

			if (lst.at(0).scheme().toUpper()==QString("http").toUpper())
			{
				setPlainText(lst.at(0).toString());
			}
			
		}
	}
	else
		DragDropInterface::dropEvent(event);


}
//if not edit mode: disable drop & drag
void TextEditWidget::dragEnterEvent ( QDragEnterEvent * event )
{
	if(isReadOnly())
		{event->ignore();return;}

	if (m_bFileDropEnabled)
	{
		if (event->mimeData()->hasUrls())
		{
			event->accept();
			return;
		}
	}
	else
		DragDropInterface::dragEnterEvent(event);

}



//return selected text
void TextEditWidget::GetDropValue(DbRecordSet &values, bool bSelectItemChildren ) const
{
	QString strSelected=textCursor().selection().toPlainText();
	values.addColumn(QVariant::String,"SELECTED_TEXT");
}


//accept string  items from list->first col/row is string
void TextEditWidget::DropHandler(QModelIndex index, int nDropType, DbRecordSet &DroppedValue,QDropEvent *event)
{
	if(DroppedValue.getRowCount()==0) return;

	//match cursor to position of mouse:
	setTextCursor(cursorForPosition(event->pos()));

	//find position from cursor inside text:
	int nDroPosition=textCursor().position();

	QString content= toPlainText();

	//insert dropped value:
	content.insert(nDroPosition,DroppedValue.getDataRef(0,0).toString());

	setPlainText(content);
}
