#ifndef ABOUTWIDGET_H
#define ABOUTWIDGET_H

#include <QtWidgets/QDialog>
#ifdef WINCE
	#include "embedded_core/embedded_core/generatedfiles/ui_aboutwidget.h"
#else
	#include "generatedfiles/ui_aboutwidget.h"
#endif

class AboutWidget : public QDialog
{
	Q_OBJECT

public:
	AboutWidget(QWidget *parent,QString strAppName, QString strModuleCode="", QString strUserName="", int nLicenseID=0, QString strReportLine1="", QString strReportLine2="");
	~AboutWidget();

private:
	Ui::AboutWidgetClass ui;
};

#endif // ABOUTWIDGET_H
