#ifndef PICTUREWIDGET_H
#define PICTUREWIDGET_H

#include <QLabel>
#include <QtWidgets/QFrame>
#include <QScrollArea>
#include <QVBoxLayout>
#include <QScrollBar>
#include <QContextMenuEvent>
#include <QAction>
#include <QAuthenticator>
#include "gui_core/gui_core/guidatamanipulator.h"


/*!
	\class  PictureWidget
	\ingroup GUICore_CustomWidgets
	\brief  Based on QLabel it provides interface for setting picture from external source


	Used to display picture, provides basic context menu.

*/


typedef bool (*FN_LOAD_FULLPICTURE)(int nPicID, DbRecordSet &rowPic);

class PictureWidget : public QFrame
{
	Q_OBJECT

public:
    PictureWidget(QWidget *parent=0);
    ~PictureWidget();


	enum MODE
	{
		MODE_PREVIEW,					//picture in frame: no buttons, no cntx menu
		MODE_PREVIEW_WITH_BUTTONS,		//picture in frame + Load/Paste/Open In new buttons & cntx menu
		MODE_FULL,						//picture in frame ,all buttons + Open In new win
		MODE_FULL_DIALOG,				//picture in frame ,all buttons (no open in new win)
	};

	void Initialize(int nMode=MODE_PREVIEW,GuiDataManipulator *pLstData=NULL, QString strPreviewColIdx="",QString strFullColIdx="",QString strPictureIdIdx="",QString strEntityIdIdx="");
	void Initialize(int nMode=MODE_PREVIEW,DbRecordSet *pLstData=NULL, QString strPreviewColIdx="",QString strFullColIdx="",QString strPictureIdIdx="",QString strEntityIdIdx="");
	void SetLoadFullPictureCallBack(FN_LOAD_FULLPICTURE pfCallBackLoadFullPicture){m_pfCallBackLoadFullPicture=pfCallBackLoadFullPicture;};
	
	void GetPictureInfo(int &width, int &height,int &nDepth,int &nPreviewSize,int &nFullSize);
	void GetPictureBinaryDataPreview(QByteArray &bytePicture);
	void GetPictureBinaryData(QByteArray &bytePicture);
	void SetPictureFromBinaryData(QByteArray &bytePicture);
	QString GetCurrentPictureFormat(){return m_strCurrentFormat;};
	QPixmap GetPixmap();
	void SetPixmap(QPixmap& pic);
	void ClearPixmap(){ErasePicture();}
	bool IsPictureChanged(){return m_bPictureChanged;} //only true if load / paste picture (flag not reseted)
	QLabel *GetLabel();
	void RefreshDisplay();
	void DisableNewWindow(){m_pNewWindow->setEnabled(false);}
	//Edit mode:
	void SetEditMode(bool bEdit=true);
	bool IsEditMode(){return m_bEdit;};

protected:
	void RefreshPictureState();
	void SetUpWidget();
	void CreateActions();
	void ChangePixMap(QPixmap *pixmap=NULL);
	void contextMenuEvent(QContextMenuEvent *event);

	void dropEvent(QDropEvent *event);
	void dragEnterEvent ( QDragEnterEvent * event );
	void dragMoveEvent(QDragMoveEvent *event );

public slots:
	void LoadPictureFromFile();
	void SavePictureToFile();
	void PastePictureFromClipboard();
	void CopyPictureToClipboard();
	void ErasePicture();
	void ShowPictureInDialog();

private slots:
	void print();
	void zoomIn();
	void zoomOut();
	void normalSize();
	void fitToWindow();
	bool ChangeDataSource(QPixmap *pixmap);
	void OnEmitAuthenticationRequired ( const QString & hostname, quint16 port, QAuthenticator *  auth);

signals:
	void PictureChanged();//emits signal when content is changed
    

private:
	void ChangePictureToolTip();
	void updateActions();
	void scaleImage(double factor);
	void adjustScrollBar(QScrollBar *scrollBar, double factor);
	bool GetLinkContent(QString strFilePath, QByteArray &content);

	FN_LOAD_FULLPICTURE m_pfCallBackLoadFullPicture;
	bool m_bEdit;
	int m_nMode;		//show mode
	bool m_bExternalDataManipulator;
	QLabel *m_pLabel;
	bool m_bRefreshInProgrres;

	QAction* m_pActLoad;
	QAction* m_pActSave;
	QAction* m_pActPrint;
	QAction* m_pActCopyToClipboard;
	QAction* m_pActPastFromClipboard;
	QAction* m_pActErase;
	QAction* m_pActZoomIn;
	QAction* m_pActZoomOut;
	QAction* m_pActNormal;
	QAction* m_pActFitWindow;
	QAction* m_SeparatorAct;
	QAction* m_pNewWindow;

	QVBoxLayout *m_pMainLayout;
	QScrollArea *m_scrollArea;

	double scaleFactor;

	QString m_strCurrentFormat;
	bool m_bPictureChanged;


	//data sources:
	GuiDataManipulator *m_pLstData; 
	int m_nPreviewColIdx;
	int m_nFullColIdx;
	QString m_strEntityRecordID;
	QString m_strPictureID;

};

#endif // PICTUREWIDGET_H



