#ifndef FREEDAYSREGISTER_H
#define FREEDAYSREGISTER_H

#include <QObject>
#include <QDate>
#include "common/common/dbrecordset.h"

class FreeDaysRegister : public QObject
{
	Q_OBJECT

public:
    FreeDaysRegister();
    ~FreeDaysRegister();

	bool GetFreeDaysForYear(int nYear, DbRecordSet &lstDays, QString strCountryISO = QString("ch"));
	bool GetFreeDays(QDate &from, QDate &to, DbRecordSet &lstDays, QString strCountryISO = QString("ch"));

protected:
	bool CalcEasterDate(int nYear, QDate &date);
	void DefineFreeDaysList(DbRecordSet &lstDays);

	bool GetFreeDaysForYear_ch(int nYear, DbRecordSet &lstDays);
};

#endif // FREEDAYSREGISTER_H
