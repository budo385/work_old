#include "dlg_enterperiod.h"

Dlg_EnterPeriod::Dlg_EnterPeriod(QWidget *parent)
    : QDialog(parent)
{
	ui.setupUi(this);
	setWindowTitle(tr("Enter Period"));

}

Dlg_EnterPeriod::~Dlg_EnterPeriod()
{

}

void Dlg_EnterPeriod::setPeriod(QDate from,QDate to)
{
	//if (!from.isValid()) from=QDate::currentDate();
	//if (!to.isValid()) to=QDate::currentDate();
	ui.dateTo->setDate(to);
	ui.dateFrom->setDate(from);
}

void Dlg_EnterPeriod::getPeriod(QDate &from,QDate &to)
{
	from=ui.dateFrom->getDate();
	to=ui.dateTo->getDate();
}

void Dlg_EnterPeriod::on_btnOK_clicked()
{
	/*
	if (ui.dateTo->getDate()<ui.dateFrom->getDate())
	{
		QMessageBox::warning(this,tr("Warning"),tr("Date range is invalid"));
		return;
	}
	*/


	accept();
}

void Dlg_EnterPeriod::on_btnCancel_clicked()
{
	reject();
}

