#include "universaltablecelldwidget.h"


TableCellWidget::TableCellWidget()
{
}

TableCellWidget::~TableCellWidget()
{
}

void TableCellWidget::SetData(int nRow, int nCol,QVariant &Data,QString strDataSourceFormat,bool bIsEditable)
{
	m_nRow=nRow;
	m_nCol=nCol;
	m_strDataSourceFormat=strDataSourceFormat;
	m_Data=Data;
	m_bEditable=bIsEditable;
}

