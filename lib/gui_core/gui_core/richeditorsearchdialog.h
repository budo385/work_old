#ifndef RICHEDITORSEARCHDIALOG_H
#define RICHEDITORSEARCHDIALOG_H

#include <QtWidgets/QDialog>
#include <QTextDocument>
#include "generatedfiles/ui_richeditorsearchdialog.h"

class RichEditorSearchDialog : public QDialog
{
	Q_OBJECT

public:
	RichEditorSearchDialog(QWidget *parent = 0);
	~RichEditorSearchDialog();

private slots:
	void on_close_pushButton_clicked();
	void on_findNext_pushButton_clicked();

private:
	Ui::RichEditorSearchDialogClass ui;

signals:
	void SearchText(QString strText, QTextDocument::FindFlags findFlags, bool bUseRegex);
};

#endif // RICHEDITORSEARCHDIALOG_H
