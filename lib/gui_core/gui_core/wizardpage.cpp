#include "wizardpage.h"
#include "wizardbase.h"
#include "bus_client/bus_client/mainentityselectioncontroller.h"
#include "common/common/entity_id_collection.h"

WizardPage::WizardPage(int nWizardPageID, QString strPageTitle, int nPageEntityID /*= -1*/, QWidget *parent /*= 0*/)
: QWidget(parent),m_pCache(NULL)
{
	m_bInitialized			= false;
	m_bComplete				= false;
	m_nNextPageCollection	= -1;
	m_nWizardPageID			= nWizardPageID;
	m_strPageTitle			= strPageTitle;
	m_nPageEntityID			= nPageEntityID;
	m_strFinishBtnTitle		= QString(tr("&Finish"));
	
	hide();
}

WizardPage::~WizardPage()
{
	WriteToCache();
}

void WizardPage::Initialize()
{
	//**************************************************************************//
	//			Copy this in any page that override Initialize() method.		//
	//**************************************************************************//
	//If already initialized return.
	if (m_bInitialized)
		return;

	//Else read from cache.
	ReadFromCache();
	m_bInitialized = true;
	//**************************************************************************//
	//**************************************************************************//
}

void WizardPage::resetPage()
{
}

bool WizardPage::isComplete()
{
	return m_bComplete;
}

int	 WizardPage::GetNextPageCollectionID()
{
	return m_nNextPageCollection;
}

QList<WizardPage*> WizardPage::GetNextPageCollectionVector()
{
	QList<WizardPage*> emptyList;
	return emptyList;
}

void WizardPage::SetNextPageCollectionID(int nNextPageCollection)
{
	m_nNextPageCollection = nNextPageCollection;
}

void WizardPage::WriteToCache()
{
	if (m_bInitialized && m_pCache)
	{
		m_pCache->PurgeCache(m_nWizardPageID);
		m_pCache->SetCache(m_nWizardPageID, m_recResultRecordSet);
	}
}

void WizardPage::ReadFromCache()
{
	//g_ClientCache.GetCache(m_nWizardPageID, m_recResultRecordSet, CacheExpired);
	if (m_pCache)
	{
		DbRecordSet *dataCache = m_pCache->GetCache(m_nWizardPageID);
		if (dataCache)
			m_recResultRecordSet = *dataCache;
		else //B.T. fixed on 29.04.2014, what if cache is empty
		{
			if (m_nWizardPageID==WIZARD_PROJECT_STORED_LIST_SELECTION_PAGE)
			{
				MainEntitySelectionController cacheProj; //special case for stored proj list
				cacheProj.Initialize(ENTITY_BUS_PROJECT);
				cacheProj.ReloadData(true);
				m_recResultRecordSet=*cacheProj.GetDataSource();
				if (m_recResultRecordSet.getColumnCount()==0)
				{
					cacheProj.SetForceFullProjectDataReload(true); //fetch all from server
					cacheProj.ReloadData(true);
					m_recResultRecordSet=*cacheProj.GetDataSource();
				}
			}
			else
			{
				if (m_nPageEntityID!=-1)
				{
					MainEntitySelectionController cacheX; //for everything else
					cacheX.Initialize(m_nPageEntityID);
					cacheX.ReloadData(true);
					m_recResultRecordSet=*cacheX.GetDataSource();
				}
			}
		}
	}

	//m_recResultRecordSet.Dump();

}

bool WizardPage::SetPageResult(DbRecordSet &RecordSet)
{
	m_recResultRecordSet = RecordSet;
	return true;
}

bool WizardPage::GetPageResult(DbRecordSet &RecordSet)
{
	RecordSet = m_recResultRecordSet;
	return true;
}

QString WizardPage::GetPageTitle()
{
	return m_strPageTitle;
}

QString	WizardPage::GetPageFinishButttonTitle()
{
	return m_strFinishBtnTitle;
}

void WizardPage::SetParent(QWidget *parent)
{
	m_pParent = parent;
}
