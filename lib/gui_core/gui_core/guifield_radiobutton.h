#ifndef GUIFIELD_RADIOBUTTON_H
#define GUIFIELD_RADIOBUTTON_H

#include "guifield.h"
#include <QRadioButton>
#include <QVariant>
#include <QHash>




/*!
	\class GuiField_RadioButton
	\brief Defines support for Radio Buttons
	\ingroup GUICore_GUIFields

	Connects to data source. Values are given in list, in same order as list of widgets of radio buttons.
	See constructor
*/
class GuiField_RadioButton : public GuiField
{
	typedef QHash<QRadioButton*,QVariant> RadioButtonList;
	typedef QHashIterator<QRadioButton*,QVariant> RadioButtonListIterator;
	typedef QHash<QRadioButton*,QVariant>::const_iterator RadioButtonListConstIterator;


	Q_OBJECT


public:
	
	GuiField_RadioButton(QString strFieldName,QList<QRadioButton*> lstButtons,QList<QVariant> lstValues,DbRecordSet* pData,DbView *TableView=NULL);

	void RefreshDisplay(); //DATA SOURCE -> UI

private:
	RadioButtonList m_lstButtons;

	void setEnabled(bool bEnable);

public slots:
	void FieldChanged(); //UI -> DATA SOURCE

};

#endif // GUIFIELD_CHECKBOX_H


