#include "guifield_checkbox.h"



GuiField_CheckBox::GuiField_CheckBox(QString strFieldName,QWidget *pWidget,GuiDataManipulator* pDataManager,DbView *TableView, QLabel *pLabel)
:GuiField(strFieldName,pWidget,pDataManager,TableView,pLabel)
{
	//test column type in datasource:
	Q_ASSERT(pDataManager->GetDataSource()->getColumnType(pDataManager->GetDataSource()->getColumnIdx(strFieldName))==QVariant::Int );


	m_pChkBoxWidget=dynamic_cast<QCheckBox*>(pWidget);
	Q_ASSERT(m_pChkBoxWidget!=NULL);
	connect(m_pChkBoxWidget,SIGNAL( stateChanged(int)),this,SLOT(FieldChanged()));

}

/*!
	If edit line widget, then this slot is invoked when text changes
	NOTE: assert is fired if row is out of bound
*/
void GuiField_CheckBox::FieldChanged()
{
	QVariant value = m_pChkBoxWidget->checkState()==Qt::Checked ? 1:0;
	//if(ValidateUserInput(value))
		m_pDataManager->ChangeData(m_nCurrentRow,m_nIndex,value);
}



/*!
	If data is changed from data manipulator, refresh content
	If data source is empty, content will be cleared.
*/
void GuiField_CheckBox::RefreshDisplay()
{
	m_pChkBoxWidget->blockSignals(true);
	//check if current row is out of bound, if so clear content;
	if(m_nCurrentRow<m_pDataManager->GetDataSource()->getRowCount()) 
	{
		int value=m_pDataManager->GetDataSource()->getDataRef(m_nCurrentRow,m_nIndex).toInt();
		m_pChkBoxWidget->setCheckState(value==0 ? Qt::Unchecked : Qt::Checked);
	}
	else
		m_pChkBoxWidget->setCheckState(Qt::Unchecked);
	
	m_pChkBoxWidget->blockSignals(false);

}

