#include "selectedcontactpicture.h"

#include <QVBoxLayout>
#include "thememanager.h"

SelectedContactPicture::SelectedContactPicture()
{
	m_SlideSize = QSize(60,80);

	QImage img;
	img.load(":Icon_Person128.png");
	QImage image(img.alphaChannel());
	img.setAlphaChannel(image);
	img.scaledToHeight(m_SlideSize.height());

	m_pPictureLabel = new QLabel;
	//m_pPictureLabel->setMaximumHeight(m_SlideSize.height());
	//m_pPictureLabel->setPixmap(QPixmap::fromImage(img));
	QVBoxLayout *vbox1=new QVBoxLayout;
	vbox1->setSpacing(0);
	vbox1->setMargin(0);
	vbox1->addSpacing(5);
	vbox1->addWidget(m_pPictureLabel);

	QHBoxLayout *hbox1=new QHBoxLayout;
	hbox1->setSpacing(0);
	hbox1->setMargin(0);
	hbox1->addStretch(1);
	hbox1->addLayout(vbox1);
	hbox1->addStretch(1);
	
	setLayout(hbox1);
	setStyleSheet(".QWidget "+ThemeManager::GetMobileSolidBkg_Dark());
	}

SelectedContactPicture::~SelectedContactPicture()
{

}

void SelectedContactPicture::SetPixmap(QPixmap &pixmap)
{
	pixmap = pixmap.scaledToHeight(m_SlideSize.height());
	m_pPictureLabel->setPixmap(pixmap);
}

void SelectedContactPicture::SetSlideSize(QSize &size)
{
	m_SlideSize = size;
	//m_pPictureLabel->setMaximumHeight(m_SlideSize.height());
}
