#include "doublespinboxadv.h"



QString DoubleSpinBoxAdv::textFromValue(double v) const
{
	return QVariant(v).toString();
}


double DoubleSpinBoxAdv::valueFromText(const QString &text) const
{
	return QVariant(text).toDouble();
}

		
QValidator::State DoubleSpinBoxAdv::validate(QString &text, int &pos) const
{
	//if conversion can be done, return state=accept
	bool ok;
	double x = QVariant(text).toDouble(&ok);
	if(ok)
		return QValidator::Acceptable;
	else
		return QValidator::Invalid;
}


