#include "listwidget.h"
#include "common/common/entity_id_collection.h"
#include <QContextMenuEvent>
#include <QMenu>
#include "common/common/datahelper.h"
#include "trans/trans/xmlutil.h"
#include "os_specific/os_specific/mime_types.h"
#include <QUrl>
#include <QMimeData>

ListWidget::ListWidget(QWidget *parent)
	: QListWidget(parent)//,DragDropInterface(this)
{
	connect(this,SIGNAL(itemSelectionChanged()),this,SLOT(SelectionChanged()));

	m_pActDelete = new QAction(tr("&Delete Selection"), this);
	m_pActDelete->setShortcut(tr("Del"));
	connect(m_pActDelete, SIGNAL(triggered()), this, SLOT(DeleteSelection()));
	this->addAction(m_pActDelete);

	m_lstFilesDropped.addColumn(QVariant::String,"PATH");
	m_lstFilesDropped.addColumn(QVariant::String,"NAME");
	m_lstFilesDropped.addColumn(QVariant::String,"EXT");

	m_bFileDropEnabled=false;
	this->setAcceptDrops(true);
	viewport ()->setAcceptDrops(true);
}

ListWidget::~ListWidget()
{


}
int ListWidget::GetDropType() const
{
	return ENTITY_DROP_TYPE_LIST_ITEM;
}

//Adds first column
//check boxes reflect selection state-> if selected checkbox activated
void ListWidget::SetData(DbRecordSet &data, int nColumnToDisplay, bool bShowCheckBoxes)
{
	blockSignals(true);
	m_lstData=data;
	clear();
	int nSize = m_lstData.getRowCount();
	for(int i=0;i<nSize;++i)
	{
			addItem(m_lstData.getDataRef(i,nColumnToDisplay).toString());
			if(bShowCheckBoxes)
			{
				QListWidgetItem *item=QListWidget::item(i);
				item->setFlags( Qt::ItemIsUserCheckable | Qt::ItemIsSelectable | Qt::ItemIsEnabled); 
				item->setCheckState(Qt::Unchecked);
			}
	}

	m_lstData.clearSelection();
	blockSignals(false);

}

void ListWidget::ClearData()
{
	blockSignals(true);
	m_lstData.clear();
	clear();
	blockSignals(false);
}




void ListWidget::GetDropValue(DbRecordSet &values, bool bSelectItemChildren ) const
{ 
	values.copyDefinition(m_lstData);
	values.merge(m_lstData,true);

}



void ListWidget::SelectionChanged()
{

	//copy selection from items to list:
	int nSize =m_lstData.getRowCount();
	for(int nRow=0;nRow<nSize;++nRow)
		m_lstData.selectRow(nRow,isItemSelected(item(nRow)));

	emit SignalSelectionChanged();
}




//if not edit mode: disable drop & drag
void ListWidget::dropEvent(QDropEvent *event)
{
	//QListWidget::dropEvent(event);

	if (m_bFileDropEnabled)
	{
		const QMimeData *mime = event->mimeData();
		
		//------------------------------------------------
		// LIST of files:
		//------------------------------------------------
		if (mime->hasUrls()) 
		{
			QList<QUrl> lst =mime->urls();

			//list of paths (files), list of application paths (exe)
			m_lstFilesDropped.clear();
			for (int i=0;i<lst.size();++i)
			{
				if (lst.at(i).scheme().toUpper()==QString("file").toUpper())
				{	QString strExt,strName;
				QString strFilePath=DataHelper::ResolveFilePath(lst.at(i).toLocalFile(),strName,strExt);
				if (strExt!="exe") 
				{
					m_lstFilesDropped.addRow();
					int nLastRow=m_lstFilesDropped.getRowCount()-1;
					m_lstFilesDropped.setData(nLastRow,0,strFilePath);
					m_lstFilesDropped.setData(nLastRow,1,strName);
					m_lstFilesDropped.setData(nLastRow,2,strExt);
				}
				}
			}
			if (m_lstFilesDropped.getRowCount()>0)
			{
				emit SignalFilesDropped();
			}

			event->accept();
			return;
		}

		//------------------------------------------------
		// LIST of documents entities:
		//------------------------------------------------

		//internal/external
		if (mime->hasFormat(SOKRATES_MIME_LIST))
		{
			int nEntityID;
			DbRecordSet lstDropped;

			//import emails:
			QByteArray arData = mime->data(SOKRATES_MIME_LIST);
			if(arData.size() > 0)
			{
				lstDropped=XmlUtil::ConvertByteArray2RecordSet_Fast(arData);
				//lstDropped.Dump();
			}
			QByteArray arDataType = mime->data(SOKRATES_MIME_DROP_TYPE);
			if(arDataType.size() > 0)
			{
				bool bOK;
				nEntityID=arDataType.toInt(&bOK);
				if (!bOK) nEntityID=-1;
			}


			if (nEntityID==ENTITY_COMM_GRID_DATA && lstDropped.getRowCount()>0)
			{
				DbRecordSet lstDocs;
				int nRow=lstDropped.find(0,QVariant(ENTITY_BUS_DM_DOCUMENTS).toInt(),true);
				if (nRow!=-1)
				{
					lstDocs=lstDropped.getDataRef(nRow,1).value<DbRecordSet>();
					if (lstDocs.getRowCount()>0)
					{
						emit SignalEntityDropped(ENTITY_BUS_DM_DOCUMENTS,lstDocs);
					}
				}

				event->accept();
				return;
			}
		}
	 }

	event->ignore();
	return;
}


Qt::DropActions ListWidget::supportedDropActions() const 
{
	return  Qt::CopyAction | Qt::MoveAction | Qt::IgnoreAction | Qt::LinkAction | Qt::ActionMask | Qt::TargetMoveAction;
}


void ListWidget::dragMoveEvent ( QDragMoveEvent * event )
{
	if (m_bFileDropEnabled)
	{
		//if (event->mimeData()->hasUrls())
		//{
			event->accept();
			return;
		//}
	}
}

//if not edit mode: disable drop & drag
void ListWidget::dragEnterEvent ( QDragEnterEvent * event )
{
	
	if (m_bFileDropEnabled)
	{
		//if (event->mimeData()->hasUrls())
		//{
			event->accept();
			return;
		//}
	}

	QListWidget::dragEnterEvent(event);
}




//accept string  items from list->first col/row is string
void ListWidget::DropHandler(QModelIndex index, int nDropType, DbRecordSet &DroppedValue,QDropEvent *event)
{

	Q_ASSERT("Not implemented");

}





/*!
Slot for opening context menu:
*/
void ListWidget::contextMenuEvent(QContextMenuEvent *event)
{
	//event:
	if(m_lstActions.size()==0) 
	{
		event->ignore();	
		return;
	}

	QMenu CnxtMenu(this);

	int nSize=m_lstActions.size();
	for(int i=0;i<nSize;++i)
		CnxtMenu.addAction(m_lstActions.at(i));

	CnxtMenu.exec(event->globalPos());
}



void ListWidget::DeleteSelection()
{
	int nSize=count();
	for (int i=0;i<nSize;++i)
	{
		QListWidgetItem *pItem=item(i);
		if (pItem)
		{
			if (pItem->isSelected())
			{
				delete pItem;
				m_lstData.deleteRow(i);
			}
		}
	}
	emit SignalSelectionDeleted();
}

void ListWidget::GetData(DbRecordSet &data)
{
	data=m_lstData;
}

void ListWidget::GetDroppedFiles(DbRecordSet &data)
{
	data=m_lstFilesDropped;
}


void ListWidget::SetCheckBoxSelection(QList<bool> lstChecked)
{
	Q_ASSERT(lstChecked.count()==m_lstData.getRowCount());

	int nSize=lstChecked.count();
	for(int i=0;i<nSize;i++)
	{
		if (lstChecked.at(i))
			item(i)->setCheckState(Qt::Checked);
		else
			item(i)->setCheckState(Qt::Unchecked);
	}

}
void ListWidget::GetCheckBoxSelection(QList<bool> &lstChecked)
{
	lstChecked.clear();
	int nSize=m_lstData.getRowCount();
	for(int i=0;i<nSize;i++)
	{
		lstChecked.append(item(i)->checkState()==Qt::Checked);
	}
}
