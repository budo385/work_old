#ifndef DATEEDITWIDGET_H
#define DATEEDITWIDGET_H

#include <QLineEdit>
#include <QDate>
#include <QVariant>
#include <QtWidgets/QMessageBox>
#include <QDebug>

/*!
	\class DateEditWidget
	\brief Based on QLineEdit, enables more flexible date input
	\ingroup GUICore_CustomWidgets

	DateEditWidget uses simple LinEdit + btnEdit(datePicker).
	Add widget to form and promote to this!

*/
class DateEditWidget : public QWidget
{
	Q_OBJECT

public:
    DateEditWidget(QWidget *parent);
    ~DateEditWidget();

	QDate getDate();
	void setDate(QDate &date);

	QLineEdit* m_lineEdit;

private slots:
	void ValidateDate();
	void OnDateEdit();

signals:
	void EmitDateChanged(QDate);
};

#endif // DATEEDITWIDGET_H
