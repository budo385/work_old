#include "propertybrowser.h"

PropertyBrowser::PropertyBrowser(QWidget *parent)
    : QtTreePropertyBrowser(parent)
{
	m_pVariantEditorFacatory	= new QtVariantEditorFactory(this);
	m_pVariantManager = new QtVariantPropertyManager();
	setFactoryForManager(m_pVariantManager, m_pVariantEditorFacatory);
	setHeaderVisible(true);
	setRootIsDecorated(false);
}

PropertyBrowser::~PropertyBrowser()
{
	
}

void PropertyBrowser::Initialize(DbRecordSet *RecordSet, QList<int> MapList)
{
	m_pRecordSet = RecordSet;
	m_MapList = MapList;

	//Set default mapping values.
	m_nRowID				= 0;
	m_nNameColumn			= 5;
	m_nTypeColumn			= 6;
	m_nValueColumn			= 7;
	m_nStrValueColumn		= 8;

	if (!m_MapList.isEmpty())
	{
		m_nRowID			= m_MapList.value(0);
		m_nNameColumn		= m_MapList.value(1);
		m_nTypeColumn		= m_MapList.value(2);
		m_nValueColumn		= m_MapList.value(3);
		m_nStrValueColumn	= m_MapList.value(4);
	}
	
	//If first time called then just clear hash, else delete items too.
	if (!m_hshRowIDToProperty.count())
		m_hshRowIDToProperty.clear();
	else
	{
		QList<QtVariantProperty*> PropertyList(m_hshRowIDToProperty.values());
		QListIterator<QtVariantProperty*> iter(PropertyList);
		while (iter.hasNext())
			removeProperty(iter.next());

		m_hshRowIDToProperty.clear();
	}
	
	CreateProperties();
	resizeFirstColumnToContents();
}

void PropertyBrowser::propertyChanged(QtProperty *property)
{
	//Cast it now for ease of use.
	QtVariantProperty *Property = static_cast<QtVariantProperty*>(property);
	//Get property attributes.
	int nRowID = m_hshRowIDToProperty.key(Property);
	
	int nTypeOfValue = Property->propertyType();
	
	QVariant PropertyValue = QVariant();
	QString  PropertyStrValue = QString();

	//If not child then it can be int or bool.
	if (nTypeOfValue == QVariant::Int || nTypeOfValue == QVariant::Bool)
		PropertyValue = Property->value();
	//Or it can be single line string.
	else
		PropertyStrValue = Property->value().toString();

	//Emit changed row ID.
	QtTreePropertyBrowser::propertyChanged(property);
	emit ValueChanged(nRowID, PropertyValue, PropertyStrValue);
}

void PropertyBrowser::CreateProperties()
{
	int RowCount = m_pRecordSet->getRowCount();
	for (int i = 0; i < RowCount; ++i)
	{
		DbRecordSet rowRecordSet = m_pRecordSet->getRow(i);
		CreateProperty(&rowRecordSet);
	}
}

void PropertyBrowser::CreateProperty(DbRecordSet *RecordSet)
{
	QtVariantProperty *Item;
	int		 RowID				= RecordSet->getDataRef(0, m_nRowID).toInt();
	QString	 PropertyName		= RecordSet->getDataRef(0, m_nNameColumn).toString();
	int		 PropertyType		= RecordSet->getDataRef(0, m_nTypeColumn).toInt();
	QVariant PropertyValue		= RecordSet->getDataRef(0, m_nValueColumn);
	QString  PropertyStrValue	= RecordSet->getDataRef(0, m_nStrValueColumn).toString();
	//Integer value.
	if(PropertyType == 0)
	{
		Item = m_pVariantManager->addProperty(QVariant::Int, PropertyName);
		Item->setValue(PropertyValue);
	}
	//Bool value.
	else if (PropertyType == 1)
	{
		Item = m_pVariantManager->addProperty(QVariant::Bool, PropertyName);
		Item->setValue(PropertyValue);
	}
	//String values.
	else
	{
		Item = m_pVariantManager->addProperty(QVariant::String, PropertyName);
		Item->setValue(PropertyStrValue);
	}

	//Insert to hash for later finding and add to browser.
	m_hshRowIDToProperty.insert(RowID, Item);
	addProperty(Item);
}
