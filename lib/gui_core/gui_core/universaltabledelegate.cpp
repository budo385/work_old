#include "universaltabledelegate.h"

static QString strToDisplay;


//----------------------------------------------------------------------
//						EDIT
//----------------------------------------------------------------------



/*!
	Override from QItemDelegate. Only provides custom editor for doubles, all other are default
*/
/*
QWidget *UniversalTableDelegate::createEditor(QWidget *parent,const QStyleOptionViewItem &style,const QModelIndex &index) const
{

	if(index.model()->data(index, Qt::DisplayRole).type()==QVariant::Double)
	{
		DoubleSpinBoxAdv *doubleEditor = new DoubleSpinBoxAdv(parent);
		doubleEditor->setMinimum(0);
		doubleEditor->setMaximum(1000000);
		doubleEditor->setDecimals(2);
		doubleEditor->installEventFilter(const_cast<UniversalTableDelegate*>(this));
		return doubleEditor;
	}
	else
		return QItemDelegate::createEditor(parent,style,index);

}
*/
/*!
	Override from QItemDelegate. Only provides custom editor for doubles, all other are default
*/
/*
void UniversalTableDelegate::setEditorData(QWidget *editor,const QModelIndex &index) const
{
	if(index.model()->data(index, Qt::DisplayRole).type()==QVariant::Double)
	{
		double value = index.model()->data(index, Qt::DisplayRole).toDouble();
		DoubleSpinBoxAdv *doubleEditor = static_cast<DoubleSpinBoxAdv*>(editor);
		doubleEditor->setValue(value);
	}
	else
		QItemDelegate::setEditorData(editor,index);
}
*/
/*!
	Override from QItemDelegate. Only provides custom editor for doubles, all other are default
*/
/*
void UniversalTableDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
	if(index.model()->data(index, Qt::DisplayRole).type()==QVariant::Double)
	{
		double value;
		DoubleSpinBoxAdv *doubleEditor = static_cast<DoubleSpinBoxAdv*>(editor);
		value=doubleEditor->value();
		model->setData(index, value);
	}
	else
		QItemDelegate::setModelData(editor,model,index);
}
*/

/*!
	Override from QItemDelegate. Sets editor geometry
*/
/*
void UniversalTableDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
	
    editor->setGeometry(option.rect);
	
}
*/












//----------------------------------------------------------------------
//						DISPLAY
//----------------------------------------------------------------------



/* 
	This override is necessary to show Date, DateTime and Time in desired format : Local settings
*/

/*
void UniversalTableDelegate::paint ( QPainter * painter, const QStyleOptionViewItem & option, const QModelIndex & index ) const
{

	//convert date field: as tablewidget always uses American date for display
	switch (index.data(Qt::DisplayRole).type()) //TOFIX for other date formats
	{
	case QVariant::Date: 
		strToDisplay=index.data(Qt::DisplayRole).toDate().toString("d.M.yyyy");
		break;

	case QVariant::DateTime:
		strToDisplay=index.data(Qt::DisplayRole).toDateTime().toString("d.M.yyyy hh:mm");
		break;

	case QVariant::Time:
		strToDisplay=index.data(Qt::DisplayRole).toTime().toString("hh:mm");
		break;

	default:
		strToDisplay="";
	}

	QItemDelegate::paint(painter, option, index );
}






void UniversalTableDelegate::drawDisplay ( QPainter * painter, const QStyleOptionViewItem & option, const QRect & rect, const QString & text ) const
{
	if (strToDisplay.isEmpty())
		QItemDelegate::drawDisplay (  painter, option, rect, text );
	else
		QItemDelegate::drawDisplay (  painter, option, rect, strToDisplay );
	
}
*/

/*
void UniversalTableDelegate::drawFocus ( QPainter * painter, const QStyleOptionViewItem & option, const QRect & rect ) const
{


}
*/