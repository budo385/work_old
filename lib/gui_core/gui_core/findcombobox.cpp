#include "findcombobox.h"

#include <QApplication>

FindComboBox::FindComboBox(QWidget *parent)
    : QComboBox(parent)
{
	setSizeAdjustPolicy(QComboBox::AdjustToMinimumContentsLength);
}

FindComboBox::~FindComboBox()
{
	m_pTreeWidget = NULL;
}

/*!
	Initialize find combo box with pointer to Universal tree widget.

	\param pTreeWidget		  - Pointer to Universal tree widget.
*/
void FindComboBox::Initialize(UniversalTreeWidget *pTreeWidget)
{
	m_pTreeWidget = pTreeWidget;

	//Set layout.
	setEditable(true);
	setCompleter(NULL);

	//Connect signals.
	connect(this, SIGNAL(editTextChanged(const QString&)), this, SLOT(on_ComboTextChanged(const QString&)));
	connect(pTreeWidget, SIGNAL(RebuildingTree()), this, SLOT(OnRebuildingTree()));
}

void FindComboBox::focusOutEvent(QFocusEvent* event)
{
	//Don't act on popup and some user focus out reasons.
	if (event->reason() != Qt::PopupFocusReason &&
		event->reason() != Qt::OtherFocusReason)
	{
		m_pTreeWidget->setFocus(Qt::MouseFocusReason);
		m_pTreeWidget->SelectionChanged();
	}

	QComboBox::focusOutEvent(event);
}

void FindComboBox::keyPressEvent(QKeyEvent* event)
{
	//If return or enter(numeric) pressed then send focus to tree and notify that selection is changed (load data).
	if(event->key() == Qt::Key_Return || event->key() == Qt::Key_Enter)
	{
		m_pTreeWidget->setFocus(Qt::MouseFocusReason);
		m_pTreeWidget->SelectionChanged();
		emit EnterPressed();
	}
	//If key down, show combo popup.
	else if (event->key() == Qt::Key_Down)//|| event->key() == Qt::Key_PageDown || event->key() == Qt::Key_PageUp)
	{
		blockSignals(true);
		showPopup();
		blockSignals(false);
		return;
	}
	//If key PageDown.
	else if (event->key() == Qt::Key_PageDown && !m_lstFoundItems.isEmpty())
	{
		//If we are on first or last found item do nothing.
		if ((m_nCurrentFoundItemNum + 1) >= m_lstFoundItems.count())
			return;

		m_pTreeWidget->blockSignals(true);
		
		m_pTreeWidget->clearSelection();
		m_nCurrentFoundItemNum++;
		m_lstFoundItems.value(m_nCurrentFoundItemNum)->setSelected(true);
		//Expand.
		m_pTreeWidget->setCurrentItem(m_lstFoundItems.value(m_nCurrentFoundItemNum));
		m_pTreeWidget->FixScrolling();
		//m_pTreeWidget->scrollToItem(m_lstFoundItems.value(m_nCurrentFoundItemNum));
		
		m_pTreeWidget->blockSignals(false);
		return;
	}
	//If key PageUp.
	else if (event->key() == Qt::Key_PageUp && !m_lstFoundItems.isEmpty())
	{
		//If we are on first or last found item do nothing.
		if (!m_nCurrentFoundItemNum)
			return;
		
		m_pTreeWidget->blockSignals(true);

		m_pTreeWidget->clearSelection();
		m_nCurrentFoundItemNum--;
		m_lstFoundItems.value(m_nCurrentFoundItemNum)->setSelected(true);
		//Expand.
		m_pTreeWidget->setCurrentItem(m_lstFoundItems.value(m_nCurrentFoundItemNum));
		m_pTreeWidget->FixScrolling();
		//m_pTreeWidget->scrollToItem(m_lstFoundItems.value(m_nCurrentFoundItemNum));
		
		m_pTreeWidget->blockSignals(false);
		return;
	}
	//If escape.
	else if (event->key() == Qt::Key_Escape)//|| event->key() == Qt::Key_PageDown || event->key() == Qt::Key_PageUp)
		m_pTreeWidget->setFocus(Qt::MouseFocusReason);

	QComboBox::keyPressEvent(event);
}

void FindComboBox::on_ComboTextChanged(const QString &text)
{
	//Fist block signals.
	this->blockSignals(true);
	m_pTreeWidget->blockSignals(true);
	m_pTreeWidget->clearSelection();
	//Clear combo and items list.
	clear();
	int i = 0;

	//B.T. improvajd: if entered text is subset of previous search text then use old found items-> can be lot faster..
	if(text.indexOf(m_strLastFindString)==0 && !m_strLastFindString.isEmpty())
	{
		QList<QTreeWidgetItem*> lstNew;
		int nSize=m_lstFoundItems.size();
		for(int k=0;k<nSize;k++)
		{
			if (m_lstFoundItems.at(k)->text(0).indexOf(text,0,Qt::CaseInsensitive)>=0)
			{
				i++;
				lstNew.append(m_lstFoundItems.at(k));
				if (i < 30) 		//Insert only first 30 found items in popup (for speed).
				{
					QString itemText = m_lstFoundItems.at(k)->data(0,Qt::DisplayRole).toString();
					int itemID		 = m_lstFoundItems.at(k)->data(0,Qt::UserRole).toInt();
					insertItem(i, itemText, itemID);
				}
			}
		}
			m_strLastFindString=text;
			m_lstFoundItems=lstNew;

	}
	else
	{
		m_strLastFindString=text;
		m_lstFoundItems.clear();

		QListIterator<QTreeWidgetItem*> iter(m_pTreeWidget->findItems(text, Qt::MatchContains | Qt::MatchRecursive));
		while (iter.hasNext())
		{
			i++;
			QTreeWidgetItem *item = iter.next();

			//Append items to found list.
			m_lstFoundItems.append(item);

			//Insert only first 30 found items in popup (for speed).
			if (i < 30)
			{
				QString itemText = item->data(0, Qt::DisplayRole).toString();
				int itemID		 = item->data(0, Qt::UserRole).toInt();
				insertItem(i, itemText, itemID);
			}
		}

	}
	//Set find combo background.
	if (i == 0)
	{
		setEditText(text);
		setStyleSheet("background: lightGray");
		QApplication::beep();
		this->blockSignals(false);
		m_pTreeWidget->blockSignals(false);
		return;
	}
	else
		setStyleSheet("background: white");

	//Select and expand.
	m_nCurrentFoundItemNum = 0;
	m_lstFoundItems.first()->setSelected(true);
	m_pTreeWidget->setCurrentItem(m_lstFoundItems.first());
	m_pTreeWidget->FixScrolling();
	//m_pTreeWidget->scrollToItem(m_lstFoundItems.first());
	
	//Set edit text and unblock signals.
	setEditText(text);
	m_pTreeWidget->blockSignals(false);
	this->blockSignals(false);
}


void FindComboBox::OnRebuildingTree()
{
	m_strLastFindString.clear();
	m_lstFoundItems.clear();

}