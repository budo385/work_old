#include "universaltablelist.h"

UniversalTableList::UniversalTableList(QWidget *parent)
	: QListWidget(parent)
{
	//setEditable(false);
	setSelectionMode(QAbstractItemView::ExtendedSelection);
	connect(this,SIGNAL(itemSelectionChanged()),this,SLOT(SelectionChanged()));
	setEnabled(true);
}

UniversalTableList::~UniversalTableList()
{

}


/*!
	Called by UniversalTableWidget when widget is created
	\param value	- sets data from UniversalTableWidget datasource, 
					  data is extracted from value using m_strDataSource
					  value must be DbRecordSet
*/
void UniversalTableList::SetData(int nRow, int nCol,QVariant &Data,QString strDataSourceFormat,bool bIsEditable)
{

	//sets data:
	TableCellWidget::SetData(nRow,nCol,Data,strDataSourceFormat,bIsEditable);

	DbRecordSet lstData = Data.value<DbRecordSet>();
	
	//clear combo content:
	disconnect(this,SIGNAL(itemSelectionChanged()),this,SLOT(SelectionChanged()));
	clear();

	//get list :format: "COLUMN_1<<'Separator'<<COLUMN_2<<COLUMN_3 ... where Separator is custom defined text printed as is
	QStringList lstDataSource=strDataSourceFormat.split("<<");
	Q_ASSERT_X(lstDataSource.size()!=0,"WidgetChild","Data Source not defined for combo box");
	int nSize =lstData.getRowCount();
	QString strItem;

	//add item into combo:
	for(int nRow=0;nRow<nSize;++nRow)
	{
		strItem="";
		for(int i=0;i<lstDataSource.size();++i)
		{
			if (lstDataSource.at(i).indexOf("'")!=-1) //if found replace '
			{
				QString strSeparator=lstDataSource.at(i);
				if (!strItem.isEmpty()) //add separator only if not empty
					strItem+=strSeparator.replace("'","");
			}
			else
			{
				strItem+=lstData.getDataRef(nRow,lstDataSource.at(i)).toString();
			}
		}
		insertItem (nRow,strItem);
		setItemSelected(item(nRow),lstData.isRowSelected(nRow)); //set selection
	}

	//set disabled:
	//if(!bIsEditable)setEnabled(false);

	connect(this,SIGNAL(itemSelectionChanged()),this,SLOT(SelectionChanged()));

}


/*!
	Called by UniversalTableWidget when widget already exist and data needs to be refreshed
	In combo box: selection is copied and set as current 
	\param value	- returns data set by SetData() (DbRecordSet), any user changes are stored in value
*/
void UniversalTableList::RefreshData(QVariant &Data,bool bCopyContent)
{
	disconnect(this,SIGNAL(itemSelectionChanged()),this,SLOT(SelectionChanged()));



	m_Data=Data; //assume that are same: only selection changed
	DbRecordSet lstData = Data.value<DbRecordSet>(); //extract

	if(bCopyContent)
	{
		clear();

		//get list :format: "COLUMN_1<<'Separator'<<COLUMN_2<<COLUMN_3 ... where Separator is custom defined text printed as is
		QStringList lstDataSource=m_strDataSourceFormat.split("<<");
		Q_ASSERT_X(lstDataSource.size()!=0,"WidgetChild","Data Source not defined for combo box");
		int nSize =lstData.getRowCount();
		QString strItem;

	//	lstData.Dump();

		//add item into combo:
		for(int nRow=0;nRow<nSize;++nRow)
		{
			strItem="";
			for(int i=0;i<lstDataSource.size();++i)
			{
				if (lstDataSource.at(i).indexOf("'")!=-1) //if found replace '
				{
					QString strSeparator=lstDataSource.at(i);
					if (!strItem.isEmpty()) //add separator only if not empty
						strItem+=strSeparator.replace("'","");
				}
				else
				{
					//qDebug()<<lstDataSource.at(i);
					strItem+=lstData.getDataRef(nRow,lstDataSource.at(i)).toString();
				}
			}
			insertItem (nRow,strItem);
			setItemSelected(item(nRow),lstData.isRowSelected(nRow)); //set selection
		}


	}

	//select all:
	int nSize =lstData.getRowCount();
	for(int nRow=0;nRow<nSize;++nRow)
		setItemSelected(item(nRow),lstData.isRowSelected(nRow));


	connect(this,SIGNAL(itemSelectionChanged()),this,SLOT(SelectionChanged()));
}


/*!
	Called by UniversalTableWidget for disabling/enabling user interaction with child widget
	\param bEdit	- true, control is clickable, false, control is disabled
*/
void UniversalTableList::SetEditMode(bool bEdit)
{
	/*
	if(!bEdit)
		setFocusPolicy(Qt::NoFocus);
	else
		setFocusPolicy(Qt::StrongFocus);
	*/
	//if(m_bEditable)setEnabled(bEdit);
}



void UniversalTableList::SelectionChanged()
{
	DbRecordSet lstData = m_Data.value<DbRecordSet>(); //extract

	//copy selection from items to list:
	int nSize =lstData.getRowCount();
	for(int nRow=0;nRow<nSize;++nRow)
		lstData.selectRow(nRow,isItemSelected(item(nRow)));

	qVariantSetValue(m_Data, lstData);		//store back

	DataChanged();	//emit signal that data has changed
}

