#ifndef DBCONNSETTINGSGUI_H
#define DBCONNSETTINGSGUI_H

#include <QDialog>
#include "./generatedfiles/ui_dbconnsettingsgui.h"
#include "db_core/db_core/dbconnsettings.h"

class DbConnSettingsGUI : public QDialog
{
    Q_OBJECT

public:
    DbConnSettingsGUI(QWidget *parent = 0, Qt::WindowFlags flags = 0);
    ~DbConnSettingsGUI();

	void SetConnections(QList<DbConnectionSettings> &lstData, int nCurrentIdx = -1);
	QList<DbConnectionSettings> &GetConnections(){ return m_lstData; };
	int GetCurSelection() const { return m_nCurSel; }

	void SetPassword(bool bUsePassword, QString strPass);
	void GetPassword(bool &bUsePassword, QString &strPass);
	
protected:	
	void ClearInfo();
	void UpdateInfo(bool bToScreen = true);
	void EnableFields(bool bEnable = true);

	QString GetHashString(QString strPassword);

protected:
	QList<DbConnectionSettings> m_lstData;
	int m_nCurSel;

	bool m_bUsePassword;
	QString m_strHash;

private:
    Ui::DbConnSettingsGUIClass ui;
	//QString TestFeatures(QSqlDatabase *database);
	void SetDriver(const QString DbName);
	QString GetDriver();

private slots:
	void on_listWidget_currentRowChanged(int);
	void on_btnChangePassword_clicked();
	void on_chkUseUserPassword_stateChanged(int);
	void on_cancelButton_clicked();
	void on_delConnButton_clicked();
	void on_saveButton_clicked();
	void on_newConnButton_clicked();
	void on_testButton_clicked();
};

#endif // DBCONNSETTINGSGUI_H
