#include "picturegraphitem.h"

#include <QPainter>
#include <QMenu>
#include <QGraphicsSceneContextMenuEvent>
#include "math.h"

#include "mobilehelper.h"

#include "pictureflowwidget.h"

PictureGraphItem::PictureGraphItem(QImage &imgPicture, int nItemIndex, QTimeLine *pMoveTimeLine, int nPicFlowType /*= 0*/, QWidget *parent /*= 0*/, QString strItemText /*= QString()*/)
{
	m_imgItemImage = imgPicture;
	m_nItemIndex = nItemIndex;
	m_imgMirrorItemImage = MobileHelper::CreateImageMirror(m_imgItemImage);
	m_strItemText = strItemText;
	m_strDisplayItemText = strItemText;

	m_pParent = parent;
	m_nOffSet = 0;
	m_nRotationAngle = 0;
	m_nCurrentPosition = 0;
	m_nNextPosition = 0;
	m_pntCurrentPos.setX(0);
	m_pntCurrentPos.setY(0);
	m_nCurrentRotationAngle = 0;
	m_ScaleFactor = 0.09;
	m_nPicFlowType = nPicFlowType;

	m_pMoveTimeLine = pMoveTimeLine;
	m_nDuration = m_pMoveTimeLine->duration();
	m_pMoveTimeLine->setUpdateInterval(5);
	connect(m_pMoveTimeLine, SIGNAL(frameChanged(int)), this, SLOT(on_frameChanged(int)));
	connect(m_pMoveTimeLine, SIGNAL(stateChanged(QTimeLine::State)), this, SLOT(on_stateChanged(QTimeLine::State)));

	setPos(0,0);

	setAcceptHoverEvents(true);
}

PictureGraphItem::~PictureGraphItem()
{

}

void PictureGraphItem::setItemPosition(int nPosition)
{
	m_nNextPosition = nPosition;

	//If the position is the same return.
	if (m_nCurrentPosition == m_nNextPosition)
		return;

	m_nOffSet = 0;
	m_nRotationAngle = 0;
	if (m_nNextPosition<0)
		m_nRotationAngle = -38;
	else if (m_nNextPosition>0)
		m_nRotationAngle = 45;

	//Are we going left or right.
	if (m_nNextPosition < m_nCurrentPosition)
	{
		//Calculate offset.
		for (int i = m_nCurrentPosition; i > m_nNextPosition; i--)
		{
			//If we go over 0 add bigger offset.
			if ((i-1) == 0)
			{
				if (m_nPicFlowType == PictureFlowWidget::MAIN_MENU_PICFLOW)
					m_nOffSet+= -150;
				else
					m_nOffSet+= -75;
			}
			else if (i == 0)
			{
				//m_nOffSet+= -150;
				if (m_nPicFlowType == PictureFlowWidget::MAIN_MENU_PICFLOW)
					m_nOffSet+= -180 + m_imgItemImage.width()/2;
				else
					m_nOffSet+= -90 + m_imgItemImage.width()/2;
			}
			else
				m_nOffSet+= -25;
		}
	}
	else if (m_nNextPosition > m_nCurrentPosition)
	{
		//Calculate offset.
		for (int i = m_nCurrentPosition; i < m_nNextPosition; i++)
		{
			//If we go over 0 add bigger offset.
			if ((i+1) == 0)
			{
				//m_nOffSet+=150;
				if (m_nPicFlowType == PictureFlowWidget::MAIN_MENU_PICFLOW)
					m_nOffSet+=180 - m_imgItemImage.width()/2;
				else
					m_nOffSet+=90 - m_imgItemImage.width()/2;
			}
			else if (i == 0)
			{
				if (m_nPicFlowType == PictureFlowWidget::MAIN_MENU_PICFLOW)
					m_nOffSet+=150;
				else
					m_nOffSet+=75;
			}
			else
				m_nOffSet+=25;
		}
	}

	m_pntNextPos.setX(m_pntCurrentPos.x() + m_nOffSet);
	m_pntNextPos.setY(m_pntCurrentPos.y());

	setZValue(-abs(nPosition));
}

void PictureGraphItem::on_frameChanged(int i)
{
	if (m_nCurrentRotationAngle != m_nRotationAngle)
	{
		if ((m_nCurrentRotationAngle < 0 && m_nRotationAngle == 0) || (m_nCurrentRotationAngle < 0 && m_nRotationAngle < 0) 
			|| (m_nRotationAngle < 0 && m_nCurrentRotationAngle == 0))
		{	
			setPos(m_pntCurrentPos.x() + m_nOffSet*i/m_nDuration, m_pntCurrentPos.y());
			QTransform trans;
			if (!m_nRotationAngle)
			{
				trans.rotate(m_nCurrentRotationAngle + abs(m_nCurrentRotationAngle)*i/m_nDuration, Qt::YAxis);
				setTransform(trans);
			}
			else
			{
				trans.rotate(m_nRotationAngle*i/m_nDuration, Qt::YAxis);
				setTransform(trans);
			}

			return;
		}
		else if (m_nRotationAngle > 0 && m_nCurrentRotationAngle == 0)
		{
			setPos(m_pntCurrentPos.x() + m_nOffSet*i/m_nDuration, m_pntCurrentPos.y());
			QTransform trans;
			trans.rotate(m_nRotationAngle*i/m_nDuration, Qt::YAxis);
			trans.scale(1, 1-m_ScaleFactor*i/m_nDuration);
			setTransform(trans);
			return;
		}
		else if (m_nCurrentRotationAngle > 0 && m_nRotationAngle == 0)
		{
			setPos(m_pntCurrentPos.x() + m_nOffSet*i/m_nDuration, m_pntCurrentPos.y());
			QTransform trans;
			trans.rotate(m_nCurrentRotationAngle - m_nCurrentRotationAngle*i/m_nDuration, Qt::YAxis);
			trans.scale(1, 1-m_ScaleFactor+m_ScaleFactor*i/m_nDuration);
			setTransform(trans);
			return;
		}
		else if (m_nRotationAngle > 0 && m_nCurrentRotationAngle < 0)
		{
			setPos(m_pntCurrentPos.x() + m_nOffSet*i/m_nDuration, m_pntCurrentPos.y());
			QTransform trans;
			trans.rotate((m_nRotationAngle/*+abs(m_nCurrentRotationAngle)*/)*i/m_nDuration, Qt::YAxis);
			trans.scale(1, 1-m_ScaleFactor*i/m_nDuration);
			setTransform(trans);
			return;
		}
		else if (m_nRotationAngle < 0 && m_nCurrentRotationAngle > 0)
		{
			setPos(m_pntCurrentPos.x() + m_nOffSet*i/m_nDuration, m_pntCurrentPos.y());
			QTransform trans;
			trans.rotate((m_nRotationAngle/*-m_nCurrentRotationAngle*/)*i/m_nDuration, Qt::YAxis);
			trans.scale(1, 1-m_ScaleFactor+m_ScaleFactor*i/m_nDuration);
			setTransform(trans);
			return;
		}
	}

	setPos(m_pntCurrentPos.x() + m_nOffSet*i/m_nDuration, m_pntCurrentPos.y());
}	

int PictureGraphItem::GetCurrentItemPosition()
{
	return m_nCurrentPosition;
}

QRectF PictureGraphItem::boundingRect() const
{
	return QRect(0,0,m_imgItemImage.width(), m_imgItemImage.height() + m_imgMirrorItemImage.height());
}

QString PictureGraphItem::GetItemText()
{
	return m_strItemText;
}

void PictureGraphItem::SetItemDisplayText(QString strItemText)
{
	m_strDisplayItemText = strItemText;
}

void PictureGraphItem::ResetOriginalDisplayText()
{
	m_strDisplayItemText = m_strItemText;
}

void PictureGraphItem::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{
	//Show context menu only if central item.
	if (m_nCurrentPosition == 0)
	{
		QMenu ContextMenu;

		QList<QAction*> lstActions;
		dynamic_cast<PictureFlowWidget*>(m_pParent)->GetContextMenuActions(lstActions);
		int nSize= lstActions.size();
		for(int i=0;i<nSize;++i)
			ContextMenu.addAction(lstActions.at(i));

		ContextMenu.exec(event->screenPos());
	}
}

void PictureGraphItem::hoverEnterEvent(QGraphicsSceneHoverEvent *event)
{
	emit hoverEnterItem(this);
}

void PictureGraphItem::hoverLeaveEvent(QGraphicsSceneHoverEvent *event)
{
	emit hoverLeaveItem(this);
}

void PictureGraphItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
	int hh = m_imgMirrorItemImage.height();
	painter->drawImage(0, 0, m_imgItemImage);
	painter->drawImage(0, m_imgItemImage.height(), m_imgMirrorItemImage);

	if ((m_nCurrentPosition == 0 && m_nNextPosition == 0) && (!m_strDisplayItemText.isEmpty()))
	{
		QStringList strList = m_strDisplayItemText.split(";");
		QRectF rect = boundingRect();
		QRectF rectum(rect.x()-50,rect.bottom()+5, rect.width()+100, 100);
		QFont font = painter->font();
		font.setBold(true);
		font.setPointSize(12);
		QPen pen;
		pen.setColor(Qt::white);
		painter->setPen(pen);
		painter->setFont(font);
		QRectF rect1;
		painter->drawText(rectum, Qt::AlignHCenter | Qt::AlignTop | Qt::TextDontClip | Qt::TextWordWrap, strList.at(0), &rect1);
		font.setBold(false);
		font.setPointSize(8);
		painter->setFont(font);
		QRectF rectum1(rect.x()-50,rect.bottom()+rect1.height()+5, rect.width()+100, 100);
		painter->drawText(rectum1, Qt::AlignHCenter | Qt::AlignTop | Qt::TextDontClip | Qt::TextWordWrap, strList.at(1));
	}

}

void PictureGraphItem::on_stateChanged(QTimeLine::State state)
{
	if (state != QTimeLine::Running)
	{
		m_pntCurrentPos = pos();
		m_nCurrentRotationAngle = m_nRotationAngle;
		m_nCurrentPosition = m_nNextPosition;
		
		//If center item emit signal.
		if (m_nCurrentPosition == 0)
			emit centerIndexChanged(m_nItemIndex, this);
	}
}
