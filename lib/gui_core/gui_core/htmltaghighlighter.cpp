#include "htmltaghighlighter.h"

HtmlTagHighlighter::HtmlTagHighlighter(QObject *parent)
	: QSyntaxHighlighter(parent)
{

}

HtmlTagHighlighter::~HtmlTagHighlighter()
{

}

void HtmlTagHighlighter::highlightBlock(const QString &text)
{
	QTextCharFormat myClassFormat;
	myClassFormat.setFontWeight(QFont::Bold);
	myClassFormat.setForeground(Qt::darkMagenta);
	QString pattern = "<([A-Z][A-Z0-9]*)\b[^>]*>(.*?)</\1>";

	QRegExp expression(pattern);
	int index = text.indexOf(expression);
	while (index >= 0) {
		int length = expression.matchedLength();
		setFormat(index, length, myClassFormat);
		index = text.indexOf(expression, index + length);
	}
}
