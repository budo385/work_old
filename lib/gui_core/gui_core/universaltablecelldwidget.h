#ifndef TABLEWIDGET_H
#define TABLEWIDGET_H

#include <QObject>
#include <QComboBox>


/*!
	\class  SignalEmiter
	\ingroup GUICore_UniversalTable
	\brief  Signal emiter class is helper class for TableCellWidget, used by widget to notify when data/selection change

	This class is separated because it uses QObject, which is also base class for QWidget, this
	can lead to problems

*/
class SignalEmiter : public QObject
{
	Q_OBJECT

public:	//this is wrapper to hide signal mechanism (hide QObject) from child widget
	void DataChanged(int nRow,int nCol){emit CellWidgetDataChange(nRow,nCol);};
	void CellClicked(int nRow,int nCol){emit CellWidgetClicked(nRow,nCol);};
signals:
	void CellWidgetDataChange(int nRow,int nCol);
	void CellWidgetClicked(int nRow,int nCol); //for selection change for special widgets
};




/*!
	\class  TableCellWidget
	\ingroup GUICore
	\brief  Abstract class provides unique interface for all possible widgets that can be displayed in the UniversalTableWidget

	Use:
	- subclass own widget from this, reimplement all methods
	- register widget at UniversalTableWidget::ColumnType enum, for now we support tohse widgets:
		COL_TYPE_TEXT=0,
		COL_TYPE_CHECKBOX=1,
		COL_TYPE_COMBOBOX=2,
		COL_TYPE_LIST=3,
		COL_TYPE_MULTILINE_TEXT=4,
		COL_TYPE_PICTURE=5,

*/
class TableCellWidget 
{

public:
	TableCellWidget();
	~TableCellWidget();

	QObject* GetSignalEmiter(){return &m_Signal;};
	void DataChanged(){m_Signal.DataChanged(m_nRow,m_nCol);};
	void CellClicked(){m_Signal.CellClicked(m_nRow,m_nCol);};
	
	
	virtual void SetData(int nRow, int nCol,QVariant &Data,QString strDataSourceFormat=QString(),bool bIsEditable=true); //called by UniversalTableWidget when widget is created
	virtual void RefreshData(QVariant &Data,bool bCopyContent=false)=0;								//called by UniversalTableWidget in refresh method when widget data is only refreshed (data -> screen)
	virtual void GetData(QVariant &Data){Data=m_Data;};						//called by UniversalTableWidget in user edit mode (user action -> data), after ChildWidgetDataChange signal is received
	virtual void SetEditMode(bool bEdit=true)=0;							//called by UniversalTableWidget for disabling/enabling user interaction with child widget
	

protected:
	int m_nRow;
	int m_nCol;
	QString m_strDataSourceFormat;
	QVariant m_Data;
	SignalEmiter m_Signal;
	bool m_bEditable;
 
};









#endif // TABLEWIDGET_H


