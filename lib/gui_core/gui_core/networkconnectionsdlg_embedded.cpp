#include "networkconnectionsdlg_embedded.h"
#include "common/common/sha256hash.h"
#include <QInputDialog>
#include <QtWidgets/QMessageBox>
#include "biginputdialog.h"

NetworkConnectionsDlg_Embedded::NetworkConnectionsDlg_Embedded(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
	//	setAttribute(Qt::WA_DeleteOnClose, true);

	//TOFIX fill all supported proxy types->BT fixed:
	ui.cboProxyType->addItem(tr("Socks5 Proxy"),QNetworkProxy::Socks5Proxy);
	ui.cboProxyType->addItem(tr("Http  Proxy"),QNetworkProxy::HttpProxy);

	m_nCurSel = -1;
	ClearInfo();
	EnableInfoFields(false);

	connect(ui.editConnName, SIGNAL(editingFinished()), this, SLOT(OnNameChanged()));

	m_bUsePassword = false;
}

NetworkConnectionsDlg_Embedded::~NetworkConnectionsDlg_Embedded()
{

}

int NetworkConnectionsDlg_Embedded::exec()
{
	int nRes = QDialog::exec();

	//handling the things after modal dialog has closed
	UpdateInfo(false);

	return nRes;
}

void NetworkConnectionsDlg_Embedded::SetConnections(QList<HTTPClientConnectionSettings> &lstData, int nCurrentIdx)
{
	//copy data
	m_lstData = lstData;

	//refresh list
	ui.lstConnections->clear();
	int nCount = m_lstData.size();
	for(int i=0; i<nCount; i++)
		ui.lstConnections->addItem(m_lstData[i].m_strCustomName);

	if(nCurrentIdx >=0)
		ui.lstConnections->setCurrentRow(nCurrentIdx);
}

void NetworkConnectionsDlg_Embedded::ClearInfo()
{
	ui.editConnName->setText("");
	ui.editHostName->setText("");
	ui.editPort->setText("");
	ui.editLookupURL->setText("");
	ui.editDatabaseID->setText("");
	ui.chkUseSSL->setChecked(false);
	ui.chkUseCompression->setChecked(false);
	ui.chkUseDFO->setChecked(false);

	ui.chkUseProxy->setChecked(false);
	ui.editProxyHost->setText("");
	ui.editProxyPort->setText("");
	ui.editProxyUser->setText("");
	ui.editProxyPass->setText("");
	ui.cboProxyType->setCurrentIndex(0);

	EnableProxyFields(false);
}

void NetworkConnectionsDlg_Embedded::UpdateInfo(bool bToScreen)
{
	if(m_nCurSel < 0)
		return;		//no selection

	if(bToScreen)
	{
		ui.editConnName->setText(m_lstData[m_nCurSel].m_strCustomName);
		ui.editHostName->setText(m_lstData[m_nCurSel].m_strServerIP);
		ui.editPort->setText(QString().sprintf("%d",m_lstData[m_nCurSel].m_nPort));	
		ui.editLookupURL->setText(m_lstData[m_nCurSel].m_strLookupURL);
		ui.editDatabaseID->setText(QString().sprintf("%d",m_lstData[m_nCurSel].m_nDatabaseID));
		ui.chkUseSSL->setChecked(m_lstData[m_nCurSel].m_bUseSSL);
		ui.chkUseCompression->setChecked(m_lstData[m_nCurSel].m_bUseCompression);
		ui.chkUseDFO->setChecked(m_lstData[m_nCurSel].m_bUseDFO);

		ui.chkUseProxy->setChecked(m_lstData[m_nCurSel].m_bUseProxy);
		ui.editProxyHost->setText(m_lstData[m_nCurSel].m_strProxyHostName);
		ui.editProxyPort->setText(QString().sprintf("%d",m_lstData[m_nCurSel].m_nProxyPort));	
		ui.editProxyUser->setText(m_lstData[m_nCurSel].m_strProxyUsername);
		ui.editProxyPass->setText(m_lstData[m_nCurSel].m_strProxyPassWord);

		int nIndex=ui.cboProxyType->findData(m_lstData[m_nCurSel].m_nProxyType); //BT fix 18.02.2008
		ui.cboProxyType->setCurrentIndex(nIndex);

		EnableProxyFields(m_lstData[m_nCurSel].m_bUseProxy);
	}
	else
	{
		m_lstData[m_nCurSel].m_strCustomName = ui.editConnName->text();
		m_lstData[m_nCurSel].m_strServerIP   = ui.editHostName->text();
		m_lstData[m_nCurSel].m_nPort		 = ui.editPort->text().toInt();
		m_lstData[m_nCurSel].m_strLookupURL	 = ui.editLookupURL->text();
		m_lstData[m_nCurSel].m_nDatabaseID = ui.editDatabaseID->text().toInt();

		m_lstData[m_nCurSel].m_bUseSSL		 = ui.chkUseSSL->isChecked();
		m_lstData[m_nCurSel].m_bUseCompression = ui.chkUseCompression->isChecked();
		m_lstData[m_nCurSel].m_bUseDFO		 = ui.chkUseDFO->isChecked();

		m_lstData[m_nCurSel].m_bUseProxy	 = ui.chkUseProxy->isChecked();
		m_lstData[m_nCurSel].m_strProxyHostName	= ui.editProxyHost->text();
		m_lstData[m_nCurSel].m_nProxyPort	 = 	ui.editProxyPort->text().toInt();	
		m_lstData[m_nCurSel].m_strProxyUsername = ui.editProxyUser->text();
		m_lstData[m_nCurSel].m_strProxyPassWord = ui.editProxyPass->text();
		m_lstData[m_nCurSel].m_nProxyType =ui.cboProxyType->itemData(ui.cboProxyType->currentIndex()).toInt(); //BT fix 18.02.2008
	}
}

void NetworkConnectionsDlg_Embedded::on_btnCancel_clicked()
{
	done(0);
}

void NetworkConnectionsDlg_Embedded::on_btnNew_clicked()
{
	//add new connection item
	HTTPClientConnectionSettings item;
	item.m_strCustomName = tr("New Connection");
	m_lstData.append(item);

	//refresh list
	ui.lstConnections->addItem(item.m_strCustomName);
	ui.lstConnections->setCurrentRow(ui.lstConnections->count()-1);

	m_nCurSel = ui.lstConnections->currentRow();
}

void NetworkConnectionsDlg_Embedded::on_btnDelete_clicked()
{
	m_nCurSel = ui.lstConnections->currentRow();

	if(m_nCurSel >= 0)
	{
		int nIdx = m_nCurSel;
		m_nCurSel = -1;

		m_lstData.removeAt(nIdx);
		ui.lstConnections->blockSignals(true);
		QListWidgetItem *item = ui.lstConnections->takeItem(nIdx);
		delete item;
		ui.lstConnections->blockSignals(false);

		int nNew = ui.lstConnections->currentRow();
		on_lstConnections_currentRowChanged(nNew);
	}
}

void NetworkConnectionsDlg_Embedded::on_btnSave_clicked()
{
	UpdateInfo(false);
	done(1);
}

void NetworkConnectionsDlg_Embedded::on_lstConnections_currentRowChanged(int nIdx)
{
	UpdateInfo(false);
	m_nCurSel = nIdx;
	UpdateInfo(true);
	EnableInfoFields(m_nCurSel >= 0);
}

void NetworkConnectionsDlg_Embedded::on_chkUseProxy_stateChanged(int nChecked)
{
	EnableProxyFields(nChecked);
}

void NetworkConnectionsDlg_Embedded::EnableProxyFields(bool bEnable)
{
	ui.editProxyHost->setEnabled(bEnable);
	ui.editProxyPort->setEnabled(bEnable);
	ui.editProxyUser->setEnabled(bEnable);
	ui.editProxyPass->setEnabled(bEnable);
	ui.cboProxyType->setEnabled(bEnable);
}

void NetworkConnectionsDlg_Embedded::EnableInfoFields(bool bEnable)
{
	ui.editConnName	->setEnabled(bEnable);
	ui.editHostName	->setEnabled(bEnable);
	ui.editPort		->setEnabled(bEnable);
	ui.editLookupURL->setEnabled(bEnable);
	ui.editDatabaseID->setEnabled(bEnable);
	ui.chkUseSSL	->setEnabled(bEnable);
	ui.chkUseCompression->setEnabled(bEnable);
	ui.chkUseDFO	->setEnabled(bEnable);
	ui.chkUseProxy	->setEnabled(bEnable);

	if(bEnable){
		EnableProxyFields(m_lstData[m_nCurSel].m_bUseProxy);
		on_chkUseDFO_stateChanged(m_lstData[m_nCurSel].m_bUseDFO);
	}
	else
		EnableProxyFields(bEnable);		
}

void NetworkConnectionsDlg_Embedded::on_btnChangePassword_clicked()
{
	QString strTitle(tr("User Settings Password"));
	QString strLabel(tr("New Password:"));

	biginputdialog dlg(strTitle, strLabel, NULL, "");
	dlg.SetPasswordMode();
	int nRes = dlg.exec();
	if(nRes > 0)
	{
		QString text = dlg.m_strInput;
		if(text.isEmpty()){
			m_strHash = "";
			ui.chkUseUserPassword->setChecked(false);
			QMessageBox::information(this, tr("Error"), tr("Password must not be empty!"), tr("&Ok"));
			return;
		}

		m_strHash = GetHashString(text);
	}
	else{
		if(m_strHash.isEmpty())
			ui.chkUseUserPassword->setChecked(false);
	}
}

void NetworkConnectionsDlg_Embedded::on_chkUseUserPassword_stateChanged(int nChecked)
{
	m_bUsePassword = nChecked;

	if(m_bUsePassword)
		ui.btnChangePassword->setEnabled(true);
	else
		ui.btnChangePassword->setEnabled(false);
}

void NetworkConnectionsDlg_Embedded::SetPassword(bool bUsePassword, QString strHash)
{
	m_bUsePassword = bUsePassword;
	m_strHash = strHash;

	ui.chkUseUserPassword->setChecked(m_bUsePassword);
}

void NetworkConnectionsDlg_Embedded::GetPassword(bool &bUsePassword, QString &strHash)
{
	bUsePassword = m_bUsePassword;
	strHash = m_strHash;
}

void NetworkConnectionsDlg_Embedded::on_chkUseDFO_stateChanged(int nChecked)
{
	if(nChecked)
		ui.editLookupURL->setEnabled(true);
	else
		ui.editLookupURL->setEnabled(false);
}

QString NetworkConnectionsDlg_Embedded::GetHashString(QString strPassword)
{
	QByteArray data;
	data.append(strPassword);

	//calculate hash value
	QByteArray hash;
	Sha256Hash Hasher;
	hash = Hasher.GetHash(data);

	//convert it into the string (binary to Base64)
	QString strResult;
	strResult = hash.toBase64();
	return strResult;
}

void NetworkConnectionsDlg_Embedded::OnNameChanged()
{
	//update listview item name
	int nRow = ui.lstConnections->currentRow();
	if(nRow >= 0)
		ui.lstConnections->item(nRow)->setText(ui.editConnName->text());
}
