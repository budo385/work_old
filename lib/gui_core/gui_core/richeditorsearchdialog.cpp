#include "richeditorsearchdialog.h"

RichEditorSearchDialog::RichEditorSearchDialog(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
	setWindowTitle(tr("Find Text"));
}

RichEditorSearchDialog::~RichEditorSearchDialog()
{

}

void RichEditorSearchDialog::on_close_pushButton_clicked()
{
	done(QDialog::Rejected);
}

void RichEditorSearchDialog::on_findNext_pushButton_clicked()
{
	QTextDocument::FindFlags flags = 0;
	if(ui.findBackward_checkBox->isChecked())
		flags = QTextDocument::FindBackward;
	if (ui.findCaseSensitively_checkBox->isChecked())
		flags = flags | QTextDocument::FindCaseSensitively;
	if (ui.findWholeWord_checkBox->isChecked())
		flags = flags | QTextDocument::FindWholeWords;
	
	bool bUseRegex;
	if (ui.useRegex_checkBox->isChecked())
		bUseRegex = true;
	else
		bUseRegex = false;
	
	emit SearchText(ui.lineEdit->text(), flags, bUseRegex);
}
