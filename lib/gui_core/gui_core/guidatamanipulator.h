#ifndef GUIDATAMANIPULATOR_H
#define GUIDATAMANIPULATOR_H

#include "common/common/dbrecordset.h"



/*!
	\class  GuiDataManipulator
	\ingroup GUICore_GUIFields
	\brief  Abstract class providing standard interface for GUI widgets for manipulating data through user interaction

	Use:
	- init with datasource
	- register it with widget, all user action will be reflected on the data source if widget calls InsertData, DeleteData and ther
	- reimplement all methods as you wish, base implementation only provides basic operations
	For example:
	- InsertRow() can be used to fill in default values for given data entites (e.g. default name for contact, etc..)
	- ChangeData() can be used to validate user input (returning false means that new values is not accepted)

*/
class GuiDataManipulator 
{

public:
	
	GuiDataManipulator(const GuiDataManipulator &that);
	void operator =(const GuiDataManipulator &that);

	GuiDataManipulator(DbRecordSet *lstData);
	virtual ~GuiDataManipulator(){};

	//Data change API:
	virtual void InsertData(int nRowInsertAt=-1,int nRowFrom=-1);	
	virtual void DeleteData(int nRow,bool bSelected=false);	
	virtual bool ChangeData(int nRow,int nCol, const QVariant &value);
	virtual bool ChangeData(int nRow,QString strColumnName, const QVariant &value); //overloaded for col name
	void SetDirtyFlagOnAllRows();
	void SetDirtyFlag(int nRow);

	virtual DbRecordSet* GetDataSource(){return m_lstData;};

	//Tracker:
	void EnableDataChangeTracking(QString strPrimaryKey="");
	void ClearState();
	void LoadFromCache();
	void GetDeletedData(DbRecordSet &lstDeleted);
	void GetWriteData(DbRecordSet &lstWrite);
	bool IsChanged();
	bool IsRowEdited(int nRow);
	bool IsRowInserted(int nRow);




	// when data manipulator changes data it can trigger refresh() signals (see  below).
	// NOTE: those signals are not triggered by Insert,Delete,Change data, you must manually call it, when changing data 
	// any GUI widget using this manipulator must respond and refresh content on screen
//signals:
//	void RefreshDisplay();
//	void RefreshDisplayRow(int nRow);
//	void RefreshSelection();


protected:
	DbRecordSet *m_lstData;
	int m_nPrimaryKeyColumnIdx;

	//track data chagnes:
	bool bTrackChanges;
	DbRecordSet DataCache;			//<copy of Data after READ or sucess WRITE
	DbRecordSet DataDeleted;		//<PK column, ID's of deleted
	DbRecordSet DataChanged;		//<PK column, ID's of edited

    
};

#endif // GUIDATAMANIPULATOR_H


