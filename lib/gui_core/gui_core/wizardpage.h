#ifndef WIZARDPAGE_H
#define WIZARDPAGE_H

#include <QWidget>
#include "common/common/cliententitycache.h"
#include "common/common/dbrecordset.h"

class WizardPage : public QWidget
{
	friend class WizardBase;
	Q_OBJECT

public:
    WizardPage(int nWizardPageID, QString strPageTitle, int nPageEntityID = -1, QWidget *parent = 0);
    ~WizardPage();

	virtual void	Initialize();
	virtual void	resetPage();
	virtual bool	isComplete();
	virtual int		GetNextPageCollectionID();
	virtual QList<WizardPage*> GetNextPageCollectionVector();
	virtual void	SetNextPageCollectionID(int nNextPageCollection);
	virtual void	WriteToCache();
	virtual void	ReadFromCache();
	virtual void	WriteToResultRecordSet(){};
	virtual bool	SetPageResult(DbRecordSet &RecordSet);
	virtual bool	GetPageResult(DbRecordSet &RecordSet);
	QString			GetPageTitle();
	virtual QString	GetPageFinishButttonTitle();
	virtual void	on_NextButtonClicked(){};
	void			SetParent(QWidget *parent);
	void			SetGlobalCache(ClientEntityCache *pCache){m_pCache=pCache;};
	bool			IsInitialized(){ return m_bInitialized; };
	void			SetFinishBtnTitle(QString strTitle){ m_strFinishBtnTitle = strTitle; };

protected:
	QWidget			*m_pParent;				//< Pointer to gui_core/wizardbase.h (for calling ClearPageHashFromThisPageUp(int) method from page).
	bool			m_bInitialized;			//< Page initialized bool (clicked on next) bool.
	bool			m_bComplete;			//< Page completed (ready for next) bool.
	int				m_nWizardID;			//< Wizard ID - from where was it called.
	int				m_nWizardPageID;		//< Page ID - defined in bus_core/entity_id_collection.h (for cache capable pages).
	int				m_nPageEntityID;		//< Page entity ID - for simple selection wizard page.
	DbRecordSet		m_recResultRecordSet;	//<	Pages cache recordset.
	int				m_nNextPageCollection;	//< Next page collection ID - for complex wizard implementation.
	QString			m_strPageTitle;			//< Wizard page title.
	ClientEntityCache *m_pCache;			//< Pointer to cache
	QString			m_strFinishBtnTitle;

signals:
	void			completeStateChanged();
	void			goNextPage();			//<can be used to jumo to next page
};

#endif // WIZARDPAGE_H
