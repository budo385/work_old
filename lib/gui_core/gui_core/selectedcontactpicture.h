#ifndef SELECTEDCONTACTPICTURE_H
#define SELECTEDCONTACTPICTURE_H

#include <QWidget>
#include <QLabel>
class SelectedContactPicture : public QWidget
{
	Q_OBJECT

public:
	SelectedContactPicture();
	~SelectedContactPicture();

	void SetPixmap(QPixmap &pixmap);
	void SetSlideSize(QSize &size);

private:
	QLabel *m_pPictureLabel;
	QSize	m_SlideSize;
};

#endif // SELECTEDCONTACTPICTURE_H
