#include "scrollablemsgbox.h"
#include <QtWidgets/QMessageBox>

ScrollableMsgBox::ScrollableMsgBox(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
	
	m_nFirstButtonReply = QMessageBox::AcceptRole;
	m_nSecondButtonReply = QMessageBox::RejectRole;
}

ScrollableMsgBox::~ScrollableMsgBox()
{
}

void ScrollableMsgBox::HideButton(int nBtnIdx)
{
	if(0 == nBtnIdx)
		ui.btnFirst->hide();
	else
		ui.btnSecond->hide();
}

void ScrollableMsgBox::SetButtonInfo(int nBtnIdx, QString strTxt, int nResult)
{
	if(0 == nBtnIdx){
		ui.btnFirst->setText(strTxt);
		m_nFirstButtonReply = nResult;
	}
	else{
		ui.btnSecond->setText(strTxt);
		m_nSecondButtonReply = nResult;
	}
}
	
void ScrollableMsgBox::SetMessage(QString strMsg)
{
	ui.txtMessage->setPlainText(strMsg);
}

void ScrollableMsgBox::on_btnFirst_clicked()
{
	done(m_nFirstButtonReply);
}
	
void ScrollableMsgBox::on_btnSecond_clicked()
{
	done(m_nSecondButtonReply);
}
