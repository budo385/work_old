#include "dragdropinterface.h"
#include "trans/trans/xmlutil.h"
#include "os_specific/os_specific/mime_types.h"
#include <QMimeData>


/*!
	When drop occurs, checks if drop is acceptable

	\param   nDropType - drop type for check
*/
bool DragDropInterface::CheckIfDropIsAcceptable(int nDropType)
{
	int c = m_lstDropTypes.count();

	if(m_lstDropTypes.indexOf(nDropType)==-1) 
		return false;
	else 
		return true;
}


/*!
	AbstractItemView override
	
*/
Qt::DropActions DragDropInterface::supportedDropActions() const
{
	return  Qt::CopyAction | Qt::MoveAction | Qt::IgnoreAction; 
}


/*!
	AbstractItemView override, checks if drop can be accepted while user drags items across widget.
	If true	accept, else block it!
	
	\param   event - QDropEvent
*/
void DragDropInterface::dragEnterEvent(QDragEnterEvent * event )
{
	
	//check to see if this is our widget (DRAG/DROP) class:
	DragDropInterface *source = dynamic_cast<DragDropInterface*>(event->source());
	if(source==NULL) 
	{
		event->accept();
		return;
	}


	//check if we support this drop:
	if(CheckIfDropIsAcceptable(source->GetDropType()))
		event->accept();
	else
		event->ignore();
	
}


/*!
	AbstractItemView override, checks if drop can be accepted if true
	then decrypt coordiantes and call DropHandler with dropped data
	
	\param   event - QDropEvent
*/
void DragDropInterface::dropEvent(QDropEvent *event)
{
	//quick patch:
	QModelIndex index;
	GetDropPosition(index, event);

	//check to see if this is our widget (DRAG/DROP) class:
	DragDropInterface *source = dynamic_cast<DragDropInterface*>(event->source());
	if(source==NULL)
	{
		const QMimeData *mime = event->mimeData();

		//------------------------------------------------
		// LIST of documents entities:
		//------------------------------------------------
		//internal/external
		if (mime->hasFormat(SOKRATES_MIME_LIST))
		{
			int nEntityID;
			DbRecordSet lstDropped;

			//import emails:
			QByteArray arData = mime->data(SOKRATES_MIME_LIST);
			if(arData.size() > 0)
			{
				lstDropped=XmlUtil::ConvertByteArray2RecordSet_Fast(arData);
			}
			QByteArray arDataType = mime->data(SOKRATES_MIME_DROP_TYPE);
			if(arDataType.size() > 0)
			{
				bool bOK;
				nEntityID=arDataType.toInt(&bOK);
				if (!bOK) nEntityID=-1;
			}

			DropHandler(index,nEntityID,lstDropped,event); //call custom handler
			event->accept();
			return;
		}
		event->ignore();
		return;
	}
		
	

	//check if we support this drop:
	if(CheckIfDropIsAcceptable(source->GetDropType()))
	{
			//Get Data:
			DbRecordSet sourceData;
			source->GetDropValue(sourceData, true);
			if(sourceData.getRowCount()>0)
			{
				DropHandler(index,source->GetDropType(),sourceData,event); //call custom handler
			}
	}
}


/*!
	Called by dropEvent to establish where drop occured.
	Default implementation for all widgets based on the QAbstractItemView, if using your own, reimplement this method
	Warning: when using index test if it is valid with isValid(), because drop can occur on anywhere in widget (not at data item)
	
	\param   index - returned index in model of drop event
	\param   event - drop event
*/
bool DragDropInterface::GetDropPosition(QModelIndex &index, QDropEvent *event)
{
	QAbstractItemView* pView=dynamic_cast<QAbstractItemView*>(m_pWidget);
	if(pView==NULL)return false;
	index=pView->indexAt(event->pos());
	return true;
}


