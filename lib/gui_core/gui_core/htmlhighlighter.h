#ifndef HtmlHighlighter_H
#define HtmlHighlighter_H

#include <QSyntaxHighlighter>

//http://doc.trolltech.com/qq/qq21-syntaxhighlighter.html#example
//TOFIX: If we wanted to improve the HTML highlighter further, we would use different highlighting for the tag delimiters, names, attributes, and attribute values. 

class HtmlHighlighter : public QSyntaxHighlighter
{
    Q_OBJECT
 
public:
    enum Construct {
        Entity,
        Tag,
		TagParamValue,
		TagParamName,
        Comment,
        LastConstruct = Comment
    };
 
    HtmlHighlighter(QTextDocument *document);
 
    void setFormatFor(Construct construct, const QTextCharFormat &format);
    QTextCharFormat formatFor(Construct construct) const { return m_formats[construct]; }
 
protected:
    enum State {
        NormalState = -1,
        InComment,
        InTag
    };
 
    void highlightBlock(const QString &text);
 
private:
    QTextCharFormat m_formats[LastConstruct + 1];
};

#endif // HtmlHighlighter_H
