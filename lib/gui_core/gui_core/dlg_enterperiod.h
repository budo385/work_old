#ifndef DLG_ENTERPERIOD_H
#define DLG_ENTERPERIOD_H

#include <QtWidgets/QDialog>
#include "generatedfiles/ui_dlg_enterperiod.h"


/*!
	\class Dlg_EnterPeriod
	\brief Dialog for entering date period
	\ingroup GUICore_CustomWidgets
*/

class Dlg_EnterPeriod : public QDialog
{
    Q_OBJECT

public:
    Dlg_EnterPeriod(QWidget *parent = 0);
    ~Dlg_EnterPeriod();

	void setPeriod(QDate from,QDate to);
	void getPeriod(QDate &from,QDate &to);

private:
    Ui::Dlg_EnterPeriodClass ui;

private slots:
	void on_btnCancel_clicked();
	void on_btnOK_clicked();

};

#endif // DLG_ENTERPERIOD_H
