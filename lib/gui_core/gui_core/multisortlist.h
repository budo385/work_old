#ifndef MULTISORTLIST_H
#define MULTISORTLIST_H

#include <QListWidget>
#include "gui_core/gui_core/dragdropinterface.h"


/*!
	\class  MultiSortList
	\ingroup GUICore_UniversalTable
	\brief  Simple list widget used by UniversalTableWidget for multicolumn sorting

	Use:

*/
class MultiSortList : public QListWidget, public DragDropInterface
{
	Q_OBJECT

public:
	MultiSortList(QWidget *parent);
	~MultiSortList();

	void SetData(DbRecordSet &data);
	int GetDropType() const;
	void GetDropValue(DbRecordSet &values, bool bSelectItemChildren = false) const;

private slots:
	void SelectionChanged();

private:
	DbRecordSet m_lstData;
    
};

#endif // MULTISORTLIST_H


