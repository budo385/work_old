#include "richeditordlg.h"

#include <QApplication>
#include <QDesktopWidget>
#include <QClipboard>
#include <QtWidgets/QDialog>
#include <QMenuBar>
#include <QToolBar>
#include <QComboBox>
#include <QTextEdit>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QFontDatabase>
#include <QPushButton>
#include <QTextCharFormat>
#include <QTextCursor>
#include <QTextList>
#include <QColorDialog>
#include <QButtonGroup>

RichEditorDlg::RichEditorDlg(QWidget *parent)
    : QDialog(parent)
{
	m_pMainLayout	  = new QVBoxLayout;
	m_pRichEditWidget = new RichEditWidget(false);
	m_pMenuBar		  = new QMenuBar;

	m_pMainLayout->setMargin(0);
	m_pRichEditWidget->resize(frameSize());

	//Add actions to menu.
	QMenu *menu = new QMenu(tr("&File"));
	menu->addAction(m_pRichEditWidget->m_pActionOpenFile);
	menu->addAction(m_pRichEditWidget->m_pActionSaveFile);
	menu->addAction(m_pRichEditWidget->m_pActionSaveFileAs);
	m_pMenuBar->addMenu(menu);

	menu = new QMenu(tr("&Edit"));
	menu->addAction(m_pRichEditWidget->m_pActionUndo);
	menu->addAction(m_pRichEditWidget->m_pActionRedo);
	menu->addSeparator();
	menu->addAction(m_pRichEditWidget->m_pActionCut);
	menu->addAction(m_pRichEditWidget->m_pActionCopy);
	menu->addAction(m_pRichEditWidget->m_pActionPaste);
	m_pMenuBar->addMenu(menu);
	
	menu = new QMenu(tr("&Format"));
	menu->addAction(m_pRichEditWidget->m_pActionTextBold);
	menu->addAction(m_pRichEditWidget->m_pActionTextItalic);
	menu->addAction(m_pRichEditWidget->m_pActionTextUnderline);
	menu->addSeparator();
	menu->addActions(m_pRichEditWidget->m_pActionGroup->actions());
	menu->addSeparator();
	menu->addAction(m_pRichEditWidget->m_pActionTextColor);
	m_pMenuBar->addMenu(menu);

	//Menu bar.
	m_pMainLayout->addWidget(m_pMenuBar);
	//Rich edit widget.
	m_pMainLayout->addWidget(m_pRichEditWidget);

	//Dialog buttons.
	QHBoxLayout *OkCancelLayout = new QHBoxLayout;
	QPushButton *OkButton		= new	QPushButton(tr("&Ok"));
	QPushButton *CancelButton	= new	QPushButton(tr("Cancel"));
	OkButton->setMinimumWidth(100);
	OkButton->setMaximumWidth(100);
	CancelButton->setMinimumWidth(100);
	CancelButton->setMaximumWidth(100);
	OkCancelLayout->addStretch(10);
	OkCancelLayout->addWidget(OkButton);
	OkCancelLayout->addWidget(CancelButton);
	OkCancelLayout->setMargin(10);
	connect(OkButton,		SIGNAL(clicked()), this, SLOT(accept()));
	connect(CancelButton,	SIGNAL(clicked()), this, SLOT(reject()));
	m_pMainLayout->addLayout(OkCancelLayout, 10);

	//Set default window title.
	setWindowTitle(tr("SOKRATES Editor"));

	//Set margin and layout.
	setLayout(m_pMainLayout);
	//Set size to 75% of screen.
	resize(qApp->desktop()->screenGeometry().width()*.75, qApp->desktop()->screenGeometry().height()*.75);

}

RichEditorDlg::~RichEditorDlg()
{

}

void RichEditorDlg::SetHtml(QString strHtml)
{
	m_pRichEditWidget->SetHtml(strHtml);

	//Enable save buttons if there is some text.
	if (!strHtml.isEmpty())
		m_pRichEditWidget->OnTextChanged();
}

QString RichEditorDlg::GetHtml()
{
	return m_pRichEditWidget->GetHtml();
}

void RichEditorDlg::SetPlainText(QString strPlainText)
{
	m_pRichEditWidget->SetPlainText(strPlainText);

	//Enable save buttons if there is some text.
	if (!strPlainText.isEmpty())
		m_pRichEditWidget->OnTextChanged();
}

void RichEditorDlg::SetDocument(QTextDocument *pTextDocument)
{
	m_pRichEditWidget->SetDocument(pTextDocument);
}

QString RichEditorDlg::GetPlainText()
{
	return m_pRichEditWidget->GetPlainText();
}

void RichEditorDlg::SetEditMode(bool bGoEdit)
{
	m_pRichEditWidget->SetEditMode(bGoEdit);
}

void RichEditorDlg::SetDialogTitle(QString strTitle)
{
	setWindowTitle(strTitle);
}

bool RichEditorDlg::IsHTMLDisplayed()
{
	return m_pRichEditWidget->IsHTMLDisplayed();
}

void RichEditorDlg::SwitchFromHTMLToNormalView()
{
	m_pRichEditWidget->SwitchFromHTMLToNormalView();
}
