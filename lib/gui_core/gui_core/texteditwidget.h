#ifndef TEXTEDITWIDGET_H
#define TEXTEDITWIDGET_H

#include <QTextEdit>
#include "gui_core/gui_core/dragdropinterface.h"


/*!
	\class TextEditWidget
	\brief  QTextEdit with dropping handler..
	\ingroup GUICore_CustomWidgets

*/
class TextEditWidget : public QTextEdit, public DragDropInterface
{
	Q_OBJECT

public:
    TextEditWidget(QWidget *parent);
    ~TextEditWidget();

	void EnableFileDrop(){m_bFileDropEnabled=true;}
	void EnableDropSignal(){m_bDropSignal=true;}

	//Drop operations by widget:
	Qt::DropActions supportedDropActions() const {return DragDropInterface::supportedDropActions();};
	void dropEvent(QDropEvent *event);
	void dragEnterEvent ( QDragEnterEvent * event );
	void dragMoveEvent ( QDragMoveEvent * event ){event->acceptProposedAction();};
	//void dragLeaveEvent ( QDragLeaveEvent * event );


	//our custom drop handlers:
	int GetDropType() const;
	virtual void GetDropValue(DbRecordSet &values, bool bSelectItemChildren = false) const;
	virtual void DropHandler(QModelIndex index, int nDropType, DbRecordSet &DroppedValue,QDropEvent *event);

signals:
	void DataDroped();


private:

	bool m_bFileDropEnabled,m_bDropSignal;
	DbRecordSet m_lstFilesDropped;
    
};

#endif // TEXTEDITWIDGET_H
