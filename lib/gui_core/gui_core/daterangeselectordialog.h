#ifndef DATERANGESELECTORDIALOG_H
#define DATERANGESELECTORDIALOG_H

#include <QtWidgets/QDialog>
#include "ui_daterangeselectordialog.h"

class DateRangeSelectorDialog : public QDialog
{
	Q_OBJECT

public:
	DateRangeSelectorDialog(QDateTime &dateFrom, QDateTime &dateTo, int nRange, QWidget *parent = 0);
	~DateRangeSelectorDialog();

signals:
	void OkSignal(QDate dtFrom, QDate dtTo, int nRange);

private:
	Ui::DateRangeSelectorDialog ui;

private slots:
	void on_btnPrev_clicked();
	void on_btnNext_clicked();
	void on_btnCurDateRange_clicked();
	void OnComboChanged(int nIdx);
	void on_okPushButton_clicked();
	void on_cancelPushButton_clicked();
	void OnFromDateFocusOut();
	void OnToDateFocusOut();
};

#endif // DATERANGESELECTORDIALOG_H
