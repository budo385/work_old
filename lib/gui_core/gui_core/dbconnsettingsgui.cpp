#include "dbconnsettingsgui.h"
#include "common/common/sha256hash.h"
#include "common/common/Status.h"
#include "db_core/db_core/db_drivers.h"
//#include "db/db/dbsqlmanager.h"
#include "testconnresult.h"
#include <QInputDialog>
#include <QtWidgets/QMessageBox>
#include "biginputdialog.h"
#include "thememanager.h"



DbConnSettingsGUI::DbConnSettingsGUI(QWidget *parent, Qt::WindowFlags flags)
    : QDialog(parent, flags)
{
	ui.setupUi(this);

	ui.DbTypeComboBox->addItem("MYSQL");
	ui.DbTypeComboBox->addItem("ORACLE");
	ui.DbTypeComboBox->addItem("FIREBIRD");
	ui.DbTypeComboBox->addItem("POSTGRESS");
	ui.testButton->setVisible(false);
	m_nCurSel = -1;
	ClearInfo();
	EnableFields(false);

	m_bUsePassword = false;
	//issue 1666:
	setStyleSheet(ThemeManager::GetGlobalWidgetStyle());
}

DbConnSettingsGUI::~DbConnSettingsGUI()
{
}
/*
QString DbConnSettingsGUI::TestFeatures(QSqlDatabase *database)
{	
	QString str;

	//Transaction.
	if (database->driver()->hasFeature(QSqlDriver::Transactions))
		str.append(tr("    Transactions supported.\n"));
	else
		str.append(tr("    Transactions not supported.\n"));

	//Query size.
	if (database->driver()->hasFeature(QSqlDriver::QuerySize))
		str.append(tr("    Query size supported.\n"));
	else
		str.append(tr("    Query size not supported.\n"));

	//Unicode.
	if (database->driver()->hasFeature(QSqlDriver::Unicode))
		str.append(tr("    Unicode supported.\n"));
	else
		str.append(tr("    Unicode not supported.\n"));

	//Prepared queries.
	if (database->driver()->hasFeature(QSqlDriver::PreparedQueries))
		str.append(tr("    Prepared queries supported.\n"));
	else
		str.append(tr("    Prepared queries not supported.\n"));

	//Last inserted Id.
	if (database->driver()->hasFeature(QSqlDriver::LastInsertId))
		str.append(tr("    Last inserted Id supported.\n"));
	else
		str.append(tr("    Last inserted Id not supported.\n"));

	//Batch operations.
	if (database->driver()->hasFeature(QSqlDriver::BatchOperations))
		str.append(tr("    Batch operations supported.\n"));
	else
		str.append(tr("    Batch operations not supported.\n"));

	return str;
}
*/

QString DbConnSettingsGUI::GetDriver()
{
	if (ui.DbTypeComboBox->currentIndex() == 0)
		return DBTYPE_MYSQL;
	else if (ui.DbTypeComboBox->currentIndex() == 1)
		return DBTYPE_ORACLE;
	else if (ui.DbTypeComboBox->currentIndex() == 2)
		return DBTYPE_FIREBIRD;
	else if (ui.DbTypeComboBox->currentIndex() == 3)
		return DBTYPE_POSTGRESQL;
	
	return "";
}

void DbConnSettingsGUI::SetDriver(const QString DbName)
{
	if (DbName == DBTYPE_MYSQL)
		ui.DbTypeComboBox->setCurrentIndex(0);
	else if (DbName == DBTYPE_ORACLE)
		ui.DbTypeComboBox->setCurrentIndex(1);
	else if (DbName == DBTYPE_FIREBIRD)
		ui.DbTypeComboBox->setCurrentIndex(2);
	else if (DbName == DBTYPE_POSTGRESQL)
		ui.DbTypeComboBox->setCurrentIndex(3);
}

void DbConnSettingsGUI::on_testButton_clicked()
{
	/*
	if (m_nCurSel < 0)
		return;	

	UpdateInfo(false);

	Status err;


	DbSqlManager Manager; // = new DbSqlManager();
	Manager.Initialize(err, m_lstData[m_nCurSel], 10);

	QSqlDatabase *database = Manager.ReserveConnection(err);
	if (err.IsOK())
	{
		TestConnResult *result = new TestConnResult(this);

		QString str;
		str.append(tr("Connection succeeded!\n\n"));
		str.append(tr("    Features:\n"));
		str.append(TestFeatures(database));
		result->ui.TextBrowser->setPlainText(str);
		result->show();
	}
	else
	{
		QMessageBox::information(this, tr("Connection test"), tr("Unable to connect to database"), tr("&Ok"));
	}
	Manager.ReleaseConnection(database);
	*/
}

void DbConnSettingsGUI::on_newConnButton_clicked()
{
	//add new connection item
	DbConnectionSettings item;
	item.m_strCustomName = tr("New Connection");
	m_lstData.append(item);
	
	//refresh list
	ui.listWidget->addItem(item.m_strCustomName);
	ui.listWidget->setCurrentRow(ui.listWidget->count()-1);
	m_nCurSel = ui.listWidget->currentRow();
}

void DbConnSettingsGUI::on_saveButton_clicked()
{
	UpdateInfo(false);
	done(1);
}

void DbConnSettingsGUI::on_delConnButton_clicked()
{
	m_nCurSel = ui.listWidget->currentRow();

	if(m_nCurSel >= 0)
	{
		int nIdx = m_nCurSel;
		m_nCurSel = -1;

		m_lstData.removeAt(nIdx);
		ui.listWidget->blockSignals(true);
		QListWidgetItem *item = ui.listWidget->takeItem(nIdx);
		delete item;
		ui.listWidget->blockSignals(false);

		int nNew = ui.listWidget->currentRow();
		on_listWidget_currentRowChanged(nNew);
	}
}

void DbConnSettingsGUI::on_cancelButton_clicked()
{
	done(0);
}

void DbConnSettingsGUI::SetPassword(bool bUsePassword, QString strHash)
{
	m_bUsePassword = bUsePassword;
	m_strHash = strHash;

	ui.chkUseUserPassword->setChecked(m_bUsePassword);
}

void DbConnSettingsGUI::GetPassword(bool &bUsePassword, QString &strHash)
{
	bUsePassword = m_bUsePassword;
	strHash = m_strHash;
}

QString DbConnSettingsGUI::GetHashString(QString strPassword)
{
	QByteArray data;
	data.append(strPassword);

	//calculate hash value
	QByteArray hash;
	Sha256Hash Hasher;
	hash = Hasher.GetHash(data);

	//convert it into the string (binary to Base64)
	QString strResult;
	strResult = hash.toBase64();
	return strResult;
}

void DbConnSettingsGUI::on_chkUseUserPassword_stateChanged(int nChecked)
{
	m_bUsePassword = nChecked;

	if(m_bUsePassword)
		ui.btnChangePassword->setEnabled(true);
	else
		ui.btnChangePassword->setEnabled(false);
}

void DbConnSettingsGUI::on_btnChangePassword_clicked()
{
	QString strTitle(tr("User Settings Password"));
	QString strLabel(tr("New Password:"));

	biginputdialog dlg(strTitle, strLabel, NULL, "");
	dlg.SetPasswordMode();
	int nRes = dlg.exec();
	if(nRes > 0)
	{
		QString text = dlg.m_strInput;
		if(text.isEmpty()){
			ui.chkUseUserPassword->setChecked(false);
			m_strHash = "";
			QMessageBox::information(this, tr("Error"), tr("Password must not be empty!"), tr("&Ok"));
			return;
		}

		m_strHash = GetHashString(text);
	}
	else{
		if(m_strHash.isEmpty())
			ui.chkUseUserPassword->setChecked(false);
	}
}

void DbConnSettingsGUI::SetConnections(QList<DbConnectionSettings> &lstData, int nCurrentIdx)
{
	//copy data
	m_lstData = lstData;

	//refresh list
	ui.listWidget->clear();
	int nCount = m_lstData.size();
	for(int i=0; i<nCount; i++)
		ui.listWidget->addItem(m_lstData[i].m_strCustomName);

	if(nCurrentIdx >=0)
		ui.listWidget->setCurrentRow(nCurrentIdx);
}

void DbConnSettingsGUI::ClearInfo()
{
	ui.ConnNameLineEdit->setText("");
	ui.DbNameLineEdit->setText("");
	ui.UserLineEdit->setText("");
	ui.PassLineEdit->setText("");
	ui.HostLineEdit->setText("");
	ui.PortLineEdit->setText("");
	ui.OptionsLineEdit->setText("");
	ui.TimeoutLineEdit->setText("");
	SetDriver(DBTYPE_MYSQL);
}

void DbConnSettingsGUI::EnableFields(bool bEnable)
{
	ui.ConnNameLineEdit	->setEnabled(bEnable);
	ui.DbNameLineEdit	->setEnabled(bEnable);
	ui.UserLineEdit		->setEnabled(bEnable);
	ui.PassLineEdit		->setEnabled(bEnable);
	ui.HostLineEdit		->setEnabled(bEnable);
	ui.PortLineEdit		->setEnabled(bEnable);
	ui.OptionsLineEdit	->setEnabled(bEnable);
	ui.TimeoutLineEdit	->setEnabled(bEnable);
	ui.DbTypeComboBox	->setEnabled(bEnable);
}


	
void DbConnSettingsGUI::UpdateInfo(bool bToScreen)
{
	if(m_nCurSel < 0)
		return;		//no selection

	//TOFIX add support for tablespace and indexspace

	if(bToScreen)
	{
		ui.ConnNameLineEdit	->setText(m_lstData[m_nCurSel].m_strCustomName);
		ui.DbNameLineEdit	->setText(m_lstData[m_nCurSel].m_strDbName);
		ui.UserLineEdit		->setText(m_lstData[m_nCurSel].m_strDbUserName);
		ui.PassLineEdit		->setText(m_lstData[m_nCurSel].m_strDbPassword);
		ui.HostLineEdit		->setText(m_lstData[m_nCurSel].m_strDbHostName);
		ui.PortLineEdit		->setText(QString().sprintf("%d",m_lstData[m_nCurSel].m_nDbPort));
		ui.OptionsLineEdit	->setText(m_lstData[m_nCurSel].m_strDbOptions);
		ui.TimeoutLineEdit	->setText(QString().sprintf("%d",m_lstData[m_nCurSel].m_nDbPoolTimeout));
		SetDriver(m_lstData[m_nCurSel].m_strDbType);
	}
	else
	{
		m_lstData[m_nCurSel].m_strCustomName = ui.ConnNameLineEdit->text();
		m_lstData[m_nCurSel].m_strDbName	 = ui.DbNameLineEdit->text();
		m_lstData[m_nCurSel].m_strDbUserName = ui.UserLineEdit->text();
		m_lstData[m_nCurSel].m_strDbPassword = ui.PassLineEdit->text();
		m_lstData[m_nCurSel].m_strDbHostName = ui.HostLineEdit->text();
		m_lstData[m_nCurSel].m_nDbPort		 = ui.PortLineEdit->text().toInt();
		m_lstData[m_nCurSel].m_strDbOptions  = ui.OptionsLineEdit->text();
		m_lstData[m_nCurSel].m_nDbPoolTimeout= ui.TimeoutLineEdit->text().toInt();
		m_lstData[m_nCurSel].m_strDbType	 = GetDriver();
	}
}

void DbConnSettingsGUI::on_listWidget_currentRowChanged(int nIdx)
{
	UpdateInfo(false);
	m_nCurSel = nIdx;
	UpdateInfo(true);
	EnableFields(m_nCurSel >= 0);
}



