#ifndef GUIFIELD_H
#define GUIFIELD_H

#include <QObject>
#include <QLabel>
#include <QWidget>
#include <QList>
#include "gui_core/gui_core/guidatamanipulator.h"
//#include "gui_core/gui_core/guifield_validator.h"
 

/*!
	\class GuiField
	\brief Defines GUI field
	\ingroup GUICore_GUIFields

	Used to describe GUI field and register changes from widget into the guidatamanipulator
	Supports:
	- QLineEdit
	- QCheckBox
	- for other controls only supports tooltip/label registration/indexing

	Use:
	- inherit and implement constructor, RefreshDisplay() and FieldChanged()
*/
class GuiField : public QObject
{
	friend class GuiFieldManager;

	Q_OBJECT

public:

	//constructors
	GuiField(QString strFieldName,QWidget *pWidget,GuiDataManipulator* pDataManager,DbView *TableView=NULL, QLabel *pLabel=NULL);
	GuiField(QString strFieldName,QWidget *pWidget,DbRecordSet* pDataSource,DbView *TableView=NULL, QLabel *pLabel=NULL);

	virtual ~GuiField();

	//set validator for user input
	//void SetValidator(GuiField_Validator* pValidator){m_pValidator=pValidator;}
	//virtual bool ValidateUserInput(QVariant& value);

	//set row(0 is def)
	void SetCurrentRow(unsigned int nRow){m_nCurrentRow=nRow;};
	virtual void RefreshDisplay()=0; //DATA SOURCE -> UI


	int		m_nIndex;					//< index (column number) inside DataSource
	QWidget *m_pWidget;					//< widget control on window
	GuiDataManipulator* m_pDataManager; //< datasource
	unsigned int 	m_nCurrentRow;		//< row inside data source in which to change data
	//GuiField_Validator* m_pValidator;

//public slots:
	//virtual void FieldChanged()=0; //UI -> DATA SOURCE

	virtual void setEnabled(bool bEnable){m_pWidget->setEnabled(bEnable);};

private:

	void Initalize(QString strFieldName,QWidget *pWidget,GuiDataManipulator* pDataManager,DbView *TableView, QLabel *pLabel=NULL);

	bool m_bUseInternalDataManipulator;
	

};




#endif // GUIFIELD_H


