#ifndef GUIFIELDMANAGER_H
#define GUIFIELDMANAGER_H

#include <QObject>
#include <QLabel>
#include <QList>

#include "gui_core/gui_core/guidatamanipulator.h"
#include "db_core/db_core/dbsqltableview.h"
#include "common/common/dbrecordset.h"

#include "guifield.h"
#include "guifield_radiobutton.h"
#include "guifield_textedit.h"


/*!
	\class  GuiFieldManager
	\ingroup GUICore_GUIFields
	\brief  Class provides easier handling of fields on form

	Features:
	- automatically stores changes from QLineEdit & QCheckBox to given column in data source
	- automatically displays Label and ToolTip defined in the view if widget and label reference is given
	- stores index of column inside data, to speed up access
	- calls ChangeData of guidatamanipulator, set validation inside this method
	- when data in datasource is changed, call RefreshDisplay to refresh content on widnow!

	Use:
	- if using many edit text boxes/chkboxes, this can speed up ui setup process
	- if using other widgets, you can still use it for displaying label/tooltip, but data change events must be manually built
	- must use GuiDataManipulator for data validation/change
	- data source inside GuiDataManipulator must be defined by valid viewID in DbSqlTableView

*/
class GuiFieldManager 
{

public:
    GuiFieldManager(GuiDataManipulator* pData,int nViewID=-1);//,GuiField_Validator *pValidator=NULL);
	GuiFieldManager(DbRecordSet* pData,int nViewID=-1);
    virtual ~GuiFieldManager();

	void RegisterField(QString strFieldName,QWidget *pWidget,QLabel *pLabel=NULL);
	void RegisterField(QString strFieldName,GuiField *m_pGuiField);
	GuiField* GetFieldHandler(QString strFieldName){return m_lstFields[strFieldName];}

	//virtual void CreateField(QString strFieldName,QWidget *m_pWidget);
	virtual void RefreshDisplay();
	int GetFieldIdx(QString strFieldName){return m_lstFields[strFieldName]->m_nIndex;}
	void SetCurrentRow(unsigned int nRow);

	void SetEditMode(bool bEdit);
private:
	QHash<QString,GuiField*> m_lstFields;

	DbView m_View;
	GuiDataManipulator* m_pDataManager;

	//GuiField_Validator* m_pValidator;
	bool m_bUseInternalDataManipulator;
   
};

#endif // GUIFIELDMANAGER_H


