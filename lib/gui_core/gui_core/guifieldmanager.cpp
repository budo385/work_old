#include "guifieldmanager.h"
#include "guifield_checkbox.h"
#include "guifield_dateedit.h"
#include "guifield_doublespinbox.h"
#include "guifield_lineedit.h"
#include "guifield_dateeditwidget.h"
#include "guifield_combobox.h"
#include "guifield_richedit.h"



//------------------------------------------------------
//			GuiFieldManager
//------------------------------------------------------

/*!
	Initializes with data source and with view from which datasource is defined

	\param pData			- data container
	\param nViewID			- view id, used for getting tooltip and label

*/
GuiFieldManager::GuiFieldManager(GuiDataManipulator* pData,int nViewID)//,GuiField_Validator *pValidator)
{
	//set data
	m_pDataManager=pData;
	m_bUseInternalDataManipulator=false;
	//set view:
	if(nViewID!=-1)
		m_View=DbSqlTableView::getView(nViewID);
	//m_pValidator=pValidator;
}


/*!
	Same as above: data source is DbRecordSet
*/
GuiFieldManager::GuiFieldManager(DbRecordSet* pData,int nViewID)
{
	//set data
	m_pDataManager= new GuiDataManipulator(pData);
	m_bUseInternalDataManipulator=true;
	//set view:
	if(nViewID!=-1)
		m_View=DbSqlTableView::getView(nViewID);
	//m_pValidator=NULL;
}


//delete all gui fields
GuiFieldManager::~GuiFieldManager()
{
	QHashIterator<QString,GuiField*> i(m_lstFields);
    while (i.hasNext()) 
	{
        i.next();
		delete i.value();
	}
	if(m_bUseInternalDataManipulator)delete m_pDataManager;
}

/*!
	Connects datasource field (DbRecordSet) to window widget. 
	Changes from widgets are stored in the datasource.
	RefreshDisplay() sets data from datasource to the widgets.
	Also it automatically stores widget/field information such as label, tooltip and index in datasource
	Based on GUIFIELD, supports these widgets:
	- linedit
	- chxbox
	- doublespinbox
	- datetime
	- picture(label)
	You can add own GUI fields or reimplement existing one, then use other  RegisterField() (below)


	\param strFieldName		- column name as defined in data source or in view
	\param pWidget			- widget used for displaying data (edit, chkbox) can be omitted
	\param pLabel			- label of widget , can be omitted

*/
void GuiFieldManager::RegisterField(QString strFieldName,QWidget *pWidget,QLabel *pLabel)
{
	
	//if line edit
	if(dynamic_cast<DateEditWidget*>(pWidget)!=NULL)
	{
		m_lstFields[strFieldName]= new GuiField_DateEditWidget(strFieldName,pWidget,m_pDataManager,&m_View,pLabel);
	}
	else if(dynamic_cast<QLineEdit*>(pWidget)!=NULL)
	{
		m_lstFields[strFieldName]= new GuiField_LineEdit(strFieldName,pWidget,m_pDataManager,&m_View,pLabel);
	}
	else if(dynamic_cast<QCheckBox*>(pWidget)!=NULL)
	{
		m_lstFields[strFieldName]= new GuiField_CheckBox(strFieldName,pWidget,m_pDataManager,&m_View,pLabel);
	}
	else if(dynamic_cast<QDoubleSpinBox*>(pWidget)!=NULL)
	{
		m_lstFields[strFieldName]= new GuiField_DoubleSpinBox(strFieldName,pWidget,m_pDataManager,&m_View,pLabel);
	}
	else if(dynamic_cast<DateTimeEditEx*>(pWidget)!=NULL) //covers: date, time, edit
	{
		m_lstFields[strFieldName]= new GuiField_DateEdit(strFieldName,pWidget,m_pDataManager,&m_View,pLabel);
	}
	else if(dynamic_cast<QTextEdit*>(pWidget)!=NULL)
	{
		m_lstFields[strFieldName]= new GuiField_TextEdit(strFieldName,pWidget,m_pDataManager,&m_View,pLabel);
	}
	else if(dynamic_cast<RichEditWidget*>(pWidget)!=NULL)
	{
		m_lstFields[strFieldName]= new GuiField_RichEdit(strFieldName,pWidget,m_pDataManager,&m_View,pLabel);
	}
	else
	{
		Q_ASSERT(false); //widget not supported
	}

	//m_lstFields[strFieldName]->SetValidator(m_pValidator);

}


/*!
	Overloaded function for custom GuiField classes.
	Create GUIFIELD with new, object will be destroyed here.

	\param strFieldName		- column name as defined in data source or in view
	\param pGuiField		- GuiField (make it with new(), object will be deleted here)
*/
void GuiFieldManager::RegisterField(QString strFieldName,GuiField *pGuiField)
{
	m_lstFields[strFieldName]=pGuiField;
	//m_lstFields[strFieldName]->SetValidator(m_pValidator);
}





/*!
	By default all changes are registered at row 0 in datasource.
	Call this to change current row on all fields inside set

	\param nRow			- new current row

*/
void GuiFieldManager::SetCurrentRow(unsigned int nRow)
{
	QHashIterator<QString,GuiField*> i(m_lstFields);
    while (i.hasNext()) 
	{
        i.next();
		i.value()->SetCurrentRow(nRow);
	}
}


/*!
	Refreshes display -> from datasource to display.
	Reimplement if you want.
	Datasource is fetched from current row, if current row does not exists inside datasource, 
	all controls will be cleared 

*/
void GuiFieldManager::RefreshDisplay()
{

	QHashIterator<QString,GuiField*> i(m_lstFields);
	while (i.hasNext()) 
	{
		i.next();
		i.value()->RefreshDisplay();
	}


}


//false: disables all registered widgets:
void GuiFieldManager::SetEditMode(bool bEdit)
{
	QHashIterator<QString,GuiField*> i(m_lstFields);
	while (i.hasNext()) 
	{
		i.next();
		i.value()->setEnabled(bEdit);
	}
}