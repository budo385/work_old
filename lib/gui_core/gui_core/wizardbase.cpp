#include "wizardbase.h"
#include <QtWidgets/QMessageBox>
#include <QApplication>
#include <QDesktopWidget>

WizardBase::WizardBase(int LocationID, QHash<int, WizardPagesCollection*> hshWizardPageHash, ClientEntityCache *pGlobalCache, QWidget *parent)
	: QDialog(parent),m_bRetry(false)
{
	//Set page number to first page.
	//m_nLocationID	= LocationID;
	m_nPageNumber	= 0;
	m_hshWizardPageHash = hshWizardPageHash;
	m_pCache=pGlobalCache;

	//Get wizard pages.
	FillPageHashFromThisPageUp(-1, LocationID);
	
	cancelButton	= new QPushButton(tr("Cancel"));
	backButton		= new QPushButton(tr("< &Back"));
	nextButton		= new QPushButton(tr("Next >"));
	finishButton	= new QPushButton(tr("&Finish"));

	connect(cancelButton,	SIGNAL(clicked()),	this,	SLOT(reject()));
	connect(backButton,		SIGNAL(clicked()),	this,	SLOT(backButtonClicked()));
	connect(nextButton,		SIGNAL(clicked()),	this,	SLOT(nextButtonClicked()));
	connect(finishButton,	SIGNAL(clicked()),	this,	SLOT(accept()));

	buttonLayout	= new QHBoxLayout;
	buttonLayout->addStretch(1);
	buttonLayout->addWidget(cancelButton);
	buttonLayout->addWidget(backButton);
	buttonLayout->addWidget(nextButton);
	buttonLayout->addWidget(finishButton);

	mainLayout		= new QVBoxLayout;
	mainLayout->addLayout(buttonLayout);
	setLayout(mainLayout);


	QWidget::setTabOrder(finishButton,nextButton);
	QWidget::setTabOrder(nextButton,backButton);
	QWidget::setTabOrder(backButton,cancelButton);

	setFirstPage();
	resize(480, 400);
}

WizardBase::~WizardBase()
{
	qDeleteAll(m_hshWizardPageHash);
	m_hshWizardPageHash.clear();
}

void WizardBase::setFirstPage()
{
	//Set first page and connect signal.
	mainLayout->insertWidget(0, m_hshWizardPageOrder.value(0));
	resize(m_hshWizardPageOrder.value(0)->frameSize());
	connect(m_hshWizardPageOrder.value(0), SIGNAL(completeStateChanged()), this, SLOT(completeStateChanged()));
	connect(m_hshWizardPageOrder.value(m_nPageNumber), SIGNAL(goNextPage()), this, SLOT(pageRequestNextPage()));
	if (!m_hshWizardPageOrder.value(0)->m_bInitialized)m_hshWizardPageOrder.value(0)->Initialize(); //B.T. impruvd
	m_hshWizardPageOrder.value(0)->show();
	m_hshWizardPageOrder.value(0)->setFocus();
	setWindowTitle(m_hshWizardPageOrder.value(0)->GetPageTitle());
	finishButton->setText(m_hshWizardPageOrder.value(0)->GetPageFinishButttonTitle());

	//First disable and then enable buttons if they read something from cache.
	DisableButtons();
	EnableButtons();
}

void WizardBase::backButtonClicked()
{	
	DisableButtons();

	//Remove old page from layout and disconnect signal.
	mainLayout->removeWidget(m_hshWizardPageOrder.value(m_nPageNumber));
	disconnect(m_hshWizardPageOrder.value(m_nPageNumber), SIGNAL(completeStateChanged()), this, SLOT(completeStateChanged()));
	disconnect(m_hshWizardPageOrder.value(m_nPageNumber), SIGNAL(goNextPage()), this, SLOT(pageRequestNextPage()));
	m_hshWizardPageOrder.value(m_nPageNumber)->hide();

	//Decrease page number.
	m_nPageNumber--;
	Q_ASSERT(m_hshWizardPageOrder.value(m_nPageNumber) != NULL);

	//Add previous page to layout and connect signal.
	mainLayout->insertWidget(0, m_hshWizardPageOrder.value(m_nPageNumber));
	resize(m_hshWizardPageOrder.value(m_nPageNumber)->frameSize());
	connect(m_hshWizardPageOrder.value(m_nPageNumber), SIGNAL(completeStateChanged()), this, SLOT(completeStateChanged()));
	connect(m_hshWizardPageOrder.value(m_nPageNumber), SIGNAL(goNextPage()), this, SLOT(pageRequestNextPage()));
	m_hshWizardPageOrder.value(m_nPageNumber)->show();
	m_hshWizardPageOrder.value(m_nPageNumber)->setFocus();

	//Set window title and finish button text.
	setWindowTitle(m_hshWizardPageOrder.value(m_nPageNumber)->GetPageTitle());
	finishButton->setText(m_hshWizardPageOrder.value(m_nPageNumber)->GetPageFinishButttonTitle());
	
	EnableButtons();
}

void WizardBase::nextButtonClicked()
{
	DisableButtons();

	//Remove old page from layout and disconnect signal.
	mainLayout->removeWidget(m_hshWizardPageOrder.value(m_nPageNumber));
	disconnect(m_hshWizardPageOrder.value(m_nPageNumber), SIGNAL(completeStateChanged()), this, SLOT(completeStateChanged()));
	disconnect(m_hshWizardPageOrder.value(m_nPageNumber), SIGNAL(goNextPage()), this, SLOT(pageRequestNextPage()));
	
	m_hshWizardPageOrder.value(m_nPageNumber)->hide();
	//Call next button clicked virtual method on pages.
	m_hshWizardPageOrder.value(m_nPageNumber)->on_NextButtonClicked();

	//Increase page number.
	m_nPageNumber++;
	Q_ASSERT(m_hshWizardPageOrder.value(m_nPageNumber) != NULL);

	//Add next page to layout and connect signal.
	mainLayout->insertWidget(0, m_hshWizardPageOrder.value(m_nPageNumber));
	resize(m_hshWizardPageOrder.value(m_nPageNumber)->frameSize());
	connect(m_hshWizardPageOrder.value(m_nPageNumber), SIGNAL(completeStateChanged()), this, SLOT(completeStateChanged()));
	connect(m_hshWizardPageOrder.value(m_nPageNumber), SIGNAL(goNextPage()), this, SLOT(pageRequestNextPage()));
	if (!m_hshWizardPageOrder.value(m_nPageNumber)->m_bInitialized)m_hshWizardPageOrder.value(m_nPageNumber)->Initialize(); //B.T. impruvd
	m_hshWizardPageOrder.value(m_nPageNumber)->show();
	m_hshWizardPageOrder.value(m_nPageNumber)->setFocus();

	//MR: issue #2493: "open it centered in 2/3 of the height and width of the actual screen"
	int nDesktopWidth = QApplication::desktop()->availableGeometry().width();	//account for dock bar
	int nDesktopHeight = QApplication::desktop()->availableGeometry().height();	//account for dock bar
	resize(nDesktopWidth*2/3, nDesktopHeight*2/3);	//resize window to 2/3 of desktop
	move(nDesktopWidth/6, nDesktopHeight/6);	//center the window on screen (1/6 of screen left from each side)
	
	//Set window title and finish button text.
	setWindowTitle(m_hshWizardPageOrder.value(m_nPageNumber)->GetPageTitle());
	finishButton->setText(m_hshWizardPageOrder.value(m_nPageNumber)->GetPageFinishButttonTitle());
	
	EnableButtons();
}

void WizardBase::completeStateChanged()
{
	DisableButtons();
	EnableButtons();
}

void WizardBase::pageRequestNextPage()
{
	nextButtonClicked();
}

void WizardBase::DisableButtons()
{
	backButton->setDisabled(true);
	nextButton->setDisabled(true);
	finishButton->setDisabled(true);
}

void WizardBase::EnableButtons()
{
	backButton->setEnabled((m_nPageNumber == 0 ? false : true));

	//nextButton->setEnabled(m_vecWizardPageOrder.value(m_nPageNumber)->isComplete() && !(m_vecWizardPageOrder.count() == m_nPageNumber+1));
	//If simple wizard.
	int NextCollectionID = m_hshWizardPageOrder.value(m_nPageNumber)->GetNextPageCollectionID();
	if (NextCollectionID == -1)
	{
		nextButton	->setEnabled(m_hshWizardPageOrder.value(m_nPageNumber)->isComplete() && !(m_hshWizardPageOrder.count() == m_nPageNumber+1));
		finishButton->setEnabled(m_hshWizardPageOrder.value(m_hshWizardPageOrder.count()-1)->isComplete());
	}
	//Else if complex wizard call next collection.
	else
	{
		FillPageHashFromThisPageUp(m_nPageNumber, NextCollectionID);
		nextButton->setEnabled(m_hshWizardPageOrder.value(m_nPageNumber)->isComplete());
		m_hshWizardPageOrder.value(m_nPageNumber)->SetNextPageCollectionID(-1);
	}
}

void WizardBase::ClearPageHashFromThisPageUp(int nThisPageNumber)
{
	nThisPageNumber++;
	while (m_hshWizardPageOrder.contains(nThisPageNumber))
	{
		m_hshWizardPageOrder.remove(nThisPageNumber);
		nThisPageNumber++;
	}
}

void WizardBase::SetMainLayoutSize(QSize szSize)
{
	QLayoutItem *item = mainLayout->itemAt(0);
	QRect rect(0, 0, szSize.width(), szSize.height());
	item->setGeometry(rect);
}

void WizardBase::FillPageHashFromThisPageUp(int nThisPageNumber, int nNextCollectionID)
{
	//Get wizard pages.
	QList<WizardPage*> lstPagesList;
	lstPagesList << GetWizardPageVector(nNextCollectionID);
	foreach(WizardPage* page, lstPagesList)
	{
		nThisPageNumber++;
		page->SetParent(this);
		page->SetGlobalCache(m_pCache);
		m_hshWizardPageOrder.insert(nThisPageNumber, page);
	}
}

QList<WizardPage*> WizardBase::GetWizardPageVector(int WizardID)
{
	//Delete extracted entries from m_hshWizardPageHash -> this pages are deleted later from wizard.
	QList<WizardPage*> pages;
	//Get pages vector and put it in for later.
	if (m_hshWizardPageHash.contains(WizardID))
	{
		pages = m_hshWizardPageHash.value(WizardID)->GetPages();
	}
	//If page vector doesn't contain call page GetNextPageCollectionVector method of current page.
	else
	{
		pages = dynamic_cast<WizardPage*>(m_hshWizardPageOrder.value(m_nPageNumber))->GetNextPageCollectionVector();
	}

	//Return pages vector - finally.
	return pages;
}

void WizardBase::GetWizPageRecordSetList(QList<DbRecordSet> &RecordSetList)
{
	RecordSetList = m_lstPageRecordSet;
}

void WizardBase::accept()
{
	bool bAccept=true;
	int PagesCount = m_hshWizardPageOrder.count();
	for (int i = 0; i < PagesCount; ++i)
	{
		DbRecordSet rec;
		bool bOK=m_hshWizardPageOrder.value(i)->GetPageResult(rec);
		if (!bOK)
			bAccept=false;
		m_lstPageRecordSet << rec;

	}


	//if some page did not succeed in retrieving data, return error
	//NEW code:
	if (bAccept)
		done(QDialog::Accepted);
	else
	{
		if (m_bRetry)	//if retry, then ask if wanna stay
		{
			if(QMessageBox::question(NULL,tr("Confirmation"),tr("Try again?"),tr("Yes"),tr("No"))) 
			{
				done(QDialog::Rejected);
			}
			else
			{
				//reset pages:
				for (int i = 0; i < PagesCount; ++i)
					m_hshWizardPageOrder.value(i)->resetPage();
			}
		}
		else
			done(QDialog::Rejected);

	}

}

WizardPagesCollection::WizardPagesCollection(QList<WizardPage*> WizardPagesList)
{
	m_vectWizardPagesList = WizardPagesList;
}

WizardPagesCollection::~WizardPagesCollection()
{
	qDeleteAll(m_vectWizardPagesList);
	m_vectWizardPagesList.clear();
}

QList<WizardPage*> WizardPagesCollection::GetPages()
{
	return m_vectWizardPagesList;
}

void WizardPagesCollection::RemovePage(int nIndex)
{
	m_vectWizardPagesList.removeAt(nIndex);
}
void WizardPagesCollection::RemoveLastPage()
{
	m_vectWizardPagesList.removeLast();
}