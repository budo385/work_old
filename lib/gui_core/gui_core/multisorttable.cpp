#include "multisorttable.h"
#include <QAction>
#include "common/common/entity_id_collection.h"

MultiSortTable::MultiSortTable(QWidget *parent)
	: QTableWidget(parent),m_bEdit(true),m_lstData(NULL),m_pData(NULL),DragDropInterface(this)
{

}

MultiSortTable::~MultiSortTable()
{
	if(m_pData!=NULL) delete m_pData;
}


/*!
	Initializes table for display. Moved out from constructor to enable connecting to virtual slots!

	\param lstData			- data source (default GuiDataManipulator will be used)

*/
void MultiSortTable::Initialize(DbRecordSet *lstData,bool bEnableForColumnSetup)
{

	//can not initialize table view twice!!!
	//Q_ASSERT(m_pData==NULL);		Komentira MP na BT inicijativu 13.3.2012 jer je pucalo u sort tabu na commgridfilter widgetu.

	//support multicol drop:
	AddAcceptableDropTypes(MULTICOLUMNSORT);
	AddAcceptableDropTypes(ENTITY_DROP_TYPE_LIST_ITEM);
	

	//store data source in default data manipulator:
	m_pData = new GuiDataManipulator(lstData);
	m_lstData=lstData;
	//connect(m_pData,SIGNAL(RefreshDisplay()),this,SLOT(RefreshDisplay()));
	//connect(m_pData,SIGNAL(RefreshDisplayRow(int)),this,SLOT(RefreshDisplayRow(int)));
	//connect(m_pData,SIGNAL(RefreshSelection()),this,SLOT(RefreshSelection()));


	//selection mode:
	setSelectionBehavior( QAbstractItemView::SelectRows);
	setSelectionMode( QAbstractItemView::ExtendedSelection);


	m_pActionDel = new QAction(tr("&Delete Selection"), this);
	m_pActionDel->setShortcut(tr("Del"));
	//pAction->setIcon(style.standardIcon(QStyle::SP_TrashIcon));
	connect(m_pActionDel, SIGNAL(triggered()), this, SLOT(DeleteSelection()));
	addAction(m_pActionDel);


	//Drag & Drop:
	setAcceptDrops(true);
	setDropIndicatorShown(true);

	//for editing one cell:
	connect(this,SIGNAL(cellChanged(int,int)),this,SLOT(CellEdited(int,int)));
	connect(this,SIGNAL(itemSelectionChanged()),this,SLOT(SelectionChanged()));

	//row size:
	m_nVerticalRowSize=25;

	//set all actions to false:
	//SetEditMode(false);


	//Add column setup:
	m_lstData->destroy();


	if (bEnableForColumnSetup)
	{
		m_lstData->addColumn(QVariant::String,"BOGW_HEADER_TEXT");
		m_lstData->addColumn(QVariant::String,"BOGW_COLUMN_NAME");
		m_lstData->addColumn(QVariant::Int,"BOGW_SORT_ORDER");  //0 - asc, 1 - desc
		m_lstData->addColumn(QVariant::Bool,"SORT_ASC");
		m_lstData->addColumn(QVariant::Bool,"SORT_DESC");
		m_lstData->addColumn(QVariant::Int,"BOGW_COLUMN_TYPE");  
		m_lstData->addColumn(QVariant::Int,"BOGW_COLUMN_POSITION");
		m_lstData->addColumn(QVariant::Int,"BOGW_EDITABLE");
		m_lstData->addColumn(QVariant::String,"BOGW_DATASOURCE");
		m_lstData->addColumn(QVariant::Int,"BOGW_COLUMN_SIZE");
		m_lstData->addColumn(QVariant::Int,"BOGW_REPORT_WIDTH");
	}
	else
	{
		m_lstData->addColumn(QVariant::String,"BOGW_HEADER_TEXT");
		m_lstData->addColumn(QVariant::String,"BOGW_COLUMN_NAME");
		m_lstData->addColumn(QVariant::Int,"BOGW_SORT_ORDER");  //0 - asc, 1 - desc
		m_lstData->addColumn(QVariant::Bool,"SORT_ASC");
		m_lstData->addColumn(QVariant::Bool,"SORT_DESC");
	}
	

	//define header:
	m_lstColumnSetup.clear();
	AddColumnToSetup(m_lstColumnSetup,"BOGW_HEADER_TEXT",tr("Column Name"),COL_TYPE_TEXT,"",false,150);
	AddColumnToSetup(m_lstColumnSetup,"SORT_ASC",tr("Ascending"),COL_TYPE_CHECKBOX);
	AddColumnToSetup(m_lstColumnSetup,"SORT_DESC",tr("Descending"),COL_TYPE_CHECKBOX);
	if (bEnableForColumnSetup)
	{
		AddColumnToSetup(m_lstColumnSetup,"BOGW_COLUMN_SIZE",tr("Screen Width (px)"),COL_TYPE_TEXT,"",true,125);
		AddColumnToSetup(m_lstColumnSetup,"BOGW_REPORT_WIDTH",tr("Report Width (mm)"),COL_TYPE_TEXT,"",true,135);
	}


	//m_lstColumnSetup.Dump();

	//create mapping:n
	m_lstColumnMapping.clear();
	//map columns from datasource
	int nSize=m_lstColumnSetup.getRowCount();
	int nColPos;
	for( int i=0;i<nSize;++i)
	{
		nColPos=m_lstData->getColumnIdx(m_lstColumnSetup.getDataRef(i,1).toString());
		Q_ASSERT(nColPos!=-1); //column defined in setup, but not exists in recordset
		m_lstColumnMapping[i]=nColPos; //maps (column displayed=column in list)
	}
	
	//create header:
	RestoreHeaderSetup();
	RefreshDisplay();
}

void MultiSortTable::InitializeForCommGrid(DbRecordSet *lstData,bool bEnableForColumnSetup/*=false*/)
{
	DbRecordSet tmp = *lstData;
	Initialize(lstData);
	DropHandler(QModelIndex(), MULTICOLUMNSORT, tmp, NULL);
}


/*!
	Based on datasource, refreshes contents of displayed items.
	Call this when data is changed (add, indert, delete, select/deselect)
	If only selection is change, call reshresh selection (clears current, sets new one)
	Column count must remain!
*/
void MultiSortTable::RefreshDisplay()
{



	//columns must match
	int nRowSize=m_lstData->getRowCount();
	int nColSize=columnCount();
	int nOldRowCount=rowCount();


	//disable signals
	disconnect(this,SIGNAL(cellChanged(int,int)),this,SLOT(CellEdited(int,int)));
	disconnect(this,SIGNAL(itemSelectionChanged()),this,SLOT(SelectionChanged()));


	//rows doesn't match remove old rows, no matter what rows, count is relevant 
	while(nRowSize<rowCount())
		removeRow(rowCount()-1); //TOFIX: can be optimized

	//clear selection on table view:
	QTableWidgetSelectionRange range(0,0,rowCount()-1,nColSize-1);  
	setRangeSelected(range,false);


	//set new row count
	setRowCount(nRowSize);

	for( int nRow=0;nRow<nRowSize;++nRow)
		RefreshRowContent(nRow);

	//enable signals
	connect(this,SIGNAL(cellChanged(int,int)),this,SLOT(CellEdited(int,int)));
	connect(this,SIGNAL(itemSelectionChanged()),this,SLOT(SelectionChanged()));
	
}




/*!
	Inserts row widget items at given position if not exists.
	If row exists updates data from datasource
	Note: 
	- setRowCount() must be set, before calling this function
	- disable signal for CELL edit & CELL SELECTED! before

	\param nRow	- row position

*/
void MultiSortTable::RefreshRowContent(int nRow)
{


	//syncronize: SORT_ORDER =0 ASC=true, other false
	if(m_lstData->getDataRef(nRow,2).toInt()==0)
	{
		m_lstData->setData(nRow,3,true);
		m_lstData->setData(nRow,4,false);
	}
	else
	{
		m_lstData->setData(nRow,3,false);
		m_lstData->setData(nRow,4,true);
	}

	//creates widget of specific type for each column
	int nSize=m_lstColumnSetup.getRowCount();
	bool bIsNewRow;
	for( int i=0;i<nSize;++i)
	{

		//Add new one if empty
		bIsNewRow=false;
		int nColType=m_lstColumnSetup.getDataRef(i,2).toInt();
		bool bEditable=m_lstColumnSetup.getDataRef(i,5).toInt();
		bool bIsCheckBox=COL_TYPE_CHECKBOX==m_lstColumnSetup.getDataRef(i,2).toInt();

			if(item(nRow,i)==NULL)	
			{

				bIsNewRow=true;
				QTableWidgetItem *newItem = new QTableWidgetItem(1000+m_lstColumnSetup.getDataRef(i,2).toInt());
				setItem(nRow, i, newItem);

				if(m_bEdit && bEditable)
					if(!bIsCheckBox)
						item(nRow,i)->setFlags(  Qt::ItemIsUserCheckable | Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled |  Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsEditable);
					else
						item(nRow,i)->setFlags(  Qt::ItemIsUserCheckable | Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled |  Qt::ItemIsSelectable | Qt::ItemIsEnabled);
				else //non EDIT:
					item(nRow,i)->setFlags(  Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled | Qt::ItemIsSelectable | Qt::ItemIsEnabled); //Qt::ItemIsEnabled
			}


			//refresh data:
			if(!bIsCheckBox)
				item(nRow,i)->setData(Qt::DisplayRole,m_lstData->getDataRef(nRow,m_lstColumnMapping[i]));
			else
				item(nRow,i)->setCheckState(m_lstData->getDataRef(nRow,m_lstColumnMapping[i]).toBool() ? Qt::Checked : Qt::Unchecked); 


	


		//if new row detected: resize to default vertical size:
		if(bIsNewRow)this->verticalHeader()->resizeSection(nRow,m_nVerticalRowSize);

	

		//refresh selection:
		//if not selection & row selected
		if ( m_lstData->isRowSelected(nRow))
		{
			QTableWidgetSelectionRange range(nRow,0,nRow,nSize-1);
			setRangeSelected(range,true);
		}
	

	}

}



/*!
	Based on datasource, refreshes contents of displayed items in given row.
	Faster if data is changed only in one row.

	\param nRow	- row
*/
void MultiSortTable::RefreshDisplayRow(int nRow)
{
	//disable signals
	disconnect(this,SIGNAL(cellChanged(int,int)),this,SLOT(CellEdited(int,int)));
	disconnect(this,SIGNAL(itemSelectionChanged()),this,SLOT(SelectionChanged()));

	//copy values
	RefreshRowContent(nRow);

	//enable signals
	connect(this,SIGNAL(cellChanged(int,int)),this,SLOT(CellEdited(int,int)));
	connect(this,SIGNAL(itemSelectionChanged()),this,SLOT(SelectionChanged()));
}



/*!
	When cell is changed, data is written back to recordset
	Warning:
	- disable signals when chaning items in grid, because they cause EDITING


	\param nRow - row
	\param nCol - column

*/
void MultiSortTable::CellEdited(int nRow, int nCol)
{
	blockSignals(true);

	Q_ASSERT(item(nRow,nCol)!=NULL); //item must be created!!!
	//qDebug()<<"cell edited at x="<<nRow<<" y="<<nCol;
	
	QVariant value = item(nRow,nCol)->data(Qt::DisplayRole);
	bool bValidationOK;
	
	//if chkbox:
	if(m_lstColumnSetup.getDataRef(nCol,2).toInt()==COL_TYPE_CHECKBOX)
	{
			bool bChecked =item(nRow,nCol)->checkState()==Qt::Checked;

			if(nCol==1) //asc
			{
				m_pData->ChangeData(nRow,m_lstColumnMapping[nCol],QVariant(bChecked));
				m_pData->ChangeData(nRow,m_lstColumnMapping[nCol+1],QVariant(!bChecked));
				item(nRow,nCol+1)->setCheckState(!bChecked ? Qt::Checked : Qt::Unchecked); 
				m_pData->ChangeData(nRow,2,QVariant((int)!bChecked));
			}
			else //desc
			{
				m_pData->ChangeData(nRow,m_lstColumnMapping[nCol],QVariant(bChecked));
				m_pData->ChangeData(nRow,m_lstColumnMapping[nCol-1],QVariant(!bChecked));
				item(nRow,nCol-1)->setCheckState(!bChecked ? Qt::Checked : Qt::Unchecked); 
				m_pData->ChangeData(nRow,2,QVariant((int)bChecked));

			}

	}
	//if widget edit signals must not be accepted, as widget itself must receive them
	else if (m_lstColumnSetup.getDataRef(nCol,2).toInt()>COL_TYPE_CHECKBOX)
	{
		blockSignals(false);
		return;
	}
	else //normal operation:
	{
		//validate and store new data
		bValidationOK=m_pData->ChangeData(nRow,m_lstColumnMapping[nCol],value);

		//if validation fails: return back old value:
		if (!bValidationOK)
		{
			item(nRow,nCol)->setData(Qt::DisplayRole,m_lstData->getDataRef(nRow,m_lstColumnMapping[nCol]));
		}
	}

	blockSignals(false);

}


//--------------------------------------------------------------
//								SELECTION HANDLERS
//--------------------------------------------------------------


/*!
	When selection is changed, record set rows are selected/deselected

*/
void MultiSortTable::SelectionChanged()
{


	QList<QTableWidgetSelectionRange> lstSelections = selectedRanges();

	int nSize=lstSelections.size();

	//if CTRL+A,then select all:
	if(nSize==1)
		if (lstSelections.at(0).topRow()==0 && lstSelections.at(0).bottomRow()==m_lstData->getRowCount()-1)
		{
			m_lstData->selectAll();
			return;
		}

	m_lstData->clearSelection();
	for(int i=0;i<nSize;++i)
	{
		QTableWidgetSelectionRange range=lstSelections.at(i);
		for(int j=range.topRow();j<=range.bottomRow();++j)
		{	
			m_lstData->selectRow(j);
			//qDebug()<<"Selection in progress on row ="<<j;
			//qDebug()<< m_lstData->isRowSelected(j);
		}
	}

}





/*!
	When selected rows inside data source are changed call this function to refresh view.
	Faster then calling RefreshDisplay()

	\param bClearDataSelected - if true (default) selection from data source is also cleared
*/
void MultiSortTable::RefreshSelection()
{

	disconnect(this,SIGNAL(itemSelectionChanged()),this,SLOT(SelectionChanged()));

	//clear old selection:
	int nRowSize=m_lstData->getRowCount();
	int nColSize=columnCount();

	QTableWidgetSelectionRange range(0,0,rowCount()-1,nColSize-1);  
	setRangeSelected(range,false);


	//refresh selection:
	//if not selection & row selected
	for( int nRow=0;nRow<nRowSize;++nRow)
		if (m_lstData->isRowSelected(nRow))
		{
			QTableWidgetSelectionRange range(nRow,0,nRow,nColSize-1);  //TOFIX: can be optimized
			setRangeSelected(range,true);
		}
	
	connect(this,SIGNAL(itemSelectionChanged()),this,SLOT(SelectionChanged()));

}



/*!
	Deletes selected rows from data source & table view


*/
void MultiSortTable::DeleteSelection()
{
	//if all then all:
	if(m_lstData->getSelectedCount()==rowCount()) 
		{DeleteTable();return;}
	
	m_pData->DeleteData(0,true);
	RefreshDisplay();
}



/*!
	Removes whole content from table & from datasource


*/
void MultiSortTable::DeleteTable()
{
	m_pData->DeleteData(-1);
	RestoreHeaderSetup();
}







//--------------------------------------------------------------
//						PUBLIC SLOTS FOR TABLE ACTIONS
//--------------------------------------------------------------
/*!
	Inserts row in data source & table at last position
*/
void MultiSortTable::InsertRow()
{
	m_pData->InsertData(); //add new row

	//set new row count
	int nRowSize=m_lstData->getRowCount();
	setRowCount(nRowSize);

	RefreshDisplayRow(nRowSize-1);

}








//--------------------------------------------------------------
//								DRAG & DROP
//--------------------------------------------------------------


//if not edit mode: disable drop & drag
void MultiSortTable::dropEvent(QDropEvent *event)
{
	if(!m_bEdit){return;}
	DragDropInterface::dropEvent(event);
}
//if not edit mode: disable drop & drag
void MultiSortTable::dragEnterEvent ( QDragEnterEvent * event )
{
	if(!m_bEdit)
	{event->ignore();return;}
	DragDropInterface::dragEnterEvent(event);
}

/*!
	Returns drop type.Inherit and override this method if using drag/drop.
	
	\return  DropType from bus_core/entity_id_collection.h

*/
int MultiSortTable::GetDropType()
{
	return -1;
}



/*!
	Returns selected values in list, as drag value
	
	\param   values - selected rows

*/
void MultiSortTable::GetDropValue(DbRecordSet &values, bool bSelectItemChildren ) const
{ 
	values.copyDefinition(*m_lstData);
	values.merge(*m_lstData,true);
}



/*!
	Called from dropEvent if widget sucessfully acceppted drop, with x,y coordinates and drop data.
	By default it insert new data at end of table view (APPEND)
	Override this to implement custom behaviour

	\param   index			- position in model (can be invalid if drop ocured outside all items). Test isValid()
	\param   nDropType		- Drop Type
	\param   DroppedValue	- Dropped data
*/
void MultiSortTable::DropHandler(QModelIndex index, int nDropType, DbRecordSet &DroppedValue,QDropEvent *event)
{
	//determine coordiantes:
	int nRow,nCol;
	if(index.isValid()) //if valid:
	{
		nRow=index.row();
		nCol=index.column();
	}
	else //invalid
	{
		nRow=-1;
		nCol=-1;
	}

	//qDebug()<<"Drop occured at row:"<<nRow<<" col: "<<nCol;

	//check for duplicates:
	for(int i=0;i<DroppedValue.getRowCount();++i)
	{
		QString strColName=DroppedValue.getDataRef(i,1).toString();
		if(m_lstData->find(1,strColName,true)!=-1)
			DroppedValue.selectRow(i);
		else
			DroppedValue.selectRow(i,false);
	}

	//remove duplos if any:
	DroppedValue.deleteSelectedRows();

	m_lstData->merge(DroppedValue);
	RefreshDisplay();
}






//--------------------------------------------------------------
//								HELPERS
//--------------------------------------------------------------

/*!
	Destroy any widgets inside table, recreates horizontal header: sets header info + tooltip + later icon can be added
	
	\param lstCols		- list with column No in grid and sort order for each column in grid

*/
void MultiSortTable::RestoreHeaderSetup()
{
	//clear setup:
	clear();
	setRowCount(0);

	int nSize=m_lstColumnSetup.getRowCount();
	setColumnCount(nSize);	//sets column count

	QTableWidgetItem *newItem;

	//add columns:
	for( int i=0;i<nSize;++i)
	{
		setColumnWidth(i,m_lstColumnSetup.getDataRef(i,3).toInt()); //resize cols to given values
		newItem=horizontalHeaderItem(i);
		if(newItem==NULL) //if not created, create it:
		{
				newItem = new QTableWidgetItem();
				setHorizontalHeaderItem(i,newItem);
		}
		
		//set header + tooltip
		newItem->setText(m_lstColumnSetup.getDataRef(i,0).toString());
		newItem->setToolTip(m_lstColumnSetup.getDataRef(i,7).toString());
	}

	//resize table to header data....brb should be user-defined..
	//resizeColumnsToContents();

	this->verticalHeader()->setHighlightSections(false);
	this->horizontalHeader()->setHighlightSections(false);
}



//--------------------------------------------------------------
//								STATIC HELPERS
//--------------------------------------------------------------



/*!
	Add's column setup info to given list, if not defined, list will be defined,
	Position is determined by order of add.
	Column name  must match those in data source
	
	\param lstSetup		- returned column setup, redefined and filled 
	\param nView		- id of view from which to define table column setup

*/
void MultiSortTable::AddColumnToSetup(DbRecordSet &lstSetup,QString strColName,QString strHeaderName,int nColType,QString strDataFormat,bool bEditable,int nWidth, QString strToolTip)
{

	//define from view:
	if(lstSetup.getColumnCount()==0)lstSetup.defineFromView(DbSqlTableView::getView(DbSqlTableView::MVIEW_GRID_COLUMN_SETUP));


	//postion is defined by order of adding:
	int nPosition=lstSetup.getRowCount();


	//add column setup:
	lstSetup.addRow();
	lstSetup.setData(nPosition,0,strHeaderName);
	lstSetup.setData(nPosition,1,strColName);
	lstSetup.setData(nPosition,2,nColType);
	lstSetup.setData(nPosition,3,nWidth);
	lstSetup.setData(nPosition,4,nPosition);
	lstSetup.setData(nPosition,5,bEditable);
	lstSetup.setData(nPosition,6,0);			//sort order: default=asc
	lstSetup.setData(nPosition,7,strToolTip);
	lstSetup.setData(nPosition,8,strDataFormat);
}




/*!
	Moves selected row one place up, if first, do nothing.

*/
void MultiSortTable::MoveRowUp()
{

	bool bSelectedFound=false;

	//find selected row:
	int i;
	for(i=0;i<m_lstData->getRowCount();++i)
	{
		if(m_lstData->isRowSelected(i)){bSelectedFound=true; break;}
	}

	if (!bSelectedFound) return; //if no selection
	if(i==0) return; //if first

	//extract row value
	DbRecordSet Row=m_lstData->getRow(i);
	
	//remove from list
	m_lstData->deleteSelectedRows();

	//insert one position up
	m_lstData->insertRow(i-1);
	m_lstData->assignRow(i-1,Row);

	//select it
	m_lstData->selectRow(i-1);

	//redraw:
	RefreshDisplay();

}


/*!
	Moves selected row one place down, if last, do nothing.

*/
void MultiSortTable::MoveRowDown()
{

	bool bSelectedFound=false;

	//find selected row:
	int i;
	for(i=0;i<m_lstData->getRowCount();++i)
	{
		if(m_lstData->isRowSelected(i)){bSelectedFound=true; break;}
	}

	if (!bSelectedFound) return; //if no selection
	if(i==m_lstData->getRowCount()-1) return; //if first

	//extract row value
	DbRecordSet Row=m_lstData->getRow(i);
	
	//remove from list
	m_lstData->deleteSelectedRows();

	//insert one position up
	m_lstData->insertRow(i+1);
	m_lstData->assignRow(i+1,Row);

	//select it
	m_lstData->selectRow(i+1);

	//redraw:
	RefreshDisplay();

}


/*!
	Gets list for sorting in format for multicolumn sort algorithm for recordset
	:Format of list is hasher [Col_Name]=sortorder
*/
void MultiSortTable::GetListForSorting(QHash<QString,int> &lstSortingCols)
{
	lstSortingCols.clear();
	for(int i=0;i<m_lstData->getRowCount();++i)
		lstSortingCols[m_lstData->getDataRef(i,1).toString()]=m_lstData->getDataRef(i,2).toInt();
}


//FOR COLUM SETUP:

void MultiSortTable::GetColumnSetupList(DbRecordSet &lstSortingCols)
{
	lstSortingCols=*m_lstData;

}


void MultiSortTable::SetColumnSetupList(DbRecordSet &lstSortingCols)
{
	m_lstData->clear();
	m_lstData->merge(lstSortingCols);
	RefreshDisplay();
}





/*!
Table goes into edit mode (off by deafult)
Warning: when exiting edit mode, clears selection

\param bEdit - true, set edit mode
*/
void MultiSortTable::SetEditMode(bool bEdit)
{
	m_pActionDel->setEnabled(bEdit);

	//BROKE SIGNAL, because this triggers signal
	blockSignals(true);

	//clear selection when exiting edit mode
	if(m_bEdit && !bEdit)
		m_lstData->clearSelection();

	m_bEdit=bEdit;
	int nRowSize=rowCount();
	int nColSize=columnCount();
	for(int nRow=0;nRow<nRowSize;++nRow)
		for(int nCol=0;nCol<nColSize;++nCol)
		{	

			//if edit and if column is editable, then edit
			int nColType=m_lstColumnSetup.getDataRef(nCol,2).toInt();
			bool bEditable=m_lstColumnSetup.getDataRef(nCol,5).toInt();
			bool bIsCheckBox=COL_TYPE_CHECKBOX==m_lstColumnSetup.getDataRef(nCol,2).toInt();

			if(nColType<COL_TYPE_COMBOBOX)
			{
				Q_ASSERT(item(nRow,nCol)!=NULL); //item must be created!!!

				if(m_bEdit && bEditable)
					if(!bIsCheckBox)
						item(nRow,nCol)->setFlags(  Qt::ItemIsUserCheckable | Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled |  Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsEditable);
					else
						item(nRow,nCol)->setFlags(  Qt::ItemIsUserCheckable | Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled |  Qt::ItemIsSelectable | Qt::ItemIsEnabled);
				else //non EDIT:
					item(nRow,nCol)->setFlags(  Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled | Qt::ItemIsSelectable | Qt::ItemIsEnabled); //Qt::ItemIsEnabled
			}
			else
			{
				//TableCellWidget *cellWidgetUniversal=dynamic_cast<TableCellWidget*>(cellWidget(nRow,nCol));
				//Q_ASSERT_X(cellWidgetUniversal!=NULL,"TABLE WIDGET","CELL WIDGET NOT SUBCLASSED FROM TableCellWidget");
				//cellWidgetUniversal->SetEditMode(m_bEdit);
			}

		}

	blockSignals(false);
}
