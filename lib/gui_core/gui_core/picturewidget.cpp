#include "picturewidget.h"
#include <QPainter>
#include <QImageWriter>
#include <QHBoxLayout>
#include <QToolButton>
#include <QMenu>
#include <QUrl>
#include <QMimeData>
#include "gui_core/gui_core/dlg_picture.h"
#include "os_specific/os_specific/printmanager.h"
#include "picturehelper.h"
#include "trans/trans/httpreadersync.h"
#include "gui_core/gui_core/logindlg.h"
#include "common/common/datahelper.h"


PictureWidget::PictureWidget(QWidget *parent)
	: QFrame(parent),m_nMode(true),m_pfCallBackLoadFullPicture(NULL)
{
	m_pLabel=NULL;
	m_pLstData=NULL;
	m_strCurrentFormat="jpg";		//when saving pic, we dont know what format, so use jpg as default if not specified
	m_bPictureChanged=false;		//flag only track from Initialize to first change...
	m_bExternalDataManipulator=false;
	m_bRefreshInProgrres=false;

	setAcceptDrops(true);
}

PictureWidget::~PictureWidget()
{
	if (m_bExternalDataManipulator && m_pLstData!=NULL)
	{
		delete m_pLstData;
	}

}



/*!
	Creates context menu actions
*/
void PictureWidget::Initialize(int  nMode,GuiDataManipulator *pLstData, QString strPreviewColIdx,QString strFullColIdx,QString strPictureIdIdx,QString strEntityIdIdx)
{

	CreateActions();
	m_nMode=nMode;
	SetUpWidget();
	RefreshPictureState();

	//store data source:
	m_pLstData=pLstData;
	if(m_pLstData!=NULL)
	{
		m_nPreviewColIdx=m_pLstData->GetDataSource()->getColumnIdx(strPreviewColIdx);
		m_nFullColIdx=m_pLstData->GetDataSource()->getColumnIdx(strFullColIdx);
		m_strEntityRecordID=strEntityIdIdx;
		m_strPictureID=strPictureIdIdx;
		Q_ASSERT(m_nPreviewColIdx!=-1);
		Q_ASSERT(m_nFullColIdx!=-1);
		Q_ASSERT(!m_strEntityRecordID.isEmpty());
		Q_ASSERT(!m_strPictureID.isEmpty());
	}

	//connect(this,SIGNAL(PictureChanged()),this,SLOT(ChangeDataSource()));
}


void PictureWidget::Initialize(int nMode,DbRecordSet *pLstData, QString strPreviewColIdx,QString strFullColIdx,QString strPictureIdIdx,QString strEntityIdIdx)
{
	//m_pLstData= new GuiDataManipulator(pLstData);
	m_bExternalDataManipulator=true;
	Initialize(nMode,new GuiDataManipulator(pLstData),strPreviewColIdx,strFullColIdx,strPictureIdIdx,strEntityIdIdx);
}

//when picture is changed, save picture into datasource if exists: 
bool PictureWidget::ChangeDataSource(QPixmap *pixmap)
{
	if(m_pLstData==NULL) return false; //exist if NULL
	if(m_pLstData->GetDataSource()->getRowCount()==0)  return false; //if no rows exit
	if(!m_bEdit) return false;

	//if changed, convert small preview, save big & preview, refresh
	QByteArray bytePicture,bytePreview;
	if(pixmap==NULL)
	{
		bytePicture=QByteArray();
		bytePreview=QByteArray();
	}
	else
	{
		PictureHelper::ConvertPixMapToByteArray(bytePicture,*pixmap,m_strCurrentFormat);
		PictureHelper::ResizePixMapToPreview(*pixmap);
		PictureHelper::ConvertPixMapToByteArray(bytePreview,*pixmap,m_strCurrentFormat);
	}

	//save data:
	m_pLstData->ChangeData(0,m_nFullColIdx,bytePicture);
	m_pLstData->ChangeData(0,m_nPreviewColIdx,bytePreview);


	return true;
}



//from data copy to display: (from row 0)
void  PictureWidget::RefreshDisplay()
{
	if (m_pLstData==NULL)return;


	m_bRefreshInProgrres=true;
	this->blockSignals(true); //dont reemit back

	//IF PREVIEW -> SHOW PREVIEW -> ELSE SHOW FULL
	//check if current row is out of bound, if so clear content;
	if(m_pLstData->GetDataSource()->getRowCount()!=0)  
	{
		//loads directly from bytearray preserving format
		QByteArray data;
		if(m_nMode==MODE_PREVIEW || m_nMode==MODE_PREVIEW_WITH_BUTTONS)
			data = m_pLstData->GetDataSource()->getDataRef(0,m_nPreviewColIdx).toByteArray();
		else
			data = m_pLstData->GetDataSource()->getDataRef(0,m_nFullColIdx).toByteArray();
		SetPictureFromBinaryData(data);
	}
	else
		ClearPixmap();

	this->blockSignals(false);
	m_bRefreshInProgrres=false;
	ChangePictureToolTip();
}





QLabel *PictureWidget::GetLabel()
{
	Q_ASSERT(m_pLabel!=NULL);
	return m_pLabel;
}


//when new pixmap is loaded, call this...
void PictureWidget::RefreshPictureState()
{
	Q_ASSERT(m_pLabel!=NULL);
	scaleFactor = 1.0;
	updateActions();
	if (!m_pActFitWindow->isChecked())
		m_pLabel->adjustSize();
	
	ChangePictureToolTip();

}


void PictureWidget::SetPixmap(QPixmap& pic)
{
	Q_ASSERT(m_pLabel!=NULL);
	ChangePixMap(&pic);
}


void PictureWidget::GetPictureBinaryDataPreview(QByteArray &bytePicture)
{
	Q_ASSERT(m_pLabel!=NULL);

	if(m_pLabel->pixmap()==NULL)
	{
		bytePicture=QByteArray();
		return; //if pic is empty
	}

	QPixmap pixmap=GetPixmap();
	PictureHelper::ResizePixMapToPreview(pixmap);
	PictureHelper::ConvertPixMapToByteArray(bytePicture,pixmap,m_strCurrentFormat);
}


void PictureWidget::GetPictureBinaryData(QByteArray &bytePicture)
{
	Q_ASSERT(m_pLabel!=NULL);

	if(m_pLabel->pixmap()==NULL)
	{
		bytePicture=QByteArray();
		return; //if pic is empty
	}

	QPixmap pixmap=GetPixmap();
	PictureHelper::ConvertPixMapToByteArray(bytePicture,pixmap,m_strCurrentFormat);
}

void PictureWidget::SetPictureFromBinaryData(QByteArray &bytePicture)
{
	Q_ASSERT(m_pLabel!=NULL);

	if(bytePicture.size()==0) 
	{
		ErasePicture();
		return; 
	}

	QPixmap pixMap;
	pixMap.loadFromData(bytePicture);
	ChangePixMap(&pixMap);
}



/*!
	Slot for opening context menu:
*/
void PictureWidget::contextMenuEvent(QContextMenuEvent *event)
{

	//no cntx menu:
	if(m_nMode==MODE_PREVIEW)return;

	QMenu CnxtMenu(this);
	QMenu CnxtMenuView(this);
	QAction *pView;


	if(m_nMode==MODE_PREVIEW_WITH_BUTTONS)
	{
		if(!m_bEdit) //if not edit only Open
		{
			CnxtMenu.addAction(m_pNewWindow);
		}
		else // if EDIT then load /paste
		{
			CnxtMenu.addAction(m_pActLoad);
			CnxtMenu.addAction(m_pActPastFromClipboard);
			CnxtMenu.addAction(m_SeparatorAct);
			CnxtMenu.addAction(m_pNewWindow);
		}
	}
	else
	{
		//view menu
		CnxtMenuView.addAction(m_pActZoomIn);
		CnxtMenuView.addAction(m_pActZoomOut);
		CnxtMenuView.addAction(m_pActNormal);
		CnxtMenuView.addAction(m_SeparatorAct);
		CnxtMenuView.addAction(m_pActFitWindow);

		if(!m_bEdit) //if not edit
		{
			//main menu
			CnxtMenu.addAction(m_pActSave);
			CnxtMenu.addAction(m_pActPrint);
			CnxtMenu.addAction(m_SeparatorAct);
			CnxtMenu.addAction(m_pActCopyToClipboard);
			CnxtMenu.addAction(m_SeparatorAct);
			pView=CnxtMenu.addMenu(&CnxtMenuView);//,QObject::tr("View"));
		}
		else
		{
			//main menu
			CnxtMenu.addAction(m_pActLoad);
			CnxtMenu.addAction(m_pActSave);
			CnxtMenu.addAction(m_pActPrint);
			CnxtMenu.addAction(m_SeparatorAct);
			CnxtMenu.addAction(m_pActCopyToClipboard);
			CnxtMenu.addAction(m_pActPastFromClipboard);
			CnxtMenu.addAction(m_SeparatorAct);
			pView=CnxtMenu.addMenu(&CnxtMenuView);//QObject::tr("View"));
		}

		if(m_nMode!=MODE_FULL_DIALOG)
		{
			CnxtMenu.addAction(m_SeparatorAct);
			CnxtMenu.addAction(m_pNewWindow);
		}

		pView->setText(QObject::tr("View"));

	}


	CnxtMenu.exec(event->globalPos());

}






/*!
	Set picture from file

*/
void PictureWidget::LoadPictureFromFile()
{
	Q_ASSERT(m_pLabel!=NULL);

	QPixmap pic;
	bool bOK=PictureHelper::LoadPicture(pic,m_strCurrentFormat); //store picture format as no Qt control stores this
	if(bOK)
	{ 
		ChangePixMap(&pic);
		m_bPictureChanged=true;
	}
}



/*!
	Set picture from file
*/
void PictureWidget::SavePictureToFile()
{
	Q_ASSERT(m_pLabel!=NULL);
	QPixmap pix = GetPixmap();
	PictureHelper::SavePicture(pix);
}



/*!
	copy to clip
*/
void PictureWidget::CopyPictureToClipboard()
{
	Q_ASSERT(m_pLabel!=NULL);
	QPixmap pix = GetPixmap();
	PictureHelper::CopyPictureToClipboard(pix);
}


/*!
	Set picture from clipboard
*/
void PictureWidget::PastePictureFromClipboard()
{

	Q_ASSERT(m_pLabel!=NULL);
	QPixmap pic;
	bool bOK=PictureHelper::PastePictureFromClipboard(pic);
	if(bOK)
	{
		m_strCurrentFormat="jpg"; //reset to default format
		ChangePixMap(&pic);
		m_bPictureChanged=true;
	}

}


void PictureWidget::ErasePicture()
{
	Q_ASSERT(m_pLabel!=NULL);
	m_strCurrentFormat="jpg"; //reset to default format
	ChangePixMap(NULL);
	m_bPictureChanged=true;

}


//show pic in dialog:
void PictureWidget::ShowPictureInDialog()
{
	//MB request: it is slow...
	QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));

	Dlg_Picture dlg;
	if(m_pLstData==NULL){
		QPixmap pix = GetPixmap();
		dlg.SetPixMap(pix);
	}
	else if (m_pLstData->GetDataSource()->getRowCount()!=0)
	{	
		//set picture from full version, but first reload if needed:
		int nRow=0;
		Status err;
		DbRecordSet rowPicture;
		bool bBigPictureLoaded=!m_pLstData->GetDataSource()->getDataRef(nRow,"BPIC_PICTURE").isNull();
		int nRecordID=m_pLstData->GetDataSource()->getDataRef(nRow,m_strEntityRecordID).toInt();
		if(!bBigPictureLoaded && nRecordID!=0) //if flag is false and ID is valid, load from server
		{
			Status err;
			int nBigPicId=m_pLstData->GetDataSource()->getDataRef(nRow,m_strPictureID).toInt();
			if (nBigPicId>0)
			{
				if (m_pfCallBackLoadFullPicture==NULL)
				{
					Q_ASSERT(false); //no call back function for loading picture defined
					return;
				}
				if(!m_pfCallBackLoadFullPicture(nBigPicId,rowPicture))
					return;
				m_pLstData->GetDataSource()->setData(nRow,"BPIC_PICTURE",rowPicture.getDataRef(0,"BPIC_PICTURE")); //save picture
			}
			else
			{
				//if empty then use preview pic:
				m_pLstData->GetDataSource()->setData(nRow,"BPIC_PICTURE",m_pLstData->GetDataSource()->getDataRef(0,m_nPreviewColIdx)); //set as preview
			}
		}
		QByteArray data = m_pLstData->GetDataSource()->getDataRef(0,m_nFullColIdx).toByteArray();
		dlg.SetPictureBinaryData(data);
		//else
		//	return;
	}
	
	//MB request: it is slow...
	QApplication::restoreOverrideCursor();


	dlg.SetEditMode(m_bEdit);
	int nRes=dlg.exec();
	if(nRes)
	{
		//save result:
		m_strCurrentFormat=dlg.GetCurrentPictureFormat();
		QPixmap pic=dlg.GetPixMap();
		ChangePixMap(&pic);
	}
	ChangePictureToolTip();

	
}



/*!
	Enable /disable context menu
*/
void PictureWidget::SetEditMode(bool bEdit)
{
	Q_ASSERT(m_pLabel!=NULL);
	m_bEdit=bEdit;

	//m_pLabel->pixmap()==NULL;
	if(!m_bEdit)
	{
		m_pActLoad->setEnabled(false);
		m_pActPastFromClipboard->setEnabled(false);
		m_pActErase->setEnabled(false);
	}
	else
	{
		m_pActLoad->setEnabled(true);
		m_pActPastFromClipboard->setEnabled(true);
		m_pActErase->setEnabled(true);
	}

}




void PictureWidget::CreateActions()
{

	//Add separator
	m_SeparatorAct = new QAction(this);
	m_SeparatorAct->setSeparator(true);


	//FILE LOAD
	m_pActLoad = new QAction(tr("&Load Picture From File"), this);
	m_pActLoad->setIcon(QIcon(":openfile.png"));
	connect(m_pActLoad, SIGNAL(triggered()), this, SLOT(LoadPictureFromFile()));

	//FILE SAVE
	m_pActSave = new QAction(tr("&Save Picture To File"), this);
	m_pActSave->setIcon(QIcon(":filesave.png"));
	connect(m_pActSave, SIGNAL(triggered()), this, SLOT(SavePictureToFile()));

	//FILE ERASE
	m_pActErase = new QAction(tr("&Erase Picture"), this);
	m_pActErase->setIcon(QIcon(":trash-16.png"));
	connect(m_pActErase, SIGNAL(triggered()), this, SLOT(ErasePicture()));

	//FILE PRINT
	m_pActPrint = new QAction(tr("&Print"), this);
	m_pActPrint->setIcon(QIcon(":print2.png"));
	connect(m_pActPrint, SIGNAL(triggered()), this, SLOT(print()));

	//CLIPBOARD:
	m_pActPastFromClipboard = new QAction(tr("Paste Picture From Clipboard"), this);
	m_pActPastFromClipboard->setShortcut(tr("CTRL+V"));
	m_pActPastFromClipboard->setShortcutContext(Qt::WidgetShortcut); //do not comment this!
	m_pActPastFromClipboard->setIcon(QIcon(":paste.png"));
	connect(m_pActPastFromClipboard, SIGNAL(triggered()), this, SLOT(PastePictureFromClipboard()));
	this->addAction(m_pActPastFromClipboard);
	

	//PASTE:
	m_pActCopyToClipboard = new QAction(tr("Copy Picture To Clipboard"), this);
	m_pActCopyToClipboard->setShortcut(tr("CTRL+C"));
	m_pActCopyToClipboard->setIcon(QIcon(":copy.png"));
	connect(m_pActCopyToClipboard, SIGNAL(triggered()), this, SLOT(CopyPictureToClipboard()));

	//ZOOM:
	m_pActZoomIn = new QAction(tr("Zoom &In (25%)"), this);
	m_pActZoomIn->setShortcut(tr("Ctrl++"));
	//m_pActZoomIn->setEnabled(false);
	m_pActZoomIn->setIcon(QIcon(":zoom_plus.png"));
	connect(m_pActZoomIn, SIGNAL(triggered()), this, SLOT(zoomIn()));

	m_pActZoomOut = new QAction(tr("Zoom &Out (25%)"), this);
	m_pActZoomOut->setShortcut(tr("Ctrl+-"));
	//m_pActZoomOut->setEnabled(false);
	m_pActZoomOut->setIcon(QIcon(":zoom_minus.png"));
	connect(m_pActZoomOut, SIGNAL(triggered()), this, SLOT(zoomOut()));

	m_pActNormal = new QAction(tr("&Original Size"), this);
	m_pActNormal->setShortcut(tr("Ctrl+S"));
	//m_pActNormal->setEnabled(false);
	m_pActNormal->setIcon(QIcon(":recycle.png"));
	connect(m_pActNormal, SIGNAL(triggered()), this, SLOT(normalSize()));

	m_pActFitWindow = new QAction(tr("&Resize to Window Width"), this);
	//m_pActFitWindow->setEnabled(false);
	m_pActFitWindow->setCheckable(true);
	m_pActFitWindow->setShortcut(tr("Ctrl+F"));
	m_pActFitWindow->setIcon(QIcon(":adjustsize.png"));
	connect(m_pActFitWindow, SIGNAL(triggered()), this, SLOT(fitToWindow()));

	//OPEN NEW
	m_pNewWindow = new QAction(tr("&Open picture in new window"), this);
	m_pNewWindow->setIcon(QIcon(":brush.png"));
	connect(m_pNewWindow, SIGNAL(triggered()), this, SLOT(ShowPictureInDialog()));

}



/*!
    SetUpWidget based on mode: with toolbar & scrollbars  OR just label
*/
void PictureWidget::SetUpWidget()
{
	//LAYOUT:
	m_pMainLayout = new QVBoxLayout;


	if(m_nMode==MODE_FULL || m_nMode==MODE_FULL_DIALOG)
	{
		
		QSize buttonSize(24, 24);

		//CREATE BUTTONS/SIZE
		QToolButton *pBtnSave			= new QToolButton;
		QToolButton *pBtnLoad			= new QToolButton;
		QToolButton *pBtnPrint			= new QToolButton;
		QToolButton *pBtnCopy			= new QToolButton;
		QToolButton *pBtnPaste			= new QToolButton;
		QToolButton *pBtnErase			= new QToolButton;
		QToolButton *pBtnZoomIn			= new QToolButton;
		QToolButton *pBtnZoomOut		= new QToolButton;
		QToolButton *pBtnFitTowWindow	= new QToolButton;
		QToolButton *pBtnOriginalSize	= new QToolButton;
		//size
		pBtnSave		->setMaximumSize(buttonSize);
		pBtnLoad		->setMaximumSize(buttonSize);
		pBtnPrint		->setMaximumSize(buttonSize);
		pBtnCopy		->setMaximumSize(buttonSize);
		pBtnPaste		->setMaximumSize(buttonSize);
		pBtnErase		->setMaximumSize(buttonSize);
		pBtnZoomIn		->setMaximumSize(buttonSize);
		pBtnZoomOut		->setMaximumSize(buttonSize);
		pBtnFitTowWindow->setMaximumSize(buttonSize);
		pBtnOriginalSize->setMaximumSize(buttonSize);

		//set auto raise
		pBtnSave->			setAutoRaise(true);
		pBtnLoad->			setAutoRaise(true);
		pBtnPrint->			setAutoRaise(true);
		pBtnErase->			setAutoRaise(true);
		pBtnCopy->			setAutoRaise(true);
		pBtnPaste->			setAutoRaise(true);
		pBtnZoomIn->		setAutoRaise(true);
		pBtnZoomOut->		setAutoRaise(true);
		pBtnFitTowWindow->	setAutoRaise(true);
		pBtnOriginalSize->	setAutoRaise(true);

		//ACTIONS:
		pBtnSave->			setDefaultAction(m_pActSave);
		pBtnLoad->			setDefaultAction(m_pActLoad);
		pBtnPrint->			setDefaultAction(m_pActPrint);
		pBtnErase->			setDefaultAction(m_pActErase);
		pBtnCopy->			setDefaultAction(m_pActCopyToClipboard);
		pBtnPaste->			setDefaultAction(m_pActPastFromClipboard);
		pBtnZoomIn->		setDefaultAction(m_pActZoomIn);
		pBtnZoomOut->		setDefaultAction(m_pActZoomOut);
		pBtnFitTowWindow->	setDefaultAction(m_pActFitWindow);
		pBtnOriginalSize->	setDefaultAction(m_pActNormal);


		//Tool bar.
		QHBoxLayout *buttonLayout = new QHBoxLayout;

		buttonLayout->setSpacing(2);
		buttonLayout->setAlignment(Qt::AlignLeft);
		buttonLayout->addWidget(pBtnLoad);
		buttonLayout->addWidget(pBtnSave);
		buttonLayout->addWidget(pBtnErase);
		buttonLayout->addWidget(pBtnPrint);
		buttonLayout->addSpacing(8);
		buttonLayout->addWidget(pBtnCopy);
		buttonLayout->addWidget(pBtnPaste);
		buttonLayout->addSpacing(8);
		buttonLayout->addWidget(pBtnZoomIn);
		buttonLayout->addWidget(pBtnZoomOut);
		buttonLayout->addWidget(pBtnOriginalSize);
		buttonLayout->addSpacing(8);
		buttonLayout->addWidget(pBtnFitTowWindow);

		buttonLayout->addStretch(7);


		//add special open dialog button
		if(m_nMode==MODE_FULL)
		{
				QToolButton *pBtnNewWin	= new QToolButton;
				pBtnNewWin->			setMaximumSize(buttonSize);
				pBtnNewWin->			setAutoRaise(true);
				pBtnNewWin->			setDefaultAction(m_pNewWindow);
			
				buttonLayout->addSpacing(8);
				buttonLayout->addWidget(pBtnNewWin);
		}

		m_pMainLayout->addLayout(buttonLayout);

		//only if not preview
		

	}
	else if(m_nMode==MODE_PREVIEW_WITH_BUTTONS)
	{

		QSize buttonSize(24, 24);

		//CREATE BUTTONS/SIZE
		QToolButton *pBtnLoad			= new QToolButton;
		QToolButton *pBtnPaste			= new QToolButton;
		//size
		pBtnLoad		->setMaximumSize(buttonSize);
		pBtnPaste		->setMaximumSize(buttonSize);
		//set auto raise
		pBtnLoad->			setAutoRaise(true);
		pBtnPaste->			setAutoRaise(true);
		//ACTIONS:
		pBtnLoad->			setDefaultAction(m_pActLoad);
		pBtnPaste->			setDefaultAction(m_pActPastFromClipboard);

		//Tool bar.
		QHBoxLayout *buttonLayout = new QHBoxLayout;

		buttonLayout->setSpacing(2);
		buttonLayout->setAlignment(Qt::AlignLeft);
		buttonLayout->addWidget(pBtnLoad);
		buttonLayout->addWidget(pBtnPaste);
		buttonLayout->addStretch(7);

		QToolButton *pBtnNewWin	= new QToolButton;
		pBtnNewWin->			setMaximumSize(buttonSize);
		pBtnNewWin->			setAutoRaise(true);
		pBtnNewWin->			setDefaultAction(m_pNewWindow);

		buttonLayout->addSpacing(8);
		buttonLayout->addWidget(pBtnNewWin);

		m_pMainLayout->addLayout(buttonLayout);

	}

	
	//LABEL + SCROLL AREA
	m_pLabel= new QLabel;
	//m_pLabel->setFrameStyle(QFrame::Box);
	//m_pLabel->setFrameShadow(QFrame::Raised);
	m_scrollArea = new QScrollArea;
	m_scrollArea->setBackgroundRole(QPalette::Dark);
	m_scrollArea->setWidget(m_pLabel);


						
	//bind CTRL+C and CTRL+V to label??
	
	m_pMainLayout->addWidget(m_scrollArea,10);
	setLayout(m_pMainLayout);

	//no offset, no cntx, no scrolls
	if(m_nMode==MODE_PREVIEW || m_nMode==MODE_PREVIEW_WITH_BUTTONS)
	{
		m_pMainLayout->setMargin(0);
		m_pMainLayout->setSpacing(0);
		m_scrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
		m_scrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	}
	else
	{
		m_pLabel->setScaledContents(true);	
		m_pMainLayout->setMargin(3);
		m_pMainLayout->setSpacing(2);
	}
}




void PictureWidget::print()
{
	if(m_pLabel->pixmap()==NULL) return; //if pic is empty
	PrintManager::PrintPixMap(m_pLabel->pixmap());
}

void PictureWidget::zoomIn()
{
	Q_ASSERT(m_pLabel!=NULL);
	scaleImage(1.25);
}

void PictureWidget::zoomOut()
{
	Q_ASSERT(m_pLabel!=NULL);
	scaleImage(0.8);
}

void PictureWidget::normalSize()
{
	Q_ASSERT(m_pLabel!=NULL);
	m_pLabel->adjustSize();
	scaleFactor = 1.0;
	m_pActZoomIn->setEnabled(scaleFactor < 3.0);
	m_pActZoomOut->setEnabled(scaleFactor > 0.333);

}

void PictureWidget::fitToWindow()
{
	Q_ASSERT(m_pLabel!=NULL);
	bool fitToWindow = m_pActFitWindow->isChecked();
	//m_scrollArea->setWidgetResizable(fitToWindow);
	if (!fitToWindow) 
	{
		//m_scrollArea->setWidgetResizable(false);
		m_scrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
		m_scrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
		normalSize();
	}
	else
	{
		if(m_pLabel->pixmap()!=NULL) 
		{
			QSize size=m_scrollArea->size();
			m_scrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
			m_scrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

			QPixmap tmp=m_pLabel->pixmap()->scaledToWidth(size.width(),Qt::SmoothTransformation);
			size.setHeight(tmp.height());
			m_pLabel->resize(size);
		}
	
	}
	updateActions();
}

QPixmap PictureWidget::GetPixmap()
{
	if(m_pLabel->pixmap()==NULL)
		return QPixmap();
	else
		return *m_pLabel->pixmap();
}




void PictureWidget::updateActions()
{

	Q_ASSERT(m_pLabel!=NULL);

	bool bEnable=!(m_pLabel->pixmap()==NULL);

	m_pActZoomIn->setEnabled(bEnable);
	m_pActZoomOut->setEnabled(bEnable);
	m_pActNormal->setEnabled(bEnable);
	m_pActSave->setEnabled(bEnable);
	m_pActPrint->setEnabled(bEnable);
	m_pActCopyToClipboard->setEnabled(bEnable);
	m_pActErase->setEnabled(bEnable);
	m_pActFitWindow->setEnabled(bEnable);


	if(bEnable)
	{
		m_pActZoomIn->setEnabled(!m_pActFitWindow->isChecked());
		m_pActZoomOut->setEnabled(!m_pActFitWindow->isChecked());
		m_pActNormal->setEnabled(!m_pActFitWindow->isChecked());
	}
	else
	{
		m_pActZoomIn->setEnabled(false);
		m_pActZoomOut->setEnabled(false);
		m_pActNormal->setEnabled(false);
	}

	//m_bEdit:
	m_pActLoad->setEnabled(m_bEdit);
	m_pActPastFromClipboard->setEnabled(m_bEdit);
	m_pActErase->setEnabled(m_bEdit);

}

void PictureWidget::scaleImage(double factor)
{
	Q_ASSERT(m_pLabel!=NULL);
	Q_ASSERT(m_pLabel->pixmap());
	scaleFactor *= factor;
	

	//adjustScrollBar(m_scrollArea->horizontalScrollBar(), factor);
	//adjustScrollBar(m_scrollArea->verticalScrollBar(), factor);

	m_pActZoomIn->setEnabled(scaleFactor < 3.0);
	m_pActZoomOut->setEnabled(scaleFactor > 0.333);
	m_pLabel->resize(scaleFactor * m_pLabel->pixmap()->size());
}



void PictureWidget::adjustScrollBar(QScrollBar *scrollBar, double factor)
{
	scrollBar->setValue(int(factor * scrollBar->value()
		+ ((factor - 1) * scrollBar->pageStep()/2)));
}



void PictureWidget::GetPictureInfo(int &width, int &height,int &nDepth,int &nPreviewSize,int &nFullSize)
{
	Q_ASSERT(m_pLabel!=NULL);
	if(m_pLabel->pixmap()!=NULL)
	{
		width=m_pLabel->pixmap()->width();
		height=m_pLabel->pixmap()->height();
		nDepth=m_pLabel->pixmap()->depth();
		
		if(m_pLstData!=NULL)
		{
			nPreviewSize=m_pLstData->GetDataSource()->getDataRef(0,m_nPreviewColIdx).toByteArray().size();
			nFullSize=m_pLstData->GetDataSource()->getDataRef(0,m_nFullColIdx).toByteArray().size();
		}
		else
		{
			nPreviewSize=0;
			nFullSize=0;
		}
		
	}
	else
	{
		width=0;
		height=0;
		nDepth=0;
		nPreviewSize=0;
		nFullSize=0;
	}
}




void PictureWidget::ChangePixMap(QPixmap *pixmap)
{
	if(!m_bRefreshInProgrres)
	{
		if(ChangeDataSource(pixmap))      //before putting picture on frame, calc preview, decide what to display, and return
		{
			RefreshDisplay();
			return;
		}
	}

	if(pixmap!=NULL)
		m_pLabel->setPixmap(*pixmap);
	else
		m_pLabel->clear();
	RefreshPictureState();
	emit PictureChanged();

}


void PictureWidget::ChangePictureToolTip()
{
	if(m_pLstData==NULL) return; //exist if NULL
	if(m_pLstData->GetDataSource()->getRowCount()==0)  return; //if no rows exit

	QByteArray bytePicture,bytePreview;
	m_pLstData->GetDataSource()->getData(0,m_nPreviewColIdx,bytePreview);
	m_pLstData->GetDataSource()->getData(0,m_nFullColIdx,bytePicture);

	//Change tooltip:
	int nWidth,nHeight,nDepth,nSizePrev,nSizeFull;
	GetPictureInfo(nWidth,nHeight,nDepth,nSizePrev,nSizeFull);
	QString strTitle="Viewing picture: "+QVariant(nWidth).toString()+" x "+QVariant(nHeight).toString()+" x "+QVariant(nDepth).toString();
	if (bytePicture.size()!=0) strTitle+=", Full Size = "+QVariant(bytePicture.size()).toString()+"b";
	strTitle+=", Preview Size = "+QVariant(bytePreview.size()).toString()+"b";
	strTitle+=", Current Format = "+m_strCurrentFormat;

	m_pLabel->setToolTip(strTitle);

}


void PictureWidget::dragEnterEvent ( QDragEnterEvent * event )
{
	//if (event->mimeData()->hasUrls())
	event->accept();

}

void PictureWidget::dragMoveEvent(QDragMoveEvent *event )
{
	event->acceptProposedAction();
}

void PictureWidget::dropEvent(QDropEvent *event)
{
	const QMimeData *mime = event->mimeData();

	if (!m_bEdit)
	{
		event->ignore();
		return;
	}
	//this->activateWindow(); //bring on top_:

	//------------------------------------------------
	//DOCS && Vcard ->import window or doc popup
	//------------------------------------------------
	if (mime->hasUrls())
	{
		QList<QUrl> lst = mime->urls();
		if (lst.size()==1)
		{
			DbRecordSet lstPaths;
			DbRecordSet lstApps;

			DataHelper::ParseFilesFromURLs(lst,lstPaths,lstApps,false,false);

			if (lstPaths.getRowCount()!=1)
			{
				event->ignore();
				return;
			}

			QString strExtension=lstPaths.getDataRef(0,2).toString().toLower();
			if (strExtension=="bmp" || strExtension=="jpeg" || strExtension=="jpg" || strExtension=="gif" || strExtension=="png" || strExtension=="tiff" )
			{
				//load picture from file
				QString strFilePath=lstPaths.getDataRef(0,0).toString();
				QByteArray bufPicture;

				QFile file(strFilePath);
				if(!file.open(QIODevice::ReadOnly))
				{
					event->ignore();
					return;
				}
				bufPicture=file.readAll();
				file.close();

				SetPictureFromBinaryData(bufPicture);

				event->accept();
				return;
			}
			else if (strExtension=="http")
			{
				QString strExtensionURL=lstPaths.getDataRef(0,1).toString().right(4).toLower();
				if (strExtensionURL==".bmp" || strExtensionURL==".jpeg" || strExtensionURL==".jpg" || strExtensionURL==".gif" || strExtensionURL==".png" || strExtensionURL==".tiff" )
				{
					QByteArray bufPicture;
					QString strFilePath=lstPaths.getDataRef(0,0).toString();
					if(GetLinkContent(strFilePath,bufPicture))
					{
						SetPictureFromBinaryData(bufPicture);
						event->accept();
						return;
					}
				}
			}
		}
	}


	//------------------------------------------------
	// Picture process
	//------------------------------------------------
	if (mime->hasImage())
	{
		QVariant var=mime->imageData();
		if (!var.isNull())
		{
			//convert to bytearray PNG
			QImage image = qvariant_cast<QImage>(var);
			QString strFormat="PNG";
			QByteArray bufPicture;
			QBuffer buffer(&bufPicture);
			buffer.open(QIODevice::WriteOnly);		
			image.save(&buffer, strFormat.toLatin1());			
			SetPictureFromBinaryData(bufPicture);
		}
		event->accept();
		return;
	}


	//default process:
	QWidget::dropEvent(event);
}


bool PictureWidget::GetLinkContent(QString strFilePath, QByteArray &content)
{
	HttpReaderSync WebReader(6000);  //6sec..
	connect(&WebReader,SIGNAL(ReEmitAuthenticationRequired ( const QString & , quint16 , QAuthenticator *  )),this, SLOT(OnEmitAuthenticationRequired ( const QString & , quint16 , QAuthenticator *  )));

	return WebReader.ReadWebContent(strFilePath, &content);
}


void PictureWidget::OnEmitAuthenticationRequired ( const QString & hostname, quint16 port, QAuthenticator *  auth)
{
	LoginDlg Dlg;
	Dlg.Initialize("",LoginDialogAbstract::LOGIN_DLG_WEB_AUTHENTICATION);
	Dlg.setWindowTitle(tr("Connect to ")+hostname);
	int nRes=Dlg.exec();
	if (nRes)
	{
		auth->setPassword(Dlg.m_strPass);
		auth->setUser(Dlg.m_strUser);
	}
}