#ifndef DLG_PICTURE_H
#define DLG_PICTURE_H

#include <QtWidgets/QDialog>
#include "generatedfiles/ui_dlg_picture.h"




class Dlg_Picture : public QDialog
{
    Q_OBJECT

public:
    Dlg_Picture(QWidget *parent = 0);
    ~Dlg_Picture();


	void GetPictureBinaryData(QByteArray &bytePicture);
	void GetPictureBinaryDataPreview(QByteArray &bytePicture);
	void SetPictureBinaryData(QByteArray &bytePicture);
	QString GetCurrentPictureFormat();

	void SetEditMode(bool bEditMode);

	QPixmap GetPixMap();
	void SetPixMap(QPixmap&);

private:
    Ui::Dlg_PictureClass ui;

private slots:
	void on_btnCancel_clicked();
	void on_btnOK_clicked();
	void PictureChanged();

};

#endif // DLG_PICTURE_H
