#include "dbrecordsetlistmodel.h"

DbRecordSetListModel::DbRecordSetListModel(QObject *Parent /*= NULL*/)
										   : QAbstractListModel(Parent)
{
	
}

DbRecordSetListModel::~DbRecordSetListModel()
{
	ClearModel();
}

void DbRecordSetListModel::ClearModel()
{
	qDeleteAll(m_hshModelItems);
	m_hshModelItems.clear();
}

int DbRecordSetListModel::rowCount(const QModelIndex &parent) const
{
	return m_hshModelItems.count();
}

/*!
Return data to display - customize for font, icon,...

\param index - index.
\param role	 - role.
\return QVariant - data.
*/
QVariant DbRecordSetListModel::data(const QModelIndex &index, int role) const
{
	//If index not valid (root item), go out.
	if (!index.isValid())
		return QVariant();

	//Get item pointer for later.
	DbRecordSetItem *item = m_hshModelItems.value(index.row());

	//Icon setting.
	if (role == Qt::DecorationRole)
	{
		QStringList IconList = item->GetIcons();
		int IconCount = IconList.count();

		if (IconCount <= 1)
		{
			QString Icon_resource = IconList.value(0);
			Icon_resource.prepend(":");
			QIcon icon;
			icon.addFile(Icon_resource);
			return QVariant(icon);
		}
		else //Add multiple icons.
		{
			//Calculate new icon length (16 pixels * number of icons.)
			int IconWidth = IconCount*16;
			QSize IconSize(IconWidth, 16); 
			//Make pixmap of future icon.
			QPixmap IconPixmap(IconSize);
			//Fill with white (alpha 0-transparent).
			IconPixmap.fill(QColor(255,255,255,0));
			//Make painter.
			QPainter painter(&IconPixmap);

			for (int i = 0; i < IconCount; i++)
			{
				//Get icon from icon list.
				QString Icon_resource = IconList.value(i);
				//Make it visible to resources.
				Icon_resource.prepend(":");

				QPixmap CurentIcon(Icon_resource);

				painter.drawPixmap(i*16, 0,CurentIcon);
			}

			QIcon icon(IconPixmap);
			return QVariant(icon);
		}
	}
	//Font decoration.
	if (role == Qt::FontRole)
	{
		QFont font;
		font.setStyle(QFont::StyleItalic);
		font.setFamily("Helvetica");
		font.setBold(true);
		font.setPointSize(10);
		return QVariant(font);
	}
	//Checked or not.
	if (role == Qt::CheckStateRole)
	{
		return QVariant();
		//		return QVariant(Qt::PartiallyChecked);
		//		return QVariant(Qt::PartiallyChecked);
	}
	//Edit role.
	if (role == Qt::EditRole)
	{
		return QVariant();
	}
	//Display role.
	if (role != Qt::DisplayRole)
	{
		return QVariant();
	}

	//Return data.
	return item->data(index.column());
}

QVariant DbRecordSetListModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
		return m_pRootItem->data(section);

	return QVariant();
}

/*!
Set header data.

\param index - section(column or row).
\param orientation - orientation.
\param value	 - value.
\param role	 - role.
\return bool.
*/
bool DbRecordSetListModel::setHeaderData ( int section, Qt::Orientation orientation, const QVariant &value, int role)
{
	if (orientation == Qt::Horizontal)
	{
		m_pRootItem->SetData(section, value);
		return true;
	}

	return true;
}

/*!
Initialize model with given recordset.

\param RecordSet			- RecordSet.
*/
void DbRecordSetListModel::InitializeModel(DbRecordSet *RecordSet)
{
	//Set root item here is just for header.
	SetRootItem();

	//Initialize.
	m_pDbRecordSet	  = RecordSet;
	//Default icon size.
	m_sizeMaxIconSize = QSize(16, 16);	
	SetupModelData();
}

/*!
Setup model data from recordset.

\param Parent	 - Parent item.
*/
void DbRecordSetListModel::SetupModelData(DbRecordSetItem *Parent /*= NULL*/)
{
	int row_count = m_pDbRecordSet->getRowCount();
	for (int row = 0; row < row_count; ++row)
	{
		//Set some data to display.
		QList<QString> data;
		QString Name = m_pDbRecordSet->getDataRef(row, 1).toString();
		QString Desr = m_pDbRecordSet->getDataRef(row, 2).toString();
		QString DisplayData = Name + " " + Desr;
		data << DisplayData;
		int RowId	 = m_pDbRecordSet->getDataRef(row, 0).toInt();

		//Role type (business or system).
		int RoleType = m_pDbRecordSet->getDataRef(row, 3).toInt();
		//Add some Icon.
		QStringList IconList;
		if (RoleType == 1)
			IconList << "1";
		else
			IconList << "3";

		//Create new item. In list item don't have children and Parent is always root item.
		DbRecordSetItem *item = new DbRecordSetItem(RowId, data, IconList, 0);
		m_hshModelItems.insert(row, item);
	}
	//Clear recordset.
	m_pDbRecordSet->clear();
}

/*!
Set  root item (just for header).

*/
void DbRecordSetListModel::SetRootItem()
{
	QList<QString> rootData;
	rootData << QString("Header1") << QString("Header2");

	m_pRootItem = new DbRecordSetItem(-1, rootData, QStringList(), 1);
}

Qt::ItemFlags DbRecordSetListModel::flags(const QModelIndex &index) const
{
	if (!index.isValid())
		return Qt::ItemIsEnabled;

	Qt::ItemFlags DefaultFlags = QAbstractItemModel::flags(index);
	return Qt::ItemIsDragEnabled | DefaultFlags;
}

Qt::DropActions DbRecordSetListModel::supportedDropActions() const
{
	return Qt::MoveAction;
}

QHash<int, DbRecordSetItem*> *DbRecordSetListModel::GetItemsHash()
{
	return &m_hshModelItems;
}
