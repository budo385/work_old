#ifndef TABLEFINDCOMBOBOX_H
#define TABLEFINDCOMBOBOX_H

#include "universaltablewidget.h"
#include <QComboBox>

/*!
\class TableFindComboBox
\brief Universal table find widget's combo box.
\ingroup GUICore_CustomWidgets

Find combo box in universal table find widget. Searches tree, highlights first found.
On focus lost loads data to detail.

*/
class TableFindComboBox : public QComboBox
{
    Q_OBJECT

public:
    TableFindComboBox(QWidget *parent = 0);
    ~TableFindComboBox();

	void Initialize(QTableWidget *pTableWidget, int nSearchedColumn=-1 /* -1 means all *//*, DbRecordSet *pLstData=NULL, int nItemTextColIdx=-1, int nOrganizationColIdx=-1*/);

private:
	//Events.
	void focusOutEvent(QFocusEvent* event);
	void keyPressEvent(QKeyEvent* event);
	void SetOrganizationName(QString &strItemText, int nItemID);

	QTableWidget				*m_pTableWidget;			//< Pointer to tree widget.
	QList<QTableWidgetItem*>	m_lstFoundItems;			//< Current found items list.
	QList<QTableWidgetItem*>	m_lstFoundItems_LastSearch;	//< Current found items list. B.T. issue 2556
	int							m_nCurrentFoundItemNum;		//< Current selected item.
	QString						m_strDefaultStyleSheet;
	QString						m_strLastFindString;		//< B.T. improved search engine
	QTableWidgetItem*			m_pLastFoundItem;
	int							m_nSearchedColumn;

	int							m_nOrganizationColIdx; //issue 2556: add organization name
	int							m_nItemTextColIdx; 
	DbRecordSet					*m_pLstData;



signals:
	void EnterPressed();
private slots:
	void on_ComboTextChanged(const QString &text);
	void on_currentIndexChanged(int index);
	void on_highlighted(int index);
	void on_view_pressed(const QModelIndex &index);
};

#endif // TABLEFINDCOMBOBOX_H
