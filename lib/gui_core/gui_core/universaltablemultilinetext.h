#ifndef UNIVERSALTABLEMULTILINETEXT_H
#define UNIVERSALTABLEMULTILINETEXT_H

#include <QTextEdit>
#include <QTextCursor>
#include <QFocusEvent>
#include "universaltablecelldwidget.h"


/*!
	\class  UniversalTableMultiLineText
	\ingroup GUICore_UniversalTable
	\brief  Widget used in UniversalTableWidget

	Use:
	- when adding column to UniversalTableWidget, mark column type=COL_TYPE_MULTILINE_TEXT (see MVIEW_GRID_COLUMN_SETUP)
	- by default, column (inside  UniversalTableWidget datasource list) must contain QString
	- Text changes are reflected back to datasource in the tablewidget

*/

class UniversalTableMultiLineText : public QTextEdit, public TableCellWidget
{
	Q_OBJECT

public:
    UniversalTableMultiLineText(QWidget *parent);
    ~UniversalTableMultiLineText();

	void SetData(int nRow, int nCol,QVariant &Data,QString strDataSourceFormat="",bool bIsEditable=true); //called by UniversalTableWidget when widget is created
	void RefreshData(QVariant &Data,bool bCopyContent=false);	//called by UniversalTableWidget in refresh method when widget data is only refreshed (data -> screen)
	void SetEditMode(bool bEdit=true);	//called by UniversalTableWidget for disabling/enabling user interaction with child widget

protected:

	virtual void mousePressEvent ( QMouseEvent * event );

	//void focusInEvent(QFocusEvent * event);

private slots:

	void TextChanged();
    
};

#endif // UNIVERSALTABLEMULTILINETEXT_H

