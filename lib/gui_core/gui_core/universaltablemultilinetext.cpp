#include "universaltablemultilinetext.h"

UniversalTableMultiLineText::UniversalTableMultiLineText(QWidget *parent)
	: QTextEdit(parent)
{
	//setReadOnly(true);
	connect(this,SIGNAL(textChanged()),this,SLOT(TextChanged()));
}

UniversalTableMultiLineText::~UniversalTableMultiLineText()
{

}



/*!
	Called by UniversalTableWidget when widget is created
	\param value	- sets data from UniversalTableWidget datasource, 
					  data is extracted from value using m_strDataSource
					  value must be DbRecordSet
*/
void UniversalTableMultiLineText::SetData(int nRow, int nCol,QVariant &Data,QString strDataSourceFormat,bool bIsEditable)
{

	//sets data:
	TableCellWidget::SetData(nRow,nCol,Data,strDataSourceFormat,bIsEditable);
	RefreshData(Data);
	setReadOnly(false); //disable edit: coz cursor appears...
	if(!bIsEditable)setReadOnly(true);//setEnabled(false);
}


/*!
	Called by UniversalTableWidget when widget already exist and data needs to be refreshed
	In combo box: selection is copied and set as current 
	\param value	- returns data set by SetData() (DbRecordSet), any user changes are stored in value
*/
void UniversalTableMultiLineText::RefreshData(QVariant &Data,bool bCopyContent)
{
	QString strData = Data.toString();

	disconnect(this,SIGNAL(textChanged()),this,SLOT(TextChanged()));
	setHtml (strData);


	connect(this,SIGNAL(textChanged()),this,SLOT(TextChanged()));
}


/*!
	Called by UniversalTableWidget for disabling/enabling user interaction with child widget
	\param bEdit	- true, control is clickable, false, control is disabled
*/
void UniversalTableMultiLineText::SetEditMode(bool bEdit)
{
	
	if(m_bEditable)setReadOnly(!bEdit);//setEnabled(bEdit);

	//setReadOnly(!bEdit);
}


/*!
	Triggerred whenever text is changed
*/
void UniversalTableMultiLineText::TextChanged()
{
	QString strData = toHtml();
	qVariantSetValue(m_Data, strData.toLatin1());		//store back

	DataChanged();	//emit signal that data has changed
}


/*
//when got focus: enable edit, else disable
void UniversalTableMultiLineText::focusInEvent(QFocusEvent * event)
{
	if(event->gotFocus())
		setReadOnly(false);
	if(event->lostFocus())
		setReadOnly(true);

	QTextEdit::focusInEvent(event);
}

*/





void UniversalTableMultiLineText::mousePressEvent ( QMouseEvent * event )
{
	CellClicked();
	QTextEdit::mousePressEvent(event);
}