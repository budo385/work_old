#ifndef FINDCOMBOBOX_H
#define FINDCOMBOBOX_H

#include "universaltreewidget.h"
#include <QComboBox>

/*!
\class FindComboBox
\brief Universal tree find widget's combo box.
\ingroup GUICore_CustomWidgets

Find combo box in universal tree find widget. Searches tree, highlights first found.
On focus lost loads data to detail.

*/
class FindComboBox : public QComboBox
{
    Q_OBJECT

public:
    FindComboBox(QWidget *parent = 0);
    ~FindComboBox();

	void Initialize(UniversalTreeWidget *pTreeWidget);

signals:
	void EnterPressed();

private:
	//Events.
	void focusOutEvent(QFocusEvent* event);
	void keyPressEvent(QKeyEvent* event);

	UniversalTreeWidget		*m_pTreeWidget;				//< Pointer to tree widget.
	QList<QTreeWidgetItem*> m_lstFoundItems;			//< Current found items list.
	int						m_nCurrentFoundItemNum;		//< Current selected item.
	QString					m_strLastFindString;		//< B.T. improved search engine

private slots:
	void on_ComboTextChanged(const QString &text);
	void OnRebuildingTree();
};

#endif // FINDCOMBOBOX_H
