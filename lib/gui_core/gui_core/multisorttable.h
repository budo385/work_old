#ifndef MULTISORTTABLE_H
#define MULTISORTTABLE_H

#include <QtWidgets/QTableWidget>
#include <QTableWidgetSelectionRange>
#include <QHeaderView>
#include <QPushButton>
#include <QDragEnterEvent>
#include <QDropEvent>
#include <QAction>

#include "db_core/db_core/dbsqltableview.h"
#include "common/common/dbrecordset.h"
#include "gui_core/gui_core/dragdropinterface.h"

//edit:
#include "gui_core/gui_core/guidatamanipulator.h"




/*!
	\class  MultiSortTable
	\ingroup GUICore_UniversalTable
	\brief  Simple table widget used by UniversalTableWidget for multicolumn sorting

	Use:

*/
class MultiSortTable : public QTableWidget, public DragDropInterface
{
	Q_OBJECT

public:
    MultiSortTable(QWidget *parent);
    ~MultiSortTable();

		enum ColumnType
	{
		COL_TYPE_TEXT=0,
		COL_TYPE_CHECKBOX=1,
		COL_TYPE_COMBOBOX=2,
		COL_TYPE_LIST=3,
		COL_TYPE_MULTILINE_TEXT=4,
		COL_TYPE_PICTURE=5,
		COL_TYPE_USER_DEFINED=20
	};

	void Initialize(DbRecordSet *lstData,bool bEnableForColumnSetup=false);
	void InitializeForCommGrid(DbRecordSet *lstData,bool bEnableForColumnSetup=false);


	//DataSource
	DbRecordSet* GetDataSource(){return m_lstData;};


	//Column setup:
	//void SetColumnSetup(DbRecordSet &lstColumnSetup);
	//void GetColumnSetup(DbRecordSet &lstColumnSetup);
	//static void DefineColumnSetupFromView(DbRecordSet &lstSetup,int nView);
	static void AddColumnToSetup(DbRecordSet &lstSetup,QString strColName,QString strHeaderName=QString(),int nColType=COL_TYPE_TEXT,QString strDataFormat=QString(),bool bEditable=true,int nWidth=70, QString strToolTip=QString());



	//Edit mode:
	void SetEditMode(bool bEdit=true);
	bool IsEditMode(){return m_bEdit;};



	//Drop operations	(override all if you want)
	Qt::DropActions supportedDropActions() const {return DragDropInterface::supportedDropActions();};
	void dropEvent(QDropEvent *event);
	void dragEnterEvent ( QDragEnterEvent * event );


	//our custom drop handlers:
	int GetDropType();	//this must be override to support drop
	void GetDropValue(DbRecordSet &values, bool bSelectItemChildren = false) const;
	void DropHandler(QModelIndex index, int nDropType, DbRecordSet &DroppedValue,QDropEvent *event);



	//Move Up/Down:
	void MoveRowUp();
	void MoveRowDown();

	//Get Result:
	void GetListForSorting(QHash<QString,int> &lstSortingCols);

	//For column setup:
	void GetColumnSetupList(DbRecordSet &lstSortingCols);
	void SetColumnSetupList(DbRecordSet &lstSortingCols);



public slots:


	//Refresh display: check col/rows, if no match add, else copy values-> to items
	void RefreshDisplay();
	void RefreshDisplayRow(int nRow); //faster for refreshing only one row
	void RefreshSelection(); 	//Selections (if only selection is changed call this, faster then RefreshDisplay):

	void InsertRow();
	void DeleteSelection();
	void DeleteTable();
	//void SelectAll();
	//void DeselectAll();


protected slots:
	void SelectionChanged();
	void CellEdited(int nRow, int nCol);


	
private:

	void RefreshRowContent(int nRow);
	void RestoreHeaderSetup();

	QAction	*m_pActionDel;

	DbRecordSet m_lstColumnSetup;
	DbRecordSet *m_lstData;

	GuiDataManipulator *m_pData;
	bool m_bExternalDataManipulator;

	QHash<int,int> m_lstColumnMapping;
	bool m_bEdit;
	int m_nVerticalRowSize;
    
};

#endif // MULTISORTTABLE_H


