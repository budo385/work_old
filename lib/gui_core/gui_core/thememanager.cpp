#include "thememanager.h"
#include <QResource>
#include <QCoreApplication>

QString ThemeManager::m_strThemePrefix;
int ThemeManager::m_nCurrentThemeID=-1; //to init change
int ThemeManager::m_nViewModeID=-1;     //to init change

#ifdef _DEBUG
//	#define NO_GRADIENT_BACKGROUND
#endif


//about border-image from http://www.w3.org/TR/css3-background/#the-border-image:
//The first of the four values is the height of the top edge and of the two top corners. The second is the width of the right edge and the two right corners. The third is the height of the bottom edge and the two bottom corners. The fourth is the width of the left edge and the two left corners.
//At the end of the value, there may be up to two keywords, that specify how the images for the sides and the middle part are scaled and tiled. If the second is absent, it is assumed to be the same as the first. If both are absent, the effect is the same as �stretch stretch�.
//


#define CE_MENU_BUTTON 			"QLabel {color:white} QPushButton { border-width: 4px; color: white;border-image:url(%1) 4px 4px 4px 4px stretch stretch }\
QPushButton:hover {  border-width: 4px; color: white;border-image:url(%2) 4px 4px 4px 4px stretch stretch}"
#define CE_MENU_BUTTON_LIGHT 	"QLabel {color:black} QPushButton { border-width: 4px; color: white;border-image:url(%1) 4px 4px 4px 4px stretch stretch }\
QPushButton:hover {  border-width: 4px; color: white;border-image:url(%2) 4px 4px 4px 4px stretch stretch}"
#define BKG_CE_MENU_DROPZONE	"QFrame#frameBkg { border-width: 4px; color: white; border-image:url(%1) 4px 4px 4px 4px stretch stretch }"
#define BKG_MAIN_TOOLBAR		"QFrame#frameToolBar { border-width: 4px; color: white; border-image:url(%1) 4px 4px 4px 4px stretch stretch }"
//#define BKG_HEADER_SIDEBAR		"QFrame { border-width: 4px; color: white; border-image:url(%1) 4px 4px 4px 4px stretch stretch }"
#define BKG_HEADER_SIDEBAR		"QFrame { border-width: 4px; color: white; background-color: rgb(72,92,130)}"
//#define BKG_HEADER_SIDEBAR_CENTER	"QFrame#frameBannerCenter { background-image:url(%1); background-repeat: repeat-x; background-origin: border padding content;}"
#define BKG_HEADER_SIDEBAR_CENTER	"QFrame#frameBannerCenter { border-width: 4px; color: white; background-color: rgb(72,92,130)}"
#define BKG_FOOTER_SIDEBAR_CENTER	"QFrame#frameFooterCenter { border-width: 4px; color: white; border-image:url(%1) 4px 4px 4px 4px stretch stretch }"
#define BKG_BORDER_STYLE			"{ border-width: 4px; color: white; border-image:url(%1) 4px 4px 4px 4px stretch stretch }"
//#define MOBILE_BKG_BORDER_STYLE		"{ border-width: 4px; color: white; border-image:url(%1) 4px 4px 4px 4px stretch stretch }"
//#define MOBILE_HEADER_BKG_BORDER_STYLE	"{ border-width: 4px; color: white; border-image:url(%1) 4px 4px 4px 4px stretch stretch }"
#define MOBILE_BKG_BORDER_STYLE		"{ border-image:url(%1)}"
#define MOBILE_HEADER_BKG_BORDER_STYLE	"{ border-image:url(%1)}"

void ThemeManager::SetViewMode(int nViewID)
{
	if (nViewID==m_nViewModeID)
		return;
	m_nViewModeID=nViewID;
}

void ThemeManager::SetCurrentThemeID(int nThemeID)
{
	if (nThemeID==m_nCurrentThemeID)
		return;
	m_nCurrentThemeID=nThemeID;
	
	//unload previous resource:
	if (!m_strThemePrefix.isEmpty())
	{
		QString strTheme=m_strThemePrefix.mid(7)+".rcc";
		bool bOK=QResource::unregisterResource(QCoreApplication::applicationDirPath()+"/themes/"+strTheme);
		Q_ASSERT(bOK);
	}
	m_strThemePrefix=GetThemePrefix(m_nCurrentThemeID);

	//load resources:
	if (!m_strThemePrefix.isEmpty())
	{
		QString strTheme=m_strThemePrefix.mid(7)+".rcc";
		bool bOK=QResource::registerResource(QCoreApplication::applicationDirPath()+"/themes/"+strTheme);
		Q_ASSERT(bOK);
	}
}



QString ThemeManager::GetCEMenuButtonStyle()
{
#ifdef NO_GRADIENT_BACKGROUND
	return "";
#endif
	return QString(CE_MENU_BUTTON).arg(m_strThemePrefix+"_CEMenuButton.png").arg(m_strThemePrefix+"_CEMenuButton_Hover.png");
}

QString ThemeManager::GetCEMenuButtonStyle_Ex()
{
#ifdef NO_GRADIENT_BACKGROUND
	return "";
#endif
	return QString(BKG_BORDER_STYLE).arg(m_strThemePrefix+"_CEMenuButton.png");
}

QString ThemeManager::GetCEDropZonePicture()
{
#ifdef NO_GRADIENT_BACKGROUND
	return "";
#endif
	return m_strThemePrefix+"_CEDropZonePicture.png";	
}

QString ThemeManager::GetCEDropZoneBkgStyle()
{
#ifdef NO_GRADIENT_BACKGROUND
	return "";
#endif
	//if (m_nCurrentThemeID==THEME_BLUE_TEMPLE)
		return QString(BKG_CE_MENU_DROPZONE).arg(m_strThemePrefix+"_CEDropZoneBkg.png");
	//else
	//	return "";

}
QString ThemeManager::GetToolbarButtonStyle()
{
	return "QLabel {color:white}";
}

QString ThemeManager::GetToolbarBkgStyle()
{
#ifdef NO_GRADIENT_BACKGROUND
	return "";
#endif
	return QString(BKG_MAIN_TOOLBAR).arg(m_strThemePrefix+"_ToolbarBkg.png");
}
QString ThemeManager::GetToolbarTextColor()
{
#ifdef NO_GRADIENT_BACKGROUND
	return "";
#endif
	switch(m_nCurrentThemeID)
	{
	case THEME_BLUE_TEMPLE:
		return "black";
		break;
	case THEME_GRAYHOUND:
		return "black";
		break;
	case THEME_CABERNET:
		return "black";
		break;
	case THEME_FOREST:
		return "black";
		break;
	case THEME_SMARAGD:
		return "black";
		break;
	default:
		return "black";
		break;
	}


}
QString ThemeManager::GetHeaderRight()
{
#ifdef NO_GRADIENT_BACKGROUND
	return "";
#endif
	return m_strThemePrefix+"_Header_Right.png";

}
QString ThemeManager::GetHeaderCenter()
{
#ifdef NO_GRADIENT_BACKGROUND
	return "";
#endif
	return m_strThemePrefix+"_Header_Center.png";
}
QString ThemeManager::GetHeaderLeft(QString strModule)
{
#ifdef NO_GRADIENT_BACKGROUND
	return "";
#endif
	if(strModule == "SC-TE")
		return m_strThemePrefix+"_Header_TE.png";
	else if(strModule == "SC-PE")
		return m_strThemePrefix+"_Header_PE.png";
	else if(strModule == "SC-BE")
		return m_strThemePrefix+"_Header_BE.png";
	else if(strModule == "SC-EE")
		return m_strThemePrefix+"_Header_EE.png";
	else
		return m_strThemePrefix+"_Header_Empty.png";

}


QString ThemeManager::GetSidebar_HeaderRight()
{
#ifdef NO_GRADIENT_BACKGROUND
	return "";
#endif
	return QString(BKG_HEADER_SIDEBAR).arg(m_strThemePrefix+"_Sidebar_Header_Right.png");

}
QString ThemeManager::GetSidebar_HeaderCenter()
{
#ifdef NO_GRADIENT_BACKGROUND
	return "";
#endif
	return QString(BKG_HEADER_SIDEBAR_CENTER).arg(m_strThemePrefix+"_Sidebar_Header_Center.png");
}
QString ThemeManager::GetSidebar_HeaderLeft()
{
#ifdef NO_GRADIENT_BACKGROUND
	return "";
#endif
	return QString(BKG_HEADER_SIDEBAR).arg(m_strThemePrefix + "");
//	return QString(BKG_HEADER_SIDEBAR).arg(m_strThemePrefix + "_Sidebar_Header_Left.png");
}



QString ThemeManager::GetThemePrefix(int nThemeID)
{

#ifdef NO_GRADIENT_BACKGROUND
	return "";
#endif

	switch(nThemeID)
	{
	case THEME_BLUE_TEMPLE:
		return ":Theme_Blue_Temple";
		break;
	case THEME_GRAYHOUND:
		return ":Theme_Grayhound";
		break;
	case THEME_CABERNET:
		return ":Theme_Cabernet";
		break;
	case THEME_FOREST:
		return ":Theme_Forest";
		break;
	case THEME_SMARAGD:
		return ":Theme_Smaragd";
		break;
	default:
		return ":Theme_Blue_Temple";
		break;
	}

}





QString ThemeManager::GetSidebar_FooterRight()
{
#ifdef NO_GRADIENT_BACKGROUND
	return "";
#endif
	return QString(BKG_HEADER_SIDEBAR).arg(m_strThemePrefix+"_Sidebar_Footer_Right.png");
}
QString ThemeManager::GetSidebar_FooterCenter()
{
#ifdef NO_GRADIENT_BACKGROUND
	return "";
#endif
	return QString(BKG_FOOTER_SIDEBAR_CENTER).arg(m_strThemePrefix+"_Sidebar_Footer_Center.png");
}
QString ThemeManager::GetSidebar_FooterLeft()
{
#ifdef NO_GRADIENT_BACKGROUND
	return "";
#endif
	return QString(BKG_HEADER_SIDEBAR).arg(m_strThemePrefix+"_Sidebar_Footer_Left.png");
}
QString ThemeManager::GetSidebar_CenterHandle()
{
#ifdef NO_GRADIENT_BACKGROUND
	return "";
#endif
	return QString("{image: url("+m_strThemePrefix+"_Sidebar_Footer_Handle.png"+"); border-width: 4px; color: white; border-image:url(%1) 4px 4px 4px 4px stretch stretch}").arg(m_strThemePrefix+"_Sidebar_Handle_Bkg.png");
}
QString ThemeManager::GetSidebar_FooterHandle()
{
#ifdef NO_GRADIENT_BACKGROUND
	return "";
#endif
	return " {image: url("+m_strThemePrefix+"_Sidebar_Footer_Handle.png)}" ;	
}


QString ThemeManager::GetSidebar_Bkg()
{
#ifdef NO_GRADIENT_BACKGROUND
	return "";
#endif
	
	switch(m_nCurrentThemeID)
	{
	case THEME_BLUE_TEMPLE:
		return "{background-color: rgb(233,236,240)}";
		break;
	case THEME_GRAYHOUND:
		return "{background-color: rgb(233,233,233)}";
		break;
	case THEME_CABERNET:
		return "{background-color: rgb(244,238,240)}";
		break;
	case THEME_FOREST:
		return "{background-color: rgb(239,248,243)}";
		break;
	case THEME_SMARAGD:
		return "{background-color: rgb(237,243,245)}";
		break;
	default:
		return "{background-color: rgb(233,236,240)}";
		break;
	}
}



QString ThemeManager::GetSidebarCommToolBar_Bkg()
{
#ifdef NO_GRADIENT_BACKGROUND
	return "";
#endif
	
	switch(m_nCurrentThemeID)
	{
	case THEME_BLUE_TEMPLE:
		return "{background: qlineargradient(x1:0, y1:0, x2:1,y2:0,stop:0 rgb(89,107,142), stop:1 rgb(223,227,233)); border-width:2px; border-radius: 8px; border-style: ridge ;border-color: gray}";
			break;
	case THEME_GRAYHOUND:
		return "{background: qlineargradient(x1:0, y1:0, x2:1,y2:0,stop:0 rgb(89,89,89), stop:1 rgb(224,224,224); border-width:	2px; border-radius: 8px; border-style: ridge ;border-color: gray}";
			break;
	case THEME_CABERNET:
		return "{background: qlineargradient(x1:0, y1:0, x2:1,y2:0,stop:0 rgb(113,43,70), stop:1 rgb(237,229,232)); border-width:	2px; border-radius: 8px; border-style: ridge ;border-color: gray}";
			break;
	case THEME_FOREST:
		return "{background: qlineargradient(x1:0, y1:0, x2:1,y2:0,stop:0 rgb(50,161,107), stop:1 rgb(230,243,237)); border-width:	2px; border-radius: 8px; border-style: ridge ;border-color: gray}";
			break;
	case THEME_SMARAGD:
		return "{background: qlineargradient(x1:0, y1:0, x2:1,y2:0,stop:0 rgb(47,113,124), stop:1 rgb(225,235,238)); border-width:	2px; border-radius: 8px; border-style: ridge ;border-color: gray}";
		break;

	default:
		return "{background: qlineargradient(x1:0, y1:0, x2:1,y2:0,stop:0 rgb(89,107,142), stop:1 rgb(223,227,233)); border-width:	2px; border-radius: 8px; border-style: ridge ;border-color: gray}";
			break;
	}
}


QString ThemeManager::GetSidebarActualName_Font()
{
#ifdef NO_GRADIENT_BACKGROUND
	return "";
#endif

	switch(m_nCurrentThemeID)
	{
	case THEME_BLUE_TEMPLE:
		return  "{font-weight:900;font-style:italic;font-family:Arial; font-size:10pt; color:black}"; 
		break;
	case THEME_GRAYHOUND:
		return  "{font-weight:900;font-style:italic;font-family:Arial; font-size:10pt; color:black}"; 
		break;
	case THEME_CABERNET:
		return  "{font-weight:900;font-style:italic;font-family:Arial; font-size:10pt; color:black}"; 
		break;
	case THEME_FOREST:
		return  "{font-weight:900;font-style:italic;font-family:Arial; font-size:10pt; color:black}"; 
		break;
	case THEME_SMARAGD:
		return  "{font-weight:900;font-style:italic;font-family:Arial; font-size:10pt; color:black}"; 
		break;
	default:
		return  "{font-weight:900;font-style:italic;font-family:Arial; font-size:10pt; color:black}"; 
		break;
	}
}

QString ThemeManager::GetSidebarChapter_Bkg()
{
#ifdef NO_GRADIENT_BACKGROUND
	return "";
#endif
	return 	QString(BKG_BORDER_STYLE).arg(m_strThemePrefix+"_Sidebar_Chapter.png");
}

QString ThemeManager::GetSidebarChapter_Font(QString strFontWeight /*= QString("900")*/, QString strFontStyle /*= QString("italic")*/, QString strFontSize /*= QString("16")*/)
{
#ifdef NO_GRADIENT_BACKGROUND
	return "";
#endif
	
	switch(m_nCurrentThemeID)
	{
	case THEME_BLUE_TEMPLE:
		return  "{font-weight:" + strFontWeight + ";font-style:" + strFontStyle + ";font-family:Arial; font-size:" + strFontSize + "pt; color:white}"; 
		break;
	case THEME_GRAYHOUND:
		return  "{font-weight:" + strFontWeight + ";font-style:" + strFontStyle + ";font-family:Arial; font-size:" + strFontSize + "pt; color:white}"; 
		break;
	case THEME_CABERNET:
		return  "{font-weight:" + strFontWeight + ";font-style:" + strFontStyle + ";font-family:Arial; font-size:" + strFontSize + "pt; color:white}"; 
		break;
	case THEME_FOREST:
		return  "{font-weight:" + strFontWeight + ";font-style:" + strFontStyle + ";font-family:Arial; font-size:" + strFontSize + "pt; color:white}"; 
		break;
	case THEME_SMARAGD:
		return  "{font-weight:" + strFontWeight + ";font-style:" + strFontStyle + ";font-family:Arial; font-size:" + strFontSize + "pt; color:white}"; 
		break;
	default:
		return  "{font-weight:" + strFontWeight + ";font-style:" + strFontStyle + ";font-family:Arial; font-size:" + strFontSize + "pt; color:white}"; 
		break;
	}
}



QString ThemeManager::GetCEMenuButtonStyleLighter()
{
	return QString(CE_MENU_BUTTON_LIGHT).arg(m_strThemePrefix+"_Button_DarkBlueGray_VeryLight.png").arg(m_strThemePrefix+"_Button_DarkBlueGray_Highlight.png");
}


QString ThemeManager::GetAvatarBkg()
{
#ifdef NO_GRADIENT_BACKGROUND
	return "";
#endif
	return m_strThemePrefix+"_AvatarFrame.png";
}


QString ThemeManager::GetContactGridBkg()
{
#ifdef NO_GRADIENT_BACKGROUND
	return "";
#endif
	QString strStyle;
	switch(m_nCurrentThemeID)
	{
	case THEME_BLUE_TEMPLE:
		{
			strStyle= "QTableWidget {show-decoration-selected: 1; color:white; selection-background-color:red; background:qradialgradient(cx:0.7, cy:0.3, radius: 1, fx:0.7, fy:0.3, stop:0 rgb(129,145,175), stop:1 rgb(70,82,106));}";
			strStyle+="QTableWidget {border-style: solid; border-top-color: qlineargradient(x1:0, y1:0, x2:0, y2:1,stop:0 rgb(52,60,76), stop:1  rgb(78,90,113)); border-left-color: qlineargradient(x1:0, y1:0, x2:1, y2:0,stop:0 rgb(52,60,76), stop:1  rgb(78,90,113)); border-bottom-color: rgb(78,90,113); border-right-color: rgb(78,90,113); border-width: 3px 1px 1px 3px}";
			strStyle+="QTableWidget:disabled {background: rgb(70,82,106)}";
		}
		break;
	case THEME_GRAYHOUND:
		{
			strStyle= "QTableWidget {show-decoration-selected: 1; color:white; selection-background-color:red; background:qradialgradient(cx:0.7, cy:0.3, radius: 1, fx:0.7, fy:0.3, stop:0 rgb(161,161,161), stop:1 rgb(107,107,107));}";
			strStyle+="QTableWidget {border-style: solid; border-top-color: qlineargradient(x1:0, y1:0, x2:0, y2:1,stop:0 rgb(74,74,74), stop:1  rgb(109,109,109)); border-left-color: qlineargradient(x1:0, y1:0, x2:1, y2:0,stop:0 rgb(74,74,74), stop:1  rgb(109,109,109)); border-bottom-color: rgb(109,109,109); border-right-color: rgb(109,109,109); border-width: 3px 1px 1px 3px}";
			strStyle+="QTableWidget:disabled {background: rgb(107,107,107)}";
		}
		break;
	case THEME_CABERNET:
		{
			strStyle= "QTableWidget {show-decoration-selected: 1; color:white; selection-background-color:red; background:qradialgradient(cx:0.7, cy:0.3, radius: 1, fx:0.7, fy:0.3, stop:0 rgb(156,86,112), stop:1 rgb(101,25,54));}";
			strStyle+="QTableWidget {border-style: solid; border-top-color: qlineargradient(x1:0, y1:0, x2:0, y2:1,stop:0 rgb(71,25,42), stop:1  rgb(105,37,63)); border-left-color: qlineargradient(x1:0, y1:0, x2:1, y2:0,stop:0 rgb(71,25,42), stop:1  rgb(105,37,63)); border-bottom-color: rgb(105,37,63); border-right-color: rgb(105,37,63); border-width: 3px 1px 1px 3px}";
			strStyle+="QTableWidget:disabled {background: rgb(101,25,54)}";
		}
		break;
	case THEME_FOREST:
		{
			strStyle= "QTableWidget {show-decoration-selected: 1; color:white; selection-background-color:red; background:qradialgradient(cx:0.7, cy:0.3, radius: 1, fx:0.7, fy:0.3, stop:0 rgb(115,190,143), stop:1 rgb(23,107,53));}";
			strStyle+="QTableWidget {border-style: solid; border-top-color: qlineargradient(x1:0, y1:0, x2:0, y2:1,stop:0 rgb(29,79,46), stop:1  rgb(45,117,70)); border-left-color: qlineargradient(x1:0, y1:0, x2:1, y2:0,stop:0 rgb(29,79,46), stop:1  rgb(45,117,70)); border-bottom-color: rgb(145,117,70); border-right-color: rgb(45,117,70); border-width: 3px 1px 1px 3px}";
			strStyle+="QTableWidget:disabled {background: rgb(23,107,53)}";
		}
		break;
	case THEME_SMARAGD:
		{
			strStyle= "QTableWidget {show-decoration-selected: 1; color:white; selection-background-color:red; background:qradialgradient(cx:0.7, cy:0.3, radius: 1, fx:0.7, fy:0.3, stop:0 rgb(97,148,158), stop:1 rgb(10,98,112));}";
			strStyle+="QTableWidget {border-style: solid; border-top-color: qlineargradient(x1:0, y1:0, x2:0, y2:1,stop:0 rgb(29,79,46), stop:1  rgb(45,117,70)); border-left-color: qlineargradient(x1:0, y1:0, x2:1, y2:0,stop:0 rgb(29,79,46), stop:1  rgb(45,117,70)); border-bottom-color: rgb(145,117,70); border-right-color: rgb(45,117,70); border-width: 3px 1px 1px 3px}";
			strStyle+="QTableWidget:disabled {background: rgb(10,98,112)}";
		}
		break;

	default:
		strStyle= "QTableWidget {show-decoration-selected: 1; color:white; selection-background-color:red; background:qradialgradient(cx:0.7, cy:0.3, radius: 1, fx:0.7, fy:0.3, stop:0 rgb(129,145,175), stop:1 rgb(70,82,106));}";
		strStyle+="QTableWidget {border-style: solid; border-top-color: qlineargradient(x1:0, y1:0, x2:0, y2:1,stop:0 rgb(52,60,76), stop:1  rgb(78,90,113)); border-left-color: qlineargradient(x1:0, y1:0, x2:1, y2:0,stop:0 rgb(52,60,76), stop:1  rgb(78,90,113)); border-bottom-color: rgb(78,90,113); border-right-color: rgb(78,90,113); border-width: 3px 1px 1px 3px}";
		strStyle+="QTableWidget:disabled {background: rgb(70,82,106)}";
		break;
	}

	return strStyle;
}

QString ThemeManager::GetTableViewBkg(QString strParentObjectName /* to avoid propagation on child widgets */)
{
#ifdef NO_GRADIENT_BACKGROUND
	return "";
#endif

	if (strParentObjectName.isEmpty())
		strParentObjectName="QTableView ";
	else
		strParentObjectName="QTableView#"+strParentObjectName+" ";

	QString strStyle;
	switch(m_nCurrentThemeID)
	{
	case THEME_BLUE_TEMPLE:
		{
			strStyle= strParentObjectName+"{show-decoration-selected: 1; color:white; selection-background-color:red; background:qradialgradient(cx:0.7, cy:0.3, radius: 1, fx:0.7, fy:0.3, stop:0 rgb(129,145,175), stop:1 rgb(70,82,106));}";
			strStyle+=strParentObjectName+"{border-style: solid; border-top-color: qlineargradient(x1:0, y1:0, x2:0, y2:1,stop:0 rgb(52,60,76), stop:1  rgb(78,90,113)); border-left-color: qlineargradient(x1:0, y1:0, x2:1, y2:0,stop:0 rgb(52,60,76), stop:1  rgb(78,90,113)); border-bottom-color: rgb(78,90,113); border-right-color: rgb(78,90,113); border-width: 3px 1px 1px 3px}";
			strStyle+=strParentObjectName+"{gridline-color: white; color: white;}";
			strStyle+="QComboBox  {color: white; background:transparent;} QLineEdit {color: black;}";
			strStyle+="QTableView:disabled {background: rgb(70,82,106)}";
			strStyle+="QHeaderView:section {color: white; background:rgb(70,82,106);} "+strParentObjectName+"QTableCornerButton::section {background:rgb(70,82,106);border: 1px}  QHeaderView {background:transparent;}";
			 
		}
		break;
	case THEME_GRAYHOUND:
		{
			strStyle= strParentObjectName+"{show-decoration-selected: 1; color:white; selection-background-color:red; background:qradialgradient(cx:0.7, cy:0.3, radius: 1, fx:0.7, fy:0.3, stop:0 rgb(161,161,161), stop:1 rgb(107,107,107));}";
			strStyle+=strParentObjectName+"{border-style: solid; border-top-color: qlineargradient(x1:0, y1:0, x2:0, y2:1,stop:0 rgb(74,74,74), stop:1  rgb(109,109,109)); border-left-color: qlineargradient(x1:0, y1:0, x2:1, y2:0,stop:0 rgb(74,74,74), stop:1  rgb(109,109,109)); border-bottom-color: rgb(109,109,109); border-right-color: rgb(109,109,109); border-width: 3px 1px 1px 3px}";
			strStyle+=strParentObjectName+"{gridline-color: white}";
			strStyle+="QComboBox  {color: white; background:transparent;} QLineEdit {color: black;}";
			strStyle+="QTableView:disabled {background: rgb(107,107,107)}";
			strStyle+="QHeaderView:section {color: white; background:rgb(107,107,107);}"+strParentObjectName+" QTableCornerButton::section {background:rgb(107,107,107);border: 1px}  QHeaderView {background:transparent;}";

		}
		break;
	case THEME_CABERNET:
		{
			strStyle= strParentObjectName+"{show-decoration-selected: 1; color:white; selection-background-color:red; background:qradialgradient(cx:0.7, cy:0.3, radius: 1, fx:0.7, fy:0.3, stop:0 rgb(156,86,112), stop:1 rgb(101,25,54));}";
			strStyle+=strParentObjectName+"{border-style: solid; border-top-color: qlineargradient(x1:0, y1:0, x2:0, y2:1,stop:0 rgb(71,25,42), stop:1  rgb(105,37,63)); border-left-color: qlineargradient(x1:0, y1:0, x2:1, y2:0,stop:0 rgb(71,25,42), stop:1  rgb(105,37,63)); border-bottom-color: rgb(105,37,63); border-right-color: rgb(105,37,63); border-width: 3px 1px 1px 3px}";
			strStyle+=strParentObjectName+"{gridline-color: white}";
			strStyle+="QComboBox  {color: white; background:transparent;} QLineEdit {color: black;}";
			strStyle+="QTableView:disabled {background: rgb(101,25,54)}";
			strStyle+="QHeaderView:section {color: white; background:rgb(101,25,54);}"+strParentObjectName+" QTableCornerButton::section {background:rgb(101,25,54);border: 1px}  QHeaderView {background:transparent;}";

		}
		break;
	case THEME_FOREST:
		{
			strStyle= strParentObjectName+"{show-decoration-selected: 1; color:white; selection-background-color:red; background:qradialgradient(cx:0.7, cy:0.3, radius: 1, fx:0.7, fy:0.3, stop:0 rgb(115,190,143), stop:1 rgb(23,107,53));}";
			strStyle+=strParentObjectName+"{border-style: solid; border-top-color: qlineargradient(x1:0, y1:0, x2:0, y2:1,stop:0 rgb(29,79,46), stop:1  rgb(45,117,70)); border-left-color: qlineargradient(x1:0, y1:0, x2:1, y2:0,stop:0 rgb(29,79,46), stop:1  rgb(45,117,70)); border-bottom-color: rgb(45,117,70); border-right-color: rgb(45,117,70); border-width: 3px 1px 1px 3px}";
			strStyle+=strParentObjectName+"{gridline-color: white}";
			strStyle+="QComboBox  {color: white; background:transparent;} QLineEdit {color: black;}";
			strStyle+="QTableView:disabled {background: rgb(23,107,53)}";
			strStyle+="QHeaderView:section {color: white; background:rgb(23,107,53);}"+strParentObjectName+" QTableCornerButton::section {background:rgb(23,107,53);border: 1px}  QHeaderView {background:transparent;}";
		}
		break;
	case THEME_SMARAGD:
		{
			strStyle= strParentObjectName+"{show-decoration-selected: 1; color:white; selection-background-color:red; background:qradialgradient(cx:0.7, cy:0.3, radius: 1, fx:0.7, fy:0.3, stop:0 rgb(97,148,158), stop:1 rgb(10,98,112));}";
			strStyle+=strParentObjectName+"{border-style: solid; border-top-color: qlineargradient(x1:0, y1:0, x2:0, y2:1,stop:0 rgb(35,77,84), stop:1  rgb(53,117,128)); border-left-color: qlineargradient(x1:0, y1:0, x2:1, y2:0,stop:0 rgb(35,77,84), stop:1  rgb(53,117,128)); border-bottom-color: rgb(53,117,128); border-right-color: rgb(53,117,128); border-width: 3px 1px 1px 3px}";
			strStyle+=strParentObjectName+"{gridline-color: white}";
			strStyle+="QComboBox  {color: white; background:transparent;} QLineEdit {color: black;}";
			strStyle+="QTableView:disabled {background: rgb(10,98,112)}";
			strStyle+="QHeaderView:section {color: white; background:rgb(10,98,112);}"+strParentObjectName+" QTableCornerButton::section {background:rgb(10,98,112);border: 1px}  QHeaderView {background:transparent;}";
		}
		break;
	default:
		strStyle= strParentObjectName+"{show-decoration-selected: 1; color:white; selection-background-color:red; background:qradialgradient(cx:0.7, cy:0.3, radius: 1, fx:0.7, fy:0.3, stop:0 rgb(129,145,175), stop:1 rgb(70,82,106));}";
		strStyle+=strParentObjectName+"{border-style: solid; border-top-color: qlineargradient(x1:0, y1:0, x2:0, y2:1,stop:0 rgb(52,60,76), stop:1  rgb(78,90,113)); border-left-color: qlineargradient(x1:0, y1:0, x2:1, y2:0,stop:0 rgb(52,60,76), stop:1  rgb(78,90,113)); border-bottom-color: rgb(78,90,113); border-right-color: rgb(78,90,113); border-width: 3px 1px 1px 3px}";
		strStyle+=strParentObjectName+"{gridline-color: white}";
		strStyle+="QComboBox  {color: white; background:transparent;} QLineEdit {color: black;}";
		strStyle+="QTableView:disabled {background: rgb(70,82,106)}";
		strStyle+="QHeaderView:section {color: white; background:rgb(70,82,106);}"+strParentObjectName+" QTableCornerButton::section {background:rgb(70,82,106);border: 1px}  QHeaderView {background:transparent;}";

		break;
	}

	return strStyle;
}

QString ThemeManager::GetGlobalWidgetStyle()
{
#ifdef NO_GRADIENT_BACKGROUND
	return "";
#endif

	QString strLineEdit="QLineEdit:disabled { color: black; background:rgb(229,229,229);}"; 
	strLineEdit+="QLineEdit { padding-left:3px; background:rgb(249,249,249); border-style: inset; border-top-color: qlineargradient(x1:0, y1:0, x2:0, y2:1,stop:0 rgb(145,145,145), stop:1  rgb(227,227,227)); border-left-color: qlineargradient(x1:0, y1:0, x2:1, y2:0,stop:0 rgb(145,145,145), stop:1  rgb(227,227,227)); border-bottom-color: rgb(145,145,145); border-right-color:rgb(145,145,145); border-width: 3px 1px 1px 3px}";
	strLineEdit+="QComboBox:disabled { color: black; background:rgb(229,229,229);}";
	strLineEdit+="QComboBox { padding-left:3px; background:rgb(249,249,249); border-style: inset; border-top-color: qlineargradient(x1:0, y1:0, x2:0, y2:1,stop:0 rgb(145,145,145), stop:1  rgb(227,227,227)); border-left-color: qlineargradient(x1:0, y1:0, x2:1, y2:0,stop:0 rgb(145,145,145), stop:1  rgb(227,227,227)); border-bottom-color: rgb(145,145,145); border-right-color:rgb(145,145,145); border-width: 3px 1px 1px 3px}";
	strLineEdit+="QComboBox::drop-down {image:none} QComboBox::down-arrow{image:url(:ControlBmp_Dropdown_Arrow.png)}";
	strLineEdit+="QComboBox::drop-down:on {image:none} QComboBox::down-arrow:on{image:url(:ControlBmp_Dropdown_Arrow_Pressed.png)}";

	strLineEdit+="QSpinBox:disabled { color: black; background:rgb(229,229,229);}";
	strLineEdit+="QSpinBox  { padding-left:3px; background:rgb(249,249,249); border-style: inset; border-top-color: qlineargradient(x1:0, y1:0, x2:0, y2:1,stop:0 rgb(145,145,145), stop:1  rgb(227,227,227)); border-left-color: qlineargradient(x1:0, y1:0, x2:1, y2:0,stop:0 rgb(145,145,145), stop:1  rgb(227,227,227)); border-bottom-color: rgb(145,145,145); border-right-color:rgb(145,145,145); border-width: 3px 1px 1px 3px}";
	strLineEdit+="QSpinBox::up-button {image:none} QSpinBox::up-arrow{image:url(:ControlBmp_Spinner_Top.png)}";
	strLineEdit+="QSpinBox::down-button {image:none} QSpinBox::down-arrow{image:url(:ControlBmp_Spinner_Bottom.png)}";
	
	strLineEdit+="QDateTimeEdit:disabled { color: black; background:rgb(229,229,229);}";
	strLineEdit+="QDateTimeEdit   { padding-left:3px; background:rgb(249,249,249); border-style: inset; border-top-color: qlineargradient(x1:0, y1:0, x2:0, y2:1,stop:0 rgb(145,145,145), stop:1  rgb(227,227,227)); border-left-color: qlineargradient(x1:0, y1:0, x2:1, y2:0,stop:0 rgb(145,145,145), stop:1  rgb(227,227,227)); border-bottom-color: rgb(145,145,145); border-right-color:rgb(145,145,145); border-width: 3px 1px 1px 3px}";
	strLineEdit+="QDateTimeEdit::up-button {image:none} QDateTimeEdit::up-arrow{image:url(:ControlBmp_Spinner_Top.png)}";
	strLineEdit+="QDateTimeEdit::down-button {image:url(no_pic)} QDateTimeEdit::down-arrow{image:url(:ControlBmp_Spinner_Bottom.png)}";
	/*
	strLineEdit+="QDateEdit:disabled { color: black; background:rgb(229,229,229);}";
	strLineEdit+="QDateEdit   { padding-left:3px; background:rgb(249,249,249); border-style: inset; border-top-color: qlineargradient(x1:0, y1:0, x2:0, y2:1,stop:0 rgb(145,145,145), stop:1  rgb(227,227,227)); border-left-color: qlineargradient(x1:0, y1:0, x2:1, y2:0,stop:0 rgb(145,145,145), stop:1  rgb(227,227,227)); border-bottom-color: rgb(145,145,145); border-right-color:rgb(145,145,145); border-width: 3px 1px 1px 3px}";
	strLineEdit+="QDateEdit::up-button {image:none} QDateEdit::up-arrow{image:url(:ControlBmp_Spinner_Top.png)}";
	strLineEdit+="QDateEdit::down-button {image:none} QDateEdit::down-arrow{image:url(:ControlBmp_Spinner_Bottom.png)}";
	*/

	strLineEdit+="QRadioButton::indicator::unchecked {image:url(:ControlBmp_Radio.png)}";
	strLineEdit+="QRadioButton::indicator::checked {image:url(:ControlBmp_Radio_Checked.png)}";
	strLineEdit+="QRadioButton::indicator::unchecked:disabled {image:url(:ControlBmp_Radio_Inactive.png)}";
	strLineEdit+="QRadioButton::indicator::checked:disabled {image:url(:ControlBmp_Radio_Inactive_Checked.png)}";
	strLineEdit+="QCheckBox::indicator::unchecked {image:url(:ControlBmp_Checkbox.png)}";
	strLineEdit+="QCheckBox::indicator::checked {image:url(:ControlBmp_Checkbox_Checked.png)}";
	strLineEdit+="QCheckBox::indicator::unchecked:disabled {image:url(:ControlBmp_Checkbox_Inactive.png)}";
	strLineEdit+="QCheckBox::indicator::checked:disabled {image:url(:ControlBmp_Checkbox_Inactive_Checked.png)}";
	strLineEdit+="QCheckBox:disabled { color: black } QRadioButton:disabled { color: black }";
	
	
	strLineEdit+="QGroupBox::indicator {width:13px; height:13px;}";
	strLineEdit+="QGroupBox::indicator::unchecked {image:url(:ControlBmp_Checkbox.png)}";
	strLineEdit+="QGroupBox::indicator::checked {image:url(:ControlBmp_Checkbox_Checked.png)}";
	strLineEdit+="QGroupBox::indicator::unchecked:disabled {image:url(:ControlBmp_Checkbox_Inactive.png)}";
	strLineEdit+="QGroupBox::indicator::checked:disabled {image:url(:ControlBmp_Checkbox_Inactive_Checked.png)}";
	strLineEdit+="QGroupBox:disabled { color: black }";
	strLineEdit+="QTextEdit {background:rgb(249,249,249); border-style: inset; border-top-color: qlineargradient(x1:0, y1:0, x2:0, y2:1,stop:0 rgb(145,145,145), stop:1  rgb(227,227,227)); border-left-color: qlineargradient(x1:0, y1:0, x2:1, y2:0,stop:0 rgb(145,145,145), stop:1  rgb(227,227,227)); border-bottom-color: rgb(145,145,145); border-right-color:rgb(145,145,145); border-width: 3px 1px 1px 3px}";

	//vert:
	strLineEdit+="QScrollBar:vertical {border-width: 1px; border-image:url(:ControlBmp_ScrollBar_Background_Vert.png) 1px 1px 1px 1px stretch stretch; width: 11px; margin: 11px 0 11px 0;}";
	strLineEdit+="QScrollBar::handle:vertical {border-width: 4px; border-image:url(:ControlBmp_ScrollBar_Slider_Vert.png) 4px 0px 4px 0px stretch stretch; min-height: 32px; }"; 
	strLineEdit+="QScrollBar::add-line:vertical {border-image:url(:ControlBmp_ScrollBar_Down.png);width: 11px; height: 11px; subcontrol-position: bottom; subcontrol-origin: margin;}";
	strLineEdit+="QScrollBar::sub-line:vertical {border-image:url(:ControlBmp_ScrollBar_Up.png); width: 11px; height: 11px; subcontrol-position: top; subcontrol-origin: margin;}"; 
	strLineEdit+="QScrollBar::up-arrow:vertical {image: none;} QScrollBar::down-arrow:vertical {image: none;}";

	//horz:
	strLineEdit+="QScrollBar:horizontal {border-width: 1px; border-image:url(:ControlBmp_ScrollBar_Background_Horz.png) 1px 1px 1px 1px stretch stretch; height: 11px; margin: 0px 11px 0px 11px;}";
	strLineEdit+="QScrollBar::handle:horizontal {border-width: 4px; border-image:url(:ControlBmp_ScrollBar_Slider_Horz.png) 4px 4px 4px 4px stretch stretch; min-width: 32px;}"; 
	strLineEdit+="QScrollBar::add-line:horizontal {border-image:url(:ControlBmp_ScrollBar_Right.png);height: 11px; width: 11px; subcontrol-position: right; subcontrol-origin: margin;}";
	strLineEdit+="QScrollBar::sub-line:horizontal {border-image:url(:ControlBmp_ScrollBar_Left.png);height: 11px; width: 11px; subcontrol-position: left; subcontrol-origin: margin;}"; 
	strLineEdit+="QScrollBar::left-arrow:horizontal {image: none;} QScrollBar::right-arrow:horizontal {image: none;}";


	strLineEdit+="QSpinBox  { padding-left:3px; background:rgb(249,249,249); border-style: inset; border-top-color: qlineargradient(x1:0, y1:0, x2:0, y2:1,stop:0 rgb(145,145,145), stop:1  rgb(227,227,227)); border-left-color: qlineargradient(x1:0, y1:0, x2:1, y2:0,stop:0 rgb(145,145,145), stop:1  rgb(227,227,227)); border-bottom-color: rgb(145,145,145); border-right-color:rgb(145,145,145); border-width: 3px 1px 1px 3px}";
	strLineEdit+="QSpinBox::up-button {image:none} QSpinBox::up-arrow{image:url(:ControlBmp_Spinner_Top.png)}";
	strLineEdit+="QSpinBox::down-button {image:none} QSpinBox::down-arrow{image:url(:ControlBmp_Spinner_Bottom.png)}";



	return strLineEdit;
}

QString ThemeManager::GetLisViewBkg()
{
#ifdef NO_GRADIENT_BACKGROUND
	return "";
#endif

	QString strStyle;
	switch(m_nCurrentThemeID)
	{
	case THEME_BLUE_TEMPLE:
			{
				strStyle= "QListView {show-decoration-selected: 1; color:white; selection-background-color:red; background:qradialgradient(cx:0.7, cy:0.3, radius: 1, fx:0.7, fy:0.3, stop:0 rgb(129,145,175), stop:1 rgb(70,82,106));}";
				strStyle+="QListView:disabled {background: rgb(70,82,106)}";
				//strStyle+="QListView {border-style: solid; border-top-color: qlineargradient(x1:0, y1:0, x2:0, y2:1,stop:0 rgb(52,60,76), stop:1  rgb(78,90,113)); border-left-color: qlineargradient(x1:0, y1:0, x2:1, y2:0,stop:0 rgb(52,60,76), stop:1  rgb(78,90,113)); border-bottom-color: rgb(78,90,113); border-right-color: rgb(78,90,113); border-width: 3px 1px 1px 3px}";
			}
		break;
		case THEME_GRAYHOUND:
			{
				strStyle= "QListView {show-decoration-selected: 1; color:white; selection-background-color:red; background:qradialgradient(cx:0.7, cy:0.3, radius: 1, fx:0.7, fy:0.3, stop:0 rgb(161,161,161), stop:1 rgb(107,107,107));}";
				strStyle+="QListView:disabled {background: rgb(107,107,107)}";
				//strStyle+="QListView {border-style: solid; border-top-color: qlineargradient(x1:0, y1:0, x2:0, y2:1,stop:0 rgb(74,74,74), stop:1  rgb(109,109,109)); border-left-color: qlineargradient(x1:0, y1:0, x2:1, y2:0,stop:0 rgb(74,74,74), stop:1  rgb(109,109,109)); border-bottom-color: rgb(109,109,109); border-right-color: rgb(109,109,109); border-width: 3px 1px 1px 3px}";
			}
		break;
		case THEME_CABERNET:
			{
				strStyle= "QListView {show-decoration-selected: 1; color:white; selection-background-color:red;}";
				strStyle+="QListView {background:qradialgradient(cx:0.7, cy:0.3, radius: 1, fx:0.7, fy:0.3, stop:0 rgb(156,86,112), stop:1 rgb(101,25,54));}";
				strStyle+="QListView:disabled {background: rgb(101,25,54)}";
				//strStyle+="QListView {border-style: solid; border-top-color: qlineargradient(x1:0, y1:0, x2:0, y2:1,stop:0 rgb(71,25,42), stop:1  rgb(105,37,63)); border-left-color: qlineargradient(x1:0, y1:0, x2:1, y2:0,stop:0 rgb(71,25,42), stop:1  rgb(105,37,63)); border-bottom-color: rgb(105,37,63); border-right-color: rgb(105,37,63); border-width: 3px 1px 1px 3px}";
			}
		break;
		case THEME_FOREST:
			{
				strStyle= "QListView {show-decoration-selected: 1; color:white; selection-background-color:red; background:qradialgradient(cx:0.7, cy:0.3, radius: 1, fx:0.7, fy:0.3, stop:0 rgb(115,190,143), stop:1 rgb(23,107,53));}";
				strStyle+="QListView:disabled {background: rgb(23,107,53)}";
				//strStyle+="QListView {border-style: solid; border-top-color: qlineargradient(x1:0, y1:0, x2:0, y2:1,stop:0 rgb(29,79,46), stop:1  rgb(45,117,70)); border-left-color: qlineargradient(x1:0, y1:0, x2:1, y2:0,stop:0 rgb(29,79,46), stop:1  rgb(45,117,70)); border-bottom-color: rgb(45,117,70); border-right-color: rgb(45,117,70); border-width: 3px 1px 1px 3px}";
			}

		break;
		case THEME_SMARAGD:
			{
				strStyle= "QListView {show-decoration-selected: 1; color:white; selection-background-color:red; background:qradialgradient(cx:0.7, cy:0.3, radius: 1, fx:0.7, fy:0.3, stop:0 rgb(97,148,158), stop:1 rgb(10,98,112));}";
				strStyle+="QListView:disabled {background: rgb(10,98,112)}";
				//strStyle+="QListView {border-style: solid; border-top-color: qlineargradient(x1:0, y1:0, x2:0, y2:1,stop:0 rgb(35,77,84), stop:1  rgb(53,117,128)); border-left-color: qlineargradient(x1:0, y1:0, x2:1, y2:0,stop:0 rgb(35,77,84), stop:1  rgb(53,117,128)); border-bottom-color: rgb(53,117,128); border-right-color: rgb(53,117,128); border-width: 3px 1px 1px 3px}";
			}

		break;
		default:
			strStyle= "QListView {show-decoration-selected: 1; color:white; selection-background-color:red; background:qradialgradient(cx:0.7, cy:0.3, radius: 1, fx:0.7, fy:0.3, stop:0 rgb(129,145,175), stop:1 rgb(70,82,106));}";
			strStyle+="QListView:disabled {background: rgb(70,82,106)}";
			//strStyle+="QListView {border-style: solid; border-top-color: qlineargradient(x1:0, y1:0, x2:0, y2:1,stop:0 rgb(52,60,76), stop:1  rgb(78,90,113)); border-left-color: qlineargradient(x1:0, y1:0, x2:1, y2:0,stop:0 rgb(52,60,76), stop:1  rgb(78,90,113)); border-bottom-color: rgb(78,90,113); border-right-color: rgb(78,90,113); border-width: 3px 1px 1px 3px}";
		break;
	}

	return strStyle;

}


QString ThemeManager::GetTreeBkg()
{
#ifdef NO_GRADIENT_BACKGROUND
	return "";
#endif
	/*
	Forest: (29,79,46),(36,94,57),(45,117,70)
	Blue Temple: (52,60,76),(65,74,93),(78,90,113)
	Cabernet: (71,25,42),(83,30,50),(105,37,63)
	Grayhound: (74,74,74),(85,85,85),(109,109,109)

	These are the gradients for the radial backgrounds:
	Blue Temple: (129,145,175)-(70,82,106)
	Grayhound: (161,161,161)-(107,107,107)
	Cabernet: (156,86,112)-(101,25,54)
	Forest: (115,190,143)-(23,107,53)

	*/
	QString strStyle;
	switch(m_nCurrentThemeID)
	{
	case THEME_BLUE_TEMPLE:
			{
				strStyle= "QTreeWidget {show-decoration-selected: 1; color:white; selection-background-color:red; background:qradialgradient(cx:0.7, cy:0.3, radius: 1, fx:0.7, fy:0.3, stop:0 rgb(129,145,175), stop:1 rgb(70,82,106));}";
				strStyle+="QTreeWidget:disabled {background: rgb(70,82,106)}";
				//strStyle+="QTreeWidget {border-style: solid; border-top-color: qlineargradient(x1:0, y1:0, x2:0, y2:1,stop:0 rgb(52,60,76), stop:1  rgb(78,90,113)); border-left-color: qlineargradient(x1:0, y1:0, x2:1, y2:0,stop:0 rgb(52,60,76), stop:1  rgb(78,90,113)); border-bottom-color: rgb(78,90,113); border-right-color: rgb(78,90,113); border-width: 3px 1px 1px 3px}";
			}
		break;
		case THEME_GRAYHOUND:
			{
				strStyle= "QTreeWidget {show-decoration-selected: 1; color:white; selection-background-color:red; background:qradialgradient(cx:0.7, cy:0.3, radius: 1, fx:0.7, fy:0.3, stop:0 rgb(161,161,161), stop:1 rgb(107,107,107));}";
				strStyle+="QTreeWidget:disabled {background: rgb(107,107,107)}";
				//strStyle+="QTreeWidget {border-style: solid; border-top-color: qlineargradient(x1:0, y1:0, x2:0, y2:1,stop:0 rgb(74,74,74), stop:1  rgb(109,109,109)); border-left-color: qlineargradient(x1:0, y1:0, x2:1, y2:0,stop:0 rgb(74,74,74), stop:1  rgb(109,109,109)); border-bottom-color: rgb(109,109,109); border-right-color: rgb(109,109,109); border-width: 3px 1px 1px 3px}";
			}
		break;
		case THEME_CABERNET:
			{
				strStyle= "QTreeWidget {show-decoration-selected: 1; color:white; selection-background-color:red;}";
				strStyle+="QTreeWidget {background:qradialgradient(cx:0.7, cy:0.3, radius: 1, fx:0.7, fy:0.3, stop:0 rgb(156,86,112), stop:1 rgb(101,25,54));}";
				strStyle+="QTreeWidget:disabled {background: rgb(101,25,54)}";
				//strStyle+="QTreeWidget {border-style: solid; border-top-color: qlineargradient(x1:0, y1:0, x2:0, y2:1,stop:0 rgb(71,25,42), stop:1  rgb(105,37,63)); border-left-color: qlineargradient(x1:0, y1:0, x2:1, y2:0,stop:0 rgb(71,25,42), stop:1  rgb(105,37,63)); border-bottom-color: rgb(105,37,63); border-right-color: rgb(105,37,63); border-width: 3px 1px 1px 3px}";
			}
		break;
		case THEME_FOREST:
			{
				strStyle= "QTreeWidget {show-decoration-selected: 1; color:white; selection-background-color:red; background:qradialgradient(cx:0.7, cy:0.3, radius: 1, fx:0.7, fy:0.3, stop:0 rgb(115,190,143), stop:1 rgb(23,107,53));}";
				strStyle+="QTreeWidget:disabled {background: rgb(23,107,53)}";
				//strStyle+="QTreeWidget {border-style: solid; border-top-color: qlineargradient(x1:0, y1:0, x2:0, y2:1,stop:0 rgb(29,79,46), stop:1  rgb(45,117,70)); border-left-color: qlineargradient(x1:0, y1:0, x2:1, y2:0,stop:0 rgb(29,79,46), stop:1  rgb(45,117,70)); border-bottom-color: rgb(45,117,70); border-right-color: rgb(45,117,70); border-width: 3px 1px 1px 3px}";
			}

		break;
		case THEME_SMARAGD:
			{
				strStyle= "QTreeWidget {show-decoration-selected: 1; color:white; selection-background-color:red; background:qradialgradient(cx:0.7, cy:0.3, radius: 1, fx:0.7, fy:0.3, stop:0 rgb(97,148,158), stop:1 rgb(10,98,112));}";
				strStyle+="QTreeWidget:disabled {background: rgb(10,98,112)}";
				//strStyle+="QTreeWidget {border-style: solid; border-top-color: qlineargradient(x1:0, y1:0, x2:0, y2:1,stop:0 rgb(35,77,84), stop:1  rgb(53,117,128)); border-left-color: qlineargradient(x1:0, y1:0, x2:1, y2:0,stop:0 rgb(35,77,84), stop:1  rgb(53,117,128)); border-bottom-color: rgb(53,117,128); border-right-color: rgb(53,117,128); border-width: 3px 1px 1px 3px}";
			}

		break;
		default:
			strStyle= "QTreeWidget {show-decoration-selected: 1; color:white; selection-background-color:red; background:qradialgradient(cx:0.7, cy:0.3, radius: 1, fx:0.7, fy:0.3, stop:0 rgb(129,145,175), stop:1 rgb(70,82,106));}";
			strStyle+="QTreeWidget:disabled {background: rgb(70,82,106)}";
			strStyle+="QTreeWidget {border-style: solid; border-top-color: qlineargradient(x1:0, y1:0, x2:0, y2:1,stop:0 rgb(52,60,76), stop:1  rgb(78,90,113)); border-left-color: qlineargradient(x1:0, y1:0, x2:1, y2:0,stop:0 rgb(52,60,76), stop:1  rgb(78,90,113)); border-bottom-color: rgb(78,90,113); border-right-color: rgb(78,90,113); border-width: 3px 1px 1px 3px}";
		break;
	}

	strStyle+="QTreeView::branch:has-siblings:!adjoins-item {border-image: url(no_pic)}";
	strStyle+="QTreeView::branch:has-siblings:adjoins-item {border-image: url(no_pic)}";
	strStyle+="QTreeView::branch:!has-children:!has-siblings:adjoins-item {border-image: url(no_pic)}";
	strStyle+="QTreeView::branch:!has-siblings {border: none}";
	strStyle+="QTreeView::branch:has-children:!has-siblings:closed,QTreeView::branch:closed:has-children:has-siblings {border-image: none; image: url(:ControlBmp_Tree_Plus.png)}";
	strStyle+="QTreeView::branch:open:has-children:!has-siblings,QTreeView::branch:open:has-children:has-siblings  {border-image: none;image: url(:ControlBmp_Tree_Minus.png)}";
	return strStyle;
		 

}

QString ThemeManager::GetMobileFrameNameBkg()
{
#ifdef NO_GRADIENT_BACKGROUND
	return "";
#endif

	switch(m_nCurrentThemeID)
	{
	case THEME_BLUE_TEMPLE:
		return "{background-color: rgb(147,171,187)}";
		break;
	case THEME_GRAYHOUND:
		return "{background-color: rgb(233,233,233)}";
		break;
	case THEME_CABERNET:
		return "{background-color: rgb(244,238,240)}";
		break;
	case THEME_FOREST:
		return "{background-color: rgb(239,248,243)}";
		break;
	case THEME_SMARAGD:
		return "{background-color: rgb(237,243,245)}";
		break;

	default:
		return "{background-color: rgb(147,171,187)}";
		break;
	}
}

QString ThemeManager::GetMobileBkg()
{
#ifdef NO_GRADIENT_BACKGROUND
	return "";
#endif

	QString strStyle;
	switch(m_nCurrentThemeID)
	{
	case THEME_BLUE_TEMPLE:
		{
			strStyle= " {background:qradialgradient(cx:0.5, cy:0.5, radius: 1, fx:0.5, fy:0.5, stop:0 rgb(129,145,175), stop:1 rgb(70,82,106));} QLabel {color:white}";
		}
		break;
	case THEME_GRAYHOUND:
		{
			strStyle= "{ background:qradialgradient(cx:0.5, cy:0.5, radius: 1, fx:0.5 fy:0.5, stop:0 rgb(161,161,161), stop:1 rgb(107,107,107));} QLabel {color:white}";
		}
		break;
	case THEME_CABERNET:
		{
			strStyle= " { background:qradialgradient(cx:0.5, cy:0.5, radius: 1, fx:0.5, fy:0.5, stop:0 rgb(156,86,112), stop:1 rgb(101,25,54));} QLabel {color:white}";
		}
		break;
	case THEME_FOREST:
		{
			strStyle= " { background:qradialgradient(cx:0.5, cy:0.5, radius: 1, fx:0.5, fy:0.5, stop:0 rgb(115,190,143), stop:1 rgb(23,107,53));} QLabel {color:white}";
		}
		break;
	case THEME_SMARAGD:
		{
			strStyle= " { background:qradialgradient(cx:0.5, cy:0.5, radius: 1, fx:0.5, fy:0.5, stop:0 rgb(97,148,158), stop:1 rgb(10,98,112));} QLabel {color:white}";
		}
		break;
	default:
		strStyle= " { background:qradialgradient(cx:0.5, cy:0.5, radius: 1, fx:0.5, fy:0.5, stop:0 rgb(129,145,175), stop:1 rgb(70,82,106));} QLabel {color:white}";
		break;
	}

	return strStyle;
}

QString ThemeManager::GetMobileSolidBkg_Dark()
{
#ifdef NO_GRADIENT_BACKGROUND
	return "";
#endif

	QString strStyle;
	switch(m_nCurrentThemeID)
	{
	case THEME_BLUE_TEMPLE:
		{
			strStyle= " {background:rgb(70,82,106)}";
		}
		break;
	case THEME_GRAYHOUND:
		{
			strStyle= " {background:rgb(107,107,107)}";
		}
		break;
	case THEME_CABERNET:
		{
			strStyle= " {background:rgb(101,25,54);}";
		}
		break;
	case THEME_FOREST:
		{
			strStyle= " {background:rgb(23,107,53)}";
		}
		break;
	case THEME_SMARAGD:
		{
			strStyle= " {background:rgb(10,98,112)}";
		}
		break;
	default:
		strStyle= " {background:rgb(70,82,106)}";
		break;
	}

	return strStyle;

}

QString ThemeManager::GetMobileSolidBkg_Light()
{
#ifdef NO_GRADIENT_BACKGROUND
	return "";
#endif

	QString strStyle;
	switch(m_nCurrentThemeID)
	{
	case THEME_BLUE_TEMPLE:
		{
			strStyle= " {background:rgb(129,145,175)}";
		}
		break;
	case THEME_GRAYHOUND:
		{
			strStyle= " {background:rgb(161,161,161)}";
		}
		break;
	case THEME_CABERNET:
		{
			strStyle= " {background:rgb(156,86,112)}";
		}
		break;
	case THEME_FOREST:
		{
			strStyle= " {background:rgb(115,190,143)}";
		}
		break;
	case THEME_SMARAGD:
		{
			strStyle= " {background:rgb(97,148,158)}";
		}
		break;
	default:
		strStyle= " {background:rgb(129,145,175)}";
		break;
	}

	return strStyle;

}

void ThemeManager::ExtractRGBFromColorString(QString strColor,int &R,int &G,int &B)
{
	R=0;G=0;B=0;
	int nIndex=strColor.indexOf("rgb(");
	if (nIndex<0)return;
	int nIndex2=strColor.indexOf(")",nIndex);
	if (nIndex2<0)return;
	nIndex=nIndex+4;
	int nLen=nIndex2-nIndex;
	
	strColor=strColor.mid(nIndex,nLen).trimmed();

	QStringList lst= strColor.split(",",QString::SkipEmptyParts);
	if (lst.size()!=3)
		return;
	R=lst.at(0).toInt();
	G=lst.at(1).toInt();
	B=lst.at(2).toInt();
}



QString ThemeManager::GetMobileChapter_Font()
{
#ifdef NO_GRADIENT_BACKGROUND
	return "";
#endif

	switch(m_nCurrentThemeID)
	{
	case THEME_BLUE_TEMPLE:
		return  "{font-weight:900;font-style:normal;font-family:Arial; font-size:9pt; color:white}"; 
		break;
	case THEME_GRAYHOUND:
		return  "{font-weight:900;font-style:normal;font-family:Arial; font-size:9pt; color:white}"; 
		break;
	case THEME_CABERNET:
		return  "{font-weight:900;font-style:normal;font-family:Arial; font-size:9pt; color:white}"; 
		break;
	case THEME_FOREST:
		return  "{font-weight:900;font-style:normal;font-family:Arial; font-size:9pt; color:white}"; 
		break;
	case THEME_SMARAGD:
		return  "{font-weight:900;font-style:normal;font-family:Arial; font-size:9pt; color:white}"; 
		break;
	default:
		return  "{font-weight:900;font-style:normal;font-family:Arial; font-size:9pt; color:white}"; 
		break;
	}
}

QString ThemeManager::GetMobileControls_Font()
{
#ifdef NO_GRADIENT_BACKGROUND
	return "";
#endif

	switch(m_nCurrentThemeID)
	{
	case THEME_BLUE_TEMPLE:
		return  "{font-style:normal;font-family:Arial; font-size:8pt; color:white}"; 
		break;
	case THEME_GRAYHOUND:
		return  "{font-style:normal;font-family:Arial; font-size:8pt; color:white}"; 
		break;
	case THEME_CABERNET:
		return  "{font-style:normal;font-family:Arial; font-size:8pt; color:white}"; 
		break;
	case THEME_FOREST:
		return  "{font-style:normal;font-family:Arial; font-size:8pt; color:white}"; 
		break;
	case THEME_SMARAGD:
		return  "{font-style:normal;font-family:Arial; font-size:8pt; color:white}"; 
		break;
	default:
		return  "{font-style:normal;font-family:Arial; font-size:8pt; color:white}"; 
		break;
	}
}

QString ThemeManager::GetMobileStatusBarBackground()
{
#ifdef NO_GRADIENT_BACKGROUND
	return "";
#endif
	QString style("QFrame#summary_frame ");
	style += QString(MOBILE_BKG_BORDER_STYLE).arg(m_strThemePrefix+"_Mobile_Status_Background.png"); 
	return style;
}

QString ThemeManager::GetMobileLeftHeaderBackground()
{
#ifdef NO_GRADIENT_BACKGROUND
	return "";
#endif
	QString style("QFrame#left_frame ");
	style += QString(MOBILE_HEADER_BKG_BORDER_STYLE).arg(m_strThemePrefix+"_Mobile_Header_Left_Background.png"); 
	return style;
}

QString ThemeManager::GetMobileCenterHeaderBackground()
{
#ifdef NO_GRADIENT_BACKGROUND
	return "";
#endif
	QString style("QFrame#center_frame ");
	style += QString(MOBILE_HEADER_BKG_BORDER_STYLE).arg(m_strThemePrefix+"_Mobile_Header_Center_Background.png"); 
	return style;
}

QString ThemeManager::GetMobileRightHeaderBackground()
{
	QString style("QFrame#right_frame ");
	style += QString(MOBILE_HEADER_BKG_BORDER_STYLE).arg(m_strThemePrefix+"_Mobile_Header_Right_Background.png"); 
	return style;
}

QString ThemeManager::GetTabBarStyle()
{
#ifdef NO_GRADIENT_BACKGROUND
	return "";
#endif
	QString strStyleTAB="QTabBar::tab:selected { margin-bottom: -4px;padding-bottom: 6px; color: white; border-left: 4px; border-right: 4px; border-top: 4px; border-bottom: 0px; border-image:url(%1) 4px 4px 4px 4px stretch stretch }";
	strStyleTAB+=" QTabBar::tab:!selected { margin-top: 2px; margin-bottom: -4px;padding-bottom: 6px; color: white; border-left: 4px; border-right: 4px; border-top: 4px; border-bottom: 0px; border-image:url(%2) 4px 4px 4px 4px stretch stretch }";
	strStyleTAB+=" QTabBar::tab:!selected:hover  { margin-top: 2px; margin-bottom: -4px;padding-bottom: 6px; color: white; border-left: 4px; border-right: 4px; border-top: 4px; border-bottom: 0px; border-image:url(%3) 4px 4px 4px 4px stretch stretch }";
	
	strStyleTAB =strStyleTAB.arg(m_strThemePrefix+"_CEMenuButton.png").arg(m_strThemePrefix+"_CEMenuButton_Hover.png").arg(m_strThemePrefix+"_Button_DarkBlueGray_VeryLight.png");
	return strStyleTAB;
}



QString ThemeManager::GetBorderStyle()
{
#ifdef NO_GRADIENT_BACKGROUND
	return "";
#endif

	QString strStyle;
	switch(m_nCurrentThemeID)
	{
	case THEME_BLUE_TEMPLE:
		{
			strStyle+=" {border-style: solid; border-top-color: qlineargradient(x1:0, y1:0, x2:0, y2:1,stop:0 rgb(52,60,76), stop:1  rgb(78,90,113)); border-left-color: qlineargradient(x1:0, y1:0, x2:1, y2:0,stop:0 rgb(52,60,76), stop:1  rgb(78,90,113)); border-bottom-color: rgb(78,90,113); border-right-color: rgb(78,90,113); border-width: 3px 1px 1px 3px}";
		}
		break;
	case THEME_GRAYHOUND:
		{
			strStyle+=" {border-style: solid; border-top-color: qlineargradient(x1:0, y1:0, x2:0, y2:1,stop:0 rgb(74,74,74), stop:1  rgb(109,109,109)); border-left-color: qlineargradient(x1:0, y1:0, x2:1, y2:0,stop:0 rgb(74,74,74), stop:1  rgb(109,109,109)); border-bottom-color: rgb(109,109,109); border-right-color: rgb(109,109,109); border-width: 3px 1px 1px 3px}";
		}
		break;
	case THEME_CABERNET:
		{
			strStyle+=" {border-style: solid; border-top-color: qlineargradient(x1:0, y1:0, x2:0, y2:1,stop:0 rgb(71,25,42), stop:1  rgb(105,37,63)); border-left-color: qlineargradient(x1:0, y1:0, x2:1, y2:0,stop:0 rgb(71,25,42), stop:1  rgb(105,37,63)); border-bottom-color: rgb(105,37,63); border-right-color: rgb(105,37,63); border-width: 3px 1px 1px 3px}";
		}
		break;
	case THEME_FOREST:
		{
			strStyle+=" {border-style: solid; border-top-color: qlineargradient(x1:0, y1:0, x2:0, y2:1,stop:0 rgb(29,79,46), stop:1  rgb(45,117,70)); border-left-color: qlineargradient(x1:0, y1:0, x2:1, y2:0,stop:0 rgb(29,79,46), stop:1  rgb(45,117,70)); border-bottom-color: rgb(45,117,70); border-right-color: rgb(45,117,70); border-width: 3px 1px 1px 3px}";
		}
		break;
	case THEME_SMARAGD:
		{
			strStyle+=" {border-style: solid; border-top-color: qlineargradient(x1:0, y1:0, x2:0, y2:1,stop:0 rgb(35,77,84), stop:1  rgb(53,117,128)); border-left-color: qlineargradient(x1:0, y1:0, x2:1, y2:0,stop:0 rgb(35,77,84), stop:1  rgb(53,117,128)); border-bottom-color: rgb(53,117,128); border-right-color: rgb(53,117,128); border-width: 3px 1px 1px 3px}";
		}

		break;
	default:
		strStyle+=" {border-style: solid; border-top-color: qlineargradient(x1:0, y1:0, x2:0, y2:1,stop:0 rgb(52,60,76), stop:1  rgb(78,90,113)); border-left-color: qlineargradient(x1:0, y1:0, x2:1, y2:0,stop:0 rgb(52,60,76), stop:1  rgb(78,90,113)); border-bottom-color: rgb(78,90,113); border-right-color: rgb(78,90,113); border-width: 3px 1px 1px 3px}";
		break;
	}

	return strStyle;
}