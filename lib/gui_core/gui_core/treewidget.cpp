#include "treewidget.h"
#include <QMenu>

TreeWidget::TreeWidget(QWidget *parent)
	: QTreeWidget(parent)
{

}

TreeWidget::~TreeWidget()
{

}


void TreeWidget::contextMenuEvent(QContextMenuEvent *event)
{
	QMenu ContextMenu(this);

	int nSize=m_lstActions.size();
	for(int i=0;i<nSize;++i)
		ContextMenu.addAction(m_lstActions.at(i));

	ContextMenu.exec(event->globalPos());
}