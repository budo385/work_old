#ifndef GUIFIELD_DOUBLESPINBOX_H
#define GUIFIELD_DOUBLESPINBOX_H

#include "guifield.h"
#include <QDoubleSpinBox>



/*!
	\class GuiField_DoubleSpinBox
	\brief Defines DoubleSpinBox
	\ingroup GUICore_GUIFields

	Connects to datasource
*/
class GuiField_DoubleSpinBox : public GuiField
{
	Q_OBJECT

public:
	GuiField_DoubleSpinBox(QString strFieldName,QWidget *pWidget,GuiDataManipulator* pDataManager,DbView *TableView=NULL, QLabel *pLabel=NULL);

	void RefreshDisplay(); //DATA SOURCE -> UI


private:
	QDoubleSpinBox* m_pEditWidget;
	int m_nDataType;

public slots:
		void FieldChanged(); //UI -> DATA SOURCE

};

#endif // GUIFIELD_DATEEDIT_H


