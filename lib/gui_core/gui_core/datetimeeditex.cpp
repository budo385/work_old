#include "datetimeeditex.h"
#include <QLineEdit>
#include <QDebug>
#include <QLocale>
#include <QCalendarWidget>
#include <QApplication>
#include <QTimer>



DateTimeEditEx::DateTimeEditEx(QWidget *parent)
	: QDateTimeEdit(parent)
{
	QString strFormat=QLocale::system().dateFormat(QLocale::ShortFormat);
	setDisplayFormat(strFormat);
	setCalendarPopup(true);
	m_invalidDateTime=QDateTime::currentDateTime(); //QDateTime(QDate(1900,1,1),QTime(0,0));
	int mins=(m_invalidDateTime.time().minute()/5)*5; //issue 2188
	int hrs=m_invalidDateTime.time().hour();
	m_invalidDateTime.setTime(QTime(hrs,mins,0,0));

	//m_invalidDateTime=QDateTime(QDate(1900,1,1),QTime(0,0));
	m_bSettingValueInProgress=false;

	connect(lineEdit(),SIGNAL(editingFinished()),this,SLOT(OnTextChanged()));
	connect(lineEdit(),SIGNAL(textEdited ( const QString &)),this,SLOT(OnTextEdited(const QString &)));
	connect(lineEdit(),SIGNAL(editingFinished()),this,SLOT(focusFromLineEdit()));
	connect(calendarWidget(),SIGNAL(clicked ( const QDate & )),this,SLOT(OnPopUpDateSelected( const QDate &)));

	calendarWidget()->setFirstDayOfWeek(Qt::Monday);
}

DateTimeEditEx::~DateTimeEditEx()
{

}

void DateTimeEditEx::stepBy ( int steps )
{
	QDateTimeEdit::stepBy(steps);
}

//by default: it is used as Date Edit, to use as DateTime or Time use those functions below:
void DateTimeEditEx::useAsDateTimeEdit(bool bNoSecondSection)
{
	QString strFormat=displayFormat()+" HH:mm:ss";
	if (bNoSecondSection)
	{
		strFormat =displayFormat()+" HH:mm";
	}
	setDisplayFormat(strFormat);
}

void DateTimeEditEx::useAsTimeEdit()
{
	QString strFormat="HH:mm:ss";
	setDisplayFormat(strFormat);
	setCalendarPopup(false);
}

void DateTimeEditEx::setReadOnly(bool bReadOnly)
{
	lineEdit()->setReadOnly(bReadOnly);
}

void DateTimeEditEx::setDateTime ( const QDateTime & dateTime )
{
	m_bSettingValueInProgress=true;

	if (dateTime.isNull())
	{
		blockSignals(true);
		QDateTimeEdit::setDateTime(m_invalidDateTime);
		lineEdit()->blockSignals(true);
		lineEdit()->setText("");
		lineEdit()->blockSignals(false);
		blockSignals(false);
	}
	else
		QDateTimeEdit::setDateTime(dateTime);

	m_bSettingValueInProgress=false;

}
void DateTimeEditEx::setDate ( const QDate & date )
{
	m_bSettingValueInProgress=true;
	if (date.isNull())
	{
		blockSignals(true);
		QDateTimeEdit::setDateTime(m_invalidDateTime);
		lineEdit()->blockSignals(true);
		lineEdit()->setText("");
		lineEdit()->blockSignals(false);
		blockSignals(false);
	}
	else
		QDateTimeEdit::setDate(date);

	m_bSettingValueInProgress=false;

}
void DateTimeEditEx::setTime ( const QTime & time )
{
	m_bSettingValueInProgress=true;
	if (time.isNull())
	{
		blockSignals(true);
		QDateTimeEdit::setDateTime(m_invalidDateTime);
		lineEdit()->blockSignals(true);
		lineEdit()->setText("");
		lineEdit()->blockSignals(false);
		blockSignals(false);
	}
	else
		QDateTimeEdit::setTime(time);

	m_bSettingValueInProgress=false;
}






QTime DateTimeEditEx::time() const
{
	if (lineEdit()->text()=="")
	{
		QTime timeInvalid;
		return timeInvalid;
	}
	else
		return QDateTimeEdit::time();

}


QDate DateTimeEditEx::date() const
{
	if (lineEdit()->text()=="")
	{
		QDate dateInvalid;
		return dateInvalid;
	}
	else
		return QDateTimeEdit::date();

}

QDateTime DateTimeEditEx::dateTime () const
{
	if (lineEdit()->text()=="")
	{
		QDateTime dateInvalid;
		return dateInvalid;
	}
	else
		return QDateTimeEdit::dateTime();
}



QValidator::State DateTimeEditEx::validate ( QString & input, int & pos ) const
{
	if (input=="")
	{
		if (lineEdit()->text()=="")
			return QValidator::Acceptable;
	}

	return QDateTimeEdit::validate ( input, pos );
}


void DateTimeEditEx::keyPressEvent ( QKeyEvent * event )
{

	

	if (lineEdit()->text()=="")
	{

		blockSignals(true);
		int nKey=event->key();

		QDateTime dateC=QDateTime::currentDateTime();
		int mins=(dateC.time().minute()/5)*5; //issue 2188
		int hrs=dateC.time().hour();
		
		if (nKey>=Qt::Key_0 && nKey <= Qt::Key_9)
		{
			dateC.setDate(QDate(dateC.date().year(),dateC.date().month(),nKey-48));
			//qDebug()<<dateC.toString();
		}

		dateC.setTime(QTime(hrs,mins,0,0));

		setDateTime(dateC); //if keypress: set current date if was empty
		blockSignals(false); //block all
		setCurrentSection(QDateTimeEdit::DaySection);
		lineEdit()->cursorForward(false,1);
		//setSelectedSection(QDateTimeEdit::DaySection | QDateTimeEdit::MonthSection); //QDateTimeEdit::DaySection);
		emit timeChanged(dateC.time());
		emit dateChanged(dateC.date());
		emit dateTimeChanged(dateC);
		return;
		//setDateTime(QDateTime::currentDateTime());
	}

	QDateTimeEdit::keyPressEvent(event);
}


QString DateTimeEditEx::textFromDateTime ( const QDateTime & dateTime ) const
{
	if (lineEdit()->text()=="" && !m_bSettingValueInProgress)
	{
		return "";
	}

	return QDateTimeEdit::textFromDateTime(dateTime);
}

//every time text changes:
void DateTimeEditEx::OnTextEdited(const QString &newVal)
{
	if (newVal.isEmpty()) //if field deleted then set invalid date
	{
		//SetInvalidDate();
		if (dateTime()!=m_invalidDateTime)
			QTimer::singleShot(0,this,SLOT(SetInvalidDate()));
		return;
	}
}

//if datetime==invalid date emit signal
void DateTimeEditEx::OnTextChanged()
{
	if (lineEdit()->text().isEmpty())
	{
		SetInvalidDate();
		return;
	}

	
	QDateTime tmp=QDateTime::fromString(lineEdit()->text(),displayFormat());
	//qDebug()<<tmp;
	if (tmp==m_invalidDateTime)
	{
		emit dateChanged(m_invalidDateTime.date());
		emit dateTimeChanged(m_invalidDateTime);
	}
	else if (!tmp.isValid()) //setting empty date:
	{
		emit dateChanged(QDate());
		emit dateTimeChanged(QDateTime());
	}
}

void DateTimeEditEx::OnPopUpDateSelected( const QDate &currentdate) //when pop selects same date as current no signal emited->this is fix
{
	QDateTime tmp=QDateTime::fromString(lineEdit()->text(),displayFormat());
	//qDebug()<<lineEdit()->text();
	if (tmp.date()==m_invalidDateTime.date())
	{
		blockSignals(true);
		QDateTime dateC=QDateTime(tmp.date(),QTime::currentTime());
		int mins=(dateC.time().minute()/5)*5; //issue 2188
		int hrs=dateC.time().hour();
		dateC.setTime(QTime(hrs,mins,0,0));
		setDateTime(dateC); //if keypress: set current date if was empty
		blockSignals(false); //block all
		setCurrentSection(QDateTimeEdit::DaySection);
		setSelectedSection(QDateTimeEdit::DaySection);
		emit timeChanged(dateC.time());
		emit dateChanged(dateC.date());
		emit dateTimeChanged(dateC);
		return;
	}
}

void DateTimeEditEx::focusOutEvent ( QFocusEvent * event )
{
	QDateTimeEdit::focusOutEvent(event);

	//emit focusOutSignal();
}

//B.T. issue 2686: when focus in, then select section
void DateTimeEditEx::focusInEvent ( QFocusEvent * event )
{
	QDateTimeEdit::focusInEvent(event);
	QTimer::singleShot(1,this,SLOT(OnHighLightCurrentSection()));
}

//B.T. issue 2686: when focus in, then select section
void DateTimeEditEx::OnHighLightCurrentSection()
{
	setSelectedSection(currentSection());
}

void DateTimeEditEx::SetInvalidDate()
{
	m_bSettingValueInProgress=true;
	blockSignals(true);

	QDateTimeEdit::setDateTime(m_invalidDateTime);
	lineEdit()->blockSignals(true);
	lineEdit()->setText("");
	lineEdit()->blockSignals(false);

	blockSignals(false);
	m_bSettingValueInProgress=false;

	emit dateChanged(QDate());
	emit dateTimeChanged(QDateTime());
	//return;
}

void DateTimeEditEx::focusFromLineEdit()
{
	emit focusOutSignal();
}

/*
void DateTimeEditEx::mousePressEvent ( QMouseEvent * event )
{
	QDateTimeEdit::mousePressEvent(event);

	QWidgetList wlist = QApplication::topLevelWidgets();

	for(int i=0; i<wlist.count(); ++i)
	{
		if( qstricmp (wlist.at(i)->metaObject()->className(), "QCalendarPopup") )
		{
			QWidget *calendarpopup = wlist.at(i);
			//if (calendarpopup->parent() == this)
			//{
			QCalendarWidget *calendar = calendarpopup->findChild<QCalendarWidget *>();
			if(calendar)
				calendar->setFirstDayOfWeek(Qt::Monday);
			//}
		}
	}

}*/





