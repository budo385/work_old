#ifndef UNIVERSALTABLEDELEGATE_H
#define UNIVERSALTABLEDELEGATE_H

#include <QtCore>
#include <QItemDelegate>
#include <QModelIndex>
#include <QPainter>
#include <QVariant>
//#include "doublespinboxadv.h"




/*!
	\class  UniversalTableDelegate
	\ingroup GUICore_UniversalTable
	\brief  This class provides a way to display and edit items in any view with desired format

	Use:

*/
/*
class UniversalTableDelegate : public QItemDelegate
{
	public:
		UniversalTableDelegate::UniversalTableDelegate(QObject *parent):QItemDelegate(parent){};

		//For Editing:
	    //QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option,const QModelIndex &index) const;
	    //void setEditorData(QWidget *editor, const QModelIndex &index) const;
		//void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;
		//void updateEditorGeometry(QWidget *editor,const QStyleOptionViewItem &option, const QModelIndex &index) const;
		//For Rendering:
		//void paint( QPainter * painter, const QStyleOptionViewItem & option, const QModelIndex & index ) const;
		//void FormatDisplayItems(QString &strToDisplay,const QVariant &value,const QModelIndex & index) const;
		void drawFocus ( QPainter * painter, const QStyleOptionViewItem & option, const QRect & rect ) const;
		//void drawDisplay ( QPainter * painter, const QStyleOptionViewItem & option, const QRect & rect, const QString & text ) const;

private:
	//QVariant m_Val;   //Date,DateTime,Time
	//QString strToDisplay;
};

*/


class DummyNoFocusDelegate : public QItemDelegate
{
public:
	DummyNoFocusDelegate(QObject *parent):QItemDelegate(parent){};
	void drawFocus ( QPainter * painter, const QStyleOptionViewItem & option, const QRect & rect ) const {};
};

#endif //UNIVERSALTABLEDELEGATE_H



