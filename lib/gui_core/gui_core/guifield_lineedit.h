#ifndef GUIFIELD_LINEEDIT_H
#define GUIFIELD_LINEEDIT_H

#include "guifield.h"
#include <QLineEdit>



/*!
	\class GuiField_LineEdit
	\brief Defines LineEdit
	\ingroup GUICore_GUIFields

	Connects to datasource: setText from it or viceversa
*/
class GuiField_LineEdit : public GuiField
{
	Q_OBJECT

public:
    GuiField_LineEdit(QString strFieldName,QWidget *pWidget,GuiDataManipulator* pDataManager,DbView *TableView=NULL, QLabel *pLabel=NULL);
    
	void RefreshDisplay(); //DATA SOURCE -> UI
	 

private:
	QLineEdit* m_pEditWidget;
	bool m_bDirty;

public slots:
	void FieldChanged(const QString &txt); //UI -> DATA SOURCE
	void SetDirtyFlag(); //UI -> DATA SOURCE
    
};

#endif // GUIFIELD_LINEEDIT_H


