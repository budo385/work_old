/****************************************************************************
**
** Copyright (C) 2003-2006 Trolltech AS. All rights reserved.
**
** This file is part of a Qt Solutions component.
**
** Licensees holding valid Qt Solutions licenses may use this file in
** accordance with the Qt Solutions License Agreement provided with the
** Software.
**
** See http://www.trolltech.com/products/solutions/index.html 
** or email sales@trolltech.com for information about Qt Solutions
** License Agreements.
**
** Contact info@trolltech.com if any conditions of this licensing are
** not clear to you.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/
#include "qtpiemenu.h"
#include "qtpieaction.h"

/*! \internal

    Constructs a QtPieAction. The \a title argument is the human
    readable title of this action, such as "New", "Open" or "Select".

    The \a receiver is a QObject in which the signal or slot \a member
    will be called when this item is activated.
*/
QtPieAction::QtPieAction(const QString &text,
			 QObject *receiver, const char *member)
    : QtPieItem(text)
{
    connect(this, SIGNAL(activated()), receiver, member);
}

/*! \internal

    Constructs a QtPieAction. The \a title argument is the human
    readable text label of this action, such as "New", "Open" or "Select".
    The QIcon \a icons will be displayed together with the text,
    or by itself if \a title is empty.

    The \a receiver is a QObject in which the signal or slot \a member
    will be called when this item is activated.
*/
QtPieAction::QtPieAction(const QIcon &icons, const QString &text,
			 QObject *receiver, const char *member)
    : QtPieItem(icons, text)
{
    connect(this, SIGNAL(activated()), receiver, member);
}

/*! \internal

    Returns the type of QtPieItem, in this case always Action.
*/
int QtPieAction::type() const
{
    return QtPieMenu::Action;
}

/*! \internal

    This function is called by the QtPieMenu parent when the item that
    holds this QtPieAction is activated.
*/
void QtPieAction::activate()
{
    emit activated();
}

