/****************************************************************************
**
** Copyright (C) 2003-2006 Trolltech AS. All rights reserved.
**
** This file is part of a Qt Solutions component.
**
** Licensees holding valid Qt Solutions licenses may use this file in
** accordance with the Qt Solutions License Agreement provided with the
** Software.
**
** See http://www.trolltech.com/products/solutions/index.html 
** or email sales@trolltech.com for information about Qt Solutions
** License Agreements.
**
** Contact info@trolltech.com if any conditions of this licensing are
** not clear to you.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/
#ifndef QTPIEITEM_H
#define QTPIEITEM_H 

#include <QWidget>
#include <QString>
#include <QIcon>

#if defined(Q_WS_WIN)
#  if !defined(QT_QTPIEMENU_EXPORT) && !defined(QT_QTPIEMENU_IMPORT)
#    define QT_QTPIEMENU_EXPORT
#  elif defined(QT_QTPIEMENU_IMPORT)
#    if defined(QT_QTPIEMENU_EXPORT)
#      undef QT_QTPIEMENU_EXPORT
#    endif
#    define QT_QTPIEMENU_EXPORT __declspec(dllimport)
#  elif defined(QT_QTPIEMENU_EXPORT)
#    undef QT_QTPIEMENU_EXPORT
#    define QT_QTPIEMENU_EXPORT __declspec(dllexport)
#  endif
#else
#  define QT_QTPIEMENU_EXPORT
#endif

class QT_QTPIEMENU_EXPORT QtPieItem: public QWidget
{
    Q_OBJECT
public:
    QtPieItem(const QString &text = QString::null, unsigned int weight = 1, QWidget *parent = 0);
    QtPieItem(const QIcon &icon, const QString &title = QString(), unsigned int weight = 1, QWidget *parent = 0);
    virtual ~QtPieItem();

    void setText(const QString &text);
    QString text() const;

    void setIcon(const QIcon &icon);
    QIcon icon() const;

    void setWeight(int weight);
    int weight() const;

    void setEnabled(bool enabled = true);
    bool isEnabled() const;

    friend class QtPieMenu;

    virtual int type() const;

private:
    QString t;
    QIcon i;
    bool e;
    unsigned int w;


};

#endif // QTPIEITEM_H
