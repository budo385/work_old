/****************************************************************************
**
** Copyright (C) 2003-2006 Trolltech AS. All rights reserved.
**
** This file is part of a Qt Solutions component.
**
** Licensees holding valid Qt Solutions licenses may use this file in
** accordance with the Qt Solutions License Agreement provided with the
** Software.
**
** See http://www.trolltech.com/products/solutions/index.html 
** or email sales@trolltech.com for information about Qt Solutions
** License Agreements.
**
** Contact info@trolltech.com if any conditions of this licensing are
** not clear to you.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/
#ifndef QTPIEACTION_H
#define QTPIEACTION_H

#include "qtpieitem.h"

#include <QObject>
#include <QString>
#include <QIcon>

class QtPieAction : public QtPieItem
{
    Q_OBJECT
public:
    QtPieAction(const QString &text,
		        QObject *receiver, const char *member);
    QtPieAction(const QIcon &icon, const QString &text,
		        QObject *receiver, const char *member);

    void activate();

signals:
    void activated();

protected:
    int type() const;

};

#endif // QTPIEACTION_H
