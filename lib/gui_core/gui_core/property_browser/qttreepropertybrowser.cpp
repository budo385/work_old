/****************************************************************************
**
** Copyright (C) 2003-2006 Trolltech ASA. All rights reserved.
**
** This file is part of a Qt Solutions component.
**
** Licensees holding valid Qt Solutions licenses may use this file in
** accordance with the Qt Solutions License Agreement provided with the
** Software.
**
** See http://www.trolltech.com/products/qt/addon/solutions/ 
** or email sales@trolltech.com for information about Qt Solutions
** License Agreements.
**
** Contact info@trolltech.com if any conditions of this licensing are
** not clear to you.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/
#include "qttreepropertybrowser.h"
#include <QSet>
#include <QIcon>
#include <QTreeWidget>
#include <QItemDelegate>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QPainter>
#include <QApplication>

//////////////////////////////////

class QtTreePropertyBrowserPrivate
{
    QtTreePropertyBrowser *q_ptr;
    Q_DECLARE_PUBLIC(QtTreePropertyBrowser)
public:

    void init(QWidget *parent);

    void propertyInserted(QtProperty *property,
                QtProperty *parentProperty, QtProperty *afterProperty);
    void propertyRemoved(QtProperty *property,
                QtProperty *parentProperty);
    void propertyChanged(QtProperty *property);
    QWidget *createEditor(QtProperty *property, QWidget *parent) const
        { return q_ptr->createEditor(property, parent); }
    QtProperty *indexToProperty(const QModelIndex &index) const;
    QTreeWidgetItem *indexToItem(const QModelIndex &index) const;
    bool lastColumn(int column) const;
    void disableItem(QTreeWidgetItem *item) const;
    void enableItem(QTreeWidgetItem *item) const;

private:
    void updateItem(QTreeWidgetItem *item);
    void insertItem(QtProperty *property,
            QTreeWidgetItem *parentItem, QTreeWidgetItem *afterItem);
    void removeItem(QTreeWidgetItem *item);

    class QtPropertyEditorView *m_treeWidget;
    QMap<QtProperty *, QSet<QTreeWidgetItem *> > m_propertyToItems;
    QMap<QTreeWidgetItem *, QtProperty *> m_itemToProperty;
    QMap<QTreeWidgetItem *, QtProperty *> m_itemToParentProperty;

    bool m_headerVisible;
};

class QtPropertyEditorView : public QTreeWidget
{
    Q_OBJECT
public:
    QtPropertyEditorView(QWidget *parent = 0)
        : QTreeWidget(parent)
        {}

    QTreeWidgetItem *indexToItem(const QModelIndex &index) const
        { return itemFromIndex(index); }
protected:

    void drawBranches(QPainter *painter, const QRect &rect, const QModelIndex &index) const;
};

void QtPropertyEditorView::drawBranches(QPainter *painter, const QRect &rect,
                             const QModelIndex &index) const
{
    QTreeWidget::drawBranches(painter, rect, index);
    QStyleOptionViewItem option = viewOptions();
    QColor color = static_cast<QRgb>(QApplication::style()->styleHint(QStyle::SH_Table_GridLineColor, &option));
    painter->save();
    painter->setPen(QPen(color));
    painter->drawLine(rect.x(), rect.bottom(), rect.right(), rect.bottom());
    painter->restore();
}

class QtPropertyEditorDelegate : public QItemDelegate
{
    Q_OBJECT
public:
    QtPropertyEditorDelegate(QObject *parent = 0)
        : QItemDelegate(parent), m_editorPrivate(0)
        {}

    void setEditorPrivate(QtTreePropertyBrowserPrivate *editorPrivate)
        { m_editorPrivate = editorPrivate; }

    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option,
            const QModelIndex &index) const;

    void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option,
            const QModelIndex &index) const;

    void paint(QPainter *painter, const QStyleOptionViewItem &option,
            const QModelIndex &index) const;

    QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const;

    void setModelData(QWidget *, QAbstractItemModel *,
            const QModelIndex &) const {}

    void setEditorData(QWidget *, const QModelIndex &) const {}
private:
    QtTreePropertyBrowserPrivate *m_editorPrivate;
};

QWidget *QtPropertyEditorDelegate::createEditor(QWidget *parent,
        const QStyleOptionViewItem &, const QModelIndex &index) const
{
    if (index.column() == 1 && m_editorPrivate) {
        QtProperty *property = m_editorPrivate->indexToProperty(index);
        QTreeWidgetItem *item = m_editorPrivate->indexToItem(index);
        if (property && item && (item->flags() & Qt::ItemIsEnabled)) {
            QWidget *editor = m_editorPrivate->createEditor(property, parent);
            if (editor) {
                editor->setAutoFillBackground(true);
                editor->setFocusPolicy(Qt::StrongFocus);
                editor->installEventFilter(const_cast<QtPropertyEditorDelegate *>(this));
            }
            return editor;
        }
    }
    return 0;
}

void QtPropertyEditorDelegate::updateEditorGeometry(QWidget *editor,
        const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    QItemDelegate::updateEditorGeometry(editor, option, index);
    editor->setGeometry(editor->geometry().adjusted(0, 0, -1, -1));
}

void QtPropertyEditorDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option,
            const QModelIndex &index) const
{
    QItemDelegate::paint(painter, option, index);
    QColor color = static_cast<QRgb>(QApplication::style()->styleHint(QStyle::SH_Table_GridLineColor, &option));
    painter->save();
    painter->setPen(QPen(color));
    if (!m_editorPrivate || (!m_editorPrivate->lastColumn(index.column()) && m_editorPrivate->indexToProperty(index)->hasValue())) {
        int right = (option.direction == Qt::LeftToRight) ? option.rect.right() : option.rect.left();
        painter->drawLine(right, option.rect.y(), right, option.rect.bottom());
    }
    painter->drawLine(option.rect.x(), option.rect.bottom(),
            option.rect.right(), option.rect.bottom());
    painter->restore();
}

QSize QtPropertyEditorDelegate::sizeHint(const QStyleOptionViewItem &option,
            const QModelIndex &index) const
{
    return QItemDelegate::sizeHint(option, index) + QSize(3, 3);
}

void QtTreePropertyBrowserPrivate::init(QWidget *parent)
{
    m_headerVisible = true;
    QHBoxLayout *layout = new QHBoxLayout(parent);
    layout->setMargin(0);
    m_treeWidget = new QtPropertyEditorView(parent);
    layout->addWidget(m_treeWidget);

    m_treeWidget->setColumnCount(2);
    QStringList labels;
	labels << parent->QObject::tr("Property") << parent->QObject::tr("Value");
    m_treeWidget->setHeaderLabels(labels);
    m_treeWidget->setAlternatingRowColors(true);
    //m_treeWidget->header()->setResizeMode(QHeaderView::Stretch);
    m_treeWidget->setEditTriggers(QAbstractItemView::CurrentChanged | QAbstractItemView::SelectedClicked);
    QtPropertyEditorDelegate *delegate = new QtPropertyEditorDelegate(parent);
    delegate->setEditorPrivate(this);
    m_treeWidget->setItemDelegate(delegate);
}

QtProperty *QtTreePropertyBrowserPrivate::indexToProperty(const QModelIndex &index) const
{
    QTreeWidgetItem *i = m_treeWidget->indexToItem(index);
    if (i) {
        QMap<QTreeWidgetItem *, QtProperty *>::ConstIterator it =
                m_itemToProperty.find(i);
        if (it != m_itemToProperty.constEnd())
            return it.value();
    }
    return 0;
}

QTreeWidgetItem *QtTreePropertyBrowserPrivate::indexToItem(const QModelIndex &index) const
{
    return m_treeWidget->indexToItem(index);
}

bool QtTreePropertyBrowserPrivate::lastColumn(int column) const
{
    return m_treeWidget->header()->visualIndex(column) == m_treeWidget->columnCount() + 1;
}

void QtTreePropertyBrowserPrivate::disableItem(QTreeWidgetItem *item) const
{
    Qt::ItemFlags flags = item->flags();
    if (flags & Qt::ItemIsEnabled) {
        flags &= ~Qt::ItemIsEnabled;
        item->setFlags(flags);
        for (int i = 0; i < item->childCount(); i++) {
            QTreeWidgetItem *child = item->child(i);
            disableItem(child);
        }
    }
}

void QtTreePropertyBrowserPrivate::enableItem(QTreeWidgetItem *item) const
{
    Qt::ItemFlags flags = item->flags();
    flags |= Qt::ItemIsEnabled;
    item->setFlags(flags);
    for (int i = 0; i < item->childCount(); i++) {
        QTreeWidgetItem *child = item->child(i);
        QtProperty *property = m_itemToProperty[child];
        if (property->isEnabled()) {
            enableItem(child);
        }
    }
}

void QtTreePropertyBrowserPrivate::propertyInserted(QtProperty *property,
        QtProperty *parentProperty, QtProperty *afterProperty)
{
    QMap<QTreeWidgetItem *, QTreeWidgetItem *> parentToAfter;
    if (afterProperty) {
        QMap<QtProperty *, QSet<QTreeWidgetItem *> >::ConstIterator it =
            m_propertyToItems.find(afterProperty);
        if (it == m_propertyToItems.constEnd())
            return;

        QSet<QTreeWidgetItem *> items = it.value();
        QSetIterator<QTreeWidgetItem *> itItem(items);
        while (itItem.hasNext()) {
            QTreeWidgetItem *i = itItem.next();
            if (m_itemToParentProperty[i] == parentProperty)
                parentToAfter[i->parent()] = i;
        }
    } else if (parentProperty) {
        QMap<QtProperty *, QSet<QTreeWidgetItem *> >::ConstIterator it =
            m_propertyToItems.find(parentProperty);
        if (it == m_propertyToItems.constEnd())
            return;

        QSet<QTreeWidgetItem *> items = it.value();
        QSetIterator<QTreeWidgetItem *> itItem(items);
        while (itItem.hasNext()) {
            QTreeWidgetItem *i = itItem.next();
            parentToAfter[i] = 0;
        }
    } else {
        parentToAfter[0] = 0;
    }

    QMap<QTreeWidgetItem *, QTreeWidgetItem *>::ConstIterator it =
                parentToAfter.constBegin();
    while (it != parentToAfter.constEnd()) {
        insertItem(property, it.key(), it.value());

        it++;
    }
}

void QtTreePropertyBrowserPrivate::insertItem(QtProperty *property,
        QTreeWidgetItem *parentItem, QTreeWidgetItem *afterItem)
{
    QTreeWidgetItem *newItem = 0;
    if (parentItem) {
        newItem = new QTreeWidgetItem(parentItem, afterItem);
    } else {
        newItem = new QTreeWidgetItem(m_treeWidget, afterItem);
    }
    newItem->setFlags(newItem->flags() | Qt::ItemIsEditable);
    m_treeWidget->setItemExpanded(newItem, true);
    m_propertyToItems[property].insert(newItem);
    m_itemToProperty[newItem] = property;
    m_itemToParentProperty[newItem] = m_itemToProperty[parentItem];

    updateItem(newItem);

    QList<QtProperty *> subItems = property->subProperties();
    QListIterator<QtProperty *> itChild(subItems);
    itChild.toBack();
    while (itChild.hasPrevious()) {
        insertItem(itChild.previous(), newItem, 0);
    }
}

void QtTreePropertyBrowserPrivate::propertyRemoved(QtProperty *property,
        QtProperty *parentProperty)
{
    QList<QTreeWidgetItem *> toRemove;
    QMap<QtProperty *, QSet<QTreeWidgetItem *> >::ConstIterator it =
        m_propertyToItems.find(property);
    if (it == m_propertyToItems.constEnd())
        return;

    QSet<QTreeWidgetItem *> items = it.value();
    QSetIterator<QTreeWidgetItem *> itItem(items);
    while (itItem.hasNext()) {
        QTreeWidgetItem *i = itItem.next();
        if (m_itemToParentProperty[i] == parentProperty)
            toRemove.append(i);
    }

    QListIterator<QTreeWidgetItem *> itRemove(toRemove);
    while (itRemove.hasNext()) {
        removeItem(itRemove.next());
    }
}

void QtTreePropertyBrowserPrivate::removeItem(QTreeWidgetItem *item)
{
    for (int i = item->childCount(); i > 0; i--) {
        removeItem(item->child(i - 1));
    }

    QtProperty *property = m_itemToProperty[item];

    m_propertyToItems[property].remove(item);
    if (m_propertyToItems[property].isEmpty())
        m_propertyToItems.remove(property);
    m_itemToProperty.remove(item);
    m_itemToParentProperty.remove(item);

    if (m_treeWidget->currentItem() == item) {
        m_treeWidget->setCurrentItem(0);
    }

    delete item;
}

void QtTreePropertyBrowserPrivate::propertyChanged(QtProperty *property)
{
    QMap<QtProperty *, QSet<QTreeWidgetItem *> >::ConstIterator it =
            m_propertyToItems.find(property);
    if (it == m_propertyToItems.constEnd())
        return;

    QSet<QTreeWidgetItem *> items = it.value();
    QSetIterator<QTreeWidgetItem *> itItem(items);
    while (itItem.hasNext()) {
        QTreeWidgetItem *i = itItem.next();
        updateItem(i);
    }
}

void QtTreePropertyBrowserPrivate::updateItem(QTreeWidgetItem *item)
{
    QtProperty *property = m_itemToProperty[item];
    item->setToolTip(0, property->toolTip());
    item->setToolTip(1, property->valueText());
    item->setStatusTip(0, property->statusTip());
    item->setWhatsThis(0, property->whatsThis());
    item->setText(0, property->propertyName());
    item->setText(1, property->valueText());
    item->setIcon(1, property->valueIcon());
    bool wasEnabled = item->flags() & Qt::ItemIsEnabled;
    bool isEnabled = wasEnabled;
    if (property->isEnabled()) {
        QTreeWidgetItem *parent = item->parent();
        if (!parent || (parent->flags() & Qt::ItemIsEnabled))
            isEnabled = true;
        else
            isEnabled = false;
    } else {
        isEnabled = false;
    }
    if (wasEnabled != isEnabled) {
        if (isEnabled)
            enableItem(item);
        else
            disableItem(item);
    }
}

/*!
    \class QtTreePropertyBrowser

    \brief The QtTreePropertyBrowser class provides QTreeWidget based
    property browser.

    A property browser is a widget that enables the user to edit a
    given set of properties. Each property is represented by a label
    specifying the property's name, and an editing widget (e.g. a line
    edit or a combobox) holding its value. A property can have zero or
    more subproperties.

    QtTreePropertyBrowser provides a tree based view for all nested
    properties, i.e. properties that have subproperties can be in an
    expanded (subproperties are visible) or collapsed (subproperties
    are hidden) state. For example:

    \image qttreepropertybrowser.png

    Use the QtAbstractPropertyBrowser API to add, insert and remove
    properties from an instance of the QtTreePropertyBrowser class.
    The properties themselves are created and managed by
    implementations of the QtAbstractPropertyManager class.

    \sa QtGroupBoxPropertyBrowser, QtAbstractPropertyBrowser
*/

/*!
    Creates a property browser with the given \a parent.
*/
QtTreePropertyBrowser::QtTreePropertyBrowser(QWidget *parent)
    : QtAbstractPropertyBrowser(parent)
{
    d_ptr = new QtTreePropertyBrowserPrivate;
    d_ptr->q_ptr = this;

    d_ptr->init(this);
}

/*!
    Destroys this property browser.

    Note that the properties that were inserted into this browser are
    \e not destroyed since they may still be used in other
    browsers. The properties are owned by the manager that created
    them.

    \sa QtProperty, QtAbstractPropertyManager
*/
QtTreePropertyBrowser::~QtTreePropertyBrowser()
{
    delete d_ptr;
}

/*!
    \property QtTreePropertyBrowser::indentation
    \brief indentation of the items in the tree view.
*/
int QtTreePropertyBrowser::indentation() const
{
    return d_ptr->m_treeWidget->indentation();
}

void QtTreePropertyBrowser::setIndentation(int i)
{
    d_ptr->m_treeWidget->setIndentation(i);
}

/*!
  \property QtTreePropertyBrowser::rootIsDecorated
  \brief whether to show controls for expanding and collapsing root items.
*/
bool QtTreePropertyBrowser::rootIsDecorated() const
{
    return d_ptr->m_treeWidget->rootIsDecorated();
}

void QtTreePropertyBrowser::setRootIsDecorated(bool show)
{
    d_ptr->m_treeWidget->setRootIsDecorated(show);
}

/*!
  \property QtTreePropertyBrowser::headerVisible
  \brief whether to show the header.

  When header is hidden both columns are automatically stretched.
*/
bool QtTreePropertyBrowser::isHeaderVisible() const
{
    return d_ptr->m_headerVisible;
}

void QtTreePropertyBrowser::setHeaderVisible(bool visible)
{
    if (d_ptr->m_headerVisible == visible)
        return;

    d_ptr->m_headerVisible = visible;
    d_ptr->m_treeWidget->header()->setVisible(visible);
    d_ptr->m_treeWidget->header()->setSectionResizeMode(visible ? QHeaderView::Interactive : QHeaderView::Stretch);
}

void QtTreePropertyBrowser::resizeFirstColumnToContents()
{
	d_ptr->m_treeWidget->resizeColumnToContents(0);
}

/*!
    \reimp
*/
void QtTreePropertyBrowser::propertyInserted(QtProperty *property,
        QtProperty *parentProperty, QtProperty *afterProperty)
{
    d_ptr->propertyInserted(property, parentProperty, afterProperty);
}

/*!
    \reimp
*/
void QtTreePropertyBrowser::propertyRemoved(QtProperty *property,
        QtProperty *parentProperty)
{
    d_ptr->propertyRemoved(property, parentProperty);
}

/*!
    \reimp
*/
void QtTreePropertyBrowser::propertyChanged(QtProperty *property)
{
    d_ptr->propertyChanged(property);
}











#include "moc_qttreepropertybrowser.cpp"
#include "qttreepropertybrowser.moc"

