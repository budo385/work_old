@ECHO OFF
REM MR: refresh .moc files when using new Qt (will not overwrite the old ones, you need to do that manually)
SET OLDDIR=%CD%
SET BIN_DIR=c:\Qt\5.0.2\5.0.2\msvc2010\bin\
PUSHD %BIN_DIR%
moc.exe %OLDDIR%\qttreepropertybrowser.cpp > %OLDDIR%\qttreepropertybrowser.moc
moc.exe %OLDDIR%\qtpropertymanager.cpp > %OLDDIR%\qtpropertymanager.moc
POPD