/****************************************************************************
**
** Copyright (C) 2003-2006 Trolltech ASA. All rights reserved.
**
** This file is part of a Qt Solutions component.
**
** Licensees holding valid Qt Solutions licenses may use this file in
** accordance with the Qt Solutions License Agreement provided with the
** Software.
**
** See http://www.trolltech.com/products/qt/addon/solutions/ 
** or email sales@trolltech.com for information about Qt Solutions
** License Agreements.
**
** Contact info@trolltech.com if any conditions of this licensing are
** not clear to you.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/
#include "qtgroupboxpropertybrowser.h"
#include <QSet>
#include <QGridLayout>
#include <QLabel>
#include <QGroupBox>
#include <QTimer>
#include <QMap>

//////////////////////////////////

class QtGroupBoxPropertyBrowserPrivate
{
    QtGroupBoxPropertyBrowser *q_ptr;
    Q_DECLARE_PUBLIC(QtGroupBoxPropertyBrowser)
public:

    void init(QWidget *parent);

    void propertyInserted(QtProperty *property,
                QtProperty *parentProperty, QtProperty *afterProperty);
    void propertyRemoved(QtProperty *property,
                QtProperty *parentProperty);
    void propertyChanged(QtProperty *property);
    QWidget *createEditor(QtProperty *property, QWidget *parent) const
        { return q_ptr->createEditor(property, parent); }

    void slotUpdate();

    struct WidgetItem
    {
        WidgetItem() : widget(0), label(0), widgetLabel(0),
                groupBox(0), layout(0), line(0), parent(0) { }
        QWidget *widget; // can be null
        QLabel *label;
        QLabel *widgetLabel;
        QGroupBox *groupBox;
        QGridLayout *layout;
        QFrame *line;
        WidgetItem *parent;
        QList<WidgetItem *> children;
    };
private:
    void updateLater();
    void updateItem(WidgetItem *item);
    void insertItem(QtProperty *property,
            WidgetItem *parentItem, WidgetItem *afterItem);
    void removeItem(WidgetItem *item);
    void insertRow(QGridLayout *layout, int row);
    void removeRow(QGridLayout *layout, int row);

    bool hasHeader(WidgetItem *item) const;

    QMap<QtProperty *, QSet<WidgetItem *> > m_propertyToItems;
    QMap<WidgetItem *, QtProperty *> m_itemToProperty;
    QMap<WidgetItem *, QtProperty *> m_itemToParentProperty;
    QGridLayout *m_mainLayout;
    QList<WidgetItem *> m_children;
    QList<WidgetItem *> m_recreateQueue;
};

void QtGroupBoxPropertyBrowserPrivate::init(QWidget *parent)
{
    m_mainLayout = new QGridLayout();
    m_mainLayout->setMargin(0);
    parent->setLayout(m_mainLayout);
    QLayoutItem *item = new QSpacerItem(0, 0,
                QSizePolicy::Fixed, QSizePolicy::Expanding);
    m_mainLayout->addItem(item, 0, 0);
}

void QtGroupBoxPropertyBrowserPrivate::slotUpdate()
{
    QListIterator<WidgetItem *> itItem(m_recreateQueue);
    while (itItem.hasNext()) {
        WidgetItem *item = itItem.next();

        WidgetItem *par = item->parent;
        QWidget *w = 0;
        QGridLayout *l = 0;
        int oldRow = -1;
        if (!par) {
            w = q_ptr;
            l = m_mainLayout;
            oldRow = m_children.indexOf(item);
        } else {
            w = par->groupBox;
            l = par->layout;
            oldRow = par->children.indexOf(item);
            if (hasHeader(par))
                oldRow += 2;
        }

        if (item->widget) {
            item->widget->setParent(w);
        } else if (item->widgetLabel) {
            item->widgetLabel->setParent(w);
        } else {
            item->widgetLabel = new QLabel(w);
        }
        int span = 1;
        if (item->widget)
            l->addWidget(item->widget, oldRow, 1, 1, 1);
        else if (item->widgetLabel)
            l->addWidget(item->widgetLabel, oldRow, 1, 1, 1);
        else
            span = 2;
        item->label = new QLabel(w);
        l->addWidget(item->label, oldRow, 0, 1, span);

        updateItem(item);
    }
    m_recreateQueue.clear();
}

void QtGroupBoxPropertyBrowserPrivate::updateLater()
{
    QTimer::singleShot(0, q_ptr, SLOT(slotUpdate()));
}

void QtGroupBoxPropertyBrowserPrivate::propertyInserted(QtProperty *property,
        QtProperty *parentProperty, QtProperty *afterProperty)
{
    QMap<WidgetItem *, WidgetItem *> parentToAfter;
    if (afterProperty) {
        QMap<QtProperty *, QSet<WidgetItem *> >::ConstIterator it =
            m_propertyToItems.find(afterProperty);
        if (it == m_propertyToItems.constEnd())
            return;

        QSet<WidgetItem *> items = it.value();
        QSetIterator<WidgetItem *> itItem(items);
        while (itItem.hasNext()) {
            WidgetItem *i = itItem.next();
            if (m_itemToParentProperty[i] == parentProperty)
                parentToAfter[i->parent] = i;
        }
    } else if (parentProperty) {
        QMap<QtProperty *, QSet<WidgetItem *> >::ConstIterator it =
            m_propertyToItems.find(parentProperty);
        if (it == m_propertyToItems.constEnd())
            return;

        QSet<WidgetItem *> items = it.value();
        QSetIterator<WidgetItem *> itItem(items);
        while (itItem.hasNext()) {
            WidgetItem *i = itItem.next();
            parentToAfter[i] = 0;
        }
    } else {
        parentToAfter[0] = 0;
    }

    QMap<WidgetItem *, WidgetItem *>::ConstIterator it =
                parentToAfter.constBegin();
    while (it != parentToAfter.constEnd()) {
        insertItem(property, it.key(), it.value());

        it++;
    }
}

void QtGroupBoxPropertyBrowserPrivate::insertItem(QtProperty *property,
        WidgetItem *parentItem, WidgetItem *afterItem)
{
    WidgetItem *newItem = new WidgetItem();
    newItem->parent = parentItem;

    QGridLayout *layout = 0;
    QWidget *parentWidget = 0;
    int row = -1;
    if (!afterItem) {
        row = 0;
        if (parentItem)
            parentItem->children.insert(0, newItem);
        else
            m_children.insert(0, newItem);
    } else {
        if (parentItem) {
            row = parentItem->children.indexOf(afterItem) + 1;
            parentItem->children.insert(row, newItem);
        } else {
            row = m_children.indexOf(afterItem) + 1;
            m_children.insert(row, newItem);
        }
    }
    if (parentItem && hasHeader(parentItem))
        row += 2;

    if (!parentItem) {
        layout = m_mainLayout;
        parentWidget = q_ptr;;
    } else {
        if (!parentItem->groupBox) {
            m_recreateQueue.removeAll(parentItem);
            WidgetItem *par = parentItem->parent;
            QWidget *w = 0;
            QGridLayout *l = 0;
            int oldRow = -1;
            if (!par) {
                w = q_ptr;
                l = m_mainLayout;
                oldRow = m_children.indexOf(parentItem);
            } else {
                w = par->groupBox;
                l = par->layout;
                oldRow = par->children.indexOf(parentItem);
                if (hasHeader(par))
                    oldRow += 2;
            }
            parentItem->groupBox = new QGroupBox(w);
            parentItem->layout = new QGridLayout();
            parentItem->groupBox->setLayout(parentItem->layout);
            if (parentItem->label) {
                l->removeWidget(parentItem->label);
                delete parentItem->label;
                parentItem->label = 0;
            }
            if (parentItem->widget) {
                l->removeWidget(parentItem->widget);
                parentItem->widget->setParent(parentItem->groupBox);
                parentItem->layout->addWidget(parentItem->widget, 0, 0, 1, 2);
                parentItem->line = new QFrame(parentItem->groupBox);
            } else if (parentItem->widgetLabel) {
                l->removeWidget(parentItem->widgetLabel);
                delete parentItem->widgetLabel;
                parentItem->widgetLabel = 0;
                //parentItem->widgetLabel->setParent(parentItem->groupBox);
                //parentItem->layout->addWidget(parentItem->widgetLabel, 0, 0, 1, 2);
                //parentItem->line = new QFrame(parentItem->groupBox);
            }
            if (parentItem->line) {
                parentItem->line->setFrameShape(QFrame::HLine);
                parentItem->line->setFrameShadow(QFrame::Sunken);
                parentItem->layout->addWidget(parentItem->line, 1, 0, 1, 2);
            }
            l->addWidget(parentItem->groupBox, oldRow, 0, 1, 2);
            updateItem(parentItem);
        }
        layout = parentItem->layout;
        parentWidget = parentItem->groupBox;
    }

    newItem->label = new QLabel(parentWidget);
    newItem->widget = createEditor(property, parentWidget);
    if (!newItem->widget)
        newItem->widgetLabel = new QLabel(parentWidget);

    insertRow(layout, row);
    int span = 1;
    if (newItem->widget)
        layout->addWidget(newItem->widget, row, 1);
    else if (newItem->widgetLabel)
        layout->addWidget(newItem->widgetLabel, row, 1);
    else
        span = 2;
    layout->addWidget(newItem->label, row, 0, 1, span);


    m_propertyToItems[property].insert(newItem);
    m_itemToProperty[newItem] = property;
    m_itemToParentProperty[newItem] = m_itemToProperty[parentItem];

    updateItem(newItem);

    QList<QtProperty *> subItems = property->subProperties();
    QListIterator<QtProperty *> itChild(subItems);
    itChild.toBack();
    while (itChild.hasPrevious()) {
        insertItem(itChild.previous(), newItem, 0);
    }
}

void QtGroupBoxPropertyBrowserPrivate::propertyRemoved(QtProperty *property,
        QtProperty *parentProperty)
{
    QList<WidgetItem *> toRemove;
    QMap<QtProperty *, QSet<WidgetItem *> >::ConstIterator it =
        m_propertyToItems.find(property);
    if (it == m_propertyToItems.constEnd())
        return;

    QSet<WidgetItem *> items = it.value();
    QSetIterator<WidgetItem *> itItem(items);
    while (itItem.hasNext()) {
        WidgetItem *i = itItem.next();
        if (m_itemToParentProperty[i] == parentProperty)
            toRemove.append(i);
    }

    QListIterator<WidgetItem *> itRemove(toRemove);
    while (itRemove.hasNext()) {
        removeItem(itRemove.next());
    }
}

void QtGroupBoxPropertyBrowserPrivate::removeItem(WidgetItem *item)
{
    for (int i = item->children.count(); i > 0; i--) {
        removeItem(item->children.at(i - 1));
    }

    WidgetItem *parentItem = item->parent;

    int row = -1;

    if (parentItem) {
        row = parentItem->children.indexOf(item);
        parentItem->children.removeAt(row);
        if (hasHeader(parentItem))
            row += 2;
    } else {
        row = m_children.indexOf(item);
        m_children.removeAt(row);
    }

    if (item->widget)
        delete item->widget;
    if (item->label)
        delete item->label;
    if (item->widgetLabel)
        delete item->widgetLabel;
    if (item->groupBox)
        delete item->groupBox;

    if (!parentItem) {
        removeRow(m_mainLayout, row);
    } else if (parentItem->children.count() != 0) {
        removeRow(parentItem->layout, row);
    } else {
        WidgetItem *par = parentItem->parent;
        QWidget *w = 0;
        QGridLayout *l = 0;
        int oldRow = -1;
        if (!par) {
            w = q_ptr;
            l = m_mainLayout;
            oldRow = m_children.indexOf(parentItem);
        } else {
            w = par->groupBox;
            l = par->layout;
            oldRow = par->children.indexOf(parentItem);
            if (hasHeader(par))
                oldRow += 2;
        }

        if (parentItem->widget) {
            parentItem->widget->hide();
            parentItem->widget->setParent(0);
        } else if (parentItem->widgetLabel) {
            parentItem->widgetLabel->hide();
            parentItem->widgetLabel->setParent(0);
        } else {
            //parentItem->widgetLabel = new QLabel(w);
        }
        l->removeWidget(parentItem->groupBox);
        delete parentItem->groupBox;
        parentItem->groupBox = 0;
        parentItem->line = 0;
        parentItem->layout = 0;
        /*
        int span = 1;
        if (parentItem->widget)
            l->addWidget(parentItem->widget, oldRow, 1, 1, 1);
        else if (parentItem->widgetLabel)
            l->addWidget(parentItem->widgetLabel, oldRow, 1, 1, 1);
        else
            span = 2;
        parentItem->label = new QLabel(w);
        l->addWidget(parentItem->label, oldRow, 0, 1, span);

        updateItem(parentItem);
        */
        if (!m_recreateQueue.contains(parentItem))
            m_recreateQueue.append(parentItem);
        updateLater();
    }
    m_recreateQueue.removeAll(item);

    QtProperty *property = m_itemToProperty[item];

    m_propertyToItems[property].remove(item);
    if (m_propertyToItems[property].isEmpty())
        m_propertyToItems.remove(property);
    m_itemToProperty.remove(item);
    m_itemToParentProperty.remove(item);

    delete item;
}

void QtGroupBoxPropertyBrowserPrivate::insertRow(QGridLayout *layout, int row)
{
    QMap<QLayoutItem *, QRect> itemToPos;
    int idx = 0;
    while (idx < layout->count()) {
        int r, c, rs, cs;
        layout->getItemPosition(idx, &r, &c, &rs, &cs);
        if (r >= row) {
            itemToPos[layout->takeAt(idx)] = QRect(r + 1, c, rs, cs);
        } else {
            idx++;
        }
    }

    QMap<QLayoutItem *, QRect>::ConstIterator it = itemToPos.constBegin();
    while (it != itemToPos.constEnd()) {
        QRect r = it.value();
        layout->addItem(it.key(), r.x(), r.y(), r.width(), r.height());

        it++;
    }
}

void QtGroupBoxPropertyBrowserPrivate::removeRow(QGridLayout *layout, int row)
{
    QMap<QLayoutItem *, QRect> itemToPos;
    int idx = 0;
    while (idx < layout->count()) {
        int r, c, rs, cs;
        layout->getItemPosition(idx, &r, &c, &rs, &cs);
        if (r > row) {
            itemToPos[layout->takeAt(idx)] = QRect(r - 1, c, rs, cs);
        } else {
            idx++;
        }
    }

    QMap<QLayoutItem *, QRect>::ConstIterator it = itemToPos.constBegin();
    while (it != itemToPos.constEnd()) {
        QRect r = it.value();
        layout->addItem(it.key(), r.x(), r.y(), r.width(), r.height());

        it++;
    }
}

bool QtGroupBoxPropertyBrowserPrivate::hasHeader(WidgetItem *item) const
{
    if (item->widget)
        return true;
    return false;
}

void QtGroupBoxPropertyBrowserPrivate::propertyChanged(QtProperty *property)
{
    QMap<QtProperty *, QSet<WidgetItem *> >::ConstIterator it =
            m_propertyToItems.find(property);
    if (it == m_propertyToItems.constEnd())
        return;

    QSet<WidgetItem *> items = it.value();
    QSetIterator<WidgetItem *> itItem(items);
    while (itItem.hasNext()) {
        WidgetItem *i = itItem.next();
        updateItem(i);
    }
}

void QtGroupBoxPropertyBrowserPrivate::updateItem(WidgetItem *item)
{
    QtProperty *property = m_itemToProperty[item];
    if (item->groupBox) {
        item->groupBox->setTitle(property->propertyName());
        item->groupBox->setToolTip(property->toolTip());
        item->groupBox->setStatusTip(property->statusTip());
        item->groupBox->setWhatsThis(property->whatsThis());
        item->groupBox->setEnabled(property->isEnabled());
    }
    if (item->label) {
        item->label->setText(property->propertyName());
        item->label->setToolTip(property->toolTip());
        item->label->setStatusTip(property->statusTip());
        item->label->setWhatsThis(property->whatsThis());
        item->label->setEnabled(property->isEnabled());
    }
    if (item->widgetLabel) {
        item->widgetLabel->setText(property->valueText());
        item->widgetLabel->setEnabled(property->isEnabled());
    }
    if (item->widget) {
        item->widget->setEnabled(property->isEnabled());
    }
    //item->setIcon(1, property->valueIcon());
}



/*!
    \class QtGroupBoxPropertyBrowser

    \brief The QtGroupBoxPropertyBrowser class provides a QGroupBox
    based property browser.

    A property browser is a widget that enables the user to edit a
    given set of properties. Each property is represented by a label
    specifying the property's name, and an editing widget (e.g. a line
    edit or a combobox) holding its value. A property can have zero or
    more subproperties.

    QtGroupBoxPropertyBrowser provides group boxes for all nested
    properties, i.e. subproperties are enclosed by a group box with
    the parent property's name as its title. For example:

    \image qtgroupboxpropertybrowser.png

    Use the QtAbstractPropertyBrowser API to add, insert and remove
    properties from an instance of the QtGroupBoxPropertyBrowser
    class. The properties themselves are created and managed by
    implementations of the QtAbstractPropertyManager class.

    \sa QtTreePropertyBrowser, QtAbstractPropertyBrowser
*/

/*!
    Creates a property browser with the given \a parent.
*/
QtGroupBoxPropertyBrowser::QtGroupBoxPropertyBrowser(QWidget *parent)
    : QtAbstractPropertyBrowser(parent)
{
    d_ptr = new QtGroupBoxPropertyBrowserPrivate;
    d_ptr->q_ptr = this;

    d_ptr->init(this);
}

/*!
    Destroys this property browser.

    Note that the properties that were inserted into this browser are
    \e not destroyed since they may still be used in other
    browsers. The properties are owned by the manager that created
    them.

    \sa QtProperty, QtAbstractPropertyManager
*/
QtGroupBoxPropertyBrowser::~QtGroupBoxPropertyBrowser()
{
    QMap<QtGroupBoxPropertyBrowserPrivate::WidgetItem *, QtProperty *>::ConstIterator it =
                d_ptr->m_itemToProperty.constBegin();
    while (it != d_ptr->m_itemToProperty.constEnd()) {
        delete it.key();
        it++;
    }
    delete d_ptr;
}

/*!
    \reimp
*/
void QtGroupBoxPropertyBrowser::propertyInserted(QtProperty *property,
        QtProperty *parentProperty, QtProperty *afterProperty)
{
    d_ptr->propertyInserted(property, parentProperty, afterProperty);
}

/*!
    \reimp
*/
void QtGroupBoxPropertyBrowser::propertyRemoved(QtProperty *property,
        QtProperty *parentProperty)
{
    d_ptr->propertyRemoved(property, parentProperty);
}

/*!
    \reimp
*/
void QtGroupBoxPropertyBrowser::propertyChanged(QtProperty *property)
{
    d_ptr->propertyChanged(property);
}











#include "moc_qtgroupboxpropertybrowser.cpp"

