#include "comboboxwidget.h"
#include <QContextMenuEvent>
#include <QMenu>

ComboBoxWidget::ComboBoxWidget(QWidget *parent)
	: QComboBox(parent)
{

}

ComboBoxWidget::~ComboBoxWidget()
{

}


//--------------------------------------------------------------
//						CONTEXT MENU
//--------------------------------------------------------------


/*!
	Slot for opening context menu:
*/
void ComboBoxWidget::contextMenuEvent(QContextMenuEvent *event)
{
	//event:
	if(m_lstActions.size()==0) 
	{
		event->ignore();	
		return;
	}

	QMenu CnxtMenu(this);

	int nSize=m_lstActions.size();
	for(int i=0;i<nSize;++i)
		CnxtMenu.addAction(m_lstActions.at(i));

	CnxtMenu.exec(event->globalPos());
}