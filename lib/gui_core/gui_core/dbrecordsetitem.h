#ifndef DBRECORDSETITEM_H
#define DBRECORDSETITEM_H

#include <QList>
#include <QVector>
#include <QStringList>
#include <QMultiHash>
#include <QHash>
#include <QVariant>
#include <QPersistentModelIndex>

/*!
	\class  DbRecordSetItem
	\ingroup GUICore_ModelClasses
	\brief  RecordSet model Item. 

	Model item for DbRecordSetModel.
*/
class DbRecordSetItem
{
public:
	DbRecordSetItem();
    DbRecordSetItem(const int RowID, const QList<QString> &data,  const QStringList Icon, const int ItemState, DbRecordSetItem *parent = NULL);
    ~DbRecordSetItem();

	DbRecordSetItem(const DbRecordSetItem &that);
	void operator =(const DbRecordSetItem &that); 

	void ClearItem();
	void appendChild(DbRecordSetItem *Child);
	DbRecordSetItem *child(int row);
	int childCount() const;
	int columnCount() const;
	QVariant data(int column) const;
	void SetData(const int column, const QVariant Value);
	QStringList GetIcons();
	int GetRowId();
	int GetItemState();
	int row() const;
	int GetLoadedChildrenCount();
	DbRecordSetItem *parent();

	void DeleteChild(DbRecordSetItem *Child);
	void DeleteChild(int Row);
	void SetParent(DbRecordSetItem *ParentItem);
	QList<int> GetChildrenRowIDList();

private:
	int						m_nItemRowID;		//< RowID in RecordSet.
	QList<int>				m_lstChildRowID;	//< Children RowID's.
	QList<QString>			m_ItemData;			//< Item data (list because of columns).
	QStringList				m_lstIcon;			//< Item icons.
	int						m_nItemState;		//< Item state 0-no children, 1-collapsed, 2-expanded.
	DbRecordSetItem			*m_pParentItem;		//< Parent.
	QVector<DbRecordSetItem*> m_ChildItems;		//< Children list.
};

#endif // DBRECORDSETITEM_H

