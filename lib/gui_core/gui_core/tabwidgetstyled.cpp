#include "tabwidgetstyled.h"
#include <QTabBar>

TabWidgetStyled::TabWidgetStyled(QWidget *parent)
	: QTabWidget(parent)
{
	QString str="* {color: black;font-weight:bold; font-style:italic; font-size:12px}";
	tabBar()->setStyleSheet(str);
}

TabWidgetStyled::~TabWidgetStyled()
{

}


void TabWidgetStyled::setTABStyle(QString strSytle)
{
	tabBar()->setStyleSheet(strSytle);
}
