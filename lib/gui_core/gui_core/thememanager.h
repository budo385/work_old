#ifndef THEMEMANAGER_H
#define THEMEMANAGER_H


#include <QString>


/*!
	\class  ThemeManager
	\brief  Global theme manager
	\ingroup GUICore

	Get current bitmaps/icons, emits signal when user changes theme/skin
*/

class ThemeManager 
{

public:
	enum Themes
	{
		THEME_BLUE_TEMPLE=0,
		THEME_GRAYHOUND,
		THEME_CABERNET,
		THEME_FOREST,
		THEME_SMARAGD
	};

	enum ViewModes
	{
		VIEW_PRO=0,
		VIEW_EASY,
		VIEW_SIDEBAR,
	};

	void OnStartUp(); 
	void OnUserLogin(); 
	void OnUserLogout(); 

	static void SetCurrentThemeID(int nThemeID); 
	static int GetCurrentThemeID(){return m_nCurrentThemeID;}
	static void SetViewMode(int nViewID);
	static int GetViewMode(){return m_nViewModeID;}

	static QString GetCEMenuButtonStyle();
	static QString GetCEMenuButtonStyle_Ex();
	static QString GetCEMenuButtonStyleLighter();
	static QString GetCEDropZonePicture();
	static QString GetCEDropZoneBkgStyle();
	static QString GetToolbarButtonStyle();
	static QString GetToolbarBkgStyle();
	static QString GetToolbarTextColor();
	static QString GetHeaderRight();
	static QString GetHeaderCenter();
	static QString GetHeaderLeft(QString strCode);
	static QString GetSidebar_HeaderRight();
	static QString GetSidebar_HeaderCenter();
	static QString GetSidebar_HeaderLeft();
	static QString GetSidebar_FooterRight();
	static QString GetSidebar_FooterCenter();
	static QString GetSidebar_FooterLeft();
	static QString GetSidebar_CenterHandle();
	static QString GetSidebar_FooterHandle();
	static QString GetSidebar_Bkg();
	static QString GetSidebarCommToolBar_Bkg();
	static QString GetSidebarActualName_Font();
	static QString GetSidebarChapter_Bkg();
	static QString GetSidebarChapter_Font(QString strFontWeight = QString("900"), QString strFontStyle = QString("italic"), QString strFontSize = QString("16"));
	static QString GetAvatarBkg();
	static QString GetContactGridBkg();
	static QString GetTableViewBkg(QString strParentObjectName="");
	static QString GetTreeBkg();
	static QString GetLisViewBkg();
	static QString GetGlobalWidgetStyle();
	static QString GetMobileFrameNameBkg();
	static QString GetMobileBkg();
	static QString GetMobileSolidBkg_Dark();
	static QString GetMobileSolidBkg_Light();
	static QString GetMobileChapter_Font();
	static QString GetMobileControls_Font();
	static QString GetMobileStatusBarBackground();
	static QString GetMobileLeftHeaderBackground();
	static QString GetMobileCenterHeaderBackground();
	static QString GetMobileRightHeaderBackground();
	static QString GetTabBarStyle();
	static QString GetBorderStyle();

	static void	   ExtractRGBFromColorString(QString strColor,int &R,int &G,int &B);

//private:
	static QString GetThemePrefix(int nThemeID);

	static QString m_strThemePrefix;
	static int m_nCurrentThemeID;
	static int m_nViewModeID;
};

#endif // THEMEMANAGER_H
