#include "gui_helper.h"
#include <QFileInfo>
#include "trans/trans/xmlutil.h"
#include <QApplication>
#include <QDesktopWidget>
static DbRecordSet g_lstWindowProperties;
QString g_strLastDir_FileOpen="";

#include "common/common/datahelper.h"

void GUI_Helper::SetButtonTextIcon(QPushButton *pWidget,QString strText,QString strIconPath)
{
	//remove existing text/layout/children
	pWidget->setText("");
	
	QLabel *label = pWidget->findChild<QLabel *>("iconWidget");
	QLabel *label1 = pWidget->findChild<QLabel *>("textWidget");
	if(NULL == label || NULL == label1)
	{
		//our layout does not exist, create a new layout
		delete pWidget->layout();

		QHBoxLayout *layout = new QHBoxLayout();
		layout->setMargin(0);
		layout->setSpacing(3);

		layout->addStretch(5);

		//add icon always
		label = new QLabel(pWidget);
		label->setObjectName("iconWidget");
		label->setPixmap(QPixmap(strIconPath));	//set icon always (to override possible previous value)

		layout->addWidget(label);

		label1 = new QLabel(pWidget);
		label1->setObjectName("textWidget");
		label1->setText(strText);

		//IMPORTANT: use already set button text color and font
		QPalette palette = pWidget->palette();
		palette.setColor(QPalette::Active, QPalette::WindowText, palette.color(QPalette::Active, QPalette::ButtonText));
		palette.setColor(QPalette::Inactive, QPalette::WindowText, palette.color(QPalette::Active, QPalette::ButtonText));
		label1->setPalette(palette);
		label1->setFont(pWidget->font());

		label1->setAlignment(Qt::AlignLeft|Qt::AlignVCenter);
		layout->addWidget(label1);

		layout->addStretch(5);

		//set new layout 
		pWidget->setLayout(layout);

		return;
	}

	//set icon always (to override possible previous value)
	label->setPixmap(QPixmap(strIconPath));

	//IMPORTANT: use already set button text color and font
	QPalette palette = pWidget->palette();
	SetButtonColor(label1, palette.color(QPalette::Active, QPalette::ButtonText));
	label1->setFont(pWidget->font());

	label1->setText(strText);
	pWidget->show();
}

void GUI_Helper::SetButtonColor(QWidget *pWidget, QColor color)
{
	QPalette palette = pWidget->palette();
	palette.setColor(QPalette::Active, QPalette::Button, color);
	palette.setColor(QPalette::Inactive, QPalette::Button, color);

	pWidget->setPalette(palette);
}

void GUI_Helper::SetButtonTextColor(QWidget *pWidget, QColor color)
{
	QPalette palette = pWidget->palette();
	palette.setColor(QPalette::Active, QPalette::ButtonText, color);
	palette.setColor(QPalette::Inactive, QPalette::ButtonText, color);
	pWidget->setPalette(palette);
}

void GUI_Helper::SetWidgetFontWeight(QWidget *pWidget, int weight)
{
	//set font bold
	QFont font = pWidget->font();
	font.setWeight(weight);
	pWidget->setFont(font);
}

void GUI_Helper::AdjustFixedWidget(QWidget *pWidgetExpandning1,QWidget *pWidgetFixed, bool bHorizontal)
{
	//set policies:
	if(bHorizontal)
	{
		QSizePolicy sizePolicy27(QSizePolicy::Preferred, QSizePolicy::Expanding);
		sizePolicy27.setHorizontalStretch(1);
		sizePolicy27.setVerticalStretch(0);
		pWidgetFixed->setSizePolicy(sizePolicy27);
	}
	else
	{
		QSizePolicy sizePolicy27(QSizePolicy::Expanding, QSizePolicy::Preferred);
		sizePolicy27.setHorizontalStretch(0);
		sizePolicy27.setVerticalStretch(1);
		pWidgetFixed->setSizePolicy(sizePolicy27);
	}

	

	//set policies:
	QSizePolicy sizePolicy28(QSizePolicy::Expanding, QSizePolicy::Expanding);
	if(bHorizontal)
	{
		sizePolicy28.setHorizontalStretch(40);
		sizePolicy28.setVerticalStretch(0);
	}
	else
	{
		sizePolicy28.setHorizontalStretch(0);
		sizePolicy28.setVerticalStretch(40);
	}

	pWidgetExpandning1->setSizePolicy(sizePolicy28);
}



void GUI_Helper::CreateStyledButton(QPushButton *pWidget,QString strIconPath,QString strTextHtml, int nIdent,QString strStyle, int nSpacing,bool bUseButtonText, int nIconWidth, int nIconHeight)
{

	if (bUseButtonText)
	{
		pWidget->setText(strTextHtml);
	}
	else
	{
		QHBoxLayout *layout = new QHBoxLayout();
		layout->setMargin(0);
		layout->setSpacing(nSpacing);
		layout->addSpacing(nIdent);
		if (!strIconPath.isEmpty())
		{
			QLabel *label = new QLabel;
			if (nIconWidth!=-1 && nIconHeight!=-1)
				label->setPixmap(QPixmap(strIconPath).scaled(nIconWidth,nIconHeight));
			else
				label->setPixmap(QPixmap(strIconPath));

			layout->addWidget(label);
		}
		if (!strTextHtml.isEmpty())
		{
			QLabel *label = new QLabel(pWidget);
			label->setTextInteractionFlags(Qt::NoTextInteraction);
			label->setText(strTextHtml);
			layout->addWidget(label);
			//connect(m_label,SIGNAL(Clicked(int)),this,SLOT(OnClick()));
		}
		layout->addStretch(1);

		if ((!strIconPath.isEmpty() && strTextHtml.isEmpty()) || (strIconPath.isEmpty() && !strTextHtml.isEmpty()))
			layout->insertStretch(0,1);
				
		
		pWidget->setLayout(layout);
	}


	//style:
	pWidget->setStyleSheet(strStyle);

}


//sets bkg image
//strFontColor = white, red, green, black, yellow, or rgb(xxx,xxx,xxx) as string
void GUI_Helper::SetStyledButtonColorBkg(QPushButton *pWidget,int nColor, QString strFontColor)
{
	QString strBkgImage;
	QString strFontStyle=GetFontStyleFromWidget(pWidget);
	QString strLabelFont="QLabel {color:"+strFontColor+"; "+strFontStyle+"}";


	switch(nColor)
	{
	case BTN_COLOR_GREEN:
		strBkgImage=strLabelFont+" QPushButton { "+strFontStyle+ " border-width: 4px; color: "+strFontColor+";border-image:url(:Button_Green.png) 4px 4px 4px 4px stretch stretch }";
		break;
	case BTN_COLOR_GREEN_1:
		strBkgImage=strLabelFont+ " QPushButton { "+strFontStyle+ "border-width: 4px; color: "+strFontColor+";border-image:url(:Button_Green.png) 4px 4px 4px 4px stretch stretch }";
		break;

	case BTN_COLOR_BROWN:
		strBkgImage=strLabelFont+" QPushButton { "+strFontStyle+ "border-width: 4px; color: "+strFontColor+";border-image:url(:Button_Red.png) 4px 4px 4px 4px stretch stretch }";
		break;
	case BTN_COLOR_PINK:
		strBkgImage=strLabelFont+" QPushButton { "+strFontStyle+ "border-width: 4px; color: "+strFontColor+";border-image:url(:Button_Violet.png) 4px 4px 4px 4px stretch stretch }";
	    break;
	case BTN_COLOR_SAND:
		strBkgImage=strLabelFont +" QPushButton { "+strFontStyle+ "border-width: 4px; color: "+strFontColor+";border-image:url(:Button_Orange.png) 4px 4px 4px 4px stretch stretch }";
	    break;
	case BTN_COLOR_YELLOW:
		strBkgImage=strLabelFont +" QPushButton { "+strFontStyle+ "border-width: 4px; color: "+strFontColor+";border-image:url(:Button_Yellow.png) 4px 4px 4px 4px stretch stretch }";
		break;

	}

	pWidget->setStyleSheet(strBkgImage);

}


QString GUI_Helper::GetFontStyleFromWidget(QPushButton *pWidget)
{

	//B.T. save font properties of button and set it to label, as label is placeholder for button text:
	QFont font=pWidget->font();
	QString strFamily="font-family:\""+font.family()+"\"; ";
	QString strSize="font-size:"+QVariant(QFontInfo(font).pixelSize()).toString()+"px; ";  //font info hopefully returns always right font
	QString strStyle;
	switch(font.style())
	{
	case QFont::StyleItalic:
		strStyle="italic";
		break;
	case QFont::StyleOblique:
		strStyle="oblique";
		break;
	default:
		strStyle="normal";
		break;
	}
	strStyle="font-style:"+strStyle+"; ";
	QString strWeight;
	switch(font.weight())
	{
	case QFont::Bold:
		strWeight="bold";
		break;
	case QFont::DemiBold: //TOFIX: mapping for other wirght: light, black,etc..
		strWeight="700";
		break;
	default:
		strWeight="normal";
		break;
	}

	strWeight="font-weight:"+strWeight+";";
	return strFamily+strSize+strStyle+strWeight;
}


void GUI_Helper::SetTabFont(QTabWidget *pWidget ,QString strStyleFont)
{
	pWidget->setProperty("TabFontSpecial",0);
	int nSize=pWidget->count();
	for (int i=0;i<nSize;++i)
	{
		QWidget *pChilds=pWidget->widget(i);
		pChilds->setProperty("TabFontSpecial",0);
	}

	if (strStyleFont.isEmpty())
		strStyleFont="{font-weight:bold; font-style:italic; font-size:14px}";

	strStyleFont="QTabWidget[TabFontSpecial=\"1\"] "+strStyleFont;

	strStyleFont="font-weight:bold"; //; font-style:italic; font-size:14px";
	strStyleFont="color:blue"; //; font-style:italic; font-size:14px";

	//pWidget->tabBar()->setStyleSheet(strStyleFont);
	//pWidget->setStyleSheet(strStyleFont);
}








int GUI_Helper::GetMenuWidthForFUI(int nFUIType)
{
	int nRow=g_lstWindowProperties.find(0,nFUIType,true);
	if (nRow==-1)
		return 250; //default
	else
		return g_lstWindowProperties.getDataRef(nRow,"MENU_WIDTH").toInt();
}

void GUI_Helper::SetMenuWidthForFUI(int nFUIType,int nWidth)
{

	int nRow=g_lstWindowProperties.find(0,nFUIType,true);
	if (nRow==-1)
	{
		g_lstWindowProperties.addRow();
		int nRowLast=g_lstWindowProperties.getRowCount()-1;
		g_lstWindowProperties.setData(nRowLast,0,nFUIType);
		g_lstWindowProperties.setData(nRowLast,"MENU_WIDTH",nWidth);
	}
	else
	{
		g_lstWindowProperties.setData(nRow,0,nFUIType);
		g_lstWindowProperties.setData(nRow,"MENU_WIDTH",nWidth);
	}

}

void GUI_Helper::SaveWindowProperties(QByteArray &byteVal)
{
	if (g_lstWindowProperties.getRowCount()>0)
	{
		byteVal=XmlUtil::ConvertRecordSet2ByteArray_Fast(g_lstWindowProperties);
	}

}



void GUI_Helper::ReadWindowProperties(QByteArray &byteVal)
{
	
	g_lstWindowProperties.destroy(); //destroy to enable load for future
	
	//QByteArray byteVal=g_pSettings->GetPersonSetting(APPEAR_WINDOW_PROPERTIES).toByteArray();
	if (byteVal.size()==0)
	{
		g_lstWindowProperties.destroy();
		g_lstWindowProperties.addColumn(QVariant::Int,"FUI_TYPE");
		g_lstWindowProperties.addColumn(QVariant::Int,"MENU_WIDTH");
		g_lstWindowProperties.addColumn(QVariant::Int,"WIDTH");
		g_lstWindowProperties.addColumn(QVariant::Int,"HEIGHT");
		return;
	}

	g_lstWindowProperties=XmlUtil::ConvertByteArray2RecordSet_Fast(byteVal);
	if (g_lstWindowProperties.getRowCount()==0)
	{
		g_lstWindowProperties.destroy();
		g_lstWindowProperties.addColumn(QVariant::Int,"FUI_TYPE");
		g_lstWindowProperties.addColumn(QVariant::Int,"MENU_WIDTH");
		g_lstWindowProperties.addColumn(QVariant::Int,"WIDTH");
		g_lstWindowProperties.addColumn(QVariant::Int,"HEIGHT");
		return;
	}
}


bool GUI_Helper::IsWidgetOutOfScreen(QWidget *pWidget)
{
	if (!pWidget)
		return false;
	QRect desktopRec=QApplication::desktop()->availableGeometry(QApplication::desktop()->screenNumber(pWidget));
	QPoint currentPos=pWidget->mapToGlobal(QPoint(0,0));

	if ((desktopRec.y()+currentPos.y()+pWidget->frameGeometry().height())>(desktopRec.y()+desktopRec.height()))
		return true;
	else
		return false;

}

void GUI_Helper::GetDateRangeComboItems(QComboBox *cboRangeSize)
{
	cboRangeSize->addItem("-", DataHelper::UNDEFINED);
	cboRangeSize->addItem(tr("Day"), DataHelper::DAY);
	cboRangeSize->addItem(tr("3 Days"), DataHelper::THREE_DAYS);
	cboRangeSize->addItem(tr("Business Days"), DataHelper::BUSS_DAYS);
	cboRangeSize->addItem(tr("Week"), DataHelper::WEEK);
	cboRangeSize->addItem(tr("7 Days"), DataHelper::SEVEN_DAYS);
	cboRangeSize->addItem(tr("Two Weeks"), DataHelper::TWO_WEEK);
	cboRangeSize->addItem(tr("14 Days"), DataHelper::FOURTEEN_DAYS);
	cboRangeSize->addItem(tr("Month"), DataHelper::MONTH);
	cboRangeSize->addItem(tr("Year"), DataHelper::YEAR); 
	cboRangeSize->setCurrentIndex(0);
}
