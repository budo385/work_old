#ifndef MACROS_H
#define MACROS_H

#include <QFile>

#if defined _DEBUG || defined WINCE
	#include <QTime>

#define _START_TIMER(timer_name)	\
	QTime timer_name;				\
	timer_name.start();

#define _STOP_TIMER(timer_name)		\
	qDebug() << #timer_name << timer_name.elapsed();

#else
	#define _START_TIMER(timer_name)
	#define _STOP_TIMER(timer_name)	
#endif


//Error macro.
#include <QtWidgets/QMessageBox>

#define _CHK_ERR_NO_MSG(X) if(!X.IsOK())															\
					{																				\
					return;																			\
					}

#define _CHK_ERR(X) if(!X.IsOK())																	\
					{																				\
					QMessageBox::critical(NULL,(QString)QObject::tr("Error"),X.getErrorText(),	\
					QMessageBox::Ok,QMessageBox::NoButton,				\
					QMessageBox::NoButton);								\
					return;																		\
					}

#define _CHK_ERR_RET_BOOL(X) if(!X.IsOK())															\
					{																				\
					QMessageBox::critical(NULL,(QString)QObject::tr("Error"),X.getErrorText(),		\
					QMessageBox::Ok,QMessageBox::NoButton,											\
					QMessageBox::NoButton);															\
					return false;																	\
					}																				\
					else																			\
					return true;

#define _CHK_ERR_RET_BOOL_ON_FAIL(X) if(!X.IsOK())															\
					{																				\
					QMessageBox::critical(NULL,(QString)QObject::tr("Error"),X.getErrorText(),		\
					QMessageBox::Ok,QMessageBox::NoButton,											\
					QMessageBox::NoButton);															\
					return false;																	\
					}																				

#define _CHK_ERR_NO_RET(X) if(!X.IsOK())															\
					{																				\
					QMessageBox::critical(NULL,(QString)QObject::tr("Error"),X.getErrorText(),		\
					QMessageBox::Ok,QMessageBox::NoButton,											\
					QMessageBox::NoButton);															\
					}
			


#define _DUMP_FILE(var)	{QFile body("D:/out.html");			\
						body.open(QIODevice::WriteOnly);	\
						body.write(var);					\
						body.close();}



#endif 