#ifndef LISTWIDGET_H
#define LISTWIDGET_H



#include <QListWidget>
#include "gui_core/gui_core/dragdropinterface.h"
#include <QAction>


/*!
	\class ListWidget
	\brief  ListWidget with dropping handler..
	\ingroup GUICore_CustomWidgets

*/
class ListWidget : public QListWidget, public DragDropInterface
{
	Q_OBJECT

public:
	ListWidget(QWidget *parent);
	~ListWidget();

	void EnableFileDrop(){m_bFileDropEnabled=true;}

	void ClearData();
	void SetData(DbRecordSet &data,int nColumnToDisplay=0, bool bShowCheckBoxes=false);
	void GetData(DbRecordSet &data);
	void GetDroppedFiles(DbRecordSet &data);
	void SetCheckBoxSelection(QList<bool> lstChecked);
	void GetCheckBoxSelection(QList<bool> &lstChecked);

	void SetContextMenuActions(QList<QAction*>& lstActions){m_lstActions=lstActions;};		//set actions for cnxt menu (alternative approach for setting context menu)
	QList<QAction*>& GetContextMenuActions(){ return m_lstActions; };

	//Drop operations by widget:
	//Qt::DropActions supportedDropActions() const {return DragDropInterface::supportedDropActions();};
	Qt::DropActions supportedDropActions() const;
	void dropEvent(QDropEvent *event);
	void dragEnterEvent ( QDragEnterEvent * event );
	void dragMoveEvent ( QDragMoveEvent * event );

	//our custom drop handlers:
	int GetDropType() const;
	virtual void GetDropValue(DbRecordSet &values, bool bSelectItemChildren = false) const;
	virtual void DropHandler(QModelIndex index, int nDropType, DbRecordSet &DroppedValue,QDropEvent *event);


	//for public access:
	DbRecordSet m_lstData;

signals:
	void SignalSelectionChanged();
	void SignalSelectionDeleted();
	void SignalFilesDropped();
	void SignalEntityDropped(int,DbRecordSet);

public slots:
	void DeleteSelection();

private slots:
	void SelectionChanged();

protected:
	
	void contextMenuEvent(QContextMenuEvent *event);
	QList<QAction*> m_lstActions;
	QAction *m_pActDelete;
	bool m_bFileDropEnabled;
	DbRecordSet m_lstFilesDropped;
};

#endif // LISTWIDGET_H
