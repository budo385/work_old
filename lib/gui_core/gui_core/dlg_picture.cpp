#include "dlg_picture.h"
#include "picturehelper.h"



Dlg_Picture::Dlg_Picture(QWidget *parent)
    : QDialog(parent)
{
	ui.setupUi(this);

	ui.framePicture->Initialize(PictureWidget::MODE_FULL_DIALOG,(GuiDataManipulator *)0);

	connect(ui.framePicture,SIGNAL(PictureChanged()),this,SLOT(PictureChanged()));
	
	this->setWindowTitle(tr("Picture Viewer"));//set dialog title:
}

Dlg_Picture::~Dlg_Picture()
{

}


//change dialog title to format of picture
void Dlg_Picture::PictureChanged()
{
	int nWidth,nHeight,nDepth,nSizePrev,nSizeFull;
	ui.framePicture->GetPictureInfo(nWidth,nHeight,nDepth,nSizePrev,nSizeFull);
	QString strTitle=tr("Picture Viewer")+QString(": ")+QVariant(nWidth).toString()+" x "+QVariant(nHeight).toString()+" x "+QVariant(nDepth).toString();
	this->setWindowTitle(strTitle);
}



QPixmap Dlg_Picture::GetPixMap()
{
	return ui.framePicture->GetPixmap();
}


void Dlg_Picture::SetPixMap(QPixmap& pic)
{
	ui.framePicture->SetPixmap(pic);
}


void Dlg_Picture::GetPictureBinaryData(QByteArray &bytePicture)
{
	ui.framePicture->GetPictureBinaryData(bytePicture);
}

void Dlg_Picture::SetPictureBinaryData(QByteArray &bytePicture)
{
	ui.framePicture->SetPictureFromBinaryData(bytePicture);
}


void Dlg_Picture::GetPictureBinaryDataPreview(QByteArray &bytePicture)
{
	QPixmap pixmap=ui.framePicture->GetPixmap();
	PictureHelper::ResizePixMapToPreview(pixmap);
	PictureHelper::ConvertPixMapToByteArray(bytePicture,pixmap,ui.framePicture->GetCurrentPictureFormat());
}

//set edit mode
void Dlg_Picture::SetEditMode(bool bEditMode)
{
	ui.framePicture->SetEditMode(bEditMode);

}

//get curr format
QString Dlg_Picture::GetCurrentPictureFormat()
{
	return ui.framePicture->GetCurrentPictureFormat();
}


//-----------------------------------------------------------------
//						EVENT HANDLERS
//-----------------------------------------------------------------
void Dlg_Picture::on_btnOK_clicked()
{
	//if changed return true
	if (ui.framePicture->IsPictureChanged())
		done(1);
	else
		done(0);
}


void Dlg_Picture::on_btnCancel_clicked()
{
	done(0);
}



