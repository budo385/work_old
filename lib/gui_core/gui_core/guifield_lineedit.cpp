#include "guifield_lineedit.h"




//------------------------------------------------------
//			LINE EDIT
//------------------------------------------------------

GuiField_LineEdit::GuiField_LineEdit(QString strFieldName,QWidget *pWidget,GuiDataManipulator* pDataManager,DbView *TableView, QLabel *pLabel)
:GuiField(strFieldName,pWidget,pDataManager,TableView,pLabel)
{
	//test column type in datasource:
	//Q_ASSERT(pDataManager->GetDataSource()->getColumnType(pDataManager->GetDataSource()->getColumnIdx(strFieldName))==QVariant::String);


	//register signal textChanged() -> ChangeData() if lineEdit:
	m_pEditWidget=dynamic_cast<QLineEdit*>(pWidget);
	Q_ASSERT(m_pEditWidget!=NULL);

	//Note: to avoid connecting to textchanged which is fired at every char change
	// we will use two signals: after focus (editingfinished and dirty flagger, which is resseted at refreshdisplay!!
	connect(m_pEditWidget,SIGNAL(textChanged ( const QString &  )),this,SLOT(FieldChanged(const QString &)));
	//connect(m_pEditWidget,SIGNAL(textEdited ( const QString &  )),this,SLOT(SetDirtyFlag()));
	//connect(m_pEditWidget,SIGNAL(editingFinished()),this,SLOT(FieldChanged()));
	m_bDirty=false;

}

/*!
	If edit line widget, then this slot is invoked when text changes
	NOTE: assert is fired if row is out of bound
*/
void GuiField_LineEdit::FieldChanged(const QString &txt)
{
	//only register if dirty:
	//if (!m_bDirty) return;

	QVariant value=txt;
	//if(ValidateUserInput(value))
		m_pDataManager->ChangeData(m_nCurrentRow,m_nIndex,value);
}



/*!
	If data is changed from data manipulator, refresh content
	If data source is empty, content will be cleared.
*/
void GuiField_LineEdit::RefreshDisplay()
{
	m_pEditWidget->blockSignals(true);

	//check if current row is out of bound, if so clear content;
	if(m_nCurrentRow<m_pDataManager->GetDataSource()->getRowCount()) 
		m_pEditWidget->setText(m_pDataManager->GetDataSource()->getDataRef(m_nCurrentRow,m_nIndex).toString());
	else
		m_pEditWidget->setText("");		

	m_pEditWidget->blockSignals(false);
	//m_bDirty=false;
}



void GuiField_LineEdit::SetDirtyFlag()
{
	m_bDirty=true;

}
