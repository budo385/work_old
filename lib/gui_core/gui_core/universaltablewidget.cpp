#include "universaltablewidget.h"
#include "os_specific/os_specific/mime_types.h"
#include "multisortcolumn.h"
#include "db_core/db_core/dbsqltableview.h"
#include "gui_core/gui_core/universaltabledelegate.h"
#include "trans/trans/xmlutil.h"
#include "thememanager.h"
#include <QDrag>

//special cell widgets:
#include "universaltablecombobox.h"
#include "universaltablelist.h"
#include "universaltablemultilinetext.h"
#include "universaltablepicture.h"


#include <QtWidgets/QMessageBox>
#include <QCommonStyle>
#include <QTableWidgetSelectionRange>
#include <QMenu>


UniversalTableWidget::UniversalTableWidget(QWidget * parent)
:QTableWidget(parent),m_bEdit(false),m_lstData(NULL),m_pData(NULL),DragDropInterface(this)
{
	m_bWidgetCopyContentRefresh=false;
	m_bExternalDataManipulator = false;
	m_bSelectionChangeEmitted=false;
	m_bGridContainsBoldItems=false;
}


UniversalTableWidget::~UniversalTableWidget()
{
	//only if not external
	if(m_pData!=NULL && !m_bExternalDataManipulator) delete m_pData;
}


/*!
	Initializes table for display. Moved out from constructor to enable connecting to virtual slots!

	\param lstData			- data source (default GuiDataManipulator will be used)
	\param bEnableDrop		- true to enable drop
	\param bEnableDrag		- true to enable drag
	\param bSingleSelection	- true to allow only single row select
	\param bSelectRows		- if false, then every cell is selected separately
	\param nVerticalRowSize	- vertical size of row

*/
void UniversalTableWidget::Initialize(DbRecordSet *lstData,bool bEnableDrop, bool bEnableDrag,bool bSingleSelection,bool bSelectRows, int nVerticalRowSize,bool bDisableCntxMenu, bool bIgnoreDropHandler)
{

	//can not initialize table view twice!!!
	//Q_ASSERT(m_pData!=NULL);

	//store data source in default data manipulator:
	if(!m_bExternalDataManipulator)
		m_pData = new GuiDataManipulator(lstData);
	
	m_lstData=lstData;

	//connect(m_pData,SIGNAL(RefreshDisplay()),this,SLOT(RefreshDisplay()));
	//connect(m_pData,SIGNAL(RefreshDisplayRow(int)),this,SLOT(RefreshDisplayRow(int)));
	//connect(m_pData,SIGNAL(RefreshSelection()),this,SLOT(RefreshSelection()));

	//issue 1649
	DummyNoFocusDelegate* DeafultTableDelegate = new DummyNoFocusDelegate(this);
	setItemDelegate (DeafultTableDelegate);

	//selection mode:
	if(bSelectRows) 
		setSelectionBehavior( QAbstractItemView::SelectRows);
	else
		setSelectionBehavior( QAbstractItemView::SelectItems);

	//if true then only one row can be selected at time:
	if(bSingleSelection) 
		setSelectionMode(QAbstractItemView::SingleSelection);
	else
		setSelectionMode(QAbstractItemView::ExtendedSelection);


	//Drag & Drop:
	setAcceptDrops(bEnableDrop);
	setDragEnabled(bEnableDrag);
	if(bEnableDrop) setDropIndicatorShown(true);

	//for editing one cell:
	connect(this,SIGNAL(cellChanged(int,int)),this,SLOT(CellEdited(int,int)));
	connect(this,SIGNAL(itemSelectionChanged()),this,SLOT(SelectionChanged()));
	connect(this,SIGNAL(cellClicked(int,int)),this,SLOT(CellClicked(int,int)));
	

	//for sorting:
	connect((QObject *)this->horizontalHeader(),SIGNAL(sectionPressed(int)),this,SLOT(SortColumn(int)));
	setSortingEnabled(false); //we implement our custom sorting mechanism
	disconnect(horizontalHeader(), SIGNAL(sectionPressed(int)), this, SLOT(selectColumn(int)));
	//set sort indicator:
	horizontalHeader()->setSortIndicatorShown(true);

	//row size:

	//this->verticalHeader()->setDefaultAlignment(Qt::AlignLeft);
	//this->verticalHeader()->setDefaultSectionSize(25);
	//this->verticalHeader()->setMovable(true);

	m_nVerticalRowSize=nVerticalRowSize;
	m_bIgnoreDropHandler = bIgnoreDropHandler;

	//create default context menu if empty list
	if(!bDisableCntxMenu && m_lstActions.size()==0)
		CreateContextMenuActions(m_lstActions);
		
	//set all actions to false:
	SetEditMode(false);

	verticalHeader()->setDefaultSectionSize(nVerticalRowSize);

	SetStyleSheetGlobal();
}




/*!
	Same as above, except it accepts pDataManipulator
*/
void UniversalTableWidget::Initialize(GuiDataManipulator *pDataManipulator,bool bEnableDrop, bool bEnableDrag,bool bSingleSelection,bool bSelectRows, int nVerticalRowSize,bool bDisableCntxMenu, bool bIgnoreDropHandler)
{
	m_bExternalDataManipulator = true;
	m_pData=pDataManipulator;

	Initialize(pDataManipulator->GetDataSource(),bEnableDrop, bEnableDrag,bSingleSelection, bSelectRows, nVerticalRowSize,bDisableCntxMenu);
}


/*!
	Initializes table for display, can be called after Initialize function
	Can be called infinite times: all 

	\param lstColumnSetup	- list in MVIEW_GRID_COLUMN_SETUP format
*/
void UniversalTableWidget::SetColumnSetup(DbRecordSet &lstColumnSetup)
{
	//can not initialize table without data source
	Q_ASSERT(m_lstData!=NULL);

	//create columns:
	m_lstColumnSetup=lstColumnSetup;
	m_lstColumnSetup.sort(4); //sort by position


	m_lstColumnMapping.clear();
	//map columns from datasource
	int nSize=m_lstColumnSetup.getRowCount();

	int nColPos;
	for( int i=0;i<nSize;++i)
	{
		nColPos=m_lstData->getColumnIdx(m_lstColumnSetup.getDataRef(i,1).toString());
		/*
		if (nColPos==-1)
		{
			qDebug()<<m_lstColumnSetup.getDataRef(i,1).toString();
			Q_ASSERT(nColPos!=-1); //column defined in setup, but not exists in recordset
			continue;
		}
		*/
		//BT: if no datasource: leave empty -1 must be handled!
		m_lstColumnMapping[i]=nColPos; //maps (column displayed=column in list) 
	}
	
	//create header:
	RestoreHeaderSetup();

	//define sort order, look up on grid, destroy previous:
	int nSection=horizontalHeader()->sortIndicatorSection();
	if (nSection>=0 && m_lstColumnMapping[nSection]>=0)
	{
		m_lstLastSort.clear();
		m_lstLastSort<<SortData(m_lstColumnMapping[nSection],horizontalHeader()->sortIndicatorOrder());
	}

	//set to false:
	//SetEditMode(false);

	RefreshDisplay();

}



/*!
	Get column setup: column widths are updated to new

	\param lstColumnSetup	- list in MVIEW_GRID_COLUMN_SETUP format
*/
void UniversalTableWidget::GetColumnSetup(DbRecordSet &lstColumnSetup)
{
	int nSize=m_lstColumnSetup.getRowCount();
	//update size:
	for( int i=0;i<nSize;++i)
		m_lstColumnSetup.setData(i,3,columnWidth(i));
	
	lstColumnSetup=m_lstColumnSetup;
}


/*!
	Sets if column is editable, only effective after resfresh

	\param nColumn		- column number as added inside setup
	\param bEditable	- true,false
*/
void UniversalTableWidget::SetColumnEditable(int nColumn,bool bEditable)
{
	m_lstColumnSetup.setData(nColumn,5,bEditable);
}


/*!
	Based on datasource, refreshes contents of displayed items.
	Call this when data is changed (add, indert, delete, select/deselect)
	If only selection is change, call reshresh selection (clears current, sets new one)
	Column count must remain!
*/
void UniversalTableWidget::RefreshDisplay(bool bApplyLastSortModel)
{
	if (m_lstData==NULL) return;
	if (m_lstData->getColumnCount()==0) return;

	setUpdatesEnabled(false);

	//columns must match
	int nRowSize=m_lstData->getRowCount();

	//disable signals
	blockSignals(true);

	//set new row count
	setRowCount(nRowSize);

	//resort data list, if last sort enabled:
	//WARNING: possible bug if some1 is relaying on data to be aligned as he/she wishes
	if (bApplyLastSortModel && m_lstLastSort.size()>0)
	{
		m_lstData->sortMulti(m_lstLastSort);
	}


	///QTime time;
	//time.start();
	RefreshRowContentFast();


	//repair bug in vertical header refresh (resize up-down 1 pixel horizontal section):
	horizontalHeader()->resizeSection(0,horizontalHeader()->sectionSize(0)+1);
	horizontalHeader()->resizeSection(0,horizontalHeader()->sectionSize(0)-1);


	//enable signals
	blockSignals(false);	

	RefreshSelection();

	TableStateChanged(); //notify other that table state probably changed

	setUpdatesEnabled(true);
}


/*!
	Based on datasource, refreshes contents of displayed items in given row.
	Faster if data is changed only in one row.

	\param nRow	- row
*/
void UniversalTableWidget::RefreshDisplayRow(int nRow)
{
	
	setUpdatesEnabled(false);

	//disable signals
	blockSignals(true);

	//columns must match
	int nRowSize=m_lstData->getRowCount();
	//set new row count
	setRowCount(nRowSize);

	//copy values
	RefreshRowContentFast(nRow);

	//selectioN:
	if ( m_lstData->isRowSelected(nRow))
	{
		QTableWidgetSelectionRange range(nRow,0,nRow,columnCount()-1);
		setRangeSelected(range,true);
	}

	//repair bug in vertical header refresh (resize up-down 1 pixel horizontal section):
	horizontalHeader()->resizeSection(0,horizontalHeader()->sectionSize(0)+1);
	horizontalHeader()->resizeSection(0,horizontalHeader()->sectionSize(0)-1);

	//enable signals
	blockSignals(false);

	TableStateChanged(); //notify other that table state probably changed

	setUpdatesEnabled(true);
}






/*!
	Inserts row widget items at given position if not exists.
	If row exists updates data from datasource
	Note: 
	- setRowCount() must be set, before calling this function
	- disable signal for CELL edit & CELL SELECTED! before

	\param nSingleRow	- if -1 whole content, else just that row

*/
void UniversalTableWidget::RefreshRowContentFast(int nStartRow)
{
	//loops through rows till column

	//creates widget of specific type for each column
	int nRowCount=rowCount();
	int nColCount=columnCount();
	if (nStartRow!=-1)
		nRowCount=nStartRow+1;
	else
		nStartRow=0;

	//issue 1684:
	QFont strStyle1("Arial",8,QFont::Normal);
	strStyle1.setStyleStrategy(QFont::PreferAntialias);

	QFont strStyle2("Arial Narrow",8,QFont::Normal);
	strStyle2.setStyleStrategy(QFont::PreferAntialias);

	if (!verticalHeader()->isHidden())
	{
		//first refresh icon:
		for( int i=nStartRow;i<nRowCount;++i)
		{
			QIcon icon;
			QString strStatusTip;
			//bool bResizeVert=false;
			GetRowIcon(i,icon,strStatusTip);
			QTableWidgetItem *vertItem = verticalHeaderItem(i);

			if (icon.isNull() && vertItem==NULL)		//only fly by if icon is null and item is not existant 
				continue;

			if(vertItem==NULL)
			{
				vertItem = new QTableWidgetItem(0);
				//vertItem->setTextAlignment(Qt::AlignRight);
				vertItem->setTextAlignment(Qt::AlignLeft);
				//vertItem->setData(Qt::DisplayRole,nRow+1);
				vertItem->setFont(strStyle1);
				vertItem->setText(QVariant(i+1).toString());
				vertItem->setIcon(icon);
				vertItem->setToolTip(strStatusTip);
				setVerticalHeaderItem(i,vertItem);
			}
			else
			{
				vertItem->setIcon(icon);
				vertItem->setToolTip(strStatusTip);
			}
		}
	}

	

	//loop by cols/rows
	bool bNewItem=false;
	QString strText;

	for( int i=0;i<nColCount;++i)
	{

		int nCol=m_lstColumnMapping[i];

		//BT--------------------------------: create dummy item for dummy column without datasource
		if (nCol==-1) //create dummy items:
		{
			for( int nRow=nStartRow;nRow<nRowCount;++nRow)
			{
				QTableWidgetItem *newItem=item(nRow,i);
				if(newItem==NULL)	//create new if not
				{
					newItem = new QTableWidgetItem;
					//non EDIT always for dummies
					newItem->setFlags(  Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled | Qt::ItemIsSelectable | Qt::ItemIsEnabled); //Qt::ItemIsEnabled
					newItem->setFont(strStyle2);
					setItem(nRow,i,newItem);
				}
				
			}

			continue; //can be -1-> skip all
		}
		//BT--------------------------------: create dummy item for dummy column without datasource
			

		//Add new one if empty
		int nColType=m_lstColumnSetup.getDataRef(i,2).toInt();
		int nDataType=m_lstData->getColumnType(nCol);
		bool bEditable=m_lstColumnSetup.getDataRef(i,5).toBool();
		//int n
		bool bIsCheckBox=COL_TYPE_CHECKBOX==m_lstColumnSetup.getDataRef(i,2).toInt();
		//int nColType=m_lstData->getColumnType(nCol);
		//bool bIsDate=QVariant::Date==m_lstData->getColumnType(nCol);


		if(nColType<COL_TYPE_COMBOBOX)
		{

			for( int nRow=nStartRow;nRow<nRowCount;++nRow)
			{

				QTableWidgetItem *newItem=item(nRow,i);
				if(newItem==NULL)	//create new if not
				{
					newItem = new QTableWidgetItem;
					bNewItem=true;

					//set item flags:
					if(m_bEdit && bEditable)
						if(!bIsCheckBox)
							newItem->setFlags(  Qt::ItemIsUserCheckable | Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled |  Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsEditable);
						else
							newItem->setFlags(  Qt::ItemIsUserCheckable | Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled |  Qt::ItemIsSelectable | Qt::ItemIsEnabled);
					else //non EDIT:
						newItem->setFlags(  Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled | Qt::ItemIsSelectable | Qt::ItemIsEnabled); //Qt::ItemIsEnabled
				}
				else
					bNewItem=false;


				//refresh data (format display for checkbook & date):
				switch(nColType)
				{
					case COL_TYPE_CHECKBOX:
						newItem->setCheckState(m_lstData->getDataRef(nRow,nCol).toBool() ? Qt::Checked : Qt::Unchecked); 
						break;
					case COL_TYPE_ICON:
						newItem->setIcon(QIcon(m_lstData->getDataRef(nRow,nCol).toString())); 
						break;
					default:
						{
							switch(nDataType)
							{
							case QVariant::Date:
								newItem->setData(Qt::DisplayRole,m_lstData->getDataRef(nRow,nCol).toDate().toString(Qt::LocaleDate));
								break;
							case QVariant::DateTime:
								{
									/*
									QString strDisplay;
									QDateTime datetime=m_lstData->getDataRef(nRow,nCol).toDateTime();
									qDebug()<<datetime;
									qDebug()<<m_lstData->getDataRef(nRow,nCol).toString();

									if (datetime.isValid())
									{
										strDisplay=datetime.date().toString(Qt::LocaleDate);
										strDisplay+=" "+datetime.time().toString("hh:mm:ss");
									}
									*/
									newItem->setData(Qt::DisplayRole,m_lstData->getDataRef(nRow,nCol).toDateTime().toString(Qt::LocaleDate));
								}
								break;
							default:
								newItem->setData(Qt::DisplayRole,m_lstData->getDataRef(nRow,nCol));
							}
						}
						break;
				}
				
	
				//essential to be last: speeds up 2x times
				if (bNewItem)
					setItem(nRow, i, newItem);
	
			}

		}
		else
		{	
			for( int nRow=nStartRow;nRow<nRowCount;++nRow)
			{
				QWidget *pWidget=cellWidget(nRow,i);

				//create if no created:
				if(pWidget==NULL)
				{

					pWidget=CreateCellWidgets(nRow,i,nColType);
					TableCellWidget *cellWidgetUniversal=dynamic_cast<TableCellWidget*>(pWidget);
					Q_ASSERT_X(cellWidgetUniversal!=NULL,"TABLE WIDGET","CELL WIDGET NOT SUBCLASSED FROM TableCellWidget");


					//m_lstColumnSetup.Dump();
					cellWidgetUniversal->SetData(nRow,i,m_lstData->getDataRef(nRow,nCol),m_lstColumnSetup.getDataRef(i,8).toString(),m_lstColumnSetup.getDataRef(i,5).toBool());
					if(bEditable) connect(cellWidgetUniversal->GetSignalEmiter(),SIGNAL(CellWidgetDataChange(int,int)),this,SLOT(CellWidgetChanged(int,int)));
					connect(cellWidgetUniversal->GetSignalEmiter(),SIGNAL(CellWidgetClicked(int,int)),this,SLOT(CustomTableCellClicked(int,int)));
					cellWidgetUniversal->SetEditMode(m_bEdit && bEditable);

					//set widget on table:
					setCellWidget(nRow,i,pWidget);


				}
				else	//just refresh display:
				{
					TableCellWidget *cellWidgetUniversal=dynamic_cast<TableCellWidget*>(pWidget);
					Q_ASSERT_X(cellWidgetUniversal!=NULL,"TABLE WIDGET","CELL WIDGET NOT SUBCLASSED FROM TableCellWidget");
					cellWidgetUniversal->RefreshData(m_lstData->getDataRef(nRow,nCol),m_bWidgetCopyContentRefresh);
					cellWidgetUniversal->SetEditMode(m_bEdit && bEditable);
				}
			}


		}

	}

}





//--------------------------------------------------------------
//								EDIT MODE
//--------------------------------------------------------------

/*!
	Table goes into edit mode (off by deafult)
	Warning: when exiting edit mode, clears selection

	\param bEdit - true, set edit mode
*/
void UniversalTableWidget::SetEditMode(bool bEdit)
{
	//BROKE SIGNAL, because this triggers signal
	blockSignals(true);

	//clear selection when exiting edit mode
	if(m_bEdit && !bEdit)
		m_lstData->clearSelection();

	m_bEdit=bEdit;
	int nRowSize=rowCount();
	int nColSize=columnCount();
	for(int nRow=0;nRow<nRowSize;++nRow)
		for(int nCol=0;nCol<nColSize;++nCol)
		{	
			
			//if edit and if column is editable, then edit
			int nColType=m_lstColumnSetup.getDataRef(nCol,2).toInt();
			bool bEditable=m_lstColumnSetup.getDataRef(nCol,5).toInt();
			bool bIsCheckBox=COL_TYPE_CHECKBOX==m_lstColumnSetup.getDataRef(nCol,2).toInt();
			
			if(nColType<COL_TYPE_COMBOBOX)
			{
				Q_ASSERT(item(nRow,nCol)!=NULL); //item must be created!!!

				if(m_bEdit && bEditable)
					if(!bIsCheckBox)
						item(nRow,nCol)->setFlags(  Qt::ItemIsUserCheckable | Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled |  Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsEditable);
					else
						item(nRow,nCol)->setFlags(  Qt::ItemIsUserCheckable | Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled |  Qt::ItemIsSelectable | Qt::ItemIsEnabled);
				else //non EDIT:
					item(nRow,nCol)->setFlags(  Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled | Qt::ItemIsSelectable | Qt::ItemIsEnabled); //Qt::ItemIsEnabled
			}
			else
			{
					TableCellWidget *cellWidgetUniversal=dynamic_cast<TableCellWidget*>(cellWidget(nRow,nCol));
					Q_ASSERT_X(cellWidgetUniversal!=NULL,"TABLE WIDGET","CELL WIDGET NOT SUBCLASSED FROM TableCellWidget");
					cellWidgetUniversal->SetEditMode(m_bEdit);
			}

		}

	//based on edit mode, enable/disable items on cnxt menu && keyboard shortcuts:
	int nSize=m_lstActions.size();
	for(int i=0;i<nSize;++i)
	{
		//means that action is separator or is always enable:
		if(m_lstActions.at(i)->isSeparator() || m_lstActions.at(i)->data().toBool()) continue;
		//enable/disable item:
		m_lstActions.at(i)->setEnabled(m_bEdit);
	}



	blockSignals(false);

}






/*!
	When cell is changed, data is written back to recordset
	Warning:
	- disable signals when chaning items in grid, because they cause EDITING


	\param nRow - row
	\param nCol - column

*/
void UniversalTableWidget::CellEdited(int nRow, int nCol)
{
	if (m_lstColumnMapping[nCol]==-1) return; //column can be invalid
	
	Q_ASSERT(item(nRow,nCol)!=NULL); //item must be created!!!


	if (!m_lstColumnSetup.getDataRef(nCol,"BOGW_EDITABLE").toBool()) return; //if not editable jump back


	//qDebug()<<"cell edited at x="<<nRow<<" y="<<nCol;
	
	QVariant value = item(nRow,nCol)->data(Qt::DisplayRole);
	bool bValidationOK;
	
	//if chkbox:
	if(m_lstColumnSetup.getDataRef(nCol,2).toInt()==COL_TYPE_CHECKBOX)
	{
		if(item(nRow,nCol)->checkState()==Qt::Checked)
			value=QVariant(true);
		else
			value=QVariant(false);
		
		//support for integers:
		if (m_lstData->getColumnType(m_lstColumnMapping[nCol])==QVariant::Int)
		{
			value=value.toInt();
		}

		//validate and store new data
		bValidationOK=m_pData->ChangeData(nRow,m_lstColumnMapping[nCol],value);
	
		//if validation fails: return back old value:
		if (!bValidationOK)
		{
			blockSignals(true);
			item(nRow,nCol)->setCheckState(m_lstData->getDataRef(nRow,m_lstColumnMapping[nCol]).toBool() ? Qt::Checked : Qt::Unchecked); 
			blockSignals(false);
		}
		else
			emit SignalDataChanged(); //new issue: emit signal when record set is changed!




	}
	//if widget edit signals must not be accepted, as widget itself must receive them
	else if (m_lstColumnSetup.getDataRef(nCol,2).toInt()>COL_TYPE_CHECKBOX)
	{
		return;
	}
	else //normal operation:
	{
		//reverse: DAte is stored as string, revert to date:
		int nDataType=m_lstData->getColumnType(m_lstColumnMapping[nCol]);
		if (nDataType==QVariant::Date)
		{
			value=value.toDate();
		}
		else if (nDataType==QVariant::DateTime)
		{
			value=value.toDateTime();
		}

		//validate and store new data
		bValidationOK=m_pData->ChangeData(nRow,m_lstColumnMapping[nCol],value);
	
		//if validation fails: return back old value:
		if (!bValidationOK)
		{
			blockSignals(true);
			item(nRow,nCol)->setData(Qt::DisplayRole,m_lstData->getDataRef(nRow,m_lstColumnMapping[nCol]));
			blockSignals(false);
		}
		else
			emit SignalDataChanged();//new issue: emit signal when record set is changed!
	}

	
}


//--------------------------------------------------------------
//								SELECTION HANDLERS
//--------------------------------------------------------------


/*!
	When selection is changed, record set rows are selected/deselected

*/
void UniversalTableWidget::SelectionChanged()
{

	m_bSelectionChangeEmitted=true;

	QList<QTableWidgetSelectionRange> lstSelections = selectedRanges();

	int nSize=lstSelections.size();

	//if CTRL+A,then select all:
	if(nSize==1)
		if (lstSelections.at(0).topRow()==0 && lstSelections.at(0).bottomRow()==m_lstData->getRowCount()-1)
		{
			m_lstData->selectAll();
			TableStateChanged();
			emit SignalSelectionChanged();
			return;
		}

	m_lstData->clearSelection();
	for(int i=0;i<nSize;++i)
	{
		QTableWidgetSelectionRange range=lstSelections.at(i);
		for(int j=range.topRow();j<=range.bottomRow();++j)
		{	
			m_lstData->selectRow(j);
			//qDebug()<<"Selection in progress on row ="<<j;
			//qDebug()<< m_lstData->isRowSelected(j);
		}
	}

	//qDebug()<<this->selectionBehavior();
	//notify that state
	TableStateChanged();
	QCoreApplication::processEvents(); //issue 1653: color row then fire signal...
	emit SignalSelectionChanged();

}





/*!
	When selected rows inside data source are changed call this function to refresh view.
	Faster then calling RefreshDisplay()

	\param bClearDataSelected - if true (default) selection from data source is also cleared
*/
void UniversalTableWidget::RefreshSelection()
{

	blockSignals(true);

	//clear old selection:
	int nRowSize=m_lstData->getRowCount();
	int nColSize=columnCount();
	int nSelectedCount=m_lstData->getSelectedCount();

	//clear old selection:
	QTableWidgetSelectionRange range(0,0,rowCount()-1,nColSize-1);  
	setRangeSelected(range,false);


	if (nSelectedCount==0)
	{
		blockSignals(false);
		return;
	}

	//all selected:
	if (nSelectedCount==nRowSize)
	{
		QTableWidgetSelectionRange range(0,0,rowCount()-1,nColSize-1);  
		setRangeSelected(range,true);
		blockSignals(false);
		return;
	}

	//one selected:
	if (nSelectedCount==1)
	{
		int nRow=m_lstData->getSelectedRow();
		QTableWidgetSelectionRange range(nRow,0,nRow,nColSize-1);  //TOFIX: can be optimized
		setRangeSelected(range,true);
		if (nColSize==1)
			setCurrentItem(item(nRow,0)); //if one columned table, set on item current
		blockSignals(false);
		return;
	}


	//refresh selection:
	//if not selection & row selected
	int nRowRange=0;
	for( int nRow=0;nRow<nRowSize;++nRow)
	{
		if (m_lstData->isRowSelected(nRow))
			nRowRange++;
		else
		{
			if (nRowRange>0)
			{
				QTableWidgetSelectionRange range(nRow-nRowRange,0,nRow-1,nColSize-1);  //TOFIX: can be optimized
				setRangeSelected(range,true);
				nRowRange=0;
			}
		}
	}

	//last:
	if (nRowRange>0)
	{
		QTableWidgetSelectionRange range(nRowSize-nRowRange,0,nRowSize-1,nColSize-1);  //TOFIX: can be optimized
		setRangeSelected(range,true);
	}
	
	blockSignals(false);

}



/*!
	Deletes selected rows from data source & table view


*/
void UniversalTableWidget::DeleteSelection()
{
	//if all then all:
	if(m_lstData->getSelectedCount()==rowCount()) 
		{DeleteTable();return;}
	
	m_pData->DeleteData(0,true);
	RefreshDisplay();
}



/*!
	Removes whole content from table & from datasource


*/
void UniversalTableWidget::DeleteTable()
{
	m_pData->DeleteData(-1);
	RestoreHeaderSetup();
}


//--------------------------------------------------------------
//								SORTING
//--------------------------------------------------------------


/*!
	Sort column: sorting order is temporarly stored in column setup
	Whole list is resorted to keep it in sync with table view
	
	\param nColPos - column in grid

*/
void UniversalTableWidget::SortColumn(int nColPos)
{
	if (m_lstColumnMapping[nColPos]==-1) return;
	int nColType=m_lstColumnSetup.getDataRef(nColPos,2).toInt();
	if(nColType>COL_TYPE_CHECKBOX) return; //no sorting on special widgets

	int nSortOrder = m_lstColumnSetup.getDataRef(nColPos,6).toInt(); // 0 - asc ,1 -desc
	//revert existing sort order (when first time, probably it go descending)
	nSortOrder = (nSortOrder==1 ? 0:1); 

	//store last:
	m_lstLastSort.clear();
	m_lstLastSort<<SortData(m_lstColumnMapping[nColPos],nSortOrder);

	horizontalHeader()->setSortIndicator(nColPos, (Qt::SortOrder)nSortOrder);

	//m_lstData->Dump();
	if (m_bGridContainsBoldItems) //if no bold items, do it, else
	{
		emit SignalRefreshBoldRows();
	}
	m_lstData->sort(m_lstColumnMapping[nColPos],nSortOrder);
	RefreshDisplay();

	//	sortItems (nColPos, (Qt::SortOrder)nSortOrder );
	//m_lstData->Dump();
	//instead of refresh display: just use table item sort:
	//sortItems (nColPos, (Qt::SortOrder)nSortOrder );
	 
	//m_lstData->Dump();

	m_lstColumnSetup.setData(nColPos,6,nSortOrder); //store back reverse
}


/*!
	Sort columns in grid/datasource by order given in list.
*/
void UniversalTableWidget::SortMultiColumn(DbRecordSet lstCols)
{
	//set sort indicator on first col in list:
	if(lstCols.getRowCount()==0)return;

	SortDataList lstSort;
	bool bSortIndicatorSet=false;

	int nSize=lstCols.getRowCount();
	for(int i=0;i<nSize;++i)
	{

		lstSort.append(SortData(m_lstData->getColumnIdx(lstCols.getDataRef(i,"BOGW_COLUMN_NAME").toString()),lstCols.getDataRef(i,"BOGW_SORT_ORDER").toInt())); // prepare sort list:
		int nRow=m_lstColumnSetup.find(1,lstCols.getDataRef(i,"BOGW_COLUMN_NAME").toString(),true);
		if(nRow!=-1)
		{
			m_lstColumnSetup.setData(nRow,6,lstCols.getDataRef(i,"BOGW_SORT_ORDER").toInt()); //store back asc/desc values for each col
			if(!bSortIndicatorSet)	//set sort indicator to first col in multisort
			{
				horizontalHeader()->setSortIndicator(nRow, (Qt::SortOrder)lstCols.getDataRef(i,"BOGW_SORT_ORDER").toInt());
				bSortIndicatorSet=true;
			}
		}
    }

/*
	nSize=lstSort.size();
	for(int i=0;i<nSize;++i)
	{
		qDebug()<<lstSort.at(i).m_nColumn<<"  "<<lstSort.at(i).m_nSortOrder;
	}
*/
	m_lstLastSort=lstSort;
	m_lstData->sortMulti(lstSort);

	RefreshDisplay(); 

}


//--------------------------------------------------------------
//						CONTEXT MENU
//--------------------------------------------------------------


/*!
	Slot for opening context menu:
*/
void UniversalTableWidget::contextMenuEvent(QContextMenuEvent *event)
{
	//event:
	if(m_lstActions.size()==0) 
	{
		event->ignore();	
		return;
	}
	
	QMenu CnxtMenu(this);
	
	int nSize=m_lstActions.size();
	for(int i=0;i<nSize;++i)
		CnxtMenu.addAction(m_lstActions.at(i));


	CnxtMenu.exec(event->globalPos());
}


/*!
	Creates default cnxt menu actions. You must manually call this method to fill action list then
	SetContextMenuActions() to set actions on grid.


	Defaults are:

	- Add row
	------------------
	- Delete selection
	- Duplicate selection
	- Delete whole content
	------------------
	- Select All
	- DeSelect All
	------------------
	- Sort

	\param nColPos - column in grid

*/
void UniversalTableWidget::CreateContextMenuActions(QList<QAction*>& lstActions)
{

	QCommonStyle style;
	//Insert new row:
	QAction* pAction = new QAction(tr("&Add New Entry"), this);
    pAction->setShortcut(tr("Ins"));
	pAction->setIcon(QIcon(":handwrite.png"));
	pAction->setData(QVariant(false));	//means that is enabled only in edit mode
    connect(pAction, SIGNAL(triggered()), this, SLOT(InsertRow()));

	lstActions.append(pAction);
	this->addAction(pAction);

	//Add separator
	QAction* SeparatorAct = new QAction(this);
	SeparatorAct->setSeparator(true);

	lstActions.append(SeparatorAct);

	
	pAction = new QAction(tr("&Delete Selection"), this);
    pAction->setShortcut(tr("Del"));
	pAction->setIcon(style.standardIcon(QStyle::SP_TrashIcon));
	pAction->setData(QVariant(false));	//means that is enabled only in edit mode
    connect(pAction, SIGNAL(triggered()), this, SLOT(DeleteSelection()));

	lstActions.append(pAction);
	this->addAction(pAction);

	pAction = new QAction(tr("Delete &All"), this);
    pAction->setShortcut(tr("CTRL+S"));
	//pAction->setIcon(QIcon(":fp.png"));
	pAction->setData(QVariant(false));	//means that is enabled only in edit mode
    connect(pAction, SIGNAL(triggered()), this, SLOT(DeleteTable()));

	lstActions.append(pAction);
	this->addAction(pAction);


	pAction = new QAction(tr("Duplicate &Selection"), this);
    pAction->setShortcut(tr("CTRL+D"));
	//pAction->setIcon(QIcon(":fp.png"));
	pAction->setData(QVariant(false));	//means that is enabled only in edit mode
    connect(pAction, SIGNAL(triggered()), this, SLOT(DuplicateSelection()));

	lstActions.append(pAction);
	this->addAction(pAction);




	//Only add this if multipleselection enabled:
	if(this->selectionMode()!=QAbstractItemView::SingleSelection)
	{

		//Add separator
		SeparatorAct = new QAction(this);
		SeparatorAct->setSeparator(true);

		lstActions.append(SeparatorAct);


		//Select All:
		pAction = new QAction(tr("&Select All"), this);
		pAction->setShortcut(tr("CTRL+A"));
		//pAction->setIcon(QIcon(":fp.png"));
		pAction->setData(QVariant(true));	//means that is always enabled
		connect(pAction, SIGNAL(triggered()), this, SLOT(SelectAll()));

		lstActions.append(pAction);


		//DeSelect All:
		pAction = new QAction(tr("D&eSelect All"), this);
		pAction->setShortcut(tr("CTRL+E"));
		//pAction->setIcon(QIcon(":fp.png"));
		pAction->setData(QVariant(true));	//means that is always enabled
		connect(pAction, SIGNAL(triggered()), this, SLOT(DeselectAll()));
		
		lstActions.append(pAction);
		this->addAction(pAction);

	}


	//Sorting:

	//Add separator
	SeparatorAct = new QAction(this);
	SeparatorAct->setSeparator(true);

	lstActions.append(SeparatorAct);


	//Select All:
	pAction = new QAction(tr("S&ort"), this);
	pAction->setShortcut(tr("CTRL+S"));
	pAction->setIcon(QIcon(":rollingdice.png"));
	pAction->setData(QVariant(true));	//means that is always enabled
	connect(pAction, SIGNAL(triggered()), this, SLOT(SortDialog()));

	lstActions.append(pAction);
}


//--------------------------------------------------------------
//						PUBLIC SLOTS FOR TABLE ACTIONS
//--------------------------------------------------------------
/*!
	Inserts row in data source & table at last position
*/
void UniversalTableWidget::InsertRow()
{
	m_pData->InsertData(); //add new row

	//set new row count
	int nRowSize=m_lstData->getRowCount();
	setRowCount(nRowSize);

	//set selection to last row:
	m_lstData->clearSelection();
	m_lstData->selectRow(nRowSize-1);


	RefreshDisplay();//Row(nRowSize-1);

	//if editable go insert mode
	if(m_bEdit)
	{
		//get first cell on new row
		QWidget *pWidget=cellWidget(nRowSize-1,0);
		if (pWidget)
		{
			pWidget->setFocus(Qt::MouseFocusReason);
		}
		else
		{
			QTableWidgetItem * firstCell=item(nRowSize-1,0) ; 
			setCurrentItem(firstCell);
			editItem(firstCell);
		}
	}
	
}


/*!
	Select all
*/
void UniversalTableWidget::SelectAll()
{
	m_lstData->selectAll();
	//set all selected:
	int nColSize=columnCount();

	//disconnect(this,SIGNAL(itemSelectionChanged()),this,SLOT(SelectionChanged()));
	QTableWidgetSelectionRange range(0,0,rowCount()-1,nColSize-1);  
	setRangeSelected(range,true);
	//connect(this,SIGNAL(itemSelectionChanged()),this,SLOT(SelectionChanged()));
}


/*!
	DeSelect all
*/
void UniversalTableWidget::DeselectAll()
{
	m_lstData->clearSelection();
	//set all selected:
	int nColSize=columnCount();

	//disconnect(this,SIGNAL(itemSelectionChanged()),this,SLOT(SelectionChanged()));
	QTableWidgetSelectionRange range(0,0,rowCount()-1,nColSize-1);  
	setRangeSelected(range,false);
	//connect(this,SIGNAL(itemSelectionChanged()),this,SLOT(SelectionChanged()));
}


/*!
	DuplicateSelection: all selected rows are duplicated:
	- new row is inserted just below selection
	- new rows are selected 

	This can be slow....
*/
void UniversalTableWidget::DuplicateSelection()
{
	//for each selected row
	int i=0,nSize=m_lstData->getRowCount();
	while(i<nSize)
	{
		//if selected
		if(m_lstData->isRowSelected(i))
		{
			m_pData->InsertData(i+1,i);	//duplicate: copy&insert row from i to i+1
			nSize=nSize+1;
			m_lstData->selectRow(i,false);
			m_lstData->selectRow(i+1,true);
			++i;//advance to next
		}
		++i;
	}

	RefreshDisplay();
}



/*!
	Opens sorting dialog for multicolumn sorting
*/
void UniversalTableWidget::SortDialog()
{

	MultiSortColumn dlgSorter;

	//init
	dlgSorter.SetColumnSetup(m_lstColumnSetup);

	//show:
	if(dlgSorter.exec()>0)
		SortMultiColumn(dlgSorter.GetResultRecordSet()); //if press OK, sort it:

}




//--------------------------------------------------------------
//								DRAG & DROP
//--------------------------------------------------------------



/*!
	Returns selected values in list, as drag value
	
	\param   values - selected rows
	\param   bSelectItemChildren - just for dragdropinterface compatibility with tree widget.

*/
void UniversalTableWidget::GetDropValue(DbRecordSet &values, bool bSelectItemChildren) const
{ 
	//qDebug()<<m_lstData->getSelectedCount();
	//m_lstData->Dump();
	values.copyDefinition(*m_lstData);
	values.merge(*m_lstData,true);
}



/*!
	Called from dropEvent if widget sucessfully acceppted drop, with x,y coordinates and drop data.
	By default it insert new data at end of table view (APPEND)
	Override this to implement custom behaviour

	\param   index			- position in model (can be invalid if drop ocured outside all items). Test isValid()
	\param   nDropType		- Drop Type
	\param   DroppedValue	- Dropped data
	\param   event			- Drop event
	
*/
void UniversalTableWidget::DropHandler(QModelIndex index, int nDropType, DbRecordSet &DroppedValue, QDropEvent *event)
{
	//If we go with drop handler.
	if (!m_bIgnoreDropHandler)
	{
		//determine coordiantes:
		int nRow,nCol;
		if(index.isValid()) //if valid:
		{
			nRow=index.row();
			nCol=index.column();
		}
		else //invalid
		{
			nRow=-1;
			nCol=-1;
		}

		//qDebug()<<"Drop occured at row:"<<nRow<<" col: "<<nCol;
		m_lstData->merge(DroppedValue);
		RefreshDisplay();
	}
	//Else just emit signal.
	else
		emit DataDroped(index, nDropType, DroppedValue, event);
}


//if not edit mode: disable drop & drag
void UniversalTableWidget::dropEvent(QDropEvent *event)
{
	if(!m_bEdit){return;}
	DragDropInterface::dropEvent(event);
}
//if not edit mode: disable drop & drag
void UniversalTableWidget::dragEnterEvent ( QDragEnterEvent * event )
{
	if(!m_bEdit)
		{event->ignore();return;}
	DragDropInterface::dragEnterEvent(event);
}

//if not edit mode: disable drop & drag
void UniversalTableWidget::dragMoveEvent ( QDragMoveEvent * event )
{
	if(!m_bEdit)
		event->ignore();
	else
		event->accept();
}




//--------------------------------------------------------------
//								HELPERS
//--------------------------------------------------------------

/*!
	Destroy any widgets inside table, recreates horizontal header: sets header info + tooltip + later icon can be added
	
	\param lstCols		- list with column No in grid and sort order for each column in grid

*/
void UniversalTableWidget::RestoreHeaderSetup()
{
	blockSignals(true);
	//clear setup:
	clear();
	setRowCount(0);

	int nSize=m_lstColumnSetup.getRowCount();
	setColumnCount(nSize);	//sets column count

	//issue 1684:
	QFont strStyle1("Arial",8,QFont::Bold,true);
	strStyle1.setStyleStrategy(QFont::PreferAntialias);

	QTableWidgetItem *newItem;

	//add columns:
	for( int i=0;i<nSize;++i)
	{
		setColumnWidth(i,m_lstColumnSetup.getDataRef(i,3).toInt()); //resize cols to given values
		newItem=horizontalHeaderItem(i);
		if(newItem==NULL) //if not created, create it:
		{
				newItem = new QTableWidgetItem();
				newItem->setFont(strStyle1);
				setHorizontalHeaderItem(i,newItem);
		}
		
		//set header + tooltip
		newItem->setText(m_lstColumnSetup.getDataRef(i,0).toString());
		newItem->setToolTip(m_lstColumnSetup.getDataRef(i,7).toString());

		if (m_lstColumnSetup.getDataRef(i,10).toInt())
			horizontalHeader()->setSectionResizeMode(i,(enum QHeaderView::ResizeMode)m_lstColumnSetup.getDataRef(i,10).toInt());

	}

	//resize table to header data....brb should be user-defined..
	//resizeColumnsToContents();


	verticalHeader()->setHighlightSections(false);
	horizontalHeader()->setHighlightSections(false);

	blockSignals(false);
}



//--------------------------------------------------------------
//								STATIC HELPERS
//--------------------------------------------------------------

/*!
	Creates column setup based on given view, skipping LM an GLOBAL ID fields if found
	All columns are editable by default.
	Warning:
	- all columns are set to TEXT type, this will not work for ByteArrays, bools, and other special data types!
	
	\param lstSetup		- returned column setup, redefined and filled
	\param nView		- id of view from which to define table column setup
	\param nSkipCols	- how many cols to skip from begining (def = 0)

*/
void UniversalTableWidget::DefineColumnSetupFromView(DbRecordSet &lstSetup,int nView,int nSkipCols)
{
	//define setup:
	lstSetup.defineFromView(DbSqlTableView::getView(DbSqlTableView::MVIEW_GRID_COLUMN_SETUP));

	//get view:
	DbView view=DbSqlTableView::getView(nView);

	QString strToolTip,strHeader;

	for(int i=nSkipCols;i<view.size();++i)
	{

		//col name:
		QString strColName=view.at(i).m_strName;

		//skip mandatory
		if(strColName.indexOf("GLOBAL_ID")!=-1) continue;
		if(strColName.indexOf("LAST_MODIFIED")!=-1) continue;
		
		strHeader=strColName; //header same as col name
		strToolTip="";

		AddColumnToSetup(lstSetup,strColName,strHeader,70,COL_TYPE_TEXT,"",true,strToolTip);
	}



}

/*!
	Add's column setup info to given list, if not defined, list will be defined,
	Position is determined by order of add.
	Column name  must match those in data source
	
	\param lstSetup		- returned column setup, redefined and filled 
	\param nView		- id of view from which to define table column setup
	\param nSectionResizeMode		enum QHeaderView::ResizeMode

*/
void UniversalTableWidget::AddColumnToSetup(DbRecordSet &lstSetup,QString strColName,QString strHeaderName,int nWidth,bool bEditable,QString strToolTip,int nColType,QString strDataFormat,int nSectionResizeMode)
{

	//define from view:
	if(lstSetup.getColumnCount()==0)lstSetup.defineFromView(DbSqlTableView::getView(DbSqlTableView::MVIEW_GRID_COLUMN_SETUP));


	//postion is defined by order of adding:
	int nPosition=lstSetup.getRowCount();


	//add column setup:
	lstSetup.addRow();
	lstSetup.setData(nPosition,0,strHeaderName);
	lstSetup.setData(nPosition,1,strColName);
	lstSetup.setData(nPosition,2,nColType);
	lstSetup.setData(nPosition,3,nWidth);
	lstSetup.setData(nPosition,4,nPosition);
	lstSetup.setData(nPosition,5,bEditable);
	lstSetup.setData(nPosition,6,0);			//sort order: default=asc
	lstSetup.setData(nPosition,7,strToolTip);
	lstSetup.setData(nPosition,8,strDataFormat);
	lstSetup.setData(nPosition,9,100);
	lstSetup.setData(nPosition,10,nSectionResizeMode); //resize mode

}




//--------------------------------------------------------------
//					WIDGET OPS
//--------------------------------------------------------------

/*!
	When widget changes (in edit mode only), this slot is invoked with widget coordinates
	
	\param nRow		- cell widget row
	\param nCol		- cell widget col

*/
void UniversalTableWidget::CellWidgetChanged(int nRow,int nCol)
{
	if (m_lstColumnMapping[nCol]==-1)return;

	//get cell widget
	TableCellWidget *cellWidgetUniversal=dynamic_cast<TableCellWidget*>(cellWidget(nRow,nCol));
	Q_ASSERT_X(cellWidgetUniversal!=NULL,"TABLE WIDGET","CELL WIDGET NOT SUBCLASSED FROM TableCellWidget");
	//get data
	QVariant data;
	cellWidgetUniversal->GetData(data);
	//set data 
	//if data change fails, return back from data source:
	if(!m_pData->ChangeData(nRow,m_lstColumnMapping[nCol],data)) 
		cellWidgetUniversal->RefreshData(m_lstData->getDataRef(nRow,m_lstColumnMapping[nCol]));
	else
		emit SignalDataChanged();//new issue: emit signal when record set is changed!

	
	//qDebug()<<"Cell Widget changed data at: "<<nRow <<"---"<<nCol;
}



/*!
	When new row is added this method is called to create new widgets.
	Reimplement this method to create own widgets.
	
	\param nRow		- cell widget row
	\param nCol		- cell widget col
	\param nColType	- cell widget type (user defined types are COL_TYPE_USER_DEFINED ++)
	\return			- QWidget pointer to newly created widget

*/
QWidget *UniversalTableWidget::CreateCellWidgets(int nRow,int nCol,int nColType)
{
	switch(nColType)
		{
			case COL_TYPE_COMBOBOX:				//note: widget is automatically destroyed by table when row size is decremented
					return new UniversalTableComboBox(this);
			case COL_TYPE_LIST:					//note: widget is automatically destroyed by table when row size is decremented
					return new UniversalTableList(this);
			case COL_TYPE_MULTILINE_TEXT:		//note: widget is automatically destroyed by table when row size is decremented
					return new UniversalTableMultiLineText(this);
			case COL_TYPE_PICTURE:				//note: widget is automatically destroyed by table when row size is decremented
					return new UniversalTablePicture(this);
			default:
				Q_ASSERT(false);//not implemeneted
	}

	return NULL;

}





/*!
	When cell clicked and selection change is not triggered, use this:

	\param nRow		- cell widget row
	\param nCol		- cell widget col
*/
void UniversalTableWidget::CellClicked(int nRow, int nCol)
{
	//trigger selection only when row inside list that is already selected:
	if(m_bSelectionChangeEmitted)
		m_bSelectionChangeEmitted=false;
	else if(m_lstData->getRowCount()==1) //only when 1 row:
		emit SignalSelectionChanged();

	if(m_bEdit)
	{
		QTableWidgetItem * firstCell=item(nRow,nCol); 
		if(firstCell->flags() & Qt::ItemIsEditable)
			editItem(firstCell);
	}
}


//warning: when selecting last row: refresh display is trigered
void UniversalTableWidget::scrollToLastItem(bool bSelectLastRow)
{
	if (m_lstData->getRowCount()==0) return;
	int nLastRow=m_lstData->getRowCount()-1;
	if (bSelectLastRow)
	{
		m_lstData->clearSelection();
		m_lstData->selectRow(nLastRow);
		RefreshDisplay();
	}

	if(columnCount()>0)
		setCurrentItem(item(nLastRow,0));
}

//warning: when selecting last row: refresh display is trigered
void UniversalTableWidget::scrollToFirstItem(bool bSelectFirstRow)
{
	if (m_lstData->getRowCount()==0) return;
	if (bSelectFirstRow)
	{
		m_lstData->clearSelection();
		m_lstData->selectRow(0);
		RefreshDisplay();
	}

	if(columnCount()>0)
	{
		setCurrentItem(item(0,0));
		scrollToItem(item(0,0));
	}
}



//set row font to bold:
void UniversalTableWidget::SetRowBold(int nRow)
{
	//blockSignals(true);
	int nCol=columnCount();
	for(int j=0; j<nCol; j++)
	{
		QFont font = item(nRow, j)->font();
		font.setBold(true);
		item(nRow, j)->setFont(font);
	}
	//blockSignals(false);

	m_bGridContainsBoldItems=true;
}




void UniversalTableWidget::ClearAllBoldFonts()
{
	setUpdatesEnabled(false);
	blockSignals(true);

	int nSize=rowCount();
	for(int i=0;i<nSize;++i)
	{
		int nSize2=columnCount();
		for(int j=0;j<nSize2;++j)
		{
			QFont font = item(i, j)->font();
			font.setBold(false);
			item(i, j)->setFont(font);
		}
	}

	blockSignals(false);
	setUpdatesEnabled(true);

	m_bGridContainsBoldItems=true;
}



//when drag: send selected items + type
QMimeData * UniversalTableWidget::mimeData( const QList<QTableWidgetItem *> items ) const
{
	QMimeData *mimeData=QTableWidget::mimeData(items);
	if (mimeData)
	{
		DbRecordSet lstSelectedItems=m_lstData->getSelectedRecordSet();
		if (lstSelectedItems.getRowCount()>0 && GetDropType()!=-1)
		{
			QByteArray byteListData=XmlUtil::ConvertRecordSet2ByteArray_Fast(lstSelectedItems);
			mimeData->setData(SOKRATES_MIME_LIST,byteListData);
			mimeData->setData(SOKRATES_MIME_DROP_TYPE,QVariant(GetDropType()).toString().toLatin1()); //pass unique type
		}
	}

	return mimeData;
}

//add custom mime types: selected list items + drop type
QStringList UniversalTableWidget::mimeTypes() const
{
	QStringList lstDefault=QTableWidget::mimeTypes();
	lstDefault<<SOKRATES_MIME_LIST;
	lstDefault<<SOKRATES_MIME_DROP_TYPE;

	return lstDefault;
}



/*!
Starts a drag by calling drag->start() using the given \a supportedActions.
*/
void UniversalTableWidget::startDrag(Qt::DropActions supportedActions)
{
	//QTableWidget::startDrag(supportedActions);
	//return;

	//Q_D(QAbstractItemView);
	QModelIndexList indexes = selectedIndexes();
	if (indexes.count() > 0) 
	{
		QMimeData *data = model()->mimeData(indexes);
		if (!data)
			return;
		QRect rect;
		QPixmap pixmap(m_strDragIcon); //= d->renderToPixmap(indexes, &rect);
		QDrag *drag = new QDrag(this);
		drag->setPixmap(pixmap);
		drag->setMimeData(data);
		drag->setHotSpot(QPoint(-10,0)); //drag->pixmap().width()/2,0drag->pixmap().height()));
		
		if (drag->start(supportedActions) == Qt::MoveAction)
		{
			//------------------B.T. copied form void QAbstractItemViewPrivate::clearOrRemove()
			// we can't remove the rows so reset the items (i.e. the view is like a table)
			QModelIndexList list = selectedIndexes();
			for (int i=0; i < list.size(); ++i) 
			{
				QModelIndex index = list.at(i);
				QMap<int, QVariant> roles = model()->itemData(index);
				for (QMap<int, QVariant>::Iterator it = roles.begin(); it != roles.end(); ++it)
					it.value() = QVariant();
				model()->setItemData(index, roles);
			}
			//------------------B.T. copied form void QAbstractItemViewPrivate::clearOrRemove()
		}
			
	}
}

//return current item among selected ones, if not found, returns first selected, or if none selected, returns -1
int UniversalTableWidget::GetCurrentSelectedRow()
{
	int nCurr=row(currentItem());
		if (m_lstData->isRowSelected(nCurr))
			return nCurr;
		else
			return m_lstData->getSelectedRow();
}
//custom widgets can not pass click event-> no selection, use this to emulate selection
void UniversalTableWidget::CustomTableCellClicked(int nRow, int nCol)
{
	//BT brute force method on recordset: if no CTRL nor SHIFT deselect all
	//if CTRL toggle selection
	//if SHIT, just select

	if (m_lstData)
	{
		bool bCTRL=false,bShift=false;
		Qt::KeyboardModifiers keys= QApplication::keyboardModifiers();

		if(Qt::ControlModifier == (Qt::ControlModifier & keys))
			bCTRL = true;

		//remember if the Alt key was pressed
		if(Qt::ShiftModifier == (Qt::AltModifier & keys))
			bShift = true;

		if(bCTRL)
			m_lstData->selectRow(nRow,!m_lstData->isRowSelected(nRow));

		if (bShift)
			m_lstData->selectRow(nRow);

		if (!bShift && !bCTRL)
		{
			m_lstData->clearSelection();
			m_lstData->selectRow(nRow);
		}


		RefreshSelection(); //from recordset -> grid
		TableStateChanged();
		emit SignalSelectionChanged();

	}
}


void UniversalTableWidget::SetStyleSheetGlobal()
{
	this->setStyleSheet(ThemeManager::GetTableViewBkg());
}