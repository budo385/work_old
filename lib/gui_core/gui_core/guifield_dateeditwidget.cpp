#include "guifield_dateeditwidget.h"




GuiField_DateEditWidget::GuiField_DateEditWidget(QString strFieldName,QWidget *pWidget,GuiDataManipulator* pDataManager,DbView *TableView, QLabel *pLabel)
:GuiField(strFieldName,pWidget,pDataManager,TableView,pLabel)
{
	//test column type in datasource:
	Q_ASSERT(pDataManager->GetDataSource()->getColumnType(pDataManager->GetDataSource()->getColumnIdx(strFieldName))==QVariant::Date);


	//register signal textChanged() -> ChangeData() if lineEdit:
	m_pEditWidget=dynamic_cast<DateEditWidget*>(pWidget);
	Q_ASSERT(m_pEditWidget!=NULL);

	connect(m_pEditWidget,SIGNAL(EmitDateChanged(QDate)),this,SLOT(FieldChanged(QDate)));

}

/*!
	This slot is invoked when date changes
	NOTE: assert is fired if row is out of bound
*/
void GuiField_DateEditWidget::FieldChanged(QDate date)
{
	QVariant value=date;
	//if(ValidateUserInput(value))
		m_pDataManager->ChangeData(m_nCurrentRow,m_nIndex,value);

}



/*!
	If data is changed from data manipulator, refresh content
	If data source is empty, content will be cleared.
*/
void GuiField_DateEditWidget::RefreshDisplay()
{
	
	m_pEditWidget->blockSignals(true);

	//check if current row is out of bound, if so clear content;
	if(m_nCurrentRow<m_pDataManager->GetDataSource()->getRowCount()) 
	{
		//m_pDataManager->GetDataSource()->Dump();
		QVariant value=m_pDataManager->GetDataSource()->getDataRef(m_nCurrentRow,m_nIndex);
		//m_pEditWidget->m_lineEdit->setText(value.toDate().toString("dd.MM.yyyy"));
		m_pEditWidget->m_lineEdit->setText(value.toDate().toString(Qt::LocalDate));
	}
	else
		m_pEditWidget->m_lineEdit->clear();		

	m_pEditWidget->blockSignals(false);
}

