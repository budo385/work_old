#ifndef NETWORKCONNECTIONSDLG_H
#define NETWORKCONNECTIONSDLG_H

#include <QtWidgets/QDialog>
#include "ui_networkconnectionsdlg.h"
#include "trans/trans/httpclientconnectionsettings.h"

class NetworkConnectionsDlg : public QDialog
{
    Q_OBJECT

public:
    NetworkConnectionsDlg(QWidget *parent = 0);
    ~NetworkConnectionsDlg();

	void SetConnections(QList<HTTPClientConnectionSettings> &lstData, int nCurrentIdx = -1);
	QList<HTTPClientConnectionSettings> &GetConnections(){ return m_lstData; };
	int GetCurSelection() const { return m_nCurSel; }

	void SetPassword(bool bUsePassword, QString strPass);
	void GetPassword(bool &bUsePassword, QString &strPass);

	int exec();

protected:
	void ClearInfo();
	void UpdateInfo(bool bToScreen = true);
	void EnableProxyFields(bool bEnable = true);
	void EnableInfoFields(bool bEnable = true);

	QString GetHashString(QString strPassword);

protected:
	QList<HTTPClientConnectionSettings> m_lstData;
	int m_nCurSel;

	bool m_bUsePassword;
	QString m_strHash;

private:
    Ui::NetworkConnectionsDlgClass ui;

private slots:
	void on_chkUseDFO_stateChanged(int);
	void on_chkUseUserPassword_stateChanged(int);
	void on_btnChangePassword_clicked();
	void on_chkUseProxy_stateChanged(int);
	void on_lstConnections_currentRowChanged(int);
	void on_btnSave_clicked();
	void on_btnDelete_clicked();
	void on_btnNew_clicked();
	void on_btnCancel_clicked();
	void OnNameChanged();
};

#endif // NETWORKCONNECTIONSDLG_H


