#ifndef UNIVERSALTABLEFINDWIDGET_H
#define UNIVERSALTABLEFINDWIDGET_H

#include <QWidget>
#include <QComboBox>
#include <QLabel>

#include "tablefindcombobox.h"
#include "universaltablewidget.h"

class UniversalTableFindWidget : public QWidget
{
    Q_OBJECT

public:
    UniversalTableFindWidget(QWidget *parent = 0);
    ~UniversalTableFindWidget();

	void Initialize(QTableWidget *pTableWidget, bool bSideBarMode=false,QString strStyleSheet="", int nSearchedColumn=-1/*, DbRecordSet *pLstData=NULL, int nItemTextColIdx=-1, int nOrganizationColIdx=-1*/);
	TableFindComboBox	*m_pFindCombo;	 //BT: public  coz I must set focus 
private:
	
	QTableWidget		*m_pTableWidget;
	QLabel				*m_pFindLabel;
};

#endif // UNIVERSALTABLEFINDWIDGET_H
