#ifndef SCROLLABLEMSGBOX_H
#define SCROLLABLEMSGBOX_H

#include <QtWidgets/QDialog>
#include "ui_scrollablemsgbox.h"

class ScrollableMsgBox : public QDialog
{
	Q_OBJECT

public:
	ScrollableMsgBox(QWidget *parent = 0);
	~ScrollableMsgBox();

	void HideButton(int nBtnIdx);
	void SetButtonInfo(int nBtnIdx, QString strTxt, int nResult);
	void SetMessage(QString strMsg);

private slots:
	void on_btnFirst_clicked();
	void on_btnSecond_clicked();

private:
	Ui::ScrollableMsgBoxClass ui;
	int m_nFirstButtonReply;
	int m_nSecondButtonReply;
};

#endif // SCROLLABLEMSGBOX_H
