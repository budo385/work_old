#ifndef TESTCONNRESULT_H
#define TESTCONNRESULT_H

#include <QtWidgets/QDialog>
#include "./generatedfiles/ui_testconnresult.h"

class TestConnResult : public QDialog
{
    Q_OBJECT

public:
    TestConnResult(QWidget *parent = 0);
    ~TestConnResult();
    Ui::TestConnResultClass ui;
};

#endif // TESTCONNRESULT_H

