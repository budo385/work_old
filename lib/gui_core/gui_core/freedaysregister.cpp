#include "freedaysregister.h"
#include <QDebug>

FreeDaysRegister::FreeDaysRegister()
{
}

FreeDaysRegister::~FreeDaysRegister()
{
}

bool FreeDaysRegister::CalcEasterDate(int nYear, QDate &date)
{
	if(nYear < 1)
		return false;

	int A = nYear % 19;
	int B = nYear % 4;
	int C = nYear % 7;
	int D = (19*A+24) % 30;
	int E = (2*B+4*C+6*D+5) % 7;

	int nDay	= 22+D+E;
	int nMonth	= 3;

	if(nDay > 31){
		nDay = D+E-9;
		nMonth = 4;
	}

	if ((26 == nDay) && (4 == nMonth))
		nDay = 19;
	if ((25 == nDay) && (4 == nMonth) && (28 == D) && (6 == E) && (A > 10))
		nDay = 18;

	date.setDate(nYear, nMonth, nDay);
	return true;
}

void FreeDaysRegister::DefineFreeDaysList(DbRecordSet &lstDays)
{
	lstDays.destroy();
	lstDays.addColumn(QVariant::Date,	"DATE");
	lstDays.addColumn(QVariant::String, "NAME");
}

bool FreeDaysRegister::GetFreeDaysForYear(int nYear, DbRecordSet &lstDays, QString strCountryISO)
{
	DefineFreeDaysList(lstDays);

	if("ch" == strCountryISO){
		return GetFreeDaysForYear_ch(nYear, lstDays);
	}

	return false;
}

bool FreeDaysRegister::GetFreeDaysForYear_ch(int nYear, DbRecordSet &lstDays)
{
	QDate dateEaster;
	CalcEasterDate(nYear, dateEaster);
	
	// Define free days
	lstDays.addRow();	
	int nRow = lstDays.getRowCount()-1; Q_ASSERT(nRow >= 0);
	lstDays.setData(nRow, "DATE", QDate(nYear, 1, 1));
	lstDays.setData(nRow, "NAME", tr("New Year"));	//Neujahr

	lstDays.addRow();	
	nRow = lstDays.getRowCount()-1; Q_ASSERT(nRow >= 0);
	lstDays.setData(nRow, "DATE", QDate(nYear, 5, 1));
	lstDays.setData(nRow, "NAME", tr("May 1st"));	//1. Mai

	lstDays.addRow();	
	nRow = lstDays.getRowCount()-1; Q_ASSERT(nRow >= 0);
	lstDays.setData(nRow, "DATE", QDate(nYear, 12, 25));
	lstDays.setData(nRow, "NAME", tr("Christmas"));	//Weihnachten

	lstDays.addRow();	
	nRow = lstDays.getRowCount()-1; Q_ASSERT(nRow >= 0);
	lstDays.setData(nRow, "DATE", QDate(nYear, 12, 26));
	lstDays.setData(nRow, "NAME", tr("St. Stephen's Day"));	//Stephanstag

	lstDays.addRow();	
	nRow = lstDays.getRowCount()-1; Q_ASSERT(nRow >= 0);
	lstDays.setData(nRow, "DATE", QDate(nYear, 8, 1));
	lstDays.setData(nRow, "NAME", tr("August 1st"));	//1. August Nationalfeiertag CH

	lstDays.addRow();	
	nRow = lstDays.getRowCount()-1; Q_ASSERT(nRow >= 0);
	lstDays.setData(nRow, "DATE", dateEaster.addDays(-48));
	lstDays.setData(nRow, "NAME", tr("Carnival Monday"));	//Rosenmontag

	lstDays.addRow();	
	nRow = lstDays.getRowCount()-1; Q_ASSERT(nRow >= 0);
	lstDays.setData(nRow, "DATE", dateEaster.addDays(-46));
	lstDays.setData(nRow, "NAME", tr("Ash Wednesday"));	//Aschermittwoch

	lstDays.addRow();	
	nRow = lstDays.getRowCount()-1; Q_ASSERT(nRow >= 0);
	lstDays.setData(nRow, "DATE", dateEaster.addDays(-2));
	lstDays.setData(nRow, "NAME", tr("Good Friday"));	//Karfreitag

	lstDays.addRow();	
	nRow = lstDays.getRowCount()-1; Q_ASSERT(nRow >= 0);
	lstDays.setData(nRow, "DATE", dateEaster);
	lstDays.setData(nRow, "NAME", tr("Easter"));	//Ostern

	lstDays.addRow();	
	nRow = lstDays.getRowCount()-1; Q_ASSERT(nRow >= 0);
	lstDays.setData(nRow, "DATE", dateEaster.addDays(1));
	lstDays.setData(nRow, "NAME", tr("Easter Monday"));	//Ostermontag

	lstDays.addRow();	
	nRow = lstDays.getRowCount()-1; Q_ASSERT(nRow >= 0);
	lstDays.setData(nRow, "DATE", dateEaster.addDays(39));
	lstDays.setData(nRow, "NAME", tr("Ascension"));	// Himmelfahrt

	lstDays.addRow();	
	nRow = lstDays.getRowCount()-1; Q_ASSERT(nRow >= 0);
	lstDays.setData(nRow, "DATE", dateEaster.addDays(49));
	lstDays.setData(nRow, "NAME", tr("Pentecost"));	//Pfingsten

	lstDays.addRow();	
	nRow = lstDays.getRowCount()-1; Q_ASSERT(nRow >= 0);
	lstDays.setData(nRow, "DATE", dateEaster.addDays(50));
	lstDays.setData(nRow, "NAME", tr("Pentecost Monday"));	//Pfingstmontag

	lstDays.addRow();	
	nRow = lstDays.getRowCount()-1; Q_ASSERT(nRow >= 0);
	lstDays.setData(nRow, "DATE", dateEaster.addDays(61));
	lstDays.setData(nRow, "NAME", tr("Corpus Christi"));	//Fronleichnam

	lstDays.sort(0, 1);	// sort by date (ascending)
	return true;
}

bool FreeDaysRegister::GetFreeDays(QDate &from, QDate &to, DbRecordSet &lstDays, QString strCountryISO)
{
	DefineFreeDaysList(lstDays);

	DbRecordSet lstTmp;
	for(int i = from.year(); i <= to.year(); i++)
	{
		if(!GetFreeDaysForYear(i, lstTmp, strCountryISO))
			return false;
		lstDays.merge(lstTmp);
	}

	//qDebug() << "GetFreeDays from:" << from << ", to:" << to;

	//sort results
	lstDays.sort(0, 1);	// sort by date (ascending)

	//remove entries falling out of the requested day/month range
	int nCnt = lstDays.getRowCount();
	for(int i=0; i<nCnt; i++){
		QDate date = lstDays.getDataRef(i, "DATE").toDate();
		if(date < from || date > to){
			//qDebug() << "GetFreeDays date not in range:" << date;
			lstDays.deleteRow(i);
			i--; nCnt--;
		}
		//else qDebug() << "GetFreeDays date is in range:" << date;
	}

	return true;
}
