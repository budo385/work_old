#ifndef DRAGDROPINTERFACE_H
#define DRAGDROPINTERFACE_H

#include <QList>
#include <QModelIndex>
#include <QAbstractItemView>
#include <QDragEnterEvent>
#include <QDropEvent>
#include "common/common/dbrecordset.h"


/*!
	\class  DragDropInterface
	\ingroup GUICore_DragDropInterface
	\brief  This class provides drag drop interface 


	Use:
	- inherit this class and implement GetDropType() (or use SetDropType() to set Drop type) and GetDropValue() methods
	- every drag source has unique ID (entity ID) stored in bus_core/entity_id_collection.h
	- every drop target can accept 0,1 or more drop types: 
	  add acceptable drop types with AddAcceptableDropTypes(), check it with CheckIfDropIsAcceptable()
	- when inherited from widget based on QAbstractItemView, reimplemnt these methods from widget and redirect call
	 here:
	 supportedDropActions();
	 dropEvent();
	 dragEnterEvent();
	 Also in constructor initialize DragDropInterface with widget pointer

*/


class DragDropInterface
{
public:

	DragDropInterface(QWidget *pWidget=NULL):m_pWidget(pWidget),m_nUniqueDropType(-1){};

	//-------------------------------------
	//			DRAG
	//-------------------------------------
	//to support drag, you must reimplement:
	virtual void SetDropType(int nUniqueDropType){m_nUniqueDropType=nUniqueDropType;};				//set unique ID
	virtual int GetDropType() const {return m_nUniqueDropType;};									//return unique ID if widget supports drag, defined in entity_id_collection.h (by default return invalid drop type)
	virtual void GetDropValue(DbRecordSet &values, bool bSelectItemChildren = false) const =0;		//return values in format known to drop target, called by target when drop occurs

	void SetDragIcon(QString strIcon){m_strDragIcon=strIcon;};

	//-------------------------------------
	//			DROP
	//-------------------------------------

	//to support drop, you must reimplement:
	virtual void DropHandler(QModelIndex index, int nDropType, DbRecordSet &DroppedValue,QDropEvent *event){}; //our custom drop handlers
	virtual bool GetDropPosition(QModelIndex &index, QDropEvent *event); //where did drop occured: default implementation for all widgets based on the QAbstractItemView

	// store acceptable types:
	void AddAcceptableDropTypes(int nDropType){m_lstDropTypes.append(nDropType);}

	//Drop operations for all widgets: just redirect call here
	Qt::DropActions supportedDropActions() const ;
	void dropEvent(QDropEvent *event);
	void dragEnterEvent (QDragEnterEvent * event );


protected:
	QString m_strDragIcon;

private:
	bool CheckIfDropIsAcceptable(int nDropType);

	QList<int> m_lstDropTypes;
	QWidget *m_pWidget;
	

	int m_nUniqueDropType;			//this ID is unique for widget, identifies its data in drag/drop operations

};

#endif //DRAGDROPINTERFACE_H




