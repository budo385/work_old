#include "guifield_combobox.h"

GuiField_ComboBox::GuiField_ComboBox(QString strFieldName,QWidget *pWidget,DbRecordSet &lstComboValues,QString strComboListMapping_Col,QString strComboListDisplay_Col,bool bIsEditable,DbRecordSet* pData)
:GuiField(strFieldName,pWidget,pData)
{
	//column inside datasource can be anything...
	//Q_ASSERT(pDataManager->GetDataSource()->getColumnType(pDataManager->GetDataSource()->getColumnIdx(strFieldName))==QVariant::String || nColumnType==QVariant::Int);

	//get indexes, assert if wrong:
	m_nComboListMapping_Idx=lstComboValues.getColumnIdx(strComboListMapping_Col);
	m_nComboListDisplay_Idx=lstComboValues.getColumnIdx(strComboListDisplay_Col);
	Q_ASSERT(m_nComboListDisplay_Idx!=-1);
	Q_ASSERT(m_nComboListDisplay_Idx!=-1);

	m_pComboWidget=dynamic_cast<QComboBox*>(pWidget);

	//if editable:
	if(bIsEditable)
	{
		Q_ASSERT(strComboListMapping_Col==strComboListDisplay_Col); //if editable, columns must be same
		m_pComboWidget->setEditable(true);
		m_pComboWidget->setAutoCompletion(true);
	}

	//load list into combo
	LoadList(lstComboValues);
}

/*!
	Combo box is selected at postion (row inside list)
*/
void GuiField_ComboBox::FieldChanged(int nPosition)
{

	QVariant value=m_lstData.getDataRef(nPosition,m_nComboListMapping_Idx);

	//if(ValidateUserInput(value))
		m_pDataManager->ChangeData(m_nCurrentRow,m_nIndex,value);
}




/*!
If data is changed from data manipulator, refresh content
If data source is empty, content will be cleared.
*/
void GuiField_ComboBox::RefreshDisplay()
{
	//disconnect signals:
	m_pComboWidget->blockSignals(true);

	//check if current row is out of bound, if so clear content;
	if(m_nCurrentRow<m_pDataManager->GetDataSource()->getRowCount()) 
	{
		QVariant value=m_pDataManager->GetDataSource()->getDataRef(m_nCurrentRow,m_nIndex); //get value from data source
		int nRowInsideCombo=m_lstData.find(m_nComboListMapping_Idx,value,true); //find that value inside combo list
		if(nRowInsideCombo!=-1)
			m_pComboWidget->setEditText(m_lstData.getDataRef(nRowInsideCombo,m_nComboListDisplay_Idx).toString()); //display display col from combo list
		else
			m_pComboWidget->setCurrentIndex(-1);
	}
	else
	{
		m_pComboWidget->setCurrentIndex(-1);
	}

	m_pComboWidget->blockSignals(false);
}


//loads list into combo
void GuiField_ComboBox::LoadList(DbRecordSet &lstComboValues)
{
	//QString strOldItem=m_pComboWidget->currentText();

	//disconnect signals:
	m_pComboWidget->blockSignals(true);

	m_pComboWidget->clear();

	//add data into combo:
	int nSize=lstComboValues.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		m_pComboWidget->addItem(lstComboValues.getDataRef(i,m_nComboListDisplay_Idx).toString());
	}


	m_pComboWidget->clearEditText();

	//connect signals:
	m_pComboWidget->blockSignals(false);

	m_lstData=lstComboValues;

}


//this is for editable combos
void GuiField_ComboBox::on_cmbName_textChanged(const QString &text)
{

	//Add new value into combo list from text field, mapping column must be same as display:
	//only if not already exists in list:
	int nRow=m_lstData.find(m_nComboListDisplay_Idx,text,true);
	if(-1 == nRow)
	{
		m_lstData.addRow();
		m_lstData.setData(m_lstData.getRowCount()-1,m_nComboListDisplay_Idx,text);
		m_pComboWidget->addItem(text);
	}

	//set current index to new
	m_pComboWidget->setCurrentIndex(m_lstData.getRowCount()-1);
}