#ifndef GUIFIELD_VALIDATOR_H
#define GUIFIELD_VALIDATOR_H


#include <QWidget>
#include <QVariant>


/*!
	\class GuiField_Validator
	\brief Abstract class as main point where user input can be tested
	\ingroup GUICore_GUIFields

	Use:
	- inherit this class, and init GuiFieldManager with it
*/
class GuiField_Validator 
{
public:
	virtual bool ValidateUserInput(QWidget *pWidget,int nRow, int nCol, QVariant& value)=0;

};




#endif // GUIFIELD_VALIDATOR_H


