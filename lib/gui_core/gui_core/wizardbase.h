#ifndef WIZARDBASE_H
#define WIZARDBASE_H

#include <QtWidgets/QDialog>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QHash>
#include "common/common/cliententitycache.h"
#include "gui_core/gui_core/wizardpage.h"

class WizardPagesCollection
{
public:
	WizardPagesCollection(QList<WizardPage*> WizardPagesList);
	~WizardPagesCollection();

	QList<WizardPage*> GetPages();
	void RemovePage(int nIndex);
	void RemoveLastPage();

private:
	QList<WizardPage*>						m_vectWizardPagesList;		//< Wizard pages vector.
};

class WizardBase : public QDialog
{
	Q_OBJECT

public:
    WizardBase(int LocationID, QHash<int, WizardPagesCollection*> hshWizardPageHash, ClientEntityCache *pGlobalCache,QWidget *parent = 0);
    ~WizardBase();

	void				GetWizPageRecordSetList(QList<DbRecordSet> &RecordSetList);	//< Get wizard recordset list.
	void				ClearPageHashFromThisPageUp(int nThisPageNumber);
	void				SetRetryMode(){m_bRetry=true;}
	void				SetMainLayoutSize(QSize szSize);

	WizardPage*			GetWizardPage(int nIdx){ return	m_hshWizardPageOrder.value(nIdx); }

protected:
	void				setFirstPage();
	void				DisableButtons();
	void				EnableButtons();
	void				FillPageHashFromThisPageUp(int nThisPageNumber, int nNextCollectionID);

	//int					m_nLocationID;
	int					m_nPageNumber;

private:
	QList<WizardPage*> GetWizardPageVector(int WizardID);

	QHash<int, WizardPagesCollection*> m_hshWizardPageHash;				//< Pages wizard hash. Defined in fui_collection/wizardregistration.h
	QHash<int, WizardPage*>	m_hshWizardPageOrder;						//< Wizard page number to wizard page.
	QList<DbRecordSet>	m_lstPageRecordSet;								//< Pages results recordset list.

	QPushButton			*cancelButton;
	QPushButton			*backButton;
	QPushButton			*nextButton;
	QPushButton			*finishButton;
	QHBoxLayout			*buttonLayout;
	QVBoxLayout			*mainLayout;
	ClientEntityCache	*m_pCache;				//< Pointer to cache
	bool				m_bRetry;

private slots:
	void				backButtonClicked();
	void				nextButtonClicked();
	void				completeStateChanged();
	void				pageRequestNextPage();
	virtual void		accept();
};

#endif // WIZARDBASE_H
