#include "guifield_radiobutton.h"



GuiField_RadioButton::GuiField_RadioButton(QString strFieldName,QList<QRadioButton*> lstButtons,QList<QVariant> lstValues,DbRecordSet* pData,DbView *TableView)
:GuiField(strFieldName,lstButtons.value(0),pData,TableView)
{

	//test column type in datasource:
	Q_ASSERT(pData->getColumnType(pData->getColumnIdx(strFieldName))==QVariant::Int);

	Q_ASSERT(lstButtons.size()==lstValues.size());	//must have same size
	Q_ASSERT(lstButtons.size()!=0);					//must have at least 1

	for(int i=0;i<lstButtons.size();++i)
	{
		m_lstButtons[lstButtons.at(i)]=lstValues.at(i);
		connect(lstButtons.at(i),SIGNAL( clicked(bool)),this,SLOT(FieldChanged()));
	}

}

/*!
	Radio button is pressed: check state of all of them, save value of checked one
	NOTE: assert is fired if row is out of bound
*/
void GuiField_RadioButton::FieldChanged()
{

	bool bChecked=false;
	RadioButtonListIterator i(m_lstButtons);
	while(i.hasNext())
	{
		i.next();
		if(i.key()-> isChecked())
		{
				QVariant value=i.value();
				//if(ValidateUserInput(value))
					m_pDataManager->ChangeData(m_nCurrentRow,m_nIndex,i.value());
				//else
				//	return; //error
				bChecked=true;
				break;

		}
	}
	//Q_ASSERT(bChecked);//RB must have value!

}




/*!
	If data is changed from data manipulator, refresh content
	If data source is empty, content will be cleared.
*/
void GuiField_RadioButton::RefreshDisplay()
{
	//check if current row is out of bound, if so clear content;
	if(m_nCurrentRow<m_pDataManager->GetDataSource()->getRowCount()) 
	{
		QVariant value = m_pDataManager->GetDataSource()->getDataRef(m_nCurrentRow,m_nIndex);

		RadioButtonListIterator i(m_lstButtons);
		bool bChecked=false;
		while(i.hasNext())
		{
			i.next();
			if(i.value()==value)
			{
				i.key()->setChecked(true);
				bChecked=true;
				break;
			}
		}

		if (!bChecked)
		{
			//set first down if value IS NULL
			RadioButtonListConstIterator k=m_lstButtons.constBegin();
			if (k.key())
				k.key()->setChecked(true);
			//Q_ASSERT(bChecked);//RB must have value!
		}
	}
	else
	{
		//set first down:
		RadioButtonListConstIterator i=m_lstButtons.constBegin();
		i.key()->setDown(true);
	}
}


void GuiField_RadioButton::setEnabled(bool bEnable)
{
	RadioButtonListIterator i(m_lstButtons);
	bool bChecked=false;
	while(i.hasNext())
	{
		i.next();
		i.key()->setEnabled(bEnable);
	}
}