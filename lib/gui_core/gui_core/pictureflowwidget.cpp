#include "pictureflowwidget.h"

#include <QHBoxLayout>
#include <QMouseEvent>
#include <QApplication>
#include <QMenu>

#include "thememanager.h"

PictureFlowWidget::PictureFlowWidget(QStringList lstPictures, QSize slideSize /*= QSize(60,80)*/, int nCenterItem /*= 0*/, int nPicFlowType /*= 0*/, QObject *parent /*= 0*/)
{
	setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

	m_pScene = new QGraphicsScene();
	setScene(m_pScene);

	m_bItemsMoveInProgress = false;
	m_szSlideSize = slideSize;
	m_nPicFlowType = nPicFlowType;
	m_nDuration = 200;
	m_pMoveTimeLine = new QTimeLine(m_nDuration, this);
	connect(m_pMoveTimeLine, SIGNAL(stateChanged(QTimeLine::State)), this, SLOT(on_timer_stateChanged(QTimeLine::State)));
	m_pMoveTimeLine->setCurveShape(QTimeLine::EaseOutCurve);
	m_pMoveTimeLine->setFrameRange(0, m_nDuration);

	StyledPushButton *m_pAddItemButton = new StyledPushButton(this, ":Add_Favorite.png", ":Add_Favorite.png", "", "", 0, 0);
	connect(m_pAddItemButton,SIGNAL(clicked()),this,SLOT(On_AddItemButton_clicked()));

	int nCount = 0;
	foreach(QString strPicture, lstPictures)
	{
		//Set alpha channel.
		QImage img(strPicture);
		QImage image(img.alphaChannel());
		img.setAlphaChannel(image);

		//Set size.
		img = img.scaledToHeight(m_szSlideSize.height());

		//Create item.
		PictureGraphItem *item = new PictureGraphItem(img, nCount, m_pMoveTimeLine, m_nPicFlowType, this);
		connect(item, SIGNAL(centerIndexChanged(int, PictureGraphItem *)), this, SLOT(on_centerIndexChanged(int, PictureGraphItem *)));
		connect(item, SIGNAL(hoverEnterItem(PictureGraphItem *)), this, SLOT(On_ItemMouseEnter(PictureGraphItem *)));
		connect(item, SIGNAL(hoverLeaveItem(PictureGraphItem *)), this, SLOT(On_ItemMouseLeave(PictureGraphItem *)));
		m_hshPictures.insert(nCount, item);
		m_pScene->addItem(item);
		item->setItemPosition(nCount-nCenterItem);
		
		if (!(nCount-nCenterItem))
			m_pCenterItem = item;

		nCount++;
	}

	m_pScene->setSceneRect(sceneRect());
	setViewportUpdateMode(QGraphicsView::SmartViewportUpdate);

	m_pMoveTimeLine->start();
}

PictureFlowWidget::~PictureFlowWidget()
{
	m_pCenterItem = NULL;
	ClearItems();
}

void PictureFlowWidget::SetItems(DbRecordSet &recContact, int nCenterIndex /*= 0*/)
{
	ClearItems();

	int nRowCount = recContact.getRowCount();
	for (int i = 0; i < nRowCount; ++i)
	{
		//Set alpha channel.
		QImage img;
		if(recContact.getDataRef(i,"BCNT_PICTURE").toByteArray().size()==0)
			img.load(":Icon_Person128.png");
		else
			img.loadFromData(recContact.getDataRef(i,"BCNT_PICTURE").toByteArray());

		//Set alpha channel.
		QImage image(img.alphaChannel());
		img.setAlphaChannel(image);

		//Set size.
		img = img.scaledToHeight(m_szSlideSize.height(), Qt::SmoothTransformation);

		//Item text.
		QString strName;
		QString strFirstName=recContact.getDataRef(i,"BCNT_FIRSTNAME").toString();
		QString strLastName=recContact.getDataRef(i,"BCNT_LASTNAME").toString();
		QString strOrgName=recContact.getDataRef(i,"BCNT_ORGANIZATIONNAME").toString();
		if (!strLastName.isEmpty())
			strName=strLastName;
		if (!strFirstName.isEmpty())
			strName+=" "+strFirstName.trimmed();
		strName = strName.trimmed();

		QString strItemText = strName + ";" + strOrgName;

		//Create item.
		PictureGraphItem *item = new PictureGraphItem(img, i, m_pMoveTimeLine, m_nPicFlowType, this, strItemText);
		connect(item, SIGNAL(centerIndexChanged(int, PictureGraphItem *)), this, SLOT(on_centerIndexChanged(int, PictureGraphItem *)));
		connect(item, SIGNAL(hoverEnterItem(PictureGraphItem *)), this, SLOT(On_ItemMouseEnter(PictureGraphItem *)));
		connect(item, SIGNAL(hoverLeaveItem(PictureGraphItem *)), this, SLOT(On_ItemMouseLeave(PictureGraphItem *)));
		m_hshPictures.insert(i, item);
		m_pScene->addItem(item);
		item->setItemPosition(i-nCenterIndex);
		if (!(i-nCenterIndex))
			m_pCenterItem = item;
	}
	
	m_pScene->setSceneRect(sceneRect());
	m_pMoveTimeLine->start();
}

void PictureFlowWidget::SetContextMenuActions(QList<QAction*> lstContextMenuActions)
{
	m_lstContextMenuActions.clear();
	m_lstContextMenuActions = lstContextMenuActions;
}

void PictureFlowWidget::GetContextMenuActions(QList<QAction*> &lstContextMenuActions)
{
	lstContextMenuActions.clear();
	lstContextMenuActions = m_lstContextMenuActions;
}

void PictureFlowWidget::setBackground(QBrush brush)
{
	setBackgroundBrush(brush);
	setCacheMode(QGraphicsView::CacheBackground);
}

void PictureFlowWidget::ClearItems()
{
	qDeleteAll(m_hshPictures);
	m_hshPictures.clear();
}

PictureGraphItem* PictureFlowWidget::GetCenterItem()
{
	return m_pCenterItem;
}

void PictureFlowWidget::SetCenterGraphItem(QGraphicsItem *item)
{
	int nCurrentItemPosition = dynamic_cast<PictureGraphItem*>(item)->GetCurrentItemPosition();
	if (nCurrentItemPosition==0)
	{
		int nIndex = m_hshPictures.key(dynamic_cast<PictureGraphItem*>(item));
		emit centerIndexSelectedForEditing(nIndex);
		return;
	}
	
	//If not central widget move all items left or right but one step at a time.
	QHashIterator<int, PictureGraphItem*> iter(m_hshPictures);
	while (iter.hasNext())
	{
		iter.next();
		int nNextItemPosition;
		int itemPosition = iter.value()->GetCurrentItemPosition();
		if (nCurrentItemPosition < 0)
			nNextItemPosition = itemPosition + abs(nCurrentItemPosition);
		else
			nNextItemPosition = itemPosition - nCurrentItemPosition;

		iter.value()->setItemPosition(nNextItemPosition);
	}

	//Start move timer - ignore user input.
	m_pMoveTimeLine->start();
}

void PictureFlowWidget::MoveItemsOneLeft()
{
	//If reached the leftmost position stop timer and do nothing.
	if ((m_hshPictures.value(0)->GetCurrentItemPosition()-1+m_hshPictures.count()) == 0)
		return;
	
	QHashIterator<int, PictureGraphItem*> iter(m_hshPictures);
	while (iter.hasNext())
	{
		iter.next();
		int nNextItemPosition = iter.value()->GetCurrentItemPosition() - 1;
		iter.value()->setItemPosition(nNextItemPosition);
	}
	
	//Start move timer - ignore user input.
	m_pMoveTimeLine->start();
}

void PictureFlowWidget::MoveItemsOneRight()
{
	//If reached the leftmost position stop timer and do nothing.
	if ((m_hshPictures.value(m_hshPictures.count()-1)->GetCurrentItemPosition()+1-m_hshPictures.count()) == 0)
		return;
	
	QHashIterator<int, PictureGraphItem*> iter(m_hshPictures);
	while (iter.hasNext())
	{
		iter.next();
		int nNextItemPosition = iter.value()->GetCurrentItemPosition() + 1;
		iter.value()->setItemPosition(nNextItemPosition);
	}
	
	//Start move timer - ignore user input.
	m_pMoveTimeLine->start();
}

void PictureFlowWidget::mousePressEvent(QMouseEvent *e)
{
	if (m_hshPictures.isEmpty())
		return;
	
	//If already moving or reached the end of one side - skip.
	if (m_bItemsMoveInProgress)
	{
		return;
	}
	
	QPoint position = e->pos();
	if (itemAt(position))
	{
		QGraphicsItem *item = itemAt(position);
		SetCenterGraphItem(item);
		//emit centerIndexChanged(m_hshPictures.key(dynamic_cast<PictureGraphItem*>(item)));
	}
	else
	{
		if (width()/2 > position.x())
			MoveItemsOneRight();
		else
			MoveItemsOneLeft();
	}
}

void PictureFlowWidget::mouseDoubleClickEvent(QMouseEvent *e)
{
	if (m_hshPictures.isEmpty())
		return;

	//If central item then send double click event.
	QPoint position = e->pos();
	if (itemAt(position))
	{
		PictureGraphItem *item = dynamic_cast<PictureGraphItem*>(itemAt(position));
		//Is central item?
		if(item->GetCurrentItemPosition()==0)
		{
			int nIndex = m_hshPictures.key(item);
			emit centerIndexDoubleClicked(nIndex);
		}
	}
}

void PictureFlowWidget::contextMenuEvent(QContextMenuEvent *e)
{
	/*
	if(itemAt(e->pos())
	{
		QGraphicsView::contextMenuEvent(e);
		return;
	}
	*/

	//Show context menu only if no items.
	QMenu ContextMenu;

	QList<QAction*> lstActions;
	GetContextMenuActions(lstActions);
	int nSize= lstActions.size();
	for(int i=0;i<nSize;++i)
		ContextMenu.addAction(lstActions.at(i));

	ContextMenu.exec(e->globalPos());
}

void PictureFlowWidget::on_centerIndexChanged(int nIndex, PictureGraphItem *item)
{
	m_pCenterItem = item;
	m_pCenterItem->ResetOriginalDisplayText();
	emit centerIndexChanged(nIndex);
	update();
}
void PictureFlowWidget::On_AddItemButton_clicked()
{
	emit AddItemButton_clicked();
}

void PictureFlowWidget::On_ItemMouseEnter(PictureGraphItem *item)
{
	if (m_bItemsMoveInProgress)
		return;
	
	//If central item - do nothing.
	if(!item->GetCurrentItemPosition())
		return;
	
	//Set center item text to hover item.
	PictureGraphItem *centerItem = GetCenterItem();

	centerItem->SetItemDisplayText(item->GetItemText());
	update();
}

void PictureFlowWidget::On_ItemMouseLeave(PictureGraphItem *item)
{
	if (m_bItemsMoveInProgress)
		return;
	
	//If central item - do nothing.
	if(!item->GetCurrentItemPosition())
		return;

	//Set center item text back to it's original.
	PictureGraphItem *centerItem = GetCenterItem();
	centerItem->ResetOriginalDisplayText();
	update();
}

void PictureFlowWidget::on_timer_stateChanged(QTimeLine::State state)
{
	m_bItemsMoveInProgress = false;
}
