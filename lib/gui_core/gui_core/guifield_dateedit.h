#ifndef GUIFIELD_DATEEDIT_H
#define GUIFIELD_DATEEDIT_H

#include "guifield.h"
#include "datetimeeditex.h"



/*!
	\class GuiField_DateEdit
	\brief Defines DateEdit
	\ingroup GUICore_GUIFields

	Connects to datasource
*/
class GuiField_DateEdit : public GuiField
{
	Q_OBJECT

public:
	GuiField_DateEdit(QString strFieldName,QWidget *pWidget,GuiDataManipulator* pDataManager,DbView *TableView=NULL, QLabel *pLabel=NULL);

	void RefreshDisplay(); //DATA SOURCE -> UI


private:
	DateTimeEditEx* m_pEditWidget;
	int m_nType; //0-datetime,1-date,2-time

public slots:
	void FieldChangedTime(const QTime & time); 
	void FieldChangedDateTime(const QDateTime & datetime); 
	void FieldChangedDate(const QDate & date);

};

#endif // GUIFIELD_DATEEDIT_H

