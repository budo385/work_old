#ifndef DBRECORDSETLISTMODEL_H
#define DBRECORDSETLISTMODEL_H

#include <QAbstractListModel>
#include <Qt>
#include <QtGlobal>
#include <QIcon>
#include <QColor>
#include <QFont>
#include <QPainter>
#include <QImageReader>
#include <QPersistentModelIndex>

#include "common/common/dbrecordset.h"
#include "dbrecordsetitem.h"

/*!
	\class  DbRecordSetListModel
	\ingroup GUICore_ModelClasses
	\brief  RecordSet list model. 

	Model class for list views.
*/
class DbRecordSetListModel : public QAbstractListModel
{
public:
    DbRecordSetListModel(QObject *Parent = NULL);
	~DbRecordSetListModel();
	virtual void InitializeModel(DbRecordSet *RecordSet = NULL);
	virtual void ClearModel();

	int rowCount(const QModelIndex &parent = QModelIndex()) const;
	virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
	QVariant headerData(int section, Qt::Orientation orientation, int role) const;

	virtual bool setHeaderData (int section, Qt::Orientation orientation, const QVariant &value, int role = Qt::EditRole);
	virtual Qt::ItemFlags flags(const QModelIndex &index) const;

	virtual Qt::DropActions supportedDropActions() const;
	
	QHash<int, DbRecordSetItem*> *GetItemsHash();
	
protected:
	virtual void SetupModelData(DbRecordSetItem *Parent = NULL);
	void SetRootItem();

	QHash<int, DbRecordSetItem*> m_hshModelItems;		//< Model items hash.
	DbRecordSet		*m_pDbRecordSet;					//< Recordset.
	DbRecordSetItem *m_pRootItem;						//< Root item.
	QSize			m_sizeMaxIconSize;					//< Max icon size (because of multiple icons).
	bool			m_bModelCleared;					//< Model cleared or not.
};

#endif // DBRECORDSETLISTMODEL_H

