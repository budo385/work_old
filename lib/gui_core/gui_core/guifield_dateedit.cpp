#include "guifield_dateedit.h"





GuiField_DateEdit::GuiField_DateEdit(QString strFieldName,QWidget *pWidget,GuiDataManipulator* pDataManager,DbView *TableView, QLabel *pLabel)
:GuiField(strFieldName,pWidget,pDataManager,TableView,pLabel)
{
	//try to determine what type:

	m_pEditWidget=dynamic_cast<DateTimeEditEx*>(pWidget);
	if (m_pEditWidget)
	{
		Q_ASSERT(m_pEditWidget!=NULL);
		m_pEditWidget-> setCalendarPopup(true);
		//test column type in datasource:
		if (pDataManager->GetDataSource()->getColumnType(pDataManager->GetDataSource()->getColumnIdx(strFieldName))==QVariant::Date)
		{
			m_nType=1;
			connect(m_pEditWidget,SIGNAL(dateChanged(const QDate &)),this,SLOT(FieldChangedDate(const QDate &)));
		}
		else
		{
			m_nType=0;
			connect(m_pEditWidget,SIGNAL(dateTimeChanged (const QDateTime &)),this,SLOT(FieldChangedDateTime(const QDateTime &)));
		}
	}
	Q_ASSERT(m_pEditWidget);
	/*
	else
	{
		m_pEditWidget=dynamic_cast<QDateEdit*>(pWidget);
		if (m_pEditWidget)
		{
			//test column type in datasource:
			Q_ASSERT(pDataManager->GetDataSource()->getColumnType(pDataManager->GetDataSource()->getColumnIdx(strFieldName))==QVariant::Date);
			m_nType=1;
			m_pEditWidget-> setCalendarPopup(true);
			connect(m_pEditWidget,SIGNAL(dateChanged(const QDate &)),this,SLOT(FieldChangedDate(const QDate &)));
		}
		else
		{
			m_pEditWidget=dynamic_cast<QTimeEdit*>(pWidget);
			if (m_pEditWidget)
			{
				//test column type in datasource:
				Q_ASSERT(pDataManager->GetDataSource()->getColumnType(pDataManager->GetDataSource()->getColumnIdx(strFieldName))==QVariant::Time);
				m_nType=2;
				connect(m_pEditWidget,SIGNAL(timeChanged (const QTime &)),this,SLOT(FieldChangedTime(const QTime &)));
			}
			else
				Q_ASSERT(false);

		}
	}
	*/



	

	

}

void GuiField_DateEdit::FieldChangedTime(const QTime & time)
{
	//if(ValidateUserInput(time))
	QVariant value=QVariant(time);
	if (m_pEditWidget->time().isNull() || !time.isValid())
		value=QVariant(QVariant::Time);
	m_pDataManager->ChangeData(m_nCurrentRow,m_nIndex,value);

}
void GuiField_DateEdit::FieldChangedDateTime(const QDateTime & datetime)
{
	//qDebug()<<datetime;
	//qDebug()<<datetime.isValid();

	//if(ValidateUserInput(datetime))
	QVariant value=QVariant(datetime);
	if (m_pEditWidget->dateTime().isNull() || !datetime.isValid())
		value=QVariant(QVariant::DateTime);
	
	m_pDataManager->ChangeData(m_nCurrentRow,m_nIndex,value);

}
void GuiField_DateEdit::FieldChangedDate(const QDate & date)
{
	//qDebug()<<date;
	//qDebug()<<date.isValid();

	QVariant value=QVariant(date);
	if (m_pEditWidget->date().isNull() || !date.isValid())
		value=QVariant(QVariant::Date);
	m_pDataManager->ChangeData(m_nCurrentRow,m_nIndex,value);
}



/*!
	If data is changed from data manipulator, refresh content
	If data source is empty, content will be cleared.
*/
void GuiField_DateEdit::RefreshDisplay()
{
	m_pEditWidget->blockSignals(true);

	//check if current row is out of bound, if so clear content;
	if(m_nCurrentRow<m_pDataManager->GetDataSource()->getRowCount()) 
		switch(m_nType)
		{
		case 0:
			m_pEditWidget->setDateTime(m_pDataManager->GetDataSource()->getDataRef(m_nCurrentRow,m_nIndex).toDateTime());
			break;
		case 1:
			m_pEditWidget->setDate(m_pDataManager->GetDataSource()->getDataRef(m_nCurrentRow,m_nIndex).toDate());
		    break;
		default:
			m_pEditWidget->setTime(m_pDataManager->GetDataSource()->getDataRef(m_nCurrentRow,m_nIndex).toTime());
		    break;
		}
	else
		m_pEditWidget->clear();		

	m_pEditWidget->blockSignals(false);
}

