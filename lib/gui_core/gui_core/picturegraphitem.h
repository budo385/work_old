#ifndef PICTUREGRAPHITEM_H
#define PICTUREGRAPHITEM_H

#include <QObject>
#include <QGraphicsItem>
#include <QGraphicsView>
#include <QTimeLine>

class PictureGraphItem : public QObject, public QGraphicsItem
{
	Q_OBJECT
	//Q_INTERFACES(QGraphicsItem)

public:
	PictureGraphItem(QImage &imgPicture, int nItemIndex, QTimeLine *pMoveTimeLine, int nPicFlowType = 0, QWidget *parent = 0, QString strItemText = QString());
	~PictureGraphItem();

	void setItemPosition(int nPosition);
	int	 GetCurrentItemPosition();
	void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
	QRectF boundingRect() const;
	QString GetItemText();
	void SetItemDisplayText(QString strItemText);
	void ResetOriginalDisplayText();

protected:
	void contextMenuEvent(QGraphicsSceneContextMenuEvent *event);
	void hoverEnterEvent(QGraphicsSceneHoverEvent *event);
	void hoverLeaveEvent(QGraphicsSceneHoverEvent *event);

private:

	QWidget		*m_pParent;
	QImage		m_imgItemImage;
	QImage		m_imgMirrorItemImage;
	QTimeLine	*m_pMoveTimeLine;
	int			m_nDuration;
	int			m_nPicFlowType;

	int			m_nNextPosition;
	int			m_nCurrentPosition;
	QPointF		m_pntCurrentPos;
	QPointF		m_pntNextPos;
	int			m_nCurrentRotationAngle;
	int			m_nItemIndex;
	QString		m_strItemText;
	QString		m_strDisplayItemText;

	int			m_nOffSet;
	int			m_nRotationAngle;
	qreal		m_ScaleFactor;

private slots:
	void on_frameChanged(int i);
	void on_stateChanged(QTimeLine::State state);
signals:
	void centerIndexChanged(int nIndex, PictureGraphItem *item);
	void hoverEnterItem(PictureGraphItem *item);
	void hoverLeaveItem(PictureGraphItem *item);
};

#endif // PICTUREGRAPHITEM_H
