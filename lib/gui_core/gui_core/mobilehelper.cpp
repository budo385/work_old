#include "mobilehelper.h"
#include "gui_core/gui_core/thememanager.h"
#include <QHBoxLayout>
#include <QtWidgets/QFrame>
#include <QApplication>
#include <QDesktopWidget>
#include <QPainter>

#include <cmath>

// Exponential blur, Jani Huhtanen, 2006
//
template<int aprec, int zprec>
static inline void blurinner(unsigned char *bptr, int &zR, int &zG, int &zB, int &zA, int alpha);

template<int aprec,int zprec>
static inline void blurrow( QImage & im, int line, int alpha);

template<int aprec, int zprec>
static inline void blurcol( QImage & im, int col, int alpha);

/*
*  expblur(QImage &img, int radius)
*
*  In-place blur of image 'img' with kernel
*  of approximate radius 'radius'.
*
*  Blurs with two sided exponential impulse
*  response.
*
*  aprec = precision of alpha parameter 
*  in fixed-point format 0.aprec
*
*  zprec = precision of state parameters
*  zR,zG,zB and zA in fp format 8.zprec
*/
template<int aprec,int zprec>
void expblur( QImage &img, int radius )
{
	if(radius<1)
		return;

	/* Calculate the alpha such that 90% of 
	the kernel is within the radius.
	(Kernel extends to infinity) 
	*/
	int alpha = (int)((1<<aprec)*(1.0f-expf(-2.3f/(radius+1.f))));

	for(int row=0;row<img.height();row++)
	{
		blurrow<aprec,zprec>(img,row,alpha);
	}

	for(int col=0;col<img.width();col++)
	{
		blurcol<aprec,zprec>(img,col,alpha);
	}
	return;
}

template<int aprec, int zprec>
static inline void blurinner(unsigned char *bptr, int &zR, int &zG, int &zB, int &zA, int alpha)
{
	int R,G,B,A;
	R = *bptr;
	G = *(bptr+1);
	B = *(bptr+2);
	A = *(bptr+3);

	zR += (alpha * ((R<<zprec)-zR))>>aprec;
	zG += (alpha * ((G<<zprec)-zG))>>aprec;
	zB += (alpha * ((B<<zprec)-zB))>>aprec;
	zA += (alpha * ((A<<zprec)-zA))>>aprec;

	*bptr =     zR>>zprec;
	*(bptr+1) = zG>>zprec;
	*(bptr+2) = zB>>zprec;
	*(bptr+3) = zA>>zprec;
}

template<int aprec,int zprec>
static inline void blurrow( QImage & im, int line, int alpha)
{
	int zR,zG,zB,zA;

	QRgb *ptr = (QRgb *)im.scanLine(line);

	zR = *((unsigned char *)ptr    )<<zprec;
	zG = *((unsigned char *)ptr + 1)<<zprec;
	zB = *((unsigned char *)ptr + 2)<<zprec;
	zA = *((unsigned char *)ptr + 3)<<zprec;

	for(int index=1; index<im.width(); index++)
	{
		blurinner<aprec,zprec>((unsigned char *)&ptr[index],zR,zG,zB,zA,alpha);
	}
	for(int index=im.width()-2; index>=0; index--)
	{
		blurinner<aprec,zprec>((unsigned char *)&ptr[index],zR,zG,zB,zA,alpha);
	}


}

template<int aprec, int zprec>
static inline void blurcol( QImage & im, int col, int alpha)
{
	int zR,zG,zB,zA;

	QRgb *ptr = (QRgb *)im.bits();
	ptr+=col;

	zR = *((unsigned char *)ptr    )<<zprec;
	zG = *((unsigned char *)ptr + 1)<<zprec;
	zB = *((unsigned char *)ptr + 2)<<zprec;
	zA = *((unsigned char *)ptr + 3)<<zprec;

	for(int index=im.width(); index<(im.height()-1)*im.width(); index+=im.width())
	{
		blurinner<aprec,zprec>((unsigned char *)&ptr[index],zR,zG,zB,zA,alpha);
	}

	for(int index=(im.height()-2)*im.width(); index>=0; index-=im.width())
	{
		blurinner<aprec,zprec>((unsigned char *)&ptr[index],zR,zG,zB,zA,alpha);
	}

}

/*
MobileHelper::MobileHelper(QObject *parent)
	: QObject(parent)
{

}

MobileHelper::~MobileHelper()
{

}
*/

QImage MobileHelper::CreateImageMirror(const QImage &image, bool bMirrorImage /*= true*/)
{
	//If not mirror then return original.
	if (!bMirrorImage)
		return QImage(); 
	
	//Mirrored image.
	QImage mirrorImage = image;
	
	//Mirror scale factor.
	qreal scaleFactor = 0.4;

	mirrorImage = mirrorImage.mirrored();
	mirrorImage = mirrorImage.scaled(mirrorImage.width(), mirrorImage.height()*scaleFactor);

	QPoint p1, p2;
	p2.setY(mirrorImage.height());

	QLinearGradient gradient(p1, p2);
	gradient.setColorAt(0, Qt::white);
	gradient.setColorAt(0.25, QColor(0, 0, 0, 20));
	gradient.setColorAt(1, Qt::transparent);

	QPainter p(&mirrorImage);
	p.setBackgroundMode(Qt::TransparentMode);
	p.setCompositionMode(QPainter::CompositionMode_DestinationIn);
	p.fillRect(0, 0, mirrorImage.width(), mirrorImage.height(), gradient);
	p.end();

	expblur<16,7>(mirrorImage, 8);

	return mirrorImage;
}

void MobileHelper::SelectedContactImageFrame_Create(QWidget *pWidget, SelectedContactPicture **pPic, QSize slideSize /*= QSize(60,80)*/)
{
	*pPic = new SelectedContactPicture();
	//Get image.
	QImage img;
	img.load(":Icon_Person128.png");
	QImage image(img.alphaChannel());
	img.setAlphaChannel(image);

	//Create layout.
	QVBoxLayout *vbox1=new QVBoxLayout;
	vbox1->setSpacing(0);
	vbox1->setMargin(0);
	vbox1->addStretch(1);
	vbox1->addWidget((*pPic));
	vbox1->addStretch(1);

	QPixmap pix = QPixmap::fromImage(img);
	(*pPic)->SetPixmap(pix);

	pWidget->setLayout(vbox1);
	if (dynamic_cast<QFrame*>(pWidget))
		pWidget->setStyleSheet(".QFrame "+ThemeManager::GetMobileSolidBkg_Dark());
	else
		pWidget->setStyleSheet(".QWidget "+ThemeManager::GetMobileSolidBkg_Dark());
}

void  MobileHelper::SelectedContactImageFrame_SetContactPicture(SelectedContactPicture *pPic, DbRecordSet &recContact)
{
	QImage img;
	if (recContact.getRowCount()>0)
	{
		if(recContact.getDataRef(0,"BCNT_PICTURE").toByteArray().size()==0)
		{
			img.load(":Icon_Person128.png");
		}
		else
		{
			img.loadFromData(recContact.getDataRef(0,"BCNT_PICTURE").toByteArray());
		}
	}
	else
	{
		img.load(":Icon_Person128.png");
	}

	QImage image(img.alphaChannel());
	img.setAlphaChannel(image);
	
	QPixmap pix = QPixmap::fromImage(img);
	pPic->SetPixmap(pix);
}

void MobileHelper::PictureFlowWidget_Create(QWidget *pWidget, PictureFlowWidget **pPic, QStringList &lstPictures, QSize slideSize /*= QSize(60,80)*/, int nCenterItem /*= 0*/, int nPicFlowType /*= 0*/)
{
	*pPic = new PictureFlowWidget(lstPictures, slideSize, nCenterItem, nPicFlowType);
	QString strr = ThemeManager::GetMobileBkg();
	(*pPic)->setStyleSheet("QGraphicsView "+ThemeManager::GetMobileBkg());

	if (pWidget)
	{
		QHBoxLayout *hbox1=new QHBoxLayout;
		hbox1->setSpacing(0);
		hbox1->setMargin(0);
		hbox1->addWidget((*pPic));

		pWidget->setLayout(hbox1);
	}
}

void MobileHelper::PictureFlowWidget_SetContactsPictures(PictureFlowWidget *pPicFlow, DbRecordSet &recContact, int nCenterIndex, QList<QAction*> lstContextMenuActions)
{
	pPicFlow->SetContextMenuActions(lstContextMenuActions);
	pPicFlow->SetItems(recContact, nCenterIndex);
}
bool MobileHelper::IsScreenInPortraitMode()
{
	int nScreen = QApplication::desktop()->primaryScreen();
	int nHeight = QApplication::desktop()->screenGeometry(nScreen).height();
	int nWidth  = QApplication::desktop()->screenGeometry(nScreen).width();

	if (nHeight>nWidth)
		return true;
	else
		return false;
}
