#ifndef PICTUREFLOWWIDGET_H
#define PICTUREFLOWWIDGET_H

#include <QWidget>
#include <QHash>
#include <QTimer>

#include "common/common/dbrecordset.h"
#include "picturegraphitem.h"
#include "styledpushbutton.h"

class PictureFlowWidget : public QGraphicsView
{
	Q_OBJECT

public:
	enum PictureFlowWidgetType
	{
		MAIN_MENU_PICFLOW = 0,
		FAVORITES_MENU_PICFLOW
	};

	PictureFlowWidget(QStringList lstPictures, QSize slideSize = QSize(60,80), int nCenterItem = 0, int nPicFlowType = 0, QObject *parent = 0);
	~PictureFlowWidget();

	void SetItems(DbRecordSet &recContact, int nCenterIndex = 0);
	void SetContextMenuActions(QList<QAction*> lstContextMenuActions);
	void GetContextMenuActions(QList<QAction*> &lstContextMenuActions);
	void setBackground(QBrush brush);

protected:
	void mousePressEvent(QMouseEvent *e);
	void mouseDoubleClickEvent(QMouseEvent *e);

	void contextMenuEvent(QContextMenuEvent *e);

private:
	void ClearItems();
	PictureGraphItem* GetCenterItem();

	void SetCenterGraphItem(QGraphicsItem *item);
	void MoveItemsOneLeft();
	void MoveItemsOneRight();

	bool							m_bItemsMoveInProgress;
	QGraphicsScene					*m_pScene;
	QHash<int, PictureGraphItem*>	m_hshPictures;
	PictureGraphItem				*m_pCenterItem;
	QSize							m_szSlideSize;
	int								m_nPicFlowType;
	QList<QAction*>					m_lstContextMenuActions;
	QPixmap							*m_AddItemImage;
	StyledPushButton				*m_pAddItemButton;
	QTimeLine						*m_pMoveTimeLine;
	int								m_nDuration;

private slots:
	void on_centerIndexChanged(int nIndex, PictureGraphItem *item);
	void On_AddItemButton_clicked();
	void On_ItemMouseEnter(PictureGraphItem *item);
	void On_ItemMouseLeave(PictureGraphItem *item);
	void on_timer_stateChanged(QTimeLine::State state);
signals:
	void centerIndexChanged(int nIndex);
	void centerIndexDoubleClicked(int nIndex);
	void centerIndexSelectedForEditing(int nIndex);
	void AddItemButton_clicked();
};

#endif // PICTUREFLOWWIDGET_H
