#include "guifield_richedit.h"



//------------------------------------------------------
//			MULTI LINE EDIT: can be connected to QByteArray or QString (looks up in type of column inside datasource)
//------------------------------------------------------

GuiField_RichEdit::GuiField_RichEdit(QString strFieldName,QWidget *pWidget,GuiDataManipulator* pDataManager,DbView *TableView, QLabel *pLabel)
:GuiField(strFieldName,pWidget,pDataManager,TableView,pLabel)
{
	//set as plain text
	m_bIsPlainText=false;

	//test column type in datasource:
	int nColumnType=pDataManager->GetDataSource()->getColumnType(pDataManager->GetDataSource()->getColumnIdx(strFieldName));
	Q_ASSERT(nColumnType==QVariant::String ); //column must be byte array or string
	//m_bIsByteArray=(nColumnType==QVariant::ByteArray);

	//register signal textChanged() ->ChangeData() if lineEdit:
	m_pEditWidget=dynamic_cast<RichEditWidget*>(pWidget);

	Q_ASSERT(m_pEditWidget!=NULL);

	connect(m_pEditWidget,SIGNAL(textChanged()),this,SLOT(FieldChanged()));

}

/*!
If edit line widget, then this slot is invoked when text changes
NOTE: assert is fired if row is out of bound
*/
void GuiField_RichEdit::FieldChanged()
{
	QVariant value;
	QString strValue;
	if(!m_bIsPlainText)
		strValue= m_pEditWidget->GetHtml();				//stores multiline content as string
	else
		strValue = m_pEditWidget->GetPlainText();		//stores multiline content as string

	value=strValue;
	m_pDataManager->ChangeData(m_nCurrentRow,m_nIndex,value);
}



/*!
	If data is changed from data manipulator, refresh content
	If data source is empty, content will be cleared.
*/
void GuiField_RichEdit::RefreshDisplay()
{
	m_pEditWidget->blockSignals(true);
	m_pEditWidget->GetTextWidget()->blockSignals(true);

	//check if current row is out of bound, if so clear content;
	if(m_nCurrentRow<m_pDataManager->GetDataSource()->getRowCount()) 
	{
		QString strContent=m_pDataManager->GetDataSource()->getDataRef(m_nCurrentRow,m_nIndex).toString();
		if(!m_bIsPlainText)
			m_pEditWidget->SetHtml(strContent);
		else
			m_pEditWidget->SetPlainText(strContent);

		//move to start of doc (issue 1034)
		if (!strContent.isEmpty())
		{
			QTextCursor cur=m_pEditWidget->GetTextWidget()->textCursor();
			cur.movePosition(QTextCursor::Start,QTextCursor::MoveAnchor);
			m_pEditWidget->GetTextWidget()->setTextCursor(cur);
			m_pEditWidget->GetTextWidget()->ensureCursorVisible();
		}
	}
	else
		m_pEditWidget->SetHtml("");		

	m_pEditWidget->blockSignals(false);
	m_pEditWidget->GetTextWidget()->blockSignals(false);

}



void GuiField_RichEdit::setEnabled(bool bEnable)
{
	m_pEditWidget->SetEditMode(bEnable);
};