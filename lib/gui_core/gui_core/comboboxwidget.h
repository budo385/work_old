#ifndef COMBOBOXWIDGET_H
#define COMBOBOXWIDGET_H

#include <QComboBox>
#include <QAction>


/*!
	\class ComboBoxWidget
	\brief  ComboBoxWidget with custom context menu
	\ingroup GUICore_CustomWidgets

	Set context menu with SetContextMenuActions
*/
class ComboBoxWidget : public QComboBox
{

public:
    ComboBoxWidget(QWidget *parent);
    ~ComboBoxWidget();

	void SetContextMenuActions(QList<QAction*>& lstActions){m_lstActions=lstActions;};		//set actions for cnxt menu (alternative approach for setting context menu)
	QList<QAction*>& GetContextMenuActions(){ return m_lstActions; };

private:
	void contextMenuEvent(QContextMenuEvent *event);
	QList<QAction*> m_lstActions;
    
};

#endif // COMBOBOXWIDGET_H
