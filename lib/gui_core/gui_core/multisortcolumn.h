#ifndef MULTISORTCOLUMN_H
#define MULTISORTCOLUMN_H

#include <QtWidgets/QDialog>
#include "generatedfiles/ui_multisortcolumn.h"


//typedef QHash<QString,int> MultiColSortList;


class MultiSortColumn : public QDialog
{
    Q_OBJECT

public:
    MultiSortColumn(QWidget *parent = 0);
    ~MultiSortColumn();

	void SetColumnSetup(DbRecordSet &lstColumns);
	void SetColumnSetup(DbRecordSet &lstColumns, DbRecordSet &lstColumnsForSort);
	DbRecordSet GetResultRecordSet();

private:
    Ui::multisortcolumnclass ui;

private slots:
	void on_btnClose_clicked();
	void on_btnOK_clicked();
};

#endif // MULTISORTCOLUMN_H


