#ifndef PROPERTYBROWSER_H
#define PROPERTYBROWSER_H

#include <QWidget>
#include <QList>

#include "property_browser/qtpropertymanager.h"
#include "property_browser/qtvariantproperty.h"
#include "property_browser/qttreepropertybrowser.h"

#include "common/common/dbrecordset.h"

class PropertyBrowser : public QtTreePropertyBrowser
{
    Q_OBJECT

public:
    PropertyBrowser(QWidget *parent = 0);
    ~PropertyBrowser();

	void Initialize(DbRecordSet *RecordSet, QList<int> MapList = QList<int>());

protected:
	void propertyChanged(QtProperty *property);

private:
	virtual void CreateProperty(DbRecordSet *RecordSet);
	void CreateProperties();

	int								m_nRowID,
									m_nNameColumn, 
									m_nTypeColumn, 
									m_nValueColumn, 
									m_nStrValueColumn;

	QtVariantEditorFactory			*m_pVariantEditorFacatory;
	QtVariantPropertyManager		*m_pVariantManager;
	DbRecordSet						*m_pRecordSet;
	QList<int>						m_MapList;
	QHash<int, QtVariantProperty*>	m_hshRowIDToProperty;

signals:
	void ValueChanged(int RowID, QVariant PropertyValue, QString  PropertyStrValue);
};

#endif // PROPERTYBROWSER_H
