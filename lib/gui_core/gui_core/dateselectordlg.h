#ifndef DATESELECTORDLG_H
#define DATESELECTORDLG_H

#include <QtWidgets/QDialog>
#include "generatedfiles/ui_dateselectordlg.h"

class DateSelectorDlg : public QDialog
{
    Q_OBJECT

public:
    DateSelectorDlg(QWidget *parent = 0);
    ~DateSelectorDlg();

	void setDate(QDate &date);
	QDate getDate(){ return m_selectedDate; };


private:
    Ui::DateSelectorDlgClass ui;

	QDate m_selectedDate;

private slots:
	void on_btnCancel_clicked();
	void on_btnOK_clicked();
	void SetupMonthView();
	void OnDateSelectionChanged();
};

#endif // DATESELECTORDLG_H
