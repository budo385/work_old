#ifndef LOGINDLG_EMBEDDED_H
#define LOGINDLG_EMBEDDED_H
#ifdef WINCE
	#include "embedded_core/embedded_core/generatedfiles/ui_logindlg_embedded.h"
#else
	#include "generatedfiles/ui_logindlg_embedded.h"
#endif
#include "logindialogabstract.h"
#include "styledpushbutton.h"
 
/*!
\class  LoginDlg_Embedded
\brief  Login dialog for embedded applications.
\ingroup GUICore

This dialog is used when logging in a system from an embedded device, 
it can also start a dialog with a list of all defined app. server (network) connections 
or database connections (if in thick mode)
*/
class LoginDlg_Embedded : public LoginDialogAbstract
{
	Q_OBJECT

public:
	LoginDlg_Embedded(QWidget *parent = 0);
	~LoginDlg_Embedded();

	void Initialize(QString strSettingsFile, int nMode=0,bool bThickClient=false);
	void RefreshConnectionsList();
	void SetDefaults(QString strDefaultUserName="", QString strDefaultConnectionName="");
	void SetPasswordFocus();

private slots:
	void on_btnLogin_clicked();
	void on_btnEditSettings_clicked();
	void OnCloseButton_clicked();
	
private:
	Ui::LoginDlg_EmbeddedClass ui;
	StyledPushButton *m_pBtnComm ;
	StyledPushButton *m_pBtnContact;
	StyledPushButton *m_pBtnProject;
	StyledPushButton *m_pCloseButton;
};

#endif // LOGINDLG_EMBEDDED_H
