#include "universaltreefindwidget.h"
#include <QLayout>
#include "gui_core/gui_core/thememanager.h"

UniversalTreeFindWidget::UniversalTreeFindWidget(QWidget *parent)
    : QWidget(parent)
{
}

UniversalTreeFindWidget::~UniversalTreeFindWidget()
{
	m_pTreeWidget = NULL;
}

/*!
	Initialize find widget with pointer to Universal tree widget.

	\param pTreeWidget		  - Pointer to Universal tree widget.
*/
void UniversalTreeFindWidget::Initialize(UniversalTreeWidget *pTreeWidget, bool bSideBarMode,QString strStyleSheet)
{
	//Set locals.
	m_pTreeWidget	= pTreeWidget;

	//Create combo box and put it in layout.
	m_pFindCombo	= new FindComboBox;
	m_pFindCombo->Initialize(m_pTreeWidget);
	
	//Find label.
	m_pFindLabel = new QLabel(tr("Find:"));
	
	//Setup layout.
	QFrame *frame=NULL;
	//issue 1662:
	//if (bSideBarMode)
	//{
		frame = new QFrame(this);
		if(frame){
			frame->setFrameShape(QFrame::StyledPanel);
			frame->setFrameShadow(QFrame::Plain);
			frame->setLineWidth(0);
			//frame->setStyleSheet(strStyleSheet+" "+ThemeManager::GetGlobalWidgetStyle());
		}
	//}

	//Setup layout.
	QHBoxLayout *layout = new QHBoxLayout;
	layout->addSpacing(4);
	layout->addWidget(m_pFindLabel, 0);
	layout->addWidget(m_pFindCombo, 10);
	layout->setContentsMargins(0,1,1,1);
	layout->setSpacing(2);

	if (frame)
	{
		frame->setLayout(layout);
		QHBoxLayout *layout2 = new QHBoxLayout;
		layout2->addWidget(frame);
		layout2->setContentsMargins(0,0,0,0);
		layout2->setSpacing(0);
		setLayout(layout2);
	}
	else
		setLayout(layout);

	//setStyleSheet(ThemeManager::GetGlobalWidgetStyle());
}
