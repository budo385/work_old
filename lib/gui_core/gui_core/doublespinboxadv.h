#ifndef DOUBLESPINBOXADV_H
#define DOUBLESPINBOXADV_H

#include <QVariant>
#include <QDoubleSpinBox>




/*!
	\class  DoubleSpinBoxAdv
	\ingroup GUICore_CustomWidgets
	\brief  Advanced Double Spin Box provides editing of double/decimal values with fixed dot as decimal separator

	Special DoubleSpinBox providing fixed dot as decimal separator regardless of local system settings 
	This was problem with delegate inside QTableWidget & QTableView

	Use:
		- default use in the UniversalTableDelegate 

*/
class DoubleSpinBoxAdv : public QDoubleSpinBox
{
	public:
		DoubleSpinBoxAdv( QWidget * parent = 0):QDoubleSpinBox(parent){};
	
	
		QString textFromValue(double v) const;
		double valueFromText(const QString &text) const;
		QValidator::State validate(QString &text, int &pos) const;

   
};

#endif // DOUBLESPINBOXADV_H

