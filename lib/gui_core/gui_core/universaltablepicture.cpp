#include "universaltablepicture.h"

UniversalTablePicture::UniversalTablePicture(QWidget *parent)
	: QLabel(parent)
{

	setScaledContents(false); //scales image...
}

UniversalTablePicture::~UniversalTablePicture()
{

}



/*!
	Called by UniversalTableWidget when widget is created
	\param value	- sets data from UniversalTableWidget datasource, 
					  data is extracted from value using m_strDataSource
					  value must be DbRecordSet
*/
void UniversalTablePicture::SetData(int nRow, int nCol,QVariant &Data,QString strDataSourceFormat, bool bIsEditable)
{

	//sets data:
	TableCellWidget::SetData(nRow,nCol,Data,strDataSourceFormat,bIsEditable);

	//load pic (maybe some work to be done here: format, scaling, etc..)
	QPixmap pixMap;
	pixMap.loadFromData(Data.value<QByteArray>());
	setPixmap(pixMap);
}


/*!
	Called by UniversalTableWidget when widget already exist and data needs to be refreshed
	In combo box: selection is copied and set as current 
	\param value	- returns data set by SetData() (DbRecordSet), any user changes are stored in value
*/
void UniversalTablePicture::RefreshData(QVariant &Data,bool bCopyContent)
{
	QPixmap pixMap;
	pixMap.loadFromData(Data.value<QByteArray>());
	setPixmap(pixMap);
}


/*!
	Called by UniversalTableWidget for disabling/enabling user interaction with child widget
	\param bEdit	- true, control is clickable, false, control is disabled
*/
void UniversalTablePicture::SetEditMode(bool bEdit)
{
	
}



