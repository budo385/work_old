#ifndef UNIVERSALTABLEWIDGET_H
#define UNIVERSALTABLEWIDGET_H


#include <QtWidgets/QTableWidget>
#include <QContextMenuEvent>
#include <QAction>
#include "common/common/dbrecordset.h"
#include "gui_core/gui_core/dragdropinterface.h"
#include "gui_core/gui_core/guidatamanipulator.h"


//typedef QHash<QString,int> MultiColSortList;



/*!
	\class  UniversalTableWidget
	\ingroup GUICore_UniversalTable
	\brief  This class provides universal table view

	Features:
	- uses DbRecordSet as data source to display data or external GuiDataManipulator
	- supports drag&drop, single or multirow select, default cotnext menu
	- supports special widgets as chkbox, combobox, picture, etc...

	Use:
	- Initialize with datasource
	- SetColumnSetup() to setup columns (header), see DefineColumnSetupFromView() & AddColumnToSetup()
	- table can be only selectable (editr mode = false) or editable: see SetEditMode()
	- supports universal multicolumn sort wizard
	- add own widgets into cells, see CreateCellWidgets()

*/
class UniversalTableWidget : public QTableWidget, public DragDropInterface
{
	Q_OBJECT

public:


	enum ColumnType
	{
		COL_TYPE_TEXT,
		COL_TYPE_CHECKBOX,
		COL_TYPE_ICON,
		COL_TYPE_COMBOBOX,
		COL_TYPE_LIST,
		COL_TYPE_MULTILINE_TEXT,
		COL_TYPE_PICTURE,
		COL_TYPE_USER_DEFINED=20
	};


	UniversalTableWidget(QWidget * parent = 0);
	virtual ~UniversalTableWidget();

	//------------------------------------------
	//		MAIN
	//------------------------------------------

	//Initialize (data source must be provided)
	void Initialize(DbRecordSet *lstData,bool bEnableDrop=false, bool bEnableDrag=false,bool bSingleSelection=false,bool bSelectRows=true, int nVerticalRowSize=25,bool bDisableCntxMenu=false, bool bIgnoreDropHandler = false);
	void Initialize(GuiDataManipulator *pDataManipulator,bool bEnableDrop=false, bool bEnableDrag=false,bool bSingleSelection=false,bool bSelectRows=true, int nVerticalRowSize=25,bool bDisableCntxMenu=false, bool bIgnoreDropHandler = false);

	//DataSource
	DbRecordSet* GetDataSource(){return m_lstData;};
	int GetCurrentSelectedRow();

	//Edit mode (to enable drag/drop & edit cells that are editable):
	void SetEditMode(bool bEdit=true);
	bool IsEditMode(){return m_bEdit;};


	//------------------------------------------
	//		COLUMN SETUP
	//------------------------------------------
	//Column setup:
	void SetColumnSetup(DbRecordSet &lstColumnSetup);
	void GetColumnSetup(DbRecordSet &lstColumnSetup);
	static void DefineColumnSetupFromView(DbRecordSet &lstSetup,int nView, int nSkipCols=0);
	static void AddColumnToSetup(DbRecordSet &lstSetup,QString strColName,QString strHeaderName="",int nWidth=100,bool bEditable=true, QString strToolTip="",int nColType=COL_TYPE_TEXT,QString strDataFormat=QString(),int nSectionResizeMode=0);
	void SetColumnEditable(int nColumn,bool bEditable);

	//------------------------------------------
	//		DROP HANDLERS
	//------------------------------------------
	//Drop operations by widget:
	Qt::DropActions supportedDropActions() const {return DragDropInterface::supportedDropActions();};
	void dropEvent(QDropEvent *event);
	void dragEnterEvent ( QDragEnterEvent * event );
	void dragMoveEvent ( QDragMoveEvent * event);
	//our custom drop handlers:
	virtual void GetDropValue(DbRecordSet &values, bool bSelectItemChildren = false) const;
	virtual void DropHandler(QModelIndex index, int nDropType, DbRecordSet &DroppedValue,QDropEvent *event);


	//------------------------------------------
	//		CNTX MENU
	//------------------------------------------
	void SetContextMenuActions(QList<QAction*>& lstActions){m_lstActions=lstActions;};		//set actions for cnxt menu (alternative approach for setting context menu)
	QList<QAction*>& GetContextMenuActions(){ return m_lstActions; };


	//------------------------------------------
	//		MISC
	//------------------------------------------
	void ClearAllBoldFonts();
	void SetRowBold(int nRow); //WARNING: call blocksignals before calling this
	
	virtual void GetRowIcon(int nRow,QIcon &RowIcon,QString &strStatusTip){};	//get icon to display on vertical header for specified row, override
	void SetWidgetCopyContentRefresh(bool bEnable){m_bWidgetCopyContentRefresh=bEnable;};
	virtual QWidget* CreateCellWidgets(int nRow,int nCol,int nColType);
	void scrollToLastItem(bool bSelectLastRow=false);	//scroll to last item in table (use after add)
	void scrollToFirstItem(bool bSelectFirstRow=false);	//scroll to last item in table (use after add)


signals:
	void SignalRefreshBoldRows();
	void SignalSelectionChanged();
	void SignalDataChanged();
	void DataDroped(QModelIndex index, int nDropType, DbRecordSet &DroppedValue, QDropEvent *event);

protected:
    void contextMenuEvent(QContextMenuEvent *event);
	virtual void CreateContextMenuActions(QList<QAction*>& lstActions);								//call this to create default cnxt menu or override
	virtual QMimeData * mimeData ( const QList<QTableWidgetItem *> items ) const;
	virtual QStringList mimeTypes () const;
	virtual void TableStateChanged(){};
	virtual void SetStyleSheetGlobal();

public slots:

	virtual void SelectionChanged();
	virtual void SortColumn(int);
	
	void SortMultiColumn(DbRecordSet lstCols/* in MVIEW_GRID_COLUMN_SETUP */);

	//Refresh display: check col/rows, if no match add, else copy values-> to items
	virtual void RefreshDisplay(bool bApplyLastSortModel=false);
	void RefreshDisplayRow(int nRow); //faster for refreshing only one row
	void RefreshSelection(); 	//Selections (if only selection is changed call this, faster then RefreshDisplay):


	//Called from Context menu:
	virtual void InsertRow();
	void DeleteSelection();
	void DeleteTable();
	void SelectAll();
	void DeselectAll();
	void DuplicateSelection();
	void SortDialog();

	//widget operations:
	virtual void CellWidgetChanged(int nRow,int nCol);



protected slots:
	virtual void CellEdited(int nRow, int nCol);
	virtual void CellClicked(int nRow, int nCol);
	virtual void CustomTableCellClicked(int nRow, int nCol);
	



	
protected:

	void RefreshRowContentFast(int nRow=-1);
	void RestoreHeaderSetup();

	virtual void startDrag(Qt::DropActions supportedActions);

	DbRecordSet m_lstColumnSetup;
	DbRecordSet *m_lstData;

	GuiDataManipulator *m_pData;
	bool m_bExternalDataManipulator;

	QHash<int,int> m_lstColumnMapping;
	bool m_bEdit;
	bool m_bIgnoreDropHandler;
	int m_nVerticalRowSize;
	QList<QAction*> m_lstActions;

	bool m_bWidgetCopyContentRefresh;		//when true, then RefreshDisplay will rebuild itself from scratch, default is only selection copy
	SortDataList m_lstLastSort;				//when sorted by user click, store last
	bool m_bSelectionChangeEmitted;
	bool m_bGridContainsBoldItems;

};


#endif //UNIVERSALTABLEWIDGET_H



