#ifndef RICHEDITWIDGET_H
#define RICHEDITWIDGET_H

#include <QtWidgets/QFrame>
#include <QTextEdit>
#include <QLineEdit>
class QMenuBar;
class QToolBar;
class QVBoxLayout;
class QHBoxLayout;
class QActionGroup;
class QTextEdit;
class QTextBrowser;
class QLineEdit;
class QComboBox;
class QTextCharFormat;
class QPushButton;
class QToolButton;
class QButtonGroup;
#include <QTextBrowser>
#include "htmlhighlighter.h"

#ifndef WINCE
#include "richeditorsearchdialog.h"
#endif

class RichEditTextView : public QTextBrowser
{
protected:
	virtual bool event (QEvent * event);

public:
	QAction	*m_pPasteAction;
};

class RichEditWidget : public QFrame
{
    Q_OBJECT

public:
    RichEditWidget(bool bShowOpenInDialogButton = true, QWidget *parent = 0);
    ~RichEditWidget();

	void		SetEmbeddedPixMode(bool bOn = true){ m_bEmbedPix = bOn; };
	void		SetHtml(QString htmlString);
	QString		GetHtml(bool bSkipPixEmbed = false);

	void		SetPlainText(QString strPlainText);
	void		SetDocument(QTextDocument *pTextDocument);
	QString		GetPlainText();
//	QTextEdit	*GetTextWidget(){return m_pTextEdit;}; //for Data connection
	QTextBrowser *GetTextWidget(){return m_pTextEdit;}; //for Data connection
	void		SetupPrintingHeader(QStringList lstPrintingHeaderData);
	bool		IsHTMLDisplayed();
	void		SwitchFromHTMLToNormalView();
	void		ShowVerticalStretchButton(bool bShow);
	void		SetInverseVerticalStretchButton();

	void		PastePictureAsJpeg(QImage &image);
	static void HtmlFormatBeautifier(QString &strData);
	static void PostprocessFixRtfEmail(QString &strEmail);
	static void PreprocessFixRtfEmail(QString &strEmail);
	void		RefreshHtmlHighlight();
	void		SetSmallBoolean(bool bSmall); //dirty just to satisfy Herzog layout changes.

	void		EnableHtmlFixing(bool bEnable){ m_bFixHTMLContents = bEnable; };
	bool		IsHtmlFixingEnabled(){	return m_bFixHTMLContents; }
	QToolButton *GetShowHtmlButton(){	return m_pShowHTMLSourceButton; };

	void		SaveToPdf(QString strFileName);

	QActionGroup		*m_pActionGroup;
	QAction				*m_pActionOpenFile,
						*m_pActionSaveFile,
						*m_pActionExportToHtml,
						*m_pActionExportToPdf,
						*m_pActionExportToOdf,
						*m_pActionSaveFileAs,
						*m_pActionTextBold,
						*m_pActionTextUnderline,
						*m_pActionTextItalic,
						*m_pActionTextColor,
						*m_pActionAlignLeft,
						*m_pActionAlignCenter,
						*m_pActionAlignRight,
						*m_pActionAlignJustify,
						*m_pActionUndo,
						*m_pActionRedo,
						*m_pActionCut,
						*m_pActionCopy,
						*m_pActionPaste,
						*m_pActionOpenInDialog, 
						*m_pActionPrintFile,
						*m_pActionInsertPicture,
						*m_pActionShowHTMLSource,
						*m_pActionVerticalOpenClose,
						*m_pActionSearchButton,
						*m_pActionSearchExtendedButton,
						*m_pActionFindPreviousButton,
						*m_pActionStrikeThrough;

	bool	m_bEmitSignal;
	bool	m_bTextIsHTML;
	bool	m_bHTMLIsDisplayed;

public slots:
	void SetEditMode(bool bEditOnly);
	void OnTextChanged();
	void InsertPicture();

protected:
	void contextMenuEvent(QContextMenuEvent *event);
	void focusInEvent(QFocusEvent * event);
	//void keyPressEvent(QKeyEvent *event);

private slots:
	void OnOpenFile();
	void OnSaveFile(QString strFormat);
	void OnPrintFile();
	void OnSaveAsFile(QString strFormat);
	void focusSlot();
	void textBold();
	void textUnderline();
	void textItalic();
	void textFamily(const QString &f);
	void textSize(const QString &p);
	void textStyle(int styleIndex);
	void textColor();
	void textAlign(QAction *a);
	void textAlignLeft();
	void textAlignCenter();
	void textAlignRight();
	void textAlignJustify();
	void clipboardDataChanged();
	void OpenInDialog();
	void cursorPositionChanged();
	void currentCharFormatChanged(const QTextCharFormat &format);
	void on_textChanged();
	void on_ShowHTMLSource();
	void on_VerticalOpenClose(bool bEmitAndSetLocalVariable = true);
	void on_SearchButton();
	void on_SearchExtendenButton();
	void on_findPreviousButton();
	void on_exportToHtmlButton();
	void on_exportToPdfButton();
	void on_exportToOdfButton();
	void on_paste();
	void on_StrikeThrough();
	
public slots:
	void SearchText(QString strText, QTextDocument::FindFlags findFlags, bool bUseRegex);

private:
	void SetupDialog();
	void SetupDialog_WINCE();
	void SetupButtons();
	void setupActions();
	void setupCombos();
	void fontChanged(const QFont &f);
	void colorChanged(const QColor &c);
	void alignmentChanged(Qt::Alignment a);	
	void connectDocumnetToSignals(QTextDocument	*pTextDocument);
	void disconnectDocumnetFromSignals();
	void AddJournalPrintingHeader();
	void AddNotePrintingHeader();
	void AddPhoneCallPrintingHeader();
	void SaveDoc(QString strFormat);
	
	bool				m_bEmbedPix;
	QStringList			m_lstTempPixFiles;

	bool				m_bEditOnly;
	bool				m_bShowOpenInDialogButton;
	bool				m_bSmaller;
	bool				m_bInverseStretchArrow;
	QTextDocument::FindFlags m_nFindFlags;
	QString				m_strHtmlText;
	QString				m_strFileName;
	QVBoxLayout			*m_pMainLayout;
	QButtonGroup		*m_pButtonGroup;
	QToolButton			*m_pOpenFileButton,
						//*m_pSaveFileButton,
						*m_pExportToHtmlButton,
						*m_pExportToPdfButton,
						*m_pExportToOdfButton,
						*m_pTextBoldButton,
						*m_pTextUnderlineButton,
						*m_pTextItalicButton,
						*m_pTextColorButton,
						*m_pAlignLeftButton,
						*m_pAlignCenterButton,
						*m_pAlignRightButton,
						*m_pAlignJustifyButton,
						*m_pUndoButton,
						*m_pRedoButton,
						*m_pCutButton,
						*m_pCopyButton,
						*m_pPasteButton,
						*m_pOpenInDialog,
						*m_pPrintButton,
						*m_pInsertPictureButton,
						*m_pShowHTMLSourceButton,
						*m_pVerticalOpenClose,
						*m_pSearchButton,
						*m_pSearchExtendedButton,
						*m_pFindPreviousButton,
						*m_pStrikeThroughButton;
//	QTextEdit			*m_pTextEdit;
	QTextBrowser		*m_pTextEdit;
	QTextDocument		*m_pTextDocument;
	QComboBox			*m_pComboStyle,
						*m_pComboFont,
						*m_pComboSize;
	QLineEdit			*m_pFindLineEdit;
	QStringList			m_lstPrintingHeaderData;
#ifndef WINCE
	RichEditorSearchDialog *m_pSearchDialog;
#endif
	HtmlHighlighter		*m_pHtmlHighlighter;

	bool				m_bInsideHTMLChange;
	bool				m_bFixHTMLContents;

signals:
	void				on_PrintIcon_clicked();
	void				textChanged();
	void				on_VerticalOpenClose_clicked(bool bSmaller);
	void				EditViewSwitched();	//RTF to "raw HTML" and back
};

#endif // RICHEDITWIDGET_H
