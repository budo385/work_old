#ifndef DATESELECTOR_H
#define DATESELECTOR_H

#include <QWidget>
#include <QButtonGroup>
#include "generatedfiles/ui_dateselector.h"

class DateSelector : public QWidget
{
	Q_OBJECT

public:
	DateSelector(QWidget *parent = 0);
	~DateSelector();

	QDate dateFrom();
	QDate dateTo();
	void setDate(QDate date);
	int getFilter();

private:
	Ui::DateSelectorClass ui;
	QButtonGroup *m_pButtonGroup;

private slots:
	void ToDateEnable(bool bEnabled);
	void on_EmitDateChanged(QDate date);
	void on_EmitToDateChanged(QDate date);
	void on_FilterChanged(int id);
	
	void on_btnPrevMonthEmailLog_clicked();
	void on_btnPrevWeekEmailLog_clicked();
	void on_btnPrevDayEmailLog_clicked();
	void on_btnNextDayEmailLog_clicked();
	void on_btnNextWeekEmailLog_clicked();
	void on_btnNextMonthEmailLog_clicked();

	void on_btnPrevMonthEmailLog_2_clicked();
	void on_btnPrevWeekEmailLog_2_clicked();
	void on_btnPrevDayEmailLog_2_clicked();
	void on_btnNextDayEmailLog_2_clicked();
	void on_btnNextWeekEmailLog_2_clicked();
	void on_btnNextMonthEmailLog_2_clicked();

signals:
	void EmitDateChanged(QDate);
	void EmitToDateChanged(QDate);
	void FilterChanged(int);
};

#endif // DATESELECTOR_H
