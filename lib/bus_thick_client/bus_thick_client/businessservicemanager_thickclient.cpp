//#ifndef SP_NO_THICK_CLIENT

#include "businessservicemanager_thickclient.h"
#include "usersessionmanager_client.h"
#include "bus_client/bus_client/messagedispatcher_client.h"
#include "db/db/dbconnectionsetup.h"
#include "common/common/config.h"
#include <QFileDialog>
#include "common/common/datahelper.h"
#include "common/common/loggerabstract.h"

//THICK global objects:
#include "bus_core/bus_core/licenseaccessrights.h"
#include "bus_server/bus_server/systemserviceset.h"
#include "bus_server/bus_server/privateserviceset.h"
#include "bus_core/bus_core/usersessionmanager.h"
#include "bus_server/bus_server/loadbalancer.h"
#include "bus_core/bus_core/messagedispatcher.h"
#include "bus_core/bus_core/servercontrolabstract.h"
#include "bus_server/bus_server/businessserviceset.h"

BusinessServiceSet*		g_BusinessServiceSet;
PrivateServiceSet*		g_PrivateServiceSet;
SystemServiceSet*		g_SystemServiceSet; 
LoadBalancer*			g_LoadBalancer;
UserSessionManager*		g_UserSessionManager;
MessageDispatcher*		g_MessageDispatcher;
LicenseAccessRights		g_ModuleLicense;
ServerControlAbstract*	g_AppServer=NULL;	//fake app server interface on client->to correctly link service_servercontrol

extern QString g_strLastDir_FileOpen;


BusinessServiceManager_ThickClient::BusinessServiceManager_ThickClient(QObject *parent)
	: QObject(parent)
{

	m_BcpManager=NULL;
	ClearPointers();
}

BusinessServiceManager_ThickClient::~BusinessServiceManager_ThickClient()
{
	Status err;
	StopService(err);
	Q_ASSERT(err.IsOK()); //stop failed
}





void BusinessServiceManager_ThickClient::ClearPointers()
{
	g_PrivateServiceSet=NULL;
	g_BusinessServiceSet=NULL;
	g_UserSessionManager=NULL;
	g_SystemServiceSet=NULL;
	m_ServerSessionManager=NULL;
	m_SrvSessionBkgTask=NULL;
	g_MessageDispatcher=NULL;
	g_LoadBalancer=NULL;  //not used in thick mode!!
	app=NULL;

}



//stop database session
void BusinessServiceManager_ThickClient::StopService(Status &pStatus, bool bFatalErrorDetected)
{
	if(!m_bServiceStarted)return;

	if (m_BcpManager)
	{
		m_BcpManager->StopThickModeTimer();
	}

	//stop bckg tasks:
	if(m_SrvSessionBkgTask!=NULL) 
	{
		m_SrvSessionBkgTask->Stop();
		delete m_SrvSessionBkgTask;
	}
	//stop session:
	if(m_ServerSessionManager!=NULL)
	{
		m_ServerSessionManager->StopSession(pStatus);
		delete m_ServerSessionManager;
	}

	//destroy objects:
	if(g_PrivateServiceSet!=NULL)	delete g_PrivateServiceSet;
	if(g_BusinessServiceSet!=NULL)	delete g_BusinessServiceSet;
	if(g_SystemServiceSet!=NULL)	delete g_SystemServiceSet;
	if(g_UserSessionManager!=NULL)	delete g_UserSessionManager;
	if(g_MessageDispatcher!=NULL)	delete g_MessageDispatcher;
	
	m_DbManager.ShutDown();
	m_bServiceStarted=false;
	ClearPointers();
}




void BusinessServiceManager_ThickClient::StartService(Status &pStatus)
{
	//if started stop it
	if(m_bServiceStarted) 
	{
		StopService(pStatus);
		if(!pStatus.IsOK()) return;
	}

	//issue 1782:
	//if portable then autoreplace drive letter with current drive:
	if (DataHelper::IsPortableVersion() && m_DbConnSettings.m_strDbType==DBTYPE_FIREBIRD)
	{
		QString strDbPath=m_DbConnSettings.m_strDbName;
		strDbPath=DataHelper::ReplaceCurrentDriveInPath(strDbPath);
		if (QFile::exists(strDbPath))
			m_DbConnSettings.m_strDbName=strDbPath;
	}


	//----------------------------------------------
	//init Database
	//----------------------------------------------
	m_DbManager.Initialize(pStatus,m_DbConnSettings,5);  //only 5 connection for embedded
	if(!pStatus.IsOK()) return;
	//----------------------------------------------
	//Core services
	//----------------------------------------------
	int nUniqueLicenseID=g_ModuleLicense.GetUniqueLicenseID();
	//----------------------------------------------
	//Sys services
	//----------------------------------------------
	g_SystemServiceSet= new SystemServiceSet(&m_DbManager);
	//----------------------------------------------
	//start App. server session (automatic garbage cleaning) in separate thread:
	//----------------------------------------------
	m_ServerSessionManager=new ServerSessionManager(g_SystemServiceSet);
	g_MessageDispatcher=new MessageDispatcher_Client();
	//----------------------------------------------
	//Backup manager initialize:
	//----------------------------------------------
	if (m_BcpManager)
	{
		m_BcpManager->Initialize(pStatus,m_DbManager.GetDbSettings(),true,false);
		if (!pStatus.IsOK()) return;

		m_BcpManager->StartUpCheck(pStatus);
		if (!pStatus.IsOK()) return;
	}


	QString IPAdrr="0.0.0.0";
	int nIsLBO=0;
	int nIsMaster=0;
	int nAppSrvSessionID=m_ServerSessionManager->StartSession(pStatus,IPAdrr,nUniqueLicenseID,SRV_ROLE_THICK,1,nIsLBO,nIsMaster,"","",m_strSerial);
	if(!pStatus.IsOK())return;
	m_SrvSessionBkgTask=new BackgroundTaskExecutor(m_ServerSessionManager,NULL,APP_SESSION_REPORT_INTERVAL*1000);
	m_SrvSessionBkgTask->Start();
	//----------------------------------------------
	//UserSession manager (heart of system):
	//----------------------------------------------
	g_UserSessionManager =new UserSessionManager_Client(g_SystemServiceSet,&g_ModuleLicense,nAppSrvSessionID);
	//----------------------------------------------
	//Launch Business & Private sets/init pointers:
	//----------------------------------------------
	g_PrivateServiceSet = new PrivateServiceSet(&m_DbManager,g_UserSessionManager);
	g_BusinessServiceSet = new BusinessServiceSet(&m_DbManager,g_UserSessionManager);
	//main app:
	app=g_BusinessServiceSet;
	m_bServiceStarted=true;
}

//true if new installation: new database is created!
bool BusinessServiceManager_ThickClient::InitializeThickClient(Status &pStatus, QString &strLicensePath,QString &strConnectionName,QString strSerialNo)
{
	m_strSerial=strSerialNo;
	
	//LICNSE CHECK
	//------------------------------------------
	QFileInfo info(strLicensePath);
	if (!info.exists())
	{
		//ask for path:
		strLicensePath=GetKeyFileDlg();
		if (strLicensePath.isEmpty())
		{
			pStatus.setError(1,tr("License file is missing or corrupted!"));
			return false;
		}
	}
	bool bOK=g_ModuleLicense.LoadLicense(strLicensePath);
	if(!bOK)
	{
		pStatus.setError(1,tr("License file is missing or corrupted!"));
		return false;
	}

	//DATABASE CHECK
	//------------------------------------------
	return DbConnectionSetup::SetupThickDatabase(pStatus,strConnectionName);
}


void BusinessServiceManager_ThickClient::InitializeBackupManager(Status &pStatus,ClientBackupManager *pBcpManager, int nBackupFreq, QString strBackupPath, QString datBackupLastDate, QString strRestoreOnNextStart,int nBackupDay,QString strBackupTime, int nDeleteOldBackupsDay)
{
	m_BcpManager=pBcpManager;
	m_BcpManager->SetClientMode(false);
	Status err;
	if (DataHelper::IsPortableVersion()) //issue 1782, if portable & PE test for keyfile
	{
		QString strNewPath=DataHelper::ReplaceCurrentDriveInPath(strBackupPath);
		if (QFile::exists(strNewPath))
			strBackupPath=strNewPath;
	}
	m_BcpManager->SetBackupData(err,nBackupFreq,strBackupPath,datBackupLastDate,strRestoreOnNextStart,nBackupDay,strBackupTime,nDeleteOldBackupsDay);

	connect(pBcpManager,SIGNAL(Thick_ClearDatabaseAfterRestore()),this,SLOT(OnThick_ClearDatabaseAfterRestore()));
	connect(pBcpManager,SIGNAL(Thick_StartDatabase()),this,SLOT(OnThick_StartDatabase()));
	connect(pBcpManager,SIGNAL(Thick_CloseDatabase()),this,SLOT(OnThick_CloseDatabase()));
}


QString BusinessServiceManager_ThickClient::GetKeyFileDlg()
{
	QString strStartDir=QDir::currentPath(); 
	QString strFilter="*.key";

	//open file dialog:
	if (g_strLastDir_FileOpen.isEmpty())
		g_strLastDir_FileOpen=strStartDir;
	else
		strStartDir=g_strLastDir_FileOpen;
	QString strFile = QFileDialog::getOpenFileName(
		NULL,
		tr("Select KeyFile"),
		strStartDir,
		strFilter);
	if (!strFile.isEmpty())
	{
		QFileInfo fileInfo(strFile);
		g_strLastDir_FileOpen=fileInfo.absolutePath();
	}
	return strFile;
}



void BusinessServiceManager_ThickClient::Thick_Initialize(Status &pStatus,ClientIniFile *pINI, ClientBackupManager *pBcpManager)
{

	QString strLicensePath=pINI->m_strKeyFilePath;
	if (strLicensePath.isEmpty())
		strLicensePath= DataHelper::GetApplicationHomeDir()+"/settings/sokrates.key";

	if (DataHelper::IsPortableVersion()) //issue 1782, if portable & PE test for keyfile
	{
		QString strNewPath=DataHelper::ReplaceCurrentDriveInPath(strLicensePath);
		if (QFile::exists(strNewPath))
			strLicensePath=strNewPath;
	}

	QString strConnectionName;
	bool bDbCreated=InitializeThickClient(pStatus,strLicensePath,strConnectionName);
	if (!pStatus.IsOK()) return;
	if (!strLicensePath.isEmpty())pINI->m_strKeyFilePath=strLicensePath;
	if (bDbCreated)
	{
		pINI->m_strConnectionName=strConnectionName;
		pINI->m_nHideLogin=1; //hide login on newDB
	}

	pINI->Save();
	if (pBcpManager)
	{
		InitializeBackupManager(pStatus,pBcpManager,pINI->m_nBackupFreq,pINI->m_strBackupPath,pINI->m_datBackupLastDate,pINI->m_strRestoreOnNextStart,pINI->m_nBackupDay,pINI->m_strBackupTime,pINI->m_nDeleteOldBackupsDay);
	}

}

void BusinessServiceManager_ThickClient::OnThick_ClearDatabaseAfterRestore()
{
	Status err;
	g_SystemServiceSet->AppSrvSession->CleanAllSessions(err);	
	if (!err.IsOK())
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,err.getErrorCode(),err.getErrorTextRaw());
}
void BusinessServiceManager_ThickClient::OnThick_StartDatabase()
{
	m_DbManager.EnablePool();

}
void BusinessServiceManager_ThickClient::OnThick_CloseDatabase()
{
	m_DbManager.ShutDown();

}

//#endif //SP_NO_THICK_CLIENT