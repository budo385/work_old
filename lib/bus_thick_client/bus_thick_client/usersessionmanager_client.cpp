#ifndef SP_NO_THICK_CLIENT
#include "usersessionmanager_client.h"

/*!
	Constructor: sets init parameters
	\param pServices			system services pointer
	\param nAppServerSessionID  app server or thick client session ID (int)
	\param nAuthTries			defines in how many tries user must login
*/
UserSessionManager_Client::UserSessionManager_Client(SystemServiceSet* pServices,LicenseAccessRightsBase *pModuleLicenseManager,int nAppServerSessionID,int nAuthTries)
:UserSessionManager(pModuleLicenseManager)
{
	m_nAuthTries=nAuthTries;
	Services=pServices;
	m_nAppServerSessionID=nAppServerSessionID;

	//recheck app. server entry
}



//------------------------------------------------------------------------------------------------------
//							CREATE, UPDATE,VALIDATE SESSION METHODS
//------------------------------------------------------------------------------------------------------


/*!
	Creates new system wide unique session id. Inserts in database with app.server_id.
	Called by Login method after successfull Login process!
	Stores in dynamic session list in memory with provided user info (id, group, module, etc..)
	If entry does exists in session list: update status = set OK.

	\param pStatus				if err occur, returned here
	\param strSession			returned session id, if all goes well
	\param rowUserData			row in TVIEW_CORE_USER_SELECTION with user data
	\param strModuleCode		client module code
	\param strMLIID				MLIID

*/
void UserSessionManager_Client::CreateSession(Status &pStatus,QString &strSession,DbRecordSet &rowUserData, QString strModuleCode, QString strMLIID,bool bIsSystemThread,QString strParentSession)
{

	Sha256Hash Hasher;
	//call Businessobject for creating unique session
	//------------------------------------------------
	int nTries=10;
	int nStoredSessionID;
	pStatus.setError(0);

	//extract user and person id if exists:
	int nUserID=rowUserData.getDataRef(0,"CUSR_ID").toInt();
	int nPersonID=rowUserData.getDataRef(0,"CUSR_PERSON_ID").toInt();

	//get max license count:
	int nMaxCount=-1;
	if (m_pModuleLicense)
		nMaxCount=m_pModuleLicense->GetMaxLicenseCount(strModuleCode,strMLIID);
	if(nMaxCount==-1)
	{
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_MOD_LICENSE_NOT_EXISTS); 
		return;
	}

	while(nTries>0)
	{
		//create session as thread_id+userid+appserid+datetime:
		QByteArray byteRandomString=QTime::currentTime().toString("hh:mm:ss.zzz").toLatin1()+ QVariant(nUserID).toString().toLatin1()+QVariant(m_nAppServerSessionID).toString().toLatin1();
		strSession=Hasher.GetHash(byteRandomString).toBase64(); //32byte length hashed session
		
		//try to insert system wide unique session, if fail iterate again
		//Unique constraint on SESSION Db field assures that no 2 same user session exists in system
		Services->UserSession->InsertSession(pStatus,strSession,m_nAppServerSessionID,nMaxCount,strModuleCode,strMLIID,nUserID,nStoredSessionID);
		if(pStatus.IsOK()) break;
		nTries--;
	}
	//means that in 10 tries we didn't succeed in creating unique Session ID, whata bad luck!
	if(nTries==0)
	{
		//pStatus.setErrorDetails(pStatus.getErrorText());//store DB error in details:
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_SESSION_CREATION_FAILED); 
		return;
	}


	//store all 
	//------------------------------------------------

	m_SessionData.bStored=true;
	m_SessionData.nStoredSessionID=nStoredSessionID;
	m_SessionData.strSession=strSession;
	m_SessionData.nUserID=nUserID;
	m_SessionData.nPersonID=nPersonID;
	m_SessionData.strModuleCode=strModuleCode;
	m_SessionData.strMLIID=strMLIID;
	m_SessionData.rowUserData=rowUserData;
	m_SessionData.bIsSystemThread=bIsSystemThread;

}




/*!
	Delete session from session list & DB
	\param pStatus	   - error if exists
	\param strSession  - expired session
*/
void UserSessionManager_Client::DeleteSession(Status &pStatus, QString strSession)
{

	Q_ASSERT_X(m_SessionData.strSession==strSession,"Session delete","Trying to delete non existent sessions");

	//delete from DB:
	QList<int> lstSessionsForDelete;
	lstSessionsForDelete.append(m_SessionData.nStoredSessionID);
	Services->UserSession->DeleteSession(pStatus,m_nAppServerSessionID,lstSessionsForDelete);

}


void UserSessionManager_Client::DeleteSessionByThreadID(Status &pStatus,int nThreadID)		
{
	DeleteSession(pStatus,m_SessionData.strSession);

}

int UserSessionManager_Client::GetContactID(QString strSession)
{
	DbRecordSet row;
	GetUserData(row);

	if (row.getRowCount()>0)
	{
		return row.getDataRef(0,"BPER_CONTACT_ID").toInt();
	}

	return 0;
}

#endif //SP_NO_THICK_CLIENT

