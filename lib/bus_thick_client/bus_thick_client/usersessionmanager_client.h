#ifndef USERSESSIONMANAGER_THICK_H
#define USERSESSIONMANAGER_THICK_H

#ifndef SP_NO_THICK_CLIENT

#include <QMutex>
#include <QThread>
#include "bus_core/bus_core/usersessionmanager.h"
#include "common/common/sha256hash.h"
#include "common/common/serverthread.h"
#include "bus_server/bus_server/systemserviceset.h"
#include "trans/trans/httpcontext.h"



/*!
    \class UserSessionManager_Client
    \brief Used for managing user sessions in thick mode
    \ingroup Bus_Server

	Caches user session info. Because user sessions are dependent on app. server entry it will create dummy entry just to simulate. 

*/
class UserSessionManager_Client : public UserSessionManager
{

public:

	UserSessionManager_Client(SystemServiceSet* pServices,LicenseAccessRightsBase *pModuleLicenseManager,int nAppServerSessionID,int nAuthTries=10);
	~UserSessionManager_Client(){};


	/*! SessionState defines state of session */
	enum SessionState {
		STATE_OK=0,				///<Session is alive and authenticated, reset counters
		STATE_LOGIN,			///<Session is empty, login in process, set counter=0, set lastActivity or track both
		STATE_REAUTHENTICATE,	///<Session was given order to reauthenticate, set counter=0, set lastActivity or track both
		STATE_EXPIRED			///<Session expired
		};			


	//managing functions
	void CreateSession(Status &pStatus,QString &strSession,DbRecordSet &rowUserData, QString strModuleCode, QString strMLIID,bool bIsSystemThread=false,QString strParentSession="");
	void CreateSessionForWebService(Status &pStatus,QString &strSession,DbRecordSet rowUserData){};
	void DeleteSession(Status &pStatus, QString strSessionL);
	void DeleteSessionByThreadID(Status &pStatus,int nThreadID);
	

	//context helper functions
	QString GetSessionID(){return m_SessionData.strSession;}
	int GetUserID(QString strSession=""){return m_SessionData.nUserID;}
	int GetSessionRecordID(QString strSession=""){return m_SessionData.nStoredSessionID;}
	int GetPersonID(QString strSession=""){return m_SessionData.nPersonID;}
	int GetContactID(QString strSession="");
	bool IsSystemUser(QString strSession=""){return (bool)m_SessionData.rowUserData.getDataRef(0,"CUSR_IS_SYSTEM").toInt();};
	void GetUserData(DbRecordSet &rowUserData,QString strSession=""){rowUserData=m_SessionData.rowUserData;}
	void GetModuleInfo(QString& strModuleCode, QString& strMLIID){strModuleCode=m_SessionData.strModuleCode;strMLIID=m_SessionData.strMLIID;}

	//get session count:
	int GetSessionCount(){return 1;}
	
	//dummy for THICK:
	int GetSocketID(QString strSession=""){return 0;};
	QString GetClientIP(QString strSession=""){return "";};
	QStringList GetSocketListForPersonID(int nPersonID){QStringList x; return x;};
	void ClearUserSessionStorage(QString strSession=""){};

private:
	
	//Empty funct for THICK MODE: (make them private..probably will crush all when called thriugh virtual base classp pointer 
	//but they should not be even referenced in thick mode
	bool AuthenticateUser(QString strCookieValue,QString &strSessionID){return false;};
	void UpdateSession(Status &pStatus,QString strSession,DbRecordSet &rowUserData, QString strModuleCode, QString strMLIID){};
	void ValidateUserRequest(Status &pStatus,QString strSession,QString strClientIP="",bool IsLogin=false,int nSocketID=0){};
	void ValidateWebUserRequest(Status &pStatus,QString strSession,const HTTPContext &ctx){};
	void DeleteExpiredUserSessions(Status &pStatus,UserSessionList &pListExpiredSessions){};
	void DeleteOrphanUserSessions(Status &err,UserSessionList lstToDelete){};
	void GetSessionList(UserSessionList &pListSessions){}; 
	void InactiveSession(Status &pStatus,int nThreadID){};		//empty for THICK
	void LockThreadActive(){};
	void UnLockThreadActive(){};
	void ClientIsAlive(int nSocketID){};
	void TestExpiredUserSessions(Status &err,UserSessionList &pListExpiredSessions){};
	
	//members:
	UserSessionData m_SessionData;			///<  single user data 
	SystemServiceSet* Services;
	int m_nAuthTries;
	int m_nAppServerSessionID;

};

#endif //SP_NO_THICK_CLIENT
#endif //USERSESSIONMANAGER_THICK_H



