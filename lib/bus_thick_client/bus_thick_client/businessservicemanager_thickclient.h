#ifndef BUSINESSSERVICEMANAGER_THICKCLIENT
#define BUSINESSSERVICEMANAGER_THICKCLIENT

#include <QObject>
#include "bus_interface/bus_interface/businessservicemanager.h"
//#include "db_core/db_core/dbconnsettings.h"
#include "common/common/status.h"

//#ifndef SP_NO_THICK_CLIENT

#include "common/common/backgroundtaskexecutor.h"
#include "db/db/dbsqlmanager.h"
#include "bus_server/bus_server/serversessionmanager.h"
#include "bus_client/bus_client/clientbackupmanager.h"
#include "bus_client/bus_client/clientinifile.h"

	/*!
		\class BusinessServiceSet_ThickClient
		\brief Holds RPC Business Set for accessing Db directly
		\ingroup Bus_Client

		Use:
		1. InitializeThickClient	-> loads license file and creates empty database if not exists
		2. InitializeBackupManager	-> initilaize bcp manager in thick mode	(skip if no bcp manager -> can run without bcp manager)
		3. SetSettings
		4. StartService (connection is not made until first db call)
		5. StopService (to disconnect or when new connection settings are defined)
	*/
class BusinessServiceManager_ThickClient : public QObject, public BusinessServiceManager
{
	Q_OBJECT

public:
	BusinessServiceManager_ThickClient(QObject *parent=NULL);
	~BusinessServiceManager_ThickClient();

	void StartService(Status &pStatus);
	void StopService(Status &pStatus, bool bFatalErrorDetected=false);
	void AbortDataTransfer(){};
	void SetSettings(DbConnectionSettings& DbConnSettings){m_DbConnSettings=DbConnSettings;};

	void Thick_Initialize(Status &pStatus,ClientIniFile *pINI, ClientBackupManager *pBcpManager=NULL);
	
private slots:
	void OnThick_ClearDatabaseAfterRestore();
	void OnThick_StartDatabase();
	void OnThick_CloseDatabase();

private:
	bool			InitializeThickClient(Status &pStatus,QString &strLicensePath,QString &strConnectionName,QString strSerialNo="");
	QString			GetKeyFileDlg();
	DbSqlManager	*GetThickDbManager(){return &m_DbManager;};
	void			InitializeBackupManager(Status &pStatus,ClientBackupManager *pBcpManager, int nBackupFreq, QString strBackupPath, QString datBackupLastDate, QString strRestoreOnNextStart,int nBackupDay,QString strBackupTime, int nDeleteOldBackupsDay);
	void			ClearPointers();

	DbConnectionSettings	m_DbConnSettings;
	DbSqlManager			m_DbManager;
	ServerSessionManager	*m_ServerSessionManager;
	BackgroundTaskExecutor	*m_SrvSessionBkgTask;
	ClientBackupManager		*m_BcpManager;
	QString					m_strSerial;
		
};

#endif // BUSINESSSERVICEMANAGER_THICKCLIENT
