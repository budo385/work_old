TEMPLATE	= lib
CONFIG		= qt warn_on release staticlib QtSql
HEADERS		= db/accessright.h \
		  db/accessrightset.h \
		  db/accessrightsid.h \
		  db/accessrightsorganizer.h \
		  db/db_drivers.h \
		  db/db_include.h \
		  db/dbautoidmanager.h \
		  db/dbautoidmanager_oracle.h \
		  db/dbconnectionslist.h \
		  db/dbconnectionspool.h \
		  db/dbconnsettings.h \
		  db/dbcurrenttime.h \
		  db/dbcurrenttime_oracle.h \
		  db/dbdatatypeconvertor.h \
		  db/dbdatatypeconvertormysql.h \
		  db/dbdirectlocker.h \
		  db/dbdriverloader.h \
		  db/dbrecordlocker.h \
		  db/dbsettings.h \
		  db/dbsimpleorm.h \
		  db/dbsqlmanager.h \
		  db/dbsqlquery.h \
		  db/dbtransactionhandler.h \
		  db/dbtransactionhandler_ms_sql.h \
		  db/dbtransactionhandler_mysql.h \
		  db/dbtransactionhandler_oracle.h \
		  db/dbtransactionhandler_postgresql.h
SOURCES		= db/accessright.cpp \
		  db/accessrightset.cpp \
		  db/accessrightsorganizer.cpp \
		  db/dbautoidmanager_oracle.cpp \
		  db/dbconnectionslist.cpp \
		  db/dbconnectionspool.cpp \
		  db/dbconnsettings.cpp \
		  db/dbcurrenttime.cpp \
		  db/dbcurrenttime_oracle.cpp \
		  db/dbdatatypeconvertor.cpp \
		  db/dbdatatypeconvertormysql.cpp \
		  db/dbdirectlocker.cpp \
		  db/dbdriverloader.cpp \
		  db/dbrecordlocker.cpp \
		  db/dbsettings.cpp \
		  db/dbsimpleorm.cpp \
		  db/dbsqlmanager.cpp \
		  db/dbsqlquery.cpp \
		  db/dbtransactionhandler.cpp \
		  db/dbtransactionhandler_ms_sql.cpp \
		  db/dbtransactionhandler_oracle.cpp
INTERFACES	= 
TARGET		= db
