#ifndef DBTRANSACTIONHANDLER_MS_SQL_H
#define DBTRANSACTIONHANDLER_MS_SQL_H

#include "db/db/dbtransactionhandler.h"


/*!
	\class DbTransactionHandlerMS_SQL
	\brief Implementation of transaction handler for MS SQL server
	\ingroup SQLModule

	Just sets isolation level to REPEATABLE READ
*/
class DbTransactionHandlerMS_SQL: public DbTransactionHandler
{
public:

	DbTransactionHandlerMS_SQL(){SetIsolationLevel("SET TRANSACTION ISOLATION LEVEL REPEATABLE READ");};

	void BeginTransaction(Status &pStatus,QSqlDatabase*) ;
	void RollbackTransaction(Status &pStatus,QSqlDatabase*);
	void CommitTransaction(Status &pStatus,QSqlDatabase*);

};
#endif //DBTRANSACTIONHANDLER_MS_SQL_H


