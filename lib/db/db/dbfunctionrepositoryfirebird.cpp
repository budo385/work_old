#include "dbfunctionrepositoryfirebird.h"
#include <QVariant>


void DbFunctionRepositoryFireBird::PrepareLimit(QString &strSQL,int nFrom, int nTo)
{
	int nIdx=strSQL.indexOf("SELECT");
	if (nIdx>=0)
	{
		nIdx=nIdx+7;
		if (nFrom>0 && nTo>0)
		{
			strSQL.insert(nIdx," FIRST "+QVariant(nTo-nFrom+1).toString()+" SKIP "+QString::number(nFrom)+" ");
		}
		else if (nFrom<=0 && nTo>0)
		{
			strSQL.insert(nIdx," FIRST "+QVariant(nTo).toString()+" ");
		}
		else if (nFrom>0)
		{
			strSQL.insert(nIdx," SKIP "+QString::number(nFrom)+" ");
		}
	}

}

//add on end of sQL:
void DbFunctionRepositoryFireBird::PrepareHierarchySelect(QString &strSQL,QString strCode,QString strCodePrefix, bool bChildren)
{
	QString strSQL1;
	int nLength=strCode.length();
	if (bChildren){
		//get descendants by code
		//strSQL1.sprintf(" SUBSTRING(%s_CODE FROM 1 FOR %d)='%s' AND CHAR_LENGTH(%s_CODE)>%d", strCodePrefix.toLatin1().constData(), nLength,strCode.toLatin1().constData(),strCodePrefix.toLatin1().constData(),nLength);
		strSQL1.sprintf(" SUBSTRING(%s_CODE FROM 1 FOR %d)=? AND CHAR_LENGTH(%s_CODE)>%d", strCodePrefix.toLatin1().constData(), nLength,strCodePrefix.toLatin1().constData(),nLength);
	}
	else{
		//get ancestors by code
		//strSQL1.sprintf(" SUBSTRING('%s' FROM 1 FOR CHAR_LENGTH(%s_CODE))=%s_CODE AND CHAR_LENGTH(%s_CODE)<%d", strCode.toLatin1().constData(), strCodePrefix.toLatin1().constData(),strCodePrefix.toLatin1().constData(),strCodePrefix.toLatin1().constData(),nLength);
		strSQL1.sprintf(" SUBSTRING(? FROM 1 FOR CHAR_LENGTH(%s_CODE))=%s_CODE AND CHAR_LENGTH(%s_CODE)<%d", strCodePrefix.toLatin1().constData(),strCodePrefix.toLatin1().constData(),strCodePrefix.toLatin1().constData(),nLength);
	}
	
	strSQL.append(strSQL1);
}


void DbFunctionRepositoryFireBird::PrepareCodeUpdate(QString &strSQL,QString strCode,QString strCodePrefix, int nOldLength)
{
	strSQL += "'" + strCode + "' || SUBSTRING(" + strCodePrefix + "_CODE FROM " + QString().sprintf("%d",nOldLength+1) + ") ";
}

void DbFunctionRepositoryFireBird::PrepareGetParentCode(QString &strSQL,QString strCode,QString strCodePrefix,QString strTableName)
{
	int nLength=strCode.length();
	strSQL +=QString("SELECT FIRST 1 %1_ID FROM %2 WHERE SUBSTRING(? FROM 1 FOR CHAR_LENGTH(%1_CODE))=%1_CODE AND CHAR_LENGTH(%1_CODE)<%3").arg(strCodePrefix).arg(strTableName).arg(nLength);
}
