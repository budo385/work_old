#include "dbdatatypeconvertor.h"


/*!
	Returns boolean list: every entry in list coresponds with column inside lstView
	TRUE: means conversion is needed, else not needed

	\param lstView				- view into which result will be fetched
	\param record				- record information from query
	\param bConversionNeeded	- if no column need conversion this flag is set to false, if only one exists, flag=true
*/
QList<bool> DbDataTypeConvertor::IsConversionNeeded(DbRecordSet& lstView, QSqlRecord* record,bool& bConversionNeeded)
{
	QList<bool> lstConvertFlags;
	int type;
	bConversionNeeded=false;

	for(int i=0;i<lstView.getColumnCount();++i)
	{
		type=lstView.getColumnType(i);
		if(type==record->value(i).type())
			lstConvertFlags.append(false);
		else
			{lstConvertFlags.append(true);bConversionNeeded=true;}
	}
	return lstConvertFlags;

}



