#include "dbautoidmanager_firebird.h"
#include <QVariant>

//#include "dbautoidmanager_oracle.h"


/*!
	Get next ID from global generator.
	Generator must be created prior to calling this function with name= "SEQ_"+"PRIMARY_KEY"
	\param status		 - return error 
	\param pDbConnection - connection
	\param nTableID		 - table id on which AutoID field is searched

	//if nPoolSize>1: reserves N id's: returns topmost: ret value-nPoolSize+1 is first, last is ret_value

	\return next id or 0 if error
*/
int DbAutoIdManagerFireBird::getNextInsertId(Status &status, QSqlDatabase *pDbConnection, QString strPrimaryKey,int nPoolSize)
{
	int nNextID = 0;	//error
	//DbTableKeyData tableData;
	status.setError(0);

	//get AUTOINC field: its PK:
	//DbSqlTableDefinition::GetKeyData(nTableID,tableData);
	QString strSequenceName = "SEQ_" + strPrimaryKey;

	QString strSql = "SELECT GEN_ID("+strSequenceName+", "+QVariant(nPoolSize).toString()+") FROM RDB$DATABASE;";

	QSqlQuery query(*pDbConnection);
	if(!query.exec(strSql))
	{
		//B.T. 26.03.2013: replace _ID with _SEQUENCE for old SPC tables try to use their generator: triger should update both ID and SEQ field at same time
		strSequenceName=strSequenceName.replace("_ID","_SEQUENCE");
		strSql = "SELECT GEN_ID("+strSequenceName+", "+QVariant(nPoolSize).toString()+") FROM RDB$DATABASE;";
		if(!query.exec(strSql))
		{
			//error executing query
			status.setError(StatusCodeSet::ERR_SQL_SEQUENCE_FETCH_FAILED);
			return 0;
		}
	}

	//fetch ID value
	query.next();
	nNextID = query.value(0).toInt();
	return nNextID;

}


