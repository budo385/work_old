#ifndef DBSETTINGS_H
#define DBSETTINGS_H

#include <QMap>
#include "db/db/dbsqlmanager.h"
#include "common/common/status.h"
#include "common/common/inifile.h"

/*!
\class DbSettings
\brief Class for accessing SQL connection settings from .ini file.
\ingroup SQLModule

Utility class for accessing SQL connection settings from .ini file.
*/
class DbSettings
{
public:
    DbSettings(QString IniFilePath);
    ~DbSettings();
	DbConnectionSettings* GetSettings(QString ConnName);

private:
	void LoadFromIni();

private:
	QString m_strIniFilePath;
	QMap <QString, DbConnectionSettings*> m_mapDbConn;
};
#endif // DBSETTINGS_H


