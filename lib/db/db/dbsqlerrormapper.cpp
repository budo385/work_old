#include "dbsqlerrormapper.h"
#include <QSqlError>



//
Status DbSQLErrorMapper::mapSQLError(QSqlQuery* pQuery)
{
	Status pStatus;
	QString strQuery=pQuery->lastQuery();
	QSqlError error=pQuery->lastError();
	//if (!error.isValid()) return pStatus;

	//only if debug, show errors from DB:
	//#ifndef _DEBUG
	//		pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL_RELEASE);
	//#else
			pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL,error.text());
	//#endif

	return pStatus;

}
