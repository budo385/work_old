#include "dbfunctionrepository_mysql.h"
#include <QVariant>



void DbFunctionRepositoryMySQL::PrepareLimit(QString &strSQL,int nFrom, int nTo)
{

	if (nFrom>0 && nTo>0)
	{
		strSQL.append(" LIMIT "+QVariant(nFrom).toString()+","+QVariant(nTo).toString());
	}
	else if (nFrom<=0 && nTo>0)
	{
		strSQL.append(" LIMIT 0,"+QVariant(nTo).toString());
	}
	else if (nFrom>0)
	{
		strSQL.append(" LIMIT "+QVariant(nFrom).toString());
	}


}

//add on end of sQL:
void DbFunctionRepositoryMySQL::PrepareHierarchySelect(QString &strSQL,QString strCode,QString strCodePrefix, bool bChildren)
{

	QString strSQL1;
	int nLength=strCode.length();
	if (bChildren)
		//strSQL1.sprintf("LOCATE('%s', %s_CODE)=1 AND LENGTH(%s_CODE)>%d", strCode.toLatin1().constData(), strCodePrefix.toLatin1().constData(), strCodePrefix.toLatin1().constData(), nLength);
		strSQL1.sprintf("LOCATE(?, %s_CODE)=1 AND LENGTH(%s_CODE)>%d", strCodePrefix.toLatin1().constData(), strCodePrefix.toLatin1().constData(), nLength);
	else
		//strSQL1.sprintf("LOCATE(%s_CODE,'%s')=1 AND LENGTH(%s_CODE)<%d", strCodePrefix.toLatin1().constData(), strCode.toLatin1().constData(), strCodePrefix.toLatin1().constData(), nLength);
		strSQL1.sprintf("LOCATE(%s_CODE,?)=1 AND LENGTH(%s_CODE)<%d", strCodePrefix.toLatin1().constData(), strCodePrefix.toLatin1().constData(), nLength);

	strSQL.append(strSQL1);
}

void DbFunctionRepositoryMySQL::PrepareCodeUpdate(QString &strSQL,QString strCode,QString strCodePrefix, int nOldLength)
{
	strSQL += "CONCAT('" + strCode + "', SUBSTRING(" + strCodePrefix + "_CODE FROM " + QString().sprintf("%d",nOldLength+1) + ")) ";
}