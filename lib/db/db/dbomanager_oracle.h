#ifndef DBOBJMANAGER_ORACLE_H
#define DBOBJMANAGER_ORACLE_H


#include "dbomanager.h"
#include <QStringList>


/*!
	\class DbObjectManager_Oracle
	\brief For managing Db objects (drop, create, etc..) for ORACLE
	\ingroup Db_Actualize

*/
class DbObjectManager_Oracle: public DbObjectManager
{
public:

	DbObjectManager_Oracle(DbConnectionSettings &DbSettings):DbObjectManager(DbSettings){};
	
	void	DropAllTriggers(Status &pStatus, QSqlDatabase *pDbConnection);
	void	DropAllConstraints(Status &pStatus, QSqlDatabase *pDbConnection);
	void	DropAllIndexes(Status &pStatus, QString strTableName,QSqlDatabase *pDbConnection);

	bool CheckIfExists_Index(QSqlDatabase *pDbConnection, QString strTableName, QString strIndexName);

	bool CheckIfExists_Trigger(QSqlDatabase *pDbConnection, QString strTriggerName);
	void DropIfExists_Trigger(Status &pStatus, QSqlDatabase *pDbConnection, QString strTriggerName);

	bool CheckIfExists_Sequence(QSqlDatabase *pDbConnection, QString strSeqName);
	void DropIfExists_Sequence(Status &pStatus, QSqlDatabase *pDbConnection, QString strSeqName);
	void CreateSequence(Status &pStatus, QSqlDatabase *pDbConnection, QString strSeqName, int nStartCounter=1);

	bool CheckIfExists_Proc(QSqlDatabase *pDbConnection, QString strProcName);
	void DropIfExists_Proc(Status &pStatus, QSqlDatabase *pDbConnection, QString strProcName);

	bool CheckIfExists_Funct(QSqlDatabase *pDbConnection, QString strFunctName);
	void DropIfExists_Funct(Status &pStatus, QSqlDatabase *pDbConnection, QString strFunctName);

	void CreateAutoIncrementID(Status &pStatus, QSqlDatabase *pDbConnection,QString strTableName, QString strAutoIncFieldName);
	void AdjustAutoIncrementAfterDbCopy(Status &pStatus, QSqlDatabase *pDbConnection,QString strTableName, QString strAutoIncFieldName);

	QString	GetTableSpace(){ return (mDbSettings.m_strTableSpace.isEmpty()?"":" TABLESPACE "+mDbSettings.m_strTableSpace); }
	QString	GetIndexSpace(){ return (mDbSettings.m_strIndexSpace.isEmpty()?"":" TABLESPACE "+mDbSettings.m_strIndexSpace); }

	void	AlterTable(Status &pStatus, QString strTableName, QString strTableCreationStatement,QStringList lstDeletedColumns, QList<QStringList> lstChangedNameColumns,QSqlDatabase *pDbConnection,bool bFillDefaultValuesForNewFields=false){};
	void	DropColumns(Status &pStatus, QString strTableName, QStringList lstDeletedColumns, QSqlDatabase *pDbConnection){};
	QString	BackupDatabase(Status &pStatus, QString strDatabaseName, QString strUser, QString strPassword,QString &strDumpFilePath,QString strDatabaseVersion,QString strHost, QString strPort,QSqlDatabase *pDbConnection){return "";};
	void	RestoreDatabase(Status &pStatus, QString strDatabaseName, QString strUser, QString strPassword,QString strDumpFilePath,QString strHost, QString strPort,QSqlDatabase *pDbConnection){};

	void	PrepareDemoDatabase(Status &pStatus, QSqlDatabase *pDbConnection){};
	void	ClearDemoDatabase(Status &pStatus, QSqlDatabase *pDbConnection){};
	void	CreateReadOnlyUser_Level1(Status &pStatus, QString strUserName, QString strPassword, QString strUserFirstName, QString strUserLastName, QString strMasterKey, QString strMasterPW, QSqlDatabase *pDbConnection){};


protected:
	bool GetTableColumnsFromDb(QString strTableName,QHash<QString,QStringList> &lstColsData,QSqlDatabase *pDbConnection){return false;};

};



#endif //DBOBJMANAGER_ORACLE_H

