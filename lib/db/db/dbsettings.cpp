#include "db/db/dbsettings.h"

DbSettings::DbSettings(QString IniFilePath)
{
	m_strIniFilePath = IniFilePath;
	LoadFromIni();
}

DbSettings::~DbSettings()
{
	//remove all objects from map: (linkage)
	QMapIterator<QString, DbConnectionSettings*> i(m_mapDbConn);
	while (i.hasNext()) 
	{
		i.next();
		delete i.value();
	}

}

void DbSettings::LoadFromIni()
{
	//Initialize ini file.
	IniFile ini;
	ini.SetPath(m_strIniFilePath);
	if (!ini.Load())
		return;
	int nCount;

	//Get number of connections.
	ini.GetValue("DbConn", "Count", nCount);

	for (int i = 0; i < nCount; ++i)
	{
		//Declare parm strings.
		QString connname, ConnName, dbtype, DbType, 
			dbname, DbName, dbuser, DbUser,
			dbpass, DbPass, dbhost, DbHost,
			dbport, DbPort, dboptions, DbOptions, 
			dbtimeout, DbTimeout;

		//Create names of strings.
		connname.sprintf("ConnName%d", i);
		dbtype.sprintf("DbType%d", i);
		dbname.sprintf("DbName%d", i);
		dbuser.sprintf("DbUser%d", i);
		dbpass.sprintf("DbPass%d", i);
		dbhost.sprintf("DbHost%d", i);
		dbport.sprintf("DbPort%d", i);
		dboptions.sprintf("DbOptions%d", i);
		dbtimeout.sprintf("DbTimeout%d", i);

		//Get values from ini.
		ini.GetValue("DbConn", connname, ConnName);
		ini.GetValue("DbConn", dbtype, DbType);
		ini.GetValue("DbConn", dbname, DbName);
		ini.GetValue("DbConn", dbuser, DbUser);
		ini.GetValue("DbConn", dbpass, DbPass);
		ini.GetValue("DbConn", dbhost, DbHost);
		ini.GetValue("DbConn", dbport, DbPort);
		ini.GetValue("DbConn", dboptions, DbOptions);
		ini.GetValue("DbConn", dbtimeout, DbTimeout);

		//insert in hash
		DbConnectionSettings *connection = new DbConnectionSettings();
		connection->m_strDbType = DbType;
		connection->m_strDbName = DbName;
		connection->m_strDbUserName = DbUser;
		connection->m_strDbPassword = DbPass;
		connection->m_strDbHostName = DbHost;
		connection->m_nDbPort = DbPort.toInt();
		connection->m_strDbOptions = DbOptions;
		connection->m_nDbPoolTimeout = DbTimeout.toInt();

		m_mapDbConn.insert(ConnName, connection);
	}
}

/*!
Returns pointer to DbConnectionSettings based on connection name.
\param ConnName	- Connection name. 
\return Pointer to DbConnectionSettings object.
*/
DbConnectionSettings* DbSettings::GetSettings(QString ConnName)
{
	return m_mapDbConn.value(ConnName);
}


