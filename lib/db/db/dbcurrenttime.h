#ifndef DBCURRENTTIME_H
#define DBCURRENTTIME_H

#include <QSqlDatabase>
#include <QSqlQuery>
#include <QDateTime>
#include "common/common/status.h"


/*!
	\class DbCurrentTime
	\ingroup SQLModule
	\brief Abstract class for implementation of currenttime() function


	For every database returns value of CURRENT_TIMESTAMP
	Note: when performed inside transaction this time should be freezed!! (Works for PostgreSql)
	This class works for MySQL,PostGreSQL, MSSQL, for ORACLE special implementation


*/
class DbCurrentTime 
{
public:

	virtual QDateTime GetCurrentDateTime(Status &status, QSqlDatabase *pDbConnection); //(UTC time if enabled)
	virtual QString GetCurrentDateTime(); //for every DB returns specific string (UTC time if enabled)
   
};

#endif // DBCURRENTTIME_H


