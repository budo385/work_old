#ifndef DBSQLERRORMAPPER_H
#define DBSQLERRORMAPPER_H

#include <QSqlQuery>
#include "common/common/status.h"



/*!
	\class DbSQLErrorMapper
	\ingroup SQLModule
	\brief Abstract class for converting database specific errors to human readable errors

*/
class DbSQLErrorMapper
{

public:
	
	virtual Status mapSQLError(QSqlQuery* pQuery);

private:
	
};

#endif // DBSQLERRORMAPPER_H
