#include "dbdatatypeconvertorfirebird.h"


/*!
	If types are different, converts database value to needed by recordset.
	For Firebird:
	- NULL values has not data types
	- TEXT field is stored as blob sub_type TEXT, or QByteArray

	\param databaseValue		- QVariant from database
	\param colTypeInRecordSet	- column type inside recordset
*/
QVariant DbDataTypeConvertorFireBird::ConvertValue(QVariant& databaseValue, int colTypeInRecordSet)
{

	//if same type, ok:
	if (databaseValue.type()==colTypeInRecordSet)return databaseValue;

	//check NULL's:
	if (databaseValue.type()==QVariant::Invalid)
	{
		//QVariant tmp((QVariant::Type)colTypeInRecordSet);
		//qDebug()<<tmp.type();
		return QVariant((QVariant::Type)colTypeInRecordSet); //return NULL variant with right type!
	}

	//check TEXT type:
	if (databaseValue.type()==QVariant::ByteArray && colTypeInRecordSet==QVariant::String)
	{
		return QString::fromUtf8(databaseValue.toByteArray());
	}

	qDebug()<<databaseValue.type();
	Q_ASSERT(false);

	
	//Q_ASSERT(colTypeInRecordSet==QVariant::Double); //only for double

	return databaseValue.toDouble();
}


