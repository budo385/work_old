#ifndef DBFUNCTIONREPOSITORY_H
#define DBFUNCTIONREPOSITORY_H

#include <QString>

/*!
	\class DbFunctionRepository
	\ingroup SQLModule
	\brief Abstract class for implementation of SQL functions


	For every database define own function that convert SQL string to proper one

*/

class DbFunctionRepository 
{
	
public:
	virtual void PrepareLimit(QString &strSQL,int nFrom, int nTo)=0;
	virtual void PrepareHierarchySelect(QString &strSQL,QString strCode,QString strCodePrefix, bool bChildren=true)=0;
	virtual void PrepareCodeUpdate(QString &strSQL,QString strCode,QString strCodePrefix, int nOldLength)=0;
	virtual void PrepareGetParentCode(QString &strSQL,QString strCode,QString strCodePrefix,QString strTableName)=0;
	
};

#endif // DBFUNCTIONREPOSITORY_H
