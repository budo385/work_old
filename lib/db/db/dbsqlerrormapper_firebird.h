#ifndef DBSQLERRORMAPPER_FIREBIRD_H
#define DBSQLERRORMAPPER_FIREBIRD_H

#include "dbsqlerrormapper.h"

class DbSQLErrorMapper_FireBird : public DbSQLErrorMapper
{

public:

	Status mapSQLError(QSqlQuery* pQuery);

private:
};


#endif // DBSQLERRORMAPPER_FIREBIRD_H
