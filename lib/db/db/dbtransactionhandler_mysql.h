#ifndef DBTRANSACTIONHANDLER_MYSQL_H
#define DBTRANSACTIONHANDLER_MYSQL_H

#include "db/db/dbtransactionhandler.h"


/*!
	\class DbTransactionHandlerMySQL
	\brief Implementation of transaction handler for MySQL
	\ingroup SQLModule

	Just sets isolation level to REPEATABLE READ
*/
class DbTransactionHandlerMySQL: public DbTransactionHandler
{
public:

	DbTransactionHandlerMySQL(){SetIsolationLevel("SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ");};

};
#endif //DBTRANSACTIONHANDLER_MYSQL_H

