#ifndef DBSQLMANAGER_H
#define DBSQLMANAGER_H

#include <QString>
#include <QList>
#include <QDateTime>
#include <QVariant>
#include <QSqlDriver>

#include "common/common/status.h"
#include "db_core/db_core/dbconnsettings.h"
#include "db/db/dbconnectionspool.h"

//ID managers:
#include "db/db/dbautoidmanager_oracle.h"
#include "dbautoidmanager_firebird.h"

//Tran managers:
#include "db/db/dbtransactionhandler_ms_sql.h"
#include "db/db/dbtransactionhandler_mysql.h"
#include "db/db/dbtransactionhandler_oracle.h"
#include "db/db/dbtransactionhandler_postgresql.h"
#include "db/db/dbtransactionhandler_firebird.h"


//DATA TYPE CONVERTORS:
#include "dbdatatypeconvertor.h"
#include "dbdatatypeconvertormysql.h"
#include "dbdatatypeconvertorfirebird.h"
#include "dbdatatypeconvertor_oracle.h"


//CURRENT DATABASE TIME:
#include "dbcurrenttime.h"
#include "dbcurrenttime_oracle.h"
#include "dbcurrenttime_firebird.h"

//SQL ERROR MAPPERS:
#include "dbsqlerrormapper.h"
#include "dbsqlerrormapper_mysql.h"
#include "dbsqlerrormapper_firebird.h"


//SQL FUNCTIONS:
#include "dbfunctionrepository.h"
#include "dbfunctionrepositoryfirebird.h"
#include "dbfunctionrepository_oracle.h"
#include "dbfunctionrepository_mysql.h"


//REST:
#include "db_core/db_core/db_drivers.h"
#include "db/db/dbdirectlocker.h"


/*!
	\class DbSqlManager
	\brief Sql Manager class
	\ingroup SQLModule

	Global object: central entry point for accessing  RDBMS.
	Implement RDBMS specific functions

*/
class DbSqlManager : private DbConnectionsPool
{
public:

	DbSqlManager();
	~DbSqlManager();

	//Session Pool interface
	QSqlDatabase *ReserveConnection(Status &status);
	DbConnectionsPool::ReleaseConnection;
	DbConnectionsPool::EnablePool;
	DbConnectionsPool::FillPoolWithMinConnections;
	DbConnectionsPool::ShutDown;
	DbConnectionsPool::ReserveExclusiveDbConnection;
	DbConnectionsPool::ReleaseExclusiveDbConnection;
	DbConnectionsPool::GetReservedConnections;
	DbConnectionsPool::GetThreadDbConnection;
	DbConnectionsPool::CheckConnections;
	

	//get db settings
	QString					GetDbUserName(){return m_ConnectionSettings.m_strDbUserName;};
	QString					GetDbName(){return m_ConnectionSettings.m_strDbName;};
	QString					GetDbType(){return m_ConnectionSettings.m_strDbType;};
	DbConnectionSettings	&GetDbSettings(){return m_ConnectionSettings;};
	int						GetDatabaseVersion(QSqlDatabase *pDbConnection);
	int						GetDatabaseVersion_SPC(QSqlDatabase *pDbConnection); 

	//gets Last Inserted ID:
	int getLastInsertId(Status &status, QSqlDatabase *pDbConnection, QSqlQuery *pQuery, QString strPrimaryKey);
	int getNextInsertId(Status &status, QSqlDatabase *pDbConnection, QString strPrimaryKey,int nPoolSize=1);

	//direct record locking
	//int LockRecordsDirect(Status &status, QSqlDatabase *pDbConnection, QString strTableName,QString strPrimaryKey, const DbLockList &lstRecords);
	//void UnLockRecordsDirect(Status &status, QSqlDatabase *pDbConnection, QString strTableName,QString strPrimaryKey,const  DbLockList &lstRecords);

	//sets new DB settings, test connection, shuts down whole pool:
	void Initialize(Status &pStatus, DbConnectionSettings &ConnectionSettings, int nPoolSizeMax=0, int nPoolSizeMin=0);

	//specific for each DB
	void BeginTransaction(Status &pStatus,QSqlDatabase*) ;
	void RollbackTransaction(Status &pStatus,QSqlDatabase*);
	void CommitTransaction(Status &pStatus,QSqlDatabase*);
	int GetCurrentTransactionCount(QSqlDatabase*);

	//Data types:
	QList<bool> IsConversionNeeded(DbRecordSet& lstView, QSqlRecord* record,bool& bConversionNeeded);
	QVariant ConvertValue(QVariant& databaseValue, int colTypeInRecordSet);

	//Current DB time:
	QDateTime GetCurrentDateTime(Status &status, QSqlDatabase *pDbConnection);
	QString GetCurrentDateTime(); //in Db specific format for SQL update/insert...

	//Special Insertion of ID & TimeStamp in SQL prepared statement:
	//int SetSQLDefaultValues(Status &status,QString &strSQL,QSqlDatabase *pDbConnection,QString strPrimaryKey,QString strLastModified,bool bDoNotGeneratePK=false);
	bool SetSQLDefaultValues(Status &pStatus,QString &strSQL,bool bInsertBindMark,QString &strPrimaryKey,int &nID,QSqlDatabase *pDbConnection);

	DbFunctionRepository* GetFunction(){return m_SqlFunctions;}

	//Get error:

	Status MapSqlError(QSqlQuery *pQuery);

private:
	void DestroyHandlers();

	DbConnectionSettings m_ConnectionSettings;			//< stores current DB settings
	int					 m_nTimeoutMs;					//< connectios Pool timeout;
	DbDirectLocker		 *m_DirectRecordLocker;			//< RDBMAS specific database level locking
	DbAutoIdManager		 *m_DbAutoIdManager;			//< RDBMAS specific autoincrement field generation
	DbTransactionHandler *m_DbTransactionManager;		//< RDBMAS specific transaction managment (for nested transactions)
	DbDataTypeConvertor	 *m_DbDataTypeConvertor;		//< RDBMAS specific database convertor: used in query when fetching data (only at fetch)
	DbCurrentTime		 *m_Time;						//< RDBMAS specific returns current timestamp
	DbSQLErrorMapper	 *m_SqlError;					//< RDBMAS specific returns right SQL error
	DbFunctionRepository *m_SqlFunctions;				//< RDBMAS specific prepares SQL functions
};

#endif // DBSQLMANAGER_H


