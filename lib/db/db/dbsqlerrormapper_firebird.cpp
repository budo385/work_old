#include "dbsqlerrormapper_firebird.h"
#include "common/common/stringhelper.h"
#include "db_core/db_core/dbsqltabledefinition.h"
#include <QSqlError>



Status DbSQLErrorMapper_FireBird::mapSQLError(QSqlQuery* pQuery)
{
	Status pStatus;//quick patch:
	QString strQuery=pQuery->lastQuery();
	QSqlError error=pQuery->lastError();
	qDebug()<<error.number();
	qDebug()<<error.databaseText();

	if (!error.isValid()) 
	{
		qDebug()<<"invalid error detected";
		pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL,"Unknown error!");
		return pStatus;
	}

	switch(error.number())
	{
	case -530: //FK-PK constraint violation
		{
			QString strTableOrigin,strEntityOrigin,strEntityOriginPlural;
			QString strTableLinked,strEntityLinked,strEntityLinkedPlural;
			QString strErrorText=error.databaseText();

			//linked table:
			int nStartFrom=strErrorText.indexOf("on table");
			if (nStartFrom>0)
			{
				strTableLinked=StringHelper::ExtractQuoteContent(strErrorText,"\"",nStartFrom);
				if (!strTableLinked.isEmpty())
				{
					DbSqlTableDefinition::GetEntityName(strTableLinked.toUpper(),strEntityLinked,strEntityLinkedPlural);
				}
			}

			if (strEntityLinked.isEmpty()) //can not detect linked table error: just DUMP full err, can't use if not:
			{
				//strEntityLinked="other";
				pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL,error.text()+"\n"+error.databaseText());
				return pStatus;
			}
			else
			{
				strEntityLinked.prepend("'");
				strEntityLinked.append("'");
			}
			//origin table:
			strEntityOrigin="";		//not allowed<-
			QString strArguments=strEntityOrigin+";"+strEntityLinked;
			pStatus.setError(StatusCodeSet::ERR_SQL_FK_CONSTRAINT_VIOLATION,strArguments);
			//extract linked table->find entity name by table name
			return pStatus;
		}
		break;

	case -913:
		{
			pStatus.setError(StatusCodeSet::ERR_SQL_DEADLOCK_TRYAGAIN);
			return pStatus;
		}
		break;

	case -836:
		{
			if (error.databaseText().contains("ERR_DB_SIZE"))
			{
				pStatus.setError(StatusCodeSet::ERR_SYSTEM_DEMO_DATABASE_LIMIT_ERROR);
				return pStatus;
			}
			else
				pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL,error.text());

		}
		break;

	case -803: //unique constraint: return specific code..client will handle
		{
			pStatus.setError(StatusCodeSet::ERR_SQL_UNIQUE_CONSTRAINT_VIOLATION); //,error.text());
		}
		break;

	default:

		//only if debug, show errors from DB:
		//#ifndef _DEBUG
		//		pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL_RELEASE);
		//#else
		pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL,error.text());
		//#endif

		break;
	}

	return pStatus;

}





