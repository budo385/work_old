#include "dbsqlerrormapper_mysql.h"
#include "common/common/stringhelper.h"
#include "db_core/db_core/dbsqltabledefinition.h"
#include <QSqlError>




//
Status DbSQLErrorMapper_MySQL::mapSQLError(QSqlQuery* pQuery)
{
	Status pStatus;
	QString strQuery=pQuery->lastQuery();
	QSqlError error=pQuery->lastError();
	if (!error.isValid()) return pStatus;


		switch(error.number())
		{
		case 1062: //unique key violation
			{
				QString strErrorText=error.databaseText();
				QString strValue=StringHelper::ExtractQuoteContent(strErrorText,"'",0);
				pStatus.setError(StatusCodeSet::ERR_SQL_DUPLICATE_ENTRY,strValue);
				return pStatus;
			}
			break;
		case 1451: //FK-PK constraint violation
			{
				QString strTableOrigin,strEntityOrigin,strEntityOriginPlural;
				QString strTableLinked,strEntityLinked,strEntityLinkedPlural;
				QString strErrorText=error.databaseText();


				//linked table:
				strTableLinked=StringHelper::ExtractQuoteContent(strErrorText,"`",0);
				if (!strTableLinked.isEmpty())
				{
					if (strTableLinked.indexOf("/")>=0)
					{
						int nFrom=strTableLinked.indexOf("/");
						strTableLinked=strTableLinked.mid(nFrom+1,strTableLinked.length()-nFrom-1);
					}
					DbSqlTableDefinition::GetEntityName(strTableLinked.toUpper(),strEntityLinked,strEntityLinkedPlural);
				}
				if (strEntityLinked.isEmpty())
					strEntityLinked="other";
				else
				{
					strEntityLinked.prepend("'");
					strEntityLinked.append("'");
				}


				
				//origin table:
				int nFrom=strErrorText.indexOf("REFERENCES");
				if (nFrom<0)
				{
					strEntityOrigin="record";
				}
				else
				{
					strTableOrigin=StringHelper::ExtractQuoteContent(strErrorText,"`",nFrom);
					if (!strTableOrigin.isEmpty())
						DbSqlTableDefinition::GetEntityName(strTableOrigin.toUpper(),strEntityOrigin,strEntityOriginPlural);
					if (strEntityOrigin.isEmpty())
						strEntityOrigin="record";
				}
				
				if (strEntityOrigin!="record")
				{
					strEntityOrigin.prepend("'");
					strEntityOrigin.append("'");
				}
				else
				{
					strEntityOrigin="";		//not allowed<-
				}

				QString strArguments=strEntityOrigin+";"+strEntityLinked;
				pStatus.setError(StatusCodeSet::ERR_SQL_FK_CONSTRAINT_VIOLATION,strArguments);


				//extract linked table->find entity name by table name
				return pStatus;
			}
			break;

		default:

			//only if debug, show errors from DB:
			//#ifndef _DEBUG
			//		pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL_RELEASE);
			//#else
					pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL,error.text());
			//#endif

		    break;
		}

	return pStatus;

}




