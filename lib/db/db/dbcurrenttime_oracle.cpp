#include "dbcurrenttime_oracle.h"



/*!
	Get current date & time from database server.
	Note if failed it will return current date time

	\return current time
*/

QDateTime DbCurrentTime_Oracle::GetCurrentDateTime(Status &status, QSqlDatabase *pDbConnection)
{
	return QDateTime::currentDateTime(); //no need to fetch from server as appserver and db server MUST reside in same TIMEZONE!!
/*
	status.setError(0);
	QString strSql = "SELECT CURRENT_TIMESTAMP FROM DUAL";
	QSqlQuery query(*pDbConnection);

	if(!query.exec(strSql))
	{
		//error executing query
		status.setError(StatusCodeSet::ERR_SQL_SEQUENCE_FETCH_FAILED);
		return QDateTime::currentDateTime();
	}

	//fetch ID value
	query.next();
	return DateTimeHandler::toUTC(query.value(0).toDateTime());
	*/
}


