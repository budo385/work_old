<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="1.1" language="de">
<context>
    <name>DbTransactionHandler</name>
    <message>
        <location filename="" line="0"/>
        <source>Failed to set isolation level for database connection!</source>
        <translation type="obsolete">Isolationsstufe für Datenbankverbindung konnte nicht hergestellt werden!</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="" line="0"/>
        <source>Failed to fetch current time from database</source>
        <translation type="obsolete">Aktuelle Zeit konnte nicht aus Datenbank gelesen werden</translation>
    </message>
    <message>
        <location filename="dbsqlmanager.cpp" line="151"/>
        <source>Connection could not be initialized. Database not supportted!</source>
        <translation>Verbindung konnte nicht hergestellt werden. Datenbank wird nicht unterstützt!</translation>
    </message>
    <message>
        <location filename="dbtransactionhandler.cpp" line="26"/>
        <source>Failed to set isolation level for database connection!</source>
        <translation>Isolationsstufe für Datenbankverbindung konnte nicht hergestellt werden!</translation>
    </message>
    <message>
        <location filename="dbconnectionsetup.cpp" line="155"/>
        <source>Data directory:</source>
        <translation>Datenverzeichnis:</translation>
    </message>
    <message>
        <location filename="dbconnectionsetup.cpp" line="155"/>
        <source> could not be created!</source>
        <translation>konnte nicht erstellt werden!</translation>
    </message>
    <message>
        <location filename="dbconnectionsetup.cpp" line="51"/>
        <source>Failed to initialize database!</source>
        <translation>Datenbank konnte nicht initialisiert werden!</translation>
    </message>
    <message>
        <location filename="dbconnectionsetup.cpp" line="109"/>
        <source>Failed to save settings file:</source>
        <translation>Einstellungdatei konnte nicht gespeichert werden:</translation>
    </message>
    <message>
        <location filename="dbconnectionsetup.cpp" line="236"/>
        <source>Failed to create fire.sql script</source>
        <translation>Firebird-SQL-Script konnte nicht erzeugt werden</translation>
    </message>
    <message>
        <location filename="dbconnectionsetup.cpp" line="253"/>
        <source>Failed to execute isql.exe. Make sure it is in application directory!</source>
        <translation type="obsolete">SQL konnte nicht ausgeführt werden! Bitte überprüfen, ob sich isql.exe im Anwendungsverzeichnis befindet!</translation>
    </message>
    <message>
        <location filename="dbconnectionsetup.cpp" line="312"/>
        <source>Failed to create database! Reason:</source>
        <translation>Datenbank konnte nicht erzeugt werden! Ursache:</translation>
    </message>
    <message>
        <location filename="dbconnectionsetup.cpp" line="287"/>
        <source>Failed to create mysql.sql script</source>
        <translation>MySQL-Script konnte nicht erzeugt werden</translation>
    </message>
    <message>
        <location filename="dbconnectionsetup.cpp" line="305"/>
        <source>Failed to execute mysql! Check paths!</source>
        <translation>MySQL konnte nicht ausgeführt werden! Bitte Pfadeinstellungen prüfen!</translation>
    </message>
    <message>
        <location filename="dbconnectionsetup.cpp" line="254"/>
        <source>Failed to execute isql.exe. Make sure it is in /firebird application subdirectory!</source>
        <translation>Fehler beim Start von isql.exe. Stellen Sie sicher, dass sich die Datei im Application-Unterverzeichnis von /Firebird befindet!</translation>
    </message>
</context>
</TS>
