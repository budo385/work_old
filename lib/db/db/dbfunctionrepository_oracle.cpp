#include "dbfunctionrepository_oracle.h"
#include <QVariant>


void DbFunctionRepository_Oracle::PrepareLimit(QString &strSQL,int nFrom, int nTo)
{

	//Q_ASSERT(false);

	Q_ASSERT(nFrom==0); //for firebird there is no support for From
	//strSQL.append()
	int nIdx=strSQL.indexOf("WHERE");
	if (nIdx>=0)
	{
		nIdx=nIdx+6;
		if (nFrom>0 && nTo>0)
		{
			strSQL.insert(nIdx," AND (rownum >= "+QString::number(nFrom)+" AND rownum <="+QString::number(nTo)+")");
		}
		else if (nFrom<=0 && nTo>0)
		{
			strSQL.insert(nIdx," AND rownum <="+QString::number(nTo));
		}
		else if (nFrom>0)
		{
			strSQL.insert(nIdx," AND rownum >="+QString::number(nFrom));
		}
	}
	else
	{
		if (nFrom>0 && nTo>0)
		{
			strSQL.append(" AND (rownum >= "+QString::number(nFrom)+" AND rownum <="+QString::number(nTo)+")");
		}
		else if (nFrom<=0 && nTo>0)
		{
			strSQL.append(" AND rownum <="+QString::number(nTo));
		}
		else if (nFrom>0)
		{
			strSQL.append(" AND rownum >="+QString::number(nFrom));
		}
	}

}

//add on end of sQL:
void DbFunctionRepository_Oracle::PrepareHierarchySelect(QString &strSQL,QString strCode,QString strCodePrefix, bool bChildren)
{
	//Q_ASSERT(false);

	QString strSQL1;
	int nLength=strCode.length();
	if (bChildren){
		//get descendants by code
		//strSQL1.sprintf(" SUBSTR(%s, 1, %d)='%s' AND LENGTH(%s_CODE)>%d", strCodePrefix.toLatin1().constData(), nLength,strCode.toLatin1().constData(),strCodePrefix.toLatin1().constData(),nLength);
		strSQL1.sprintf(" SUBSTR(%s, 1, %d)=? AND LENGTH(%s_CODE)>%d", strCodePrefix.toLatin1().constData(), nLength,strCodePrefix.toLatin1().constData(),nLength);
	}
	else{
		//get ancestors by code
		strSQL1.sprintf(" SUBSTR(?, 1,LENGTH(%s_CODE))=%s_CODE AND LENGTH(%s_CODE)<%d", strCodePrefix.toLatin1().constData(),strCodePrefix.toLatin1().constData(),strCodePrefix.toLatin1().constData(),nLength);
	}

	strSQL.append(strSQL1);
}


void DbFunctionRepository_Oracle::PrepareCodeUpdate(QString &strSQL,QString strCode,QString strCodePrefix, int nOldLength)
{
	//Q_ASSERT(false);

	strSQL += "'" + strCode + "' || SUBSTR(" + strCodePrefix + "_CODE, " + QString().sprintf("%d",nOldLength+1) + ") ";
}