#ifndef DBSQLERRORMAPPER_MYSQL_H
#define DBSQLERRORMAPPER_MYSQL_H

#include "dbsqlerrormapper.h"

class DbSQLErrorMapper_MySQL : public DbSQLErrorMapper
{

public:

	Status mapSQLError(QSqlQuery* pQuery);

private:

	
};

#endif // DBSQLERRORMAPPER_MYSQL_H
