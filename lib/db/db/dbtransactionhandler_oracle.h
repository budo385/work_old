#ifndef DBTRANSACTIONHANDLER_ORACLE_H
#define DBTRANSACTIONHANDLER_ORACLE_H

#include "db/db/dbtransactionhandler.h"


/*!
	\class DbTransactionHandlerOracle
	\brief Implementation of transaction handler for Oracle
	\ingroup SQLModule

	Just sets isolation level to REPEATABLE COMMITED, Oracle does not support readable read, 
	SERIALIZABLE is to restrictive.
	Oracle does not support RELEASE SAVEPOINT.
*/
class DbTransactionHandlerOracle: public DbTransactionHandler
{
public:

	DbTransactionHandlerOracle(){SetIsolationLevel("ALTER SESSION SET ISOLATION_LEVEL = READ COMMITTED");};

	void CommitTransaction(Status &pStatus,QSqlDatabase*);


};
#endif //DBTRANSACTIONHANDLER_ORACLE_H

