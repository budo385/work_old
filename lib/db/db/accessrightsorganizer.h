#ifndef ACCESSRIGHTSORGANIZER_H
#define ACCESSRIGHTSORGANIZER_H

#include <QHash>

#include "accessrightsid.h"
#include "accessright.h"
#include "accessrightset.h"

class AccessRightsOrganizer
{
public:
    AccessRightsOrganizer();
    ~AccessRightsOrganizer();

	AccessRight* GetAccessRight(int AccessRightID);
	AccessRightSet* GetAccessRightSet(int AccessRightSetID);

private:
	void InitializeAccessRights();
	void InitializeAccessRightSets();
	void InitializeARSRights();

	QHash<int, AccessRight *>		m_hshAccessRights;
	QHash<int, AccessRightSet *>	m_hshAccessRightSets;
};

#endif // ACCESSRIGHTSORGANIZER_H
