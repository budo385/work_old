#include "dbrecordlocker.h"


quint64 DbRecordLocker::m_nUniqueLockKey = 0;

/*!
	Gets connection if not exists.

	\param pStatus			- return error if connection create failed
	\param pDatabaseManager - external manager
	\param strUserSession	- user session (must be set)
	\param pDb				- connection, if be NULL, then own connection will be created
	\param bCheckParents	- if true (then it will lock parent table instead of given record id's to lock)


*/
DbRecordLocker::DbRecordLocker(Status &pStatus, DbSqlManager* pDatabaseManager, QString strUserSession, 
							   QSqlDatabase* pDb, bool bCheckParents /*= true*/)
{
	m_pDbManager = pDatabaseManager;
	m_pDb = pDb;
	m_strUserSession=strUserSession;
	m_bCheckParents = bCheckParents;
	Q_ASSERT(!m_strUserSession.isEmpty()); //assert if session is invalid!!!
	
	if (m_pDb!=NULL)
	{
		m_bKeepConnectionFlag = true;
	}
	else
	{
		m_pDb = m_pDbManager->ReserveConnection(pStatus); //reserve connection
		if(!pStatus.IsOK()) return;
		m_bKeepConnectionFlag = false;
	}
}


// release connection
DbRecordLocker::~DbRecordLocker()
{
	//if we reserverd then release:
	if (!m_bKeepConnectionFlag)
		m_pDbManager->ReleaseConnection(m_pDb);
}


/*!
	Lock records, can lock all or nothing or just partial, in later case it will return list with records that are already locked

	\param pStatus				- return error
	\param TableID				- table id
	\param LockList				- list of ID records for locking
	\param strLockedResourceID	- returned resource id
	\param pLstStatusRows		- filled only with bad ones (doesnt succeed in locking) if bAllOrNothing= false
	\param bAllOrNothing		- true means all or nothing, else pLstStatusRows is filled with bad ones

	\return bool		- FALSE if status=error
*/
bool DbRecordLocker::LockRecords(Status& pStatus, int TableID, DbLockList &LockList, QString& strLockedResourceID, DbRecordSet *pLstStatusRows,bool bAllOrNothing)
{
	//check only if both agree
	if (!m_bCheckParents || DbSqlTableDefinition::GetParentID(TableID) == -1)
		return LockRecordsInternal(pStatus, TableID, LockList, strLockedResourceID,pLstStatusRows,bAllOrNothing);
	else
	{
		DbLockList tmp=GetParentLockList(pStatus, TableID, LockList);
		return LockRecordsInternal(pStatus, DbSqlTableDefinition::GetParentID(TableID),tmp, 
								   strLockedResourceID,pLstStatusRows,bAllOrNothing);
	}
/*	
	if (!m_bCheckParents && DbSqlTableDefinition::GetParentID(TableID) == -1)
		return LockRecordsInternal(pStatus, TableID, LockList, strLockedResourceID,pLstStatusRows,bAllOrNothing);
	else
		return LockRecordsInternal(pStatus, DbSqlTableDefinition::GetParentID(TableID), 
		GetParentLockList(pStatus, TableID, LockList), 
		strLockedResourceID,pLstStatusRows,bAllOrNothing);
*/
}

/*!
	Unlock records by resource ID
	\param pStatus,				- returns erros
	\param pLockedResourceID	- locked res. id
	\return bool				- FALSE if pStatus has errors
*/
bool DbRecordLocker::UnlockByResourceID(Status& pStatus, QString& pLockedResourceID)
{
	DbSqlQuery query(pStatus,m_pDbManager, m_pDb);
	query.BeginTransaction(pStatus);
	if(!pStatus.IsOK())return false;

	query.Prepare(pStatus, "DELETE FROM CORE_LOCKING WHERE CORL_RESOURCE_ID = :CORL_RESOURCE_ID");
	query.BindValue(":CORL_RESOURCE_ID", pLockedResourceID);

	// If some error came out
	if (!query.ExecutePrepared(pStatus))
	{
		pStatus.setError(StatusCodeSet::ERR_SQL_LOCKING_FAIL, 
		"Error trying to delete from CORL_TABLE values for RESOURCE_ID " + pLockedResourceID);
		query.Rollback();
		return false;
	}

	// If there was nothing to unlock or everything passed just fine, smile.
	pStatus.setError(StatusCodeSet::ERR_NONE);
	query.Commit(pStatus);
	return true;
}

/*!
	Unlock records by session ID
	\param pStatus,				- returns erros
	\param pUserSessionID		- user session
	\return bool				- FALSE if pStatus has errors
*/
bool DbRecordLocker::UnlockByUserSessionID(Status& pStatus, QString& pUserSessionID)
{
	DbSqlQuery query(pStatus,m_pDbManager, m_pDb);
	query.BeginTransaction(pStatus);
	if(!pStatus.IsOK())return false;
	query.Prepare(pStatus, "DELETE FROM CORE_LOCKING WHERE CORL_SESSION_ID = :CORL_SESSION_ID");
	query.BindValue(":CORL_SESSION_ID"	, pUserSessionID);
	
	// If some error came out
	if (!query.ExecutePrepared(pStatus))
	{
		pStatus.setError(StatusCodeSet::ERR_SQL_LOCKING_FAIL, 
			"Error trying to delete from CORL_TABLE values for SESSION_ID " + pUserSessionID);
		query.Rollback();
		return false;
	}

	// If there was nothing to unlock or everything passed just fine, smile.
	pStatus.setError(StatusCodeSet::ERR_NONE);
	query.Commit(pStatus);
	return true;
}


/*!
	Checks if records are locked

	\param pStatus		- return error if one is locked
	\param TableID		- table id
	\param LockList		- list of ID records for locking
	\return bool		- FALSE if status=error
*/

bool DbRecordLocker::IsLocked(Status& pStatus, int TableID, DbLockList &LockList)
{
	if (!m_bCheckParents || DbSqlTableDefinition::GetParentID(TableID) == -1)
		return IsLockedInternal(pStatus, TableID, LockList);
	else
	{
		DbLockList tmp=GetParentLockList(pStatus, TableID, LockList);
		return IsLockedInternal(pStatus, DbSqlTableDefinition::GetParentID(TableID),tmp);
	}
}

bool DbRecordLocker::LockRecordsInternal(Status& pStatus, int TableID, DbLockList &LockList,QString& strLockedResourceID, DbRecordSet *pLstStatusRows,bool bAllOrNothing)
{
	if (!LockList.count())
	{
		pStatus.setError(StatusCodeSet::ERR_NONE);
		return true;
	}

	//reset error:
	pStatus.setError(StatusCodeSet::ERR_NONE);
	FormatStatusRowsLst(pLstStatusRows);//redefine status list

	//only if allornothing
	if(bAllOrNothing)
	{ //test if already locked:
	 if(IsLocked(pStatus,TableID,LockList)) return false;
	 if(!pStatus.IsOK())return false;
	}

	//locked resource refresh
	m_nUniqueLockKey++;
	strLockedResourceID = m_strUserSession + "_" + QVariant((quint64)m_nUniqueLockKey).toString();

	//begin transaction (only when all or nothing, otherwise lock what u can!
	DbSqlQuery query(pStatus,m_pDbManager, m_pDb); 
	if(bAllOrNothing)query.BeginTransaction(pStatus);
	if(!pStatus.IsOK())return false;

	query.Prepare(pStatus, "INSERT INTO CORE_LOCKING (CORL_DAT_LAST_MODIFIED, CORL_RECORD_ID, CORL_RESOURCE_ID, CORL_SESSION_ID, CORL_TABLE_ID) VALUES (:CORL_DAT_LAST_MODIFIED, :CORL_RECORD_ID, :CORL_RESOURCE_ID, :CORL_SESSION, :CORL_TABLE_ID)");

	for (int i = 0; i < LockList.size(); ++i)
	{
		query.BindValue(":CORL_DAT_LAST_MODIFIED", QDateTime::currentDateTime());
		query.BindValue(":CORL_RECORD_ID", LockList.at(i));
		query.BindValue(":CORL_RESOURCE_ID", strLockedResourceID);
		query.BindValue(":CORL_SESSION_ID", m_strUserSession);
		query.BindValue(":CORL_TABLE_ID", TableID);

		if (!query.ExecutePrepared(pStatus))
		{
			if(bAllOrNothing)
			{
				query.Rollback();
				return false;
			}
			else
			{
				//add as error row in returned list
				if(pLstStatusRows!=NULL)
				{
					pLstStatusRows->addRow();
					pLstStatusRows->setData(pLstStatusRows->getRowCount()-1,"ID",LockList.at(i));
					pLstStatusRows->setData(pLstStatusRows->getRowCount()-1,"STATUS_CODE",StatusCodeSet::ERR_SQL_LOCKING_FAIL);
					pLstStatusRows->setData(pLstStatusRows->getRowCount()-1,"STATUS_TEXT",QT_TR_NOOP("Record is locked by another user!"));
				}
			}
			
		}
	}

	if(bAllOrNothing)query.Commit(pStatus);
	return true;
}



DbLockList DbRecordLocker::GetParentLockList(Status& pStatus, int TableID, DbLockList &LockList)
{
	QString QryStr = DbSqlTableDefinition::GetParentSqlRecordLockString(TableID);

	QryStr += " (";
	//Construct query string from lock list.
	for (int i = 0; i < LockList.size(); ++i)
	{
		QryStr += QVariant(LockList.at(i)).toString();
		QryStr += ",";
	}
	QryStr.truncate(QryStr.size()-1);
	QryStr += ") ";

	DbSqlQuery query(pStatus, m_pDbManager, m_pDb);

	query.Execute(pStatus, QryStr);

	//If there is something returned, then it is locked.
	DbLockList ParentLockList;
	while (query.Next())
	{
		if (!ParentLockList.contains(query.Value(0).toInt()))
			ParentLockList.append(query.Value(0).toInt());
	}
	
	return ParentLockList;
}


bool DbRecordLocker::IsLockedInternal(Status& pStatus, int TableID, DbLockList &LockList)
{
	DbSqlQuery query(pStatus, m_pDbManager, m_pDb);

	QString QryStr = "SELECT CORL_RECORD_ID,CORL_SESSION_ID FROM CORE_LOCKING WHERE CORL_TABLE_ID = ";
	QryStr += QVariant(TableID).toString();
	QryStr += " AND CORL_RECORD_ID IN (";

	//Construct query string from lock list.
	for (int i = 0; i < LockList.size(); ++i) 
	{
		QryStr += QVariant(LockList.at(i)).toString();
		QryStr += ",";
	}
	QryStr.truncate(QryStr.size()-1);
	QryStr += ") ";

	query.Execute(pStatus, QryStr);

	//If there is something returned, then it is locked.
	while (query.Next())
	{
		/*
		QString errText("Table " + TableID);
		errText.append(" at row ");
		errText.append(query.Value(0).toString());
		errText.append(" already locked!");
		*/
		pStatus.setError(StatusCodeSet::ERR_SQL_ALREADY_LOCKED,query.Value(1).toString()); //as text return session id of locked resource (first)
		return true;
	}

	return false;
}




void DbRecordLocker::FormatStatusRowsLst(DbRecordSet *pLstStatusRows)
{
	if (pLstStatusRows == NULL)
		return;

	pLstStatusRows->destroy();
	pLstStatusRows->addColumn(QVariant::Int, QString("ID"));
	pLstStatusRows->addColumn(QVariant::Int, QString("STATUS_CODE"));
	pLstStatusRows->addColumn(QVariant::String, QString("STATUS_TEXT"));
}


