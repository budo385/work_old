#ifndef DBTRANSACTIONHANDLER_POSTGRESQL_H
#define DBTRANSACTIONHANDLER_POSTGRESQL_H

#include "db/db/dbtransactionhandler.h"


/*!
	\class DbTransactionHandlerPostgreSQL
	\brief Implementation of transaction handler for PostgreSQL
	\ingroup SQLModule

	Just sets isolation level to REPEATABLE READ
*/
class DbTransactionHandlerPostgreSQL: public DbTransactionHandler
{
public:

	DbTransactionHandlerPostgreSQL(){SetIsolationLevel("SET SESSION CHARACTERISTICS AS TRANSACTION ISOLATION LEVEL REPEATABLE READ");};
};
#endif //DBTRANSACTIONHANDLER_POSTGRESQL_H


