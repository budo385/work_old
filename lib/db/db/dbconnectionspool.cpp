#include "dbconnectionspool.h"
#include <QSqlError>
#include <QTime>
#include <QStringList>
#include <QDebug>
#include <QCoreApplication>
#include "common/common/logger.h"
#include "common/common/sleeper.h"
#include "common/common/threadid.h"

#define CONNECTION_RELEASE_CHUNK 10 //number of connection to be release from pool in each check if all unused

/*!
	Constructor
	\param nSize		- maximum pool size
*/
DbConnectionsPool::DbConnectionsPool() :
	m_pTransManager(NULL)
{
	m_pSem = NULL;
	m_nDbPort = -1;
	m_bEnabled=true;
	m_nLastConnectionReservedCount=0;
	m_nMinSize=0;
	m_nMaxSize=0;

}

DbConnectionsPool::~DbConnectionsPool()
{
	ShutDown();
	if(m_pSem!=NULL)delete m_pSem;
}

void DbConnectionsPool::setPoolSize( int nMaxSize, int nMinSize)
{
	if(m_pSem!=NULL)delete m_pSem;
	if(nMaxSize!=0)
		m_pSem= new QSemaphore(nMaxSize);
	else
		m_pSem= new QSemaphore(100000); //unlimited is 100k

	m_nMaxSize=nMaxSize;
	m_nMinSize=nMinSize;
	
};


void DbConnectionsPool::EnablePool(bool bEnable)
{
	m_AccessLock.lock();
	m_bEnabled = bEnable;
	m_AccessLock.unlock();
}

//open initial connections right away
void DbConnectionsPool::FillPoolWithMinConnections()
{
	if (m_bEnabled && m_nMinSize>0)
	{
		QList<QSqlDatabase *> lstConns;
		for (int i=0;i<m_nMinSize;i++)
		{
			Status err;
			lstConns.append(ReserveConnection(err));
			if (!err.IsOK()) //if one fails abort...
				return;
		}
		for (int i=0;i<lstConns.size();i++)
		{
			QSqlDatabase *pDB=lstConns.at(i);
			ReleaseConnection(pDB);
		}
	}
}

void DbConnectionsPool::setDatabaseType( const QString & type )
{
	m_strDbType = type;
}

void DbConnectionsPool::setHostName( const QString & host )
{
	m_strDbHost = host;
}

void DbConnectionsPool::setUserName( const QString & user )
{
	m_strDbUser = user;
}

void DbConnectionsPool::setPassword( const QString & password )
{
	m_strDbPassword = password;
}

void DbConnectionsPool::setConnectOptions( const QString & options)
{
	m_strDbOptions = options;
}

void DbConnectionsPool::setDatabaseName( const QString & name )
{
	m_strDbName = name;
}

void DbConnectionsPool::setPort( int port )
{
	m_nDbPort = port;
}



/*!
	ReserveConnection, stops thread until connection is available
	\param status - return error
	\param nTimeoutMs - if 0 waits for ever, else waits with time specified (milisec)

	\return QSqlDatabase* new connection for use
*/
QSqlDatabase *DbConnectionsPool::ReserveConnection(Status &status, int nTimeoutMs)
{
	QSqlDatabase *pDb = NULL;
	status.setError(0);	//reset error

	if(!m_bEnabled)
	{
		status.setError(StatusCodeSet::ERR_SQL_SESSION_POOL_BLOCKED);
		return pDb;			//pool is disabled, no new connections allowed
	}

	if(0 == nTimeoutMs)		//blocking mode - no timeout required
	{
		//QTime timer_api;
		//timer_api.start();

		m_pSem->acquire(1);	//wait infinite for resource to become available
		pDb = DoReserveConnection(status);
		if (!status.IsOK())
		{
			m_pSem->release(1);
		}

		//int nElapsed=timer_api.elapsed();
		//QString strMsg=QString("ReserveConnection, total elapse: %1, thread: %2").arg(nElapsed).arg(ThreadIdentificator::GetCurrentThreadID());
		//ApplicationLogger::logMessage(StatusCodeSet::TYPE_SPEED_TEST_LEVEL,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,strMsg);

	}
	else
	{
		//version with timeout
		//semaphore regulates access to the resources
		if(!m_pSem->tryAcquire(1,nTimeoutMs))
		{
			qDebug("Connection pool: Timeout elapsed\n");
			status.setError(StatusCodeSet::ERR_SQL_SESSION_POOL_BLOCKED);
		}
		else
		{
			pDb = DoReserveConnection(status);
			if (!status.IsOK())
				m_pSem->release(1);
		}
	}
	return pDb;
}


/*!
	Private DoReserveConnection, multithread safe
*/
QSqlDatabase *DbConnectionsPool::DoReserveConnection(Status &status)
{
	QSqlDatabase *pDb = NULL;
	QStringList list;
	status.setError(0);
	
	m_mainPoolLock.lockForWrite();

	//search existing free connection (if any)
	int nMax = m_lstPool.size();
	for(int i=0; i<nMax; i++)
	{
		if(!m_lstPool.at(i).bLocked)	//found free connection
		{
			m_lstPool[i].bLocked = true;	//make a reservation
			pDb = m_lstPool.at(i).pDb;
			m_lstPool[i].nThreadID = ThreadIdentificator::GetCurrentThreadID(); //set thread identifier
#ifndef QT_NO_DEBUG
			//_Debug_Check_For_MultiReservations();
#endif QT_NO_DEBUG
			break;
		}
	}
	m_mainPoolLock.unlock();

	//create new connection:
	if(!pDb)
	{
		DBCN db_entry = CreateNewConnection(status,m_strDbType,m_strDbHost,m_strDbUser,m_strDbPassword,m_strDbOptions,m_nDbPort,m_strDbName);
		if (!status.IsOK())
			return NULL;
		pDb =db_entry.pDb;
		m_mainPoolLock.lockForWrite();
#ifndef QT_NO_DEBUG
		//_Debug_Check_For_MultiReservations();
#endif QT_NO_DEBUG
		m_lstPool.append(db_entry);
		//QString strMsg=QString("DoReserveConnection, total DB connections: %1").arg(m_lstPool.size());
		//ApplicationLogger::logMessage(StatusCodeSet::TYPE_SPEED_TEST_LEVEL,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,strMsg);
		m_mainPoolLock.unlock();
	}

	m_hshThreadConnectionsLock.lockForWrite();
	//qDebug()<<"adding connection to thread "<<ThreadIdentificator::GetCurrentThreadID();
	//Q_ASSERT(!m_hshThreadConnections.contains(ThreadIdentificator::GetCurrentThreadID())); //if connection is inside cache already--error
	m_hshThreadConnections[ThreadIdentificator::GetCurrentThreadID()]=pDb;

	m_hshThreadConnectionsLock.unlock();

	return pDb;
}

/*!
	ReleaseConnection, does not break DB connection, leave it in pool, mark as free 

	\param pConnection - connection for release
*/
void DbConnectionsPool::ReleaseConnection(QSqlDatabase *&pConnection)
{
	if(NULL == pConnection)
		return;

	m_mainPoolLock.lockForWrite();

	bool bReleased = false;

	//find the connection
	int nMax = m_lstPool.size();
	for(int i=0; i<nMax; i++)
	{
		if(pConnection == m_lstPool.at(i).pDb)
		{
			//check for orphan transactions:
#ifndef QT_NO_DEBUG
			if(m_pTransManager)
			{
				if(!m_pTransManager->CheckDbConnection(pConnection))
				{
					//log that connection remains locked!
					//dump emergency file log:
					Logger log;
					log.EnableFileLogging(QCoreApplication::applicationDirPath()+"/settings/debug_emergency.log");
					log.EnableConsoleLogging();
					log.SetLogLevel(4);
					int nThread=ThreadIdentificator::GetCurrentThreadID();
					log.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SQL_ORPHAN_TRANSACTIONS_DETECTED,"Database connection remains locked: "+m_lstPool[i].strName);
					log.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_SQL_CURRENT_DB_CONNECTIONS,QVariant(nMax).toString());
					//BT. 20.01.2009. un-register connection for thread: used to cache last thread connection (faster connection fetch)
					m_hshThreadConnectionsLock.lockForWrite();
					Q_ASSERT(m_hshThreadConnections.contains(ThreadIdentificator::GetCurrentThreadID())); //if connection is not inside cache -- error
					m_hshThreadConnections.remove(m_lstPool[i].nThreadID);		
					//qDebug()<<"Removed Thread: "<<ThreadIdentificator::GetCurrentThreadID()<<" -- db: "<<pConnection;
					m_hshThreadConnectionsLock.unlock();
					m_lstPool[i].nThreadID = 0;      //reset thread id
					pConnection = NULL;	// reset the pointer
					m_mainPoolLock.unlock();
					return;
				}
			}
#endif QT_NO_DEBUG

			if (m_lstPool.at(i).bLocked)
				bReleased=true;
			int nThread =m_lstPool.at(i).nThreadID;

			m_lstPool[i].bLocked = false;	 //unlock
			m_lstPool[i].nThreadID = 0;      //reset thread id
			m_mainPoolLock.unlock();

			//BT. 20.01.2009. un-register connection for thread: used to cache last thread connection (faster connection fetch)
			m_hshThreadConnectionsLock.lockForWrite();
			//Q_ASSERT(m_hshThreadConnections.contains(ThreadIdentificator::GetCurrentThreadID())); //if connection is not inside cache -- error
			//qDebug()<<"removing connection from thread "<<ThreadIdentificator::GetCurrentThreadID();
			m_hshThreadConnections.remove(nThread);		
			m_hshThreadConnectionsLock.unlock();

			break;
		}
	}

	pConnection = NULL;	// reset the pointer
	
	if(bReleased)
		m_pSem->release(1);	//release 1 resource
}



/*!
	Detach all free Db connections, if reserved found in list, fires ASSERT
*/
void DbConnectionsPool::Clear()
{
	m_mainPoolLock.lockForWrite();
	//for each unlocked connection in the pool
	int nIdx = 0;
	while(nIdx < m_lstPool.size())
	{
		if(!m_lstPool.at(nIdx).bLocked)
		{
			delete m_lstPool.at(nIdx).pDb;
			QSqlDatabase::removeDatabase (m_lstPool.at(nIdx).strName);
			m_lstPool.removeAt(nIdx);
		}
		else
			nIdx++;
	}
	m_mainPoolLock.unlock();

	//BT: 20.01.2009: clear all cache:
	m_hshThreadConnectionsLock.lockForWrite();
	m_hshThreadConnections.clear();
	m_hshThreadConnectionsLock.unlock();

	//unregister connection from tran manager:
	if(m_pTransManager)m_pTransManager->RemoveAllDbConnections();

	//all connections should be returned to the pool before clearing
	Q_ASSERT(0 == m_lstPool.size());
}




/*!
	Gets number of currently reserverd connections

	\return No of current Db conn in use
*/
int DbConnectionsPool::GetReservedConnections()
{
	int nReserved=0;
	
	m_mainPoolLock.tryLockForRead();

	int nMax = m_lstPool.size();
	for(int i=0; i<nMax; i++)
		if(m_lstPool.at(i).bLocked)
			nReserved++;

	m_mainPoolLock.unlock();

	return nReserved;
}


/*!
	Blocks pool until all connections are free, then clears all session while pool remains blocked 

	\return No of current Db conn in use
*/
void DbConnectionsPool::ShutDown()
{
	//lock pool
	EnablePool(false);

	//wait until all DB connections are released: maybe some timeout assert here !?
	while(GetReservedConnections()>m_nMinSize)
	{
		ThreadSleeper::Sleep(1);
	}
	
	//detach all DB connections
	Clear();
}



/*!
	Blocks pool until only one connection reamins. 
	Pool remains blocked such assuring that you have 1 exclusive conn (at least from this app. server instance)
	Use: reserve connection, call this method, but re-enable pool after this, thank you

	\param pStatus  - if something goes wrong
	\param pDb		- exclusive connection
*/


void DbConnectionsPool::ReserveExclusiveDbConnection(Status &pStatus, QSqlDatabase **pDb)
{
	//reqeust  conn
	*pDb=ReserveConnection(pStatus);
	if(!pStatus.IsOK()) return;

	//lock pool
	EnablePool(false);

	//wait until all DB connections are released: maybe some timeout assert here !?
	while(GetReservedConnections()>1)
	{
		ThreadSleeper::Sleep(1);//1ms
	}
}


/*!
	Returns exclusive connection/ enables poool
	\param pDb		- exclusive connection
*/
void DbConnectionsPool::ReleaseExclusiveDbConnection(QSqlDatabase **pDb)
{
	//release conn
	ReleaseConnection(*pDb);

	//enable pool 
	EnablePool(true);

	pDb=NULL;
}


/*!
	DEBUG ONLY: check if one thread reserver more then ONE connection!!! ASSERT if so!!!
*/

void DbConnectionsPool::_Debug_Check_For_MultiReservations()
{
	QList <struct DBCN> lstTemp=m_lstPool;
	int nMax = lstTemp.size();
	int nThreadID;int i;
	if( nMax==0)return;
	
	while(lstTemp.size()>0)
	{
		nThreadID= lstTemp.at(0).nThreadID;
		lstTemp.removeFirst(); 
		if(nThreadID==0) continue; //0 is empty
		for(i=0; i<lstTemp.size(); i++)
			if(lstTemp[i].nThreadID==nThreadID)
			{Q_ASSERT_X(false,"Db ConenctionPool",QString("More then one Db connection is reserved by thread id:"+QVariant(nThreadID).toString()).toLatin1());return;}

	}
}

//Get connection for current thread
//return NULL if connection not registered to current thread!
QSqlDatabase *DbConnectionsPool::GetThreadDbConnection()
{
	QReadLocker lock(&m_hshThreadConnectionsLock);
	QSqlDatabase *pDb=m_hshThreadConnections.value(ThreadIdentificator::GetCurrentThreadID(),NULL);
	return pDb;
}




//B.T. thread safe function, can be called from all threads, create new connection, thus increasing concurrency when there is high demand for new DB connections
DBCN DbConnectionsPool::CreateNewConnection(Status &status, QString strDbType, QString strDbHost, QString strDbUser, QString strDbPassword, QString strDbOptions, int nDbPort, QString strDbName)
{
		struct DBCN db_conn_data;

		//QTime timer_api;
		//timer_api.start();

		//B.T. Below code generates unique conn name as QSqlDatabase holds all connection within different instances, name check is essential
		//calculate random (unique) connection name
		int nCntTries=4;
		QString strName;
		while (nCntTries>0)
		{
			QString strRnd; 
			strRnd.setNum(qrand());
			strName = m_strDbType + QTime::currentTime().toString("hhmmsszzz") + strRnd+QString::number(ThreadIdentificator::GetCurrentThreadID());
			if (!QSqlDatabase::contains(strName))
				break;
			ThreadSleeper::Sleep(1); //sleep 1 msec before next try
			nCntTries--;
		}
		if (nCntTries==0)
		{
			//qDebug()<<"Reserving new DB connection failed";
			status.setError(StatusCodeSet::ERR_SQL_CONNECTION_RESERVATION_FAILED,"1;New DB connection name creation failed");
			return db_conn_data;
		}

		//int nReserveName= timer_api.elapsed();
		//timer_api.restart();
		//int nCreateDB=0,nOpenDB=0;

		QSqlDatabase *pDb = new QSqlDatabase(QSqlDatabase::addDatabase(strDbType, strName));
		if(pDb)
		{
			// initialize session
			if(!strDbHost.isEmpty())
				pDb->setHostName(strDbHost);
			if(!strDbUser.isEmpty())
				pDb->setUserName(strDbUser);
			if(!strDbPassword.isEmpty())
				pDb->setPassword(strDbPassword);
			if(!strDbOptions.isEmpty())
				pDb->setConnectOptions(strDbOptions);
			if(nDbPort > 0)
				pDb->setPort(nDbPort);
			if(!strDbName.isEmpty())
				pDb->setDatabaseName(strDbName);

			//nCreateDB= timer_api.elapsed();
			//timer_api.restart();

			//open session
			if(!pDb->open())
			{
				//fill error string
				QSqlError err = pDb->lastError();
				status.setError(StatusCodeSet::ERR_SQL_CONNECTION_RESERVATION_FAILED,QVariant(err.number()).toString()+";"+err.text());
				delete pDb;
				QSqlDatabase::removeDatabase (strName);	//removes connection
			}
			else
			{
				//nOpenDB= timer_api.elapsed();
				//timer_api.restart();

				//on success, add entry into the pool with locked status
				db_conn_data.strName = strName;
				db_conn_data.pDb	  = pDb;
				db_conn_data.bLocked = true;
				db_conn_data.nThreadID= ThreadIdentificator::GetCurrentThreadID();

				if (m_pTransManager)m_pTransManager->SetConnectionIsolationLevel(status,pDb);
				if (!status.IsOK())
				{
					delete pDb;
					QSqlDatabase::removeDatabase (strName);	//removes connection
				}
			}
		}
		else
		{
			status.setError(StatusCodeSet::ERR_SQL_CONNECTION_RESERVATION_FAILED,"1;New DB object creation failed");
			QSqlDatabase::removeDatabase (strName); //removes connection
		}


		//int nElapsed=timer_api.elapsed()+nReserveName+nCreateDB+nOpenDB;
		//if (nElapsed>100)
		//{
		//	QString strMsg=QString("CreateNewConnection, total elapse: %1, reserveName: %2, create DB: %3, open DB: %4, thread: %5").arg(nElapsed).arg(nReserveName).arg(nCreateDB).arg(nOpenDB).arg(ThreadIdentificator::GetCurrentThreadID());
		//	ApplicationLogger::logMessage(StatusCodeSet::TYPE_SPEED_TEST_LEVEL,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,strMsg);
		//}

		return db_conn_data;
}

//if CONNECTION_PERIOD_CHECK passed release unused connections
//use it from garbage collector...one thread...
void DbConnectionsPool::CheckConnections()
{
	//count unused/used connections
	m_mainPoolLock.lockForRead();
	int nMax = m_lstPool.size();
	int nLocked=0;
	for(int i=0; i<nMax; i++)
	{
		if(m_lstPool.at(i).bLocked)
			nLocked++;
	}
	m_mainPoolLock.unlock();


	if ((nMax-nLocked>CONNECTION_RELEASE_CHUNK) && ((m_nLastConnectionReservedCount-nLocked)>=0))
	{
		//release 20 unlocked:
		m_mainPoolLock.lockForWrite();
		int nMax = m_lstPool.size();
		int nUnlocked=0;
		for(int i=nMax-1; i>=m_nMinSize; i--)
		{
			if(!m_lstPool.at(i).bLocked)
			{
				delete m_lstPool.at(i).pDb;
				QSqlDatabase::removeDatabase (m_lstPool.at(i).strName);
				m_lstPool.removeAt(i);
				nUnlocked++;
				if (nUnlocked>=CONNECTION_RELEASE_CHUNK)
					break;
			}
		}
		m_mainPoolLock.unlock();

		QString strMsg=QString("CheckConnections, disconnected DB connections: %1, left in pool: %2").arg(nUnlocked).arg(m_lstPool.size());
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,strMsg);

	}
	m_nLastConnectionReservedCount=nLocked;
}