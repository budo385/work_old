#ifndef DBTRANSACTIONHANDLER_H
#define DBTRANSACTIONHANDLER_H

#include <QHash>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QMutex>
#include <QMutexLocker>
#include "common/common/status.h"



/*!
	\class DbTransactionHandler
	\brief Manages transactions, base class, for each DB, special implementation
	\ingroup SQLModule

	Keeps tracks of nested transactions. Initialize database session with predefined ISOLATION LEVEL.
	Most databases does not support nested transactions explictily, rather by SAVEPOINT mechanism.
	Different implementation for each database.
	Separated from SessionPool to enable higher concurency.

	Tracks all Db connections, used from DbConnectionsPool.
*/
class DbTransactionHandler
{
public:

	//tracks all active Db connections
	//void AddDbConnection(Status &pStatus,QSqlDatabase *pDbConnection);
	void SetConnectionIsolationLevel(Status &pStatus,QSqlDatabase *pDbConnection);
	void RemoveAllDbConnections();
	bool CheckDbConnection(QSqlDatabase *pDbConnection);
	void SetIsolationLevel(QString strLevel){m_StrIsolationLevel=strLevel;};

	//Nested transaction support:
	int ReserveTransaction(QSqlDatabase*);    
	int GetTransactionCount(QSqlDatabase*);   
	void ReleaseTransaction(QSqlDatabase*);   

	//Transaction manager (by default uses SAVEPOINT mechanism for nested transactions by SQL-99 standard
	//reimplement for each DB
	void BeginTransaction(Status &pStatus,QSqlDatabase*) ;
	void RollbackTransaction(Status &pStatus,QSqlDatabase*);
	void CommitTransaction(Status &pStatus,QSqlDatabase*);

protected:
	QMutex mutex;								///< for protecting list
	QString m_StrIsolationLevel;
	QHash<QSqlDatabase*,int> m_LstTransCounters;

};
#endif //DBTRANSACTIONHANDLER_H


