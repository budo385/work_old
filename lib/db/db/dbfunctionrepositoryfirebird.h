#ifndef DBFUNCTIONREPOSITORYFIREBIRD_H
#define DBFUNCTIONREPOSITORYFIREBIRD_H

#include "dbfunctionrepository.h"

/*!
	\class DbFunctionRepository
	\ingroup SQLModule
	\brief Abstract class for implementation of SQL functions


	For every database define own function that convert SQL string to proper one
*/
class DbFunctionRepositoryFireBird : public DbFunctionRepository
{

public:
	void PrepareLimit(QString &strSQL,int nFrom, int nTo);
	void PrepareHierarchySelect(QString &strSQL,QString strCode,QString strCodePrefix, bool bChildren=true);
	void PrepareCodeUpdate(QString &strSQL,QString strCode,QString strCodePrefix, int nOldLength);
	void PrepareGetParentCode(QString &strSQL,QString strCode,QString strCodePrefix,QString strTableName);

};

#endif // DBFUNCTIONREPOSITORYFIREBIRD_H
