#ifndef ACCESSRIGHTSET_H
#define ACCESSRIGHTSET_H

#include <QVector>

#include "db_core/db_core/dbrecordset.h"
#include "db_core/db_core/dbsqltableview.h"
#include "accessrightsid.h"
#include "accessright.h"

class AccessRightSet
{
public:
    AccessRightSet();
	AccessRightSet(DbRecordSet	  &AccessRightSetRecordSet);
	AccessRightSet(AccessRightSet &AccRightSet);
	void operator =(const AccessRightSet &that); 
    ~AccessRightSet();

	void AddAccessRight(int AccessRightID);
	void RemoveAccessRight(int AccessRightID);

	void GetARSRecordSet(DbRecordSet &ARSRecordSet);
	void GetARSAddList(QVector<int> &ARSAddList);
	void GetARSRemoveList(QVector<int> &ARSRemoveList);

private:
	QVector<int>	m_hshVersionAddAccessRights;
	QVector<int>	m_hshVersionRemoveAccessRights;
	DbRecordSet		m_recAccessRightSet;
};


#endif // ACCESSRIGHTSET_H
