#include "dbomanager.h"
#include <QSqlRecord>
#include <QObject>
#include <QStringList>
#include <QDebug>

//----------------------------------------------------
//				TABLE
//----------------------------------------------------

/*!
	Check If Exists Table (case insensitive)

	\param pDbConnection - db connection
	\param strTableName  - object name
	\return  1 if exists
*/
bool DbObjectManager::CheckIfExists_Table(QSqlDatabase *pDbConnection, QString strTableName)
{

	QStringList lstTables= pDbConnection->tables();
	if (!lstTables.contains(strTableName,Qt::CaseInsensitive))
		return false;
	else
		return true;
}

bool DbObjectManager::CheckIfExists_Column(QSqlDatabase *pDbConnection, QString strTableName,QString strColumnName)
{
	QSqlRecord rec=pDbConnection->record(strTableName);
	return rec.contains(strColumnName);
}

/*!
	Drop table if exists (case insensitive)

	\param pStatus		 - if statement fails
	\param pDbConnection - db connection
	\param strTableName  - object name
*/
void DbObjectManager::DropIfExists_Table(Status &pStatus, QSqlDatabase *pDbConnection, QString strTableName)
{

	pStatus.setError(0);
	//check if exists:
	if(!CheckIfExists_Table(pDbConnection,strTableName)) return;

	QString strSql = "DROP TABLE " + strTableName;

	QSqlQuery query(*pDbConnection);
	if(!query.exec(strSql))
	{
		qDebug()<<query.lastError().text();
		//error executing query
		pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, QObject::tr("Failed to delete table: ")+strTableName);
		//pStatus.setErrorDetails(query.lastError().text()); //SQL error details 
	}
}




//----------------------------------------------------
//				VIEW
//----------------------------------------------------


/*!
	Check If Exists View (case insensitive)

	\param pDbConnection - db connection
	\param strViewName  - object name
	\return  1 if exists
*/
bool DbObjectManager::CheckIfExists_View(QSqlDatabase *pDbConnection, QString strViewName)
{

	QStringList lstTables= pDbConnection->tables(QSql::Views);
	if (!lstTables.contains(strViewName,Qt::CaseInsensitive))
		return false;
	else
		return true;
}


/*!
	Drop view if exists (case insensitive)

	\param pStatus		 - if statement fails
	\param pDbConnection - db connection
	\param strViewName  - object name
*/
void DbObjectManager::DropIfExists_View(Status &pStatus, QSqlDatabase *pDbConnection, QString strViewName)
{

	pStatus.setError(0);
	//check if exists:
	if(!CheckIfExists_View(pDbConnection,strViewName)) return;

	QString strSql = "DROP VIEW " + strViewName;

	QSqlQuery query(*pDbConnection);
	if(!query.exec(strSql))
	{
		//error executing query
		pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, QObject::tr("Failed to delete view: ")+strViewName);
		//pStatus.setErrorDetails(query.lastError().text()); //SQL error details 
	}
}



//----------------------------------------------------
//				INDEX
//----------------------------------------------------



/*!
	Drop index if exists (case insensitive)

	\param pStatus		 - if statement fails
	\param pDbConnection - db connection
	\param strTableName	 - table name
	\param strIndexName  - object name
*/
void DbObjectManager::DropIfExists_Index(Status &pStatus, QSqlDatabase *pDbConnection, QString strTableName, QString strIndexName)
{

	pStatus.setError(0);
	//check if exists:
	if(!CheckIfExists_Index(pDbConnection,strTableName, strIndexName)) return;

	QString strSql = "DROP INDEX " + strIndexName;

	qDebug()<<strSql;

	QSqlQuery query(*pDbConnection);
	if(!query.exec(strSql))
	{
		//error executing query
		pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, QObject::tr("Failed to delete index: ")+strIndexName);
		//pStatus.setErrorDetails(query.lastError().text()); //SQL error details 
		//qDebug()<<query.lastError().text();
	}
}






//parse string: only <name,type, notnull>
bool DbObjectManager::GetTableColumnsFromString(QString strCreateTable,QHash<QString,QStringList> &lstColsData)
{
	int nX=strCreateTable.indexOf("(");
	QStringList lstCols=strCreateTable.mid(nX+1,strCreateTable.length()).split(",",QString::SkipEmptyParts,Qt::CaseInsensitive);

	lstCols.removeLast();

	//if decimal, parse all to find
	QStringList lstColsParsed;
	int nSize=lstCols.size();
	for(int i=0;i<nSize;++i)
	{
		QString strToken=lstCols.at(i).simplified();
		if (strToken.indexOf("DECIMAL(")>=0)
		{
			if (!strToken.contains(")")) //skip if all ok
			{
				if (i==nSize-1) //check range
					break;
				strToken+=","+lstCols.at(i+1);
				i++;
			}
		}
		lstColsParsed.append(strToken);
	}
	
	lstCols=lstColsParsed;

	nSize=lstCols.size();
	for(int i=0;i<nSize;++i)
	{
		QStringList lstNewColData;
		QString strCol=lstCols.at(i).trimmed();
		if (strCol.indexOf("not")>0)
		{
			strCol.replace("not null","");
			int nFirstSpace=strCol.indexOf(" ");
			lstNewColData<<strCol.left(nFirstSpace).trimmed();
			lstNewColData<<strCol.mid(nFirstSpace+1,strCol.length()).trimmed();
			lstNewColData<<"not null";
		}
		else
		{
			strCol.replace("null","");
			int nFirstSpace=strCol.indexOf(" ");
			lstNewColData<<strCol.left(nFirstSpace).trimmed();						//name
			lstNewColData<<strCol.mid(nFirstSpace+1,strCol.length()).trimmed();		//type
			lstNewColData<<"null";													//null
		}

		
		if (lstNewColData.at(0).isEmpty())
		{
			Q_ASSERT("Error in parsing table data");
		}

		lstColsData[lstNewColData.at(0)]=lstNewColData;
	}
	return true;
}





