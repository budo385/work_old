#ifndef DBCONNECTIONRESERVER_H
#define DBCONNECTIONRESERVER_H


#include "dbsqlmanager.h"
#include <QSqlDatabase>


/*!
	\class DbConnectionReserver
	\brief Local connection reserver: automatic release connection in destructor
	\ingroup SQLModule

	When sing multiple calls to different BO object it is to much overhead that every call reserve own connection.
	So use this reserve, instance it locally, connection will be reserverd if not alredy and released automatically
	when object is destroyed

*/
class DbConnectionReserver 
{
	

public:
    DbConnectionReserver(Status &err,DbSqlManager *DbManager);
    ~DbConnectionReserver();

private:
	DbSqlManager *m_DbManager;
	bool m_bConnectionReserverdHere;
	QSqlDatabase* m_pDbConn;

    
};

#endif // DBCONNECTIONRESERVER_H
