#ifndef DBFUNCTIONREPOSITORY_MYSQL_H
#define DBFUNCTIONREPOSITORY_MYSQL_H

#include "dbfunctionrepository.h"

/*!
	\class DbFunctionRepository
	\ingroup SQLModule
	\brief Abstract class for implementation of SQL functions


	For every database define own function that convert SQL string to proper one
*/
class DbFunctionRepositoryMySQL : public DbFunctionRepository
{

public:
	void PrepareLimit(QString &strSQL,int nFrom, int nTo);
	void PrepareHierarchySelect(QString &strSQL,QString strCode,QString strCodePrefix, bool bChildren=true);
	void PrepareCodeUpdate(QString &strSQL,QString strCode,QString strCodePrefix, int nOldLength);
	void PrepareGetParentCode(QString &strSQL,QString strCode,QString strCodePrefix,QString strTableName){};

};

#endif // DBFUNCTIONREPOSITORY_MYSQL_H
