#include "dbdatatypeconvertormysql.h"



/*!
	If types are different, converts database value to needed by recordset.
	For MYSQL, only DECIMAL needs convert to double

	\param databaseValue		- QVariant from database
	\param colTypeInRecordSet	- column type inside recordset
*/
QVariant DbDataTypeConvertorMySQL::ConvertValue(QVariant& databaseValue, int colTypeInRecordSet)
{

	//if same type, ok:
	if (databaseValue.type()==colTypeInRecordSet)return databaseValue;

	//qDebug()<<databaseValue.type();

	Q_ASSERT(colTypeInRecordSet==QVariant::Double); //only for double

	return databaseValue.toDouble();
}



