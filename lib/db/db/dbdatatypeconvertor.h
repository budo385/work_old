#ifndef DB_DATATYPECONVERTOR_H
#define DB_DATATYPECONVERTOR_H

#include <QSqlDatabase>
#include <QSqlRecord>
#include "common/common/status.h"
#include "common/common/dbrecordset.h"

/*!
	\class DbDataTypeConvertor
	\ingroup SQLModule
	\brief Abstract class for converting data types from database specific to QT QVariant types

*/

class DbDataTypeConvertor
{
public:

	QList<bool> IsConversionNeeded(DbRecordSet& lstView, QSqlRecord* record,bool& bConversionNeeded);
	virtual QVariant ConvertValue(QVariant& databaseValue, int colTypeInRecordSet)=0;
};

#endif //DB_DATATYPECONVERTOR_H


