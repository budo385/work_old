#ifndef DBTRANSACTIONHANDLER_FIREBIRD_H
#define DBTRANSACTIONHANDLER_FIREBIRD_H

#include "db/db/dbtransactionhandler.h"

/*!
	\class DbTransactionHandlerFireBird
	\brief Implementation of transaction handler for FireBird
	\ingroup SQLModule

	Firebird from 1.5ver supports SAVEPOINT mechanism for nested transaction.
	SNAPSHOT is default= same as REPEATABLE READ

	ISOLATION LEVEL determines how a transaction interacts with other simultaneous
	transactions accessing the same tables. The default ISOLATION LEVEL is SNAPSHOT. It
	provides a repeatable-read view of the database at the moment the transaction starts.
	Changes made by other simultaneous transactions are not visible.
	SNAPSHOT TABLE STABILITY provides a repeatable read of the database by ensuring that
	transactions cannot write to tables, though they may still be able to read from them.
	READ COMMITTED enables a transaction to see the most recently committed changes made
	by other simultaneous transactions. It can also update rows as long as no update conflict
	occurs. Uncommitted changes made by other transactions remain invisible until
	committed. READ COMMITTED also provides two optional parameters:
	g NO RECORD_VERSION, the default, reads only the latest version of a row. If the WAIT lock
	resolution option is specified, then the transaction waits until the latest version of a row
	is committed or rolled back, and retries its read.
	g RECORD_VERSION reads the latest committed version of a row, even if more recent
	uncommitted version also resides on disk.
	The RESERVING clause enables a transaction to register its desired level of access for
	specified tables when the transaction starts instead of when the transaction attempts its
	operations on that table. Reserving tables at transaction start can reduce the possibility
	of deadlocks.
	The USING clause, available only in SQL, can be used to conserve system resources by
	limiting the number of databases a transaction can access.
*/


class DbTransactionHandlerFireBird : public DbTransactionHandler
{
public:
	DbTransactionHandlerFireBird(){};
	//DbTransactionHandlerFireBird(){SetIsolationLevel("SET TRANSACTION ISOLATION LEVEL SNAPSHOT");};

    
};

#endif // DBTRANSACTIONHANDLER_FIREBIRD_H
