#include "accessrightsorganizer.h"

AccessRightsOrganizer::AccessRightsOrganizer()
{
	//Initialize Access Rights. 
	InitializeAccessRights();
	//Initialize Access Right Sets. 
	InitializeAccessRightSets();
	//Put a list of Access Rights to each Access Right Set. 
	InitializeARSRights();
}

AccessRightsOrganizer::~AccessRightsOrganizer()
{
	qDeleteAll(m_hshAccessRights);
	qDeleteAll(m_hshAccessRightSets);
}

void AccessRightsOrganizer::InitializeAccessRights()
{
	DbRecordSet AccessRightRecordSet;

	//********************************************************************
	//**							ACC_RIGHT_0							**
	//********************************************************************
	AccessRightRecordSet.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CORE_ACCESSRIGHTS));
	AccessRightRecordSet.addRow();											//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);								//ARS ID
	//AccessRightRecordSet.setData(0, 4, 0);								//AR Role ID.
	AccessRightRecordSet.setData(0, 5, "Access Right 0");					//AR Name.
	AccessRightRecordSet.setData(0, 6, 0);									//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 0);									//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");									//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, ACC_RIGHT_0);						//AR Code.

	m_hshAccessRights.insert(ACC_RIGHT_0, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**							ACC_RIGHT_1							**
	//********************************************************************
	AccessRightRecordSet.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CORE_ACCESSRIGHTS));
	AccessRightRecordSet.addRow();											//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);								//ARS ID
	//AccessRightRecordSet.setData(0, 4, 0);								//AR Role ID.
	AccessRightRecordSet.setData(0, 5, "Access Right 1");					//AR Name.
	AccessRightRecordSet.setData(0, 6, 0);									//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 0);									//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");									//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, ACC_RIGHT_1);						//AR Code.

	m_hshAccessRights.insert(ACC_RIGHT_1, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();

	//********************************************************************
	//**							ACC_RIGHT_2							**
	//********************************************************************
	AccessRightRecordSet.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CORE_ACCESSRIGHTS));
	AccessRightRecordSet.addRow();											//Skip ID, GLOBAL_ID and DAT_LAST_MODIFIED
	//AccessRightRecordSet.setData(0, 3, 0);								//ARS ID
	//AccessRightRecordSet.setData(0, 4, 0);								//AR Role ID.
	AccessRightRecordSet.setData(0, 5, "Access Right 2");					//AR Name.
	AccessRightRecordSet.setData(0, 6, 0);									//AR Type (0-Integer, 1-bool, 2-string).
	AccessRightRecordSet.setData(0, 7, 0);									//AR Value (Value of type if int or bool (0,1)).
	AccessRightRecordSet.setData(0, 8, "");									//AR Descry (Additional description. (For string type)).
	AccessRightRecordSet.setData(0, 9, ACC_RIGHT_2);						//AR Code.

	m_hshAccessRights.insert(ACC_RIGHT_2, new AccessRight(AccessRightRecordSet));
	AccessRightRecordSet.clear();
}

void AccessRightsOrganizer::InitializeAccessRightSets()
{
	DbRecordSet AccessRightSetRecordSet;

	//********************************************************************
	//**						ACC_RIGHT_SET_0							**
	//********************************************************************
	AccessRightSetRecordSet.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CORE_ACCRSET));
	AccessRightSetRecordSet.addRow();
	AccessRightSetRecordSet.setData(0, 3, "ACC_RIGHT_SET_0");				//ARS Name.
	AccessRightSetRecordSet.setData(0, 4, "ACC_RIGHT_SET_0 Description");	//ARS Description.
	AccessRightSetRecordSet.setData(0, 5, 0);								//ARS Type (0-system, 1-business).
	AccessRightSetRecordSet.setData(0, 5, ACC_RIGHT_SET_0);					//ARS Code.

	m_hshAccessRightSets.insert(ACC_RIGHT_SET_0, new AccessRightSet(AccessRightSetRecordSet));
	AccessRightSetRecordSet.clear();

	//********************************************************************
	//**						ACC_RIGHT_SET_1							**
	//********************************************************************
	AccessRightSetRecordSet.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CORE_ACCRSET));
	AccessRightSetRecordSet.addRow();
	AccessRightSetRecordSet.setData(0, 3, "ACC_RIGHT_SET_1");				//ARS Name.
	AccessRightSetRecordSet.setData(0, 4, "ACC_RIGHT_SET_1 Description");	//ARS Description.
	AccessRightSetRecordSet.setData(0, 5, 0);								//ARS Type (0-system, 1-business).
	AccessRightSetRecordSet.setData(0, 5, ACC_RIGHT_SET_1);					//ARS Code.

	m_hshAccessRightSets.insert(ACC_RIGHT_SET_1, new AccessRightSet(AccessRightSetRecordSet));
	AccessRightSetRecordSet.clear();

	//********************************************************************
	//**						ACC_RIGHT_SET_2							**
	//********************************************************************
	AccessRightSetRecordSet.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CORE_ACCRSET));
	AccessRightSetRecordSet.addRow();
	AccessRightSetRecordSet.setData(0, 3, "ACC_RIGHT_SET_2");				//ARS Name.
	AccessRightSetRecordSet.setData(0, 4, "ACC_RIGHT_SET_2 Description");	//ARS Description.
	AccessRightSetRecordSet.setData(0, 5, 0);								//ARS Type (0-system, 1-business).
	AccessRightSetRecordSet.setData(0, 5, ACC_RIGHT_SET_2);					//ARS Code.

	m_hshAccessRightSets.insert(ACC_RIGHT_SET_2, new AccessRightSet(AccessRightSetRecordSet));
	AccessRightSetRecordSet.clear();

	//********************************************************************
	//**						ACC_RIGHT_SET_3							**
	//********************************************************************
	AccessRightSetRecordSet.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CORE_ACCRSET));
	AccessRightSetRecordSet.addRow();
	AccessRightSetRecordSet.setData(0, 3, "ACC_RIGHT_SET_3");				//ARS Name.
	AccessRightSetRecordSet.setData(0, 4, "ACC_RIGHT_SET_3 Description");	//ARS Description.
	AccessRightSetRecordSet.setData(0, 5, 0);								//ARS Type (0-system, 1-business).
	AccessRightSetRecordSet.setData(0, 5, ACC_RIGHT_SET_3);					//ARS Code.

	m_hshAccessRightSets.insert(ACC_RIGHT_SET_3, new AccessRightSet(AccessRightSetRecordSet));
	AccessRightSetRecordSet.clear();

	//********************************************************************
	//**						ACC_RIGHT_SET_4							**
	//********************************************************************
	AccessRightSetRecordSet.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CORE_ACCRSET));
	AccessRightSetRecordSet.addRow();
	AccessRightSetRecordSet.setData(0, 3, "ACC_RIGHT_SET_4");				//ARS Name.
	AccessRightSetRecordSet.setData(0, 4, "ACC_RIGHT_SET_4 Description");	//ARS Description.
	AccessRightSetRecordSet.setData(0, 5, 0);								//ARS Type (0-system, 1-business).
	AccessRightSetRecordSet.setData(0, 5, ACC_RIGHT_SET_4);					//ARS Code.

	m_hshAccessRightSets.insert(ACC_RIGHT_SET_4, new AccessRightSet(AccessRightSetRecordSet));
	AccessRightSetRecordSet.clear();
}

void AccessRightsOrganizer::InitializeARSRights()
{
	//********************************************************************
	//**						ACC_RIGHT_SET_0							**
	//********************************************************************
	//Add access rights to access right set in version.
	m_hshAccessRightSets.value(ACC_RIGHT_SET_0)->AddAccessRight(ACC_RIGHT_0);
	m_hshAccessRightSets.value(ACC_RIGHT_SET_0)->AddAccessRight(ACC_RIGHT_1);
	m_hshAccessRightSets.value(ACC_RIGHT_SET_0)->AddAccessRight(ACC_RIGHT_2);

	//Remove access rights from access right set in version - just for example.
	//m_hshAccessRightSets.value(ACC_RIGHT_SET_0)->RemoveAccessRight(ARS_VERSION_0, ACC_RIGHT_2);

	//********************************************************************
	//**						ACC_RIGHT_SET_1							**
	//********************************************************************
	//Add access rights to access right set in version.
	m_hshAccessRightSets.value(ACC_RIGHT_SET_1)->AddAccessRight(ACC_RIGHT_0);
	m_hshAccessRightSets.value(ACC_RIGHT_SET_1)->AddAccessRight(ACC_RIGHT_1);
	m_hshAccessRightSets.value(ACC_RIGHT_SET_1)->AddAccessRight(ACC_RIGHT_2);

	//********************************************************************
	//**						ACC_RIGHT_SET_2							**
	//********************************************************************
	//Add access rights to access right set in version.
	m_hshAccessRightSets.value(ACC_RIGHT_SET_2)->AddAccessRight(ACC_RIGHT_0);
	m_hshAccessRightSets.value(ACC_RIGHT_SET_2)->AddAccessRight(ACC_RIGHT_1);
	m_hshAccessRightSets.value(ACC_RIGHT_SET_2)->AddAccessRight(ACC_RIGHT_2);

	//********************************************************************
	//**						ACC_RIGHT_SET_3							**
	//********************************************************************
	//Add access rights to access right set in version.
	m_hshAccessRightSets.value(ACC_RIGHT_SET_3)->AddAccessRight(ACC_RIGHT_0);
	m_hshAccessRightSets.value(ACC_RIGHT_SET_3)->AddAccessRight(ACC_RIGHT_1);
	m_hshAccessRightSets.value(ACC_RIGHT_SET_3)->AddAccessRight(ACC_RIGHT_2);

	//********************************************************************
	//**						ACC_RIGHT_SET_4							**
	//********************************************************************
	//Add access rights to access right set in version.
	m_hshAccessRightSets.value(ACC_RIGHT_SET_4)->AddAccessRight(ACC_RIGHT_0);
	m_hshAccessRightSets.value(ACC_RIGHT_SET_4)->AddAccessRight(ACC_RIGHT_1);
	m_hshAccessRightSets.value(ACC_RIGHT_SET_4)->AddAccessRight(ACC_RIGHT_2);
}

AccessRight* AccessRightsOrganizer::GetAccessRight(int AccessRightID)
{
	return m_hshAccessRights.value(AccessRightID);
}

AccessRightSet* AccessRightsOrganizer::GetAccessRightSet(int AccessRightSetID)
{
	return m_hshAccessRightSets.value(AccessRightSetID);
}


