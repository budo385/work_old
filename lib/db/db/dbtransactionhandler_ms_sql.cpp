#include "dbtransactionhandler_ms_sql.h"

/*!
	Reservers transaction: MS SQL allows explict nested transactions
	
	\param pStatus			- if something goes wrong
	\param pDbConnection	- pDbConnection connection to add (tracked by unique name)

*/
void DbTransactionHandlerMS_SQL::BeginTransaction(Status &pStatus, QSqlDatabase* pDbConnection) 
{

	pStatus.setError(StatusCodeSet::ERR_NONE);
	int nTranCount=ReserveTransaction(pDbConnection);
	
	if (!pDbConnection->transaction()) pStatus.setError(StatusCodeSet::ERR_SQL_TRANSACTION_FAIL);

}


/*!
	Rollbacks transaction, MS SQL allows explict nested transactions, rollbacks latest
	
	\param pStatus			- if something goes wrong
	\param pDbConnection	- pDbConnection connection to add (tracked by unique name)

*/
void DbTransactionHandlerMS_SQL::RollbackTransaction(Status &pStatus, QSqlDatabase* pDbConnection) 
{

	pStatus.setError(StatusCodeSet::ERR_NONE);

	int nTranCount=GetTransactionCount(pDbConnection);
	
	if (!pDbConnection->rollback()){ pStatus.setError(StatusCodeSet::ERR_SQL_TRANSACTION_FAIL); return;}

	//decrease counter:
	ReleaseTransaction(pDbConnection);

}



/*!
	Commits transaction, MS SQL allows explict nested transactions, commit latest
	
	\param pStatus			- if something goes wrong
	\param pDbConnection	- pDbConnection connection to add (tracked by unique name)

*/
void DbTransactionHandlerMS_SQL::CommitTransaction(Status &pStatus, QSqlDatabase* pDbConnection) 
{

	pStatus.setError(StatusCodeSet::ERR_NONE);

	int nTranCount=GetTransactionCount(pDbConnection);
	
	if (!pDbConnection->commit()) { pStatus.setError(StatusCodeSet::ERR_SQL_TRANSACTION_FAIL); return;}
	
	//decrease counter:
	ReleaseTransaction(pDbConnection);

}



