#ifndef DBOBJECTMANAGER_FIREBIRD_H
#define DBOBJECTMANAGER_FIREBIRD_H


#include <QSqlRecord>
#include "dbomanager.h"


/*!
	\class DbObjectManager_FireBird
	\brief For managing Db objects (drop, create, etc..) for firebird
	\ingroup Db_Actualize

*/
class DbObjectManager_FireBird: public DbObjectManager
{
public:

	DbObjectManager_FireBird(DbConnectionSettings &DbSettings):DbObjectManager(DbSettings){};

	void	DropAllTriggers(Status &pStatus, QSqlDatabase *pDbConnection);
	void	DropAllConstraints(Status &pStatus, QSqlDatabase *pDbConnection);
	void	DropAllIndexes(Status &pStatus, QString strTableName,QSqlDatabase *pDbConnection);

	bool CheckIfExists_Index(QSqlDatabase *pDbConnection, QString strTableName, QString strIndexName);
	void DropIfExists_Index(Status &pStatus, QSqlDatabase *pDbConnection, QString strTableName, QString strIndexName);

	bool CheckIfExists_Trigger(QSqlDatabase *pDbConnection, QString strTriggerName);
	void DropIfExists_Trigger(Status &pStatus, QSqlDatabase *pDbConnection, QString strTriggerName);

	// no sequences for mySQL, fire ASSERT, are you sure u know what are u doing?
	bool CheckIfExists_Sequence(QSqlDatabase *pDbConnection, QString strSeqName);
	void DropIfExists_Sequence(Status &pStatus, QSqlDatabase *pDbConnection, QString strSeqName);
	void CreateSequence(Status &pStatus, QSqlDatabase *pDbConnection, QString strSeqName, int nStartCounter=1);

	bool CheckIfExists_Proc(QSqlDatabase *pDbConnection, QString strProcName);
	void DropIfExists_Proc(Status &pStatus, QSqlDatabase *pDbConnection, QString strProcName);

	bool CheckIfExists_Funct(QSqlDatabase *pDbConnection, QString strFunctName);
	void DropIfExists_Funct(Status &pStatus, QSqlDatabase *pDbConnection, QString strFunctName);

	void CreateAutoIncrementID(Status &pStatus, QSqlDatabase *pDbConnection,QString strTableName, QString strAutoIncFieldName);
	void AdjustAutoIncrementAfterDbCopy(Status &pStatus, QSqlDatabase *pDbConnection,QString strTableName, QString strAutoIncFieldName);

	void	AlterTable(Status &pStatus, QString strTableName, QString strTableCreationStatement,QStringList lstDeletedColumns, QList<QStringList> lstChangedNameColumns,QSqlDatabase *pDbConnection,bool bFillDefaultValuesForNewFields=false);
	void	DropColumns(Status &pStatus, QString strTableName, QStringList lstDeletedColumns, QSqlDatabase *pDbConnection);
	QString	BackupDatabase(Status &pStatus, QString strDatabaseName, QString strUser, QString strPassword,QString &strDumpFilePath,QString strDatabaseVersion,QString strHost, QString strPort,QSqlDatabase *pDbConnection);
	void	RestoreDatabase(Status &pStatus, QString strDatabaseName, QString strUser, QString strPassword,QString strDumpFilePath,QString strHost, QString strPort,QSqlDatabase *pDbConnection);

	// no storage spaces for firebird:
	QString	GetTableSpace(){ return "";}
	QString	GetIndexSpace(){ return "";}

	void	PrepareDemoDatabase(Status &pStatus, QSqlDatabase *pDbConnection);
	void	ClearDemoDatabase(Status &pStatus, QSqlDatabase *pDbConnection);

	void	CreateReadOnlyUser_Level1(Status &pStatus, QString strUserName, QString strPassword, QString strUserFirstName, QString strUserLastName, QString strMasterKey, QString strMasterPW, QSqlDatabase *pDbConnection);


protected:
	virtual bool GetTableColumnsFromDb(QString strTableName,QHash<QString,QStringList> &lstColsData,QSqlDatabase *pDbConnection);

};


#endif // DBOBJECTMANAGER_FIREBIRD_H
