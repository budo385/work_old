#ifndef DBCURRENTTIME_ORACLE_H
#define DBCURRENTTIME_ORACLE_H

#include "dbcurrenttime.h"

/*!
	\class DbCurrentTime_Oracle
	\ingroup SQLModule
	\brief Abstract class for implementation of currenttime() function for ORACLE

*/
class DbCurrentTime_Oracle : public DbCurrentTime
{
public:
	
	QDateTime GetCurrentDateTime(Status &status, QSqlDatabase *pDbConnection);
    
};

#endif // DBCURRENTTIME_ORACLE_H


