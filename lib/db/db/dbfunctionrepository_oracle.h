#ifndef DBFUNCTIONREPOSITORY_ORACLE_H
#define DBFUNCTIONREPOSITORY_ORACLE_H

#include "dbfunctionrepository.h"

class DbFunctionRepository_Oracle : public DbFunctionRepository
{
public:
	void PrepareLimit(QString &strSQL,int nFrom, int nTo);
	void PrepareHierarchySelect(QString &strSQL,QString strCode,QString strCodePrefix, bool bChildren=true);
	void PrepareCodeUpdate(QString &strSQL,QString strCode,QString strCodePrefix, int nOldLength);
	void PrepareGetParentCode(QString &strSQL,QString strCode,QString strCodePrefix,QString strTableName){};
	
};

#endif // DBFUNCTIONREPOSITORY_ORACLE_H
