#ifndef DBOBJMANAGER_H
#define DBOBJMANAGER_H

#include <QString>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include "common/common/status.h"
#include "db_core/db_core/dbconnsettings.h"



/*!
	\class DbObjectManager
	\brief Abstract class for managing Db objects (drop, create, etc..)
	\ingroup Db_Actualize

	Inherit this class and implement methods for each Database separately
	Note: Table handling is automatic (solved by QT)
	This methods are common to all databases, and they are implemented here:
	- CheckIfExists_Table()
	- DropIfExists_Table()
	- DropIfExists_Index()

*/
class DbObjectManager
{
public:

	//DB User, Table & Index space (probably needed only for DB2 & ORACLE)
	DbObjectManager(DbConnectionSettings& DbSettings):mDbSettings(DbSettings){};

	virtual void	DropAllTriggers(Status &pStatus, QSqlDatabase *pDbConnection)=0;
	virtual void	DropAllConstraints(Status &pStatus, QSqlDatabase *pDbConnection)=0;
	virtual void	DropAllIndexes(Status &pStatus, QString strTableName,QSqlDatabase *pDbConnection)=0;

	virtual bool	CheckIfExists_Table(QSqlDatabase *pDbConnection, QString strTableName);
	virtual void	DropIfExists_Table(Status &pStatus, QSqlDatabase *pDbConnection, QString strTableName);
	virtual bool	CheckIfExists_Column(QSqlDatabase *pDbConnection, QString strTableName,QString strColumnName);

	virtual bool	CheckIfExists_View(QSqlDatabase *pDbConnection, QString strViewName);
	virtual void	DropIfExists_View(Status &pStatus, QSqlDatabase *pDbConnection, QString strViewName);

	virtual bool	CheckIfExists_Index(QSqlDatabase *pDbConnection, QString strTableName, QString strIndexName)=0;
	virtual void	DropIfExists_Index(Status &pStatus, QSqlDatabase *pDbConnection, QString strTableName, QString strIndexName);

	virtual bool	CheckIfExists_Trigger(QSqlDatabase *pDbConnection, QString strTriggerName)=0;
	virtual void	DropIfExists_Trigger(Status &pStatus, QSqlDatabase *pDbConnection, QString strTriggerName)=0;

	virtual bool	CheckIfExists_Proc(QSqlDatabase *pDbConnection, QString strProcName)=0;
	virtual void	DropIfExists_Proc(Status &pStatus, QSqlDatabase *pDbConnection, QString strProcName)=0;

	virtual bool	CheckIfExists_Funct(QSqlDatabase *pDbConnection, QString strFunctName)=0;
	virtual void	DropIfExists_Funct(Status &pStatus, QSqlDatabase *pDbConnection, QString strFunctName)=0;

	virtual bool	CheckIfExists_Sequence(QSqlDatabase *pDbConnection, QString strSeqName)=0;
	virtual void	DropIfExists_Sequence(Status &pStatus, QSqlDatabase *pDbConnection, QString strSeqName)=0;
	virtual void	CreateSequence(Status &pStatus, QSqlDatabase *pDbConnection, QString strSeqName, int nStartCounter=1)=0;

	//auto update table:
	virtual void	AlterTable(Status &pStatus, QString strTableName, QString strTableCreationStatement,QStringList lstDeletedColumns, QList<QStringList> lstChangedNameColumns,QSqlDatabase *pDbConnection,bool bFillDefaultValuesForNewFields=false)=0;
	virtual void	DropColumns(Status &pStatus, QString strTableName, QStringList lstDeletedColumns, QSqlDatabase *pDbConnection)=0;


	//auto ID: (for DB2 & ORACLE)
	virtual void	CreateAutoIncrementID(Status &pStatus, QSqlDatabase *pDbConnection,QString strTableName, QString strAutoIncFieldName)=0;
	virtual void	AdjustAutoIncrementAfterDbCopy(Status &pStatus, QSqlDatabase *pDbConnection,QString strTableName, QString strAutoIncFieldName)=0;

	//return SQL appendix for storing in specified storage space
	virtual QString	GetTableSpace()=0;
	virtual QString	GetIndexSpace()=0;

	virtual QString	BackupDatabase(Status &pStatus, QString strDatabaseName, QString strUser, QString strPassword,QString &strDumpFilePath,QString strDatabaseVersion,QString strHost, QString strPort,QSqlDatabase *pDbConnection)=0;
	virtual void	RestoreDatabase(Status &pStatus, QString strDatabaseName, QString strUser, QString strPassword,QString strDumpFilePath,QString strHost, QString strPort,QSqlDatabase *pDbConnection)=0;

	virtual void	PrepareDemoDatabase(Status &pStatus, QSqlDatabase *pDbConnection)=0;
	virtual void	ClearDemoDatabase(Status &pStatus, QSqlDatabase *pDbConnection)=0;

	virtual void	CreateReadOnlyUser_Level1(Status &pStatus, QString strUserName, QString strPassword, QString strUserFirstName, QString strUserLastName, QString strMasterKey, QString strMasterPW, QSqlDatabase *pDbConnection)=0;

protected:
	//QString m_strDatabaseUser;		///< database user, needed for Oracle specific operations
	//QString m_strTableSpace;		///< table space
	//QString m_strIndexSpace;		///< index space
	DbConnectionSettings mDbSettings;

	virtual bool GetTableColumnsFromString(QString strCreateTable,QHash<QString,QStringList> &lstColsData);
	virtual bool GetTableColumnsFromDb(QString strTableName,QHash<QString,QStringList> &lstColsData,QSqlDatabase *pDbConnection)=0;

};



#endif //DBOBJMANAGER_H


