#include "dbconnectionreserver.h"

DbConnectionReserver::DbConnectionReserver(Status &err,DbSqlManager *DbManager)
{
	m_pDbConn = DbManager->GetThreadDbConnection();
	if(m_pDbConn==NULL)
	{
		m_pDbConn=DbManager->ReserveConnection(err);
		if (!err.IsOK())return;
		m_bConnectionReserverdHere=true;
		m_DbManager=DbManager;
	}
	else
		m_bConnectionReserverdHere=false;
}

DbConnectionReserver::~DbConnectionReserver()
{
	if(m_bConnectionReserverdHere)
		m_DbManager->ReleaseConnection(m_pDbConn);
}
