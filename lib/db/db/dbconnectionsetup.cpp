#include "dbconnectionsetup.h"
#include <QDir>
#include <QFile>
#include <QStringList>
#include <QCoreApplication>
#include <QProcess>

#include "common/common/datahelper.h"
#include "db_core/db_core/dbconnectionslist.h"
#include "common/common/config.h"
#include "db_core/db_core/db_drivers.h"

DbConnectionSetup::DbConnectionSetup()
{

}

DbConnectionSetup::~DbConnectionSetup()
{

}


//true: database is copied, connection setup, else nothing to do
bool DbConnectionSetup::SetupThickDatabase(Status &pStatus, QString& strConnectionName)
{
	pStatus.setError(0);

	//check if exists:
	bool bCreated=false;
	QString strDataDir=DataHelper::GetApplicationHomeDir()+"/data";
	QDir setDir(strDataDir);
	if (!setDir.exists())
	{
		QString strAppDir=DataHelper::GetApplicationHomeDir();
		QDir bcpDirCreate(strAppDir);
		if(bcpDirCreate.mkdir("data"))
		{
			bCreated=true;
			strDataDir=strAppDir+"/data";
		}
		else
		{
			pStatus.setError(1,QObject::tr("Data directory:")+strAppDir+"/data"+QObject::tr(" could not be created!"));
			return false;
		}
	}

	//means that exists:
	if (!bCreated)
		return false;

	
	//if not, just copy from template to that dir:
	if(!DataHelper::CopyFolderContent(QCoreApplication::applicationDirPath()+"/data",strDataDir))
	{
		pStatus.setError(1,QObject::tr("Failed to initialize database!"));
		return false;
	}

	//create new connection:
	//settings:
	QString strSettingsPath=DataHelper::GetApplicationHomeDir()+"/settings/database_connections.cfg";

	DbConnectionSettings settingsFireBird;

	settingsFireBird.m_strCustomName="DEFAULT";
	settingsFireBird.m_strDbName=QDir::toNativeSeparators(strDataDir+"/DATA.FDB");
	settingsFireBird.m_strDbUserName="SYSDBA";
	settingsFireBird.m_strDbPassword="masterkey";
	settingsFireBird.m_strDbType=DBTYPE_FIREBIRD;



	//set it as default:
	int nTries=0;
	strConnectionName="DEFAULT";
	while (nTries<4)
	{

		if(CreateDatabaseConnection(pStatus,strSettingsPath,settingsFireBird))
			return true;
		strConnectionName="DEFAULT_"+QVariant(nTries).toString();
		nTries++;
	}

	return false;

}


//false if connection with same name already exists, else, all ok
bool DbConnectionSetup::CreateDatabaseConnection(Status &pStatus,QString strFile,DbConnectionSettings settings)
{
	//setup connection:
	DbConnectionsList m_objDataDb;
	if(m_objDataDb.Load(strFile,MASTER_PASSWORD))
	{
		int nCnt = m_objDataDb.m_lstConnections.size();
		for(int i=0; i<nCnt; i++)
			if (m_objDataDb.m_lstConnections[i].m_strCustomName==settings.m_strCustomName)
				return false;

//		pStatus.setError(1,QObject::tr("Failed to load settings file:!")+strFile);
//		return false;
	}



	m_objDataDb.m_lstConnections.append(settings);


	if(!m_objDataDb.Save(strFile,MASTER_PASSWORD))
	{
		pStatus.setError(1,QObject::tr("Failed to save settings file:")+strFile);
		return false;
	}

	return true;
}

bool DbConnectionSetup::GetDatabaseConnection(Status &pStatus,QString strFile,QString strConnName,DbConnectionSettings &settings)
{
	//setup connection:
	DbConnectionsList m_objDataDb;
	if(m_objDataDb.Load(strFile,MASTER_PASSWORD))
	{
		int nCnt = m_objDataDb.m_lstConnections.size();
		for(int i=0; i<nCnt; i++)
			if (m_objDataDb.m_lstConnections[i].m_strCustomName==strConnName)
			{
				settings=m_objDataDb.m_lstConnections[i];
				return true;
			}
	}
	return false;
}

bool DbConnectionSetup::SetupAppServerDatabase(Status &pStatus,QString strUser,QString strPass,QString strDbType,QString strDatabaseName,QString& strConnectionName,QString strDirPath,QString strDbConnectionFilePath)
{

	pStatus.setError(0);

	//check if exists:
	bool bCreated=false;
	QString strDataDir=QCoreApplication::applicationDirPath()+"/data";
	if (strDirPath.isEmpty())
	{
		QDir setDir(strDataDir);
		if (!setDir.exists())
		{
			QString strAppDir=QCoreApplication::applicationDirPath();
			QDir bcpDirCreate(strAppDir);
			if(bcpDirCreate.mkdir("data"))
			{
				bCreated=true;
				strDataDir=strAppDir+"/data";
			}
			else
			{
				pStatus.setError(1,QObject::tr("Data directory:")+strAppDir+"/data"+QObject::tr(" could not be created!"));
				return false;
			}
		}
	}
	else
		strDataDir=strDirPath;
	

	//means that exists:
	//if (!bCreated)
	//	return false;

	QString strDatabasePath;

	//nDbType
	if (strDbType==DBTYPE_FIREBIRD)
	{
		strDatabasePath=strDataDir+"/"+strDatabaseName+".FDB";
		CreateFireBirdDatabase(pStatus,strUser,strPass,strDatabasePath);
		if (!pStatus.IsOK())
			return false;
	}
	else if (strDbType==DBTYPE_MYSQL)
	{
		strDatabasePath=strDatabaseName;
		CreateMySQLDatabase(pStatus,strUser,strPass,strDatabasePath);
		if (!pStatus.IsOK())
			return false;
	}

	//create new connection:
	//settings:
	QString strSettingsPath=QCoreApplication::applicationDirPath()+"/settings/database_connections.cfg";
	if (!strDbConnectionFilePath.isEmpty())
		strSettingsPath=strDbConnectionFilePath;

	DbConnectionSettings settingsFireBird;

	settingsFireBird.m_strCustomName=strDatabaseName;
	settingsFireBird.m_strDbName=QDir::toNativeSeparators(strDatabasePath);
	settingsFireBird.m_strDbUserName=strUser;
	settingsFireBird.m_strDbPassword=strPass;
	settingsFireBird.m_strDbType=strDbType;



	//set it as default:
	int nTries=0;
	strConnectionName=strDatabaseName;
	while (nTries<4)
	{

		if(CreateDatabaseConnection(pStatus,strSettingsPath,settingsFireBird))
			return true;
		strConnectionName=strDatabaseName+QVariant(nTries).toString();
		nTries++;
	}

	//pStatus.setError(1,QObject::tr("Failed to setup database connection!"));
	return false;
}



void DbConnectionSetup::CreateFireBirdDatabase(Status &pStatus,QString strUser,QString strPass,QString strDatabasePath)
{
	strDatabasePath=QDir::toNativeSeparators(strDatabasePath);
	QString strExec="CREATE DATABASE '"+strDatabasePath+"' user '"+strUser+"' password '"+strPass+"' DEFAULT CHARACTER SET UTF8;";

	QFile file(QDir::tempPath()+"/fire.sql");
	if (file.exists())
		file.remove();

	if(file.open(QIODevice::WriteOnly | QIODevice::Text))
	{
		file.write(strExec.toLatin1());
		file.close();
	}
	else
	{
		pStatus.setError(1,QObject::tr("Failed to create fire.sql script"));
		return;
	}


	//exec:
	pStatus.setError(0);
	QStringList lstArgs;
	lstArgs<<"-q";
	lstArgs<<"-i";
	lstArgs<<QDir::tempPath()+"/fire.sql";

	QProcess app;
	QString strDir=QCoreApplication::applicationDirPath()+"/firebird/bin";
	app.setWorkingDirectory(strDir);
	app.start(strDir+"/isql.exe",lstArgs);
	if (!app.waitForFinished(-1)) //wait forever
	{		
		pStatus.setError(1,QObject::tr("Failed to execute isql.exe. Make sure it is in /firebird application subdirectory!"));
		return;
	}
	int nExitCode=app.exitCode();
	QByteArray errContent=app.readAllStandardError();
	if (errContent.size()!=0)
	{
		pStatus.setError(1,QObject::tr("Failed to create database! Reason:"+errContent.left(4095)));
		return;
	}


	file.remove();
}


void DbConnectionSetup::CreateMySQLDatabase(Status &pStatus,QString strUser,QString strPass,QString strDatabaseName)
{

	QString strExec="CREATE DATABASE "+strDatabaseName+";";

	//QFile file("mysql.sql");
	QFile file(QCoreApplication::applicationDirPath()+"/mysql.sql");
	if (file.exists())
		file.remove();

	if(file.open(QIODevice::WriteOnly | QIODevice::Text))
	{
		file.write(strExec.toLatin1());
		file.close();
	}
	else
	{
		pStatus.setError(1,QObject::tr("Failed to create mysql.sql script"));
		return;
	}


	//exec:
	pStatus.setError(0);
	QStringList lstArgs;
	lstArgs<<"-u";
	lstArgs<<strUser;
	lstArgs<<"-p"+strPass;

	QProcess app;
	app.setWorkingDirectory(QCoreApplication::applicationDirPath());
	app.setStandardInputFile("mysql.sql");
	app.start("mysql",lstArgs);
	if (!app.waitForFinished(-1)) //wait forever
	{		
		pStatus.setError(1,QObject::tr("Failed to execute mysql! Check paths!"));
		return;
	}
	int nExitCode=app.exitCode();
	QByteArray errContent=app.readAllStandardError();
	if (errContent.size()!=0)
	{
		pStatus.setError(1,QObject::tr("Failed to create database! Reason:"+errContent.left(4095)));
		return;
	}


	file.remove();



}