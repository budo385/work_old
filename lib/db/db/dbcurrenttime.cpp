#include "dbcurrenttime.h"



/*!
	Get current date & time from database server.
	Note if failed it will return current date time

	\return current time
*/

QDateTime DbCurrentTime::GetCurrentDateTime(Status &status, QSqlDatabase *pDbConnection)
{
	return QDateTime::currentDateTime(); //no need to fetch from server as appserver and db server MUST reside in same TIMEZONE!!
	/*
	QString strSql = "SELECT CURRENT_TIMESTAMP";
	QSqlQuery query(*pDbConnection);

	if(!query.exec(strSql))
	{
		//error executing query
		status.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL,QObject::tr("Failed to fetch current time from database"));
		return QDateTime::currentDateTime();
	}

	//fetch ID value
	query.next();
	return DateTimeHandler::toUTC(query.value(0).toDateTime());
	*/
}


QString DbCurrentTime::GetCurrentDateTime()
{
	return "'"+QDateTime::currentDateTime().toString("yyyy-MM-dd HH.mm.ss")+"'";
	//return "CURRENT_TIMESTAMP";
}