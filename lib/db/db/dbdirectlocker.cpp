#include "dbdirectlocker.h"
#include <QVariant>
#include <QStringList>

/*!
	Lock records using database engine (SHARED locks). Lock records in chunks. If one chunk fail then it rollback.
	Dont use transaction because locking inside transaction on DB is whole new story..

	\param status		 - return error 
	\param pDbConnection - connection
	\param strTableName	 - table name
	\param strPrimaryKey - pk name
	\param lstRecords	 - list of record id to be locked (primary key of table)
	\return				 - number of actually locked records
*/
int DbDirectLocker::LockRecords(Status &status, QSqlDatabase *pDbConnection, QString strTableName,QString strPrimaryKey, const DbLockList& lstRecords)
{
	int nChunk = 200,i;	//chunks of 200 max
	status.setError(0);
	QString strWhere,strSQL;
	QStringList rollbackList;

	int nRowsLocked=0;

	//prepare select & update SQL's
	QString strSqlSelect = "SELECT " + strPrimaryKey + " FROM "+ strTableName;
	QString strSqlUpdate = "UPDATE " + strTableName+ " SET "+ strPrimaryKey + " = "+strPrimaryKey;

	//init query
	QSqlQuery query(*pDbConnection);

	int nSize=lstRecords.size();
	for(i=0;i<nSize;++i)
	{
		if(strWhere.isEmpty())
			strWhere = " WHERE "+strPrimaryKey+" IN ("+QVariant(lstRecords.at(i)).toString();
		else
			strWhere+= ","+QVariant(lstRecords.at(i)).toString();

		//execute chunk:
		if(i>nChunk||i==lstRecords.size()-1)
		{
			strSQL=strSqlSelect+strWhere+") WITH LOCK";	//key SQL for record locking, can differ from DB to DB
			nChunk+=200; //next 200
			if(!query.exec(strSQL))
			{
				//qDebug()<<query.lastError().text();

				//roll back chunks, dont check up, just try...
				for(int j=0;j<rollbackList.size();++j)
					query.exec(rollbackList.at(j));
				//error executing query
				status.setError(StatusCodeSet::ERR_SQL_LOCKING_FAIL);
				return 0;
			}
			int nRowsAffected=query.size();
			if (nRowsAffected==-1)
				nRowsLocked+=i+1+200-nChunk;//heh who understand this....
			else
				nRowsLocked+=nRowsAffected;

			rollbackList.append(strSqlUpdate+strWhere+")");
			strWhere="";
		}
	}

	return nRowsLocked;

}


/*!
	UnLock records: just perform self update on PK!!

	\param status		 - return error 
	\param pDbConnection - connection
	\param strTableName	 - table name
	\param strPrimaryKey - pk name
	\param lstRecords	 - list of record id to be locked (primary key of table)
*/
void DbDirectLocker::UnLockRecords(Status &status, QSqlDatabase *pDbConnection, QString strTableName,QString strPrimaryKey, const DbLockList &lstRecords)
{
	int nChunk = 200,i;	//chunks of 200 max
	status.setError(0);
	QString strWhere,strSQL;

	//prepare select & update SQL's
	QString strSqlUpdate = "UPDATE " + strTableName+ " SET "+ strPrimaryKey + " = "+strPrimaryKey;

	//init query
	QSqlQuery query(*pDbConnection);

	for(i=0;i<lstRecords.size();++i)
	{
		if(strWhere.isEmpty())
			strWhere = " WHERE "+strPrimaryKey+" IN ("+QVariant(lstRecords.at(i)).toString();
		else
			strWhere+= ","+QVariant(lstRecords.at(i)).toString();

		//execute chunk:
		if(i>nChunk||i==lstRecords.size()-1)
		{
			strSQL=strSqlUpdate+strWhere+") ";	//key SQL for record locking, can differ from DB to DB
			nChunk+=200; //next 200
			if(!query.exec(strSQL))
			{
				//error executing query
				status.setError(StatusCodeSet::ERR_SQL_UNLOCKING_FAIL);
				return;
			}
			strWhere="";
		}
	}


}


