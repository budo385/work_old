#include "dbsqlmanager.h"
#include "db_core/db_core/dbsqltabledefinition.h"


DbSqlManager::DbSqlManager()
:m_DirectRecordLocker(NULL),
m_DbAutoIdManager(NULL),
m_DbTransactionManager(NULL),
m_DbDataTypeConvertor(NULL),
m_Time(NULL),
m_SqlError(NULL),
m_SqlFunctions(NULL)
{

}

//free memory
DbSqlManager::~DbSqlManager()
{
	DestroyHandlers();
}

void DbSqlManager::DestroyHandlers()
{
	if(m_DirectRecordLocker!=NULL) delete m_DirectRecordLocker;
	if(m_DbAutoIdManager!=NULL)delete m_DbAutoIdManager;
	if(m_DbDataTypeConvertor!=NULL)delete m_DbDataTypeConvertor;
	if(m_Time!=NULL)delete m_Time;
	if(m_SqlError!=NULL)delete m_SqlError;
	if(m_SqlFunctions!=NULL)delete m_SqlFunctions;
	if(m_DbTransactionManager!=NULL)delete m_DbTransactionManager;
	SetTransactionManager(NULL);

	m_DbTransactionManager=NULL;
	m_DirectRecordLocker=NULL;
	m_DbAutoIdManager=NULL;
	m_DbDataTypeConvertor=NULL;
	m_Time=NULL;
	m_SqlError=NULL;
	m_SqlFunctions=NULL;
}

//if DefPoolSize=0 then maximum number of DB connection is not limited!!!
void DbSqlManager::Initialize(Status &pStatus, DbConnectionSettings &ConnectionSettings, int nPoolSizeMax, int nPoolSizeMin)
{

	//first try to destroy all db connections:
	ShutDown();

	//stores new conn settings
	m_ConnectionSettings=ConnectionSettings;
	setDatabaseType(ConnectionSettings.m_strDbType);
	setHostName(ConnectionSettings.m_strDbHostName);
	setUserName( ConnectionSettings.m_strDbUserName);
	setPassword(ConnectionSettings.m_strDbPassword);
	setDatabaseName(ConnectionSettings.m_strDbName);
	setConnectOptions(ConnectionSettings.m_strDbOptions);
	if (ConnectionSettings.m_nDbPort)setPort(ConnectionSettings.m_nDbPort);
	//set new size:
	setPoolSize(nPoolSizeMax,nPoolSizeMin);

	//store timeout:
	m_nTimeoutMs = ConnectionSettings.m_nDbPoolTimeout;


	//----------------------------------
	//	INIT DB SPECIFIC 
	//----------------------------------

	//destroy previous
	DestroyHandlers();

	//based on Db type init db specific objects: autoid, locker, etc...
	if (GetDbType()==DBTYPE_ORACLE) 
	{
		if(!QSqlDatabase::isDriverAvailable(DBTYPE_ORACLE))
		{
			pStatus.setError(StatusCodeSet::ERR_SQL_DB_DRIVER_MISSING,DBTYPE_ORACLE);
			return;
		}
		m_DbAutoIdManager= new DbAutoIdManagerOracle;
		m_DirectRecordLocker = new DbDirectLocker; 
		m_DbTransactionManager= new DbTransactionHandlerOracle;
		m_Time= new DbCurrentTime_Oracle;
		m_SqlError = new DbSQLErrorMapper;
		m_DbDataTypeConvertor= new DbDataTypeConvertor_Oracle;
		m_SqlFunctions= new DbFunctionRepository_Oracle;

	}
	else if (GetDbType()==DBTYPE_MYSQL) 
	{
		if(!QSqlDatabase::isDriverAvailable(DBTYPE_MYSQL))
		{
			pStatus.setError(StatusCodeSet::ERR_SQL_DB_DRIVER_MISSING,DBTYPE_MYSQL);
			return;
		}
		m_DirectRecordLocker = new DbDirectLocker; 
		m_DbTransactionManager= new DbTransactionHandlerMySQL;
		m_DbDataTypeConvertor= new DbDataTypeConvertorMySQL;
		m_Time= new DbCurrentTime;
		m_SqlError = new DbSQLErrorMapper_MySQL;
		m_SqlFunctions= new DbFunctionRepositoryMySQL;
	}
	else if (GetDbType()==DBTYPE_MS_SQL) 
	{
		if(!QSqlDatabase::isDriverAvailable(DBTYPE_MS_SQL))
		{
			pStatus.setError(StatusCodeSet::ERR_SQL_DB_DRIVER_MISSING,DBTYPE_MS_SQL);
			return;
		}
		m_DirectRecordLocker = new DbDirectLocker; 
		m_DbTransactionManager= new DbTransactionHandlerMS_SQL;
		m_Time= new DbCurrentTime;
		m_SqlError = new DbSQLErrorMapper;
	}
	else if (GetDbType()==DBTYPE_POSTGRESQL) 
	{
		if(!QSqlDatabase::isDriverAvailable(DBTYPE_POSTGRESQL))
		{
			pStatus.setError(StatusCodeSet::ERR_SQL_DB_DRIVER_MISSING,DBTYPE_POSTGRESQL);
			return;
		}
		m_DirectRecordLocker = new DbDirectLocker; 
		m_DbTransactionManager= new DbTransactionHandlerPostgreSQL;
		m_Time= new DbCurrentTime;
		m_SqlError = new DbSQLErrorMapper;
	}
	else if (GetDbType()==DBTYPE_FIREBIRD) 
	{
		if(!QSqlDatabase::isDriverAvailable(DBTYPE_FIREBIRD))
		{
			pStatus.setError(StatusCodeSet::ERR_SQL_DB_DRIVER_MISSING,DBTYPE_FIREBIRD);
			return;
		}
		m_DbAutoIdManager= new DbAutoIdManagerFireBird;
		m_DirectRecordLocker = new DbDirectLocker; 
		m_DbTransactionManager= new DbTransactionHandlerFireBird;
		m_DbDataTypeConvertor= new DbDataTypeConvertorFireBird;
		m_Time= new DbCurrentTime_FireBird;
		m_SqlError = new DbSQLErrorMapper_FireBird;
		m_SqlFunctions= new DbFunctionRepositoryFireBird;
	}
	else
	{
		//Q_ASSERT_X(false,"Database not yet supportted!","Database not yet supportted!");
		pStatus.setError(1,QObject::tr("Connection could not be initialized. Database not supportted!"));
		return;
	}

	//damn...
	Q_ASSERT_X(m_DirectRecordLocker!=NULL,"Database not yet supported!","Database not yet supported!");
	Q_ASSERT_X(m_DbTransactionManager!=NULL,"Database not yet supported!","Database not yet supported!");
	Q_ASSERT_X(m_DbDataTypeConvertor!=NULL,"Database not yet supported!","Database not yet supported!");
	Q_ASSERT_X(m_Time!=NULL,"Database not yet supported!","Database not yet supported!");
	Q_ASSERT_X(m_SqlError!=NULL,"Database not yet supported!","Database not yet supported!");
	Q_ASSERT_X(m_SqlFunctions!=NULL,"Database not yet supported!","Database not yet supported!");

	//sets tran manager:
	SetTransactionManager(m_DbTransactionManager);

	//engage the pool
	EnablePool(true);

	//test connection (if settings are valid:
	QSqlDatabase *pDbConnection=ReserveConnection(pStatus);
	if(pStatus.IsOK())
	{
		ReleaseConnection(pDbConnection);
		FillPoolWithMinConnections();
	}
}




/*!
	Reserves connection from DbConnectionPool. Adds TimeOut if setted
	\param status  -error
	\return QSqlDatabase *
*/
QSqlDatabase *DbSqlManager::ReserveConnection(Status &status) 
{
	return (DbConnectionsPool::ReserveConnection(status, m_nTimeoutMs));
}


//--------------------------------------------------------------------
//							LAST ID
//--------------------------------------------------------------------


/*!
	Based on database in use returns last ID, if error return error, if DB doesnt support, fire ASSERT!
	Get last inserted ID on pDbConnection
	\param pStatus		 - return error 
	\param pDbConnection - connection
	\param pQuery		 - query with last execute INSERT statement
	\param nTableID		 - table id on which AutoID field is searched

	\return next id or 0 if error

	WARNING:
	Not all databases support lastinsertID: FireBird only uses NextID!!
*/
int DbSqlManager::getLastInsertId(Status &pStatus, QSqlDatabase *pDbConnection, QSqlQuery *pQuery, QString strPrimaryKey)
{
	//reset error
	pStatus.setError(0);

	//if supported first access by autoid managers:
	if(m_DbAutoIdManager!=NULL)
		return m_DbAutoIdManager->getLastInsertId(pStatus, pDbConnection, strPrimaryKey);


	//get from query itself if supported:
	if(pDbConnection->driver()->hasFeature(QSqlDriver::LastInsertId))
		return (pQuery->lastInsertId().toInt());
	
	//if not found, raise ASSERT:
	Q_ASSERT_X(false,"LAST INSERT ID", "DB doesnt support this feature");
	return 0;
}

int DbSqlManager::getNextInsertId(Status &pStatus, QSqlDatabase *pDbConnection, QString strPrimaryKey,int nPoolSize)
{
	//reset error
	pStatus.setError(0);

	//if supported first access by autoid managers:
	if(m_DbAutoIdManager!=NULL)
		return m_DbAutoIdManager->getNextInsertId(pStatus, pDbConnection, strPrimaryKey,nPoolSize);

	//if not found, raise ASSERT:
	Q_ASSERT_X(false,"NEXT INSERT ID", "DB doesn't support this feature");
	return 0;
}


//--------------------------------------------------------------------
//							DIRECT LOCKING
//--------------------------------------------------------------------



/*!
	Database specific locking mechanism \sa DbDirectLocker
*/
/*
int DbSqlManager::LockRecordsDirect(Status &status, QSqlDatabase *pDbConnection, QString strTableName,QString strPrimaryKey, const DbLockList &lstRecords)
{
	//return 0;
	//Q_ASSERT(false);
	//return 0; //BT: issue 974, lock direct on FIREBIRD does not work: 
	//from 1.5: FB supports FOR UPDATE and WITH LOCK!!! -> see release notes....
	//	record locker:
	return m_DirectRecordLocker->LockRecords(status, pDbConnection, strTableName,strPrimaryKey, lstRecords);

}
*/

/*!
	Database specific unlocking mechanism \sa DbDirectLocker
*/
/*
void DbSqlManager::UnLockRecordsDirect(Status &status, QSqlDatabase *pDbConnection, QString strTableName,QString strPrimaryKey, const DbLockList &lstRecords)
{
	//Q_ASSERT(false);
	//return; //BT: issue 974, lock direct on FIREBIRD does not work


	//	record locker:
	m_DirectRecordLocker->UnLockRecords(status, pDbConnection, strTableName,strPrimaryKey, lstRecords);

}
*/


//--------------------------------------------------------------------
//							TRANSACTIONS
//--------------------------------------------------------------------


/*!
	Database specific unlocking mechanism \sa DbTransactionHandler
*/
void DbSqlManager::BeginTransaction(Status &pStatus,QSqlDatabase* pDbConnection) 
{
	m_DbTransactionManager->BeginTransaction(pStatus,pDbConnection);
}

/*!
	Database specific unlocking mechanism \sa DbTransactionHandler
*/
void DbSqlManager::RollbackTransaction(Status &pStatus,QSqlDatabase* pDbConnection) 
{
	m_DbTransactionManager->RollbackTransaction(pStatus,pDbConnection);
}

/*!
	Database specific unlocking mechanism \sa DbTransactionHandler
*/
void DbSqlManager::CommitTransaction(Status &pStatus,QSqlDatabase* pDbConnection) 
{
	m_DbTransactionManager->CommitTransaction(pStatus,pDbConnection);
}

int DbSqlManager::GetCurrentTransactionCount(QSqlDatabase* pDbConnection)
{
	return m_DbTransactionManager->GetTransactionCount(pDbConnection);
}


//--------------------------------------------------------------------
//							DATA TYPES
//--------------------------------------------------------------------



QList<bool> DbSqlManager::IsConversionNeeded(DbRecordSet& lstView, QSqlRecord* record,bool& bConversionNeeded)
{
	return m_DbDataTypeConvertor->IsConversionNeeded(lstView,record,bConversionNeeded);
}

QVariant DbSqlManager::ConvertValue(QVariant& databaseValue, int colTypeInRecordSet)
{
	return m_DbDataTypeConvertor->ConvertValue(databaseValue,colTypeInRecordSet);
}

//--------------------------------------------------------------------
//							TIME
//--------------------------------------------------------------------

QDateTime DbSqlManager::GetCurrentDateTime(Status &status, QSqlDatabase *pDbConnection)
{
	return m_Time->GetCurrentDateTime(status,pDbConnection);
}
QString DbSqlManager::GetCurrentDateTime()
{
	return m_Time->GetCurrentDateTime();
}


//--------------------------------------------------------------------
//					ID & TIMESTAMP for SPECIAL DB's:
//--------------------------------------------------------------------
/*!
	If database need special support:
	- changes SQL statement and add ID & LAST_MODIFIED col:
	If UPDATE only update DAT_LAST_MODIFIED
	- Calls GetNextID() to fill in ID field, return value if generated, 0 for update

	\param strSQL			- incoming SQL insert or update statement is changed: ID =? & LAST_MODIFIED=CURRENT_TIMESTAMP();
	\param strPrimaryKey	- name of ID field
	\param strLastModified	- name of last modified field
	
	\return 0 if SQL is not changed, else return newly generated ID,-1 if Id not generated by SQL changed

*/

//For dummy bases, prepares autoinc column, returns valid value if ok, else, base is intelligent

bool DbSqlManager::SetSQLDefaultValues(Status &pStatus,QString &strSQL,bool bInsertBindMark,QString &strPrimaryKey,int &nID,QSqlDatabase *pDbConnection)
{
	pStatus.setError(0);
	if (GetDbType()!=DBTYPE_FIREBIRD) return false;

	QString strSQLType=strSQL.left(6).toUpper();
	if (strSQLType=="INSERT")
	{
		QString strTableName,strLastModified,strCreated;
		if(!DbSqlTableDefinition::GetTableDataFromSQL(true,strSQL,strTableName,strPrimaryKey,strLastModified,strCreated))
			return false;

		bool bUseCreateDate=false;
		//if those tables, then use create date->issue 2575
		if (strTableName=="BUS_PROJECTS" || strTableName=="CE_COMM_ENTITY" || strTableName=="BUS_CM_CONTACT")
		{
			bUseCreateDate=true;
		}

		//get new PK:
		nID=-1;
		if(!bInsertBindMark)
		{
			nID=getNextInsertId(pStatus,pDbConnection,strPrimaryKey);
			if (!pStatus.IsOK())
				return false;
		}

		//insert new value for PK:
		//insert: INSERT INTO table (...,PK) VALUES(..,PK)
		int nBracket=strSQL.indexOf(")");
		if (nBracket==-1) return false;

		if (bUseCreateDate)
			strSQL=strSQL.insert(nBracket,","+strPrimaryKey+","+strLastModified+","+strCreated);
		else
			strSQL=strSQL.insert(nBracket,","+strPrimaryKey+","+strLastModified);

		nBracket=strSQL.lastIndexOf(")");
		if (nBracket==-1) return false;


		if (bInsertBindMark)
		{
			if (bUseCreateDate)
				strSQL=strSQL.insert(nBracket,",?,"+m_Time->GetCurrentDateTime()+","+m_Time->GetCurrentDateTime());
			else
				strSQL=strSQL.insert(nBracket,",?,"+m_Time->GetCurrentDateTime());

			int nCnt=strSQL.count("?")-1;
			//qDebug()<<strSQL;
			nID=nCnt;
			return true;
		}
		else
		{
			if (bUseCreateDate)
				strSQL=strSQL.insert(nBracket,","+QVariant(nID).toString()+","+m_Time->GetCurrentDateTime()+","+m_Time->GetCurrentDateTime());
			else
				strSQL=strSQL.insert(nBracket,","+QVariant(nID).toString()+","+m_Time->GetCurrentDateTime());

			return true;
		}

	}
	else if (strSQLType=="UPDATE")
	{
		nID=-1; //reset PK always when UPDATE: no need to update ID...
		QString strTableName,strLastModified,strCreated;
		if(!DbSqlTableDefinition::GetTableDataFromSQL(false,strSQL,strTableName,strPrimaryKey,strLastModified,strCreated))
			return false;

		if (strSQL.indexOf(strLastModified)==-1)	//if update and last modified inside, skip, else set
		{
			//insert: UPDATE table SET LAST=CURRENT_TIMESTAMP(), SET...
			int nSet=strSQL.indexOf("SET ")+4;
			Q_ASSERT(nSet!=-1);
			strSQL=strSQL.insert(nSet,strLastModified+"="+m_Time->GetCurrentDateTime()+",");
			return true;
		}


	}

	return false;
}

//map sql error:
Status DbSqlManager::MapSqlError(QSqlQuery *pQuery)
{
	return m_SqlError->mapSQLError(pQuery);
}

int DbSqlManager::GetDatabaseVersion(QSqlDatabase *pDbConnection)
{
	Status err;
	if (!pDbConnection)
	{
		pDbConnection = ReserveConnection(err); //reserve connection
		if(!err.IsOK()) return 0;
	}
	
	QString strSQL="SELECT CDI_DATABASE_VERSION FROM CORE_DATABASE_INFO";
	QSqlQuery query(*pDbConnection);
	if(!query.exec(strSQL))
	{
		return 0;
	}

	if (query.next())
	{
		return query.value(0).toInt();
	}
	return 0;
}

//0 if table not exists
int DbSqlManager::GetDatabaseVersion_SPC(QSqlDatabase *pDbConnection)
{
	Status err;
	if (!pDbConnection)
	{
		pDbConnection = ReserveConnection(err); //reserve connection
		if(!err.IsOK()) return 0;
	}

	QString strSQL="SELECT DEF_VERSION FROM FL_Default";
	QSqlQuery query(*pDbConnection);
	if(!query.exec(strSQL))
	{
		return 0;
	}

	if (query.next())
	{
		return query.value(0).toInt();
	}
	return 0;
}
