#include "dbomanager_firebird.h"
#include <QVariant>
#include <QProcess>
#include <QDebug>
#include <QFileInfo>
#include <QDateTime>
#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QCoreApplication>

#include "common/common/zipcompression.h"


//----------------------------------------------------
//				INDEX
//----------------------------------------------------



/*!
Check If Exists Index (case insensitive)

\param pDbConnection	- db connection
\param strTableName		- table name
\param strIndexName		- index name
\return  1 if exists
*/
bool DbObjectManager_FireBird::CheckIfExists_Index(QSqlDatabase *pDbConnection, QString strTableName, QString strIndexName)
{
	//fetch all indexes for table
	QString strSql = "SELECT RDB$INDEX_NAME FROM RDB$INDICES WHERE RDB$RELATION_NAME='" + strTableName +"' AND RDB$INDEX_NAME='"+strIndexName+"'";

	QSqlQuery query(*pDbConnection);query.setForwardOnly(true);
	if(!query.exec(strSql))
	{
		return false;
	}

	//find our index:
	while (query.next()) 
	{
		//qDebug()<<query.value(0).toString().trimmed().upper();
		if(query.value(0).toString().trimmed().toUpper()==strIndexName) return true;
	}

	return false;
}




//----------------------------------------------------
//				TRIGGER
//----------------------------------------------------


/*!
Check If Exists Trigger (case insensitive)

\param pDbConnection	- db connection
\param strTriggerName   - object name
\return  1 if exists
*/
bool DbObjectManager_FireBird::CheckIfExists_Trigger(QSqlDatabase *pDbConnection, QString strTriggerName)
{
	QString strSql = "SELECT COUNT(*) FROM RDB$TRIGGERS WHERE RDB$TRIGGER_NAME='"+strTriggerName+"'";
	QSqlQuery query(*pDbConnection);
	if(!query.exec(strSql))
	{
		return false;
		//error executing query
		//pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, QObject::tr("Failed check trigger: ")+strTriggerName);
		//pStatus.setErrorDetails(query.lastError().text()); //SQL error details 
	}


	query.next();
	int nCount = query.value(0).toInt();

	if( nCount==0)
		return false;
	else
		return true;
}


/*!
Drop Trigger if exists (case insensitive)

\param pStatus		 - if statement fails
\param pDbConnection - db connection
\param strTriggerName  - object name
*/
void DbObjectManager_FireBird::DropIfExists_Trigger(Status &pStatus, QSqlDatabase *pDbConnection, QString strTriggerName)
{

	pStatus.setError(0);
	//check if exists:
	if(!CheckIfExists_Trigger(pDbConnection,strTriggerName)) return;

	QString strSql = "DROP TRIGGER " + strTriggerName;

	QSqlQuery query(*pDbConnection);
	if(!query.exec(strSql))
	{
		//error executing query
		pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, QObject::tr("Failed to delete trigger: ")+strTriggerName+" - "+query.lastError().text());
		//pStatus.setErrorDetails(query.lastError().text()); //SQL error details 
	}
}





//----------------------------------------------------
//				SEQUENCE
//----------------------------------------------------



/*!
Check If Exists Sequence (case insensitive)

\param pDbConnection	- db connection
\param strSeqName   - object name
\return  1 if exists
*/
bool DbObjectManager_FireBird::CheckIfExists_Sequence(QSqlDatabase *pDbConnection, QString strSeqName)
{

	QString strSql = "SELECT COUNT(*) FROM RDB$GENERATORS WHERE RDB$GENERATOR_NAME='"+strSeqName+"'";
	QSqlQuery query(*pDbConnection);
	if(!query.exec(strSql))
	{
		return false;
		//error executing query
		//pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, QObject::tr("Failed check sequence: ")+strTriggerName);
		//pStatus.setErrorDetails(query.lastError().text()); //SQL error details 
	}


	query.next();
	int nCount = query.value(0).toInt();

	if( nCount==0)
		return false;
	else
		return true;
}


/*!
Drop Sequence if exists (case insensitive)

\param pStatus		 - if statement fails
\param pDbConnection - db connection
\param strSeqName  - object name
*/
void DbObjectManager_FireBird::DropIfExists_Sequence(Status &pStatus, QSqlDatabase *pDbConnection, QString strSeqName)
{

	pStatus.setError(0);
	//check if exists:
	if(!CheckIfExists_Sequence(pDbConnection,strSeqName)) return;

	QString strSql = "DROP SEQUENCE " + strSeqName;

	QSqlQuery query(*pDbConnection);
	if(!query.exec(strSql))
	{
		//error executing query
		pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, QObject::tr("Failed to delete sequence: ")+strSeqName+" - "+query.lastError().text());
		//pStatus.setErrorDetails(query.lastError().text()); //SQL error details 
	}
}


/*!
Create Sequence, drop if exists

\param pStatus		 - if statement fails
\param pDbConnection - db connection
\param strSeqName	 - object name
\param nStartCounter - start sequence from this number
*/
void DbObjectManager_FireBird::CreateSequence(Status &pStatus, QSqlDatabase *pDbConnection, QString strSeqName, int nStartCounter)
{
	//delete prev. seq
	DropIfExists_Sequence(pStatus,pDbConnection,strSeqName);
	if(!pStatus.IsOK()) return;

	QString strSql = "CREATE SEQUENCE "+strSeqName;
	QSqlQuery query(*pDbConnection);

	if(!query.exec(strSql))
	{
		//error executing query
		pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, QObject::tr("Sequence creation failed: ")+strSeqName+" - "+query.lastError().text());
		//pStatus.setErrorDetails(query.lastError().text()); //SQL error details 
	}
}



//----------------------------------------------------
//				PROCEDURE
//----------------------------------------------------



/*!
Check If Exists PROCEDURE (case insensitive)

\param pDbConnection	- db connection
\param strProcName   - object name
\return  1 if exists
*/
bool DbObjectManager_FireBird::CheckIfExists_Proc(QSqlDatabase *pDbConnection, QString strProcName)
{
	QString strSql = "SELECT COUNT(*) FROM RDB$PROCEDURES WHERE RDB$PROCEDURE_NAME='"+strProcName+"'";
	QSqlQuery query(*pDbConnection);
	if(!query.exec(strSql))
	{
		return false;
		//error executing query
		//pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, QObject::tr("Failed check sequence: ")+strProcName);
		//pStatus.setErrorDetails(query.lastError().text()); //SQL error details 
	}


	query.next();
	int nCount = query.value(0).toInt();

	if( nCount==0)
		return false;
	else
		return true;
}


/*!
Drop PROCEDURE if exists (case insensitive)

\param pStatus		 - if statement fails
\param pDbConnection - db connection
\param strProcName  - object name
*/
void DbObjectManager_FireBird::DropIfExists_Proc(Status &pStatus, QSqlDatabase *pDbConnection, QString strProcName)
{

	pStatus.setError(0);
	//check if exists:
	if(!CheckIfExists_Proc(pDbConnection,strProcName)) return;

	QString strSql = "DROP PROCEDURE " + strProcName;

	QSqlQuery query(*pDbConnection);
	if(!query.exec(strSql))
	{
		//error executing query
		pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, QObject::tr("Failed to delete procedure: ")+strProcName+" - "+query.lastError().text());
		//pStatus.setErrorDetails(query.lastError().text()); //SQL error details 
	}
}





//----------------------------------------------------
//				FUNCTION
//----------------------------------------------------



/*!
Check If Exists FUNCTION (case insensitive)

\param pDbConnection	- db connection
\param strFunctName   - object name
\return  1 if exists
*/
bool DbObjectManager_FireBird::CheckIfExists_Funct(QSqlDatabase *pDbConnection, QString strFunctName)
{

	QString strSql = "SELECT COUNT(*) FROM RDB$FUNCTIONS WHERE RDB$FUNCTION_NAME='"+strFunctName+"'";
	QSqlQuery query(*pDbConnection);
	if(!query.exec(strSql))
	{
		//error executing query
		return false;
		//pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, QObject::tr("Failed check function: ")+strFunctName);
		//pStatus.setErrorDetails(query.lastError().text()); //SQL error details 
	}


	query.next();
	int nCount = query.value(0).toInt();

	if( nCount==0)
		return false;
	else
		return true;
}


/*!
Drop FUNCTION if exists (case insensitive)

\param pStatus		 - if statement fails
\param pDbConnection - db connection
\param strFunctName  - object name
*/
void DbObjectManager_FireBird::DropIfExists_Funct(Status &pStatus, QSqlDatabase *pDbConnection, QString strFunctName)
{

	pStatus.setError(0);
	//check if exists:
	if(!CheckIfExists_Funct(pDbConnection,strFunctName)) return;

	QString strSql = "DROP EXTERNAL FUNCTION " + strFunctName;

	QSqlQuery query(*pDbConnection);
	if(!query.exec(strSql))
	{
		//error executing query
		pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, QObject::tr("Failed to delete procedure: ")+strFunctName+" - "+query.lastError().text());
		//pStatus.setErrorDetails(query.lastError().text()); //SQL error details 
	}
}




//----------------------------------------------------
//				AUTO INCREMENT ID
//----------------------------------------------------



/*!
	One generator for each table ID:

	\param pStatus				 -error
	\param pDbConnection		 -db connection
	\param strTableName			 -table name
	\param strAutoIncFieldName   -field on which ID will be created
*/
void DbObjectManager_FireBird::CreateAutoIncrementID(Status &pStatus, QSqlDatabase *pDbConnection,QString strTableName, QString strAutoIncFieldName)
{
	pStatus.setError(0);
	QString strSeqName="SEQ_" + strAutoIncFieldName;

	//drop generator, do not check for errors if not exists:
	QString strSQL="DROP SEQUENCE "+strSeqName;
	QSqlQuery query(*pDbConnection);
	query.exec(strSQL);
	

	strSQL="CREATE SEQUENCE "+strSeqName;
	if(!query.exec(strSQL))
	{
		//error executing query
		pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, QObject::tr("Failed to create generator: ")+strSeqName+query.lastError().text());
	}

}

void DbObjectManager_FireBird::AdjustAutoIncrementAfterDbCopy(Status &pStatus, QSqlDatabase *pDbConnection,QString strTableName, QString strAutoIncFieldName)
{

	QSqlQuery query(*pDbConnection);
	QString strSelMax="SELECT MAX("+strAutoIncFieldName+") FROM "+strTableName;
	if(!query.exec(strSelMax))
	{
		pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, QObject::tr("Failed to adjust Sequence start value: ")+query.lastError().text());
		return;

	}

	while (query.next())
	{
		int nMax=query.value(0).toInt();
		QString strSeqName="SEQ_" + strAutoIncFieldName;
		QString strSQL="ALTER SEQUENCE "+strSeqName+" RESTART WITH "+QVariant(nMax+1).toString();
		if(!query.exec(strSQL))
		{
			pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, QObject::tr("Failed to adjust Sequence start value: ")+query.lastError().text());
			return;

		}
	}


}



//drops all triggers in the database:
void DbObjectManager_FireBird::DropAllTriggers(Status &pStatus, QSqlDatabase *pDbConnection)
{
	QString strSql = "SELECT RDB$TRIGGER_NAME FROM RDB$TRIGGERS";
	QSqlQuery query(*pDbConnection);
	if(!query.exec(strSql))
	{
		pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, QObject::tr("Failed to delete triggers: ")+query.lastError().text());
		return;

	}

	while (query.next())
	{
		QString strTriggerName=query.value(0).toString();
		if (strTriggerName.indexOf("TRG_")!=0)				//to skip system triggers
			continue;
		QString strSql = "DROP TRIGGER " + strTriggerName;

		QSqlQuery query(*pDbConnection);
		if(!query.exec(strSql))
		{
			//error executing query
			pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, QObject::tr("Failed to delete triggers: ")+query.lastError().text());	
			return;
		}
	}

}


//drops all constraints in the database (only FK's):
void DbObjectManager_FireBird::DropAllConstraints(Status &pStatus, QSqlDatabase *pDbConnection)
{
	QString strSql= "SELECT DISTINCT rc.RDB$RELATION_NAME,rc.RDB$CONSTRAINT_NAME FROM RDB$RELATION_CONSTRAINTS AS rc ";
	strSql+= "LEFT JOIN RDB$REF_CONSTRAINTS refc ON rc.RDB$CONSTRAINT_NAME = refc.RDB$CONSTRAINT_NAME WHERE rc.RDB$CONSTRAINT_TYPE = 'FOREIGN KEY'";
	QSqlQuery query(*pDbConnection);
	if(!query.exec(strSql))
	{
		pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, QObject::tr("Failed to delete constraints: ")+query.lastError().text());
		return;
	}

	while (query.next())
	{
		QString strSql = "ALTER TABLE "+query.value(0).toString().trimmed().toUpper()+" DROP CONSTRAINT "+query.value(1).toString().trimmed().toUpper();
		//QString strSql = "DROP CONSTRAINT "+query.value(0).toString().trimmed().toUpper();

		//qDebug()<<strSql;
		QSqlQuery query(*pDbConnection);
		if(!query.exec(strSql))
		{
			//error executing query
			pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, QObject::tr("Failed to delete constraints: ")+query.lastError().text());
		}
	}

}



void DbObjectManager_FireBird::DropAllIndexes(Status &pStatus, QString strTableName,QSqlDatabase *pDbConnection)
{
	QString strSql = "SELECT RDB$INDEX_NAME,RDB$INDEX_ID FROM RDB$INDICES WHERE RDB$RELATION_NAME='" + strTableName+"'";
	QSqlQuery query(*pDbConnection);query.setForwardOnly(true);
	if(!query.exec(strSql))
	{
		pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, query.lastError().text());
		return;
	}

	//delete them all:
	int fieldNo = 0;
	while (query.next())  //Cannot delete index used by an Integrity Constraint: pk or fk indexes can not be deleted!!!
	{
		//must skip drop of primary key: tablename+_PK
		//if (query.value(fieldNo).toString().trimmed().toUpper()==strTableName+"_PK")
		//	continue;
		DropIfExists_Index(pStatus,pDbConnection,strTableName,query.value(fieldNo).toString().trimmed().toUpper());
		//With FB version Firebird-2.5.0.26074-0 and above I can not relay on error msg, just delete as much as you can, probably 99% errors are ok (pk or fk)
		/*
		if (!pStatus.IsOK()) //ignore error if we are trying to delete PK/FK indexes of constraints (hidden indexes)
			if (pStatus.getErrorText().indexOf("Cannot delete index used by an Integrity Constraint")>=0)
			{
				pStatus.setError(0);
				continue;
			}
		if (!pStatus.IsOK())
			return;
		*/
	}

	pStatus.setError(0);//clear error
}


/*!
	Drop index if exists (case insensitive)

	\param pStatus		 - if statement fails
	\param pDbConnection - db connection
	\param strTableName	 - table name
	\param strIndexName  - object name
*/
void DbObjectManager_FireBird::DropIfExists_Index(Status &pStatus, QSqlDatabase *pDbConnection, QString strTableName, QString strIndexName)
{

	pStatus.setError(0);
	//check if exists:
	if(!CheckIfExists_Index(pDbConnection,strTableName, strIndexName)) return;

	QString strSql = "DROP INDEX " + strIndexName;

	QSqlQuery query(*pDbConnection);
	if(!query.exec(strSql))
	{
		//error executing query
		pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, QObject::tr("Failed to delete index: ")+strIndexName+" - "+query.lastError().text());
		//pStatus.setErrorDetails(query.lastError().text()); //SQL error details 
	}
}



/*
Try This:

show table tablename;
show procedure procedurename;
show trigger triggername;
*/


bool DbObjectManager_FireBird::GetTableColumnsFromDb(QString strTableName,QHash<QString,QStringList> &lstColsData,QSqlDatabase *pDbConnection)
{
	//get table desc:
	//B.T.: change length-> to use charlength as in UTF8 database all varchar fields have 4x time internal length then actual char length!!!
	QSqlQuery query(*pDbConnection);
	QString strSql = "SELECT RDB$RELATION_FIELDS.RDB$FIELD_NAME,RDB$TYPES.RDB$TYPE_NAME, RDB$RELATION_FIELDS.RDB$NULL_FLAG,RDB$FIELDS.RDB$CHARACTER_LENGTH,RDB$FIELDS.RDB$FIELD_SUB_TYPE, RDB$FIELDS.RDB$FIELD_SCALE, RDB$FIELDS.RDB$FIELD_PRECISION\
		FROM RDB$RELATIONS \
		INNER JOIN RDB$RELATION_FIELDS ON RDB$RELATIONS.RDB$RELATION_NAME = RDB$RELATION_FIELDS.RDB$RELATION_NAME \
		LEFT JOIN RDB$FIELDS ON RDB$RELATION_FIELDS.RDB$FIELD_SOURCE = RDB$FIELDS.RDB$FIELD_NAME \
		LEFT JOIN RDB$TYPES ON RDB$FIELDS.RDB$FIELD_TYPE = RDB$TYPES.RDB$TYPE";
	strSql+=" WHERE RDB$RELATIONS.RDB$RELATION_NAME = '"+strTableName+"'";
	strSql+=" AND RDB$RELATIONS.RDB$SYSTEM_FLAG = 0 AND RDB$TYPES.RDB$FIELD_NAME='RDB$FIELD_TYPE' ORDER BY RDB$RELATION_FIELDS.RDB$FIELD_POSITION";

	if(!query.exec(strSql))
	{
		qDebug()<<query.lastError().text();
		return false;
	} 

	

	while (query.next())
	{
		QStringList lstData;
		lstData.append(query.value(0).toString().toUpper().trimmed());
		QString strType=query.value(1).toString().toUpper().trimmed();

		if (strType=="LONG")
		{
			strType="INTEGER";
		}
		else if (strType=="SHORT")
		{
			strType="SMALLINT";
		}
		else if (strType=="VARYING")
		{
			strType="VARCHAR("+query.value(3).toString()+")";
		}
		else if (strType=="BLOB")
		{
			if (query.value(4).toInt()==1) //text
				strType="BLOB sub_type TEXT";
			else
				strType="BLOB";
		}
		else if (strType=="INT64")
		{
			int nScale=query.value(5).toInt();
			nScale=abs(nScale);
			strType="DECIMAL("+query.value(6).toString()+","+QVariant(nScale).toString()+")";
		}


		lstData.append(strType);

		if (query.value(2).toInt()!=1)				//RDB$NULL_FLAG=1 means NOT NULL column
			lstData.append("null");
		else
			lstData.append("not null");

		lstColsData[lstData.at(0)]=lstData;
	}

	return true;
}


//recreate indexes after this:
//called on end of org, to make possible business procedures update...
void DbObjectManager_FireBird::DropColumns(Status &pStatus, QString strTableName, QStringList lstDeletedColumns, QSqlDatabase *pDbConnection)
{

	QHash<QString,QStringList> lstCols,lstColsDb;
	QSqlQuery query(*pDbConnection);

	//get table cols from db
	if(!GetTableColumnsFromDb(strTableName,lstColsDb,pDbConnection))
	{
		pStatus.setError(1,QObject::tr("Failed to fetch table info from database for table: ")+ strTableName);
		return;
	}

	//FIREBIRD specific: must drop all indexes before alter...brb...

	//drop all that have to be dropped
	//--------------
	QString strDrop;
	int nSize=lstDeletedColumns.size();
	for(int i=0;i<nSize;++i)
	{
		if (lstColsDb.contains(lstDeletedColumns.at(i)))
			strDrop+="DROP "+lstDeletedColumns.at(i)+",";
	}
	if (!strDrop.isEmpty())
	{
		//DropAllIndexes(pStatus,strTableName,pDbConnection);
		//if (!pStatus.IsOK()) return;
		strDrop.chop(1);
		strDrop="ALTER TABLE "+strTableName+" "+strDrop;
		if(!query.exec(strDrop))
		{
			pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, query.lastError().text());
			return;
		}
	}
}



//table: must exists....
//- col del
//- col rename
//- col datatype change
void DbObjectManager_FireBird::AlterTable(Status &pStatus, QString strTableName, QString strTableCreationStatement,QStringList lstDeletedColumns, QList<QStringList> lstChangedNameColumns,QSqlDatabase *pDbConnection, bool bFillDefaultValuesForNewFields)
{
	QHash<QString,QStringList> lstCols,lstColsDb;
	QSqlQuery query(*pDbConnection);

	//get table cols/data type from string
	if(!GetTableColumnsFromString(strTableCreationStatement,lstCols))
	{
		pStatus.setError(1,QObject::tr("Failed to parse create table statement for table: ")+ strTableName);
		return;
	}

	//get table cols from db
	if(!GetTableColumnsFromDb(strTableName,lstColsDb,pDbConnection))
	{
		pStatus.setError(1,QObject::tr("Failed to fetch table info from database for table: ")+ strTableName);
		return;
	}

	//FIREBIRD specific: must drop all indexes before alter...brb...
	bool bIndexesDropped=false;



	//drop all that have to be dropped
	//--------------
	QString strDrop;
	int nSize=lstDeletedColumns.size();
	for(int i=0;i<nSize;++i)
	{
		if (lstColsDb.contains(lstDeletedColumns.at(i)))
			strDrop+="DROP "+lstDeletedColumns.at(i)+",";
	}
	if (!strDrop.isEmpty())
	{

		if (!bIndexesDropped) //firebird: recreate indexes
		{
			DropAllIndexes(pStatus,strTableName,pDbConnection);
			if (!pStatus.IsOK()) return;
			bIndexesDropped=true;
		}

		strDrop.chop(1);
		strDrop="ALTER TABLE "+strTableName+" "+strDrop;
		if(!query.exec(strDrop))
		{
			pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, query.lastError().text());
			return;
		}
	}

	//change names all that have to be changed
	//--------------
	QString strChange;
	nSize=lstChangedNameColumns.size();
	for(int i=0;i<nSize;++i)
	{
		if (lstColsDb.contains(lstChangedNameColumns.at(i).at(0)))
		{
			QStringList lstDbParams=lstColsDb[lstChangedNameColumns.at(i).at(0)];
			strChange+="ALTER "+lstDbParams.at(0)+" TO "+lstChangedNameColumns.at(i).at(1)+",";
		}
	}
	if (!strChange.isEmpty())
	{
		if (!bIndexesDropped) //firebird: recreate indexes
		{
			DropAllIndexes(pStatus,strTableName,pDbConnection);
			if (!pStatus.IsOK()) return;
			bIndexesDropped=true;
		}

		strChange.chop(1);
		strChange="ALTER TABLE "+strTableName+" "+strChange;
		if(!query.exec(strChange))
		{
			pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, query.lastError().text());
			return;
		}
	}

	//reload from db if drop or name changed occurred:
	if (!strDrop.isEmpty() || !strChange.isEmpty())
	{
		GetTableColumnsFromDb(strTableCreationStatement,lstColsDb,pDbConnection);
	}


	//alter: change data type or add new col
	//compare by name: if found test is data type is changed, else add new

	//not null and null->
	//update RDB$RELATION_FIELDS set	RDB$NULL_FLAG = NULL	where (RDB$FIELD_NAME = 'BPER_FIRST_NAME') and	(RDB$RELATION_NAME = 'BUS_PERSON')
	//update RDB$RELATION_FIELDS set	RDB$NULL_FLAG = 1	where (RDB$FIELD_NAME = 'BPER_FIRST_NAME') and	(RDB$RELATION_NAME = 'BUS_PERSON')

	QString strAlter;
	QString strUpdateFillDefaultVals;
	QHashIterator <QString,QStringList> x(lstCols);
	while (x.hasNext()) 
	{
		x.next();


		if (lstColsDb.contains(x.key())) //change data type
		{
			QStringList lstDbParams=lstColsDb[x.key()];
			QStringList lstNewParams=x.value();

			if (lstDbParams.at(1)!=lstNewParams.at(1)) // || lstDbParams.at(2)!=lstNewParams.at(2)) --> FIREBIRD DO NOT ALLOW CHANGE FROM NULL TO NOT NULL AND VICE VERSA
			{
				if (lstNewParams.at(1)=="TIMESTAMP" && lstDbParams.at(1).indexOf("DATETIME")>=0)continue;			//firebird: timestamp=datetime
				if (lstNewParams.at(0).indexOf("DAT_LAST_MODIFIED")>0)continue;										//firebird: ignore timestamp last modified

				QString strNewDataType=lstNewParams.at(1);
				int nfirstSpace=strNewDataType.indexOf(" ");
				if (nfirstSpace>0)
					strNewDataType=strNewDataType.left(nfirstSpace);

				//B.T. on 2013-20-09: disable attempt to change numeric data type: probably this will fail anyway: use special routine to do that: create new, copy values, delete old..etc..
				//Note: Decimal(3) or (6) = INTEGER on FB
				if(strNewDataType.toLower().contains("integer") || strNewDataType.toLower().contains("decimal")  || strNewDataType.toLower().contains("float") || strNewDataType.toLower().contains("smallint") || strNewDataType.toLower().contains("bigint"))
				{
					//qDebug()<<"can not alter numeric data type fld: "<<x.key()<<" old type: " <<  lstDbParams.at(1)<<" new type: "<<strNewDataType;
					continue;
				}

				strAlter+="ALTER "+lstNewParams.at(0)+" TYPE "+strNewDataType+",";; //+" "+lstNewParams.at(2)
			}

			//B.T. added on 29.02.2012: modify NOT NULL to NULL and vice versa is possible but not through ALTER table, rather direct hack into system tables
			if (lstDbParams.at(2)!=lstNewParams.at(2)) //B.T. -> explicit change of null and not null property
			{
				if (lstDbParams.at(2).toLower()=="not null" && lstNewParams.at(2).toLower()=="null")
				{
					QString strSQL="update RDB$RELATION_FIELDS set	RDB$NULL_FLAG = NULL	where (RDB$FIELD_NAME = '%1') and	(RDB$RELATION_NAME = '%2')";
					strSQL=strSQL.arg(x.key()).arg(strTableName);
					if(!query.exec(strSQL))
					{
						pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, query.lastError().text());
						return;
					}
				}
				else if (lstNewParams.at(2).toLower()=="null" && lstNewParams.at(2).toLower()=="not null")
				{

					QString strSQL="update RDB$RELATION_FIELDS set	RDB$NULL_FLAG = 1 where (RDB$FIELD_NAME = '%1') and	(RDB$RELATION_NAME = '%2')";
					strSQL=strSQL.arg(x.key()).arg(strTableName);
					if(!query.exec(strSQL))
					{
						pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, query.lastError().text());
						return;
					}
				}
			}


		}
		else			//add column
		{

			QStringList lstDbParams=lstColsDb[x.key()];
			QStringList lstNewParams=x.value();
			if (lstNewParams.at(2)=="null")			//in firebird-> null is not keyword, just not null
				strAlter+="ADD "+lstNewParams.at(0)+" "+lstNewParams.at(1)+",";
			else
				strAlter+="ADD "+lstNewParams.at(0)+" "+lstNewParams.at(1)+" "+lstNewParams.at(2)+",";

			if (bFillDefaultValuesForNewFields)
			{
				if (!(lstNewParams.at(1).toLower().contains("timestamp") || lstNewParams.at(1).toLower().contains("datetime")  || lstNewParams.at(1).toLower().contains("date")))
				{
					if (lstNewParams.at(1).toLower().contains("char") || lstNewParams.at(1).toLower().contains("varchar") || lstNewParams.at(1).toLower().contains("blob"))
						strUpdateFillDefaultVals+=lstNewParams.at(0)+"='',";
					else
						strUpdateFillDefaultVals+=lstNewParams.at(0)+"=0,";
				}
			}
		}
	}

	if (!strAlter.isEmpty())
	{
		qDebug()<<"Altering table: "<<strTableName;
		if (!bIndexesDropped) //firebird: recreate indexes
		{
			DropAllIndexes(pStatus,strTableName,pDbConnection);
			if (!pStatus.IsOK()) 
				return;
			bIndexesDropped=true;
		}

		if(strAlter.right(1)==",") //if , then chop
			strAlter.chop(1);

		strAlter="ALTER TABLE "+strTableName+" "+strAlter;
		//for old database to prevent changing data type...as this is done in special funct...
		if (strAlter.indexOf("BEM_REPLY_TO")>=0)
		{
			qDebug()<<strAlter;
		}

		if (!(strAlter.indexOf("BCFL_VALUE")>=0)) 
		{
			if(!query.exec(strAlter))
			{
				pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, query.lastError().text());
				return;
			}
		}
	}

	if (!strUpdateFillDefaultVals.isEmpty())
	{
		qDebug()<<"Filling default values for table: "<<strTableName;

		if(strUpdateFillDefaultVals.right(1)==",") //if , then chop
			strUpdateFillDefaultVals.chop(1);

		strUpdateFillDefaultVals="UPDATE "+strTableName+" SET "+strUpdateFillDefaultVals;
		if(!query.exec(strUpdateFillDefaultVals))
		{
			pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, query.lastError().text());
			return;
		}
	}

}



//use mysqldump, make sure that path is in system path
//WARNING: USE INNODB storage engine
//NOTE: to enable more quicker backups use binray logs (not used by default)
//strDumpFilePath is path to backup directory: C:/backup
//name is formatted as: DATABASE_DBVER_YYYYMMDDHHMM_COUNT where count is incremental number if there more then 1 backup generated in one minute
//returns: strDumpFilePath will contain full path to newly genreated backup file
QString DbObjectManager_FireBird::BackupDatabase(Status &pStatus, QString strDatabaseName, QString strUser, QString strPassword,QString &strDumpFilePath,QString strDatabaseVersion,QString strHost, QString strPort,QSqlDatabase *pDbConnection)
{
	//-----------------------GENERATE NEW BACKUP FILE NAME---------------------------------
	qDebug()<<"Database backup in progress";
	
	//check if already exists same name in directory:
	int nCount=0;
	QString strDumpFileName;
	QString strCompleteDumpFilePath;
	do 
	{
		//precheck name: in THICK client we have: C:\ddd.fdb, in thin not
		QFileInfo dbName(strDatabaseName);
		QString strDatabaseNameShort=dbName.baseName();

		strDumpFileName=strDatabaseNameShort+"_"+strDatabaseVersion+"_"+QDateTime::currentDateTime().toString("yyyyMMddHHmm")+"_"+QVariant(nCount).toString()+".nbk";
		strCompleteDumpFilePath=strDumpFilePath+"/"+strDumpFileName;
		QString strFileCheckZip=strCompleteDumpFilePath;
		strFileCheckZip.replace(".nbk",".zip");
		QFileInfo fileExistCheck(strFileCheckZip);
		if (!fileExistCheck.exists()) break;
		nCount++;
	} while(nCount<100);


	if (nCount==100)
	{
		pStatus.setError(1,QObject::tr("Can not determine new backup file name, backup creation failed!"));
		return "";

	}


	//-----------------------BACKUP---------------------------------
	//B.T. 19.12.2011: use NBACKUP
	//-----------------------BACKUP---------------------------------

	strCompleteDumpFilePath=QDir::toNativeSeparators(strCompleteDumpFilePath);

	QStringList lstArgs;

	QString strDbConnect=strDatabaseName;
	//if (!strHost.isEmpty())
	//	strDbConnect=strHost+":"+strDbConnect;

	lstArgs<<"-U";
	lstArgs<<strUser;
	lstArgs<<"-P";
	lstArgs<<strPassword;
	lstArgs<<"-B";
	lstArgs<<"0";
	lstArgs<<strDbConnect;
	lstArgs<<strCompleteDumpFilePath;

	/*
	lstArgs<<"-t";
	lstArgs<<"-user";
	lstArgs<<strUser;
	lstArgs<<"-password";
	lstArgs<<strPassword;
	lstArgs<<strDbConnect;
	lstArgs<<strCompleteDumpFilePath;
	*/

	//qDebug()<<lstArgs;

	QProcess mysqldump;
	QString strDir=QCoreApplication::applicationDirPath()+"/firebird/bin";
	mysqldump.setWorkingDirectory(strDir);
	mysqldump.start(strDir+"/nbackup.exe",lstArgs);
	if (!mysqldump.waitForFinished(-1)) //wait indefently
	{		
		pStatus.setError(1,QObject::tr("Failed to execute nbackup. Make sure it is in /firebird/bin subdirectory!"));
		return "";
	}
	int nExitCode=mysqldump.exitCode();
	QByteArray errContent=mysqldump.readAllStandardError();
	if (errContent.size()!=0)
	{
		pStatus.setError(1,QObject::tr("Failed to execute nbackup! Reason:"+errContent.left(4095))); //max buff=4kb
		return "";
	}

	//-----------------------MODIFY BACKUP FILE ---------------------------------
	QFile file(strCompleteDumpFilePath);


	//-----------------------ZIP & DELETE ORIGINAL IF SUCESS ---------------------------------


	QFileInfo infoBcp(strCompleteDumpFilePath);
	QString strZip=infoBcp.absolutePath()+"/"+infoBcp.baseName()+".zip";
	QStringList lstIn,lstDirs;
	lstIn<<strCompleteDumpFilePath;
	if(!ZipCompression::ZipFiles(strZip,lstIn))
	{
		pStatus.setError(1,QObject::tr("Error while trying to zip backup. Make sure that zip compression tool is present in the application path."));
		strDumpFilePath=strCompleteDumpFilePath;
	}
	else
	{
		strDumpFilePath=strZip;
		file.remove();
	}

	return infoBcp.baseName();
}

//use mysql, make sure that path is in system path
//strDumpFilePath -complete path to bcp file
void DbObjectManager_FireBird::RestoreDatabase(Status &pStatus, QString strDatabaseName, QString strUser, QString strPassword,QString strDumpFilePath,QString strHost, QString strPort,QSqlDatabase *pDbConnection)
{

	//precheck name:
	QFileInfo dbName(strDatabaseName);
	QString strDatabaseNameShort=dbName.baseName();


	qDebug()<<"Database restore from backup in progress";
	//---------------------------CHECK FILE----------------------------------------------
	QFileInfo info(strDumpFilePath);
	if (!info.exists())
	{
		QString strMsg=QObject::tr("Database restore from ")+strDumpFilePath+QObject::tr(" failed! File not found!");
		pStatus.setError(1,strMsg);
		return;
	}



	//---------------------------UNZIP IT----------------------------------------------

	bool bUnziped=false;
	bool bUsegbak=false;
	if (info.suffix()=="zip") //restore if zipped:
	{
		QStringList lstData;
		if(!ZipCompression::Unzip(QDir::toNativeSeparators(strDumpFilePath),QDir::toNativeSeparators(info.absolutePath()),lstData))
		{
			QString strMsg=QObject::tr("Database restore from ")+strDumpFilePath+QObject::tr(" failed! Can not unzip file: ")+strDumpFilePath;
			pStatus.setError(1,strMsg);
			return;
		}
		strDumpFilePath=info.absolutePath()+"/"+info.baseName()+".bcp";

		if (QFile::exists(strDumpFilePath))
			bUsegbak=true;
		else
			strDumpFilePath=info.absolutePath()+"/"+info.baseName()+".nbk";
		bUnziped=true;
	}




	//--------------------------RESTORE DB----------------------------------------------
	QStringList lstArgs;
	strDumpFilePath=QDir::toNativeSeparators(strDumpFilePath);

	QString strDbConnect=strDatabaseName;


	if (bUsegbak)
	{
		if (!strHost.isEmpty())
			strDbConnect=strHost+":"+strDbConnect;

		lstArgs<<"-r";
		lstArgs<<"o";
		lstArgs<<"-user";
		lstArgs<<strUser;
		lstArgs<<"-password";
		lstArgs<<strPassword;
		lstArgs<<strDumpFilePath;
		lstArgs<<strDbConnect;

		QProcess mysql;
		QString strDir=QCoreApplication::applicationDirPath()+"/firebird/bin";
		mysql.setWorkingDirectory(strDir);
		mysql.start(strDir+"/gbak.exe",lstArgs);
		if (!mysql.waitForFinished(-1)) //wait forever
		{		
			pStatus.setError(1,QObject::tr("Failed to execute gbak. Make sure it is in /firebird/bin subdirectory!"));
			if (bUnziped)
			{
				QFile fileRestoreZip(strDumpFilePath);
				fileRestoreZip.remove();
			}
			return;
		}
		int nExitCode=mysql.exitCode();
		QByteArray errContent=mysql.readAllStandardError();
		//QByteArray fileContent=mysql.readAllStandardOutput();
		if (errContent.size()!=0)
		{
			pStatus.setError(1,QObject::tr("Failed to execute gbak! Reason:"+errContent.left(4095)));
			if (bUnziped)
			{
				QFile fileRestoreZip(strDumpFilePath);
				fileRestoreZip.remove();
			}
			return;
		}

	}
	else
	{
		//Warning: FDB file must be unlocked and renamed to db_bcp_restore.fdb
		//if already exists, delete it
		//QFileInfo info_db_file(strDbConnect);
		QFileInfo info(strDbConnect);
		QString strDbTempCopy=info.absolutePath()+"/"+info.completeBaseName()+"_bcp_restore_nbackup.fdb";
		strDbTempCopy=QDir::cleanPath(strDbTempCopy);

		//remove old copy:
		if(QFile::exists(strDbTempCopy))
		{
			if(!QFile::remove(strDbTempCopy))
			{
				pStatus.setError(1,QString("Failed to restore database %1. File %2 is locked by another process. Please shutdown all services that are using this database!").arg(strDbConnect).arg(strDbTempCopy));
				if (bUnziped)
				{
					QFile fileRestoreZip(strDumpFilePath);
					fileRestoreZip.remove();
				}
				return;
			}
		}

		if(!QFile::rename(strDbConnect,strDbTempCopy))
		{
			pStatus.setError(1,QString("Failed to restore database %1. File is locked by another process. Please shutdown all services that are using this database!").arg(strDbConnect));
			if (bUnziped)
			{
				QFile fileRestoreZip(strDumpFilePath);
				fileRestoreZip.remove();
			}
			return;
		}


		lstArgs<<"-R";
		lstArgs<<strDbConnect;
		lstArgs<<strDumpFilePath;

		QProcess mysql;
		QString strDir=QCoreApplication::applicationDirPath()+"/firebird/bin";
		mysql.setWorkingDirectory(strDir);
		mysql.start(strDir+"/nbackup.exe",lstArgs);
		if (!mysql.waitForFinished(-1)) //wait forever
		{		
			pStatus.setError(1,QObject::tr("Failed to execute nbackup. Make sure it is in /firebird/bin subdirectory!"));
			if (bUnziped)
			{
				QFile fileRestoreZip(strDumpFilePath);
				fileRestoreZip.remove();
			}

			QFile::rename(strDbTempCopy, strDbConnect); //return back original base:
			return;
		}
		int nExitCode=mysql.exitCode();
		QByteArray errContent=mysql.readAllStandardError();
		//QByteArray fileContent=mysql.readAllStandardOutput();
		if (errContent.size()!=0)
		{
			pStatus.setError(1,QObject::tr("Failed to execute nbackup! Reason:"+errContent.left(4095)));
			if (bUnziped)
			{
				QFile fileRestoreZip(strDumpFilePath);
				fileRestoreZip.remove();
			}
			
			QFile::rename(strDbTempCopy, strDbConnect); //return back original base:
			return;
		}

		QFile::remove(strDbTempCopy); //remove original DB from disk!!
	}





	//--------------------------CHECK DB----------------------------------------------
	//read flag from DB, to ensure that DB is successfully restored



	if (bUnziped)
	{
		QFile fileRestoreZip(strDumpFilePath);
		fileRestoreZip.remove();
	}

}


void DbObjectManager_FireBird::PrepareDemoDatabase(Status &pStatus, QSqlDatabase *pDbConnection)
{

	ClearDemoDatabase(pStatus,pDbConnection);


	//create exception:
	//------------------------------------------------
	QString strSQL="CREATE EXCEPTION ERR_DB_SIZE 'Operation failed: maximum database size reached!'";
	QSqlQuery query(*pDbConnection);
	if(!query.exec(strSQL))
	{
		qDebug()<<query.lastError().text();
		pStatus.setError(StatusCodeSet::ERR_GENERAL,"Failed to prepare demo database");
		return;
	}

	//header
	QString strHeader=" DECLARE VARIABLE var_max INTEGER;";
	strHeader+=" DECLARE VARIABLE var_curr INTEGER; BEGIN ";
	strHeader+="SELECT SUM(CDI_DEMO_DB_MAX_SIZE) FROM CORE_DATABASE_INFO INTO :var_max;";
	//insert trigger: (before, check, update)
	QString strInsertCheck=" SELECT SUM(CDI_DEMO_DB_CURR_SIZE)+octet_length(%1)/1024 FROM CORE_DATABASE_INFO INTO :var_curr;";
	strInsertCheck+=" IF (var_curr > var_max) THEN EXCEPTION ERR_DB_SIZE; ELSE ";
	strInsertCheck+=" UPDATE CORE_DATABASE_INFO SET CDI_DEMO_DB_CURR_SIZE=CDI_DEMO_DB_CURR_SIZE+octet_length(%1)/1024;";
	//delete trigger: (after, update)
	QString strDeleteCheck=" UPDATE CORE_DATABASE_INFO SET CDI_DEMO_DB_CURR_SIZE=CDI_DEMO_DB_CURR_SIZE-octet_length(%1)/1024;";
	//edit triger: (before, check, update)
	QString strEditCheck=" SELECT SUM(CDI_DEMO_DB_CURR_SIZE)+octet_length(%1)/1024-octet_length(%2)/1024 FROM CORE_DATABASE_INFO INTO :var_curr;";
	strEditCheck+=" IF (var_curr > var_max) THEN EXCEPTION ERR_DB_SIZE; ELSE ";
	strEditCheck+=" UPDATE CORE_DATABASE_INFO SET CDI_DEMO_DB_CURR_SIZE=CDI_DEMO_DB_CURR_SIZE+octet_length(%1)/1024-octet_length(%2)/1024;";
	//headers;
	QString strTriggerDel="CREATE TRIGGER TRG_%1_DEL FOR %1\
						ACTIVE AFTER DELETE AS \
						BEGIN";
	QString strTriggerIns="CREATE TRIGGER TRG_%1_INS FOR %1\
						  ACTIVE BEFORE INSERT AS";
	QString strTriggerEdit="CREATE TRIGGER TRG_%1_EDIT FOR %1\
						  ACTIVE BEFORE UPDATE AS";

	
	//email: BUS_EMAIL.BEM_BODY
	//------------------------------------------------
	QString strTrigger=strTriggerDel.arg("BUS_EMAIL")+strDeleteCheck.arg("OLD.BEM_BODY")+" END ";
	//qDebug()<<strTrigger;
	if(!query.exec(strTrigger))
	{
		qDebug()<<query.lastError().text();
		pStatus.setError(StatusCodeSet::ERR_GENERAL,"Failed to prepare demo database");
		return;
	}
	strTrigger=strTriggerIns.arg("BUS_EMAIL")+strHeader+strInsertCheck.arg("NEW.BEM_BODY")+" END ";
	//qDebug()<<strTrigger;
	if(!query.exec(strTrigger))
	{
		qDebug()<<query.lastError().text();
		pStatus.setError(StatusCodeSet::ERR_GENERAL,"Failed to prepare demo database");
		return;
	}
	strTrigger=strTriggerEdit.arg("BUS_EMAIL")+strHeader+strEditCheck.arg("NEW.BEM_BODY").arg("OLD.BEM_BODY")+" END ";
	//qDebug()<<strTrigger;
	if(!query.exec(strTrigger))
	{
		qDebug()<<query.lastError().text();
		pStatus.setError(StatusCodeSet::ERR_GENERAL,"Failed to prepare demo database");
		return;
	}

	//BUS_DM_REVISIONS.BDMR_CONTENT
	//------------------------------------------------
	strTrigger=strTriggerDel.arg("BUS_DM_REVISIONS")+strDeleteCheck.arg("OLD.BDMR_CONTENT")+" END ";
	//qDebug()<<strTrigger;
	if(!query.exec(strTrigger))
	{
		qDebug()<<query.lastError().text();
		pStatus.setError(StatusCodeSet::ERR_GENERAL,"Failed to prepare demo database");
		return;
	}
	strTrigger=strTriggerIns.arg("BUS_DM_REVISIONS")+strHeader+strInsertCheck.arg("NEW.BDMR_CONTENT")+" END ";
	//qDebug()<<strTrigger;
	if(!query.exec(strTrigger))
	{
		qDebug()<<query.lastError().text();
		pStatus.setError(StatusCodeSet::ERR_GENERAL,"Failed to prepare demo database");
		return;
	}
	strTrigger=strTriggerEdit.arg("BUS_DM_REVISIONS")+strHeader+strEditCheck.arg("NEW.BDMR_CONTENT").arg("OLD.BDMR_CONTENT")+" END ";
	//qDebug()<<strTrigger;
	if(!query.exec(strTrigger))
	{
		qDebug()<<query.lastError().text();
		pStatus.setError(StatusCodeSet::ERR_GENERAL,"Failed to prepare demo database");
		return;
	}

	//BUS_CM_PICTURE.BCMPC_PICTURE
	//------------------------------------------------
	strTrigger=strTriggerDel.arg("BUS_CM_PICTURE")+strDeleteCheck.arg("OLD.BCMPC_PICTURE")+" END ";
	if(!query.exec(strTrigger))
	{
		qDebug()<<query.lastError().text();
		pStatus.setError(StatusCodeSet::ERR_GENERAL,"Failed to prepare demo database");
		return;
	}
	strTrigger=strTriggerIns.arg("BUS_CM_PICTURE")+strHeader+strInsertCheck.arg("NEW.BCMPC_PICTURE")+" END ";
	if(!query.exec(strTrigger))
	{
		qDebug()<<query.lastError().text();
		pStatus.setError(StatusCodeSet::ERR_GENERAL,"Failed to prepare demo database");
		return;
	}
	strTrigger=strTriggerEdit.arg("BUS_CM_PICTURE")+strHeader+strEditCheck.arg("NEW.BCMPC_PICTURE").arg("OLD.BCMPC_PICTURE")+" END ";
	if(!query.exec(strTrigger))
	{
		qDebug()<<query.lastError().text();
		pStatus.setError(StatusCodeSet::ERR_GENERAL,"Failed to prepare demo database");
		return;
	}

	//BUS_BIG_PICTURE.BPIC_PICTURE
	//------------------------------------------------
	strTrigger=strTriggerDel.arg("BUS_BIG_PICTURE")+strDeleteCheck.arg("OLD.BPIC_PICTURE")+" END ";
	if(!query.exec(strTrigger))
	{
		qDebug()<<query.lastError().text();
		pStatus.setError(StatusCodeSet::ERR_GENERAL,"Failed to prepare demo database");
		return;
	}
	strTrigger=strTriggerIns.arg("BUS_BIG_PICTURE")+strHeader+strInsertCheck.arg("NEW.BPIC_PICTURE")+" END ";
	if(!query.exec(strTrigger))
	{
		qDebug()<<query.lastError().text();
		pStatus.setError(StatusCodeSet::ERR_GENERAL,"Failed to prepare demo database");
		return;
	}
	strTrigger=strTriggerEdit.arg("BUS_BIG_PICTURE")+strHeader+strEditCheck.arg("NEW.BPIC_PICTURE").arg("OLD.BPIC_PICTURE")+" END ";
	if(!query.exec(strTrigger))
	{
		qDebug()<<query.lastError().text();
		pStatus.setError(StatusCodeSet::ERR_GENERAL,"Failed to prepare demo database");
		return;
	}

	//BUS_EMAIL_ATTACHMENT.BEA_CONTENT
	//------------------------------------------------
	strTrigger=strTriggerDel.arg("BUS_EMAIL_ATTACHMENT")+strDeleteCheck.arg("OLD.BEA_CONTENT")+" END ";
	if(!query.exec(strTrigger))
	{
		qDebug()<<query.lastError().text();
		pStatus.setError(StatusCodeSet::ERR_GENERAL,"Failed to prepare demo database");
		return;
	}
	strTrigger=strTriggerIns.arg("BUS_EMAIL_ATTACHMENT")+strHeader+strInsertCheck.arg("NEW.BEA_CONTENT")+" END ";
	if(!query.exec(strTrigger))
	{
		qDebug()<<query.lastError().text();
		pStatus.setError(StatusCodeSet::ERR_GENERAL,"Failed to prepare demo database");
		return;
	}
	strTrigger=strTriggerEdit.arg("BUS_EMAIL_ATTACHMENT")+strHeader+strEditCheck.arg("NEW.BEA_CONTENT").arg("OLD.BEA_CONTENT")+" END ";
	if(!query.exec(strTrigger))
	{
		qDebug()<<query.lastError().text();
		pStatus.setError(StatusCodeSet::ERR_GENERAL,"Failed to prepare demo database");
		return;
	}
}

void DbObjectManager_FireBird::ClearDemoDatabase(Status &pStatus, QSqlDatabase *pDbConnection)
{
	//DO not check for errors:
	QSqlQuery query(*pDbConnection);
	
	//email: BUS_EMAIL.BEM_BODY
	//------------------------------------------------
	query.exec("DROP TRIGGER TRG_BUS_EMAIL_DEL");
	query.exec("DROP TRIGGER TRG_BUS_EMAIL_INS");
	query.exec("DROP TRIGGER TRG_BUS_EMAIL_EDIT");

	//BUS_DM_REVISIONS.BDMR_CONTENT
	//------------------------------------------------
	query.exec("DROP TRIGGER TRG_BUS_DM_REVISIONS_DEL");
	query.exec("DROP TRIGGER TRG_BUS_DM_REVISIONS_INS");
	query.exec("DROP TRIGGER TRG_BUS_DM_REVISIONS_EDIT");
	//BUS_CM_PICTURE.BCMPC_PICTURE
	//------------------------------------------------
	query.exec("DROP TRIGGER TRG_BUS_CM_PICTURE_DEL");
	query.exec("DROP TRIGGER TRG_BUS_CM_PICTURE_INS");
	query.exec("DROP TRIGGER TRG_BUS_CM_PICTURE_EDIT");
	//BUS_BIG_PICTURE.BPIC_PICTURE
	//------------------------------------------------
	query.exec("DROP TRIGGER TRG_BUS_BIG_PICTURE_DEL");
	query.exec("DROP TRIGGER TRG_BUS_BIG_PICTURE_INS");
	query.exec("DROP TRIGGER TRG_BUS_BIG_PICTURE_EDIT");
	//BUS_EMAIL_ATTACHMENT.BEA_CONTENT
	//------------------------------------------------
	query.exec("DROP TRIGGER TRG_BUS_EMAIL_ATTACHMENT_DEL");
	query.exec("DROP TRIGGER TRG_BUS_EMAIL_ATTACHMENT_INS");
	query.exec("DROP TRIGGER TRG_BUS_EMAIL_ATTACHMENT_EDIT");


	//exception:
	//------------------------------------------------
	QString strSQL="DROP EXCEPTION ERR_DB_SIZE";
	query.exec(strSQL);
	
}


//create user: (gsec must be present)
void DbObjectManager_FireBird::CreateReadOnlyUser_Level1(Status &pStatus, QString strUserName, QString strPassword, QString strUserFirstName, QString strUserLastName, QString strMasterKey, QString strMasterPW, QSqlDatabase *pDbConnection)
{

	//--------------------------------------
	//		drop user first, ignore errors
	//--------------------------------------
	QStringList lstArgs;

	lstArgs<<"-user";
	lstArgs<<strMasterKey;
	lstArgs<<"-password";
	lstArgs<<strMasterPW;
	lstArgs<<"-delete";
	lstArgs<<strUserName;

	QProcess mysqldump;
	QString strDir=QCoreApplication::applicationDirPath()+"/firebird/bin";
	mysqldump.setWorkingDirectory(strDir);
	mysqldump.start(strDir+"/gsec.exe",lstArgs);
	mysqldump.waitForFinished(-1);


	//--------------------------------------
	//		create user
	//--------------------------------------
	lstArgs.clear();
	
	lstArgs<<"-user";
	lstArgs<<strMasterKey;
	lstArgs<<"-password";
	lstArgs<<strMasterPW;
	lstArgs<<"-add";
	lstArgs<<strUserName;
	lstArgs<<"-pw";
	lstArgs<<strPassword;
	lstArgs<<"-fname";
	lstArgs<<strUserFirstName;
	lstArgs<<"-lname";
	lstArgs<<strUserLastName;


	strDir=QCoreApplication::applicationDirPath()+"/firebird/bin";
	mysqldump.setWorkingDirectory(strDir);
	mysqldump.start(strDir+"/gsec.exe",lstArgs);
	if (!mysqldump.waitForFinished(-1)) //wait indefently
	{		
		pStatus.setError(1,QObject::tr("Failed to execute gsec. Make sure it is in /firebird/bin subdirectory!"));
		return;
	}
	int nExitCode=mysqldump.exitCode();
	QByteArray errContent=mysqldump.readAllStandardError();
	if (errContent.size()!=0)
	{
		pStatus.setError(1,QObject::tr("Failed to execute gsec! Reason:"+errContent.left(4095))); //max buff=4kb
		return;
	}


	//--------------------------------------
	// create role and assign 'em to user (we need sql query)
	//--------------------------------------

	QSqlQuery query(*pDbConnection);

	pDbConnection->transaction();
	
	QString strRoleName=strUserName+"_ReadOnlyUser_Level1";
	QString strSQL="CREATE ROLE "+strRoleName;

	//first drop role:
	QString strSQL_Delete="DROP ROLE "+strRoleName;
	query.exec(strSQL_Delete); 


	if(!query.exec(strSQL))
	{
		//error executing query
		pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, QObject::tr("Failed to create role: ")+strRoleName+" - "+query.lastError().text());
		pDbConnection->rollback();
		return;
	}

	QStringList lstTables;
	lstTables<<"bus_cm_contact"<<"bus_cm_address"<<"bus_cm_address_types"<<"bus_cm_types"<<"bus_cm_debtor"<<"bus_cm_payment_conditions"<<"bus_projects"<<"bus_person"<<"bus_nmrx_relation"<<"bus_nmrx_role";

	int nSize=lstTables.size();
	for (int i=0;i<nSize;i++)
	{
		strSQL = "GRANT SELECT ON " +lstTables.at(i) + " TO ROLE "+strRoleName;
		if(!query.exec(strSQL))
		{
			//error executing query
			pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, QObject::tr("Failed to grant select on role: ")+strRoleName+" - "+query.lastError().text());
			pDbConnection->rollback();
			return;
		}
	}

	strSQL ="GRANT "+strRoleName+" TO "+strUserName;

	if(!query.exec(strSQL))
	{
		//error executing query
		pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, QObject::tr("Failed to grant role on user: ")+strUserName+" - "+query.lastError().text());
		pDbConnection->rollback();
		return;
	}


	pDbConnection->commit();



}