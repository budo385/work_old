#include "dbomanager_mysql.h"
#include <QProcess>
#include <QDebug>
#include <QDateTime>
#include <QFileInfo>
#include <QFile>
#include <QDir>
#include "common/common/zipcompression.h"


//----------------------------------------------------
//				TABLE
//----------------------------------------------------


/*!
	Drop table if exists (case insensitive)

	\param pStatus		 - if statement fails
	\param pDbConnection - db connection
	\param strTableName  - object name
*/
void DbObjectManager_MySQL::DropIfExists_Table(Status &pStatus, QSqlDatabase *pDbConnection, QString strTableName)
{

	pStatus.setError(0);
	//check if exists:

	QString strSql = "DROP TABLE IF EXISTS " + strTableName;

	QSqlQuery query(*pDbConnection);;
	if(!query.exec(strSql))
	{
		qDebug()<<query.lastError().text();
		//error executing query
		pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, QObject::tr("Failed to delete table: ")+strTableName);
		//pStatus.setErrorDetails(query.lastError().text()); //SQL error details 
	}
}


//----------------------------------------------------
//				INDEX
//----------------------------------------------------



/*!
	Check If Exists Index (case insensitive)

	\param pDbConnection	- db connection
	\param strTableName		- table name
	\param strIndexName		- index name
	\return  1 if exists
*/
bool DbObjectManager_MySQL::CheckIfExists_Index(QSqlDatabase *pDbConnection, QString strTableName, QString strIndexName)
{
	//fetch all indexes for table
	QString strSql = "SHOW INDEX FROM " + strTableName +" FROM "+mDbSettings.m_strDbName;

	QSqlQuery query(*pDbConnection);query.setForwardOnly(true);
	if(!query.exec(strSql))
	{
		return false;
	}

	//find our index:
	int fieldNo = query.record().indexOf("Key_name");
	while (query.next()) 
	{
		if(query.value(fieldNo).toString()==strIndexName) return true;
    }

	return false;
}


/*!
	Drop index if exists (case insensitive)

	\param pStatus		 - if statement fails
	\param pDbConnection - db connection
	\param strTableName	 - table name
	\param strIndexName  - object name
*/
void DbObjectManager_MySQL::DropIfExists_Index(Status &pStatus, QSqlDatabase *pDbConnection, QString strTableName, QString strIndexName)
{

	pStatus.setError(0);
	//check if exists:
	if(!CheckIfExists_Index(pDbConnection,strTableName, strIndexName)) return;

	QString strSql = "DROP INDEX " + strIndexName+ " ON "+strTableName;

	QSqlQuery query(*pDbConnection);
	if(!query.exec(strSql))
	{
		//error executing query
		pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, QObject::tr("Failed to delete index: ")+strIndexName);
		//pStatus.setErrorDetails(query.lastError().text()); //SQL error details 
	}
}


//----------------------------------------------------
//				TRIGGER
//----------------------------------------------------


/*!
	Check If Exists Trigger (case insensitive)

	\param pDbConnection	- db connection
	\param strTriggerName   - object name
	\return  1 if exists
*/
bool DbObjectManager_MySQL::CheckIfExists_Trigger(QSqlDatabase *pDbConnection, QString strTriggerName)
{
	QString strSql = "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TRIGGERS WHERE TRIGGER_NAME='"+strTriggerName+"' AND TRIGGER_SCHEMA = '"+mDbSettings.m_strDbName+"'";
	QSqlQuery query(*pDbConnection);
	if(!query.exec(strSql))
	{
		return false;
		//error executing query
		//pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, QObject::tr("Failed check trigger: ")+strTriggerName);
		//pStatus.setErrorDetails(query.lastError().text()); //SQL error details 
	}


	query.next();
	int nCount = query.value(0).toInt();

	if( nCount==0)
		return false;
	else
		return true;
}


/*!
	Drop Trigger if exists (case insensitive)

	\param pStatus		 - if statement fails
	\param pDbConnection - db connection
	\param strTriggerName  - object name
*/
void DbObjectManager_MySQL::DropIfExists_Trigger(Status &pStatus, QSqlDatabase *pDbConnection, QString strTriggerName)
{

	pStatus.setError(0);
	//check if exists:
	if(!CheckIfExists_Trigger(pDbConnection,strTriggerName)) return;

	QString strSql = "DROP TRIGGER " + strTriggerName;

	QSqlQuery query(*pDbConnection);
	if(!query.exec(strSql))
	{
		//error executing query
		pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, QObject::tr("Failed to delete trigger: ")+strTriggerName);
		//pStatus.setErrorDetails(query.lastError().text()); //SQL error details 
	}
}


//drops all triggers in the database:
void DbObjectManager_MySQL::DropAllTriggers(Status &pStatus, QSqlDatabase *pDbConnection)
{
	pStatus.setError(0);

	QString strSql = "SELECT TRIGGER_NAME FROM INFORMATION_SCHEMA.TRIGGERS WHERE TRIGGER_SCHEMA = '"+mDbSettings.m_strDbName+"'";
	QSqlQuery query(*pDbConnection);
	if(!query.exec(strSql))
	{
		pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, QObject::tr("Failed to delete triggers"));
		return;

	}

	while (query.next())
	{
		QString strTriggerName=query.value(0).toString();
		QString strSql = "DROP TRIGGER " + strTriggerName;

		QSqlQuery query(*pDbConnection);
		if(!query.exec(strSql))
		{
			//error executing query
			pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, QObject::tr("Failed to delete triggers"));	
			return;
		}
	}

}


//drops all constr in the database:
void DbObjectManager_MySQL::DropAllConstraints(Status &pStatus, QSqlDatabase *pDbConnection)
{
	pStatus.setError(0);

	QString strSql = "SELECT TABLE_NAME,CONSTRAINT_NAME,CONSTRAINT_TYPE FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE TABLE_SCHEMA = '"+mDbSettings.m_strDbName+"'";
	QSqlQuery query(*pDbConnection);
	if(!query.exec(strSql))
	{
		pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, QObject::tr("Failed to delete constraints"));
		return;
	}

	while (query.next())
	{
		//if(query.value(1).toString()=="PRIMARY" || query.value(2).toString()=="UNIQUE")continue;  //skip primary & unqiue indexes:

		if(query.value(2).toString()!="FOREIGN KEY") continue;
		strSql = "ALTER TABLE "+query.value(0).toString()+" DROP FOREIGN KEY "+query.value(1).toString();
		//else
		//	strSql = "ALTER TABLE "+query.value(0).toString()+" DROP KEY "+query.value(1).toString();

		//qDebug()<<strSql;
		QSqlQuery query(*pDbConnection);
		if(!query.exec(strSql))
		{
			//error executing query
			qDebug()<<query.lastError().text();
			pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, QObject::tr("Failed to delete constraints"));
		}
	}

}



//----------------------------------------------------
//				SEQUENCE
//----------------------------------------------------



/*!
	Check If Exists Sequence (case insensitive)

	\param pDbConnection	- db connection
	\param strSeqName   - object name
	\return  1 if exists
*/
bool DbObjectManager_MySQL::CheckIfExists_Sequence(QSqlDatabase *pDbConnection, QString strSeqName)
{

	Q_ASSERT_X(false,"MYSQL", "Trying to use sequnce with MYSQL DB!!!");
		return false;
}


/*!
	Drop Sequence if exists (case insensitive)

	\param pStatus		 - if statement fails
	\param pDbConnection - db connection
	\param strSeqName  - object name
*/
void DbObjectManager_MySQL::DropIfExists_Sequence(Status &pStatus, QSqlDatabase *pDbConnection, QString strSeqName)
{

	pStatus.setError(0);
	Q_ASSERT_X(false,"MYSQL", "Trying to use sequnce with MYSQL DB!!!");
		return;
}


/*!
	Create Sequence, drop if exists

	\param pStatus		 - if statement fails
	\param pDbConnection - db connection
	\param strSeqName	 - object name
	\param nStartCounter - start sequence from this number
*/
void DbObjectManager_MySQL::CreateSequence(Status &pStatus, QSqlDatabase *pDbConnection, QString strSeqName, int nStartCounter)
{

	pStatus.setError(0);
	Q_ASSERT_X(false,"MYSQL", "Trying to use sequnce with MYSQL DB!!!");
		return;
}



//----------------------------------------------------
//				PROCEDURE
//----------------------------------------------------



/*!
	Check If Exists PROCEDURE (case insensitive)

	\param pDbConnection	- db connection
	\param strProcName   - object name
	\return  1 if exists
*/
bool DbObjectManager_MySQL::CheckIfExists_Proc(QSqlDatabase *pDbConnection, QString strProcName)
{
	QString strSql = "SELECT COUNT(*) FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME='"+strProcName+"' AND ROUTINE_SCHEMA = '"+mDbSettings.m_strDbName+"'";
	QSqlQuery query(*pDbConnection);
	if(!query.exec(strSql))
	{
		return false;
		//error executing query
		//pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, QObject::tr("Failed check sequence: ")+strProcName);
		//pStatus.setErrorDetails(query.lastError().text()); //SQL error details 
	}


	query.next();
	int nCount = query.value(0).toInt();

	if( nCount==0)
		return false;
	else
		return true;
}


/*!
	Drop PROCEDURE if exists (case insensitive)

	\param pStatus		 - if statement fails
	\param pDbConnection - db connection
	\param strProcName  - object name
*/
void DbObjectManager_MySQL::DropIfExists_Proc(Status &pStatus, QSqlDatabase *pDbConnection, QString strProcName)
{

	pStatus.setError(0);
	//check if exists:
	if(!CheckIfExists_Proc(pDbConnection,strProcName)) return;

	QString strSql = "DROP PROCEDURE " + strProcName;

	QSqlQuery query(*pDbConnection);
	if(!query.exec(strSql))
	{
		//error executing query
		pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, QObject::tr("Failed to delete procedure: ")+strProcName);
		//pStatus.setErrorDetails(query.lastError().text()); //SQL error details 
	}
}





//----------------------------------------------------
//				FUNCTION
//----------------------------------------------------



/*!
	Check If Exists FUNCTION (case insensitive)

	\param pDbConnection	- db connection
	\param strFunctName   - object name
	\return  1 if exists
*/
bool DbObjectManager_MySQL::CheckIfExists_Funct(QSqlDatabase *pDbConnection, QString strFunctName)
{

	QString strSql = "SELECT COUNT(*) FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME='"+strFunctName+"' AND ROUTINE_SCHEMA = '"+mDbSettings.m_strDbName+"'";
	QSqlQuery query(*pDbConnection);
	if(!query.exec(strSql))
	{
		//error executing query
		return false;
		//pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, QObject::tr("Failed check function: ")+strFunctName);
		//pStatus.setErrorDetails(query.lastError().text()); //SQL error details 
	}


	query.next();
	int nCount = query.value(0).toInt();

	if( nCount==0)
		return false;
	else
		return true;
}


/*!
	Drop FUNCTION if exists (case insensitive)

	\param pStatus		 - if statement fails
	\param pDbConnection - db connection
	\param strFunctName  - object name
*/
void DbObjectManager_MySQL::DropIfExists_Funct(Status &pStatus, QSqlDatabase *pDbConnection, QString strFunctName)
{

	pStatus.setError(0);
	//check if exists:
	if(!CheckIfExists_Proc(pDbConnection,strFunctName)) return;

	QString strSql = "DROP FUNCTION " + strFunctName;

	QSqlQuery query(*pDbConnection);
	if(!query.exec(strSql))
	{
		//error executing query
		pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, QObject::tr("Failed to delete procedure: ")+strFunctName);
		//pStatus.setErrorDetails(query.lastError().text()); //SQL error details 
	}
}




//----------------------------------------------------
//				AUTO INCREMENT ID
//----------------------------------------------------



/*!
	For MySQL we use auto inc fields, empty funct

	\param pStatus				 -error
	\param pDbConnection		 -db connection
	\param strTableName			 -table name
	\param strAutoIncFieldName   -field on which ID will be created
*/
void DbObjectManager_MySQL::CreateAutoIncrementID(Status &pStatus, QSqlDatabase *pDbConnection,QString strTableName, QString strAutoIncFieldName)
{

	//Already OK!!
	pStatus.setError(0);
}



//skip primary:
void DbObjectManager_MySQL::DropAllIndexes(Status &pStatus, QString strTableName,QSqlDatabase *pDbConnection)
{
	QString strSql = "SHOW INDEX FROM " + strTableName +" FROM "+mDbSettings.m_strDbName;
	QSqlQuery query(*pDbConnection);query.setForwardOnly(true);
	if(!query.exec(strSql))
	{
		pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, query.lastError().text());
		return;
	}

	//delete them all:
	int fieldNo = query.record().indexOf("Key_name");
	while (query.next()) 
	{
		QString strIndexName=query.value(fieldNo).toString();
		if (strIndexName=="PRIMARY") continue;
		DropIfExists_Index(pStatus,pDbConnection,strTableName,query.value(fieldNo).toString());
		if (!pStatus.IsOK())
			return;
	}
}

//use mysqldump, make sure that path is in system path
//WARNING: USE INNODB storage engine
//NOTE: to enable more quicker backups use binray logs (not used by default)
//strDumpFilePath is path to backup directory: C:/backup
//name is formatted as: DATABASE_DBVER_YYYYMMDDHHMM_COUNT where count is incremental number if there more then 1 backup generated in one minute
//returns: strDumpFilePath will contain full path to newly genreated backup file
QString DbObjectManager_MySQL::BackupDatabase(Status &pStatus, QString strDatabaseName, QString strUser, QString strPassword,QString &strDumpFilePath,QString strDatabaseVersion,QString strHost, QString strPort,QSqlDatabase *pDbConnection)
{

	//-----------------------GENERATE NEW BACKUP FILE NAME---------------------------------

	//check if already exists same name in directory:
	int nCount=0;
	QString strDumpFileName;
	QString strCompleteDumpFilePath;
	do 
	{
		strDumpFileName=strDatabaseName+"_"+strDatabaseVersion+"_"+QDateTime::currentDateTime().toString("yyyyMMddHHmm")+"_"+QVariant(nCount).toString()+".bcp";
		strCompleteDumpFilePath=strDumpFilePath+"/"+strDumpFileName;
		QString strFileCheckZip=strCompleteDumpFilePath;
		strFileCheckZip.replace(".bcp",".zip");
		QFileInfo fileExistCheck(strFileCheckZip);
		if (!fileExistCheck.exists()) break;
		nCount++;
	} while(nCount<100);


	if (nCount==100)
	{
		pStatus.setError(1,QObject::tr("Can not determine new backup file name, backup creation failed!"));
		return "";

	}


	//-----------------------BACKUP---------------------------------

	QStringList lstArgs;

	lstArgs<<"-u";
	lstArgs<<strUser;
	lstArgs<<"-p"+strPassword;
	lstArgs<<"--opt";
	lstArgs<<"--skip-add-locks";
	lstArgs<<"--skip-lock-tables";
	lstArgs<<"--single-transaction";
	lstArgs<<"--routines";
	lstArgs<<"--triggers";
	lstArgs<<"--hex-blob";
	lstArgs<<"--complete-insert";
	lstArgs<<strDatabaseName;

	QProcess mysqldump;
	mysqldump.setStandardOutputFile(strCompleteDumpFilePath);
	mysqldump.start("mysqldump",lstArgs);
	if (!mysqldump.waitForFinished(-1)) //wait indefently
	{		
		pStatus.setError(1,QObject::tr("Failed to execute mysldump!"));
		return "";
	}
	int nExitCode=mysqldump.exitCode();
	QByteArray errContent=mysqldump.readAllStandardError();
	//QByteArray fileContent=mysqldump.readAllStandardOutput();
	if (errContent.size()!=0)
	{
		pStatus.setError(1,QObject::tr("Failed to execute mysldump! Reason:"+errContent));
		return "";
	}

	//-----------------------MODIFY BACKUP FILE ---------------------------------
	QByteArray fileContent;
	QFile file(strCompleteDumpFilePath);
	if(!file.open(QIODevice::ReadWrite))
	{
		pStatus.setError(1,QObject::tr("Backup file can not be written to disk!"));
		return "";
	}

	fileContent=file.readAll();
	fileContent.prepend("SET AUTOCOMMIT = 0; SET FOREIGN_KEY_CHECKS=0;");
	fileContent.append("SET FOREIGN_KEY_CHECKS = 1; COMMIT; SET AUTOCOMMIT = 1; ");

	//separete version:
	int nCommunicatorVersion=strDatabaseName.left(strDatabaseName.indexOf("SPC")).toInt();
	fileContent.append("UPDATE CORE_DATABASE_INFO SET CDI_DATABASE_VERSION="+QVariant(nCommunicatorVersion).toString()+", CDI_LAST_RESTORE_FILE= '"+strDumpFileName+"'"); //add only dump file name (no path)
	
	//-----------------------WRITE IT BACK ---------------------------------
	file.reset();
	file.write(fileContent);
	file.close();

	//-----------------------ZIP & DELETE ORIGINAL IF SUCESS ---------------------------------


	QFileInfo infoBcp(strCompleteDumpFilePath);
	QString strZip=infoBcp.absolutePath()+"/"+infoBcp.baseName()+".zip";
	QStringList lstIn,lstDirs;
	lstIn<<strCompleteDumpFilePath;
	if(!ZipCompression::ZipFiles(strZip,lstIn))
		strDumpFilePath=strCompleteDumpFilePath;
	else
	{
		strDumpFilePath=strZip;
		file.remove();
	}

	return infoBcp.baseName();
		
}

//use mysql, make sure that path is in system path
//strDumpFilePath -complete path to bcp file
void DbObjectManager_MySQL::RestoreDatabase(Status &pStatus, QString strDatabaseName, QString strUser, QString strPassword,QString strDumpFilePath,QString strHost, QString strPort,QSqlDatabase *pDbConnection)
{

	qDebug()<<"Database restore from backup in progress";
	//---------------------------CHECK FILE----------------------------------------------
	QFileInfo info(strDumpFilePath);
	if (!info.exists())
	{
		QString strMsg=QObject::tr("Database restore from ")+strDumpFilePath+QObject::tr(" failed! File not found!");
		pStatus.setError(1,strMsg);
		return;
	}


	//---------------------------UNZIP IT----------------------------------------------

	bool bUnziped=false;
	if (info.suffix()=="zip") //restore if zipped:
	{
		QStringList lstData;
		if(!ZipCompression::Unzip(QDir::toNativeSeparators(strDumpFilePath),QDir::toNativeSeparators(info.absolutePath()),lstData))
		{
			QString strMsg=QObject::tr("Database restore from ")+strDumpFilePath+QObject::tr(" failed! Can not unzip file: ")+strDumpFilePath;
			pStatus.setError(1,strMsg);
			return;
		}
		strDumpFilePath=info.absolutePath()+"/"+info.baseName()+".bcp";;
		bUnziped=true;
	}
	



	//--------------------------RESTORE DB----------------------------------------------
	QStringList lstArgs;

	lstArgs<<"-u";
	lstArgs<<strUser;
	lstArgs<<"-p"+strPassword;
	lstArgs<<strDatabaseName;

	QProcess mysql;
	mysql.setStandardInputFile(strDumpFilePath);
	mysql.start("mysql",lstArgs);
	if (!mysql.waitForFinished(-1)) //wait forever
	{		
		pStatus.setError(1,QObject::tr("Failed to execute mysql!"));
		return;
	}
	int nExitCode=mysql.exitCode();
	QByteArray errContent=mysql.readAllStandardError();
	//QByteArray fileContent=mysql.readAllStandardOutput();
	if (errContent.size()!=0)
	{
		pStatus.setError(1,QObject::tr("Failed to execute mysql! Reason:"+errContent));
		return;
	}


	//--------------------------CHECK DB----------------------------------------------
	//read flag from DB, to ensure that DB is successfully restored

	/*
	if (!bSkipVersionCheck) //only if allowed:
	{
		QString strLastRestoreFile;
		//make query
		QString strSQL="SELECT CDI_LAST_RESTORE_FILE FROM CORE_DATABASE_INFO";
		QSqlQuery query(*pDbConnection);
		query.setForwardOnly(true);
		if(!query.exec(strSQL))
		{
			pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, query.lastError().text());
			return;
		}

		//read current version
		if (query.next())
			strLastRestoreFile=query.value(0).toString();

		//check file:
		//qDebug()<<info.fileName();
		QFileInfo info2(strDumpFilePath);
		if (strLastRestoreFile!=info2.fileName())
		{
			QString strMsg=QObject::tr("Database restore from ")+strDumpFilePath+QObject::tr(" failed! Version Check Flag does not match! Database is corrupted, please restore again!");
			pStatus.setError(1,strMsg);
			return;
		}

	}

*/

	if (bUnziped)
	{
		QFile fileRestoreZip(strDumpFilePath);
		fileRestoreZip.remove();
	}

}

//called on end of org, to make possible business procedures update...
void DbObjectManager_MySQL::DropColumns(Status &pStatus, QString strTableName, QStringList lstDeletedColumns, QSqlDatabase *pDbConnection)
{

	QHash<QString,QStringList> lstCols,lstColsDb;
	QSqlQuery query(*pDbConnection);

	//get table cols from db
	if(!GetTableColumnsFromDb(strTableName,lstColsDb,pDbConnection))
	{
		pStatus.setError(1,QObject::tr("Failed to fetch table info from database for table: ")+ strTableName);
		return;
	}


	//drop all that have to be dropped
	//--------------
	QString strDrop;
	int nSize=lstDeletedColumns.size();
	for(int i=0;i<nSize;++i)
	{
		if (lstColsDb.contains(lstDeletedColumns.at(i)))
			strDrop+="DROP "+lstDeletedColumns.at(i)+",";
	}
	if (!strDrop.isEmpty())
	{
		strDrop.chop(1);
		strDrop="ALTER TABLE "+strTableName+" "+strDrop;
		if(!query.exec(strDrop))
		{
			pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, query.lastError().text());
			return;
		}
	}

}



//built for MYSQL:
//core function for Db upgrade: compares table creation to db, alters table:
//table: must exists....
//- col del
//- col rename
//- col datatype change
void DbObjectManager_MySQL::AlterTable(Status &pStatus, QString strTableName, QString strTableCreationStatement,QStringList lstDeletedColumns, QList<QStringList> lstChangedNameColumns,QSqlDatabase *pDbConnection,bool bFillDefaultValuesForNewFields)
{
	QHash<QString,QStringList> lstCols,lstColsDb;
	QSqlQuery query(*pDbConnection);

	//get table cols/data type from string
	if(!GetTableColumnsFromString(strTableCreationStatement,lstCols))
	{
		pStatus.setError(1,QObject::tr("Failed to parse create table statement for table: ")+ strTableName);
		return;
	}

	//get table cols from db
	if(!GetTableColumnsFromDb(strTableName,lstColsDb,pDbConnection))
	{
		pStatus.setError(1,QObject::tr("Failed to fetch table info from database for table: ")+ strTableName);
		return;
	}



	//drop all that have to be dropped
	//--------------
	QString strDrop;
	int nSize=lstDeletedColumns.size();
	for(int i=0;i<nSize;++i)
	{
		if (lstColsDb.contains(lstDeletedColumns.at(i)))
			strDrop+="DROP "+lstDeletedColumns.at(i)+",";
	}
	if (!strDrop.isEmpty())
	{
		strDrop.chop(1);
		strDrop="ALTER TABLE "+strTableName+" "+strDrop;
		if(!query.exec(strDrop))
		{
			pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, query.lastError().text());
			return;
		}
	}

	//change names all that have to be changed
	//--------------
	QString strChange;
	nSize=lstChangedNameColumns.size();
	for(int i=0;i<nSize;++i)
	{
		if (lstColsDb.contains(lstChangedNameColumns.at(i).at(0)))
		{
			QStringList lstDbParams=lstColsDb[lstChangedNameColumns.at(i).at(0)];
			strChange+="CHANGE "+lstDbParams.at(0)+" "+lstChangedNameColumns.at(i).at(1)+" "+lstDbParams.at(1)+" "+lstDbParams.at(2)+",";
		}
	}
	if (!strChange.isEmpty())
	{
		strChange.chop(1);
		strChange="ALTER TABLE "+strTableName+" "+strChange;
		if(!query.exec(strChange))
		{
			pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, query.lastError().text());
			return;
		}
	}

	//reload from db if drop or name changed occured:
	if (!strDrop.isEmpty() || !strChange.isEmpty())
	{
		GetTableColumnsFromDb(strTableCreationStatement,lstColsDb,pDbConnection);
	}


	//alter: change data type or add new col
	//compare by name: if found test is data type is changed, else add new
	QString strAlter;
	QString strUpdateFillDefaultVals;
	QHashIterator <QString,QStringList> x(lstCols);
	while (x.hasNext()) 
	{
		x.next();


		if (lstColsDb.contains(x.key())) //change data type
		{
			QStringList lstDbParams=lstColsDb[x.key()];
			QStringList lstNewParams=x.value();

			if (lstDbParams.at(1)!=lstNewParams.at(1) || lstDbParams.at(2)!=lstNewParams.at(2))
			{


				if (lstNewParams.at(1)=="INTEGER" && lstDbParams.at(1).indexOf("INT")>=0)continue;					//mysql special case: int(11)==integer
				if (lstNewParams.at(1)=="SMALLINT" && lstDbParams.at(1).indexOf("SMALLINT")>=0)continue;			//mysql special case: smallint(6)==smallint
				if (lstNewParams.at(1)=="TIMESTAMP" && lstDbParams.at(1).indexOf("DATETIME")>=0)continue;			//firebird: timestamp=datetime
				strAlter+="MODIFY "+lstNewParams.at(0)+" "+lstNewParams.at(1)+" "+lstNewParams.at(2)+",";
			}
		}
		else			//add column
		{

			QStringList lstDbParams=lstColsDb[x.key()];
			QStringList lstNewParams=x.value();
			strAlter+="ADD COLUMN "+lstNewParams.at(0)+" "+lstNewParams.at(1)+" "+lstNewParams.at(2)+",";

			if (bFillDefaultValuesForNewFields)
			{
				if (!(lstNewParams.at(1).toLower().contains("timestamp") || lstNewParams.at(1).toLower().contains("datetime")  || lstNewParams.at(1).toLower().contains("date")))
				{
					if (lstNewParams.at(1).toLower().contains("char") || lstNewParams.at(1).toLower().contains("varchar") || lstNewParams.at(1).toLower().contains("blob"))
						strUpdateFillDefaultVals+=lstNewParams.at(0)+"=0,";
					else
						strUpdateFillDefaultVals+=lstNewParams.at(0)+"='',";
				}
			}
		}
	}

	if (!strAlter.isEmpty())
	{
		qDebug()<<"Altering table: "<<strTableName;

		strAlter.chop(1);
		strAlter="ALTER TABLE "+strTableName+" "+strAlter;
		if(!query.exec(strAlter))
		{
			pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, query.lastError().text());
			return;
		}
	}

	if (!strUpdateFillDefaultVals.isEmpty())
	{
		qDebug()<<"Filling default values for table: "<<strTableName;

		if(strUpdateFillDefaultVals.right(1)==",") //if , then chop
			strUpdateFillDefaultVals.chop(1);

		strUpdateFillDefaultVals="UPDATE "+strTableName+" SET "+strUpdateFillDefaultVals;
		if(!query.exec(strUpdateFillDefaultVals))
		{
			pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, query.lastError().text());
			return;
		}
	}

}



//default implementation is for MYSQL:
bool DbObjectManager_MySQL::GetTableColumnsFromDb(QString strTableName,QHash<QString,QStringList> &lstColsData,QSqlDatabase *pDbConnection)
{
	//get table desc:
	QSqlQuery query(*pDbConnection);
	if(!query.exec("DESCRIBE "+strTableName)) return false;

	while (query.next())
	{
		QStringList lstData;
		lstData.append(query.value(0).toString().toUpper().trimmed());
		lstData.append(query.value(1).toString().toUpper().trimmed());
		if (query.value(2).toString().toUpper()=="YES")
			lstData.append("null");
		else
			lstData.append("not null");

		lstColsData[lstData.at(0)]=lstData;
	}

	return true;
}


