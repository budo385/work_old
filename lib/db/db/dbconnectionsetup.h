#ifndef DBCONNECTIONSETUP_H
#define DBCONNECTIONSETUP_H

#include <QString>
#include "common/common/status.h"
#include "db_core/db_core/dbconnsettings.h"

class DbConnectionSetup  
{
public:
	DbConnectionSetup();
	~DbConnectionSetup();

	static bool SetupThickDatabase(Status &pStatus,QString& strConnectionName);
	static bool SetupAppServerDatabase(Status &pStatus,QString strUser,QString strPass,QString strDbType,QString strDatabaseName,QString& strConnectionName,QString strDirPath="",QString strDbConnectionFilePath="");
	static bool CreateDatabaseConnection(Status &pStatus,QString strFile,DbConnectionSettings settings);
	static bool GetDatabaseConnection(Status &pStatus,QString strFile,QString strConnName,DbConnectionSettings &settings);

	static void CreateFireBirdDatabase(Status &pStatus,QString strUser,QString strPass,QString strDatabasePath);
	static void CreateMySQLDatabase(Status &pStatus,QString strUser,QString strPass,QString strDatabaseName);

private:
	
};

#endif // DBCONNECTIONSETUP_H
