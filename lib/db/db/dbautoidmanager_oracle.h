#ifndef DBAUTOIDMANAGERORACLE_H
#define DBAUTOIDMANAGERORACLE_H

#include <QSqlDatabase>
#include <QSqlQuery>
#include "common/common/status.h"
#include "db/db/dbautoidmanager.h"



/*!
	\class DbAutoIdManagerOracle
	\ingroup SQLModule
	\brief Implements a class to return na ID for a last insert/update query call

	Use as static

*/

class DbAutoIdManagerOracle : public DbAutoIdManager
{
public:
	int getNextInsertId(Status &status, QSqlDatabase *pDbConnection, QString strPrimaryKey,int nPoolSize=1){Q_ASSERT(false);return 0;}; //not supported!;
	int getLastInsertId(Status &status, QSqlDatabase *pDbConnection, QString strPrimaryKey);
};

#endif //DBAUTOIDMANAGERORACLE_H


