#ifndef DBCURRENTTIME_FIREBIRD_H
#define DBCURRENTTIME_FIREBIRD_H

#include "dbcurrenttime.h"



/*!
	\class DbCurrentTime_Oracle
	\ingroup SQLModule
	\brief Abstract class for implementation of currenttime() function for ORACLE
*/
class DbCurrentTime_FireBird : public DbCurrentTime
{
public:

	QDateTime GetCurrentDateTime(Status &status, QSqlDatabase *pDbConnection);
};


#endif // DBCURRENTTIME_FIREBIRD_H
