#ifndef DBDATATYPECONVERTOR_ORACLE_H
#define DBDATATYPECONVERTOR_ORACLE_H


#include "dbdatatypeconvertor.h"


class DbDataTypeConvertor_Oracle : public DbDataTypeConvertor
{

public:
	QVariant ConvertValue(QVariant& databaseValue, int colTypeInRecordSet);
};

#endif // DBDATATYPECONVERTOR_ORACLE_H
