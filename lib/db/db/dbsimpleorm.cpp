#include "db/db/dbsimpleorm.h"
#include "common/common/config.h"
#include "db_core/db_core/dbsqltableview.h"


/*!
	Sets all parameters here, not multithread safe
	\param pStatus			- error
	\param TableID			- table id
	\param pManager			- db manager
	\param strUserSession	- user session id (for locking)
	\param nUserID			- user id (for history table)
	\param pDb				- external connection, if NULL then own connection will be reserved

*/
DbSimpleOrm::DbSimpleOrm(Status &pStatus,int TableID,DbSqlManager *pManager)
:m_Query(NULL),m_pManager(NULL),m_pDb(NULL)
{
	pStatus.setError(0);

	//table data
	m_nTableID		 = TableID;
	DbSqlTableDefinition::GetKeyData(m_nTableID,m_pTableKeyData);
	m_nTableFullView = m_pTableKeyData.m_nViewID;	
	m_strPrimaryKey=m_pTableKeyData.m_strPrimaryKey;
	m_strLastModified=m_pTableKeyData.m_strLastModified;
	
	//db connection
	m_pManager		 = pManager;
	Q_ASSERT(m_pManager!=NULL);

	//create query object: if incoming m_pDb==NULL query will reserve own
	m_Query= new DbSqlQuery(pStatus,m_pManager);
	if(!pStatus.IsOK()) {return;}
	m_pDb = m_Query->GetDbConnection();

	Q_ASSERT(m_pDb != NULL);
	
}

DbSimpleOrm::~DbSimpleOrm()
{
	if(m_Query!=NULL)delete m_Query;
}




/*!
	Write to database from &pLstForWrite, return details in &pLstStatusRows and error in pStatus.
	Write records by FULL table view.
	Note: pLstForWrite must contain mandatory field LastModified if table has one, otherwise not.
	WARNING: if not in transaction, error will be return even if one row fails to write

	\param pStatus,			- error
	\param pLstForWrite		- list of records for write 
	\param nQueryView		- instead of using full table view (table), record set can be written by nQueryView (e.g. to update only some columns ), -1 : use full view
	\param nSkipLastColumns	- number of columns to skip from end of recordset (query must be valid, but recordset can be expanded with dynamic cols)
	\param pLstStatusRows	- list of statuses for each row (if passed by, then all records will be written and errors stored in list, else all in 1 transaction 
	\return bool			- if status==error, then FALSE
*/
void DbSimpleOrm::Write(Status &pStatus, DbRecordSet &pLstForWrite, int nQueryView, int nSkipLastColumns,DbRecordSet *pLstStatusRows)
{
	pStatus.setError(StatusCodeSet::ERR_NONE);

	if(nQueryView==-1)nQueryView=m_nTableFullView;
	DbView viewData=DbSqlTableView::getView(nQueryView);
	int nInsertSkipFirstCol=viewData.m_nSkipFirstColsInsert;
	int nUpdateSkipFirstCol=viewData.m_nSkipFirstColsInsert;
	nSkipLastColumns=viewData.m_nSkipLastColsWrite+nSkipLastColumns; 

	if (pLstStatusRows)
	{
		FormatStatusRowsLst(pLstStatusRows);
		pLstStatusRows->addRow(pLstForWrite.getRowCount());
	}

	int nPrimaryKeyColumn=GetPrimaryColumnNo(pLstForWrite);
	int nLastModifiedColumn=-1;
	if(!m_strLastModified.isEmpty())
	{
		nLastModifiedColumn=pLstForWrite.getColumnIdx(m_strLastModified);
		Q_ASSERT(nLastModifiedColumn >= 0);
	}

	bool bTransaction=true;
	if (pLstStatusRows!=NULL || pLstForWrite.getRowCount()==1) //no transaction if only 1 row or status list is active
		bTransaction=false;


	if (bTransaction)
	{	
		m_Query->BeginTransaction(pStatus);
		if(!pStatus.IsOK())	return;
	}

	DoUpdateRows(pStatus,pLstForWrite,nQueryView,nPrimaryKeyColumn,nLastModifiedColumn,viewData,nUpdateSkipFirstCol,nSkipLastColumns,pLstStatusRows);
	if (!pStatus.IsOK())
	{
		if (bTransaction)
			m_Query->Rollback();
		return;
	}
	DoInsertRows(pStatus,pLstForWrite,nQueryView,nPrimaryKeyColumn,nLastModifiedColumn,viewData,nInsertSkipFirstCol,nSkipLastColumns,pLstStatusRows);
	if (!pStatus.IsOK())
	{
		if (bTransaction)
			m_Query->Rollback();
		return;
	}

	//Get current timestamp: set to all records to avoid needleeds DB reading (this is OK if in transaction, TIME remains same):
	if (nLastModifiedColumn>=0)
	{
		QDateTime datLastModified=m_pManager->GetCurrentDateTime(pStatus,m_pDb);
		if (!pStatus.IsOK())
		{
			if (bTransaction)
				m_Query->Rollback();
			return;
		}
		pLstForWrite.setColValue(nLastModifiedColumn,datLastModified);
	}

	if (bTransaction)
		m_Query->Commit(pStatus);
}


/*!
	Read records by FULL view
	\param pStatus			- error
	\param pLstRead			- list of records for read, list is recreated 1:1 to full table view query result 
	\param strWhereClause	- additional WHERE clause
	\param nQueryView		- instead of using full table view (table), record set can be fetched by nQueryView
*/
void DbSimpleOrm::Read (Status &pStatus, DbRecordSet &pLstRead, QString strWhereClause, int nQueryView, bool bDistinct)
{
	//determine what query to use:
	if(nQueryView==-1)nQueryView=m_nTableFullView;

	QString strValue=BindLike(strWhereClause); //if SQL contain LIKE ->replace it and bind it....
	if (strValue.isEmpty())
	{
		if(m_Query->Execute(pStatus, DbSqlTableView::getSQLView(nQueryView, bDistinct) + " " + strWhereClause))
		{
			pLstRead.defineFromView(DbSqlTableView::getView(nQueryView));	//redefine	to full view
			m_Query->FetchData(pLstRead);					//read all by names (if not said other).
		}
	}
	else
	{
		m_Query->Prepare(pStatus,DbSqlTableView::getSQLView(nQueryView, bDistinct) + " " + strWhereClause);
		if (!pStatus.IsOK())
			return;
		m_Query->bindValue(0,strValue);
		if(m_Query->ExecutePrepared(pStatus))
		{
			pLstRead.defineFromView(DbSqlTableView::getView(nQueryView));	//redefine	to full view
			m_Query->FetchData(pLstRead);					//read all by names (if not said other).
		}
	}
}




/*!
	Delete records from &pLstForDelete, always all in nothing (transaction)
	Uses chunk technology, (chunk=200) so if deleting <200 it is one SQL query!

	\param pStatus,			- error
	\param pLstForDelete	- list of records for delete
	\param pBoolLock		- lock before write
	\param strWhereClause	- special additional condition to be set (must begin with " AND ...."
	\return bool			- if status==error, then FALSE
*/
bool DbSimpleOrm::DeleteFast(Status &pStatus, DbRecordSet &pLstForDelete,QString strWhereClause)//,bool pBoolLock)
{
	pStatus.setError(0);
	int nPrimaryKeyColumn=GetPrimaryColumnNo(pLstForDelete);

	m_Query->BeginTransaction(pStatus);
	if(!pStatus.IsOK())
	{
		return false;
	}

	int nChunk = 200;	//chunks of 200 max
	QString strWhere,strSQL;

	//prepare select & update SQL's
	QString strSqlStart = "DELETE FROM "+ m_pTableKeyData.m_strTableName;

	int nSize=pLstForDelete.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		if(strWhere.isEmpty())
			strWhere = " WHERE "+m_pTableKeyData.m_strPrimaryKey+" IN ("+pLstForDelete.getDataRef(i, nPrimaryKeyColumn).toString();
		else
			strWhere+= ","+pLstForDelete.getDataRef(i, nPrimaryKeyColumn).toString();

		//execute chunk:
		if(i>nChunk||i==nSize-1)
		{
			strSQL=strSqlStart+strWhere+") "+strWhereClause;	
			nChunk+=200; //next 200
			if(!m_Query->Execute(pStatus,strSQL))
			{
				m_Query->Rollback();
				return false;			
			}
			strWhere="";
		}
	}

	//commit & unlock:
	m_Query->Commit(pStatus);
	if(pStatus.IsOK())
		return true;
	else
		return false;
}

/*!
	Same as above
*/
bool DbSimpleOrm::DeleteFast(Status &pStatus, QList<int> &pLstForDelete, QString strWhereClause)//,bool pBoolLock )
{
	DbRecordSet temp;
	DbSqlTableDefinition::CopyFromList2RecordSet(m_nTableID,pLstForDelete,temp);
	return DeleteFast(pStatus, temp,strWhereClause);
}






//--------------------------------------------------------------------
//								HELPER
//--------------------------------------------------------------------



void DbSimpleOrm::FormatStatusRowsLst(DbRecordSet *pLstStatusRows)
{
	if(pLstStatusRows==NULL)return;
	pLstStatusRows->destroy();
	pLstStatusRows->addColumn(QVariant::Int, QString("ID"));
	pLstStatusRows->addColumn(QVariant::Int, QString("STATUS_CODE"));
	pLstStatusRows->addColumn(QVariant::String, QString("STATUS_TEXT"));
}



/* just assert to check wheteevr PK column exists in list */
int DbSimpleOrm::GetPrimaryColumnNo(const DbRecordSet &pLst)
{
	//find pk column inside list
	int nPrimaryKeyColumn= pLst.getColumnIdx(m_strPrimaryKey);
	Q_ASSERT_X(nPrimaryKeyColumn!=-1,"SIMPLE_ORM", "Primary key column not found");
	return nPrimaryKeyColumn;
}






//Read records based on id's from pLstOfID where strColIDName contains ID's, strColFK2ParentIDName is name of FK to parent ID column inside pLstRead table
// strWhereClauseBefore - inserted before WHERE
// strWhereClauseAfter - inserted after WHERE PK in (n1,n2..)
void DbSimpleOrm::ReadFromParentIDs(Status &pStatus, DbRecordSet &pLstRead, QString strColFK2ParentIDName, const DbRecordSet &pLstOfID,QString strColIDName,
								   QString strWhereClauseBefore,QString strWhereClauseAfter,int nQueryView, bool bDistinct)
{
	int nCount=0;
	QString strWhere,strWhereIn;
	DbRecordSet lstChunkRead;

	if (nQueryView==-1)nQueryView=m_nTableFullView;

	//issue 1342: when incoming list is defined, then error occur in PE mode
	pLstRead.destroy();
	pLstRead.defineFromView(DbSqlTableView::getView(nQueryView)); //define list
	
	if (pLstOfID.getRowCount()==0)
	{
		return;
	}

	while (nCount>=0)
	{
		//compile FK in (n1,n2,...) statement
		nCount=DbSqlTableDefinition::GenerateChunkedWhereStatement(pLstOfID,strColIDName,strWhereIn,nCount);
		if (nCount<0) break;
		strWhere=strWhereClauseBefore+" WHERE " +strColFK2ParentIDName+" IN "+strWhereIn+strWhereClauseAfter;
		Read(pStatus,lstChunkRead,strWhere,nQueryView);	//read
		if(!pStatus.IsOK())return;	//error

		//issue 1342, read list always defined
		//if(pLstRead.getColumnCount()==0) //if not defined, define
		//	pLstRead.copyDefinition(lstChunkRead);
		
		pLstRead.merge(lstChunkRead);    //merge
	}


}

//deletes from table based on set of Id's (pLstOfID in column strColIDName) which are inside Db field strColFK2ParentIDName
void DbSimpleOrm::DeleteFromParentIDs(Status &pStatus, QString strColFK2ParentIDName, const DbRecordSet &pLstOfID,QString strColIDName,
						 QString strWhereClauseBefore,QString strWhereClauseAfter)
{

	m_Query->BeginTransaction(pStatus);
	if(!pStatus.IsOK()) return;

	int nCount=0;
	QString strSQL,strWhereIn;
	DbRecordSet lstChunkRead;

	while (nCount>=0)
	{
		//compile FK in (n1,n2,...) statement
		nCount=DbSqlTableDefinition::GenerateChunkedWhereStatement(pLstOfID,strColIDName,strWhereIn,nCount);
		if (nCount<0) break;
		strSQL="DELETE FROM "+m_pTableKeyData.m_strTableName+strWhereClauseBefore+" WHERE " +strColFK2ParentIDName+" in "+strWhereIn+strWhereClauseAfter;
		m_Query->Execute(pStatus,strSQL);
		if(!pStatus.IsOK())
		{
			m_Query->Rollback();
			return;
		}
	}

	m_Query->Commit(pStatus);
}

//when using LIKE 'string%' in where statement this must be used with prepare statement and bind fro umlaut and other staff
QString DbSimpleOrm::BindLike(QString &strWhere)
{
	int nPos1=strWhere.indexOf("LIKE");
	if (nPos1<0)return "";
	int nPos2=strWhere.indexOf("'",nPos1+1);
	if (nPos2<0)return "";
	int nPos3=strWhere.indexOf("'",nPos2+1);
	if (nPos3<0)return "";

	QString strVal=strWhere.mid(nPos2,nPos3-nPos2+1);
	if (!strVal.isEmpty())
		strWhere.replace(strVal,"?");
	else
		return "";

	strVal=strVal.replace("'"," ").trimmed();
	return strVal;
}

//insert all (if pLstStatusRows is NULL, returns on first error, else expect that list is already created)
void DbSimpleOrm::DoInsertRows(Status &pStatus, DbRecordSet &pLstForWrite, int nQueryView, int nPrimaryKeyColumn,int nLastModifiedColumn, DbView &viewData,int nInsertSkipFirstCol,int nSkipLastColumns,DbRecordSet *pLstStatusRows)
{
	//select all for insert
	int nSelected=pLstForWrite.find(nPrimaryKeyColumn,(int)0); 
	if (nSelected==0)
		return;

	//test to see if we can go in ultrafast mode: only for integer variables
	bool bUltraFastMode=false; 
	int nEndColumn=pLstForWrite.getColumnCount()-nSkipLastColumns;
	Q_ASSERT(nInsertSkipFirstCol<=nEndColumn);
	if (m_pManager->GetDbType()==DBTYPE_FIREBIRD)
	{
		bUltraFastMode=true; //try ultrafast if FB, if one col found that is not int, then revert to safe/slower
		for(int i=nInsertSkipFirstCol;i<nEndColumn;i++)
		{
			if (pLstForWrite.getColumnType(i)!=QVariant::Int)
			{
				bUltraFastMode=false;
				break;
			}
		}
	}

	//reserve id pool:<only for  oracle, firebird, rest:=autoincrement_id -> fetch by lastid())
	int nStartID=m_pManager->getNextInsertId(pStatus,m_pDb,m_strPrimaryKey,nSelected);
	//qDebug()<<"insert_reserve end:"<<nStartID<<" selected: "<<nSelected;
	if(!pStatus.IsOK())return;
	bool bUsePoolOfIDs=false;
	if (nStartID>0)
	{
		nStartID=nStartID-nSelected+1;
		bUsePoolOfIDs=true;
		//qDebug()<<"insert_reserve start ID:"<<nStartID;
	}
	if (!bUsePoolOfIDs || pLstStatusRows) //only if pool of ids is in use or no statutes return, ultrafast mode is enabled
		bUltraFastMode=false;


	if (!bUltraFastMode)
	{
		//qDebug()<<"slow insert";

		//prepare insert:
		QString strSQL;
		QSet<int> lstIsLongVarchar;
		QSet<int> *plstIsLongVarchar=NULL;
		if (m_pManager->GetDbType()==DBTYPE_FIREBIRD)
		{
			strSQL=DbSqlTableView::getSQLInsert(nQueryView,&lstIsLongVarchar);
			m_Query->Prepare(pStatus, strSQL);
			if (lstIsLongVarchar.count()>0) 
				plstIsLongVarchar=&lstIsLongVarchar;
			else
				plstIsLongVarchar=NULL;
		}
		else
			m_Query->Prepare(pStatus, DbSqlTableView::getSQLInsert(nQueryView));
		
		if(!pStatus.IsOK())return;

		int nSize=pLstForWrite.getRowCount();
		for(int i=0;i<nSize;i++)
		{
			if (!pLstForWrite.isRowSelected(i)) continue;

			pStatus.setError(0); //reset error for next loop
			int nBindedVars=m_Query->BindRowValues(pLstForWrite, i, nInsertSkipFirstCol,nSkipLastColumns,0,plstIsLongVarchar);
			//qDebug()<<"insert slow pk: "<<nStartID;
			if (bUsePoolOfIDs)
				m_Query->ExecutePrepared(pStatus,nStartID);
			else
				m_Query->ExecutePrepared(pStatus);

			if(!pStatus.IsOK() && !pLstStatusRows) //if inside tran, exit at once
				return;

			//If insert get id column and put it back in list & status list.
			if (!bUsePoolOfIDs)
			{
				int nLastInsertID=m_Query->GetLastInsertedID(pStatus,m_strPrimaryKey);
				if(pStatus.IsOK())
					pLstForWrite.setData(i, nPrimaryKeyColumn, nLastInsertID);  //if error it will be stored at 0
			}
			else
			{
				pLstForWrite.setData(i, nPrimaryKeyColumn, nStartID);  //if error it will be stored at 0
				nStartID++;
			}

			//Fill Statuses (ID,StatusCode,StatusText)
			if(pLstStatusRows!=NULL)
			{
				pLstStatusRows->setData(i, 0, pLstForWrite.getDataRef(i, nPrimaryKeyColumn));
				pLstStatusRows->setData(i, 1, pStatus.getErrorCode());
				pLstStatusRows->setData(i, 2, pStatus.getErrorTextRaw());
			}
			else if(!pStatus.IsOK())
				return;
		}
	}
	else //compile SQL statements (only for FIREBIRD)
	{
		//qDebug()<<"ultra fast insert";
		QString strSQLInsert;
		strSQLInsert=DbSqlTableView::getSQLInsert(nQueryView);
		int ndummy1;
		m_pManager->SetSQLDefaultValues(pStatus,strSQLInsert,true,m_strPrimaryKey,ndummy1,m_pDb);
		if(!pStatus.IsOK())return;
		strSQLInsert=strSQLInsert.left(strSQLInsert.indexOf("VALUES (")); //get all before values

		int nSize=pLstForWrite.getRowCount();
		QString strSQL;
		int nCnt=0;

		QString strLastModifiedVal=m_pManager->GetCurrentDateTime();

		for(int i=0;i<nSize;i++)
		{
			if (!pLstForWrite.isRowSelected(i)) continue;

			//bind values:
			QString strValues=" VALUES (";
			for(int k=nInsertSkipFirstCol;k<nEndColumn;k++)
			{
				Q_ASSERT(pLstForWrite.getColumnType(k)==QVariant::Int);
				if(!pLstForWrite.getDataRef(i,k).isNull())
					strValues+=pLstForWrite.getDataRef(i,k).toString()+",";
				else
					strValues+="NULL, ";
			}
			pLstForWrite.setData(i, nPrimaryKeyColumn, nStartID);  //set new ID
			strValues+=QVariant(nStartID).toString();
			strValues+=","+strLastModifiedVal+")";
			strSQL+=strSQLInsert+strValues+";";

			//qDebug()<<"insert pk value"<<nStartID<<" for row: "<<i;
			nStartID++;
			nCnt++;
			if (nCnt>254 || strSQL.size()>7900 || i==nSize-1) //if 255 statements or 8Kb buffer or end->execute chunk
			{
				if (!m_Query->GetQSqlQuery()->exec("EXECUTE BLOCK AS BEGIN "+strSQL+" END"))
				{
					pStatus=m_pManager->MapSqlError(m_Query->GetQSqlQuery());
					return;		
				}
				strSQL="";
				nCnt=0;
			}
		}
	}
}


//insert all (if pLstStatusRows is NULL, returns on first error, else expect that list is already created)
void DbSimpleOrm::DoUpdateRows(Status &pStatus, DbRecordSet &pLstForWrite, int nQueryView, int nPrimaryKeyColumn,int nLastModifiedColumn, DbView &viewData,int nUpdateSkipFirstCol,int nSkipLastColumns,DbRecordSet *pLstStatusRows)
{
	//select all for update
	int nSelected=pLstForWrite.find(nPrimaryKeyColumn,(int)0,false,false,true,false); 
	if (nSelected==0)
		return;

	//test to see if we can go in ultrafast mode: only for integer variables
	bool bUltraFastMode=false; 
	int nEndColumn=pLstForWrite.getColumnCount()-nSkipLastColumns;
	Q_ASSERT(nUpdateSkipFirstCol<=nEndColumn);
	if (m_pManager->GetDbType()==DBTYPE_FIREBIRD)
	{
		bUltraFastMode=true; //try ultrafast if FB, if one col found that is not int, then revert to safe/slower
		for(int i=nUpdateSkipFirstCol;i<nEndColumn;i++)
		{
			if (pLstForWrite.getColumnType(i)!=QVariant::Int)
			{
				bUltraFastMode=false;
				break;
			}
		}
	}

	if (pLstStatusRows) //only if no statutes return, ultrafast mode is enabled
		bUltraFastMode=false;


	if (!bUltraFastMode)
	{
		QString strSQL;
		QSet<int> lstIsLongVarchar;
		QSet<int> *plstIsLongVarchar=NULL;
		if (m_pManager->GetDbType()==DBTYPE_FIREBIRD)
		{
			strSQL=DbSqlTableView::getSQLUpdate(nQueryView,&lstIsLongVarchar)+" WHERE " + m_pTableKeyData.m_strPrimaryKey + " = ?";
			if (lstIsLongVarchar.count()>0) //if not filled then skip it, use NULL
				plstIsLongVarchar=&lstIsLongVarchar;
			else
				plstIsLongVarchar=NULL;
		}
		else
			strSQL=DbSqlTableView::getSQLUpdate(nQueryView)+" WHERE " + m_pTableKeyData.m_strPrimaryKey + " = ?";
		
		m_Query->Prepare(pStatus, strSQL);
		if(!pStatus.IsOK())return;

		int nSize=pLstForWrite.getRowCount();
		for(int i=0;i<nSize;i++)
		{
			if (!pLstForWrite.isRowSelected(i)) continue;
			pStatus.setError(0); //reset error for next loop

			int nBindedVars=m_Query->BindRowValues(pLstForWrite, i, nUpdateSkipFirstCol,nSkipLastColumns,0,plstIsLongVarchar);
			m_Query->bindValue(nBindedVars,pLstForWrite.getDataRef(i, nPrimaryKeyColumn));
			m_Query->ExecutePrepared(pStatus);

			if(!pStatus.IsOK() && !pLstStatusRows) //if inside tran, exit at once
				return;

			//Fill Statuses (ID,StatusCode,StatusText)
			if(pLstStatusRows!=NULL)
			{
				pLstStatusRows->setData(i, 0, pLstForWrite.getDataRef(i, nPrimaryKeyColumn));
				pLstStatusRows->setData(i, 1, pStatus.getErrorCode());
				pLstStatusRows->setData(i, 2, pStatus.getErrorTextRaw());
			}

		}
	}
	else //compile SQL statements (only for FIREBIRD)
	{
		QString strLastModifiedVal=m_pManager->GetCurrentDateTime();

		QString strSQLUpdate="UPDATE "+m_pTableKeyData.m_strTableName+" SET ";
		int nSize=pLstForWrite.getRowCount();
		QString strSQL;
		int nCnt=0;
		for(int i=0;i<nSize;i++)
		{
			if (!pLstForWrite.isRowSelected(i)) continue;

			//bind values:
			QString strValues;
			for(int k=nUpdateSkipFirstCol;k<nEndColumn;k++)
			{
				Q_ASSERT(pLstForWrite.getColumnType(k)==QVariant::Int);
				strValues+=pLstForWrite.getColumnName(k)+"=";
				if(!pLstForWrite.getDataRef(i,k).isNull())
					strValues+=pLstForWrite.getDataRef(i,k).toString()+",";
				else
					strValues+="NULL, ";
			}
			strValues+=m_strLastModified+"="+strLastModifiedVal;
			strValues+=" WHERE " + m_pTableKeyData.m_strPrimaryKey + "="+pLstForWrite.getDataRef(i,nPrimaryKeyColumn).toString();
			strSQL+=strSQLUpdate+strValues+";";

			nCnt++;
			if (nCnt>254 || strSQL.size()>7900 || i==nSize-1) //if 255 statements or 8Kb buffer or end->execute chunk
			{
				if (!m_Query->GetQSqlQuery()->exec("EXECUTE BLOCK AS BEGIN "+strSQL+" END"))
				{
					pStatus=m_pManager->MapSqlError(m_Query->GetQSqlQuery());
					return;		
				}
				strSQL="";
				nCnt=0;
			}
		}
	}
}




/*!
	- Writes data (insert or update) in database and deletes records fromd database that are not contained inside lstData.
	- lstData returns with new ID's if insert.
	- Not in transaction, if bDeleteOrphanData=true, use transaction from 'outside'

	\param Ret_pStatus				- returns error
	\param nTableID					- table id in which data from lstData is to be written
	\param lstData					- data to be written
	\param strParentFKColName		- column inside lstData, if data has parent use this column ss filter
	\param nParentFKValue			- value of strParentFKColName to filter by
	\param nSkipLastColWrite		- default 0, number of cols to skip in write from end of subdata list
	\param bDeleteOrphanData		- default false, if true then all records that are not contained inside lstData will be deleted from database
	\param strSQLWhereFilter		- extra where condition (without WHERE or AND)..
*/
void DbSimpleOrm::WriteSubData(Status &pStatus, int nTableID,DbRecordSet &lstData,QString strParentFKColName,int nParentFKValue,bool bDeleteOrphanData,int nSkipLastColWrite,QString strSQLWhereFilter)
{
	pStatus.setError(0);

	Q_ASSERT(m_pManager->GetCurrentTransactionCount(GetDbConnection())>0); //must be inside transaction

	if (bDeleteOrphanData)
	{
		//table data
		QString strPrimaryKey=DbSqlTableDefinition::GetTablePrimaryKey(nTableID);
		QString strTableName=DbSqlTableDefinition::GetTableName(nTableID);

		QString strSQL = "SELECT "+strPrimaryKey+" FROM "+strTableName;
		if (!strParentFKColName.isEmpty()) 
		{
			strSQL+=" WHERE "+strParentFKColName+"="+QVariant(nParentFKValue).toString();
			if (!strSQLWhereFilter.isEmpty())
				strSQL+=" AND "+strSQLWhereFilter;
		}
		else
		{
			if (!strSQLWhereFilter.isEmpty())
				strSQL+=" WHERE "+strSQLWhereFilter;
		}

		m_Query->Execute(pStatus,strSQL);
		if(!pStatus.IsOK()) return;

		//compare: if old ones does not exists in incoming list, delete 'em
		DbRecordSet lstDbData;
		QString strSQLForDelete="";
		m_Query->FetchData(lstDbData);
		int nSize=lstDbData.getRowCount();
		for(int i=0;i<nSize;i++)
		{
			if (lstData.find(strPrimaryKey,lstDbData.getDataRef(i,0).toInt(),true)<0) //not found: to be deleted!
			{
				strSQLForDelete += lstDbData.getDataRef(i,0).toString()+",";
			}
		}
		//if list does not contains some id's from Db, then delete them from Db.
		if (!strSQLForDelete.isEmpty())
		{
			strSQLForDelete.chop(1); //last ,
			strSQLForDelete=" DELETE FROM " + strTableName+" WHERE " +strPrimaryKey +" IN ("+strSQLForDelete+")";
			m_Query->Execute(pStatus,strSQLForDelete);
			if(!pStatus.IsOK()) return;	
		}
	}

	//assign new parent id:
	if (!strParentFKColName.isEmpty()) 
		lstData.setColValue(lstData.getColumnIdx(strParentFKColName),nParentFKValue);

	//write new if we have:
	if (lstData.getRowCount()>0)
	{
		DbSimpleOrm TableOrm(pStatus,nTableID,m_pManager); //make local copy...
		if(!pStatus.IsOK()) return;	
		TableOrm.Write(pStatus,lstData,-1,nSkipLastColWrite);
		if(!pStatus.IsOK()) return;	
	}

}