#include "dbautoidmanager_oracle.h"
#include <QVariant>

/*!
	Get last inserted ID on pDbConnection
	\param status		 - return error 
	\param pDbConnection - connection
	\param nTableID		 - table id on which AutoID field is searched

	\return next id or 0 if error
*/
int DbAutoIdManagerOracle::getLastInsertId(Status &status, QSqlDatabase *pDbConnection, QString strPrimaryKey)
{
	int nLastID = 0;	//error
	//DbTableKeyData tableData;
	status.setError(0);


	//get AUTOINC field: its PK:
	//DbSqlTableDefinition::GetKeyData(nTableID,tableData);
	QString strSequenceName = "SEQ_" + strPrimaryKey;

	QString strSql = "SELECT " + strSequenceName + ".CURRVAL FROM DUAL";

	QSqlQuery query(*pDbConnection);
	if(!query.exec(strSql))
	{
		//error executing query
		status.setError(StatusCodeSet::ERR_SQL_SEQUENCE_FETCH_FAILED);
		return 0;
	}

	//fetch ID value
	query.next();
	nLastID = query.value(0).toInt();
	return nLastID;

}


