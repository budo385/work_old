#include "dbtransactionhandler.h"
#include <QDebug>
#include <QCoreApplication>

#include "common/common/logger.h"
#include "common/common/threadid.h"
/*!
	Adds new Db connection to local list of connections.
	When Added, connection is intialized with ISOLATION LEVEL, if sql fails error is returned.
	
	\param pStatus			- if something goes wrong
	\param pDbConnection	- connection to add (tracked by unique name)

*/
void DbTransactionHandler::SetConnectionIsolationLevel(Status &pStatus, QSqlDatabase *pDbConnection)
{
	//DOES NOT WORKS FOR FIREBIRD: Default is snapshot
	//try to set isolation level, raise error if not set:
	if(!m_StrIsolationLevel.isEmpty())
	{
		//Q_ASSERT_X(!m_StrIsolationLevel.isEmpty(),"Isolation level not set","");
		QSqlQuery query(*pDbConnection);

		if(!query.exec(m_StrIsolationLevel))
		{
			//error executing query
			//qDebug() << query.lastError().text();
			pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL,QObject::tr("Failed to set isolation level for database connection!"));
			return;
		}
	}
}

/*!
	Clears connection list
*/
void DbTransactionHandler::RemoveAllDbConnections()
{
	QMutexLocker locker(&mutex);//locks access
	m_LstTransCounters.clear();
}

/*!
	Checks if connection has unclosed transactions->fires assert
	\param	- pDbConnection connection 

*/
bool DbTransactionHandler::CheckDbConnection(QSqlDatabase *pDbConnection)
{
	QMutexLocker locker(&mutex);//locks access
	if (m_LstTransCounters.value(pDbConnection,0)!=0)
	{
		//Q_ASSERT_X(m_LstTransCounters[pDbConnection]==0,"Transaction orphans detected", "Transaction count does not match, probably Begin Tran without Commit/Rollback");
		//if release mode then dump log, do not let connection to be unreserved
		//dump emergency file log:
		Logger log;
		log.EnableFileLogging(QCoreApplication::applicationDirPath()+"/settings/debug_emergency.log");
		log.EnableConsoleLogging();
		log.SetLogLevel(4);
		int nThread=ThreadIdentificator::GetCurrentThreadID();
		log.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SQL_ORPHAN_TRANSACTIONS_DETECTED," Transaction count does not match, probably Begin Tran without Commit/Rollback. Client Thread="+QVariant(nThread).toString());

		return false;
	}
	else
		return true;

}




/*!
	Increases transaction count
	\param	- pDbConnection connection to add (tracked by unique name)
	\return - number of transactions

*/
int DbTransactionHandler::ReserveTransaction(QSqlDatabase *pDbConnection)
{
	QMutexLocker locker(&mutex);//locks access
	m_LstTransCounters[pDbConnection]=m_LstTransCounters.value(pDbConnection,0)+1;
	return m_LstTransCounters[pDbConnection];

}

/*!
	Gets tran count
	\return - number of transactions

*/
int DbTransactionHandler::GetTransactionCount(QSqlDatabase *pDbConnection)
{
	QMutexLocker locker(&mutex);//locks access
	return m_LstTransCounters.value(pDbConnection,0);
}



/*!
	Decreases tran count
	\return - number of transactions

*/
void DbTransactionHandler::ReleaseTransaction(QSqlDatabase *pDbConnection)
{
	m_LstTransCounters[pDbConnection]=m_LstTransCounters.value(pDbConnection,0)-1;
	Q_ASSERT(m_LstTransCounters[pDbConnection]>=0);
}



/*!
	Reservers transaction: if 1 then use begintransaction, else use savepoints
	Note: access is not locked coz, only one thread can have same DbConnection at time
	
	\param pStatus			- if something goes wrong
	\param pDbConnection	- pDbConnection connection to add (tracked by unique name)

*/
void DbTransactionHandler::BeginTransaction(Status &pStatus, QSqlDatabase* pDbConnection) 
{

	pStatus.setError(StatusCodeSet::ERR_NONE);

	int nTranCount=ReserveTransaction(pDbConnection);
	
	//single transaction
	if(nTranCount==1)
	{
		if (!pDbConnection->transaction()) 
		{
			qDebug()<<pDbConnection->lastError().text();
			pStatus.setError(StatusCodeSet::ERR_SQL_TRANSACTION_FAIL);
			ReleaseTransaction(pDbConnection);
		}
		return;
	}
	
	//nested:
	//qDebug()<<"Nested transaction in progress, depth:"<<nTranCount;

	QString strSQL="SAVEPOINT point"+QVariant(nTranCount).toString();
	QSqlQuery query(*pDbConnection);
	if(!query.exec(strSQL))
	{	
		qDebug() << query.lastError().text();
		pStatus.setError(StatusCodeSet::ERR_SQL_TRANSACTION_FAIL);
		ReleaseTransaction(pDbConnection);
	}

}


/*!
	Rollbacks transaction, gets current count if !=1 then use ROLLBACK SAVEPOINT
	Note: access is not locked coz, only one thread can have same DbConnection at time
	
	\param pStatus			- if something goes wrong
	\param pDbConnection	- pDbConnection connection to add (tracked by unique name)

*/
void DbTransactionHandler::RollbackTransaction(Status &pStatus, QSqlDatabase* pDbConnection) 
{

	pStatus.setError(StatusCodeSet::ERR_NONE);

	int nTranCount=GetTransactionCount(pDbConnection);
	
	//single transaction
	if(nTranCount==1)
	{
		if (!pDbConnection->rollback()) pStatus.setError(StatusCodeSet::ERR_SQL_TRANSACTION_FAIL);
		ReleaseTransaction(pDbConnection);
		return;
	}
	
	//nested:
	//qDebug()<<"Nested transaction rollback in progress, depth:"<<nTranCount;

	QString strSQL="ROLLBACK TO SAVEPOINT point"+QVariant(nTranCount).toString();
	QSqlQuery query(*pDbConnection);
	if(!query.exec(strSQL))
	{	qDebug() << query.lastError().text();
		pStatus.setError(StatusCodeSet::ERR_SQL_TRANSACTION_FAIL);
	}


	//decrease counter:
	ReleaseTransaction(pDbConnection);

}



/*!
	Commits transaction, gets current count if !=1 then use RELEASE SAVEPOINT
	Note: access is not locked coz, only one thread can have same DbConnection at time
	
	\param pStatus			- if something goes wrong
	\param pDbConnection	- pDbConnection connection to add (tracked by unique name)

*/
void DbTransactionHandler::CommitTransaction(Status &pStatus, QSqlDatabase* pDbConnection) 
{

	pStatus.setError(StatusCodeSet::ERR_NONE);

	int nTranCount=GetTransactionCount(pDbConnection);
	
	//single transaction
	if(nTranCount==1)
	{
		if (!pDbConnection->commit()) pStatus.setError(StatusCodeSet::ERR_SQL_TRANSACTION_FAIL);
		ReleaseTransaction(pDbConnection);
		return;
	}
	
	//nested:
	//qDebug()<<"Nested transaction commit in progress, depth:"<<nTranCount;

	QString strSQL="RELEASE SAVEPOINT point"+QVariant(nTranCount).toString();
	QSqlQuery query(*pDbConnection);
	if(!query.exec(strSQL))
	{	qDebug() << query.lastError().text();
		pStatus.setError(StatusCodeSet::ERR_SQL_TRANSACTION_FAIL);
	}

	//decrease counter:
	ReleaseTransaction(pDbConnection);

}



