#ifndef DBAUTOIDMANAGER_FIREBIRD_H
#define DBAUTOIDMANAGER_FIREBIRD_H

#include <QSqlDatabase>
#include <QSqlQuery>
#include "common/common/status.h"
#include "db/db/dbautoidmanager.h"


/*!
	\class DbAutoIdManagerFireBird
	\ingroup SQLModule
	\brief Implements a class to return na ID for last/next id to be inserted

	Use as static

*/

class DbAutoIdManagerFireBird : public DbAutoIdManager
{

public:
	int getNextInsertId(Status &status, QSqlDatabase *pDbConnection, QString strPrimaryKey,int nPoolSize=1);
	//Note: uses GENERATOR which is global for all user connections, so this is not safe fucntion to call after insert
	//altought is possible to get current generator value: 
	//SELECT GEN_ID('name',0) FROM rdb$database;
	int getLastInsertId(Status &status, QSqlDatabase *pDbConnection, QString strPrimaryKey){Q_ASSERT(false);return 0;}; //not supported!;


};

#endif // DBAUTOIDMANAGER_FIREBIRD_H
