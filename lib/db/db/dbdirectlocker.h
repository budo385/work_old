#ifndef DBDIRECTLOCKER_H
#define DBDIRECTLOCKER_H

#include <QList>
#include <QSqlDatabase>
#include <QSqlQuery>
#include "common/common/status.h"
#include "db_core/db_core/dbtableiddefinition.h"
#include <QSqlError>

typedef QList<int> DbLockList;

/*!
	\class DbDirectLocker
	\brief Unlike DbRecordLocker, this class uses short lived databasse locks
	\ingroup SQLModule

	Class for database specific row locking.
	If some Db requires special implementation, then override this class and reimplement.
	USE this class only inside one business object process.
	Records are automatically unlocked either by UPDATE or DELETE SQL on locked ID's.
*/
class DbDirectLocker
{
public:

	virtual int LockRecords(Status &status, QSqlDatabase *pDbConnection, QString strTableName,QString strPrimaryKey, const DbLockList& lstRecords);
	virtual void UnLockRecords(Status &status, QSqlDatabase *pDbConnection, QString strTableName,QString strPrimaryKey, const DbLockList& lstRecords);

};
#endif //DBDIRECTLOCKER_H

