#include "dbsqlquery.h"
#include <QSqlDriver>
#include <QSqlError>
#include <QSqlRecord>
#include <QSqlField>
#include "db_core/db_core/dbsqltableview.h"
#include "common/common/logger.h"
#include "common/common/threadid.h"
#include "common/common/sleeper.h"

/*!
	Gets connection if not exists.
	\param pDatabaseManager - external manager
	\param pStatus			- return error if connection create failed
	\param pDb				- connection, if be NULL, then own connection will be created

	\return void
*/
DbSqlQuery::DbSqlQuery(Status &pStatus,DbSqlManager* pDatabaseManager)
:m_pDbQuery(NULL),m_pDb(NULL),m_pDbManager(NULL),m_bKeepConnectionFlag(false),m_nPrefetchedPK(-1),m_nPrefetchedPKBindPosition(-1)
{
	pStatus.setError(0);
	Q_ASSERT(pDatabaseManager!=NULL);

	m_plstAllLongVarCharFields	= DbSqlTableView::getAllLongVarCharFields();
	m_pDbManager				= pDatabaseManager;
	m_pDb						= m_pDbManager->GetThreadDbConnection();

	if (m_pDb!=NULL)
	{
		m_bKeepConnectionFlag = true;
	}
	else
	{
		m_pDb = m_pDbManager->ReserveConnection(pStatus); //reserve connection
		if(!pStatus.IsOK()) return;
		m_bKeepConnectionFlag = false;
	}

	//create query object:
	m_pDbQuery = new QSqlQuery(*m_pDb);
	m_pDbQuery->setForwardOnly(true);	

}

DbSqlQuery::~DbSqlQuery()
{
	if(m_pDbQuery!=NULL)
		delete m_pDbQuery;

	//if we reserverd then release:
	if (!m_bKeepConnectionFlag)
	{
		m_pDbManager->ReleaseConnection(m_pDb);
	}

}



//--------------------------------------------------------------
//						TRANSACTIONS
//--------------------------------------------------------------

/*!
	Begins transaction. 
	\param pStatus			- return error 
	\return void
*/
void DbSqlQuery::BeginTransaction(Status &pStatus)
{
	m_pDbManager->BeginTransaction(pStatus,m_pDb);
}

/*!
	Rollback transaction ASSERT test only coz, hmm, its kinda 
*/
void DbSqlQuery::Rollback()
{
	Status pStatus;
	m_pDbManager->RollbackTransaction(pStatus,m_pDb);
	Q_ASSERT_X(pStatus.IsOK(),"QUERY","Transaction rollback failed");
}

/*!
	Commit transaction
	\param pStatus			- return error 
	\return void
*/
void DbSqlQuery::Commit(Status &pStatus)
{
	m_pDbManager->CommitTransaction(pStatus,m_pDb);
}






//--------------------------------------------------------------
//						EXECUTION
//--------------------------------------------------------------

/*!
	Execute Sql statement by default releases connection, returns true if statement not fail.
	\param pStatus error	- error
	\param SqlStatement		- SQL to execute
	\return bool
*/
bool DbSqlQuery::Execute(Status &pStatus, QString SqlStatement, bool bSkipSetDefaults)
{
	if (!bSkipSetDefaults)
	{
		//for firebird: because sequence generator is global, there is no safe method to fetch last inserted ID, so cache it localy, store PK column
		bool bOK=m_pDbManager->SetSQLDefaultValues(pStatus,SqlStatement,false,m_strPrefetchedPKName,m_nPrefetchedPK,m_pDb);
		if (!pStatus.IsOK())	return false;
		if (!bOK)
			m_nPrefetchedPK=-1;
	}


	if (!m_pDbQuery->exec(SqlStatement))
	{
		//store error details
		pStatus=m_pDbManager->MapSqlError(m_pDbQuery);
		if (pStatus.getErrorCode()==StatusCodeSet::ERR_SQL_DEADLOCK_TRYAGAIN) //if deadlock let's give it 2nd try
		{
			ThreadSleeper::Sleep(100);
			if (!m_pDbQuery->exec(SqlStatement))
			{
				pStatus=m_pDbManager->MapSqlError(m_pDbQuery);
				return false;		
			}
			else
			{
				pStatus.setError(StatusCodeSet::ERR_NONE);
				return true;	
			}
		}
		return false;		
	}
	else
	{
		pStatus.setError(StatusCodeSet::ERR_NONE);
		return true;		
	}
}

/*!
	Execute prepared Sql statement
	\param pStatus		- error
	\nPrefetchPrimaryKeyValue - only for FB: if <> -1 will not automatically insert PK_ID into binded ? 
	\return bool		- if Status contains error, return FALSE
*/
bool DbSqlQuery::ExecutePrepared(Status &pStatus, int nPrefetchPrimaryKeyValue, bool bSkipSetDefaults)
{
	if (!bSkipSetDefaults)
	{
		if (nPrefetchPrimaryKeyValue<=0)
		{
			//if firebird, then bind rest values (pk, time,etc..)
			if (m_nPrefetchedPKBindPosition>=0 && !m_strPrefetchedPKName.isEmpty())
			{
				Status err;
				m_nPrefetchedPK=m_pDbManager->getNextInsertId(err,m_pDb,m_strPrefetchedPKName);
				if (err.IsOK())
					m_pDbQuery->bindValue(m_nPrefetchedPKBindPosition, m_nPrefetchedPK);
				else
					m_nPrefetchedPK=-1;
			}
		}
		else
		{
			Q_ASSERT(m_nPrefetchedPKBindPosition>=0);
			m_pDbQuery->bindValue(m_nPrefetchedPKBindPosition, nPrefetchPrimaryKeyValue); //use this value instead of one server call: can save time in N inserts
		}
	}

	if (!m_pDbQuery->exec())
	{
		//store error details
		pStatus=m_pDbManager->MapSqlError(m_pDbQuery);
		if (pStatus.getErrorCode()==StatusCodeSet::ERR_SQL_DEADLOCK_TRYAGAIN) //if deadlock let's give it 2nd try
		{
			if (!m_pDbQuery->exec())
			{
				pStatus=m_pDbManager->MapSqlError(m_pDbQuery);
				return false;		
			}
			else
			{
				pStatus.setError(StatusCodeSet::ERR_NONE);
				return true;	
			}
		}
		return false;		
	}
	else
	{
		pStatus.setError(StatusCodeSet::ERR_NONE);
		return true;		
	}
}


/*!
	Prepare Sql statement
	\param pStatus		- error
	\param SqlStatement	- SQL to prepare

	\return				- if true then call GetNextID() prior to insert values!

*/
void DbSqlQuery::Prepare(Status &pStatus, QString SqlStatement, bool bSkipSetDefaults)
{
	if (!bSkipSetDefaults)
	{
		//for firebird: because sequence generator is global, there is no safe method to fetch last inserted ID, so cache it locally, store PK column
		bool bOK=m_pDbManager->SetSQLDefaultValues(pStatus,SqlStatement,true,m_strPrefetchedPKName,m_nPrefetchedPKBindPosition,m_pDb);
		if (!pStatus.IsOK())	return;
		if (!bOK)
		{
			m_nPrefetchedPK=-1;
			m_nPrefetchedPKBindPosition=-1;
		}
	}


	if (!m_pDbQuery->prepare(SqlStatement))
	{
		pStatus=m_pDbManager->MapSqlError(m_pDbQuery);
		//pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL,m_pDbQuery->lastError().text());
	}
	else
		pStatus.setError(StatusCodeSet::ERR_NONE);
}




//--------------------------------------------------------------
//						BINDS
//--------------------------------------------------------------



/*!
	Bind value by placeholder QSqlQuery funct.
	\param Placeholder  - placeholder to bind inside SQL statement (:PLACE1,:PLACE2,)
	\param Value		- value to bind
*/

void DbSqlQuery::bindValue(const QString &Placeholder, const QVariant &Value)
{
	Q_ASSERT(m_pDb != NULL);
	Q_ASSERT(m_pDbQuery != NULL);

	m_pDbQuery->bindValue(Placeholder, Value);
}



/*!
	Bind value by position QSqlQuery funct.
	\param Position - pos to bind inside SQL statement (?,?,?)
	\param Value	- value to bind
*/

void DbSqlQuery::bindValue(int Position, const QVariant &Value)
{
	Q_ASSERT(m_pDb != NULL);
	Q_ASSERT(m_pDbQuery != NULL);
	m_pDbQuery->bindValue(Position, Value);
}



/*!
	Bind values from Recordset on specified Row;
	Use it after prepare and before execute for inserting large ammount of rows of data into the DB

	\param RecordSet		- record set, column defintion is used to bind vars
	\param Row				- defines row from which vars inside recordset will be bind
	\param nSkipFirstNCols	- skips first cols from recordset for bind (def=0)
	\param nSkipLastNCols	- skips last cols from recordset for bind (def=0)
	\param nBindOffset		- starts bind from this position (def=0);
	
	if list iy empty then binding will be 1:1 (first column in rec will be binded first, etc...)

	\return number of binded vars (?) + bind offset
*/
int DbSqlQuery::BindRowValues(DbRecordSet &RecordSet, const int Row, int nSkipFirstNCols,int nSkipLastNCols,int nBindOffset,QSet<int> *pLstIsLongVarcharField)
{
	//RecordSet.Dump();
	int nEndCnt=nBindOffset+RecordSet.getColumnCount()-nSkipLastNCols;
	int i=nBindOffset;
	for (int nRowCnt = nSkipFirstNCols; nRowCnt < nEndCnt; ++nRowCnt)
	{
		//qDebug()<<i<<"--"<<nRowCnt<<"--"<<RecordSet.getDataRef(Row, nRowCnt).toString();
		if (pLstIsLongVarcharField)
		{
			if (pLstIsLongVarcharField->contains(nRowCnt)) //only for FB: test if longvarchar if it is then set it
			{
				Q_ASSERT(RecordSet.getColumnType(nRowCnt)==QVariant::String); //must be string column
				QByteArray byteStr=RecordSet.getDataRef(Row, nRowCnt).toString().toUtf8();
				m_pDbQuery->bindValue(i, byteStr);
				i++;
				continue;
			}
		}
		
		m_pDbQuery->bindValue(i, RecordSet.getDataRef(Row, nRowCnt));
		i++;

	}	

	
	return i;
}


//--------------------------------------------------------------
//						FETCH DATA
//--------------------------------------------------------------


/*!
	Fetch data into DbRecordSet. 
	- data values are automatically converted to match types in the recordset.
	- if record set is not defined (column count=0 then it will defined from DB fields)

	\param RecordSet		- filled with resultset from query


*/
void DbSqlQuery::FetchData(DbRecordSet &RecordSet)
{
	Q_ASSERT(m_pDbQuery != NULL);
	Q_ASSERT(m_pDb != NULL);
	
	//Define some locals.
	int record_count = m_pDbQuery->record().count();	//Record column count.
	//int query_size   = m_pDbQuery->size();				//Record size(rows).
	//QByteArray tmpByte; //BT added to support by ref


	//Do we need data value conversion:
	bool bConversionNeeded;
	QList<bool> lstConvertFlags=m_pDbManager->IsConversionNeeded(RecordSet,&m_pDbQuery->record(),bConversionNeeded);


	QSqlRecord rec = m_pDbQuery->record();

	//if firebird always convert all:
	if (m_pDbManager->GetDbType()==DBTYPE_FIREBIRD)
	{
		//Define view if empty
		if (RecordSet.getColumnCount()==0)
		{
			RecordSet.destroy(); //destroy recordset
			for (int i=0; i < record_count; i++)
			{
				if (rec.field(i).type()==QVariant::ByteArray) //FOR FB only: get all longvarchar fields:
				{
					if(m_plstAllLongVarCharFields->indexOf(rec.fieldName(i))>=0)
					{
						RecordSet.addColumn(QVariant::String, rec.fieldName(i));
						continue;
					}
				}
				
				RecordSet.addColumn(rec.field(i).type(), rec.fieldName(i));
			}
		}

		int i=0;
		while (m_pDbQuery->next())
		{
			RecordSet.addRow();
			for (int j=0; j < record_count; j++)
			{
				QVariant value=m_pDbQuery->value(j);
				RecordSet.setData(i, j, m_pDbManager->ConvertValue(value,RecordSet.getColumnType(j))); //for FIREBIRD : to distinct between string blob and binary blob
			}
			i++;
		}
		return;
	}


	//Define view if empty
	if (RecordSet.getColumnCount()==0)
	{
		RecordSet.destroy(); //destroy recordset
		for (int i=0; i < record_count; i++)
		{
			RecordSet.addColumn(rec.field(i).type(), rec.fieldName(i));
		}
	}

	
	//Speed is what I need: if no conversion then normal:
	if(!bConversionNeeded)
	{
		int i=0;
		while (m_pDbQuery->next())
		{
			RecordSet.addRow();
			for (int j=0; j < record_count; j++)
			{
				RecordSet.setData(i, j, m_pDbQuery->value(j));
			}
			i++;
		}
	}
	else //if conversion, skip all that do not need it, convert only one that need (MYSQL(DECIMAL)-QString->double)
	{
		int i=0;
		while (m_pDbQuery->next())
		{
			RecordSet.addRow();
			for (int j=0; j < record_count; j++)
			{
				if(lstConvertFlags.at(j))//if true then try to convert:
				{
					QVariant value=m_pDbQuery->value(j);
					RecordSet.setData(i, j, m_pDbManager->ConvertValue(value,RecordSet.getColumnType(j)));
				}
				else
					RecordSet.setData(i, j, m_pDbQuery->value(j));
			
			}
			i++;
		}
	}
}

//gets first record and returns false if there is no record
bool DbSqlQuery::FetchNextRow(DbRecordSet &RecordSet, bool bClearPreviousRows)
{
	Q_ASSERT(m_pDbQuery != NULL);
	Q_ASSERT(m_pDb != NULL);

	//Define some locals.
	int record_count = m_pDbQuery->record().count();	//Record column count.

	//Do we need data value conversion:
	bool bConversionNeeded;
	QList<bool> lstConvertFlags=m_pDbManager->IsConversionNeeded(RecordSet,&m_pDbQuery->record(),bConversionNeeded);

	if(bClearPreviousRows)
		RecordSet.clear();

	QSqlRecord rec = m_pDbQuery->record();

	//if firebird always convert all:
	if (m_pDbManager->GetDbType()==DBTYPE_FIREBIRD)
	{
		//Define view if empty
		if (RecordSet.getColumnCount()==0)
		{
			RecordSet.destroy(); //destroy recordset
			for (int i=0; i < record_count; i++)
			{
				if (rec.field(i).type()==QVariant::ByteArray) //FOR FB only: get all longvarchar fields:
				{
					if(m_plstAllLongVarCharFields->indexOf(rec.fieldName(i))>=0)
					{
						RecordSet.addColumn(QVariant::String, rec.fieldName(i));
						continue;
					}
				}

				RecordSet.addColumn(rec.field(i).type(), rec.fieldName(i));
			}
		}

		if (m_pDbQuery->next())
		{
			RecordSet.addRow();
			int i=RecordSet.getRowCount()-1;
			for (int j=0; j < record_count; j++)
			{
				QVariant value=m_pDbQuery->value(j);
				RecordSet.setData(i, j, m_pDbManager->ConvertValue(value,RecordSet.getColumnType(j))); //for FIREBIRD : to distinct between string blob and binary blob
			}
			return true;
		}
		else
			return false;
	}


	//Define view if empty
	if (RecordSet.getColumnCount()==0)
	{
		RecordSet.destroy(); //destroy recordset
		for (int i=0; i < record_count; i++)
		{
			RecordSet.addColumn(rec.field(i).type(), rec.fieldName(i));
		}
	}


	//Speed is what I need: if no conversion then normal:
	if(!bConversionNeeded)
	{
		if (m_pDbQuery->next())
		{
			RecordSet.addRow();
			int i=RecordSet.getRowCount()-1;
			for (int j=0; j < record_count; j++)
			{
				RecordSet.setData(i, j, m_pDbQuery->value(j));
			}
			return true;
		}
		else
			return false;
	}
	else //if conversion, skip all that do not need it, convert only one that need (MYSQL(DECIMAL)-QString->double)
	{
		if (m_pDbQuery->next())
		{
			RecordSet.addRow();
			int i=RecordSet.getRowCount()-1;
			for (int j=0; j < record_count; j++)
			{
				if(lstConvertFlags.at(j))//if true then try to convert:
				{
					QVariant value=m_pDbQuery->value(j);
					RecordSet.setData(i, j, m_pDbManager->ConvertValue(value,RecordSet.getColumnType(j)));
				}
				else
					RecordSet.setData(i, j, m_pDbQuery->value(j));

			}
			return true;
		}
		else
			return false;
		
	}

}




/*!
Returns the value of field index in the current record. 
An invalid QVariant is returned if field index does not exist, if the query is inactive, 
or if the query is positioned on an invalid record.
\param Index - index of the returned field.
\return QVariant
*/

QVariant DbSqlQuery::value(int Index)
{
	return m_pDbQuery->value(Index);
}


//--------------------------------------------------------------
//						LOCK
//--------------------------------------------------------------

//NEVER USE DIRECT !!!
/*!
	Lock records directly on DB \sa DbDirectLocker
*/
/*
int DbSqlQuery::LockRecordsDirect(Status &status, QString strTableName,QString strPrimaryKey, const DbLockList &lstRecords)
{
	status.setError(0);
	//return 1;
	//Locking direct only allowed in special cases: only used by application server and core services on core tables:
	Q_ASSERT(strTableName=="CORE_IPACCESSLIST" || strTableName=="CORE_APP_SRV_SESSION");
	int nX=m_pDbManager->LockRecordsDirect(status, m_pDb, strTableName,strPrimaryKey, lstRecords);
	if (!status.IsOK())
	{
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Locking failed on APP SRV SESSION table direct record, thread: " +QVariant(ThreadIdentificator::GetCurrentThreadID()).toString()+" first locking record: "+QVariant(lstRecords.at(0)).toString()+" out of "+QVariant(lstRecords.size()).toString()+" records!");
	}
	return nX;
}
*/
/*!
	UnLock records directly on DB \sa DbDirectLocker
*/
/*
void DbSqlQuery::UnLockRecordsDirect(Status &status, QString strTableName,QString strPrimaryKey, const DbLockList &lstRecords)
{
	//return;
	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"UNLocking APP SRV SESSION table direct record, thread: " +QVariant(ThreadIdentificator::GetCurrentThreadID()).toString()+" first locking record: "+QVariant(lstRecords.at(0)).toString()+" out of "+QVariant(lstRecords.size()).toString()+" records!");
	m_pDbManager->UnLockRecordsDirect(status, m_pDb, strTableName,strPrimaryKey, lstRecords);
	if (!status.IsOK())
	{
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"UNLocking failed on APP SRV SESSION table direct record, thread: " +QVariant(ThreadIdentificator::GetCurrentThreadID()).toString()+" first locking record: "+QVariant(lstRecords.at(0)).toString()+" out of "+QVariant(lstRecords.size()).toString()+" records!");
	}
}
*/

//--------------------------------------------------------------
//						HELPER
//--------------------------------------------------------------


/*!
	Get last inserted ID
	\param pStatus	- error
	\param nTableID	- table id
	\return int
*/
int DbSqlQuery::GetLastInsertedID(Status &pStatus, QString strPrimaryKey)
{
	//Q_ASSERT(m_pDb != NULL);

	if (m_nPrefetchedPK!=-1)
		return m_nPrefetchedPK;
	else
		return m_pDbManager->getLastInsertId(pStatus,m_pDb,m_pDbQuery,strPrimaryKey);
}

/*!
	Get next inserted ID
	\param pStatus	- error
	\param nTableID	- table id
	\return int
*/
int DbSqlQuery::GetNextInsertedID(Status &pStatus, QString strPrimaryKey)
{
	//Q_ASSERT(m_pDb != NULL);

	if (m_nPrefetchedPK!=-1)
		return m_nPrefetchedPK;
	else
		return m_pDbManager->getNextInsertId(pStatus,m_pDb,strPrimaryKey);
}



bool DbSqlQuery::next()
{
	return m_pDbQuery->next();
}
int  DbSqlQuery::size() const
{	
	return m_pDbQuery->size();
}

int  DbSqlQuery::rowsAffected() const
{
	return m_pDbQuery->numRowsAffected();
}


void DbSqlQuery::clear()
{
	m_pDbQuery->clear();
}



/*
	Write's record data: must have ID field first, and _DAT_LAST_MODIFIED column. If ID is empty then insert, else update.
	pLstIsLongVarcharField are indexes of special varchar fields...
*/
void DbSqlQuery::WriteData(Status &pStatus,QString strTableName, DbRecordSet &lstData, int nSkipDefaultColumnsCnt)
{

	QString strSQLUpdate;
	QString strSQLInsert=" (";
	QString strSQLInsertBinds=" VALUES (";
	QString strIDName=lstData.getColumnName(0);

	QSet<int> lstIsLongVarcharField;
	int nSize=lstData.getColumnCount();
	for (int i=nSkipDefaultColumnsCnt;i<nSize;i++) //skip first two
	{
		QString strColName=lstData.getColumnName(i);
		if(m_plstAllLongVarCharFields->indexOf(strColName)>=0)
		{
			lstIsLongVarcharField.insert(i);
		}
		strSQLInsert+=strColName+",";
		strSQLUpdate+=strColName+"=?,";
		strSQLInsertBinds+="?,";
	}

	strSQLInsert.chop(1);
	strSQLInsert+=+") ";
	strSQLUpdate.chop(1);
	strSQLInsertBinds.chop(1);
	strSQLInsertBinds+=+") ";

	strSQLInsert="INSERT INTO "+strTableName+strSQLInsert+strSQLInsertBinds;
	strSQLUpdate="UPDATE "+strTableName+" SET " +strSQLUpdate;


	nSize=lstData.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		int nID=lstData.getDataRef(i,0).toInt();
		if (nID==0) //insert
		{
			Prepare(pStatus,strSQLInsert);
			if (!pStatus.IsOK()) return;
			BindRowValues(lstData,i,nSkipDefaultColumnsCnt,0,0,&lstIsLongVarcharField);
			ExecutePrepared(pStatus);
			int nID=GetLastInsertedID(pStatus,strIDName);
			lstData.setData(i,strIDName,nID);
			if (!pStatus.IsOK()) return;
		}
		else //edit
		{
			Prepare(pStatus,strSQLUpdate+" WHERE "+strIDName+"="+QString::number(nID));
			if (!pStatus.IsOK()) return;
			BindRowValues(lstData,i,nSkipDefaultColumnsCnt,0,0,&lstIsLongVarcharField);
			ExecutePrepared(pStatus);
			if (!pStatus.IsOK()) return;
		}
	}

}

/*
	From lstOfIDs, column defined nIdxOfIDColumnInsideList, take ID's and delete them from table strTableName
	Note: name of ID column must match name in database 
	Note: all is done inside transaction, by chunks of the 100
	Note: does not have to be list of integeres, can be list of strings: detected by column type.
	Note: strWhereAfter can contain additional filter in format " AND XXX=1"
*/

void DbSqlQuery::DeleteData( Status &pStatus,QString strTableName,DbRecordSet &lstOfIDs, int nIdxOfIDColumnInsideList, QString strWhereAfter)
{
	QString strColIDName=lstOfIDs.getColumnName(nIdxOfIDColumnInsideList);
	QString strWhereClauseBefore =" DELETE FROM "+strTableName;
	bool bAddQuotes = (lstOfIDs.getColumnType(nIdxOfIDColumnInsideList)==QVariant::String);

	int nCount=0;
	QString strWhereIn;

	BeginTransaction(pStatus);
	if (!pStatus.IsOK()) return;

	while (nCount>=0)
	{
		//compile FK in (n1,n2,...) statement
		nCount=GenerateChunkedWhereStatement(lstOfIDs,strColIDName,strWhereIn,nCount,100,bAddQuotes);
		if (nCount<0) break;
		QString strSQL=strWhereClauseBefore+" WHERE " +strColIDName+" IN "+strWhereIn+strWhereAfter;

		Execute(pStatus,strSQL);
		if (!pStatus.IsOK())
		{
			Rollback();
			return;
		}
	}
	Commit(pStatus);

}



// Prepare chunked statement in format (n1,n2,n3....) where n is from pLstOfID
// NOTE: if nChunkSize == -1 then no chunk:it will to to end of list
// pLstOfID must have strColIDName column inside filled with n's
// return -1 if strSQL is empty, else it returns actually chunked rows
int DbSqlQuery::GenerateChunkedWhereStatement(const DbRecordSet &pLstOfID,QString strColIDName,QString &strWhere,int nStartFrom,int nChunkSize,bool bAddQuotes)
{
	Q_ASSERT(nStartFrom>=0);

	int nSize=pLstOfID.getRowCount();
	if(nStartFrom>=nSize) return -1;		//reach end

	int nChunk;
	if(nChunkSize==-1)
		nChunk=pLstOfID.getRowCount();
	else
		nChunk=nStartFrom+nChunkSize;		//chunks of 200 max from offset

	//limit to size:
	if(nChunk>nSize)
		nChunk=nSize;


	strWhere.clear();	//clear string

	//ID must be present
	int nIDColIdx=pLstOfID.getColumnIdx(strColIDName);
	Q_ASSERT(nIDColIdx!=-1);

	int i;

	//set first (as must be at least one if we are here):
	if (!bAddQuotes)
		strWhere = " ("+pLstOfID.getDataRef(nStartFrom, nIDColIdx).toString();
	else
		strWhere = " ('"+pLstOfID.getDataRef(nStartFrom, nIDColIdx).toString()+"'";

	//start from 2nd:
	for(i=nStartFrom+1;i<nChunk;i++)
	{
		if (!bAddQuotes)
			strWhere+= ","+pLstOfID.getDataRef(i, nIDColIdx).toString();
		else
			strWhere+= ",'"+pLstOfID.getDataRef(i, nIDColIdx).toString()+"'";
	}

	strWhere+=") ";		//close bracket


	return i;			//return no rows actually added
}
