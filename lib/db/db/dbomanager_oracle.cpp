#include "dbomanager_oracle.h"
#include <QSqlRecord>
#include <QVariant>



//----------------------------------------------------
//				INDEX
//----------------------------------------------------



/*!
	Check If Exists Index (case insensitive)

	\param pDbConnection	- db connection
	\param strTableName		- table name
	\param strIndexName		- index name
	\return  1 if exists
*/
bool DbObjectManager_Oracle::CheckIfExists_Index(QSqlDatabase *pDbConnection, QString strTableName, QString strIndexName)
{
	QString strSql = "select count(*) from all_indexes where table_name= '" +strTableName+"' AND OWNER = '"+mDbSettings.m_strDbUserName+"' AND index_name='"+strIndexName+"'";
	QSqlQuery query(*pDbConnection);
	if(!query.exec(strSql))
	{
		return false;
		//error executing query
		//pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, QObject::tr("Failed check index: ")+strIndexName);
		//pStatus.setErrorDetails(query.lastError().text()); //SQL error details 
	}


	query.next();
	int nCount = query.value(0).toInt();

	if( nCount==0)
		return false;
	else
		return true;
}




//----------------------------------------------------
//				TRIGGER
//----------------------------------------------------


/*!
	Check If Exists Trigger (case insensitive)

	\param pDbConnection	- db connection
	\param strTriggerName   - object name
	\return  1 if exists
*/
bool DbObjectManager_Oracle::CheckIfExists_Trigger(QSqlDatabase *pDbConnection, QString strTriggerName)
{
	QString strSql = "SELECT COUNT(*) FROM ALL_TRIGGERS WHERE TRIGGER_NAME='"+strTriggerName+"' AND OWNER = '"+mDbSettings.m_strDbUserName+"'";
	QSqlQuery query(*pDbConnection);
	if(!query.exec(strSql))
	{
		return false;
		//error executing query
		//pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, QObject::tr("Failed check trigger: ")+strTriggerName);
		//pStatus.setErrorDetails(query.lastError().text()); //SQL error details 
	}


	query.next();
	int nCount = query.value(0).toInt();

	if( nCount==0)
		return false;
	else
		return true;
}


/*!
	Drop Trigger if exists (case insensitive)

	\param pStatus		 - if statement fails
	\param pDbConnection - db connection
	\param strTriggerName  - object name
*/
void DbObjectManager_Oracle::DropIfExists_Trigger(Status &pStatus, QSqlDatabase *pDbConnection, QString strTriggerName)
{

	pStatus.setError(0);
	//check if exists:
	if(!CheckIfExists_Trigger(pDbConnection,strTriggerName)) return;

	QString strSql = "DROP TRIGGER " + strTriggerName;

	QSqlQuery query(*pDbConnection);
	if(!query.exec(strSql))
	{
		//error executing query
		pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, QObject::tr("Failed to delete trigger: ")+strTriggerName);
		//pStatus.setErrorDetails(query.lastError().text()); //SQL error details 
	}
}





//----------------------------------------------------
//				SEQUENCE
//----------------------------------------------------



/*!
	Check If Exists Sequence (case insensitive)

	\param pDbConnection	- db connection
	\param strSeqName   - object name
	\return  1 if exists
*/
bool DbObjectManager_Oracle::CheckIfExists_Sequence(QSqlDatabase *pDbConnection, QString strSeqName)
{

	QString strSql = "SELECT COUNT(*) FROM ALL_SEQUENCES WHERE SEQUENCE_NAME='"+strSeqName+"' AND SEQUENCE_OWNER = '"+mDbSettings.m_strDbUserName+"'";
	QSqlQuery query(*pDbConnection);
	if(!query.exec(strSql))
	{
		return false;
		//error executing query
		//pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, QObject::tr("Failed check sequence: ")+strTriggerName);
		//pStatus.setErrorDetails(query.lastError().text()); //SQL error details 
	}


	query.next();
	int nCount = query.value(0).toInt();

	if( nCount==0)
		return false;
	else
		return true;
}


/*!
	Drop Sequence if exists (case insensitive)

	\param pStatus		 - if statement fails
	\param pDbConnection - db connection
	\param strSeqName  - object name
*/
void DbObjectManager_Oracle::DropIfExists_Sequence(Status &pStatus, QSqlDatabase *pDbConnection, QString strSeqName)
{

	pStatus.setError(0);
	//check if exists:
	if(!CheckIfExists_Sequence(pDbConnection,strSeqName)) return;

	QString strSql = "DROP SEQUENCE " + strSeqName;

	QSqlQuery query(*pDbConnection);
	if(!query.exec(strSql))
	{
		//error executing query
		pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, QObject::tr("Failed to delete sequence: ")+strSeqName);
		//pStatus.setErrorDetails(query.lastError().text()); //SQL error details 
	}
}


/*!
	Create Sequence, drop if exists

	\param pStatus		 - if statement fails
	\param pDbConnection - db connection
	\param strSeqName	 - object name
	\param nStartCounter - start sequence from this number
*/
void DbObjectManager_Oracle::CreateSequence(Status &pStatus, QSqlDatabase *pDbConnection, QString strSeqName, int nStartCounter)
{

	//delete prev. seq
	DropIfExists_Sequence(pStatus,pDbConnection,strSeqName);
	if(!pStatus.IsOK()) return;

	QString strSql = "CREATE SEQUENCE "+strSeqName+" START WITH "+QVariant(nStartCounter).toString()+" INCREMENT BY  1 NOCYCLE";
	QSqlQuery query(*pDbConnection);

	if(!query.exec(strSql))
	{
		//error executing query
		pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, QObject::tr("Sequence creation failed: ")+strSeqName);
		//pStatus.setErrorDetails(query.lastError().text()); //SQL error details 
	}
}



//----------------------------------------------------
//				PROCEDURE
//----------------------------------------------------



/*!
	Check If Exists PROCEDURE (case insensitive)

	\param pDbConnection	- db connection
	\param strProcName   - object name
	\return  1 if exists
*/
bool DbObjectManager_Oracle::CheckIfExists_Proc(QSqlDatabase *pDbConnection, QString strProcName)
{

	QString strSql = "SELECT COUNT(*) FROM ALL_PROCEDURES WHERE PROCEDURE_NAME='"+strProcName+"' AND OWNER = '"+mDbSettings.m_strDbUserName+"'";
	QSqlQuery query(*pDbConnection);
	if(!query.exec(strSql))
	{
		return false;
		//error executing query
		//pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, QObject::tr("Failed check sequence: ")+strProcName);
		//pStatus.setErrorDetails(query.lastError().text()); //SQL error details 
	}


	query.next();
	int nCount = query.value(0).toInt();

	if( nCount==0)
		return false;
	else
		return true;
}


/*!
	Drop PROCEDURE if exists (case insensitive)

	\param pStatus		 - if statement fails
	\param pDbConnection - db connection
	\param strProcName  - object name
*/
void DbObjectManager_Oracle::DropIfExists_Proc(Status &pStatus, QSqlDatabase *pDbConnection, QString strProcName)
{

	pStatus.setError(0);
	//check if exists:
	if(!CheckIfExists_Proc(pDbConnection,strProcName)) return;

	QString strSql = "DROP PROCEDURE " + strProcName;

	QSqlQuery query(*pDbConnection);
	if(!query.exec(strSql))
	{
		//error executing query
		pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, QObject::tr("Failed to delete procedure: ")+strProcName);
		//pStatus.setErrorDetails(query.lastError().text()); //SQL error details 
	}
}





//----------------------------------------------------
//				FUNCTION
//----------------------------------------------------



/*!
	Check If Exists FUNCTION (case insensitive)

	\param pDbConnection	- db connection
	\param strFunctName   - object name
	\return  1 if exists
*/
bool DbObjectManager_Oracle::CheckIfExists_Funct(QSqlDatabase *pDbConnection, QString strFunctName)
{

	QString strSql = "SELECT COUNT(*) FROM ALL_PROCEDURES WHERE PROCEDURE_NAME='"+strFunctName+"' AND OWNER = '"+mDbSettings.m_strDbUserName+"'";
	QSqlQuery query(*pDbConnection);
	if(!query.exec(strSql))
	{
		//error executing query
		return false;
		//pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, QObject::tr("Failed check function: ")+strFunctName);
		//pStatus.setErrorDetails(query.lastError().text()); //SQL error details 
	}


	query.next();
	int nCount = query.value(0).toInt();

	if( nCount==0)
		return false;
	else
		return true;
}


/*!
	Drop FUNCTION if exists (case insensitive)

	\param pStatus		 - if statement fails
	\param pDbConnection - db connection
	\param strFunctName  - object name
*/
void DbObjectManager_Oracle::DropIfExists_Funct(Status &pStatus, QSqlDatabase *pDbConnection, QString strFunctName)
{

	pStatus.setError(0);
	//check if exists:
	if(!CheckIfExists_Proc(pDbConnection,strFunctName)) return;

	QString strSql = "DROP FUNCTION " + strFunctName;

	QSqlQuery query(*pDbConnection);
	if(!query.exec(strSql))
	{
		//error executing query
		pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, QObject::tr("Failed to delete procedure: ")+strFunctName);
		//pStatus.setErrorDetails(query.lastError().text()); //SQL error details 
	}
}




//----------------------------------------------------
//				AUTO INCREMENT ID
//----------------------------------------------------



/*!
	Create Sequence + insert trigger on strAutoIncFieldName /  strTableName 
	Sequence is named: "SEQ_" + strAutoIncFieldName
	Insert trigger is named:"TRG_" + strAutoIncFieldName
	Warning: destroys any previously defined insert trigger and sequence by that name!

	\param pStatus				 -error
	\param pDbConnection		 -db connection
	\param strTableName			 -table name
	\param strAutoIncFieldName   -field on which ID will be created
*/
void DbObjectManager_Oracle::CreateAutoIncrementID(Status &pStatus, QSqlDatabase *pDbConnection,QString strTableName, QString strAutoIncFieldName)
{

	QString strSeqName="SEQ_" + strAutoIncFieldName;
	QString strTriggerName="TRG_" + strAutoIncFieldName;
	pStatus.setError(0);
	
	//check if table exists:
	if(!CheckIfExists_Table(pDbConnection,strTableName))
		{ pStatus.setError(StatusCodeSet::ERR_SQL_UNKNOWN_TABLE,QObject::tr("Table does not exists: ")+strTableName);return;}

	//delete insert trigger
	DropIfExists_Trigger(pStatus,pDbConnection,strTriggerName);
	if(!pStatus.IsOK()) return;

	//delete & create prev. seq
	CreateSequence(pStatus,pDbConnection,strSeqName);
	if(!pStatus.IsOK()) return;

	//create insert trigger:
	QString strSql = "create or replace trigger "+strTriggerName+" before "; 
	strSql+=" insert on "+strTableName+"  for each row ";
	strSql+=" begin ";
	strSql+=" select "+strSeqName+".nextval into :new."+strAutoIncFieldName," from dual; ";		
	strSql+=" end; ";


	QSqlQuery query(*pDbConnection);
	if(!query.exec(strSql))
	{
		//error executing query
		pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, QObject::tr("Trigger creation failed: ")+strTriggerName);
		//pStatus.setErrorDetails(query.lastError().text()); //SQL error details 
	}

}



void DbObjectManager_Oracle::AdjustAutoIncrementAfterDbCopy(Status &pStatus, QSqlDatabase *pDbConnection,QString strTableName, QString strAutoIncFieldName)
{

	QSqlQuery query(*pDbConnection);
	QString strSelMax="SELECT MAX("+strAutoIncFieldName+") FROM "+strTableName;
	if(!query.exec(strSelMax))
	{
		pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, QObject::tr("Failed to adjust Sequence start value: ")+query.lastError().text());
		return;

	}

	while (query.next())
	{
		int nMax=query.value(0).toInt();
		QString strSeqName="SEQ_" + strAutoIncFieldName;
		QString strSQL="ALTER SEQUENCE "+strSeqName+" RESTART WITH "+QVariant(nMax+1).toString();
		if(!query.exec(strSQL))
		{
			pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, QObject::tr("Failed to adjust Sequence start value: ")+query.lastError().text());
			return;

		}
	}


}


//drops all triggers in the database:
void DbObjectManager_Oracle::DropAllTriggers(Status &pStatus, QSqlDatabase *pDbConnection)
{
	QString strSql = "SELECT TRIGGER_NAME FROM INFORMATION_SCHEMA.TRIGGERS WHERE TRIGGER_SCHEMA = '"+mDbSettings.m_strDbName+"'";
	QSqlQuery query(*pDbConnection);
	if(!query.exec(strSql))
	{
		pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, QObject::tr("Failed to delete triggers"));
		return;

	}

	while (query.next())
	{
		QString strTriggerName=query.value(0).toString();
		QString strSql = "DROP TRIGGER " + strTriggerName;

		QSqlQuery query(*pDbConnection);
		if(!query.exec(strSql))
		{
			//error executing query
			pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, QObject::tr("Failed to delete triggers"));	
			return;
		}
	}

}


//drops all constraints in the database:
void DbObjectManager_Oracle::DropAllConstraints(Status &pStatus, QSqlDatabase *pDbConnection)
{
	QString strSql = "SELECT TABLE_NAME,CONSTRAINT_NAME FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE TABLE_SCHEMA = '"+mDbSettings.m_strDbName+"'";
	QSqlQuery query(*pDbConnection);
	if(!query.exec(strSql))
	{
		pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, QObject::tr("Failed to delete constraints"));
		return;
	}

	while (query.next())
	{
		QString strSql = "ALTER TABLE "+query.value(0).toString()+" DROP FOREIGN KEY "+query.value(1).toString();

		QSqlQuery query(*pDbConnection);
		if(!query.exec(strSql))
		{
			//error executing query
			pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, QObject::tr("Failed to delete constraints"));
		}
	}

}


void DbObjectManager_Oracle::DropAllIndexes(Status &pStatus, QString strTableName,QSqlDatabase *pDbConnection)
{
	QString strSql = "select * from all_indexes where table_name= '" +strTableName+"' AND OWNER = '"+mDbSettings.m_strDbUserName;
	QSqlQuery query(*pDbConnection);query.setForwardOnly(true);
	if(!query.exec(strSql))
	{
		pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, query.lastError().text());
		return;
	}

	//delete them all:
	int fieldNo = query.record().indexOf("index_name");
	while (query.next()) 
	{
		DropIfExists_Index(pStatus,pDbConnection,strTableName,query.value(fieldNo).toString());
		if (!pStatus.IsOK())
			return;
	}
}