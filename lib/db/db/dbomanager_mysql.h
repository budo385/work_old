#ifndef DBOBJMANAGER_MYSQL_H
#define DBOBJMANAGER_MYSQL_H


#include <QSqlRecord>
#include "dbomanager.h"


/*!
	\class DbObjectManager_MySQL
	\brief For managing Db objects (drop, create, etc..) for MYSQL
	\ingroup Db_Actualize


	- MYSQL DOES NOT SUPPORT NESTED TRANSACTIONS!!!!
	- WARNING FOR MYSQL, all those commands commits transactions (no undo possible:
	ALTER FUNCTION, ALTER PROCEDURE, ALTER TABLE, BEGIN, CREATE DATABASE, CREATE FUNCTION, CREATE INDEX, CREATE PROCEDURE, CREATE TABLE, DROP DATABASE, DROP FUNCTION, DROP INDEX, DROP PROCEDURE, DROP TABLE, LOAD MASTER DATA, LOCK TABLES, RENAME TABLE, SET AUTOCOMMIT=1, START TRANSACTION, TRUNCATE, UNLOCK TABLES.


*/
class DbObjectManager_MySQL: public DbObjectManager
{
public:

	DbObjectManager_MySQL(DbConnectionSettings &DbSettings):DbObjectManager(DbSettings){};


	void DropIfExists_Table(Status &pStatus, QSqlDatabase *pDbConnection, QString strTableName);

	bool CheckIfExists_Index(QSqlDatabase *pDbConnection, QString strTableName, QString strIndexName);
	void DropIfExists_Index(Status &pStatus, QSqlDatabase *pDbConnection, QString strTableName, QString strIndexName);

	bool CheckIfExists_Trigger(QSqlDatabase *pDbConnection, QString strTriggerName);
	void DropIfExists_Trigger(Status &pStatus, QSqlDatabase *pDbConnection, QString strTriggerName);

	void DropAllTriggers(Status &pStatus, QSqlDatabase *pDbConnection);
	void DropAllConstraints(Status &pStatus, QSqlDatabase *pDbConnection);
	void DropAllIndexes(Status &pStatus, QString strTableName,QSqlDatabase *pDbConnection);

	// no sequences for mySQL, fire ASSERT, are you sure u know what are u doing?
	bool CheckIfExists_Sequence(QSqlDatabase *pDbConnection, QString strSeqName);
	void DropIfExists_Sequence(Status &pStatus, QSqlDatabase *pDbConnection, QString strSeqName);
	void CreateSequence(Status &pStatus, QSqlDatabase *pDbConnection, QString strSeqName, int nStartCounter=1);

	bool CheckIfExists_Proc(QSqlDatabase *pDbConnection, QString strProcName);
	void DropIfExists_Proc(Status &pStatus, QSqlDatabase *pDbConnection, QString strProcName);

	bool CheckIfExists_Funct(QSqlDatabase *pDbConnection, QString strFunctName);
	void DropIfExists_Funct(Status &pStatus, QSqlDatabase *pDbConnection, QString strFunctName);

	void CreateAutoIncrementID(Status &pStatus, QSqlDatabase *pDbConnection,QString strTableName, QString strAutoIncFieldName);
	void AdjustAutoIncrementAfterDbCopy(Status &pStatus, QSqlDatabase *pDbConnection,QString strTableName, QString strAutoIncFieldName){}; //not impl...not needed

	void	AlterTable(Status &pStatus, QString strTableName, QString strTableCreationStatement,QStringList lstDeletedColumns, QList<QStringList> lstChangedNameColumns,QSqlDatabase *pDbConnection,bool bFillDefaultValuesForNewFields=false);
	void	DropColumns(Status &pStatus, QString strTableName, QStringList lstDeletedColumns, QSqlDatabase *pDbConnection);
	QString	BackupDatabase(Status &pStatus, QString strDatabaseName, QString strUser, QString strPassword,QString &strDumpFilePath,QString strDatabaseVersion,QString strHost, QString strPort,QSqlDatabase *pDbConnection);
	void	RestoreDatabase(Status &pStatus, QString strDatabaseName, QString strUser, QString strPassword,QString strDumpFilePath,QString strHost, QString strPort,QSqlDatabase *pDbConnection);


	// no storage spaces for mySQL:
	QString	GetTableSpace(){ return "";}
	QString	GetIndexSpace(){ return "";}

	void	PrepareDemoDatabase(Status &pStatus, QSqlDatabase *pDbConnection){};
	void	ClearDemoDatabase(Status &pStatus, QSqlDatabase *pDbConnection){};
	void	CreateReadOnlyUser_Level1(Status &pStatus, QString strUserName, QString strPassword, QString strUserFirstName, QString strUserLastName, QString strMasterKey, QString strMasterPW, QSqlDatabase *pDbConnection){};


protected:
	bool GetTableColumnsFromDb(QString strTableName,QHash<QString,QStringList> &lstColsData,QSqlDatabase *pDbConnection);
};



#endif //DBOBJMANAGER_MYSQL_H

