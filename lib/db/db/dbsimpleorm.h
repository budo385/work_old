#ifndef DBSIMPLEORM_H
#define DBSIMPLEORM_H


#include <QString>
#include <QList>
#include "common/common/status.h"
#include "common/common/dbrecordset.h"
#include "db/db/dbsqlquery.h"
#include "db/db/dbsqlmanager.h"
#include "db_core/db_core/dbsqltabledefinition.h"

/*!
	\class DbSimpleOrm
	\brief Locks,Unlocks, Writes, Delete list of records in batch job
	\ingroup SQLModule

	Warning: non-mulitthread safe, can not be instanced as global object!
	At any write operation, writes automatically mandatory fields (last modified)
*/
class DbSimpleOrm
{
public:

	DbSimpleOrm(Status &pStatus,int TableID,DbSqlManager *pManager);
	~DbSimpleOrm();

	void Write(Status &pStatus, DbRecordSet &pLstForWrite, int nQueryView=-1, int nSkipLastColumns=0,DbRecordSet *pLstStatusRows=NULL);
	void Read (Status &pStatus, DbRecordSet &pLstRead, QString strWhereClause = "",int nQueryView=-1, bool bDistinct = false);
	bool DeleteFast(Status &pStatus, DbRecordSet &pLstForDelete,QString strWhereClause = "");
	bool DeleteFast(Status &pStatus, QList<int> &pLstForDelete,QString strWhereClause = "");

	//HELPER for chunk read/delete:
	void ReadFromParentIDs(Status &pStatus, DbRecordSet &pLstRead, QString strColFK2ParentIDName, const DbRecordSet &pLstOfID,QString strColIDName,QString strWhereClauseBefore="",QString strWhereClauseAfter="",int nQueryView=-1, bool bDistinct = false);
	void DeleteFromParentIDs(Status &pStatus, QString strColFK2ParentIDName, const DbRecordSet &pLstOfID,QString strColIDName,QString strWhereClauseBefore="",QString strWhereClauseAfter="");
	void WriteSubData(Status &pStatus, int nTableID,DbRecordSet &lstData,QString strParentFKColName="",int nParentFKValue=-1,bool bDeleteOrphanData=true,int nSkipLastColWrite=0,QString strSQLWhereFilter="");

	//returns inside objects for others to use:
	QSqlDatabase*	GetDbConnection(){return m_pDb;};
	DbSqlQuery*		GetDbSqlQuery(){return m_Query;};

private:

	void	FormatStatusRowsLst(DbRecordSet *pLstStatusRows);
	int		GetPrimaryColumnNo(const DbRecordSet &pLst);
	QString BindLike(QString &strWhere);
	void	DoInsertRows(Status &pStatus, DbRecordSet &pLstForWrite, int nQueryView, int nPrimaryKeyColumn,int nLastModifiedColumn, DbView &viewData,int nInsertSkipFirstCol,int nSkipLastColumns,DbRecordSet *pLstStatusRows=NULL);
	void	DoUpdateRows(Status &pStatus, DbRecordSet &pLstForWrite, int nQueryView, int nPrimaryKeyColumn,int nLastModifiedColumn, DbView &viewData,int nUpdateSkipFirstCol,int nSkipLastColumns,DbRecordSet *pLstStatusRows=NULL);

	//table data
	int				m_nTableID;
	DbTableKeyData	m_pTableKeyData;
	int				m_nTableFullView;
	QString			m_strPrimaryKey;
	QString			m_strLastModified;

	DbSqlManager	*m_pManager;
	QSqlDatabase	*m_pDb;
	DbSqlQuery		*m_Query;



};
#endif // DBSQLTABLEVIEW_H

