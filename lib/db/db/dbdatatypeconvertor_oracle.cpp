#include "dbdatatypeconvertor_oracle.h"



/*!
	If types are different, converts database value to needed by recordset.
	For MYSQL, only DECIMAL needs convert to double

	\param databaseValue		- QVariant from database
	\param colTypeInRecordSet	- column type inside recordset
*/
QVariant DbDataTypeConvertor_Oracle::ConvertValue(QVariant& databaseValue, int colTypeInRecordSet)
{
	Q_ASSERT(false); //don't know what to convert to what

	/*
	//if same type, ok:
	if (databaseValue.type()==colTypeInRecordSet)return databaseValue;

	//qDebug()<<databaseValue.type();

	Q_ASSERT(colTypeInRecordSet==QVariant::Double); //only for double

	*/
	return databaseValue.toDouble();
}
