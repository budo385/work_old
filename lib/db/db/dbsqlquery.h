#ifndef DBSQLQUERY_H
#define DBSQLQUERY_H

#include "common/common/status.h"
#include "db/db/dbsqlmanager.h"
#include "common/common/dbrecordset.h"
#include <QSqlQuery>


/*!
	\class DbSqlQuery
	\brief Sql Query class, built upon QSqlQuery, provides some extra functionality
	\ingroup SQLModule

	Class for Sql statements execution, transaction handling, record locking, handled over DbManager.
	Note: if not given in constructor, connection will be created and held until destruction,
	otherwise connection will not be 'touched'

*/
class DbSqlQuery
{
public:
	
	DbSqlQuery(Status &pStatus,DbSqlManager* pDatabaseManager);
	~DbSqlQuery();

	//transactions
	void BeginTransaction(Status &pStatus);
	void Rollback();
	void Commit(Status &pStatus);

	//execution
	bool Execute(Status &pStatus, QString SqlStatement, bool bSkipSetDefaults=false);
	bool ExecutePrepared(Status &pStatus, int nPrefetchPrimaryKeyValue=-1, bool bSkipSetDefaults=false);
	void Prepare(Status &pStatus, QString SqlStatement, bool bSkipSetDefaults=false);

	//bindings
	void bindValue(const QString &Placeholder, const QVariant &Value);
	void bindValue(int Position, const QVariant &Value);
	int BindRowValues(DbRecordSet &RecordSet, const int Row, int nSkipFirstNCols=0,int nSkipLastNCols=0,int nBindOffset=0,QSet<int> *pLstIsLongVarcharField=NULL);
	//fetch data
	void FetchData(DbRecordSet &RecordSet);
	bool FetchNextRow(DbRecordSet &RecordSet, bool bClearPreviousRows=false);
	void WriteData(Status &pStatus,QString strTableName,DbRecordSet &RecordSet, int nSkipDefaultColumnsCnt=2);
	void DeleteData(Status &pStatus,QString strTableName,DbRecordSet &lstOfIDs, int nIdxOfIDColumnInsideList, QString strWhereAfter="");

	QVariant value(int index);

	//helper
	int	 GetLastInsertedID(Status &pStatus, QString strPrimaryKey);
	int	 GetNextInsertedID(Status &pStatus, QString strPrimaryKey);

	
	bool next();
	int	 size() const;
	int	 rowsAffected() const;
	void clear();
	
	
	//lock records:
	//int LockRecordsDirect(Status &status, QString strTableName,QString strPrimaryKey, const DbLockList &lstRecords);
	//void UnLockRecordsDirect(Status &status, QString strTableName,QString strPrimaryKey, const DbLockList &lstRecords);

	//returns inside objects for others to use:
	QSqlDatabase* GetDbConnection(){return m_pDb;};
	QSqlQuery* GetQSqlQuery(){return m_pDbQuery;};


private:
	int GenerateChunkedWhereStatement(const DbRecordSet &pLstOfID,QString strColIDName,QString &strWhere,int nStartFrom,int nChunkSize,bool bAddQuotes);

	DbSqlManager*		m_pDbManager;
	QSqlDatabase*		m_pDb;
	QSqlQuery*			m_pDbQuery;
	bool				m_bKeepConnectionFlag;
	int					m_nPrefetchedPK;
	int					m_nPrefetchedPKBindPosition;
	QString				m_strPrefetchedPKName;
	QStringList*		m_plstAllLongVarCharFields;
};
#endif // DBSQLQUERY_H


