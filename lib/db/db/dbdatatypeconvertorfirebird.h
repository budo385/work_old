#ifndef DBDATATYPECONVERTORFIREBIRD_H
#define DBDATATYPECONVERTORFIREBIRD_H

#include "dbdatatypeconvertor.h"

class DbDataTypeConvertorFireBird : public DbDataTypeConvertor
{

public:
	QVariant ConvertValue(QVariant& databaseValue, int colTypeInRecordSet);

    
};

#endif // DBDATATYPECONVERTORFIREBIRD_H
