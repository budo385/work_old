#include "dbtransactionhandler_oracle.h"


/*!
	Commits transaction, gets current count if !=1 then do nothing, 
	coz Oracle doesnt support release savepoint. Last Commit should commit all changes.
	Be carefull with this....
	
	\param pStatus			- if something goes wrong
	\param pDbConnection	- pDbConnection connection to add (tracked by unique name)

*/
void DbTransactionHandlerOracle::CommitTransaction(Status &pStatus, QSqlDatabase* pDbConnection) 
{

	pStatus.setError(StatusCodeSet::ERR_NONE);

	int nTranCount=GetTransactionCount(pDbConnection);
	
	//single transaction
	if(nTranCount==1)
	{
		if (!pDbConnection->commit()) pStatus.setError(StatusCodeSet::ERR_SQL_TRANSACTION_FAIL);
		return;
	}
	
	//nested:
	//qDebug()<<"Nested transaction commit in progress, depth:"<<nTranCount;

/*
	QString strSQL="RELEASE SAVEPOINT point"+QVariant(nTranCount).toString();
	QSqlQuery query(*pDbConnection);
	if(!query.exec(strSQL))
		pStatus.setError(StatusCodeSet::ERR_SQL_TRANSACTION_FAIL);
*/

	//decrease counter:
	ReleaseTransaction(pDbConnection);

}



