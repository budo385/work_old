#ifndef DBDATATYPECONVERTORMYSQL_H
#define DBDATATYPECONVERTORMYSQL_H

#include "dbdatatypeconvertor.h"


class DbDataTypeConvertorMySQL : public DbDataTypeConvertor
{

public:
	QVariant ConvertValue(QVariant& databaseValue, int colTypeInRecordSet);
};

#endif // DBDATATYPECONVERTORMYSQL_H



