#ifndef ACCESSRIGHTSID_H
#define ACCESSRIGHTSID_H

/*!
\brief Definitions of Access Rights and Access Rights Sets IDs
\ingroup Db

Header file with definitions for Access Rights and Access Rights Sets IDs.

*/

//************************************************************************
//**						Access Right ID's							**
//************************************************************************
#define ACC_RIGHT_0					0 
#define ACC_RIGHT_1					1
#define ACC_RIGHT_2					2 
#define ACC_RIGHT_3					3
#define ACC_RIGHT_4					4 
#define ACC_RIGHT_5					5
    

//************************************************************************
//**					  Access Right Set ID's							**
//************************************************************************
#define ACC_RIGHT_SET_0				0
#define ACC_RIGHT_SET_1				1
#define ACC_RIGHT_SET_2				2
#define ACC_RIGHT_SET_3				3
#define ACC_RIGHT_SET_4				4


//************************************************************************
//**					Access Right Set Version ID's					**
//************************************************************************
#define ARS_VERSION_0				0

#endif // ACCESSRIGHTSID_H
