#ifndef DbConnectionsPool_H
#define DbConnectionsPool_H

#include <QList>
#include <QMutex>
#include <QSemaphore>
#include <QSqlDatabase>
#include <QReadWriteLock>
#include <QDateTime>

#include "common/common/status.h"
#include "db/db/dbtransactionhandler.h"




struct DBCN {
	QString strName;
	QSqlDatabase *pDb; 
	bool bLocked;
	int nThreadID;	//<for debugging to control how many connectin per thread
};



/*!
	\class DbConnectionsPool
	\brief Implements a pool with limited number of connections to a single database
	\ingroup SQLModule

	Pool object handles sharing a number of database connections among multiple 'users'.
	Multithread safe!
*/
class DbConnectionsPool
{
public:
    DbConnectionsPool();
    virtual ~DbConnectionsPool();
	
	//Init Transaction manager:
	void SetTransactionManager(DbTransactionHandler* pTransManager){m_pTransManager=pTransManager;};

	//database information
	void setDatabaseType   ( const QString & type );
	void setHostName       ( const QString & host );
	void setUserName       ( const QString & user );
	void setPassword       ( const QString & password );
	void setConnectOptions ( const QString & options = QString() );
	void setDatabaseName   ( const QString & name );
	void setPort           ( int port );
	void setPoolSize       ( int nMaxSize, int nMinSize=0 );

	//main pool interface
	QSqlDatabase*	ReserveConnection(Status &status, int nTimeoutMs = 0);
	void			ReleaseConnection(QSqlDatabase *&pConnection);
	QSqlDatabase*	GetThreadDbConnection();
	int				GetReservedConnections();
	void			EnablePool(bool bEnable = true);
	void			FillPoolWithMinConnections();
	void			ShutDown();
	void			ReserveExclusiveDbConnection(Status &pStatus, QSqlDatabase **pDb);
	void			ReleaseExclusiveDbConnection(QSqlDatabase **pDb);
	void			CheckConnections();

protected:
	void _Debug_Check_For_MultiReservations();

	QSqlDatabase	*DoReserveConnection(Status &status);
	DBCN			CreateNewConnection(Status &status, QString strDbType, QString strDbHost, QString strDbUser, QString strDbPassword, QString strDbOptions, int nDbPort, QString strDbName);
	void			Clear();
	
	int		m_nMaxSize;					// max size of the pool
	int		m_nMinSize;					// min size of the pool
	bool	m_bEnabled;

	QList<struct DBCN> m_lstPool;	// pool itself
	QHash<int,QSqlDatabase *>	m_hshThreadConnections;		//cache for connections per thread
	QReadWriteLock				m_hshThreadConnectionsLock;	//lock for m_hshThreadConnections
	QReadWriteLock				m_mainPoolLock;				//lock for m_lstPool
	QMutex						m_AccessLock;				//just for enabling/disabling pool
	QSemaphore					*m_pSem;					//for counting connections, if pool max size is set to N, new reservations attempts will hold until pool can be reached

	//database information
	QString m_strDbType;
	QString m_strDbOptions;
	QString m_strDbName;
	QString m_strDbHost;
	QString m_strDbUser;
	QString m_strDbPassword;
	int m_nDbPort;
	DbTransactionHandler* m_pTransManager;

	int							m_nLastConnectionReservedCount;

};


#endif // DbConnectionsPool_H


