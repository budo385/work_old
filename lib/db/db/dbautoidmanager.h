#ifndef DBAUTOIDMANAGER_H
#define DBAUTOIDMANAGER_H

#include <QSqlDatabase>
#include "common/common/status.h"

/*!
	\class DbAutoIdManager
	\ingroup SQLModule
	\brief Abstract class for implementation of Lastinsert id fetch method

*/

class DbAutoIdManager
{
public:
	virtual int getNextInsertId(Status &status, QSqlDatabase *pDbConnection, QString strPrimaryKey,int nPoolSize=1)=0;
	virtual int getLastInsertId(Status &status, QSqlDatabase *pDbConnection, QString strPrimaryKey)=0;
};

#endif //DBAUTOIDMANAGER_H


