#ifndef DBRECORDLOCKER_H
#define DBRECORDLOCKER_H

#include <QList>
#include "db_core/db_core/dbtableiddefinition.h"
#include "common/common/status.h"
#include "db/db/dbsqlmanager.h"
#include "db/db/dbsqlquery.h"
#include "db_core/db_core/dbsqltabledefinition.h"

typedef QList<int> DbLockList;

/*!
	\class DbRecordLocker
	\brief Locker class
	\author MP
	\ingroup SQLModule

	Class for row locking. Not multi thread safe
*/
class DbRecordLocker
{
public:
	DbRecordLocker (Status& pStatus, DbSqlManager* pDatabaseManager, QString strUserSession, QSqlDatabase *pDb = NULL, bool bCheckParents = true);
	virtual ~DbRecordLocker();

	virtual bool LockRecords(Status& pStatus, int TableID, DbLockList &LockList,QString& strLockedResourceID, DbRecordSet *pLstStatusRows=NULL, bool bAllOrNothing=true);
	bool UnlockByResourceID(Status& pStatus, QString& pLockedResourceID);
	bool UnlockByUserSessionID(Status& pStatus, QString& pUserSessionID);
	bool IsLocked(Status& pStatus, int TableID, DbLockList &LockList);

	//returns inside objects for others to use:
	QSqlDatabase* GetDbConnection(){return m_pDb;};

private:
	bool LockRecordsInternal(Status& pStatus, int TableID, DbLockList &LockList, QString& strLockedResourceID, DbRecordSet *pLstStatusRows=NULL,bool bAllOrNothing=true);
	bool IsLockedInternal(Status& pStatus, int TableID, DbLockList &LockList);
	DbLockList GetParentLockList(Status& pStatus, int TableID, DbLockList &LockList);
	
	void FormatStatusRowsLst(DbRecordSet *pLstStatusRows);
	DbSqlManager	*m_pDbManager;
	QSqlDatabase	*m_pDb;
	QString			m_strUserSession;
	static quint64	m_nUniqueLockKey;
	bool			m_bKeepConnectionFlag;
	bool			m_bCheckParents;
};
#endif // DBRECORDLOCKER_H

