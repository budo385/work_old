#ifndef RPCSTUBCLIENTSET_H
#define RPCSTUBCLIENTSET_H


#include <QtCore> 
#include <QDebug>
#include "common/common/status.h"
#include "connectionhandler.h"
#include "rpcstub_collection.h"
#include "bus_interface/bus_interface/interface_businessserviceset.h"


/*!
    \class RpcStubClientSet
    \brief Repository of all stub objects, register your stub here
    \ingroup Bus_Trans_Client

	Instanced by BusinessServiceSetProxy in thin client mode. Redirects all business calls to stubs.
	Making STUB object:
	- make business object interface, put it int BusinessServiceSetInterface
	- see RpcStub_TestBusinessObject on how to write own stub
	- when finsihed include stub object header file in the rpcstubcollection.h
	- instance pointer to apropriate stubrpc object in constructor, register it in m_ServiceList
	When destroyed all stubs are automatically destroyed (from m_ServiceList)

*/

class RpcStubClientSet : public Interface_BusinessServiceSet
{
public:
	RpcStubClientSet(ConnectionHandler *pConnectionHandler,int RPCType);
	virtual ~RpcStubClientSet();

protected:
	void CheckSet();

	QList<RpcStub *> m_StubList;	//< Used to store pointers to subskeletons in skeleton set

};


#endif //RPCSTUBCLIENTSET_H
