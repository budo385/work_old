#ifndef RPCHTTPCLIENT_H
#define RPCHTTPCLIENT_H

#include <QtCore>

#include "common/common/status.h"
#include "trans/trans/httpcontext.h"
#include "trans/trans/httprequest.h"
#include "trans/trans/httpresponse.h"
#include "trans/trans/httphandler.h"
#include "trans/trans/httpclientconnection.h"
#include "trans/trans/httpclientconnectionsettings.h"
#include "trans/trans/rpcskeleton.h"
#include "trans/trans/rpchttpheader.h"



/*!
    \class RpcHTTPClient
    \brief Client handler for RPC messages over HTTP,base class
    \ingroup Bus_Trans_Client

	This is RPC modificitaion of HTTP client.
	Sends RPC, adds HTTP header and uses HTTPClientThread to make call to the server.
	Can accept server messages, must register pSrvMsgSkeletonSet to accept srv. messages

*/
class RpcHTTPClient : protected HTTPHandler, public HTTPClientThread, protected RpcProtocol
{

public:
	RpcHTTPClient(RpcSkeleton *pSrvMsgSkeletonSet, int pNumRPCProtocolType);
	~RpcHTTPClient(){};

public:
	//--------------------------------------
	//Main interface: override
	//--------------------------------------
	void SetConnectionSettings(HTTPClientConnectionSettings& pConnection);
	HTTPClientConnectionSettings GetConnectionSettings();
	void Connect(Status& pStatus);
	void Disconnect(Status& pStatus);
	void SendData(Status& pStatus, HTTPRequest* pHTTPRequest, HTTPResponse* pHTTPResponse=NULL);	//Implementation of SendData

	//helper funct:
	void ReConnect(Status& pStatus,QString strIP,int nPort);

private:
	void HandleRequest(Status &status, HTTPContext &ctx, HTTPRequest &request, HTTPResponse &response,HTTPStreamHandler **pStreamHandler=NULL);

	RpcSkeleton *m_SrvMsgSkeletonSet;
	RpcHttpHeader m_HttpHeader;
};


#endif //RPCHTTPCLIENT_H



