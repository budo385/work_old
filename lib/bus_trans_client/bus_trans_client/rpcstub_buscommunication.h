#ifndef RPCSTUB_BUSCOMMUNICATION_H__
#define RPCSTUB_BUSCOMMUNICATION_H__

#include "common/common/status.h"
#include "bus_interface/bus_interface/interface_buscommunication.h"
#include "bus_trans_client/bus_trans_client/connectionhandler.h"
#include "trans/trans/rpcstubmessagehandler.h"

class RpcStub_BusCommunication: public Interface_BusCommunication, public RpcStub
{
public:
	RpcStub_BusCommunication(ConnectionHandler *pConnectionHandler,int RPCType):
		RpcStub(RPCType){m_ConnectionHandler=pConnectionHandler;}

	void SaveCommFilterViews(Status &Ret_pStatus, int &Ret_nViewID, DbRecordSet &recView, DbRecordSet &recViewsData);
	void GetDefaultDesktopFilterViews(Status &Ret_pStatus, int nLoggedPersonID, DbRecordSet &RetOut_recViews);
	void GetCommFilterViews(Status &Ret_pStatus, DbRecordSet &RetOut_recViews, int nPersonID, int nGridType);
	void GetCommFilterData(Status &Ret_pStatus, DbRecordSet &RetOut_recSortRecordSet, DbRecordSet &RetOut_recViewsData, DbRecordSet &RetOut_recViewsChBoxesData, int nViewID, int nLoggedPersonID);
	void GetCommFilterViewDataAndViews(Status &Ret_pStatus, DbRecordSet &RetOut_recViewData, DbRecordSet &RetOut_recViews, int nViewID, int nGridType);
	void GetGridViewForContact(Status &Ret_pStatus, int &RetOut_nViewID, int nContactID);
	void GetGridViewForProject(Status &Ret_pStatus, int &RetOut_nViewID, int nProjectID);
	void GetPersonCommData(Status &Ret_pStatus, bool &Ret_bRowsStripped, DbRecordSet &RetOut_recEmails, DbRecordSet &RetOut_recDocuments, DbRecordSet &RetOut_recVoiceCalls, int PersonID, DbRecordSet recFilter, bool bForceFilterByTaskType = false);
	void GetContactCommData(Status &Ret_pStatus, DbRecordSet &RetOut_recEmails, DbRecordSet &RetOut_recDocuments, DbRecordSet &RetOut_recVoiceCalls, int ContactID, DbRecordSet recFilter);
	void GetProjectCommData(Status &Ret_pStatus, DbRecordSet &RetOut_recEmails, DbRecordSet &RetOut_recDocuments, DbRecordSet &RetOut_recVoiceCalls, int nProjectID, DbRecordSet recFilter);
	void GetCalendarCommData(Status &Ret_pStatus, DbRecordSet &RetOut_recEmails, DbRecordSet &RetOut_recDocuments, DbRecordSet &RetOut_recVoiceCalls, DbRecordSet recEntities, DbRecordSet recFilter);
	void GetCalendarGridCommData(Status &Ret_pStatus, DbRecordSet &RetOut_recEmails, DbRecordSet &RetOut_recDocuments, DbRecordSet &RetOut_recVoiceCalls, DbRecordSet recEntities, DbRecordSet recFilter);
	void GetCommGridDataByIDs(Status &Ret_pStatus, DbRecordSet &RetOut_recData, DbRecordSet recIDsRecordset, int nGridType, int nEntityType);
	void GetComplexFilterData(Status &Ret_pStatus, DbRecordSet &RetOut_recFilterData, int nPersonID);
	void GetContactFromEmail(Status &Ret_pStatus, DbRecordSet &RetOut_lstData);
	void SetTaskIsDone(Status &Ret_pStatus, DbRecordSet &lstData);
	void PostPoneTasks(Status &Ret_pStatus, DbRecordSet &lstData, QDateTime datDueDateTime, QDateTime datStartDateTime);
	void SetEmailsRead(Status &Ret_pStatus, DbRecordSet &lstData);
	void ChangeEntityAssigment(Status &Ret_pStatus, int nAssignedType,int nAssignedID, int nOperation, DbRecordSet &Data);
	void ReadCEMenuData(Status &Ret_pStatus, int nPersonID, DbRecordSet &Ret_DocTemplate, DbRecordSet &Ret_DocApps, DbRecordSet &Ret_DocCheckOut,DbRecordSet &Ret_EmailTemplate);
	void AssignCommDataToContact(Status &Ret_pStatus, DbRecordSet &Data, int nContactID);
	void AssignCommDataToProject(Status &Ret_pStatus, DbRecordSet &Data, int nProjectID);
	void CheckIfRecordExists(Status &Ret_pStatus, int nEntityType, int nRecordID, bool &Ret_bExists);

private:
	ConnectionHandler *m_ConnectionHandler; ///< connection handler
};

#endif	// RPCSTUB_BUSCOMMUNICATION_H__
