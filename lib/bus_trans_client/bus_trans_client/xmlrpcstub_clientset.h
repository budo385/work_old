#ifndef XMLRPCSTUBCLIENTSET_H
#define XMLRPCSTUBCLIENTSET_H

#include "common/common/status.h"
#include "rpcstub_clientset.h"
#include "bus_interface/bus_interface/interface_businessserviceset.h"


/*!
    \class XmlRpcStubClientSet
    \brief XML-RPC implementation of RPC stub set
    \ingroup Bus_Trans_Client

	\sa RpcStubClientSet
*/

class XmlRpcStubClientSet : public RpcStubClientSet
{
public:
	XmlRpcStubClientSet(XmlRpcHTTPConnectionHandler *pConnectionHandler):RpcStubClientSet(pConnectionHandler,RPC_PROTOCOL_TYPE_XML_RPC){};
	virtual ~XmlRpcStubClientSet(){};

};


#endif //XMLRPCSTUBCLIENTSET_H
