#include "rpcstub_clientsimpleorm.h"

void RpcStub_ClientSimpleORM::Write(Status &Ret_pStatus, int nTableID, DbRecordSet &RetOut_pLstForWrite, QString pLockResourceID, int nQueryView, int nSkipLastColumns, DbRecordSet &RetOut_pLstForDelete)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&nTableID,"nTableID");
	msg.msg_in->AddParameter(&RetOut_pLstForWrite,"RetOut_pLstForWrite");
	msg.msg_in->AddParameter(&pLockResourceID,"pLockResourceID");
	msg.msg_in->AddParameter(&nQueryView,"nQueryView");
	msg.msg_in->AddParameter(&nSkipLastColumns,"nSkipLastColumns");
	msg.msg_in->AddParameter(&RetOut_pLstForDelete,"RetOut_pLstForDelete");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("ClientSimpleORM.Write",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&RetOut_pLstForWrite,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(1,&RetOut_pLstForDelete,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_ClientSimpleORM::WriteParentSubData(Status &Ret_pStatus, int nTableID, DbRecordSet &RetOut_pLstForWrite, int nParentId, QString strColParentName,QString pLockResourceID ,int nQueryView )
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&nTableID,"nTableID");
	msg.msg_in->AddParameter(&RetOut_pLstForWrite,"RetOut_pLstForWrite");
	msg.msg_in->AddParameter(&nParentId,"nParentId");
	msg.msg_in->AddParameter(&strColParentName,"strColParentName");
	msg.msg_in->AddParameter(&pLockResourceID,"pLockResourceID");
	msg.msg_in->AddParameter(&nQueryView,"nQueryView");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("ClientSimpleORM.WriteParentSubData",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&RetOut_pLstForWrite,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_ClientSimpleORM::Read(Status &Ret_pStatus, int nTableID, DbRecordSet &Ret_pLstRead, QString strWhereClause , int nQueryView , bool bDistinct )
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&nTableID,"nTableID");
	msg.msg_in->AddParameter(&strWhereClause,"strWhereClause");
	msg.msg_in->AddParameter(&nQueryView,"nQueryView");
	msg.msg_in->AddParameter(&bDistinct,"bDistinct");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("ClientSimpleORM.Read",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&Ret_pLstRead,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_ClientSimpleORM::ReadAdv(Status &Ret_pStatus, DbRecordSet &RetOut_pLstRead, QString strJoinClause)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&RetOut_pLstRead,"RetOut_pLstRead");
	msg.msg_in->AddParameter(&strJoinClause,"strJoinClause");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("ClientSimpleORM.ReadAdv",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&RetOut_pLstRead,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_ClientSimpleORM::ReadFromParentIDs(Status &Ret_pStatus, int nTableID,DbRecordSet &Ret_pLstRead, QString strColFK2ParentIDName, DbRecordSet &pLstOfID,QString strColIDName,QString strWhereClauseBefore,QString strWhereClauseAfter,int nQueryView, bool bDistinct )
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&nTableID,"nTableID");
	msg.msg_in->AddParameter(&strColFK2ParentIDName,"strColFK2ParentIDName");
	msg.msg_in->AddParameter(&pLstOfID,"pLstOfID");
	msg.msg_in->AddParameter(&strColIDName,"strColIDName");
	msg.msg_in->AddParameter(&strWhereClauseBefore,"strWhereClauseBefore");
	msg.msg_in->AddParameter(&strWhereClauseAfter,"strWhereClauseAfter");
	msg.msg_in->AddParameter(&nQueryView,"nQueryView");
	msg.msg_in->AddParameter(&bDistinct,"bDistinct");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("ClientSimpleORM.ReadFromParentIDs",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&Ret_pLstRead,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_ClientSimpleORM::Delete(Status &Ret_pStatus, int nTableID, DbRecordSet &pLstForDelete, QString pLockResourceID, DbRecordSet &Ret_pLstStatusRows, bool pBoolTransaction)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&nTableID,"nTableID");
	msg.msg_in->AddParameter(&pLstForDelete,"pLstForDelete");
	msg.msg_in->AddParameter(&pLockResourceID,"pLockResourceID");
	msg.msg_in->AddParameter(&pBoolTransaction,"pBoolTransaction");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("ClientSimpleORM.Delete",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&Ret_pLstStatusRows,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_ClientSimpleORM::Lock(Status &Ret_pStatus, int nTableID, DbRecordSet &pLstForLocking, QString &Ret_pResourceID, DbRecordSet &Ret_pLstStatusRows, bool bAllOrNothing )
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&nTableID,"nTableID");
	msg.msg_in->AddParameter(&pLstForLocking,"pLstForLocking");
	msg.msg_in->AddParameter(&bAllOrNothing,"bAllOrNothing");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("ClientSimpleORM.Lock",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&Ret_pResourceID,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(1,&Ret_pLstStatusRows,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_ClientSimpleORM::UnLock(Status &Ret_pStatus, int nTableID, bool &Ret_bUnLocked, QString &pResourceID)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&nTableID,"nTableID");
	msg.msg_in->AddParameter(&pResourceID,"pResourceID");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("ClientSimpleORM.UnLock",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&Ret_bUnLocked,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_ClientSimpleORM::UnLockByRecordID(Status &Ret_pStatus, int nTableID, DbRecordSet &pLstForUnLocking, bool &Ret_bUnLocked)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&nTableID,"nTableID");
	msg.msg_in->AddParameter(&pLstForUnLocking,"pLstForUnLocking");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("ClientSimpleORM.UnLockByRecordID",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&Ret_bUnLocked,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_ClientSimpleORM::IsLocked(Status &Ret_pStatus, int nTableID, bool &Ret_bLocked, DbRecordSet &LockList)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&nTableID,"nTableID");
	msg.msg_in->AddParameter(&LockList,"LockList");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("ClientSimpleORM.IsLocked",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&Ret_bLocked,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_ClientSimpleORM::GetRowCount(Status &Ret_pStatus, int nTableID, QString strWhereClause, int &Ret_nCount)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&nTableID,"nTableID");
	msg.msg_in->AddParameter(&strWhereClause,"strWhereClause");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("ClientSimpleORM.GetRowCount",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&Ret_nCount,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_ClientSimpleORM::ExecuteSQL(Status &Ret_pStatus, QString strSQL, DbRecordSet &Ret_LstReturn)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&strSQL,"strSQL");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("ClientSimpleORM.ExecuteSQL",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&Ret_LstReturn,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_ClientSimpleORM::WriteHierarchicalData(Status &Ret_pStatus, int nTableID,DbRecordSet &RetOut_pLstForWrite, QString pLockResourceID , QString strSQLWhereFilter, int nQueryView )
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&nTableID,"nTableID");
	msg.msg_in->AddParameter(&RetOut_pLstForWrite,"RetOut_pLstForWrite");
	msg.msg_in->AddParameter(&pLockResourceID,"pLockResourceID");
	msg.msg_in->AddParameter(&strSQLWhereFilter,"strSQLWhereFilter");
	msg.msg_in->AddParameter(&nQueryView,"nQueryView");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("ClientSimpleORM.WriteHierarchicalData",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&RetOut_pLstForWrite,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_ClientSimpleORM::DeleteHierarchicalData(Status &Ret_pStatus, int nTableID,DbRecordSet &pLstForDelete, QString pLockResourceID , QString strSQLWhereFilter)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&nTableID,"nTableID");
	msg.msg_in->AddParameter(&pLstForDelete,"pLstForDelete");
	msg.msg_in->AddParameter(&pLockResourceID,"pLockResourceID");
	msg.msg_in->AddParameter(&strSQLWhereFilter,"strSQLWhereFilter");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("ClientSimpleORM.DeleteHierarchicalData",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
}

