#ifndef RPCSTUB_BOENTITY_TESTCONTACT_H__
#define RPCSTUB_BOENTITY_TESTCONTACT_H__

#include <QtCore>
#include <QDebug>
#include "common/common/status.h"
#include "bus_interface/bus_interface/interface_boentity.h"
#include "connectionhandler.h"
#include "db_core/db_core/dbcore_include.h"
#include "trans/trans/rpcstubmessagehandler.h"

class RpcStub_BoEntity_TestContact: public Interface_BoEntity, public RpcStub
{
public:
	RpcStub_BoEntity_TestContact(ConnectionHandler *pConnectionHandler,int RPCType):
		RpcStub(RPCType){m_ConnectionHandler=pConnectionHandler;}

	void Read(Status &Ret_pStatus, DbRecordSet &RetOut_Data, SQLDBPointer* pDbConn = NULL);
	void Write(Status &Ret_pStatus, DbRecordSet &RetOut_Data, SQLDBPointer* pDbConn = NULL);
	void Delete(Status &Ret_pStatus, DbRecordSet &Data, SQLDBPointer* pDbConn = NULL);
	void PrepareForEdit(Status &Ret_pStatus, DbRecordSet &RetOut_Data, bool bOnlyCheckForReload=false,SQLDBPointer* pDbConn = NULL);
	void CancelEdit(Status &Ret_pStatus, DbRecordSet &Data, SQLDBPointer* pDbConn = NULL);
	void Copy(Status &Ret_pStatus, DbRecordSet &Data, DbRecordSet &Ret_Data,SQLDBPointer* pDbConn = NULL);

private:
	ConnectionHandler *m_ConnectionHandler; ///< connection handler
};

#endif	// RPCSTUB_BOENTITY_TESTCONTACT_H__
