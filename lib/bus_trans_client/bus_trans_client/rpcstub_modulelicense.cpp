#include "rpcstub_modulelicense.h"

void RpcStub_ModuleLicense::GetAllModuleLicenseData(Status& Ret_pStatus, QString& strModuleCode, QString& strMLIID,DbRecordSet& RetOut_pList,QString& RetOut_strUserName, int& RetOut_nLicenseID, QString &RetOut_strReportLine1, QString &RetOut_strReportLine2, QString &Ret_strCustomerID,int &Ret_nCustomSolutionID)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&strModuleCode,"strModuleCode");
	msg.msg_in->AddParameter(&strMLIID,"strMLIID");
	msg.msg_in->AddParameter(&RetOut_pList,"RetOut_pList");
	msg.msg_in->AddParameter(&RetOut_strUserName,"RetOut_strUserName");
	msg.msg_in->AddParameter(&RetOut_nLicenseID,"RetOut_nLicenseID");
	msg.msg_in->AddParameter(&RetOut_strReportLine1,"RetOut_strReportLine1");
	msg.msg_in->AddParameter(&RetOut_strReportLine2,"RetOut_strReportLine2");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("ModuleLicense.GetAllModuleLicenseData",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&RetOut_pList,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(1,&RetOut_strUserName,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(2,&RetOut_nLicenseID,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(3,&RetOut_strReportLine1,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(4,&RetOut_strReportLine2,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(5,&Ret_strCustomerID,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(6,&Ret_nCustomSolutionID,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_ModuleLicense::GetFPList(Status& Ret_pStatus, QString& strModuleCode, QString& strMLIID, DbRecordSet& RetOut_pList)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&strModuleCode,"strModuleCode");
	msg.msg_in->AddParameter(&strMLIID,"strMLIID");
	msg.msg_in->AddParameter(&RetOut_pList,"RetOut_pList");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("ModuleLicense.GetFPList",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&RetOut_pList,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_ModuleLicense::GetLicenseInfo(Status& Ret_pStatus, QString& RetOut_strUserName, int& RetOut_nLicenseID, QString &RetOut_strReportLine1, QString &RetOut_strReportLine2, QString &Ret_strCustomerID,int &Ret_nCustomSolutionID)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&RetOut_strUserName,"RetOut_strUserName");
	msg.msg_in->AddParameter(&RetOut_nLicenseID,"RetOut_nLicenseID");
	msg.msg_in->AddParameter(&RetOut_strReportLine1,"RetOut_strReportLine1");
	msg.msg_in->AddParameter(&RetOut_strReportLine2,"RetOut_strReportLine2");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("ModuleLicense.GetLicenseInfo",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&RetOut_strUserName,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(1,&RetOut_nLicenseID,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(2,&RetOut_strReportLine1,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(3,&RetOut_strReportLine2,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(4,&Ret_strCustomerID,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(5,&Ret_nCustomSolutionID,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

