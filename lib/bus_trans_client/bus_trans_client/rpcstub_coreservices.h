#ifndef RPCSTUB_CORESERVICES_H__
#define RPCSTUB_CORESERVICES_H__

#include "common/common/status.h"
#include "bus_interface/bus_interface/interface_coreservices.h"
#include "bus_trans_client/bus_trans_client/connectionhandler.h"
#include "trans/trans/rpcstubmessagehandler.h"

class RpcStub_CoreServices: public Interface_CoreServices, public RpcStub
{
public:
	RpcStub_CoreServices(ConnectionHandler *pConnectionHandler,int RPCType):
		RpcStub(RPCType){m_ConnectionHandler=pConnectionHandler;}

	void ReadLBOList(Status &Ret_pStatus, DbRecordSet &Ret_Data);
	void CheckVersion(Status& Ret_Status,QString strClientVersion,QString &Ret_strServerVersion,QString& Ret_strServerDbVersion,QString& Ret_strTemplateInUse,int& Ret_nAskOnStartup,int& Ret_nStartStartUpWizard,QByteArray& Ret_byteVersion, int &Ret_nClientTimeZoneOffsetMinutes);
	void UpdateVersion(Status& Ret_Status,QByteArray byteVersion);
	void ReadUserSessionData(Status& Ret_Status,DbRecordSet &Ret_UserData, DbRecordSet &Ret_ARSet,DbRecordSet &Ret_ContactData, DbRecordSet &Ret_recSettings, DbRecordSet &Ret_recOptions, DbRecordSet &Ret_lstMessages);
	void GetUserSessions(Status& Ret_Status,int &Ret_nUserSessions);
	void SavePersonalSettings(Status &Ret_pStatus, DbRecordSet &RetOut_pLstForWrite);
	void SavePersonalSettingsByPersonID(Status &Ret_pStatus, int nPersonID, DbRecordSet &RetOut_pLstForWrite);
	void GetPersonalSettingsByPersonID(Status &Ret_pStatus, int nPersonID, DbRecordSet &RetOut_pLstForWrite);

private:
	ConnectionHandler *m_ConnectionHandler; ///< connection handler
};

#endif	// RPCSTUB_CORESERVICES_H__
