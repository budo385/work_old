#ifndef RPCSTUB_BUSEMAIL_H__
#define RPCSTUB_BUSEMAIL_H__

#include "common/common/status.h"
#include "bus_interface/bus_interface/interface_busemail.h"
#include "bus_trans_client/bus_trans_client/connectionhandler.h"
#include "trans/trans/rpcstubmessagehandler.h"

class RpcStub_BusEmail: public Interface_BusEmail, public RpcStub
{
public:
	RpcStub_BusEmail(ConnectionHandler *pConnectionHandler,int RPCType):
		RpcStub(RPCType){m_ConnectionHandler=pConnectionHandler;}

	void Read(Status &Ret_pStatus, int nEmailID, DbRecordSet &RetOut_Data, DbRecordSet &Ret_ContactLink,DbRecordSet &Ret_ScheduleTask,DbRecordSet &Ret_ContactEmails, DbRecordSet &Ret_Attachments,DbRecordSet &Ret_UAR,DbRecordSet &Ret_GAR );
	void Write(Status &Ret_pStatus, DbRecordSet &RetOut_Data, DbRecordSet &RetOut_ContactLink, DbRecordSet &RetOut_ScheduleTask, DbRecordSet &RetOut_Attachments,QString strLockRes,DbRecordSet &RetOut_UAR,DbRecordSet &RetOut_GAR);
	void WriteMultiple(Status &Ret_pStatus, DbRecordSet &RetOut_Data, DbRecordSet &RetOut_ContactLink, DbRecordSet &RetOut_ScheduleTask, DbRecordSet &RetOut_Attachments,QString strLockRes,bool bSkipExisting, int &RetOut_nSkippedOrReplacedCnt, bool bAddDupsAsNewRow);
	void WriteEmailLog(Status &Ret_pStatus, DbRecordSet &RetOut_Data, QString strLockRes);
	void List(Status &Ret_pStatus, DbRecordSet &RetOut_lstData, QDate &datDateFrom, QDate &datDateTo, int nOwnerID, int nFilterID);
	void Delete(Status &Ret_pStatus, DbRecordSet &RetOut_lstIDs);
	void UpdateEmailFromClient(Status &Ret_pStatus, DbRecordSet &Emails, DbRecordSet &Attachments);
	void SetEmailRead(Status &Ret_pStatus, int nCE_ID);
	void WriteAttachments(Status &Ret_pStatus,int nEmailID, DbRecordSet &RetOut_Attachments);
	void WriteEmailAttachmentsFromUserStorage(Status &Ret_pStatus,int nEmailID, DbRecordSet &Attachments);

private:
	ConnectionHandler *m_ConnectionHandler; ///< connection handler
};

#endif	// RPCSTUB_BUSEMAIL_H__
