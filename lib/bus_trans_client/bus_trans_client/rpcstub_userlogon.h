#ifndef RPCSTUB_USERLOGON_H__
#define RPCSTUB_USERLOGON_H__

#include "common/common/status.h"
#include "bus_interface/bus_interface/interface_userlogon.h"
#include "bus_trans_client/bus_trans_client/connectionhandler.h"
#include "trans/trans/rpcstubmessagehandler.h"

class RpcStub_UserLogon: public Interface_UserLogon, public RpcStub
{
public:
	RpcStub_UserLogon(ConnectionHandler *pConnectionHandler,int RPCType):
		RpcStub(RPCType){m_ConnectionHandler=pConnectionHandler;}

	void Login(Status& Ret_Status, QString strUsername, QString strPassword,QString strModuleCode, QString strMLIID="", QString strParentSession="", int nClientTimeZoneOffsetMinutes=0);
	void Logout(Status& Ret_Status);
	void ChangePassword(Status& Ret_Status,QString strUsername, QByteArray strAuthToken,QByteArray strClientNonce,QByteArray newPass,QString strNewUserName);

private:
	void Login(Status& Ret_Status, QString& RetOut_strSessionID,QString strUsername, QByteArray strAuthToken,QByteArray strClientNonce,QString strModuleCode, QString strMLIID="", QString strParentSession="", bool bIsWebLogin=false, int nClientTimeZoneOffsetMinutes=0){};
	void IsAlive(Status& Ret_Status){};
	ConnectionHandler *m_ConnectionHandler; ///< connection handler
};

#endif	// RPCSTUB_USERLOGON_H__
