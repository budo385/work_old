#include "connectionisalive.h"


/*!
	Constructor: accepts pointer to conn. stub set
	\param pStubConnHandlerSet connection stub set
*/

ConnectionIsAlive::ConnectionIsAlive(RpcStubConnHandlerSet *pStubConnHandlerSet)
{

	m_StubConnHandlerSet=pStubConnHandlerSet;
	m_nIsAliveInterval=300000; //300secs
	m_nTimerID=-1; //disable timer
}

/*!
	Loads IsAlive interval from server
	\param strSessionID session ID, cached locally
	\param nIsAliveInterval isALIVE in Seconds
*/
void ConnectionIsAlive::InitIsAlive(QString strSessionID,int nIsAliveInterval)
{
	m_nIsAliveInterval=nIsAliveInterval*1000;
	m_strSessionID=strSessionID;
}

/*!
	Starts Timer
*/
void ConnectionIsAlive::StartIsAlive(){
	m_nTimerID=startTimer(m_nIsAliveInterval);
}

/*!
	Stops Timer
*/
void ConnectionIsAlive::StopIsAlive(){
	if(m_nTimerID!=-1)killTimer(m_nTimerID);
	m_nTimerID=-1;
}


/*!
	Sends Async call to server every N msec defined by interval	
	Note: overriden Q_OBJECT base class method invoked by its internal timer
*/
void ConnectionIsAlive::timerEvent(QTimerEvent *event){
	//check if our timer:
	Status err; // do not check
	qDebug()<<"IsAlive ping!";
	if(event->timerId()==m_nTimerID) m_StubConnHandlerSet->IsAlive(err,m_strSessionID);
	if(!err.IsOK()) qDebug()<<err.getErrorText();
}
