#ifndef RPCSTUB_BUSADDRESSSCHEMAS_H__
#define RPCSTUB_BUSADDRESSSCHEMAS_H__

#include "common/common/status.h"
#include "bus_interface/bus_interface/interface_busaddressschemas.h"
#include "bus_trans_client/bus_trans_client/connectionhandler.h"
#include "trans/trans/rpcstubmessagehandler.h"

class RpcStub_BusAddressSchemas: public Interface_BusAddressSchemas, public RpcStub
{
public:
	RpcStub_BusAddressSchemas(ConnectionHandler *pConnectionHandler,int RPCType):
		RpcStub(RPCType){m_ConnectionHandler=pConnectionHandler;}

	void Write(Status &Ret_pStatus, DbRecordSet &RetOut_Data,QString strLockRes);
	void Read(Status &Ret_pStatus, DbRecordSet &Ret_Data);
	void Delete(Status &Ret_pStatus, int nRecordID);
	void Lock(Status &Ret_pStatus, int nRecordID,QString &Ret_strLockRes);
	void Unlock(Status &Ret_pStatus, QString strLockRes);

private:
	ConnectionHandler *m_ConnectionHandler; ///< connection handler
};

#endif	// RPCSTUB_BUSADDRESSSCHEMAS_H__
