#include "rpchttpclient.h"
#include "common/common/config_version_sc.h"

/*!
	Constructor: registers domain and itself as server msg handler
	\param pSrvMsgSkeletonSet pointer to skeleton set to which server msg will be dispatched
	\param pNumRPCProtocolType - RPC protocol 
*/
RpcHTTPClient::RpcHTTPClient(RpcSkeleton *pSrvMsgSkeletonSet, int pNumRPCProtocolType)
:RpcProtocol(pNumRPCProtocolType)
{
	m_SrvMsgSkeletonSet=pSrvMsgSkeletonSet;	//stores pointer to set to which server messages will be dispatch
	RegisterServerMsgHandler(this); //register itself to httpclientconnection
	
	m_HttpHeader.InitRpcProtocol(this);	//init http header informator
	m_HttpHeader.SetHTTPTags(QString(SERVER_HTTP_DESCRIPTION).arg(APPLICATION_VERSION),QString(CLIENT_HTTP_DESCRIPTION).arg(APPLICATION_VERSION));
}





//-------------------------------------------------------------------------
//					CONN. PARAMETERS
//-------------------------------------------------------------------------


/*!
	Sets connection settings, does not connect until explicit call to connect, or to send data
	/param pConnSettings 
*/
void RpcHTTPClient::SetConnectionSettings(HTTPClientConnectionSettings& pConnSettings)
{
	//if connected/destroy connection:
	Status err;
	err.setError(0);
	if(IsConnected())HTTPClientThread::Disconnect(err);
	Q_ASSERT(err.IsOK());

	//set new settings
	HTTPClientThread::SetConnectionSettings(pConnSettings); //sets conn settings

	m_HttpHeader.InitHost(pConnSettings.m_strServerIP,pConnSettings.m_nPort);
	m_HttpHeader.UseCompression(pConnSettings.m_bUseCompression);
}

/*!
	Gets connection settings
	/return ConnSettings 
*/
HTTPClientConnectionSettings RpcHTTPClient::GetConnectionSettings(){
	return HTTPClientThread::GetConnectionSettings(); //gets conn settings
}


/*!
	Connect to server based on previously settes conn. settings. Error if already connected
	/param pStatus, 0 if OK
*/
void RpcHTTPClient:: Connect(Status& pStatus)
{
	HTTPClientConnectionSettings tmpJBLinux=HTTPClientThread::GetConnectionSettings(); //je.. Linux
	HTTPClientThread::Connect(pStatus,tmpJBLinux);
}



/*!
	Closes current connection, uses same conn. parameters as before, but new IP:port
	/param pStatus	, 0 if OK
	/param strIP	, new IP address
	/param nPort	, new port, if 0 then usess old port!
*/
void RpcHTTPClient::ReConnect(Status& pStatus,QString strIP,int nPort){

	//if connected, broke connection:
	if(IsConnected())
		{ Disconnect(pStatus);if(!pStatus.IsOK())return;}
	
	//set new IP address:
	HTTPClientConnectionSettings conn=HTTPClientThread::GetConnectionSettings();
	conn.m_strServerIP=strIP;
	if(nPort!=0)conn.m_nPort=nPort;
	SetConnectionSettings(conn);

	//try to connect:
	Connect(pStatus);
}


/*!
	Disconnect from server: wait for current operation
*/
void RpcHTTPClient::Disconnect(Status& pStatus){
	HTTPClientThread::Disconnect(pStatus);
}

//-------------------------------------------------------------------------
//						RPC message handlers
//-------------------------------------------------------------------------


/*!
	Send request to the server, if pHTTPResponse=NULL then request is ASYNC otherwise it is SYNC (thread waits for response)
	Inserts HTTP XML-RPC header. Automatically connects based on given conn. parameters if not connected.
	/param pStatus, 0 if OK
	/param pHTTPRequest client request
	/param HTTPResponse server response, IF NULL then ASYNC mode
*/
void RpcHTTPClient::SendData(Status& pStatus, HTTPRequest* pHTTPRequest, HTTPResponse* pHTTPResponse){
	
	//if not connected try to connect:
	if(!IsConnected())
		{ Connect(pStatus);if(!pStatus.IsOK())return;}
	
	//add common values to header:
	m_HttpHeader.SetHttpRequestHeader(*pHTTPRequest);

	//qDebug() << pHTTPRequest->toString();
	//qDebug() << pHTTPRequest->GetBody()->constData();

	//send data:
	HTTPClientThread::SendData(pStatus,pHTTPRequest,pHTTPResponse);
}

/*!
	Invoked by socket thread when new server message arrives. Only accepts ASYNC messages, no response is returned
	Only request parameter is important: it contains RPC server request message.
	Request is dispatched to the registered ClientSkeletonServerMessageSet
	/param status	- returned status (not important, as caller probably dont check this)
	/param ctx		- HTTPContext (not valid, as server IP context is not usefull)
	/param request	- HTTP request from server
	/param response - HTTP response from handler (not valid, as this is async handler)
*/
void RpcHTTPClient::HandleRequest(Status &status, HTTPContext &ctx, HTTPRequest &request, HTTPResponse &response,HTTPStreamHandler **pStreamHandler)
{

	//dispatch server message if registered
	Status err;
	if(m_SrvMsgSkeletonSet!=NULL)
		m_SrvMsgSkeletonSet->HandleRPC(err,request.GetBody(),response.GetBody(),"");
	
	//qDebug()<<"Serving Server message:\r\n";
	//qDebug()<<request.toString()<<"\r\n"<<request.GetBody();

	status.setError(0);
}

