#include "rpcstub_clientset.h"


/*!
	Make instance of every stub. Client directly calls this stub pointers.
	Pointers are registered into list for automated deletion.

	\param pConnectionHandler ConnectionHandler object for handling RPC calls over NET
	\param RPCType defines RPC protocol in use (defined in rpcprotocol.h)

*/
RpcStubClientSet::RpcStubClientSet(ConnectionHandler *pConnectionHandler,int RPCType)
{

	//---------------------------------------------------
	//	Make instances of each stub object, store pointer in list
	//---------------------------------------------------

	UserLogon=new RpcStub_UserLogon(pConnectionHandler,RPCType); m_StubList.append(dynamic_cast<RpcStub_UserLogon*>(UserLogon));
	ModuleLicense =new RpcStub_ModuleLicense(pConnectionHandler,RPCType); m_StubList.append(dynamic_cast<RpcStub_ModuleLicense*>(ModuleLicense));
	BusEventLog =new RpcStub_BusEventLog(pConnectionHandler,RPCType); m_StubList.append(dynamic_cast<RpcStub_BusEventLog*>(BusEventLog));
	AccessRights =new RpcStub_AccessRights(pConnectionHandler,RPCType); m_StubList.append(dynamic_cast<RpcStub_AccessRights*>(AccessRights));
	MainEntitySelector =new RpcStub_MainEntitySelector(pConnectionHandler,RPCType); m_StubList.append(dynamic_cast<RpcStub_MainEntitySelector*>(MainEntitySelector));
	CoreServices  =new RpcStub_CoreServices(pConnectionHandler,RPCType); m_StubList.append(dynamic_cast<RpcStub_CoreServices*>(CoreServices));
	BusContact  =new RpcStub_BusContact(pConnectionHandler,RPCType); m_StubList.append(dynamic_cast<RpcStub_BusContact*>(BusContact));
	Reports =new RpcStub_Reports(pConnectionHandler,RPCType); m_StubList.append(dynamic_cast<RpcStub_Reports*>(Reports));
	ClientSimpleORM = new RpcStub_ClientSimpleORM(pConnectionHandler,RPCType); m_StubList.append(dynamic_cast<RpcStub_ClientSimpleORM*>(ClientSimpleORM));
	BusAddressSchemas =new RpcStub_BusAddressSchemas(pConnectionHandler,RPCType); m_StubList.append(dynamic_cast<RpcStub_BusAddressSchemas*>(BusAddressSchemas));
	VoiceCallCenter = new RpcStub_VoiceCallCenter(pConnectionHandler,RPCType); m_StubList.append(dynamic_cast<RpcStub_VoiceCallCenter*>(VoiceCallCenter));
	BusEmail = new RpcStub_BusEmail(pConnectionHandler,RPCType); m_StubList.append(dynamic_cast<RpcStub_BusEmail*>(BusEmail));
	BusImport = new RpcStub_BusImport(pConnectionHandler,RPCType); m_StubList.append(dynamic_cast<RpcStub_BusImport*>(BusImport));
	StoredProjectLists = new RpcStub_StoredProjectLists(pConnectionHandler,RPCType); m_StubList.append(dynamic_cast<RpcStub_StoredProjectLists*>(StoredProjectLists));
	BusGridViews = new RpcStub_BusGridViews(pConnectionHandler,RPCType); m_StubList.append(dynamic_cast<RpcStub_BusGridViews*>(BusGridViews));
	BusNMRX = new RpcStub_BusNMRX(pConnectionHandler,RPCType); m_StubList.append(dynamic_cast<RpcStub_BusNMRX*>(BusNMRX));
	BusGroupTree = new RpcStub_BusGroupTree(pConnectionHandler,RPCType); m_StubList.append(dynamic_cast<RpcStub_BusGroupTree*>(BusGroupTree));
	BusDocuments = new RpcStub_BusDocuments(pConnectionHandler,RPCType); m_StubList.append(dynamic_cast<RpcStub_BusDocuments*>(BusDocuments));
	BusCommunication = new RpcStub_BusCommunication(pConnectionHandler,RPCType); m_StubList.append(dynamic_cast<RpcStub_BusCommunication*>(BusCommunication));
	ServerControl = new RpcStub_ServerControl(pConnectionHandler,RPCType); m_StubList.append(dynamic_cast<RpcStub_ServerControl*>(ServerControl));
	BusProject = new RpcStub_BusProject(pConnectionHandler,RPCType); m_StubList.append(dynamic_cast<RpcStub_BusProject*>(BusProject));
	BusPerson = new RpcStub_BusPerson(pConnectionHandler,RPCType); m_StubList.append(dynamic_cast<RpcStub_BusPerson*>(BusPerson));
	BusSms  = new RpcStub_BusSms(pConnectionHandler,RPCType); m_StubList.append(dynamic_cast<RpcStub_BusSms*>(BusSms));
	BusCalendar  = new RpcStub_BusCalendar(pConnectionHandler,RPCType); m_StubList.append(dynamic_cast<RpcStub_BusCalendar*>(BusCalendar));
	BusCustomFields  = new RpcStub_BusCustomFields(pConnectionHandler,RPCType); m_StubList.append(dynamic_cast<RpcStub_BusCustomFields*>(BusCustomFields));

	//check pointers (only in debug):
#ifndef QT_NO_DEBUG
	CheckSet();
#endif

}



/*!
	Destructor:
	Deletes all reserverd memory
*/
RpcStubClientSet::~RpcStubClientSet()
{

	//release memory:
for(int i=0;i<m_StubList.size();++i)
	delete m_StubList.at(i);

}


//check for initialization
void RpcStubClientSet::CheckSet()
{
		for(int i=0;i<m_StubList.size();++i)
		{
			if(m_StubList.at(i)==NULL)
				Q_ASSERT_X(false,"INIT_SRV","NULL Pointers detected!");
		
		}
}
