#ifndef USERSTORAGEHTTPCLIENT_H
#define USERSTORAGEHTTPCLIENT_H

#include "trans/trans/httpclientconnection.h"
#include "trans/trans/httpclientconnectionsettings.h"
#include "common/common/authenticator.h"
#include "common/common/threadsync.h"

class UserStorageHTTPClient : public HTTPClientThread
{
	Q_OBJECT

public:
	UserStorageHTTPClient();
	~UserStorageHTTPClient();

	void SetConnectionSettings(const HTTPClientConnectionSettings& pConnection, const Authenticator &SessionCache);

	void DownLoadBackup(Status& pStatus,QString strFile,bool bOverWrite=false);
	void UpLoadBackup(Status& pStatus,QString strFile,bool bOverWrite=false);
	void DownLoadFile(Status& pStatus,QString strFile,bool bOverWrite=false);
	void UpLoadFile(Status& pStatus,QString strFile,bool bOverWrite=false);
	void ClearFromUserStorage(Status& pStatus,QString strFile="");

public slots:
	void OnExecDownLoadBackup(QString strFile,bool bOverWrite=false);
	void OnExecUpLoadBackup(QString strFile,bool bOverWrite=false);
	void OnExecDownLoadFile(QString strFile,bool bOverWrite=false);
	void OnExecUpLoadFile(QString strFile,bool bOverWrite=false);
	void OnExecClearFromUserStorage(QString strFile="");
	void OnCancelOperation();

signals:
	void OperationEnded(int nErrCode, QString strErrorRawText);

private:
	void Connect(Status& pStatus);
	void Disconnect(Status& pStatus);
	void SetRequestHeader(HTTPRequest &request);

	Authenticator m_SessionCache;
	QString m_strHost;
	QString m_strClientTag;
	QString m_strAuthCookieString;
};



//Execute download/upload in separate client thread:
//set settings and start thread by start thread
//use functions Exec...Abort, catch signals: SocketOperationInProgress and SignalOperationEnded

class UserStorageHTTPClientThread : public QThread
{
	Q_OBJECT

public:
	UserStorageHTTPClientThread();
	~UserStorageHTTPClientThread();
	 
	void StartThread(const HTTPClientConnectionSettings& pConnection, const Authenticator &SessionCache);

	void ExecDownLoadBackup(QString strFile,bool bOverWrite=false);
	void ExecUpLoadBackup(QString strFile,bool bOverWrite=false);
	void ExecDownLoadFile(QString strFile,bool bOverWrite=false);
	void ExecUpLoadFile(QString strFile,bool bOverWrite=false);
	void ExecClearFromUserStorage(QString strFile="");
	void AbortOperation();

signals:
	//intern
	void SignalExecDownLoadBackup(QString strFile,bool bOverWrite);
	void SignalExecUpLoadBackup(QString strFile,bool bOverWrite);
	void SignalExecDownLoadFile(QString strFile,bool bOverWrite);
	void SignalExecUpLoadFile(QString strFile,bool bOverWrite);
	void SignalExecClearFromUserStorage(QString strFile);
	void SignalDestroy();
	void SignalAbortOperation();

	//use this:
	void SocketOperationInProgress(int,qint64,qint64);
	void SignalOperationEnded(int, QString);


protected:
	void run();

	Authenticator m_SessionCache;
	HTTPClientConnectionSettings m_ConnSettings;
	ThreadSynchronizer m_ThreadSync;///< for syncing between mastr & slave thread

};


#endif // USERSTORAGEHTTPCLIENT_H
