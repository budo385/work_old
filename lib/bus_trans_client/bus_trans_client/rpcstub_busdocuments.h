#ifndef RPCSTUB_BUSDOCUMENTS_H__
#define RPCSTUB_BUSDOCUMENTS_H__

#include "common/common/status.h"
#include "bus_interface/bus_interface/interface_busdocuments.h"
#include "bus_trans_client/bus_trans_client/connectionhandler.h"
#include "trans/trans/rpcstubmessagehandler.h"

class RpcStub_BusDocuments: public Interface_BusDocuments, public RpcStub
{
public:
	RpcStub_BusDocuments(ConnectionHandler *pConnectionHandler,int RPCType):
		RpcStub(RPCType){m_ConnectionHandler=pConnectionHandler;}

	void ReadUserPaths(Status &Ret_pStatus, int nPersonID,DbRecordSet &Ret_Data);
	void ReadUserPathsForApplication(Status &Ret_pStatus, int nApplicationID,DbRecordSet &Ret_Data);
	void ReadTemplates(Status &Ret_pStatus, int nPersonID,DbRecordSet &Ret_Data);
	void Write(Status &Ret_pStatus, DbRecordSet &RetOut_Data, DbRecordSet &RetOut_ProjectLink,DbRecordSet &RetOut_ContactLink,DbRecordSet &RetOut_ScheduleTask,DbRecordSet &RetOut_DocumentRevision,QString strLockRes,DbRecordSet &RetOut_UAR,DbRecordSet &RetOut_GAR);
	void Read(Status &Ret_pStatus, int nDocumentID, DbRecordSet &Ret_Data, DbRecordSet &Ret_ProjectLink,DbRecordSet &Ret_ContactLink,DbRecordSet &Ret_ScheduleTask,DbRecordSet &Ret_Revisions, DbRecordSet &Ret_CheckOutInfo,DbRecordSet &Ret_UAR,DbRecordSet &Ret_GAR);
	void DeleteDocument(Status &Ret_pStatus, int nDocumentID,QString strLockRes="");
	void WriteApplication(Status &Ret_pStatus, DbRecordSet &RetOut_Data,QString strLockRes);
	void ReadNoteDocument(Status &Ret_pStatus, int nDocumentID, QString &Ret_strData);
	void ReloadRevisions(Status &Ret_pStatus, int nDocumentID, DbRecordSet &Ret_CheckOutInfo, DbRecordSet &Ret_Revisions);
	void CheckIn(Status &Ret_pStatus, int nDocumentID, DbRecordSet &RetOut_Destroy_DocumentRevision,bool bOverWrite,DbRecordSet &Ret_CheckOutInfo, bool bSkipLock,int nSize);
	void CheckOut(Status &Ret_pStatus, int nDocumentID, int nCheckOutPersonID,DbRecordSet &Ret_DocumentRevision,bool bReadOnly,DbRecordSet &Ret_CheckOutInfo,QString strCheckOutPath,bool bSkipLock,int nRevisionID);
	void CheckOutToUserStorage(Status &Ret_pStatus, int nDocumentID, int nCheckOutPersonID,bool bReadOnly,DbRecordSet &Ret_DocumentRevision,DbRecordSet &Ret_CheckOutInfo,QString strCheckOutPath="",bool bSkipLock=false,int nRevisionID=-1);
	void CheckInFromUserStorage(Status &Ret_pStatus, int nDocumentID, int nCheckInPersonID,bool bOverWrite,DbRecordSet &Ret_DocumentRevision,DbRecordSet &Ret_CheckOutInfo, bool bSkipLock=false,QString strTag="", QString strSubDirectory="");
	void CheckInFromEmailAttachment(Status &Ret_pStatus, int nAttachmentID, int nOperation, int nProjectID, int nContactID,DbRecordSet &RetOut_DocumentIDs);
	void CancelCheckOut(Status &Ret_pStatus, int nDocumentID, int nCheckOutPersonID,bool bReadOnly);
	void CheckOutRevision(Status &Ret_pStatus, int nRevisionID, DbRecordSet &Ret_DocumentRevision);
	void UpdateRevision(Status &Ret_pStatus, int nRevisionID, QString strRevisionTag);
	void DeleteRevision(Status &Ret_pStatus, int nDocumentID,int nRevisionID, bool bLock);
	void IsCheckedOut(Status &Ret_pStatus, int nDocumentID, int &Ret_nCheckOutPersonID,QString &Ret_strCheckedOutPath);
	void GetDocumentFileNameAndSize(Status &Ret_pStatus, int nDocumentID, QString &Ret_strFileName, int &Ret_nSize,int nRevisionID=-1);
	void GetDocumentProjectAndContact(Status &Ret_pStatus, int nDocumentID, int &Ret_nProjectID,int &Ret_nContactID);
	void SetCheckOutPath(Status &Ret_pStatus, int nDocumentID, QString strPath);

private:
	ConnectionHandler *m_ConnectionHandler; ///< connection handler
};

#endif	// RPCSTUB_BUSDOCUMENTS_H__
