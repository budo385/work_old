#include "rpcstub_storedprojectlists.h"

void RpcStub_StoredProjectLists::GetListNames(Status &Ret_pStatus, DbRecordSet &Ret_Data)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("StoredProjectLists.GetListNames",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&Ret_Data,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_StoredProjectLists::DeleteList(Status &Ret_pStatus, int nListID)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&nListID,"nListID");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("StoredProjectLists.DeleteList",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
}

void RpcStub_StoredProjectLists::CreateList(Status &Ret_pStatus, QString strListName, DbRecordSet &lstData, int &RetOut_nListID, bool bIsSelection, QString strSelectionXML, bool bIsFilter, QString strFilterXML)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&strListName,"strListName");
	msg.msg_in->AddParameter(&lstData,"lstData");
	msg.msg_in->AddParameter(&RetOut_nListID,"RetOut_nListID");
	msg.msg_in->AddParameter(&bIsSelection,"bIsSelection");
	msg.msg_in->AddParameter(&strSelectionXML,"strSelectionXML");
	msg.msg_in->AddParameter(&bIsFilter,"bIsFilter");
	msg.msg_in->AddParameter(&strFilterXML,"strFilterXML");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("StoredProjectLists.CreateList",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&RetOut_nListID,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_StoredProjectLists::GetListData(Status &Ret_pStatus, int nListID, DbRecordSet &Ret_lstData, bool &Ret_bIsSelection, QString &Ret_strSelectionXML, bool &Ret_bIsFilter, QString &Ret_strFilterXML, bool bGetDescOnly)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&nListID,"nListID");
	msg.msg_in->AddParameter(&bGetDescOnly,"bGetDescOnly");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("StoredProjectLists.GetListData",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&Ret_lstData,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(1,&Ret_bIsSelection,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(2,&Ret_strSelectionXML,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(3,&Ret_bIsFilter,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(4,&Ret_strFilterXML,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_StoredProjectLists::SetListData(Status &Ret_pStatus, int nListID, DbRecordSet &lstData, bool bIsSelection, QString strSelectionXML, bool bIsFilter, QString strFilterXML)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&nListID,"nListID");
	msg.msg_in->AddParameter(&lstData,"lstData");
	msg.msg_in->AddParameter(&bIsSelection,"bIsSelection");
	msg.msg_in->AddParameter(&strSelectionXML,"strSelectionXML");
	msg.msg_in->AddParameter(&bIsFilter,"bIsFilter");
	msg.msg_in->AddParameter(&strFilterXML,"strFilterXML");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("StoredProjectLists.SetListData",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
}

void RpcStub_StoredProjectLists::GetListDataLevelByLevel(Status &Ret_pStatus, int nListID, DbRecordSet &Ret_lstData, QString strParentProjectCode, int nLevelsDeep)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&nListID,"nListID");
	msg.msg_in->AddParameter(&strParentProjectCode,"strParentProjectCode");
	msg.msg_in->AddParameter(&nLevelsDeep,"nLevelsDeep");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("StoredProjectLists.GetListDataLevelByLevel",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&Ret_lstData,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

