#ifndef RPCSTUB_BUSIMPORT_H__
#define RPCSTUB_BUSIMPORT_H__

#include "common/common/status.h"
#include "bus_interface/bus_interface/interface_busimport.h"
#include "bus_trans_client/bus_trans_client/connectionhandler.h"
#include "trans/trans/rpcstubmessagehandler.h"

class RpcStub_BusImport: public Interface_BusImport, public RpcStub
{
public:
	RpcStub_BusImport(ConnectionHandler *pConnectionHandler,int RPCType):
		RpcStub(RPCType){m_ConnectionHandler=pConnectionHandler;}

	void ImportProjects(Status &Ret_pStatus, DbRecordSet &RetOut_Data,int nOperation);
	void ImportPersons(Status &Ret_pStatus, DbRecordSet &RetOut_Data,int nOperation, bool bCreateUsers, bool bCreateContacts, int nRoleID);
	void ImportContacts(Status &Ret_pStatus, DbRecordSet &RetOut_Data, DbRecordSet &RetOut_ErrRow, int nOperation, bool bCreateUsers);
	void ImportUserContactRelationships(Status &Ret_pStatus, DbRecordSet &RetOut_Data);
	void ImportContactContactRelationships(Status &Ret_pStatus, DbRecordSet &RetOut_Data);
	void GetContactContactRelationships(Status &Ret_pStatus, DbRecordSet &RetOut_Data);
	void ImportContactCustomData(Status &Ret_pStatus, QString strCustomFieldName, int nTableID, DbRecordSet &RetOut_Data);
	void ProcessProjects_Import(Status &Ret_pStatus, QString strFileExt, QString strFilePrefix, QString strFileExtOfProcessed, QString strDirPath, DbRecordSet &Ret_ProcessedFiles, DbRecordSet &Ret_pErrors);
	void ProcessProjects_Export(Status &Ret_pStatus, QString strFileExt, QString strFilePrefix, QString strFileExtOfProcessed, QString strDirPath, int nExportLasN, bool bExportAddDateTimeOnExportFile, DbRecordSet &Ret_ProcessedFiles, DbRecordSet &Ret_pErrors, DbRecordSet &ProjectsForExport, bool bClientSideExport, QByteArray &Ret_File, bool bUtf8);
	void ProcessUsers_Import(Status &Ret_pStatus, QString strFileExt, QString strFilePrefix, QString strFileExtOfProcessed, QString strDirPath, DbRecordSet &Ret_ProcessedFiles, DbRecordSet &Ret_pErrors, int nRoleID);
	void ProcessUsers_Export(Status &Ret_pStatus, QString strFileExt, QString strFilePrefix, QString strFileExtOfProcessed, QString strDirPath, int nExportLasN, bool bExportAddDateTimeOnExportFile, DbRecordSet &Ret_ProcessedFiles, DbRecordSet &Ret_pErrors, DbRecordSet &UsersForExport, bool bClientSideExport, QByteArray &Ret_File, bool bUtf8);
	void ProcessDebtors_Export(Status &Ret_pStatus, QString strFileExt, QString strFilePrefix, QString strFileExtOfProcessed, QString strDirPath, int nExportLasN, bool bExportAddDateTimeOnExportFile, DbRecordSet &Ret_ProcessedFiles, DbRecordSet &Ret_pErrors, int nAddressTypeFilter, bool bUtf8);
	void ProcessContacts_Import(Status &Ret_pStatus, QString strFileExt, QString strFilePrefix, QString strFileExtOfProcessed, QString strDirPath, DbRecordSet &Ret_ProcessedFiles, DbRecordSet &Ret_pErrors);
	void ProcessContacts_Export(Status &Ret_pStatus, QString strFileExt, QString strFilePrefix, QString strFileExtOfProcessed, QString strDirPath, int nExportLasN, bool bExportAddDateTimeOnExportFile, DbRecordSet &Ret_ProcessedFiles, DbRecordSet &Ret_pErrors, DbRecordSet &UsersForExport, bool bClientSideExport, QByteArray &Ret_File, bool bUtf8, bool bSokratesHdr, bool bColumnTitles, bool bOutlookExport, QString strLang, QByteArray datHedFile);
	void ProcessContactContactRelations_Export(Status &Ret_pStatus, QString strFileExt, QString strFilePrefix, QString strFileExtOfProcessed, QString strDirPath, int nExportLasN, bool bExportAddDateTimeOnExportFile, DbRecordSet &Ret_ProcessedFiles, DbRecordSet &Ret_pErrors, DbRecordSet &UsersForExport, bool bClientSideExport, QByteArray &Ret_File, bool bUtf8, bool bSokratesHdr, bool bColumnTitles, bool bOutlookExport, QString strLang, QByteArray datHedFile);
	void ProcessStoredProjectList_Import(Status &Ret_pStatus, QString strFileExt, QString strFilePrefix, QString strFileExtOfProcessed, QString strDirPath, DbRecordSet &Ret_ProcessedFiles, DbRecordSet &Ret_pErrors);

private:
	ConnectionHandler *m_ConnectionHandler; ///< connection handler
};

#endif	// RPCSTUB_BUSIMPORT_H__
