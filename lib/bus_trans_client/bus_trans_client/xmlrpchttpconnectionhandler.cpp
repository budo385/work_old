#include "xmlrpchttpconnectionhandler.h"
#include "xmlrpchttpclient.h"
#include "common/common/threadid.h"

/*!
	Constructor: init XML-RPC type of connection
	\param pConnection connection settings
*/

XmlRpcHTTPConnectionHandler::XmlRpcHTTPConnectionHandler()
{

	//make skeleton object:
	SkeletonSrvMsgSet= new RpcSkeletonServerMessageSet(RPC_PROTOCOL_TYPE_XML_RPC);


	//init HTTPClient
	m_HttpClient= new XmlRpcHTPPClient(SkeletonSrvMsgSet);

	//init base class:
	InitConnection(RPC_PROTOCOL_TYPE_XML_RPC);
}


//hope that base class destructor is automatically called
XmlRpcHTTPConnectionHandler::~XmlRpcHTTPConnectionHandler()
{
	if (SkeletonSrvMsgSet!=NULL)
		delete SkeletonSrvMsgSet;
}

void XmlRpcHTTPConnectionHandler::StartThreadConnection(Status& pStatus,int nConnectionThreadID)
{
	if (nConnectionThreadID==-1)
		nConnectionThreadID=ThreadIdentificator::GetCurrentThreadID();
	Q_ASSERT(m_nMainThreadID!=nConnectionThreadID); //can not start thread from main
	ConnectionHandler *pNewConn=m_hshConnections.value(nConnectionThreadID,NULL);
	if (pNewConn==NULL)
	{
		pNewConn= new XmlRpcHTTPConnectionHandler(); //create new if does not exists
		qDebug()<<"new connection for thread: "<<nConnectionThreadID<< " created!";
	}

	if (pNewConn->IsLogged())
		pNewConn->Logout(pStatus);
	pStatus.setError(0); //ignore log out error
	HTTPClientConnectionSettings set = Priv_GetRpcHttpClient()->GetConnectionSettings();
	pNewConn->SetConnectionSettings(set);
	pNewConn->Login(pStatus,m_SessionCache.m_strUserName,m_SessionCache.m_strPassword,m_SessionCache.m_strModuleCode,m_SessionCache.m_strMLIID,Priv_GetSessionID());
	if (!pStatus.IsOK())
		 return;

	qDebug()<<"new connection for thread: "<<nConnectionThreadID<< " logged and ready for use!";

	QWriteLocker lock(&m_hshConnectionsLock);
	Q_ASSERT(!m_hshConnections.contains(nConnectionThreadID)); //if connection is inside cache already--error
	m_hshConnections[nConnectionThreadID]=pNewConn;

	//qDebug()<<"(+) new conn:"<<ThreadIdentificator::GetCurrentThreadID()<<"-"<<nConnectionThreadID<<" RPC m_Handler="<<pNewConn;
}
void XmlRpcHTTPConnectionHandler::CloseThreadConnection(Status& pStatus,int nConnectionThreadID)
{
	if (nConnectionThreadID==-1)
		nConnectionThreadID=ThreadIdentificator::GetCurrentThreadID();

	//qDebug()<<"delete connection for thread: "<<nConnectionThreadID;

	Q_ASSERT(m_nMainThreadID!=nConnectionThreadID); //can not start thread from main
	QWriteLocker lock(&m_hshConnectionsLock);
	ConnectionHandler *p=m_hshConnections.value(nConnectionThreadID,NULL);
	if (p==NULL)
		return;

	//ignore in real life:
	p->Logout(pStatus);
	//Q_ASSERT(pStatus.IsOK());
	//qDebug()<<"connection for thread: "<<nConnectionThreadID<< " log out!"<< " err:"<<pStatus.getErrorText();

	p->GetRpcHttpClient()->Disconnect(pStatus);
	//Q_ASSERT(pStatus.IsOK());
	//qDebug()<<"connection for thread: "<<nConnectionThreadID<< " diconnect!"<< " err:"<<pStatus.getErrorText();

	//qDebug()<<"(-) remove conn:"<<ThreadIdentificator::GetCurrentThreadID()<<"-"<<nConnectionThreadID<<" RPC m_Handler="<<p;
	delete p;
	m_hshConnections.remove(nConnectionThreadID);


}
