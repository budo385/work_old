#include "rpcstub_buscalendar.h"

void RpcStub_BusCalendar::SaveCalFilterView(Status &Ret_pStatus, int &Ret_nViewID, DbRecordSet recView, DbRecordSet recCalEntities, DbRecordSet recCalTypeIDs)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&recView,"recView");
	msg.msg_in->AddParameter(&recCalEntities,"recCalEntities");
	msg.msg_in->AddParameter(&recCalTypeIDs,"recCalTypeIDs");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusCalendar.SaveCalFilterView",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&Ret_nViewID,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusCalendar::GetCalFilterViews(Status &Ret_pStatus, DbRecordSet &RetOut_recViews, int nPersonID, int nGridType )
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&RetOut_recViews,"RetOut_recViews");
	msg.msg_in->AddParameter(&nPersonID,"nPersonID");
	msg.msg_in->AddParameter(&nGridType,"nGridType");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusCalendar.GetCalFilterViews",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&RetOut_recViews,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusCalendar::GetCalFilterViewsWithEntitesAndTypes(Status &Ret_pStatus, DbRecordSet &RetOut_recViews, DbRecordSet &RetOut_recCalEntities, DbRecordSet &RetOut_recCalTypeIDs, int nPersonID, int nGridType )
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&RetOut_recViews,"RetOut_recViews");
	msg.msg_in->AddParameter(&RetOut_recCalEntities,"RetOut_recCalEntities");
	msg.msg_in->AddParameter(&RetOut_recCalTypeIDs,"RetOut_recCalTypeIDs");
	msg.msg_in->AddParameter(&nPersonID,"nPersonID");
	msg.msg_in->AddParameter(&nGridType,"nGridType");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusCalendar.GetCalFilterViewsWithEntitesAndTypes",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&RetOut_recViews,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(1,&RetOut_recCalEntities,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(2,&RetOut_recCalTypeIDs,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusCalendar::GetCalFilterView(Status &Ret_pStatus, DbRecordSet &RetOut_recViewsData, DbRecordSet &RetOut_recCalEntities, DbRecordSet &RetOut_recCalTypeIDs, int nViewID)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&RetOut_recViewsData,"RetOut_recViewsData");
	msg.msg_in->AddParameter(&RetOut_recCalEntities,"RetOut_recCalEntities");
	msg.msg_in->AddParameter(&RetOut_recCalTypeIDs,"RetOut_recCalTypeIDs");
	msg.msg_in->AddParameter(&nViewID,"nViewID");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusCalendar.GetCalFilterView",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&RetOut_recViewsData,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(1,&RetOut_recCalEntities,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(2,&RetOut_recCalTypeIDs,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusCalendar::DeleteCalFilterView(Status &Ret_pStatus, int nViewID)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&nViewID,"nViewID");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusCalendar.DeleteCalFilterView",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
}

void RpcStub_BusCalendar::WriteEvent(Status &Ret_pStatus, DbRecordSet &RetOut_Data, DbRecordSet &RetOut_Parts,DbRecordSet &RetOut_ScheduleTask,QString strLockRes,DbRecordSet &RetOut_UAR,DbRecordSet &RetOut_GAR)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&RetOut_Data,"RetOut_Data");
	msg.msg_in->AddParameter(&RetOut_Parts,"RetOut_Parts");
	msg.msg_in->AddParameter(&RetOut_ScheduleTask,"RetOut_ScheduleTask");
	msg.msg_in->AddParameter(&strLockRes,"strLockRes");
	msg.msg_in->AddParameter(&RetOut_UAR,"RetOut_UAR");
	msg.msg_in->AddParameter(&RetOut_GAR,"RetOut_GAR");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusCalendar.WriteEvent",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&RetOut_Data,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(1,&RetOut_Parts,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(2,&RetOut_ScheduleTask,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(3,&RetOut_UAR,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(4,&RetOut_GAR,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusCalendar::ReadEvent(Status &Ret_pStatus, int nCalEventID, DbRecordSet &Ret_Data, DbRecordSet &Ret_Parts, DbRecordSet &Ret_ScheduleTask,DbRecordSet &Ret_UAR,DbRecordSet &Ret_GAR)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&nCalEventID,"nCalEventID");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusCalendar.ReadEvent",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&Ret_Data,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(1,&Ret_Parts,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(2,&Ret_ScheduleTask,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(3,&Ret_UAR,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(4,&Ret_GAR,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusCalendar::ReadTemplates(Status &Ret_pStatus, DbRecordSet &Ret_Data)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusCalendar.ReadTemplates",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&Ret_Data,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusCalendar::ReadOnePart(Status &Ret_pStatus, int nPartID,DbRecordSet &RetOut_RowPart,DbRecordSet &RetOut_RowActiveOption)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&nPartID,"nPartID");
	msg.msg_in->AddParameter(&RetOut_RowPart,"RetOut_RowPart");
	msg.msg_in->AddParameter(&RetOut_RowActiveOption,"RetOut_RowActiveOption");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusCalendar.ReadOnePart",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&RetOut_RowPart,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(1,&RetOut_RowActiveOption,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusCalendar::ReadOneEvent(Status &Ret_pStatus, int nBCEV_ID, DbRecordSet &Ret_EventParts, DbRecordSet &Ret_Contacts, DbRecordSet &Ret_Resources, DbRecordSet &Ret_Projects, DbRecordSet &Ret_Breaks)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&nBCEV_ID,"nBCEV_ID");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusCalendar.ReadOneEvent",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&Ret_EventParts,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(1,&Ret_Contacts,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(2,&Ret_Resources,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(3,&Ret_Projects,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(4,&Ret_Breaks,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusCalendar::ReadEventPartsCountForEntities(Status &Ret_pStatus, DbRecordSet &RetOut_recEntities, DbRecordSet recFilter, QDateTime datStartDate, QDateTime datEndDate)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&RetOut_recEntities,"RetOut_recEntities");
	msg.msg_in->AddParameter(&recFilter,"recFilter");
	msg.msg_in->AddParameter(&datStartDate,"datStartDate");
	msg.msg_in->AddParameter(&datEndDate,"datEndDate");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusCalendar.ReadEventPartsCountForEntities",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&RetOut_recEntities,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusCalendar::ReadEventParts(Status &Ret_pStatus, DbRecordSet recFilter, int nEntityType, int nEntityID, QDateTime datStartDate, QDateTime datEndDate, DbRecordSet &Ret_EventParts, DbRecordSet &Ret_Contacts, DbRecordSet &Ret_Resources, DbRecordSet &Ret_Projects, DbRecordSet &Ret_Breaks)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&recFilter,"recFilter");
	msg.msg_in->AddParameter(&nEntityType,"nEntityType");
	msg.msg_in->AddParameter(&nEntityID,"nEntityID");
	msg.msg_in->AddParameter(&datStartDate,"datStartDate");
	msg.msg_in->AddParameter(&datEndDate,"datEndDate");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusCalendar.ReadEventParts",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&Ret_EventParts,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(1,&Ret_Contacts,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(2,&Ret_Resources,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(3,&Ret_Projects,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(4,&Ret_Breaks,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusCalendar::ReadEventPartsForInsert(Status &Ret_pStatus, DbRecordSet recEntities, QDateTime datStartDate, QDateTime datEndDate, DbRecordSet &Ret_EventParts, DbRecordSet &Ret_Contacts, DbRecordSet &Ret_Resources, DbRecordSet &Ret_Projects, DbRecordSet &Ret_Breaks)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&recEntities,"recEntities");
	msg.msg_in->AddParameter(&datStartDate,"datStartDate");
	msg.msg_in->AddParameter(&datEndDate,"datEndDate");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusCalendar.ReadEventPartsForInsert",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&Ret_EventParts,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(1,&Ret_Contacts,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(2,&Ret_Resources,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(3,&Ret_Projects,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(4,&Ret_Breaks,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusCalendar::ModifyEventDateRange(Status &Ret_pStatus, int nCalEventPartID, int nBCOL_ID, QDateTime datFrom, QDateTime datTo)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&nCalEventPartID,"nCalEventPartID");
	msg.msg_in->AddParameter(&nBCOL_ID,"nBCOL_ID");
	msg.msg_in->AddParameter(&datFrom,"datFrom");
	msg.msg_in->AddParameter(&datTo,"datTo");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusCalendar.ModifyEventDateRange",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
}

void RpcStub_BusCalendar::DeleteEventPart(Status &Ret_pStatus, int nCalEventPartID, int nBCOL_ID, int nBCEV_ID)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&nCalEventPartID,"nCalEventPartID");
	msg.msg_in->AddParameter(&nBCOL_ID,"nBCOL_ID");
	msg.msg_in->AddParameter(&nBCEV_ID,"nBCEV_ID");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusCalendar.DeleteEventPart",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
}

void RpcStub_BusCalendar::DeleteEventParts(Status &Ret_pStatus, DbRecordSet recDeleteItemsData)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&recDeleteItemsData,"recDeleteItemsData");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusCalendar.DeleteEventParts",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
}

void RpcStub_BusCalendar::ReadReservation(Status &Ret_pStatus, int nEntityType, int nEntityID, QDateTime datStartDate, QDateTime datEndDate, DbRecordSet &Ret_Reservations)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&nEntityType,"nEntityType");
	msg.msg_in->AddParameter(&nEntityID,"nEntityID");
	msg.msg_in->AddParameter(&datStartDate,"datStartDate");
	msg.msg_in->AddParameter(&datEndDate,"datEndDate");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusCalendar.ReadReservation",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&Ret_Reservations,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusCalendar::ReadEventPartEntitesForOneEntity(Status &Ret_pStatus, DbRecordSet recFilter, int nEntityType, int nEntityID, QDateTime datStartDate, QDateTime datEndDate, DbRecordSet &Ret_Contacts, DbRecordSet &Ret_Resources, DbRecordSet &Ret_Projects)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&recFilter,"recFilter");
	msg.msg_in->AddParameter(&nEntityType,"nEntityType");
	msg.msg_in->AddParameter(&nEntityID,"nEntityID");
	msg.msg_in->AddParameter(&datStartDate,"datStartDate");
	msg.msg_in->AddParameter(&datEndDate,"datEndDate");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusCalendar.ReadEventPartEntitesForOneEntity",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&Ret_Contacts,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(1,&Ret_Resources,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(2,&Ret_Projects,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusCalendar::ModifyEventPresence(Status &Ret_pStatus, QDateTime datFrom, QDateTime datTo, DbRecordSet recFilter, DbRecordSet lstAssignedEntities, int nPresenceTypeID)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&datFrom,"datFrom");
	msg.msg_in->AddParameter(&datTo,"datTo");
	msg.msg_in->AddParameter(&recFilter,"recFilter");
	msg.msg_in->AddParameter(&lstAssignedEntities,"lstAssignedEntities");
	msg.msg_in->AddParameter(&nPresenceTypeID,"nPresenceTypeID");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusCalendar.ModifyEventPresence",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
}

void RpcStub_BusCalendar::SetInviteStatus(Status &Ret_pStatus, int nCalEventID,int nPartID,int nInviteID,int nContactId_ConfirmedBy, int nPersonID_ConfirmedBy, int nStatus,int nOwnerID,bool bNotifyOwner)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&nCalEventID,"nCalEventID");
	msg.msg_in->AddParameter(&nPartID,"nPartID");
	msg.msg_in->AddParameter(&nInviteID,"nInviteID");
	msg.msg_in->AddParameter(&nContactId_ConfirmedBy,"nContactId_ConfirmedBy");
	msg.msg_in->AddParameter(&nPersonID_ConfirmedBy,"nPersonID_ConfirmedBy");
	msg.msg_in->AddParameter(&nStatus,"nStatus");
	msg.msg_in->AddParameter(&nOwnerID,"nOwnerID");
	msg.msg_in->AddParameter(&bNotifyOwner,"bNotifyOwner");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusCalendar.SetInviteStatus",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
}

void RpcStub_BusCalendar::SendInviteBySokrates(Status &Ret_pStatus, int nCalEventID,DbRecordSet &DataInvite)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&nCalEventID,"nCalEventID");
	msg.msg_in->AddParameter(&DataInvite,"DataInvite");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusCalendar.SendInviteBySokrates",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
}

void RpcStub_BusCalendar::PrepareInviteRecordsForSendByEmail(Status &Ret_pStatus, DbRecordSet &RetOut_DataInvite, int nDefaultEmailTemplateID)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&RetOut_DataInvite,"RetOut_DataInvite");
	msg.msg_in->AddParameter(&nDefaultEmailTemplateID,"nDefaultEmailTemplateID");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusCalendar.PrepareInviteRecordsForSendByEmail",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&RetOut_DataInvite,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusCalendar::WriteInviteRecordsAfterSend(Status &Ret_pStatus, DbRecordSet &DataInvite,bool bOnlyUpdateSentStatus)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&DataInvite,"DataInvite");
	msg.msg_in->AddParameter(&bOnlyUpdateSentStatus,"bOnlyUpdateSentStatus");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusCalendar.WriteInviteRecordsAfterSend",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
}

void RpcStub_BusCalendar::WriteInviteReplyStatus(Status &Ret_pStatus, DbRecordSet &RetOut_DataInvite, int nTypeOfMessage)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&RetOut_DataInvite,"RetOut_DataInvite");
	msg.msg_in->AddParameter(&nTypeOfMessage,"nTypeOfMessage");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusCalendar.WriteInviteReplyStatus",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&RetOut_DataInvite,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusCalendar::WriteFromOutlook(Status &Ret_pStatus, DbRecordSet &RetOut_Data, bool bSkipExisting,DbRecordSet &Ret_lstStatuses)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&RetOut_Data,"RetOut_Data");
	msg.msg_in->AddParameter(&bSkipExisting,"bSkipExisting");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusCalendar.WriteFromOutlook",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&RetOut_Data,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(1,&Ret_lstStatuses,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusCalendar::ReadForOutlook(Status &Ret_pStatus, DbRecordSet &Ret_Data, QDate datFrom, QDate datTo)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&datFrom,"datFrom");
	msg.msg_in->AddParameter(&datTo,"datTo");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusCalendar.ReadForOutlook",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&Ret_Data,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusCalendar::GetEventPartContacts(Status &Ret_pStatus, DbRecordSet &recParts, DbRecordSet &Ret_Contacts)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&recParts,"recParts");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusCalendar.GetEventPartContacts",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&Ret_Contacts,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusCalendar::GetEventPartResources(Status &Ret_pStatus, DbRecordSet &recParts, DbRecordSet &Ret_Resources)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&recParts,"recParts");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusCalendar.GetEventPartResources",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&Ret_Resources,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusCalendar::GetEventPartProjects(Status &Ret_pStatus, DbRecordSet &recParts, DbRecordSet &Ret_Projects)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&recParts,"recParts");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusCalendar.GetEventPartProjects",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&Ret_Projects,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusCalendar::GetEventPartBreaks(Status &Ret_pStatus, DbRecordSet &recParts, DbRecordSet &Ret_Breaks)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&recParts,"recParts");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusCalendar.GetEventPartBreaks",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&Ret_Breaks,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

