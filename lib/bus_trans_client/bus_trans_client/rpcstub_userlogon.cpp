#include "rpcstub_userlogon.h"


/*!
	Client side implementation of Login

	\param Ret_Status			- if err occur, returned here
	\param strUsername			- usr name
	\param strPassword			- pass
	\param strModuleCode		- client module code
	\param strMLIID				- MLIID
	\param strParentSession		- only for thin client: opens more then 1 client connection
*/
void RpcStub_UserLogon::Login(Status& Ret_Status, QString strUsername, QString strPassword,QString strModuleCode, QString strMLIID, QString strParentSession, int nClientTimeZoneOffsetMinutes)
{
	m_ConnectionHandler->Login(Ret_Status,strUsername,strPassword,strModuleCode,strMLIID,strParentSession,nClientTimeZoneOffsetMinutes);
}



/*!
	Client side implementation of Logout

	\param Ret_Status			- if err occur, returned here
	\param pDbConn				- db conn
*/
void RpcStub_UserLogon::Logout(Status& Ret_Status)
{
	m_ConnectionHandler->Logout(Ret_Status);
}



void RpcStub_UserLogon::ChangePassword(Status& Ret_Status,QString strUsername, QByteArray strAuthToken,QByteArray strClientNonce,QByteArray newPass,QString strNewUserName)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&strUsername,"strUsername");
	msg.msg_in->AddParameter(&strAuthToken,"strAuthToken");
	msg.msg_in->AddParameter(&strClientNonce,"strClientNonce");
	msg.msg_in->AddParameter(&newPass,"newPass");
	msg.msg_in->AddParameter(&strNewUserName,"strNewUserName");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("UserLogon.ChangePassword",Ret_Status);
	if (!Ret_Status.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_Status,msg);
	if (!Ret_Status.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_Status);

	//if not OK, return
	if (!Ret_Status.IsOK()) return;

	//extract return parameters from msg_out:
}

