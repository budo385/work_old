#include "rpcstub_coreservices.h"

void RpcStub_CoreServices::ReadLBOList(Status &Ret_pStatus, DbRecordSet &Ret_Data)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("CoreServices.ReadLBOList",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&Ret_Data,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_CoreServices::CheckVersion(Status& Ret_Status,QString strClientVersion,QString &Ret_strServerVersion,QString& Ret_strServerDbVersion,QString& Ret_strTemplateInUse,int& Ret_nAskOnStartup,int& Ret_nStartStartUpWizard,QByteArray& Ret_byteVersion, int &Ret_nClientTimeZoneOffsetMinutes)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&strClientVersion,"strClientVersion");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("CoreServices.CheckVersion",Ret_Status);
	if (!Ret_Status.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_Status,msg);
	if (!Ret_Status.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_Status);

	//if not OK, return
	if (!Ret_Status.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&Ret_strServerVersion,Ret_Status); if (!Ret_Status.IsOK()) return;
	msg.msg_out->GetParameter(1,&Ret_strServerDbVersion,Ret_Status); if (!Ret_Status.IsOK()) return;
	msg.msg_out->GetParameter(2,&Ret_strTemplateInUse,Ret_Status); if (!Ret_Status.IsOK()) return;
	msg.msg_out->GetParameter(3,&Ret_nAskOnStartup,Ret_Status); if (!Ret_Status.IsOK()) return;
	msg.msg_out->GetParameter(4,&Ret_nStartStartUpWizard,Ret_Status); if (!Ret_Status.IsOK()) return;
	msg.msg_out->GetParameter(5,&Ret_byteVersion,Ret_Status); if (!Ret_Status.IsOK()) return;
	msg.msg_out->GetParameter(6,&Ret_nClientTimeZoneOffsetMinutes,Ret_Status); if (!Ret_Status.IsOK()) return;
}

void RpcStub_CoreServices::UpdateVersion(Status& Ret_Status,QByteArray byteVersion)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&byteVersion,"byteVersion");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("CoreServices.UpdateVersion",Ret_Status);
	if (!Ret_Status.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_Status,msg);
	if (!Ret_Status.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_Status);

	//if not OK, return
	if (!Ret_Status.IsOK()) return;

	//extract return parameters from msg_out:
}

void RpcStub_CoreServices::ReadUserSessionData(Status& Ret_Status,DbRecordSet &Ret_UserData, DbRecordSet &Ret_ARSet,DbRecordSet &Ret_ContactData, DbRecordSet &Ret_recSettings, DbRecordSet &Ret_recOptions, DbRecordSet &Ret_lstMessages)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("CoreServices.ReadUserSessionData",Ret_Status);
	if (!Ret_Status.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_Status,msg);
	if (!Ret_Status.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_Status);

	//if not OK, return
	if (!Ret_Status.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&Ret_UserData,Ret_Status); if (!Ret_Status.IsOK()) return;
	msg.msg_out->GetParameter(1,&Ret_ARSet,Ret_Status); if (!Ret_Status.IsOK()) return;
	msg.msg_out->GetParameter(2,&Ret_ContactData,Ret_Status); if (!Ret_Status.IsOK()) return;
	msg.msg_out->GetParameter(3,&Ret_recSettings,Ret_Status); if (!Ret_Status.IsOK()) return;
	msg.msg_out->GetParameter(4,&Ret_recOptions,Ret_Status); if (!Ret_Status.IsOK()) return;
	msg.msg_out->GetParameter(5,&Ret_lstMessages,Ret_Status); if (!Ret_Status.IsOK()) return;
}

void RpcStub_CoreServices::GetUserSessions(Status& Ret_Status,int &Ret_nUserSessions)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("CoreServices.GetUserSessions",Ret_Status);
	if (!Ret_Status.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_Status,msg);
	if (!Ret_Status.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_Status);

	//if not OK, return
	if (!Ret_Status.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&Ret_nUserSessions,Ret_Status); if (!Ret_Status.IsOK()) return;
}

void RpcStub_CoreServices::SavePersonalSettings(Status &Ret_pStatus, DbRecordSet &RetOut_pLstForWrite)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&RetOut_pLstForWrite,"RetOut_pLstForWrite");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("CoreServices.SavePersonalSettings",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&RetOut_pLstForWrite,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_CoreServices::SavePersonalSettingsByPersonID(Status &Ret_pStatus, int nPersonID, DbRecordSet &RetOut_pLstForWrite)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&nPersonID,"nPersonID");
	msg.msg_in->AddParameter(&RetOut_pLstForWrite,"RetOut_pLstForWrite");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("CoreServices.SavePersonalSettingsByPersonID",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&RetOut_pLstForWrite,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_CoreServices::GetPersonalSettingsByPersonID(Status &Ret_pStatus, int nPersonID, DbRecordSet &RetOut_pLstForWrite)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&nPersonID,"nPersonID");
	msg.msg_in->AddParameter(&RetOut_pLstForWrite,"RetOut_pLstForWrite");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("CoreServices.GetPersonalSettingsByPersonID",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&RetOut_pLstForWrite,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

