#ifndef RPCSTUB_MAINENTITYSELECTOR_H__
#define RPCSTUB_MAINENTITYSELECTOR_H__

#include "common/common/status.h"
#include "bus_interface/bus_interface/interface_mainentityselector.h"
#include "bus_trans_client/bus_trans_client/connectionhandler.h"
#include "trans/trans/rpcstubmessagehandler.h"

class RpcStub_MainEntitySelector: public Interface_MainEntitySelector, public RpcStub
{
public:
	RpcStub_MainEntitySelector(ConnectionHandler *pConnectionHandler,int RPCType):
		RpcStub(RPCType){m_ConnectionHandler=pConnectionHandler;}

	void ReadData(Status &Ret_pStatus, int nEntityID, DbRecordSet Filter, DbRecordSet &Ret_Data);
	void ReadDataBatch(Status &Ret_pStatus, DbRecordSet lstEntityIDs, DbRecordSet lstFilters, DbRecordSet &Ret_Data);
	void ReadDataAll(Status &Ret_pStatus, int nEntityID,int nCountInClientCache, QDateTime datClientCacheTime, DbRecordSet &Ret_Data);
	void ReadDataOne(Status &Ret_pStatus, int nEntityID,DbRecordSet Filter, DbRecordSet &Ret_Data);

private:
	ConnectionHandler *m_ConnectionHandler; ///< connection handler
};

#endif	// RPCSTUB_MAINENTITYSELECTOR_H__
