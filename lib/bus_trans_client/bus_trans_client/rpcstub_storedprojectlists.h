#ifndef RPCSTUB_STOREDPROJECTLISTS_H__
#define RPCSTUB_STOREDPROJECTLISTS_H__

#include "common/common/status.h"
#include "bus_interface/bus_interface/interface_storedprojectlists.h"
#include "bus_trans_client/bus_trans_client/connectionhandler.h"
#include "trans/trans/rpcstubmessagehandler.h"

class RpcStub_StoredProjectLists: public Interface_StoredProjectLists, public RpcStub
{
public:
	RpcStub_StoredProjectLists(ConnectionHandler *pConnectionHandler,int RPCType):
		RpcStub(RPCType){m_ConnectionHandler=pConnectionHandler;}

	void GetListNames(Status &Ret_pStatus, DbRecordSet &Ret_Data);
	void DeleteList(Status &Ret_pStatus, int nListID);
	void CreateList(Status &Ret_pStatus, QString strListName, DbRecordSet &lstData, int &RetOut_nListID, bool bIsSelection, QString strSelectionXML, bool bIsFilter, QString strFilterXML);
	void GetListData(Status &Ret_pStatus, int nListID, DbRecordSet &Ret_lstData, bool &Ret_bIsSelection, QString &Ret_strSelectionXML, bool &Ret_bIsFilter, QString &Ret_strFilterXML, bool bGetDescOnly);
	void SetListData(Status &Ret_pStatus, int nListID, DbRecordSet &lstData, bool bIsSelection, QString strSelectionXML, bool bIsFilter, QString strFilterXML);
	void GetListDataLevelByLevel(Status &Ret_pStatus, int nListID, DbRecordSet &Ret_lstData, QString strParentProjectCode="", int nLevelsDeep=-1);

private:
	ConnectionHandler *m_ConnectionHandler; ///< connection handler
};

#endif	// RPCSTUB_STOREDPROJECTLISTS_H__
