#ifndef IPREDIRECTOR_H
#define IPREDIRECTOR_H


#include <QtCore>
#include "common/common/status.h"
#include "trans/trans/ipaddress.h"
#include "rpchttpclient.h"
#include "trans/trans/httpreadersync.h"
#include <QtWidgets/QSplashScreen>
#include <QApplication>
#include <QtWidgets/QMessageBox>

#include "rpcstub_connhandlerset.h"


typedef QList<IpAddressAdv> IpList;


/*!
    \class ConnectionRedirector
    \brief Used inside ConnectionHandler object for redirecting connection in DFO process
    \ingroup Bus_Trans_Client

	Use: 
	- initialize with redirector list with InitializeRedirectList() function
	- call RebuildRedirectorList() to refresh redirector list
	- call RedirectConnection() to redirect connection in XmlHttpClient to new IP address

	Warning: access to RpcHTTPClient must be locked while RedirectConnection() is executing
*/
class ConnectionRedirector: private QObject
{
     Q_OBJECT
public:
	
	ConnectionRedirector(RpcHTTPClient *pHttpclient,RpcStubConnHandlerSet *pStubConnHandlerSet);

	void InitializeRedirectList(QString strURLLookup);
	void RedirectConnection(Status& pStatus); 
	void RebuildRedirectorListFromServer(QString strSession);		//call this when session is started (fetch all servers from app. server)
	void RebuildRedirectorListLocal();			//call this before Login (dynamic IP or other sources)


private:
	
	void DynamicIPLookUp();
	void MarkIPAsFailure(IpAddressAdv IPadr);
	void MarkIPAsSucess(IpAddressAdv IPadr);


	RpcHTTPClient *m_RpcHttpClient;			///< rpchttp client 
	IpList m_redirectorList;	///< list of IP address for redirection: IP:port
	QString m_strLookupURL;					///< URL for web lookup of IP address of app. server
	int m_nRedirections;					///< number or maximum redirect tries

	//for web lookup:
	QBuffer m_httpResponse;
	QHttp m_httpReq;



	RpcStubConnHandlerSet *m_StubConnHandlerSet;	///<pointer to client stub set

};


#endif //IPREDIRECTOR_H
