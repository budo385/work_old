#ifndef RPCSTUBCONNHANDLERSET_H
#define RPCSTUBCONNHANDLERSET_H


#include <QtCore>
#include <QDebug>
#include "common/common/status.h"
#include "trans/trans/rpcstub.h"
#include "rpchttpclient.h"
#include "trans/trans/rpcstubmessagehandler.h"



/*!
    \class RpcStubConnHandlerSet
    \brief Client side stub set of esential functions for error/connection handling
    \ingroup Bus_Trans_Client

	This stub set is isolated, because error handling mechanism inside ConnectionHandler 
	can use this stubs internally.
	Error handling mechanism is implemented in global stub set.
	Uses already initialized HttpClient object to make call.
	Multithread safe.
*/

class RpcStubConnHandlerSet : public RpcStub  
{
public:
	RpcStubConnHandlerSet(RpcHTTPClient *pHttpclient,int RPCType);

	//LOGIN/SESSION
	void Login(Status& Ret_Status, QString& RetOut_strSessionID,QString strUsername, QByteArray strAuthToken,QByteArray strClientNonce,QString strModuleCode, QString strMLIID="", QString strParentSession="", bool bIsWebLogin=false, int nClientTimeZoneOffsetMinutes=0);		
	void Logout(Status& Ret_Status,QString strSessionID);
	void IsAlive(Status& Ret_Status,QString strSessionID);

	//LBO/DFO REDIRECTOR
	void ReadLBOList(Status &Ret_pStatus, QString strSessionID,DbRecordSet &Ret_Data);
private:
	RpcHTTPClient *m_RpcHttpClient;		///< http client for sending reuests to server
};


#endif //RPCSTUBCONNHANDLERSET_H
