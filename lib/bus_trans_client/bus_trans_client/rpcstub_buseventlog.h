#ifndef RPCSTUB_BUSEVENTLOG_H__
#define RPCSTUB_BUSEVENTLOG_H__

#include "common/common/status.h"
#include "bus_interface/bus_interface/interface_buseventlog.h"
#include "bus_trans_client/bus_trans_client/connectionhandler.h"
#include "trans/trans/rpcstubmessagehandler.h"

class RpcStub_BusEventLog: public Interface_BusEventLog, public RpcStub
{
public:
	RpcStub_BusEventLog(ConnectionHandler *pConnectionHandler,int RPCType):
		RpcStub(RPCType){m_ConnectionHandler=pConnectionHandler;}

	void AddEvent(Status &Ret_pStatus, int &Ret_EventID, int EventCode, QString EventText, int EventType, int EvCategoryID, QDateTime EventDateTime = QDateTime());
	void DeleteEventByEventID(Status &Ret_pStatus, int EventID);
	void DeleteEventByDateTime(Status &Ret_pStatus, QDateTime DateOlderThenGivenDateTime);
	void GetEventRecordsetByType(Status &Ret_pStatus, int EvType, DbRecordSet &Ret_EventRecordSet);
	void GetEventRecordsetByCategory(Status &Ret_pStatus, int EvCategoryID, DbRecordSet &Ret_EventRecordSet);
	void GetEventRecordsetByDateTime(Status &Ret_pStatus, QDateTime BeginTime, QDateTime EndTime, DbRecordSet &Ret_EventRecordSet);

private:
	ConnectionHandler *m_ConnectionHandler; ///< connection handler
};

#endif	// RPCSTUB_BUSEVENTLOG_H__
