#ifndef RPCSTUB_CLIENTSIMPLEORM_H__
#define RPCSTUB_CLIENTSIMPLEORM_H__

#include "common/common/status.h"
#include "bus_interface/bus_interface/interface_clientsimpleorm.h"
#include "bus_trans_client/bus_trans_client/connectionhandler.h"
#include "trans/trans/rpcstubmessagehandler.h"

class RpcStub_ClientSimpleORM: public Interface_ClientSimpleORM, public RpcStub
{
public:
	RpcStub_ClientSimpleORM(ConnectionHandler *pConnectionHandler,int RPCType):
		RpcStub(RPCType){m_ConnectionHandler=pConnectionHandler;}

	void Write(Status &Ret_pStatus, int nTableID, DbRecordSet &RetOut_pLstForWrite, QString pLockResourceID, int nQueryView, int nSkipLastColumns, DbRecordSet &RetOut_pLstForDelete);
	void WriteParentSubData(Status &Ret_pStatus, int nTableID, DbRecordSet &RetOut_pLstForWrite, int nParentId, QString strColParentName,QString pLockResourceID = "",int nQueryView = -1);
	void Read(Status &Ret_pStatus, int nTableID, DbRecordSet &Ret_pLstRead, QString strWhereClause = "", int nQueryView = -1, bool bDistinct = false);
	void ReadAdv(Status &Ret_pStatus, DbRecordSet &RetOut_pLstRead, QString strJoinClause);
	void ReadFromParentIDs(Status &Ret_pStatus, int nTableID,DbRecordSet &Ret_pLstRead, QString strColFK2ParentIDName, DbRecordSet &pLstOfID,QString strColIDName,QString strWhereClauseBefore="",QString strWhereClauseAfter="",int nQueryView=-1, bool bDistinct = false);
	void Delete(Status &Ret_pStatus, int nTableID, DbRecordSet &pLstForDelete, QString pLockResourceID, DbRecordSet &Ret_pLstStatusRows, bool pBoolTransaction);
	void Lock(Status &Ret_pStatus, int nTableID, DbRecordSet &pLstForLocking, QString &Ret_pResourceID, DbRecordSet &Ret_pLstStatusRows, bool bAllOrNothing = true);
	void UnLock(Status &Ret_pStatus, int nTableID, bool &Ret_bUnLocked, QString &pResourceID);
	void UnLockByRecordID(Status &Ret_pStatus, int nTableID, DbRecordSet &pLstForUnLocking, bool &Ret_bUnLocked);
	void IsLocked(Status &Ret_pStatus, int nTableID, bool &Ret_bLocked, DbRecordSet &LockList);
	void GetRowCount(Status &Ret_pStatus, int nTableID, QString strWhereClause, int &Ret_nCount);
	void ExecuteSQL(Status &Ret_pStatus, QString strSQL, DbRecordSet &Ret_LstReturn);
	void WriteHierarchicalData(Status &Ret_pStatus, int nTableID,DbRecordSet &RetOut_pLstForWrite, QString pLockResourceID = "", QString strSQLWhereFilter="", int nQueryView = -1);
	void DeleteHierarchicalData(Status &Ret_pStatus, int nTableID,DbRecordSet &pLstForDelete, QString pLockResourceID = "", QString strSQLWhereFilter="");

private:
	ConnectionHandler *m_ConnectionHandler; ///< connection handler
};

#endif	// RPCSTUB_CLIENTSIMPLEORM_H__
