#include "rpcstub_connhandlerset.h"



/*!
	Constructor: init with HTTP client with TCP connection to server
	\param pHttpclient - connection to server
	\param RPCType is constant defining what RPC protocol to use: 
	RPC_PROTOCOL_TYPE_XML_RPC, RPC_PROTOCOL_TYPE_SOAP, etc..defiend in rpcprotocol.h
*/
RpcStubConnHandlerSet::RpcStubConnHandlerSet(RpcHTTPClient *pHttpclient,int RPCType)
:RpcStub(RPCType)
{
	m_RpcHttpClient=pHttpclient;
}




//-------------------------------------------------------------------------
//					XML STUBS
//-------------------------------------------------------------------------

/*!
	Login stub

*/
void RpcStubConnHandlerSet::Login(Status& Ret_Status, QString& RetOut_strSessionID,QString strUsername, QByteArray strAuthToken,
								  QByteArray strClientNonce,QString strModuleCode, 
								  QString strMLIID, QString strParentSession, bool bIsWebLogin, int nClientTimeZoneOffsetMinutes)
{

	RpcStubMessageHandler msg(this);

	//create param list: (SESSION MUST BE FIRST ALWAYS)
	msg.msg_in->AddParameter(&RetOut_strSessionID);					//this is done because session is added at first place, always, and in this special case its is send as parameter
	msg.msg_in->AddParameter(&RetOut_strSessionID);					//this is IN/OUT parameter
	msg.msg_in->AddParameter(&strUsername);
	msg.msg_in->AddParameter(&strAuthToken);
	msg.msg_in->AddParameter(&strClientNonce);
	msg.msg_in->AddParameter(&strModuleCode);
	msg.msg_in->AddParameter(&strMLIID);
	msg.msg_in->AddParameter(&strParentSession);
	msg.msg_in->AddParameter(&bIsWebLogin);
	msg.msg_in->AddParameter(&nClientTimeZoneOffsetMinutes);
	


	//generate rpc call:
	msg.msg_in->GenerateRPC("UserLogon.Login",Ret_Status);
		if (!Ret_Status.IsOK())return;
		
	//make call to server:
	m_RpcHttpClient->SendData(Ret_Status,&msg.HttpRequest,&msg.HttpResponse);
		if (!Ret_Status.IsOK())return;

	//get fault if there is one:	
	msg.msg_out->CheckForFault(Ret_Status);

	//if OK, retrieve sessionID & IsAlive:
	if (Ret_Status.IsOK())
	{
		msg.msg_out->GetParameter(0,&RetOut_strSessionID,Ret_Status);
	}


}


/*!
	Logout stub

*/
void RpcStubConnHandlerSet::Logout(Status& Ret_Status,QString strSessionID)
{

	RpcStubMessageHandler msg(this);

	//set session ID as mandatory first param:
	msg.msg_in->AddParameter(&strSessionID);					//this is done because session is added at first place, always, and in this special case its is send as parameter

	//generate rpc call:
	msg.msg_in->GenerateRPC("UserLogon.Logout",Ret_Status);
		if (!Ret_Status.IsOK())return;

	//make call to server:
	m_RpcHttpClient->SendData(Ret_Status,&msg.HttpRequest,&msg.HttpResponse);
		if (!Ret_Status.IsOK())return;

	//get fault if there is one:	
	msg.msg_out->CheckForFault(Ret_Status);

}




/*!
	Returns LBO list if available, used later in connection handler for redirection in DFO
*/
void RpcStubConnHandlerSet::ReadLBOList(Status &Ret_pStatus, QString strSessionID,DbRecordSet &Ret_Data)
{

	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	msg.msg_in->AddParameter(&strSessionID);

	//add all other parameters
	Ret_Data.destroy();
	msg.msg_in->AddParameter(&Ret_Data);

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("CoreServices.ReadLBOList",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_RpcHttpClient->SendData(Ret_pStatus,&msg.HttpRequest,&msg.HttpResponse);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&Ret_Data,Ret_pStatus); 
	
	if (!Ret_pStatus.IsOK()) return;
}


void RpcStubConnHandlerSet::IsAlive(Status& Ret_Status,QString strSessionID)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("UserLogon.IsAlive",Ret_Status);
	if (!Ret_Status.IsOK())return;
	//make SYNC call to server:
	m_RpcHttpClient->SendData(Ret_Status,&msg.HttpRequest,&msg.HttpResponse);
	if (!Ret_Status.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_Status);

	//if not OK, return
	if (!Ret_Status.IsOK()) return;

	//extract return parameters from msg_out:

}