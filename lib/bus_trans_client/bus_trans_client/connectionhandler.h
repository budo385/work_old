#ifndef XMLHTTPCONNECTIONHANDLER_H
#define XMLHTTPCONNECTIONHANDLER_H


#include <QtCore>
#include <QThread>
#include <QMutex>
#include <QtWidgets/QSplashScreen>

#include "common/common/status.h"
#include "common/common/authenticator.h"
#include "rpcstub_connhandlerset.h"
#include "connectionredirector.h"
#include "rpcskeleton_servermessageset.h"
#include "common/common/backgroundtaskexecutor.h"




/*!
    \class ConnectionHandler
    \brief Abstract class for client connection handler (RPC & HTTP based). Multithreaded.
    \ingroup Bus_Trans_Client

	Used in stub collection as global object for sending data to server. Access is serialized.
	Implements:
	- error handling
	- IsAlive mechanism
	- caching of connection & authentication parameters 
	- persistent TCP connection
	- uses local rpc based stubs for Login,Logout,IsAlive methods
	Use:
	- inherit and implement right RPC protocol
*/
class IsAlive_Client;

class ConnectionHandler : public QObject
{
	friend class IsAlive_Client;
	Q_OBJECT

public:
	ConnectionHandler();
	virtual ~ConnectionHandler();

	/* connection pool interface -> reimplement to enable pool: add new connection from this, login and store in list...*/
	virtual void					StartThreadConnection(Status& pStatus,int nConnectionThreadID=-1)=0;
	virtual void					CloseThreadConnection(Status& pStatus,int nConnectionThreadID=-1)=0;
	void							SetMainThreadConnection(int nThread){m_nMainThreadID=nThread;};
	virtual void					CloseAllThreadConnections(Status& pStatus);

	/* common interface */
	void							Login(Status& pStatus, QString pUsername, QString pPassword,QString strModuleCode, QString strMLIID, QString strParentSession="", int nClientTimeZoneOffsetMinutes=0);
	void							Logout(Status& pStatus);
	bool							IsLogged();
	QString							GetSessionID();
	RpcHTTPClient					*GetRpcHttpClient();
	RpcSkeletonServerMessageSet*	GetSkeletonSet();
	void							SetConnectionSettings(HTTPClientConnectionSettings& pConnection);
	void							GetConnectionSettings(HTTPClientConnectionSettings& pConnection, Authenticator &pSessionCache);
	void							SendRPC(Status& pStatus, RpcStubMessageHandler &msg);	
	bool							IsSocketInUse();

public slots:
	void OnSocketError(int nErrorCode,QString rawText);
signals:
	void RaiseFatalError(int);			//general socket error/xml parse failed, close application

protected:
	//call this first!! (instead of constructor
	void InitConnection(int RPCType);
	
	//child classes will init this params:
	RpcHTTPClient *m_HttpClient;						///< RpcHttp Client
	RpcStubConnHandlerSet *m_StubConnHandlerSet;		///< Connection stub set of some basic functions (login,..)
	RpcSkeletonServerMessageSet* SkeletonSrvMsgSet;

	/* common interface */
	void							Priv_Login(Status& pStatus, QString pUsername, QString pPassword,QString strModuleCode, QString strMLIID, QString strParentSession="", int nClientTimeZoneOffsetMinutes=0);
	void							Priv_Logout(Status& pStatus);
	bool							Priv_IsLogged(){return m_bLogged;}
	QString							Priv_GetSessionID();
	RpcHTTPClient*					Priv_GetRpcHttpClient(){return m_HttpClient;};
	RpcSkeletonServerMessageSet*	Priv_GetSkeletonSet(){return SkeletonSrvMsgSet;};
	void							Priv_SetConnectionSettings(HTTPClientConnectionSettings& pConnection);
	void							Priv_GetConnectionSettings(HTTPClientConnectionSettings& pConnection, Authenticator &pSessionCache);
	void							Priv_SendRPC(Status& pStatus, RpcStubMessageHandler &msg);	
	bool							Priv_IsSocketInUse();
	void							Priv_IsAlive(Status &pStatus);
	void							StopIsAlive();
	void							StartIsAlive();


	//error handlers
	void TestForFatalError(Status pStatus);
	void ErrorHandler(bool &bTryAgain,Status& pStatus);	
	void errReauthenticate(Status& pStatus);
	void errSessionExpired(Status& pStatus);
	void ShowDFOMessage(Status pStatus, QString strExtrMsg="");

	ConnectionHandler *GetConnectionByThread();
	
	//members
	int						m_nErrorTries;					///< number or current tries to recover from error
	QMutex					m_Mutex;						///< serialize access to this global object, no more then 1 thread can use this object in same time
	Authenticator			m_SessionCache;					///< session cache
	ConnectionRedirector*	m_Redirector;					///< connection redirector
	bool					m_bUseDFO;						///< error handler can be switched off
	bool					m_bLogged;						///< true if logged
	bool					m_bFatalErrorState;				///<if true, no acces is allowed (fire assert: just for debug)

	QReadWriteLock					m_hshConnectionsLock;
	QHash<int,ConnectionHandler *>	m_hshConnections;
	int						m_nMainThreadID;
	BackgroundTaskExecutor *m_IsAliveBkgTask;
};


class IsAlive_Client : public BackgroundTaskInterface
{
	void ExecuteTask(int nCallerID=-1,int nTaskID=-1,QString strData="");
public:
	ConnectionHandler *m_Handler;
};


#endif //XMLHTTPCONNECTIONHANDLER_H

