#include "xmlrpchttpclient.h"


/*!
	Constructor: registers to RPC2 domain and itself as server msg handler
*/
XmlRpcHTPPClient::XmlRpcHTPPClient(RpcSkeleton *pSrvMsgSkeletonSet)
:RpcHTTPClient(pSrvMsgSkeletonSet,RPC_PROTOCOL_TYPE_XML_RPC)
{
	SetDomain(DOMAIN_XML_RPC);	//sets domain to handler of incoming srv. mesages
}



