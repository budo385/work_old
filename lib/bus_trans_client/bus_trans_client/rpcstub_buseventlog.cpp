#include "rpcstub_buseventlog.h"

void RpcStub_BusEventLog::AddEvent(Status &Ret_pStatus, int &Ret_EventID, int EventCode, QString EventText, int EventType, int EvCategoryID, QDateTime EventDateTime )
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&EventCode,"EventCode");
	msg.msg_in->AddParameter(&EventText,"EventText");
	msg.msg_in->AddParameter(&EventType,"EventType");
	msg.msg_in->AddParameter(&EvCategoryID,"EvCategoryID");
	msg.msg_in->AddParameter(&EventDateTime,"EventDateTime");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusEventLog.AddEvent",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&Ret_EventID,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusEventLog::DeleteEventByEventID(Status &Ret_pStatus, int EventID)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&EventID,"EventID");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusEventLog.DeleteEventByEventID",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
}

void RpcStub_BusEventLog::DeleteEventByDateTime(Status &Ret_pStatus, QDateTime DateOlderThenGivenDateTime)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&DateOlderThenGivenDateTime,"DateOlderThenGivenDateTime");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusEventLog.DeleteEventByDateTime",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
}

void RpcStub_BusEventLog::GetEventRecordsetByType(Status &Ret_pStatus, int EvType, DbRecordSet &Ret_EventRecordSet)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&EvType,"EvType");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusEventLog.GetEventRecordsetByType",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&Ret_EventRecordSet,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusEventLog::GetEventRecordsetByCategory(Status &Ret_pStatus, int EvCategoryID, DbRecordSet &Ret_EventRecordSet)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&EvCategoryID,"EvCategoryID");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusEventLog.GetEventRecordsetByCategory",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&Ret_EventRecordSet,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusEventLog::GetEventRecordsetByDateTime(Status &Ret_pStatus, QDateTime BeginTime, QDateTime EndTime, DbRecordSet &Ret_EventRecordSet)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&BeginTime,"BeginTime");
	msg.msg_in->AddParameter(&EndTime,"EndTime");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusEventLog.GetEventRecordsetByDateTime",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&Ret_EventRecordSet,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

