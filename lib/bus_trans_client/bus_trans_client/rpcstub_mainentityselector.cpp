#include "rpcstub_mainentityselector.h"

void RpcStub_MainEntitySelector::ReadData(Status &Ret_pStatus, int nEntityID, DbRecordSet Filter, DbRecordSet &Ret_Data)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&nEntityID,"nEntityID");
	msg.msg_in->AddParameter(&Filter,"Filter");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("MainEntitySelector.ReadData",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&Ret_Data,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_MainEntitySelector::ReadDataBatch(Status &Ret_pStatus, DbRecordSet lstEntityIDs, DbRecordSet lstFilters, DbRecordSet &Ret_Data)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&lstEntityIDs,"lstEntityIDs");
	msg.msg_in->AddParameter(&lstFilters,"lstFilters");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("MainEntitySelector.ReadDataBatch",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&Ret_Data,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_MainEntitySelector::ReadDataAll(Status &Ret_pStatus, int nEntityID,int nCountInClientCache, QDateTime datClientCacheTime, DbRecordSet &Ret_Data)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&nEntityID,"nEntityID");
	msg.msg_in->AddParameter(&nCountInClientCache,"nCountInClientCache");
	msg.msg_in->AddParameter(&datClientCacheTime,"datClientCacheTime");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("MainEntitySelector.ReadDataAll",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&Ret_Data,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_MainEntitySelector::ReadDataOne(Status &Ret_pStatus, int nEntityID,DbRecordSet Filter, DbRecordSet &Ret_Data)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&nEntityID,"nEntityID");
	msg.msg_in->AddParameter(&Filter,"Filter");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("MainEntitySelector.ReadDataOne",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&Ret_Data,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

