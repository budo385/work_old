<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="tr">
<context>
    <name>ConnectionHandler</name>
    <message>
        <location filename="connectionhandler.cpp" line="459"/>
        <location filename="connectionhandler.cpp" line="461"/>
        <source>Dynamic fail over in progress. Reason: </source>
        <translation>Dynamischer Fail-Over-Serverwechsel wird durchgeführt. Grund: </translation>
    </message>
    <message>
        <location filename="connectionhandler.cpp" line="501"/>
        <source>Dynamic fail over was unable to reestablished connection. Session is terminated.</source>
        <translation>Dynamischer Fail-Over-Serverwechsel konnte keine Verbindung herstellen. Sitzung beendet.</translation>
    </message>
    <message>
        <location filename="connectionhandler.cpp" line="389"/>
        <source>Server issued redirected command. Redirecting to: </source>
        <translation>Eine serverinitiierte Umleitung erfolgt nach: </translation>
    </message>
    <message>
        <location filename="connectionhandler.cpp" line="395"/>
        <source>Server issued Re-authentication command! </source>
        <translation>Der Server verlangt eine Neu-Authentifizierung! </translation>
    </message>
    <message>
        <location filename="connectionhandler.cpp" line="399"/>
        <source>Re-authentication process is done. Connection to server successfully reestablished!</source>
        <translation>Neu-Authentifizierung durchgeführt. Verbindung zum Server erfolgreich wiederhergestellt!</translation>
    </message>
    <message>
        <location filename="connectionhandler.cpp" line="262"/>
        <source>Data Transfer in progress! Can not complete request!</source>
        <translation>Datentransfer wird durchgeführt. Währenddessen kann keine weitere Funktion ausgeführt werden!</translation>
    </message>
</context>
<context>
    <name>ConnectionRedirector</name>
    <message>
        <location filename="connectionredirector.cpp" line="92"/>
        <source>Trying reconnection to: </source>
        <translation>Verbindung wird wiederhergestellt zu: </translation>
    </message>
    <message>
        <location filename="connectionredirector.cpp" line="114"/>
        <source>Connection established to: </source>
        <translation>Verbindung hergestellt zu: </translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Dynamic fail over in progress. Reason: </source>
        <translation type="obsolete">Dynamischer Serverwechsel wird durchgeführt. Grund: </translation>
    </message>
    <message>
        <source>Dynamic fail over was unable to reestablished connection. Session is terminated.</source>
        <translation type="obsolete">Dynamischer Serverwechsel konnte keine Verbindung herstellen. Sitzung beendet.</translation>
    </message>
    <message>
        <source>Server issued redirected command. Redirecting to: </source>
        <translation type="obsolete">Eine serverinitiierte umleitung erfolgt nach: </translation>
    </message>
    <message>
        <source>Server issued Re-authentication command! </source>
        <translation type="obsolete">Der Server verlangt eine Neu-Authentifizierung! </translation>
    </message>
    <message>
        <source>Re-authentication process is done. Connection to server successfully reestablished!</source>
        <translation type="obsolete">Neu-Authentifizierung durchgeführt. Verbindung zum Server erfolgreich wiederhergestellt!</translation>
    </message>
</context>
</TS>
