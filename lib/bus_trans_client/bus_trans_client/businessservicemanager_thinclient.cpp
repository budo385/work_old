#include "businessservicemanager_thinclient.h"
#include "bus_trans_client/bus_trans_client/xmlrpcstub_clientset.h"
#include "common/common/logger.h"
#include "common/common/config_version_sc.h"
#include "common/common/threadid.h"

extern BusinessServiceManager	*g_pBoSet;	//it's me, it's me, it's me

BusinessServiceManager_ThinClient::BusinessServiceManager_ThinClient(QObject *parent)
	: QObject(parent)
{
	m_pRpcConnection=NULL;
}

BusinessServiceManager_ThinClient::~BusinessServiceManager_ThinClient()
{
	Status err;
	StopService(err);

	//just to be sure in case after fatal error:
	if(app!=NULL) delete app;
	app=NULL;
	if(m_pRpcConnection!=NULL) delete m_pRpcConnection;
	m_pRpcConnection=NULL;

}


/*!
	Creates BO set based on settings

	\param pStatus			- error
*/

void BusinessServiceManager_ThinClient::StartServiceSet(Status &pStatus)
{
	pStatus.setError(0);
	//if started stop it
	if(m_bServiceStarted) 
	{
		StopService(pStatus);
		if(!pStatus.IsOK()) return;
	}
	//clear pointers:
	if(app!=NULL) delete app;
	app=NULL;
	if(m_pRpcConnection!=NULL) delete m_pRpcConnection;
	m_pRpcConnection=NULL;
	//make connection, init with parameters
	CreateRPCNetworkLayer(RPC_PROTOCOL_TYPE_XML_RPC);
	m_pRpcConnection->SetConnectionSettings(m_ConnSettings);
	//connect to reemit signals:
	connect(m_pRpcConnection->GetRpcHttpClient(),SIGNAL(SocketOperationInProgress(int,qint64,qint64)),this,SIGNAL(CommunicationInProgress(int,qint64,qint64)));
	connect(m_pRpcConnection,SIGNAL(RaiseFatalError(int)),this,SLOT(OnFatalErrorDetected(int)));
	connect(m_pRpcConnection->GetSkeletonSet(),SIGNAL(Signal_ServerMsg(DbRecordSet)),this,SIGNAL(CommunicationServerMessageRecieved(DbRecordSet)));
	
	m_bServiceStarted=true;
}



/*!
	Stops service, destroy all objects, flags

	\param pStatus			- error
*/
void BusinessServiceManager_ThinClient::StopService(Status &pStatus, bool bFatalErrorDetected)
{
	pStatus.setError(0);
	if(!m_bServiceStarted) return;

	//only reset if not fatal (leave those bo's to live... until destruct or next start)
	if (!bFatalErrorDetected)
	{
		if(app!=NULL) delete app;
		app=NULL;
		if(m_pRpcConnection!=NULL)
		{
			Status err;
			m_pRpcConnection->CloseAllThreadConnections(err); //clean all child connections
			delete m_pRpcConnection;
		}
		m_pRpcConnection=NULL;
	}
	else
	{
		Status err;
		if(m_pRpcConnection)
		{
			m_pRpcConnection->CloseAllThreadConnections(err); //clean all child connections
			m_pRpcConnection->GetRpcHttpClient()->Disconnect(err);
		}
	}

	m_bServiceStarted=false;
}

//disable access to network:
void BusinessServiceManager_ThinClient::OnFatalErrorDetected(int nErr)
{
	//if exists:
	emit CommunicationFatalError(nErr);
}



void BusinessServiceManager_ThinClient::AbortDataTransfer()
{
	if (m_pRpcConnection)
	{
		m_pRpcConnection->GetRpcHttpClient()->CancelOperation();
	}
}

void BusinessServiceManager_ThinClient::CreateRPCNetworkLayer(int nRPC_TypeID)
{

	switch(nRPC_TypeID)
	{
	case RPC_PROTOCOL_TYPE_XML_RPC:
		{
			//make connection:
			XmlRpcHTTPConnectionHandler *pXmlRpcConnection= new XmlRpcHTTPConnectionHandler;
			//make stub collection	
			app= new XmlRpcStubClientSet(pXmlRpcConnection);
			m_pRpcConnection=pXmlRpcConnection; //store connection;
		}
		break;
	case RPC_PROTOCOL_TYPE_SOAP:
		{

		}
		break;
	default:
		break;
	}

	if (m_pRpcConnection)
		m_pRpcConnection->SetMainThreadConnection(ThreadIdentificator::GetCurrentThreadID());
}



void BusinessServiceManager_ThinClient::QcLogin (Status &pStatus,QString strUser,QString strPassword, HTTPClientConnectionSettings connection,QString strModuleCode, QString strMLIID)
{
	SetSettings(connection);
	StartServiceSet(pStatus);
	if (!pStatus.IsOK())
		return;

	int nClientTimeZone=0; //remain to be fixed

	//login:
	app->UserLogon->Login(pStatus,strUser,strPassword,strModuleCode,strMLIID,"",nClientTimeZone);
	if(!pStatus.IsOK())
	{
		Status err;
		StopService(err);
		return;
	}

	//check APP version:
	QString strAppServerVersion,strAppServerDatabaseVersion,strTemplateInUse;
	int nAskForTemplate;
	QByteArray byteDemoKey;
	int nStartUpWizardEnabled;
	int Ret_nClientTimeZoneOffsetMinutes; //this is actual difference from clien to server time-> use only in special messages from server, as XML, RPc, REST and other layer are automatically converte
	g_pBoSet->app->CoreServices->CheckVersion(pStatus,QString(APPLICATION_VERSION),strAppServerVersion,strAppServerDatabaseVersion,strTemplateInUse,nAskForTemplate,nStartUpWizardEnabled,byteDemoKey,Ret_nClientTimeZoneOffsetMinutes);
	if(!pStatus.IsOK())
	{
		Status err;
		app->UserLogon->Logout(err);
		if (pStatus.getErrorCode()==StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH)
		{
			pStatus.setError(StatusCodeSet::ERR_SYSTEM_VERSION_MISMATCH);
			//AskForUpdate(strAppServerVersion);
		}
		return;
	}
	if (strAppServerVersion!=QString(APPLICATION_VERSION))
	{
		Status err;
		app->UserLogon->Logout(err);
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_VERSION_MISMATCH);
		//AskForUpdate(strAppServerVersion);
		return;
	}
	//check DB:
	if (strAppServerDatabaseVersion!=QVariant(DATABASE_VERSION).toString())
	{
		app->UserLogon->Logout(pStatus);	//logout first:
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_VERSION_MISMATCH);
		//TEMPORARY: ask version:
		//QString strMsg=tr("Database version do not match, notify administrator!");
		//int nResult=QMessageBox::information(NULL,tr("Warning"),strMsg);
		return;
	}

	//Options and settings.
	g_pBoSet->app->CoreServices->ReadUserSessionData(pStatus,m_recUserData,m_rowARSet,m_recContactData,m_recSettings,m_recOptions,m_recSrvMsg);
	if(!pStatus.IsOK())return;

}

void BusinessServiceManager_ThinClient::QcLogout (Status &pStatus)
{
	if (!m_bServiceStarted)
		return;
	Q_ASSERT(app);
	//StopIsAlive();
	app->UserLogon->Logout(pStatus);	//logout first:
	StopService(pStatus);
}

void BusinessServiceManager_ThinClient::QcGetUserData(DbRecordSet &recUserData,DbRecordSet &recContactData,DbRecordSet &recSettings, DbRecordSet &recOptions,DbRecordSet &rowARSet,DbRecordSet &recSrvMsg)
{
	recUserData=m_recUserData;
	recContactData=m_recContactData;
	recSettings=m_recSettings;
	recOptions=m_recOptions;
	rowARSet=m_rowARSet;
	recSrvMsg=m_recSrvMsg;
}

void BusinessServiceManager_ThinClient::StartThreadConnection(Status& pStatus, QObject *pSignalReceiver)
{	
	if (m_pRpcConnection)
	{
		m_pRpcConnection->StartThreadConnection(pStatus);
		if (pSignalReceiver)
		{
			connect(m_pRpcConnection->GetRpcHttpClient(),SIGNAL(SocketOperationInProgress(int,qint64,qint64)),pSignalReceiver,SIGNAL(CommunicationInProgress(int,qint64,qint64)));
			connect(m_pRpcConnection,SIGNAL(RaiseFatalError(int)),pSignalReceiver,SLOT(OnFatalErrorDetected(int)));
			connect(m_pRpcConnection->GetSkeletonSet(),SIGNAL(Signal_ServerMsg(DbRecordSet)),pSignalReceiver,SIGNAL(CommunicationServerMessageRecieved(DbRecordSet)));
		}
	}
}
void BusinessServiceManager_ThinClient::CloseThreadConnection(Status& pStatus)
{
	if (m_pRpcConnection)
		m_pRpcConnection->CloseThreadConnection(pStatus);
}

void BusinessServiceManager_ThinClient::GetSettings(HTTPClientConnectionSettings& pConnSettings, Authenticator &pAuthCache)
{
	m_pRpcConnection->GetConnectionSettings(pConnSettings,pAuthCache);
}







