#include "userstoragehttpclient.h"
#include "trans/trans/httpfilestreamer.h"
#include "trans/trans/config_trans.h"
#include "common/common/config_version_sc.h"
#include "common/common/userstoragehelper.h"

UserStorageHTTPClient::UserStorageHTTPClient()
{
	m_strClientTag=QString(CLIENT_HTTP_DESCRIPTION).arg(APPLICATION_VERSION);
}

UserStorageHTTPClient::~UserStorageHTTPClient()
{
	//qDebug()<<"UserStorageHTTPClient destroyed";

}

void UserStorageHTTPClient::SetConnectionSettings(const HTTPClientConnectionSettings& pConnSettings, const Authenticator &SessionCache)
{
	m_SessionCache=SessionCache;

	QHostInfo info = QHostInfo::fromName(pConnSettings.m_strServerIP);
	m_strHost=info.hostName();
	if (pConnSettings.m_nPort!=80)
		m_strHost+=":"+QVariant(pConnSettings.m_nPort).toString();

	QByteArray clientNonce,clientAuthToken; 
	m_SessionCache.GenerateAuthenticationToken(clientNonce,clientAuthToken);
	
	//qDebug()<<clientAuthToken.toBase64();
	//qDebug()<<clientNonce.toBase64();

	//m_strAuthQueryString=QString("/")+DOMAIN_HTTP_USER_STORAGE+"?";
	//m_strAuthQueryString+="user="+m_SessionCache.m_strUserName+"&";
	//m_strAuthQueryString+="token="+QUrl::toPercentEncoding(clientAuthToken.toBase64())+"&";
	//m_strAuthQueryString+="nonce="+QUrl::toPercentEncoding(clientNonce.toBase64())+"&";
	//m_strAuthQueryString+="parent_session="+QUrl::toPercentEncoding(m_SessionCache.m_SessionID); //will test if this session exists and running..

	m_strAuthCookieString=UserStorageHelper::GenerateAuthCookie(m_SessionCache.m_SessionID);

	//check:
	/*
	QUrl url(m_strAuthQueryString);
	qDebug()<<url.allQueryItemValues("token");
	qDebug()<<url.allQueryItemValues("nonce");
	qDebug()<<QUrl::fromPercentEncoding(url.queryItemValue("token").toLatin1());
	qDebug()<<QUrl::fromPercentEncoding(url.queryItemValue("nonce").toLatin1());
	QByteArray token=QByteArray::fromBase64(QUrl::fromPercentEncoding(url.queryItemValue("token").toLatin1()).toLatin1());
	QByteArray nonce=QByteArray::fromBase64(QUrl::fromPercentEncoding(url.queryItemValue("nonce").toLatin1()).toLatin1());
	Q_ASSERT(clientAuthToken==token);
	Q_ASSERT(clientNonce==nonce);
 */

	//if connected/destroy connection:
	Status err;
	err.setError(0);
	if(IsConnected())HTTPClientThread::Disconnect(err);
	Q_ASSERT(err.IsOK());

	//set new settings
	HTTPClientThread::SetConnectionSettings(pConnSettings); //sets conn settings

}

void UserStorageHTTPClient:: Connect(Status& pStatus)
{
	HTTPClientConnectionSettings tmpJBLinux=HTTPClientThread::GetConnectionSettings(); 
	HTTPClientThread::Connect(pStatus,tmpJBLinux);
}



void UserStorageHTTPClient::Disconnect(Status& pStatus){
	HTTPClientThread::Disconnect(pStatus);
}


void UserStorageHTTPClient::SetRequestHeader(HTTPRequest &request)
{
	//set request:
	if(m_strHost!="")
		request.setValue("Host",m_strHost);
	request.setValue("User-agent",m_strClientTag); 
	request.setValue("Connection","close");
	request.addValue("Accept-Encoding","deflate");
	request.addValue("Cookie",m_strAuthCookieString);
}



//strFile=full path
void UserStorageHTTPClient::DownLoadBackup(Status& pStatus,QString strFile,bool bOverWrite)
{
	pStatus.setError(0);

	//create stream object, init streamer
	HTTPFileStreamer *pChunker=new HTTPFileStreamer;
	pChunker->InitFileOperation(pStatus,strFile,true,bOverWrite);
	if (!pStatus.IsOK()){delete pChunker;return;}

	//connect
	Connect(pStatus);
	if (!pStatus.IsOK()){delete pChunker;return;}

	HTTPRequest request;
	HTTPResponse response;

	QFileInfo info(strFile);
	SetRequestHeader(request);
	QString strPath=QString("/")+DOMAIN_HTTP_USER_STORAGE+"/"+QUrl::toPercentEncoding(info.fileName());
	strPath+="?method=downloadbackup";
	request.setRequest("GET",strPath);
	pChunker->SetRequestHeader(request);
	
	//qDebug()<<request.toString();

	HTTPClientThread::SendData(pStatus,&request,&response,pChunker);
	
	//qDebug()<<response.GetBody()->size();

	Status err;
	Disconnect(err); //disconnect first, ignore errors
	if (!pStatus.IsOK())
	{
		pChunker->DeleteFile();
		delete pChunker;
		return;
	}
	
	pChunker->Close();



	//HTTP check:
	if (response.statusCode()!=200)
	{
		if (!response.GetBody()->isEmpty())
			pStatus.setError(StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("HTTP Transfer failed. Reason: %1").arg(QString(*response.GetBody())));
		else
			pStatus.setError(StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("Transfer failed. HTTP error code: %1").arg(response.statusCode()));
		pChunker->DeleteFile();
		delete pChunker;
		return;
	}

	//check chunker
	if(!pChunker->GetLastError().IsOK())
	{
		pStatus=pChunker->GetLastError();
		pChunker->DeleteFile();
		delete pChunker;
		return;
	}

	//qDebug()<<pChunker->GetFileSize();
	//qDebug()<<pChunker->GetTotalStreamSize();

	//means that server decided to send whole file inside response:
	if (pChunker->GetFileSize()==0 && response.value("Content-Length").toInt()>0)
	{
		pChunker->DeleteFile();
		delete pChunker;

		QByteArray data=*response.GetBody();

		QFile file_write(strFile);
		if(!file_write.open(QIODevice::WriteOnly))
		{
			pStatus.setError(StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("Transfer failed. Can not open file for write: %1").arg(strFile));
			return;
		}

		file_write.write(data);
		file_write.close();
		return;
	}



	if (pChunker->GetFileSize()!=pChunker->GetTotalStreamSize()&& pChunker->GetTotalStreamSize()!=0)
	{
		pChunker->DeleteFile();
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("Operation failed. CRC checksum failed!"));
	}

	delete pChunker;

}
void UserStorageHTTPClient::UpLoadBackup(Status& pStatus,QString strFile,bool bOverWrite)
{
	pStatus.setError(0);

	//create stream object, init streamer
	HTTPFileStreamer *pChunker=new HTTPFileStreamer;
	pChunker->InitFileOperation(pStatus,strFile,false,false);
	if (!pStatus.IsOK()){delete pChunker;return;}

	//connect
	Connect(pStatus);
	if (!pStatus.IsOK()){delete pChunker;return;}

	HTTPRequest request;
	HTTPResponse response;

	QFileInfo info(strFile);
	SetRequestHeader(request);
	QString strPath=QString("/")+DOMAIN_HTTP_USER_STORAGE+"/"+QUrl::toPercentEncoding(info.fileName());
	strPath+="?method=uploadbackup";
	if (bOverWrite)
		strPath+="&overwrite=yes";
	request.setRequest("POST",strPath);
	HTTPUtil::SetHeaderDataBasedOnFile(&request,strFile);
	request.setValue("Transfer-Encoding","chunked");
	request.setValue("Except","100-continue");
	pChunker->SetRequestHeader(request);
	
	//qDebug()<<request.toString();

	HTTPClientThread::SendData(pStatus,&request,&response,pChunker);
	Status err;
	Disconnect(err); //disconnect first, ignore errors

	if (!pStatus.IsOK()){delete pChunker;return;}

	pChunker->Close();

	//HTTP check:
	if (response.statusCode()!=200)
	{
		if (!response.GetBody()->isEmpty())
			pStatus.setError(StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("HTTP Transfer failed. Reason: %1").arg(QString(*response.GetBody())));
		else
			pStatus.setError(StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("Transfer failed. HTTP error code: %1").arg(response.statusCode()));
		delete pChunker;
		return;
	}

	//check chunker
	if(!pChunker->GetLastError().IsOK())
	{
		pStatus=pChunker->GetLastError();
	}

	delete pChunker;
}


//strFile=full path
void UserStorageHTTPClient::DownLoadFile(Status& pStatus,QString strFile,bool bOverWrite)
{
	pStatus.setError(0);

	//create stream object, init streamer
	HTTPFileStreamer *pChunker=new HTTPFileStreamer;
	pChunker->InitFileOperation(pStatus,strFile,true,bOverWrite);
	if (!pStatus.IsOK()){delete pChunker;return;}

	//connect
	Connect(pStatus);
	if (!pStatus.IsOK()){delete pChunker;return;}

	HTTPRequest request;
	HTTPResponse response;

	QFileInfo info(strFile);
	SetRequestHeader(request);
	QString strPath=QString("/")+DOMAIN_HTTP_USER_STORAGE+"/"+QUrl::toPercentEncoding(info.fileName());
	request.setRequest("GET",strPath);
	pChunker->SetRequestHeader(request);

	HTTPClientThread::SendData(pStatus,&request,&response,pChunker);
	Status err;
	Disconnect(err); 
	if (!pStatus.IsOK()){pChunker->DeleteFile();delete pChunker;return;}
	pChunker->Close();

	//HTTP check:
	if (response.statusCode()!=200)
	{
		if (!response.GetBody()->isEmpty())
			pStatus.setError(StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("HTTP Transfer failed. Reason: %1").arg(QString(*response.GetBody())));
		else
			pStatus.setError(StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("Transfer failed. HTTP error code: %1").arg(response.statusCode()));
		pChunker->DeleteFile();
		delete pChunker;
		return;
	}

	//means that server decided to send whole file inside response:
	if (pChunker->GetFileSize()==0 && response.value("Content-Length").toInt()>0)
	{
		pChunker->DeleteFile();
		delete pChunker;

		QByteArray data=*response.GetBody();

		QFile file_write(strFile);
		if(!file_write.open(QIODevice::WriteOnly))
		{
			pStatus.setError(StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("Transfer failed. Can not open file for write: %1").arg(strFile));
			return;
		}

		file_write.write(data);
		file_write.close();
		return;
	}
	
	//check chunker
	if(!pChunker->GetLastError().IsOK())
	{
		pStatus=pChunker->GetLastError();
		pChunker->DeleteFile();
		delete pChunker;
		return;
	}

	//crc check:
	QString strFileName;
	//qint64 nOriginalFileSize=0;
	//bool bOK=HTTPUtil::ExtractFileInfoFromContentDisposition(response.value("Content-Disposition"),strFileName,nOriginalFileSize);
	if (pChunker->GetFileSize()!=pChunker->GetTotalStreamSize() && pChunker->GetTotalStreamSize()!=0)
	{
		pChunker->DeleteFile();
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("Operation failed. CRC checksum failed!"));
	}

	delete pChunker;

}
void UserStorageHTTPClient::UpLoadFile(Status& pStatus,QString strFile,bool bOverWrite)
{
	pStatus.setError(0);

	//create stream object, init streamer
	HTTPFileStreamer *pChunker=new HTTPFileStreamer;
	pChunker->InitFileOperation(pStatus,strFile,false,false);
	if (!pStatus.IsOK()){delete pChunker;return;}

	//connect
	Connect(pStatus);
	if (!pStatus.IsOK()){delete pChunker;return;}

	HTTPRequest request;
	HTTPResponse response;

	QFileInfo info(strFile);
	SetRequestHeader(request);
	QString strPath=QString("/")+DOMAIN_HTTP_USER_STORAGE+"/"+QUrl::toPercentEncoding(info.fileName());
	strPath+="?method=upload";
	if (bOverWrite)
		strPath+="&overwrite=yes";
	request.setRequest("POST",strPath);
	HTTPUtil::SetHeaderDataBasedOnFile(&request,strFile);
	request.setValue("Transfer-Encoding","chunked");
	request.setValue("Except","100-continue");
	pChunker->SetRequestHeader(request);

	HTTPClientThread::SendData(pStatus,&request,&response,pChunker);
	Status err;
	Disconnect(err); //disconnect first, ignore errors

	if (!pStatus.IsOK()){delete pChunker;return;}

	pChunker->Close();

	//HTTP check:
	if (response.statusCode()!=200)
	{
		if (!response.GetBody()->isEmpty())
			pStatus.setError(StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("HTTP Transfer failed. Reason: %1").arg(QString(*response.GetBody())));
		else
			pStatus.setError(StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("Transfer failed. HTTP error code: %1").arg(response.statusCode()));
		delete pChunker;
		return;
	}

	//check chunker
	if(!pChunker->GetLastError().IsOK())
	{
		pStatus=pChunker->GetLastError();
	}

	delete pChunker;
}

void UserStorageHTTPClient::ClearFromUserStorage(Status& pStatus,QString strFile)
{
	pStatus.setError(0);

	//connect
	Connect(pStatus);
	if (!pStatus.IsOK()){return;}

	HTTPRequest request;
	HTTPResponse response;

	QFileInfo info(strFile);
	SetRequestHeader(request);
	QString strPath=QString("/")+DOMAIN_HTTP_USER_STORAGE+"/"+QUrl::toPercentEncoding(info.fileName());
	strPath+="?method=clear";
	//if (!strPath.isEmpty())
	//	strPath+="&filename="+QUrl::toPercentEncoding(info.fileName());
	request.setRequest("POST",strPath);

	HTTPClientThread::SendData(pStatus,&request,&response);
	Status err;
	Disconnect(err); //disconnect first, ignore errors

	if (!pStatus.IsOK()){return;}

	//HTTP check:
	if (response.statusCode()!=200)
	{
		if (!response.GetBody()->isEmpty())
			pStatus.setError(StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("HTTP Transfer failed. Reason: %1").arg(QString(*response.GetBody())));
		else
			pStatus.setError(StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("Transfer failed. HTTP error code: %1").arg(response.statusCode()));
		return;
	}

}


void UserStorageHTTPClient::OnExecDownLoadBackup(QString strFile,bool bOverWrite)
{
	Status err;
	DownLoadBackup(err,strFile,bOverWrite);
	emit OperationEnded(err.getErrorCode(),err.getErrorTextRaw());
}
void UserStorageHTTPClient::OnExecUpLoadBackup(QString strFile,bool bOverWrite)
{
	Status err;
	UpLoadBackup(err,strFile,bOverWrite);
	emit OperationEnded(err.getErrorCode(),err.getErrorTextRaw());
}
void UserStorageHTTPClient::OnExecDownLoadFile(QString strFile,bool bOverWrite)
{
	Status err;
	DownLoadFile(err,strFile,bOverWrite);
	emit OperationEnded(err.getErrorCode(),err.getErrorTextRaw());
}
void UserStorageHTTPClient::OnExecUpLoadFile(QString strFile,bool bOverWrite)
{
	Status err;
	UpLoadFile(err,strFile,bOverWrite);
	emit OperationEnded(err.getErrorCode(),err.getErrorTextRaw());
}
void UserStorageHTTPClient::OnExecClearFromUserStorage(QString strFile)
{
	Status err;
	ClearFromUserStorage(err,strFile);
	emit OperationEnded(err.getErrorCode(),err.getErrorTextRaw());
}
void UserStorageHTTPClient::OnCancelOperation()
{
	CancelOperation();
}




UserStorageHTTPClientThread::UserStorageHTTPClientThread()
: QThread(NULL)
{

}


/*!
Destructor: stops socket thread
*/
UserStorageHTTPClientThread::~UserStorageHTTPClientThread()
{
	emit SignalDestroy();				//signal socket to init auto-destruct sequence
	wait(100);
	if(isRunning())
	{
		quit();		//stop thread
		wait();
	}
}

void UserStorageHTTPClientThread::StartThread(const HTTPClientConnectionSettings& pConnection, const Authenticator &SessionCache)
{
	m_ConnSettings=pConnection;
	m_SessionCache=SessionCache;

	m_ThreadSync.ThreadSetForWait();
	start(); //start thread
	m_ThreadSync.ThreadWait(); //wait until thread is started

}

void UserStorageHTTPClientThread::run()
{
	UserStorageHTTPClient* pClient=new UserStorageHTTPClient();
	pClient->SetConnectionSettings(m_ConnSettings,m_SessionCache);

	connect(this,SIGNAL(SignalExecDownLoadBackup(QString,bool)),pClient,SLOT(OnExecDownLoadBackup(QString,bool)),Qt::QueuedConnection);
	connect(this,SIGNAL(SignalExecUpLoadBackup(QString,bool)),pClient,SLOT(OnExecUpLoadBackup(QString,bool)),Qt::QueuedConnection);
	connect(this,SIGNAL(SignalExecDownLoadFile(QString,bool)),pClient,SLOT(OnExecDownLoadFile(QString,bool)),Qt::QueuedConnection);
	connect(this,SIGNAL(SignalExecUpLoadFile(QString,bool)),pClient,SLOT(OnExecUpLoadFile(QString,bool)),Qt::QueuedConnection);
	connect(this,SIGNAL(SignalExecClearFromUserStorage(QString)),pClient,SLOT(OnExecClearFromUserStorage(QString)),Qt::QueuedConnection);
	
	connect(this,SIGNAL(SignalDestroy()),pClient,SLOT(deleteLater()));
	connect(this,SIGNAL(SignalAbortOperation()),pClient,SLOT(OnCancelOperation()));
	connect(pClient,SIGNAL(OperationEnded(int, QString)),this,SIGNAL(SignalOperationEnded(int, QString)));	
	connect(pClient,SIGNAL(SocketOperationInProgress(int,qint64,qint64)),this,SIGNAL(SocketOperationInProgress(int,qint64,qint64)));
	
	m_ThreadSync.ThreadRelease();
	exec(); 
}

void UserStorageHTTPClientThread::ExecDownLoadBackup(QString strFile,bool bOverWrite)
{
	emit SignalExecDownLoadBackup(strFile,bOverWrite);
}
void UserStorageHTTPClientThread::ExecUpLoadBackup(QString strFile,bool bOverWrite)
{
	emit SignalExecUpLoadBackup(strFile,bOverWrite);
}
void UserStorageHTTPClientThread::ExecDownLoadFile(QString strFile,bool bOverWrite)
{
	emit SignalExecDownLoadFile(strFile,bOverWrite);
}
void UserStorageHTTPClientThread::ExecUpLoadFile(QString strFile,bool bOverWrite)
{
	emit SignalExecUpLoadFile(strFile,bOverWrite);
}
void UserStorageHTTPClientThread::ExecClearFromUserStorage(QString strFile)
{
	emit SignalExecClearFromUserStorage(strFile);
}
void UserStorageHTTPClientThread::AbortOperation()
{
	emit SignalAbortOperation();
}
