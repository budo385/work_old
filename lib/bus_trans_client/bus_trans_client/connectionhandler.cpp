#include "connectionhandler.h"
#include "common/common/config.h"
#include "common/common/threadid.h"
#include "common/common/loggerabstract.h"
#include "trans/trans/httpserverping.h"

ConnectionHandler::ConnectionHandler()
:m_StubConnHandlerSet(NULL),m_Redirector(NULL),m_HttpClient(NULL),m_bLogged(false),m_nErrorTries(0),m_bFatalErrorState(false),m_IsAliveBkgTask(NULL)
{

}

//destroy dyn objects
ConnectionHandler::~ConnectionHandler()
{
	StopIsAlive();
	//Status err;
	//CloseAllThreadConnections(err);
	//Q_ASSERT(err.IsOK());
	if(m_Redirector!=NULL)delete m_Redirector;
	if(m_StubConnHandlerSet!=NULL)delete m_StubConnHandlerSet;
	if(m_HttpClient!=NULL)delete m_HttpClient;
}


/*!
	InitConnection: must be called first to init connection handler
	\param RPCType defines RPC protocol in use (defined in rpcprotocol.h)

	Removed from constructor for easier handling. No check is done afterwards, so dont forget to call this method from inherited class!!
*/

void ConnectionHandler::InitConnection(int RPCType)
{
	if (m_bFatalErrorState)return;

	//inherited class must set m_HttpClient and call this method afterwards
	Q_ASSERT_X(m_HttpClient!=NULL,"ConnectionHandler","Error HTTP client not initated");

	m_SessionCache.ClearSessionParameters();

	m_StubConnHandlerSet=new RpcStubConnHandlerSet(m_HttpClient,RPCType);	//init special stubber
	m_Redirector= new ConnectionRedirector(m_HttpClient,m_StubConnHandlerSet);					//init redirector

	//detect errors:
	connect(m_HttpClient,SIGNAL(SocketReportedError(int,QString)),this,SLOT(OnSocketError(int,QString)));

}


/*!
	Set's connection settings
	- if connected, disconnect
	- reinit ip redirector lists

	\param pConnection connection settings
	
*/

void ConnectionHandler::Priv_SetConnectionSettings(HTTPClientConnectionSettings& pConnection)
{

	if (m_bFatalErrorState)return;

	//sets new settings to http client
	m_HttpClient->SetConnectionSettings(pConnection);

	//init DFO:
	m_bUseDFO=pConnection.m_bUseDFO;

	//init redirector
	m_Redirector->InitializeRedirectList(pConnection.m_strLookupURL);

}










//-------------------------------------------------------------------------
//					PUBLIC INTERFACE
//-------------------------------------------------------------------------


/*!
	Logout, clears cached parameters.Access is serialized. No error handler

	\param pStatus returns error
*/
void ConnectionHandler::Priv_Logout(Status& pStatus)
{

	//wait for another thread:
	QMutexLocker locker(&m_Mutex); //lock access

	if (m_bFatalErrorState)
	{
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_CONNECTION_BROKEN);
		return;
	}

	int m_nErrorTries=0;

	//if not logged: return:
	if(!m_bLogged)
	{	//pStatus.setError(StatusCodeSet::ERR_SYSTEM_NOT_LOGGED);  
		return; 
	}


	//try logout:
	bool bTryAgain=false;
	do
	{
		m_StubConnHandlerSet->Logout(pStatus,m_SessionCache.GetSessionID());
		ErrorHandler(bTryAgain,pStatus); //if error redirect to error handler
	}
	while(bTryAgain);

	//if ok:
	if(pStatus.IsOK())
	{
		StopIsAlive();
		m_HttpClient->Disconnect(pStatus); //close connection
		m_SessionCache.ClearSessionParameters();//clear sess parameters
		m_bLogged=false;
	}

	TestForFatalError(pStatus);		//test for error
}



/*!
	Gets session ID
*/
QString ConnectionHandler:: Priv_GetSessionID()
{
	if (m_bFatalErrorState)return "";

	return m_SessionCache.GetSessionID();
}



	


/*!
	Logins to client, generates Auth. token, caches sessionid and auth params, if error detected try 
	to recover. Uses internal xml-stubs for some basic conn. handling functions (Login,Logout,IsAlive)
	Access is serialized.

	\param pStatus returns error
	\param pUsername username
	\param pPassword password
	\param strModuleCode		client module code
	\param strMLIID				MLIID
*/
void ConnectionHandler::Priv_Login(Status& pStatus, QString pUsername, QString pPassword,QString strModuleCode, QString strMLIID, QString strParentSession, int nClientTimeZoneOffsetMinutes)
{

	QMutexLocker locker(&m_Mutex); //lock access
	
	if (m_bFatalErrorState)
	{
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_CONNECTION_BROKEN);
		return;
	}


	int m_nErrorTries=0;

	
	//D' error:
	if(m_bLogged)
	{	pStatus.setError(StatusCodeSet::ERR_SYSTEM_ALREADY_LOGGED); return; }


	if(m_bUseDFO)m_Redirector->RebuildRedirectorListLocal();

	QByteArray clientNonce,clientAuthToken; //32bytes long cnonce & auth token
	m_SessionCache.m_nClientTimeZoneOffsetMinutes=nClientTimeZoneOffsetMinutes;
	m_SessionCache.SetSessionParameters(pUsername,pPassword,strModuleCode, strMLIID);//cache login params
	m_SessionCache.GenerateAuthenticationToken(clientNonce,clientAuthToken);//create cnonce

	//call server method:
	QString strSessionID;

	//error handler loop
	//----------------------------------------------------
	//clear status:
	pStatus.setError(0);
	bool bTryAgain=false;

	do
	{
		m_StubConnHandlerSet->Login(pStatus,strSessionID,pUsername,clientAuthToken,clientNonce,strModuleCode, strMLIID,strParentSession,false,nClientTimeZoneOffsetMinutes);
		ErrorHandler(bTryAgain,pStatus); //if error redirect to error handler

	}
	while(bTryAgain);

	//----------------------------------------------------

	//we have LOGIN:
	if (pStatus.IsOK())
	{
		m_SessionCache.SetSessionID(strSessionID);						//store new SESSION

		HTTPClientConnectionSettings conn=m_HttpClient->GetConnectionSettings(); //init new pinger:
		HTTPServerPing pinger;
		pinger.SetParameters(conn.m_strServerIP,conn.m_nPort,conn.m_bUseSSL,m_SessionCache);
		m_HttpClient->SetServerPingHandler(pinger);

		if(m_bUseDFO)m_Redirector->RebuildRedirectorListFromServer(strSessionID);	//rebuild redirector list
		m_bLogged=true;
		//qDebug()<<pinger.Ping();
	}

	
	if (pStatus.IsOK())
		StartIsAlive();


	

}


//not very accurate, but can be used to test condition from one thread (main GUI)
bool ConnectionHandler::Priv_IsSocketInUse()
{
	if (m_Mutex.tryLock())
	{
		m_Mutex.unlock();
		return false;
	}
	else
		return true;
}


/*!
	Sends RPC request to the server. Tries to recover from error if detected.

	\param pStatus returns error from transport layer and from server function
	\param msg contains HTTPRequest and HTTPResponse
	\param bAsyncCall if true then call is async
*/
void ConnectionHandler::Priv_SendRPC(Status& pStatus, RpcStubMessageHandler &msg)
{
	//qDebug()<<"Send rpc, thread: "<<ThreadIdentificator::GetCurrentThreadID() << msg.msg_in->GetBuffer()->left(100).constData();

	if (!m_Mutex.tryLock(5000)) //for multiple windows...when process request passes all events.. 5sec timeout...
	{
		pStatus.setError(1,tr("Data Transfer in progress! Can not complete request!"));
		return;
	}

	//m_Mutex.tryLock();
	//QMutexLocker locker(&m_Mutex); //lock access

	if (m_bFatalErrorState)
	{
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_CONNECTION_BROKEN);
		m_Mutex.unlock();
		return;
	}


	//D' error:
	if(!m_bLogged)
	{	pStatus.setError(StatusCodeSet::ERR_SYSTEM_NOT_LOGGED); 
		m_Mutex.unlock();
		return; 
	}


	//clear status:
	pStatus.setError(0);
	m_nErrorTries=0;

	//error handler loop
	//----------------------------------------------------

	bool bTryAgain=false;


#ifndef QT_NO_DEBUG
	QTime TestServerResponseTime;
	TestServerResponseTime.start();
#endif

	do
	{

		//send data to server:
		m_HttpClient->SendData(pStatus,&msg.HttpRequest,&msg.HttpResponse);
		
		//if error redirect to error handler	
		ErrorHandler(bTryAgain,pStatus); 
		if(bTryAgain) continue;
		
		//extract response, check for fault if first error is cleared
		if(pStatus.IsOK())
		{
			msg.msg_out->CheckForFault(pStatus);
			if (!pStatus.IsOK()){ ErrorHandler(bTryAgain,pStatus); continue;} 	//if fault detected try to recover, call errorhandler
		}
	}
	while(bTryAgain);
	
	//raise fatal error
	TestForFatalError(pStatus);		//test for error

	//qDebug()<<"End Send rpc, thread: "<<ThreadIdentificator::GetCurrentThreadID();

	m_Mutex.unlock();

	

}	








//-------------------------------------------------------------------------
//					ERROR HANDLER
//-------------------------------------------------------------------------

/*!
	Tries to recover from error. Recursively call itself, maximum is defined by MAX_CLIENT_ERROR_RECOVER_ATTEMPS
	Can handle this type of errors:
	-TIMEOUT	(transport) ERR_HTTP_OPERATION_TIMEOUT
	-REDIRECT	(transport) ERR_SYSTEM_REDIRECT
	-REAUTHENTICATE	(session) ERR_SYSTEM_REAUTHENTICATE
	-SESSION EXPIRE (session) ERR_SYSTEM_SESSION_EXPIRED

	\param bTryAgain	- returns flag indicating caller to try again last op
	\param pStatus		- returns error
*/
void ConnectionHandler::ErrorHandler(bool &bTryAgain,Status &pStatus)
{


	//is pStatus OK then ok:
	if(pStatus.IsOK()){bTryAgain=false;return;}

	//if DFO disabled return
	if(!m_bUseDFO){bTryAgain=false;return;}

	//reset try again flag: if not match it will exit from here
	bTryAgain=false;
	

	//test error try counter:
	m_nErrorTries++;
	if (m_nErrorTries>=MAX_CLIENT_ERROR_RECOVER_ATTEMPS){bTryAgain=false; return;}

	//try to detect error:
	switch(pStatus.getErrorCode())
	{

		//system errors: firewall, adsl, wrong IP, app. server is down:
		case StatusCodeSet::ERR_HTTP_FAILED_CONNECT:
		case StatusCodeSet::ERR_HTTP_HOST_NOT_FOUND:
		case StatusCodeSet::ERR_HTTP_OPERATION_TIMEOUT:
		case StatusCodeSet::ERR_HTTP_HOST_CLOSED:
			
			//if( m_nErrorTries==1)
			//	{bTryAgain=true; return;}	//try again with same operation for second time
			//else
			ShowDFOMessage(pStatus);
			m_Redirector->RedirectConnection(pStatus); 
			if(pStatus.IsOK())bTryAgain=true; //if redirection sucess, try again
			break;

		case StatusCodeSet::ERR_SYSTEM_REDIRECT:
			ShowDFOMessage(pStatus,tr("Server issued redirected command. Redirecting to: ")+pStatus.getErrorTextRaw());
			m_Redirector->RedirectConnection(pStatus); //single shot
			if(pStatus.IsOK())bTryAgain=true; //if redirection sucess, try again
			break;

		case StatusCodeSet::ERR_SYSTEM_REAUTHENTICATE:
			ShowDFOMessage(pStatus,tr("Server issued Re-authentication command! "));
			errReauthenticate(pStatus); 
			if(pStatus.IsOK())
			{
				ApplicationLogger::logMessage(StatusCodeSet::TYPE_INFORMATION,0,tr("Re-authentication process is done. Connection to server successfully reestablished!"));
				bTryAgain=true; //if reauthentication fails, return
			}
			break;

		case StatusCodeSet::ERR_SYSTEM_SESSION_EXPIRED:
			//unrecoverable error
			errSessionExpired(pStatus); 
			bTryAgain=false; 
			break; //unrecoverable error
	}



}


/*!
	Tries to re-login with cached parameters: session ID MUST be preserved

	\param pStatus returns error
*/
void ConnectionHandler::errReauthenticate(Status& pStatus)
{


	QString strUsername,strPass,strSessionID,strModuleCode, strMLIID;
	QByteArray clientNonce,clientAuthToken; //32bytes long cnonce & auth token
	
	m_SessionCache.GetSessionParameters(strUsername,strPass,strModuleCode, strMLIID);
	m_SessionCache.GenerateAuthenticationToken(clientNonce,clientAuthToken);//create cnonce
	strSessionID=m_SessionCache.GetSessionID();

	//call server method:
	m_StubConnHandlerSet->Login(pStatus,strSessionID,strUsername,clientAuthToken,clientNonce,strModuleCode, strMLIID,"",false,m_SessionCache.m_nClientTimeZoneOffsetMinutes);

}

/*!
	New session must be fetched:relogin but this can-not be done at this level!
	Because if user was holding some locks, those locks are lost: GUI action is needed!
	Simple solution: clear cache, return error to caller!

	\param pStatus returns error
*/

void ConnectionHandler::errSessionExpired(Status& pStatus)
{
	
	m_SessionCache.ClearSessionParameters();
	m_nErrorTries=MAX_CLIENT_ERROR_RECOVER_ATTEMPS;			//block any further error call
}




void ConnectionHandler::ShowDFOMessage(Status pStatus, QString strExtrMsg)
{
	QString strMessage;
	if (strExtrMsg.isEmpty())
		strMessage=tr("Dynamic fail over in progress. Reason: ")+pStatus.getErrorText();
	else
		strMessage=tr("Dynamic fail over in progress. Reason: ")+strExtrMsg;
	
	ApplicationLogger::logMessage(StatusCodeSet::TYPE_WARNING,0,strMessage);
	QApplication::processEvents(QEventLoop::ExcludeUserInputEvents);
}






void ConnectionHandler::OnSocketError(int nErrorCode,QString rawText)
{
	//already processing fatal error: discard any socket messages that might be left in que
	if(m_bFatalErrorState) return;

	//try lock: if already locked, just exit to prevent deadlock: at next network call, redirection will be done
	if (!m_Mutex.tryLock()) return;

	//LOG: broken link
	Status err;
	err.setError(nErrorCode,rawText);

	//ok: we have error, now try to reanimate connection (DFO):
	if(m_bUseDFO)
	{
		ShowDFOMessage(err);
		err.setError(0); //reset to be safe
		m_Redirector->RedirectConnection(err); //this call will loop until max__allowed redirections
		if (err.IsOK())
		{
			errReauthenticate(err);		//after new connection, reauthenticate session
			if (err.IsOK())
			{
				m_Mutex.unlock();
				return;
			}
		}
	}

	ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,nErrorCode,tr("Dynamic fail over was unable to reestablished connection. Session is terminated."));
	m_bFatalErrorState=true;
	StopIsAlive();
	m_Mutex.unlock();				//release right now: to avoid deadlock,m_bFatalErrorState flag is enough to block access
	emit RaiseFatalError(nErrorCode);
	
}




//error will be displayed: just emit FatalError signal: clear all FUI, clear variables, do not logout
//call this private only when under lock
void ConnectionHandler::TestForFatalError(Status pStatus)
{
	if (pStatus.IsOK()) return; //if all ok

	if (pStatus.getErrorCode()==StatusCodeSet::ERR_HTTP_USER_OPERATION_ABORTED)
		return;

	//intercept all fatal errors  (HTTP connection && session (expired, etc..))
	int nCategory=StatusCodeSet::getMessageCategoryID(pStatus.getErrorCode());
	if (nCategory==StatusCodeSet::CATEGORY_NETWORK ||pStatus.getErrorCode() == StatusCodeSet::ERR_SYSTEM_SESSION_EXPIRED) 
	{
		m_bFatalErrorState=true;
		//pStatus.setError(StatusCodeSet::ERR_SYSTEM_CONNECTION_BROKEN);	//reset error to avoid..
		StopIsAlive();
		emit RaiseFatalError(pStatus.getErrorCode());
	}
}


ConnectionHandler * ConnectionHandler::GetConnectionByThread()
{
	QReadLocker lock(&m_hshConnectionsLock);
	ConnectionHandler *p=m_hshConnections.value(ThreadIdentificator::GetCurrentThreadID(),NULL);

	if (p==NULL)
		return this;
	else
		return p;
}

void ConnectionHandler::GetConnectionSettings(HTTPClientConnectionSettings& pConnection, Authenticator &pSessionCache)
{
	GetConnectionByThread()->Priv_GetConnectionSettings(pConnection, pSessionCache);
}
void ConnectionHandler::Priv_GetConnectionSettings(HTTPClientConnectionSettings& pConnection, Authenticator &pSessionCache)
{
	pConnection=m_HttpClient->GetConnectionSettings();
	pSessionCache=m_SessionCache;
}


void ConnectionHandler::Login(Status& pStatus, QString pUsername, QString pPassword,QString strModuleCode, QString strMLIID, QString strParentSession, int nClientTimeZoneOffsetMinutes)
{
	GetConnectionByThread()->Priv_Login(pStatus,pUsername,pPassword,strModuleCode,strMLIID,strParentSession,nClientTimeZoneOffsetMinutes);
}

void ConnectionHandler::Logout(Status& pStatus)
{
	GetConnectionByThread()->Priv_Logout(pStatus);
}

bool ConnectionHandler::IsLogged()
{
	return GetConnectionByThread()->Priv_IsLogged();
}

QString ConnectionHandler::GetSessionID()
{
	return GetConnectionByThread()->Priv_GetSessionID();
}
RpcHTTPClient* ConnectionHandler::GetRpcHttpClient()
{
	return GetConnectionByThread()->Priv_GetRpcHttpClient();

}
RpcSkeletonServerMessageSet*	ConnectionHandler::GetSkeletonSet()
{
	return GetConnectionByThread()->Priv_GetSkeletonSet();
}
void ConnectionHandler::SetConnectionSettings(HTTPClientConnectionSettings& pConnection)
{
	GetConnectionByThread()->Priv_SetConnectionSettings(pConnection);
}
void ConnectionHandler::SendRPC(Status& pStatus, RpcStubMessageHandler &msg)
{
	ConnectionHandler *p=GetConnectionByThread();
//#ifdef _DEBUG
	//qDebug()<<ThreadIdentificator::GetCurrentThreadID()<<" send RPC m_Handler="<<p;
//#endif
	p->Priv_SendRPC(pStatus,msg);
}
bool ConnectionHandler::IsSocketInUse()
{
	return GetConnectionByThread()->Priv_IsSocketInUse();
}
void ConnectionHandler::Priv_IsAlive(Status &pStatus)
{
//#ifdef _DEBUG
//	qDebug()<<"Is Alive, thread: "<<ThreadIdentificator::GetCurrentThreadID();
//	qDebug()<<"Is Alive, m_Handler="<<this;
//#endif

	if(Priv_IsSocketInUse())
		return;
	m_StubConnHandlerSet->IsAlive(pStatus,m_SessionCache.GetSessionID());
}
void ConnectionHandler::CloseAllThreadConnections(Status& pStatus)
{
	QReadLocker lock(&m_hshConnectionsLock);
	QHash<int,ConnectionHandler *>	tmp_hshConnections=m_hshConnections; //copy hash to temp as we are about to delete hash..
	lock.unlock();
	QHashIterator<int,ConnectionHandler *> i(tmp_hshConnections);

	//loop: store only last err
	while (i.hasNext()) 
	{
		i.next();
		Status err;
		CloseThreadConnection(err,i.key());
		if (!err.IsOK())
			pStatus=err;
	}
}

void ConnectionHandler::StopIsAlive()
{
	if (m_IsAliveBkgTask) 
	{
		m_IsAliveBkgTask->Stop();  
		delete m_IsAliveBkgTask;
		m_IsAliveBkgTask=NULL;
	}
}

void ConnectionHandler::StartIsAlive()
{
//#ifdef _DEBUG
//	qDebug()<<"StartIsAlive, thread: "<<ThreadIdentificator::GetCurrentThreadID();
//	qDebug()<<"m_Handler="<<this;
//#endif

	StopIsAlive();
	IsAlive_Client *pIsAlive = new IsAlive_Client;
	pIsAlive->m_Handler=this;
	m_IsAliveBkgTask = new BackgroundTaskExecutor(pIsAlive,NULL,CLIENT_IS_ALIVE_INTERVAL*1000,true,true);
	m_IsAliveBkgTask->Start();
}

//report to server:
void IsAlive_Client::ExecuteTask(int nCallerID,int nTaskID,QString strData)
{
	if (!m_Handler)
		return;
//#ifdef _DEBUG
//	qDebug()<<"IsAlive ExecuteTask m_Handler="<<this;
//#endif
	Status err;
	m_Handler->Priv_IsAlive(err); //pointer is direct link to another connection
	if(!err.IsOK())
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_CLIENT_ISALIVE_FAILED,err.getErrorText());
}


