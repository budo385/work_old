#ifndef RPCSTUB_BUSNMRX_H__
#define RPCSTUB_BUSNMRX_H__

#include "common/common/status.h"
#include "bus_interface/bus_interface/interface_busnmrx.h"
#include "bus_trans_client/bus_trans_client/connectionhandler.h"
#include "trans/trans/rpcstubmessagehandler.h"

class RpcStub_BusNMRX: public Interface_BusNMRX, public RpcStub
{
public:
	RpcStub_BusNMRX(ConnectionHandler *pConnectionHandler,int RPCType):
		RpcStub(RPCType){m_ConnectionHandler=pConnectionHandler;}

	void ReadOnePair(Status &Ret_pStatus, int nM_TableID,int nN_TableID, int nKey1,int nKey2,DbRecordSet &Ret_Data,int nX_TableID=-1);
	void ReadMultiPairs(Status &Ret_pStatus, DbRecordSet &DataPairs, DbRecordSet &Ret_Data,int nX_TableID=-1);
	void Write(Status &Ret_pStatus, DbRecordSet &RetOut_Data,QString strLockRes,int nX_TableID=-1);
	void WriteSimple(Status &Ret_pStatus, DbRecordSet &RetOut_Data,QString strLockRes,int nX_TableID=-1);
	void Delete(Status &Ret_pStatus, DbRecordSet &DataForDelete);
	void Lock(Status &Ret_pStatus, int nNMRX_ID,QString &Ret_strLockRes);
	void LockMulti(Status &Ret_pStatus, DbRecordSet lstLockData,QString &Ret_strLockRes);
	void Unlock(Status &Ret_pStatus, QString strLockRes);
	void WriteRelations(Status &Ret_pStatus, DbRecordSet lstRelations, int nTable1_ID, int nTable2_ID);
	void WriteCEContactLink(Status &Ret_pStatus, DbRecordSet lstCeIDs, DbRecordSet &RetOut_Data);
	void WriteCEProjectLink(Status &Ret_pStatus, DbRecordSet lstCeIDs, DbRecordSet &RetOut_Data);
	void CreateAssignmentData(Status &Ret_pStatus,DbRecordSet &Ret_lstNMRXData,int nMasterTableID,int nAssigmentTableID,DbRecordSet &lstIDForAssign, QString strDefaultRoleName,int nMasterTableRecordID,int nX_TableID, DbRecordSet &lstPersons,bool &Ret_bInserted);
	void ReadRole(Status &Ret_pStatus, int nN_TableID,int nM_TableID, DbRecordSet &Ret_Data);
	void WriteRole(Status &Ret_pStatus, DbRecordSet &RetOut_Data,DbRecordSet &DataForDelete,QString strLockRes);
	void DeleteRole(Status &Ret_pStatus, DbRecordSet &DataForDelete);
	void LockRole(Status &Ret_pStatus, int nN_TableID,int nM_TableID, QString &Ret_strLockRes);
	void UnlockRole(Status &Ret_pStatus, QString strLockRes);
	void CheckRole(Status &Ret_pStatus, int nN_TableID,int nM_TableID,QString strRoleName, DbRecordSet &Ret_Data, bool &Ret_bRoleInserted);

private:
	ConnectionHandler *m_ConnectionHandler; ///< connection handler
};

#endif	// RPCSTUB_BUSNMRX_H__
