#ifndef RPCSTUB_REPORTS_H__
#define RPCSTUB_REPORTS_H__

#include "common/common/status.h"
#include "bus_interface/bus_interface/interface_reports.h"
#include "bus_trans_client/bus_trans_client/connectionhandler.h"
#include "trans/trans/rpcstubmessagehandler.h"

class RpcStub_Reports: public Interface_Reports, public RpcStub
{
public:
	RpcStub_Reports(ConnectionHandler *pConnectionHandler,int RPCType):
		RpcStub(RPCType){m_ConnectionHandler=pConnectionHandler;}

	void GetReportRecordSet(Status &Ret_pStatus, DbRecordSet &Ret_pReportRecordSet, int nViewID, QString strWhere);

private:
	ConnectionHandler *m_ConnectionHandler; ///< connection handler
};

#endif	// RPCSTUB_REPORTS_H__
