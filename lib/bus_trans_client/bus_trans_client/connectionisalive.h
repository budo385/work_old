#ifndef CONNISALIVE_H
#define CONNISALIVE_H


#include <QtCore>
#include "common/common/status.h"
#include "rpcstub_connhandlerset.h"



/*!
    \class ConnectionIsAlive
    \brief Used inside ConnectionHandler for reporting it's status to server in regular intervals
    \ingroup Bus_Trans_Client
*/

class ConnectionIsAlive: private QObject
{
     Q_OBJECT

public:

	ConnectionIsAlive(RpcStubConnHandlerSet *pStubConnHandlerSet);

	void InitIsAlive(QString strSessionID,int nIsAliveInterval);
	void StartIsAlive();
	void StopIsAlive();

private:
	void timerEvent(QTimerEvent *event); //overriden QObject timer method
	
	RpcStubConnHandlerSet *m_StubConnHandlerSet;	///<pointer to client stub set
	int m_nIsAliveInterval;							///<interval in msec
	int m_nTimerID;									///<timer id
	QString m_strSessionID;							///<session id

};


#endif //CONNISALIVE_H
