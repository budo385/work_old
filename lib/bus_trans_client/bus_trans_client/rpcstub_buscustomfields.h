#ifndef RPCSTUB_BUSCUSTOMFIELDS_H__
#define RPCSTUB_BUSCUSTOMFIELDS_H__

#include "common/common/status.h"
#include "bus_interface/bus_interface/interface_buscustomfields.h"
#include "bus_trans_client/bus_trans_client/connectionhandler.h"
#include "trans/trans/rpcstubmessagehandler.h"

class RpcStub_BusCustomFields: public Interface_BusCustomFields, public RpcStub
{
public:
	RpcStub_BusCustomFields(ConnectionHandler *pConnectionHandler,int RPCType):
		RpcStub(RPCType){m_ConnectionHandler=pConnectionHandler;}

	void Write(Status &Ret_pStatus, DbRecordSet &RetOut_Data,DbRecordSet &RetOut_Selections,QString strLockedRes);
	void Read(Status &Ret_pStatus, int nCustomFieldID, DbRecordSet &Ret_Data,DbRecordSet &Ret_Selections);
	void ReadFields(Status &Ret_pStatus, int nTableID, DbRecordSet &Ret_Data);
	void ReadUserCustomData(Status &Ret_pStatus, int nTableID, int nRecordID, DbRecordSet &Ret_Data);
	void WriteUserCustomData(Status &Ret_pStatus, int nTableID, int nRecordID, DbRecordSet &RetOut_Data);
	void ReadUserCustomDataForInsert(Status &Ret_pStatus,int nTableID, DbRecordSet &Ret_Data);

private:
	ConnectionHandler *m_ConnectionHandler; ///< connection handler
};

#endif	// RPCSTUB_BUSCUSTOMFIELDS_H__
