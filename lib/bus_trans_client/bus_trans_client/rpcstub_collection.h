#ifndef RPCSTUBCOLLECTION_H
#define RPCSTUBCOLLECTION_H


//-----------------------------------------------------------
//Holds header for all RPC stubs, register your stub here:
//-----------------------------------------------------------

#include "rpcstub_userlogon.h"
#include "rpcstub_modulelicense.h"
#include "rpcstub_buseventlog.h"
#include "rpcstub_accessrights.h"
#include "rpcstub_coreservices.h"
#include "rpcstub_buscontact.h"
#include "rpcstub_busaddressschemas.h"
#include "rpcstub_mainentityselector.h"
#include "rpcstub_reports.h"
#include "rpcstub_voicecallcenter.h"
#include "rpcstub_busemail.h"
#include "rpcstub_clientsimpleorm.h"
#include "rpcstub_busimport.h"
#include "rpcstub_storedprojectlists.h"
#include "rpcstub_busgridviews.h"
#include "rpcstub_busnmrx.h"
#include "rpcstub_busgrouptree.h"
#include "rpcstub_busdocuments.h"
#include "rpcstub_buscommunication.h"
#include "rpcstub_servercontrol.h"
#include "rpcstub_busproject.h"
#include "rpcstub_busperson.h"
#include "rpcstub_bussms.h"
#include "rpcstub_buscalendar.h"
#include "rpcstub_buscustomfields.h"

#endif //RPCSTUBCOLLECTION_H