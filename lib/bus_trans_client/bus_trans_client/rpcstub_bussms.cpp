#include "rpcstub_bussms.h"

void RpcStub_BusSms::ReadSms(Status &Ret_pStatus, int nVoiceCallID,DbRecordSet &Ret_CallData, DbRecordSet &Ret_UAR,DbRecordSet &Ret_GAR)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&nVoiceCallID,"nVoiceCallID");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusSms.ReadSms",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&Ret_CallData,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(1,&Ret_UAR,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(2,&Ret_GAR,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusSms::WriteSms(Status &Ret_pStatus, DbRecordSet &RetOut_CallData, QString strLockRes,DbRecordSet &RetOut_ScheduleTask,DbRecordSet &RetOut_UAR,DbRecordSet &RetOut_GAR)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&RetOut_CallData,"RetOut_CallData");
	msg.msg_in->AddParameter(&strLockRes,"strLockRes");
	msg.msg_in->AddParameter(&RetOut_ScheduleTask,"RetOut_ScheduleTask");
	msg.msg_in->AddParameter(&RetOut_UAR,"RetOut_UAR");
	msg.msg_in->AddParameter(&RetOut_GAR,"RetOut_GAR");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusSms.WriteSms",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&RetOut_CallData,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(1,&RetOut_ScheduleTask,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(2,&RetOut_UAR,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(3,&RetOut_GAR,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusSms::ListSmsMessages(Status &Ret_pStatus, DbRecordSet &RetOut_lstCallData, QDate &datDate, int nOwnerID)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&RetOut_lstCallData,"RetOut_lstCallData");
	msg.msg_in->AddParameter(&datDate,"datDate");
	msg.msg_in->AddParameter(&nOwnerID,"nOwnerID");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusSms.ListSmsMessages",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&RetOut_lstCallData,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusSms::DeleteSmsMessages(Status &Ret_pStatus, DbRecordSet &RetOut_lstIDs)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&RetOut_lstIDs,"RetOut_lstIDs");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusSms.DeleteSmsMessages",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&RetOut_lstIDs,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

