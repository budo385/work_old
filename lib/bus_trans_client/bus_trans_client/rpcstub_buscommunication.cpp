#include "rpcstub_buscommunication.h"

void RpcStub_BusCommunication::SaveCommFilterViews(Status &Ret_pStatus, int &Ret_nViewID, DbRecordSet &recView, DbRecordSet &recViewsData)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&recView,"recView");
	msg.msg_in->AddParameter(&recViewsData,"recViewsData");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusCommunication.SaveCommFilterViews",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&Ret_nViewID,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusCommunication::GetDefaultDesktopFilterViews(Status &Ret_pStatus, int nLoggedPersonID, DbRecordSet &RetOut_recViews)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&nLoggedPersonID,"nLoggedPersonID");
	msg.msg_in->AddParameter(&RetOut_recViews,"RetOut_recViews");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusCommunication.GetDefaultDesktopFilterViews",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&RetOut_recViews,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusCommunication::GetCommFilterViews(Status &Ret_pStatus, DbRecordSet &RetOut_recViews, int nPersonID, int nGridType)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&RetOut_recViews,"RetOut_recViews");
	msg.msg_in->AddParameter(&nPersonID,"nPersonID");
	msg.msg_in->AddParameter(&nGridType,"nGridType");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusCommunication.GetCommFilterViews",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&RetOut_recViews,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusCommunication::GetCommFilterData(Status &Ret_pStatus, DbRecordSet &RetOut_recSortRecordSet, DbRecordSet &RetOut_recViewsData, DbRecordSet &RetOut_recViewsChBoxesData, int nViewID, int nLoggedPersonID)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&RetOut_recSortRecordSet,"RetOut_recSortRecordSet");
	msg.msg_in->AddParameter(&RetOut_recViewsData,"RetOut_recViewsData");
	msg.msg_in->AddParameter(&RetOut_recViewsChBoxesData,"RetOut_recViewsChBoxesData");
	msg.msg_in->AddParameter(&nViewID,"nViewID");
	msg.msg_in->AddParameter(&nLoggedPersonID,"nLoggedPersonID");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusCommunication.GetCommFilterData",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&RetOut_recSortRecordSet,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(1,&RetOut_recViewsData,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(2,&RetOut_recViewsChBoxesData,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusCommunication::GetCommFilterViewDataAndViews(Status &Ret_pStatus, DbRecordSet &RetOut_recViewData, DbRecordSet &RetOut_recViews, int nViewID, int nGridType)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&RetOut_recViewData,"RetOut_recViewData");
	msg.msg_in->AddParameter(&RetOut_recViews,"RetOut_recViews");
	msg.msg_in->AddParameter(&nViewID,"nViewID");
	msg.msg_in->AddParameter(&nGridType,"nGridType");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusCommunication.GetCommFilterViewDataAndViews",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&RetOut_recViewData,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(1,&RetOut_recViews,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusCommunication::GetGridViewForContact(Status &Ret_pStatus, int &RetOut_nViewID, int nContactID)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&RetOut_nViewID,"RetOut_nViewID");
	msg.msg_in->AddParameter(&nContactID,"nContactID");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusCommunication.GetGridViewForContact",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&RetOut_nViewID,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusCommunication::GetGridViewForProject(Status &Ret_pStatus, int &RetOut_nViewID, int nProjectID)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&RetOut_nViewID,"RetOut_nViewID");
	msg.msg_in->AddParameter(&nProjectID,"nProjectID");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusCommunication.GetGridViewForProject",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&RetOut_nViewID,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusCommunication::GetPersonCommData(Status &Ret_pStatus, bool &Ret_bRowsStripped, DbRecordSet &RetOut_recEmails, DbRecordSet &RetOut_recDocuments, DbRecordSet &RetOut_recVoiceCalls, int PersonID, DbRecordSet recFilter, bool bForceFilterByTaskType )
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&RetOut_recEmails,"RetOut_recEmails");
	msg.msg_in->AddParameter(&RetOut_recDocuments,"RetOut_recDocuments");
	msg.msg_in->AddParameter(&RetOut_recVoiceCalls,"RetOut_recVoiceCalls");
	msg.msg_in->AddParameter(&PersonID,"PersonID");
	msg.msg_in->AddParameter(&recFilter,"recFilter");
	msg.msg_in->AddParameter(&bForceFilterByTaskType,"bForceFilterByTaskType");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusCommunication.GetPersonCommData",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&Ret_bRowsStripped,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(1,&RetOut_recEmails,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(2,&RetOut_recDocuments,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(3,&RetOut_recVoiceCalls,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusCommunication::GetContactCommData(Status &Ret_pStatus, DbRecordSet &RetOut_recEmails, DbRecordSet &RetOut_recDocuments, DbRecordSet &RetOut_recVoiceCalls, int ContactID, DbRecordSet recFilter)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&RetOut_recEmails,"RetOut_recEmails");
	msg.msg_in->AddParameter(&RetOut_recDocuments,"RetOut_recDocuments");
	msg.msg_in->AddParameter(&RetOut_recVoiceCalls,"RetOut_recVoiceCalls");
	msg.msg_in->AddParameter(&ContactID,"ContactID");
	msg.msg_in->AddParameter(&recFilter,"recFilter");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusCommunication.GetContactCommData",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&RetOut_recEmails,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(1,&RetOut_recDocuments,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(2,&RetOut_recVoiceCalls,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusCommunication::GetProjectCommData(Status &Ret_pStatus, DbRecordSet &RetOut_recEmails, DbRecordSet &RetOut_recDocuments, DbRecordSet &RetOut_recVoiceCalls, int nProjectID, DbRecordSet recFilter)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&RetOut_recEmails,"RetOut_recEmails");
	msg.msg_in->AddParameter(&RetOut_recDocuments,"RetOut_recDocuments");
	msg.msg_in->AddParameter(&RetOut_recVoiceCalls,"RetOut_recVoiceCalls");
	msg.msg_in->AddParameter(&nProjectID,"nProjectID");
	msg.msg_in->AddParameter(&recFilter,"recFilter");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusCommunication.GetProjectCommData",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&RetOut_recEmails,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(1,&RetOut_recDocuments,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(2,&RetOut_recVoiceCalls,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusCommunication::GetCalendarCommData(Status &Ret_pStatus, DbRecordSet &RetOut_recEmails, DbRecordSet &RetOut_recDocuments, DbRecordSet &RetOut_recVoiceCalls, DbRecordSet recEntities, DbRecordSet recFilter)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&RetOut_recEmails,"RetOut_recEmails");
	msg.msg_in->AddParameter(&RetOut_recDocuments,"RetOut_recDocuments");
	msg.msg_in->AddParameter(&RetOut_recVoiceCalls,"RetOut_recVoiceCalls");
	msg.msg_in->AddParameter(&recEntities,"recEntities");
	msg.msg_in->AddParameter(&recFilter,"recFilter");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusCommunication.GetCalendarCommData",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&RetOut_recEmails,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(1,&RetOut_recDocuments,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(2,&RetOut_recVoiceCalls,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusCommunication::GetCalendarGridCommData(Status &Ret_pStatus, DbRecordSet &RetOut_recEmails, DbRecordSet &RetOut_recDocuments, DbRecordSet &RetOut_recVoiceCalls, DbRecordSet recEntities, DbRecordSet recFilter)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&RetOut_recEmails,"RetOut_recEmails");
	msg.msg_in->AddParameter(&RetOut_recDocuments,"RetOut_recDocuments");
	msg.msg_in->AddParameter(&RetOut_recVoiceCalls,"RetOut_recVoiceCalls");
	msg.msg_in->AddParameter(&recEntities,"recEntities");
	msg.msg_in->AddParameter(&recFilter,"recFilter");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusCommunication.GetCalendarGridCommData",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&RetOut_recEmails,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(1,&RetOut_recDocuments,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(2,&RetOut_recVoiceCalls,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusCommunication::GetCommGridDataByIDs(Status &Ret_pStatus, DbRecordSet &RetOut_recData, DbRecordSet recIDsRecordset, int nGridType, int nEntityType)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&RetOut_recData,"RetOut_recData");
	msg.msg_in->AddParameter(&recIDsRecordset,"recIDsRecordset");
	msg.msg_in->AddParameter(&nGridType,"nGridType");
	msg.msg_in->AddParameter(&nEntityType,"nEntityType");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusCommunication.GetCommGridDataByIDs",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&RetOut_recData,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusCommunication::GetComplexFilterData(Status &Ret_pStatus, DbRecordSet &RetOut_recFilterData, int nPersonID)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&RetOut_recFilterData,"RetOut_recFilterData");
	msg.msg_in->AddParameter(&nPersonID,"nPersonID");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusCommunication.GetComplexFilterData",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&RetOut_recFilterData,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusCommunication::GetContactFromEmail(Status &Ret_pStatus, DbRecordSet &RetOut_lstData)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&RetOut_lstData,"RetOut_lstData");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusCommunication.GetContactFromEmail",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&RetOut_lstData,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusCommunication::SetTaskIsDone(Status &Ret_pStatus, DbRecordSet &lstData)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&lstData,"lstData");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusCommunication.SetTaskIsDone",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
}

void RpcStub_BusCommunication::PostPoneTasks(Status &Ret_pStatus, DbRecordSet &lstData, QDateTime datDueDateTime, QDateTime datStartDateTime)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&lstData,"lstData");
	msg.msg_in->AddParameter(&datDueDateTime,"datDueDateTime");
	msg.msg_in->AddParameter(&datStartDateTime,"datStartDateTime");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusCommunication.PostPoneTasks",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
}

void RpcStub_BusCommunication::SetEmailsRead(Status &Ret_pStatus, DbRecordSet &lstData)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&lstData,"lstData");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusCommunication.SetEmailsRead",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
}

void RpcStub_BusCommunication::ChangeEntityAssigment(Status &Ret_pStatus, int nAssignedType,int nAssignedID, int nOperation, DbRecordSet &Data)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&nAssignedType,"nAssignedType");
	msg.msg_in->AddParameter(&nAssignedID,"nAssignedID");
	msg.msg_in->AddParameter(&nOperation,"nOperation");
	msg.msg_in->AddParameter(&Data,"Data");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusCommunication.ChangeEntityAssigment",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
}

void RpcStub_BusCommunication::ReadCEMenuData(Status &Ret_pStatus, int nPersonID, DbRecordSet &Ret_DocTemplate, DbRecordSet &Ret_DocApps, DbRecordSet &Ret_DocCheckOut,DbRecordSet &Ret_EmailTemplate)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&nPersonID,"nPersonID");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusCommunication.ReadCEMenuData",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&Ret_DocTemplate,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(1,&Ret_DocApps,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(2,&Ret_DocCheckOut,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(3,&Ret_EmailTemplate,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusCommunication::AssignCommDataToContact(Status &Ret_pStatus, DbRecordSet &Data, int nContactID)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&Data,"Data");
	msg.msg_in->AddParameter(&nContactID,"nContactID");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusCommunication.AssignCommDataToContact",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
}

void RpcStub_BusCommunication::AssignCommDataToProject(Status &Ret_pStatus, DbRecordSet &Data, int nProjectID)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&Data,"Data");
	msg.msg_in->AddParameter(&nProjectID,"nProjectID");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusCommunication.AssignCommDataToProject",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
}

void RpcStub_BusCommunication::CheckIfRecordExists(Status &Ret_pStatus, int nEntityType, int nRecordID, bool &Ret_bExists)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&nEntityType,"nEntityType");
	msg.msg_in->AddParameter(&nRecordID,"nRecordID");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusCommunication.CheckIfRecordExists",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&Ret_bExists,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

