#ifndef RPCSKELETONSERVERMESSAGESET_H
#define RPCSKELETONSERVERMESSAGESET_H


#include <QtCore>
#include <QDebug>
#include "common/common/status.h"
#include "trans/trans/rpcskeleton.h"
#include "trans/trans/rpcskeletonmessagehandler.h"


/*!
    \class RpcSkeletonServerMessageSet
    \brief Client side skeleton set for server messages (ASYNC)
    \ingroup Bus_Trans_Client
*/

class RpcSkeletonServerMessageSet : public QObject, public RpcSkeleton
{

	Q_OBJECT 

public:
	RpcSkeletonServerMessageSet(int RPCType);

	//put your server msg skeleton here
	//skeleton function for each server method 
	void ServerMsg(RpcSkeletonMessageHandler &rpc);

	//implement this function:
	bool HandleRPC(Status &err,QByteArray *pBufRequest,QByteArray *pBufResponse, QString strRPCMethod, int nClientTimeZoneOffsetMinutes=0);

	//for each server handler make signal:
signals:
	void Signal_ServerMsg(DbRecordSet recMsg);


};



#endif //RPCSKELETONSERVERMESSAGESET_H
