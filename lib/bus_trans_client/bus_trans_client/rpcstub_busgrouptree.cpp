#include "rpcstub_busgrouptree.h"

void RpcStub_BusGroupTree::ReadTree(Status &Ret_pStatus, int nGroupEntityTypeID,DbRecordSet &Ret_Data)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&nGroupEntityTypeID,"nGroupEntityTypeID");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusGroupTree.ReadTree",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&Ret_Data,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusGroupTree::WriteTree(Status &Ret_pStatus, DbRecordSet &RetOut_Data,QString strLockRes)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&RetOut_Data,"RetOut_Data");
	msg.msg_in->AddParameter(&strLockRes,"strLockRes");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusGroupTree.WriteTree",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&RetOut_Data,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusGroupTree::DeleteTree(Status &Ret_pStatus, int nTreeID)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&nTreeID,"nTreeID");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusGroupTree.DeleteTree",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
}

void RpcStub_BusGroupTree::LockTree(Status &Ret_pStatus, int nTreeID,QString &Ret_strLockRes)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&nTreeID,"nTreeID");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusGroupTree.LockTree",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&Ret_strLockRes,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusGroupTree::UnLockTree(Status &Ret_pStatus, QString strLockRes)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&strLockRes,"strLockRes");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusGroupTree.UnLockTree",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
}

void RpcStub_BusGroupTree::ReadGroupTree(Status &Ret_pStatus, int nTreeID,DbRecordSet &Ret_Data)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&nTreeID,"nTreeID");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusGroupTree.ReadGroupTree",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&Ret_Data,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusGroupTree::WriteGroupItem(Status &Ret_pStatus, int nTreeID,DbRecordSet &RetOut_Data)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&nTreeID,"nTreeID");
	msg.msg_in->AddParameter(&RetOut_Data,"RetOut_Data");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusGroupTree.WriteGroupItem",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&RetOut_Data,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusGroupTree::DeleteGroupItem(Status &Ret_pStatus, int nTreeID,DbRecordSet &DataForDelete)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&nTreeID,"nTreeID");
	msg.msg_in->AddParameter(&DataForDelete,"DataForDelete");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusGroupTree.DeleteGroupItem",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
}

void RpcStub_BusGroupTree::ChangeGroupContent(Status &Ret_pStatus, int nGroupEntityTypeID,int nGroupItemID,int nOperation,DbRecordSet &RetOut_GroupContent)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&nGroupEntityTypeID,"nGroupEntityTypeID");
	msg.msg_in->AddParameter(&nGroupItemID,"nGroupItemID");
	msg.msg_in->AddParameter(&nOperation,"nOperation");
	msg.msg_in->AddParameter(&RetOut_GroupContent,"RetOut_GroupContent");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusGroupTree.ChangeGroupContent",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&RetOut_GroupContent,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusGroupTree::ReadGroupContent(Status &Ret_pStatus, int nGroupEntityTypeID,DbRecordSet &GroupItems,DbRecordSet &Ret_GroupContent)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&nGroupEntityTypeID,"nGroupEntityTypeID");
	msg.msg_in->AddParameter(&GroupItems,"GroupItems");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusGroupTree.ReadGroupContent",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&Ret_GroupContent,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusGroupTree::WriteGroupItemData(Status &Ret_pStatus, int nGroupItemID, QString strExtCategory, QString strDescription)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&nGroupItemID,"nGroupItemID");
	msg.msg_in->AddParameter(&strExtCategory,"strExtCategory");
	msg.msg_in->AddParameter(&strDescription,"strDescription");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusGroupTree.WriteGroupItemData",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
}

void RpcStub_BusGroupTree::WriteGroupContentData(Status &Ret_pStatus, int nGroupEntityTypeID, DbRecordSet &GroupItems)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&nGroupEntityTypeID,"nGroupEntityTypeID");
	msg.msg_in->AddParameter(&GroupItems,"GroupItems");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusGroupTree.WriteGroupContentData",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
}

void RpcStub_BusGroupTree::RemoveGroupContentItem(Status &Ret_pStatus, int nGroupEntityTypeID,DbRecordSet &GroupItemsForRemove,DbRecordSet &Groups)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&nGroupEntityTypeID,"nGroupEntityTypeID");
	msg.msg_in->AddParameter(&GroupItemsForRemove,"GroupItemsForRemove");
	msg.msg_in->AddParameter(&Groups,"Groups");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusGroupTree.RemoveGroupContentItem",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
}

void RpcStub_BusGroupTree::GetChildrenGroups(Status &Ret_pStatus, int nGroupParentID,DbRecordSet &Ret_ChildrenGroups)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&nGroupParentID,"nGroupParentID");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusGroupTree.GetChildrenGroups",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&Ret_ChildrenGroups,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

