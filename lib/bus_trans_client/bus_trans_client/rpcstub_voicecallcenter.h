#ifndef RPCSTUB_VOICECALLCENTER_H__
#define RPCSTUB_VOICECALLCENTER_H__

#include "common/common/status.h"
#include "bus_interface/bus_interface/interface_voicecallcenter.h"
#include "bus_trans_client/bus_trans_client/connectionhandler.h"
#include "trans/trans/rpcstubmessagehandler.h"

class RpcStub_VoiceCallCenter: public Interface_VoiceCallCenter, public RpcStub
{
public:
	RpcStub_VoiceCallCenter(ConnectionHandler *pConnectionHandler,int RPCType):
		RpcStub(RPCType){m_ConnectionHandler=pConnectionHandler;}

	void ReadCall(Status &Ret_pStatus, int nVoiceCallID,DbRecordSet &Ret_CallData, DbRecordSet &Ret_UAR,DbRecordSet &Ret_GAR);
	void WriteCall(Status &Ret_pStatus, DbRecordSet &RetOut_CallData, QString strLockRes,DbRecordSet &RetOut_ScheduleTask,DbRecordSet &RetOut_UAR,DbRecordSet &RetOut_GAR);
	void ListCalls(Status &Ret_pStatus, DbRecordSet &RetOut_lstCallData, QDate &datDate, int nOwnerID);
	void DeleteCalls(Status &Ret_pStatus, DbRecordSet &RetOut_lstIDs);

private:
	ConnectionHandler *m_ConnectionHandler; ///< connection handler
};

#endif	// RPCSTUB_VOICECALLCENTER_H__
