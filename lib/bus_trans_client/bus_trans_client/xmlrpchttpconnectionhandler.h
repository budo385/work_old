#ifndef XMLRPCHTTPCONNECTIONHANDLER_H
#define XMLRPCHTTPCONNECTIONHANDLER_H


#include "connectionhandler.h"


/*!
    \class XmlRpcHttpConnectionHandler
    \brief XML-RPC implementation of ConnectionHandler
    \ingroup Bus_Trans_Client

	Main integrator object for client XML-RPC client connection.
	Init only with connection settings
*/

class XmlRpcHTTPConnectionHandler:public ConnectionHandler
{

public:
	XmlRpcHTTPConnectionHandler();
	virtual ~XmlRpcHTTPConnectionHandler();

	virtual void StartThreadConnection(Status& pStatus,int nConnectionThreadID=-1);
	virtual void CloseThreadConnection(Status& pStatus,int nConnectionThreadID=-1);


};


#endif //XMLRPCHTTPCONNECTIONHANDLER_H
