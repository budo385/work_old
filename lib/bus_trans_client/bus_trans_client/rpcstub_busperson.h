#ifndef RPCSTUB_BUSPERSON_H__
#define RPCSTUB_BUSPERSON_H__

#include "common/common/status.h"
#include "bus_interface/bus_interface/interface_busperson.h"
#include "bus_trans_client/bus_trans_client/connectionhandler.h"
#include "trans/trans/rpcstubmessagehandler.h"

class RpcStub_BusPerson: public Interface_BusPerson, public RpcStub
{
public:
	RpcStub_BusPerson(ConnectionHandler *pConnectionHandler,int RPCType):
		RpcStub(RPCType){m_ConnectionHandler=pConnectionHandler;}

	void ReadData(Status &Ret_pStatus, int nPersonID, DbRecordSet &Ret_Data,DbRecordSet &Ret_CoreUserData, DbRecordSet &Ret_RoleList);
	void WriteData(Status &Ret_pStatus, DbRecordSet &RetOut_Data, DbRecordSet &RetOut_CoreUserData, QString pLockResourceID="", int nPersonRoleId=-1);
	void GetPersonProjectListID(Status &Ret_pStatus, int nPersonID,int &Ret_nListID);

private:
	ConnectionHandler *m_ConnectionHandler; ///< connection handler
};

#endif	// RPCSTUB_BUSPERSON_H__
