#ifndef XMLRPCHTTPCLIENT_H
#define XMLRPCHTTPCLIENT_H

#include <QtCore>

#include "common/common/status.h"
#include "rpchttpclient.h"


/*!
    \class XmlRpcHTPPClient
    \brief Handler for XMLRPC messages over HTTP
    \ingroup Bus_Trans_Client

	Sends HTTP request to server, adds HTTP header and uses HTTPClient to make call to the server
	Dispatches incoming server messages if skeletonset is registered

*/
class XmlRpcHTPPClient : public RpcHTTPClient
{
public:
	XmlRpcHTPPClient(RpcSkeleton *pSrvMsgSkeletonSet);

};


#endif //XMLRPCHTTPCLIENT_H


