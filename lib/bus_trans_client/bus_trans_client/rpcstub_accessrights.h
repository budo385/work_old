#ifndef RPCSTUB_ACCESSRIGHTS_H__
#define RPCSTUB_ACCESSRIGHTS_H__

#include "common/common/status.h"
#include "bus_interface/bus_interface/interface_accessrights.h"
#include "bus_trans_client/bus_trans_client/connectionhandler.h"
#include "trans/trans/rpcstubmessagehandler.h"

class RpcStub_AccessRights: public Interface_AccessRights, public RpcStub
{
public:
	RpcStub_AccessRights(ConnectionHandler *pConnectionHandler,int RPCType):
		RpcStub(RPCType){m_ConnectionHandler=pConnectionHandler;}

	void GetRoleList(Status &Ret_pStatus, DbRecordSet &Ret_pPersonRoleListRecordSet);
	void GetPersons(Status &Ret_pStatus, DbRecordSet &Ret_pPersonsRecordSet);
	void GetPersonRoleList(Status &Ret_pStatus, DbRecordSet &Ret_pPersonRoleListRecordSet, int PersonID);
	void InsertNewRoleToPerson(Status &Ret_pStatus, int PersonID, int RoleID);
	void DeleteRoleFromPerson(Status &Ret_pStatus, int PersonID, int RoleID);
	void GetRoleListForUsers(Status &Ret_pStatus, DbRecordSet &Ret_pUserRoleListRecordSet);
	void GetUsers(Status &Ret_pStatus, DbRecordSet &Ret_pUsersRecordSet);
	void GetUserRoleList(Status &Ret_pStatus, DbRecordSet &Ret_pUserRoleListRecordSet, int UserID);
	void InsertNewRoleToUser(Status &Ret_pStatus, int UserID, int RoleID);
	void DeleteRoleFromUser(Status &Ret_pStatus, int UserID, int RoleID);
	void GetUserAccRightsRecordSet(Status &Ret_pStatus, DbRecordSet &Ret_pUserAccRRecordSet, int UserID);
	void GetRoles(Status &Ret_pStatus, DbRecordSet &Ret_pUserRoleListRecordSet);
	void GetBERoles(Status &Ret_pStatus, DbRecordSet &Ret_pRoleListRecordSet);
	void GetTERoles(Status &Ret_pStatus, DbRecordSet &Ret_pRoleListRecordSet);
	void GetRoleAccessRightSets(Status &Ret_pStatus, DbRecordSet &Ret_pAccessRightsListRecordSet, int RoleID);
	void GetAccessRightsSets(Status &Ret_pStatus, DbRecordSet &Ret_pAccessRightsSetListRecordSet);
	void DeleteRole(Status &Ret_pStatus, int RoleID);
	void DeleteAccRSetFromRole(Status &Ret_pStatus, int RoleID, int AccRSetID);
	void InsertNewAccRSetToRole(Status &Ret_pStatus, int RoleID, int AccRSetID);
	void InsertNewRole(Status &Ret_pStatus, int &Ret_nNewRoleRowID, QString RoleName, QString RoleDescription, int RoleType);
	void RenameRole(Status &Ret_pStatus, int RoleID, QString NewRoleName, QString NewRoleDescription);
	void GetAccessRights(Status &Ret_pStatus, DbRecordSet &Ret_pAccessRightsRecordSet, int RoleID, int AccRSetID);
	void SetAccessRightValue(Status &Ret_pStatus, int AccRightID, QString Value);
	void GetDefaultRoles(Status &Ret_pStatus, int RoleType, DbRecordSet &Ret_pDefaultRoleRecordSet);
	void ChangeAccessRights(Status &Ret_pStatus, DbRecordSet &ChangedRightsRecordSet);
	void ReadGUAR(Status &Ret_pStatus, int nRecord, int nTableID,DbRecordSet &Ret_UAR, DbRecordSet &Ret_GAR);
	void WriteGUAR(Status &Ret_pStatus, int nRecord, int nTableID,DbRecordSet &RetOut_UAR, DbRecordSet &RetOut_GAR);
	void PrepareGUAR(Status &Ret_pStatus, int nRecord, int nTableID, DbRecordSet &RetOut_UAR, DbRecordSet &RetOut_GAR, DbRecordSet &Ret_lstGroupUsers,int &Ret_ParentRecord,int &Ret_ParentTableID,QString &Ret_ParentName);
	void TestUAR(Status &Ret_pStatus, int nTableID, int nPersonID, int nAccessLevel, DbRecordSet &lstTest_IDs, DbRecordSet &Ret_lstResult_IDs, bool bReturnAllowed=true);
	void UpdateProjectGUAR(Status &Ret_pStatus, DbRecordSet &lstProject_IDs);
	void UpdateGroupContactsGUAR(Status &Ret_pStatus, DbRecordSet &lstData);
	void UpdateProjectCommEntityGUAR(Status &Ret_pStatus, int nTableID, DbRecordSet &lstData);

private:
	ConnectionHandler *m_ConnectionHandler; ///< connection handler
};

#endif	// RPCSTUB_ACCESSRIGHTS_H__
