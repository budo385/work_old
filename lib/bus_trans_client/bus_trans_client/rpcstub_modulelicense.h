#ifndef RPCSTUB_MODULELICENSE_H__
#define RPCSTUB_MODULELICENSE_H__

#include "common/common/status.h"
#include "bus_interface/bus_interface/interface_modulelicense.h"
#include "bus_trans_client/bus_trans_client/connectionhandler.h"
#include "trans/trans/rpcstubmessagehandler.h"

class RpcStub_ModuleLicense: public Interface_ModuleLicense, public RpcStub
{
public:
	RpcStub_ModuleLicense(ConnectionHandler *pConnectionHandler,int RPCType):
		RpcStub(RPCType){m_ConnectionHandler=pConnectionHandler;}

	void GetAllModuleLicenseData(Status& Ret_pStatus, QString& strModuleCode, QString& strMLIID,DbRecordSet& RetOut_pList,QString& RetOut_strUserName, int& RetOut_nLicenseID, QString &RetOut_strReportLine1, QString &RetOut_strReportLine2, QString &Ret_strCustomerID,int &Ret_nCustomSolutionID);
	void GetFPList(Status& Ret_pStatus, QString& strModuleCode, QString& strMLIID, DbRecordSet& RetOut_pList);
	void GetLicenseInfo(Status& Ret_pStatus, QString& RetOut_strUserName, int& RetOut_nLicenseID, QString &RetOut_strReportLine1, QString &RetOut_strReportLine2, QString &Ret_strCustomerID,int &Ret_nCustomSolutionID);

private:
	ConnectionHandler *m_ConnectionHandler; ///< connection handler
};

#endif	// RPCSTUB_MODULELICENSE_H__
