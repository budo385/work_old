#ifndef RPCSTUB_BUSGRIDVIEWS_H__
#define RPCSTUB_BUSGRIDVIEWS_H__

#include "common/common/status.h"
#include "bus_interface/bus_interface/interface_busgridviews.h"
#include "bus_trans_client/bus_trans_client/connectionhandler.h"
#include "trans/trans/rpcstubmessagehandler.h"

class RpcStub_BusGridViews: public Interface_BusGridViews, public RpcStub
{
public:
	RpcStub_BusGridViews(ConnectionHandler *pConnectionHandler,int RPCType):
		RpcStub(RPCType){m_ConnectionHandler=pConnectionHandler;}

	void Write(Status &Ret_pStatus, DbRecordSet &RetOut_Data,QString strLockRes,QString strOldView,int nGridID);
	void Read(Status &Ret_pStatus, int nGridID,DbRecordSet &Ret_Data);
	void Delete(Status &Ret_pStatus, QString strView,int nGridID);
	void Lock(Status &Ret_pStatus, QString strView,QString &Ret_strLockRes,int nGridID);
	void Unlock(Status &Ret_pStatus, QString strLockRes);

private:
	ConnectionHandler *m_ConnectionHandler; ///< connection handler
};

#endif	// RPCSTUB_BUSGRIDVIEWS_H__
