#include "rpcstub_busimport.h"

void RpcStub_BusImport::ImportProjects(Status &Ret_pStatus, DbRecordSet &RetOut_Data,int nOperation)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&RetOut_Data,"RetOut_Data");
	msg.msg_in->AddParameter(&nOperation,"nOperation");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusImport.ImportProjects",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&RetOut_Data,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusImport::ImportPersons(Status &Ret_pStatus, DbRecordSet &RetOut_Data,int nOperation, bool bCreateUsers, bool bCreateContacts, int nRoleID)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&RetOut_Data,"RetOut_Data");
	msg.msg_in->AddParameter(&nOperation,"nOperation");
	msg.msg_in->AddParameter(&bCreateUsers,"bCreateUsers");
	msg.msg_in->AddParameter(&bCreateContacts,"bCreateContacts");
	msg.msg_in->AddParameter(&nRoleID,"nRoleID");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusImport.ImportPersons",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&RetOut_Data,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusImport::ImportContacts(Status &Ret_pStatus, DbRecordSet &RetOut_Data, DbRecordSet &RetOut_ErrRow, int nOperation, bool bCreateUsers)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&RetOut_Data,"RetOut_Data");
	msg.msg_in->AddParameter(&RetOut_ErrRow,"RetOut_ErrRow");
	msg.msg_in->AddParameter(&nOperation,"nOperation");
	msg.msg_in->AddParameter(&bCreateUsers,"bCreateUsers");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusImport.ImportContacts",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&RetOut_Data,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(1,&RetOut_ErrRow,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusImport::ImportUserContactRelationships(Status &Ret_pStatus, DbRecordSet &RetOut_Data)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&RetOut_Data,"RetOut_Data");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusImport.ImportUserContactRelationships",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&RetOut_Data,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusImport::ImportContactContactRelationships(Status &Ret_pStatus, DbRecordSet &RetOut_Data)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&RetOut_Data,"RetOut_Data");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusImport.ImportContactContactRelationships",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&RetOut_Data,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusImport::GetContactContactRelationships(Status &Ret_pStatus, DbRecordSet &RetOut_Data)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&RetOut_Data,"RetOut_Data");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusImport.GetContactContactRelationships",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&RetOut_Data,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusImport::ImportContactCustomData(Status &Ret_pStatus, QString strCustomFieldName, int nTableID, DbRecordSet &RetOut_Data)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&strCustomFieldName,"strCustomFieldName");
	msg.msg_in->AddParameter(&nTableID,"nTableID");
	msg.msg_in->AddParameter(&RetOut_Data,"RetOut_Data");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusImport.ImportContactCustomData",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&RetOut_Data,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusImport::ProcessProjects_Import(Status &Ret_pStatus, QString strFileExt, QString strFilePrefix, QString strFileExtOfProcessed, QString strDirPath, DbRecordSet &Ret_ProcessedFiles, DbRecordSet &Ret_pErrors)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&strFileExt,"strFileExt");
	msg.msg_in->AddParameter(&strFilePrefix,"strFilePrefix");
	msg.msg_in->AddParameter(&strFileExtOfProcessed,"strFileExtOfProcessed");
	msg.msg_in->AddParameter(&strDirPath,"strDirPath");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusImport.ProcessProjects_Import",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&Ret_ProcessedFiles,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(1,&Ret_pErrors,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusImport::ProcessProjects_Export(Status &Ret_pStatus, QString strFileExt, QString strFilePrefix, QString strFileExtOfProcessed, QString strDirPath, int nExportLasN, bool bExportAddDateTimeOnExportFile, DbRecordSet &Ret_ProcessedFiles, DbRecordSet &Ret_pErrors, DbRecordSet &ProjectsForExport, bool bClientSideExport, QByteArray &Ret_File, bool bUtf8)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&strFileExt,"strFileExt");
	msg.msg_in->AddParameter(&strFilePrefix,"strFilePrefix");
	msg.msg_in->AddParameter(&strFileExtOfProcessed,"strFileExtOfProcessed");
	msg.msg_in->AddParameter(&strDirPath,"strDirPath");
	msg.msg_in->AddParameter(&nExportLasN,"nExportLasN");
	msg.msg_in->AddParameter(&bExportAddDateTimeOnExportFile,"bExportAddDateTimeOnExportFile");
	msg.msg_in->AddParameter(&ProjectsForExport,"ProjectsForExport");
	msg.msg_in->AddParameter(&bClientSideExport,"bClientSideExport");
	msg.msg_in->AddParameter(&bUtf8,"bUtf8");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusImport.ProcessProjects_Export",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&Ret_ProcessedFiles,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(1,&Ret_pErrors,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(2,&Ret_File,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusImport::ProcessUsers_Import(Status &Ret_pStatus, QString strFileExt, QString strFilePrefix, QString strFileExtOfProcessed, QString strDirPath, DbRecordSet &Ret_ProcessedFiles, DbRecordSet &Ret_pErrors, int nRoleID)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&strFileExt,"strFileExt");
	msg.msg_in->AddParameter(&strFilePrefix,"strFilePrefix");
	msg.msg_in->AddParameter(&strFileExtOfProcessed,"strFileExtOfProcessed");
	msg.msg_in->AddParameter(&strDirPath,"strDirPath");
	msg.msg_in->AddParameter(&nRoleID,"nRoleID");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusImport.ProcessUsers_Import",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&Ret_ProcessedFiles,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(1,&Ret_pErrors,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusImport::ProcessUsers_Export(Status &Ret_pStatus, QString strFileExt, QString strFilePrefix, QString strFileExtOfProcessed, QString strDirPath, int nExportLasN, bool bExportAddDateTimeOnExportFile, DbRecordSet &Ret_ProcessedFiles, DbRecordSet &Ret_pErrors, DbRecordSet &UsersForExport, bool bClientSideExport, QByteArray &Ret_File, bool bUtf8)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&strFileExt,"strFileExt");
	msg.msg_in->AddParameter(&strFilePrefix,"strFilePrefix");
	msg.msg_in->AddParameter(&strFileExtOfProcessed,"strFileExtOfProcessed");
	msg.msg_in->AddParameter(&strDirPath,"strDirPath");
	msg.msg_in->AddParameter(&nExportLasN,"nExportLasN");
	msg.msg_in->AddParameter(&bExportAddDateTimeOnExportFile,"bExportAddDateTimeOnExportFile");
	msg.msg_in->AddParameter(&UsersForExport,"UsersForExport");
	msg.msg_in->AddParameter(&bClientSideExport,"bClientSideExport");
	msg.msg_in->AddParameter(&bUtf8,"bUtf8");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusImport.ProcessUsers_Export",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&Ret_ProcessedFiles,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(1,&Ret_pErrors,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(2,&Ret_File,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusImport::ProcessDebtors_Export(Status &Ret_pStatus, QString strFileExt, QString strFilePrefix, QString strFileExtOfProcessed, QString strDirPath, int nExportLasN, bool bExportAddDateTimeOnExportFile, DbRecordSet &Ret_ProcessedFiles, DbRecordSet &Ret_pErrors, int nAddressTypeFilter, bool bUtf8)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&strFileExt,"strFileExt");
	msg.msg_in->AddParameter(&strFilePrefix,"strFilePrefix");
	msg.msg_in->AddParameter(&strFileExtOfProcessed,"strFileExtOfProcessed");
	msg.msg_in->AddParameter(&strDirPath,"strDirPath");
	msg.msg_in->AddParameter(&nExportLasN,"nExportLasN");
	msg.msg_in->AddParameter(&bExportAddDateTimeOnExportFile,"bExportAddDateTimeOnExportFile");
	msg.msg_in->AddParameter(&nAddressTypeFilter,"nAddressTypeFilter");
	msg.msg_in->AddParameter(&bUtf8,"bUtf8");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusImport.ProcessDebtors_Export",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&Ret_ProcessedFiles,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(1,&Ret_pErrors,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusImport::ProcessContacts_Import(Status &Ret_pStatus, QString strFileExt, QString strFilePrefix, QString strFileExtOfProcessed, QString strDirPath, DbRecordSet &Ret_ProcessedFiles, DbRecordSet &Ret_pErrors)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&strFileExt,"strFileExt");
	msg.msg_in->AddParameter(&strFilePrefix,"strFilePrefix");
	msg.msg_in->AddParameter(&strFileExtOfProcessed,"strFileExtOfProcessed");
	msg.msg_in->AddParameter(&strDirPath,"strDirPath");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusImport.ProcessContacts_Import",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&Ret_ProcessedFiles,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(1,&Ret_pErrors,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusImport::ProcessContacts_Export(Status &Ret_pStatus, QString strFileExt, QString strFilePrefix, QString strFileExtOfProcessed, QString strDirPath, int nExportLasN, bool bExportAddDateTimeOnExportFile, DbRecordSet &Ret_ProcessedFiles, DbRecordSet &Ret_pErrors, DbRecordSet &UsersForExport, bool bClientSideExport, QByteArray &Ret_File, bool bUtf8, bool bSokratesHdr, bool bColumnTitles, bool bOutlookExport, QString strLang, QByteArray datHedFile)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&strFileExt,"strFileExt");
	msg.msg_in->AddParameter(&strFilePrefix,"strFilePrefix");
	msg.msg_in->AddParameter(&strFileExtOfProcessed,"strFileExtOfProcessed");
	msg.msg_in->AddParameter(&strDirPath,"strDirPath");
	msg.msg_in->AddParameter(&nExportLasN,"nExportLasN");
	msg.msg_in->AddParameter(&bExportAddDateTimeOnExportFile,"bExportAddDateTimeOnExportFile");
	msg.msg_in->AddParameter(&UsersForExport,"UsersForExport");
	msg.msg_in->AddParameter(&bClientSideExport,"bClientSideExport");
	msg.msg_in->AddParameter(&bUtf8,"bUtf8");
	msg.msg_in->AddParameter(&bSokratesHdr,"bSokratesHdr");
	msg.msg_in->AddParameter(&bColumnTitles,"bColumnTitles");
	msg.msg_in->AddParameter(&bOutlookExport,"bOutlookExport");
	msg.msg_in->AddParameter(&strLang,"strLang");
	msg.msg_in->AddParameter(&datHedFile,"datHedFile");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusImport.ProcessContacts_Export",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&Ret_ProcessedFiles,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(1,&Ret_pErrors,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(2,&Ret_File,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusImport::ProcessContactContactRelations_Export(Status &Ret_pStatus, QString strFileExt, QString strFilePrefix, QString strFileExtOfProcessed, QString strDirPath, int nExportLasN, bool bExportAddDateTimeOnExportFile, DbRecordSet &Ret_ProcessedFiles, DbRecordSet &Ret_pErrors, DbRecordSet &UsersForExport, bool bClientSideExport, QByteArray &Ret_File, bool bUtf8, bool bSokratesHdr, bool bColumnTitles, bool bOutlookExport, QString strLang, QByteArray datHedFile)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&strFileExt,"strFileExt");
	msg.msg_in->AddParameter(&strFilePrefix,"strFilePrefix");
	msg.msg_in->AddParameter(&strFileExtOfProcessed,"strFileExtOfProcessed");
	msg.msg_in->AddParameter(&strDirPath,"strDirPath");
	msg.msg_in->AddParameter(&nExportLasN,"nExportLasN");
	msg.msg_in->AddParameter(&bExportAddDateTimeOnExportFile,"bExportAddDateTimeOnExportFile");
	msg.msg_in->AddParameter(&UsersForExport,"UsersForExport");
	msg.msg_in->AddParameter(&bClientSideExport,"bClientSideExport");
	msg.msg_in->AddParameter(&bUtf8,"bUtf8");
	msg.msg_in->AddParameter(&bSokratesHdr,"bSokratesHdr");
	msg.msg_in->AddParameter(&bColumnTitles,"bColumnTitles");
	msg.msg_in->AddParameter(&bOutlookExport,"bOutlookExport");
	msg.msg_in->AddParameter(&strLang,"strLang");
	msg.msg_in->AddParameter(&datHedFile,"datHedFile");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusImport.ProcessContactContactRelations_Export",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&Ret_ProcessedFiles,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(1,&Ret_pErrors,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(2,&Ret_File,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

void RpcStub_BusImport::ProcessStoredProjectList_Import(Status &Ret_pStatus, QString strFileExt, QString strFilePrefix, QString strFileExtOfProcessed, QString strDirPath, DbRecordSet &Ret_ProcessedFiles, DbRecordSet &Ret_pErrors)
{
	//make an instance of rpc msg handler
	RpcStubMessageHandler msg(this);

	//create RPC parameter list
	//----------------------------

	//first parameter is SESSION
	QString strSessionID=m_ConnectionHandler->GetSessionID();
	msg.msg_in->AddParameter(&strSessionID,"strSessionID");

	//add all other parameters
	msg.msg_in->AddParameter(&strFileExt,"strFileExt");
	msg.msg_in->AddParameter(&strFilePrefix,"strFilePrefix");
	msg.msg_in->AddParameter(&strFileExtOfProcessed,"strFileExtOfProcessed");
	msg.msg_in->AddParameter(&strDirPath,"strDirPath");

	//make a RPC call
	//----------------------------

	//generate rpc call:
	msg.msg_in->GenerateRPC("BusImport.ProcessStoredProjectList_Import",Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	//make SYNC call to server:
	m_ConnectionHandler->SendRPC(Ret_pStatus,msg);
	if (!Ret_pStatus.IsOK())return;

	//compile response
	//----------------------------

	//get fault if there is one
	msg.msg_out->CheckForFault(Ret_pStatus);

	//if not OK, return
	if (!Ret_pStatus.IsOK()) return;

	//extract return parameters from msg_out:
	msg.msg_out->GetParameter(0,&Ret_ProcessedFiles,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
	msg.msg_out->GetParameter(1,&Ret_pErrors,Ret_pStatus); if (!Ret_pStatus.IsOK()) return;
}

