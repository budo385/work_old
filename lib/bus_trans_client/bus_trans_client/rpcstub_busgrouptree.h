#ifndef RPCSTUB_BUSGROUPTREE_H__
#define RPCSTUB_BUSGROUPTREE_H__

#include "common/common/status.h"
#include "bus_interface/bus_interface/interface_busgrouptree.h"
#include "bus_trans_client/bus_trans_client/connectionhandler.h"
#include "trans/trans/rpcstubmessagehandler.h"

class RpcStub_BusGroupTree: public Interface_BusGroupTree, public RpcStub
{
public:
	RpcStub_BusGroupTree(ConnectionHandler *pConnectionHandler,int RPCType):
		RpcStub(RPCType){m_ConnectionHandler=pConnectionHandler;}

	void ReadTree(Status &Ret_pStatus, int nGroupEntityTypeID,DbRecordSet &Ret_Data);
	void WriteTree(Status &Ret_pStatus, DbRecordSet &RetOut_Data,QString strLockRes);
	void DeleteTree(Status &Ret_pStatus, int nTreeID);
	void LockTree(Status &Ret_pStatus, int nTreeID,QString &Ret_strLockRes);
	void UnLockTree(Status &Ret_pStatus, QString strLockRes);
	void ReadGroupTree(Status &Ret_pStatus, int nTreeID,DbRecordSet &Ret_Data);
	void WriteGroupItem(Status &Ret_pStatus, int nTreeID,DbRecordSet &RetOut_Data);
	void DeleteGroupItem(Status &Ret_pStatus, int nTreeID,DbRecordSet &DataForDelete);
	void ChangeGroupContent(Status &Ret_pStatus, int nGroupEntityTypeID,int nGroupItemID,int nOperation,DbRecordSet &RetOut_GroupContent);
	void ReadGroupContent(Status &Ret_pStatus, int nGroupEntityTypeID,DbRecordSet &GroupItems,DbRecordSet &Ret_GroupContent);
	void WriteGroupItemData(Status &Ret_pStatus, int nGroupItemID, QString strExtCategory, QString strDescription);
	void WriteGroupContentData(Status &Ret_pStatus, int nGroupEntityTypeID, DbRecordSet &GroupItems);
	void RemoveGroupContentItem(Status &Ret_pStatus, int nGroupEntityTypeID,DbRecordSet &GroupItemsForRemove,DbRecordSet &Groups);
	void GetChildrenGroups(Status &Ret_pStatus, int nGroupParentID,DbRecordSet &Ret_ChildrenGroups);

private:
	ConnectionHandler *m_ConnectionHandler; ///< connection handler
};

#endif	// RPCSTUB_BUSGROUPTREE_H__
