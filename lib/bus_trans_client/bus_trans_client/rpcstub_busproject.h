#ifndef RPCSTUB_BUSPROJECT_H__
#define RPCSTUB_BUSPROJECT_H__

#include "common/common/status.h"
#include "bus_interface/bus_interface/interface_busproject.h"
#include "bus_trans_client/bus_trans_client/connectionhandler.h"
#include "trans/trans/rpcstubmessagehandler.h"

class RpcStub_BusProject: public Interface_BusProject, public RpcStub
{
public:
	RpcStub_BusProject(ConnectionHandler *pConnectionHandler,int RPCType):
		RpcStub(RPCType){m_ConnectionHandler=pConnectionHandler;}

	void ReadProjectDataSideBar(Status &Ret_pStatus, int nProjectID,DbRecordSet &Ret_Data,DbRecordSet &DataNMRXPersonFilter,DbRecordSet &Ret_DataNMRXPerson);
	void NewProjectsFromTemplate(Status &Ret_pStatus, DbRecordSet &RetOut_Data, bool bCopyCE);
	void ReadData(Status &Ret_pStatus, int nProjectID,DbRecordSet &Ret_Data,DbRecordSet &DataNMRXPersonFilter,DbRecordSet &Ret_DataNMRXPerson,DbRecordSet &Ret_UAR,DbRecordSet &Ret_GAR, DbRecordSet &Ret_CustomFlds);
	void WriteData(Status &Ret_pStatus,DbRecordSet &RetOut_Data, QString pLockResourceID, DbRecordSet &RetOut_UAR,DbRecordSet &RetOut_GAR, DbRecordSet &RetOut_CustomFlds);

private:
	ConnectionHandler *m_ConnectionHandler; ///< connection handler
};

#endif	// RPCSTUB_BUSPROJECT_H__
