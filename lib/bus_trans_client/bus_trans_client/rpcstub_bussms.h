#ifndef RPCSTUB_BUSSMS_H__
#define RPCSTUB_BUSSMS_H__

#include "common/common/status.h"
#include "bus_interface/bus_interface/interface_bussms.h"
#include "bus_trans_client/bus_trans_client/connectionhandler.h"
#include "trans/trans/rpcstubmessagehandler.h"

class RpcStub_BusSms: public Interface_BusSms, public RpcStub
{
public:
	RpcStub_BusSms(ConnectionHandler *pConnectionHandler,int RPCType):
		RpcStub(RPCType){m_ConnectionHandler=pConnectionHandler;}

	void ReadSms(Status &Ret_pStatus, int nVoiceCallID,DbRecordSet &Ret_CallData, DbRecordSet &Ret_UAR,DbRecordSet &Ret_GAR);
	void WriteSms(Status &Ret_pStatus, DbRecordSet &RetOut_CallData, QString strLockRes,DbRecordSet &RetOut_ScheduleTask,DbRecordSet &RetOut_UAR,DbRecordSet &RetOut_GAR);
	void ListSmsMessages(Status &Ret_pStatus, DbRecordSet &RetOut_lstCallData, QDate &datDate, int nOwnerID);
	void DeleteSmsMessages(Status &Ret_pStatus, DbRecordSet &RetOut_lstIDs);

private:
	ConnectionHandler *m_ConnectionHandler; ///< connection handler
};

#endif	// RPCSTUB_BUSSMS_H__
