#include "rpcskeleton_servermessageset.h"

/*---------------------------------------------------------------------------------*/
//							TEST TEXT
/*---------------------------------------------------------------------------------*/


//set NameSpace, and store poointer to business object

RpcSkeletonServerMessageSet::RpcSkeletonServerMessageSet(int RPCType)
	:RpcSkeleton(RPCType)
{
	
	SetNameSpace("ServerMessages");
}


//dispatch RPC request to right skeleton fucntion

bool RpcSkeletonServerMessageSet::HandleRPC(Status &err,QByteArray *pBufRequest,QByteArray *pBufResponse, QString strRPCMethod,int nClientTimeZoneOffsetMinutes){

	//init locals:
	RpcSkeletonMessageHandler rpc(this,pBufRequest,pBufResponse,nClientTimeZoneOffsetMinutes);

	//get namespace, test for error:
	QString strNameSpace=rpc.GetRequestMethodNameSpace();
	if (strNameSpace!="ServerMessages"){rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_INVALID_NAMESPACE);return false;}

	//get methodname, test for error:
	QString strMethodName=rpc.msg_in->GetMethodName(err);
		if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return false;}

	//redirect call by method name:
	if(strMethodName.indexOf("ServerMsg")>=0)
		{ServerMsg(rpc);return true;}



	//if not found return error:
	rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_FUNCTION_NOT_EXISTS);
	return false;
}



//skeleton function

void RpcSkeletonServerMessageSet::ServerMsg(RpcSkeletonMessageHandler &rpc)
{

	Status err;

	//test parameter count:
	if(rpc.msg_in->GetParameterCount()!=1)
		{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}

	//extract parameters to local vars:
	DbRecordSet recMsg;
	
	//getParam (Only OUTGOING & RETURNED-OUTGOING) from client
	rpc.msg_in->GetParameter(0,&recMsg,err);
	if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}

	//invoke handler:
	emit Signal_ServerMsg(recMsg);

}

