#ifndef BUSINESSSERVICEMANAGER_THINCLIENT_H
#define BUSINESSSERVICEMANAGER_THINCLIENT_H

#include <QObject>
#include "bus_interface/bus_interface/businessservicemanager.h"
//#include "trans/trans/httpclientconnectionsettings.h"
#include "bus_trans_client/bus_trans_client/xmlrpchttpconnectionhandler.h"


/*!
	\class BusinessServiceSet_ThinClient
	\brief Holds RPC Business Set Stubs for accessing server
	\ingroup Bus_Client

	Use:
	1. SetSettings
	2. StartService (connection is not made until first server call)
	3. StopService (to disconnect or when new connection settings are defined)

*/
class BusinessServiceManager_ThinClient : public QObject, public BusinessServiceManager
{
	Q_OBJECT

public:
	BusinessServiceManager_ThinClient(QObject *parent=NULL);
	~BusinessServiceManager_ThinClient();


	void StartServiceSet(Status &pStatus);
	void StopService(Status &pStatus, bool bFatalErrorDetected=false);
	void AbortDataTransfer();

	void SetSettings(HTTPClientConnectionSettings& pConnSettings){m_ConnSettings=pConnSettings;}
	QString GetServerIPAddress(){return m_ConnSettings.m_strServerIP;};

	//get connection settings:
	void GetSettings(HTTPClientConnectionSettings& pConnSettings, Authenticator &pAuthCache);

	//multi thread connection interface:
	void StartThreadConnection(Status& pStatus, QObject *pSignalReceiver=NULL);
	void CloseThreadConnection(Status& pStatus);

	//quick interface for establishing connection to the appserver:
	void QcLogin (Status &pStatus,QString strUser,QString strPassword, HTTPClientConnectionSettings connection,QString strModuleCode="SC-BE", QString strMLIID="");
	void QcLogout (Status &err);
	void QcGetUserData(DbRecordSet &recUserData,DbRecordSet &recContactData,DbRecordSet &recSettings, DbRecordSet &recOptions,DbRecordSet &rowARSet,DbRecordSet &recSrvMsg);

signals:
	void CommunicationInProgress(int,qint64,qint64);
	void CommunicationFatalError(int);
	void CommunicationServerMessageRecieved(DbRecordSet);

private slots:
	void OnFatalErrorDetected(int);

private:
	void CreateRPCNetworkLayer(int nRPC_TypeID);

	HTTPClientConnectionSettings m_ConnSettings;
	ConnectionHandler* m_pRpcConnection;
	
	//for quicky, last logged user data:
	DbRecordSet m_recUserData,m_recContactData,m_recSettings, m_recOptions,m_rowARSet,m_recSrvMsg; 
};



#endif // BUSINESSSERVICEMANAGER_THINCLIENT_H
