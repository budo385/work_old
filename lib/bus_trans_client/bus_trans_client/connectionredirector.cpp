#include "connectionredirector.h"
#include "common/common/config.h"

#include "common/common/loggerabstract.h"


/*!
	Constructor: init with HTTP client with TCP connection to server
	\param pHttpclient - connection to server
*/
ConnectionRedirector::ConnectionRedirector(RpcHTTPClient *pHttpclient,RpcStubConnHandlerSet *pStubConnHandlerSet)
{
	m_StubConnHandlerSet=pStubConnHandlerSet;
	m_RpcHttpClient=pHttpclient;
	m_strLookupURL="";
	m_nRedirections=0;

}


/*!
	Initializes redirector with string lookup URL
	\param strURLLookup - URL for web lookup of IP address
*/
void ConnectionRedirector::InitializeRedirectList(QString strURLLookup)
{
	m_strLookupURL=strURLLookup;
}

/*!
	Connection in m_RpcHttpClient will be redirected to new IP address stored in redirector list
	Redirect mechanism:
	1. lookup inside pStatus for redirect message: ERR_SYSTEM_REDIRECT, new IP is stored in pStatus.ErrorText
	2. refresh/build redirector list: lookup web URL if setted
	3. try to redirect until the end of redirector list or until reached MAX_CLIENT_REDIRECTION_ATTEMPS
	WARNING: this object must have exclusive lock on HTTPClient connection object
	NOTE: internal redirector list is rebuild if some IP address iniside is invalid

	\param pStatus - return ok if redirection sucess, else ERR_HTTP_OPERATION_TIMEOUT
*/
void ConnectionRedirector::RedirectConnection(Status& pStatus)
{

	bool bIPOK;
	IpAddressAdv appserverIP;

	if (m_nRedirections==0)
	{
		DynamicIPLookUp();
		qSort(m_redirectorList); //sort list by tries-> first are those with less tries

		int nCount = m_redirectorList.size()-1;
		for(int i=0; i<nCount; i++) 
		{
			qDebug()<<m_redirectorList.at(i).m_strIP<<" - "<<m_redirectorList.at(i).m_nPort<<" -"<<m_redirectorList.at(i).m_nTries;
			ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Redirection list entry (sorted): " + m_redirectorList.at(i).m_strIP+" - "+QVariant(m_redirectorList.at(i).m_nPort).toString()+" -"+QVariant(m_redirectorList.at(i).m_nTries).toString());
		}
	}

	m_nRedirections++;

	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Redirection loop at: " + QVariant(m_nRedirections).toString());

	
	
	if(m_nRedirections>MAX_CLIENT_REDIRECTION_ATTEMPS) //if reached maximum redirection tries, exit with error:
	{
		m_nRedirections=0;
		pStatus.setError(StatusCodeSet::ERR_HTTP_OPERATION_TIMEOUT);
		return;
	}

	//extract IP address from pStatus if REDIRECT:
	bIPOK=false;
	if (pStatus.getErrorCode()==StatusCodeSet::ERR_SYSTEM_REDIRECT)
			bIPOK=appserverIP.SetIpAddress(pStatus.getErrorText());
	
	//if not fetched then lookup local list:
	if(!bIPOK)
	{
		if (m_nRedirections>m_redirectorList.size()) //reached end of list
			{
				m_nRedirections=0;
				pStatus.setError(StatusCodeSet::ERR_HTTP_OPERATION_TIMEOUT);
				return;
			}
		appserverIP=m_redirectorList.at(m_nRedirections-1);//redirections starts with 1
	}


	//notify user:
	QString strMessage=tr("Trying reconnection to: ")+appserverIP.m_strIP;
	ApplicationLogger::logMessage(StatusCodeSet::TYPE_INFORMATION,0,strMessage);
	QApplication::processEvents(QEventLoop::ExcludeUserInputEvents);


	//reconnect connection, if fails call back
	m_RpcHttpClient->ReConnect(pStatus,appserverIP.m_strIP,appserverIP.m_nPort);
	if (!pStatus.IsOK())
		{
			MarkIPAsFailure(appserverIP);			//failed to connect so place at end of list
			//if(m_redirectorList.size()==0) return;	//if reach end of redirection list, exit with last error
			
			pStatus.setError(0); 
			RedirectConnection(pStatus);		//recursive call
			return;
		}


	m_nRedirections=0;
	MarkIPAsSucess(appserverIP); //sets on top of list

	//notify user:
	strMessage=tr("Connection established to: ")+appserverIP.m_strIP;
	ApplicationLogger::logMessage(StatusCodeSet::TYPE_INFORMATION,0,strMessage);
	QApplication::processEvents(QEventLoop::ExcludeUserInputEvents);


}


/*!
	Rebuilds redirector list. Called by outside object, probably when session is started!
	Clears any previous content.
*/
void ConnectionRedirector::RebuildRedirectorListLocal()
{
	m_redirectorList.clear(); //clear old content
	DynamicIPLookUp(); //blocking function (max 2secs)
}


/*!
	Called after LOGIN, builds local LBO/DFO list:
	List is used primarly in DFO process.
	LBO process is done at Login()
	Preserves previous content of list
*/
void ConnectionRedirector::RebuildRedirectorListFromServer(QString strSession)
{
	
	DbRecordSet lstLBO;
	Status err;

	m_StubConnHandlerSet->ReadLBOList(err,strSession,lstLBO); //list ordered by load factor/lowest is first
	if(!err.IsOK()) qDebug()<<err.getErrorText();

	//LOGGER
	IpAddressAdv IPadr;
	m_redirectorList.clear(); //clear old content-> dynamic lookup is always executed on DFO to get newest state!

	//add server IP as first:
	HTTPClientConnectionSettings settings=m_RpcHttpClient->GetConnectionSettings();
	if(IPadr.SetIpAddress(settings.m_strServerIP+":"+QVariant(settings.m_nPort).toString()))
		if (m_redirectorList.indexOf(IPadr)<0&&IPadr.m_strIP!="")
			m_redirectorList.append(IPadr);

	//_DUMP(lstLBO);

	int nIP=lstLBO.getColumnIdx("COAS_IP_ADDRESS");
	int nSize=lstLBO.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		if(IPadr.SetIpAddress(lstLBO.getDataRef(i,nIP).toString()))
			if (m_redirectorList.indexOf(IPadr)<0&&IPadr.m_strIP!="")
				m_redirectorList.append(IPadr);
	}

}






//---------------------------------------------------------------------
//							PRIVATE HANDLERS
//---------------------------------------------------------------------

/*!
	Removes invalid IP address from list
*/
void ConnectionRedirector::MarkIPAsFailure(IpAddressAdv IPadr)
{
	int nCount = m_redirectorList.size()-1;
	for(int i=nCount; i>=0; i--) //back counting coz we are deleting record inside list
	{
		if(IPadr==m_redirectorList.at(i))
		{
			m_redirectorList[i].m_nTries++;
			return;
		}
	}
}

//clears tries flag
void ConnectionRedirector::MarkIPAsSucess(IpAddressAdv IPadr)
{
	int nCount = m_redirectorList.size()-1;
	for(int i=nCount; i>=0; i--)		
	{
		if(IPadr==m_redirectorList.at(i))
		{
			m_redirectorList[i].m_nTries=0;
			return;
		}
	}
}






/*!
	Parses HTML page containing Dynamic IP address.
	Open another TCP/HTTP connection to the URL specified in m_strLookupURL
	Warning: this is blocking function!
	Page with IP must be in format: <html>83.131.144.83</html>
	or if port: <html>83.131.144.83:port</html>
*/
void ConnectionRedirector::DynamicIPLookUp()
{



	if(m_strLookupURL.isEmpty()) return; //if empty go away:

	HttpReaderSync WebReader;
	QByteArray url;

	bool bOK=WebReader.ReadWebContent(m_strLookupURL,&url);
	if(!bOK) return; //log: dynamic lookup failed!!!


	int nStart=url.indexOf("<html>");
	if(nStart<0)
		return;
	else
		nStart+=6;

	int nLen=url.indexOf("</html>")-nStart;

	//just lookup web IP address if setted:
	IpAddressAdv appserverIP;
	bOK=appserverIP.SetIpAddress(url.mid(nStart,nLen));

	//if address is valid:appednt o list
	if(bOK)
	{
		//add to redirector list, but only if not exists:
		if (m_redirectorList.indexOf(appserverIP)<0&&appserverIP.m_strIP!="")
			m_redirectorList.append(appserverIP);
		

	}

}
	
		

