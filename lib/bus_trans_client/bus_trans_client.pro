TEMPLATE	= lib
CONFIG		= qt warn_on release staticlib
HEADERS		= bus_trans_client/connectionhandler.h \
		  bus_trans_client/connectionisalive.h \
		  bus_trans_client/connectionredirector.h \
		  bus_trans_client/rpchttpclient.h \
		  bus_trans_client/rpcskeleton_servermessageset.h \
		  bus_trans_client/rpcstub_accessrights.h \
		  bus_trans_client/rpcstub_boentity_busperson.h \
		  bus_trans_client/rpcstub_boentity_testcontact.h \
		  bus_trans_client/rpcstub_boentity_testtree.h \
		  bus_trans_client/rpcstub_buseventlog.h \
		  bus_trans_client/rpcstub_clientset.h \
		  bus_trans_client/rpcstub_collection.h \
		  bus_trans_client/rpcstub_connhandlerset.h \
		  bus_trans_client/rpcstub_mainentityselector.h \
		  bus_trans_client/rpcstub_modulelicense.h \
		  bus_trans_client/rpcstub_testbusinessobject.h \
		  bus_trans_client/rpcstub_testcontact.h \
		  bus_trans_client/rpcstub_treeview.h \
		  bus_trans_client/rpcstub_userlogon.h \
		  bus_trans_client/xmlrpchttpclient.h \
		  bus_trans_client/xmlrpchttpconnectionhandler.h \
		  bus_trans_client/xmlrpcstub_clientset.h
SOURCES		= bus_trans_client/connectionhandler.cpp \
		  bus_trans_client/connectionisalive.cpp \
		  bus_trans_client/connectionredirector.cpp \
		  bus_trans_client/rpchttpclient.cpp \
		  bus_trans_client/rpcskeleton_servermessageset.cpp \
		  bus_trans_client/rpcstub_accessrights.cpp \
		  bus_trans_client/rpcstub_boentity_busperson.cpp \
		  bus_trans_client/rpcstub_boentity_testcontact.cpp \
		  bus_trans_client/rpcstub_boentity_testtree.cpp \
		  bus_trans_client/rpcstub_buseventlog.cpp \
		  bus_trans_client/rpcstub_clientset.cpp \
		  bus_trans_client/rpcstub_connhandlerset.cpp \
		  bus_trans_client/rpcstub_mainentityselector.cpp \
		  bus_trans_client/rpcstub_modulelicense.cpp \
		  bus_trans_client/rpcstub_testbusinessobject.cpp \
		  bus_trans_client/rpcstub_testcontact.cpp \
		  bus_trans_client/rpcstub_treeview.cpp \
		  bus_trans_client/rpcstub_userlogon.cpp \
		  bus_trans_client/xmlrpchttpclient.cpp \
		  bus_trans_client/xmlrpchttpconnectionhandler.cpp
INTERFACES	= 
TARGET		= bus_trans_client
INCLUDEPATH	+= ../	/usr/lib/qt4/include/QtSql /usr/lib/qt4/include/QtNetwork
