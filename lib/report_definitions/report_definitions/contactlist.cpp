#include "contactlist.h"
#include "gui_core/gui_core/macros.h"
#include "bus_client/bus_client/modulelicenseclientcache.h"
extern ModuleLicenseClientCache			g_FunctionPoint;			//global function point tester
#include "bus_interface/bus_interface/businessservicemanager.h"
#include "trans/trans/httpclientconnectionsettings.h"
#include "bus_core/bus_core/mainentityfilter.h"
extern BusinessServiceManager *g_pBoSet;


ContactList::ContactList()
	: ReportPage()
{

}

ContactList::~ContactList()
{

}
//BT: lstPageRecordSet:0 = data
bool ContactList::SetWizardRecordSetList(QList<DbRecordSet> &lstPageRecordSet)
{
	if (lstPageRecordSet.size()!=1)
	{
		Q_ASSERT(false);
		return false;
	}

	Status status;
	//Define contact full view recordset.
	DbRecordSet recContactRecordSet(lstPageRecordSet.value(0));
	//Get selected contacts count.
	int nContactCount = recContactRecordSet.getRowCount();
	QString strWhere = " BCNT_ID IN (";

	//Get selected departments recordset.
	//recContactRecordSet = m_lstPageRecordSet.value(1);
	for (int i = 0; i < nContactCount; ++i)
	{
		//recContactRecordSet.Dump();
		strWhere = strWhere + recContactRecordSet.getDataRef(i, 0).toString();
		if (nContactCount != (i+1))
			strWhere = strWhere + ",";
	}
	strWhere = strWhere + ") ";

	recContactRecordSet.clear();

	MainEntityFilter Filter;
	Filter.SetFilter(MainEntityFilter::FILTER_SQL_WHERE,strWhere);
	DbRecordSet recFilter(Filter.Serialize());

	//Get data for contacts.
	_SERVER_CALL(BusContact->ReadData(status, recFilter, recContactRecordSet))
	_CHK_ERR_RET_BOOL_ON_FAIL(status);

	//Insert in detail hash.
	m_hshDetailRecordSetHash.insert("detail", recContactRecordSet);

	return true;
}

void ContactList::ReportMethodCall(QString FunctionName)
{
	if (FunctionName == "InitalizeLabels")
		InitalizeLabels();
	else if (FunctionName == "InitContactDetails")
		InitContactDetails();
}

void ContactList::InitalizeLabels()
{
	QString strMsg;
	int nSN;
	QString strReportLine1, strReportLine2;
	QString strCustomerID;
	int nCustomerSolutionID;
	g_FunctionPoint.GetLicenseInfo(strMsg, nSN, strReportLine1, strReportLine2,strCustomerID,nCustomerSolutionID);


	QString dateLabel(tr("Date:"));
	QString pageLabel(tr("Page:"));

	INS(Firm_name, strReportLine1);
	INS(Firm_address, strReportLine2);
	INS(date_label, dateLabel);
	INS(page_label, pageLabel);

	//INS(contact_label, contact_label);

	//Detail header labels.
	QString NameLabel(tr("Name"));
	QString OrganizationLabel(tr("Organization"));
	QString PhonesLabel(tr("Phones"));
	QString EmailsLabel(tr("Emails"));
	QString InternetLabel(tr("Internet"));
	QString AddressesLabel(tr("Addresses"));

	INS(name_label, NameLabel);
	INS(organization_label, OrganizationLabel);
	INS(phones_label, PhonesLabel);
	INS(emails_label, EmailsLabel);
	INS(internet_label, InternetLabel);
	INS(address_label, AddressesLabel);
}

void ContactList::InitContactDetails()
{
	//Insert other details hashes.
	DbRecordSet recPhones		= GET(LST_PHONE).value<DbRecordSet>();
	DbRecordSet recAddresses	= GET(LST_ADDRESS).value<DbRecordSet>();
	DbRecordSet recEmails		= GET(LST_EMAIL).value<DbRecordSet>();
	DbRecordSet recInternet		= GET(LST_INTERNET).value<DbRecordSet>();

	//Set recordset for sub detail.
	QList<int> lstRowCounts;
	lstRowCounts << recPhones.getRowCount();
	lstRowCounts << recAddresses.getRowCount();
	lstRowCounts << recEmails.getRowCount();
	lstRowCounts << recInternet.getRowCount();

	//Set it to 1 because it needs at least to print name...
	int nMaxRows = 1;	//Number of rows.
	int nCount = 1;		//Number of recordset (1 - recPhones, 2 - recAddresses, ...)
	int nID = 0;		//Which recordset has more rows.
	foreach(int nRowCount, lstRowCounts)
	{
		if (nRowCount > nMaxRows)
		{
			nMaxRows = nRowCount;
			nID = nCount;
		}
		nCount++;
	}

	//Take care of labels
	//Name
	QString strFirstName= GET(BCNT_FIRSTNAME).toString();
	QString strLastName = GET(BCNT_LASTNAME).toString();
	QString strName;
	if (!strFirstName.isEmpty())
		strName = strFirstName + " " + strLastName;
	else
		strName = strLastName;
	INS(name, strName);
	
	//Organization
	QString strOrgName	= GET(BCNT_ORGANIZATIONNAME).toString();
	INS(organization_multi_label, strOrgName);

	//Other labels.
	//_DUMP(recPhones);
	//_DUMP(recAddresses);
	//_DUMP(recEmails);
	//_DUMP(recInternet);

	//Row count.
	int nRowCount;
	//Phone.
	QString strPhoneType = "";
	QString strPhoneNumber = "";
	QString strLabel = "";
	nRowCount = recPhones.getRowCount();
	for (int i = 0; i < nRowCount; i++)
	{
		strPhoneType	= recPhones.getDataRef(i, "BCMT_TYPE_NAME").toString();
		strPhoneNumber	= recPhones.getDataRef(i, "BCMP_FULLNUMBER").toString();
		if (strPhoneType.isEmpty())
			strLabel	+= strPhoneNumber;
		else
			strLabel	+= strPhoneType + ": " + strPhoneNumber;
		
		//If not last line add line break.
		if (!((i+1) == nRowCount))
			strLabel += "\n";
	}
	INS(phone_multi_label, strLabel);

	//Addresses.
	QString strAddressType = "";
	QString strAddress = "";
	QString strAddressLabel = "";
	nRowCount = recAddresses.getRowCount();
	for (int i = 0; i < nRowCount; i++)
	{
		strAddressType		= recAddresses.getDataRef(i, "BCMT_TYPE_NAME").toString();
		strAddress			= recAddresses.getDataRef(i, "BCMA_FORMATEDADDRESS").toString();
		if(strAddressType.isEmpty())
			strAddressLabel	+= strAddress;
		else
			strAddressLabel	+= strAddressType + ": " + strAddress;

		//If not last line add line break.
		if (!((i+1) == nRowCount))
			strAddressLabel += "\n";
	}
	INS(address_multi_label, strAddressLabel);

	//Email.
	QString strEmailType = "";
	QString strEmail = "";
	QString strEmailLabel = "";
	nRowCount = recEmails.getRowCount();
	for (int i = 0; i < nRowCount; i++)
	{
		strEmailType	= recEmails.getDataRef(i, "BCMT_TYPE_NAME").toString();
		strEmail		= recEmails.getDataRef(i, "BCME_ADDRESS").toString();
		if (strEmailType.isEmpty())
			strEmailLabel+= strEmail;
		else
			strEmailLabel+= strEmailType + ": " + strEmail;

		//If not last line add line break.
		if (!((i+1) == nRowCount))
			strEmailLabel += "\n";
	}
	INS(email_multi_label, strEmailLabel);

	//Internet.
	QString strInternetType = "";
	QString strInternet = "";
	QString strInternetLabel = "";
	nRowCount = recInternet.getRowCount();
	for (int i = 0; i < nRowCount; i++)
	{
		strInternetType	= recInternet.getDataRef(i, "BCMT_TYPE_NAME").toString();
		strInternet		= recInternet.getDataRef(i, "BCMI_ADDRESS").toString();
		if (strInternetType.isEmpty())
			strInternetLabel+= strInternet;
		else
			strInternetLabel+= strInternetType + ": " + strInternet;

		//If not last line add line break.
		if (!((i+1) == nRowCount))
			strInternetLabel += "\n";
	}
	INS(internet_multi_label, strInternetLabel);
}
