#ifndef DEPARTMENTSTRUCTURES_H
#define DEPARTMENTSTRUCTURES_H

#include "report_core/report_core/reportpage.h"

class DepartmentStructures : public ReportPage
{
	Q_OBJECT

public:
	DepartmentStructures();
    ~DepartmentStructures();

	bool SetWizardRecordSetList(QList<DbRecordSet> &lstPageRecordSet);
	void ReportMethodCall(QString FunctionName);

private:
    void GetDepartmentCount();
	void InitalizeLabels();
};

#endif // DEPARTMENTSTRUCTURES_H
