#ifndef CONTACTDETAILS_H
#define CONTACTDETAILS_H

#include "report_core/report_core/reportpage.h"

class ContactDetails : public ReportPage
{
	Q_OBJECT

public:
    ContactDetails();
    ~ContactDetails();

	bool SetWizardRecordSetList(QList<DbRecordSet> &lstPageRecordSet);
	void ReportMethodCall(QString FunctionName);

private:
	void InitalizeLabels();
	void PrintContactNameLabel();
	void InitContactDetails();
   
};

#endif // CONTACTDETAILS_H
