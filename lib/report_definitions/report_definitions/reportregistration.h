#ifndef REPORTREGISTRATION_H
#define REPORTREGISTRATION_H

#include "report_core/report_core/reportpage.h"

class ReportRegistration 
{

public:

	enum ReportID
	{
		REPORT_CONTACT_DETAILS					= 1,
		REPORT_CONTACT_COMMUNICATION_DETAILS	= 2,
		REPORT_CONTACT_LIST						= 7,
		REPORT_PROJECT_LIST						= 8,
		REPORT_CONTACT_RELATIONS				= 9,
	};

	enum Destination
	{
		REPORT_DESTINATION_PREVIEW,
		REPORT_DESTINATION_PRINTER,
		REPORT_DESTINATION_PDF,
		REPORT_DESTINATION_ASK
	};

	static void					GetReportPage(QString strReportFile,ReportPage **pPage);
	static void					GetReportPage(int nReportID,ReportPage **pPage);
	static QString				GetReportName(int nReportID);
	static int					GetReportIDFromFile(QString strReportFile);
	static QHash<int,QString>	GetReportData();
	static QString				GetReportPath(int nReportID);
/*

    ReportRegistration();
    ~ReportRegistration();

	QHash<int, ReportPage*> *GetReportRegistrationHash();
	QHash<int, QString>		*GetReportIDToNameHash();

	QHash<int, ReportPage*>	m_hshReportPageHash;		//< Report registration hash (add reports to this hash).
	QHash<int, QString>	m_hshReportIDToNameHash;		//< Report integer ID to Name hash (for report wizards).
*/
};

#endif // REPORTREGISTRATION_H
