# ----------------------------------------------------
# This file is generated by the Qt Visual Studio Add-in.
# ------------------------------------------------------

HEADERS += ./contactrelationslist.h \
    ./reportregistration.h \
    ./contactcommunication.h \
    ./contactdetails.h \
    ./contactlist.h \
    ./departmentstructures.h \
    ./projectlist.h \
    ./simplereport.h
SOURCES += ./reportregistration.cpp \
    ./contactcommunication.cpp \
    ./contactdetails.cpp \
    ./contactlist.cpp \
    ./contactrelationslist.cpp \
    ./departmentstructures.cpp \
    ./projectlist.cpp \
    ./simplereport.cpp
