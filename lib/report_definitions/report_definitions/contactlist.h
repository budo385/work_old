#ifndef CONTACTLIST_H
#define CONTACTLIST_H

#include "report_core/report_core/reportpage.h"

class ContactList : public ReportPage
{
	Q_OBJECT

public:
    ContactList();
    ~ContactList();

	bool SetWizardRecordSetList(QList<DbRecordSet> &lstPageRecordSet);
	void ReportMethodCall(QString FunctionName);

private:
	void InitalizeLabels();
	void InitContactDetails();
	void PrintSubDetailLabels();

};

#endif // CONTACTLIST_H
