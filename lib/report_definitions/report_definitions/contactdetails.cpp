#include "contactdetails.h"
#include "gui_core/gui_core/macros.h"
#include "bus_interface/bus_interface/businessservicemanager.h"
#include "trans/trans/httpclientconnectionsettings.h"
#include "bus_core/bus_core/mainentityfilter.h"
extern BusinessServiceManager *g_pBoSet;
#include "bus_client/bus_client/modulelicenseclientcache.h"
extern ModuleLicenseClientCache g_FunctionPoint;

#include <QtWidgets/QMessageBox>

ContactDetails::ContactDetails() : ReportPage()
{
	m_bCallInterface = false;
}

ContactDetails::~ContactDetails()
{

}

//BT: lstPageRecordSet:0 = data
bool ContactDetails::SetWizardRecordSetList(QList<DbRecordSet> &lstPageRecordSet)
{
	if (lstPageRecordSet.size()!=1)
	{
		Q_ASSERT(false);
		return false;
	}

	Status status;
	//Define contact full view recordset.
	DbRecordSet recContactRecordSet(lstPageRecordSet.value(0));
	//Get selected contacts count.
	int nContactCount = recContactRecordSet.getRowCount();

	if (nContactCount >= 150)
	{
		int nRet = QMessageBox::warning(NULL, tr("Warning!"), tr("This operation could last several minutes. Continue?"),	QMessageBox::Ok | QMessageBox::Cancel, QMessageBox::Cancel);
		if (nRet == QMessageBox::Cancel)
			return false;
	}

	QString strWhere = " BCNT_ID IN (";
	//Get selected departments recordset.
	//recContactRecordSet = m_lstPageRecordSet.value(1);
	for (int i = 0; i < nContactCount; ++i)
	{
		//recContactRecordSet.Dump();
		strWhere = strWhere + recContactRecordSet.getDataRef(i, 0).toString();
		if (nContactCount != (i+1))
			strWhere = strWhere + ",";
	}
	strWhere = strWhere + ") ";
	
	recContactRecordSet.clear();
	
	MainEntityFilter Filter;
	Filter.SetFilter(MainEntityFilter::FILTER_SQL_WHERE,strWhere);
	DbRecordSet recFilter(Filter.Serialize());

	//Get data for contacts.
	_SERVER_CALL(BusContact->ReadData(status, recFilter, recContactRecordSet))
	_CHK_ERR_RET_BOOL_ON_FAIL(status);

/*
	//Add missing columns.
	recContactRecordSet.addColumn(QVariant::ByteArray, "BCNT_PICTURE");
	//Add picture and description columns.
	nContactCount = recContactRecordSet.getRowCount();
	for (int i = 0; i < nContactCount; ++i)
	{
		int nRowID = recContactRecordSet.getDataRef(i, 0).toInt();
		QString StrWhere = " WHERE BCNT_ID = " + QVariant(nRowID).toString();
		DbRecordSet recPictureAndDescr;

		_SERVER_CALL(ClientSimpleORM->Read(status, BUS_CM_CONTACT, recPictureAndDescr, StrWhere))
		_CHK_ERR_RET_BOOL_ON_FAIL(status);

		//recPictureAndDescr.Dump();

		//Add values to picture and to description.
		recContactRecordSet.setData(i, "BCNT_PICTURE", recPictureAndDescr.getDataRef(0, "BCNT_PICTURE"));
	}
*/	
	//Insert in detail hash.
	m_hshDetailRecordSetHash.insert("detail", recContactRecordSet);

	return true;
}

void ContactDetails::ReportMethodCall(QString FunctionName)
{
	if (FunctionName == "InitalizeLabels")
		InitalizeLabels();
	else if (FunctionName == "PrintContactNameLabel")
		PrintContactNameLabel();
	else if (FunctionName == "InitContactDetails")
		InitContactDetails();
}

void ContactDetails::InitalizeLabels()
{
	QString strMsg;
	int nSN;
	QString strReportLine1, strReportLine2;
	QString strCustomerID;
	int nCustomerSolutionID;
	g_FunctionPoint.GetLicenseInfo(strMsg, nSN, strReportLine1, strReportLine2,strCustomerID,nCustomerSolutionID);


	QString dateLabel(tr("Date:"));
	QString pageLabel(tr("Page:"));

	INS(Firm_name, strReportLine1);
	INS(Firm_address, strReportLine2);
	INS(date_label, dateLabel);
	INS(page_label, pageLabel);

	//INS(contact_label, contact_label);

	//Detail labels.
	QString FirstNameLabel(tr("First Name:"));
	QString MiddleNameLabel(tr("Middle Name:"));
	QString LastNameLabel(tr("Last Name:"));
	QString NickNameLabel(tr("Nick Name:"));
	QString SexLabel(tr("Sex:"));
	QString BirthdayLabel(tr("Birthday:"));
	
	QString OrganizationLabel(tr("Organization:"));
	QString DepartmentLabel(tr("Department:"));
	QString LocationLabel(tr("Location:"));
	QString FunctionLabel(tr("Function:"));
	QString ProfessionLabel(tr("Profession:"));

	QString DescriptionLabel(tr("Description:"));
	QString DescriptionLabel1(tr("Description"));

	QString PhoneNumberTitleLabel(tr("Phone Numbers"));
	QString PhoneNumberLabel(tr("Number"));
	QString PhoneTypeLabel(tr("Type"));
	QString NameLabel(tr("Name"));

	QString EmailNumberTitleLabel(tr("Email Addresses"));
	QString InternetNumberTitleLabel(tr("Internet Addresses"));
	QString AddressTitleLabel(tr("Addresses"));
	QString AddressLabel(tr("Address"));
	
	INS(first_name_label, FirstNameLabel);
	INS(middle_name_label, MiddleNameLabel);
	INS(last_name_label, LastNameLabel);
	INS(nick_name_label, NickNameLabel);
	INS(sex_label, SexLabel);
	INS(birthday_label, BirthdayLabel);

	INS(organization_label, OrganizationLabel);
	INS(department_label, DepartmentLabel);
	INS(location_label, LocationLabel);
	INS(function_label, FunctionLabel);
	INS(profession_label, ProfessionLabel);
	
	INS(description_label, DescriptionLabel);

	INS(phone_number_title_label, PhoneNumberTitleLabel);
	INS(phone_number_label, PhoneNumberLabel);
	INS(phone_type_label, PhoneTypeLabel);
	INS(phone_name_label, NameLabel);
	INS(description1_label, DescriptionLabel1);

	INS(email_number_title_label, EmailNumberTitleLabel);

	INS(internet_number_title_label, InternetNumberTitleLabel);
	
	INS(address_title_label, AddressTitleLabel);
	INS(address_label, AddressLabel);
}

void ContactDetails::PrintContactNameLabel()
{
	QString strFirstName = GET(BCNT_FIRSTNAME).toString();
	QString strLastName = GET(BCNT_LASTNAME).toString();
	QString strOrgName = GET(BCNT_ORGANIZATIONNAME).toString();
	QString strDeptName = GET(BCNT_DEPARTMENTNAME).toString();
	QString ContactLabel(tr("Contact: "));
	ContactLabel += strFirstName;
	if (!strLastName.isEmpty())
		ContactLabel += " " + strLastName;
	if (!strOrgName.isEmpty())
		ContactLabel += ", " + strOrgName;
	
	INS(contact_label, ContactLabel);

	//Sex label.
	QString strMale(tr("Male"));
	QString strFemale(tr("Female"));
	bool Female = GET(BCNT_SEX).toBool();
	if (Female)
		INS(BCNT_SEX, strFemale);
	else
		INS(BCNT_SEX, strMale);

	INS(organization_multi_label, strOrgName);
	INS(department_multi_label, strDeptName);
}

void ContactDetails::InitContactDetails()
{
	//Insert other details hashes.
	DbRecordSet recPhones		= GET(LST_PHONE).value<DbRecordSet>();
	DbRecordSet recAddresses	= GET(LST_ADDRESS).value<DbRecordSet>();
	DbRecordSet recEmails		= GET(LST_EMAIL).value<DbRecordSet>();
	DbRecordSet recInternet		= GET(LST_INTERNET).value<DbRecordSet>();

	m_hshDetailRecordSetHash.insert("phone_detail", recPhones);
	m_hshDetailRecordSetHash.insert("address_detail", recAddresses);
	m_hshDetailRecordSetHash.insert("email_detail", recEmails);
	m_hshDetailRecordSetHash.insert("internet_detail", recInternet);
}
