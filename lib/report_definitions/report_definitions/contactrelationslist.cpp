#include "ContactRelationsList.h"
#include "gui_core/gui_core/macros.h"
#include "bus_client/bus_client/modulelicenseclientcache.h"
extern ModuleLicenseClientCache			g_FunctionPoint;			//global function point tester
#include "bus_interface/bus_interface/businessservicemanager.h"
#include "trans/trans/httpclientconnectionsettings.h"
#include "bus_core/bus_core/mainentityfilter.h"
extern BusinessServiceManager *g_pBoSet;

//#ifndef min
//#define min(a,b) (((a)<(b))? (a):(b))
//#endif

ContactRelationsList::ContactRelationsList()
	: ReportPage()
{
	m_bFirstContact = true;
}

ContactRelationsList::~ContactRelationsList()
{

}
//BT: lstPageRecordSet:0 = data
bool ContactRelationsList::SetWizardRecordSetList(QList<DbRecordSet> &lstPageRecordSet)
{
	if (lstPageRecordSet.size()!=1)
	{
		Q_ASSERT(false);
		return false;
	}

	Status status;
	//Define contact full view recordset.
	DbRecordSet recContactRecordSet(lstPageRecordSet.value(0));
	int nContactCount = recContactRecordSet.getRowCount();

	_SERVER_CALL(BusImport->GetContactContactRelationships(status, recContactRecordSet))
	if(!status.IsOK()){
		QMessageBox::information(NULL, "Error", status.getErrorText());
		return false;
	}

	DbRecordSet recData = recContactRecordSet;
	recData.clear();
	recData.addColumn(QVariant::Int,	"Contact");			//Is contact or a relation.
	recData.addColumn(QVariant::Int,	"BUCR_ROLE_DIRECTION");
	recData.addColumn(QVariant::String, "BUCR_ROLE");
	recData.addColumn(QVariant::String, "BUCR_DESCRIPTION");
	recData.addColumn(QVariant::String, "RELATION_BCNT_FIRSTNAME");
	recData.addColumn(QVariant::String, "RELATION_BCNT_LASTNAME");
	recData.addColumn(QVariant::String, "RELATION_BCNT_ORGANIZATIONNAME");
	recData.addColumn(QVariant::String, "RELATION_BCNT_MIDDLENAME");
	
	//_DUMP(recData);

	int nRowCount = recContactRecordSet.getRowCount();
	for (int i = 0; i < nRowCount; i++)
	{
		int nRow = recData.getRowCount();
		recData.addRow();
		int nBCNT_ID = recContactRecordSet.getDataRef(i, "BCNT_ID").toInt();
		QString strBCNT_LASTNAME = recContactRecordSet.getDataRef(i, "BCNT_LASTNAME").toString();
		QString strBCNT_FIRSTNAME = recContactRecordSet.getDataRef(i, "BCNT_FIRSTNAME").toString();
		QString strBCNT_ORGANIZATIONNAME = recContactRecordSet.getDataRef(i, "BCNT_ORGANIZATIONNAME").toString();
		int nBCNT_TYPE = recContactRecordSet.getDataRef(i, "BCNT_TYPE").toInt();
		QString strBCNT_NAME = recContactRecordSet.getDataRef(i, "BCNT_NAME").toString();
		recData.setData(nRow, "BCNT_ID", nBCNT_ID);
		recData.setData(nRow, "BCNT_LASTNAME", strBCNT_LASTNAME);
		recData.setData(nRow, "BCNT_FIRSTNAME", strBCNT_FIRSTNAME);
		recData.setData(nRow, "BCNT_ORGANIZATIONNAME", strBCNT_ORGANIZATIONNAME);
		recData.setData(nRow, "BCNT_TYPE", nBCNT_TYPE);
		recData.setData(nRow, "BCNT_NAME", strBCNT_NAME);
		recData.setData(nRow, "Contact", 1);
		DbRecordSet lstRoles = recContactRecordSet.getDataRef(i, "LST_ROLES").value<DbRecordSet>();
		int nRolesCount = lstRoles.getRowCount();
		for (int j = 0; j < nRolesCount; j++)
		{
			nRow = recData.getRowCount();
			recData.addRow();
			//_DUMP(recData);
			//_DUMP(lstRoles);
			recData.setData(nRow, "BCNT_ID", nBCNT_ID);
			recData.setData(nRow, "BCNT_LASTNAME", strBCNT_LASTNAME);
			recData.setData(nRow, "BCNT_FIRSTNAME", strBCNT_FIRSTNAME);
			recData.setData(nRow, "BCNT_ORGANIZATIONNAME", strBCNT_ORGANIZATIONNAME);
			recData.setData(nRow, "BCNT_TYPE", nBCNT_TYPE);
			recData.setData(nRow, "BCNT_NAME", strBCNT_NAME);
			recData.setData(nRow, "Contact", 0);
			recData.setData(nRow, "BUCR_ROLE_DIRECTION", lstRoles.getDataRef(j, "BUCR_ROLE_DIRECTION").toInt());
			recData.setData(nRow, "BUCR_ROLE", lstRoles.getDataRef(j, "BUCR_ROLE").toString());
			recData.setData(nRow, "BUCR_DESCRIPTION", lstRoles.getDataRef(j, "BUCR_DESCRIPTION").toString());
			recData.setData(nRow, "RELATION_BCNT_FIRSTNAME", lstRoles.getDataRef(j, "BCNT_FIRSTNAME").toString());
			recData.setData(nRow, "RELATION_BCNT_LASTNAME", lstRoles.getDataRef(j, "BCNT_LASTNAME").toString());
			recData.setData(nRow, "RELATION_BCNT_ORGANIZATIONNAME", lstRoles.getDataRef(j, "BCNT_ORGANIZATIONNAME").toString());
			recData.setData(nRow, "RELATION_BCNT_MIDDLENAME", lstRoles.getDataRef(j, "BCNT_MIDDLENAME").toString());
		}
	}

	_DUMP(recData);
	//Insert in detail hash.
	m_hshDetailRecordSetHash.insert("detail", recData);
	//_DUMP(recContactRecordSet);
	return true;
}

void ContactRelationsList::ReportMethodCall(QString FunctionName)
{
	if (FunctionName == "InitalizeLabels")
		InitalizeLabels();
	else if (FunctionName == "InitContactDetails")
		InitContactDetails();
}

void ContactRelationsList::InitalizeLabels()
{
	//global headers
	int nSN;
	QString strMsg, strReportLine1, strReportLine2;
	QString strCustomerID;
	int nCustomerSolutionID;
	g_FunctionPoint.GetLicenseInfo(strMsg, nSN, strReportLine1, strReportLine2,strCustomerID,nCustomerSolutionID);


	INS(Firm_name, strReportLine1);
	INS(Firm_address, strReportLine2);
	INS(date_label, QString(tr("Date:")));
	INS(page_label, QString(tr("Page:")));

	//Detail header labels.
	INS(contactinfo_label, QString(tr("Contact Info")));
}

void ContactRelationsList::InitContactDetails()
{
	int nContact = GET(Contact).toInt();
	bool bIsContact = GET(Contact).toBool();
	if (bIsContact)
	{
		m_hshReportPrintingItems.value("detail_print")->m_bHideItem = false;
		m_hshReportPrintingItems.value("role_detail_print")->m_bHideItem = true;
		if (m_bFirstContact)
			m_hshReportPrintingItems.value("contact_divider")->m_bHideItem = true;
		else
			m_hshReportPrintingItems.value("contact_divider")->m_bHideItem = false;
		m_bFirstContact = false;
		//Contact Label: "Last Name, First + Middle Names + Organization"
		QString strFirstName= GET(BCNT_FIRSTNAME).toString();
		QString strLastName = GET(BCNT_LASTNAME).toString();
		QString strMidName	= GET(BCNT_MIDDLENAME).toString();
		QString strOrgName	= GET(BCNT_ORGANIZATIONNAME).toString();

		QString strLabel = strLastName;
		if (!strLabel.isEmpty())
			strLabel += ",";

		if (!strFirstName.isEmpty()){
			if (!strLabel.isEmpty())
				strLabel += " ";
			strLabel += strFirstName;
		}
		if (!strMidName.isEmpty()){
			if (!strLabel.isEmpty())
				strLabel += " ";
			strLabel += strMidName;
		}
		if (!strOrgName.isEmpty()){
			if (!strLabel.isEmpty())
				strLabel += " ";
			strLabel += strOrgName;
		}

		INS(name, strLabel);

		//calculate list of subdetails
		//DbRecordSet lstRoles = GET(LST_ROLES).value<DbRecordSet>();
		//_DUMP(lstRoles);
		//m_hshDetailRecordSetHash.insert("role_detail", lstRoles);
	}
	else
	{	
		m_hshReportPrintingItems.value("detail_print")->m_bHideItem = true;
		m_hshReportPrintingItems.value("role_detail_print")->m_bHideItem = false;
		m_hshReportPrintingItems.value("contact_divider")->m_bHideItem = true;

		//Role name label
		QString strData = GET(BUCR_ROLE).toString();

		int bIsAB = GET(BUCR_ROLE_DIRECTION).toInt();
		if(bIsAB)
			strData += " >> ";
		else
			strData += " << ";

		//Contact Label: "Last Name, First + Middle Names + Organization"
		QString strFirstName= GET(RELATION_BCNT_FIRSTNAME).toString();
		QString strLastName = GET(RELATION_BCNT_LASTNAME).toString();
		QString strMidName	= GET(RELATION_BCNT_MIDDLENAME).toString();
		QString strOrgName	= GET(RELATION_BCNT_ORGANIZATIONNAME).toString();

		QString strLabel = strLastName;
		if (!strLabel.isEmpty())
			strLabel += ",";

		if (!strFirstName.isEmpty()){
			if (!strLabel.isEmpty())
				strLabel += " ";
			strLabel += strFirstName;
		}
		if (!strMidName.isEmpty()){
			if (!strLabel.isEmpty())
				strLabel += " ";
			strLabel += strMidName;
		}
		if (!strOrgName.isEmpty()){
			if (!strLabel.isEmpty())
				strLabel += " ";
			strLabel += strOrgName;
		}

		strData += strLabel;

		INS(role_data, strData);
	}
}

void ContactRelationsList::InitRoleDetails()
{
}
