#ifndef ContactRelationsList_H
#define ContactRelationsList_H

#include "report_core/report_core/reportpage.h"

class ContactRelationsList : public ReportPage
{
	Q_OBJECT

public:
    ContactRelationsList();
    ~ContactRelationsList();

	bool SetWizardRecordSetList(QList<DbRecordSet> &lstPageRecordSet);
	void ReportMethodCall(QString FunctionName);

private:
	void InitalizeLabels();
	void InitContactDetails();
	void InitRoleDetails();
	bool m_bFirstContact;

};

#endif // ContactRelationsList_H
