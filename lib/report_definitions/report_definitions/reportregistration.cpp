#include "reportregistration.h"
#include <QFileInfo>
#include <QCoreApplication>


//************************************************************
//						REPORT DECLARATIONS
//************************************************************

//#include "simplereport.h"
//#include "departmentstructures.h"

#include "contactdetails.h"
#include "contactlist.h"
#include "projectlist.h"
#include "contactcommunication.h"
#include "contactrelationslist.h"


void ReportRegistration::GetReportPage(QString strReportFile,ReportPage **pPage)
{
	int nRepID=GetReportIDFromFile(strReportFile);
	GetReportPage(nRepID,pPage);
}
void ReportRegistration::GetReportPage(int nReportID,ReportPage **pPage)
{
	switch(nReportID)
	{
	case REPORT_CONTACT_DETAILS:
		*pPage= new ContactDetails();
		break;
	case REPORT_CONTACT_COMMUNICATION_DETAILS:
		*pPage= new ContactCommunication();
		break;
	case REPORT_CONTACT_LIST:
		*pPage= new ContactList();
		break;
	case REPORT_PROJECT_LIST:
		*pPage= new ProjectList();
		break;
	case REPORT_CONTACT_RELATIONS:
		*pPage= new ContactRelationsList();
		break;
	default:
		Q_ASSERT(false);
		*pPage=NULL;
		break;
	}
}

QHash<int,QString> ReportRegistration::GetReportData()
{
	QHash<int,QString> repData;
	repData[REPORT_CONTACT_DETAILS]=QObject::tr("Contact Details");
	repData[REPORT_CONTACT_COMMUNICATION_DETAILS]=QObject::tr("Contact Communication Details");
	repData[REPORT_CONTACT_LIST]=QObject::tr("Contact List");
	repData[REPORT_PROJECT_LIST]=QObject::tr("Project List");
	repData[REPORT_CONTACT_RELATIONS]=QObject::tr("Contact Relationships");
	return repData;
}
QString ReportRegistration::GetReportPath(int nReportID)
{
	switch(nReportID)
	{
	case REPORT_CONTACT_DETAILS:
		return QCoreApplication::applicationDirPath()+"/report_definitions/1.xml";
		break;
	case REPORT_CONTACT_COMMUNICATION_DETAILS:
		return QCoreApplication::applicationDirPath()+"/report_definitions/2.xml";
		break;
	case REPORT_CONTACT_LIST:
		return QCoreApplication::applicationDirPath()+"/report_definitions/7.xml";
		break;
	case REPORT_PROJECT_LIST:
		return QCoreApplication::applicationDirPath()+"/report_definitions/8.xml";
		break;
	case REPORT_CONTACT_RELATIONS:
		return QCoreApplication::applicationDirPath()+"/report_definitions/9.xml";
		break;
	default:
		Q_ASSERT(false);
		break;
	}
	return "";
}

QString ReportRegistration::GetReportName(int nReportID)
{
	QHash<int,QString> lstData=GetReportData();
	return lstData.value(nReportID,"");
}


int ReportRegistration::GetReportIDFromFile(QString strReportFile)
{
	QFileInfo report_file(strReportFile);
	bool bOK;
	int nRepID=report_file.baseName().toInt(&bOK);
	Q_ASSERT(bOK);
	return nRepID;
}




/*
ReportRegistration::ReportRegistration()
{
	
	//*************************************************************
	//		Add report definitions here.
	//*************************************************************

	//Contact details.
	if(g_FunctionPoint.IsFPAvailable(FP_REPORT__CONTACT_DETAILS))
	{
		m_hshReportPageHash.insert(1, new ContactDetails());
		m_hshReportIDToNameHash.insert(1, tr("Contact Details"));
	}
	
	//Contact communication details.
	if(g_FunctionPoint.IsFPAvailable(FP_REPORT__CONTACT_COMMUNICATION_DETAILS))
	{
		m_hshReportPageHash.insert(2, new ContactCommunication());
		m_hshReportIDToNameHash.insert(2, tr("Contact Communication Details"));
	}
	
	//Contact list.
	if(g_FunctionPoint.IsFPAvailable(FP_REPORT__CONTACT_LIST))
	{
		m_hshReportPageHash.insert(7, new ContactList());
		m_hshReportIDToNameHash.insert(7, tr("Contact List"));
	}
	
	//Project list.
	if(g_FunctionPoint.IsFPAvailable(FP_REPORT__PROJECT_LIST))
	{
		m_hshReportPageHash.insert(8, new ProjectList());
		m_hshReportIDToNameHash.insert(8, tr("Project List"));
	}
}

ReportRegistration::~ReportRegistration()
{
	qDeleteAll(m_hshReportPageHash);
	m_hshReportPageHash.clear();
	m_hshReportIDToNameHash.clear();
}

QHash<int, ReportPage*> *ReportRegistration::GetReportRegistrationHash()
{
	return &m_hshReportPageHash;
}

QHash<int, QString>		*ReportRegistration::GetReportIDToNameHash()
{
	return &m_hshReportIDToNameHash;
}
*/
