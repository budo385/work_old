#ifndef PROJECTLIST_H
#define PROJECTLIST_H

#include "report_core/report_core/reportpage.h"

class ProjectList : public ReportPage
{
	Q_OBJECT

public:
	ProjectList();
	~ProjectList();
	bool SetWizardRecordSetList(QList<DbRecordSet> &lstPageRecordSet);

	void ReportMethodCall(QString FunctionName);

private:
	void InitalizeLabels();
	void InitDetail();
	QFont CalculateFont(int nLevel);
};

#endif // PROJECTLIST_H
