#ifndef CONTACTCOMMUNICATION_H
#define CONTACTCOMMUNICATION_H

#include "report_core/report_core/reportpage.h"
#include <QtWidgets/QFrame>

class ContactCommunication : public ReportPage
{
	Q_OBJECT

public:
	ContactCommunication();
	~ContactCommunication();

	bool SetWizardRecordSetList(QList<DbRecordSet> &lstPageRecordSet);
	void ReportMethodCall(QString FunctionName);

	void PrintContactWidget();
private:
	void InitalizeLabels();
	void PrintContactNameLabel();
	void InitContactDetails();

	int m_nViewID;
	DbRecordSet m_recFilterViewData;
	QHash<int, DbRecordSet> m_hshContactID2recEmail;
	QHash<int, DbRecordSet> m_hshContactID2recDocument;
	QHash<int, DbRecordSet> m_hshContactID2recVoiceCall;
};

#endif // CONTACTCOMMUNICATION_H
