#include "contactcommunication.h"
#include "gui_core/gui_core/macros.h"
#include "bus_client/bus_client/modulelicenseclientcache.h"
extern ModuleLicenseClientCache			g_FunctionPoint;			//global function point tester
#include "bus_interface/bus_interface/businessservicemanager.h"
#include "bus_core/bus_core/mainentityfilter.h"
extern BusinessServiceManager *g_pBoSet;

#include "gui_core/gui_core/picturehelper.h"
//#include "fui_collection/fui_collection/commworkbenchwidget.h"
#include "fui_collection/fui_collection/commgridview.h"
#include <QHeaderView>
#include <QLayout>
#include "trans/trans/xmlutil.h"

ContactCommunication::ContactCommunication()
{
	m_nViewID = -1;
}

ContactCommunication::~ContactCommunication()
{
}

//BT: lstPageRecordSet:0 = data, 1 =filter
bool ContactCommunication::SetWizardRecordSetList(QList<DbRecordSet> &lstPageRecordSet)
{
	if (lstPageRecordSet.size()!=2)
	{
		Q_ASSERT(false);
		return false;
	}

	//Filter data.
	DbRecordSet tmp(lstPageRecordSet.value(1));
	m_nViewID = tmp.getDataRef(0, "VIEW_ID").toInt();
	QByteArray byt = tmp.getDataRef(0, "VIEW_DATA_RECORDSET").toByteArray();
	m_recFilterViewData = XmlUtil::ConvertByteArray2RecordSet_Fast(byt);

	Status status;
	//Define contact full view recordset.
	DbRecordSet recContactRecordSet(lstPageRecordSet.value(0));

	//Get selected contacts count.
	int nContactCount = recContactRecordSet.getRowCount();
	QString strWhere = " BCNT_ID IN (";

	//Get selected departments recordset.
	//recContactRecordSet = m_lstPageRecordSet.value(1);
	for (int i = 0; i < nContactCount; ++i)
	{
		//recContactRecordSet.Dump();
		strWhere = strWhere + recContactRecordSet.getDataRef(i, 0).toString();
		if (nContactCount != (i+1))
			strWhere = strWhere + ",";
	}
	strWhere = strWhere + ") ";

	MainEntityFilter Filter;
	Filter.SetFilter(MainEntityFilter::FILTER_SQL_WHERE,strWhere);
	DbRecordSet recFilter(Filter.Serialize());

	//Get data for contacts.
	DbRecordSet recRetrievedContacts;
	_SERVER_CALL(BusContact->ReadData(status, recFilter, recRetrievedContacts))
	_CHK_ERR_RET_BOOL_ON_FAIL(status);

	//Sort retrieved recordset.
	DbRecordSet tmpRetrivedContacts;
	tmpRetrivedContacts.copyDefinition(recRetrievedContacts);
	int nCount = recContactRecordSet.getRowCount();
	for (int i = 0; i < nCount; ++i)
	{
		int nBCNT_ID = recContactRecordSet.getDataRef(i, "BCNT_ID").toInt();
		int nRow = recRetrievedContacts.find("BCNT_ID", nBCNT_ID, true);
		if (nRow >= 0)
			tmpRetrivedContacts.merge(recRetrievedContacts.getRow(nRow));
	}

	recRetrievedContacts.clear();
	recRetrievedContacts.merge(tmpRetrivedContacts);

	//Insert in detail hash.
	m_hshDetailRecordSetHash.insert("detail", recRetrievedContacts);

	//Get filter data if view selected.
	if (m_nViewID>0)
	{
		DbRecordSet recFilterViews;
		_SERVER_CALL(BusCommunication->GetCommFilterViewDataAndViews(status, m_recFilterViewData, recFilterViews, m_nViewID, 1/*Grid type - contacts*/))
		_CHK_ERR_RET_BOOL_ON_FAIL(status);

		int nRow = recFilterViews.find("BUSCV_ID", m_nViewID, true);
		QString strFilterName = recFilterViews.getDataRef(nRow, "BUSCV_NAME").toString();
		INS(filter_label, strFilterName);
	}

	//Get grid recordsets for each contact.
	int nFoundContacts = recRetrievedContacts.getRowCount();
	for (int i = 0; i < nFoundContacts; ++i)
	{
		DbRecordSet recEmail, recDocument, recVoiceCall;
		int nContactID = recRetrievedContacts.getDataRef(i, "BCNT_ID").toInt();
		_SERVER_CALL(BusCommunication->GetContactCommData(status, recEmail, recDocument, recVoiceCall, nContactID, m_recFilterViewData))
		_CHK_ERR_RET_BOOL_ON_FAIL(status);
		m_hshContactID2recEmail.insert(nContactID, recEmail);
		m_hshContactID2recDocument.insert(nContactID, recDocument);
		m_hshContactID2recVoiceCall.insert(nContactID, recVoiceCall);
	}

	return true;
}

void ContactCommunication::ReportMethodCall(QString FunctionName)
{
	if (FunctionName == "InitalizeLabels")
		InitalizeLabels();
	else if (FunctionName == "PrintContactNameLabel")
		PrintContactNameLabel();
	else if (FunctionName == "InitContactDetails")
		InitContactDetails();
	else if (FunctionName == "PrintContactWidget")
		PrintContactWidget();
}


void ContactCommunication::InitalizeLabels()
{
	QString strMsg;
	int nSN;
	QString strReportLine1, strReportLine2;
	QString strCustomerID;
	int nCustomerSolutionID;
	g_FunctionPoint.GetLicenseInfo(strMsg, nSN, strReportLine1, strReportLine2,strCustomerID,nCustomerSolutionID);


	QString dateLabel(tr("Date:"));
	QString pageLabel(tr("Page:"));

	INS(Firm_name, strReportLine1);
	INS(Firm_address, strReportLine2);
	INS(date_label, dateLabel);
	INS(page_label, pageLabel);

	//Detail header labels.
	//Detail header labels.
	QString DepartmentLabel(tr("Department:"));
	QString LocationLabel(tr("Location:"));
	QString PhonesLabel(tr("Phones"));
	QString EmailsLabel(tr("Emails"));
	QString InternetLabel(tr("Internet"));
	QString AddressesLabel(tr("Addresses"));

	INS(department_label, DepartmentLabel);
	INS(location_label, LocationLabel);
	INS(phones_label, PhonesLabel);
	INS(emails_label, EmailsLabel);
	INS(internet_label, InternetLabel);
	INS(address_label, AddressesLabel);
}

void ContactCommunication::PrintContactNameLabel()
{
	QString strFirstName = GET(BCNT_FIRSTNAME).toString();
	QString strLastName = GET(BCNT_LASTNAME).toString();
	QString strOrgName = GET(BCNT_ORGANIZATIONNAME).toString();
	QString ContactLabel(tr("Contact: "));
	ContactLabel += strFirstName;
	if (!strLastName.isEmpty())
		ContactLabel += " " + strLastName;
	if (!strOrgName.isEmpty())
		ContactLabel += ", " + strOrgName;
	
	INS(contact_label, ContactLabel);

	//Sex label.
	QString strMale(tr("Male"));
	QString strFemale(tr("Female"));
	bool Female = GET(BCNT_SEX).toBool();
	if (Female)
		INS(BCNT_SEX, strFemale);
	else
		INS(BCNT_SEX, strMale);
}

void ContactCommunication::InitContactDetails()
{
	//Insert other details hashes.
	DbRecordSet recPhones		= GET(LST_PHONE).value<DbRecordSet>();
	DbRecordSet recAddresses	= GET(LST_ADDRESS).value<DbRecordSet>();
	DbRecordSet recEmails		= GET(LST_EMAIL).value<DbRecordSet>();
	DbRecordSet recInternet		= GET(LST_INTERNET).value<DbRecordSet>();

	//Set it to 1 because it needs at least to print name...
	//Set recordset for sub detail.
	QList<int> lstRowCounts;
	lstRowCounts << recPhones.getRowCount();
	lstRowCounts << recAddresses.getRowCount();
	lstRowCounts << recEmails.getRowCount();
	lstRowCounts << recInternet.getRowCount();
	
	int nMaxRows = 1;	//Number of rows.
	int nCount = 1;		//Number of recordset (1 - recPhones, 2 - recAddresses, ...)
	int nID = 0;		//Which recordset has more rows.
	foreach(int nRowCount, lstRowCounts)
	{
		if (nRowCount > nMaxRows)
		{
			nMaxRows = nRowCount;
			nID = nCount;
		}
		nCount++;
	}

	//Take care of labels
	//Name
	QString strFirstName= GET(BCNT_FIRSTNAME).toString();
	QString strLastName = GET(BCNT_LASTNAME).toString();
	QString strName;
	if (!strFirstName.isEmpty())
		strName = strFirstName + " " + strLastName;
	else
		strName = strLastName;
	INS(name, strName);

	//Organization
	QString strOrgName	= GET(BCNT_ORGANIZATIONNAME).toString();
	INS(organization_multi_label, strOrgName);

	//Row count.
	int nRowCount;
	//Phone.
	QString strPhoneType = "";
	QString strPhoneNumber = "";
	QString strLabel = "";
	nRowCount = recPhones.getRowCount();
	for (int i = 0; i < nRowCount; i++)
	{
		strPhoneType	= recPhones.getDataRef(i, "BCMT_TYPE_NAME").toString();
		strPhoneNumber	= recPhones.getDataRef(i, "BCMP_FULLNUMBER").toString();
		if (strPhoneType.isEmpty())
			strLabel	+= strPhoneNumber;
		else
			strLabel	+= strPhoneType + ": " + strPhoneNumber;

		//If not last line add line break.
		if (!((i+1) == nRowCount))
			strLabel += "\n";
	}
	INS(phone_multi_label, strLabel);

	//Addresses.
	QString strAddressType = "";
	QString strAddress = "";
	QString strAddressLabel = "";
	nRowCount = recAddresses.getRowCount();
	for (int i = 0; i < nRowCount; i++)
	{
		strAddressType		= recAddresses.getDataRef(i, "BCMT_TYPE_NAME").toString();
		strAddress			= recAddresses.getDataRef(i, "BCMA_FORMATEDADDRESS").toString();
		if(strAddressType.isEmpty())
			strAddressLabel	+= strAddress;
		else
			strAddressLabel	+= strAddressType + ": " + strAddress;

		//If not last line add line break.
		if (!((i+1) == nRowCount))
			strAddressLabel += "\n";
	}
	INS(address_multi_label, strAddressLabel);

	//Email.
	QString strEmailType = "";
	QString strEmail = "";
	QString strEmailLabel = "";
	nRowCount = recEmails.getRowCount();
	for (int i = 0; i < nRowCount; i++)
	{
		strEmailType	= recEmails.getDataRef(i, "BCMT_TYPE_NAME").toString();
		strEmail		= recEmails.getDataRef(i, "BCME_ADDRESS").toString();
		if (strEmailType.isEmpty())
			strEmailLabel+= strEmail;
		else
			strEmailLabel+= strEmailType + ": " + strEmail;

		//If not last line add line break.
		if (!((i+1) == nRowCount))
			strEmailLabel += "\n";
	}
	INS(email_multi_label, strEmailLabel);

	//Internet.
	QString strInternetType = "";
	QString strInternet = "";
	QString strInternetLabel = "";
	nRowCount = recInternet.getRowCount();
	for (int i = 0; i < nRowCount; i++)
	{
		strInternetType	= recInternet.getDataRef(i, "BCMT_TYPE_NAME").toString();
		strInternet		= recInternet.getDataRef(i, "BCMI_ADDRESS").toString();
		if (strInternetType.isEmpty())
			strInternetLabel+= strInternet;
		else
			strInternetLabel+= strInternetType + ": " + strInternet;

		//If not last line add line break.
		if (!((i+1) == nRowCount))
			strInternetLabel += "\n";
	}
	INS(internet_multi_label, strInternetLabel);
}

void ContactCommunication::PrintContactWidget()
{
	DbRecordSet recEmail, recDocument, recVoiceCall;
	int nContactID = GET(BCNT_ID).toInt();
	recEmail	= m_hshContactID2recEmail.value(nContactID);
	recDocument = m_hshContactID2recDocument.value(nContactID);
	recVoiceCall= m_hshContactID2recVoiceCall.value(nContactID);
	
	WidgetDetail *widget_detail = static_cast<WidgetDetail*>(m_hshReportPrintingItems.value("widget_detail"));
	//Reset widget detail.
	widget_detail->Reset();
	double detailWidth	= widget_detail->m_dDetailWidth*m_nLogDPIX/25.4;

	if (!recEmail.getRowCount() && !recDocument.getRowCount() && !recVoiceCall.getRowCount())
		widget_detail->m_bHideItem = true;
	else
		widget_detail->m_bHideItem = false;

	int nRowCount = recEmail.getRowCount() + recDocument.getRowCount() + recVoiceCall.getRowCount();
	int nWidgetHeight = nRowCount*40; //Single row height is 40.

	QFrame frame;
	frame.setFrameStyle(QFrame::NoFrame);
	frame.setSizePolicy(QSizePolicy::Fixed, QSizePolicy::MinimumExpanding);
	frame.setFixedWidth(detailWidth);
	frame.setMinimumHeight(nWidgetHeight);
	CommGridView view;
	view.Initialize(&recEmail, &recDocument, &recVoiceCall, NULL, true);
	view.horizontalHeader()->hide();
	view.verticalHeader()->hide();
	view.setFrameStyle(QFrame::NoFrame);
	view.setFixedWidth(detailWidth+16);	//This 16 is for correct drawing - don't know why.
	view.setMinimumHeight(nWidgetHeight);
	view.setSizePolicy(QSizePolicy::Fixed, QSizePolicy::MinimumExpanding);

	QPixmap pixmap(frame.width(), frame.height());
	pixmap.fill(Qt::blue);

	QVBoxLayout layout;
	layout.addWidget(&view);
	frame.setLayout(&layout);
	frame.render(&pixmap);

	//Add picture to widget detail.
	widget_detail->m_PrintingWidgetPixmap = pixmap;
}
