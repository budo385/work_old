#ifndef SIMPLEREPORT_H
#define SIMPLEREPORT_H

#include "report_core/report_core/reportpage.h"

class SimpleReport : public ReportPage
{
public:
    SimpleReport();
    ~SimpleReport();
};

#endif // SIMPLEREPORT_H
