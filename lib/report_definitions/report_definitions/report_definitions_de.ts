<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>ContactCommunication</name>
    <message>
        <location filename="contactcommunication.cpp" line="137"/>
        <source>Date:</source>
        <translation>Datum:</translation>
    </message>
    <message>
        <location filename="contactcommunication.cpp" line="138"/>
        <source>Page:</source>
        <translation>Seite:</translation>
    </message>
    <message>
        <location filename="contactcommunication.cpp" line="147"/>
        <source>Department:</source>
        <translation>Abteilung:</translation>
    </message>
    <message>
        <location filename="contactcommunication.cpp" line="148"/>
        <source>Location:</source>
        <translation>Standort:</translation>
    </message>
    <message>
        <location filename="contactcommunication.cpp" line="149"/>
        <source>Phones</source>
        <translation>Telefonnummern</translation>
    </message>
    <message>
        <location filename="contactcommunication.cpp" line="150"/>
        <source>Emails</source>
        <translation>E-Mail-Adressen</translation>
    </message>
    <message>
        <location filename="contactcommunication.cpp" line="151"/>
        <source>Internet</source>
        <translation>Internet</translation>
    </message>
    <message>
        <location filename="contactcommunication.cpp" line="152"/>
        <source>Addresses</source>
        <translation>Adressen</translation>
    </message>
    <message>
        <location filename="contactcommunication.cpp" line="167"/>
        <source>Contact: </source>
        <translation>Kontakt: </translation>
    </message>
    <message>
        <location filename="contactcommunication.cpp" line="177"/>
        <source>Male</source>
        <translation>männlich</translation>
    </message>
    <message>
        <location filename="contactcommunication.cpp" line="178"/>
        <source>Female</source>
        <translation>weiblich</translation>
    </message>
</context>
<context>
    <name>ContactDetails</name>
    <message>
        <source>Helix Business Soft AG</source>
        <translation type="obsolete">Helix Business Soft AG</translation>
    </message>
    <message>
        <source>5405 Baden-Dättwill</source>
        <translation type="obsolete">CH-5405 Baden-Dättwill</translation>
    </message>
    <message>
        <location filename="contactdetails.cpp" line="112"/>
        <source>Date:</source>
        <translation>Datum:</translation>
    </message>
    <message>
        <location filename="contactdetails.cpp" line="113"/>
        <source>Page:</source>
        <translation>Seite:</translation>
    </message>
    <message>
        <location filename="contactdetails.cpp" line="123"/>
        <source>First Name:</source>
        <translation>Vorname:</translation>
    </message>
    <message>
        <location filename="contactdetails.cpp" line="124"/>
        <source>Middle Name:</source>
        <translation>Zweiter Vorname:</translation>
    </message>
    <message>
        <location filename="contactdetails.cpp" line="125"/>
        <source>Last Name:</source>
        <translation>Nachname:</translation>
    </message>
    <message>
        <location filename="contactdetails.cpp" line="126"/>
        <source>Nick Name:</source>
        <translation>Rufname:</translation>
    </message>
    <message>
        <location filename="contactdetails.cpp" line="127"/>
        <source>Sex:</source>
        <translation>Geschlecht:</translation>
    </message>
    <message>
        <location filename="contactdetails.cpp" line="128"/>
        <source>Birthday:</source>
        <translation>Geburtstag:</translation>
    </message>
    <message>
        <location filename="contactdetails.cpp" line="130"/>
        <source>Organization:</source>
        <translation>Organisation:</translation>
    </message>
    <message>
        <location filename="contactdetails.cpp" line="131"/>
        <source>Department:</source>
        <translation>Abteilung:</translation>
    </message>
    <message>
        <location filename="contactdetails.cpp" line="132"/>
        <source>Location:</source>
        <translation>Standort:</translation>
    </message>
    <message>
        <location filename="contactdetails.cpp" line="133"/>
        <source>Function:</source>
        <translation>Funktion:</translation>
    </message>
    <message>
        <location filename="contactdetails.cpp" line="134"/>
        <source>Profession:</source>
        <translation>Beruf:</translation>
    </message>
    <message>
        <location filename="contactdetails.cpp" line="136"/>
        <source>Description:</source>
        <translation>Beschreibung:</translation>
    </message>
    <message>
        <location filename="contactdetails.cpp" line="137"/>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <location filename="contactdetails.cpp" line="139"/>
        <source>Phone Numbers</source>
        <translation>Telefonnummern</translation>
    </message>
    <message>
        <location filename="contactdetails.cpp" line="140"/>
        <source>Number</source>
        <translation>Nummer</translation>
    </message>
    <message>
        <location filename="contactdetails.cpp" line="141"/>
        <source>Type</source>
        <translation>Art</translation>
    </message>
    <message>
        <location filename="contactdetails.cpp" line="142"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="contactdetails.cpp" line="144"/>
        <source>Email Addresses</source>
        <translation>E-Mail-Adressen</translation>
    </message>
    <message>
        <location filename="contactdetails.cpp" line="145"/>
        <source>Internet Addresses</source>
        <translation>Internet-Adressen</translation>
    </message>
    <message>
        <location filename="contactdetails.cpp" line="146"/>
        <source>Addresses</source>
        <translation>Adressen</translation>
    </message>
    <message>
        <location filename="contactdetails.cpp" line="147"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <location filename="contactdetails.cpp" line="184"/>
        <source>Contact: </source>
        <translation>Kontakt: </translation>
    </message>
    <message>
        <location filename="contactdetails.cpp" line="194"/>
        <source>Male</source>
        <translation>männlich</translation>
    </message>
    <message>
        <location filename="contactdetails.cpp" line="195"/>
        <source>Female</source>
        <translation>weiblich</translation>
    </message>
    <message>
        <location filename="contactdetails.cpp" line="39"/>
        <source>Warning!</source>
        <translation>Warnung!</translation>
    </message>
    <message>
        <location filename="contactdetails.cpp" line="39"/>
        <source>This operation could last several minutes. Continue?</source>
        <translation>Diese Funktion könnte mehrere Minuten dauern. Weiterfahren?</translation>
    </message>
</context>
<context>
    <name>ContactList</name>
    <message>
        <source>Helix Business Soft AG</source>
        <translation type="obsolete">Helix Business Soft AG</translation>
    </message>
    <message>
        <source>5405 Baden-Dättwill</source>
        <translation type="obsolete">CH-5405 Baden-Dättwill</translation>
    </message>
    <message>
        <location filename="contactlist.cpp" line="82"/>
        <source>Date:</source>
        <translation>Datum:</translation>
    </message>
    <message>
        <location filename="contactlist.cpp" line="83"/>
        <source>Page:</source>
        <translation>Seite:</translation>
    </message>
    <message>
        <location filename="contactlist.cpp" line="93"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="contactlist.cpp" line="94"/>
        <source>Organization</source>
        <translation>Organisation</translation>
    </message>
    <message>
        <location filename="contactlist.cpp" line="95"/>
        <source>Phones</source>
        <translation>Telefonnummern</translation>
    </message>
    <message>
        <location filename="contactlist.cpp" line="96"/>
        <source>Emails</source>
        <translation>E-Mail-Adressen</translation>
    </message>
    <message>
        <location filename="contactlist.cpp" line="97"/>
        <source>Internet</source>
        <translation>Internet</translation>
    </message>
    <message>
        <location filename="contactlist.cpp" line="98"/>
        <source>Addresses</source>
        <translation>Adressen</translation>
    </message>
</context>
<context>
    <name>ContactRelationsList</name>
    <message>
        <location filename="contactrelationslist.cpp" line="127"/>
        <source>Date:</source>
        <translation>Datum:</translation>
    </message>
    <message>
        <location filename="contactrelationslist.cpp" line="128"/>
        <source>Page:</source>
        <translation>Seite:</translation>
    </message>
    <message>
        <location filename="contactrelationslist.cpp" line="131"/>
        <source>Contact Info</source>
        <translation>Kontakte-Info</translation>
    </message>
</context>
<context>
    <name>DepartmentStructures</name>
    <message>
        <location filename="departmentstructures.cpp" line="58"/>
        <source>Totaly </source>
        <translation>Total </translation>
    </message>
    <message>
        <location filename="departmentstructures.cpp" line="58"/>
        <source> departments.</source>
        <translation> Abteilungen.</translation>
    </message>
    <message>
        <location filename="departmentstructures.cpp" line="64"/>
        <source>Helix Business Soft AG</source>
        <translation>Helix Business Soft AG</translation>
    </message>
    <message>
        <location filename="departmentstructures.cpp" line="65"/>
        <source>5405 Baden-D�ttwill</source>
        <oldsource>5405 Baden-Dättwill</oldsource>
        <translation type="unfinished">CH-5405 Baden-Dättwill</translation>
    </message>
    <message>
        <location filename="departmentstructures.cpp" line="67"/>
        <source>Date:</source>
        <translation>Datum:</translation>
    </message>
    <message>
        <location filename="departmentstructures.cpp" line="68"/>
        <source>Page:</source>
        <translation>Seite:</translation>
    </message>
    <message>
        <location filename="departmentstructures.cpp" line="69"/>
        <source>Code</source>
        <translation>Code</translation>
    </message>
    <message>
        <location filename="departmentstructures.cpp" line="70"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="departmentstructures.cpp" line="71"/>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <location filename="departmentstructures.cpp" line="72"/>
        <source>Cost center</source>
        <translation>Kostenstelle</translation>
    </message>
</context>
<context>
    <name>ProjectList</name>
    <message>
        <location filename="projectlist.cpp" line="58"/>
        <source>Date:</source>
        <translation>Datum:</translation>
    </message>
    <message>
        <location filename="projectlist.cpp" line="59"/>
        <source>Page:</source>
        <translation>Seite:</translation>
    </message>
    <message>
        <location filename="projectlist.cpp" line="67"/>
        <source>Code</source>
        <translation>Code</translation>
    </message>
    <message>
        <location filename="projectlist.cpp" line="68"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="projectlist.cpp" line="69"/>
        <source>Leader</source>
        <translation>Leiter</translation>
    </message>
    <message>
        <location filename="projectlist.cpp" line="70"/>
        <source>Start Date</source>
        <translation>Startdatum</translation>
    </message>
    <message>
        <location filename="projectlist.cpp" line="71"/>
        <source>Deadline</source>
        <translation>Termin</translation>
    </message>
    <message>
        <location filename="projectlist.cpp" line="72"/>
        <source>Completion</source>
        <translation>Fertigstellung</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="reportregistration.cpp" line="54"/>
        <source>Contact Details</source>
        <translation>Kontaktblatt</translation>
    </message>
    <message>
        <location filename="reportregistration.cpp" line="55"/>
        <source>Contact Communication Details</source>
        <translation>Details Kontakt-Kommunikation</translation>
    </message>
    <message>
        <location filename="reportregistration.cpp" line="56"/>
        <source>Contact List</source>
        <translation>Kontaktliste</translation>
    </message>
    <message>
        <location filename="reportregistration.cpp" line="57"/>
        <source>Project List</source>
        <translation>Projektliste</translation>
    </message>
    <message>
        <location filename="reportregistration.cpp" line="58"/>
        <source>Contact Relationships</source>
        <translation>Kontakt-Beziehungen</translation>
    </message>
</context>
<context>
    <name>ReportRegistration</name>
    <message>
        <source>Contact Details</source>
        <translation type="obsolete">Kontaktblatt</translation>
    </message>
    <message>
        <source>Contact List</source>
        <translation type="obsolete">Kontaktliste</translation>
    </message>
    <message>
        <source>Contact Communication Details</source>
        <translation type="obsolete">Details Kontakt-Kommunikation</translation>
    </message>
    <message>
        <source>Project List</source>
        <translation type="obsolete">Projektliste</translation>
    </message>
</context>
</TS>
