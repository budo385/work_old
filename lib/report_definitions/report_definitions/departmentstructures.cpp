#include "departmentstructures.h"
#include "db_core/db_core/dbtableiddefinition.h"
#include "db_core/db_core/dbsqltableview.h"

DepartmentStructures::DepartmentStructures() : ReportPage()
{
}

DepartmentStructures::~DepartmentStructures()
{
}

//BT: lstPageRecordSet:0 = data
bool DepartmentStructures::SetWizardRecordSetList(QList<DbRecordSet> &lstPageRecordSet)
{

	if (lstPageRecordSet.size()!=1)
	{
		Q_ASSERT(false);
		return false;
	}
	//Set report detail things.
	m_hshDetailToTableIDHash.insert("detail", BUS_DEPARTMENT);
	m_hshDetailToTableViewHash.insert("detail", (int)DbSqlTableView::TVIEW_ORG_DEPT_REPORT);

	//Get report wizard recordset.
	DbRecordSet DepartmentsRecordSet;
	QString strWhere = "WHERE BDEPT_ID IN (";
	
	//Get selected departments recordset.
	DepartmentsRecordSet = m_lstPageRecordSet.value(0);

	int RowCount = DepartmentsRecordSet.getRowCount();
	for (int i = 0; i < RowCount; ++i)
	{
		strWhere = strWhere + "'" + DepartmentsRecordSet.getDataRef(i, 0).toString() + "'" ;
		if (RowCount != (i+1))
			strWhere = strWhere + ",";
	}
	strWhere = strWhere + ") ";
	strWhere += " AND BDEPT_ORGANIZATION_ID = BORG_ID ORDER BY BORG_ID, BDEPT_ORGANIZATION_ID";

	m_hshDetailToWhereConditionHash.insert("detail", strWhere);

	return true;
}

void DepartmentStructures::ReportMethodCall(QString FunctionName)
{
	if (FunctionName == "InitalizeLabels")
		InitalizeLabels();
	else if (FunctionName == "GetDepartmentCount")
		GetDepartmentCount();
}

void DepartmentStructures::GetDepartmentCount()
{
	QString strLabel = tr("Totaly ") + GET(department_sum_field).toString() + tr(" departments.");
	INS(detail_label2, strLabel);
}

void DepartmentStructures::InitalizeLabels()
{
	QString HelixNameLabel(tr("Helix Business Soft AG"));
	QString HelixAddressLabel(tr("5405 Baden-D�ttwill"));

	QString dateLabel(tr("Date:"));
	QString pageLabel(tr("Page:"));
	QString codeLabel(tr("Code"));
	QString nameLabel(tr("Name"));
	QString descrLabel(tr("Description"));
	QString costCenterLabel(tr("Cost center"));

	INS(Helix_name, HelixNameLabel);
	INS(Helix_address, HelixAddressLabel);

	INS(date_label, dateLabel);
	INS(page_label, pageLabel);
	INS(code_label, codeLabel);
	INS(name_label, nameLabel);
	INS(description_label, descrLabel);
	INS(cost_center_label, costCenterLabel);
}
