#include "projectlist.h"
#include "bus_client/bus_client/modulelicenseclientcache.h"
extern ModuleLicenseClientCache			g_FunctionPoint;			//global function point tester
#include "bus_interface/bus_interface/businessservicemanager.h"
extern BusinessServiceManager *g_pBoSet;
#include "db_core/db_core/dbsqltabledefinition.h"
#include "db_core/db_core/dbsqltableview.h"
#include "gui_core/gui_core/macros.h"



ProjectList::ProjectList()
{
}

ProjectList::~ProjectList()
{

}

void ProjectList::ReportMethodCall(QString FunctionName)
{
	if (FunctionName == "InitalizeLabels")
		InitalizeLabels();
	if (FunctionName == "InitDetail")
		InitDetail();
}
//BT: lstPageRecordSet:0 = data
bool ProjectList::SetWizardRecordSetList(QList<DbRecordSet> &lstPageRecordSet)
{
	if (lstPageRecordSet.size()!=1)
	{
		Q_ASSERT(false);
		return false;
	}

	Status status;
	//Define contact full view recordset.
	DbRecordSet recWizardProjectRecordSet(lstPageRecordSet.value(0));
	DbRecordSet recDetailRecordSet;
	_SERVER_CALL(ClientSimpleORM->ReadFromParentIDs(status, BUS_PROJECT, recDetailRecordSet, "BUSP_ID", recWizardProjectRecordSet, "BUSP_ID", "", " ORDER BY BUSP_CODE", DbSqlTableView::MVIEW_REPORT_PROJECTS_VIEW))
	_CHK_ERR_RET_BOOL_ON_FAIL(status);

	m_hshDetailRecordSetHash.insert("detail_0", recDetailRecordSet);

	return true;
}

void ProjectList::InitalizeLabels()
{
	QString strMsg;
	int nSN;
	QString strReportLine1, strReportLine2;
	QString strCustomerID;
	int nCustomerSolutionID;
	g_FunctionPoint.GetLicenseInfo(strMsg, nSN, strReportLine1, strReportLine2,strCustomerID,nCustomerSolutionID);

	QString dateLabel(tr("Date:"));
	QString pageLabel(tr("Page:"));

	INS(Firm_name, strReportLine1);
	INS(Firm_address, strReportLine2);
	INS(date_label, dateLabel);
	INS(page_label, pageLabel);

	//Detail header labels.
	QString CodeLabel(tr("Code"));
	QString NameLabel(tr("Name"));
	QString LeaderLabel(tr("Leader"));
	QString StartDateLabel(tr("Start Date"));
	QString DeadLineLabel(tr("Deadline"));
	QString CompletionLabel(tr("Completion"));

	INS(code_label, CodeLabel);
	INS(name_label, NameLabel);
	INS(leader_label, LeaderLabel);
	INS(startdate_label, StartDateLabel);
	INS(deadline_label, DeadLineLabel);
	INS(completion_label, CompletionLabel);
}

void ProjectList::InitDetail()
{
	//Code_name_multi_label.
	QString strCode = GET(BUSP_CODE).toString() + " " + GET(BUSP_NAME).toString();

	int nLevel = GET(BUSP_LEVEL).toInt();
	int nXoffset = 0;
	int nLabelWidthCorrection = 0;
	
	nXoffset = (nLevel-1) * 5;
	nLabelWidthCorrection = (nLevel-1) * (-5);

	int dX = 13;
	int m_nLabelLength = 95;
	static_cast<FieldElement*>(m_hshPrintingElementHash.value("code_name_multi_label"))->m_dX = dX + nXoffset;
	static_cast<FieldElement*>(m_hshPrintingElementHash.value("code_name_multi_label"))->m_nLabelLength = m_nLabelLength + nLabelWidthCorrection;
	static_cast<FieldElement*>(m_hshPrintingElementHash.value("code_name_multi_label"))->m_Font = CalculateFont(nLevel);

	INS(code_name_multi_label, strCode);

	//Initials label.
	QString strInitials = GET(BPER_INITIALS).toString();
	INS(initials_label, strInitials);

	//Start Date label.
	QString strStartDate = GET(BUSP_START_DATE).toDate().toString(Qt::LocalDate);
	INS(start_date_label, strStartDate);

	//Deadline Date label.
	QString strDeadlineDate = GET(BUSP_DEADLINE_DATE).toDate().toString(Qt::LocalDate);
	INS(deadline_date_label, strDeadlineDate);

	//Deadline Date label.
	QString strCompletionDate = GET(BUSP_COMPLETION_DATE).toDate().toString(Qt::LocalDate);
	INS(completion_date_label, strCompletionDate);
}

QFont ProjectList::CalculateFont(int nLevel)
{
	QFont LabelFont;
	LabelFont.setFamily("Helvetica");
	
	switch(nLevel)
	{
	case 1:
		LabelFont.setPointSize(12);
		LabelFont.setBold(true);
		LabelFont.setItalic(false);
		break;
	case 2:
		LabelFont.setPointSize(11);
		LabelFont.setBold(true);
		LabelFont.setItalic(true);
		break;
	case 3:
		LabelFont.setPointSize(10);
		LabelFont.setBold(true);
		LabelFont.setItalic(false);
		break;
	case 4:
		LabelFont.setPointSize(10);
		LabelFont.setBold(false);
		LabelFont.setItalic(false);
		break;
	case 5:
		LabelFont.setPointSize(9);
		LabelFont.setBold(false);
		LabelFont.setItalic(true);
		break;
	case 6:
		LabelFont.setPointSize(8);
		LabelFont.setBold(false);
		LabelFont.setItalic(false);
		break;
	case 7:
		LabelFont.setPointSize(7);
		LabelFont.setBold(false);
		LabelFont.setItalic(false);
		break;
	default:
		LabelFont.setPointSize(7);
		LabelFont.setBold(false);
		LabelFont.setItalic(false);
		break;
	}

	return LabelFont;
}
