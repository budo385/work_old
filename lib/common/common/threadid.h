#ifndef THREAD_ID_H 
#define THREAD_ID_H

#include <QThread>
//#include "common/serverthread.h"

/*!
    \class ThreadIdentificator
    \brief Gets current thread ID, use as static
    \ingroup Thread

	Uses QThread::currentThreadId()

*/


class ThreadIdentificator
{
public:
	static int GetCurrentThreadID();
};



class CComplex
{
public:
		CComplex():m_real(),m_imag(){};
		CComplex(double re,double im): m_real(re),m_imag(im){};
		CComplex(CComplex & c){m_real = c.m_real; m_imag=c.m_imag;};
		CComplex & operator=(CComplex &c)
		{
			if(this != &c)
			{
				m_real = c.m_real;
				m_imag=c.m_imag;
			}
			return *this;
		}
private: double m_real;
		 double m_imag;

};

class CComplexArray{
	CComplex *m_pArray;
	int m_size;
public:
	CComplexArray(int N);
	~CComplexArray();
	int GetSize(){return m_size;};
	CComplex & operator [] (int index);
};

/*
void T0Vector(const CNiz & in,vector<int> &out);

class CNiz{
	friend void T0Vector(const CNiz & in,vector<int> &out);
		
protected:
	unsigned int m_kap;
	int * m_niz;
public:
	CNiz(){m_kap=100;m_niz=new int [100];}
	CNiz(unsigned int kapacitet){m_kap=kapacitet;m_niz=new int [kapacitet];}
	~CNiz(){delete[m_kap] m_niz;};
	void Set(unsigned int indeks, int broj);
	int Get(unsigned int indeks);

	
};

class CKompleksniNiz : public CNiz
{



};

*/

#endif //THREAD_ID_H
