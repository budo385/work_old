#include "dbrecordset.h"
#include "loggerabstract.h"

static int static_fiasko_workaround=DbRecordSet::GetVariantType(); //this will ensure that it will be executed as global object (first)

//only compares list of filters:
bool DbRecordSet::DBColumn::operator ==(const DbRecordSet::DBColumn &that)
{
	if (m_strName==that.m_strName && m_nType==that.m_nType)
		return true;
	else
		return false;
}


DbRecordSet::DBRowAdvanced::DBRowAdvanced(const DBRowAdvanced &that)
{
	operator = (that);
}
DbRecordSet::DBRowAdvanced &DbRecordSet::DBRowAdvanced::operator =(const DBRowAdvanced &that)
{
	if(this != &that)
	{
		DBRow=that.DBRow;
		bSelected=that.bSelected;
	}
	return *this;
}



//compares two rows, called by qTableSort
bool DbRecordSet::DBRowComparator::operator ()(const DBRowAdvanced &a, const DBRowAdvanced &b) const
{
	bool bRes = false;
	bool bEqual=false;
	int nRes;


	int nSize=m_lstSortCol.size();


	for(int i=0;i<nSize;++i)
	{
		int nSortCol=m_lstSortCol.at(i).m_nColumn;
		//bool bSortAsc=m_lstSortCol.at(i).m_nSortOrder;			

		if(m_lstSortCol.at(i).m_nSortOrder==0) //ascending
		{


			switch((*m_lstColumns)[nSortCol].m_nType)
			{
			case QVariant::Int:
				bRes = a.DBRow.at(nSortCol).value<int>() < b.DBRow.at(nSortCol).value<int>();
				if(!bRes)bEqual=a.DBRow.at(nSortCol).value<int>() == b.DBRow.at(nSortCol).value<int>();
				break;
			case QVariant::String:
				//sorting string in right sort order:
				nRes=QString::localeAwareCompare(a.DBRow.at(nSortCol).value<QString>(),b.DBRow.at(nSortCol).value<QString>());
				if (nRes>0)
					bRes=false;
				else if (nRes<0)
					bRes=true;
				else
					bEqual=true;
				break;
			case QVariant::Double:
				bRes = a.DBRow.at(nSortCol).value<double>() < b.DBRow.at(nSortCol).value<double>();
				if(!bRes)bEqual=a.DBRow.at(nSortCol).value<double>() == b.DBRow.at(nSortCol).value<double>();
				break;
			case QVariant::Bool:
				bRes = a.DBRow.at(nSortCol).value<bool>() < b.DBRow.at(nSortCol).value<bool>();
				if(!bRes)bEqual=a.DBRow.at(nSortCol).value<bool>() == b.DBRow.at(nSortCol).value<bool>();
				break;
			case QVariant::DateTime:
				bRes = a.DBRow.at(nSortCol).value<QDateTime>() < b.DBRow.at(nSortCol).value<QDateTime>();
				if(!bRes)bEqual=a.DBRow.at(nSortCol).value<QDateTime>() == b.DBRow.at(nSortCol).value<QDateTime>();
				break;
			case QVariant::Date:
				bRes = a.DBRow.at(nSortCol).value<QDate>() < b.DBRow.at(nSortCol).value<QDate>();
				if(!bRes)bEqual=a.DBRow.at(nSortCol).value<QDate>() == b.DBRow.at(nSortCol).value<QDate>();
				break;
			case QVariant::Time:
				bRes = a.DBRow.at(nSortCol).value<QTime>() < b.DBRow.at(nSortCol).value<QTime>();
				if(!bRes)bEqual=a.DBRow.at(nSortCol).value<QTime>() == b.DBRow.at(nSortCol).value<QTime>();
				break;
			case QVariant::ByteArray:
				bRes = a.DBRow.at(nSortCol).value<QByteArray>() < b.DBRow.at(nSortCol).value<QByteArray>();
				if(!bRes)bEqual=a.DBRow.at(nSortCol).value<QByteArray>() == b.DBRow.at(nSortCol).value<QByteArray>();
				break;
			default:
				Q_ASSERT(false);	//comparison not implemented for this type
			}
			//if equal continue in loop
			if(bEqual)
			{bEqual=false;continue;}
			else
				break;  //means event occur: either less or greater
		}


		else

		{
			int nSortCol=m_lstSortCol.at(i).m_nColumn;
			switch((*m_lstColumns)[nSortCol].m_nType)
			{
			case QVariant::Int:
				bRes = a.DBRow.at(nSortCol).value<int>() > b.DBRow.at(nSortCol).value<int>();
				if(!bRes)bEqual=a.DBRow.at(nSortCol).value<int>() == b.DBRow.at(nSortCol).value<int>();
				break;
			case QVariant::String:
				//sorting string in right sort order:
				nRes=QString::localeAwareCompare(a.DBRow.at(nSortCol).value<QString>(),b.DBRow.at(nSortCol).value<QString>());
				if (nRes<0)
					bRes=false;
				else if (nRes>0)
					bRes=true;
				else
					bEqual=true;
				break;
			case QVariant::Double:
				bRes = a.DBRow.at(nSortCol).value<double>() > b.DBRow.at(nSortCol).value<double>();
				if(!bRes)bEqual=a.DBRow.at(nSortCol).value<double>() == b.DBRow.at(nSortCol).value<double>();
				break;
			case QVariant::Bool:
				bRes = a.DBRow.at(nSortCol).value<bool>() > b.DBRow.at(nSortCol).value<bool>();
				if(!bRes)bEqual=a.DBRow.at(nSortCol).value<bool>() == b.DBRow.at(nSortCol).value<bool>();
				break;
			case QVariant::DateTime:
				bRes = a.DBRow.at(nSortCol).value<QDateTime>() > b.DBRow.at(nSortCol).value<QDateTime>();
				if(!bRes)bEqual=a.DBRow.at(nSortCol).value<QDateTime>() == b.DBRow.at(nSortCol).value<QDateTime>();
				break;
			case QVariant::Date:
				bRes = a.DBRow.at(nSortCol).value<QDate>() > b.DBRow.at(nSortCol).value<QDate>();
				if(!bRes)bEqual=a.DBRow.at(nSortCol).value<QDate>() == b.DBRow.at(nSortCol).value<QDate>();
				break;
			case QVariant::Time:
				bRes = a.DBRow.at(nSortCol).value<QTime>() > b.DBRow.at(nSortCol).value<QTime>();
				if(!bRes)bEqual=a.DBRow.at(nSortCol).value<QTime>() == b.DBRow.at(nSortCol).value<QTime>();
				break;
			case QVariant::ByteArray:
				bRes = a.DBRow.at(nSortCol).value<QByteArray>() > b.DBRow.at(nSortCol).value<QByteArray>();
				if(!bRes)bEqual=a.DBRow.at(nSortCol).value<QByteArray>() == b.DBRow.at(nSortCol).value<QByteArray>();
				break;
			default:
				Q_ASSERT(false);	//comparison not implemented for this type
			}

			//if equal continue in loop
			if(bEqual)
			{bEqual=false;continue;}
			else
				break;  //means event occur: either less or greater 
		}

	}


	return bRes;
}

int DbRecordSet::GetVariantType()
{
	static int nDbRecType=qRegisterMetaType<DbRecordSet>("DbRecordSet");	//B.T. to avoid static fiasko!!!->this line is executed on first call, then it will be ok...
	return nDbRecType;
}

DbRecordSet::DbRecordSet()
{
}

DbRecordSet::~DbRecordSet()
{
}

DbRecordSet::DbRecordSet(const DbRecordSet &that)
{
	operator = (that);
}

DbRecordSet &DbRecordSet::operator =(const DbRecordSet &that)
{
	if(this != &that)
	{
		m_lstColumns	= that.m_lstColumns;
		m_lstData		= that.m_lstData;
		m_hshColumnMap	= that.m_hshColumnMap;
	}
	return *this;
}


DbRecordSet::DbRecordSet(const DbView &view)
{
	defineFromView(view);
}




//-------------------------------------------------
//				SORT DATA
//-------------------------------------------------

SortData::SortData(const SortData &that)
{
	operator = (that);
}

SortData &SortData::operator =(const SortData &that)
{
	if(this != &that)
	{
		m_nColumn			= that.m_nColumn;
		m_nSortOrder		= that.m_nSortOrder;
	}
	return *this;
}



//-------------------------------------------------------------------
//				ADV ROW
//-------------------------------------------------------------------

//copy constructors
/*
DbRecordSet::DBRowAdvanced::DBRowAdvanced(const DBRowAdvanced &that)
{
	operator = (that);
}

DbRecordSet::DBRowAdvanced &DbRecordSet::DBRowAdvanced::operator =(const DBRowAdvanced &that)
{
	bSelected=that.bSelected;
	DBRow=that.DBRow;
	return *this;
}

*/

/*!
	Adds column into recordset definiton, at end
	\param nDataType	- data type (see QVariant)
	\param strName		- unique name of col
	\param strAlias		- col alias or header (opt)
	\param strTable		- table name (opt)

	\return true if success

*/
bool DbRecordSet::addColumn(int nDataType, const QString &strName)//, const QString &strAlias, const QString &strTable)
{
	//column name must be unique
	Q_ASSERT(getColumnIdx(strName) == -1);
		//return false;	// already exists

	//store column definition
	DBColumn column;
	column.m_nType		= nDataType;
	//column.m_strAlias	= strAlias;
	column.m_strName	= strName;
	//column.m_strTable	= strTable;

	m_lstColumns.append(column);
	m_hshColumnMap[strName]=m_lstColumns.size()-1;

	//rebuild existing data
	if (m_lstData.size()>0)
		redefineContents(m_lstColumns.size()-1, true);

	return true;
}


/*!
	Gets column defintion at given column position, if out of range return false
	\param nColPos column index
	\param nDataType QVariant::Type
	\param strName name of column
	\param strAlias column alias
	\param strTable table name
	\return false if column with index nColPos does not exist in record set
*/
bool DbRecordSet::getColumn(int nColPos,int &nDataType, QString &strName) const //, QString &strAlias,QString &strTable)  const
{
	//check range (automatic by QT QList object:
	//Q_ASSERT(if( nColPos<0||nColPos>=m_lstColumns.size()) return false;
	
	//get column data:
	nDataType=m_lstColumns.at(nColPos).m_nType;
	strName=m_lstColumns.at(nColPos).m_strName;
	//strAlias=m_lstColumns.at(nColPos).m_strAlias;
	//strTable=m_lstColumns.at(nColPos).m_strTable;

	return true;
}

bool DbRecordSet::setColumn(int nColPos,int nDataType, QString strName)//, QString &strAlias,QString &strTable)
{
	//check range:
	if( nColPos<0||nColPos>=m_lstColumns.size()) return false;

	m_hshColumnMap.remove(m_lstColumns.at(nColPos).m_strName);

	//set column data:
	DBColumn column;
	column.m_nType = nDataType;
	column.m_strName = strName;
	//column.m_strAlias = strAlias;
	//column.m_strTable = strTable;

	m_hshColumnMap[strName]=nColPos;

	m_lstColumns.replace(nColPos, column);
	return true;
}

bool DbRecordSet::removeColumn(int nCol)
{
	//rebuild existing data
	redefineContents(nCol, false);

	m_hshColumnMap.remove(m_lstColumns.at(nCol).m_strName);
	//MR 2008.04.22 decrement column indexes in the map (only columns after this one)
	int nMax = m_lstColumns.size();
	for(int i=nCol+1; i<nMax; i++)
		m_hshColumnMap[m_lstColumns.at(i).m_strName]=i-1;	//prepare to delete

	m_lstColumns.removeAt(nCol);
	return true;
}

bool DbRecordSet::renameColumn(int nCol, QString strNewName)
{
	m_hshColumnMap.remove(m_lstColumns.at(nCol).m_strName);
	m_lstColumns[nCol].m_strName = strNewName;
	m_hshColumnMap[strNewName]=nCol;
	return true;
}

bool DbRecordSet::renameColumn(QString strOldName, QString strNewName)
{
	int nCol = getColumnIdx(strOldName);
	return renameColumn(nCol, strNewName);
}



/*!
	Adds empty row at end of recordset
	\param	nRowToAdd	no of rows to add: default is 1
	\return true		if success

*/
bool DbRecordSet::addRow(int nRowToAdd)
{
	if (nRowToAdd==0) 
		return true;
	DBRowAdvanced row;
	int lstSize = m_lstColumns.size();
	for(int i=0; i < lstSize; i++)
		row.DBRow.append(QVariant((QVariant::Type)m_lstColumns[i].m_nType));

	if (nRowToAdd==1)
		m_lstData.append(row);
	else
	{
		for (int i=0;i<nRowToAdd;++i)
			m_lstData.append(row);
	}
	return true;
}

bool DbRecordSet::insertRow(int nPosition)
{
	if (nPosition<0)
		return false;

	//If position is bigger than number of rows then append rows.
	if (nPosition >= getRowCount())
	{
		addRow();
		return true;
	}

	//Else insert rows at position.
	DBRowAdvanced row;
	int lstSize = m_lstColumns.size();
	for(int i=0; i < lstSize; i++)
		row.DBRow.append(QVariant((QVariant::Type)m_lstColumns[i].m_nType));

	m_lstData.insert(nPosition, row);
	return true;
}

bool DbRecordSet::deleteRow(int nRow)
{
	//assert for valid index
	Q_ASSERT(nRow >= 0 && nRow < m_lstData.size());

	if(0 <= nRow && nRow < m_lstData.size())
	{
		m_lstData.removeAt(nRow);
		return true;
	}
	return false;
}


//-----------------------------------------
//			GET DATA by REF:
//-----------------------------------------

/*!
	Fastest way to get data from recordset, you must convert manaully to desired datatype.
	Use getData to fetch data directly into specified datatype

	\param nRow			- target row [0..getRowCount()-1]
	\param nCol			- target col [0..getColumnCount()-1]
	\return QVariant	- value as QVariant use .toInt() to convert to int, or .toString() or other QVariant funct
*/
/*
QVariant &DbRecordSet::getDataRef(int nRow, int nCol)
{
	//assert for valid indexes 
	Q_ASSERT(nRow >= 0 && nRow < m_lstData.size());
	Q_ASSERT(nCol >= 0 && nCol < m_lstColumns.size());
	Q_ASSERT(nCol >= 0 && nCol < m_lstData.at(nRow).DBRow.size());

	return m_lstData[nRow].DBRow[nCol];
}



QVariant &DbRecordSet::getDataRef(int nRow, QString strColumn)
{
	int	nCol = getColumnIdx(strColumn);
	Q_ASSERT(nCol!=-1);		//range check
	//assert for valid indexes 
	Q_ASSERT(nRow >= 0 && nRow < m_lstData.size());
	Q_ASSERT(nCol >= 0 && nCol < m_lstColumns.size());
	Q_ASSERT(nCol >= 0 && nCol < m_lstData.at(nRow).DBRow.size());

	return m_lstData[nRow].DBRow[nCol];
}



const QVariant &DbRecordSet::getDataRef(int nRow, int nCol) const
{
	//assert for valid indexes 
	Q_ASSERT(nRow >= 0 && nRow < m_lstData.size());
	Q_ASSERT(nCol >= 0 && nCol < m_lstColumns.size());
	Q_ASSERT(nCol >= 0 && nCol < m_lstData.at(nRow).DBRow.size());

	return m_lstData[nRow].DBRow[nCol];
}

const QVariant &DbRecordSet::getDataRef(int nRow, QString strColumn) const
{
	int	nCol = getColumnIdx(strColumn);
	Q_ASSERT(nCol!=-1);		//range check
	//assert for valid indexes 
	Q_ASSERT(nRow >= 0 && nRow < m_lstData.size());
	Q_ASSERT(nCol >= 0 && nCol < m_lstColumns.size());
	Q_ASSERT(nCol >= 0 && nCol < m_lstData.at(nRow).DBRow.size());

	return m_lstData[nRow].DBRow[nCol];
}

*/




void DbRecordSet::destroy()
{
	//destroy definition and data
	m_lstColumns.clear();
	m_hshColumnMap.clear();
	m_lstData.clear();
}

void DbRecordSet::clear()
{
	//clear data, keep definition
	m_lstData.clear();
}

/*!
	Destroy any previous def, define from given view.
	Example: 
	\code
	DbRecordSet appsession;
	appsession.defineFromView(DbSqlTableView::getView(DbSqlTableDefinition::GetFullViewID(CORE_APP_SRV_SESSION)));
	\endcode

	\param view			- view in DbView
	\return true if success

*/
bool DbRecordSet::defineFromView(const DbView &view)
{
	destroy();
	int nCols = view.size();
	for(int i=0; i<nCols; i++)
	{
		if(!addColumn(view[i].m_nType, view[i].m_strName))
			return false;
	}
	return true;
}

bool DbRecordSet::renameColumnsFromAlias(const DbView &view)
{
	int nCols = view.size(); 
	if (nCols!=getColumnCount())//if does not match
		return false;

	for(int i=0; i<nCols; i++)
	{
		if (!view[i].m_strNameAlias.isEmpty())
		{
			renameColumn(i,view[i].m_strNameAlias);
		}
	}
	return true;
}

bool DbRecordSet::copyDefinition(const DbRecordSet &set, bool bDestroyPrevious)
{
	if(bDestroyPrevious)
		destroy();

	int nCols = set.m_lstColumns.size();
	for(int i=0; i<nCols; i++)
	{
		//add new column if the column name does not exist already
		if(getColumnIdx(set.m_lstColumns[i].m_strName) < 0)
			if(!addColumn(set.m_lstColumns[i].m_nType, set.m_lstColumns[i].m_strName))
				return false;
	}

	return true;
}

void DbRecordSet::redefineContents(int nCol, bool bAdded)
{
	Q_ASSERT(nCol >= 0 && nCol < m_lstColumns.size());

	int nType = m_lstColumns[nCol].m_nType;
	QVariant data((QVariant::Type)nType);

	int nNumRows = m_lstData.size();
	for(int i=0; i<nNumRows; i++)
	{
		//each existing column data must be changed
		//to accomodate list definition changes
		if(bAdded)
			m_lstData[i].DBRow.insert(nCol, data);
		else
			m_lstData[i].DBRow.removeAt(nCol);
	}
}


//nSortAscending=0, means ASCENDING
void DbRecordSet::sort(int nCol, int nSortAscending)
{
	if (nCol==-1)
		return;

	SortDataList listSort;
	listSort<<SortData(nCol,nSortAscending);
	//QTime time;
	//time.start();
	sortAdvanced(m_lstData,listSort,0);			//very speedy when colcount is moderate
		

	//qDebug()<<"sortAdv:"<<time.elapsed();
	
}

void DbRecordSet::sort(QString strColumnName, int nSortAscending)
{
	int nColumnIndex = getColumnIdx(strColumnName);
	return sort(nColumnIndex, nSortAscending);
}

//sorts lists by given list. List contains number of column to sort, order in list determines significants
void DbRecordSet::sortMulti(SortDataList &lstCols)
{
	if (lstCols.size()==1) //if one, it will be oke to use speedy
	{
		sortAdvanced(m_lstData,lstCols,0);
		return;
	}
	
	//QTime time;
	//time.start();
	DBRowComparator cmp;

	cmp.m_lstSortCol = lstCols;
	cmp.m_lstColumns = &m_lstColumns;

	qStableSort(m_lstData.begin(), m_lstData.end(), cmp);
	//qDebug()<<"sortOld:"<<time.elapsed();
	
}




//------------------------------------------------------------
//					GET DATA HANDLERS BY &value
//------------------------------------------------------------



void DbRecordSet::getData(int nRow, int nCol, int &value)
{
	QVariant &data = getDataRef(nRow, nCol);
	Q_ASSERT(data.type() == QVariant::Int);	//is type valid?
	value = data.value<int>();
}

void DbRecordSet::getData(int nRow, int nCol, uint &value)
{
	QVariant &data = getDataRef(nRow, nCol);
	Q_ASSERT(data.type() == QVariant::UInt);	//is type valid?
	value = data.value<int>();
}
void DbRecordSet::getData(int nRow, int nCol, qulonglong &value)
{
	QVariant &data = getDataRef(nRow, nCol);
	Q_ASSERT(data.type() == QVariant::ULongLong);	//is type valid?
	value = data.value<qulonglong>();
}


void DbRecordSet::getData(int nRow, int nCol, QString &value)
{
	QVariant &data = getDataRef(nRow, nCol);
	Q_ASSERT(data.type() == QVariant::String);	//is type valid?
	value = data.value<QString>();
}

void DbRecordSet::getData(int nRow, int nCol, bool &value)
{
	QVariant &data = getDataRef(nRow, nCol);
	Q_ASSERT(data.type() == QVariant::Bool);	//is type valid?
	value = data.value<bool>();
}

void DbRecordSet::getData(int nRow, int nCol, QByteArray &value)
{
	QVariant &data = getDataRef(nRow, nCol);
	Q_ASSERT(data.type() == QVariant::ByteArray);	//is type valid?
	value = data.value<QByteArray>();
}

void DbRecordSet::getData(int nRow, int nCol, QDateTime &value)
{
	QVariant &data = getDataRef(nRow, nCol);
	Q_ASSERT(data.type() == QVariant::DateTime);	//is type valid?
	value = data.value<QDateTime>();
}

void DbRecordSet::getData(int nRow, int nCol, QDate &value)
{
	QVariant &data = getDataRef(nRow, nCol);
	Q_ASSERT(data.type() == QVariant::Date);	//is type valid?
	value = data.value<QDate>();
}
	
void DbRecordSet::getData(int nRow, int nCol, QTime &value)
{
	QVariant &data = getDataRef(nRow, nCol);
	Q_ASSERT(data.type() == QVariant::Time);	//is type valid?
	value = data.value<QTime>();
}

void DbRecordSet::getData(int nRow, int nCol, double &value)
{
	QVariant &data = getDataRef(nRow, nCol);
	Q_ASSERT(data.type() == QVariant::Double);	//is type valid?
	value = data.value<double>();
}

void DbRecordSet::getData(int nRow, int nCol, DbRecordSet &value)
{
	QVariant &data = getDataRef(nRow, nCol);
	Q_ASSERT(data.userType() == DbRecordSet::GetVariantType());	//is type valid?
	value = data.value<DbRecordSet>();
}


//------------------------------------------------------------
//			same as above with column name instead of index
//------------------------------------------------------------

void DbRecordSet::getData(int nRow, QString strColumn, int &value)
{
	int	nCol = getColumnIdx(strColumn);
	if(-1 != nCol)
		getData(nRow, nCol, value);
}

void DbRecordSet::getData(int nRow, QString strColumn, uint &value)
{
	int	nCol = getColumnIdx(strColumn);
	if(-1 != nCol)
		getData(nRow, nCol, value);
}

void DbRecordSet::getData(int nRow, QString strColumn, qulonglong &value)
{
	int	nCol = getColumnIdx(strColumn);
	if(-1 != nCol)
		getData(nRow, nCol, value);
}

void DbRecordSet::getData(int nRow, QString strColumn, QString &value)
{
	int	nCol = getColumnIdx(strColumn);
	if(-1 != nCol)
		getData(nRow, nCol, value);
}

void DbRecordSet::getData(int nRow, QString strColumn, bool &value)
{
	int	nCol = getColumnIdx(strColumn);
	if(-1 != nCol)
		getData(nRow, nCol, value);
}

void DbRecordSet::getData(int nRow, QString strColumn, QByteArray &value)
{
	int	nCol = getColumnIdx(strColumn);
	if(-1 != nCol)
		getData(nRow, nCol, value);
}

void DbRecordSet::getData(int nRow, QString strColumn, QDateTime &value)
{
	int	nCol = getColumnIdx(strColumn);
	if(-1 != nCol)
		getData(nRow, nCol, value);
}

void DbRecordSet::getData(int nRow, QString strColumn, QDate &value)
{
	int	nCol = getColumnIdx(strColumn);
	if(-1 != nCol)
		getData(nRow, nCol, value);
}

void DbRecordSet::getData(int nRow, QString strColumn, QTime &value)
{
	int	nCol = getColumnIdx(strColumn);
	if(-1 != nCol)
		getData(nRow, nCol, value);
}

void DbRecordSet::getData(int nRow, QString strColumn, double &value)
{
	int	nCol = getColumnIdx(strColumn);
	if(-1 != nCol)
		getData(nRow, nCol, value);
}

void DbRecordSet::getData(int nRow, QString strColumn, DbRecordSet &value)
{
	int	nCol = getColumnIdx(strColumn);
	if(-1 != nCol)
		getData(nRow, nCol, value);
}





//------------------------------------------------------------
//					SET DATA HANDLERS BY &value
//------------------------------------------------------------


/*!
	Sets data inside recordset. For each datatype there is special overloaded funct

	\param nRow			- target row [0..getRowCount()-1]
	\param nCol			- target col [0..getColumnCount()-1]
	\param value		- value to insert
*/
void DbRecordSet::setData(int nRow, int nCol, int value)
{
	QVariant &data = getDataRef(nRow, nCol);
	Q_ASSERT(data.type() == QVariant::Int);	//is type valid?
	qVariantSetValue(data, value);
}

void DbRecordSet::setData(int nRow, int nCol, uint value)
{
	QVariant &data = getDataRef(nRow, nCol);
	Q_ASSERT(data.type() == QVariant::UInt);	//is type valid?
	qVariantSetValue(data, value);
}

void DbRecordSet::setData(int nRow, int nCol, qulonglong value)
{
	QVariant &data = getDataRef(nRow, nCol);
	Q_ASSERT(data.type() == QVariant::ULongLong);	//is type valid?
	qVariantSetValue(data, value);
}
	
void DbRecordSet::setData(int nRow, int nCol, QString &value)
{
	QVariant &data = getDataRef(nRow, nCol);
	Q_ASSERT(data.type() == QVariant::String);	//is type valid?
	qVariantSetValue(data, value);
}

void DbRecordSet::setData(int nRow, int nCol, const char * value)
{
	QString strValue(value);
	QVariant &data = getDataRef(nRow, nCol);
	Q_ASSERT(data.type() == QVariant::String);	//is type valid?
	qVariantSetValue(data, strValue);
}

void DbRecordSet::setData(int nRow, int nCol, bool value)
{
	QVariant &data = getDataRef(nRow, nCol);
	Q_ASSERT(data.type() == QVariant::Bool);	//is type valid?
	qVariantSetValue(data, value);
}

void DbRecordSet::setData(int nRow, int nCol, QByteArray &value)
{
	QVariant &data = getDataRef(nRow, nCol);
	Q_ASSERT(data.type() == QVariant::ByteArray);	//is type valid?
	qVariantSetValue(data, value);
}

void DbRecordSet::setData(int nRow, int nCol, QDateTime value)
{
	QVariant &data = getDataRef(nRow, nCol);
	Q_ASSERT(data.type() == QVariant::DateTime);	//is type valid?
	qVariantSetValue(data, value);
}

void DbRecordSet::setData(int nRow, int nCol, QDate value)
{
	QVariant &data = getDataRef(nRow, nCol);
	Q_ASSERT(data.type() == QVariant::Date);	//is type valid?
	qVariantSetValue(data, value);
}

void DbRecordSet::setData(int nRow, int nCol, QTime value)
{
	QVariant &data = getDataRef(nRow, nCol);
	Q_ASSERT(data.type() == QVariant::Time);	//is type valid?
	qVariantSetValue(data, value);
}

void DbRecordSet::setData(int nRow, int nCol, double value)
{
	QVariant &data = getDataRef(nRow, nCol);
	Q_ASSERT(data.type() == QVariant::Double);	//is type valid?
	qVariantSetValue(data, value);
}

void DbRecordSet::setData(int nRow, int nCol, DbRecordSet &value)
{
	QVariant &data = getDataRef(nRow, nCol);
	Q_ASSERT(data.userType() == DbRecordSet::GetVariantType());	//is type valid?
	qVariantSetValue(data, value);
}
/*
void DbRecordSet::setData(int nRow, int nCol, QVariant &value)
{
	Q_ASSERT(nRow >= 0 && nRow < m_lstData.size());
	Q_ASSERT(nCol >= 0 && nCol < m_lstColumns.size());
	Q_ASSERT(value.type()==m_lstColumns[nCol].m_nType || (m_lstColumns[nCol].m_nType==GetVariantType() && value.type()==QVariant::UserType)); //if types doesnt match

	m_lstData[nRow].DBRow[nCol]=value;
}

void DbRecordSet::setData(int nRow, int nCol, const QVariant &value)
{
	//qDebug()<< "DbRecordSet::setData compare variant types [" << value.type() << "] == [" <<m_lstColumns[nCol].m_nType << "]?";
	Q_ASSERT(nRow >= 0 && nRow < m_lstData.size());
	Q_ASSERT(nCol >= 0 && nCol < m_lstColumns.size());
	Q_ASSERT(value.type()==m_lstColumns[nCol].m_nType || (m_lstColumns[nCol].m_nType==GetVariantType() && value.type()==QVariant::UserType)); //if types doesnt match

	m_lstData[nRow].DBRow[nCol]=value;
}
*/

//------------------------------------------------------------
//same as above with column name instead of index
//------------------------------------------------------------
void DbRecordSet::setData(int nRow, QString strColumn, int value)
{
	int	nCol = getColumnIdx(strColumn);
	setData(nRow, nCol, value); //if -1 ,ASSERT will fire, that's OK
}

void DbRecordSet::setData(int nRow, QString strColumn, uint value)
{
	int	nCol = getColumnIdx(strColumn);
	setData(nRow, nCol, value); //if -1 ,ASSERT will fire, that's OK
}

void DbRecordSet::setData(int nRow, QString strColumn, qulonglong value)
{
	int	nCol = getColumnIdx(strColumn);
	setData(nRow, nCol, value); //if -1 ,ASSERT will fire, that's OK
}

void DbRecordSet::setData(int nRow, QString strColumn, QString &value)
{
	int	nCol = getColumnIdx(strColumn);
	setData(nRow, nCol, value);//if -1 ,ASSERT will fire, that's OK
}

void DbRecordSet::setData(int nRow, QString strColumn, const char * value)
{
	int	nCol = getColumnIdx(strColumn);
	setData(nRow, nCol, value);//if -1 ,ASSERT will fire, that's OK
}

void DbRecordSet::setData(int nRow, QString strColumn, bool value)
{
	int	nCol = getColumnIdx(strColumn);
	setData(nRow, nCol, value);//if -1 ,ASSERT will fire, that's OK
}

void DbRecordSet::setData(int nRow, QString strColumn, QByteArray &value)
{
	int	nCol = getColumnIdx(strColumn);
	setData(nRow, nCol, value);//if -1 ,ASSERT will fire, that's OK
}

void DbRecordSet::setData(int nRow, QString strColumn, QDateTime value)
{
	int	nCol = getColumnIdx(strColumn);
	setData(nRow, nCol, value);//if -1 ,ASSERT will fire, that's OK
}

void DbRecordSet::setData(int nRow, QString strColumn, QDate value)
{
	int	nCol = getColumnIdx(strColumn);
	setData(nRow, nCol, value);//if -1 ,ASSERT will fire, that's OK
}

void DbRecordSet::setData(int nRow, QString strColumn, QTime value)
{
	int	nCol = getColumnIdx(strColumn);
	setData(nRow, nCol, value);//if -1 ,ASSERT will fire, that's OK
}

void DbRecordSet::setData(int nRow, QString strColumn, double value)
{
	int	nCol = getColumnIdx(strColumn);
	setData(nRow, nCol, value);//if -1 ,ASSERT will fire, that's OK
}

void DbRecordSet::setData(int nRow, QString strColumn, DbRecordSet &value)
{
	int	nCol = getColumnIdx(strColumn);
	setData(nRow, nCol, value);//if -1 ,ASSERT will fire, that's OK
}
/*
void DbRecordSet::setData(int nRow, QString strColumn, QVariant &value)
{
	int	nCol = getColumnIdx(strColumn);
	setData(nRow, nCol, value);//if -1 ,ASSERT will fire, that's OK
}

void DbRecordSet::setData(int nRow, QString strColumn, const QVariant &value)
{
	int	nCol = getColumnIdx(strColumn);
	setData(nRow, nCol, value);//if -1 ,ASSERT will fire, that's OK
}
*/



//----------------------------------------------------------------
//					SELECTION 
//----------------------------------------------------------------


/*!
	Clear selection
*/

void DbRecordSet::clearSelection()
{
	int nDataSize = m_lstData.size();
	for(int i=0; i<nDataSize ;++i)
		m_lstData[i].bSelected=false;
}

/*!
	Is row selected
	\return				- true if row selected

*/
bool DbRecordSet::isRowSelected(int nRow) const
{
	return m_lstData[nRow].bSelected;
}

/*!
	Selectes row or deselects 
	\param nRow			- target row [0..getRowCount()-1]
	\param bSelect		- if true (default), select, else deselect

*/
void DbRecordSet::selectRow(int nRow, bool bSelect)
{
	if(bSelect)
		m_lstData[nRow].bSelected=true;
	else
		m_lstData[nRow].bSelected=false;
}

/*!
	Selectes from - to rows or deselects 
	\param nRowFrom		- row from [0..getRowCount()-1]
	\param nRowTo		- row to [nRowFrom..getRowCount()-1]
	\param bSelect		- if true (default), select, else deselect

*/
void DbRecordSet::selectRowFromTo(int nRowFrom, int nRowTo, bool bSelect/*=true*/)
{
	if(nRowFrom<0 || nRowTo>=getRowCount())
		return;

	for (int i=nRowFrom; i<=nRowTo; i++)
	{
		selectRow(i, bSelect);
	}
}

/*!
	Select all
*/
void DbRecordSet::selectAll()
{
	int nSize=getRowCount();
	for(int i=0;i<nSize;++i)
		m_lstData[i].bSelected=true;
}



/*!
	Returns number of selected rows
	\return				- no of selected rows
*/
int DbRecordSet::getSelectedCount()
{
	int nTotalDeleted = 0;
	int nCount = m_lstData.size();
	for(int i=0; i<nCount; i++){
		if(m_lstData[i].bSelected)
		{
			nTotalDeleted ++;
		}
	}
	return nTotalDeleted;
}

/*!
	Deletes selected rows
	\return				- no of deleted rows
*/
int DbRecordSet::deleteSelectedRows()
{
	int nTotalDeleted = 0;
	int nCount = m_lstData.size()-1;
	for(int i=nCount; i>=0; i--){
		if(m_lstData[i].bSelected)
		{
			deleteRow(i);
			nTotalDeleted ++;
		}
	}

	return nTotalDeleted;
}


/*!
	Deletes selected rows
	\return				- no of deleted rows
*/
int DbRecordSet::deleteUnSelectedRows()
{
	int nTotalDeleted = 0;
	int nCount = m_lstData.size()-1;
	for(int i=nCount; i>=0; i--){
		if(!m_lstData[i].bSelected)
		{
			deleteRow(i);
			nTotalDeleted ++;
		}
	}

	return nTotalDeleted;
}

/*!
	Gets first selected row begining from nFrom

	\param nFrom	- search start from
	\return			- selected row No,-1 if no selection
*/
int DbRecordSet::getSelectedRow(int nFrom)
{
	int nCount = m_lstData.size();
	for(int i=nFrom;i<nCount;++i)
		if(m_lstData[i].bSelected) return i;

	return -1;
}

/*!
	Gets selected rows beginning from nFrom

	\param nFrom	- search start from
	\return			- selected rows list (QList<int>).
*/
QList<int> DbRecordSet::getSelectedRows(int nFrom /* = 0*/)
{
	QList<int> selectedRows;
	int nCount = m_lstData.size();
	for(int i=nFrom;i<nCount;++i)
		if(m_lstData[i].bSelected) selectedRows << i;

	return selectedRows;
}

/*!
	Gets selected rows recordset beginning from nFrom

	\param nFrom	- search start from
	\return			- selected rows list (DbrecordSet).
*/
DbRecordSet DbRecordSet::getSelectedRecordSet(int nFrom/*=0*/)
{
	DbRecordSet temp;
	temp.copyDefinition(*this);
	int nColMax=getColumnCount();
	int nCurrentRow = 0;

	int nCount = m_lstData.size();
	for(int i=nFrom;i<nCount;++i)
	{
		if(m_lstData[i].bSelected) 
		{
			temp.addRow();
			for(int j=0;j<nColMax;++j)
				temp.setData(nCurrentRow,j,m_lstData.at(i).DBRow.at(j));

			nCurrentRow++;
		}
	}
	
	return temp;
}

//----------------------------------------------------------------
//					MERGE 
//----------------------------------------------------------------



/*!
	Merges two lists: adds rows from source list to target list, maps by column name, use selection from source list 
	If target list does not have columns from source then columns are empty Variants (NULL)

	\param list				- source list
	\param bOnlySelected	- if true copy only selected rows
	\param bMapByPosition	- if true map columns by position. If column numbers are not equal it discads the diference.
*/
void DbRecordSet::merge(const DbRecordSet &list, bool bOnlySelected, bool bMapByPosition /*= false*/)
{
	//create list to list column mapping to speed-up the process
	MapCols mapCols;
	if (!bMapByPosition)
		mapColumns(mapCols,list);

	int nSourceColCount = list.getColumnCount();
	int nDestColCount	= getColumnCount();
	
	//Merge two lists by name or by position.
	int nMax = list.getRowCount();
	for(int nRow=0; nRow<nMax; nRow++)
	{
		//test selection
		if(bOnlySelected)
			if(!list.m_lstData[nRow].bSelected) //isRowSelected(nRow))
				continue;
		
		//addrow
		addRow();
		int nIdx = getRowCount()-1;	
		
		if (!bMapByPosition)
		{
			//copy column values by map:
			MapColsIterator i(mapCols);
			while (i.hasNext()) 
			{
				i.next();
				setData(nIdx,i.key(),list.getDataRef(nRow,i.value())); //VARIANT
			}
		}
		else
		{
			for (int i = 0; i < nSourceColCount; ++i)
			{
				if(i < nDestColCount)
					setData(nIdx, i, list.getDataRef(nRow,i));
			}
		}
	}
}

//list must have same def as source
void DbRecordSet::prependList(const DbRecordSet &list)
{
	Q_ASSERT(getColumnCount()==list.getColumnCount());

	int nSize=list.getRowCount()-1;
	for(int i=nSize;i>=0;i--)
	{
		m_lstData.prepend(list.m_lstData.at(i));
	}
}



//----------------------------------------------------------------
//					ROW OPS
//----------------------------------------------------------------


/*!
	Returns recordset with one row from row specified

	\param nRow			- target row [0..getRowCount()-1]
	\return				- row
*/
DbRecordSet DbRecordSet::getRow(int nRow) const
{
	DbRecordSet temp;
	temp.copyDefinition(*this);
	temp.addRow();
	int nColMax=getColumnCount();
	for(int i=0;i<nColMax;++i)
		temp.setData(0,i,m_lstData.at(nRow).DBRow.at(i));
	return temp;
}

/*!
	Assigns 1st row from recordset to specified row in this recordset

	\param nRow			- target row [0..getRowCount()-1]
	\param Row			- row to be assigned
	\param bByName		- false: number of columns are same, else, by name
	
	\return				- row
*/
void DbRecordSet::assignRow(int nRow,DbRecordSet& Row, bool bByName,MapCols *mapColsSource)
{
	if(!bByName)
	{
		//assume that have same No of cols, error if not:
		Q_ASSERT(getColumnCount()==Row.getColumnCount());
		int nColMax=getColumnCount();
		for(int i=0;i<nColMax;++i)
			setData(nRow,i,Row.getDataRef(0,i));
	}
	else
	{
		MapCols mapCols;
		if (!mapColsSource)
		{
			mapColumns(mapCols,Row);
			mapColsSource=&mapCols;
		}
		
		//loop through mapped cols
		MapColsIterator i(*mapColsSource);
		while (i.hasNext()) 
		{
			i.next();
			setData(nRow,i.key(),Row.getDataRef(0,i.value()));
		}
	}
}

/*!
	Assigns all rows from recordset to the selected rows in this recordset
	NOTE: if number differs, it will only assign first N rows that are selected

	\param Rows			- rows to be assigned
	\param bByName		- false: number of columns are same, else, by name
	
	\return				- row
*/	
void DbRecordSet::assignRow(DbRecordSet& Rows, bool bByName, bool bAssignToSelected,MapCols *mapColsSource)
{

	int nRowCnt=Rows.getRowCount();

	//map cols if needed, check cols if not by name
	MapCols mapCols;
	if(bByName)
	{
		if (!mapColsSource)
		{
			mapColumns(mapCols,Rows);
			mapColsSource=&mapCols;
		}
	}
	else{
		Q_ASSERT(getColumnCount()==Rows.getColumnCount());
	}

	//loop on selected rows:
	int nSourceRow=0;
	int nCount = m_lstData.size();
	for(int nRow=0; nRow<nCount;nRow++)
	{
		if(!m_lstData[nRow].bSelected && bAssignToSelected)continue;	//skip if not selected
		if(nSourceRow>=nRowCnt)break;//test source rows, break if end
		
		if(!bByName)
		{
			int nColMax=getColumnCount();
			for(int k=0;k<nColMax;++k)
				setData(nRow,k,Rows.getDataRef(nSourceRow,k));
		}
		else
		{
			Q_ASSERT(mapColsSource != NULL);
			if(mapColsSource){
				//loop through mapped cols
				MapColsIterator i(*mapColsSource);
				while (i.hasNext()) 
				{
					i.next();
					setData(nRow,i.key(),Rows.getDataRef(nSourceRow,i.value()));
				}
			}
		}

		nSourceRow++;	
	}

}



//----------------------------------------------------------------
//					HELPER
//----------------------------------------------------------------
// maps columns from this record set to given list:
//key=index of column here, value=column index in list (by name & type) if no match, no value
void DbRecordSet::mapColumns(MapCols& mapCols,const DbRecordSet& list)
{
	//create list to list column mapping to speed-up the process
	for(int i=0; i<getColumnCount(); i++)
	{
		//find matching column by name
		int nColDst = list.getColumnIdx(m_lstColumns[i].m_strName);
		if(nColDst >= 0)
			if(list.m_lstColumns[nColDst].m_nType != m_lstColumns[i].m_nType)
				nColDst = -1;	// typed must also match, or no matching!

		//store the match informtion into the map
		if(nColDst!=-1)mapCols[i] = nColDst;
	}
}




//----------------------------------------------------------------
//					HELPER
//----------------------------------------------------------------
//
/*!
	Sorts list ascending and removes duplicate values in given column

	\param nCol			- column
*/
void  DbRecordSet::removeDuplicates(int nCol)
{

	//QTime time;
	//time.start();
	sort(nCol); //sort asc
	//qDebug()<<"sort:"<<time.elapsed();

	int i=getRowCount()-1;

	while(i>0)
	{
		QVariant value = getDataRef(i,nCol).toString(); //get last value
		while(i>0)
		{
			i--;
			if(value==getDataRef(i,nCol).toString())
				deleteRow(i);//del if same
			else
				break;  //if not same store new value...
		}
	}
	
}

//count number of occurrences of value T inside col
int DbRecordSet::countColValue(int nCol, QVariant &value, bool bOnlySelected)
{
	int nCount=0;
	int nSize=getRowCount();
	for(int i=0; i<nSize; i++)
		if(!bOnlySelected || (bOnlySelected && isRowSelected(i)))
			if(value==m_lstData[i].DBRow[nCol])
				nCount++;

	return nCount;
}



/*!
	Same as find, uses QVariant, can not be in template

	\param nCol			- target col [0..getColumnCount()-1]
	\param value		- value for search
	\param bFindFirst	- true = returns index of found element or -1 if nothing was found, false=returns number of selected items
	\param bSearchOnSelected	- searches on selected
	\param bDeselectNonMatches	- deselects non matches
	\param bMatchEqual	- if false then searches all !
	\return number of selected items of if search one, number of row, or -1 if not found

*/
int DbRecordSet::find(int nCol, QVariant value, bool bFindFirst, bool bSearchOnSelected, bool bDeselectNonMatches, bool bMatchEqual)
{
	if(bFindFirst)
	{
		int nSize=getRowCount();
		for(int i=0; i<nSize; i++)
		{
			if(bMatchEqual)	//match if values are equal
			{
				if(value == getDataRef(i, nCol))
					return i;
			}
			else //match if values are NOT equal
			{
				if(value != getDataRef(i, nCol))
					return i;
			}
		}
		return -1;
	}
	else
	{
		int nCount = 0;
		if(bSearchOnSelected)
		{
			//search only the selected items
			//- only deselect non-matches if requested
			int nCount = m_lstData.size();
			for(int nRow=0; nRow<nCount;nRow++)
			{
				if(!m_lstData[nRow].bSelected) continue;	//skip if not selected
				if(bDeselectNonMatches)
					if( (bMatchEqual && value  == getDataRef(nRow, nCol)) ||
						!(bMatchEqual || value == getDataRef(nRow, nCol)))
						selectRow(nRow, false);
			}
		}
		else
		{
			//search entire list
			int nSize=getRowCount();
			for(int i=0; i<nSize; i++)
			{
				// !(x1 || x2) || x1 && x2 = result!!!
				if( (bMatchEqual && value  == getDataRef(i, nCol)) ||
					!(bMatchEqual || value == getDataRef(i, nCol)))
				{
					selectRow(i, true);			// match found, select it
					nCount ++;
				}
				else if (bDeselectNonMatches)
					selectRow(i, false);		// deselect non-match if required
			}
		}
		return nCount;
	}
}


//Max: finds first from zero if more then 1 row
//returns row with max, and value
int DbRecordSet::GetMaximum(int nCol, QVariant &max_value)
{
	max_value=QVariant();
	if(getRowCount()==0)
		return -1;

	//copy
	DbRecordSet tmpCopy(*this);
	tmpCopy.sort(nCol,0);
	max_value=tmpCopy.getDataRef(tmpCopy.getRowCount()-1,nCol); //get max

	//find value inisde our set:
	int nRow=find(nCol,max_value,true);
	Q_ASSERT(nRow!=-1); //must be valid

	return nRow;
}

//Min: finds first from zero if more then 1 row
//returns row with min, and value
int DbRecordSet::GetMinimum(int nCol, QVariant &min_value)
{
	min_value=QVariant();
	if(getRowCount()==0)
		return -1;

	//copy
	DbRecordSet tmpCopy(*this);
	tmpCopy.sort(nCol,1);
	min_value=tmpCopy.getDataRef(tmpCopy.getRowCount()-1,nCol); //get max

	//find value inisde our set:
	int nRow=find(nCol,min_value);
	Q_ASSERT(nRow!=-1); //must be valid

	return nRow;
}

int DbRecordSet::getStoredByteSize()
{
	int nCount = 0;
	int nRows = getRowCount();
	int nCols = getColumnCount();

	for(int i=0; i<nRows; i++)
		for(int j=0; j<nCols; j++)
			if(!m_lstData[i].DBRow[j].isNull()){
				switch(m_lstData[i].DBRow[j].type())
				{
					case QVariant::String:		nCount += m_lstData[i].DBRow[j].toString().length() * 2; break;
					case QVariant::ByteArray:	nCount += m_lstData[i].DBRow[j].toByteArray().length(); break;
					case QVariant::Double:		nCount += sizeof(double); break;
					case QVariant::Int:			nCount += sizeof(int); break;
					case QVariant::Date:		nCount += sizeof(QDate); break;
					case QVariant::DateTime:	nCount += sizeof(QDateTime); break;
					default:
						{
							//qDebug()<<m_lstData[i].DBRow[j].type();
							//qDebug()<<m_lstData[i].DBRow[j].userType();
							//qDebug()<<DbRecordSet::GetVariantType();
							if (m_lstData[i].DBRow[j].userType()==DbRecordSet::GetVariantType()) //recursive: add all sublists into total bytes
							{
								nCount += m_lstData[i].DBRow[j].value<DbRecordSet>().getStoredByteSize();
							}
						}
				}
			}
		
	return nCount;
}

void DbRecordSet::joinList(DbRecordSet &list, int nColIdx)
{
	//add contents of parameter list to current list, match by nColIdx column
	merge(list);
	removeDuplicates(nColIdx);
}

void DbRecordSet::removeList(DbRecordSet &list, int nColIdx)
{
	//remove contents of parameter list from current list, match by nColIdx column
	int nCnt = list.getRowCount();
	for(int i=0; i<nCnt; i++){
		QVariant qID = list.getDataRef(i, nColIdx);
		int nIdx = find(nColIdx, qID, true);
		if(nIdx >= 0){
			//row from parameter list found in our list, delete it
			deleteRow(nIdx);
		}
	}
}

void DbRecordSet::intersectList(DbRecordSet &list, int nColIdx)
{
	//remove contents from current list not found in parameter list, match by nColIdx column
	int nCnt = getRowCount();
	for(int i=0; i<nCnt; i++){
		QVariant qID = getDataRef(i, nColIdx);
		int nIdx = list.find(nColIdx, qID, true);
		if(nIdx < 0){
			//our row not found in the parameter list, delete it
			deleteRow(i);
			nCnt --; i --;
		}
	}
}

void DbRecordSet::Dump()	// display debug info
{
	#ifdef _DEBUG
		int nRows = getRowCount();
		int nCols = getColumnCount();
		qDebug()<< "DbRecordSet::Dump - size (rows, cols): " << nRows << "," << nCols;

		qDebug()<< "DbRecordSet::Dump - recordset columns:";
		for(int i=0; i<nCols; i++)
			qDebug()<< m_lstColumns[i].m_strName << "(type:" << m_lstColumns[i].m_nType << ")";

		qDebug()<< "DbRecordSet::Dump - recordset data:";
		for(int i=0; i<nRows; i++)
			for(int j=0; j<nCols; j++)
				if(m_lstData[i].DBRow[j].isNull())
					qDebug() << "Data(" << i << "," << j << "," << m_lstColumns[j].m_strName << "): NULL";
				else
					qDebug() << "Data(" << i << "," << j << "," << m_lstColumns[j].m_strName << "): " << m_lstData[i].DBRow[j];

	#endif
}

//uses m_lstColumns for columns definitions
//uses QMap ability to sort keys, qSort & QStableSort are slow on large data.
//copying large amount of data to map and back is very fast, as they all use shared memory
//In debug mode: 547 msecs for 32000 rows
void DbRecordSet::sortAdvanced(QList<DBRowAdvanced> &lstData,SortDataList &lstColsForSort,int nIndex)
{
	if (nIndex>=lstColsForSort.size())
		return;
	bool bSortMulti=false;
	int nCol=lstColsForSort.at(nIndex).m_nColumn;
	bool bSortAsc=lstColsForSort.at(nIndex).m_nSortOrder;
	//if ((nIndex+1)<lstColsForSort.size())
	//	bSortMulti=true;

	if (nCol>m_lstColumns.size()-1)return;	//->AR rights set
	//qDebug()<<m_lstColumns.size();
	
	switch(m_lstColumns[nCol].m_nType)
	{
		case QVariant::Int:
			{
				//Dump();

				//fill map, map will sort
				QMap<int, DBRowAdvanced> map;
				int nSize=lstData.size();
				for (int i=0;i<nSize;++i)
					map.insertMulti(lstData.at(i).DBRow.at(nCol).toInt(),lstData.at(i));

				//get from map sorted values
				int k=-1;
				//qDebug()<<map.count();
				//int znj=0;
				if (lstColsForSort.at(nIndex).m_nSortOrder==0)
				{
					QMap<int, DBRowAdvanced>::const_iterator i = map.constBegin();
					while (i != map.constEnd()) 
					{
						++k;
						lstData[k]=i.value();
						++i;
					}
				}
				else
				{
					QMap<int, DBRowAdvanced>::const_iterator i = map.constEnd();
					if (i!=map.constBegin()) //if end=begin, exit
					{
						do
						{
							--i;
							++k;
							lstData[k]=i.value();
						}
						while (i != map.constBegin()); 
					}
				}
			}
			break;

		case QVariant::String:
			{
				//Dump();

				//fill map, map will sort
				QMap<StringComparator, DBRowAdvanced> map;
				int nSize=lstData.size();
				for (int i=0;i<nSize;++i)
					map.insertMulti(StringComparator(lstData.at(i).DBRow.at(nCol).toString()),lstData.at(i));

				//get from map sorted values
				int k=-1;
				//qDebug()<<map.count();
				if (lstColsForSort.at(nIndex).m_nSortOrder==0)
				{
					QMap<StringComparator, DBRowAdvanced>::const_iterator i = map.constBegin();
					while (i != map.constEnd()) 
					{
						++k;
						lstData[k]=i.value();
						++i;
					}
				}
				else
				{
					QMap<StringComparator, DBRowAdvanced>::const_iterator i = map.constEnd();
					if (i!=map.constBegin()) //if end=begin, exit
					{
						do
						{
							--i;
							++k;
							lstData[k]=i.value();
						}
						while (i != map.constBegin()); 
					}
				}
			}
			break;

		case QVariant::Double:
			{
				//fill map, map will sort
				QMap<double, DBRowAdvanced> map;
				int nSize=lstData.size();
				for (int i=0;i<nSize;++i)
					map.insertMulti(lstData.at(i).DBRow.at(nCol).toDouble(),lstData.at(i));

				//get from map sorted values
				int k=-1;
				if (lstColsForSort.at(nIndex).m_nSortOrder==0)
				{
					QMap<double, DBRowAdvanced>::const_iterator i = map.constBegin();
					while (i != map.constEnd()) 
					{
						++k;
						lstData[k]=i.value();
						++i;
					}
				}
				else
				{
					QMap<double, DBRowAdvanced>::const_iterator i = map.constEnd();
					if (i!=map.constBegin()) //if end=begin, exit
					{
						do
						{
							--i;
							++k;
							lstData[k]=i.value();
						}
						while (i != map.constBegin()); 
					}
				}
			}
			break;
		case QVariant::Bool:
			{
				//fill map, map will sort
				QMap<bool, DBRowAdvanced> map;
				int nSize=lstData.size();
				for (int i=0;i<nSize;++i)
					map.insertMulti(lstData.at(i).DBRow.at(nCol).toBool(),lstData.at(i));

				//get from map sorted values
				int k=-1;
				if (lstColsForSort.at(nIndex).m_nSortOrder==0)
				{
					QMap<bool, DBRowAdvanced>::const_iterator i = map.constBegin();
					while (i != map.constEnd()) 
					{
						++k;
						lstData[k]=i.value();
						++i;
					}
				}
				else
				{
					QMap<bool, DBRowAdvanced>::const_iterator i = map.constEnd();
					if (i!=map.constBegin()) //if end=begin, exit
					{
						do
						{
							--i;
							++k;
							lstData[k]=i.value();
						}
						while (i != map.constBegin()); 
					}
				}
			}
			break;
		case QVariant::DateTime:
			{
				//fill map, map will sort
				QMap<QDateTime, DBRowAdvanced> map;
				int nSize=lstData.size();
				for (int i=0;i<nSize;++i)
					map.insertMulti(lstData.at(i).DBRow.at(nCol).toDateTime(),lstData.at(i));

				//get from map sorted values
				int k=-1;
				if (lstColsForSort.at(nIndex).m_nSortOrder==0)
				{
					QMap<QDateTime, DBRowAdvanced>::const_iterator i = map.constBegin();
					while (i != map.constEnd()) 
					{
						++k;
						lstData[k]=i.value();
						++i;
					}
				}
				else
				{
					QMap<QDateTime, DBRowAdvanced>::const_iterator i = map.constEnd();
					if (i!=map.constBegin()) //if end=begin, exit
					{
						do
						{
							--i;
							++k;
							lstData[k]=i.value();
						}
						while (i != map.constBegin()); 
					}
				}
			}
			break;
		case QVariant::Date:
			{
				//fill map, map will sort
				QMap<QDate, DBRowAdvanced> map;
				int nSize=lstData.size();
				for (int i=0;i<nSize;++i)
					map.insertMulti(lstData.at(i).DBRow.at(nCol).toDate(),lstData.at(i));

				//get from map sorted values
				int k=-1;
				if (lstColsForSort.at(nIndex).m_nSortOrder==0)
				{
					QMap<QDate, DBRowAdvanced>::const_iterator i = map.constBegin();
					while (i != map.constEnd()) 
					{
						++k;
						lstData[k]=i.value();
						++i;
					}
				}
				else
				{
					QMap<QDate, DBRowAdvanced>::const_iterator i = map.constEnd();
					if (i!=map.constBegin()) //if end=begin, exit
					{
						do
						{
							--i;
							++k;
							lstData[k]=i.value();
						}
						while (i != map.constBegin()); 
					}
				}
			}
			break;
		case QVariant::Time:
			{
				//fill map, map will sort
				QMap<QTime, DBRowAdvanced> map;
				int nSize=lstData.size();
				for (int i=0;i<nSize;++i)
					map.insertMulti(lstData.at(i).DBRow.at(nCol).toTime(),lstData.at(i));

				//get from map sorted values
				int k=-1;
				if (lstColsForSort.at(nIndex).m_nSortOrder==0)
				{
					QMap<QTime, DBRowAdvanced>::const_iterator i = map.constBegin();
					while (i != map.constEnd()) 
					{
						++k;
						lstData[k]=i.value();
						++i;
					}
				}
				else
				{
					QMap<QTime, DBRowAdvanced>::const_iterator i = map.constEnd();
					if (i!=map.constBegin()) //if end=begin, exit
					{
						do
						{
							--i;
							++k;
							lstData[k]=i.value();
						}
						while (i != map.constBegin()); 
					}
				}
			}
			break;
		case QVariant::ByteArray:
			{
				//fill map, map will sort
				QMap<QByteArray, DBRowAdvanced> map;
				int nSize=lstData.size();
				for (int i=0;i<nSize;++i)
					map.insertMulti(lstData.at(i).DBRow.at(nCol).toByteArray(),lstData.at(i));

				//get from map sorted values
				int k=-1;
				if (lstColsForSort.at(nIndex).m_nSortOrder==0)
				{
					QMap<QByteArray, DBRowAdvanced>::const_iterator i = map.constBegin();
					while (i != map.constEnd()) 
					{
						++k;
						lstData[k]=i.value();
						++i;
					}
				}
				else
				{
					QMap<QByteArray, DBRowAdvanced>::const_iterator i = map.constEnd();
					if (i!=map.constBegin()) //if end=begin, exit
					{
						do
						{
							--i;
							++k;
							lstData[k]=i.value();
						}
						while (i != map.constBegin()); 
					}
				}
			}
			break;
		//default:
		//	Q_ASSERT(false);	//comparison not implemented for this type

	}

}


QVariant & DbRecordSet::getDataRef( int nRow, int nCol )
{
	ApplicationLogger::logAssert(bool(nRow<0 || nRow>=m_lstData.size() || nCol<0 || nCol>=m_lstColumns.size()),"DbRecordSet::getDataRef");
	return m_lstData[nRow].DBRow[nCol];
}

const QVariant & DbRecordSet::getDataRef( int nRow, int nCol ) const
{
	ApplicationLogger::logAssert(bool(nRow<0 || nRow>=m_lstData.size() || nCol<0 || nCol>=m_lstColumns.size()),"DbRecordSet::getDataRef");
	return m_lstData[nRow].DBRow[nCol];
}

QVariant & DbRecordSet::getDataRef(int nRow, QString strColumn)
{
	int nColIdx = getColumnIdx(strColumn);
	Q_ASSERT_X(nColIdx >= 0, "getdata", QString("Unknown column %1").arg(strColumn).toLocal8Bit().constData());
	ApplicationLogger::logAssert(bool(nRow<0 || nRow>=m_lstData.size() || nColIdx<0 || nColIdx>=m_lstColumns.size()),"DbRecordSet::getDataRef");

	return m_lstData[nRow].DBRow[nColIdx];
};

const QVariant & DbRecordSet::getDataRef(int nRow, QString strColumn) const 
{
	int nColIdx = getColumnIdx(strColumn);
	Q_ASSERT_X(nColIdx >= 0, "getdata", QString("Unknown column %1").arg(strColumn).toLocal8Bit().constData());
	ApplicationLogger::logAssert(bool(nRow<0 || nRow>=m_lstData.size() || nColIdx<0 || nColIdx>=m_lstColumns.size()),"DbRecordSet::getDataRef");

	return m_lstData[nRow].DBRow[nColIdx];
};


void DbRecordSet::setData(int nRow, int nCol, QVariant &value){
	Q_ASSERT(value.type() == m_lstData[nRow].DBRow[nCol].type());
	ApplicationLogger::logAssert(bool(nRow<0 || nRow>=m_lstData.size() || nCol<0 || nCol>=m_lstColumns.size()),"DbRecordSet::setData");
	m_lstData[nRow].DBRow[nCol]=value;
};	//BY REF

void DbRecordSet::setData(int nRow, int nCol, const QVariant &value){
	Q_ASSERT(value.type() == m_lstData[nRow].DBRow[nCol].type());
	ApplicationLogger::logAssert(bool(nRow<0 || nRow>=m_lstData.size() || nCol<0 || nCol>=m_lstColumns.size()),"DbRecordSet::setData");
	m_lstData[nRow].DBRow[nCol]=value;
};	//BY REF

void DbRecordSet::setData(int nRow, QString strColumn, QVariant &value){
	int nColIdx = getColumnIdx(strColumn);
	Q_ASSERT_X(nColIdx >= 0, "setdata", QString("Unknown column %1").arg(strColumn).toLocal8Bit().constData());
	ApplicationLogger::logAssert(bool(nRow<0 || nRow>=m_lstData.size() || nColIdx<0 || nColIdx>=m_lstColumns.size()),"DbRecordSet::setData");
	Q_ASSERT(value.type() == m_lstData[nRow].DBRow[nColIdx].type());
	m_lstData[nRow].DBRow[nColIdx]=value;

};	//BY REF

void DbRecordSet::setData(int nRow, QString strColumn, const QVariant &value){
	int nColIdx = getColumnIdx(strColumn);
	Q_ASSERT_X(nColIdx >= 0, "setdata", QString("Unknown column %1").arg(strColumn).toLocal8Bit().constData());
	ApplicationLogger::logAssert(bool(nRow<0 || nRow>=m_lstData.size() || nColIdx<0 || nColIdx>=m_lstColumns.size()),"DbRecordSet::setData");
	Q_ASSERT(value.type() == m_lstData[nRow].DBRow[nColIdx].type());
	m_lstData[nRow].DBRow[nColIdx]=value;
};	//BY REF



StringComparator::StringComparator(QString str)
{
	m_strValue=str;
}
StringComparator::StringComparator(const StringComparator &that)
{
	m_strValue=that.m_strValue;
}
StringComparator &StringComparator::operator =(const StringComparator &that)
{
	if(this != &that)
	{
		m_strValue=that.m_strValue;
	}
	return *this;
}

bool StringComparator::operator ==(const StringComparator &that) const
{

	return m_strValue==that.m_strValue;
}
bool StringComparator::operator <(const StringComparator &that) const
{
	return QString::localeAwareCompare(m_strValue,that.m_strValue)<0;
}
