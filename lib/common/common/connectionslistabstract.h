#ifndef CONNECTIONSLISTABSTRACT_H
#define CONNECTIONSLISTABSTRACT_H

#include <QString>

class ConnectionsListAbstract
{
public:

	virtual void Clear()=0;
	virtual bool Load(QString strFile, QString strPassword)=0;
	virtual bool Save(QString strFile, QString strPassword)=0;

	//should be called after Load() call, before Save()
	virtual QString GetPasswordHash()=0;
	virtual bool MatchUserPassword(QString strPassword)=0;
	virtual bool IsUserPasswordSet()=0;
	virtual bool SetUserPasswordHash(QString strPassHash)=0;
	virtual void ClearUserPassword()=0;
	//virtual bool GetDbSettings(QString strSettingsName,DbConnectionSettings &info)=0;


};


#endif // CONNECTIONSLISTABSTRACT_H
