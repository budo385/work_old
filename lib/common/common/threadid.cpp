#include "threadid.h"

/*!
	Gets ID from current thread, returns -1 if thread doesnt support getId function or not setted
	\return unique thread id
*/

int ThreadIdentificator::GetCurrentThreadID()
{
	Qt::HANDLE pHandler = QThread::currentThreadId();
	int nThreadID =(long)pHandler;	//FIX: must use long or the compilation on OSX fails (64bit build)
	Q_ASSERT_X(nThreadID!=0,"Illegal Server Thread","Trying to get user context data for non initalized thread, not subclassed from Common.ServerThread!!");
	return nThreadID;

}

void Test1();

void Test1()
{
	CComplex a(4,7);
	CComplex b(a);
	CComplex c;
	c=b;
}

CComplexArray::CComplexArray( int N )
{
	m_size=N;
	m_pArray = new CComplex[N];

}

CComplexArray::~CComplexArray()
{
	delete[m_size] m_pArray;
}

CComplex & CComplexArray::operator[]( int index )
{
	return *(m_pArray+index);
}

/*
void T0Vector( const CNiz & in,vector<int> &out )
{
	vector.reserve(in.m_kap); //reserve m_kap places in out array (not necessary but it can speed up memory allocation) 

	//copy one by one, value from m_niz to out array
	for (int i=0;i<in.m_kap;i++)
	{
		int nValueFromCNiz=*(in.m_niz+i); //get int value from m_niz
		out.pushback(nValueFromCNiz); //add at end of out array
	}

}*/