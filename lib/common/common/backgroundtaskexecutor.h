#ifndef BACKGROUNDTASKEXECUTOR_H
#define BACKGROUNDTASKEXECUTOR_H


#include <QThread>
#include "backgroundtask.h"
#include "common/common/threadsync.h"


/*!
	\class BackgroundTaskExecutor
	\brief One object providing system to execute methods of objects in another thread
	\ingroup Common

	IMPORTANT:
	- Must STOP task before deleting extrnal task object

	Use:
	- inherit own task object from BackgroundTaskInterface, register it here
	- ExecuteTask() of this task object  will be called in separate thread created here
	- Can be used as local fire & forget, see below:

	Example:
	\code
	//will call ExecuteTask() method of myTask object once in separete thread then it will destroy all
	(new BackgroundTaskExecutor(new myTask(),0,false,true,true))->Start();
	\endcode

*/
class BackgroundTaskExecutor : public QThread
{
	Q_OBJECT

public:
	BackgroundTaskExecutor(BackgroundTaskInterface *pTask,QObject *parent=NULL,int nTimeInterval=0,bool bPeriodical=true, bool bDeleteTaskAfterFinish=false,bool bDeleteSelfAfterFinish=false);
	~BackgroundTaskExecutor();

	void Start(bool bStartTaskNow=false,int nStartTaskAfterMSec=0);	
	void StartNow(int nCallerID=-1,int nTaskID=-1,QString strData="");												//execute task once
	void Stop();	
	void SetTimeInterval(int nTimeInterval){m_nTimeInterval=nTimeInterval;}; //used after stop to restart task with new interval!

signals:
	void StartTask();
	void StopTask();
	void StartTaskNow(int nCallerID,int nTAskID,QString data);
	

protected slots:
	virtual void Finished();

protected:
	void timerEvent(QTimerEvent *event); //overriden QObject timer method

protected:
	void run(); //thread support
	ThreadSynchronizer m_ThreadSync;	//< for syncing between mastr & slave thread
	BackgroundTaskInterface* m_pTask;	//< external task object
	int m_nTimeInterval;
	bool m_bPeriodical;					
	bool m_bDeleteTaskAfterFinish;		//< when Finished() slot is invoked delete task or when this object is destroyed
	bool m_bDeleteSelfAfterFinish;		//< when Finished() slot , delete itself
	int m_nTimerID;

    
};

#endif // BACKGROUNDTASKEXECUTOR_H
