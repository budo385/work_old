#include "phonet.h"
#include <QLibrary>
#include "PH_EXT.H"

static QLibrary s_phonet_dll;;

typedef int (*tphonet)(char src[], char dest[], int len, int mode);
typedef int (*tcheck_rules)(int language, int trace_only);

bool Phonet::ConvertString2Phonet(QString strIn,QString &strOut)
{
	//check if loaded:
	if (!s_phonet_dll.isLoaded())
	{
		s_phonet_dll.setFileName("phonet");
		if(!s_phonet_dll.load())
		{
			return false;
		}
	}
	if (strIn.isEmpty())
	{
		strOut="";
		return true;
	}

	char szOut[4096];

	//->init DBO with current connection:
	tphonet pfphonet = (tphonet) s_phonet_dll.resolve("phonet");
	if (!pfphonet)
		return false;

	int nCount=pfphonet(strIn.toLatin1().data(),szOut,4096,PHONET_GERMAN+FIRST_RULES);
	if (nCount<=0)
		return false;

	strOut=QString(szOut);

	return true;

}
