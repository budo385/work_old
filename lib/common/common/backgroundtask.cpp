#include "backgroundtask.h"



/*!
	Inits with task parameters:
	- Task can be executed once (nTimeInterval=0)
	- Task execution can be delayed (nTimeInterval<>0 && bPeriodical=false
	- Task execution can be repeately executed (nTimeInterval<>0 && bPeriodical=true)

	\param pTask			- pointer to task
	\param nTimeInterval	- in msec, interval in which Task will be executed,if 0 then once
	\param bPeriodical		- if true then periodical execution 
*/
BackgroundTask::BackgroundTask(BackgroundTaskInterface *pTask,int nTimeInterval, bool bPeriodical)
{
	m_pTask=pTask;
	m_nTimeInterval=nTimeInterval;
	m_bPeriodical=bPeriodical;
	m_nTimerID=-1;
	m_bTaskFinished=false; //when task is over it is deleted, but in some cases deleteLater is executed much later, this flag will prevent 2nd deleted object from receiving signals
}

BackgroundTask::~BackgroundTask()
{

}


/*!
	Called by timer or by BackgroundTaskExecutor, executes task, override to implement logic
*/
void BackgroundTask::Start()
{
	if (m_bTaskFinished) 
		{qDebug()<<"Deleted object received signal";return;}

	Q_ASSERT(m_nTimerID==-1); //first stop task then start again

	//scheduled for delay or repeat or execute once
	if(m_nTimeInterval>0)
		m_nTimerID=startTimer(m_nTimeInterval);
	else
	{
		m_pTask->ExecuteTask();
		m_bTaskFinished=true;
		emit TaskFinished(); //only emitted if task is executed once
	}
}


// regardless of time interval, executes task
void BackgroundTask::StartNow(int nCallerID,int nTaskID,QString strData)
{
	if (m_bTaskFinished) 
		{return;}
	m_pTask->ExecuteTask(nCallerID,nTaskID,strData);
}


/*!
	Called by BackgroundTaskExecutor when stopping task
*/
void BackgroundTask::Stop()
{
	if(m_nTimerID!=-1)killTimer(m_nTimerID);
	m_nTimerID=-1;

}


void BackgroundTask::OnDelete()
{
	blockSignals(true);
	deleteLater();
}


//overriden QObject timer method
void BackgroundTask::timerEvent(QTimerEvent *event)
{
	if(event->timerId()==m_nTimerID)
	{
		if (m_bTaskFinished) 
			{qDebug()<<"Deleted object received signal";return;}

		if(!m_bPeriodical) Stop();				//stop timer first if non periodical
		m_pTask->ExecuteTask();	//exec task
		if(!m_bPeriodical) 
		{
			m_bTaskFinished=true;
			emit TaskFinished(); //only emitted if task is executed once
		}
	}

}


