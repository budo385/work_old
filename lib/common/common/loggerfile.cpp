#include "loggerfile.h"
#include <QFileInfo>

LoggerFile::LoggerFile()
{
}


LoggerFile::~LoggerFile()
{
	//close log file, flush buffer:
	if(m_fileLogFile.isOpen())
	{
		if (m_byteBuffer.size())
			m_fileLogFile.write(m_byteBuffer);
		m_fileLogFile.close();
	}
		
}
void LoggerFile::FlushLog()
{
	if(m_fileLogFile.isOpen())
	{
		if(m_fileLogFile.write(m_byteBuffer)!=-1)
		{
			m_fileLogFile.flush();
			m_byteBuffer.clear();
		}
	}
}


/*!
	Initialize logger (give log file path)
	\param Path			- file path
	\param nBufferSize	- Kb
	\param nMaxLogSize	- Mb
	\return			- false if file can not be opened
*/
bool LoggerFile::Initialize(const QString &strPath,int nBufferSize,int nMaxLogSize)
{
	m_szLogFilePath = strPath;
	if (nMaxLogSize<0)nMaxLogSize=0;
	if (nMaxLogSize>1000)nMaxLogSize=1000; //max 1000Mb
	if (nMaxLogSize==0)nMaxLogSize=1000; //max 1000Mb

	if (nBufferSize<0)nBufferSize=0;
	if (nBufferSize>4096)nBufferSize=4096; //max 4Mb buffer

	m_nBufferSizeForFileLogger=(qint64)nBufferSize*1024;
	m_nMaxLogSize=(qint64)nMaxLogSize*1048576;
	m_byteBuffer.clear();
	
	//close log file if already open:
	if(m_fileLogFile.isOpen())
		m_fileLogFile.close();

	//open file:
	m_fileLogFile.setFileName(strPath);
	return m_fileLogFile.open(QIODevice::Append | QIODevice::Text);
}

/*!
	Write to log file

	\param nMsgID			- unique msg ID
	\param strMsgArguments	- message arguments, cen be empty
	\param nMsgType			- warning, error or success...see LoggerAbstract
*/

void LoggerFile::logMessage (int nMsgType,int nMsgID, QString strMsgArguments)
{
	//if file not open, return
	if(!m_fileLogFile.isOpen())
		return;
	
	//just to retrieve msg: msgID must be in statuscodeset!!!
	QString strCategoryName;
	QString strMsgTypeName;
	QString strMsgText;
	int nCatID;

	//get formatted msg:
	Status err;
	err.setError(nMsgID,strMsgArguments);
	strMsgText=err.getErrorText();
	StatusCodeSet::getMessageDetails(nMsgType,nMsgID,nCatID,strCategoryName,strMsgTypeName);

	QTextStream out(&m_byteBuffer,QIODevice::Append);
	QString strMsg=QString("%1\t%2\t%3\t%4\t%5\t%6").arg(QDateTime::currentDateTime().toString("yyyy-MM-dd hh.mm.ss.zzz")).arg(nMsgType).arg(nMsgID).arg(strMsgTypeName).arg(strCategoryName).arg(strMsgText);
	out<<strMsg<<endl;

	/*
	//Time   - Category   - ID  - Txt
	out <<  QDateTime::currentDateTime().toString("yyyy-MM-dd hh.mm.ss")+"  "+nMsgType	  
		<<  strMsgTypeName << "\t " 
		<<  strCategoryName	<<  "\t "
		<<  strError <<	endl;
	*/

	//flush buffer if > 64kb (default):
	if (m_byteBuffer.size()>m_nBufferSizeForFileLogger)
	{
		if(m_fileLogFile.write(m_byteBuffer)!=-1)
		{
			m_fileLogFile.flush();
			m_byteBuffer.clear();
		}
	}

	CheckForMaxSize();
}


void LoggerFile::SetMaxLogSize(int nMaxLogSize)
{
	if (nMaxLogSize<0)nMaxLogSize=0;
	if (nMaxLogSize>1000)nMaxLogSize=1000; //max 500Mb
	if (nMaxLogSize==0)nMaxLogSize=1000; //max 1000Mb

	m_nMaxLogSize=(qint64)nMaxLogSize*1048576;
}
void LoggerFile::SetMemoryBufferSize(int nBufferSize)
{
	if (nBufferSize<0)nBufferSize=0;
	if (nBufferSize>4096)nBufferSize=4096; //max 4Mb buffer
	m_nBufferSizeForFileLogger=(qint64)nBufferSize*1024;
}


//if reached max size: copy all into new file, delete old one, rename new one...
void LoggerFile::CheckForMaxSize()
{
	if (m_nMaxLogSize==0)
		return;

	if (m_fileLogFile.size()>m_nMaxLogSize)
	{
		m_fileLogFile.flush();
		m_byteBuffer.clear();

		int nNewPos=m_fileLogFile.size()-m_nMaxLogSize+m_nMaxLogSize*0.20; //truncate 20%Mb below maximum
		if (nNewPos<=0)
			nNewPos=0;

		if (nNewPos==0)
		{
			m_fileLogFile.close();
			m_fileLogFile.remove();
			m_fileLogFile.open(QIODevice::Append | QIODevice::Text);
			return;
		}
	
		m_fileLogFile.close();
		m_fileLogFile.open(QIODevice::ReadOnly | QIODevice::Text);

		m_fileLogFile.seek(nNewPos);
		QByteArray byteBuffer=m_fileLogFile.read(2000000);
		int nPosNewLine=byteBuffer.indexOf("\n");
		if (nPosNewLine>=0)
		{
			byteBuffer=byteBuffer.mid(nPosNewLine+1);
		}
		else
			byteBuffer.clear();

		if (byteBuffer.size()==0)
		{
			m_fileLogFile.close();
			m_fileLogFile.remove();
			m_fileLogFile.open(QIODevice::Append | QIODevice::Text);
			return;
		}

		QFileInfo info(m_szLogFilePath);
		QFile fileTemp;
		fileTemp.setFileName(info.absolutePath()+"/temp.log");
		fileTemp.open(QIODevice::WriteOnly);
		fileTemp.write(byteBuffer);
		while(m_fileLogFile.bytesAvailable()>0)
		{
			byteBuffer=m_fileLogFile.read(2000000); //2Mb chunks
			fileTemp.write(byteBuffer);
		}

		m_fileLogFile.close();
		m_fileLogFile.remove();
		fileTemp.rename(info.absolutePath()+"/"+info.fileName());
		fileTemp.close();
		m_fileLogFile.open(QIODevice::Append | QIODevice::Text);
	}
}

//in MB
qint64	LoggerFile::GetCurrentLogSize()
{
	if(!m_fileLogFile.isOpen())
		return 0;

	return m_fileLogFile.size();
}

//reads log from nCurrentPosition backwards to the nChunkSize: finds first entry
//returns nCurrentPosition to start of red block
//if nCurrentPosition=0, whole file is red, if incoming nCurrentPosition=-1, read from end
//nLogFileSizeOnStartOfChunkRead = used to check if log is truncated while it was read, initial value=-1
//no mutext needed:
//if bGoForward, nLogFileSizeOnStartOfChunkRead must >0 and <=size, and at end: nCurrentPosition=nLogFileSizeOnStartOfChunkRead
bool LoggerFile::GetLog(QByteArray &logData,qint64 &nCurrentPosition,  qint64 &nLogFileSizeOnStartOfChunkRead, int nChunkSize)
{
	QFile fileLog(m_szLogFilePath);

	if(!fileLog.open(QIODevice::ReadOnly))
		return false;

	if (nLogFileSizeOnStartOfChunkRead!=-1)
	{
		if (nLogFileSizeOnStartOfChunkRead>fileLog.size()) //log file is truncated, repeat chunk reads...
			return false;
	}

	nLogFileSizeOnStartOfChunkRead=fileLog.size();
	if (nCurrentPosition>=0)
		nCurrentPosition=nCurrentPosition-nChunkSize;
	else
		nCurrentPosition=fileLog.size()-nChunkSize;
	if (nCurrentPosition<0)nCurrentPosition=0;
	

	fileLog.seek(nCurrentPosition);
	if (nCurrentPosition>0) //find new line, backwards or to 0:
	{
		QByteArray start_char=fileLog.peek(1);
		while (nCurrentPosition>0 && start_char!="\n")
		{
			nCurrentPosition--;
			fileLog.seek(nCurrentPosition);
			start_char=fileLog.peek(1);
			nChunkSize++; //increment chunk to catch last part...
		}
		if (nCurrentPosition!=0)
		{
			nCurrentPosition++; //postion after \n
			fileLog.seek(nCurrentPosition);
		}
	}
	logData=fileLog.read(nChunkSize); 

	//cut off garbage at start
	/*
	int nPosNewLine=logData.indexOf("\n"); //find first new line, muv cursor to that position
	if (nPosNewLine>=0 && nCurrentPosition>0)
	{
		logData=logData.mid(nPosNewLine+1);
		nCurrentPosition+=nPosNewLine;
	}
	*/
	//cut off garbage at end: should be aligned on second read
	int nPosLastNewLine=logData.lastIndexOf("\n");
	if (nPosLastNewLine>=0)
		if (nPosLastNewLine!=(logData.size()-1))
		{
			logData.chop(logData.size()-1-nPosLastNewLine);
		}

	fileLog.close();

	return true;
}