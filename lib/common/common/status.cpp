#include "status.h"
#include <QObject>
#include <QStringList>

//#include "loggerabstract.h"	//for logging all errors
//static void	*s_pErrorLogger=NULL;

void Status::operator =(const Status &that)
{
	m_nErrorCode			= that.m_nErrorCode;
	m_szErrorText			= that.m_szErrorText;
}

/*!
	Sets the Error by code
	\param ErrorCode required
	\param ErrorText
*/
void Status::setError(int ErrorCode, QString ErrorTextArguments)
{
	m_nErrorCode = ErrorCode;
	m_szErrorText= ErrorTextArguments;
/*
	if (s_pErrorLogger && m_nErrorCode!=0)
	{

		LoggerAbstract *p=static_cast<LoggerAbstract*>(s_pErrorLogger);
		p->logMessage(StatusCodeSet::TYPE_ERROR,ErrorCode,ErrorTextArguments);
	}
*/
}




//returns error text (translated)
//if error has number != ERR_GENERAL, text will be lookup in StatusCodeSet repositoty
//if error text !=Empty, it will be displayed as is, if text in repository doesn't have %
//if text in repository  have %, error text is considered to be argument
QString Status::getErrorText()
{
	//if general error return msg back as is
	if(m_nErrorCode==StatusCodeSet::ERR_GENERAL)
	{
		FormatLongMessages(m_szErrorText);
		if (m_szErrorText.isEmpty())
		{
			m_szErrorText=QObject::tr("General Error");
		}
		return m_szErrorText;
	}

	QString strErrorText;
	if(m_szErrorText.isEmpty())		//no text is set, text is stored inside repository
	{
		StatusCodeSet::getErrorDetails(m_nErrorCode, strErrorText);
		FormatLongMessages(strErrorText);
		return strErrorText; //already translated
	}
	else //text is set, it can be argument or full message
	{
		StatusCodeSet::getErrorDetails(m_nErrorCode, strErrorText);
		
		if(strErrorText.indexOf('%')!=-1)	//if argument found in err msg, then treat m_szErrorText as set of arguments
		{
			strErrorText=strErrorText; //translate msg without arguments
			QStringList lstArguments=m_szErrorText.split(";");
			for(int i=0;i<lstArguments.size();++i)
			{
				QString strArg=lstArguments.at(i); //try to translate argument
				strErrorText=strErrorText.arg(strArg);
			}
			FormatLongMessages(strErrorText);
			return strErrorText;	//if arguments, return formatted msg
		}
		else
		{
			FormatLongMessages(m_szErrorText);
			return m_szErrorText;	//return setted error as translated msg
		}
	}
}

void Status::FormatLongMessages(QString& ErrorText)
{
	return;

	int nMaxCharsInLine = 70;
	
	int nPos = 0;
	while(true)
	{
		int nNextNewLine = ErrorText.indexOf('\n', nPos);
		int nLinelength  = -1;
		if(nNextNewLine >= 0)
			nLinelength = nNextNewLine - nPos;
		else
			nLinelength = ErrorText.count() - nPos;

		if(nLinelength > nMaxCharsInLine){
			ErrorText.insert(nPos + nMaxCharsInLine, '\n');
			nPos += nMaxCharsInLine + 1;
		}
		else if(nNextNewLine >= 0){
			nPos += nNextNewLine + 1;
		}
		else
			break;
	}
}

/*
void Status::SetErrorLogger(void *pLogger)
{
	s_pErrorLogger=pLogger;
}


void* Status::GetErrorLogger()
{
	return s_pErrorLogger;
}
*/