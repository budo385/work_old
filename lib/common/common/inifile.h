/*!
\class  IniFile
\author MR
\brief  This class provides access to plain text INI file
*/
#ifndef INIFILE_H
#define INIFILE_H

#include <string>
#include <vector>
#include <QString>

class IniFile
{
protected:
	//ini file entry
	class IniEntry
	{
	public:
		IniEntry(){};
		IniEntry(const IniEntry &that){ operator =(that); }

		IniEntry& operator=(const IniEntry &that)
		{
			if(this != &that){   // don't copy yourself
				m_name          = that.m_name;
				m_value         = that.m_value;
			}
			return *this;
		}

		//needed for key list searching
		bool operator == (const IniEntry &that) const { return m_name == that.m_name; };

		std::string m_name;
		std::string m_value;
	};

	//ini file section
	class IniSection
	{
	public:
		IniSection(){};
		IniSection(const IniSection &that){ operator =(that); }
		IniSection& operator=(const IniSection & that)
		{
			if(this != &that){   // don't copy yourself
				m_name          = that.m_name;
				m_entries       = that.m_entries;
			}
			return *this;
		}

		//needed for section list searching
		bool operator == (const IniSection &that) const { return m_name == that.m_name; }

		std::string m_name;
		std::vector<IniEntry>  m_entries;
	};

	typedef std::vector<IniEntry>::iterator		IniEntryIterator;
	typedef std::vector<IniSection>::iterator	IniSectionIterator;

public:
	IniFile();
	virtual ~IniFile();

	// storage handling
	void SetPath(const char *szPath);
	void SetPath(QString strPath);

	virtual bool Load();
	virtual bool Save();

	// contents handling
	void Clear();

	bool GetValue(const char *szSection, const char *szKey, bool &nValue, bool nDefault = false);
	bool GetValue(QString strSection, QString strKey, bool &nValue, bool nDefault = false);

	bool GetValue(const char *szSection, const char *szKey, int &nValue, int nDefault = 0);
	bool GetValue(QString strSection, QString strKey, int &nValue, int nDefault = 0);

	bool GetValue(const char *szSection, const char *szKey, std::string &strValue, const char *szDefault = "");
	bool GetValue(QString strSection, QString strKey, QString &strValue, QString strDefault="");

	void SetValue(const char *szSection, const char *szKey, const bool nValue);
	void SetValue(QString strSection, QString strKey, const bool nValue);

	void SetValue(const char *szSection, const char *szKey, const int nValue);
	void SetValue(QString strSection, QString strKey, const int nValue);

	void SetValue(const char *szSection, const char *szKey, const char *szValue);
	void SetValue(QString strSection, QString strKey, QString strValue);

	void AddSection(const char *szSection);
	void AddSection(QString strSection);

	bool RemoveSection(const char *szSection);
	bool RemoveSection(QString strSection);

	bool SectionExists(const char *szSection);
	bool SectionExists(QString strSection);

	bool EntryExists(const char *szSection, const char *szKey);
	bool EntryExists(QString strSection, QString strKey);

protected:
	void ParseIniLine(std::string &strLine);
	void ParseIniLines(std::string &strLines);
	bool FindSection(const char *szSection, IniSectionIterator &ItRes);
	bool FindEntry(const char *szSection, const char *szKey, IniEntryIterator &ItRes);

	bool EnsurePathDirExists();

protected:
	QString	m_strFilePath;			// INI file name
	std::vector<IniSection> m_data;		// parsed contents
};

#endif
