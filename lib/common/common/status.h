/*!
	\class Status
	\brief Standard status object (for setting error code, and retrieving by calling function)
	\ingroup Common/ErrorHandler

	Use (three ways):

	Use (three ways):

	A) SetError(1,"Text") , getErrorText() will return text "Text" or use SetError(StatusCodeSet::ERR_GENERAL,"Text");

	B) Define number in StatusCodeSet, e.g. ERR_NUM_1, but do not define text in StatusCodeSet:
	SetError(StatusCodeSet::ERR_NUM_1)
	or 
	SetError(StatusCodeSet::ERR_NUM_1,"MyText"), getErrorText() will return "MyText"

	C) Define number in StatusCodeSet, e.g. ERR_NUM_2, and define text in StatusCodeSet:
	SetError(StatusCodeSet:ERR_NUM_2), getErrorText() will return text from StatusCodeSet,
	SetError(StatusCodeSet:ERR_NUM_2,"MyText"), getErrorText() will return "MyText"

	if text from StatusCodeSet has % arguments then "MyText" will be considered as argument
	more arguments can be passed in one call (delimited by semicolon ;)
	See example: MSG_SYSTEM_APP_SRV_SUCESS_START

	Note:
	getErrorTextRaw() will return text as it was set, use this call to copy status code/text into lists
	or other status objects.
	Note: 
	- DbSimpleOrm returns rawErrorText inside statuslist in Write method
	- DbSqlQuery returns argument describing SQL error for error: ERR_SQL_STATEMENT_FAIL ("SQL statement execution failed! Reason: %1")
	- Arguments will be translated

*/
#ifndef STATUS_H
#define STATUS_H
#include <QString>
#include "statuscodeset.h" 

class Status
{
public:
	Status():m_nErrorCode(0){};
	Status(const Status &that){ operator=(that); }

	void		operator =(const Status &that);
	void		setError(int ErrorCode, QString ErrorText = "");
	inline bool	IsOK(){return m_nErrorCode==0;}
	int 		getErrorCode(){return m_nErrorCode;};
	QString		getErrorText();
	QString		getErrorTextRaw(){return m_szErrorText;};

	//static void SetErrorLogger(void *pLogger);
	//static void* GetErrorLogger();

protected:
	void FormatLongMessages(QString& ErrorText);

private:
	int		m_nErrorCode;			///< Error Code
	QString	m_szErrorText;			///< Error Text


};
#endif // STATUS_H
