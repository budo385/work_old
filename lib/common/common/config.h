#ifndef CONFIG_H
#define CONFIG_H

//#define SPECIAL_DATABASE_SUPPORT	//for complex Write methods with NextID (only for firebird)!!

//for decoding INI files
#define MASTER_PASSWORD "SOKRATES(R)"
#define LOCAL_SYSTEM_PASSWORD "CHIVAVA"


//-------------------------------
//GLOBAL HARDCODED CONFIG OPTIONS:
//-------------------------------

//ROLES:
#define SRV_ROLE_THICK		0
#define SRV_ROLE_APPSERVER	1
#define SRV_ROLE_JOBSERVER	2


//TIME OUTS:
#define CLIENT_IS_ALIVE_INTERVAL 300			//secs	//Client Is alive period report (must be global for garbage collectors on different app. servers)
#define CLIENT_IS_ALIVE_INTERVAL_CHECK 320		//secs	//Server check Is alive period (greater then client for connection problems and dynamic fail over) greater then CLIENT_IS_ALIVE_INTERVAL+CLIENT_CONNECTION_TIMEOUT_INTERVAL
#define GARBAGE_COLLECTOR_INTERVAL 340			//secs	//GBC interval, must be greater then CLIENT_IS_ALIVE_INTERVAL_CHECK
#define MASTER_GARBAGE_COLLECTOR_INTERVAL 660	//secs	//GBC interval, must be greater then APP_SESSION_REPORT_INTERVAL
#define APP_SESSION_REPORT_INTERVAL 240			//secs	//Interval in which server report
#define APP_SESSION_REPORT_INTERVAL_CHECK 260	//secs	//Interval in which master will test if server is down > APP_SESSION_REPORT_INTERVAL
#define LBO_LIST_REFRESH_INTERVAL	600			//secs	//LBO List will expire in 
#define EMAIL_SYNC_INTERVAL 60					//secs	//email sync interval
#define EMAIL_PUSH_CHECK_INTERVAL 120			//secs	//email sync interval

#define GUI_CLOSE_WINDOW_RETIRES_AFTER_BROKEN_CONN	3	//how many times to test for all modal dialogs to close after firing fatal error handler


//CLIENT TRIES:
#define MAX_CLIENT_ERROR_RECOVER_ATTEMPS 3				//(min 3 if you want to enable normal behaviour) When error occurs, how many times to retry before declaring error
#define MAX_CLIENT_REDIRECTION_ATTEMPS 3				//(min 3 if you want to enable normal behaviour) When client is given order to redirect, limits number of maximum redirection tries

//SESSION SECURITY PARAMS:
#define SESSION_AUTHENTICATION_TIMEOUT 60				//(seconds)authentication process timeout (time in which client must login or disconnect), after this kickban
#define SESSION_AUTHENTICATION_MAX_ATTEMPTS	10			//defines in how many tries user must login, after this kickban
#define SESSION_KICKBAN_PERIOD	3600					//(seconds) for how long client IP address is banned for accessing server (0 for infinite) ->1 hour (wrong sessions or damgage rpc)
#define SESSION_CHECK_INTERVAL	600						//(seconds)time interval in which user session will be checked into database (at any new request)




#endif