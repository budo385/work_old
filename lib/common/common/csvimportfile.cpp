#include "csvimportfile.h"
#include <QFile>
#include <QStringList>

#define PARSE_FIELD_START	1
#define PARSE_FIELD_MIDDLE	2

QByteArray ReadLineFromBlob(QByteArray &data, int nStart = 0, int *pEnd = NULL)
{
	QByteArray strLine;
	int nEnd = data.indexOf('\n', nStart);
	if(nEnd > 0)
		strLine = data.mid(nStart, nEnd-nStart);
	if(NULL != pEnd)
		*pEnd = nEnd;
	return strLine;	
}

QByteArray ReadChunkFromBlob(QByteArray &data, int nSize, int nStart = 0, int *pEnd = NULL)
{
	QByteArray strLine;
	if(nStart + nSize > data.size())
		nSize = data.size()-nStart;
	int nEnd = nStart + nSize;
	strLine = data.mid(nStart, nEnd-nStart);
	if(NULL != pEnd)
		*pEnd = nEnd;
	return strLine;	
}

CsvImportFile::CsvImportFile()
{
	m_bUtf8 = false;
	m_cDelimiter = '\t';	//default delimiter
}

CsvImportFile::~CsvImportFile()
{
}

void CsvImportFile::Load(Status &status, QString strFile, DbRecordSet &list, const QStringList &lstFormatLines, const QStringList &lstUtf8FormatLines, bool bHdrOptional, FN_PROGRESS pProgress/*QProgressDialog *pDlg*/, unsigned long nUserData)
{
	//open the file
	QFile file(strFile);
	if (!file.open(QIODevice::ReadOnly)){
		status.setError(1, tr(QT_TR_NOOP("Failed to open the file!")));
		return;
	}

	list.destroy();

	//init parser
	m_nCurRecLine  = 0;
	m_nCurRecField = 0;

	//read first line - format specification
	QString strFirstLine = file.readLine(1000);

	//strip line endings (up to two chars)
	if( strFirstLine.length()>0 && 
		(strFirstLine.at(strFirstLine.length()-1) == '\r' ||
		 strFirstLine.at(strFirstLine.length()-1) == '\n')){
			strFirstLine = strFirstLine.mid(0, strFirstLine.length()-1);
	}
	if( strFirstLine.length()>0 && 
		(strFirstLine.at(strFirstLine.length()-1) == '\r' ||
		 strFirstLine.at(strFirstLine.length()-1) == '\n')){
			strFirstLine = strFirstLine.mid(0, strFirstLine.length()-1);
	}

	//find if the line matches 1st list
	int nLinePos = -1;
	int nMax = lstFormatLines.length();
	int i;
	for(i=0; i<nMax; i++){
		if(strFirstLine == lstFormatLines[i]){
			nLinePos = i;
			m_strHdr = strFirstLine;
			break;
		}
	}
	if(nLinePos >= 0)
		m_bUtf8 = false;
	else
	{
		//find if the line matches 2nd list
		nMax = lstUtf8FormatLines.length();
		for(i=0; i<nMax; i++){
			if(strFirstLine == lstUtf8FormatLines[i]){
				nLinePos = i;
				m_strHdr = strFirstLine;
				break;
			}
		}

		if(nLinePos >= 0)
			m_bUtf8 = true;
		else
		{
			if(!bHdrOptional){
				status.setError(1, tr(QT_TR_NOOP("Invalid file format header!")));	// wrong file format header
				return;
			}
		}
	}

	//reset position if header not accepted
	if(bHdrOptional && nLinePos < 0)
		file.seek(0);

	//if( strFirstLine != strFormatLine &&
	//	strFirstLine != strUtf8FormatLine){
	//	status.setError(1, tr(QT_TR_NOOP("Invalid file format header!")));	// wrong file format header
	//	return;
	//}
	//m_bUtf8 = (strFirstLine == strUtf8FormatLine);

	//now parse the rest of the data dynamically
	QString strChunk;
	while (!file.atEnd()){
		if(pProgress){
			if(!pProgress(nUserData))	// increment progress
				return;	//canceled
		}
		if(m_bUtf8)
			strChunk = strChunk.fromUtf8(file.read(10000));
		else
			strChunk = strChunk.fromLocal8Bit(file.read(10000));
		ParseChunk(strChunk);
	}
	if(!m_strCurField.isEmpty())
		AddFieldData(m_strCurField);

	list = m_list; //copy result
}

void CsvImportFile::LoadFromBlob(Status &status, QByteArray &data, DbRecordSet &list, const QStringList &lstFormatLines, const QStringList &lstUtf8FormatLines, bool bHdrOptional, FN_PROGRESS pProgress, unsigned long nUserData)
{
	list.destroy();

	//init parser
	m_nCurRecLine  = 0;
	m_nCurRecField = 0;

	//read first line - format specification
	int nOffset = 0;
	QString strFirstLine = ReadLineFromBlob(data, 0, &nOffset);
	nOffset ++;

	//strip line endings (up to two chars)
	if( strFirstLine.length()>0 && 
		(strFirstLine.at(strFirstLine.length()-1) == '\r' ||
		 strFirstLine.at(strFirstLine.length()-1) == '\n')){
			strFirstLine = strFirstLine.mid(0, strFirstLine.length()-1);
	}
	if( strFirstLine.length()>0 && 
		(strFirstLine.at(strFirstLine.length()-1) == '\r' ||
		 strFirstLine.at(strFirstLine.length()-1) == '\n')){
			strFirstLine = strFirstLine.mid(0, strFirstLine.length()-1);
	}

	//find if the line matches 1st list
	int nLinePos = -1;
	int nMax = lstFormatLines.length();
	int i;
	for(i=0; i<nMax; i++){
		if(strFirstLine == lstFormatLines[i]){
			nLinePos = i;
			m_strHdr = strFirstLine;
			break;
		}
	}
	if(nLinePos >= 0)
		m_bUtf8 = false;
	else
	{
		//find if the line matches 2nd list
		nMax = lstUtf8FormatLines.length();
		for(i=0; i<nMax; i++){
			if(strFirstLine == lstUtf8FormatLines[i]){
				nLinePos = i;
				m_strHdr = strFirstLine;
				break;
			}
		}

		if(nLinePos >= 0)
			m_bUtf8 = true;
		else
		{
			if(!bHdrOptional){
				status.setError(1, tr(QT_TR_NOOP("Invalid file format header!")));	// wrong file format header
				return;
			}
		}
	}

	//reset position if header not accepted
	if(bHdrOptional && nLinePos < 0)
		nOffset = 0;

	//if( strFirstLine != strFormatLine &&
	//	strFirstLine != strUtf8FormatLine){
	//	status.setError(1, tr(QT_TR_NOOP("Invalid file format header!")));	// wrong file format header
	//	return;
	//}
	//m_bUtf8 = (strFirstLine == strUtf8FormatLine);

	//now parse the rest of the data dynamically
	QString strChunk;
	while (nOffset >= 0 && nOffset < data.size()){
		if(pProgress){
			if(!pProgress(nUserData))	// increment progress
				return;	//canceled
		}
		if(m_bUtf8)
			strChunk = strChunk.fromUtf8(ReadChunkFromBlob(data, 10000, nOffset, &nOffset));
		else
			strChunk = strChunk.fromLocal8Bit(ReadChunkFromBlob(data, 10000, nOffset, &nOffset));
		ParseChunk(strChunk);
	}
	if(!m_strCurField.isEmpty())
		AddFieldData(m_strCurField);

	list = m_list; //copy result
}

// parse chunk of data
void CsvImportFile::ParseChunk(QString &strChunk)
{
	int nChunkSize  = strChunk.length();
	for(int i=0; i<nChunkSize; i++)
	{
		QChar cCurrent = strChunk.at(i);		

		if(cCurrent == m_cDelimiter)
		{
			//see if the field really ended (if quoted, it must end with un-escaped quote)
			if(CheckQuoteEnded())
			{
				AddFieldData(m_strCurField);
				m_nCurRecField ++;
			}
			else
			{
				m_strCurField += cCurrent; // delimiter is a part of the field
			}
		}
		else if(cCurrent == '\r' || 
				cCurrent == '\n')
		{
			//see if the field really ended (if quoted, it must end with un-escaped quote)
			if(CheckQuoteEnded())
			{
				if( m_strCurField.isEmpty() &&
					0 == m_nCurRecField)
				{
					//skip empty line
				}
				else
				{
					AddFieldData(m_strCurField);

					//qDebug() << "CSV Parser: line " << m_nCurRecLine << " has " << m_nCurRecField+1 << " fields";

					//start the new line
					m_nCurRecLine ++;
					m_nCurRecField = 0;
				}

				//skip next new line char
				if( (i < nChunkSize-1) && 
					(strChunk.at(i+1) == '\r' || strChunk.at(i+1) == '\n' ))
				{
					i++;
					continue;
				}
			}
			else
			{
				m_strCurField += cCurrent; // new line char is a part of the field
			}
		}
		else
			m_strCurField += cCurrent;
	}
}

void CsvImportFile::AddFieldData(QString &strData)
{
	//detect if we need to add new line
	int nLines = m_list.getRowCount();
	if(m_nCurRecLine >= nLines)	m_list.addRow();
	
	//detect if we need to add new field
	int nFields = m_list.getColumnCount();
	if(m_nCurRecField >= nFields) m_list.addColumn(QVariant::String, QString("Col%1").arg(nFields));

	//remove quotes here
	bool bWasQuoted = false;
	int nLength = strData.length();
	if( nLength > 1 && 
		strData.at(0) == '"' && 
		strData.at(nLength-1) == '"')
	{
		strData = strData.mid(1, nLength-2);
		bWasQuoted = true;
	}

	//replace escaped quotes "" with "
	strData.replace("\"\"", "\"");

	//MB request on 2006.11.13 ID:224 "// equals new line"
	//strData.replace("//", "\n");
	//MB request on 2008.09.30 ID:1784 "@@ now equals new line"
	if(!bWasQuoted)
		strData.replace("@@", "\n");

	//now convert all possible line endings into the Win one
	strData.replace("\r\n",   "\n");
	strData.replace("\r",	  "\n");
	strData.replace("\n",	  "\r\n");

	//qDebug() << "Field: [" << strData << "]";

	//append the content to the current field
	m_list.setData(m_nCurRecLine, m_nCurRecField, strData);
	strData.clear();
}

bool CsvImportFile::CheckQuoteEnded()
{
	//see if the field starts with quotes
	int nCurLen = m_strCurField.length();
	if(!(nCurLen > 0 && m_strCurField.at(0) == '"'))
		return true;	// no quoting used

	// quoting used, check if quote is validly closed
	int nQuotesCount = 0;
	for(int i=nCurLen-1; i>0; i--)
		if(m_strCurField.at(i) == '"') nQuotesCount++; else break;

	// skip if the quote is escaped with
	if(1 == nQuotesCount && nCurLen>1 && m_strCurField.at(nCurLen-2) == '\\')
		return false;	// quote is escaped

	//must be a pair number of consecutive quotes (ignoring last one)
	if( nQuotesCount > 0 && 
		(nQuotesCount-1)%2 == 0)
	{
		return true;
	}
	return false;
}
