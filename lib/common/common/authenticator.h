#ifndef SESSIONHANDLER_H
#define SESSIONHANDLER_H

#include "common/common/sha256hash.h"
#include <QDateTime>



/*!
    \class Authenticator
    \brief Client session handler: generates auth token for Login
    \ingroup Bus_Core

	Caches authentication parameters, session_id, genereated cnonce and auth token for Login process

*/
class Authenticator
{

public:

	//new:
	static void GenerateAuthenticationToken(QString strUserName,QString strPassword, QByteArray& ClientNonce,QByteArray& AuthToken);
	static void GenerateAuthenticationToken(QString strUserName,QByteArray bytePassword, QByteArray& ClientNonce,QByteArray& AuthToken);
	static bool Authenticate(QByteArray byteClientNonce,QByteArray byteClientAuthToken, QString strUserName,QByteArray bytePassword);
	static bool AuthenticateFromWebService(QString strNonce,QString strAuthToken, QString strUserName,QByteArray bytePassword);
	static QString GenerateAuthenticationTokenForWebService(QString strUserName,QString strPassword, QString strServerNonce);

	static QByteArray GeneratePassword(QString strUserName,QString strPassword);
	static QByteArray GenerateRandomPassword(int nLength, bool bNoNumbers=false);
	static QByteArray GenerateRandomSequence(QByteArray addSalt);

	void GenerateAuthenticationToken(QByteArray& ClientNonce,QByteArray& AuthToken);

	//authentication of keyfile:
	static bool Demo_ParseKey(QByteArray &key,QString &strAppVer, int &nDbVersion,QDateTime &lastModified,int &nChkSum, int &nLocked,QDateTime &startDemoDate);
	static bool Demo_CreateKey(QByteArray &key,QString strAppVer, int nDbVersion,QDateTime lastModified, int nChkSum, int nLocked,QDateTime startDemoDate);
	static bool Demo_ResetKey(QByteArray &key,QDateTime startDemoDate,QString strAppVerLatest, int nDbVersionLatest);
	static bool Demo_UpdateDbVersionKey(QByteArray &key, int nCurrentDbVersion,QString strAppVerLatest, int nDbVersionLatest);


	//auth param cache
	void SetSessionParameters(QString strUserName,QString strPassword,QString strModuleCode, QString strMLIID);
	void GetSessionParameters(QString& strUserName,QString& strPassword,QString& strModuleCode, QString& strMLIID);
	void ClearSessionParameters();


	//session cache
	void SetSessionID(QString strSessionID){m_SessionID=strSessionID;}
	QString GetSessionID(){return m_SessionID;}


	QString m_strUserName,m_strPassword,m_SessionID;
	QString m_strModuleCode;
	QString m_strMLIID;
	int		m_nClientTimeZoneOffsetMinutes;
};


#endif //SESSIONHANDLER_H


