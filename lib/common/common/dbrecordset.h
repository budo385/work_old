
#ifndef DBRECORDSET_H
#define DBRECORDSET_H

#include <QDebug>
#include <QString>
#include <QList>
#include <QSet>
#include <QVariant>
#include <QDateTime>
#include "dbrecordsetdefintion.h"

#ifdef _DEBUG
	#define _DUMP(X) X.Dump();
#else
	#define _DUMP(X) // X.Dump();
#endif

typedef QHash<int, int> MapCols;
typedef QHashIterator<int, int> MapColsIterator;	///< iterator


/*!
	\class  SortData
	\ingroup Db_Core
	\brief  Public class for defining sort data info for one column, every column can be sorted with own sort order
*/

class SortData
{
public:
	//copy & other constructors
	SortData();
	SortData(int nColumn,int nSortOrder = 0):m_nColumn(nColumn),m_nSortOrder(nSortOrder){};
	SortData(const SortData &that);
	SortData &operator =(const SortData &that); 

	int m_nColumn;
	int m_nSortOrder;	//0 asc, 1 desc
};


typedef QList<SortData> SortDataList;



/*!
	\class  StringComparator
	\ingroup Db_Core
	\brief  Class for sorting QString with localaware funct
*/

class StringComparator
{
public:
	//copy & other constructors
	//SortData();
	StringComparator(QString str);
	StringComparator(const StringComparator &that);
	StringComparator &operator =(const StringComparator &that); 
	bool operator ==(const StringComparator &that) const; 
	bool operator <(const StringComparator &that) const; 

	QString m_strValue;
};


/*!
	\class  DbRecordSet
	\ingroup Db_Core
	\brief  This class provides data record storage


	Use:
	- create record set by adding columns dynamically with addColumn()
	- create record set from view defined in DbSqlTableView class with defineFromView()
	- clear content with clear()
	- add rows with addRow(), add vars with setData()
	- get vars from recordset by getDataRef() (fastest) or by getData()
	- use find(),merge(), etc..

*/
class DbRecordSet
{

	//---------------------------------------------------
	//					INTERNAL OBJECTS
	//---------------------------------------------------

	//Class for one row
	class DBRowAdvanced
	{
	public:
		DBRowAdvanced():bSelected(false){};
		DBRowAdvanced(const DBRowAdvanced &that);
		DBRowAdvanced &operator =(const DBRowAdvanced &that); 

		QList<QVariant> DBRow; 
		bool bSelected;
	};

	//class describing single data column 
	class DBColumn
	{
		public:
			bool operator ==(const DBColumn &that); 
			//QString m_strAlias;	//'header'
			QString m_strName;	//SQL table field?
			//QString m_strTable;	//SQL table name
			int	m_nType;		//enum QVariant::Type
	};

	//class needed to sort the list
	class DBRowComparator 
	{
		public:
			bool operator ()(const DBRowAdvanced &a, const DBRowAdvanced &b) const;

			SortDataList m_lstSortCol;
			QList<DBColumn> * m_lstColumns;
	};

public:
    DbRecordSet();
	DbRecordSet(const DbView &view);
    ~DbRecordSet();

	DbRecordSet(const DbRecordSet &that);
	DbRecordSet &operator =(const DbRecordSet &that); 

	static int GetVariantType();

	//work with definition
	bool addColumn(int nDataType, const QString &strName);//, const QString &strAlias = QString(), const QString &strTable = QString());
	bool getColumn(int nCol, int &nDataType, QString &strName) const;//, QString &strAlias,QString &strTable) const; //get column definition
	int getColumnType(int nCol) const { return m_lstColumns[nCol].m_nType; };
	QString getColumnName(int nCol) const { return m_lstColumns[nCol].m_strName; };

	bool setColumn(int nCol, int nDataType, QString strName);//, QString &strAlias,QString &strTable); //set column definition
	bool removeColumn(int nCol);
	bool renameColumn(int nCol, QString strNewName);
	bool renameColumn(QString strOldName, QString strNewName);
	void destroy();
	inline int  getColumnCount() const {return m_lstColumns.size();};
	inline int	getColumnIdx(const QString strName) const {return m_hshColumnMap.value(strName,-1);};

	bool defineFromView(const DbView &view);
	bool copyDefinition(const DbRecordSet &set, bool bDestroyPrevious = true);
	bool renameColumnsFromAlias(const DbView &view);


	//work with data content
	//-------------------------------------------------------------
	void clear();
	bool addRow(int nRowToAdd=1);
	bool insertRow(int nPosition);
	bool deleteRow(int nRow);
	inline int  getRowCount() const {return m_lstData.size();}
	void  removeDuplicates(int nCol);



	//row ops
	//-------------------------------------------------------------
	DbRecordSet getRow(int nRow) const;
	void assignRow(int nRow,DbRecordSet& Row, bool bByName=false,MapCols *mapColsSource=NULL);
	void assignRow(DbRecordSet& Rows, bool bByName=false,bool bAssignToSelected=true,MapCols *mapColsSource=NULL);

	void mapColumns(MapCols& mapCols,const DbRecordSet& list);


	//Get/Set data methods
	//-------------------------------------------------------------

	/*inline*/ QVariant &getDataRef(int nRow, int nCol);
	/*inline*/ const QVariant &getDataRef(int nRow, int nCol) const;

	//Same as above with column name instead of index
	/*inline*/ QVariant &getDataRef(int nRow, QString strColumn);
	/*inline*/ const QVariant &getDataRef(int nRow, QString strColumn) const; 
	

	//Gets method, puts var from list into provided var: copy frm list to local object, copy to your var
	//When Adding new data types they must be added in DbSqlQuery.FetchData method (dbsqlquery.cpp).

	void getData(int nRow, int nCol, int &value);
	void getData(int nRow, int nCol, uint &value);
	void getData(int nRow, int nCol, qulonglong &value);
	void getData(int nRow, int nCol, QString &value);
	void getData(int nRow, int nCol, bool &value);
	void getData(int nRow, int nCol, QByteArray &value);
	void getData(int nRow, int nCol, QDateTime &value);
	void getData(int nRow, int nCol, QDate &value);
	void getData(int nRow, int nCol, QTime &value);
	void getData(int nRow, int nCol, double &value);
	void getData(int nRow, int nCol, DbRecordSet &value);
	

	//Gets method, puts var from list into provided var: copy frm list to local object, copy to your var
	//Note: this is slower, cot every call must search through list of fields by name
	//Same as above with column name instead of index

	void getData(int nRow, QString strColumn, int &value);
	void getData(int nRow, QString strColumn, uint &value);
	void getData(int nRow, QString strColumn, qulonglong &value);
	void getData(int nRow, QString strColumn, QString &value);
	void getData(int nRow, QString strColumn, bool &value);
	void getData(int nRow, QString strColumn, QByteArray &value);
	void getData(int nRow, QString strColumn, QDateTime &value);
	void getData(int nRow, QString strColumn, QDate &value);
	void getData(int nRow, QString strColumn, QTime &value);
	void getData(int nRow, QString strColumn, double &value);
	void getData(int nRow, QString strColumn, DbRecordSet &value);
	
	
	void setData(int nRow, int nCol, int value);
	void setData(int nRow, int nCol, uint value);
	void setData(int nRow, int nCol, qulonglong value);
	void setData(int nRow, int nCol, QString &value);	//BY REF
	void setData(int nRow, int nCol, const char * value);	//string literal
	void setData(int nRow, int nCol, bool value);
	void setData(int nRow, int nCol, QByteArray &value);	//BY REF
	void setData(int nRow, int nCol, QDateTime value);
	void setData(int nRow, int nCol, QDate value);
	void setData(int nRow, int nCol, QTime value);
	void setData(int nRow, int nCol, double value);
	void setData(int nRow, int nCol, DbRecordSet &value);	//BY REF
	
	/*inline*/ void setData(int nRow, int nCol, QVariant &value);
	/*inline*/ void setData(int nRow, int nCol, const QVariant &value);

	//same as above with column name instead of index
	void setData(int nRow, QString strColumn, int value);
	void setData(int nRow, QString strColumn, uint value);
	void setData(int nRow, QString strColumn, qulonglong value);
	void setData(int nRow, QString strColumn, QString &value);	//BY REF
	void setData(int nRow, QString strColumn, const char * value);	//string literal
	void setData(int nRow, QString strColumn, bool value);
	void setData(int nRow, QString strColumn, QByteArray &value);	//BY REF
	void setData(int nRow, QString strColumn, QDateTime value);
	void setData(int nRow, QString strColumn, QDate value);
	void setData(int nRow, QString strColumn, QTime value);
	void setData(int nRow, QString strColumn, double value);
	void setData(int nRow, QString strColumn, DbRecordSet &value);	//BY REF
	/*inline*/ void setData(int nRow, QString strColumn, QVariant &value);
	/*inline*/ void setData(int nRow, QString strColumn, const QVariant &value);

	//sort
	//----------------------------------------------
	void sort(int nCol, int nSortAscending = 0);  //zero is ASC, default
	void sort(QString strColumn, int nSortAscending = 0);  //zero is ASC, default
	void sortMulti(SortDataList &lstCols);
	
	

	// MIN MAX
	//----------------------------------------------
	int GetMaximum(int nCol, QVariant &max_value);
	int GetMinimum(int nCol, QVariant &min_value);



	// selections
	//----------------------------------------------
	void clearSelection();
	void selectAll();
	void selectRow(int nRow, bool bSelect=true);
	void selectRowFromTo(int nRowFrom, int nRowTo, bool bSelect=true);
	bool isRowSelected(int nRow) const;
	int deleteSelectedRows();
	int deleteUnSelectedRows();
	int getSelectedCount();
	int getSelectedRow(int nFrom=0);
	QList<int> getSelectedRows(int nFrom=0);
	DbRecordSet getSelectedRecordSet(int nFrom=0);


	//find, merge:
	//----------------------------------------------
	//if  bFindFirst returns index of found element or -1 if nothing was found
	//if !bFindFirst returns number of selected items
	template<typename T> int find(int nCol, T value, bool bFindFirst = false, bool bSearchOnSelected = false, bool bDeselectNonMatches = true, bool bMatchEqual = true);
	int find(int nCol, QVariant value, bool bFindFirst = false, bool bSearchOnSelected = false, bool bDeselectNonMatches = true, bool bMatchEqual = true);
	template<typename T> int find(QString strColumnName, T value, bool bFindFirst = false, bool bSearchOnSelected = false, bool bDeselectNonMatches = true, bool bMatchEqual = true);

	template <class T> void setColValue(int nCol, T value, bool bOnlySelected = false);
	template <class T> void setColValue(QString strColumnName, T value, bool bOnlySelected = false);
	template <class T> int countColValue(int nCol, T value, bool bOnlySelected = false);
	int countColValue(int nCol, QVariant &value, bool bOnlySelected = false);

	void merge(const DbRecordSet &list, bool bOnlySelected = false, bool bMapByPosition = false);
	void prependList(const DbRecordSet &list);

	int getStoredByteSize();
	void Dump();	// display debug info

	void joinList(DbRecordSet &list, int nColIdx);			//add contents of parameter list to current list, match by nColIdx column
	void removeList(DbRecordSet &list, int nColIdx);		//remove contents of parameter list from current list, match by nColIdx column
	void intersectList(DbRecordSet &list, int nColIdx);		//remove contents from current list not found in parameter list, match by nColIdx column
	
protected:
	void sortAdvanced(QList<DBRowAdvanced> &lstData,SortDataList &lstColsForSort,int nIndex);
	void redefineContents(int nCol, bool bAdded = true);
	

protected:
	QList<DBColumn>		 m_lstColumns;		//list description
	QHash<QString,int>	 m_hshColumnMap;	//list description
	QList<DBRowAdvanced> m_lstData;			//list contents

};

Q_DECLARE_METATYPE(DbRecordSet)


//-----------------------------------------------------------------
//					TEMPLATE FUNCTIONS
//-----------------------------------------------------------------

/*!
	Finds desired value inside one column, selects rows or returns first one

	\param nCol			- target col [0..getColumnCount()-1]
	\param value		- value for search
	\param bFindFirst	- true = returns index of found element or -1 if nothing was found, false=returns number of selected items
	\param bSearchOnSelected	- searches on selected
	\param bDeselectNonMatches	- deselects non matches
	\param bMatchEqual	- if false then searches all !
	\return number of selected items of if search one, number of row, or -1 if not found

*/
template<typename T> int DbRecordSet::find(QString strColumnName, T value, bool bFindFirst /*= false*/, bool bSearchOnSelected /*= false*/, bool bDeselectNonMatches /*= true*/, bool bMatchEqual /*= true*/)
{
	int nColumnIndex = getColumnIdx(strColumnName);
	if(nColumnIndex < 0)
	{
		Q_ASSERT(false); //column does not exist!!!
		return -1;
	}
	return find(nColumnIndex, value, bFindFirst, bSearchOnSelected, bDeselectNonMatches, bMatchEqual);
}

template<typename T> int DbRecordSet::find(int nCol, T value, bool bFindFirst, bool bSearchOnSelected, bool bDeselectNonMatches, bool bMatchEqual)
{
	if(bFindFirst)
	{
		int nSize=getRowCount();
		for(int i=0; i<nSize; i++)
		{
			if(bSearchOnSelected){
				if(!m_lstData[i].bSelected) continue;	//skip if not selected
			}

			if(bMatchEqual)	//match if values are equal
			{
				if(value == getDataRef(i, nCol).value<T>())
					return i;
			}
			else //match if values are NOT equal
			{
				if(value != getDataRef(i, nCol).value<T>())
					return i;
			}
		}
		return -1;
	}
	else
	{
		int nCount = 0; 
		if(bSearchOnSelected)
		{
			//search only the selected items
			//- only deselect non-matches if requested
			int nSize = m_lstData.size();
			for(int nRow=0; nRow<nSize;nRow++)
			{
				if(!m_lstData[nRow].bSelected) continue;	//skip if not selected
				if(bDeselectNonMatches)
					if( (bMatchEqual && value  != getDataRef(nRow, nCol).value<T>()) ||
						!(bMatchEqual || value != getDataRef(nRow, nCol).value<T>()))
					{
							selectRow(nRow, false); //deselect
					}
					else
						nCount++;	//found item is already selected (bSearchOnSelected)
				else
					nCount++;
			}
		}
		else
		{
			//search entire list
			int nSize=getRowCount();
			for(int i=0; i<nSize; i++)
			{
				// !(x1 || x2) || x1 && x2 = result!!!
				if(value  == getDataRef(i, nCol).value<T>())
				{
					if (bMatchEqual)
					{
						m_lstData[i].bSelected=true;
						nCount ++;
					}
					else if (bDeselectNonMatches)
						m_lstData[i].bSelected=false;
				}
				else
				{					
					if (!bMatchEqual)
					{
						m_lstData[i].bSelected=true;
						nCount ++;
					}
					else if (bDeselectNonMatches)
						m_lstData[i].bSelected=false;
				}

				/*
				// !(x1 || x2) || x1 && x2 = result!!!
				if( (bMatchEqual && value  == getDataRef(i, nCol).value<T>()) ||
					!(bMatchEqual || value == getDataRef(i, nCol).value<T>()))
				{
					//selectRow(i, true);			// match found, select it
					m_lstData[i].bSelected=true;
					nCount ++;
				}
				else if (bDeselectNonMatches)
					m_lstData[i].bSelected=false;
					//selectRow(i, false);		// deselect non-match if required
				*/
			}
		}
		return nCount;
	}
}

template <class T> void DbRecordSet::setColValue(int nCol, T value, bool bOnlySelected)
{
	int nSize=getRowCount();
	for(int i=0; i<nSize; i++)
		if(!bOnlySelected || (bOnlySelected && isRowSelected(i)))
			setData(i, nCol, value);
}
template <class T> void DbRecordSet::setColValue(QString strColumnName, T value, bool bOnlySelected)
{
	int nCol=getColumnIdx(strColumnName);
	Q_ASSERT_X(nCol >= 0, "setcolvalue", QString("Unknown column %1").arg(strColumnName).toLocal8Bit().constData());
	if(nCol >= 0){
		int nSize=getRowCount();
		for(int i=0; i<nSize; i++)
			if(!bOnlySelected || (bOnlySelected && isRowSelected(i)))
				setData(i, nCol, value);
	}
}

//count number of occurrences of value T inside col
template <class T> int DbRecordSet::countColValue(int nCol, T value, bool bOnlySelected)
{
	int nCount=0;
	int nSize=getRowCount();
	for(int i=0; i<nSize; i++)
		if(!bOnlySelected || (bOnlySelected && isRowSelected(i)))
			if(value==m_lstData[i].DBRow[nCol].value<T>())
				nCount++;
	
	return nCount;
}

#endif

