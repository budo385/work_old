/*!
\class  Sha256Hash
\brief  Simple class to calculate SHA256 hash for a given string
*/
#ifndef SHA256HASH_H
#define SHA256HASH_H

#include <QString>
#include <QByteArray>

class Sha256Hash
{
public:
    Sha256Hash();
    ~Sha256Hash();

	static QByteArray GetHash(QByteArray &byteData);
};

#endif // SHA256HASH_H
