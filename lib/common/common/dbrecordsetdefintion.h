#ifndef DBRECORDSETDEFINTION_H
#define DBRECORDSETDEFINTION_H

#include <QString>
#include <QList>
#include <QVariant>
#include <QSet>


/*!
	\class DbColumnEx
	\brief Defines one column inside DbRecordSet
	\ingroup Db_Core

	Name of column and data type are mandatory.
	Alias is actually name of column placed in header, must be translated with QObject::tr macro
	TableName specifies from which SQL table, record set column derives
*/
class DbColumnEx
{
public:

	DbColumnEx(QString strName,int nType, bool bIsLongVarChar=false,QString strDesc="");
	DbColumnEx(QString strName,QString strNameAlias,int nType, bool bIsLongVarChar=false,QString strDesc="");
	DbColumnEx(const DbColumnEx &that);
	void operator =(const DbColumnEx &that);

public:
	QString		m_strName;	
	QString		m_strNameAlias;	
	int			m_nType;		
	bool		m_bIsLongVarChar;
	QString		m_strDesc;
};




/*!
	\class DbView
	\brief Defines set of DbColumnEx columns. Used for defining DbRecordSet
	\ingroup Db_Core
*/
class DbView : public QList<DbColumnEx>
{
public:

	DbView(){clear();};			// init view at start
	DbView(const DbView &that);	
	void operator =(const DbView &that);

	int m_nViewID;				///< View ID -> enum ViewID.
	QString m_strSelectSQL;		///< Select statement.
	QString m_strInsertSQL;		///< Insert statement.
	QString m_strUpdateSQL;		///< Update statement.
	QString m_strTables;			///< One or more table names for recordset view
	int m_nSkipFirstColsInsert;	///< Insert map list.
	int m_nSkipFirstColsUpdate;	///< Update map list.
	int m_nSkipLastCols;		///< number of columns to skip from end (for dynamic add-on columns) on read ops
	int m_nSkipLastColsWrite;	///< number of columns to skip from end (for dynamic add-on columns) on write ops
	QSet<int> m_LstIsLongVarcharField;				//is column VARCHAR blob which maps to STRING (only for FB)

	void clear()
	{
		QList<DbColumnEx>::clear();m_nSkipFirstColsInsert=0;m_nSkipFirstColsUpdate=0;
		m_strSelectSQL="";m_strInsertSQL="";m_strUpdateSQL="";m_strTables="";m_nSkipLastCols=0;m_nSkipLastColsWrite=0;
		m_LstIsLongVarcharField.clear();
	}
};



#endif


