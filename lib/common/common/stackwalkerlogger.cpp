#include "stackwalkerlogger.h"
#include "statuscodeset.h"
#include "threadid.h"
#include "loggerabstract.h"

QByteArray StackWalkerLogger::GetBacktrace()
{
#ifdef _WIN32
	ShowCallstack();
#endif
	return m_buffer;
}

void StackWalkerLogger::DumpCallStack2Log(bool bIsCrashSignalDetected)
{
	QByteArray bufCallStack=GetBacktrace();
	if (bIsCrashSignalDetected)
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("Application crash signal handler is dumping call stack for thread %1").arg(ThreadIdentificator::GetCurrentThreadID()));
	else
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("Dumping call stack for thread %1").arg(ThreadIdentificator::GetCurrentThreadID()));

	ApplicationLogger::logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,bufCallStack);
}


#ifdef _WIN32
void StackWalkerLogger::OnOutput(LPCSTR szText)
{
	m_buffer.append(szText);
}

void StackWalkerLogger::OnLoadModule(LPCSTR img, LPCSTR mod, DWORD64 baseAddr, DWORD size, DWORD result, LPCSTR symType, LPCSTR pdbName, ULONGLONG fileVersion)
{
	Q_UNUSED(img);
	Q_UNUSED(mod);
	Q_UNUSED(baseAddr);
	Q_UNUSED(size);
	Q_UNUSED(result);
	Q_UNUSED(symType);
	Q_UNUSED(pdbName);
	Q_UNUSED(fileVersion);

	//overriden to skip writing module info, not interested for now
}


#endif