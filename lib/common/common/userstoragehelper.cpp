#include "userstoragehelper.h"
#include "common/common/authenticator.h"
#include "trans/trans/config_trans.h"
#include <QUrl>
#include <QFileInfo>

QString UserStorageHelper::GenerateAuthQueryString(QString strUserName,QByteArray bytePassword,QString strParentSession)
{
	QByteArray clientNonce,clientAuthToken; 
	Authenticator::GenerateAuthenticationToken(strUserName,bytePassword,clientNonce,clientAuthToken);

	QString strAuthQueryString;
	strAuthQueryString=QString("/")+DOMAIN_HTTP_USER_STORAGE+"?";
	strAuthQueryString+="user="+strUserName+"&";
	strAuthQueryString+="token="+QUrl::toPercentEncoding(clientAuthToken.toBase64())+"&";
	strAuthQueryString+="nonce="+QUrl::toPercentEncoding(clientNonce.toBase64())+"&";
	strAuthQueryString+="parent_session="+QUrl::toPercentEncoding(strParentSession); //will test if this session exists and running..

	return strAuthQueryString;
}

QString UserStorageHelper::GenerateRequestQueryString(QString strFileName,QString strMethod,QString strSubDir)
{
	QFileInfo info(strFileName);

	QString strPath=QString("/")+DOMAIN_HTTP_USER_STORAGE;
	if (!strSubDir.isEmpty())
	{
		strPath += "/"+QUrl::toPercentEncoding(strSubDir);
	}
	strPath += "/"+QUrl::toPercentEncoding(info.fileName());
	//strPath += "/"+QUrl::toPercentEncoding(info.path());

	if (strMethod!="download")
	{
		strPath+="&method="+strMethod;
	}

	//strPath = QDir::cleanPath(strPath);
	return strPath;

	/*
	QString strPath=strAuthString;
	strPath+="&method="+strMethod;
	strPath+="&filename="+QUrl::toPercentEncoding(info.fileName());
	if (!strDirName.isEmpty())
		strPath+="&dir="+QUrl::toPercentEncoding(strDirName);
	return strPath;
	*/
}

QString UserStorageHelper::GenerateAuthCookie(QString strSession)
{
	return "web_session = "+strSession;
}