#ifndef LOGGER_H
#define LOGGER_H


#include <QList>
#include <QReadWriteLock>
#include "common/common/loggerabstract.h"
#include <QObject>
#include "statuscodeset.h"

/*!
	\class Logger
	\brief Logger Class
	\ingroup Bus_Core

	Global object for registering msg to file, db ot syslog.
	App. server only should use logger.
	Warning: logging can be slow operation, only used rarely or to report serius errors, intruder alerts, debug info, etc..
*/


class Logger : public LoggerAbstract
{
	Q_OBJECT

public:
		
	Logger();
	~Logger();

	void	logMessage (int nMsgType,int nMsgID, QString strMsgArguments="" ); 

	void	SetLogLevel(int nLevel);
	void	SetMaxLogSize(int nMaxLogSize);
	void	SetMemoryBufferSize(int nBufferSize);
	int		GetLogLevel();
	int		GetMaxLogSize();
	int		GetMemoryBufferSize();
	qint64	GetCurrentLogSize();
	
	bool	GetLog(QByteArray &logData,qint64 &nCurrentPosition,qint64 &nLogFileSizeOnStartOfChunkRead,int nChunkSize=1048576);
	bool	EnableFileLogging(QString strFullFilePath,int nBufferSize=64000,int nMaxLogSize=0);
	void	EnableConsoleLogging(){m_bConsoleLogger=true;}
	void	FlushFileLog();
	QString GetFileLogPath();

signals:
	void	MessageLogged(int nMsgType,int nMsgID, QString strMsgArguments);

private:
	bool	IsMessageForLog(int nMsgType,int nMsgID, QString strMsgArguments) const;

	LoggerAbstract *m_pFileLogger;
	//LoggerAbstract *m_pSysLogger;
	//LoggerAbstract *m_pDbLogger;
	QReadWriteLock	m_lock;
	bool			m_bConsoleLogger;
	int				m_nLogLevel;
};
#endif //LOGGER_H
