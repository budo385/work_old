#include "cliententitycache.h"
#include "common/common/datahelper.h"
#include "common/common/entity_id_collection.h"

/*
	Set default cache expire time to 1hr.
	Note: even if cache data expires, data will be preserved in the cache, until outside observer purge or reload data!

	\param nExpireTime	- time (seconds) in which cache will expire (default = 1hr=60*60)
*/
ClientEntityCache::ClientEntityCache()
:m_pGlobalMsgDispatcher(NULL)
{
}

ClientEntityCache::~ClientEntityCache()
{

}

void ClientEntityCache::SetGlobalMsgDispatcher(ObsrPtrn_Subject* pGlobalMsgDispatcher, int nAddSignalId, int nEditSignalId, int nRemoveSignalId)
{
	m_pGlobalMsgDispatcher=pGlobalMsgDispatcher;
	m_nAddSignalId=nAddSignalId;
	m_nEditSignalId=nEditSignalId;
	m_nRemoveSignalId=nRemoveSignalId;

}
/*
	Purges caches, emits CachePurged() signal

	\param nEntityID	- purges cache for given EntityID, if -1, then clear all cached entities
*/
void ClientEntityCache::PurgeCache(int nEntityID)
{

	if(nEntityID==-1)
	{
		m_Cache.clear();
	}	
	else
	{
		if(m_Cache.contains(nEntityID))
		{
			m_Cache.remove(nEntityID);
		}
	}

}


/*
	Returns cache data, if not found in cache return false

	\param nEntityID	- ID of cache data

	\return				- NULL if not exists, if exists modify directly!!

*/
DbRecordSet* ClientEntityCache::GetCache(int nEntityID)
{
	if(!m_Cache.contains(nEntityID)) return NULL;
	return &(m_Cache[nEntityID]);
}




/*
	Sets cache data, if already exists -> please use pointer from GetCache() to avoid destruction

	\param nEntityID				- ID of cache data
	\param Data						- stored data

*/
void ClientEntityCache::SetCache(int nEntityID, DbRecordSet& Data)
{
	if (Data.getColumnCount()>0) //to prevent crash
		m_Cache[nEntityID]=Data; //created new entry in hash or overrides previous...
}



/*
	Modifies cache data, after DB operation, if not exists, adds if add/edit else skip..
	Fires global signal!
	NOTE: assumes that primary key of both lists are at index=0

	\param nEntityID				- ID of cache data
	\param newData					- stored data
	\param nOperation				- operation on cache as defines in DataHelper
	\param pSender					- sender of this message: prevents backfire: that msg do not return to sender

*/
void ClientEntityCache::ModifyCache(int nEntityID, DbRecordSet& newData, int nOperation,ObsrPtrn_Observer *pSender, bool bSkipGlobalNotifySignal)
{
	//if was empty: create:
	if(!m_Cache.contains(nEntityID) && nOperation!=DataHelper::OP_REMOVE)
	{
		DbRecordSet dataCache;
		dataCache.copyDefinition(newData);
		m_Cache[nEntityID]=dataCache;
	}

	//operate on lists: ADD, EDIT, DELETE
	DataHelper::DataOperation(nOperation,&newData,&(m_Cache[nEntityID]));

	//issue 2630: sort organizations by name:
	if (nEntityID==ENTITY_CONTACT_ORGANIZATION)
	{
		(m_Cache[nEntityID]).sort((m_Cache[nEntityID]).getColumnIdx("NAME")); //sort by name
	}

	//if skip, then skip:
	if (bSkipGlobalNotifySignal || !m_pGlobalMsgDispatcher)
		return;

	int nSignal=-1;
	if (nOperation==DataHelper::OP_EDIT)nSignal=m_nEditSignalId;
	if (nOperation==DataHelper::OP_REMOVE)nSignal=m_nRemoveSignalId;
	if (nOperation==DataHelper::OP_ADD)nSignal=m_nAddSignalId;
	Q_ASSERT(nSignal!=-1);

	QVariant varData;
	qVariantSetValue(varData,newData);
	m_pGlobalMsgDispatcher->notifyObservers(nSignal,nEntityID,varData,pSender); 
}


//cache will need reload test after 1800sec=30mins
//reload test means that client will check on server if it needs reload
//in 30min period, client will not access server, cache is probably good
bool ClientEntityCache::IsCacheNeedReload(int nEntityID)
{
	//if was empty: create:
	if(!m_Cache.contains(nEntityID))
	{
		return true;
	}
	else
		return m_CacheReloadTime[nEntityID].addSecs(1800)<QDateTime::currentDateTime();

}

void ClientEntityCache::SetCacheReloadCurrentTime(int nEntityID)
{
	m_CacheReloadTime[nEntityID]=QDateTime::currentDateTime();
}
