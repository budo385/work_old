#include "inifile.h"
#include <algorithm>    //find
#include <QFileInfo>
#include <QDir>
#include <QFile>
#include "common/common/logger.h"

IniFile::IniFile()
{

}

IniFile::~IniFile()
{
}

void IniFile::SetPath(const char *szPath)
{
	m_strFilePath = szPath;
}

void IniFile::SetPath(QString strPath)
{
	m_strFilePath = strPath;
}

void IniFile::Clear()
{
	m_data.clear();
}

bool IniFile::EnsurePathDirExists()
{
	//get dir from file path
	QFileInfo info(m_strFilePath);
	QString strDir  = info.absolutePath();

	//try to create entire path
	QDir dir;
	return dir.mkpath (strDir);
}

bool IniFile::Load()
{
	//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("IniFile::Load 1"));

	Clear();

	//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("IniFile::Load 2"));

	//open INI file for reading
	QFile iniFile(m_strFilePath);
	if (!iniFile.open(QIODevice::ReadOnly|QIODevice::Text))
		return false;

	//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("IniFile::Load 3"));

	std::string	strLine;

	//read from file one line at a time
	bool bDone = false;
	while(!bDone)
	{
		strLine = iniFile.readLine(2000).constData();
		int nLen = strLine.size();
		if(nLen > 0){
			if(strLine.at(nLen-1) == '\r' || strLine.at(nLen-1) == '\n')
				strLine = strLine.substr(0, nLen-1);
			ParseIniLine(strLine);
		}
		bDone = iniFile.atEnd();
	}

	//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("IniFile::Load end"));

	iniFile.close();
	return true;
}

bool IniFile::Save()
{
	if(!EnsurePathDirExists())
		return false;

	//open INI file for writing
	QFile iniFile(m_strFilePath);
	if (!iniFile.open(QIODevice::WriteOnly|QIODevice::Text))
		return false;

	char szBuffer[4000];
	size_t nSections = m_data.size();
	for(size_t i=0; i<nSections; i++)
	{
		//write line with section name
		sprintf(szBuffer, "[%s]\n", m_data[i].m_name.c_str());
		iniFile.write(szBuffer, strlen(szBuffer));

		size_t nEntries = m_data[i].m_entries.size();
		for(size_t j=0; j<nEntries; j++)
		{
			//write "key = value"
			sprintf(szBuffer, "%s=%s\n",
				m_data[i].m_entries[j].m_name.c_str(),
				m_data[i].m_entries[j].m_value.c_str());

			iniFile.write(szBuffer, strlen(szBuffer));
		}

		iniFile.write("\n", 1);
	}

	iniFile.close();
	return true;
}

void IniFile::ParseIniLines(std::string &strLines)
{
	int nPos = strLines.find("\n");
	while(nPos >= 0)
	{
		std::string strLine = strLines.substr(0, nPos);
		strLines = strLines.substr(nPos+1);
		ParseIniLine(strLine);

		nPos = strLines.find("\n");
	}

	if(strLines.size()>0)
		ParseIniLine(strLines);
}

void IniFile::ParseIniLine(std::string &strLine)
{
	if(strLine.empty())
		return;

	//is that a section in the buffer
	if('[' == strLine[0])
	{
		size_t nPos = strLine.find_first_of(']');
		if(nPos != std::string::npos)
		{
			IniSection section;
			section.m_name = strLine.substr(1, nPos-1);

			//add new section to the end of storage
			m_data.push_back(section);
		}
	}
	else
	{
		//else is the line "key=value"
		size_t nPos = strLine.find_first_of('=');
		if(nPos != std::string::npos && nPos > 0)    //there must be space for key name
		{
			//add new key to the last section in the storage
			IniEntry entry;
			entry.m_name  = strLine.substr(0, nPos);
			entry.m_value = strLine.substr(nPos+1, strLine.size()-nPos-1);

			size_t size = m_data.size();
			if(size > 0)
				m_data[size-1].m_entries.push_back(entry);
		}
	}
}

bool IniFile::FindSection(const char *szSection, IniSectionIterator &ItRes)
{
	IniSection section;
	section.m_name = szSection;

	ItRes = std::find(m_data.begin(), m_data.end(), section);
	return (ItRes != m_data.end());
}

bool IniFile::FindEntry(const char *szSection, const char *szKey, IniEntryIterator &ItRes)
{
	IniSectionIterator ItSec;
	if(FindSection(szSection, ItSec)) //section exists
	{
		IniEntry entry;
		entry.m_name = szKey;

		ItRes = std::find(ItSec->m_entries.begin(), ItSec->m_entries.end(), entry);
		return (ItRes != ItSec->m_entries.end());
	}

	return false;
}

bool IniFile::SectionExists(const char *szSection)
{
	IniSectionIterator It;
	return FindSection(szSection, It);
}

bool IniFile::SectionExists(QString strSection)
{
	return SectionExists(strSection.toUtf8().constData());
}

bool IniFile::EntryExists(const char *szSection, const char *szKey)
{
	IniEntryIterator It;
	return FindEntry(szSection, szKey, It);
}

bool IniFile::EntryExists(QString strSection, QString strKey)
{
	return EntryExists(strSection.toUtf8().constData(), strKey.toUtf8().constData());
}

void IniFile::AddSection(const char *szSection)
{
	IniSectionIterator It;
	if(!FindSection(szSection, It))
	{
		IniSection section;
		section.m_name = szSection;
		m_data.push_back(section);
	}
	//section already exists
}

void IniFile::AddSection(QString strSection)
{
	AddSection(strSection.toUtf8().constData());
}

bool IniFile::RemoveSection(const char *szSection)
{
	IniSectionIterator It;
	if(FindSection(szSection, It))
		m_data.erase(It);

	return true;
}

bool IniFile::RemoveSection(QString strSection)
{
	return RemoveSection(strSection.toUtf8().constData());
}

bool IniFile::GetValue(const char *szSection, const char *szKey, bool &nValue, bool nDefault)
{
	IniEntryIterator It;
	if(FindEntry(szSection, szKey, It)){
		nValue = (atoi(It->m_value.c_str()) > 0) ? true : false;
		return true;
	}

	nValue = nDefault;
	return false;
}
	
bool IniFile::GetValue(QString strSection, QString strKey, bool &nValue, bool nDefault)
{
	return GetValue(strSection.toUtf8().constData(), strKey.toUtf8().constData(), nValue, nDefault);
}

bool IniFile::GetValue(const char *szSection, const char *szKey, int  &nValue, int nDefault)
{
	IniEntryIterator It;
	if(FindEntry(szSection, szKey, It)){
		nValue = atoi(It->m_value.c_str());
		return true;
	}

	nValue = nDefault;
	return false;
}

bool IniFile::GetValue(QString strSection, QString strKey, int &nValue, int nDefault)
{
	return GetValue(strSection.toUtf8().constData(), strKey.toUtf8().constData(), nValue, nDefault);
}

bool IniFile::GetValue(const char *szSection, const char *szKey, std::string &strValue, const char *szDefault)
{
	IniEntryIterator It;
	if(FindEntry(szSection, szKey, It)){
		strValue = It->m_value;
		return true;
	}

	strValue = szDefault;
	return false;
}

bool IniFile::GetValue(QString strSection, QString strKey, QString &strValue, QString strDefault)
{
	std::string strVal;
	bool bRes = GetValue(strSection.toUtf8().constData(), strKey.toUtf8().constData(), strVal, strDefault.toUtf8().constData());
	strValue = QString::fromUtf8(strVal.c_str());
	return bRes;
}

void IniFile::SetValue(const char *szSection, const char *szKey, const bool nValue)
{
	SetValue(szSection, szKey, (nValue) ? "1" : "0");
}

void IniFile::SetValue(QString strSection, QString strKey, const bool nValue)
{
	SetValue(strSection.toUtf8().constData(), strKey.toUtf8().constData(), nValue);
}

void IniFile::SetValue(const char *szSection, const char *szKey, const int nValue)
{
	char szBuffer[20];
	sprintf(szBuffer, "%d", nValue);	// convert int to string
	SetValue(szSection, szKey, szBuffer);
}

void IniFile::SetValue(QString strSection, QString strKey, const int nValue)
{
	SetValue(strSection.toUtf8().constData(), strKey.toUtf8().constData(), nValue);
}

void IniFile::SetValue(const char *szSection, const char *szKey, const char *szValue)
{
	IniEntryIterator It;
	if(FindEntry(szSection, szKey, It)){
		It->m_value = szValue;    //update existing key
		return;
	}

	AddSection(szSection);   //ensure section exists

	IniSectionIterator ItSec;
	if(FindSection(szSection, ItSec))
	{
		IniEntry entry;
		entry.m_name  = szKey;
		entry.m_value = szValue;

		ItSec->m_entries.push_back(entry);
	}
}

void IniFile::SetValue(QString strSection, QString strKey, QString strValue)
{
	SetValue(strSection.toUtf8().constData(), strKey.toUtf8().constData(), strValue.toUtf8().constData());
}
