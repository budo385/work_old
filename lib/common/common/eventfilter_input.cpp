#include "eventfilter_input.h"



bool EventFilter_Input::eventFilter(QObject *obj, QEvent *event)
{
	if (obj->objectName()!=m_strObjectAllowed)
		return true;

	//only pass input events: key or mouse click on our target allowed object
	if (event->type() == QEvent::KeyPress || event->type() ==QEvent::MouseButtonPress) 
	{
		return QObject::eventFilter(obj, event);
	} 
	else 
	{
		return true;
	}
} 