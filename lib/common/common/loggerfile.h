
#ifndef LOGGERFILE_H
#define LOGGERFILE_H
#include <QFile>
#include <QTextStream>
#include "common/common/loggerabstract.h"
#include "common/common/status.h"
#include "common/common/statuscodeset.h"
#include <QDate>


/*!
	\class LoggerFile
	\brief File Logger Class
	\ingroup Bus_Core

	Class for logging into file
*/

class LoggerFile : public LoggerAbstract
{
public:
	LoggerFile();
	~LoggerFile();

	void	logMessage (int type,int nMsgID, QString strMsgArguments="");
	
	bool	Initialize(const QString &Path,int nBufferSize=64 /*Kb*/,int nMaxLogSize=0 /*Mb*/);
	void	FlushLog();
	void	SetMaxLogSize(int nMaxLogSize);
	void	SetMemoryBufferSize(int nBufferSize);
	
	QString		GetFilePath(){return m_szLogFilePath;};
	int			GetMaxLogSize(){return m_nMaxLogSize/1048576;}
	int			GetMemoryBufferSize(){return m_nBufferSizeForFileLogger/1024;}
	qint64		GetCurrentLogSize();
	bool		GetLog(QByteArray &logData,qint64 &nCurrentPosition, qint64 &nLogFileSizeOnStartOfChunkRead,int nChunkSize=1048576);

private:
	void			CheckForMaxSize();
	QString			m_szLogFilePath;
	QFile			m_fileLogFile;
	bool			m_bFileOpen;
	int				m_nBufferSizeForFileLogger;
	QByteArray		m_byteBuffer;
	qint64			m_nMaxLogSize;
};
#endif // LOGGERFILE_H
