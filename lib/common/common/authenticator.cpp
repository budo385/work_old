#include "authenticator.h"
#include <QTime>
#include <QVariant>
#include <QStringList>
#include <QFileInfo>
#include <QCoreApplication>

#include <stdlib.h>
#include <time.h>
#include "rijndael.h"
#include "sha2.h"
#include "config.h"

/*!
	Generates radnom client nonce string and authtoken as: SHA256(username:password:cnonce)
	\param ClientNonce Client nonces string, send to server as key to decrpyt auth token
	\param AuthToken SHA256(username:password:cnonce)
*/
void Authenticator::GenerateAuthenticationToken(QByteArray& ClientNonce,QByteArray& AuthToken){

	Sha256Hash Hasher;

	//create cnonce as username+datetime:
	QByteArray strCnonce=GenerateRandomSequence(m_strUserName.toUtf8());
	ClientNonce=Hasher.GetHash(strCnonce);

	//create auth token:
	QByteArray bytePassword=GeneratePassword(m_strUserName,m_strPassword); //hasher:user+pass
	QByteArray byteAuthToken=m_strUserName.toUtf8()+":"+bytePassword+":"+ClientNonce; //user+pass+random conce
	AuthToken=Hasher.GetHash(byteAuthToken);
}

void Authenticator::GenerateAuthenticationToken(QString strUserName,QString strPassword, QByteArray& ClientNonce,QByteArray& AuthToken)
{
	Sha256Hash Hasher;
	QByteArray strCnonce=GenerateRandomSequence(strUserName.toUtf8());
	ClientNonce=Hasher.GetHash(strCnonce);

	QByteArray bytePassword=GeneratePassword(strUserName,strPassword); //hasher:user+pass
	QByteArray byteAuthToken=strUserName.toUtf8()+":"+bytePassword+":"+ClientNonce; //user+pass+random conce
	AuthToken=Hasher.GetHash(byteAuthToken);
}

void Authenticator::GenerateAuthenticationToken(QString strUserName,QByteArray bytePassword, QByteArray& ClientNonce,QByteArray& AuthToken)
{
	Sha256Hash Hasher;
	QByteArray strCnonce=GenerateRandomSequence(strUserName.toUtf8());
	ClientNonce=Hasher.GetHash(strCnonce);

	QByteArray byteAuthToken=strUserName.toUtf8()+":"+bytePassword+":"+ClientNonce; //user+pass+random conce
	AuthToken=Hasher.GetHash(byteAuthToken);
}


/*!
	Compares clientAuthToken with genereated based on cnonce, username and password
	\param byteClientNonce		- Client nonces string, send to server as key to decrpyt auth token
	\param byteClientAuthToken  - Client auth token (send instead of password)
	\param strUserName			- username
	\param strPassword			- password
*/
bool Authenticator::Authenticate(QByteArray byteClientNonce,QByteArray byteClientAuthToken, QString strUserName,QByteArray bytePassword)
{
	Sha256Hash Hasher;
	QByteArray serverAuthToken;

	//create auth token:
	QByteArray byteAuthToken=strUserName.toUtf8()+":"+bytePassword+":"+byteClientNonce;
	serverAuthToken=Hasher.GetHash(byteAuthToken);

	//compare to incoming:
	if(serverAuthToken==byteClientAuthToken)
		return true;
	else
		return false;
}
/*!
	Compares clientAuthToken with genereated based on cnonce, username and password
	\param byteClientNonce		- nonce string as is sent from server
	\param byteClientAuthToken  - Client auth token: hsh(user+":"+pass+":"+":"+nonce) (every hsh convert to utf and hex)
	\param strUserName			- username
	\param bytePassword			- stored pass in database as: user.toLatin1+":"+pass.toLatin1 (Latin1???)
*/
bool Authenticator::AuthenticateFromWebService(QString strNonce,QString strAuthToken, QString strUserName,QByteArray bytePassword)
{
	Sha256Hash Hasher;

	//create auth token:
	QByteArray byteAuthToken=strUserName.toUtf8()+":"+bytePassword.toHex()+":"+strNonce.toUtf8();
	QString serverAuthToken=QString(Hasher.GetHash(byteAuthToken).toHex()).toUtf8();

	//compare to incoming:
	if(serverAuthToken==strAuthToken)
		return true;
	else
		return false;
}


QString Authenticator::GenerateAuthenticationTokenForWebService(QString strUserName,QString strPassword, QString strServerNonce)
{
	Sha256Hash Hasher;
	QByteArray bytePassword=strUserName.toUtf8()+":"+strPassword.toUtf8();

	bytePassword=Hasher.GetHash(bytePassword).toHex(); //hasher:user+pass
	QByteArray byteAuthToken=strUserName.toUtf8()+":"+bytePassword+":"+strServerNonce.toUtf8(); 
	QByteArray token=Hasher.GetHash(byteAuthToken);

	return QString(token.toHex()).toUtf8();

}
/*!
	Generates hashed password for storage into DB

	\param strUserName			- username
	\param strPassword			- password
	\return						- hashed password with SALT :)
*/
QByteArray Authenticator::GeneratePassword(QString strUserName,QString strPassword)
{
	Sha256Hash Hasher;
	QByteArray bytePassword=strUserName.toUtf8()+":"+strPassword.toUtf8();
	//QByteArray bytePassword=strUserName.toLatin1()+":"+strPassword.toLatin1();
	return Hasher.GetHash(bytePassword);
}

/*!
	Generates random sequence based on current time, not definable length
	
	\param						- add more random data at start of sequence, lets make thing better
	\return						- random bytes
*/
QByteArray Authenticator::GenerateRandomSequence(QByteArray addSalt)
{
	return addSalt+(QTime::currentTime().toString("hhmmsszzz")+QDateTime::currentDateTime().toString()).toUtf8();
}



/*!
	Generates random password 

	\param		nLength			- password legth
	\return						- random bytes
*/
QByteArray Authenticator::GenerateRandomPassword(int nLength, bool bNoNumbers)
{
	// The list of possible password characters
	const char *sChars="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

	if (bNoNumbers)
		sChars+=10;

	int iCharCount = strlen(sChars);
	
	// Seed the random number generator with the time
	srand(QTime::currentTime().toString("hhmmsszzz").toULong());

	// Throw away the first two.
	rand(); rand();

	QString tmp;

	// For each character
	for (int I = 0; I < nLength; I++)
	{
		// Pick a random character
		int iChar = rand() * iCharCount / RAND_MAX;

		// Display the character
		tmp.append(sChars[iChar]);
	}

	return tmp.toLatin1();

}



/*!
	Caches session parameters
	\param strUserName username
	\param strPassword password
	\param strModuleCode		client module code
	\param strMLIID				MLIID
*/
void Authenticator::SetSessionParameters(QString strUserName,QString strPassword,QString strModuleCode, QString strMLIID){

	m_strUserName=strUserName;
	m_strPassword=strPassword;
	m_strModuleCode=strModuleCode;
	m_strMLIID=strMLIID;
}

/*!
	Retrieves cached session parameters
	\param strUserName username
	\param strPassword password
	\param strModuleCode		client module code
	\param strMLIID				MLIID
*/
void Authenticator::GetSessionParameters(QString& strUserName,QString& strPassword,QString& strModuleCode, QString& strMLIID){

	strUserName=m_strUserName;
	strPassword=m_strPassword;
	strModuleCode=m_strModuleCode;
	strMLIID=m_strMLIID;
}


/*!
	Clears cached session parameters
*/
void Authenticator::ClearSessionParameters(){

	m_strUserName="";
	m_strPassword="";
	m_strModuleCode="";
	m_strMLIID="";
	m_SessionID="";
}





/*

// Copyright 2002, Ted Felix
// Feel free to use for whatever.
#include <iostream.h>
#include <stdlib.h>
#include <time.h>
#include <windows.h>
{
	char *pDummy;
	// Get the password length from the first argument
	long iLength = strtol(argv[1], &pDummy, 10);

	// Seed the random number generator with the time
	srand(GetTickCount());

	// Throw away the first two.
	rand(); rand();

	// The list of possible password characters
	const char sChars[] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	int iCharCount = strlen(sChars);

	// For each character
	for (int I = 0; I < iLength; I++)
	{
		// Pick a random character
		int iChar = rand() * iCharCount / RAND_MAX;

		// Display the character
		cout << sChars[iChar];
	}
	// Write a newline for neatness
	cout << endl;
	return 0;
}

*/



//create byte array, semicolon separated string (startDemoDate can be null)
bool Authenticator::Demo_CreateKey(QByteArray &key,QString strAppVer, int nDbVersion,QDateTime lastModified, int nChkSum, int nLocked,QDateTime startDemoDate)
{
	key.clear();

	QString strKey;
	strKey+=strAppVer+";";
	strKey+=QVariant(nDbVersion).toString()+";";
	strKey+=lastModified.toString("ddMMyyyy-hhmm")+";";//not used!!!
	strKey+=QVariant(nChkSum).toString()+";";
	strKey+=QVariant(nLocked).toString()+";";
	if (startDemoDate.isNull())
		strKey+=";";
	else
		strKey+=startDemoDate.toString("ddMMyyyy-hhmm")+";";


	//use MASTERPASSWORD AS KEY:
	unsigned char szKey[32];
	sha256_ctx ctx;
	sha256_begin(&ctx);
	sha256_hash((const unsigned char *)QString(MASTER_PASSWORD).toLatin1().constData(),QString(MASTER_PASSWORD).size(), &ctx);
	sha256_end((unsigned char *)szKey, &ctx);

	//after header start writing encrypted contents
	Rijndael crypt;
	crypt.init(Rijndael::CBC, Rijndael::Encrypt, szKey, Rijndael::Key32Bytes);

	//NOTE: buffer must be multiple of 16 bytes (rijndael uses 16bytes=128bit blocks)
	const int BUFF_SIZE = 10*1024;
	//TOFIX: const int bsize = BUFF_SIZE*8;		// size in bits
	char szInBuf[BUFF_SIZE];
	char szOutBuf[BUFF_SIZE+16];


	//write string to file in encrypted blocks
	int nSize = strKey.toLatin1().size();
	while(nSize > 0)
	{
		int nChunk = BUFF_SIZE;
		if(nSize < BUFF_SIZE)
			nChunk = nSize;

		memcpy(szInBuf, strKey.toLatin1().constData(), nChunk);

		//encrypt content block
		int len = 0;
		if(nSize < BUFF_SIZE)
			len = crypt.padEncrypt((const UINT8*)szInBuf, nChunk, (UINT8*)szOutBuf);
		else{
			len = crypt.blockEncrypt((const UINT8*)szInBuf, 8*nChunk, (UINT8*)szOutBuf);
			len = len/8;
		}

		if(len > 0)
		{
			key.append(QByteArray::fromRawData(szOutBuf,len));
		}
		else
			break;

		nSize -= nChunk;
		strKey = strKey.mid(nChunk);
	}

	return true;
}


bool Authenticator::Demo_ParseKey(QByteArray &key,QString &strAppVer, int &nDbVersion,QDateTime &lastModified,int &nChkSum, int &nLocked,QDateTime &startDemoDate)
{
	//key.clear();

	//use MASTERPASSWORD AS KEY:
	unsigned char szKey[32];
	sha256_ctx ctx;
	sha256_begin(&ctx);
	sha256_hash((const unsigned char *)QString(MASTER_PASSWORD).toLatin1().constData(),QString(MASTER_PASSWORD).size(), &ctx);
	sha256_end((unsigned char *)szKey, &ctx);


	//get string from key:
	Rijndael crypt;
	crypt.init(Rijndael::CBC, Rijndael::Decrypt, szKey, Rijndael::Key32Bytes);

	//NOTE: buffer must be multiple of 16 bytes (rijndael uses 16bytes=128bit blocks)
	const int BUFF_SIZE = 10*1024;
	//TOFIX: const int bsize = BUFF_SIZE*8;		// size in bits
	char szInBuf[BUFF_SIZE];
	char szOutBuf[BUFF_SIZE+16+1];		// one byte is for \0


	
	QString strKey;
	//read from file in blocks
	while(1)
	{
		int nToRead = key.size(); 
		if (nToRead==0)
			break;

		int nReadReq = BUFF_SIZE;
		if(nToRead < BUFF_SIZE)
			nReadReq = nToRead;

		memcpy(szInBuf, key, nReadReq);

		//calculate how much have we read
		if(key.size() > 0)
		{
			//decrypt read contents
			int len = crypt.blockDecrypt((const UINT8*)szInBuf, 8*nReadReq, (UINT8*)szOutBuf);
			if(len > 0)
			{
				int nBytes = len/8;
				strKey=QString::fromLatin1(szOutBuf,nBytes);
				strKey.append('\0');
			}
			key.chop(nReadReq);
		}
		else
			break;
	}


	int nInd=strKey.lastIndexOf(";");
	if (nInd==-1)
		return false;
	strKey=strKey.left(nInd+1);

	//check string:
	QStringList lstItems=strKey.split(";");
	if (lstItems.size()!=7)
		return false;

	strAppVer=lstItems.at(0);
	bool bOK;
	nDbVersion=lstItems.at(1).toInt(&bOK);
	if (!bOK) return false;
	lastModified=QDateTime::fromString(lstItems.at(2),"ddMMyyyy-hhmm"); //not used!!!
	//if (!lastModified.isValid())
	//	return false;
	nChkSum=lstItems.at(3).toInt(&bOK);
	if (!bOK) return false;
	nLocked=lstItems.at(4).toInt(&bOK);
	if (!bOK) return false;

	//demo start date: can be invalid
	if (lstItems.at(5).isEmpty())
		startDemoDate=QVariant(QVariant::DateTime).toDateTime();
	else
		startDemoDate=QDateTime::fromString(lstItems.at(5),"ddMMyyyy-hhmm");


	return true;
}


//generates key: locked=0, when start demo or when organize DB
bool Authenticator::Demo_ResetKey(QByteArray &key,QDateTime startDemoDate,QString strAppVerLatest, int nDbVersionLatest)
{
	key.clear();
	QDateTime lastModif;
	int nSize=0;
	//calc chk sum & last modif:
	QFileInfo sokrates(QCoreApplication::applicationDirPath()+"/sokrates.exe");
	if (sokrates.exists())
	{
		lastModif=sokrates.lastModified();
		nSize=sokrates.size();
	}
	QFileInfo app(QCoreApplication::applicationDirPath()+"/appserver.exe");
	if (app.exists())
	{
		lastModif=app.lastModified();
		nSize=app.size();
	}

	if(!Demo_CreateKey(key,strAppVerLatest,nDbVersionLatest,lastModif,nSize,0,startDemoDate))
	{
		return false;
	}
	//if (nSize==0)
	//{
	//	return false;
	//}

	return true;
}


//check & updates: if not OK, locks
bool Authenticator::Demo_UpdateDbVersionKey(QByteArray &key, int nCurrentDbVersion,QString strAppVerLatest, int nDbVersionLatest)
{
	QDateTime lastModif,datStart;
	int nSize,nLocked;
	int nDbVersion;
	QString strAppVersion;
	if(!Demo_ParseKey(key,strAppVersion,nDbVersion,lastModif,nSize,nLocked,datStart)) //ignore if locked!!!<-template db will be reorganized!
	{
		return Demo_CreateKey(key,strAppVerLatest,nDbVersionLatest,lastModif,nSize,1,datStart);	//lock
	}

	//BT: only update, do not check..who knows what is...
	//if(nDbVersion!=nCurrentDbVersion && nDbVersion>24)
	//{
	//	return Demo_CreateKey(key,strAppVerLatest,nDbVersionLatest,lastModif,nSize,1,datStart); //lock
	//}


	//update DB:
	return Demo_CreateKey(key,strAppVersion,nDbVersionLatest,lastModif,nSize,nLocked,datStart); //save
}