#ifndef CSVIMPORTFILE_H
#define CSVIMPORTFILE_H

#include "common/common/status.h"
#include "common/common/dbrecordset.h"
//#include <QProgressDialog>

typedef bool (*FN_PROGRESS)(unsigned long nUserData);

class CsvImportFile : public QObject
{
	Q_OBJECT

public:
    CsvImportFile();
    ~CsvImportFile();

	void Load(Status &status, QString strFile, DbRecordSet &list, const QStringList &lstFormatLines, const QStringList &lstUtf8FormatLines, bool bHdrOptional = false, FN_PROGRESS pProgress /*QProgressDialog *pDlg*/ = NULL, unsigned long nUserData = 0);
	void LoadFromBlob(Status &status, QByteArray &data, DbRecordSet &list, const QStringList &lstFormatLines, const QStringList &lstUtf8FormatLines, bool bHdrOptional = false, FN_PROGRESS pProgress /*QProgressDialog *pDlg*/ = NULL, unsigned long nUserData = 0);

	QString GetHeaderLine(){ return m_strHdr; };

protected:
	void ParseChunk(QString &strChunk);
	void AddFieldData(QString &strData);
	bool CheckQuoteEnded();

protected:
	QChar m_cDelimiter;		// field delimiter
	DbRecordSet m_list;		// parse result
	QString m_strHdr;

	//parser state variables
	bool m_bUtf8;
	int  m_nCurRecLine;
	int  m_nCurRecField;
	QString m_strCurField;
};

#endif // CSVIMPORTFILE_H
