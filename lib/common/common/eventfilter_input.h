#ifndef EVENTFILTER_INPUT_H
#define EVENTFILTER_INPUT_H

#include <QObject>
#include <QEvent>
#include <QString>

/*!
	\class  EventFilter_Input
	\ingroup Common
	\brief  Filters all events except input (key stroke or mouse) on object defined by objectName

	Use:
	- installEventFilter to install this filter
*/


class EventFilter_Input : public QObject
{
	Q_OBJECT
	
public:
	void SetTargetObjectName(QString strName){m_strObjectAllowed=strName;};
	
protected:
	bool eventFilter(QObject *obj, QEvent *ev);

private:
	QString m_strObjectAllowed;
	
};

#endif // EVENTFILTER_INPUT_H
