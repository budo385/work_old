#include "zipcompression.h"
#include <zlib.h>
//#ifndef WINCE
#include <QStringList>
#include <QDebug>
#include <QTextCodec>
#include <QFile>
//#include "quazip.h"
//#include "quazipfile.h"
#include <QFileInfo>
#include <QDir>
#include <QCoreApplication>
#include <QProcess>

#define CHUNK_STREAM_DEFLATE 65535
#define CHUNK_FILE_ZIP 1048576


bool ZipCompression::ZipFiles(QString strZipArchive, QStringList &lstInFiles) //, QStringList &lstEntryNames)
{

#ifdef __APPLE__
	if (lstInFiles.size()==0 || strZipArchive.isEmpty())
	{
		return false;
	}
#else
	if (lstInFiles.size()==0 || strZipArchive.isEmpty() || !QFile::exists(QCoreApplication::applicationDirPath()+"/zip.exe"))
	{
		return false;
	}
#endif

	strZipArchive=QDir::toNativeSeparators(strZipArchive);
	
	QStringList lstArgs;
	lstArgs<<"-j"; //ignore source directories
	lstArgs<<strZipArchive;
	int nSize=lstInFiles.size();
	for(int i=0;i<nSize;i++)
	{
		QString strFile=QDir::toNativeSeparators(lstInFiles.at(0));
		lstArgs<<strFile;
	}

	QProcess proc_zip;
	proc_zip.start("zip",lstArgs);
	if (!proc_zip.waitForFinished(-1)) //wait indefently
	{		
		return false;
	}
	int nExitCode=proc_zip.exitCode();
	QByteArray errContent=proc_zip.readAllStandardError();
	if (errContent.size()!=0)
	{
		qDebug()<<errContent.left(4095);
		return false;
	}
	else if (nExitCode!=0)
	{
		return false;
	}

	return true;



/*

	QuaZip zip(strZipArchive);
	if(!zip.open(QuaZip::mdCreate)) 
		return false;

	int nSize=lstInFiles.size();
	for(int i=0;i<nSize;i++)
	{
		QFile inFile(lstInFiles[i]);
		QFileInfo inFileInfo(lstInFiles[i]);
		QuaZipFile outFile(&zip);
		char c;
		if(!inFile.open(QIODevice::ReadOnly)) 
			return false;
		if(!outFile.open(QIODevice::WriteOnly, QuaZipNewInfo(inFileInfo.fileName(), inFile.fileName())))  //
			return false;

		while(inFile.bytesAvailable()>0)
		{
			QByteArray buffer=inFile.read(CHUNK_FILE_ZIP); //1Mb chunks
			outFile.write(buffer);
		}

		if(outFile.getZipError()!=UNZ_OK)
			return false;
		outFile.close();
		if(outFile.getZipError()!=UNZ_OK) 
			return false;
		inFile.close();
	}
	zip.close();
	if(zip.getZipError()!=0) 
		return false;

	return true;*/

}

bool ZipCompression::Unzip(QString strZipArchive,  QString strDestinationDir,  QStringList &lstOutFiles,bool bOverWriteIfExists)
{
	QString strUnzipPath = "unzip";

#ifdef __APPLE__
	strUnzipPath = "/usr/bin/unzip";
	if (strZipArchive.isEmpty())
	{
		return false;
	}
#else
	if (!QFile::exists(QCoreApplication::applicationDirPath()+"/unzip.exe") || /*strDestinationDir.isEmpty() ||*/ strZipArchive.isEmpty())
	{
		return false;
	}
#endif

	strZipArchive=QDir::toNativeSeparators(strZipArchive);

	//------------------------------------
	//		UNZIP
	//------------------------------------

	QStringList lstArgs;
	strDestinationDir=QDir::toNativeSeparators(strDestinationDir);
	lstArgs.clear();
	lstArgs<<"-o"; //overwrite
	lstArgs<<strZipArchive;
	lstArgs<<"-d";
	lstArgs<<strDestinationDir;

	QProcess proc_zip;
	proc_zip.start(strUnzipPath,lstArgs);
	if (!proc_zip.waitForFinished(-1)) //wait indefently
	{		
		return false;
	}
	int nExitCode=proc_zip.exitCode();
	QByteArray errContent=proc_zip.readAllStandardError();
	if (errContent.size()!=0)
	{
		qDebug()<<errContent.left(4095);
		lstOutFiles<<errContent.left(4095);

		return false;
	}
	else if (nExitCode!=0)
	{
		return false;
	}
	QByteArray strContent=proc_zip.readAllStandardOutput();
	if (strContent.size()>0)
	{

		QStringList lstParts=QString(strContent).split("\n", QString::SkipEmptyParts);
		if (lstParts.size()<2)
		{
			return false;
		}
		lstParts.removeAt(0);
		int nSize=lstParts.size();
		for(int i=0;i<nSize;i++)
		{
			QString strFileInfo=lstParts.at(i);
			int n1=strFileInfo.indexOf(":");
			if (n1<0) return false;
			strFileInfo=strFileInfo.mid(n1+1).trimmed().simplified();
			lstOutFiles<<strFileInfo;
		}


	}

	return true;



	/*
	QDir dir(strDestinationDir);
	if (!dir.exists())
		return false;

	QuaZip zip(strZipArchive);
	if(!zip.open(QuaZip::mdUnzip)) 
		return false;
	//zip.setFileNameCodec("IBM866");
	QuaZipFileInfo info;
	QuaZipFile file(&zip);
	QFile out;
	QString name;
	char c;
	for(bool more=zip.goToFirstFile(); more; more=zip.goToNextFile()) {
		if(!file.open(QIODevice::ReadOnly))
			return false;
		name=file.getActualFileName();
		if(file.getZipError()!=UNZ_OK)
			return false;

		//create file in dest dir, if overwrite remove prev
		out.setFileName(dir.filePath(name));
		if (out.exists() && bOverWriteIfExists)
			out.remove();
		
		// this will fail if "name" contains subdirectories, but we don't mind that
		out.open(QIODevice::WriteOnly);
		while(file.pos()<file.size())
		{
			QByteArray buffer=file.read(CHUNK_FILE_ZIP); //1Mb chunks
			out.write(buffer);
		}
		out.close();
		if(file.getZipError()!=UNZ_OK) 
			return false;
		if(!file.atEnd()) 
			return false;
		file.close();
		if(file.getZipError()!=UNZ_OK) 
			return false;

		lstOutFiles<<out.fileName(); //full?
	}
	zip.close();
	if(zip.getZipError()!=UNZ_OK) 
		return false;

	return true;
	*/
}


//QByteArray ZipCompression::Deflate(const uchar* data, int nbytes, int compressionLevel)
bool ZipCompression::Deflate(const QByteArray &source, QByteArray &target, int compressionLevel) 
{
	target.clear();
	int nBytesToProcess=source.size();

	if (nBytesToProcess == 0)
		return true;
	
	if (!source.constData()) 
	{
		qWarning("Deflate: Data is null");
		return false;
	}

	if (compressionLevel < -1 || compressionLevel > 9)
		compressionLevel = Z_DEFAULT_COMPRESSION;

	int err;
	z_stream strm;

	/* allocate deflate state */
	strm.zalloc = Z_NULL;
	strm.zfree = Z_NULL;
	strm.opaque = Z_NULL;
	err = ::deflateInit(&strm, compressionLevel);
	if (err != Z_OK)
		return false;

	strm.next_in  = (unsigned char *)source.constData();
	strm.avail_in = nBytesToProcess;
	
	QByteArray buffer;
	buffer.resize(CHUNK_STREAM_DEFLATE);
	bool bError=false;
	int nFlag=Z_FINISH;

	do 
	{
		strm.avail_out = CHUNK_STREAM_DEFLATE;
		strm.next_out = (unsigned char *)buffer.constData();

		err = ::deflate(&strm, nFlag);   
		if (err==Z_STREAM_ERROR || err==Z_DATA_ERROR || err==Z_MEM_ERROR || err==Z_VERSION_ERROR)  //other states including buff error are ignored
		{
			bError=true;
			break;
		} 
		Q_ASSERT(strm.avail_out<=CHUNK_STREAM_DEFLATE);
		unsigned int nActualBytes = CHUNK_STREAM_DEFLATE - strm.avail_out;
		Q_ASSERT(nActualBytes<=buffer.size());
		target.append(buffer.constData(),nActualBytes);

	} while (strm.avail_out == 0);

	Q_ASSERT(strm.avail_in == 0);     /* all input will be used */
	Q_ASSERT(err == Z_STREAM_END);    /* stream will be complete */

	::deflateEnd(&strm);

	if (bError)
	{
		target.clear();
		return false;
	}
	
	return true;
}


//QByteArray ZipCompression::Deflate(const uchar* data, int nbytes, int compressionLevel)
bool ZipCompression::Inflate(const QByteArray &source, QByteArray &target) 
{
	target.clear();
	int nBytesToProcess=source.size();

	if (nBytesToProcess == 0)
		return true;

	if (!source.constData()) 
	{
		qWarning("Inflate: Data is null");
		return false;
	}

	int err;
	z_stream strm;

	/* allocate deflate state */
	strm.zalloc = Z_NULL;
	strm.zfree = Z_NULL;
	strm.opaque = Z_NULL;
	err = ::inflateInit(&strm);
	if (err != Z_OK)
		return false;

	strm.next_in  = (unsigned char *)source.constData();
	strm.avail_in = nBytesToProcess;

	QByteArray buffer;
	buffer.resize(CHUNK_STREAM_DEFLATE);
	bool bError=false;
	int nFlag=Z_FINISH;

	do 
	{
		strm.avail_out = CHUNK_STREAM_DEFLATE;
		strm.next_out = (unsigned char *)buffer.constData();

		err = ::inflate(&strm, nFlag);   
		if (err==Z_STREAM_ERROR || err==Z_DATA_ERROR || err==Z_MEM_ERROR || err==Z_VERSION_ERROR || err==Z_NEED_DICT)  //other states including buff error are ignored
		{
			bError=true;
			break;
		}
		Q_ASSERT(strm.avail_out<=CHUNK_STREAM_DEFLATE);
		unsigned int nActualBytes = CHUNK_STREAM_DEFLATE - strm.avail_out;
		Q_ASSERT(nActualBytes<=buffer.size());
		target.append(buffer.constData(),nActualBytes);

	} while (strm.avail_out == 0);
	
	Q_ASSERT(err == Z_STREAM_END);      /* stream will be complete */

	::inflateEnd(&strm);

	if (bError)
	{
		target.clear();
		return false;
	}
	return true;
}

//#endif