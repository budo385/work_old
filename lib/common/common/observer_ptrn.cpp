#include "observer_ptrn.h"

//when observer is destroyed without unregistering from subject, c_lstSubjects is used to unregister subject (see observer destructor)

ObsrPtrn_Subject::ObsrPtrn_Subject()
:m_bBlockSignals(false)
{
	QList<ObsrPtrn_Subject*> &c_lstSubjects=globalInitsubjectList(); //reinit global list
	if (c_lstSubjects.contains(this))
		return;
	c_lstSubjects.append(this);
}

ObsrPtrn_Subject::~ObsrPtrn_Subject()
{
	QList<ObsrPtrn_Subject*> &c_lstSubjects=globalInitsubjectList(); //reinit global list
	c_lstSubjects.removeAll(this);
}


QList<ObsrPtrn_Subject*> &ObsrPtrn_Subject::globalInitsubjectList()
{
	static QList<ObsrPtrn_Subject*> *lst = new QList<ObsrPtrn_Subject*>();		//this is not a leak-> when prog exists, all heap is cleaned (this is global object)
	QList<ObsrPtrn_Subject*>& ref_lst = *lst;
	return ref_lst;
}

void ObsrPtrn_Subject::globalUnregisterObserver(ObsrPtrn_Observer *pObj)
{
	QList<ObsrPtrn_Subject*> &c_lstSubjects=globalInitsubjectList(); //reinit global list
	int nSize=c_lstSubjects.count();
	for(int i=0;i<nSize;++i)
	{
		c_lstSubjects.at(i)->unregisterObserver(pObj);
	}

}


void ObsrPtrn_Subject::registerObserver(ObsrPtrn_Observer *pObj)
{
	if (m_lstObservers.contains(pObj))
		return;
	
	if (!m_bBlockSignals) //if subject is blocked do not modify list->it is used in notifyobserver routine
		m_lstObservers.insert(pObj);
	else
		m_lstObserversAppend.insert(pObj);
}

void ObsrPtrn_Subject::unregisterObserver(ObsrPtrn_Observer *pObj)
{
	m_lstObservers.remove(pObj); //will fail if not exists in list
}

void ObsrPtrn_Subject::unregisterAllObservers()
{
	m_lstObservers.clear();
}


/*!
	Sends message to the all registered observers

	\param nMsgCode			- msg code
	\param nMsgDetails		- msg details - additional integer value for message distinction
	\param value			- msg value, can be empty if not used
	\parama pSender			- if sender is found inside list, message will not be sent to him

*/
void ObsrPtrn_Subject::notifyObservers(int nMsgCode,int nMsgDetails,const QVariant value,ObsrPtrn_Observer const *pSender)
{
	if(m_bBlockSignals) return;

	m_bBlockSignals=true; //disable observer cascading on same subject: inside updateObserver, no global signal can be fired
	QSet<ObsrPtrn_Observer *>::const_iterator It = m_lstObservers.begin();
	while(It != m_lstObservers.end())
	{
		//ObsrPtrn_Observer *pObs=(*It);
		//qDebug()<<pObs;
		if(pSender!=*It)//if not sender
			(*It)->updateObserver(this, nMsgCode,nMsgDetails,value);
		It ++;
	}
	if (m_lstObserversAppend.size()>0) //add newly created observer while was in loop: this prevents firing notifyObserver on newly crated observers
	{
		m_lstObservers.unite(m_lstObserversAppend);
		m_lstObserversAppend.clear();
	}
	m_bBlockSignals=false; //re.enable
}




ObsrPtrn_Observer::~ObsrPtrn_Observer()
{
	ObsrPtrn_Subject::globalUnregisterObserver(this);
}