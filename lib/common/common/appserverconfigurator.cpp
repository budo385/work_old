#include "appserverconfigurator.h"
#include <QProcess>
#include <QSettings>
#include <QFile>
#include <QDir>

#include "common/common/config_version_sc.h"
#include "common/common/datahelper.h"
#include "service/qtservice.h"
#include "common/common/logger.h"

AppServerConfigurator::AppServerConfigurator()
{
	m_strAppSrvPath=QCoreApplication::applicationDirPath();
	m_strAppLogPath=m_strAppSrvPath+"/settings/appserver.log";
}

AppServerConfigurator::~AppServerConfigurator()
{

}

QString AppServerConfigurator::GetServiceName()
{
	if (m_strServiceDescriptor.isEmpty())
		return m_strServiceName;
	else
		return m_strServiceName+" - "+m_strServiceDescriptor;
}



void AppServerConfigurator::Stop(Status &pStatus)
{

	pStatus.setError(0);
	if (!IsServiceRunning())
		return;
	QProcess app;
	app.setWorkingDirectory(m_strAppSrvPath);

	QStringList lstArgs;
	lstArgs<<"-t";
	app.start(m_strAppSrvPath+"/"+m_strAppExeName,lstArgs);
	if (!app.waitForFinished(-1)) //wait forever
	{		
		QByteArray fileContent=app.readAllStandardOutput();
		QByteArray errContent=app.readAllStandardError();
		if (errContent.isEmpty())
			pStatus.setError(1,"Failed to find service: "+GetServiceName()+" Reason: "+fileContent.left(4095));
		else
			pStatus.setError(1,"Failed to find service: "+GetServiceName()+" Reason: "+errContent.left(4095));
		return;
	}
	int nExitCode=app.exitCode();
	QByteArray errContent=app.readAllStandardError();
	QByteArray fileContent=app.readAllStandardOutput();
	if (nExitCode !=0)
	{
		if (errContent.isEmpty())
			pStatus.setError(1,"Failed to stop service: "+GetServiceName()+" Reason: "+fileContent.left(4095));
		else
			pStatus.setError(1,"Failed to stop service: "+GetServiceName()+" Reason: "+errContent.left(4095));
		return;
	}


#ifdef __APPLE__

	QString strCmd="ps -ef";
	app.start(strCmd);
	if (!app.waitForFinished(-1)) //wait forever
	{		
		QByteArray fileContent=app.readAllStandardOutput();
		QByteArray errContent=app.readAllStandardError();
		if (errContent.isEmpty())
			pStatus.setError(1,"Failed to find service: "+GetServiceName()+" Reason: "+fileContent.left(4095));
		else
			pStatus.setError(1,"Failed to find service: "+GetServiceName()+" Reason: "+errContent.left(4095));
		return;
	}
	nExitCode=app.exitCode();
	QString strfileContent=app.readAllStandardOutput();

	//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("Terminate Result: %1").arg(QString(fileContent)));

	QStringList lstLines=strfileContent.split("\n", QString::SkipEmptyParts);

	//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("Terminate size lines: %1").arg(lstLines.size()));

	for (int i=0;i<lstLines.size();i++)
	{
		if (lstLines.at(i).indexOf(GetServiceName())>0)
		{
			//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("Terminate found line ID: %1").arg(QString(lstLines.at(i))));

			QString strTarget=lstLines.at(i).left(12);
			strTarget=strTarget.replace("501","");
			strTarget=strTarget.trimmed();

			QString strProcessID=strTarget;
			//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("Terminate Process ID: %1").arg(strProcessID));
			bool bOK;
			int nTest=strProcessID.toInt(&bOK);
			if (bOK)
			{
				QString strCmdKill="kill -9 "+strProcessID;
				ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("Terminate executed: %1").arg(strCmdKill));
				QProcess::execute(strCmdKill);
			}
		}
	}

#endif

}
void AppServerConfigurator::Start(Status &pStatus)
{
	pStatus.setError(0);
	if (IsServiceRunning())
		return;

	QProcess app;
	app.setWorkingDirectory(m_strAppSrvPath);
	QStringList lstArgs;
	qDebug()<<"Starting service "<<QString(m_strAppExeName);
	app.start(m_strAppSrvPath+"/"+m_strAppExeName,lstArgs);
	if (!app.waitForFinished(-1)) //wait forever
	{		
		QByteArray fileContent=app.readAllStandardOutput();
		QByteArray errContent=app.readAllStandardError();
		if (errContent.isEmpty())
			pStatus.setError(1,"Failed to find service: "+GetServiceName()+" Reason: "+fileContent.left(4095));
		else
			pStatus.setError(1,"Failed to find service: "+GetServiceName()+" Reason: "+errContent.left(4095));
		return;
	}

#ifndef __APPLE__
	int nExitCode=app.exitCode();
	QByteArray errContent=app.readAllStandardError();
	QByteArray fileContent=app.readAllStandardOutput();
	if (nExitCode !=0)
	{
		if (errContent.isEmpty())
			pStatus.setError(1,"Failed to start service: "+GetServiceName()+" Reason: "+fileContent.left(4095));
		else
			pStatus.setError(1,"Failed to start service: "+GetServiceName()+" Reason: "+errContent.left(4095));
		return;
	}
	
#endif
}


bool AppServerConfigurator::IsAutoStart()
{
	QSettings settingsApp("HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentCOntrolSet\\Services\\"+GetServiceName(),	QSettings::NativeFormat);
	int nStart=settingsApp.value("Start",0).toInt();
	if (nStart==2)
		return true;
	else
		return false;
}




QString AppServerConfigurator::LastError()
{
	//parse error log: display to user:
	QString strLastError;
	QFile log(m_strAppLogPath);
	if(log.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		QString strMsgErr=QVariant(StatusCodeSet::ERR_SYSTEM_APP_SRV_FAILED_START).toString();
		QString strMsgErr_2=QVariant(StatusCodeSet::ERR_SYSTEM_JAMES_SRV_FAILED_TO_REGISTER).toString();
		
		int nStartPointer=0;
		qint64 nLogSize=log.size();
		if (nLogSize-10000>0)
			nStartPointer=nLogSize-10000;
		log.seek(nStartPointer);
		QString strContent=log.read(10000);
		//QString strContent=log.readAll();
		QStringList strLines=strContent.split("\n");
		int nSize=strLines.size();
		for(int i=nSize-1;i>=0;--i)
		{
			if (strLines.at(i).indexOf(strMsgErr)>=0)
			{
				QStringList strParts=strLines.at(i).split("\t");
				if (strParts.size()>0)
				{
					strLastError=strParts.at(strParts.size()-1);
					break;
				}
			}
			if (strLines.at(i).indexOf(strMsgErr_2)>=0)
			{
				QStringList strParts=strLines.at(i).split("\t");
				if (strParts.size()>0)
				{
					strLastError=strParts.at(strParts.size()-1);
					break;
				}
			}
		}
		log.close();
	}

	return strLastError;
}
/*
QStringList AppServerConfigurator::GetLastMessages(int nLastMsgCnt=10)
{
	//parse error log: display to user:
	QString strLastError;
	QFile log(m_strAppLogPath);
	if(log.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		QString strMsgErr="Msg("+QVariant(StatusCodeSet::ERR_SYSTEM_APP_SRV_FAILED_START).toString()+")";
		int nStartPointer=0;
		qint64 nSize=log.size();
		if (nSize-10000>0)
			nStartPointer=nSize-10000;
		log.seek(nStartPointer);
		QString strContent=log.read(10000);
		QStringList strLines=strContent.split("\n");
		int nSize=strLines.size();
		int nEndCounter=nSize-nLastMsgCnt;
		if (nEndCounter<0)nEndCounter=0;

		QStringList lstOut;
		
		for(int i=nSize-1;i>=nEndCounter;--i)
		{
			if (strLines.at(i).indexOf(strMsgErr)>=0)
			{
				QStringList strParts=strLines.at(i).split("\t");
				if (strParts.size()>0)
				{
					strLastError=strParts.at(strParts.size()-1);
					break;
				}
			}
		}
		log.close();
	}

	return strLastError;
}
*/



void AppServerConfigurator::InstallService(Status &pStatus, bool bAutoStart)
{
	//start with -i
	pStatus.setError(0);

	QProcess app;
	app.setWorkingDirectory(m_strAppSrvPath);
	QStringList lstArgs;
	lstArgs<<"-i";

	if (!m_strUser.isEmpty() && !m_strPass.isEmpty())
	{
		//lstArgs<<"\""+m_strUser+"\"";
		//lstArgs<<"\""+m_strPass+"\"";
		lstArgs<<m_strUser;
		lstArgs<<m_strPass;
	}

	if (!m_strServiceDescriptor.isEmpty())
		lstArgs<<m_strServiceDescriptor;

#ifndef __APPLE__
	if (bAutoStart)
	{
		lstArgs<<"auto";
	}
#endif


	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,1,QString("InstallService args:"+lstArgs.join("-")));

	//qDebug()<<QString("InstallService args:"+lstArgs.join("-"));
	//qDebug()<<QString("InstallService path:"+m_strAppSrvPath+"/"+m_strAppExeName);

	app.start(m_strAppSrvPath+"/"+m_strAppExeName,lstArgs);
	if (!app.waitForFinished(-1)) //wait forever
	{		
		QByteArray fileContent=app.readAllStandardOutput();
		QByteArray errContent=app.readAllStandardError();
		if (errContent.isEmpty())
			pStatus.setError(1,"Failed to find service: "+GetServiceName()+" Reason: "+fileContent.left(4095));
		else
			pStatus.setError(1,"Failed to find service: "+GetServiceName()+" Reason: "+errContent.left(4095));
		return;
	}
	int nExitCode=app.exitCode();
	QByteArray errContent=app.readAllStandardError();
	QByteArray fileContent=app.readAllStandardOutput();
	if (nExitCode !=0)
	{
		if (errContent.isEmpty())
			pStatus.setError(1,"Failed to install service: "+GetServiceName()+" Reason: "+fileContent.left(4095));
		else
			pStatus.setError(1,"Failed to install service: "+GetServiceName()+" Reason: "+errContent.left(4095));
		return;
	}

#ifdef __APPLE__ //for MAC: to make service autostart there is no other way then to create bat script for startup:
	if (bAutoStart)
		Mac_CreateStartUpBat(m_strServiceName+m_strServiceDescriptor+"_start.sh");	

#else
	if (!m_strUser.isEmpty())
		AddUserLogonAsServicePrivilege(pStatus,m_strUser);
#endif


}


//must have grant.exe in path!!!
bool AppServerConfigurator::AddUserLogonAsServicePrivilege(Status &pStatus,QString strUserName)
{
	pStatus.setError(0);

#ifdef _WIN32

	//start with -i
	pStatus.setError(0);

	QProcess app;
	app.setWorkingDirectory(m_strAppSrvPath);
	QStringList lstArgs;
	lstArgs<<"add";
	lstArgs<<"SeServiceLogonRight";
	lstArgs<<strUserName;
	

	app.start(m_strAppSrvPath+"/grant.exe",lstArgs);
	if (!app.waitForFinished(-1)) //wait forever
	{		
		QByteArray fileContent=app.readAllStandardOutput();
		QByteArray errContent=app.readAllStandardError();
		if (errContent.isEmpty())
			pStatus.setError(1,"Failed to AddUserLogonAsServicePrivilege Reason: "+fileContent.left(4095));
		else
			pStatus.setError(1,"Failed to AddUserLogonAsServicePrivilege Reason: "+errContent.left(4095));
		return false;
	}
	int nExitCode=app.exitCode();
	QByteArray errContent=app.readAllStandardError();
	QByteArray fileContent=app.readAllStandardOutput();
	if (nExitCode !=0)
	{
		if (errContent.isEmpty())
			pStatus.setError(1,"Failed to AddUserLogonAsServicePrivilege Reason: "+fileContent.left(4095));
		else
			pStatus.setError(1,"Failed to AddUserLogonAsServicePrivilege Reason: "+errContent.left(4095));
		return false;
	}
	
#endif

	return true;

}

void AppServerConfigurator::StartOrInstall(Status &pStatus, bool bAutoStart)
{
	if(!IsServiceInstalled(GetServiceName()))
	{
		InstallService(pStatus,bAutoStart);
		return;
	}
	else
	{
		Start(pStatus);
	}

}

void AppServerConfigurator::UnInstallService(Status &pStatus)
{
	//start with -i
	pStatus.setError(0);

	QProcess app;
	app.setWorkingDirectory(m_strAppSrvPath);
	QStringList lstArgs;
	lstArgs<<"-u";
	app.start(m_strAppSrvPath+"/"+m_strAppExeName,lstArgs);
	if (!app.waitForFinished(-1)) //wait forever
	{		
		pStatus.setError(1,"Failed to un-install service:"+GetServiceName());
		return;
	}
	int nExitCode=app.exitCode();
	QByteArray errContent=app.readAllStandardError();
	QByteArray fileContent=app.readAllStandardOutput();
	if (errContent.size()!=0  && nExitCode!=0)
	{
		pStatus.setError(1,"Failed to send command to service! Reason:"+errContent.left(4095));
		return;
	}
}

void AppServerConfigurator::SetAutoStart(bool bAuto)
{
	QSettings settingsApp("HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentCOntrolSet\\Services\\"+GetServiceName(),	QSettings::NativeFormat);

	if (bAuto)
		settingsApp.setValue("Start",2);
	else
		settingsApp.setValue("Start",3);
}


//into: /Library/StartupItems/
bool AppServerConfigurator::Mac_CreateStartUpBat(QString strFileName)
{

#ifndef WIN32
	QString strExec="#!/bin/sh \n";
	strExec+="./"+m_strAppSrvPath+m_strAppExeName;

	if (strFileName.isEmpty())
		strFileName="start_server.sh";

	qDebug()<<"saving: "<<strFileName;

	QFile file("/Library/StartupItems/"+strFileName);
	if (file.exists())
		file.remove();

	if(file.open(QIODevice::WriteOnly | QIODevice::Text))
	{
		file.write(strExec.toLatin1());
		file.close();
		return true;
		qDebug()<<"file saved: "<<file.fileName();
	}

	qDebug()<<"Error while saving: "<<file.fileName();

#endif


	return false;
}


bool AppServerConfigurator::CreateUnInstallBat(bool bEnableFB_Deinstall)
{

#ifdef WIN32

	QString strExec=m_strAppExeName+ " -t "+"\n";		//stop
	strExec+=m_strAppExeName+ " -u ";			//uninstall

	QFile file(m_strAppSrvPath+"/uninstall.bat");
	if (file.exists())
		file.remove();

	if (bEnableFB_Deinstall)
	{
		//to include firebird uninstall?
		QFile fileUninstall(m_strAppSrvPath+"/firebird_local");
		if (fileUninstall.exists())
		{
			//change dir
			QString strCD=m_strAppSrvPath+"/firebird/bin";
			strCD=QDir::toNativeSeparators(strCD);
			strCD="CD "+strCD+"";
			strExec+="\n"+strCD;

			//exec firebird uninstall
			QString strFire=m_strAppSrvPath+"/firebird/bin/uninstall.bat";
			strFire=QDir::toNativeSeparators(strFire);
			strFire="CALL \""+strFire+"\"";
			strExec+="\n"+strFire;
		}
	}

	if(file.open(QIODevice::WriteOnly | QIODevice::Text))
	{
		file.write(strExec.toLatin1());
		file.close();
		return true;
	}
	else
	{
		return false;
	}

#else
	
	QString strExec="./"+m_strAppExeName+ " -t "+"\n";		//stop
	strExec+="./"+m_strAppExeName+ " -u ";			//uninstall

	//add to rm start bat:
	strExec+="\n";
	strExec+="rm /Library/StartupItems/"+m_strServiceName+m_strAppExeName+"_start.sh";

	QFile file(m_strAppSrvPath+"/uninstall.sh");
	if (file.exists())
		file.remove();

	if(file.open(QIODevice::WriteOnly | QIODevice::Text))
	{
		file.write(strExec.toLatin1());
		file.close();

		QProcess app;
		app.setWorkingDirectory(m_strAppSrvPath);
		app.start(m_strAppSrvPath+"/chmod +x uninstall.sh");
		if (!app.waitForFinished(-1)) //wait forever
		{		
			qDebug()<< "Failed to create uninstall.sh";
			return false;
		}
		int nExitCode=app.exitCode();
		QByteArray errContent=app.readAllStandardError();
		QByteArray fileContent=app.readAllStandardOutput();
		if (errContent.size()!=0  && nExitCode!=0)
		{
			qDebug()<<"Failed to create uninstall.sh. Reason:"+errContent.left(4095);
			return false;
		}

		return true;
	}
	else
	{
		return false;
	}

#endif




}


//return if service is running:
bool AppServerConfigurator::IsServiceRunning(QString strService)
{
	if (strService.isEmpty())  strService=GetServiceName();

	QtServiceController v(strService);
	return v.isRunning();
}

//return if service is installed:
bool AppServerConfigurator::IsServiceInstalled(QString strService)
{
	if (strService.isEmpty())  strService=GetServiceName();

	QtServiceController v(strService);
	return v.isInstalled();
}


void AppServerConfigurator::InstallFirebirdService(Status &pStatus)
{
	pStatus.setError(0);

	QString strInstallBat=m_strAppSrvPath+"/firebird/bin/install_super.bat";
	strInstallBat=QDir::toNativeSeparators(strInstallBat);
	QString strApp="cmd /C";
	strInstallBat=strInstallBat.trimmed();
	strInstallBat.prepend("\"");
	strInstallBat.append("\"");
	strApp=strApp+" "+strInstallBat;


	QProcess app;
	app.setWorkingDirectory(m_strAppSrvPath+"/firebird/bin");
	app.start(strApp);
	if (!app.waitForFinished(-1)) //wait forever
	{		
		pStatus.setError(1,"Failed to execute: "+strInstallBat);
		return;
	}
	int nExitCode=app.exitCode();
	QByteArray errContent=app.readAllStandardError();
	QByteArray fileContent=app.readAllStandardOutput();
	if (errContent.size()!=0  && nExitCode!=0)
	{
		pStatus.setError(1,"Failed to install FireBird Service! Reason:"+errContent.left(4095));
		return;
	}


	//create file:
	QFile fileUninstall(m_strAppSrvPath+"/firebird_local");
	if (!fileUninstall.exists())
	{
		if(fileUninstall.open(QIODevice::WriteOnly | QIODevice::Text))
		{
			fileUninstall.write("local");
			fileUninstall.close();
		}
	}

}