#include "sha256hash.h"
#include "sha2.h"

Sha256Hash::Sha256Hash()
{
}

Sha256Hash::~Sha256Hash()
{
}


/*!
	Accepts bytes and hash them with sha256, note: must use QByteArray, do not use QString

	\param byteData				- binary stream for hashing (can be longa as you wish)
	\return hash, digested 32 byte length array of bytes
*/
QByteArray Sha256Hash::GetHash(QByteArray &byteData)
{
	//generate SHA256 hash from input data
	unsigned char szHash[32];

	sha256_ctx ctx;
	sha256_begin(&ctx);
	sha256_hash((const unsigned char *)byteData.constData(), byteData.size(), &ctx);
	sha256_end(szHash, &ctx);

	return QByteArray((const char *)szHash, 32);
}
