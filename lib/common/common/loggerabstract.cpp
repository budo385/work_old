#include "loggerabstract.h"
#include "statuscodeset.h"
#include "stackwalkerlogger.h"
#include "threadid.h"


LoggerAbstract *ApplicationLogger::m_pGlobalLoggerSingleton=NULL;
void ApplicationLogger::logMessage(int nMsgType,int nMsgID, QString strMsgArguments)
{
	if (m_pGlobalLoggerSingleton)
		m_pGlobalLoggerSingleton->logMessage(nMsgType,nMsgID,strMsgArguments);

}

void ApplicationLogger::logAssert( bool bIfTrueConditionThenTrigger, QString strName, bool bDumpCallStack)
{
	if (bIfTrueConditionThenTrigger)
	{
		logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("Assert %1 detected from thread %2").arg(strName).arg(ThreadIdentificator::GetCurrentThreadID()));
		if (bDumpCallStack)
		{
			StackWalkerLogger logger;
			logger.DumpCallStack2Log();
		}
	}
}