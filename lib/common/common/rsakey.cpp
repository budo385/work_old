#include "rsakey.h"
#include <openssl/rsa.h>
#include <openssl/err.h>
#include <openssl/pem.h>
#include <openssl/bio.h>
#include <openssl/evp.h>
#include <openssl/rand.h>
//libeay32MD.lib ssleay32MD.lib

RSA_Key::RSA_Key()
{
	m_pKey = NULL;
}

RSA_Key::~RSA_Key()
{
	Clear();
}

bool RSA_Key::IsPublicKey()
{
	return (IsValid() && static_cast<RSA*>(m_pKey)->e && static_cast<RSA*>(m_pKey)->n);
}

bool RSA_Key::IsPrivateKey()
{
	return (IsValid() && static_cast<RSA*>(m_pKey)->p && static_cast<RSA*>(m_pKey)->q);
}

int RSA_Key::GetKeySize()
{
	if(IsValid())
		return RSA_size(static_cast<RSA*>(m_pKey));
	return 0;
}

void RSA_Key::Clear()
{
	if(IsValid()){
		RSA_free(static_cast<RSA*>(m_pKey));
		m_pKey = NULL;
	}
}

bool RSA_Key::Generate(int nSize, int nExp)
{
	Clear();	// just in case

	//TOFIX replace NULLs with progress callback method ?
	m_pKey = RSA_generate_key(nSize, nExp, NULL, NULL);

	return IsValid();
}

bool RSA_Key::WritePublicKey(const char *szFile)
{
	if(!IsValid())
		return false;	// nothing to write

	FILE *pOut = fopen(szFile, "wb");
	if(NULL == pOut)
		return false;	// failed to open file

	PEM_write_RSAPublicKey(pOut, static_cast<RSA*>(m_pKey));

	fclose(pOut);
	return true;
}

bool RSA_Key::WritePrivateKey(const char *szFile, const char *szPass)
{
	if(!IsValid())
		return false;	// nothing to write

	FILE *pOut = fopen(szFile, "wb");
	if(NULL == pOut)
		return false;	// failed to open file

	//support to write DES protected private key (if password given)
	const EVP_CIPHER *pCiph = (szPass != NULL)? EVP_des_ede3_cbc() : NULL;
	PEM_write_RSAPrivateKey(pOut, static_cast<RSA*>(m_pKey), pCiph, (unsigned char *)szPass, (szPass)? (int)strlen(szPass):0, NULL, NULL);

	fclose(pOut);
	return true;
}

bool RSA_Key::ReadPublicKey(const char *szFile, const char *szPass)
{
	FILE *pIn = fopen(szFile, "rb");
	if(NULL == pIn)
		return false;	// failed to open file

	if(!IsValid())
		m_pKey = RSA_new();

	if(szPass)
		m_strPass = szPass;	// store password temporary

	RSA* pKey=static_cast<RSA*>(m_pKey);
	PEM_read_RSAPublicKey(pIn, &pKey, (szPass)? pass_cb : NULL, this);
	m_pKey=pKey;

	m_strPass = "";	// do not store password forever
	fclose(pIn);
	return true;
}

bool RSA_Key::ReadPrivateKey(const char *szFile, const char *szPass)
{
	FILE *pIn = fopen(szFile, "rb");
	if(NULL == pIn)
		return false;	// failed to open file

	if(!IsValid())
		m_pKey = RSA_new();

	if(szPass)
		m_strPass = szPass;	// store password temporary
	RSA* pKey=static_cast<RSA*>(m_pKey);
	PEM_read_RSAPrivateKey(pIn, &pKey, (szPass)? pass_cb : NULL, this);
	m_pKey=pKey;

	m_strPass = "";	// do not store password forever
	fclose(pIn);
	return true;
}

bool RSA_Key::ReadPublicKeyFromBuffer(const char *szBuffer, int nSize, const char *szPass)
{
	bool bSuccess = false;
	BIO* biomem = BIO_new_mem_buf((void *)szBuffer, nSize);
	if(biomem)
	{
		if(szPass)
			m_strPass = szPass;	// store password temporary

		RSA* pKey=static_cast<RSA*>(m_pKey);
		if(PEM_read_bio_RSAPublicKey(biomem, &pKey, (szPass)? pass_cb : NULL, this))
			bSuccess = true;
		m_pKey=pKey;

		m_strPass = "";	// do not store password forever
		BIO_free_all(biomem);
	} 
	return bSuccess;
}

bool RSA_Key::ReadPrivateKeyFromBuffer(const char *szBuffer, int nSize, const char *szPass)
{
	bool bSuccess = false;
	BIO* biomem = BIO_new_mem_buf((void *)szBuffer, nSize);
	if(biomem)
	{
		if(szPass)
			m_strPass = szPass;	// store password temporary

		RSA* pKey=static_cast<RSA*>(m_pKey);
		if(PEM_read_bio_RSAPrivateKey(biomem, &pKey, (szPass)? pass_cb : NULL, this))
			bSuccess = true;
		m_pKey=pKey;

		m_strPass = "";	// do not store password forever
		BIO_free_all(biomem);
	} 
	return bSuccess;
}

int RSA_Key::Encrypt(const char *szSrc, int nLenSrc, char *szDst, int nPadding)
{
	return Operation(RSA_ENCRYPT, szSrc, nLenSrc, szDst, nPadding);
}

int RSA_Key::Sign(const char *szSrc, int nLenSrc, char *szDst, int nPadding)
{
	return Operation(RSA_SIGN, szSrc, nLenSrc, szDst, nPadding);
}

int RSA_Key::Verify(const char *szSrc, int nLenSrc, char *szDst, int nPadding)
{
	return Operation(RSA_VERIFY, szSrc, nLenSrc, szDst, nPadding);
}

int RSA_Key::Decrypt(const char *szSrc, int nLenSrc, char *szDst, int nPadding)
{
	return Operation(RSA_DECRYPT, szSrc, nLenSrc, szDst, nPadding);
}

int RSA_Key::Operation(int nOp, const char *szSrc, int nLenSrc, char *szDst, int nPadding)
{
	int nTotal = 0;	//bytes decrypted

	if(IsValid())
	{
		int nToDo = nLenSrc;
		int nBlock = GetKeySize();

		// when encrypting with padding leave some space for padding
		if(nOp == RSA_ENCRYPT || nOp == RSA_SIGN){
			if(RSA_PKCS1_PADDING == nPadding)
				nBlock -= 12;	
			else if(RSA_PKCS1_OAEP_PADDING == nPadding)
				nBlock -= 42;
		}

		//encrypt block by block
		while(nToDo > 0)
		{
			int nSrcBlk  = (nToDo>=nBlock)? nBlock : nToDo;
			int nLen = 0;
			
			//execute operation on a single block of data
			switch(nOp){
				case RSA_ENCRYPT:
					nLen = RSA_public_encrypt(nSrcBlk, (const unsigned char *)szSrc, (unsigned char *)szDst, static_cast<RSA*>(m_pKey), nPadding);
					break;
				case RSA_DECRYPT:
					nLen = RSA_private_decrypt(nSrcBlk, (const unsigned char *)szSrc, (unsigned char *)szDst, static_cast<RSA*>(m_pKey), nPadding);
					break;
				case RSA_SIGN:
					nLen = RSA_private_encrypt(nSrcBlk, (const unsigned char *)szSrc, (unsigned char *)szDst, static_cast<RSA*>(m_pKey), nPadding);
					break;
				case RSA_VERIFY:
					nLen = RSA_public_decrypt(nSrcBlk, (const unsigned char *)szSrc, (unsigned char *)szDst, static_cast<RSA*>(m_pKey), nPadding);
					break;
			}
			if(nLen > 0)
			{
				szSrc += nSrcBlk;
				nToDo -= nSrcBlk;
				szDst += nLen;
				nTotal+= nLen;
			}
			else
				return -1;	//error
		}
		return nTotal;
	}
	return -1;
}

int RSA_Key::GetError()
{
	return ERR_get_error();
}

int OpenSSLHelper::Extern_RAND_bytes(unsigned char *buf,int num)
{
	return RAND_bytes(buf, num);		
}


int pass_cb(char *buf, int size, int rwflag, void *u)
{
	//Q_UNUSED(rwflag);

	RSA_Key *pKey = (RSA_Key *)u;

	//copy password into the receiving buffer
	int nLen = pKey->m_strPass.size();
	if (nLen <= 0)   return 0;
	if (nLen > size) nLen = size;	// truncate if longer than available buffer

	memcpy(buf, pKey->m_strPass.c_str(), nLen);

	return nLen;
}
