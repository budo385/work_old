#include "dbrecordsetdefintion.h"


//------------------------------------------------------
//			DbColumnEx
//------------------------------------------------------

DbColumnEx::DbColumnEx(QString strName,int nType,bool bIsLongVarChar,QString strDesc)
{
	m_strName			= strName;
	m_nType				= nType;
	m_bIsLongVarChar	= bIsLongVarChar;
	m_strDesc			= strDesc;
}
DbColumnEx::DbColumnEx(QString strName,QString strNameAlias,int nType,bool bIsLongVarChar,QString strDesc)
{
	m_strName			= strName;
	m_strNameAlias		= strNameAlias;
	m_nType				= nType;
	m_bIsLongVarChar	= bIsLongVarChar;
	m_strDesc			= strDesc;
}

DbColumnEx::DbColumnEx(const DbColumnEx &that)
{
	operator =(that);
}

void DbColumnEx::operator =(const DbColumnEx &that)
{
	m_strName				= that.m_strName;
	m_strNameAlias			= that.m_strNameAlias;
	m_nType					= that.m_nType;
	m_bIsLongVarChar		= that.m_bIsLongVarChar;
	m_strDesc				= that.m_strDesc;
}



//------------------------------------------------------
//			DbView
//------------------------------------------------------

DbView::DbView(const DbView &that) : QList<DbColumnEx>()
{
	operator =(that);
}

void DbView::operator =(const DbView &that)
{
	QList<DbColumnEx>::operator=(that);
	m_strSelectSQL			= that.m_strSelectSQL;
	m_strInsertSQL			= that.m_strInsertSQL;
	m_strUpdateSQL			= that.m_strUpdateSQL;
	m_strTables				= that.m_strTables;
	m_nSkipFirstColsInsert	= that.m_nSkipFirstColsInsert;
	m_nSkipFirstColsUpdate	= that.m_nSkipFirstColsUpdate;
	m_nSkipLastCols			= that.m_nSkipLastCols;
	m_nViewID				= that.m_nViewID;
	m_nSkipLastColsWrite	= that.m_nSkipLastColsWrite;
	m_LstIsLongVarcharField = that.m_LstIsLongVarcharField;
}

