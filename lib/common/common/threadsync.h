#ifndef THREADSYNC_H 
#define THREADSYNC_H


#include <QReadWriteLock>
#include <QThread>
#include <QDebug>
#include <QSemaphore>
#include "sleeper.h"


/*!
    \class ThreadSynchronizer
    \brief Provides mechanism for synchronizing threads
    \ingroup Thread

	QWaitCondition can not be used sometimes in syncing threads. Use this object.
	Share this object between two or more threads that needs syncing. It's multithread safe

	Use inside thread that must be stopped (master):
		\code
		ThreadSyncro->ThreadSetForWait();			//prepare for wait
		emit SignalToOtherThread();	//notify slave thread
		ThreadSyncro->ThreadWait();				//wait until something is come back from slave
		\endcode

	Use this code inside slave thread 
		\code
		ThreadSyncro->ThreadRelease(); //wake calling thread if he fell at sleep :)
		\endcode
	
	NOTE: ThreadSyncro is shared 'global' object

*/
class ThreadSynchronizer
{ 
public:
	//ThreadSynchronizer():m_bWaitFlag(false),m_Sem(1){};
	ThreadSynchronizer():m_Sem(1){};
	ThreadSynchronizer(const ThreadSynchronizer &that);
	ThreadSynchronizer &operator =(const ThreadSynchronizer &that); 

	bool ThreadWaitTimeOut(int nTimeoutMsec=500);
	void ThreadWait();
	void ThreadRelease();
	void ThreadSetForWait();

private:

	//bool IsThreadFree();
	//QReadWriteLock m_RWLock;	///< RWlock for accessing shared data
	//bool m_bWaitFlag;			///< shared flag for resync between threads, 0 - means that threads are free, 1- means one thread is waiting for other
	QSemaphore m_Sem;
	QMutex lock;
};


#endif // THREADSYNC_H
