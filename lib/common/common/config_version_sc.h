#ifndef CONFIG_VERSION_SC_H
#define CONFIG_VERSION_SC_H

#define DATABASE_VERSION_SPC		65809					//current DB version of SPC Project Control
#define DATABASE_VERSION			134						//current DB version Communicator
#define APPLICATION_VERSION			"V2014-397.000.022"		
#define APPLICATION_BUILD_DATE		"09.09.2014"			//public	
#define SERVER_DESCRIPTION_TAG		"SOKRATES AppServer"	//use as service name


#endif //CONFIG_VERSION_SC_H