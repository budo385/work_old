#ifndef CONFIG_CODES_H
#define CONFIG_CODES_H

//this will remain same, hihi, don't tell Marin
#define SERVER_CODE_JAMES		4
#define SERVER_CODE_TARANTULA	5
#define SERVER_CODE_JOHN		6


//Application prefix: first two digits
#define DIGIT_APP_EVERDOCS	"11"
#define DIGIT_APP_EVERPICS	"10"

//Platform prefix: next two digits
#define DIGIT_PLATFORM_IPHONE		"00"
#define DIGIT_PLATFORM_ANDROID		"01"
#define DIGIT_PLATFORM_BLACKBERRY	"02"

#define DIGIT_PLATFORM_WINDOWS_CLIENT	"05"
#define DIGIT_PLATFORM_MAC_CLIENT		"06"
#define DIGIT_PLATFORM_LINUX_CLIENT		"07"

#define DIGIT_PLATFORM_BROWSER_WEBAPP		"10"
#define DIGIT_PLATFORM_MOBILE_IPHONE_WEBAPP	"11"

//App status: lite, pro, none (next 1 digit)
#define DIGIT_STATUS_NONE		"0"
#define DIGIT_STATUS_LITE		"1"
#define DIGIT_STATUS_PRO		"2"

//Add ons (next 3 decimal digits: coded as binary, bit by bit and displayed as decimal, hehe)
//Here are represent decimal values that are to be added to the program code:
//e.g. if SSL=ON, then PROG_CODE += BIT_ADDON_SSL, etc....

#define BIT_ADDON_SSL						"001"
#define BIT_ADDON_DOCUMENT_PACKAGE			"002"
#define BIT_ADDON_DOCUMENT_AND_SSL_PACKAGE	"003"


//Examples of client codes:
//for Sara on iPhone: 11000000
//for Lucy on iPhone: 10000000
//if Sara has addon to browse ordinary files then: 11000000+BIT_ADDON_DOCUMENT_PACKAGE=11000002
//if Sara has both addons active (SSL and browse ordinary files then): 11000000+BIT_ADDON_SSL+BIT_ADDON_DOCUMENT_PACKAGE=11000003
//for WebApp on iPhone for James:  10110000
//for WebApp on Windows for James: 10100000



#endif //CONFIG_CODES_H