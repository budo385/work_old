/*!
	\class LoggerAbstract
	\brief Abstract Logger Class
	\ingroup Bus_Core

	Abstract class for instances of Logger.
	Log messages are stored with this params:
	- msg id,
	- msg text arguments
	- category id
	- datetime
*/

#ifndef LOGGERABSTRACT_H
#define LOGGERABSTRACT_H

#include <QString>
#include <QObject>

class LoggerAbstract : public QObject //do not remove Qobject from this
{

public:
	virtual ~LoggerAbstract(){};


	enum MessageLevel
	{
		LOG_LEVEL_NONE=0,		//no logging
		LOG_LEVEL_BASIC=1,		//basic: all erros & warnings AND information from  common,business & system category
		LOG_LEVEL_ADVANCED=2,	//all but no debug
		LOG_LEVEL_DEBUG=3,		//all + debug info
		LOG_LEVEL_SPEED_TEST=4,	//just TYPE_SPEED_TEST_LEVEL log messages
	};

	virtual void logMessage(int nMsgType,int nMsgID, QString strMsgArguments="")=0;
};

//singleton class for application logger
class ApplicationLogger
{
public:
	static void setApplicationLogger(LoggerAbstract *pGlobalAppLogger){m_pGlobalLoggerSingleton=pGlobalAppLogger;}
	static void logMessage(int nMsgType,int nMsgID, QString strMsgArguments="");
	static void logAssert(bool bIfTrueConditionThenTrigger, QString strName, bool bDumpCallStack=true);

private:
	static LoggerAbstract *m_pGlobalLoggerSingleton;

};

#endif // LOGGERABSTRACT_H
