#ifndef CLIENTENTITYCACHE_H
#define CLIENTENTITYCACHE_H

#include <QTime>
#include <QDateTime>
#include <QHash>
#include "common/common/dbrecordset.h"
#include "common/common/observer_ptrn.h"

/*!
	\class  ClientEntityCache
	\brief  Global object ans client cache for all data distinguish by Entity ID from bus_core/entity_id_collection.h 
	\ingroup Bus_Client

	Every data can be stored here by unique id defined in bus_core/entity_id_collection.h 
	Data in cache expires after m_nExpireTime, from last set.

	Data inside cache is stored:
	EntityID	- unique entity ID/data ID from bus_core/entity_id_collection.h 
	Time		- time of cache load
	Data		- data itself

	Use:
	- init with default expire timeout

	Note:
	- mainselection controllers uses this to avoid needless server calls

*/
class ClientEntityCache 
{

public:
	ClientEntityCache();
    ~ClientEntityCache();

	void SetGlobalMsgDispatcher(ObsrPtrn_Subject* pGlobalMsgDispatcher, int nAddSignalId, int nEditSignalId, int nRemoveSignalId);
	void PurgeCache(int nEntityID=-1);
	DbRecordSet* GetCache(int nEntityID);
	void SetCache(int nEntityID, DbRecordSet& Data);
	void ModifyCache(int nEntityID, DbRecordSet& newData, int nOperation,ObsrPtrn_Observer *pSender=NULL, bool bSkipGlobalNotifySignal=false);

	//time:
	bool IsCacheNeedReload(int nEntityID);
	void SetCacheReloadCurrentTime(int nEntityID);

	
private:
	QHash<int,DbRecordSet>	m_Cache;
	QHash<int,QDateTime>	m_CacheReloadTime;
	ObsrPtrn_Subject*		m_pGlobalMsgDispatcher;
	int m_nAddSignalId; 
	int m_nEditSignalId;
	int m_nRemoveSignalId;
    
};

#endif // CLIENTENTITYCACHE_H
