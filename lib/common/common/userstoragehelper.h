#ifndef USERSTORAGEHELPER_H
#define USERSTORAGEHELPER_H

#include <QByteArray>
#include <QString>

class UserStorageHelper
{
public:

	static QString GenerateAuthQueryString(QString strUserName,QByteArray bytePassword,QString strParentSession);
	static QString GenerateAuthCookie(QString strSession);
	static QString GenerateRequestQueryString(QString strFileName,QString strMethod="download",QString strSubDir="");
	
};

#endif
