#ifndef CONFIG_VERSION_JOHN_H
#define CONFIG_VERSION_JOHN_H

#include "config_codes.h"

#define DATABASE_VERSION				0										
#define APPLICATION_VERSION				"V2013-000.000.051"		
#define APPLICATION_BUILD_DATE			"10.01.2014"							
#define SERVER_JOHN_SERVICE_NAME		"EverDOCs-Service"							//use as service name
#define SERVER_JOHN_SERVICE_DESC		"EverDOCs Bridge"	//use as service desc
#define SERVER_JOHN_WEBSERVICE_EXPIRE	1200 //seconds (20min)

#define PROGRAM_CODE_MIN		0
#define PROGRAM_CODE_MAX		5	//update this after adding new code

#define JOHN_PROGRAM_VERSION_MAJOR			1
#define JOHN_PROGRAM_VERSION_MINOR			0
#define JOHN_PROGRAM_VERSION_REVISION		11

#define JOHN_PORT_FORWARD_NATPMP			1
#define JOHN_PORT_FORWARD_UPNP				2
#define JOHN_PORT_FORWARD_MANUAL			3

#define CATALOG_STORAGE_PREFIX "user_catalog"
#define CATALOG_STORAGE_PREFIX_SIZE 12
#define USER_STORAGE_PREFIX "user_temp"
#define USER_STORAGE_PREFIX_SIZE 9
#define THUMB_CACHE_PREFIX "thumb_cache"
#define THUMB_CACHE_PREFIX_SIZE 11



#define FFH_CONNECT_SERVER_MASTER_IP	"http://www.everxconnect.com:11000"
//#define FFH_CONNECT_SERVER_MASTER_IP	"http://192.168.200.19:9999"
#define FFH_CONNECT_SERVER_LIST			"http://www.everpics.com/everxconnect2.txt" // semicolon delimited list of tarantula IP's
#define FFH_CONNECT_SERVER_TIMEOUT		10 //seconds
#define FFH_WEBAPP_VERSION_URL			"http://everpics.com/everpics/downloads/epwa_version.txt"
#define FFH_WEBAPP_DATA_URL				"http://everpics.com/everpics/downloads/webdata"

#define  SETTINGS_PASSWORD "KapulA_01234"
#define  USERNAME_GUEST "guest"


#endif //CONFIG_VERSION_JOHN_H