#include "datahelper.h"
#include "common/common/status.h"
#include <QFileInfo>
#include <QFileInfoList>
#include <QDir>
#include <QUrl>
#include <QCoreApplication>
#include <QSettings>

#ifdef _WIN32
	//#include <windows.h>
	#ifndef WINCE
		#include <shlobj.h>
	#endif //WINCE
	#define MAX_VALUE_LENGTH 16383
#else // linux stuff
 #ifdef __APPLE__
	#include <sys/param.h>
	#include <sys/mount.h>
	#include <sys/stat.h>
 #else
	#include <sys/vfs.h>
	#include <sys/stat.h>
 #endif
#endif // _WIN32



bool DataHelper::m_bIsPortable=false;

//Constructs SQL = statement based on field type: strings like, others =
//value is stored as string
//if failes it returns empty string
QString DataHelper::GetFilterBasedOnFieldType(DbView &view,QString strColName,QString strColValue1, QString strColValue2, bool bIgnoreCase)
{
	QString strSQLWhere;

	int nSize=view.size();
	for(int i=0;i<nSize;++i)
	{
		if (view.at(i).m_strName==strColName)
		{
			switch(view.at(i).m_nType)
			{
				case QVariant::Date:
					{
						getDateSqlString(strSQLWhere, strColName, strColValue1, strColValue2);
						break;
					}

				case QVariant::DateTime: //just use date portion (for BCNT_DAT_CREATED)
					{
						getDateSqlString(strSQLWhere, strColName, strColValue1, strColValue2);
						break;
					}
			
				case QVariant::Int:
					strSQLWhere+=strColName+" = "+strColValue1;
					break;
				default:
					getCharacterSqlString(strSQLWhere, strColName, strColValue1, strColValue2, bIgnoreCase);
					break;
			}
			return strSQLWhere;
		}
	}

	//if empty treat as strinG:
	if (strSQLWhere.isEmpty())
	{
		if (strColName=="BCNT_BIRTHDAY" || strColName=="BGCN_CMCA_VALID_FROM" || strColName=="BGCN_CMCA_VALID_TO" || strColName=="BCNT_DAT_CREATED" || strColName=="BCNT_FOUNDATIONDATE")
		{
			getDateSqlString(strSQLWhere, strColName, strColValue1, strColValue2);
		}
		else if (strColName=="BCNT_SEX") //if int value
		{
			strSQLWhere+=strColName+" = "+strColValue1;
		}
		else //if character.
		{
			getCharacterSqlString(strSQLWhere, strColName, strColValue1, strColValue2, bIgnoreCase);
		}
	}

	return strSQLWhere;
}


//parses inputed user string: this is specific for us:
//takes local settings, DDMMYYY and american YYYY.MM.DD
QDate DataHelper::ParseDateString(QString strColValue)
{
	QDate datTemp;
	if(strColValue.isEmpty())
		return datTemp;

	//try to convert:
	datTemp=QDate::fromString(strColValue,"dd.MM.yyyy");
	if(datTemp.isNull())
		datTemp=QDate::fromString(strColValue,"d.M.yyyy");
	if(datTemp.isNull())
		datTemp=QDate::fromString(strColValue,Qt::ISODate);
	if(datTemp.isNull())
		datTemp=QDate::fromString(strColValue,Qt::LocaleDate);
	if(datTemp.isNull())
		datTemp=QDate::fromString(strColValue,"yyyy.MM.dd");
	if(datTemp.isNull())
		datTemp=QDate::fromString(strColValue,"yyyy/MM/dd");

	if(datTemp.isNull())
	{
		//add 100 years for >2000
		datTemp=QDate::fromString(strColValue,"d.M.yy");
		if(datTemp.isNull()) //exit if this also failes
			return datTemp;
		QDate tmp=datTemp;
		if(tmp.year()<2000)
			datTemp=datTemp.addYears(100);
	}

	return datTemp;
}





//merges two lists: lstNewData & lstData into lstData based on operation
//lstDataResultForWrite and lstDataResultForDelete are only those records that are to be written to modify original list to proper state
void DataHelper::DataOperation(int nOperation,DbRecordSet *lstNewData, DbRecordSet *lstData,  DbRecordSet *lstDataResultForWrite,DbRecordSet *lstDataResultForDelete, int nPrimaryKeyIndex,int nPrimaryKeyIndexNewData)
{

	//define:
	if (lstDataResultForDelete)lstDataResultForDelete->copyDefinition(*lstData);
	if (lstDataResultForWrite)lstDataResultForWrite->copyDefinition(*lstData);

	if (nPrimaryKeyIndexNewData==-1)
		nPrimaryKeyIndexNewData=nPrimaryKeyIndex;
	int nRow;

	switch(nOperation)
	{
	case OP_ADD:
		if (lstData->getRowCount()==0 && lstData->getColumnCount()==lstNewData->getColumnCount())
			*lstData=*lstNewData; //only if lists are same!
		else
			lstData->merge(*lstNewData);
		
		if (lstDataResultForWrite)*lstDataResultForWrite=*lstNewData;
		break;
	case OP_REMOVE:
		{
			int nSize=lstNewData->getRowCount();
			for(int i=0;i<nSize;++i)
			{
				lstData->find(nPrimaryKeyIndex,lstNewData->getDataRef(i,nPrimaryKeyIndexNewData));
				if (lstDataResultForDelete)
					lstDataResultForDelete->merge(*lstData,true);
				lstData->deleteSelectedRows();
			}
			break;
		}
	case OP_REPLACE: 
		{
			//just assign new data:
			if (lstData->getColumnCount()==lstNewData->getColumnCount())  //only if lists are same!
			{
				if (lstDataResultForDelete)*lstDataResultForDelete=*lstData;
				if (lstDataResultForWrite)*lstDataResultForWrite=*lstNewData;
				*lstData=*lstNewData;
			}
			else
			{
				if (lstDataResultForDelete)*lstDataResultForDelete=*lstData;
				lstData->merge(*lstNewData);
				if (lstDataResultForWrite)*lstDataResultForWrite=*lstData;
			}
			break;
		}

	case OP_INTERSECT: //only items that are in both lists
		{

			int nSize=lstNewData->getRowCount();
			lstData->selectAll();
			for(int i=0;i<nSize;++i)
			{
				nRow=lstData->find(nPrimaryKeyIndex,lstNewData->getDataRef(i,nPrimaryKeyIndexNewData),true);
				if (nRow!=-1)
					lstData->selectRow(nRow,false);
			}
			if (lstDataResultForDelete)
				lstDataResultForDelete->merge(*lstData,true);
			lstData->deleteSelectedRows();
			break;
		}

	case OP_EDIT: //replace items from one list that are found and add new if not found
		{
			int nSize=lstNewData->getRowCount();
			MapCols mapped;
			lstData->mapColumns(mapped,*lstNewData); //only to speed up:cache mapping from source to target

			//if stored list is empty, put it inside:
			if (lstData->getRowCount()==0)
			{
				if (lstData->getColumnCount()==lstNewData->getColumnCount())  //only if lists are same!
				{
					*lstData=*lstNewData;
					if (lstDataResultForWrite)*lstDataResultForDelete=*lstNewData; //all new data is for write
				}
				else
				{
					lstData->merge(*lstNewData);
					if (lstDataResultForWrite)*lstDataResultForDelete=*lstData; //all new data is for write
				}
				return;
			}

			//lstData->selectAll();
			lstNewData->clearSelection();
			for(int i=0;i<nSize;++i)
			{
				nRow=lstData->find(nPrimaryKeyIndex,lstNewData->getDataRef(i,nPrimaryKeyIndexNewData),true);
				if (nRow!=-1){
					DbRecordSet rowTmp = lstNewData->getRow(i);
					lstData->assignRow(nRow,rowTmp,true,&mapped);
				}
				else
				{
					lstNewData->selectRow(i);
					//DbRecordSet newRow=lstNewData->getRow(i);
					//lstData->addRow();
					//lstData->assignRow(lstData->getRowCount()-1,newRow,true);
				}
			}
			lstData->merge(*lstNewData,true);
			
			if (lstDataResultForWrite)*lstDataResultForDelete=*lstNewData; //all new data is for write
			break;
		}

	case OP_EDIT_ONLY:
		{
			int nSize=lstNewData->getRowCount();
			MapCols mapped;
			lstData->mapColumns(mapped,*lstNewData); //only to speed up:cache mapping from source to target

			//if stored list is empty, put it inside:
			if (lstData->getRowCount()==0)
			{
				return;
			}

			//lstData->selectAll();
			lstNewData->clearSelection();
			for(int i=0;i<nSize;++i)
			{
				nRow=lstData->find(nPrimaryKeyIndex,lstNewData->getDataRef(i,nPrimaryKeyIndexNewData),true);
				if (nRow!=-1)
				{
					DbRecordSet rowTmp = lstNewData->getRow(i);
					lstData->assignRow(nRow,rowTmp,true,&mapped);
					lstNewData->selectRow(i);
				}
			}

			if (lstDataResultForWrite) //all new data that is edited is for write
			{
				lstDataResultForDelete->copyDefinition(*lstNewData);
				lstDataResultForDelete->merge(*lstNewData,true); 
			}
			break;
		}

	}


}

//lstDataToCheck will be compared against lstPossibleDuplicates
//if duplicates are found inside lstPossibleDuplicates, they will be deleted
//search is base don column 0 (id)
void DataHelper::RemoveDuplicates(DbRecordSet *lstDataToCheck, DbRecordSet *lstPossibleDuplicates, int nLstDataToCheckIDIdx,int nLstDPossibleDuplicatesIDIdx)
{
	//delete duplos from origin list:
	lstPossibleDuplicates->removeDuplicates(nLstDPossibleDuplicatesIDIdx);
	lstPossibleDuplicates->clearSelection();

	//search each item inside original list:
	int nSize=lstPossibleDuplicates->getRowCount();
	int nRow;
	for(int i=0;i<nSize;++i)
	{
		nRow=lstDataToCheck->find(nLstDataToCheckIDIdx,lstPossibleDuplicates->getDataRef(i,nLstDPossibleDuplicatesIDIdx),true);
		if (nRow!=-1)
			lstPossibleDuplicates->selectRow(i);
	}

	lstPossibleDuplicates->deleteSelectedRows();
}



//checks if file exec, if sym link points to exec, extract real target, or original path
//return name without path/extension
//return extension, if executable, returns "exe".
//return extension, if executable, returns "dir".
QString DataHelper::ResolveFilePath(QString strPath,QString &strFileName,QString &strExtension)
{
	QFileInfo fileInfo(strPath);
	if (fileInfo.isSymLink())
	{
		strFileName=fileInfo.baseName();
		QString strFilePath=fileInfo.symLinkTarget();
		QFileInfo fileInfoExe(strFilePath);
		if (fileInfoExe.isExecutable())
		{
			
			strExtension="exe";
			//return strPath;				//return symlink
			return strFilePath;
		}
		else
		{
			strExtension=fileInfoExe.suffix();		//not link, target
			return strFilePath;						//not link, target
		}
	}
	else if (fileInfo.isDir())
	{
		strFileName=fileInfo.baseName();
		strExtension="dir";
		return strPath;
	}
	else if (fileInfo.isExecutable())
	{
		strFileName=fileInfo.baseName();
		strExtension="exe";
		return strPath;
	}
	else //not exec: try to figure out extension
	{
		strFileName=fileInfo.baseName();
		strExtension=fileInfo.suffix();
		return strPath;
	}

}


//return fields, separated by coma = ,
QString DataHelper::getSQLColumnsFromRecordSet(DbRecordSet &record)
{
	//if not defined, generate automatic, store back to list :)
	QString strSql;

	int nSize = record.getColumnCount();
	for(int i=0; i < nSize ;++i)
	{
		strSql+=record.getColumnName(i);
		strSql+=",";							//add comma between
	}
	strSql.chop(1);

	//store back to skip init again:
	return strSql;
}


void DataHelper::ParseFilesFromURLs(QList<QUrl> &lst,DbRecordSet &lstPaths, DbRecordSet &lstApps, bool bRecursiveFolders,bool bAcceptApplications)
{

	//list of paths (files), list of application paths (exe)
	lstPaths.addColumn(QVariant::String,"PATH");
	lstPaths.addColumn(QVariant::String,"NAME");
	lstPaths.addColumn(QVariant::String,"EXT");
	lstApps.copyDefinition(lstPaths);


	for (int i=0;i<lst.size();++i)
	{
		if (lst.at(i).scheme().toUpper()==QString("file").toUpper())
		{				//files:
			QString strExt,strName;
			QString strFilePath=ResolveFilePath(lst.at(i).toLocalFile(),strName,strExt);
			if (strExt=="exe") //we got exe file
			{
				if (bAcceptApplications)
				{
					lstApps.addRow();
					lstApps.setData(lstApps.getRowCount()-1,0,strFilePath);
					lstApps.setData(lstApps.getRowCount()-1,1,strName);
				}
				else
				{
					QFileInfo fileInfo(strFilePath); //if application are not consider apps, just as ordinary docs
					lstPaths.addRow();
					lstPaths.setData(lstPaths.getRowCount()-1,0,strFilePath);
					lstPaths.setData(lstPaths.getRowCount()-1,1,strName);
					lstPaths.setData(lstPaths.getRowCount()-1,2,fileInfo.suffix());
				}
			}
			else if(strExt=="dir")
			{
				DbRecordSet lstRecPaths,lstRecApps;
				GetFilesFromFolder(strFilePath,lstRecPaths,lstRecApps,true);
				lstPaths.merge(lstRecPaths);
				if (bAcceptApplications)
					lstApps.merge(lstRecApps);
				else
					lstPaths.merge(lstRecApps);

			}
			else
			{
				lstPaths.addRow();
				lstPaths.setData(lstPaths.getRowCount()-1,0,strFilePath);
				lstPaths.setData(lstPaths.getRowCount()-1,1,strName);
				lstPaths.setData(lstPaths.getRowCount()-1,2,strExt);
			}
		}
		else //if (lst.at(i).scheme().toUpper()==QString("http").toUpper())		//http, mailto, other ...
		{
			lstPaths.addRow();
			lstPaths.setData(lstPaths.getRowCount()-1,0,lst.at(i).toString());
			QString strName=QFileInfo(lst.at(i).path()).fileName();		//filename
			if (strName.isEmpty())
				strName=lst.at(i).host();								//host
			if (strName.isEmpty())	
				strName=lst.at(i).path();								//whole path
			lstPaths.setData(lstPaths.getRowCount()-1,1,strName);		
			lstPaths.setData(lstPaths.getRowCount()-1,2,"HTTP");
		}
	}


}


//get files: ifbUseRelativeFilePath then PATH will contain only relative path+filename to strFolderPath, else full path with filename
void DataHelper::GetFilesFromFolder(QString strFolderPath,DbRecordSet &lstPaths, DbRecordSet &lstApps, bool bRecursiveFolders, bool bUseRelativeFilePath,QString strRelativeRootPath)
{

	lstPaths.destroy();
	lstApps.destroy();
	lstPaths.addColumn(QVariant::String,"PATH");
	lstPaths.addColumn(QVariant::String,"NAME");
	lstPaths.addColumn(QVariant::String,"EXT");
	lstApps.copyDefinition(lstPaths);


	QDir dirRel(strRelativeRootPath);
	QDir dir(strFolderPath);
	if (!dir.exists())
		return;


	QFileInfoList	lstFiles=dir.entryInfoList();
	int nSize=lstFiles.size();
	for(int i=0;i<nSize;++i)
	{
		if (lstFiles.at(i).isDir())
		{
			//skip current and parent directory:
			if (lstFiles.at(i).fileName()==".") continue;
			if (lstFiles.at(i).fileName()=="..") continue;

			//Get all files from folder:
			if (bRecursiveFolders)
			{ 
				DbRecordSet lstRecPaths,lstRecApps;
				GetFilesFromFolder(lstFiles.at(i).absoluteFilePath(),lstRecPaths,lstRecApps,true,bUseRelativeFilePath,strRelativeRootPath);
				lstPaths.merge(lstRecPaths);
				lstApps.merge(lstRecApps);
			}
		}
		else if (lstFiles.at(i).isExecutable())
		{
			lstApps.addRow();
			if (bUseRelativeFilePath)
				lstApps.setData(lstApps.getRowCount()-1,0,QFileInfo(dirRel.relativeFilePath(lstFiles.at(i).absoluteFilePath())).absoluteFilePath());
			else
				lstApps.setData(lstApps.getRowCount()-1,0,lstFiles.at(i).absoluteFilePath());

			lstApps.setData(lstApps.getRowCount()-1,1,lstFiles.at(i).baseName());
		}
		else if (lstFiles.at(i).isSymLink()) //only to executable
		{
			QString strFilePath=lstFiles.at(i).symLinkTarget();
			QFileInfo fileInfoExe(strFilePath);
			if (fileInfoExe.isExecutable())
			{
				lstApps.addRow();
				if (bUseRelativeFilePath)
					lstApps.setData(lstApps.getRowCount()-1,0,QFileInfo(dirRel.relativeFilePath(fileInfoExe.absoluteFilePath())).absoluteFilePath());
				else
					lstApps.setData(lstApps.getRowCount()-1,0,fileInfoExe.absoluteFilePath());
				lstApps.setData(lstApps.getRowCount()-1,1,fileInfoExe.baseName());
			}
		}
		else
		{
			//Get all files from folder:
			lstPaths.addRow();
			if (bUseRelativeFilePath)
			{
				lstPaths.setData(lstPaths.getRowCount()-1,0,QFileInfo(dirRel.relativeFilePath(lstFiles.at(i).absoluteFilePath())).filePath());
			}
			else
				lstPaths.setData(lstPaths.getRowCount()-1,0,lstFiles.at(i).absoluteFilePath());
			lstPaths.setData(lstPaths.getRowCount()-1,1,lstFiles.at(i).baseName());
			lstPaths.setData(lstPaths.getRowCount()-1,2,lstFiles.at(i).suffix());
		}
	}

}


qint64 DataHelper::GetDiskUsage(QString strFolderPath, bool bRecursiveFolders)
{
	QDir dir(strFolderPath);
	if (!dir.exists())
		return 0;

	qint64 nTotalBytes=0;

	QFileInfoList	lstFiles=dir.entryInfoList();
	int nSize=lstFiles.size();
	for(int i=0;i<nSize;++i)
	{
		if (lstFiles.at(i).isDir())
		{
			//skip current and parent directory:
			if (lstFiles.at(i).fileName()==".") continue;
			if (lstFiles.at(i).fileName()=="..") continue;

			//Get all files from folder:
			if (bRecursiveFolders)
				nTotalBytes+=GetDiskUsage(lstFiles.at(i).absoluteFilePath(),true);
		}
		else
		{
			nTotalBytes+=lstFiles.at(i).size();
		}
	}

	return nTotalBytes;
}

bool getFreeTotalSpace(const QString& sDirPath, double& fTotal, double& fFree)
{
#ifdef Q_OS_WIN
	//Windows code
	QString sCurDir = QDir::current().absolutePath();
	QDir::setCurrent( sDirPath );
	
	ULARGE_INTEGER free,total;
	bool bRes = ::GetDiskFreeSpaceExA( 0 , &free , &total , NULL );
	if ( !bRes ) return false;

	QDir::setCurrent( sCurDir );

	fFree = static_cast<double>( static_cast<__int64>(free.QuadPart) );
	fTotal = static_cast<double>( static_cast<__int64>(total.QuadPart) );

#else //linux

	struct stat stst;
	struct statfs stfs;

	if ( ::stat(sDirPath.toLocal8Bit(),&stst) == -1 ) return false;
	if ( ::statfs(sDirPath.toLocal8Bit(),&stfs) == -1 ) return false;

	fFree = stfs.f_bavail * ( stst.st_blksize );
	fTotal = stfs.f_blocks * ( stst.st_blksize );

#endif // _WIN32

	return true;
}

qint64	DataHelper::GetDiskFreeSpaceBytes(QString strDrive)
{
	double fTotal, fFree;
	if(getFreeTotalSpace(strDrive, fTotal, fFree)){
		return fFree;
	}
	return -1;	//error
}

bool DataHelper::RemoveAllFromDirectory(QString strFolderPath, bool bRecursiveFolders, bool bRemoveDirectory)
{
	QDir dir(strFolderPath);
	if (!dir.exists())
		return false;

	QFileInfoList	lstFiles=dir.entryInfoList();
	int nSize=lstFiles.size();
	for(int i=0;i<nSize;++i)
	{
		if (lstFiles.at(i).isDir())
		{
			//skip current and parent directory:
			if (lstFiles.at(i).fileName()==".") continue;
			if (lstFiles.at(i).fileName()=="..") continue;

			//Get all files from folder:
			bool bOK=true;
			if (bRecursiveFolders)
				bOK=RemoveAllFromDirectory(lstFiles.at(i).absoluteFilePath(),true,true);
			if (!bOK)
				return false;
		}
		else
		{
			bool bOK=QFile::remove(lstFiles.at(i).absoluteFilePath());
			if (!bOK)
				return false;
		}
	}

	if (bRemoveDirectory)
		return dir.rmdir(strFolderPath);

	return true;
}

QList<QString> DataHelper::GetSubDirectories(QString strRootPath)
{
	QList<QString> lstDirs;

	QDir dir(strRootPath);
	if (!dir.exists())
		return lstDirs;

	QFileInfoList	lstFiles=dir.entryInfoList();
	int nSize=lstFiles.size();
	for(int i=0;i<nSize;++i)
	{
		if (lstFiles.at(i).isDir())
		{
			if (lstFiles.at(i).fileName()==".") continue;
			if (lstFiles.at(i).fileName()=="..") continue;
			lstDirs.append(lstFiles.at(i).fileName());
		}
	}

	return lstDirs;
}

//strApplicationName can be shorten or full exe name (no path)
QString DataHelper::Registry_GetApplicationPathFromRegistry(QString strApplicationName)
{
	strApplicationName=Registry_MapApplicationNameToExeName(strApplicationName);
	QStringList lstKeys=Registry_EnumerateSubKeys("HKEY_CLASSES_ROOT","Applications");

	QString strApplicationKey;
	int nSize=lstKeys.size();
	for(int i=0;i<nSize;++i)
	{
		if (lstKeys.at(i).toUpper().indexOf(strApplicationName.toUpper(),Qt::CaseInsensitive)!=-1)
		{
			strApplicationKey=lstKeys.at(i);
			break;
		}
	}

	if (strApplicationKey.isEmpty())
		return "";


	//FETCH REGISTRY VALUE: for "open", "read", "execute" values
	QString strValue;
	QString strKey;
	strApplicationKey.replace("/","\\");
	strKey="Applications\\"+strApplicationKey;

	if(!Registry_GetKeyValue("HKEY_CLASSES_ROOT",strKey+"\\shell\\open\\command",strValue))
		if(!Registry_GetKeyValue("HKEY_CLASSES_ROOT",strKey+"\\shell\\read\\command",strValue))
			if(!Registry_GetKeyValue("HKEY_CLASSES_ROOT",strKey+"\\shell\\edit\\command",strValue))
				if(!Registry_GetKeyValue("HKEY_CLASSES_ROOT",strKey+"\\shell\\execute\\command",strValue))

	if (strValue.isEmpty())
		return "";

	strValue=strValue.trimmed();

	//extract path:
	int index=strValue.toUpper().indexOf(".EXE");
	if (index==-1) 	return "";
	strValue=strValue.left(index+4);
	strValue.replace("\"",""); //destry quotes if exists
	return strValue;
}


//try to set fully  name, if not, return same...brb....this is stupid
QString DataHelper::Registry_MapApplicationNameToExeName(QString strApplicationName)
{
	strApplicationName=strApplicationName.toUpper();

	//map half names to fully quilifed names:
	if (strApplicationName.indexOf("WORDPAD",Qt::CaseInsensitive)!=-1)
	{
		strApplicationName="WORDPAD.EXE";
	}
	//if (strApplicationName.indexOf("WORD",Qt::CaseInsensitive)!=-1)
	//{
	//	strApplicationName="WINWORD.EXE";
	//}
	else if (strApplicationName.indexOf("EXCEL",Qt::CaseInsensitive)!=-1)
	{
		strApplicationName="EXCEL.EXE";
	}
	else if (strApplicationName.indexOf("POWERPOINT",Qt::CaseInsensitive)!=-1)
	{
		strApplicationName="POWERPNT.EXE";
	}
	else if (strApplicationName.indexOf("FIREFOX",Qt::CaseInsensitive)!=-1)
	{
		strApplicationName="FIREFOX.EXE";
	}
	else if (strApplicationName.indexOf("FRONTPAGE",Qt::CaseInsensitive)!=-1)
	{
		strApplicationName="FRONTPG.EXE";
	}
	else if (strApplicationName.indexOf("ACROBAT",Qt::CaseInsensitive)!=-1)
	{
		strApplicationName="AcroRD32.exe";
	}
	else if (strApplicationName.indexOf("WINZIP",Qt::CaseInsensitive)!=-1)
	{
		strApplicationName="Winzip32.exe";
	}
	else if (strApplicationName.indexOf("IRFAN",Qt::CaseInsensitive)!=-1)
	{
		strApplicationName="i_view32.exe";
	}
	else if (strApplicationName.indexOf("PAINT",Qt::CaseInsensitive)!=-1)
	{
		strApplicationName="mspaint.exe";
	}

	return strApplicationName;

}

//strRootKey is HKEY_CLASSES_ROOT,HKEY_CURRENT_USER or other: 
//strKey is in format: "mailto\\shell\\open\\command"
QStringList DataHelper::Registry_EnumerateSubKeys(QString strRootKey,QString strKey)
{
	QSettings settings(strRootKey+"\\"+strKey,	QSettings::NativeFormat);
	QStringList lst=settings.allKeys();
	QStringList lstOut;
	int nSize=lst.size();
	for(int i=0;i<nSize;i++)
	{
		QString strKey=lst.at(i);
		int nPos=strKey.indexOf("/");
		if (nPos>=0)
			strKey=strKey.left(nPos);
		lstOut.append(strKey);
	}
	return lstOut;
}

//get default key value
bool DataHelper::Registry_GetKeyValue(QString strRootKey,QString strKey,QString &strValue)
{
	QSettings settings(strRootKey, QSettings::NativeFormat);
	if (settings.contains(strKey))
	{
		strValue = settings.value(strKey).toString();
	}
	else
	{
		QSettings settingsD(strRootKey+"\\"+strKey,	QSettings::NativeFormat);
		if (!settingsD.contains(".")) //default key
			return false;
		strValue = settingsD.value(".").toString();
	}
	return true;
}


bool DataHelper::Registry_SetKeyValue(QString strRootKey,QString strKey,QString strValue)
{
	QSettings settings(strRootKey,	QSettings::NativeFormat);
	settings.setValue(strKey,strValue);
	return true;
}

//get home  dir:
//Windows: C:\My Documents And Seetings\User\Application Data
QString DataHelper::GetHomeDir()
{
	//issue 1619
	if (m_bIsPortable)
	{
		return  QCoreApplication::applicationDirPath();
	}

#ifdef _WIN32
		QString strAppDir=qgetenv("APPDATA");
		QDir testDir(strAppDir);
		if (!testDir.exists())
		{
			strAppDir=QDir::homePath();
			testDir.setPath(strAppDir);
		}
		return testDir.path();
#else
		QDir testDir=QDir::homePath();
		return testDir.path();
#endif

	return "";
}

//get home  dir:
//Windows: C:\My Documents And Seetings\User\Application Data\Sokrates Communicator
QString DataHelper::GetApplicationHomeDir()
{
	//issue 1619
	if (m_bIsPortable)
	{
		return  QCoreApplication::applicationDirPath();
	}


#if defined _WIN32 && !defined WINCE
		QString strAppDir=qgetenv("APPDATA");
		QDir testDir(strAppDir);
		if (!testDir.exists())
		{
			strAppDir=QDir::homePath();
			testDir.setPath(strAppDir);
		}
		if (!testDir.exists())
		{
			strAppDir=QCoreApplication::applicationDirPath();
			testDir.setPath(strAppDir);
		}

		Q_ASSERT(QCoreApplication::applicationName()!=""); //must be set

		//test if our app dir exists
		if(!testDir.exists(QCoreApplication::applicationName()))
			if(!testDir.mkdir(QCoreApplication::applicationName()))
				return "";

		return testDir.path()+"/"+QCoreApplication::applicationName();
#else

	#if __APPLE__
		QString strAppDir=QDir::homePath()+"/Library/Application Support";
		QDir testDir(strAppDir);
		if (!testDir.exists())
		{
			strAppDir=QDir::homePath();
			testDir.setPath(strAppDir);
		}
		//test if our app dir exists
		if(!testDir.exists(QCoreApplication::applicationName()))
			if(!testDir.mkdir(QCoreApplication::applicationName()))
				return "";

		return testDir.path()+"/"+QCoreApplication::applicationName();
	#else
		QString strAppDir=QDir::homePath();
		QDir testDir(strAppDir);
		if (!testDir.exists())
		{
			strAppDir=QCoreApplication::applicationDirPath();
			testDir.setPath(strAppDir);
		}
		//test if our app dir exists
		if(!testDir.exists(QCoreApplication::applicationName()))
			if(!testDir.mkdir(QCoreApplication::applicationName()))
				return "";

		return testDir.path()+"/"+QCoreApplication::applicationName();
	#endif

#endif

return "";
	
}


QString DataHelper::GetAppDataDir()
{
	//Do not change this with portable flags!!!!
#ifdef _WIN32
		QString strAppDir=qgetenv("APPDATA");
		QDir testDir(strAppDir);
		if (!testDir.exists())
		{
			strAppDir=QDir::homePath();
			testDir.setPath(strAppDir);
		}
		return testDir.path();
#else
		QDir testDir=QDir::homePath();
		return testDir.path();
#endif

	return "";
}

QString DataHelper::EncodeFileName(QString strFileName)
{

	//remove whites:
	strFileName=strFileName.simplified();

	//remove punct (:,.)
	int nSize=strFileName.size();
	for(int i=0;i<nSize;++i)
	{
		if (strFileName.at(i).isPunct())
			strFileName[i]='_';
	}
	return strFileName;

}


//copy all files from 1 to 2 (not overwrite)
bool DataHelper::CopyFolderContent(QString strSource,QString strTarget, bool bCopySubfolders)
{
	//read all files from backup:
	QDir dirSource(strSource);
	if (!dirSource.exists())
	{
		return false;
	}

	QDir dirTarget(strTarget);
	if (!dirTarget.exists())
	{
		return false;
	}


	//Algorithm finds first free slot: it will iterate 9999 times (max) before exiting...
	QFileInfoList	lstFiles=dirSource.entryInfoList();
	int nSize=lstFiles.size();
	for(int i=0;i<nSize;++i)
	{
		QString fileName=lstFiles.at(i).fileName();

		if (lstFiles.at(i).isFile())
		{
			QFile file(lstFiles.at(i).filePath());
			//if(!file.copy(strTarget+"/"+fileName))
			file.copy(strTarget+"/"+fileName);
				//return false;
		}
		else if(bCopySubfolders && 
				lstFiles.at(i).isDir() && 
				fileName != "." &&
				fileName != "..")
		{
			//recursively copy the 
			dirTarget.mkdir(fileName);
			CopyFolderContent(strSource + "/" + fileName, strTarget + "/" + fileName, true);
		}
	}

	return true;
}

QString DataHelper::GetFormatedFileSize(double dblFileSize, int nDecimalPlaces, bool bSkipByteMark)
{
	double dblTmp = 0;
	QString strStr;
	
	if (dblFileSize/1024 < 1024)
	{
		if (!bSkipByteMark)
			strStr = QString::number(dblFileSize/1024, 'f', nDecimalPlaces) + " kB";
		else
			strStr = QString::number(dblFileSize/1024, 'f', nDecimalPlaces);
	}
	else if (dblFileSize >= 1024 && ((dblFileSize/1024)/1024 < 1024))
	{
		if (!bSkipByteMark)
			strStr = QString::number((dblFileSize/1024)/1024, 'f', nDecimalPlaces) + " MB";
		else
			strStr = QString::number((dblFileSize/1024)/1024, 'f', nDecimalPlaces);
	}
	else 
	{
		if (!bSkipByteMark)
			strStr = QString::number(((dblFileSize/1024)/1024)/1024, 'f', nDecimalPlaces) + " GB";
		else
			strStr = QString::number(((dblFileSize/1024)/1024)/1024, 'f', nDecimalPlaces);
	}

	return strStr;
}



bool DataHelper::IsUserAdmin(QString strUsername)
{
#ifdef _WIN32
	/*
		DWORD dwLevel = 2;
		LPUSER_INFO_2 pBuf = NULL;
		NET_API_STATUS nStatus;
		LPCWSTR in1=NULL;
		
		//
		// Call the NetUserGetInfo function; specify level 10.
		//
		nStatus = NetUserGetInfo(in1,
			(LPCWSTR)strUsername.utf16(),
			dwLevel,
			(LPBYTE *)&pBuf);
		//
		// If the call succeeds, print the user information.
		//
		if (nStatus == NERR_Success)
		{
			if (pBuf != NULL)
			{
				if (pBuf->usri2_priv==USER_PRIV_ADMIN)
					return true;
			}
		}
*/
#endif
	return false;
}

//replace drive letter with drive letter on which application is started
QString	DataHelper::ReplaceCurrentDriveInPath(QString strPath)
{
	strPath=QDir::toNativeSeparators(strPath);
	static QString strAppDrive=QDir::toNativeSeparators(QCoreApplication::applicationDirPath()).left(QDir::toNativeSeparators(QCoreApplication::applicationDirPath()).indexOf(QDir::separator())).trimmed();
	int nPos=strPath.indexOf(QDir::separator());
	if (nPos>=0)
		return QDir::toNativeSeparators(strAppDrive+strPath.mid(nPos));
	else
		return strPath;
}

QString DataHelper::ExtractTextFromHTML(QString strHtml)
{
	QRegExp rx("<body[^>]*>");
	rx.setCaseSensitivity(Qt::CaseInsensitive);
	int i = strHtml.indexOf(rx);
	strHtml = strHtml.remove(0, i);
	QRegExp rx1("</body>");
	rx1.setCaseSensitivity(Qt::CaseInsensitive);
	i = strHtml.indexOf(rx1);
	strHtml = strHtml.remove(i, strHtml.length());

	//Remove style tags.
	QRegExp rx3("<style[^>]*>");
	QRegExp rx4("</style>");
	do {
		int s1 = strHtml.indexOf(rx3);
		int s2 = strHtml.indexOf(rx4);
		if (s1>0)
		{
			strHtml = strHtml.remove(s1, s2-s1+QString("</style>").length());
		}
	} while (strHtml.indexOf(rx3)>0);

	//Remove comment tags.
	//strHtml = strHtml.replace(QRegExp("/(\[if\s.*?\].*?\[endif\]\s*-->/)"), "");

/*	QRegExp rx5("<!--\[if[^>]*>");
	QRegExp rx6("endif\]-->");
	do {
		int s1 = strHtml.indexOf(rx5);
		int s2 = strHtml.indexOf(rx6);
		if (s1>0)
		{
			strHtml = strHtml.remove(s1, s2-s1+QString("-->").length());
		}
	} while (strHtml.indexOf(rx5)>0 && (strHtml.indexOf(rx5)<strHtml.indexOf(rx6)));*/

	//Remove comment tags.
	QRegExp rx7("<!--[^>]*>");
	QRegExp rx8("-->");
	do {
		int s1 = strHtml.indexOf(rx7);
		int s2 = strHtml.indexOf(rx8);
		if (s1>0)
		{
			strHtml = strHtml.remove(s1, s2-s1+QString("-->").length());
		}
	} while (strHtml.indexOf(rx7)>0 && (strHtml.indexOf(rx7)<strHtml.indexOf(rx8)));

	QRegExp rx2("<[^>]*>");
	rx2.setCaseSensitivity(Qt::CaseInsensitive);
	strHtml = strHtml.replace(rx2, "");

	return strHtml;
/*
	strTmp = strTmp.right(strTmp.indexOf("<body", 0, Qt::CaseInsensitive));
	
	QDomDocument doc;
	doc.setContent(strHtml);
	strTmp = doc.elementsByTagName("BODY").at(0).toElement().text();

	return strTmp;
*/
}

QString DataHelper::ReturnCommGridTextFromHTML(QString strText)
{
	QRegExp rx2("<[^>]*>");
	if(strText.startsWith("<html") || strText.startsWith("<!DOCTYPE") || strText.contains(rx2))
	{
		strText = DataHelper::ExtractTextFromHTML(strText);
	}

	return strText;
}

const char* DataHelper::GetCompileDate()
{
	int month, day, year;
	const char *months[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug",
		"Sep", "Oct", "Nov", "Dec"};
	char temp [100] = __DATE__;
	static char result [100] = "";


	year = atoi(temp + 7);
	*(temp + 6) = 0;
	day = atoi(temp + 4);
	*(temp + 3) = 0;

	for (int i = 0; i < 12; i++)
	{
		if (!strcmp(temp, months[i]))
		{
			month = i + 1;
			break;
		}
	}

	//create the
	sprintf(result, "%02d.%02d.%04d", day, month, year);
	return result;
}


//one way encoding: chars: * . " / \ [ ] : ; | = 
QString DataHelper::EncodeSession2Directory(QString strSession)
{
	strSession=strSession.replace("*","a");
	strSession=strSession.replace(".","b");
	strSession=strSession.replace("\"","c");
	strSession=strSession.replace("/","d");
	strSession=strSession.replace("\\","e");
	strSession=strSession.replace("[","f");
	strSession=strSession.replace("]","g");
	strSession=strSession.replace(":","h");
	strSession=strSession.replace(";","i");
	strSession=strSession.replace("|","j");
	strSession=strSession.replace("=","k");
	return strSession;
}


//uses input date or current date if input date is invalid, from= monday, to= sunday
void DataHelper::GetCurrentWeek(QDate input,QDate &from, QDate &to)
{
	QDate currDate=QDate::currentDate();
	if(input.isValid())
		currDate=input;
	int nDayOfWeek=currDate.dayOfWeek()-1;
	from=currDate.addDays(-nDayOfWeek);
	to=from.addDays(6);
}

void DataHelper::GetPartOfRecordSet(DbRecordSet &lstData,int nFromN, int nToN)
{
	int nCount=lstData.getRowCount();
	if (nCount==0)
		return;

	//MP requested and it does make sense: limit on list rowcount
	if (nToN>=nCount)
	{
		nToN=lstData.getRowCount()-1;
	}

	if (nFromN>=nCount)
		return;

	if (nFromN<0 && nToN<0)
		return;

	lstData.clearSelection();
	if (nFromN==nToN) //MP: if same return that 1
	{
		lstData.selectRow(nFromN);
		lstData.deleteUnSelectedRows();
		return;
	}
	
	

	if (nFromN<0)nFromN=0; //fix to enter loop

	if (nFromN>=0 && nToN>nFromN)
	{
		//check range boundaries:
		int nDif=nToN-nFromN;
		if(nDif>0)
		{
			if (nFromN>=nCount-1)
			{
				nFromN=nCount-nDif;
				if (nFromN<0)nFromN=0;
				nToN=nCount-1;
			}
			if (nFromN<0) nFromN=0;
			if (nToN>nCount-1)
				nToN=nCount-1;
			
			for (int i=nFromN;i<=nToN;i++)
			{
				lstData.selectRow(i);
			}
			lstData.deleteUnSelectedRows();
		}
	}
}

//get part of list by letters from strFromLetter (include) and to strToLetter (exclude), on of can be empty..
void DataHelper::GetPartOfRecordSet(DbRecordSet &lstData,QString strColumn, QString strFromLetter, QString strToLetter)
{
	int nCount=lstData.getRowCount();
	if (nCount==0)
		return;

	if (strFromLetter.isEmpty() && strToLetter.isEmpty())
		return;

	int nidxStart=0;
	lstData.clearSelection();
	bool bFind=false;

	//find start:
	if (!strFromLetter.isEmpty())
	{
		for (int i=0;i<nCount;i++)
		{
			nidxStart=i;
			int nRes=QString::localeAwareCompare(lstData.getDataRef(i,strColumn).toString().left(1).toLower(),strFromLetter);
			if (nRes>=0)
			{
				bFind=true;
				break;
			}
		}
		if (!bFind) //only if all not match then return empty list:
		{
			lstData.clear();
			return;
		}
	}
	
	//h -> (m,p)
    //h -> (a,d)

	//go to TO letter but not include
	if (!strToLetter.isEmpty())
	{
		for (int i=nidxStart;i<nCount;i++)
		{
			int nRes=QString::localeAwareCompare(lstData.getDataRef(i,strColumn).toString().left(1).toLower(),strToLetter);
			if (nRes>=0)
			{
				break;
			}
			lstData.selectRow(i);
		}
	}
	else
	{
		for (int i=nidxStart;i<nCount;i++)
		{
			lstData.selectRow(i);
		}
	}


	lstData.deleteUnSelectedRows();
}


void DataHelper::ReencodeStringToUnicode(const QString &strHtml,QString &strHtmlEncoded)
{
	strHtmlEncoded.clear();
	int nSize=strHtml.size();
	for (int i=0;i<nSize;i++)
	{
		QChar ch=strHtml.at(i);
		if(strHtml.at(i).unicode()>127)
			strHtmlEncoded.append("&#"+QString::number(strHtml.at(i).unicode())+";");
		else
			strHtmlEncoded.append(ch);
	}

}

void DataHelper::ReencodeStringToUnicodeHex(const QString &strHtml,QString &strHtmlEncoded)
{
	strHtmlEncoded.clear();
	int nSize=strHtml.size();
	for (int i=0;i<nSize;i++)
	{
		QChar ch=strHtml.at(i);
		int nUnicode=strHtml.at(i).unicode();
		if(nUnicode<65 || nUnicode>122)
		{
			QString chX=QString("%%1").arg(strHtml.at(i).unicode(),0,16);
			strHtmlEncoded.append(chX.toUpper());
		}
		else
			strHtmlEncoded.append(ch);
	}
}

void DataHelper::ReencodeStringToUnicodeHexMime(const QString &strHtml,QString &strHtmlEncoded)
{
	strHtmlEncoded.clear();
	strHtmlEncoded="=?iso-8859-1?Q?";
	int nSize=strHtml.size();
	for (int i=0;i<nSize;i++)
	{
		QChar ch=strHtml.at(i);
		int nUnicode=strHtml.at(i).unicode();
		if(nUnicode>122)
		{
			QString chX=QString("=%1").arg(strHtml.at(i).unicode(),0,16);
			strHtmlEncoded.append(chX.toUpper());
		}
		else if (nUnicode==32)
		{
			strHtmlEncoded.append("_");
		}
		else
			strHtmlEncoded.append(ch);
	}

	strHtmlEncoded=strHtmlEncoded+"?=";
}



#define  MAIL_LINE_LENGTH 73
//base64 header to 76 char/line:
QByteArray DataHelper::EncodeBase64To73LineLength(QByteArray &byteBase64)
{
	QByteArray data_out;
	int nSize=byteBase64.size();
	data_out.reserve(nSize);

	int i=0;
	while(i<nSize)
	{
		data_out.append(byteBase64.mid(i,MAIL_LINE_LENGTH));
		data_out.append("\r\n");
		i+=MAIL_LINE_LENGTH; 
	}

	return data_out;
}





//on windows: my docs, on linux same as applicationhomedir
//Windows: C:\My Documents And Seetings\User\My Documents\Sokrates Communicator
QString	DataHelper::GetMyDocumentsDir()
{
	//issue 1619: if portable return app dir
	if (DataHelper::IsPortableVersion())
	{
		return  QCoreApplication::applicationDirPath();
	}

#if defined _WIN32 && !defined WINCE

	//get MY DOCUMENTS:
	QString strAppDir;
	TCHAR buf[MAX_VALUE_LENGTH];
	if(SHGetSpecialFolderPath(NULL,buf,CSIDL_PERSONAL,true))
		strAppDir=QString::fromUtf16((ushort*)buf);//, wsstrlen(buf));

	QDir testDir(strAppDir);
	if (!testDir.exists())
		return DataHelper::GetApplicationHomeDir();
	else
	{
		//test if our app dir exists
		if(!testDir.exists(QCoreApplication::applicationName()))
			if(!testDir.mkdir(QCoreApplication::applicationName()))
				return "";
		return testDir.path()+"/"+QCoreApplication::applicationName(); //return doc path
	}

#else
	return DataHelper::GetApplicationHomeDir();
#endif
}

QString	DataHelper::GetAllUserApplicationData()
{
	QString strAppDir;
	
#if defined _WIN32 && !defined WINCE

	//get MY DOCUMENTS:
	TCHAR buf[MAX_VALUE_LENGTH];
	if(SHGetSpecialFolderPath(NULL,buf,CSIDL_COMMON_APPDATA,true))
		strAppDir=QString::fromUtf16((ushort*)buf);//, wsstrlen(buf));

#else 
  #ifdef __APPLE__ 
	//hardcore path:
	strAppDir = "/Users/Shared/";
  #endif
#endif


	QDir testDir(strAppDir);
	if (testDir.exists())
	{
		//test if our app dir exists
		if(!testDir.exists(QCoreApplication::applicationName()))
			if(!testDir.mkdir(QCoreApplication::applicationName()))
				return "";
		return testDir.path()+"/"+QCoreApplication::applicationName(); //return doc path
	}


	return strAppDir;
}



QString	DataHelper::GetFirstCdRomDrive()
{
	QString strResult;

#if defined _WIN32 && !defined WINCE // WINCE

	TCHAR szDrives[128];
	TCHAR *pDrive;

	DWORD r = GetLogicalDriveStrings(sizeof(szDrives), szDrives);
	if(r = 0 || r > sizeof(szDrives)) return strResult;

	pDrive = szDrives;  // Point to the first drive
	while(*pDrive != '\0')
	{
		if(DRIVE_CDROM == GetDriveType(pDrive))
		{
			strResult = QString::fromUtf16((const ushort *)pDrive);
			return strResult;
		}
		pDrive += 4;  // Point to the next drive
	}
#else
	QFileInfoList drives=QDir::drives();
	if (drives.count()>0)
	{
		strResult=drives.at(drives.count()-1).absoluteFilePath(); //return last
	}

#endif // WINCE

	return strResult;
}

void DataHelper::DateRangeSelectorPreviousNextButtonClicked(QDate &datFrom, QDate &datTo, int nRange, int nStep)
{
	int nDiff = datFrom.daysTo(datTo);

	switch(nRange){
		case UNDEFINED:	// "-"
			if (nStep>0)
			{
				datFrom = datFrom.addDays(nDiff+1);
				datTo = datTo.addDays(nDiff+1);
			}
			else
			{
				datFrom = datFrom.addDays(-nDiff-1);
				datTo = datTo.addDays(-nDiff-1);
			}
			break;
		case DAY: // "Day"
			if (nStep>0)
			{
				datFrom = datFrom.addDays(1);
				datTo = datTo.addDays(1);
			}
			else
			{
				datFrom = datFrom.addDays(-1);
				datTo = datTo.addDays(-1);
			}
			//datTo = datFrom; 
			//datTo = datTo.addDays(1);
			break;
		case WEEK: // "Week"
			{
				int nWeekday = datFrom.dayOfWeek();
				if (nStep>0)
				{
					datFrom = datFrom.addDays(7-nWeekday+1); 
					datTo = datFrom; 
					datTo = datTo.addDays(6);
				}
				else
				{
					datFrom = datFrom.addDays(-(7+nWeekday-1)); 
					datTo = datFrom; 
					datTo = datTo.addDays(6);
				}
			}
			break;
		case TWO_WEEK: // "Two Week"
			{
				int nWeekday = datFrom.dayOfWeek();
				if (nStep>0)
				{
					datFrom = datFrom.addDays(14-nWeekday+1); 
					datTo = datFrom; 
					datTo = datTo.addDays(13);
				}
				else
				{
					datFrom = datFrom.addDays(-(14+nWeekday-1)); 
					datTo = datFrom; 
					datTo = datTo.addDays(13);
				}
			}
			break;
		case MONTH: // "Month"
			{
				if (nStep>0)
				{
					datFrom = datFrom.addMonths(1);
					datTo = datFrom;
					datTo = datTo.addMonths(1);
					datTo = datTo.addDays(-1);

					/*datFrom = datFrom.addMonths(-1);
					if(datFrom.day() > 1)
						datFrom = datFrom.addDays(-(datFrom.day()-1));
					datTo = datFrom;
					int nDaysInMonth = datFrom.daysInMonth();
					datTo = datTo.addDays(nDaysInMonth-1);*/
				}
				else
				{
					datFrom = datFrom.addMonths(-1);
					datTo = datFrom;
					datTo = datTo.addMonths(1);
					datTo = datTo.addDays(-1);

					/*datFrom = datFrom.addMonths(1);
					if(datFrom.day() > 1)
						datFrom = datFrom.addDays(-(datFrom.day()-1));
					datTo = datFrom;
					int nDaysInMonth = datFrom.daysInMonth();
					datTo = datTo.addDays(nDaysInMonth-1);*/
				}
			}
			break;
		case YEAR: // "Year" - move to the start of the prev year
			{
				if (nStep>0)
				{
					datFrom = datFrom.addYears(1);
					datTo = datFrom;
					datTo = datTo.addYears(1);
					datTo = datTo.addDays(-1);

					/*int nYear = datFrom.year() + 1;
					datFrom = QDate(nYear, 1, 1);
					datTo = QDate(nYear, 12, 31); */
				}
				else
				{
					datFrom = datFrom.addYears(-1);
					datTo = datFrom;
					datTo = datTo.addYears(1);
					datTo = datTo.addDays(-1);

					/*int nYear = datFrom.year() - 1;
					datFrom = QDate(nYear, 1, 1);
					datTo = QDate(nYear, 12, 31); */
				}
			}
			break;
		case THREE_DAYS: // "3 days"
			{
				if (nStep>0)
				{
					datFrom = datFrom.addDays(1);
					datTo = datTo.addDays(1);
				}
				else
				{
					datFrom = datFrom.addDays(-1);
					datTo = datTo.addDays(-1);
				}
			}
			break;
		case BUSS_DAYS: // "Business Days"
			{
				int nWeekday = datFrom.dayOfWeek();
				if (nStep>0)
				{
					datFrom = datFrom.addDays(7-nWeekday+1); 
					datTo = datFrom; 
					datTo = datTo.addDays(4);
				}
				else
				{
					datFrom = datFrom.addDays(-(7+nWeekday-1)); 
					datTo = datFrom; 
					datTo = datTo.addDays(4);
				}
			}
			break;
		case SEVEN_DAYS: // "Seven Days"
			{
				int nWeekday = datFrom.dayOfWeek();
				if (nStep>0)
				{
					datFrom = datFrom.addDays(7); 
					datTo = datFrom; 
					datTo = datTo.addDays(6);
				}
				else
				{
					datFrom = datFrom.addDays(-7); 
					datTo = datFrom; 
					datTo = datTo.addDays(6);
				}
			}
			break;
		case FOURTEEN_DAYS: // "Seven Days"
			{
				int nWeekday = datFrom.dayOfWeek();
				if (nStep>0)
				{
					datFrom = datFrom.addDays(14);
					datTo = datFrom;
					datTo = datTo.addDays(13);
				}
				else
				{
					datFrom = datFrom.addDays(-14);
					datTo = datFrom;
					datTo = datTo.addDays(13);
				}
			}
			break;
	}
}

void DataHelper::DateRangeSelectorCurrentButtonClicked(QDate &datFrom, QDate &datTo, int nRange)
{
	int nDiff = datFrom.daysTo(datTo);
	switch(nRange){
		case UNDEFINED:	// "-" (preserve range ending on today)
			datTo = QDate::currentDate();
			datFrom = datTo;
			datFrom = datFrom.addDays(-nDiff);
			break;
		case DAY: // "Day"
			datFrom = QDate::currentDate();
			datTo = datFrom; 
			break;
		case WEEK: // "Week" (current week)
			{
				datFrom = QDate::currentDate();
				int nWeekday = datFrom.dayOfWeek();
				datFrom = datFrom.addDays(-nWeekday+1); 
				datTo = datFrom; 
				datTo = datTo.addDays(6);
			}
			break;
		case TWO_WEEK: // "Two Week" (from current week)
			{
				datFrom = QDate::currentDate();
				int nWeekday = datFrom.dayOfWeek();
				datFrom = datFrom.addDays(-nWeekday+1); 
				datTo = datFrom; 
				datTo = datTo.addDays(13);
			}
			break;
		case MONTH: // "Month"
			{
				datFrom = QDate::currentDate();
				if(datFrom.day() > 1)
					datFrom = datFrom.addDays(-(datFrom.day()-1));
				datTo = datFrom;
				int nDaysInMonth = datFrom.daysInMonth();
				datTo = datTo.addDays(nDaysInMonth-1);
			}
			break;
		case YEAR: // "Year" - move to the start of the this year
			datFrom = QDate::currentDate();
			datFrom = QDate(datFrom.year(), 1, 1);
			datTo = QDate(datFrom.year(), 12, 31); 
			break;
		case THREE_DAYS: // "3 days"
			datFrom = QDate::currentDate();
			datTo = datFrom.addDays(2); 
			break;
		case BUSS_DAYS: // "Business Days"
			{
				datFrom = QDate::currentDate();
				int nWeekday = datFrom.dayOfWeek();
				datFrom = datFrom.addDays(-nWeekday+1); 
				datTo = datFrom; 
				datTo = datTo.addDays(4);
			}
			break;
		case SEVEN_DAYS: // "Seven Days"
			{
				datFrom = QDate::currentDate();
				datTo = datFrom; 
				datTo = datTo.addDays(6);
			}
			break;
		case FOURTEEN_DAYS: // "Fourteen Days"
			{
				datFrom = QDate::currentDate();
				datTo = datFrom;
				datTo = datTo.addDays(13);
			}
			break;
	}
}

void DataHelper::CalendarRangeSelectorClicked(QDate &datFrom, QDate &datTo, int nRange)
{
	int nDiff = datFrom.daysTo(datTo);
	switch(nRange){
		case UNDEFINED:	// "-" (preserve range ending on today)
			datFrom = datTo;
			datFrom = datFrom.addDays(-nDiff);
			break;
		case DAY: // "Day"
			datTo = datFrom; 
			break;
		case WEEK: // "Week" (current week)
			{
				int nWeekday = datFrom.dayOfWeek();
				datFrom = datFrom.addDays(-nWeekday+1); 
				datTo = datFrom; 
				datTo = datTo.addDays(6);
			}
			break;
		case TWO_WEEK: // "Two Weeks" (from current week)
			{
				int nWeekday = datFrom.dayOfWeek();
				datFrom = datFrom.addDays(-nWeekday+1); 
				datTo = datFrom; 
				datTo = datTo.addDays(13);
			}
			break;
		case MONTH: // "Month"
			{
				if(datFrom.day() > 1)
					datFrom = datFrom.addDays(-(datFrom.day()-1));
				datTo = datFrom;
				int nDaysInMonth = datFrom.daysInMonth();
				datTo = datTo.addDays(nDaysInMonth-1);
			}
			break;
		case YEAR: // "Year" - move to the start of the this year
			datFrom = QDate(datFrom.year(), 1, 1);
			datTo = QDate(datFrom.year(), 12, 31); 
			break;
		case THREE_DAYS: // "3 days"
			datTo = datFrom.addDays(2); 
			break;
		case BUSS_DAYS: // "Business Days"
			{
				int nWeekday = datFrom.dayOfWeek();
				datFrom = datFrom.addDays(-nWeekday+1); 
				datTo = datFrom; 
				datTo = datTo.addDays(4);
			}
			break;
		case SEVEN_DAYS: // "Seven Days"
			{
				datTo = datFrom; 
				datTo = datTo.addDays(6);
			}
			break;
		case FOURTEEN_DAYS: // "Seven Days"
			{
				datTo = datFrom;
				datTo = datTo.addDays(13);
			}
			break;
	}
}

void DataHelper::GetToFromDateInDateDialog(QDate &datFrom, QDate &datTo, int &nRange, bool bTo)
{
	switch(nRange){
		case UNDEFINED:	// "-" (preserve range ending on today)
			if (datTo<datFrom)
			{
				if (bTo)
				{
					datTo=datFrom;
				}
				else
				{
					datFrom=datTo;
				}
			}
			break;
		case DAY: // "Day"
			{
				if (bTo)
				{
					datTo = datFrom; 
				}
				else
				{
					datFrom = datTo; 
				}
			}
			break;
		case WEEK: // "Week" (current week)
			{
				if (bTo)
				{
					int nWeekday = datFrom.dayOfWeek();
					datFrom = datFrom.addDays(-nWeekday+1); 
					datTo = datFrom; 
					datTo = datTo.addDays(6);
				}
				else
				{
					int nWeekday = datFrom.dayOfWeek();
					datFrom = datFrom.addDays(-nWeekday+1); 
					datFrom = datTo.addDays(-6);
				}
			}
			break;
		case TWO_WEEK: // "Two Weeks" (from current week)
			{
				if (bTo)
				{
					int nWeekday = datFrom.dayOfWeek();
					datFrom = datFrom.addDays(-nWeekday+1); 
					datTo = datFrom; 
					datTo = datTo.addDays(13);
				}
				else
				{
					int nWeekday = datFrom.dayOfWeek();
					datFrom = datFrom.addDays(-nWeekday+1); 
					datTo = datFrom; 
					datFrom = datTo.addDays(-13);
				}
			}
			break;
		case MONTH: // "Month"
			{
				if (bTo)
				{
					if(datFrom.day() > 1)
					datFrom = datFrom.addDays(-(datFrom.day()-1));
					datTo = datFrom;
					int nDaysInMonth = datFrom.daysInMonth();
					datTo = datTo.addDays(nDaysInMonth-1);
				}
				else
				{
					if(datFrom.day() > 1)
					datFrom = datFrom.addDays(-(datFrom.day()-1));
					datTo = datFrom;
					int nDaysInMonth = datTo.daysInMonth();
					datFrom = datTo.addDays(-nDaysInMonth+1);
				}
			}
			break;
		case YEAR: // "Year" - move to the start of the this year
			{
				if (bTo)
				{
					datTo = datFrom.addYears(1);
					//datFrom = QDate(datFrom.year(), 1, 1);
					//datTo = QDate(datFrom.year(), 12, 31); 
				}
				else
				{
					datFrom = datTo.addYears(-1);
					//			datFrom = QDate(datFrom.year(), 1, 1);
					//			datTo = QDate(datFrom.year(), 12, 31); 
				}
			}
			break;
		case THREE_DAYS: // "3 days"
			{
				if (bTo)
				{
					datTo = datFrom.addDays(2); 
				}
				else
				{
					datFrom = datTo.addDays(-2);
				}
			}
			break;
		case BUSS_DAYS: // "Business Days"
			{
				if (bTo)
				{
					int nWeekday = datFrom.dayOfWeek();
					datFrom = datFrom.addDays(-nWeekday+1); 
					datTo = datFrom; 
					datTo = datTo.addDays(4);
				}
				else
				{
					int nWeekday = datTo.dayOfWeek();
					datFrom = datTo.addDays(-nWeekday+1); 
					datTo = datFrom; 
					datTo = datTo.addDays(4);
				}
			}
			break;
		case SEVEN_DAYS: // "Seven Days"
			{
				if (bTo)
				{
					datTo = datFrom; 
					datTo = datTo.addDays(-6);
				}
				else
				{
					datFrom = datTo;
					datFrom = datFrom.addDays(6);
				}
			}
			break;
		case FOURTEEN_DAYS: // "Seven Days"
			{
				if (bTo)
				{
					datTo = datFrom;
					datTo = datTo.addDays(-13);
				}
				else
				{
					datFrom = datTo;
					datFrom = datFrom.addDays(13);
				}
			}
			break;
	}
}

void DataHelper::getDateSqlString(QString &strSQLWhere, QString strColName, QString strColValue1, QString strColValue2)
{
	QDate date=ParseDateString(strColValue1);
	if(date.isNull())
		strSQLWhere="";			//return empty

	QDate date1=ParseDateString(strColValue2);
	if (date1.isNull())
	{
		date1=date;
	}

	strSQLWhere+=strColName+" >= '"+date.toString("yyyy.MM.dd")+"' AND "+strColName+" <= '"+date1.toString("yyyy.MM.dd")+"' ";
}

void DataHelper::getCharacterSqlString(QString &strSQLWhere, QString strColName, QString strColValue1, QString strColValue2, bool bIgnoreCase)
{
	if(bIgnoreCase)
	{
		//Issue #2620 for these fields make full search.
		if (strColName=="BCMI_ADDRESS" || strColName=="BCME_ADDRESS" || strColName=="BCNT_DESCRIPTION" || strColName=="BCMA_FORMATEDADDRESS" || strColName=="BCNT_LOCATION")
		{
			strSQLWhere+="LOWER("+strColName+") LIKE '%"+strColValue1.toLower()+"%'";
		}
		else if (strColName=="BCMA_ZIP" || strColName=="BCMD_DEBTORCODE" || strColName=="BCMD_DEBTORACCOUNT")
		{
			strSQLWhere+="LOWER("+strColName+") >= '"+strColValue1.toLower()+"' AND LOWER("+strColName+") <= '"+strColValue2.toLower();
			//B.T. issue 2691: when searching string with <> any string longer then searched value is involved so char length must be specified
			//e.e. if sql= BCMA_ZIP>1000 and BCMA_ZIP<2000, result will contain records with BCMA_ZIP=10000
			if (strColValue1.length()!=strColValue2.length())
			{
				strSQLWhere+="' AND CHAR_LENGTH("+strColName+")>="+QString::number(strColValue1.length()); 
				strSQLWhere+="' AND CHAR_LENGTH("+strColName+")<="+QString::number(strColValue2.length());
			}
			else
			{
				strSQLWhere+="' AND CHAR_LENGTH("+strColName+")="+QString::number(strColValue2.length());
			}
		}
		else
		{
			strSQLWhere+="LOWER("+strColName+") LIKE '"+strColValue1.toLower()+"%'";
		}
	}
	else
	{
		if (strColName=="BCMI_ADDRESS" || strColName=="BCME_ADDRESS" || strColName=="BCNT_DESCRIPTION" || strColName=="BCMA_FORMATEDADDRESS" || strColName=="BCNT_LOCATION")
		{
			strSQLWhere+=strColName+" LIKE '%"+strColValue1+"%'";
		}
		else if (strColName=="BCMA_ZIP" || strColName=="BCMD_DEBTORCODE" || strColName=="BCMD_DEBTORACCOUNT")
		{
			strSQLWhere+=strColName+" >= '"+strColValue1+"' AND "+strColName+" <= '"+strColValue2; //B.T.: lower is not needed when case is relevant!!!
			//B.T. issue 2691: when searching string with <> any string longer then searched value is involved so char length must be specified
			//e.e. if sql= BCMA_ZIP>1000 and BCMA_ZIP<2000, result will contain records with BCMA_ZIP=10000
			if (strColValue1.length()!=strColValue2.length())
			{
				strSQLWhere+="' AND CHAR_LENGTH("+strColName+")>="+QString::number(strColValue1.length()); 
				strSQLWhere+="' AND CHAR_LENGTH("+strColName+")<="+QString::number(strColValue2.length());
			}
			else
			{
				strSQLWhere+="' AND CHAR_LENGTH("+strColName+")="+QString::number(strColValue2.length());
			}
		}
		else
		{
			strSQLWhere+=strColName+" LIKE '"+strColValue1+"%'";
		}
	}
}

//get local time zone offset from UTC (minutes): e.g. for CRO in summer value is -120 (- to substract from local time to get to UTC time)..
int	DataHelper::GetTimeZoneOffsetUTC()
{
	QDateTime dt1 = QDateTime::currentDateTime();
	QDateTime dt2 = dt1.toUTC();
	dt1.setTimeSpec(Qt::UTC);
	int offset = dt1.secsTo(dt2) / 60;

	return offset;
}




bool DataHelper::LoadFile(QString strInputFile, QByteArray &buffer)
{
	QFile file(strInputFile);	
	if (file.open(QIODevice::ReadOnly))
	{
		buffer = file.readAll();
		file.close();
		return true;
	}
	else{
		return false;
	}
}

void DataHelper::DateRangeSelectorPreviousButtonClicked(QDate &datFrom, QDate &datTo, int nRange)
{

}
	
void DataHelper::DateRangeSelectorNextButtonClicked(QDate &datFrom, QDate &datTo, int nRange)
{

}

void DataHelper::GetToDateInDateDialog(QDate &datFrom, QDate &datTo, int nRange)
{

}
	
void DataHelper::GetFromDateInDateDialog(QDate &datFrom, QDate &datTo, int nRange)
{

}

bool DataHelper::LoadFile(QString strInputFile, QString &buffer)
{
	QFile file(strInputFile);	
	if (file.open(QIODevice::ReadOnly))
	{
		buffer = QString(file.readAll());
		file.close();
		return true;
	}
	else{
		return false;
	}
}

bool DataHelper::WriteFile(QString strFile, QByteArray &buffer)
{

	QFile file(strFile);	
	if (file.open(QIODevice::WriteOnly))
	{
		file.write(buffer);
		file.close();
		return true;
	}
	else{
		return false;
	}

}




void DataHelper::getCharacterSqlStringCustomFld(QString &strSQLWhere, QString strColName, QString strColValue1, QString strColValue2, bool bIgnoreCase)
{
	if(bIgnoreCase)
	{
		strSQLWhere+="LOWER("+strColName+") LIKE '%"+strColValue1.toLower()+"%'";
	}
	else
	{
		strSQLWhere+=strColName+" LIKE '%"+strColValue1+"%'";

	}
}
bool compareSortAsc( const QString &w1 , const QString &w2 )
{
	return w1 < w2;
}
bool compareSortDesc( const QString &w1 , const QString &w2 )
{
	return w1 > w2;
}
void DataHelper::SortList(QStringList &lstData, bool bAsc)
{
	if (bAsc)
		qStableSort(lstData.begin(),lstData.end(),compareSortAsc);
	else
		qStableSort(lstData.begin(),lstData.end(),compareSortDesc);
}



QString DataHelper::GetFormatedTime(quint64 nMsec)
{
	if (nMsec/1000 < 60)
	{
		return QString::number(nMsec/1000)+"s";
	}
	else if (nMsec/(60*1000) < 60)
	{
		return QString::number(nMsec/(60*1000))+"m";
	}
	else
	{
		int nHours= nMsec/(60*60*1000);
		int nMinutes= (nMsec%(60*60*1000));
		nMinutes= nMinutes/(60*1000);
		return QString::number(nHours)+"h  "+QString::number(nMinutes)+"m";
	}

}

QTime DataHelper::GetTimeFromSeconds( quint64 nSeconds )
{
	if (nSeconds < 60)
	{
		return QTime(0,0,nSeconds);
	}
	else if (nSeconds/60 < 60)
	{
		int nMinutes=nSeconds/60;
		int nSec=nSeconds-nMinutes*60;
		return QTime(0,nMinutes,nSec);
	}
	else
	{
		int nHours= nSeconds/(60*60);
		int nMinutes=nSeconds/60;
		int nSec=nSeconds-nHours*60*60-nMinutes*60;
		return QTime(nHours,nMinutes,nSec);
	}
}

