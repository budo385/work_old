#ifndef SERVERTHREAD_H 
#define SERVERTHREAD_H

#include <QThread>

/*!
    \class ServerThread
    \brief Base class for every thread on server side
    \ingroup Thread

	Used for user context operation, every user is recognized by its thread or thread ID.
	Thread is defined by m_nThreadID. Programmer is responsible to generate unique ThreadId. 
	Default value is -1.

	Not using Qt::HANDLE currentThreadId () because its platfom dependent
	Use:
	\code
	pointer =dynamic_cast <ServerThread*>QThread::currentThread()
	if(pointer==NULL) 
		error("Thread has no user context!");
	else
		ThreadID=pointer->GetThreadID();"
	\endcode


*/
class ServerThread : public QThread
{ 
	Q_OBJECT

public:
	ServerThread(QObject *parent=NULL):QThread(parent){m_nThreadID=-1;};
	int GetThreadID(){return m_nThreadID;};

protected:
	void SetThreadID(int intThreadID){ m_nThreadID=intThreadID;};

private:
	int m_nThreadID;
};


#endif //SERVERTHREAD_H
