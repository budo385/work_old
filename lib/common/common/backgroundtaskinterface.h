#ifndef BACKGROUNDTASKINTERFACE_H
#define BACKGROUNDTASKINTERFACE_H

#include <QString>

/*!
	\class BackgroundTaskInterface
	\brief Abstract class used as interface for executing task in separate thread
	\ingroup Common

	
	Use:
	- Any object can be run in separate thread (background)
	- Inherit this class and implement ExecuteTask() method
	- Object must be multithread safe
	- Register and run it in the BackgroundTaskExecutor
	

*/
class BackgroundTaskInterface 
{
public:
	virtual ~BackgroundTaskInterface(){};
	virtual void ExecuteTask(int nCallerID=-1,int nTaskID=-1,QString strData="")=0;
};

#endif // BACKGROUNDTASK_H
