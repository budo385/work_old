#ifndef APPSERVERCONFIGURATOR_H
#define APPSERVERCONFIGURATOR_H


//to configure service for appserver: to implement on linux, inherit and reimplement all fucntions
//service is installed by name:  m_strServiceName - m_strServiceDescriptor

#include "common/common/status.h"

class AppServerConfigurator
{
public:

	AppServerConfigurator();
	~AppServerConfigurator();

	//100% compatible API:
	virtual void	Stop(Status &pStatus);
	virtual void	Start(Status &pStatus);
	virtual bool	IsServiceRunning(QString strService="");
	virtual bool	IsServiceInstalled(QString strService="");
	virtual void	StartOrInstall(Status &pStatus, bool bAutoStart=false);
	virtual void	InstallService(Status &pStatus, bool bAutoStart=false);
	virtual void	UnInstallService(Status &pStatus);	
	virtual void	SetServiceUserAndPass(QString strUser, QString strPass){m_strUser=strUser;m_strPass=strPass;};
	virtual bool	AddUserLogonAsServicePrivilege(Status &pStatus,QString strUserName);

	virtual bool	IsAutoStart();
	virtual QString LastError();
	virtual QString GetServiceName();

	virtual void	SetAutoStart(bool bAuto);

	virtual bool	CreateUnInstallBat(bool bEnableFB_Deinstall=false);
	virtual bool	Mac_CreateStartUpBat(QString strFileName="");

	//DB
	virtual void	InstallFirebirdService(Status &pStatus);


	QString			GetDir(){return m_strAppSrvPath;};

	QString m_strAppSrvPath;
	QString m_strAppLogPath;
	QString m_strAppExeName;
	QString m_strServiceName;
	QString m_strServiceDescriptor;
	QString m_strUser;
	QString m_strPass;
};

#endif // APPSERVERCONFIGURATOR_H
