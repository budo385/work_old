#ifndef OBSERVER_PTRN_H
#define OBSERVER_PTRN_H

#include <QSet>
#include <QVariant>


class ObsrPtrn_Observer;

/*!
	\class  ObsrPtrn_Subject
	\ingroup Common
	\brief  Observer pattern - subject

	Use:
	- inherit it to register observers
*/
class ObsrPtrn_Subject
{
public:
	ObsrPtrn_Subject();
	virtual ~ObsrPtrn_Subject();

	static void globalUnregisterObserver(ObsrPtrn_Observer *pObj); //BT to prevent crash when observer is destroyed and subject holds invalid pointer
	static QList<ObsrPtrn_Subject*> &globalInitsubjectList();

	void registerObserver(ObsrPtrn_Observer *pObj);
	void unregisterObserver(ObsrPtrn_Observer *pObj);
	void unregisterAllObservers();

	void notifyObservers(int nMsgCode,int nMsgDetail=0,const QVariant val=QVariant(),ObsrPtrn_Observer const *pSender=NULL);

	void blockObserverSignals(bool bBlock=true){m_bBlockSignals=bBlock;}

private:
    QSet<ObsrPtrn_Observer *>  m_lstObservers;
	QSet<ObsrPtrn_Observer *>  m_lstObserversAppend;
	bool m_bBlockSignals;
};


/*!
	\class  ObsrPtrn_Observer
	\ingroup Common
	\brief  Observer pattern - observer

	Use:
	- inherit it to register it with ObsrPtrn_Subject and to receive events
*/
class ObsrPtrn_Observer
{
public:
	ObsrPtrn_Observer(){};
	virtual ~ObsrPtrn_Observer();

	virtual void updateObserver(ObsrPtrn_Subject* pSubject, int nMsgCode,int nMsgDetail=0,const QVariant val=QVariant()) = 0;
};


#endif // OBSERVER_PTRN_H
