#ifndef THREADSPAWNER_H
#define THREADSPAWNER_H

#include <QThread>
#include <QObject>
#include "common/common/threadsync.h"

//inherit from this one
class ThreadSpawnedObject : public QObject
{
public:
	ThreadSpawnedObject(QObject *parent=NULL):QObject(parent){};

};


class ThreadSpawner : public QThread
{
	Q_OBJECT

public:
	ThreadSpawner ():m_pThreadObject(NULL),sem(1){};
	virtual ~ThreadSpawner();
	virtual void Start();
	virtual void Stop();
	ThreadSpawnedObject* GetThreadSpawnedObject()
	{return m_pThreadObject;}

protected:
	void run(); //thread support
	virtual ThreadSpawnedObject* CreateThreadSpawnedObject()=0;
private:
	ThreadSpawnedObject* m_pThreadObject;
	QSemaphore sem;
	ThreadSynchronizer m_t_sync;
	
};

#endif // THREADSPAWNER_H
