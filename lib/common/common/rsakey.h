#ifndef RSA_KEY_H
#define RSA_KEY_H


#include <string>

//TOFIX get key fingerprint, key(s) to string, ...
// use: int RSA_blinding_on(RSA *rsa, BN_CTX *ctx);
// void RSA_blinding_off(RSA *rsa);
#define LOCAL_RSA_PKCS1_PADDING 1
// RSA operations
#define RSA_ENCRYPT	1
#define RSA_DECRYPT	2
#define RSA_SIGN	3
#define RSA_VERIFY	4

int pass_cb(char *buf, int size, int rwflag, void *u);

class RSA_Key
{
	friend int pass_cb(char *buf, int size, int rwflag, void *u);

public:
	RSA_Key();
	~RSA_Key();

	inline bool IsValid() { return NULL != m_pKey; };
	bool IsPublicKey();
	bool IsPrivateKey();

	int  GetKeySize();	// size in bytes

	void Clear();
	bool Generate(int nSize = 2048, int nExp = 65537);

	bool WritePublicKey(const char *szFile);
	bool WritePrivateKey(const char *szFile, const char *szPass = NULL);

	bool ReadPublicKey(const char *szFile, const char *szPass = NULL);
	bool ReadPrivateKey(const char *szFile, const char *szPass = NULL);

	bool ReadPublicKeyFromBuffer(const char *szBuffer, int nSize, const char *szPass = NULL);
	bool ReadPrivateKeyFromBuffer(const char *szBuffer, int nSize, const char *szPass = NULL);

	//encrypt content of any length
	int  Sign(const char *szSrc, int nLenSrc, char *szDst, int nPadding = LOCAL_RSA_PKCS1_PADDING);
	int  Verify(const char *szSrc, int nLenSrc, char *szDst, int nPadding = LOCAL_RSA_PKCS1_PADDING);
	int  Encrypt(const char *szSrc, int nLenSrc, char *szDst, int nPadding = LOCAL_RSA_PKCS1_PADDING);
	int  Decrypt(const char *szSrc, int nLenSrc, char *szDst, int nPadding = LOCAL_RSA_PKCS1_PADDING);

	int  GetError();

protected:
	int  Operation(int nOp, const char *szSrc, int nLenSrc, char *szDst, int nPadding = LOCAL_RSA_PKCS1_PADDING);

protected:
	void *m_pKey;
	std::string	m_strPass;	// temporary storage
};


class OpenSSLHelper
{
	public:
		static int  Extern_RAND_bytes(unsigned char *buf,int num);
};

#endif // RSA_KEY_H
