/*!
\class  EncryptedIniFile
\author MR
\brief  This class provides access to encrypted INI file

File is encrypted using AES (Rijndael) algorithm. Additionaly
INI file format has a header storing format name and its version,
password hash value (SHA256) and original file size.

*/

#ifndef ENCRYPTEDINIFILE_H
#define ENCRYPTEDINIFILE_H

#include "inifile.h"

//TOFIX define custom errors!, GetErrorCode()

class EncryptedIniFile : public IniFile
{
public:
	EncryptedIniFile();
	~EncryptedIniFile();

	virtual bool Load();
	virtual bool Save();
	virtual bool LoadEnc();
	virtual bool SaveEnc();

	static bool AES_EncryptBinaryData(const QString strPassword,const QByteArray &byteDecoded,QByteArray &byteEncoded,int &nOriginalDataSize);
	static bool AES_DecryptBinaryData(const QString strPassword,const QByteArray &byteEncoded,QByteArray &byteDecoded,const int nOriginalDataSize=-1);


	void SetPassword(const char *szPassword);
	void SetPassword(QString strPassword);

protected:
	std::string	m_strPassword;
	QString m_strPasswordQ;
};

#endif // ENCRYPTEDINIFILE_H
