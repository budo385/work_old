#ifndef DATETIMEHANDLER_H
#define DATETIMEHANDLER_H

#include <QDateTime>

class DateTimeHandlerX 
{
public:

	static QDateTime currentDateTime();
	static QDateTime toUTC(const QDateTime &);
	static QDateTime fromUTC(const QDateTime &);

	static bool s_bUseUTCTimeZone;		//only used for currentdatetime
	static bool s_bClientSideUTC2LocalConvertEnabled;		//used for toUTC and fromUTC: client side must convert all values from server

private:
	
};

#endif // DATETIMEHANDLER_H
