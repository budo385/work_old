#ifndef LOGGERSYSLOG_H
#define LOGGERSYSLOG_H

#include "common/common/loggerabstract.h"



/*!
	\class LoggerSysLog
	\brief Abstract Logger Class
	\ingroup Bus_Core

	Class for logging into system event log (syslog/Linux or EventLog/Windows)
	Warning:
	- For Linux, whole message details must be written
	- For Windows, special message.dll must be registered into registry to properly work
*/
class LoggerSysLog : public LoggerAbstract
{
public:
    LoggerSysLog();
    ~LoggerSysLog();


	bool Initialize();			
	void logMessage (int nMsgType,int nMsgID, QString strMsgArguments="" );

private:
	//QtServiceBase * m_SysLog;
    
};

#endif // LOGGERSYSLOG_H
