#ifndef FUNCTOR_BASE_H
#define FUNCTOR_BASE_H

/*!
	\class  Template_Functor
	\ingroup Common
	\brief  use as Functor for callback functions of objects

	Use:
	- for every different function add own: constructor, function pt and callback
	- implemented:
	- bool (obj)->(int)
	- void (obj)->void();

*/
template <class T>
class Template_Functor
{
public:
	Template_Functor(T *who,void (T::*func)(void)):callee(who),callback1(func){}
	Template_Functor(T *who,bool (T::*func)(int)):callee(who),callback2(func){}
	  void Call_voidFn_void() //void (T::*func)(void)
		{  (callee->*callback1)();  }
	  bool Call_boolFn_int(int p1)//bool (T::*func)(int)
	  {  (return callee->*callback2)(p1);  }

private:
	T *callee;
	void (T::*callback1)(void);
	bool (T::*callback2)(int);
};



#endif // EVENTFILTER_INPUT_H