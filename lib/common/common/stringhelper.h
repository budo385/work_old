#ifndef STRINGHELPER_H
#define STRINGHELPER_H

#include <QString>

class StringHelper 
{

public:
	static QString RemovSpecialChars(QString &strInput);
	static QString ExtractQuoteContent(QString &strInput, QString strQuoteChar="'",int nFrom=0);
    
};

#endif // STRINGHELPER_H
