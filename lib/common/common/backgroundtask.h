#ifndef BACKGROUNDTASK_H
#define BACKGROUNDTASK_H

#include <QObject>
#include <QThread>
#include <QTimerEvent>
#include <QDebug>
#include "common/common/threadsync.h"
#include "backgroundtaskinterface.h"


/*!
	\class BackgroundTask
	\brief Abstract class providing framework for starting tasks in separate (background thread)
	\ingroup Common

	- Task can be executed once (nTimeInterval=0) (task execution is delayed until thread 1 returns to loop)
	- Task execution can be delayed (nTimeInterval<>0 && bPeriodical=false
	- Task execution can be repeately executed (nTimeInterval<>0 && bPeriodical=true)


	Used inside BackgroundTaskExecutor as object in another thread
*/
class BackgroundTask : public QObject
{
	Q_OBJECT

public:
	BackgroundTask(BackgroundTaskInterface *pTask,int nTimeInterval=0, bool bPeriodical=true);
    ~BackgroundTask();

public slots:
	virtual void Start();									//	used by Executor to start task
	virtual void Stop();									//	used by Executor to stop task
	virtual void StartNow(int nCallerID=-1,int nTaskID=-1,QString strData="");	//	regardless of time interval, executes task
	virtual void OnDelete();								//	block all requests, delete itself

signals:
	void TaskFinished();		// use this signal to notify master thread that TaskWasExecuted

protected:
	void timerEvent(QTimerEvent *event); //overriden QObject timer method

	int m_nTimeInterval;
	int m_nTimerID;
	bool m_bPeriodical;
	bool m_bTaskFinished;
	BackgroundTaskInterface* m_pTask;
	
    
};

#endif // BACKGROUNDTASK_H
