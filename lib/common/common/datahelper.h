#ifndef DATAHELPER_H
#define DATAHELPER_H


#include <QString>
#include <QDate>
#include <QHash>
#include "common/common/dbrecordset.h"


/*!
	\class DataHelper
	\brief 
	\ingroup Bus_Core
*/
class DataHelper 
{
public:

	enum DateRanges
	{
		UNDEFINED	= 0,
		DAY			= 1,
		WEEK		= 2,
		MONTH		= 3,
		YEAR		= 4,
		THREE_DAYS	= 5,
		BUSS_DAYS	= 6,
		TWO_WEEK	= 7,
		SEVEN_DAYS	= 8,
		FOURTEEN_DAYS= 9
	};

	enum DataOperations
	{
		OP_ADD,
		OP_REMOVE,
		OP_REPLACE,
		OP_INTERSECT,
		OP_EDIT,		//this edits existing and adds new ones if they not exists
		OP_EDIT_ONLY	//this edits existing only
	};

	static QString	GetFilterBasedOnFieldType(DbView &view,QString strColName, QString strColValue1, QString strColValue2, bool bIgnoreCase=false);
	static QDate	ParseDateString(QString strColValue);
	static void		DataOperation(int nOperation,DbRecordSet *lstNewData, DbRecordSet *lstData,  DbRecordSet *lstDataResultForWrite=NULL,DbRecordSet *lstDataResultForDelete=NULL, int nPrimaryKeyIndex=0,int nPrimaryKeyIndexNewData=-1);
	static void		RemoveDuplicates(DbRecordSet *lstDataToCheck, DbRecordSet *lstPossibleDuplicates, int nLstDataToCheckIDIdx=0,int nLstDPossibleDuplicatesIDIdx=0);
	static QString	ResolveFilePath(QString strPath,QString &strFileName,QString &strExtension);
	static QString	getSQLColumnsFromRecordSet(DbRecordSet &record);
	static void		GetPartOfRecordSet(DbRecordSet &lstData,int nFromN, int nToN);
	static void		GetPartOfRecordSet(DbRecordSet &lstData,QString strColumn, QString strFromLetter="", QString strToLetter="");

	//file & dir operations:
	static void			ParseFilesFromURLs(QList<QUrl> &lst,DbRecordSet &lstPaths, DbRecordSet &lstApps, bool bRecursiveFolders=true,bool bAcceptApplications=false);
	static void			GetFilesFromFolder(QString strFolderPath,DbRecordSet &lstPaths, DbRecordSet &lstApps, bool bRecursiveFolders=true, bool bUseRelativeFilePath=false,QString strRelativeRootPath="");
	static QString		EncodeFileName(QString strFileName);
	static qint64		GetDiskUsage(QString strFolderPath, bool bRecursiveFolders=true);
	static qint64		GetDiskFreeSpaceBytes(QString strDrive);
	static bool 		CopyFolderContent(QString strSource,QString strTarget, bool bCopySubfolders = false);
	static QString		GetFormatedFileSize(double dblFileSize, int nDecimalPlaces=3, bool bSkipByteMark=false);
	static bool 		RemoveAllFromDirectory(QString strFolderPath, bool bRecursiveFolders=true, bool bRemoveDirectory=false);
	static QList<QString> GetSubDirectories(QString strRootPath);
	static QString		EncodeSession2Directory(QString strSession);
	static bool			LoadFile(QString strInputFile, QByteArray &buffer);
	static bool			LoadFile(QString strInputFile, QString &buffer);
	static bool			WriteFile(QString strFile, QByteArray &buffer);
	static QString		GetFormatedTime(quint64 nMsec);
	static QTime		GetTimeFromSeconds(quint64 nSec);

	//WIN32/MAC specific:
	static QString		GetAllUserApplicationData();
	//WIN32 specific:
	static QString		GetMyDocumentsDir();
	static QString		GetFirstCdRomDrive();

	//timezone:
	static int			GetTimeZoneOffsetUTC();
	
	//windows registry helpers (only if _WIN32):
	static QString		Registry_GetApplicationPathFromRegistry(QString strApplicationName);
	static QString		Registry_MapApplicationNameToExeName(QString strApplicationName);
	static QStringList	Registry_EnumerateSubKeys(QString strRootKey,QString strKey);
	static bool			Registry_GetKeyValue(QString strRootKey,QString strPathToValue,QString &strValue);
	static bool			Registry_SetKeyValue(QString strRootKey,QString strPathToValue,QString strValue);
	static bool			IsUserAdmin(QString strUsername);

	//sarch for home path of application: create dir SOKRATES Communicator if not found, store all data there....
	static QString		GetApplicationHomeDir();
	static QString		GetHomeDir();
	static	bool		IsPortableVersion(){return m_bIsPortable;};
	static	void		SetPortableVersion(bool bPortable){m_bIsPortable=bPortable;};
	static QString		GetAppDataDir();
	static QString		ReplaceCurrentDriveInPath(QString strPath);
	static QString		ExtractTextFromHTML(QString strHtml);
	static QString		ReturnCommGridTextFromHTML(QString strText);
	static const char*	GetCompileDate();
	static void			GetCurrentWeek(QDate input,QDate &from, QDate &to);
	static void			ReencodeStringToUnicode(const QString &strHtml,QString &strHtmlEncoded);
	static void			ReencodeStringToUnicodeHex(const QString &strHtml,QString &strHtmlEncoded);
	static void			ReencodeStringToUnicodeHexMime(const QString &strHtml,QString &strHtmlEncoded);
	
	//encode/decode:
	static QByteArray	EncodeBase64To73LineLength(QByteArray &byteBase64);
	
	//Date selector and calendar time selector.
	static void DateRangeSelectorPreviousNextButtonClicked(QDate &datFrom, QDate &datTo, int nRange, int nStep);
	static void GetToFromDateInDateDialog(QDate &datFrom, QDate &datTo, int &nRange, bool bTo);
	static void DateRangeSelectorCurrentButtonClicked(QDate &datFrom, QDate &datTo, int nRange);
	static void CalendarRangeSelectorClicked(QDate &datFrom, QDate &datTo, int nRange);
	static void GetDateRangeSelectorClicked(QDate datFrom, QDate datTo, int &nRange);

	static void getDateSqlString(QString &strSQLWhere, QString strColName, QString strColValue1, QString strColValue2);
	static void getCharacterSqlString(QString &strSQLWhere, QString strColName, QString strColValue1, QString strColValue2, bool bIgnoreCase);
	static void getCharacterSqlStringCustomFld(QString &strSQLWhere, QString strColName, QString strColValue1, QString strColValue2, bool bIgnoreCase);

	static void DateRangeSelectorPreviousButtonClicked(QDate &datFrom, QDate &datTo, int nRange);
	static void DateRangeSelectorNextButtonClicked(QDate &datFrom, QDate &datTo, int nRange);
	static void GetToDateInDateDialog(QDate &datFrom, QDate &datTo, int nRange);
	static void GetFromDateInDateDialog(QDate &datFrom, QDate &datTo, int nRange);

	static void SortList(QStringList &lstData, bool bAsc=true);
private:

	static bool			m_bIsPortable;

};

#endif // DATAHELPER_H
