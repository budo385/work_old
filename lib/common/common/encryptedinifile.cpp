#include "encryptedinifile.h"
#include "rijndael.h"
#include "sha2.h"
#include <QFile>

EncryptedIniFile::EncryptedIniFile()
{
}

EncryptedIniFile::~EncryptedIniFile()
{
}

void EncryptedIniFile::SetPassword(const char *szPassword)
{
	m_strPassword = szPassword;
}

void EncryptedIniFile::SetPassword(QString strPassword)
{
	m_strPasswordQ = strPassword;
	m_strPassword = strPassword.toLatin1().constData();
}

bool EncryptedIniFile::Load()
{
	Clear();

	if(m_strPassword.size() == 0)
		return false;

	//open INI file for reading
	QFile iniFile(m_strFilePath);
	if (!iniFile.open(QIODevice::ReadOnly))
		return false;

	//convert password into SHA256 hash block (used as encryption key)
	unsigned char szKey[32];
	sha256_ctx ctx;
	sha256_begin(&ctx);
	sha256_hash((const unsigned char *)m_strPassword.c_str(), m_strPassword.size(), &ctx);
	sha256_end((unsigned char *)szKey, &ctx);

	//calculate hash of a hash
	unsigned char szKeyHash[32];
	sha256_begin(&ctx);
	sha256_hash(szKey, 32, &ctx);
	sha256_end((unsigned char *)szKeyHash, &ctx);

	//read INI header
	char szHeader[41];
	iniFile.read(szHeader, 41);

	//test format signature - first 5 header bytes
	if(0 != memcmp("INI10", szHeader, strlen("INI10")))	
		return false;	// invalid signature

	//test if encryption key hashes match
	if(0 != memcmp(szKeyHash, szHeader+5, 32))
		return false;	// invalid password

	//TOFIX: int nOrigFileSize = *(int *)(szHeader + 37);

	Rijndael crypt;
	crypt.init(Rijndael::CBC, Rijndael::Decrypt, szKey, Rijndael::Key32Bytes);

	//NOTE: buffer must be multiple of 16 bytes (rijndael uses 16bytes=128bit blocks)
	const int BUFF_SIZE = 10*1024;
	//TOFIX: const int bsize = BUFF_SIZE*8;		// size in bits
	char szInBuf[BUFF_SIZE];
	char szOutBuf[BUFF_SIZE+16+1];		// one byte is for \0

	//calc file size
	int nFileSize = iniFile.size();

	//read from file in blocks
	while(1)
	{
		int nLastPos = iniFile.pos();
		int nToRead  = nFileSize-nLastPos; 
		if(nToRead <= 0)
			break;

		int nReadReq = BUFF_SIZE;
		if(nToRead < BUFF_SIZE)
			nReadReq = nToRead;

		iniFile.read(szInBuf, nReadReq);

		//calculate how much have we read
		int nCurPos = (int)iniFile.pos();
		int nRead = nCurPos - nLastPos;
		if(nRead > 0)
		{
			//decrypt read contents
			int len = crypt.blockDecrypt((const UINT8*)szInBuf, 8*nRead, (UINT8*)szOutBuf);
			if(len > 0)
			{
				int nBytes = len/8;
				szOutBuf[nBytes] = '\0'; 
				std::string strLine = szOutBuf;
				ParseIniLines(strLine);
			}
		}
		else
			break;
	}

	return true;
}

bool EncryptedIniFile::Save()
{
	if(m_strPassword.size() == 0)
		return false;

	if(!EnsurePathDirExists())
		return false;

	//open INI file for writing
	QFile iniFile(m_strFilePath);
	if (!iniFile.open(QIODevice::WriteOnly))
		return false;

	//convert password into SHA256 hash block (used as encryption key)
	unsigned char szKey[32];
	sha256_ctx ctx;
	sha256_begin(&ctx);
	sha256_hash((const unsigned char *)m_strPassword.c_str(), m_strPassword.size(), &ctx);
	sha256_end((unsigned char *)szKey, &ctx);

	//calculate hash of a hash
	unsigned char szKeyHash[32];
	sha256_begin(&ctx);
	sha256_hash(szKey, 32, &ctx);
	sha256_end((unsigned char *)szKeyHash, &ctx);

	//write INI header
	char szHeader[41];
	memset(szHeader, 0, sizeof(szHeader));
	memcpy(szHeader, "INI10", strlen("INI10"));	//format signature - 5 bytes
	memcpy(szHeader+5, szKeyHash, 32);
	//memcpy(szHeader+37, &nOrigFileSize, sizeof(nOrigFileSize));
	iniFile.write(szHeader, 41);

	//after header start writing encrypted contents
	Rijndael crypt;
	crypt.init(Rijndael::CBC, Rijndael::Encrypt, szKey, Rijndael::Key32Bytes);

	//NOTE: buffer must be multiple of 16 bytes (rijndael uses 16bytes=128bit blocks)
	const int BUFF_SIZE = 10*1024;
	//TOFIX: const int bsize = BUFF_SIZE*8;		// size in bits
	char szInBuf[BUFF_SIZE];
	char szOutBuf[BUFF_SIZE+16];

	//store entire INI content into the single string
	std::string strData;
	size_t nSections = m_data.size();
	for(size_t i=0; i<nSections; i++)
	{
		//write line with section name
		sprintf(szInBuf, "[%s]\n", m_data[i].m_name.c_str());
		strData += szInBuf;

		size_t nEntries = m_data[i].m_entries.size();
		for(size_t j=0; j<nEntries; j++)
		{
			//write "key = value"
			sprintf(szInBuf, "%s=%s\n",
				m_data[i].m_entries[j].m_name.c_str(),
				m_data[i].m_entries[j].m_value.c_str());

			strData += szInBuf;
		}

		strData += "\n";
	}

	//write string to file in encrypted blocks
	int nSize = strData.size();
	while(nSize > 0)
	{
		int nChunk = BUFF_SIZE;
		if(nSize < BUFF_SIZE)
			nChunk = nSize;

		memcpy(szInBuf, strData.c_str(), nChunk);

		//encrypt content block
		int len = 0;
		if(nSize < BUFF_SIZE)
			len = crypt.padEncrypt((const UINT8*)szInBuf, nChunk, (UINT8*)szOutBuf);
		else{
			len = crypt.blockEncrypt((const UINT8*)szInBuf, 8*nChunk, (UINT8*)szOutBuf);
			len = len/8;
		}

		if(len > 0)
		{
			iniFile.write(szOutBuf, len);
		}
		else
			break;

		nSize -= nChunk;
		strData = strData.substr(nChunk);
	}

	return true;
}



bool EncryptedIniFile::LoadEnc()
{
	Clear();

	//open INI file for reading
	QFile iniFile(m_strFilePath);
	if (!iniFile.open(QIODevice::ReadOnly))
		return false;

	QByteArray data_enc,buffer;
	data_enc=iniFile.readAll();
	if(!AES_DecryptBinaryData(m_strPasswordQ,data_enc,buffer))
		return false;


	QList<QByteArray> lstLines=buffer.split('\n');
	int nSize=lstLines.size();
	for(int i=0; i<nSize; i++)
	{
		std::string	strLine=lstLines.at(i).constData();
		ParseIniLine(strLine);
	}

	iniFile.close();
	return true;
}

bool EncryptedIniFile::SaveEnc()
{
	if(!EnsurePathDirExists())
		return false;

	QByteArray data;

	size_t nSections = m_data.size();
	for(size_t i=0; i<nSections; i++)
	{
		QByteArray buffer;

		buffer.append("[");
		buffer.append(m_data[i].m_name.c_str());
		buffer.append("]\n");

		size_t nEntries = m_data[i].m_entries.size();
		for(size_t j=0; j<nEntries; j++)
		{
			buffer.append(m_data[i].m_entries[j].m_name.c_str());
			buffer.append("=");
			buffer.append(m_data[i].m_entries[j].m_value.c_str());
			buffer.append("\n");
		}

		buffer.append("\n");
		data.append(buffer);
	}

	//data.append('0');

	
	QByteArray data_enc;
	int nFileSize;
	if(!AES_EncryptBinaryData(m_strPasswordQ,data,data_enc,nFileSize))
		return false;

	//open INI file for writing
	QFile iniFile(m_strFilePath);
	if (!iniFile.open(QIODevice::WriteOnly))
		return false;

	iniFile.write(data_enc);
	iniFile.close();

	return true;
}


bool EncryptedIniFile::AES_EncryptBinaryData(const QString strPassword,const QByteArray &byteDecoded,QByteArray &byteEncoded,int &nOriginalDataSize)
{
	byteEncoded.clear();
	int nBytesToProcess=byteDecoded.size();
	nOriginalDataSize=nBytesToProcess;

	if (nBytesToProcess == 0)
		return true;

	if (!byteDecoded.constData()) 
	{
		qWarning("SHA256_EncryptBinaryData: Data is null");
		return false;
	}

	//convert password into SHA256 hash block (used as encryption key)
	unsigned char szKey[32];
	sha256_ctx ctx;
	sha256_begin(&ctx);
	sha256_hash((const unsigned char *)strPassword.toLatin1().constData(), strPassword.size(), &ctx);
	sha256_end((unsigned char *)szKey, &ctx);

	//calculate hash of a hash
	unsigned char szKeyHash[32];
	sha256_begin(&ctx);
	sha256_hash(szKey, 32, &ctx);
	sha256_end((unsigned char *)szKeyHash, &ctx);

	//after header start writing encrypted contents
	Rijndael crypt;
	crypt.init(Rijndael::CBC, Rijndael::Encrypt, szKey, Rijndael::Key32Bytes);

	//NOTE: buffer must be multiple of 16 bytes (rijndael uses 16bytes=128bit blocks)
	const int BUFF_SIZE = 10*1024;
	QByteArray bufferIn;
	QByteArray bufferOut;
	bufferIn.resize(BUFF_SIZE+16);
	bufferOut.resize(BUFF_SIZE+16);

	//write bytearray encrypted
	int nPosition=0;
	while(nBytesToProcess > 0)
	{
		int nChunk = BUFF_SIZE;
		if(nBytesToProcess < BUFF_SIZE)
			nChunk = nBytesToProcess;

		bufferIn=byteDecoded.mid(nPosition,nChunk);

		//encrypt content block
		int len = 0;
		if(nBytesToProcess < BUFF_SIZE)
			len = crypt.padEncrypt((const unsigned char *)bufferIn.constData(), nChunk, (unsigned char *)bufferOut.data());
		else{
			len = crypt.blockEncrypt((const unsigned char *)bufferIn.constData(), 8*nChunk, (unsigned char *)bufferOut.data());
			len = len/8;
		}

		if(len > 0)
		{
			byteEncoded.append(bufferOut.constData(),len);
		}
		else
			break;

		nBytesToProcess -= nChunk;
		nPosition+=nChunk;
	}

	return true;

}
bool EncryptedIniFile::AES_DecryptBinaryData(const QString strPassword,const QByteArray &byteEncoded,QByteArray &byteDecoded,const int nOriginalDataSize)
{
	byteDecoded.clear();
	int nBytesToProcess=byteEncoded.size();

	if (nBytesToProcess == 0)
		return true;

	if (!byteEncoded.constData()) 
	{
		qWarning("SHA256_DecryptBinaryData: Data is null");
		return false;
	}

	//convert password into SHA256 hash block (used as encryption key)
	unsigned char szKey[32];
	sha256_ctx ctx;
	sha256_begin(&ctx);
	sha256_hash((const unsigned char *)strPassword.toLatin1().constData(), strPassword.size(), &ctx);
	sha256_end((unsigned char *)szKey, &ctx);

	//calculate hash of a hash
	unsigned char szKeyHash[32];
	sha256_begin(&ctx);
	sha256_hash(szKey, 32, &ctx);
	sha256_end((unsigned char *)szKeyHash, &ctx);

	Rijndael crypt;
	crypt.init(Rijndael::CBC, Rijndael::Decrypt, szKey, Rijndael::Key32Bytes);

	//NOTE: buffer must be multiple of 16 bytes (rijndael uses 16bytes=128bit blocks)
	const int BUFF_SIZE = 10*1024;

	QByteArray bufferIn;
	QByteArray bufferOut;
	bufferIn.resize(BUFF_SIZE+16);
	bufferOut.resize(BUFF_SIZE+16);
	int nPosition=0;

	while(1)
	{
		int nChunk = BUFF_SIZE;
		if(nBytesToProcess < BUFF_SIZE)
			nChunk = nBytesToProcess;

		bufferIn=byteEncoded.mid(nPosition,nChunk);
	
		//decrypt read contents
		int len = crypt.blockDecrypt((const unsigned char *)bufferIn.constData(), 8*nChunk, (unsigned char *)bufferOut.data());
		if(len > 0)
		{
			len = len/8;
			byteDecoded.append(bufferOut.constData(),len);

			nBytesToProcess -= nChunk;
			nPosition+=nChunk;
		}
		else
			break;

	}

	if (nOriginalDataSize>0)
		byteDecoded.resize(nOriginalDataSize);
	
	return true;
}



