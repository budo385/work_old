#include "stringhelper.h"


//remove new line, tab and other special chars:
QString StringHelper::RemovSpecialChars(QString &strInput)
{
	QString strClean;

	//remove all chars non numbers:
	int nSize=strInput.size();
	for(int i=0;i<nSize;++i)
	{
		if (strInput.at(i).isNumber() || strInput.at(i).isLetter() || strInput.at(i).isPunct() || strInput.at(i)==' ')
		{
			strClean+=strInput.at(i);
		}
	}
	return strClean;
}



QString StringHelper::ExtractQuoteContent(QString &strInput, QString strQuoteChar,int nFrom)
{
	int nStart=strInput.indexOf(strQuoteChar,nFrom);
	if (nStart<0) return "";
	int nEnd=strInput.indexOf(strQuoteChar,nStart+1);
	if (nEnd<=nStart+1) return "";
	return strInput.mid(nStart+1,nEnd-nStart-1); 
}
