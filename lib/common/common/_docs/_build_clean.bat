@ECHO OFF
REM 
REM Script to clean Atol project tree on windows (delete temporary files)
REM 	NOTE: requires some recent Windows (NT based)
REM

IF NOT "%OS%"=="Windows_NT" GOTO :End

rmdir /S /Q ..\Debug
rmdir /S /Q ..\Release
rmdir /S /Q ..\GeneratedFiles
rmdir /S /Q ..\Deleted



rmdir /S /Q ..\..\Debug
rmdir /S /Q ..\..\Release
rmdir /S /Q ..\..\GeneratedFiles
rmdir /S /Q ..\..\Deleted



del /Q ..\..\*.user
del /Q ..\*.user
del /Q ..\..\*.ncb
del /F /Q /A H ..\..\*.suo


:End
