#include "backgroundtaskexecutor.h"
#include "loggerabstract.h"
#include "statuscodeset.h"
#include "threadid.h"

/*!
	Slot is invoked for one-time exec tasks, when execution is over.
	Thread is stopped, if allowed, internal backgroundtask object is destroyed
	If allowed, external task object is also deleted.
	If allowed, this object is self deleted.
	This last two is handy for local instantantion and 'forget' about it method

	\param pTask					- external task object inherited from BackgroundTaskInterface
	\param parent					- parent object, can be used to automatically destroy this
	\param nTimeInterval			- interval (msec) in which ExecuteTask will be repeated, if 0, then once and stop
	\param bPeriodical				- if true then ExecuteTask() every  nTimeInterval, else once
	\param bDeleteTaskAfterFinish	- WARNING: pTask will be deleted after ExecuteTask() if bPeriodical=false (must be instanced by new)
	\param bDeleteSelfAfterFinish	- WARNING: this object will be deleted after ExecuteTask() if bPeriodical=false (handy for local object)
*/
BackgroundTaskExecutor::BackgroundTaskExecutor(BackgroundTaskInterface *pTask,QObject *parent,int nTimeInterval,bool bPeriodical, bool bDeleteTaskAfterFinish,bool bDeleteSelfAfterFinish)
:QThread(parent)
{
	m_nTimeInterval=nTimeInterval;
	m_pTask=pTask;
	m_bPeriodical=bPeriodical;
	m_bDeleteTaskAfterFinish=bDeleteTaskAfterFinish;
	m_bDeleteSelfAfterFinish=bDeleteSelfAfterFinish;
	m_nTimerID=-1;
}




//stop thread.> automatic delete of , destroy task if
BackgroundTaskExecutor::~BackgroundTaskExecutor()
{

	if(m_bDeleteTaskAfterFinish &&m_pTask!=NULL)
		delete m_pTask;

	//stop thread gently if running:
	if(isRunning())
	{
		quit();
		wait();
	}
}


//start Thread
void BackgroundTaskExecutor::run()
{

	//create task:
	BackgroundTask *pNewThreadTask= new BackgroundTask(m_pTask,m_nTimeInterval,m_bPeriodical);

	//connect signals:
	connect(this,SIGNAL(StartTask()),pNewThreadTask,SLOT(Start()));
	connect(this,SIGNAL(StartTaskNow(int,int,QString)),pNewThreadTask,SLOT(StartNow(int,int,QString)));
	connect(this,SIGNAL(StopTask()),pNewThreadTask,SLOT(Stop()));
	connect(this,SIGNAL(finished ()),pNewThreadTask,SLOT(OnDelete()));	//when this object is destroyed, destroy local TASK
	//connect(this,SIGNAL(finished ()),pNewThreadTask,SLOT(deleteLater()));	//when this object is destroyed, destroy local TASK
	//NOTE: calling right after finish, will keep alive this task object (deffered delete is not yet executed)
	connect(pNewThreadTask,SIGNAL(TaskFinished()),this,SLOT(Finished()));			

	m_ThreadSync.ThreadRelease();
	exec();
}




/*!
	Starts Task, waits until thread is started, 
	\param bStartTaskNow		- if true, task will be executed once, when another thread starts regardless of timeinterval
	\param nStartTaskAfterMSec	- if != 0 then task will start after N msecs
	WARNING if m_nTimeInterval=0, this flag is ignored, as it has same effect
*/
void BackgroundTaskExecutor::Start(bool bStartTaskNow,int nStartTaskAfterMSec)
{

	if (nStartTaskAfterMSec)
	{
		m_nTimerID=startTimer(nStartTaskAfterMSec);
		return;
	}

	//if thread is not running start it:
	if(!isRunning())
	{
		m_ThreadSync.ThreadSetForWait();	//prepare for wait until thread is started
		start(); //start thread
		m_ThreadSync.ThreadWait();			
	}		

	emit StartTask();					//thread started, fire event to start task

	if(bStartTaskNow && m_nTimeInterval!=0)	//fire to start task right now
		emit StartTaskNow(-1,-1,"");
}


void BackgroundTaskExecutor::StartNow(int nCallerID,int nTaskID,QString data)		//start will be scheduled: if object is destroyed in mean time->backup was in progress, skip
{
	if(!isRunning())
	{
		m_ThreadSync.ThreadSetForWait();	//prepare for wait until thread is started
		start(); //start thread
		m_ThreadSync.ThreadWait();			
	}
	emit StartTaskNow(nCallerID,nTaskID,data);
}

/*!
	Stops task, thread is kept running
*/
void BackgroundTaskExecutor::Stop()
{
	emit StopTask();
	if(isRunning())
	{
		quit();
		//wait();

		//BT: sometimes tasks are blocked: wait few seconds then terminate 'em
		bool bOK=wait(5000);		//wait 5sec
		if (!bOK)
		{
			ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_HTTP_THREAD_FAILED_TO_CLOSED,QString::number(ThreadIdentificator::GetCurrentThreadID()));
			terminate();
		}
	}
}


/*!
	Slot is invoked for one-time exec tasks, when execution is over.
	Thread is stopped, if allowed, internal backgroundtask object is destroyed
	If allowed, external task object is also deleted.
	If allowed, this object is self deleted.
	This last two is handy for local instantacion and 'forget' about it method

*/
void BackgroundTaskExecutor::Finished()
{

	//stop thread: 
	if(isRunning())
	{
		quit();
		wait();
	}

	//if set destroy
	if(m_bDeleteTaskAfterFinish)	
	{
		delete m_pTask;
		m_pTask=NULL;
	}

	//schedule this object for deletion
	if(m_bDeleteSelfAfterFinish)
		deleteLater();				
}


//overriden QObject timer method
void BackgroundTaskExecutor::timerEvent(QTimerEvent *event)
{
	if(event->timerId()==m_nTimerID)
	{
		killTimer(m_nTimerID);
		m_nTimerID=-1;
		Start(true); //start task now
	} 

}