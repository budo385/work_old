<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="1.1" language="de">
<context>
    <name>CsvImportFile</name>
    <message>
        <location filename="csvimportfile.cpp" line="44"/>
        <source>Failed to open the file!</source>
        <translation>Datei kann nicht geöffnet werden!</translation>
    </message>
    <message>
        <location filename="csvimportfile.cpp" line="190"/>
        <source>Invalid file format header!</source>
        <translation>Ungültiger Dateiformat-Header!</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="statuscodeset.cpp" line="21"/>
        <source>Unknown Error!</source>
        <translation>Unbekannter Fehler!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="21"/>
        <source>HTTP    </source>
        <translation type="obsolete">HTTP    </translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="21"/>
        <source>XMLRPC  </source>
        <translation type="obsolete">XMLRPC  </translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="21"/>
        <source>RPC     </source>
        <translation type="obsolete">RPC     </translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="46"/>
        <source>SQL     </source>
        <translation>SQL     </translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="49"/>
        <source>SYSTEM  </source>
        <translation>SYSTEM  </translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="52"/>
        <source>BUSINESS</source>
        <translation>BUSINESS</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="55"/>
        <source>COMMON  </source>
        <translation>COMMON  </translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="55"/>
        <source>Success    </source>
        <translation type="obsolete">Erfolg    </translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="66"/>
        <source>Error      </source>
        <translation>Fehler      </translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="69"/>
        <source>Warning    </source>
        <translation>Warnung    </translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="72"/>
        <source>Information</source>
        <translation>Information</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="97"/>
        <source>No Error</source>
        <translation>Kein Fehler</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="99"/>
        <source>XML parsing failed!</source>
        <translation>Parsen von XML misslungen!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="100"/>
        <source>XML RPC generation failed!</source>
        <translation>XML RPC -Generierung misslungen!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="101"/>
        <source>XML RPC wrong data type.XML parsing failed!</source>
        <translation>XML RPC mit falschem Datentpy. Parsen von XML misslungen!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="103"/>
        <source>XML parsing failed for parameter %1!</source>
        <translation>XML-Parsieren fehlgeschlagen für Parameter %1!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="104"/>
        <source>XML parsing failed for message body at line %1, column %2, error is: %3</source>
        <translation>XML-Parsieren fehlgeschlagen für Message Body in Linie %1, Splte %2, Fehler: %3</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="105"/>
        <source>XML parsing failed, parameter %1 is missing</source>
        <translation>XML-Parsieren fehlgeschlagen da Parameter %1 fehlt</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="106"/>
        <source>XML parsing failed, invalid XML document!</source>
        <translation>XML-Parsieren fehlgeschlagen da XML-Dokument ungültig!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="108"/>
        <source>Invalid NameSpace!</source>
        <translation>Ungültiger Namensraum (Namespace)!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="109"/>
        <source>Application server function dispatcher: Function not supported!</source>
        <translation>Application Server-Methoden-Dispatcher: Funktion nicht unterstützt!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="110"/>
        <source>Parameter count doesn&apos;t match!</source>
        <translation>Ungültige Parameter!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="111"/>
        <source>Invalid RPC request!</source>
        <translation>Ungültige RPC-Anforderung!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="113"/>
        <source>Connection already exists!</source>
        <translation>Verbindung besteht bereits!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="120"/>
        <source>Host not found!</source>
        <translation>Applikationsserver nicht gefunden!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="115"/>
        <source>Host closed connection!</source>
        <translation>Verbindung durch Applikationsserver abgebrochen!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="116"/>
        <source>Timeout error!</source>
        <translation>Fehler durch Zeitüberschreitung!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="117"/>
        <source>General socket error!</source>
        <translation>Allgemeiner Socket-Fehler!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="118"/>
        <source>Socket not connected!</source>
        <translation>Socket nicht verbunden!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="119"/>
        <source>Invalid HTTP message detected!</source>
        <translation>Ungültige HTTP-Nachricht festgestellt!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="121"/>
        <source>Server failed to start listener on IP address: %1, at port: %2, reason: %3!</source>
        <translation>Applikationsserver konnte keinen Listener installieren an der IP-Adresse: %1 auf Port: %2, Ursache: %3! </translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="122"/>
        <source>Server already running!</source>
        <translation>Applikationsserver läuft bereits!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="124"/>
        <source>Socket created: %1, ip: %2</source>
        <translation>Socket erzeugt: %1, IP: %2</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="125"/>
        <source>Socket closed: %1, ip: %2</source>
        <translation>Socket geschlossen: %1, IP: %2</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="126"/>
        <source>Client thread failed to finish, terminate issued, socket_id: %1 </source>
        <translation>Client-Thread konnte nicht beendet werden, Terminierung ausgegeben, Socket ID: %1</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="131"/>
        <source>Record is locked by another user!</source>
        <translation>Datensatz ist durch anderen Benutzer gesperrt!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="132"/>
        <source>SQL transaction failed!</source>
        <translation>SQL-Transaktion misslungen!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="133"/>
        <source>SQL rollback failed!</source>
        <translation>SQL-Rollback misslungen!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="134"/>
        <source>SQL statement execution failed! Reason: %1</source>
        <translation>SQL-Statement konnte nicht ausgeführt werdne! Ursache: %1</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="135"/>
        <source>Unknown database driver type!</source>
        <translation>Unbekannter Datenbanktreiber!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="136"/>
        <source>Unknown table version!</source>
        <translation>Unbekannte Tabellenversion!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="137"/>
        <source>Error occurred while fetching Last inserted id!</source>
        <translation>Fehler beim lesen der letzten verwendeten ID!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="138"/>
        <source>General database error: there is no available database connection, please try again in few moments!</source>
        <translation>Allgemeiner Datenbankfehler: Keine verfügbare Datenbankverbindung, bitte später versuchen!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="139"/>
        <source>Resource unlocking failed!</source>
        <translation>Datensatz konnte nicht entsperrt werden!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="140"/>
        <source>Connection reservation failed. SQL error code: %1, SQL text: %2</source>
        <translation>Verbindungsreservierung misslungen. SQL Fehlercode: %1, SQL Text: %2</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="141"/>
        <source>Database driver: %1 is missing or corrupted</source>
        <translation>Datenbanktreiber: %1 fehlt oder ist beschädigt</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="142"/>
        <source>SQL statement execution failed!</source>
        <translation>Fehler bei Ausführung SQL-Statement!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="143"/>
        <source>The value %1 has already been used for another record. Please change it before saving the record!</source>
        <translation>Der eindeutige Wert %1 wurde bereits für einen anderen Datensatz verwendet. Bitte Ändern vor Speichern des Datensatzes!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="144"/>
        <source>Can not modify or delete %1 record because there are %2 records that are related to it!</source>
        <translation>Kann %1 Datensatz nicht ändern andere löschen, weil noch %2 Datensätze damit verknüpft sind!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="146"/>
        <source>Number of active database connections: %1</source>
        <translation>Anzahl aktiver Datenbankverbindungen: %1</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="147"/>
        <source>Orphan Transactions detected: %1</source>
        <translation>Verlorene Transaktionen gefunden: %1</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="148"/>
        <source>Operation failed, duplicate value detected in the database!</source>
        <translation>Ausführung abgebrochen wegen Doublette in der Datenbank!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="152"/>
        <source>Session Expired!</source>
        <translation>Sitzung abgelaufen!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="153"/>
        <source>Server is full. Try again in few minutes!</source>
        <translation>Der Datenbankserver ist voll. Bitte etwas später versuchen!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="154"/>
        <source>Session creation failed, login failed. Try again in few minutes!</source>
        <translation>Login misslungen, Sitzung konnte nicht angemeldet werden. In einigen Minuten erneut versuchen!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="155"/>
        <source>Not Logged!</source>
        <translation>Nicht eingeloggt!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="156"/>
        <source>Already Logged!</source>
        <translation>Bereits eingeloggt!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="157"/>
        <source>Login failed. Module code and MLIID combination does not exist!</source>
        <translation>Login misslungen. Kombination von Modul und Niederlassungs-ID nicht in Lizenz verfügbar!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="158"/>
        <source>Login failed. Reached maximum number of license in use!</source>
        <translation>Login misslungen: Maximale Anzahl der gleichzeitigen Benutzer gemäss Lizenz ist erreicht! Bitte später erneut versuchen!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="159"/>
        <source>Application server session creation failed, invalid license number detected in system!</source>
        <translation>Sitzung auf Application Server konnte nicht erstellt werden, da eine ungültige Lizenz festgestellt wurde!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="160"/>
        <source>Application server session creation failed, same session ID, but different data! Clean Up database!</source>
        <translation>Sitzung auf Application Server konnte nicht erstellt werden: Unterschiedliche Daten bei gleicher Session ID! Datenbank bereinigen!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="161"/>
        <source>Application server failed to start. Reason: %1</source>
        <translation>Applikationsserver konnte nicht gestartet werden. Ursache: %1</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="310"/>
        <source>Failed to create document from then attachment.File Error: %1!</source>
        <translation>Aus dem Anhang konnte kein Dokument erstellt werden. Dateifehler: %1!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="311"/>
        <source>Operation failed. The hierarchical code is already used!</source>
        <translation>Ausführung abgebrochen. Der hierarchische Code wird bereits verwendet!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="163"/>
        <source>Application server &amp; client software version does not match. Please update your software!</source>
        <translation>Version nicht aktuell (Client &amp; Server unterscheiden sich). Update durchführen!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="164"/>
        <source>Connection to server is lost!</source>
        <translation>Verbindung zum Applikationsserver abgebrochen!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="165"/>
        <source>Please Re-login!</source>
        <translation>Bitte neu anmelden!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="285"/>
        <source>Login failed! Invalid username or password!</source>
        <translation>Kein Zugriff! Name oder Passwort falsch!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="286"/>
        <source>Login failed! User has no permission to access system!</source>
        <translation>Login abgebrochen! Der Benutzer verfügt nicht über die für den Zugriff auf das System erforderlichen Zugriffsrechte!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="288"/>
        <source>Entry with this code already exists in the database!</source>
        <translation>Ein Eintrag mit dem gleichen Code besteht schon in der Datenbank!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="289"/>
        <source>Can not create user, unable to generate unique username!</source>
        <translation>Benutzer kann nicht erzeugt werden, da kein eindeutiger Benutzername besteht!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="290"/>
        <source>Can not lock record. Record does not exist in the database, probably was deleted by another user!</source>
        <translation>Datensatz kann nicht gesperrt werden: Datensatz nicht gefunden, wurde wahrscheinlich von einem anderen Benutzer gelöscht!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="291"/>
        <source>Can not load record. Record does not exist in the database, probably was deleted by another user!</source>
        <translation>Datensatz kann nicht gesperrt werden: Datensatz nicht gefunden, wurde wahrscheinlich von einem anderen Benutzer gelöscht!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="292"/>
        <source>Email already have assigned contacts, you can not change assignment!</source>
        <translation>Kontaktzuweisung zu E-Mail kann nicht geändert werden!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="166"/>
        <source>There are already %1 LBO Servers active in systems and %2 MASTER Servers active.</source>
        <translation>Es sind bereits %1 LBO Server und %2 MASTER Server aktiv.</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="194"/>
        <source>Database reorganization started.</source>
        <translation>Datenbankreorganisation gestartet.</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="195"/>
        <source>Database successfully reorganized to version %1</source>
        <translation>Datenbank erfolgreich reorganisiert auf Version %1</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="196"/>
        <source>Restoring database from %1</source>
        <translation>Datenbank wird wiederhergestellt von %1</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="197"/>
        <source>Database successfully restored from %1</source>
        <translation>Datenbank erfolgreich wiederhergestellt von %1</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="198"/>
        <source>Database backup started.</source>
        <translation>Datenbank-Backup gestartet</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="200"/>
        <source>Project import/export operation started.</source>
        <translation>Import/Export von Projekten gestartet.</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="201"/>
        <source>Project import/export operation failed: %1</source>
        <translation>Import/Export von Projekten nicht erfolgreich: %1</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="202"/>
        <source>Project import/export operation ended. No files processed!</source>
        <translation>Import/Export von Projekten beendet. Keine Dateien verarbeitet!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="203"/>
        <source>Project record %1 from file %2 failed at import/export, reason: %3</source>
        <translation>Import/Export von Projekt-Datensatz %1 aus der Datei %2 nicht erfolgreich, Grund: %3</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="204"/>
        <source>Project import/export operation - successfully processed file %1, successfully processed records: %2, failed records: %3</source>
        <translation>Import/Export von Projekten - Datei %1 erfolgreich verarbeitet, verarbeitete Datensätze: %2, nicht erfolgreich verarbeitete Datensätze: %3</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="205"/>
        <source>Project import/export operation - failed to processed file %1, reason: %2</source>
        <translation>Import/Export von Projekten - Verarbeitung von Datei %1 misslungen, Grund: %2</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="206"/>
        <source>Project import/export operation ended.</source>
        <translation>Import/Export von Projekten abgeschlossen.</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="207"/>
        <source>User import/export operation started.</source>
        <translation>Import/Export von Benutzern gestartet.</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="208"/>
        <source>User import/export operation failed: %1</source>
        <translation>Import/Export von Benutzern nicht erfolgreich: %1</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="209"/>
        <source>User import/export operation ended. No files processed!</source>
        <translation>Import/Export von Benutzern beendet. Keine Dateien verarbeitet!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="210"/>
        <source>User record %1 from file %2 failed at import/export, reason: %3</source>
        <translation>Import/Export von Benutzern-Datensatz %1 aus der Datei %2 nicht erfolgreich, Grund: %3</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="211"/>
        <source>User import/export operation - successfully processed file %1, successfully processed records: %2, failed records: %3</source>
        <translation>Import/Export von Benutzern - Datei %1 erfolgreich verarbeitet, verarbeitete Datensätze: %2, nicht erfolgreich verarbeitete Datensätze: %3</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="212"/>
        <source>User import/export operation - failed to processed file %1, reason: %2</source>
        <translation>Import/Export von Benutzern - Verarbeitung von Datei %1 misslungen, Grund: %2</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="213"/>
        <source>User import/export operation ended.</source>
        <translation>Import/Export von Benutzern abgeschlossen.</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="214"/>
        <source>Debtor import/export operation started.</source>
        <translation>Import/Export von Debitoren gestartet.</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="215"/>
        <source>Debtor import/export operation failed: %1</source>
        <translation>Import/Export von Debitoren nicht erfolgreich: %1</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="216"/>
        <source>Debtor import/export operation ended. No files processed!</source>
        <translation>Import/Export von Debitoren beendet. Keine Dateien verarbeitet!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="217"/>
        <source>Debtor record %1 from file %2 failed at import/export, reason: %3</source>
        <translation>Import/Export von Debitoren-Datensatz %1 aus der Datei %2 nicht erfolgreich, Grund: %3</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="218"/>
        <source>Debtor import/export operation - successfully processed file %1, successfully processed records: %2, failed records: %3</source>
        <translation>Import/Export von Debitoren - Datei %1 erfolgreich verarbeitet, verarbeitete Datensätze: %2, nicht erfolgreich verarbeitete Datensätze: %3</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="219"/>
        <source>Debtor import/export operation - failed to processed file %1, reason: %2</source>
        <translation>Import/Export von Debitoren - Verarbeitung von Datei %1 misslungen, Grund: %2</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="220"/>
        <source>Debtor import/export operation ended.</source>
        <translation>Import/Export von Debitoren abgeschlossen.</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="221"/>
        <source>Contact import/export operation started.</source>
        <translation>Import/Export von Kontakten gestartet.</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="222"/>
        <source>Contact import/export operation failed: %1</source>
        <translation>Import/Export von Kontakten nicht erfolgreich: %1</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="223"/>
        <source>Contact import/export operation ended. No files processed!</source>
        <translation>Import/Export von Kontakten beendet. Keine Dateien verarbeitet!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="224"/>
        <source>Contact record %1 from file %2 failed at import/export, reason: %3</source>
        <translation>Import/Export von Kontakt-Datensatz %1 aus der Datei %2 nicht erfolgreich, Grund: %3</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="225"/>
        <source>Contact import/export operation - successfully processed file %1, successfully processed records: %2, failed records: %3</source>
        <translation>Import/Export von Kontakten - Datei %1 erfolgreich verarbeitet, verarbeitete Datensätze: %2, nicht erfolgreich verarbeitete Datensätze: %3</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="226"/>
        <source>Contact import/export operation - failed to processed file %1, reason: %2</source>
        <translation>Import/Export von Kontakten - Verarbeitung von Datei %1 misslungen, Grund: %2</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="227"/>
        <source>Contact import/export operation ended.</source>
        <translation>Import/Export von Kontakten abgeschlossen.</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="237"/>
        <source>Client is alive, thread: %1, ip: %2, user_id: %3</source>
        <translation>Client ist aktiv, Thread: %1, IP: %2, Benutzer-ID: %3</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="238"/>
        <source>User %1 has logged, thread: %2, ip: %3, socket_id: %4, user_id: %5, session: %6</source>
        <translation>Benutzer %1 ist eingeloggt, Thread: %2, IP: %3, Socket ID: %4, Benutzer ID: %5, Session: %6</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="239"/>
        <source>User %1 has logged out, thread: %2, ip: %3, socket_id: %4, user_id: %5, session: %6</source>
        <translation>Benutzer %1 ist ausgeloggt, Thread: %2, IP: %3, Socket ID: %4, Benutzer ID: %5, Session: %6</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="240"/>
        <source>Deleted expired server session id: %1</source>
        <translation>Abgelaufene Server-Session gelöscht. ID: %1</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="241"/>
        <source>Server failed to update server session. Reason: %1</source>
        <translation>Server konnte die Server-Session nicht aktualisieren. Grund: %1</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="242"/>
        <source>Master Garbage Collector encounter own session inside expired session list! Possible cause: deadlock prevented service to update session record!</source>
        <translation>Der Master Garbage Collector hat seine eigene Session unter den abgelaufenen Sessions gefunden! Mögliche Ursache: Ein Deadlock verhinderte, dass der Dienst den Session-Datensatz aktualisieren konnte!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="243"/>
        <source>Server Manager Process started, thread: %1</source>
        <translation>Server-Managerprozess gestartet, Thread: %1</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="244"/>
        <source>Server Manager Process failed: %1</source>
        <translation>Server-Managerprozess abgebrochen, Thread: %1</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="245"/>
        <source>Server is scheduled to restart in %1 seconds</source>
        <translation>Ser Server wird in %1 Sekunden neu starten</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="247"/>
        <source>Server update failed. Failed to execute update script. Please check script location!</source>
        <translation>Serveraktualisierung fehlgeschlagen. Update-Skript konnte nicht ausgeführt werden. Bitte überprüfen Sie den Speicherort des Skripts!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="248"/>
        <source>Number of active user sessions: %1</source>
        <translation>Anzahl aktiver Benutzer-Sessions: %1</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="249"/>
        <source>User %1 is successfully re authenticated, Dynamic Fail Over Process succeeded, session: %2, ip: %3, user_id: %4</source>
        <translation>Benutzer %1 erfolgreich wieder-autentifiziert, Dynamischer Fail-Over Prozess erfolgreich, Session: %2, IP: %3, Benutzer ID: %4</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="250"/>
        <source>User %1 session set to inactive, client connection broken, keeping session for Dynamic Fail Over Process, session: %2, ip: %3, user_id: %4</source>
        <translation>Benutzer Session %1 wurde inaktiv gesetzt, die Client-Verbindung ist abgebrochen, Session wird für den dynamischen Fail-Over Prozess aufbewahrt, Session: %2, IP: =3, Benutzer ID: %4</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="251"/>
        <source>User Session Manager detected and cleaned %1 inactive orphan user sessions.</source>
        <translation>Der Session-Manager detektierte und löschte %1 inaktive verlorene Benutzer-Session(s).</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="252"/>
        <source>User Session Manager failed: %1</source>
        <translation>Benutzer-Session-Manager fehlgeschlagen: %1</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="253"/>
        <source>Re authentication challenge issued to the user %1, session: %2, ip: %3, user_id: %4</source>
        <translation>Wieder-Autentifizierungs-Aufforderung an Benutzer %1 ausgegeben, Session: %2, IP: %3, Benutzer ID: %4</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="254"/>
        <source>Server reached maximum connections, connection refused!</source>
        <translation>Maximale Anzahl Verbindungen zum Server erreicht, Verbindung verweigert!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="255"/>
        <source>Server reached maximum connections, redirecting client to %1</source>
        <translation>Maximale Anzahl Verbindungen zum Server erreicht, Client wird an %1 weitergeleitet</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="256"/>
        <source>Client IsAlive function failed: %1</source>
        <translation>IsAlive-Funktion des Clients fehlgeschlagen: %1</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="278"/>
        <source>Client %1 is kicked from server, client was sending damaged XML-RPC messages!</source>
        <translation>Client %1 wurde vom Server entfernt, der Client sendete nicht konforme XML-RPC-Nachrichten!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="279"/>
        <source>User %1 is kicked from server due to long re authentication time, client IP address %3 is banned, session: %2, user_id: %4</source>
        <translation>Benutzer %1 wurde vom Server entfernt aufgrund der langen Wieder-Autentifizierungszeit, die Client IP-Adresse %3 wurde gebannt, Session: %2, Benutzer ID: %4</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="280"/>
        <source>Client %1 is kicked from server. Reason: %2</source>
        <translation>Client %1 wurde vom Server entfernt. Grund: %2</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="281"/>
        <source>Client %1 address is banned, access refused!</source>
        <translation>Adresse von Client %1 ist gebannt, Zugriff verweigert!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="282"/>
        <source>Client address %1 is kicked and added on the server kick-ban list!</source>
        <translation>Die Client-Adresse %1 wurde zurückgewiesen und der Server-Rückweisungsliste hinzugefügt!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="293"/>
        <source>Locked by: %1</source>
        <translation>Gesperrt von: %1</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="294"/>
        <source>Locked by Another User!</source>
        <translation>Gesperrt durch einen anderen Benutzer!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="295"/>
        <source>Operation failed, contact with id = %1 does not exist!</source>
        <translation>Ausführung abgebrochen, ein Kontakt mit der ID = %1 existiert nicht!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="296"/>
        <source>Operation failed, default person does not exist!</source>
        <translation>Ausführung abgebrochen, kein Default-Benutzer gefunden!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="297"/>
        <source>Template Data could not be fetched!</source>
        <translation>Vorlage konnte nicht gefunden werden!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="298"/>
        <source>No default organization entered!</source>
        <translation>Keine Default-Organisation definiert!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="299"/>
        <source>Error while fetching number of licenses in use!</source>
        <translation>Fehler beim Lesen der Anzahl benutzter Lizenzen!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="300"/>
        <source>Some Items could not be deleted due to insufficient access rights!</source>
        <translation>Einige Daten konnten wegen ungenügender Zugriffsrechte nicht gelöscht werden!</translation>
    </message>
    <message>
        <location filename="status.cpp" line="45"/>
        <source>General Error</source>
        <translation>Allgemeiner Fehler</translation>
    </message>
    <message>
        <location filename="status.cpp" line="45"/>
        <source>Deadlock detected, database is under heavy load, please try again!</source>
        <translation type="obsolete">Blockierung festgestellt. Die Datenbank ist ausgelastet, bitte später versuchen!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="167"/>
        <source>Server is scheduled for update at %1. Please, save all your work and log off from server before update.</source>
        <translation>Der Application Server ist zum Update vorgesehen um %1. Bitte speichern Sie Ihre Arbeit und schliessen Sie Communicator vor dem Update.</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="168"/>
        <source>Server is going for restart in %1 minutes. Please, save all your work and log off from server.</source>
        <translation>Der Server wird in %1 Minuten neu gestartet. Bitte speichern Sie Ihre Arbeit und melden Sie sich aus Communicator ab.</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="169"/>
        <source>The database size limit is almost reached: The current database size has %1Mb, the maximal allowed size is %2Mb. Upgrade your license to get unlimited database access!</source>
        <translation>Die maximale Grösse der Datenbank ist fast erreicht: Die aktuelle Datenbank hat %1 MB, maximal zugelassen sind %2 MB. Bitte erweitern Sie Ihre Lizenz um unbegrenzten Datenbankzugriff zu bekommen!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="170"/>
        <source>The database size limit is reached. Upgrade your license to get unlimited database access. Operation aborted!</source>
        <translation>Die maximale Grösse der Datenbank ist erreicht. Bitte erweitern Sie Ihre Lizenz um unbegrenzten Datenbankzugriff zu bekommen! Ausführung abgebrochen!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="301"/>
        <source>Export/Import operation failed, can not create temporary directory: %1!</source>
        <translation>Export/Import fehlgeschlagen, das temporäre Verzeichnis kann nicht erstellt werden: %1!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="302"/>
        <source>Export/Import operation failed, can not access temporary directory: %1!</source>
        <translation>Export/Import fehlgeschlagen, auf das temporäre Verzeichnis kann nicht zugegriffen werden: %1!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="303"/>
        <source>Export/Import operation failed, can not delete file: %1!</source>
        <translation>Export/Import fehlgeschlagen, die Datei %1 konnte nicht gelöscht werden!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="123"/>
        <source>General socket error: %1!</source>
        <translation>Allgemeiner Socket-Fehler: %1!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="171"/>
        <source>The database backup/restore operation is in progress, backup data can not be load/modified, please try again later!</source>
        <translation>Eine Datenbank-Sicherung (oder -Wiederherstellung) wird durchgeführt, Sicherungsdaten können jetzt nicht geladen oder bearbeitet werden, bitte später versuchen!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="172"/>
        <source>Backup Manager not available!</source>
        <translation>Backup-Manager nicht verfügbar!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="173"/>
        <source>Application server not available!</source>
        <translation>Application Server nicht verfügbar!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="174"/>
        <source>Import/Export Manager not available!</source>
        <translation>Import/Export-Manager nicht verfügbar!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="175"/>
        <source>New backup %1 successfully created!</source>
        <translation>Neue Sicherung %1 erfolgreich erstellt!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="176"/>
        <source>The import/export operation is in progress, import/export data can not be load/modified, please try again later!</source>
        <translation>Import/Export wird durchgeführt, Daten können nicht geladen/bearbeitet werden, bitte später versuchen!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="177"/>
        <source>Import/export operation failed: %1!</source>
        <translation>Import/Export-Vorgang nicht erfolgreich: %1!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="178"/>
        <source>Backup operation failed: %1!</source>
        <translation>Sicherung nicht erfolgreich: %1!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="180"/>
        <source>Import/export operation succeeded!</source>
        <translation>Import/Export-Vorgang erfolgreich!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="179"/>
        <source>Successfully processed: %1 records, failed to process: %2 at %3
Last file processed: %4
Last status: %5</source>
        <translation>Erfolgreich verarbeitet: %1 Datensätze, nicht erfolgreich: %2 bei %3
Letzte verarbeitete Datei: %4
Letzter Status: %5</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="304"/>
        <source>You do not have sufficient access rights to perform this operation!</source>
        <translation>Kein Zugriffsrecht!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="43"/>
        <source>NETWORK </source>
        <translation>Netzwerk</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="145"/>
        <source>Deadlock detected, update failed!</source>
        <translation>Deadlock festgestellt, Aktualisierung fehlgeschlagen!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="162"/>
        <source>Application server successfully started: %1, port: %2, SSL: %3, session: %4, thread: %5</source>
        <translation>Application-Server erfolgreicht gestartet: %1, Port: %2, SSL: %3, Session: %4, Thread: %5</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="181"/>
        <source>Application server stopped</source>
        <translation>Application-Server angehalten</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="182"/>
        <source>Connected to the database: %1 on host: %2</source>
        <translation>Verbunden mit Datenbank: %1 auf Host %2</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="183"/>
        <source>Backup Manager failed to start! Reason: %1</source>
        <translation>Backup-Manager konnte nicht starten! Grund: %1</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="184"/>
        <source>Garbage Collector started, thread: %1</source>
        <translation>Garbage Collector gestartet, Thread: %1</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="185"/>
        <source>Garbage Collector failed: %1</source>
        <translation>Garbage Collector fehlgeschlagen: %1</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="186"/>
        <source>Garbage Collector successfully cleaned: %1 expired sessions</source>
        <translation>Garbage Collector hat %1 abgelaufene Sessions erfolgreich entfernt</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="187"/>
        <source>Garbage Collector successfully cleaned: %1 orphan client connections/sockets</source>
        <translation>Garbage Collector hat %1 verlorene Client-Verbindungen/Sockets erfolgreich entfernt</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="188"/>
        <source>Garbage Collector successfully cleaned: %1 orphan client sessions</source>
        <translation>Garbage Collector hat %1 verlorene Client-Sessions erfolgreich entfernt</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="189"/>
        <source>Master Garbage Collector started, thread: %1</source>
        <translation>Master Garbage Collector gestartet, Thread: %1</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="190"/>
        <source>Master Garbage Collector failed: %1</source>
        <translation>Master Garbage Collector fehlgeschlagen: %1</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="191"/>
        <source>Message Dispatcher started, thread: %1</source>
        <translation>Message Dispatcher gestarted, Thread: %1</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="193"/>
        <source>Import/Export process started, thread: %1</source>
        <translation>Import/Export-Prozess gestartet, Thread: %1</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="192"/>
        <source>Message Dispatcher failed: %1</source>
        <translation>Message-Dispatcher fehlgeschlagen: %1</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="257"/>
        <source>Parent session does not exist, re-login again!</source>
        <translation>Die übergeordnete Session existiert nicht, bitte neu einloggen!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="127"/>
        <source>Request buffer invalid. Operation aborted!</source>
        <translation>Anfragepuffer ungültig. Aufgabe abgebrochen!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="246"/>
        <source>Server is stopping. Update in progress, executing update script %1</source>
        <translation>Server wird angehalten. Aktualisierung wird durchgeführt, das Update-Script %1 wird ausgeführt</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="305"/>
        <source>Operation failed, document does not have any revisions uploaded!</source>
        <translation>Aufgabe abgebrochen, das Dokument verfügt über keine gespeicherten Revisionen!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="306"/>
        <source>Operation failed, there is no more server disk space available!</source>
        <translation>Aufgabe abgebrochen, kein Server-Speicherplatz mehr verfügbar!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="307"/>
        <source>Writing blob to file failed: %1!</source>
        <translation>Speichern von Binärdaten in Datei abgebrochen: %1!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="308"/>
        <source>Reading blob from file failed: %1!</source>
        <translation>Lesen von Binärdaten aus Datei abgebrochen: %1!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="309"/>
        <source>Document can not be checked out because it is checked out by other user!</source>
        <translation>Das Dokument kann nicht ausgecheckt werden, weil ein anderer Benutzer es bereits ausgecheckt hat!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="128"/>
        <source>Server rejected request: %1</source>
        <translation>Anfrage von Server abgewiesen: %1</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="102"/>
        <source>XML RPC requested size %1 for transfer exceeds maximum allowed 80Mb size, operation aborted!</source>
        <translation>Die über XML RPC angeforderte Datenpaketgrösse %1 übersteigt die maximal erlaubte Transfergrösse von 80MB. Vorgang abgebrochen!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="258"/>
        <source>Failed to create reminder: %1</source>
        <translation>Erinnerung konnte nicht erstellt werden: %1</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="259"/>
        <source>Failed to send &apos;%1&apos; reminder by Sokrates notification system.</source>
        <translation>Erinnerung &apos;%1&apos; konnte durch den SOKRATES Nachrichtendienst nicht versendet werden.</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="261"/>
        <source>Failed to send &apos;%1&apos; reminder by SMS (%2).</source>
        <translation>Erinnerung &apos;%1&apos; konnte nicht über SMS versendet werden (%2).</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="264"/>
        <source>Reminder list rebuild failed: %1</source>
        <translation>Liste der Erinnerungshinweise konnte nicht aufgebaut werden: %1</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="265"/>
        <source>Failed to read reminder settings: %1</source>
        <translation>Erinnerungs-Einstellungen konnten nicht gelesen werden: %1</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="266"/>
        <source>Reminder Manager not available!</source>
        <translation>Erinnerungsdienst nicht verfügbar!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="260"/>
        <source>Failed to send &apos;%1&apos; reminder by E-mail (%2).</source>
        <translation>Erinnerung &apos;%1&apos; konnte nicht über E-Mail versendet werden (%2).</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="262"/>
        <source>Failed to send reminder with id: %1. Reason: %2</source>
        <translation>Erinnerung mit der ID %1 konnte nicht verschickt werden. Grund: %2</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="263"/>
        <source>Reminder clean up failed: %1</source>
        <translation>Erinnerung %1 konnte nicht bereinigt werden</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>Failed to register server: %1</source>
        <translation>Server konnte nicht registriert werden: %1</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="268"/>
        <source>Failed to unregister server: %1</source>
        <translation>Server konnte nicht deregistriert werden: %1</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="269"/>
        <source>Failed to forward port %1</source>
        <translation>Port %1 konnte nicht weitergeleitet werden</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="270"/>
        <source>Failed to test server connectivity. Please check your Internet connection and status of forwarded port!</source>
        <translation>Serververbindung konnte nicht bestätigt werden. Bitte überprüfen Sie Ihre Internetverbindung und den Status der Port-Weiterleitung auf dem Router!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="271"/>
        <source>Application server is starting.&lt;br&gt;Incoming connections are not yet possible. Please wait.</source>
        <translation>Der Application Server wird gestartet.&lt;br&gt;Eingehende Verbindungen noch nicht möglich, bitte warten.</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="272"/>
        <source>Application server is checking/reorganizing database.&lt;br&gt;This may take some time. Incoming connections are not possible. Please wait.</source>
        <translation>Der Application Server überprüft/reorganisiert die Datenbank.&lt;br&gt;Dies kann einigeZeit dauern. Eingehende Verbindungen noch nicht möglich, bitte warten.</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="273"/>
        <source>Application server is starting services.&lt;br&gt;Incoming connections are not yet possible. Please wait.</source>
        <translation>Der Application Server startet als Dienst.&lt;br&gt;Eingehende Verbindungen noch nicht möglich, bitte warten.</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="274"/>
        <source>Application server successfully started.</source>
        <translation>Der Application Server ist gestartet und aktiv.</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="275"/>
        <source>Application server failed to start.&lt;br&gt;%1</source>
        <translation>Der Application Server konnte nicht gestartet werden.&lt;br&gt;%1</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="228"/>
        <source>Stored Project List import/export operation started.</source>
        <translation>Import/Export von gespeicherten Projektlisten gestartet.</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="232"/>
        <source>Stored Project List import/export operation - successfully processed file %1, successfully processed records: %2, failed records: %3</source>
        <translation>Import/Export von gespeicherten Projektlisten - Datei %1 erfolgreich verarbeitet, verarbeitete Datensätze: %2, nicht erfolgreich verarbeitete Datensätze: %3</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="234"/>
        <source>Stored Project List import/export operation ended.</source>
        <translation>Import/Export von gespeicherten Projektlisten abgeschlossen.</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="229"/>
        <source>Stored Project List import/export operation failed: %1</source>
        <translation>Import/Export von gespeicherten Projektlisten nicht erfolgreich: %1</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="230"/>
        <source>Stored Project List import/export operation ended. No files processed!</source>
        <translation>Import/Export von gespeicherten Projektlisten beendet. Keine Dateien verarbeitet!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="231"/>
        <source>Stored Project List record %1 from file %2 failed at import/export, reason: %3</source>
        <translation>Import/Export von gespeicherten Projektlisten - Datensatz %1 aus der Datei %2 nicht erfolgreich, Grund: %3</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="233"/>
        <source>Stored Project List import/export operation - failed to processed file %1, reason: %2</source>
        <translation>Import/Export von gespeicherten Projektlisten - Verarbeitung von Datei %1 misslungen, Grund: %2</translation>
    </message>
</context>
<context>
    <name>StatusCodeSet</name>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>Unknown Error!</source>
        <translation type="obsolete">Unbekannter Fehler!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>HTTP    </source>
        <translation type="obsolete">HTTP    </translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>XMLRPC  </source>
        <translation type="obsolete">XMLRPC  </translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>RPC     </source>
        <translation type="obsolete">RPC     </translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>SQL     </source>
        <translation type="obsolete">SQL     </translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>SYSTEM  </source>
        <translation type="obsolete">SYSTEM  </translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>BUSINESS</source>
        <translation type="obsolete">BUSINESS</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>COMMON  </source>
        <translation type="obsolete">COMMON  </translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>Success    </source>
        <translation type="obsolete">Erfolg    </translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>Error      </source>
        <translation type="obsolete">Fehler      </translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>Warning    </source>
        <translation type="obsolete">Warnung    </translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>Information</source>
        <translation type="obsolete">Information</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>No Error</source>
        <translation type="obsolete">Kein Fehler</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>XML parsing failed!</source>
        <translation type="obsolete">Parsen von XML misslungen!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>XML RPC generation failed!</source>
        <translation type="obsolete">XML RPC -Generierung misslungen!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>XML RPC wrong data type.XML parsing failed!</source>
        <translation type="obsolete">XML RPC mit falschem Datentpy. Parsen von XML misslungen!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>Invalid NameSpace!</source>
        <translation type="obsolete">Ungültiger Namensraum (Namespace)!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>Application server function dispatcher: Function not suported!</source>
        <translation type="obsolete">Application Server-Methoden-Dispatcher: Funktion nicht unterstützt!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>Parameter count doesn&apos;t match!</source>
        <translation type="obsolete">Ungültige Parameter!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>Invalid RPC request!</source>
        <translation type="obsolete">Ungültige RPC-Anforderung!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>Connection already exists!</source>
        <translation type="obsolete">Verbindung besteht bereits!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>Host not found!</source>
        <translation type="obsolete">Applikationsserver nicht gefunden!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>Host closed connection!</source>
        <translation type="obsolete">Verbindung durch Applikationsserver abgebrochen!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>Timeout error!</source>
        <translation type="obsolete">Fehler durch Zeitüberschreitung!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>General socket error!</source>
        <translation type="obsolete">Allgemeiner Socket-Fehler!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>Socket not connected!</source>
        <translation type="obsolete">Socket nicht verbunden!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>Invalid HTTP message detected!</source>
        <translation type="obsolete">Ungültige HTTP-Nachricht festgestellt!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>Server failed to start listener on IP address: %1, at port: %2!</source>
        <translation type="obsolete">Applikationsserver konnte keinen Listener installieren an der IP-Adresse: %1 auf Port: %2!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>Server already running!</source>
        <translation type="obsolete">Applikationsserver läuft bereits!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>Record is locked by another user!</source>
        <translation type="obsolete">Datensatz ist durch anderen Benutzer gesperrt!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>SQL transaction failed!</source>
        <translation type="obsolete">SQL-Transaktion misslungen!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>SQL rollback failed!</source>
        <translation type="obsolete">SQL-Rollback misslungen!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>SQL statement execution failed! Reason: %1</source>
        <translation type="obsolete">SQL-Statement konnte nicht ausgeführt werdne! Ursache: %1</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>Unknown database driver type!</source>
        <translation type="obsolete">Unbekannter Datenbanktreiber!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>Unknown table version!</source>
        <translation type="obsolete">Unbekannte Tabellenversion!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>Error occurred while fetching Last inserted id!</source>
        <translation type="obsolete">Fehler beim lesen der letzten verwendeten ID!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>General database error: there is no available database connection, please try again in few moments!</source>
        <translation type="obsolete">Allgemeiner Datenbankfehler: Keine verfügbare Datenbankverbindung, bitte später versuchen!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>Resource unlocking failed!</source>
        <translation type="obsolete">Datensatz konnte nicht entsperrt werden!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>Connection reservation failed. SQL error code: %1, SQL text: %2</source>
        <translation type="obsolete">Verbindungsreservierung misslungen. SQL Fehlercode: %1, SQL Text: %2</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>Database driver: %1 is missing or corrupted</source>
        <translation type="obsolete">Datenbanktreiber: %1 fehlt oder ist beschädigt</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>SQL statement execution failed!</source>
        <translation type="obsolete">Fehler bei Ausführung SQL-Statement!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>The value %1 has already been used for another record. Please change it before saving the record!</source>
        <translation type="obsolete">Der eindeutige Wert %1 wurde bereits für einen anderen Datensatz verwendet. Bitte Ändern vor Speichern des Datensatzes!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>Can not modify or delete %1 record because there are %2 records that are related to it!</source>
        <translation type="obsolete">Kann %1 Datensatz nicht ändern oder löschen, weil noch %2 Datensetze damit verknüpft sind!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>Session Expired!</source>
        <translation type="obsolete">Sitzung abgelaufen!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>Server is full. Try again in few minutes!</source>
        <translation type="obsolete">Der Datenbankserver ist voll. Bitte etwas später versuchen!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>Session creation failed, login failed. Try again in few minutes!</source>
        <translation type="obsolete">Login misslungen, Sitzung konnte nicht angemeldet werden. In einigen Minuten erneut versuchen!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>Not Logged!</source>
        <translation type="obsolete">Nicht eingeloggt!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>Already Logged!</source>
        <translation type="obsolete">Bereits eingeloggt!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>Login failed. Module code and MLIID combination does not exist!</source>
        <translation type="obsolete">Login misslungen. Kombination von Modul und Niederlassungs-ID nicht in Lizenz verfügbar!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>Login failed. Reached maximum number of license in use!</source>
        <translation type="obsolete">Login misslungen: Maximale Anzahl der gleichzeitigen Benutzer gemäss Lizenz ist erreicht! Bitte später erneut versuchen!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>Application server session creation failed, invalid license number detected in system!</source>
        <translation type="obsolete">Sitzung auf Application Server konnte nicht erstellt werden, da eine ungültige Lizenz festgestellt wurde!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>Application server session creation failed, same session ID, but different data! Clean Up database!</source>
        <translation type="obsolete">Sitzung auf Application Server konnte nicht erstellt werden: Unterschiedliche Daten bei gleicher Session ID! Datenbank bereinigen!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>Application server failed to start. Reason: %1</source>
        <translation type="obsolete">Applikationsserver konnte nicht gestartet werden. Ursache: %1</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>Application server successfully started: %1, port: %2, SSL: %3</source>
        <translation type="obsolete">Applikationsserver erfolgreich gestartet: %1, port: %2, SSL: %3</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>Application server &amp; client software version does not match. Please update your software!</source>
        <translation type="obsolete">Version nicht aktuell (Client &amp; Server unterscheiden sich). Update durchführen!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>Connection to server is lost!</source>
        <translation type="obsolete">Verbindung zum Applikationsserver abgebrochen!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>Please Re-login!</source>
        <translation type="obsolete">Bitte neu anmelden!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>Login failed! Invalid username or password!</source>
        <translation type="obsolete">Kein Zugriff! Name oder Passwort falsch!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>Login failed! User has no permission to access system!</source>
        <translation type="obsolete">Anmeldung misslungen! Kein Zugriffsrecht!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>Client with username: %1 sucessfully logged!</source>
        <translation type="obsolete">Benutzer %1 erfolgreich angemeldet!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>Entry with this code already exists in the database!</source>
        <translation type="obsolete">Ein Eintrag mit dem gleichen Code besteht schon in der Datenbank!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>Can not create user, unable to generate unique username!</source>
        <translation type="obsolete">Benutzer kann nicht erzeugt werden, da kein eindeutiger Benutzername besteht!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>Can not lock record. Record does not exists in the database, probably was deleted by another user!</source>
        <translation type="obsolete">Datensatz kann nicht gesperrt werden: Datensatz nicht gefunden, wurde wahrscheinlich von anderem Benutzer gelöscht!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>Can not load record. Record does not exists in the database, probably was deleted by another user!</source>
        <translation type="obsolete">Datensatz kann nicht geladen werden: Datensatz nicht gefunden, wurde wahrscheinlich von anderem Benutzer gelöscht!</translation>
    </message>
    <message>
        <location filename="statuscodeset.cpp" line="267"/>
        <source>Email already have assigned contacts, you can not change assignment!</source>
        <translation type="obsolete">Kontaktzuweisung zu E-Mail kann nicht geändert werden!</translation>
    </message>
</context>
</TS>
