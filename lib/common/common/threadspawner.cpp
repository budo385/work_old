#include "threadspawner.h"
#include "common/common/sleeper.h"


//stop thread.> automatic delete of , destroy task if
ThreadSpawner::~ThreadSpawner()
{
	//stop thread gently if running:
	if(isRunning())
	{
		quit();
		wait();
	}
}

void ThreadSpawner::Start()
{
	if (isRunning())
		return;

	//wait until thread actually starts
	m_t_sync.ThreadSetForWait();
	start();
	m_t_sync.ThreadWait();
}


void ThreadSpawner::Stop()
{
	if(isRunning())
	{
		quit();
		wait();
	}

}


//start Thread
void ThreadSpawner::run()
{
	m_pThreadObject= CreateThreadSpawnedObject();
	connect(this,SIGNAL(finished ()),m_pThreadObject,SLOT(deleteLater()));	//when this object is destroyed, destroy local TASK
	m_t_sync.ThreadRelease();
	exec();
}

