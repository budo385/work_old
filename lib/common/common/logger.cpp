#include "logger.h"
#include "loggerfile.h"
#include "common/common/status.h"
#include <QDebug>

Logger::Logger()
:m_lock(QReadWriteLock::Recursive),m_pFileLogger(NULL),/*m_pDbLogger(NULL),m_pSysLogger(NULL),*/m_bConsoleLogger(false),m_nLogLevel(LOG_LEVEL_NONE)//,m_bUseAsGlobalLog(false)
{


}

Logger::~Logger()
{
	if(m_pFileLogger!=NULL)delete m_pFileLogger;
	//if(m_pDbLogger!=NULL)delete m_pDbLogger;
	//if(m_pSysLogger!=NULL)delete m_pSysLogger;
}


bool Logger::EnableFileLogging(QString strFullFilePath,int nBufferSize,int nMaxLogSize)
{
	if(m_pFileLogger==NULL)m_pFileLogger = new LoggerFile;
	if(!dynamic_cast<LoggerFile*>(m_pFileLogger)->Initialize(strFullFilePath,nBufferSize,nMaxLogSize))
	{
		delete m_pFileLogger;
		m_pFileLogger=NULL;
		return false;
	}
	return true;

}

void Logger::FlushFileLog()
{
	m_lock.lockForWrite();
	if (m_pFileLogger)
	{
		dynamic_cast<LoggerFile*>(m_pFileLogger)->FlushLog();
	}
	m_lock.unlock();
}
QString Logger::GetFileLogPath()
{
	if (m_pFileLogger)
	{
		return dynamic_cast<LoggerFile*>(m_pFileLogger)->GetFilePath();
	}
	return "";
}

/*!
	Write log messages to all targets:
	mutex on file / syslog, Db/Bo logger does not need mutex
*/
void Logger::logMessage (int nMsgType,int nMsgID, QString strMsgArguments)
{
	if (m_nLogLevel==LOG_LEVEL_NONE)
		return;

	//BT: quick patch:
	if (nMsgType==StatusCodeSet::TYPE_SPEED_TEST_LEVEL)
	{
		m_lock.lockForWrite();
		if (m_pFileLogger)
			m_pFileLogger->logMessage(nMsgType,nMsgID,strMsgArguments);
		m_lock.unlock();
		return;
	}

	if (m_nLogLevel==LOG_LEVEL_NONE || m_nLogLevel==LOG_LEVEL_SPEED_TEST)
		return;

	m_lock.lockForRead();
	if (!IsMessageForLog(nMsgType,nMsgID,strMsgArguments))
	{
		m_lock.unlock();
		return;
	}
	m_lock.unlock();
	m_lock.lockForWrite();
	
	//log to file:
	if(m_pFileLogger!=NULL)
	{
		m_pFileLogger->logMessage(nMsgType,nMsgID,strMsgArguments);
	}

	//if console->put error text on console
	if(m_bConsoleLogger)
	{
		Status err;
		err.setError(nMsgID,strMsgArguments);
		QString str=QTime::currentTime().toString("hh.mm.ss.zzz")+": "+err.getErrorText();
		qDebug()<<str;
	}

	m_lock.unlock();

	//notify all observer:
	emit MessageLogged(nMsgType,nMsgID,strMsgArguments);

}


bool Logger::IsMessageForLog(int nMsgType,int nMsgID, QString strMsgArguments) const
{
	if (m_nLogLevel>=LOG_LEVEL_DEBUG)
		return true;
	if (m_nLogLevel==LOG_LEVEL_NONE)
		return false;

	if (nMsgType==StatusCodeSet::TYPE_DEBUG_INFORMATION)
		return false;
	if (nMsgType==StatusCodeSet::TYPE_ERROR || nMsgType==StatusCodeSet::TYPE_WARNING)
		return true;

	int nCatID;
	nCatID=StatusCodeSet::getMessageCategoryID(nMsgID);

	if (m_nLogLevel==LOG_LEVEL_BASIC)
	{
		if (nCatID==StatusCodeSet::CATEGORY_COMMON || nCatID==StatusCodeSet::CATEGORY_SYSTEM || nCatID==StatusCodeSet::CATEGORY_BUSINESS)
			return true;
		else
			return false;
	}
	return true;
}



void Logger::SetLogLevel(int nLevel)
{
	m_lock.lockForWrite();
	if (nLevel<0)nLevel=0;
	if (nLevel>LOG_LEVEL_SPEED_TEST)nLevel=LOG_LEVEL_SPEED_TEST;
	m_nLogLevel=nLevel;
	m_lock.unlock();
}

void Logger::SetMaxLogSize(int nMaxLogSize)
{
	m_lock.lockForWrite();
	if (m_pFileLogger)
	{
		dynamic_cast<LoggerFile*>(m_pFileLogger)->SetMaxLogSize(nMaxLogSize);
	}
	m_lock.unlock();
}
void Logger::SetMemoryBufferSize(int nBufferSize)
{
	m_lock.lockForWrite();
	if (m_pFileLogger)
	{
		dynamic_cast<LoggerFile*>(m_pFileLogger)->SetMemoryBufferSize(nBufferSize);
	}
	m_lock.unlock();
}

int	Logger::GetLogLevel()
{
	QReadLocker lck(&m_lock);
	return m_nLogLevel;
}
int	Logger::GetMaxLogSize()
{
	QReadLocker lck(&m_lock);
	if (m_pFileLogger)
	{
		return dynamic_cast<LoggerFile*>(m_pFileLogger)->GetMaxLogSize();
	}
	else return 0;
}
int	Logger::GetMemoryBufferSize()
{
	QReadLocker lck(&m_lock);
	if (m_pFileLogger)
	{
		return dynamic_cast<LoggerFile*>(m_pFileLogger)->GetMemoryBufferSize();
	}
	else return 0;
}

qint64 Logger::GetCurrentLogSize()
{
	QReadLocker lck(&m_lock);
	if (m_pFileLogger)
	{
		return dynamic_cast<LoggerFile*>(m_pFileLogger)->GetCurrentLogSize();
	}
	else return 0;
}

bool Logger::GetLog(QByteArray &logData,qint64 &nCurrentPosition,qint64 &nLogFileSizeOnStartOfChunkRead,int nChunkSize)
{
	QReadLocker lck(&m_lock);
	if (m_pFileLogger)
	{
		return dynamic_cast<LoggerFile*>(m_pFileLogger)->GetLog(logData,nCurrentPosition,nLogFileSizeOnStartOfChunkRead,nChunkSize);
	}
	else return 0;
}