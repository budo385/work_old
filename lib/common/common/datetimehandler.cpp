#include "DateTimeHandler.h"

bool DateTimeHandlerX::s_bUseUTCTimeZone=false;
bool DateTimeHandlerX::s_bClientSideUTC2LocalConvertEnabled=false;

QDateTime DateTimeHandlerX::currentDateTime()
{
	if (!s_bUseUTCTimeZone) //if use local time, then conversion is not needed
		return QDateTime::currentDateTime();
	else
	{
		QDateTime tmp= QDateTime::currentDateTime().toUTC();
		tmp.setTimeSpec(Qt::LocalTime);
		return tmp;
	}
}

QDateTime DateTimeHandlerX::toUTC(const QDateTime &tx)
{
	if (!s_bClientSideUTC2LocalConvertEnabled) //if use local time, then conversion is not needed
		return tx;

	QDateTime tmp= tx.toUTC();
	tmp.setTimeSpec(Qt::LocalTime);
	return tmp;
}

QDateTime DateTimeHandlerX::fromUTC(const QDateTime &tx)
{
	if (!s_bClientSideUTC2LocalConvertEnabled) //if use local time, then conversion is not needed
		return tx;

	QDateTime tmp=tx;
	tmp.setTimeSpec(Qt::UTC);
	return tmp.toLocalTime();
}
