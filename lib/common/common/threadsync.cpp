#include "threadsync.h"

/*------------------------------------------------------------------------*/
/*						  MANUAL SYNCING OF THREADS                       */
/*------------------------------------------------------------------------*/


ThreadSynchronizer::ThreadSynchronizer(const ThreadSynchronizer &that)
{
	operator = (that);
}
ThreadSynchronizer &ThreadSynchronizer::operator =(const ThreadSynchronizer &that)
{
	//no copy for mutex and semaphore..
	//if(this != &that)
	//{
		//m_Sem= that.m_Sem;
		//lock=that.lock;
	//}
	return *this;
}

bool ThreadSynchronizer::ThreadWaitTimeOut(int nTimeoutMsec)
{
	if(m_Sem.tryAcquire(1, nTimeoutMsec))
	{
		ThreadRelease();
		return true;
	}
	else
		return false;
}

/*! 
	Main thread is waiting for slave
*/
void ThreadSynchronizer::ThreadWait()
{
	//Q_ASSERT(m_Sem.available()<=1);

	m_Sem.acquire(1); //wait for another thread
	//qDebug()<<"Semaphore, available locks:"<<m_Sem.available();
	ThreadRelease(); //m_Sem.release(1); //clear sempahore

/*

	//StopThread until flag is free
	while (!IsThreadFree())
	{
		//QThread::msleep(1);
		ThreadSleeper::Sleep(1);//sleep minimum=1msec
	}
	*/
}

/*! 
	Called by slave thread to release master
*/
void ThreadSynchronizer::ThreadRelease()
{
	Q_ASSERT(m_Sem.available()<=1);


	lock.lock(); //This is needed coz 2 threads can release lock on semaphore in same time

	if(m_Sem.available()==0)m_Sem.release(1); //two threads? not under mutex?

	lock.unlock();

/*
	return;

	m_RWLock.lockForWrite();
	m_bWaitFlag=false;
	m_RWLock.unlock();
*/
}


/*! 
	Called by master thread to prepare for waiting
*/
void ThreadSynchronizer::ThreadSetForWait()
{

	//qDebug()<<"Semap: available"<<m_Sem.available();
	Q_ASSERT(m_Sem.available()<=1);
	m_Sem.acquire(1);

/*
	//set flag for wait:
	m_RWLock.lockForWrite();
	m_bWaitFlag=true;
	m_RWLock.unlock();

*/
}


/*! 
	Test if flag is free
*/
/*
bool ThreadSynchronizer::IsThreadFree()
{

	return m_Sem.available();


	m_RWLock.lockForRead();
	bool bTmp=!m_bWaitFlag;
	m_RWLock.unlock(); 
	return bTmp;

}
*/




