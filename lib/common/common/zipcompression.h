#ifndef ZIPCOMPRESSION_H
#define ZIPCOMPRESSION_H

#include <QString>
//#ifndef WINCE

class ZipCompression
{
public:
	/*
		Parametri:
		1. string sa path-om .zip datoteke
		2. lista stringova sa full path-ovima ulaznih datoteka (npr. "C:\ante\ninja.doc")
		3. lista stringova sa path-ovima subdirectory ulaznih datoteka unutar .zip-a
			(npr. "subdir\subdir") 
			nOTE: this are only relative subdirectories, add empty string if u want into root
	*/
	static bool			ZipFiles(QString strZipArchive, QStringList &lstInFiles);
	static bool			Unzip (QString strZipArchive,  QString strDestinationDir, QStringList &lstOutFiles, bool bOverWriteIfExists=true);
	static bool			Deflate(const QByteArray &source, QByteArray &target, int compressionLevel=-1); 
	static bool			Inflate(const QByteArray &source, QByteArray &target); 

};
/*
#else
class ZipCompression
{
public:
	//Parametri:
	//1. string sa path-om .zip datoteke
	//2. lista stringova sa full path-ovima ulaznih datoteka (npr. "C:\ante\ninja.doc")
	//3. lista stringova sa path-ovima subdirectory ulaznih datoteka unutar .zip-a
	//(npr. "subdir\subdir") 
	//nOTE: this are only relative subdirectories, add empty string if u want into root
	static bool ZipFiles(QString &strZipArchive, QStringList &lstInFiles){return false;}

	//lstOutFiles list full paths->unzipped files
	static bool Unzip (const QString &strZipArchive,  const QString &strDestinationDir,  QStringList &lstOutFiles, bool bOverWriteIfExists=true){return false;}

};
//#endif //WINCE
*/
#endif // ZipCompression_H

