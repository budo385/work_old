#ifndef STACKWALKERLOGGER_H
#define STACKWALKERLOGGER_H

#include <QByteArray>

#ifdef _WIN32
	#include "signal.h"
	#include "stackwalker.h"	//backtrace for Windows
#endif
	class StackWalkerLogger : public StackWalker
	{
	public:
		QByteArray	GetBacktrace();
		void		DumpCallStack2Log(bool bIsCrashSignalDetected=false);
	protected:
		QByteArray m_buffer;
	#ifdef _WIN32
		virtual void OnOutput(LPCSTR szText);
		virtual void OnLoadModule(LPCSTR img, LPCSTR mod, DWORD64 baseAddr, DWORD size, DWORD result, LPCSTR symType, LPCSTR pdbName, ULONGLONG fileVersion);
	#endif
	};


#ifdef _WIN32
	#define CRASH_SIGNAL_HANDLER	extern "C" void signal_handler(int sig)\
	{\
		StackWalkerLogger logger;\
		logger.DumpCallStack2Log(true);\
		_exit(1);\
		}\
		BOOL WINAPI HandlerRoutine1(DWORD dwCtrlType)\
		{\
		signal_handler(0);\
		return TRUE;\
		}\
		LONG WINAPI TopLevelExceptionHandler(PEXCEPTION_POINTERS pExceptionInfo)\
		{\
		signal_handler(0);\
		return EXCEPTION_CONTINUE_SEARCH;\
		}\
		void PrepareCrashLog()\
		{\
		SetConsoleCtrlHandler(HandlerRoutine1, TRUE);\
		SetUnhandledExceptionFilter(TopLevelExceptionHandler);\
		signal(SIGABRT, signal_handler);\
		signal(SIGFPE,  signal_handler);\
		signal(SIGINT,  signal_handler);\
		signal(SIGILL,  signal_handler);\
		signal(SIGSEGV, signal_handler);\
		}

#endif


#endif // STACKWALKERLOGGER_H
