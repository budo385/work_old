#include "statuscodeset.h"
#include <QObject>


/* init static members*/
bool StatusCodeSet::bInitialized=false;
QHash<int,QString> StatusCodeSet::arrayErrors;


/*!
Gets Error Text and Error severity based on ErrorCode
\param ErrorCode
\param ErrorText
\param ErrorSeverity
*/
void StatusCodeSet::getErrorDetails(int ErrorCode, QString &ErrorText)
{
	// if not initialized init:
	if (!bInitialized)initialize();

	// get error text, if not found return unknown error
	ErrorText=arrayErrors.value(ErrorCode,QObject::tr("Unknown Error!"));
}


int StatusCodeSet::getMessageCategoryID(int ErrorCode)
{
	if(ErrorCode<100) return CATEGORY_COMMON;
	if(ErrorCode<300) return CATEGORY_NETWORK;
	if(ErrorCode<400) return CATEGORY_SQL;
	if(ErrorCode<600) return CATEGORY_SYSTEM;
	if(ErrorCode>1000) return CATEGORY_BUSINESS;

	return CATEGORY_COMMON;
}



QString StatusCodeSet::getMessageCategoryName(int nCategoryID)
{
	switch(nCategoryID)
	{
	case CATEGORY_NETWORK:
		return QObject::tr("NETWORK ");
		break;
	case CATEGORY_SQL:
		return QObject::tr("SQL     ");
		break;
	case CATEGORY_SYSTEM:
		return QObject::tr("SYSTEM  ");
		break;
	case CATEGORY_BUSINESS:
		return QObject::tr("BUSINESS");
		break;
	default:
		return QObject::tr("COMMON  ");
	    break;
	}
}


QString StatusCodeSet::getMessageTypeName(int nMsgType)
{
	switch(nMsgType)
	{
	case TYPE_ERROR:
		return QObject::tr("Error      ");
		break;
	case TYPE_WARNING:
		return QObject::tr("Warning    ");
		break;
	default:
		return QObject::tr("Information");

	}
}

/*!
	Returns msg details
*/
void StatusCodeSet::getMessageDetails (int nMsgType,int nMsgID, int &nCategoryID,QString &strCategoryTxt,QString &strMsgType)
{
	// get message information:
	nCategoryID=getMessageCategoryID(nMsgID);
	strCategoryTxt=getMessageCategoryName(nCategoryID);
	strMsgType=getMessageTypeName(nMsgType);
}



/*!
Initialize function for array of codes: on first call this function will be called
*/
void StatusCodeSet::initialize(){
	
	bInitialized=true;

	arrayErrors[ERR_NONE]=QObject::tr("No Error");

	arrayErrors[ERR_XMLRPC_PARSE_FAIL]=QObject::tr("XML parsing failed!");
	arrayErrors[ERR_XMLRPC_RPC_GENERATE_FAIL]=QObject::tr("XML RPC generation failed!");
	arrayErrors[ERR_XMLRPC_RPC_WRONG_DATA_TYPE]=QObject::tr("XML RPC wrong data type.XML parsing failed!");
	arrayErrors[ERR_XMLRPC_RPC_SIZE_EXCEEDED]=QObject::tr("XML RPC requested size %1 for transfer exceeds maximum allowed 80Mb size, operation aborted!");
	arrayErrors[ERR_XML_PARAM_PARSE_FAIL]=QObject::tr("XML parsing failed for parameter %1!");
	arrayErrors[ERR_XML_BODY_PARSE_FAIL]=QObject::tr("XML parsing failed for message body at line %1, column %2, error is: %3");
	arrayErrors[ERR_XML_PARAM_MISSING]=QObject::tr("XML parsing failed, parameter %1 is missing");
	arrayErrors[ERR_XML_PARSE_FAIL_INVALID_XML]=QObject::tr("XML parsing failed, invalid XML document!");
	
	arrayErrors[ERR_RPC_INVALID_NAMESPACE]=QObject::tr("Invalid NameSpace!");
	arrayErrors[ERR_RPC_FUNCTION_NOT_EXISTS]=QObject::tr("Application server function dispatcher: Function not supported!");
	arrayErrors[ERR_RPC_PARAMETER_COUNT_MISMATCH]=QObject::tr("Parameter count doesn't match!");
	arrayErrors[ERR_RPC_INVALID_REQUEST]=QObject::tr("Invalid RPC request!");		

	arrayErrors[ERR_HTTP_SOCKET_ALREADY_CONNECTED]=QObject::tr("Connection already exists!");
	arrayErrors[ERR_HTTP_HOST_NOT_FOUND]=QObject::tr("Host not found!");
	arrayErrors[ERR_HTTP_HOST_CLOSED]=QObject::tr("Host closed connection!");
	arrayErrors[ERR_HTTP_OPERATION_TIMEOUT]=QObject::tr("Timeout error!");
	arrayErrors[ERR_HTTP_GENERAL_SOCKET_ERROR]=QObject::tr("General socket error!");
	arrayErrors[ERR_HTTP_NOT_CONNECTED]=QObject::tr("Socket not connected!");
	arrayErrors[ERR_HTTP_INVALID_HTTP_MESSAGE]=QObject::tr("Invalid HTTP message detected!");
	arrayErrors[ERR_HTTP_FAILED_CONNECT]=QObject::tr("Host not found!");
	arrayErrors[ERR_HTTP_SERVER_FAILED_TO_START]=QObject::tr("Server failed to start listener on IP address: %1, at port: %2, reason: %3!");
	arrayErrors[ERR_HTTP_SERVER_ALREADY_RUNNING]=QObject::tr("Server already running!");
	arrayErrors[ERR_HTTP_GENERAL_SOCKET_ERROR_DESC]=QObject::tr("General socket error: %1!");
	arrayErrors[MSG_HTTP_SOCKET_CREATED]=QObject::tr("Socket created: %1, ip: %2");
	arrayErrors[MSG_HTTP_SOCKET_CLOSED]=QObject::tr("Socket closed: %1, ip: %2");
	arrayErrors[ERR_HTTP_THREAD_FAILED_TO_CLOSED]=QObject::tr("Client thread failed to finish, terminate issued, socket_id: %1 ");
	arrayErrors[ERR_HTTP_REQUEST_BUFFER_INVALID]=QObject::tr("Request buffer invalid. Operation aborted!");
	arrayErrors[ERR_HTTP_SERVER_REJECTED_REQUEST]=QObject::tr("Server rejected request: %1");
	
	
	arrayErrors[ERR_SQL_LOCKING_FAIL]=QObject::tr("Record is locked by another user!");
	arrayErrors[ERR_SQL_TRANSACTION_FAIL]=QObject::tr("SQL transaction failed!");
	arrayErrors[ERR_SQL_ROLLBACK_FAIL]=QObject::tr("SQL rollback failed!");
	arrayErrors[ERR_SQL_STATEMENT_FAIL]=QObject::tr("SQL statement execution failed! Reason: %1");
	arrayErrors[ERR_SQL_UNKNOWN_DBTYPE]=QObject::tr("Unknown database driver type!");
	arrayErrors[ERR_SQL_UNKNOWN_TABLEVERSION]=QObject::tr("Unknown table version!");
	arrayErrors[ERR_SQL_SEQUENCE_FETCH_FAILED]=QObject::tr("Error occurred while fetching Last inserted id!");
	arrayErrors[ERR_SQL_SESSION_POOL_BLOCKED]=QObject::tr("General database error: there is no available database connection, please try again in few moments!");
	arrayErrors[ERR_SQL_UNLOCKING_FAIL]=QObject::tr("Resource unlocking failed!");
	arrayErrors[ERR_SQL_CONNECTION_RESERVATION_FAILED]=QObject::tr("Connection reservation failed. SQL error code: %1, SQL text: %2");
	arrayErrors[ERR_SQL_DB_DRIVER_MISSING]=QObject::tr("Database driver: %1 is missing or corrupted");
	arrayErrors[ERR_SQL_STATEMENT_FAIL_RELEASE]=QObject::tr("SQL statement execution failed!");
	arrayErrors[ERR_SQL_DUPLICATE_ENTRY]=QObject::tr("The value %1 has already been used for another record. Please change it before saving the record!");
	arrayErrors[ERR_SQL_FK_CONSTRAINT_VIOLATION]=QObject::tr("Can not modify or delete %1 record because there are %2 records that are related to it!");
	arrayErrors[ERR_SQL_DEADLOCK_TRYAGAIN]=QObject::tr("Deadlock detected, update failed!");
	arrayErrors[ERR_SQL_CURRENT_DB_CONNECTIONS]=QObject::tr("Number of active database connections: %1");
	arrayErrors[ERR_SQL_ORPHAN_TRANSACTIONS_DETECTED]=QObject::tr("Orphan Transactions detected: %1");
	arrayErrors[ERR_SQL_UNIQUE_CONSTRAINT_VIOLATION]=QObject::tr("Operation failed, duplicate value detected in the database!");

	
	
	arrayErrors[ERR_SYSTEM_SESSION_EXPIRED]=QObject::tr("Session Expired!");
	arrayErrors[ERR_SYSTEM_SERVERFULL]=QObject::tr("Server is full. Try again in few minutes!");
	arrayErrors[ERR_SYSTEM_SESSION_CREATION_FAILED]=QObject::tr("Session creation failed, login failed. Try again in few minutes!");
	arrayErrors[ERR_SYSTEM_NOT_LOGGED]=QObject::tr("Not Logged!");
	arrayErrors[ERR_SYSTEM_ALREADY_LOGGED]=QObject::tr("Already Logged!");
	arrayErrors[ERR_SYSTEM_MOD_LICENSE_NOT_EXISTS]=QObject::tr("Login failed. Module code and MLIID combination does not exist!");	
	arrayErrors[ERR_SYSTEM_MOD_LICENSE_MAX_REACHED]=QObject::tr("Login failed. Reached maximum number of license in use!");	
	arrayErrors[ERR_SYSTEM_MOD_LICENSE_INVALID]=QObject::tr("Application server session creation failed, invalid license number detected in system!");	
	arrayErrors[ERR_SYSTEM_APP_SESSION_LOST]=QObject::tr("Application server session creation failed, same session ID, but different data! Clean Up database!");	
	arrayErrors[ERR_SYSTEM_APP_SRV_FAILED_START]=QObject::tr("Application server failed to start. Reason: %1");	
	arrayErrors[MSG_SYSTEM_APP_SRV_SUCESS_START]=QObject::tr("Application server successfully started: %1, port: %2, SSL: %3, session: %4, thread: %5");	
	arrayErrors[ERR_SYSTEM_VERSION_MISMATCH]=QObject::tr("Application server & client software version does not match. Please update your software!");
	arrayErrors[ERR_SYSTEM_CONNECTION_BROKEN]=QObject::tr("Connection to server is lost!");
	arrayErrors[ERR_SYSTEM_SILENT_LOGIN_AGAIN]=QObject::tr("Please Re-login!");
	arrayErrors[ERR_SYSTEM_MASTER_DETECTED]=QObject::tr("There are already %1 LBO Servers active in systems and %2 MASTER Servers active.");
	arrayErrors[ERR_SYSTEM_SERVER_UPDATE_AT]=QObject::tr("Server is scheduled for update at %1. Please, save all your work and log off from server before update.");
	arrayErrors[ERR_SYSTEM_SERVER_RESETART_IN]=QObject::tr("Server is going for restart in %1 minutes. Please, save all your work and log off from server.");
	arrayErrors[ERR_SYSTEM_DEMO_DATABASE_WARNING]=QObject::tr("The database size limit is almost reached: The current database size has %1Mb, the maximal allowed size is %2Mb. Upgrade your license to get unlimited database access!");
	arrayErrors[ERR_SYSTEM_DEMO_DATABASE_LIMIT_ERROR]=QObject::tr("The database size limit is reached. Upgrade your license to get unlimited database access. Operation aborted!");
	arrayErrors[ERR_SYSTEM_BACKUP_MANAGER_BUSY]=QObject::tr("The database backup/restore operation is in progress, backup data can not be load/modified, please try again later!");
	arrayErrors[ERR_SYSTEM_BACKUP_MANAGER_NOT_EXISTS]=QObject::tr("Backup Manager not available!");
	arrayErrors[ERR_SYSTEM_APP_SERVER_NOT_EXISTS]=QObject::tr("Application server not available!");
	arrayErrors[ERR_SYSTEM_IMP_EXP_NOT_EXISTS]=QObject::tr("Import/Export Manager not available!");
	arrayErrors[MSG_SYSTEM_BACKUP_SUCCESS]=QObject::tr("New backup %1 successfully created!");
	arrayErrors[ERR_SYSTEM_IMP_EXP_MANAGER_BUSY]=QObject::tr("The import/export operation is in progress, import/export data can not be load/modified, please try again later!");
	arrayErrors[ERR_SYSTEM_IMP_EXP_FAIL]=QObject::tr("Import/export operation failed: %1!");
	arrayErrors[ERR_SYSTEM_BACKUP_FAIL]=QObject::tr("Backup operation failed: %1!");
	arrayErrors[ERR_SYSTEM_IMP_EXP_SUCCESS]=QObject::tr("Successfully processed: %1 records, failed to process: %2 at %3\nLast file processed: %4\nLast status: %5");
	arrayErrors[ERR_SYSTEM_IMP_EXP_SUCCESS_ALL]=QObject::tr("Import/export operation succeeded!");
	arrayErrors[MSG_SYSTEM_APP_SRV_SUCESS_STOP]=QObject::tr("Application server stopped");	
	arrayErrors[MSG_SYSTEM_DATABASE_CONN_SUCESS]=QObject::tr("Connected to the database: %1 on host: %2");	
	arrayErrors[ERR_SYSTEM_BACKUP_MANAGER_FAIL]=QObject::tr("Backup Manager failed to start! Reason: %1");	
	arrayErrors[MSG_SYSTEM_GARBAGE_START]=QObject::tr("Garbage Collector started, thread: %1");	
	arrayErrors[ERR_SYSTEM_GARBAGE_FAILED]=QObject::tr("Garbage Collector failed: %1");	
	arrayErrors[MSG_SYSTEM_GARBAGE_CLEANED_EXPIRED_SESSIONS]=QObject::tr("Garbage Collector successfully cleaned: %1 expired sessions");	
	arrayErrors[MSG_SYSTEM_GARBAGE_CLEANED_ORPHAN_SOCKETS]=QObject::tr("Garbage Collector successfully cleaned: %1 orphan client connections/sockets");	
	arrayErrors[MSG_SYSTEM_GARBAGE_CLEANED_ORPHAN_SESSIONS]=QObject::tr("Garbage Collector successfully cleaned: %1 orphan client sessions");	
	arrayErrors[MSG_SYSTEM_MASTER_GARBAGE_START]=QObject::tr("Master Garbage Collector started, thread: %1");	
	arrayErrors[ERR_SYSTEM_MASTER_GARBAGE_FAILED]=QObject::tr("Master Garbage Collector failed: %1");	
	arrayErrors[MSG_SYSTEM_MESSAGE_DISPATCHER_START]=QObject::tr("Message Dispatcher started, thread: %1");	
	arrayErrors[ERR_SYSTEM_MESSAGE_DISPATCHER_FAILED]=QObject::tr("Message Dispatcher failed: %1");
	arrayErrors[MSG_SYSTEM_IMP_EXP_START]=QObject::tr("Import/Export process started, thread: %1");	
	arrayErrors[MSG_SYSTEM_BACKUP_MANAGER_REORGANIZE_START]=QObject::tr("Database reorganization started.");	
	arrayErrors[MSG_SYSTEM_BACKUP_MANAGER_REORGANIZE_SUCCESS]=QObject::tr("Database successfully reorganized to version %1");	
	arrayErrors[MSG_SYSTEM_BACKUP_MANAGER_RESTORE_START]=QObject::tr("Restoring database from %1");	
	arrayErrors[MSG_SYSTEM_BACKUP_MANAGER_RESTORE_SUCCESS]=QObject::tr("Database successfully restored from %1");	
	arrayErrors[MSG_SYSTEM_BACKUP_START]=QObject::tr("Database backup started.");	
	
	arrayErrors[MSG_SYSTEM_IMP_EXP_PROJECT_START]=QObject::tr("Project import/export operation started.");	
	arrayErrors[ERR_SYSTEM_IMP_EXP_PROJECT_FAILED]=QObject::tr("Project import/export operation failed: %1");	
	arrayErrors[MSG_SYSTEM_IMP_EXP_PROJECT_NO_PROCESS]=QObject::tr("Project import/export operation ended. No files processed!");	
	arrayErrors[ERR_SYSTEM_IMP_EXP_PROJECT_RECORD_FAILED]=QObject::tr("Project record %1 from file %2 failed at import/export, reason: %3");
	arrayErrors[MSG_SYSTEM_IMP_EXP_PROJECT_PROCESS_FILE_SUCCESS]=QObject::tr("Project import/export operation - successfully processed file %1, successfully processed records: %2, failed records: %3");	
	arrayErrors[ERR_SYSTEM_IMP_EXP_PROJECT_PROCESS_FILE_FAILED]=QObject::tr("Project import/export operation - failed to processed file %1, reason: %2");	
	arrayErrors[MSG_SYSTEM_IMP_EXP_PROJECT_END]=QObject::tr("Project import/export operation ended.");	
	arrayErrors[MSG_SYSTEM_IMP_EXP_USER_START]=QObject::tr("User import/export operation started.");	
	arrayErrors[ERR_SYSTEM_IMP_EXP_USER_FAILED]=QObject::tr("User import/export operation failed: %1");	
	arrayErrors[MSG_SYSTEM_IMP_EXP_USER_NO_PROCESS]=QObject::tr("User import/export operation ended. No files processed!");	
	arrayErrors[ERR_SYSTEM_IMP_EXP_USER_RECORD_FAILED]=QObject::tr("User record %1 from file %2 failed at import/export, reason: %3");
	arrayErrors[MSG_SYSTEM_IMP_EXP_USER_PROCESS_FILE_SUCCESS]=QObject::tr("User import/export operation - successfully processed file %1, successfully processed records: %2, failed records: %3");	
	arrayErrors[ERR_SYSTEM_IMP_EXP_USER_PROCESS_FILE_FAILED]=QObject::tr("User import/export operation - failed to processed file %1, reason: %2");	
	arrayErrors[MSG_SYSTEM_IMP_EXP_USER_END]=QObject::tr("User import/export operation ended.");	
	arrayErrors[MSG_SYSTEM_IMP_EXP_DEBTOR_START]=QObject::tr("Debtor import/export operation started.");	
	arrayErrors[ERR_SYSTEM_IMP_EXP_DEBTOR_FAILED]=QObject::tr("Debtor import/export operation failed: %1");	
	arrayErrors[MSG_SYSTEM_IMP_EXP_DEBTOR_NO_PROCESS]=QObject::tr("Debtor import/export operation ended. No files processed!");	
	arrayErrors[ERR_SYSTEM_IMP_EXP_DEBTOR_RECORD_FAILED]=QObject::tr("Debtor record %1 from file %2 failed at import/export, reason: %3");
	arrayErrors[MSG_SYSTEM_IMP_EXP_DEBTOR_PROCESS_FILE_SUCCESS]=QObject::tr("Debtor import/export operation - successfully processed file %1, successfully processed records: %2, failed records: %3");	
	arrayErrors[ERR_SYSTEM_IMP_EXP_DEBTOR_PROCESS_FILE_FAILED]=QObject::tr("Debtor import/export operation - failed to processed file %1, reason: %2");	
	arrayErrors[MSG_SYSTEM_IMP_EXP_DEBTOR_END]=QObject::tr("Debtor import/export operation ended.");	
	arrayErrors[MSG_SYSTEM_IMP_EXP_CONTACT_START]=QObject::tr("Contact import/export operation started.");	
	arrayErrors[ERR_SYSTEM_IMP_EXP_CONTACT_FAILED]=QObject::tr("Contact import/export operation failed: %1");	
	arrayErrors[MSG_SYSTEM_IMP_EXP_CONTACT_NO_PROCESS]=QObject::tr("Contact import/export operation ended. No files processed!");	
	arrayErrors[ERR_SYSTEM_IMP_EXP_CONTACT_RECORD_FAILED]=QObject::tr("Contact record %1 from file %2 failed at import/export, reason: %3");
	arrayErrors[MSG_SYSTEM_IMP_EXP_CONTACT_PROCESS_FILE_SUCCESS]=QObject::tr("Contact import/export operation - successfully processed file %1, successfully processed records: %2, failed records: %3");	
	arrayErrors[ERR_SYSTEM_IMP_EXP_CONTACT_PROCESS_FILE_FAILED]=QObject::tr("Contact import/export operation - failed to processed file %1, reason: %2");	
	arrayErrors[MSG_SYSTEM_IMP_EXP_CONTACT_END]=QObject::tr("Contact import/export operation ended.");	
	arrayErrors[MSG_SYSTEM_IMP_EXP_SPL_START]=QObject::tr("Stored Project List import/export operation started.");	
	arrayErrors[ERR_SYSTEM_IMP_EXP_SPL_FAILED]=QObject::tr("Stored Project List import/export operation failed: %1");	
	arrayErrors[MSG_SYSTEM_IMP_EXP_SPL_NO_PROCESS]=QObject::tr("Stored Project List import/export operation ended. No files processed!");	
	arrayErrors[ERR_SYSTEM_IMP_EXP_SPL_RECORD_FAILED]=QObject::tr("Stored Project List record %1 from file %2 failed at import/export, reason: %3");
	arrayErrors[MSG_SYSTEM_IMP_EXP_SPL_PROCESS_FILE_SUCCESS]=QObject::tr("Stored Project List import/export operation - successfully processed file %1, successfully processed records: %2, failed records: %3");	
	arrayErrors[ERR_SYSTEM_IMP_EXP_SPL_PROCESS_FILE_FAILED]=QObject::tr("Stored Project List import/export operation - failed to processed file %1, reason: %2");	
	arrayErrors[MSG_SYSTEM_IMP_EXP_SPL_END]=QObject::tr("Stored Project List import/export operation ended.");	


	arrayErrors[MSG_SYSTEM_CLIENT_IS_ALIVE]=QObject::tr("Client is alive, thread: %1, ip: %2, user_id: %3");	
	arrayErrors[MSG_SYSTEM_CLIENT_LOGGED]=QObject::tr("User %1 has logged, thread: %2, ip: %3, socket_id: %4, user_id: %5, session: %6");	
	arrayErrors[MSG_SYSTEM_CLIENT_LOGOUT]=QObject::tr("User %1 has logged out, thread: %2, ip: %3, socket_id: %4, user_id: %5, session: %6");	
	arrayErrors[MSG_SYSTEM_APP_SESSION_DELETED]=QObject::tr("Deleted expired server session id: %1");	
	arrayErrors[ERR_SYSTEM_APP_SESSION_UPDATE_FAILED]=QObject::tr("Server failed to update server session. Reason: %1");	
	arrayErrors[ERR_SYSTEM_APP_SESSION_MASTER_ENCOUNTER_OWN]=QObject::tr("Master Garbage Collector encounter own session inside expired session list! Possible cause: deadlock prevented service to update session record!");	
	arrayErrors[MSG_SYSTEM_SRV_SESSION_START]=QObject::tr("Server Manager Process started, thread: %1");	
	arrayErrors[ERR_SYSTEM_SRV_SESSION_FAILED]=QObject::tr("Server Manager Process failed: %1");	
	arrayErrors[MSG_SYSTEM_SRV_SESSION_UPDATE_IS_SCHEDULED]=QObject::tr("Server is scheduled to restart in %1 seconds");	
	arrayErrors[MSG_SYSTEM_SRV_SESSION_UPDATE_START]=QObject::tr("Server is stopping. Update in progress, executing update script %1");	
	arrayErrors[ERR_SYSTEM_SRV_SESSION_UPDATE_FAILED]=QObject::tr("Server update failed. Failed to execute update script. Please check script location!");	
	arrayErrors[MSG_SYSTEM_USER_SESSIONS_CURRENT]=QObject::tr("Number of active user sessions: %1");	
	arrayErrors[MSG_SYSTEM_USER_SESSIONS_REAUTH]=QObject::tr("User %1 is successfully re authenticated, Dynamic Fail Over Process succeeded, session: %2, ip: %3, user_id: %4");	
	arrayErrors[MSG_SYSTEM_USER_SESSIONS_INACTIVE]=QObject::tr("User %1 session set to inactive, client connection broken, keeping session for Dynamic Fail Over Process, session: %2, ip: %3, user_id: %4");	
	arrayErrors[MSG_SYSTEM_USER_SESSIONS_CLEAN_INACTIVE]=QObject::tr("User Session Manager detected and cleaned %1 inactive orphan user sessions.");	
	arrayErrors[ERR_SYSTEM_USER_SESSIONS_FAILED]=QObject::tr("User Session Manager failed: %1");
	arrayErrors[ERR_SYSTEM_REAUTHENTICATE]=QObject::tr("Re authentication challenge issued to the user %1, session: %2, ip: %3, user_id: %4");
	arrayErrors[MSG_SYSTEM_USER_SESSIONS_MAX_CONNECTIONS_REACH]=QObject::tr("Server reached maximum connections, connection refused!");
	arrayErrors[MSG_SYSTEM_USER_SESSIONS_LBO_REDIRECT]=QObject::tr("Server reached maximum connections, redirecting client to %1");
	arrayErrors[ERR_SYSTEM_CLIENT_ISALIVE_FAILED]=QObject::tr("Client IsAlive function failed: %1");
	arrayErrors[ERR_SYSTEM_PARENT_SESSION_INVALID]=QObject::tr("Parent session does not exist, re-login again!");	
	arrayErrors[ERR_SYSTEM_FAILED_TO_CREATE_REMINDER]=QObject::tr("Failed to create reminder: %1");	
	arrayErrors[ERR_SYSTEM_FAILED_TO_SEND_REMINDER_SOKRATES]=QObject::tr("Failed to send '%1' reminder by Sokrates notification system.");	
	arrayErrors[ERR_SYSTEM_FAILED_TO_SEND_REMINDER_EMAIL]=QObject::tr("Failed to send '%1' reminder by E-mail (%2).");	
	arrayErrors[ERR_SYSTEM_FAILED_TO_SEND_REMINDER_SMS]=QObject::tr("Failed to send '%1' reminder by SMS (%2).");	
	arrayErrors[ERR_SYSTEM_FAILED_TO_SEND_REMINDER]=QObject::tr("Failed to send reminder with id: %1. Reason: %2");	
	arrayErrors[ERR_SYSTEM_REMINDER_GBC_FAILED]=QObject::tr("Reminder clean up failed: %1");	
	arrayErrors[ERR_SYSTEM_REMINDER_REBUILD_FAILED]=QObject::tr("Reminder list rebuild failed: %1");	
	arrayErrors[ERR_SYSTEM_FAILED_READ_REMINDER_SETTINGS]=QObject::tr("Failed to read reminder settings: %1");	
	arrayErrors[ERR_SYSTEM_REMINDER_MANAGER_NOT_EXISTS]=QObject::tr("Reminder Manager not available!");
	arrayErrors[ERR_SYSTEM_JAMES_SRV_FAILED_TO_REGISTER]=QObject::tr("Failed to register server: %1");
	arrayErrors[ERR_SYSTEM_JAMES_SRV_FAILED_TO_UNREGISTER]=QObject::tr("Failed to unregister server: %1");
	arrayErrors[ERR_SYSTEM_JAMES_SRV_FAILED_TO_FORWARD_PORT]=QObject::tr("Failed to forward port %1");
	arrayErrors[ERR_SYSTEM_JAMES_SRV_FAILED_TO_TEST_CONNECTIVITY]=QObject::tr("Failed to test server connectivity. Please check your Internet connection and status of forwarded port!");
	arrayErrors[ERR_SYSTEM_SC_APPSERVER_STARTING]=QObject::tr("Application server is starting.<br>Incoming connections are not yet possible. Please wait.");
	arrayErrors[ERR_SYSTEM_SC_APPSERVER_REORGANIZING_DATABASE]=QObject::tr("Application server is checking/reorganizing database.<br>This may take some time. Incoming connections are not possible. Please wait.");
	arrayErrors[ERR_SYSTEM_SC_APPSERVER_STARTING_SERVICES]=QObject::tr("Application server is starting services.<br>Incoming connections are not yet possible. Please wait.");
	arrayErrors[ERR_SYSTEM_SC_APPSERVER_STARTED_OK]=QObject::tr("Application server successfully started.");
	arrayErrors[ERR_SYSTEM_SC_APPSERVER_FAILED_TO_START]=QObject::tr("Application server failed to start.<br>%1");

	
	arrayErrors[ERR_SECURITY_RPC_INVALID_REQUEST]=QObject::tr("Client %1 is kicked from server, client was sending damaged XML-RPC messages!");
	arrayErrors[ERR_SECURITY_SESSION_REAUTH_EXPIRED]=QObject::tr("User %1 is kicked from server due to long re authentication time, client IP address %3 is banned, session: %2, user_id: %4");
	arrayErrors[ERR_SECURITY_HTTP_CONNECTION_FAILED]=QObject::tr("Client %1 is kicked from server. Reason: %2");
	arrayErrors[ERR_SECURITY_IP_BANNED]=QObject::tr("Client %1 address is banned, access refused!");
	arrayErrors[ERR_SECURITY_IP_KICKED]=QObject::tr("Client address %1 is kicked and added on the server kick-ban list!");
	

	arrayErrors[ERR_BUS_LOGIN_FAILED_WRONG_PASS_USER]=QObject::tr("Login failed! Invalid username or password!");
	arrayErrors[ERR_BUS_LOGIN_FAILED_NO_ACTIVE]=QObject::tr("Login failed! User has no permission to access system!");
	arrayErrors[ERR_BUS_LOGIN_FAILED_NO_SUBSCRIPTION]=QObject::tr("User has no permission to access system: subscription expired!");
	//arrayErrors[MSG_BUS_LOGIN_CLIENT]=QObject::tr("Client with username: %1 sucessfully logged!");
	arrayErrors[MSG_HIER_ENTITY_RECORD_DUPLICATE_CODE]=QObject::tr("Entry with this code already exists in the database!");
	arrayErrors[ERR_BUS_FAILED_TO_GENERATE_USERNAME]=QObject::tr("Can not create user, unable to generate unique username!");
	arrayErrors[ERR_BUS_FAILED_TO_LOCK_ENTITY]=QObject::tr("Can not lock record. Record does not exist in the database, probably was deleted by another user!");
	arrayErrors[ERR_BUS_FAILED_TO_LOAD_RECORD]=QObject::tr("Can not load record. Record does not exist in the database, probably was deleted by another user!");
	arrayErrors[ERR_BUS_EMAIL_CONTACT_ASSIGNMENT_EXIST]=QObject::tr("Email already have assigned contacts, you can not change assignment!");
	arrayErrors[ERR_SQL_LOCKED_BY_ANOTHER]=QObject::tr("Locked by: %1");
	arrayErrors[ERR_SQL_LOCKED_BY_ANOTHER_UNKNOWN]=QObject::tr("Locked by Another User!");
	arrayErrors[ERR_BUS_CONTACT_NOT_EXISTS]=QObject::tr("Operation failed, contact with id = %1 does not exist!");
	arrayErrors[ERR_BUS_PERSON_NOT_EXISTS]=QObject::tr("Operation failed, default person does not exist!");
	arrayErrors[ERR_BUS_TEMPLATE_DATA_NOT_EXISTS]=QObject::tr("Template Data could not be fetched!");
	arrayErrors[ERR_BUS_DEFAULT_ORG_NOT_EXISTS]=QObject::tr("No default organization entered!");
	arrayErrors[ERR_BUS_FETCHING_LICENSE]=QObject::tr("Error while fetching number of licenses in use!");
	arrayErrors[ERR_BUS_ACESS_REFUSED_ON_DELETE]=QObject::tr("Some Items could not be deleted due to insufficient access rights!");
	arrayErrors[ERR_BUS_FTP_EI_FAIL_CREATE_TEMP_DIR]=QObject::tr("Export/Import operation failed, can not create temporary directory: %1!");
	arrayErrors[ERR_BUS_FTP_EI_FAIL_CHANGE_TO_TEMP_DIR]=QObject::tr("Export/Import operation failed, can not access temporary directory: %1!");
	arrayErrors[ERR_BUS_FTP_EI_FAIL_DELETE_FILE]=QObject::tr("Export/Import operation failed, can not delete file: %1!");
	arrayErrors[ERR_BUS_ACCESS_DENIED]=QObject::tr("You do not have sufficient access rights to perform this operation!");
	arrayErrors[ERR_BUS_DOC_REVISION_NOT_FOUND]=QObject::tr("Operation failed, document does not have any revisions uploaded!");
	arrayErrors[ERR_BUS_USER_STORAGE_NO_DISK_SPACE]=QObject::tr("Operation failed, there is no more server disk space available!");
	arrayErrors[ERR_BUS_CHUNKED_BLOB2FILE_FAILED]=QObject::tr("Writing blob to file failed: %1!");
	arrayErrors[ERR_BUS_CHUNKED_FILE2BLOB_FAILED]=QObject::tr("Reading blob from file failed: %1!");
	arrayErrors[ERR_BUS_FAILED_TO_CHECK_OUT_DOCUMENT]=QObject::tr("Document can not be checked out because it is checked out by other user!");
	arrayErrors[ERR_BUS_DOCUMENT_FROM_ATTACH]=QObject::tr("Failed to create document from then attachment.File Error: %1!");
	arrayErrors[ERR_BUS_DUPLICATE_PROJECT_CODE]=QObject::tr("Operation failed. The hierarchical code is already used!");
	
	
	
	

	
}





