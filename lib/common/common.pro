TEMPLATE	= lib
CONFIG		= qt warn_on release staticlib
HEADERS		= common/config.h \
		  common/connsettings.h \
		  common/encryptedinifile.h \
		  common/inifile.h \
		  common/logger.h \
		  common/loggerabstract.h \
		  common/loggerfile.h \
		  common/loggerremote.h \
		  common/observer_ptrn.h \
		  common/rijndael.h \
		  common/rsakey.h \
		  common/serverthread.h \
		  common/sha2.h \
		  common/sha256hash.h \
		  common/sleeper.h \
		  common/status.h \
		  common/statuscodeset.h \
		  common/threadid.h \
		  common/threadsync.h
SOURCES		= common/encryptedinifile.cpp \
		  common/inifile.cpp \
		  common/logger.cpp \
		  common/loggerabstract.cpp \
		  common/loggerfile.cpp \
		  common/loggerremote.cpp \
		  common/observer_ptrn.cpp \
		  common/rijndael.cpp \
		  common/rsakey.cpp \
		  common/sha2.cpp \
		  common/sha256hash.cpp \
		  common/status.cpp \
		  common/statuscodeset.cpp \
		  common/threadid.cpp \
		  common/threadsync.cpp
INTERFACES	= 
TARGET		= common
LIBS		+= -lssl -lcrypto

include common/service/qtservice.pri
