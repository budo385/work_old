openssl genrsa -des3 -out sokrates.pkey 1024
openssl req -new -x509 -days 1001 -key sokrates.pkey -out sokrates.cert
