Last build is 1.0.1g. 4/2014:

To rebuild OpenSSL:
1. download latest OpenSSL source https://www.openssl.org/ and Perl: (ActivePerl)
2. extract SSL source to c:/OpenSSLSource
3. copy Configure.bat in c:/OpenSSLSource dir and execute
4. copy Build.bat in c:/OpenSSLSource dir and execute (Visual Studio must be in path  and ml.exe present)
5. new dll's are in c:/OpenSSLSource/out32dll
6. execute CopyFiles.bat in c:/OpenSSLSource to create and copy all new files into C:/OpenSSL (not needed), but copy all new libeay and ssleay dll's and libs in deploy and debug directories and in SokratesXP\lib\openssl\win32_build\

To recreate new appserver custom SSL certificates, use _create.bat batch (use MASTER_PASSWORD, defined in config.h), copy _create.bat in the c:\OpenSSLSource\out32dll\ where all files are if freshly builded or make sure OpenSSL binary files are in PATH
To create certificate request file to use verified/paid SSL certificate see _create_csr_request_file.bat

WARNING: libs are export, dll's are build as MD (multithread, release)
to change this edit ms/ntdll.mak change /MD to /MDd /MT or whatever then nmake!

