#ifndef DBCREATE_F_TAET_LIST_ENTRY_H
#define DBCREATE_F_TAET_LIST_ENTRY_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_TAET_LIST_ENTRY : public DbSqlTableCreation
{
public: 
	DbCreate_F_TAET_LIST_ENTRY():DbSqlTableCreation(F_TAET_LIST_ENTRY){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_TAET_LIST_ENTRY_H
