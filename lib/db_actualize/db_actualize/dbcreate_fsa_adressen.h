#ifndef DBCREATE_FSA_ADRESSEN_H
#define DBCREATE_FSA_ADRESSEN_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_FSA_ADRESSEN : public DbSqlTableCreation
{
public: 
	DbCreate_FSA_ADRESSEN():DbSqlTableCreation(FSA_ADRESSEN){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // FSA_ADRESSEN_H
