#include "dbcreate_bus_project.h"

//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_PROJECTS::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
									create table BUS_PROJECTS ( \
									BUSP_ID INTEGER not null,\
									BUSP_GLOBAL_ID VARCHAR(15) null,\
									BUSP_DAT_LAST_MODIFIED TIMESTAMP not null,\
									BUSP_CODE VARCHAR(50) not null,\
									BUSP_LEVEL INTEGER not null,\
									BUSP_PARENT INTEGER null,\
									BUSP_HASCHILDREN INTEGER not null,\
									BUSP_ICON VARCHAR(50) null,\
									BUSP_MAINPARENT_ID INTEGER null,\
									BUSP_NAME VARCHAR(100) not null,\
									BUSP_STYLE INTEGER not null,\
									BUSP_START_DATE DATE null,\
									BUSP_DEADLINE_DATE DATE null,\
									BUSP_COMPLETION_DATE DATE null,\
									BUSP_LATEST_COMPLETION_DATE DATE null,\
									BUSP_OFFER_DATE DATE null,\
									BUSP_OFFER_VALID_DATE DATE null,\
									BUSP_ORDER_DATE DATE null,\
									BUSP_CONTRACT_DATE DATE null,\
									BUSP_LEADER_ID INTEGER null,\
									BUSP_RESPONSIBLE_ID INTEGER null,\
									BUSP_DESCRIPTION LONGVARCHAR null,\
									BUSP_ACTIVE_FLAG INTEGER null,\
									BUSP_TEMPLATE_FLAG INTEGER null,\
									BUSP_LOCATION LONGVARCHAR null,\
									BUSP_TIME_BUDGET DECIMAL(10,2) null,\
									BUSP_FIXED_FEE DECIMAL(10,2) null,\
									BUSP_COST_BUDGET DECIMAL(10,2) null,\
									BUSP_ORGANIZATION_ID INTEGER null,\
									BUSP_DEPARTMENT_ID INTEGER null,\
									BUSP_DEFAULT_GRID_VIEW_ID INTEGER null,\
									BUSP_DAT_CREATED DATETIME null,\
									BUSP_FPROJCT_SEQ INTEGER null,\
									BUSP_INVOICING_TYPE SMALLINT null,\
									BUSP_PRODUCTIVE SMALLINT null,\
									BUSP_NOTCHARGEABLE SMALLINT null,\
									BUSP_INVOICEABLE SMALLINT null,\
									BUSP_ABSENTTIME SMALLINT null,\
									BUSP_HOLIDAYS SMALLINT null,\
									BUSP_STATUS_ID INTEGER null,\
									BUSP_AREA_ID INTEGER null,\
									BUSP_SECTOR_ID INTEGER null,\
									BUSP_INDUSTRY_ID INTEGER null,\
									BUSP_SERVICE_TYPE_ID INTEGER null,\
									BUSP_PROCESS_ID INTEGER null,\
									BUSP_RATE_CAT_ID INTEGER null,\
									BUSP_EXP_RATE_CAT_ID INTEGER null,\
									BUSP_PLAN_COST_CAT_ID INTEGER null,\
									BUSP_TYPE_ID INTEGER null,\
									BUSP_DEF_ACTIVITY_ID INTEGER null,\
									constraint BUS_PROJECTS_PK primary key (BUSP_ID))");

	//no Db versioning:
	m_lstDeletedCols<<"BCNT_DEFAULT_GRID_VIEW";
	m_lstDeletedCols<<"BUSP_SIBLING";

	lstSQL.append("\
										alter table BUS_PROJECTS\
										add constraint BUS_PERS_BUS_PROJ_FK1 foreign key (\
											BUSP_LEADER_ID)\
										 references BUS_PERSON (\
											BPER_ID) ON DELETE SET NULL");

	lstSQL.append("\
										alter table BUS_PROJECTS\
										add constraint BUS_PERS_BUS_PROJ_FK2 foreign key (\
											BUSP_RESPONSIBLE_ID)\
										 references BUS_PERSON (\
											BPER_ID) ON DELETE SET NULL");

	lstSQL.append("\
										alter table BUS_PROJECTS\
											add constraint BUS_ORG_PROJ_FK1 foreign key (\
											BUSP_ORGANIZATION_ID)\
										 references BUS_ORGANIZATIONS (\
											BORG_ID)");

	lstSQL.append("\
										alter table BUS_PROJECTS\
											add constraint BUS_DEP_PROJ_FK1 foreign key (\
											BUSP_DEPARTMENT_ID)\
										 references BUS_DEPARTMENTS (\
											BDEPT_ID)");

	lstSQL.append("\
										alter table BUS_PROJECTS\
											add constraint BUS_PROJ_PROJ_FK1 foreign key (\
											BUSP_PARENT)\
											references BUS_PROJECTS (\
											BUSP_ID) \
											ON DELETE CASCADE");

	lstSQL.append("\
										   alter table BUS_PROJECTS\
										   add constraint BUSP_DEF_GRID_VIEW_FK foreign key (\
										   BUSP_DEFAULT_GRID_VIEW_ID)\
										   references BUS_COMM_VIEW (\
										   BUSCV_ID) \
											");
	lstSQL.append("\
											alter table BUS_PROJECTS\
											add constraint BUSP_FK_MAINPARENT foreign key (\
											BUSP_MAINPARENT_ID)\
											references BUS_PROJECTS (\
											BUSP_ID)");

	lstSQL.append("alter table BUS_PROJECTS\
				  add constraint FK_BUSP_STATUS_ID foreign key (\
				  BUSP_STATUS_ID)\
				  references SPC_PR_STATUS (\
				  SFPS_ID) ON DELETE SET NULL");

	lstSQL.append("alter table BUS_PROJECTS\
				  add constraint FK_BUSP_AREA_ID foreign key (\
				  BUSP_AREA_ID)\
				  references SPC_PR_AREA (\
				  SEPB_ID) ON DELETE SET NULL");

	lstSQL.append("alter table BUS_PROJECTS\
				  add constraint FK_BUSP_SECTOR_ID foreign key (\
				  BUSP_SECTOR_ID)\
				  references SPC_SECTOR (\
				  SEFG_ID) ON DELETE SET NULL");

	lstSQL.append("alter table BUS_PROJECTS\
				  add constraint FK_BUSP_INDUSTRY_ID foreign key (\
				  BUSP_INDUSTRY_ID)\
				  references SPC_INDUSTRY_GROUP (\
				  SEWG_ID) ON DELETE SET NULL");

	lstSQL.append("alter table BUS_PROJECTS\
				  add constraint FK_BUSP_SERVICE_TYPE_ID foreign key (\
				  BUSP_SERVICE_TYPE_ID)\
				  references SPC_SERVICE_TYPE (\
				  SELA_ID) ON DELETE SET NULL");

	lstSQL.append("alter table BUS_PROJECTS\
				  add constraint FK_BUSP_PROCESS_ID foreign key (\
				  BUSP_PROCESS_ID)\
				  references SPC_PROCESS (\
				  SEPZ_ID) ON DELETE SET NULL");

	lstSQL.append("alter table BUS_PROJECTS\
				  add constraint FK_BUSP_RATE_CAT_ID foreign key (\
				  BUSP_RATE_CAT_ID)\
				  references SPC_RATE_CAT (\
				  SETK_ID) ON DELETE SET NULL");

	lstSQL.append("alter table BUS_PROJECTS\
				  add constraint FK_BUSP_EXP_RATE_CAT_ID foreign key (\
				  BUSP_EXP_RATE_CAT_ID)\
				  references SPC_RATE_CAT (\
				  SETK_ID) ON DELETE SET NULL");

	lstSQL.append("alter table BUS_PROJECTS\
				  add constraint FK_BUSP_PLAN_COST_CAT_ID foreign key (\
				  BUSP_PLAN_COST_CAT_ID)\
				  references SPC_RATE_CAT (\
				  SETK_ID) ON DELETE SET NULL");

	lstSQL.append("alter table BUS_PROJECTS\
				  add constraint FK_BUSP_TYPE_ID foreign key (\
				  BUSP_TYPE_ID)\
				  references SPC_PR_TYPE (\
				  SEAP_ID) ON DELETE SET NULL");

	lstSQL.append("alter table BUS_PROJECTS\
				  add constraint FK_BUSP_DEF_ACTIVITY_ID foreign key (\
				  BUSP_DEF_ACTIVITY_ID)\
				  references SPC_ACTIVITY (\
				  SET_ID) ON DELETE SET NULL");
	
}

//probably common to all DB's:
void DbCreate_BUS_PROJECTS::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
	dboList["IND_BUSP_PARENT"] = "create index IND_BUSP_PARENT on BUS_PROJECTS (BUSP_PARENT)";  
	dboList["IND_BUSP_UNQ_HIER"] = "create index IND_BUSP_UNQ_HIER on BUS_PROJECTS (BUSP_LEVEL  ,BUSP_PARENT   )";  
	dboList["IND_BUS_PROJ_CODE"] = "create unique index IND_BUS_PROJ_CODE on BUS_PROJECTS (BUSP_CODE)";  
	dboList["IND_BUSP_LEVEL"] = "create index IND_BUSP_LEVEL on BUS_PROJECTS (BUSP_LEVEL  )  ";  
	dboList["IND_BUSP_ORG_ID"] = "create index IND_BUSP_ORG_ID on BUS_PROJECTS (	BUSP_ORGANIZATION_ID  ) ";  
	dboList["IND_BUSP_LAST_MODIFIED"] = "create index IND_BUSP_LAST_MODIFIED on BUS_PROJECTS ( BUSP_DAT_LAST_MODIFIED )";
		 
}


void DbCreate_BUS_PROJECTS::DefineTriggers(SQLDBObjectCreate &dboList)
{

	//create triggers TOFIX: foe each DB, must be different
	dboList["TRG_BUS_PROJECT_DELETE"]="\
									 CREATE TRIGGER TRG_BUS_PROJECT_DELETE FOR BUS_PROJECTS\
									 ACTIVE AFTER DELETE AS \
									 BEGIN \
									 DELETE FROM bus_nmrx_relation WHERE OLD.BUSP_ID = BNMR_TABLE_KEY_ID_1 AND BNMR_TABLE_1="+QVariant(BUS_PROJECT).toString()+";\
									 DELETE FROM bus_nmrx_relation WHERE OLD.BUSP_ID = BNMR_TABLE_KEY_ID_2 AND BNMR_TABLE_2="+QVariant(BUS_PROJECT).toString()+";\
									 DELETE FROM CORE_ACC_USER_REC WHERE OLD.BUSP_ID = CUAR_RECORD_ID AND CUAR_TABLE_ID="+QVariant(BUS_PROJECT).toString()+";\
									 DELETE FROM CORE_ACC_GROUP_REC WHERE OLD.BUSP_ID = CGAR_RECORD_ID AND CGAR_TABLE_ID="+QVariant(BUS_PROJECT).toString()+";\
									 DELETE FROM BUS_CUSTOM_BOOL WHERE OLD.BUSP_ID = BCB_RECORD_ID AND BCB_TABLE_ID="+QVariant(BUS_PROJECT).toString()+";\
									 DELETE FROM BUS_CUSTOM_DATE WHERE OLD.BUSP_ID = BCD_RECORD_ID AND BCD_TABLE_ID="+QVariant(BUS_PROJECT).toString()+";\
									 DELETE FROM BUS_CUSTOM_DATETIME WHERE OLD.BUSP_ID = BCDT_RECORD_ID AND BCDT_TABLE_ID="+QVariant(BUS_PROJECT).toString()+";\
									 DELETE FROM BUS_CUSTOM_FLOAT WHERE OLD.BUSP_ID = BCFL_RECORD_ID AND BCFL_TABLE_ID="+QVariant(BUS_PROJECT).toString()+";\
									 DELETE FROM BUS_CUSTOM_INT WHERE OLD.BUSP_ID = BCI_RECORD_ID AND BCI_TABLE_ID="+QVariant(BUS_PROJECT).toString()+";\
									 DELETE FROM BUS_CUSTOM_LONGTEXT WHERE OLD.BUSP_ID = BCLT_RECORD_ID AND BCLT_TABLE_ID="+QVariant(BUS_PROJECT).toString()+";\
									 DELETE FROM BUS_CUSTOM_TEXT WHERE OLD.BUSP_ID = BCT_RECORD_ID AND BCT_TABLE_ID="+QVariant(BUS_PROJECT).toString()+";\
									 END\
									 ";

}

