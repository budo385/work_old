#include "dbcreate_core_acc_user_rec.h"


//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_CORE_ACC_USER_REC::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
								   create table CORE_ACC_USER_REC (\
								   CUAR_ID INTEGER not null,\
								   CUAR_GLOBAL_ID VARCHAR(15) null,\
								   CUAR_DAT_LAST_MODIFIED TIMESTAMP not null,\
								   CUAR_TABLE_ID INTEGER not null,\
								   CUAR_RECORD_ID INTEGER not null,\
								   CUAR_PERSON_ID INTEGER null,\
								   CUAR_PARENT_ID INTEGER null,\
								   CUAR_CHILD_INHERIT SMALLINT not null,\
								   CUAR_CHILD_COMM_INHERIT SMALLINT not null,\
								   CUAR_GROUP_DEPENDENT SMALLINT not null,\
								   CUAR_RIGHT SMALLINT null, constraint CORE_ACC_USER_REC_PK primary key (CUAR_ID) )");  



	lstSQL.append("\
								   alter table CORE_ACC_USER_REC\
								   add constraint CORE_ACC_USER_REC_FK1 foreign key (\
								   CUAR_PARENT_ID)\
								   references CORE_ACC_USER_REC (\
								   CUAR_ID)\
								   ON DELETE SET NULL");

	lstSQL.append("\
								   alter table CORE_ACC_USER_REC\
								   add constraint PERSON_ACC_USER_REC_FK1 foreign key (\
								   CUAR_PERSON_ID)\
								   references BUS_PERSON (\
								   BPER_ID)\
								   ON DELETE CASCADE");
	
	
}


//probably common to all DB's:
void DbCreate_CORE_ACC_USER_REC::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
	dboList["IND_CORE_ACC_USR_UNIQUE"] = "create unique index IND_CORE_ACC_USR_UNIQUE on CORE_ACC_USER_REC (CUAR_TABLE_ID,CUAR_RECORD_ID,CUAR_PERSON_ID)";  
	dboList["IND_CORE_ACC_USR_SELECT"] = "create index IND_CORE_ACC_USR_SELECT on CORE_ACC_USER_REC (CUAR_TABLE_ID,CUAR_RECORD_ID)";  
	dboList["IND_CORE_ACC_USR_PARENT"] = "create index IND_CORE_ACC_USR_PARENT on CORE_ACC_USER_REC (CUAR_PARENT_ID )";  
	//dboList["IND_CORE_ACC_USR_RECORD"] = "create index IND_CORE_ACC_USR_RECORD on CORE_ACC_USER_REC (CUAR_TABLE_ID,CUAR_RECORD_ID)";
}




