#ifndef DBCREATE_F_COUNTERS_H
#define DBCREATE_F_COUNTERS_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_COUNTERS : public DbSqlTableCreation
{
public: 
	DbCreate_F_COUNTERS():DbSqlTableCreation(F_COUNTERS){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_COUNTERS_H
