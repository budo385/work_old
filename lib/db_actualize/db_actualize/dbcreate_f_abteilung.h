#ifndef DBCREATE_F_ABTEILUNG_H
#define DBCREATE_F_ABTEILUNG_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_ABTEILUNG : public DbSqlTableCreation
{
public: 
	DbCreate_F_ABTEILUNG():DbSqlTableCreation(F_ABTEILUNG){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_ABTEILUNG_H
