#include "dbcreate_bus_dm_meta_data.h"


//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_DM_META_DATA::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
								   create table BUS_DM_META_DATA (\
								   BDME_ID INTEGER not null,\
								   BDME_GLOBAL_ID VARCHAR(15) null,\
								   BDME_DAT_LAST_MODIFIED TIMESTAMP not null,\
								   BDME_SERVER_FILE_ID INTEGER not null,\
								   BDME_VALUE_TYPE INTEGER not null,\
								   BDME_VALUE VARCHAR(1000) not null, constraint BUS_DM_META_DATA_PK primary key (BDME_ID) ) ");  


	lstSQL.append("\
								   alter table BUS_DM_META_DATA\
								   add constraint BUS_SRV_FILE_META_DAT_FK1 foreign key (\
								   BDME_SERVER_FILE_ID)\
								   references BUS_DM_SERVER_FILE (BDSF_ID) ");

}



//probably common to all DB's:
void DbCreate_BUS_DM_META_DATA::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();

	//dboList["IND_BDME_VALUE_TYPE"] = "create index IND_BDME_VALUE_TYPE on BUS_DM_META_DATA (BDME_VALUE_TYPE)";  
	//dboList["IND_BDME_VALUE"] = "create index IND_BDME_VALUE on BUS_DM_META_DATA (BDME_VALUE)";  
	  
}



