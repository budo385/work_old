#ifndef DBCREATE_BUS_DM_DOCUMENTS_H
#define DBCREATE_BUS_DM_DOCUMENTS_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"




class DbCreate_BUS_DM_DOCUMENTS : public DbSqlTableCreation
{
public:
	DbCreate_BUS_DM_DOCUMENTS():DbSqlTableCreation(BUS_DM_DOCUMENTS){};  //init table
	

private:
	//create generic Table def:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);

	//probably common to all DB's:
	void DefineIndexes(SQLDBObjectCreate &dboList);
};
#endif // DBCREATE_BUS_DM_DOCUMENTS_H
