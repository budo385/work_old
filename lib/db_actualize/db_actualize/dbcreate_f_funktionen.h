#ifndef DBCREATE_F_FUNKTIONEN_H
#define DBCREATE_F_FUNKTIONEN_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_FUNKTIONEN : public DbSqlTableCreation
{
public: 
	DbCreate_F_FUNKTIONEN():DbSqlTableCreation(F_FUNKTIONEN){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_FUNKTIONEN_H
