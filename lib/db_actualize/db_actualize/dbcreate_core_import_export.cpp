#include "dbcreate_core_import_export.h"



//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_CORE_IMPORT_EXPORT::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
								   create table CORE_IMPORT_EXPORT ( \
								   CIE_ID INTEGER not null, \
								   CIE_GLOBAL_ID VARCHAR(15) null, \
								   CIE_DAT_LAST_MODIFIED TIMESTAMP null, \
								   CIE_GLOBAL_SCAN_PERIOD SMALLINT not null, \
								   CIE_PROJ_ENABLED SMALLINT null, \
								   CIE_PROJ_IMPORT SMALLINT null, \
								   CIE_PROJ_SCAN_TYPE SMALLINT null, \
								   CIE_PROJ_SCAN_PERIOD INTEGER null, \
								   CIE_PROJ_SOURCE SMALLINT null, \
								   CIE_PROJ_SOURCE_DIR VARCHAR(250) null, \
								   CIE_PROJ_SOURCE_FTP_ID INTEGER null, \
								   CIE_PROJ_SOURCE_FTP_DIR VARCHAR(250) null, \
								   CIE_PROJ_FILE_EXT VARCHAR(50) null, \
								   CIE_PROJ_FILE_PREFIX VARCHAR(50) null, \
								   CIE_PROJ_FILE_PREFIX_PROCESS VARCHAR(50) null, \
								   CIE_PROJ_EXPORT_TYPE SMALLINT null, \
								   CIE_PROJ_EXPORT_LAST_N INTEGER null, \
								   CIE_PROJ_ADD_DATETIME SMALLINT null, \
								   CIE_PROJ_STATUS_DATE DATETIME null, \
								   CIE_PROJ_STATUS_ERROR VARCHAR(250) null, \
								   CIE_PROJ_STATUS_LAST_FILE VARCHAR(250) null, \
								   CIE_PROJ_STATUS_REC_OK INTEGER null, \
								   CIE_PROJ_STATUS_REC_FAIL INTEGER null, \
								   CIE_USER_ENABLED SMALLINT null, \
								   CIE_USER_IMPORT SMALLINT null, \
								   CIE_USER_SCAN_TYPE SMALLINT null, \
								   CIE_USER_SCAN_PERIOD INTEGER null, \
								   CIE_USER_SOURCE SMALLINT null, \
								   CIE_USER_SOURCE_DIR VARCHAR(250) null, \
								   CIE_USER_SOURCE_FTP_ID INTEGER null, \
								   CIE_USER_SOURCE_FTP_DIR VARCHAR(250) null, \
								   CIE_USER_FILE_EXT VARCHAR(50) null, \
								   CIE_USER_FILE_PREFIX VARCHAR(50) null, \
								   CIE_USER_FILE_PREFIX_PROCESS VARCHAR(50) null, \
								   CIE_USER_EXPORT_TYPE SMALLINT null, \
								   CIE_USER_EXPORT_LAST_N INTEGER null, \
								   CIE_USER_ADD_DATETIME SMALLINT null, \
								   CIE_USER_STATUS_DATE DATETIME null, \
								   CIE_USER_STATUS_ERROR VARCHAR(250) null, \
								   CIE_USER_STATUS_LAST_FILE VARCHAR(250) null, \
								   CIE_USER_STATUS_REC_OK INTEGER null, \
								   CIE_USER_STATUS_REC_FAIL INTEGER null, \
								   CIE_USER_DEF_ROLE_ID INTEGER null, \
								   CIE_DEBT_ENABLED SMALLINT null, \
								   CIE_DEBT_IMPORT SMALLINT null, \
								   CIE_DEBT_SCAN_TYPE SMALLINT null, \
								   CIE_DEBT_SCAN_PERIOD INTEGER null, \
								   CIE_DEBT_SOURCE SMALLINT null, \
								   CIE_DEBT_SOURCE_DIR VARCHAR(250) null, \
								   CIE_DEBT_SOURCE_FTP_ID INTEGER null, \
								   CIE_DEBT_SOURCE_FTP_DIR VARCHAR(250) null, \
								   CIE_DEBT_FILE_EXT VARCHAR(50) null, \
								   CIE_DEBT_FILE_PREFIX VARCHAR(50) null, \
								   CIE_DEBT_FILE_PREFIX_PROCESS VARCHAR(50) null, \
								   CIE_DEBT_EXPORT_TYPE SMALLINT null, \
								   CIE_DEBT_EXPORT_LAST_N INTEGER null, \
								   CIE_DEBT_ADD_DATETIME SMALLINT null, \
								   CIE_DEBT_STATUS_DATE DATETIME null, \
								   CIE_DEBT_STATUS_ERROR VARCHAR(250) null, \
								   CIE_DEBT_STATUS_LAST_FILE VARCHAR(250) null, \
								   CIE_DEBT_STATUS_REC_OK INTEGER null, \
								   CIE_DEBT_STATUS_REC_FAIL INTEGER null, \
								   CIE_DEBT_ADDRESS_TYPE_ID INTEGER null, \
								   CIE_CONT_ENABLED SMALLINT null, \
								   CIE_CONT_IMPORT SMALLINT null, \
								   CIE_CONT_SCAN_TYPE SMALLINT null, \
								   CIE_CONT_SCAN_PERIOD INTEGER null, \
								   CIE_CONT_SOURCE SMALLINT null, \
								   CIE_CONT_SOURCE_DIR VARCHAR(250) null, \
								   CIE_CONT_SOURCE_FTP_ID INTEGER null, \
								   CIE_CONT_SOURCE_FTP_DIR VARCHAR(250) null, \
								   CIE_CONT_FILE_EXT VARCHAR(50) null, \
								   CIE_CONT_FILE_PREFIX VARCHAR(50) null, \
								   CIE_CONT_FILE_PREFIX_PROCESS VARCHAR(50) null, \
								   CIE_CONT_EXPORT_TYPE SMALLINT null, \
								   CIE_CONT_EXPORT_LAST_N INTEGER null, \
								   CIE_CONT_ADD_DATETIME SMALLINT null, \
								   CIE_CONT_STATUS_DATE DATETIME null, \
								   CIE_CONT_STATUS_ERROR VARCHAR(250) null, \
								   CIE_CONT_STATUS_LAST_FILE VARCHAR(250) null, \
								   CIE_CONT_STATUS_REC_OK INTEGER null, \
								   CIE_CONT_STATUS_REC_FAIL INTEGER,\
								   CIE_USE_UNICODE INTEGER,\
								   CIE_SPLST_ENABLED SMALLINT null, \
								   CIE_SPLST_IMPORT SMALLINT null, \
								   CIE_SPLST_SCAN_TYPE SMALLINT null, \
								   CIE_SPLST_SCAN_PERIOD INTEGER null, \
								   CIE_SPLST_SOURCE SMALLINT null, \
								   CIE_SPLST_SOURCE_DIR VARCHAR(250) null, \
								   CIE_SPLST_SOURCE_FTP_ID INTEGER null, \
								   CIE_SPLST_SOURCE_FTP_DIR VARCHAR(250) null, \
								   CIE_SPLST_FILE_EXT VARCHAR(50) null, \
								   CIE_SPLST_FILE_PREFIX VARCHAR(50) null, \
								   CIE_SPLST_FILE_PREFIX_PROCESS VARCHAR(50) null, \
								   CIE_SPLST_EXPORT_TYPE SMALLINT null, \
								   CIE_SPLST_EXPORT_LAST_N INTEGER null, \
								   CIE_SPLST_ADD_DATETIME SMALLINT null, \
								   CIE_SPLST_STATUS_DATE DATETIME null, \
								   CIE_SPLST_STATUS_ERROR VARCHAR(250) null, \
								   CIE_SPLST_STATUS_LAST_FILE VARCHAR(250) null, \
								   CIE_SPLST_STATUS_REC_OK INTEGER null, \
								   CIE_SPLST_STATUS_REC_FAIL INTEGER null, \
								   constraint CORE_IMPORT_EXPORT_PK primary key (CIE_ID) )");  


	lstSQL.append("\
								   alter table CORE_IMPORT_EXPORT\
								   add constraint CORE_IMP_FTP_FK1 foreign key (\
								   CIE_PROJ_SOURCE_FTP_ID)\
								   references CORE_FTP_SERVERS (\
								   CFTP_ID)  \
								   ON DELETE SET NULL");

	//B.T.: I can not execute this constraint for some reason: when done from IBmanager it works...brb!?
/*
	lstSQL.append("\
				  alter table CORE_IMPORT_EXPORT\
				  add constraint CIE_USER_DEF_ROLE_FK foreign key (\
				  CIE_USER_DEF_ROLE_ID)\
				  references CORE_ROLE (\
				  CROL_ID)  \
				  ON DELETE SET NULL");
*/

	

	/*
	lstSQL.append("\
								   alter table CORE_IMPORT_EXPORT\
								   add constraint COR_I_FTP_FK2 foreign key (\
								   CIE_USER_SOURCE_FTP_ID)\
								   references CORE_FTP_SERVERS (\
								   CFTP_ID)  \
								   ON DELETE SET NULL");

	
	lstSQL.append("\
								   alter table CORE_IMPORT_EXPORT\
								   add constraint CORE_I_FTP_FK3 foreign key (\
								   CIE_DEBT_SOURCE_FTP_ID)\
								   references CORE_FTP_SERVERS (\
								   CFTP_ID)  \
								   ON DELETE SET NULL");

	lstSQL.append("\
								   alter table CORE_IMPORT_EXPORT\
								   add constraint CORE_IMP_EXP_FTP_FK4 foreign key (\
								   CIE_CONT_SOURCE_FTP_ID)\
								   references CORE_FTP_SERVERS (\
								   CFTP_ID)  \
								   ON DELETE SET NULL");
				*/				   
}



//probably common to all DB's:
void DbCreate_CORE_IMPORT_EXPORT::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();

}


