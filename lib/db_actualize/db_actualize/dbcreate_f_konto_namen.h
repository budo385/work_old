#ifndef DBCREATE_F_KONTO_NAMEN_H
#define DBCREATE_F_KONTO_NAMEN_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_KONTO_NAMEN : public DbSqlTableCreation
{
public: 
	DbCreate_F_KONTO_NAMEN():DbSqlTableCreation(F_KONTO_NAMEN){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_KONTO_NAMEN_H
