#include "dbcreate_bus_cm_debtor.h"




//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_CM_DEBTOR::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
											create table BUS_CM_DEBTOR ( \
											BCMD_ID INTEGER not null,\
											BCMD_GLOBAL_ID VARCHAR(15) null,\
											BCMD_DAT_LAST_MODIFIED TIMESTAMP not null,\
											BCMD_CONTACT_ID INTEGER not null,\
											BCMD_DEBTORCODE VARCHAR(50) null,\
											BCMD_DEBTORACCOUNT VARCHAR(50) null,\
											BCMD_DESCRIPTION LONGVARCHAR null,\
											BCMD_CUSTOMERCODE VARCHAR(80) null,\
											BCMD_INVOICE_TEXT LONGVARCHAR null,\
											BCMD_PAYMENT_ID INTEGER null,\
											BCMD_INTERNAL_DEBTOR SMALLINT null,	constraint BUS_CM_DEBTOR_PK primary key (BCMD_ID) )  ");

	//constraint for table creation:
	lstSQL.append("\
											alter table BUS_CM_DEBTOR\
											add constraint BUS_CONTACTS_DEBTOR_FK1 foreign key (\
											BCMD_CONTACT_ID)\
											references BUS_CM_CONTACT (\
											BCNT_ID)  \
											ON DELETE CASCADE");

	lstSQL.append("\
										   alter table BUS_CM_DEBTOR\
										   add constraint BUS_PAYMENT_COND_FK1 foreign key (\
										   BCMD_PAYMENT_ID)\
										   references BUS_CM_PAYMENT_CONDITIONS (\
										   BCMPY_ID)");  
}



//probably common to all DB's:
void DbCreate_BUS_CM_DEBTOR::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
	dboList["IND_BCMD_CONTACT_ID"] = "create index IND_BCMD_CONTACT_ID on BUS_CM_DEBTOR (BCMD_CONTACT_ID)  ";  
	dboList["IND_BCMD_PAYMENT_ID"] = "create index IND_BCMD_PAYMENT_ID on BUS_CM_DEBTOR (BCMD_PAYMENT_ID)";  
	dboList["IND_BCMD_DEBTORCODE"] = "create index IND_BCMD_DEBTORCODE on BUS_CM_DEBTOR (BCMD_DEBTORCODE)";  
}


