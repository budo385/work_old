#include "dbcreate_bus_group_items.h"

//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_GROUP_ITEMS::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
											create table BUS_GROUP_ITEMS ( \
											BGIT_ID INTEGER not null,\
											BGIT_GLOBAL_ID VARCHAR(15) null,\
											BGIT_DAT_LAST_MODIFIED TIMESTAMP not null,\
											BGIT_CODE VARCHAR(50) not null,\
											BGIT_LEVEL INTEGER not null,\
											BGIT_PARENT INTEGER null,\
											BGIT_HASCHILDREN INTEGER not null,\
											BGIT_ICON VARCHAR(50) null,\
											BGIT_MAINPARENT_ID INTEGER null,\
											BGIT_NAME VARCHAR(100) not null,\
											BGIT_STYLE INTEGER null,\
											BGIT_DESCRIPTION LONGVARCHAR null,\
											BGIT_EXTERNAL_CATEGORY VARCHAR(100) null,\
											BGIT_TREE_ID INTEGER not null, constraint BUS_GROUP_ITEMS_PK primary key (BGIT_ID) ) ");

	m_lstDeletedCols<<"BGIT_SIBLING";


	lstSQL.append("\
											alter table BUS_GROUP_ITEMS\
											add constraint BUS_TREE_GR_ITEM_FK foreign key (\
											BGIT_TREE_ID)\
											references BUS_GROUP_TREE (\
											BGTR_ID)  \
											ON DELETE CASCADE");


	lstSQL.append("\
											alter table BUS_GROUP_ITEMS\
											add constraint BUS_GROUP_ITEMS_FK_PARENT foreign key (\
											BGIT_PARENT)\
											references BUS_GROUP_ITEMS (\
											BGIT_ID) ON DELETE CASCADE");

	lstSQL.append("\
											alter table BUS_GROUP_ITEMS\
											add constraint BUS_GROUP_ITEMS_FK_MAINPARENT foreign key (\
											BGIT_MAINPARENT_ID)\
											references BUS_GROUP_ITEMS (\
											BGIT_ID)");

	//no Db versioning:
	
}



//probably common to all DB's:
void DbCreate_BUS_GROUP_ITEMS::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
	dboList["IND_GROUP_GROUP_CODE"] = "create unique index IND_GROUP_GROUP_CODE on BUS_GROUP_ITEMS (BGIT_CODE,	BGIT_TREE_ID)  ";  
	dboList["IND_GROUP_LEVEL"] = "create index IND_GROUP_LEVEL on BUS_GROUP_ITEMS (BGIT_LEVEL  ,BGIT_TREE_ID  )  ";  
	dboList["IND_GROUP_PARENT_ID"] = "create index IND_GROUP_PARENT_ID on BUS_GROUP_ITEMS (BGIT_PARENT  ,	BGIT_TREE_ID  )  ";  
	dboList["IND_GROUP_UNQ_HIER"] = "create index IND_GROUP_UNQ_HIER on BUS_GROUP_ITEMS (BGIT_LEVEL  ,BGIT_PARENT  , BGIT_TREE_ID  ) ";  

}




void DbCreate_BUS_GROUP_ITEMS::DefineTriggers(SQLDBObjectCreate &dboList)
{
	//create triggers TOFIX: foe each DB, must be different
	dboList["TRG_BUS_GROUP_ITEMS_DELETE"]="\
									  CREATE TRIGGER TRG_BUS_GROUP_ITEMS_DELETE FOR BUS_GROUP_ITEMS\
									  ACTIVE AFTER DELETE AS \
										 BEGIN \
										 DELETE FROM CORE_ACC_GROUP_REC WHERE OLD.BGIT_ID = CGAR_GROUP_ID;\
										 END\
										 ";

}