#include "dbcreate_bus_big_picture.h"



//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_BIG_PICTURE::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
											create table BUS_BIG_PICTURE ( \
											BPIC_ID INTEGER not null,\
											BPIC_GLOBAL_ID VARCHAR(15) null,\
											BPIC_DAT_LAST_MODIFIED TIMESTAMP not null,\
											BPIC_PICTURE LONGVARBINARY not null, constraint BUS_BIG_PICTURE_PK primary key (BPIC_ID) )"); 

	m_lstDeletedCols<<"BPIC_TYPE_ID";
}



//probably common to all DB's:
void DbCreate_BUS_BIG_PICTURE::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
}


