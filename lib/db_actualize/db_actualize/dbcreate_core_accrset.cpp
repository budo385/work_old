#include "dbcreate_core_accrset.h"
//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_CORE_ACCRSET::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
											create table CORE_ACCRSET (					\
											CAST_ID INTEGER not null,					\
											CAST_GLOBAL_ID VARCHAR(15) null,			\
											CAST_DAT_LAST_MODIFIED TIMESTAMP not null,	\
											CAST_NAME VARCHAR(50) not null,				\
											CAST_ACCRSET_DESC VARCHAR(50) null,			\
											CAST_ACCRSET_TYPE INTEGER not null,			\
											CAST_CODE INTEGER not null,					\
											constraint CORE_ACCRSET_PK primary key (CAST_ID) )");
	
}

//probably common to all DB's:
void DbCreate_CORE_ACCRSET::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
}
