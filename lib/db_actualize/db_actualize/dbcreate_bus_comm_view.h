#ifndef DBCREATE_BUS_COMM_VIEW_H
#define DBCREATE_BUS_COMM_VIEW_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"

class DbCreate_BUS_COMM_VIEW : public DbSqlTableCreation
{
public:
	DbCreate_BUS_COMM_VIEW():DbSqlTableCreation(BUS_COMM_VIEW){};  //init table

private:
	//create generic Table def:
	void DefineFields(QStringList &lstSQL);

	//probably common to all DB's:
	void DefineIndexes(SQLDBObjectCreate &dboList);
};

#endif // DBCREATE_BUS_COMM_VIEW_H
