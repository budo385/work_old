#include "dbcreate_ff_def_fakt_pos.h"


//--------------------------------------------------
// ODBC - CREATE
//--------------------------------------------------
void DbCreate_FF_DEF_FAKT_POS::DefineFields(QStringList &lstSQL) 
{
	//Table schema statements for ODBC.
	lstSQL.append(" 	CREATE TABLE FF_DEF_FAKT_POS (\
										DFB_SEQUENCE INTEGER  NOT NULL,\
										DFB_FORM VARCHAR(56)  NOT NULL,DFB_SORT DECIMAL(10) NOT NULL,\
										DFB_BAUSTEIN VARCHAR(56)  NOT NULL,\
										constraint FF_DEF_FAKT_POS_PK primary key (DFB_SEQUENCE))	");

}

void DbCreate_FF_DEF_FAKT_POS::DefineIndexes(SQLDBObjectCreate &dboList) 
{
		//add index definition by its name (important).
		dboList["IND_DFB_FORM"] = "CREATE INDEX IND_DFB_FORM ON FF_DEF_FAKT_POS (DFB_FORM)";
		dboList["IND_DFB_SORT"] = "CREATE INDEX IND_DFB_SORT ON FF_DEF_FAKT_POS (DFB_SORT)";
		dboList["IND_DFB_BAUSTEIN"] = "CREATE INDEX IND_DFB_BAUSTEIN ON FF_DEF_FAKT_POS (DFB_BAUSTEIN)";

}

void DbCreate_FF_DEF_FAKT_POS::DefineViews(SQLDBObjectCreate &dboList) 
{


	dboList["VW_FF_DEF_FAKT_POS"] = "CREATE VIEW VW_FF_DEF_FAKT_POS AS SELECT DFB_SEQUENCE,DFB_FORM,DFB_SORT,DFB_BAUSTEIN FROM FF_DEF_FAKT_POS";

	dboList["VWS_FF_DEF_FAKT_POS"] = "CREATE VIEW VWS_FF_DEF_FAKT_POS AS SELECT DFB_SEQUENCE,DFB_FORM,DFB_SORT,DFB_BAUSTEIN FROM FF_DEF_FAKT_POS";
}

void DbCreate_FF_DEF_FAKT_POS::DefineTriggers(SQLDBObjectCreate &dboList) 
{
	if(m_DbType==DBTYPE_FIREBIRD)
	{

		dboList["TR_FF_DEF_FAKT_POS"] = "CREATE TRIGGER TR_FF_DEF_FAKT_POS FOR FF_DEF_FAKT_POS BEFORE INSERT OR UPDATE AS BEGIN \
IF(NEW.DFB_SORT IS NULL) THEN NEW.DFB_SORT=0;\
IF(NEW.DFB_FORM IS NULL) THEN NEW.DFB_FORM='';\
IF(NEW.DFB_BAUSTEIN IS NULL) THEN NEW.DFB_BAUSTEIN='';\
END;";
	}
}
