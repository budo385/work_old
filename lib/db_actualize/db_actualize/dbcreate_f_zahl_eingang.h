#ifndef DBCREATE_F_ZAHL_EINGANG_H
#define DBCREATE_F_ZAHL_EINGANG_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_ZAHL_EINGANG : public DbSqlTableCreation
{
public: 
	DbCreate_F_ZAHL_EINGANG():DbSqlTableCreation(F_ZAHL_EINGANG){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_ZAHL_EINGANG_H
