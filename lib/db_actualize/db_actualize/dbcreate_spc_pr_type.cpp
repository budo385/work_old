#include "dbcreate_spc_pr_type.h"


//--------------------------------------------------
// ODBC - CREATE
//--------------------------------------------------
void DbCreate_SPC_PR_TYPE::DefineFields(QStringList &lstSQL) 
{
	//Table schema statements for ODBC.
	lstSQL.append(" 	CREATE TABLE SPC_PR_TYPE (\
										SEAP_ID INTEGER not null,\
										SEAP_GLOBAL_ID VARCHAR(15) null,\
										SEAP_DAT_LAST_MODIFIED TIMESTAMP not null,\
										SEAP_CODE VARCHAR(50) not null,\
										SEAP_LEVEL INTEGER not null,\
										SEAP_PARENT INTEGER null,\
										SEAP_HASCHILDREN INTEGER not null,\
										SEAP_ICON VARCHAR(50) null,\
										SEAP_MAINPARENT_ID INTEGER null,\
										SEAP_NAME VARCHAR(100) not null,\
										SEAP_STYLE INTEGER null,\
										SEAP_F_PROJEKTART_SEQ INTEGER null,\
										constraint SPC_PR_TYPE_PK primary key (SEAP_ID))");

}

void DbCreate_SPC_PR_TYPE::DefineIndexes(SQLDBObjectCreate &dboList) 
{
		//add index definition by its name (important).
		dboList["IND_SEAP_NAME"] = "CREATE INDEX IND_SEAP_NAME ON SPC_PR_TYPE (SEAP_NAME)";
		dboList["IND_SEAP_CODE"] = "CREATE UNIQUE INDEX IND_SEAP_CODE ON SPC_PR_TYPE (SEAP_CODE)";
		dboList["IND_SEAP_PARENT"] = "create index IND_SEAP_PARENT on SPC_PR_TYPE (SEAP_PARENT)";  
}

void DbCreate_SPC_PR_TYPE::DefineViews(SQLDBObjectCreate &dboList) 
{

}

void DbCreate_SPC_PR_TYPE::DefineTriggers(SQLDBObjectCreate &dboList) 
{

}
