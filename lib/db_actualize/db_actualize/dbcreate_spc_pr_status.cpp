#include "dbcreate_spc_pr_status.h"


//--------------------------------------------------
// ODBC - CREATE
//--------------------------------------------------
void DbCreate_SPC_PR_STATUS::DefineFields(QStringList &lstSQL) 
{
	//Table schema statements for ODBC.
	lstSQL.append(" 	CREATE TABLE SPC_PR_STATUS (\
										SFPS_ID INTEGER not null,\
										SFPS_GLOBAL_ID VARCHAR(15) null,\
										SFPS_DAT_LAST_MODIFIED TIMESTAMP not null,\
										SFPS_CODE VARCHAR(50) not null,\
										SFPS_NAME VARCHAR(100) not null,\
										SFPS_F_PR_STATUS_SEQ INTEGER null,\
										constraint SPC_PR_STATUS_PK primary key (SFPS_ID))");
}

void DbCreate_SPC_PR_STATUS::DefineIndexes(SQLDBObjectCreate &dboList) 
{
		//add index definition by its name (important).
		dboList["IND_SFPS_NAME"] = "CREATE INDEX IND_SFPS_NAME ON SPC_PR_STATUS (SFPS_NAME)";
		dboList["IND_SFPS_CODE"] = "CREATE UNIQUE INDEX IND_SFPS_CODE ON SPC_PR_STATUS (SFPS_CODE)";
}

void DbCreate_SPC_PR_STATUS::DefineViews(SQLDBObjectCreate &dboList) 
{

}

void DbCreate_SPC_PR_STATUS::DefineTriggers(SQLDBObjectCreate &dboList) 
{

}
