#include "dbcreate_bus_custom_date.h"



//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_CUSTOM_DATE::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
											create table BUS_CUSTOM_DATE ( \
											BCD_ID INTEGER not null,\
											BCD_GLOBAL_ID VARCHAR(15) null,\
											BCD_DAT_LAST_MODIFIED TIMESTAMP not null,\
											BCD_CUSTOM_FIELD_ID INTEGER not null,\
											BCD_VALUE DATE not null,\
											BCD_TABLE_ID INTEGER not null,\
											BCD_RECORD_ID INTEGER not null, constraint BUS_CUSTOM_DATE_PK primary key (BCD_ID) ) "); 
											
							lstSQL.append("\
											alter table BUS_CUSTOM_DATE\
											add constraint BUS_CUSTOM_DATE_FK1 foreign key (\
											BCD_CUSTOM_FIELD_ID)\
											references BUS_CUSTOM_FIELDS (\
											BCF_ID)  \
											ON DELETE CASCADE");

}

//probably common to all DB's:
void DbCreate_BUS_CUSTOM_DATE::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
	dboList["IND_BCD_CI_TABLE_REC"] = "create index IND_BCD_CI_TABLE_REC on  BUS_CUSTOM_DATE (BCD_TABLE_ID,BCD_RECORD_ID)"; 
	dboList["IND_BCD_TABLE_VAL"] = "create index IND_BCD_TABLE_VAL on BUS_CUSTOM_DATE (BCD_VALUE)"; 
	dboList["IND_BCD_CI_UNIQUE"] = "create unique index IND_BCD_CI_UNIQUE on BUS_CUSTOM_DATE (BCD_TABLE_ID,BCD_RECORD_ID,BCD_CUSTOM_FIELD_ID)";

}


