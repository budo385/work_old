#ifndef DBCREATE_FF_DEF_FAKT_POS_H
#define DBCREATE_FF_DEF_FAKT_POS_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_FF_DEF_FAKT_POS : public DbSqlTableCreation
{
public: 
	DbCreate_FF_DEF_FAKT_POS():DbSqlTableCreation(FF_DEF_FAKT_POS){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // FF_DEF_FAKT_POS_H
