#ifndef DBCREATE_F_REPORTSETS_H
#define DBCREATE_F_REPORTSETS_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_REPORTSETS : public DbSqlTableCreation
{
public: 
	DbCreate_F_REPORTSETS():DbSqlTableCreation(F_REPORTSETS){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_REPORTSETS_H
