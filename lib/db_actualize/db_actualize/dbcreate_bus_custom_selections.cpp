#include "dbcreate_bus_custom_selections.h"



//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_CUSTOM_SELECTIONS::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
											create table BUS_CUSTOM_SELECTIONS ( \
											BCS_ID INTEGER not null,\
											BCS_GLOBAL_ID VARCHAR(15) null,\
											BCS_DAT_LAST_MODIFIED TIMESTAMP not null,\
											BCS_CUSTOM_FIELD_ID INTEGER not null,\
											BCS_VALUE VARCHAR(200) not null,\
											BCS_RADIO_BUTTON_LABEL VARCHAR(300) null,\
											BCS_IS_DEFAULT SMALLINT not null, constraint BUS_CUSTOM_SELECTIONS_PK primary key (BCS_ID) ) "); 
											
							lstSQL.append("\
											alter table BUS_CUSTOM_SELECTIONS\
											add constraint BUS_CUSTOM_SELECTIONS_FK1 foreign key (\
											BCS_CUSTOM_FIELD_ID)\
											references BUS_CUSTOM_FIELDS (\
											BCF_ID)  \
											ON DELETE CASCADE");

}

//probably common to all DB's:
void DbCreate_BUS_CUSTOM_SELECTIONS::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
}


