#include "dbcreate_bus_push_message.h"


//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_PUSH_MESSAGE::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
											create table BUS_PUSH_MESSAGE (					\
											BPM_ID INTEGER not null,					\
											BPM_GLOBAL_ID VARCHAR(15) null,			\
											BPM_DAT_LAST_MODIFIED TIMESTAMP not null,	\
											BPM_CREATED TIMESTAMP not null,	\
											BPM_MESSAGE_BODY VARCHAR(1024) not null,		\
											BPM_PUSH_TOKEN VARCHAR(2000) not null,		\
											BPM_OS_TYPE INTEGER not null,				\
											BPM_BADGE INTEGER null,						\
											BPM_SOUNDNAME VARCHAR(100) null,			\
											BPM_STATUS INTEGER null,			\
											BPM_ERROR_TEXT VARCHAR(1024) null,			\
											BPM_PERSON_FK_ID INTEGER not null,			\
											BPM_SEND_TRIES INTEGER null,			\
											BPM_CUSTOM_DATA_LIST VARCHAR(1024) null,			\
											BPM_DAT_SENT TIMESTAMP null,	\
											constraint BUS_PUSH_MESSAGE_PK primary key (BPM_ID) )");

	lstSQL.append("\
											alter table BUS_PUSH_MESSAGE\
											add constraint BUS_PUSH_MESSAGE_FK1 foreign key (\
											BPM_PERSON_FK_ID)\
											references BUS_PERSON (\
											BPER_ID)  ON DELETE CASCADE ");
}

//probably common to all DB's:
void DbCreate_BUS_PUSH_MESSAGE::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
	dboList["IND_BPM_STATUS"] = "create index IND_BPM_STATUS on BUS_PUSH_MESSAGE (BPM_STATUS)";
	dboList["IND_BPM_DAT_SENT"] = "create index IND_BPM_DAT_SENT on BUS_PUSH_MESSAGE (BPM_DAT_SENT)";
	dboList["IND_BPM_CREATED"] = "create index IND_BPM_CREATED on BUS_PUSH_MESSAGE (BPM_CREATED)";

}


