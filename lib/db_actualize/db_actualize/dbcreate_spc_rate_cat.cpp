#include "dbcreate_spc_rate_cat.h"


//--------------------------------------------------
// ODBC - CREATE
//--------------------------------------------------
void DbCreate_SPC_RATE_CAT::DefineFields(QStringList &lstSQL) 
{
	//Table schema statements for ODBC.
	lstSQL.append(" 	CREATE TABLE SPC_RATE_CAT (\
										SETK_ID INTEGER not null,\
										SETK_GLOBAL_ID VARCHAR(15) null,\
										SETK_DAT_LAST_MODIFIED TIMESTAMP not null,\
										SETK_CODE VARCHAR(50)  NOT NULL,\
										SETK_NAME VARCHAR(100)  NOT NULL,\
										SETK_F_TARIFKATEG_SEQ INTEGER null,\
										constraint SPC_RATE_CAT_PK primary key (SETK_ID))");


}

void DbCreate_SPC_RATE_CAT::DefineIndexes(SQLDBObjectCreate &dboList) 
{
		//add index definition by its name (important).
		dboList["IND_SETK_CODE"] = "CREATE UNIQUE INDEX IND_SETK_CODE ON SPC_RATE_CAT (SETK_CODE)";
		dboList["IND_SETK_NAME"] = "CREATE INDEX IND_SETK_NAME ON SPC_RATE_CAT (SETK_NAME)";
}

void DbCreate_SPC_RATE_CAT::DefineViews(SQLDBObjectCreate &dboList) 
{
}

void DbCreate_SPC_RATE_CAT::DefineTriggers(SQLDBObjectCreate &dboList) 
{

}
