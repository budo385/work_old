#include "dbcreate_core_test_entity_contact.h"



//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------

void DbCreate_TEST_ENTITY_CONTACT::DefineFields(QStringList &lstSQL)
{

	//Table shema statements for ODBC.
	m_TableDDL[DBTYPE_ODBC]["First"].append("\
	create table TEST_ENTITY_CONTACT (\
	TEC_ID INTEGER not null,\
	TEC_GLOBAL_ID VARCHAR(15) null,\
	TEC_DAT_LAST_MODIFIED TIMESTAMP not null,\
	TEC_NAME VARCHAR(50) not null,\
	TEC_LASTNAME VARCHAR(50) not null,\
	TEC_SEX INTEGER not null, constraint TEST_ENTITY_CONTACT_PK primary key (TEC_ID) )");  

	//no Db versioning:
	m_TableDDL[DBTYPE_ODBC]["Last"]=m_TableDDL[DBTYPE_ODBC]["First"];

	
	//create indexes
	SQLDBObjectCreate dboList;
	DefineIndexes(dboList);
	m_Indexes[DBTYPE_ODBC]=dboList;
	dboList.clear();
	
	//create triggers
	m_Triggers[DBTYPE_ODBC]=dboList;


	//--------------------------------------------------
	// ODBC - UPGRADE DEFS
	//---------------------------------------------------
	
	//Now for each Db_version upgrade, set alternations or init to empty list:
	//Init to empty other lists:
	SQLDBObjectUpdate dboUpgrade;

	m_Fields_Upgrade[DBTYPE_ODBC]=dboUpgrade;
}




/* Probably index defintion is same for all DB's */
void DbCreate_TEST_ENTITY_CONTACT::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index defintion by its name (important)
	dboList.clear();
}





