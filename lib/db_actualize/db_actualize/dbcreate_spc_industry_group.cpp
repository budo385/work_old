#include "dbcreate_spc_industry_group.h"


//--------------------------------------------------
// ODBC - CREATE
//--------------------------------------------------
void DbCreate_SPC_INDUSTRY_GROUP::DefineFields(QStringList &lstSQL) 
{
	//Table schema statements for ODBC.
	lstSQL.append(" 	CREATE TABLE SPC_INDUSTRY_GROUP (\
										SEWG_ID INTEGER not null,\
										SEWG_GLOBAL_ID VARCHAR(15) null,\
										SEWG_DAT_LAST_MODIFIED TIMESTAMP not null,\
										SEWG_CODE VARCHAR(50) not null,\
										SEWG_LEVEL INTEGER not null,\
										SEWG_PARENT INTEGER null,\
										SEWG_HASCHILDREN INTEGER not null,\
										SEWG_ICON VARCHAR(50) null,\
										SEWG_MAINPARENT_ID INTEGER null,\
										SEWG_NAME VARCHAR(100) not null,\
										SEWG_STYLE INTEGER null,\
										SEWG_F_WIRTSCH_GRUPPE_SEQ INTEGER null,\
										constraint SPC_INDUSTRY_GROUP_PK primary key (SEWG_ID))");

}

void DbCreate_SPC_INDUSTRY_GROUP::DefineIndexes(SQLDBObjectCreate &dboList) 
{
		//add index definition by its name (important).
		dboList["IND_SEWG_CODE"] = "CREATE INDEX IND_SEWG_CODE ON SPC_INDUSTRY_GROUP (SEWG_CODE)";
		dboList["IND_SEWG_NAME"] = "CREATE INDEX IND_SEWG_NAME ON SPC_INDUSTRY_GROUP (SEWG_NAME)";
		dboList["IND_SEWG_PARENT"] = "create index IND_SEWG_PARENT on SPC_INDUSTRY_GROUP (SEWG_PARENT)";  
}

void DbCreate_SPC_INDUSTRY_GROUP::DefineViews(SQLDBObjectCreate &dboList) 
{

}

void DbCreate_SPC_INDUSTRY_GROUP::DefineTriggers(SQLDBObjectCreate &dboList) 
{

}
