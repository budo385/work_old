#ifndef DBCREATE_BUS_DM_REVISIONS_H
#define DBCREATE_BUS_DM_REVISIONS_H


#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"




class DbCreate_BUS_DM_REVISIONS : public DbSqlTableCreation
{
public:
	DbCreate_BUS_DM_REVISIONS():DbSqlTableCreation(BUS_DM_REVISIONS){};  //init table
	

private:
	//create generic Table def:
	void DefineFields(QStringList &lstSQL);

	//probably common to all DB's:
	void DefineIndexes(SQLDBObjectCreate &dboList);
};
#endif // DBCREATE_BUS_DM_REVISIONS_H
