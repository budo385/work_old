#ifndef DBCREATE_F_ZAHLUNGSARTEN_H
#define DBCREATE_F_ZAHLUNGSARTEN_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_ZAHLUNGSARTEN : public DbSqlTableCreation
{
public: 
	DbCreate_F_ZAHLUNGSARTEN():DbSqlTableCreation(F_ZAHLUNGSARTEN){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_ZAHLUNGSARTEN_H
