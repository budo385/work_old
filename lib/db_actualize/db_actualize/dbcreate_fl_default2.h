#ifndef DBCREATE_FL_DEFAULT2_H
#define DBCREATE_FL_DEFAULT2_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_FL_DEFAULT2 : public DbSqlTableCreation
{
public: 
	DbCreate_FL_DEFAULT2():DbSqlTableCreation(FL_DEFAULT2){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // FL_DEFAULT2_H
