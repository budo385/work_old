#ifndef DBCREATE_F_KONTENPLAN_H
#define DBCREATE_F_KONTENPLAN_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_KONTENPLAN : public DbSqlTableCreation
{
public: 
	DbCreate_F_KONTENPLAN():DbSqlTableCreation(F_KONTENPLAN){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_KONTENPLAN_H
