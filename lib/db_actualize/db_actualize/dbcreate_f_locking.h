#ifndef DBCREATE_F_LOCKING_H
#define DBCREATE_F_LOCKING_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_LOCKING : public DbSqlTableCreation
{
public: 
	DbCreate_F_LOCKING():DbSqlTableCreation(F_LOCKING){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_LOCKING_H
