#include "dbcreate_bus_dm_revisions.h"




//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_DM_REVISIONS::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
											create table BUS_DM_REVISIONS ( \
											BDMR_ID INTEGER not null,\
											BDMR_GLOBAL_ID VARCHAR(15) null,\
											BDMR_DAT_LAST_MODIFIED TIMESTAMP not null,\
											BDMR_DOCUMENT_ID INTEGER not null,\
											BDMR_DAT_CHECK_IN DATETIME not null,\
											BDMR_NAME VARCHAR(250) not null,\
											BDMR_REVISION_TAG VARCHAR(50) null,\
											BDMR_CONTENT LONGVARBINARY null,\
											BDMR_CHECK_IN_USER_ID INTEGER null,\
											BDMR_IS_COMPRESSED INTEGER null, \
											BDMR_SIZE INTEGER null, constraint BUS_DM_REVISIONS_PK primary key (BDMR_ID) )");  

	lstSQL.append("\
											alter table BUS_DM_REVISIONS\
											add constraint BUS_PERSON_DM_REV_FK1 foreign key (\
											BDMR_CHECK_IN_USER_ID)\
											references BUS_PERSON (\
											BPER_ID) ON DELETE SET NULL");


	lstSQL.append("\
											alter table BUS_DM_REVISIONS\
											add constraint DM_DOC_DM_REVIS_FK1 foreign key (\
											BDMR_DOCUMENT_ID)\
											references BUS_DM_DOCUMENTS (\
											BDMD_ID)  \
											ON DELETE CASCADE");

}



//probably common to all DB's:
void DbCreate_BUS_DM_REVISIONS::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();

	dboList["IND_BDMR_USER_ID"] = "create index IND_BDMR_USER_ID on BUS_DM_REVISIONS (	BDMR_CHECK_IN_USER_ID)  ";
	dboList["IND_BDMR_DOC_ID"] = "create index IND_BDMR_DOC_ID on BUS_DM_REVISIONS (	BDMR_DOCUMENT_ID)  ";
	dboList["IND_BDMR_CHECK_IN"] = "create index IND_BDMR_CHECK_IN on BUS_DM_REVISIONS (	BDMR_DAT_CHECK_IN)  ";
}


