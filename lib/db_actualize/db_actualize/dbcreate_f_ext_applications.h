#ifndef DBCREATE_F_EXT_APPLICATIONS_H
#define DBCREATE_F_EXT_APPLICATIONS_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_EXT_APPLICATIONS : public DbSqlTableCreation
{
public: 
	DbCreate_F_EXT_APPLICATIONS():DbSqlTableCreation(F_EXT_APPLICATIONS){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_EXT_APPLICATIONS_H
