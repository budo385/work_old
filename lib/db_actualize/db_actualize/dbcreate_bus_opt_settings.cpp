#include "dbcreate_bus_opt_settings.h"

//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_OPT_SETTINGS::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
											create table BUS_OPT_SETTINGS (				\
											BOUS_ID INTEGER not null,					\
											BOUS_GLOBAL_ID VARCHAR(15) null,			\
											BOUS_DAT_LAST_MODIFIED TIMESTAMP not null,	\
											BOUS_SETTING_SET_ID INTEGER not null,		\
											BOUS_SETTING_ID INTEGER not null,			\
											BOUS_SETTING_VISIBLE INTEGER not null,		\
											BOUS_SETTING_TYPE INTEGER not null,			\
											BOUS_VALUE INTEGER null,					\
											BOUS_VALUE_STRING VARCHAR(255) null,		\
											BOUS_VALUE_BYTE LONGVARBINARY null,			\
											BOUS_PERSON_ID INTEGER null,				\
											constraint BUS_OPT_SETTINGS_PK primary key (BOUS_ID) ) ");


	lstSQL.append("\
											alter table BUS_OPT_SETTINGS					\
											add constraint BUS_PERS_SETTINGS foreign key (	\
											BOUS_PERSON_ID)									\
											references BUS_PERSON (							\
											BPER_ID)										\
											ON DELETE CASCADE");
}



//probably common to all DB's:
void DbCreate_BUS_OPT_SETTINGS::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
	dboList["IND_BOUS_PERSON_ID"] = "create index IND_BOUS_PERSON_ID on BUS_OPT_SETTINGS (BOUS_PERSON_ID  )  ";  
	dboList["IND_BOUS_PERSON_BOUS_SETTING_ID"] = "create unique index IND_BOUS_PERSON_BOUS_SETTING_ID on BUS_OPT_SETTINGS (BOUS_PERSON_ID,BOUS_SETTING_ID) ";  

}
