#include "dbcreate_f_projectuserdefinedfields.h"


//--------------------------------------------------
// ODBC - CREATE
//--------------------------------------------------
void DbCreate_F_PROJECTUSERDEFINEDFIELDS::DefineFields(QStringList &lstSQL) 
{
	//Table schema statements for ODBC.
	lstSQL.append(" 	CREATE TABLE F_PROJECTUSERDEFINEDFIELDS (\
										EPUL_FIELD_ID DECIMAL(10) NOT NULL,\
										EPUL_FIELD_TYPE VARCHAR(111)  NOT NULL,EPUL_FIELD_NAME VARCHAR(111)  NOT NULL,\
										EPUL_SEQUENCE INTEGER  NOT NULL,EPUL_GLOBAL_ID VARCHAR(14)  NOT NULL,\
										EPUL_DAT_AENDERUNG DATETIME ,EPUL_DAT_EROEFFNUNG DATETIME ,\
										EPUL_MODIFIED_BY VARCHAR(36)  NOT NULL,EPUL_VALID_SINCE DATETIME ,\
										EPUL_EXPIRE_DATE DATETIME ,\
										constraint F_PROJECTUSERDEFINEDFIELDS_PK primary key (EPUL_SEQUENCE))	");

}

void DbCreate_F_PROJECTUSERDEFINEDFIELDS::DefineIndexes(SQLDBObjectCreate &dboList) 
{
		//add index definition by its name (important).
		dboList["IND_EPUL_FIELD_ID"] = "CREATE INDEX IND_EPUL_FIELD_ID ON F_PROJECTUSERDEFINEDFIELDS (EPUL_FIELD_ID)";
		dboList["IND_EPUL_GLOBAL_ID"] = "CREATE INDEX IND_EPUL_GLOBAL_ID ON F_PROJECTUSERDEFINEDFIELDS (EPUL_GLOBAL_ID)";
		dboList["IND_EPUL_DAT_AENDERUNG"] = "CREATE INDEX IND_EPUL_DAT_AENDERUNG ON F_PROJECTUSERDEFINEDFIELDS (EPUL_DAT_AENDERUNG)";
		dboList["IND_EPUL_EXPIRE_DATE"] = "CREATE INDEX IND_EPUL_EXPIRE_DATE ON F_PROJECTUSERDEFINEDFIELDS (EPUL_EXPIRE_DATE)";
		dboList["IND_EPUL_VALID_SINCE"] = "CREATE INDEX IND_EPUL_VALID_SINCE ON F_PROJECTUSERDEFINEDFIELDS (EPUL_VALID_SINCE)";

}

void DbCreate_F_PROJECTUSERDEFINEDFIELDS::DefineViews(SQLDBObjectCreate &dboList) 
{


	dboList["VW_F_PROJECTUSERDEFINEDFIELDS"] = "CREATE VIEW VW_F_PROJECTUSERDEFINEDFIELDS AS SELECT EPUL_FIELD_ID,EPUL_FIELD_TYPE,EPUL_FIELD_NAME,EPUL_SEQUENCE ,EPUL_GLOBAL_ID,EPUL_DAT_AENDERUNG,EPUL_DAT_EROEFFNUNG,EPUL_MODIFIED_BY ,EPUL_VALID_SINCE,EPUL_EXPIRE_DATE FROM F_PROJECTUSERDEFINEDFIELDS";

	dboList["VWS_F_PROJECTUSERDEFINEDFIELDS"] = "CREATE VIEW VWS_F_PROJECTUSERDEFINEDFIELDS AS SELECT EPUL_FIELD_ID,EPUL_FIELD_TYPE,EPUL_FIELD_NAME,EPUL_SEQUENCE ,EPUL_GLOBAL_ID,EPUL_DAT_AENDERUNG,EPUL_DAT_EROEFFNUNG,EPUL_MODIFIED_BY ,EPUL_VALID_SINCE,EPUL_EXPIRE_DATE FROM F_PROJECTUSERDEFINEDFIELDS";
}

void DbCreate_F_PROJECTUSERDEFINEDFIELDS::DefineTriggers(SQLDBObjectCreate &dboList) 
{
	if(m_DbType==DBTYPE_FIREBIRD)
	{

		dboList["TR_F_PROJECTUSERDEFINEDFIELDS"] = "CREATE TRIGGER TR_F_PROJECTUSERDEFINEDFIELDS FOR F_PROJECTUSERDEFINEDFIELDS BEFORE INSERT OR UPDATE AS BEGIN \
IF(NEW.EPUL_FIELD_ID IS NULL) THEN NEW.EPUL_FIELD_ID=0;\
IF(NEW.EPUL_FIELD_TYPE IS NULL) THEN NEW.EPUL_FIELD_TYPE='';\
IF(NEW.EPUL_FIELD_NAME IS NULL) THEN NEW.EPUL_FIELD_NAME='';\
IF(NEW.EPUL_GLOBAL_ID IS NULL) THEN NEW.EPUL_GLOBAL_ID='';\
IF(NEW.EPUL_MODIFIED_BY IS NULL) THEN NEW.EPUL_MODIFIED_BY='';\
END;";
	}
}
