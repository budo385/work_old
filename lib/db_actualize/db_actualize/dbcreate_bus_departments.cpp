#include "dbcreate_bus_departments.h"

//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_DEPARTMENTS::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
										create table BUS_DEPARTMENTS ( \
										BDEPT_ID INTEGER not null, \
										BDEPT_GLOBAL_ID VARCHAR(15) null, \
										BDEPT_DAT_LAST_MODIFIED TIMESTAMP not null, \
										BDEPT_CODE VARCHAR(50) not null, \
										BDEPT_LEVEL INTEGER not null, \
										BDEPT_PARENT INTEGER null, \
										BDEPT_HASCHILDREN INTEGER not null, \
										BDEPT_ICON VARCHAR(50) null, \
										BDEPT_STYLE INTEGER not null, \
										BDEPT_MAINPARENT_ID INTEGER null, \
										BDEPT_NAME VARCHAR(100) not null, \
										BDEPT_ACTIVE_FLAG INTEGER not null, \
										BDEPT_DESCRIPTION LONGVARCHAR null, \
										BDEPT_CONTACT_ID INTEGER null, \
										BDEPT_ORGANIZATION_ID INTEGER null, \
										BDEPT_F_ABTEILUNG_SEQ INTEGER null,\
										BDEPT_COST_CENTER_ID INTEGER null, constraint BUS_DEPARTMENTS_PK primary key (BDEPT_ID) )");


	m_lstDeletedCols<<"BDEPT_SIBLING";

	//constraint for table creation:
	/* TOFIX this causes crash when editing an item in the FUI !!!!!	*/
	lstSQL.append("\
											alter table BUS_DEPARTMENTS\
											add constraint BUS_DEPARTMENTS_PK_FK_PARENT foreign key (\
											BDEPT_PARENT)\
											references BUS_DEPARTMENTS (\
											BDEPT_ID) ON DELETE CASCADE");


	lstSQL.append("\
											alter table BUS_DEPARTMENTS\
											add constraint BUS_DEPARTMENTS_CONT_FK1 foreign key (\
											BDEPT_CONTACT_ID)\
											references BUS_CM_CONTACT (\
											BCNT_ID) \
											");

	lstSQL.append("\
								   alter table BUS_DEPARTMENTS\
								   add constraint BUS_DEPT_ORG_K1 foreign key (\
								   BDEPT_ORGANIZATION_ID)\
								   references BUS_ORGANIZATIONS (BORG_ID)\
											");	

	lstSQL.append("\
								   alter table BUS_DEPARTMENTS\
								   add constraint BUS_DEPT_COST_CENTER_K1 foreign key (\
								   BDEPT_COST_CENTER_ID)\
								   references BUS_COST_CENTERS (BCTC_ID) ON DELETE SET NULL\
											");	

	lstSQL.append("\
								   alter table BUS_DEPARTMENTS\
								   add constraint BUS_DEPT_PK_FK_MAINPARENT foreign key (\
								   BDEPT_MAINPARENT_ID)\
								   references BUS_DEPARTMENTS (\
								   BDEPT_ID)");

}

//probably common to all DB's:
void DbCreate_BUS_DEPARTMENTS::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
	dboList["IND_BDEPT_CONTACT_ID"] = "create index IND_BDEPT_CONTACT_ID on BUS_DEPARTMENTS (BDEPT_CONTACT_ID) ";  
	dboList["IND_BDEPT_ORG_ID"] = "create index IND_BDEPT_ORG_ID on BUS_DEPARTMENTS (BDEPT_ORGANIZATION_ID)";  
	dboList["IND_BDEPT_CODE_ORG"] = "create unique index IND_BDEPT_CODE_ORG on BUS_DEPARTMENTS (BDEPT_CODE,BDEPT_ORGANIZATION_ID) ";  
	dboList["IND_BDEPT_PARENT"] = "create index IND_BDEPT_PARENT on BUS_DEPARTMENTS (BDEPT_PARENT)";  
	dboList["IND_BDEPT_HIER"] = "create index IND_BDEPT_HIER on BUS_DEPARTMENTS (BDEPT_LEVEL,BDEPT_PARENT)";  
}

