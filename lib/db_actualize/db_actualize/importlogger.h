#ifndef IMPORTLOGGER_H
#define IMPORTLOGGER_H

#include <QObject>
#include <QHash>
#include <QHashIterator>

/*
	set Cnt Records

	Log file is as:
	1ln = TotalRecords
	2ln = step (e.g. step 1/10 Importing data for table %1
	3ln = curr_ins/total_ins

	err log is only written when something is in err...
*/

class ImportLogger
{

public:
	ImportLogger();
	~ImportLogger();

	void SetLogPath(QString strLogPath){m_strLogPath=strLogPath;}
	void WriteImportProgress(QString strTable, int nCurrentRec, bool bIsData, bool bEnd=false);
	void WriteStep(QString strStep, int nCurrentTableIdx=0);
	void WriteErrLog(QString strPath,QByteArray byteErr);

	void SetTableCnt(int nTableCnt){m_nTableCnt=nTableCnt;};
	void RecalcTotal();
	void SetDataRecords(QString strTable,int nCnt){m_hshDataCnt[strTable]=nCnt;}
	void SetBinRecords(QString strTable,int nCnt){m_hshBinCnt[strTable]=nCnt;};

private:
	void FlushLog();

	QHash<QString,int> m_hshDataCnt;
	QHash<QString,int> m_hshBinCnt;

	QString m_strLogPath;
	QString m_strCurrStep;
	int		m_nCurrTableRec;
	quint64	m_nCurrTotalRec;
	QString m_strCurrTable;
	bool	m_bCurrImportIsData;
	quint64 m_nTotalRec;
	int		m_nCurrentTableIdx;
	int		m_nTableCnt;
};

#endif // IMPORTLOGGER_H
