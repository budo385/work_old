#include "dbcreate_f_letzter_exp.h"


//--------------------------------------------------
// ODBC - CREATE
//--------------------------------------------------
void DbCreate_F_LETZTER_EXP::DefineFields(QStringList &lstSQL) 
{
	//Table schema statements for ODBC.
	lstSQL.append(" 	CREATE TABLE F_LETZTER_EXP (\
										ELE_LAST_EXP DATETIME ,\
										ELE_SEQUENCE INTEGER  NOT NULL,ELE_TYP DECIMAL(3) NOT NULL,\
										constraint F_LETZTER_EXP_PK primary key (ELE_SEQUENCE))	");

}

void DbCreate_F_LETZTER_EXP::DefineIndexes(SQLDBObjectCreate &dboList) 
{
		//add index definition by its name (important).
		dboList["IND_ELE_LAST_EXP"] = "CREATE INDEX IND_ELE_LAST_EXP ON F_LETZTER_EXP (ELE_LAST_EXP)";

}

void DbCreate_F_LETZTER_EXP::DefineViews(SQLDBObjectCreate &dboList) 
{


	dboList["VW_F_LETZTER_EXP"] = "CREATE VIEW VW_F_LETZTER_EXP AS SELECT ELE_LAST_EXP,ELE_SEQUENCE,ELE_TYP FROM F_LETZTER_EXP";

	dboList["VWS_F_LETZTER_EXP"] = "CREATE VIEW VWS_F_LETZTER_EXP AS SELECT ELE_LAST_EXP,ELE_SEQUENCE,ELE_TYP FROM F_LETZTER_EXP";
}

void DbCreate_F_LETZTER_EXP::DefineTriggers(SQLDBObjectCreate &dboList) 
{
	if(m_DbType==DBTYPE_FIREBIRD)
	{

		dboList["TR_F_LETZTER_EXP"] = "CREATE TRIGGER TR_F_LETZTER_EXP FOR F_LETZTER_EXP BEFORE INSERT OR UPDATE AS BEGIN \
IF(NEW.ELE_TYP IS NULL) THEN NEW.ELE_TYP=0;\
END;";
	}
}
