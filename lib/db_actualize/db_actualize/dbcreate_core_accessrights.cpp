#include "dbcreate_core_accessrights.h"

//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_CORE_ACCESSRIGHTS::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
											create table CORE_ACCESSRIGHTS (			\
											CAR_ID INTEGER not null,					\
											CAR_GLOBAL_ID VARCHAR(15) null,				\
											CAR_DAT_LAST_MODIFIED TIMESTAMP not null,	\
											CAR_ACCRSET_ID INTEGER not null,			\
											CAR_ROLE_ID INTEGER not null,				\
											CAR_NAME VARCHAR(255) null,					\
											CAR_TYPE INTEGER not null,					\
											CAR_VALUE INTEGER not null,					\
											CAR_VALUE_DESCR VARCHAR(255) null,			\
											CAR_CODE INTEGER not null,					\
											constraint CORE_ACCESSRIGHTS_PK primary key (CAR_ID) )");

	//no Db versioning:

	lstSQL.append("alter table CORE_ACCESSRIGHTS					\
							   add constraint CORE_ROLE_CORE_ACCESSRIGHTS_FK1	\
							   foreign key (CAR_ROLE_ID)						\
							   references CORE_ROLE (CROL_ID) ON DELETE CASCADE");

	lstSQL.append("alter table CORE_ACCESSRIGHTS					\
							   add constraint CORE_ACCRSET_ACCR_FK1\
							   foreign key (CAR_ACCRSET_ID)					\
							   references CORE_ACCRSET (CAST_ID) ON DELETE CASCADE");
	
}

//probably common to all DB's:
void DbCreate_CORE_ACCESSRIGHTS::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
	dboList["IND_CAR_ROLE_ID"]	  = "create index IND_CAR_ROLE_ID on CORE_ACCESSRIGHTS (CAR_ROLE_ID)";
	dboList["IND_CAR_ACCRSET_ID"] = "create index IND_CAR_ACCRSET_ID on CORE_ACCESSRIGHTS (CAR_ACCRSET_ID)";
}
 
