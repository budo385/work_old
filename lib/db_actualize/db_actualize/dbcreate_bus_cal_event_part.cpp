#include "dbcreate_bus_cal_event_part.h"


//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_CAL_EVENT_PART::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
								   create table BUS_CAL_EVENT_PART (\
								   BCEP_ID INTEGER not null,\
								   BCEP_GLOBAL_ID VARCHAR(15) null,\
								   BCEP_DAT_LAST_MODIFIED TIMESTAMP not null,\
								   BCEP_EVENT_ID INTEGER not null,\
								   BCEP_COMM_ENTITY_ID INTEGER not null,\
								   BCEP_STATUS INTEGER not null,\
								   BCEP_PRESENCE_TYPE_ID INTEGER null,\
								   BCEP_TYPE_ID INTEGER null,\
								   BCEP_DESCRIPTION LONGVARCHAR null,\
								   BCEP_ASSIGN_NEW_CRP SMALLINT null,\
								   constraint BUS_CAL_EVENT_PART_PK primary key (BCEP_ID) )   ");  


	lstSQL.append("\
								   alter table BUS_CAL_EVENT_PART\
								   add constraint BUS_TYPES_CAL_EV_PART_FK1 foreign key (\
								   BCEP_PRESENCE_TYPE_ID)\
								   references BUS_CM_TYPES (\
								   BCMT_ID)	");	


	lstSQL.append("\
								   alter table BUS_CAL_EVENT_PART\
								   add constraint BUS_CAL_EV_CAL_EV_PART_FK1 foreign key (\
								   BCEP_EVENT_ID)\
								   references BUS_CAL_EVENT (\
								   BCEV_ID)	ON DELETE CASCADE "); //if event is deleted->all parts also

	lstSQL.append("\
								   alter table BUS_CAL_EVENT_PART\
								   add constraint CE_COMM_CAL_EV_PART_FK1 foreign key (\
								   BCEP_COMM_ENTITY_ID)\
								   references CE_COMM_ENTITY (\
								   CENT_ID)	ON DELETE CASCADE "); //if ce_entity is deleted->all parts are also deleted

	lstSQL.append("\
								   alter table BUS_CAL_EVENT_PART\
								   add constraint BUS_TYPES_CAL_EV_PART_FK2 foreign key (\
								   BCEP_TYPE_ID)\
								   references BUS_CM_TYPES (\
								   BCMT_ID) 	");



	//no Db versioning:
	
	//create indexes
	SQLDBObjectCreate dboList;
	DefineIndexes(dboList);
	m_Indexes[DBTYPE_ODBC]=dboList;
	dboList.clear();

	dboList["TRG_BUS_CALEV_PART_DELETE"]="\
									  CREATE TRIGGER TRG_BUS_CALEV_PART_DELETE AFTER DELETE ON BUS_CAL_EVENT_PART\
									  FOR EACH ROW\
										 BEGIN \
										 DELETE FROM CE_COMM_ENTITY WHERE OLD.BCEP_COMM_ENTITY_ID = CENT_ID;\
										 END\
										 ";

	//create triggers
	m_Triggers[DBTYPE_ODBC]=dboList;

 	
}



//probably common to all DB's:
void DbCreate_BUS_CAL_EVENT_PART::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();

}


void DbCreate_BUS_CAL_EVENT_PART::DefineTriggers(SQLDBObjectCreate &dboList)
{
	//create triggers TOFIX: foe each DB, must be different
	dboList["TRG_BUS_CALEV_PART_DELETE"]="\
									  CREATE TRIGGER TRG_BUS_CALEV_PART_DELETE FOR BUS_CAL_EVENT_PART\
									  ACTIVE AFTER DELETE AS \
									  BEGIN \
									  DELETE FROM CE_COMM_ENTITY WHERE OLD.BCEP_COMM_ENTITY_ID = CENT_ID;\
									  END\
									  ";
}


