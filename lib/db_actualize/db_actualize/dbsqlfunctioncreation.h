#ifndef DBSQLFUNCTIONCREATION_H
#define DBSQLFUNCTIONCREATION_H



#include <QString>
#include <QHash>
#include "common/common/status.h"
#include "db/db/dbsqlquery.h"
#include "db/db/dbsqlmanager.h"
#include "db/db/dbomanager.h"





typedef QHash<QString, QString> SQLDBObjectCreate;					///< object creation statement pair: object_name , object SQL defintion
typedef QHash<QString, SQLDBObjectCreate> SQLDBObjectCreate_DbType; ///< list of object creation SQL's indexed by Database Type
typedef QHashIterator<QString, QString> SQLDBObjectCreateIterator;	///< iterator

/*!
	\class DbSqlFunctionCreation
	\brief HardCoded SQL function creation statements for each DB, no version tracking
	\ingroup Db_Actualize

	When invoked, all previous functions are dropped, new created, only to the latest DB version

	Use:
	- Add own function here in Initialize() , for each DB version
*/


class DbSqlFunctionCreation
{
public:

	void DropAllFunctions(Status &pStatus, bool bCommunicatorFunct=true);
	void CreateFunctions(Status &pStatus, bool bCommunicatorFunct=true);
	void InitDbSettings(DbSqlManager *pManager, QSqlDatabase *pDb,DbObjectManager *pDboManager);
	QStringList	GetFunctions();

protected:
	//INIT WITH CREATE COMMANDS for each DB:
	//for each DB....
	void Init_Oracle();
	void Init_MySQL();
	void Init_FireBird();

	//not implemented:
	void Init_MS_SQL(){};
	void Init_PostgreSQL(){};
	void Init_DB2(){};

	DbSqlManager	*m_pManager;
	QSqlDatabase	*m_pDb;
	DbObjectManager *m_DbObjectManager;

	//always executed with drop or replace if exists (no alter)
	SQLDBObjectCreate_DbType m_Functions;			///< Dbo Objects for Communicator database
	SQLDBObjectCreate_DbType m_FunctionsSPC;		///< Dbo Objects for SPC database

	//stored data:
	QString m_DbType;
};


#endif //DBSQLFUNCTIONCREATION_H

