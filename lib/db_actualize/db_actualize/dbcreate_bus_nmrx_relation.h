#ifndef DBCREATE_BUS_NMRX_RELATION_H
#define DBCREATE_BUS_NMRX_RELATION_H


#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_BUS_NMRX_RELATION : public DbSqlTableCreation
{
public:
	DbCreate_BUS_NMRX_RELATION():DbSqlTableCreation(BUS_NMRX_RELATION){};  //init table
	

private:
	//create generic Table def:
	void DefineFields(QStringList &lstSQL);
	//probably common to all DB's:
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineTriggers(SQLDBObjectCreate &dboList);
};

#endif // DBCREATE_BUS_NMRX_RELATION_H
