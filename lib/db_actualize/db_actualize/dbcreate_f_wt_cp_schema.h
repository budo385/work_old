#ifndef DBCREATE_F_WT_CP_SCHEMA_H
#define DBCREATE_F_WT_CP_SCHEMA_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_WT_CP_SCHEMA : public DbSqlTableCreation
{
public: 
	DbCreate_F_WT_CP_SCHEMA():DbSqlTableCreation(F_WT_CP_SCHEMA){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_WT_CP_SCHEMA_H
