#ifndef DBCREATE_F_PERIODENART_H
#define DBCREATE_F_PERIODENART_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_PERIODENART : public DbSqlTableCreation
{
public: 
	DbCreate_F_PERIODENART():DbSqlTableCreation(F_PERIODENART){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_PERIODENART_H
