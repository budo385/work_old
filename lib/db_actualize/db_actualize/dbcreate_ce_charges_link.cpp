#include "dbcreate_ce_charges_link.h"



//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_CE_CHARGES_LINK::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
											create table CE_CHARGES_LINK ( \
											CECH_ID INTEGER not null,\
											CECH_GLOBAL_ID VARCHAR(15) null,\
											CECH_DAT_LAST_MODIFIED TIMESTAMP null,\
											CECH_CHARGE_ID INTEGER not null,\
											CECH_COMM_ENTITY_ID INTEGER not null, constraint CE_CHARGES_LINK_PK primary key (CECH_ID) ) ");


	//constraint for table creation:
	lstSQL.append("\
											alter table CE_CHARGES_LINK\
											add constraint CE_ENTITY_CHRG_LNK_FK1 foreign key (\
											CECH_COMM_ENTITY_ID)\
											references CE_COMM_ENTITY (\
											CENT_ID)\
											ON DELETE CASCADE");

}



//probably common to all DB's:
void DbCreate_CE_CHARGES_LINK::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
	dboList["IND_CECH_ENTITY_ID"] = "create index IND_CECH_ENTITY_ID on CE_CHARGES_LINK (CECH_COMM_ENTITY_ID)";  
}


