#ifndef DBCREATE_FD_DOK_VORLAGEN_H
#define DBCREATE_FD_DOK_VORLAGEN_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_FD_DOK_VORLAGEN : public DbSqlTableCreation
{
public: 
	DbCreate_FD_DOK_VORLAGEN():DbSqlTableCreation(FD_DOK_VORLAGEN){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // FD_DOK_VORLAGEN_H
