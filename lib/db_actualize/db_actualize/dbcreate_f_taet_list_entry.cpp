#include "dbcreate_f_taet_list_entry.h"


//--------------------------------------------------
// ODBC - CREATE
//--------------------------------------------------
void DbCreate_F_TAET_LIST_ENTRY::DefineFields(QStringList &lstSQL) 
{
	//Table schema statements for ODBC.
	lstSQL.append(" 	CREATE TABLE F_TAET_LIST_ENTRY (\
										TE_SEQUENCE INTEGER  NOT NULL,\
										TE_TN_CODE VARCHAR(34)  NOT NULL,TE_ET_CODE VARCHAR(23)  NOT NULL,\
										TE_VALID_FROM DATETIME ,TE_VALID_TO DATETIME ,\
										TE_VIS_INS VARCHAR(14)  NOT NULL,TE_VIS_UPD VARCHAR(14)  NOT NULL,\
										TE_INS_DAT DATETIME ,TE_UPD_DAT DATETIME ,\
										TE_GLOBAL_ID VARCHAR(34)  NOT NULL,TE_CHANGE_FL DECIMAL(3) NOT NULL,\
										TE_CI_TN_ET_CODE VARCHAR(2)  NOT NULL,TE_DAT_AENDERUNG DATETIME ,\
										TE_DAT_EROEFFNUNG DATETIME ,TE_MODIFIED_BY VARCHAR(36)  NOT NULL,\
										TE_VALID_SINCE DATETIME ,TE_EXPIRE_DATE DATETIME ,\
										constraint F_TAET_LIST_ENTRY_PK primary key (TE_SEQUENCE))	");

}

void DbCreate_F_TAET_LIST_ENTRY::DefineIndexes(SQLDBObjectCreate &dboList) 
{
		//add index definition by its name (important).
		dboList["IND_TE_TN_CODE"] = "CREATE INDEX IND_TE_TN_CODE ON F_TAET_LIST_ENTRY (TE_TN_CODE)";
		dboList["IND_TE_ET_CODE"] = "CREATE INDEX IND_TE_ET_CODE ON F_TAET_LIST_ENTRY (TE_ET_CODE)";
		dboList["IND_TE_CI_TN_ET_CODE"] = "CREATE INDEX IND_TE_CI_TN_ET_CODE ON F_TAET_LIST_ENTRY (TE_TN_CODE,TE_ET_CODE)";
		dboList["IND_TE_GLOBAL_ID"] = "CREATE INDEX IND_TE_GLOBAL_ID ON F_TAET_LIST_ENTRY (TE_GLOBAL_ID)";
		dboList["IND_TE_DAT_AENDERUNG"] = "CREATE INDEX IND_TE_DAT_AENDERUNG ON F_TAET_LIST_ENTRY (TE_DAT_AENDERUNG)";
		dboList["IND_TE_EXPIRE_DATE"] = "CREATE INDEX IND_TE_EXPIRE_DATE ON F_TAET_LIST_ENTRY (TE_EXPIRE_DATE)";
		dboList["IND_TE_VALID_SINCE"] = "CREATE INDEX IND_TE_VALID_SINCE ON F_TAET_LIST_ENTRY (TE_VALID_SINCE)";

}

void DbCreate_F_TAET_LIST_ENTRY::DefineViews(SQLDBObjectCreate &dboList) 
{


	dboList["VW_F_TAET_LIST_ENTRY"] = "CREATE VIEW VW_F_TAET_LIST_ENTRY AS SELECT TE_SEQUENCE,TE_TN_CODE,TE_ET_CODE,TE_VALID_FROM ,TE_VALID_TO,TE_VIS_INS,TE_VIS_UPD,TE_INS_DAT \
							 ,TE_UPD_DAT,TE_GLOBAL_ID,TE_CHANGE_FL,TE_CI_TN_ET_CODE ,TE_DAT_AENDERUNG,TE_DAT_EROEFFNUNG,TE_MODIFIED_BY,TE_VALID_SINCE ,TE_EXPIRE_DATE FROM F_TAET_LIST_ENTRY";

	dboList["VWS_F_TAET_LIST_ENTRY"] = "CREATE VIEW VWS_F_TAET_LIST_ENTRY AS SELECT TE_SEQUENCE,TE_TN_CODE,TE_ET_CODE,TE_VALID_FROM ,TE_VALID_TO,TE_VIS_INS,TE_VIS_UPD,TE_INS_DAT \
							 ,TE_UPD_DAT,TE_GLOBAL_ID,TE_CHANGE_FL,TE_CI_TN_ET_CODE ,TE_DAT_AENDERUNG,TE_DAT_EROEFFNUNG,TE_MODIFIED_BY,TE_VALID_SINCE ,TE_EXPIRE_DATE FROM F_TAET_LIST_ENTRY";
}

void DbCreate_F_TAET_LIST_ENTRY::DefineTriggers(SQLDBObjectCreate &dboList) 
{
	if(m_DbType==DBTYPE_FIREBIRD)
	{

		dboList["TR_F_TAET_LIST_ENTRY"] = "CREATE TRIGGER TR_F_TAET_LIST_ENTRY FOR F_TAET_LIST_ENTRY BEFORE INSERT OR UPDATE AS BEGIN \
IF(NEW.TE_CHANGE_FL IS NULL) THEN NEW.TE_CHANGE_FL=0;\
IF(NEW.TE_TN_CODE IS NULL) THEN NEW.TE_TN_CODE='';\
IF(NEW.TE_ET_CODE IS NULL) THEN NEW.TE_ET_CODE='';\
IF(NEW.TE_VIS_INS IS NULL) THEN NEW.TE_VIS_INS='';\
IF(NEW.TE_VIS_UPD IS NULL) THEN NEW.TE_VIS_UPD='';\
IF(NEW.TE_GLOBAL_ID IS NULL) THEN NEW.TE_GLOBAL_ID='';\
IF(NEW.TE_CI_TN_ET_CODE IS NULL) THEN NEW.TE_CI_TN_ET_CODE='';\
IF(NEW.TE_MODIFIED_BY IS NULL) THEN NEW.TE_MODIFIED_BY='';\
END;";
	}
}
