#ifndef DBCREATE_F_BANKVERBINDUNGEN_H
#define DBCREATE_F_BANKVERBINDUNGEN_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_BANKVERBINDUNGEN : public DbSqlTableCreation
{
public: 
	DbCreate_F_BANKVERBINDUNGEN():DbSqlTableCreation(F_BANKVERBINDUNGEN){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_BANKVERBINDUNGEN_H
