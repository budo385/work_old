#include "dbcreate_spc_service_type.h"


//--------------------------------------------------
// ODBC - CREATE
//--------------------------------------------------
void DbCreate_SPC_SERVICE_TYPE::DefineFields(QStringList &lstSQL) 
{
	//Table schema statements for ODBC.
	lstSQL.append(" 	CREATE TABLE SPC_SERVICE_TYPE (\
										SELA_ID INTEGER not null,\
										SELA_GLOBAL_ID VARCHAR(15) null,\
										SELA_DAT_LAST_MODIFIED TIMESTAMP not null,\
										SELA_CODE VARCHAR(50) not null,\
										SELA_LEVEL INTEGER not null,\
										SELA_PARENT INTEGER null,\
										SELA_HASCHILDREN INTEGER not null,\
										SELA_ICON VARCHAR(50) null,\
										SELA_MAINPARENT_ID INTEGER null,\
										SELA_NAME VARCHAR(100) not null,\
										SELA_STYLE INTEGER null,\
										SELA_F_LEISTUNGSARTEN_SEQ INTEGER null,\
										constraint SPC_SERVICE_TYPE_PK primary key (SELA_ID))");


}

void DbCreate_SPC_SERVICE_TYPE::DefineIndexes(SQLDBObjectCreate &dboList) 
{
		//add index definition by its name (important).
		dboList["IND_SELA_CODE"] = "CREATE INDEX IND_SELA_CODE ON SPC_SERVICE_TYPE (SELA_CODE)";
		dboList["IND_SELA_NAME"] = "CREATE INDEX IND_SELA_NAME ON SPC_SERVICE_TYPE (SELA_NAME)";
		dboList["IND_SELA_PARENT"] = "create index IND_SELA_PARENT on SPC_SERVICE_TYPE (SELA_PARENT)";  
}

void DbCreate_SPC_SERVICE_TYPE::DefineViews(SQLDBObjectCreate &dboList) 
{
}

void DbCreate_SPC_SERVICE_TYPE::DefineTriggers(SQLDBObjectCreate &dboList) 
{
}
