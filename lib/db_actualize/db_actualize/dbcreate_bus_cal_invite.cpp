#include "dbcreate_bus_cal_invite.h"


//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_CAL_INVITE::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
								   create table BUS_CAL_INVITE (\
								   BCIV_ID INTEGER not null,\
								   BCIV_GLOBAL_ID VARCHAR(15) null,\
								   BCIV_DAT_LAST_MODIFIED TIMESTAMP not null,\
								   BCIV_INVITE SMALLINT not null,\
								   BCIV_IS_SENT SMALLINT not null,\
								   BCIV_STATUS INTEGER not null,\
								   BCIV_BY_EMAIL SMALLINT not null,\
								   BCIV_BY_SMS SMALLINT not null,\
								   BCIV_BY_NOTIFICATION SMALLINT not null,\
								   BCIV_EMAIL_ADDRESS VARCHAR(250) null,\
								   BCIV_SMS_NUMBERS VARCHAR(250) null,\
								   BCIV_EMAIL_TEMPLATE_ID INTEGER null,\
								   BCIV_ANSWER_TEXT LONGVARCHAR null,\
								   BCIV_OUID VARCHAR(250) null,\
								   BCIV_SUBJECT_ID VARCHAR(250) null, constraint BUS_CAL_INVITE_PK primary key (BCIV_ID) )  ");  


	lstSQL.append("\
								   alter table BUS_CAL_INVITE\
								   add constraint BUS_CAL_IV_EMAIL_FK1 foreign key (\
								   BCIV_EMAIL_TEMPLATE_ID)\
								   references BUS_EMAIL (\
								   BEM_ID) ON DELETE SET NULL");
}



//probably common to all DB's:
void DbCreate_BUS_CAL_INVITE::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
}



