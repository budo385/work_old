#ifndef DBCREATE_F_PR_CACHE_H
#define DBCREATE_F_PR_CACHE_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_PR_CACHE : public DbSqlTableCreation
{
public: 
	DbCreate_F_PR_CACHE():DbSqlTableCreation(F_PR_CACHE){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_PR_CACHE_H
