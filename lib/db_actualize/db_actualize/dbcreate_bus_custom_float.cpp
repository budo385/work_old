#include "dbcreate_bus_custom_float.h"



//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_CUSTOM_FLOAT::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
											create table BUS_CUSTOM_FLOAT ( \
											BCFL_ID INTEGER not null,\
											BCFL_GLOBAL_ID VARCHAR(15) null,\
											BCFL_DAT_LAST_MODIFIED TIMESTAMP not null,\
											BCFL_CUSTOM_FIELD_ID INTEGER not null,\
											BCFL_VALUE VARCHAR(100) not null,\
											BCFL_TABLE_ID INTEGER not null,\
											BCFL_RECORD_ID INTEGER not null, constraint BUS_CUSTOM_FLOAT_PK primary key (BCFL_ID) ) "); 
											
							lstSQL.append("\
											alter table BUS_CUSTOM_FLOAT\
											add constraint BUS_CUSTOM_FLOAT_FK1 foreign key (\
											BCFL_CUSTOM_FIELD_ID)\
											references BUS_CUSTOM_FIELDS (\
											BCF_ID)  \
											ON DELETE CASCADE");

}

//probably common to all DB's:
void DbCreate_BUS_CUSTOM_FLOAT::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
	dboList["IND_BCFL_CI_TABLE_REC"] = "create index IND_BCFL_CI_TABLE_REC on BUS_CUSTOM_FLOAT (BCFL_TABLE_ID,BCFL_RECORD_ID)"; 
	dboList["IND_BCFL_TABLE_VAL"] = "create index IND_BCFL_TABLE_VAL on BUS_CUSTOM_FLOAT (BCFL_VALUE)"; 
	dboList["IND_BCFL_CI_UNIQUE"] = "create unique index IND_BCFL_CI_UNIQUE on BUS_CUSTOM_FLOAT (BCFL_TABLE_ID,BCFL_RECORD_ID,BCFL_CUSTOM_FIELD_ID)"; 

}


