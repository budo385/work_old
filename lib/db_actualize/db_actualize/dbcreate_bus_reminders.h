#ifndef DBCREATE_BUS_REMINDERS_H
#define DBCREATE_BUS_REMINDERS_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_BUS_REMINDERS : public DbSqlTableCreation
{
public:
	DbCreate_BUS_REMINDERS():DbSqlTableCreation(BUS_REMINDERS){};  //init table
	
private:
	//create generic Table def:
	void DefineFields(QStringList &lstSQL);

	//probably common to all DB's:
	void DefineIndexes(SQLDBObjectCreate &dboList);
};


#endif // DBCREATE_BUS_REMINDERS_H
