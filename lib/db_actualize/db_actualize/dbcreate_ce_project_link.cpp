#include "dbcreate_ce_project_link.h"


//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_CE_PROJECT_LINK::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
											create table CE_PROJECT_LINK ( \
											CELP_ID INTEGER not null,\
											CELP_GLOBAL_ID VARCHAR(15) null,\
											CELP_DAT_LAST_MODIFIED TIMESTAMP null,\
											CELP_PROJECT_ID INTEGER not null,\
											CELP_COMM_ENTITY_ID INTEGER not null, \
											CELP_LINK_ROLE_ID INTEGER null, \
											constraint CE_PROJECT_LINK_PK primary key (CELP_ID) )");

	/*
	//constraint for table creation:
	lstSQL.append("\
											alter table CE_PROJECT_LINK\
											add constraint CE_ENTITY_PROJ_FK1 foreign key (\
											CELP_COMM_ENTITY_ID)\
											references CE_COMM_ENTITY (\
											CENT_ID) \
											ON DELETE CASCADE");


	lstSQL.append("\
											alter table CE_PROJECT_LINK\
											add constraint PROJ_PROJ_LNK_FK1 foreign key (\
											CELP_PROJECT_ID)\
											references BUS_PROJECTS (\
											BUSP_ID) \
											ON DELETE CASCADE");

											*/
	
	
}



//probably common to all DB's:
void DbCreate_CE_PROJECT_LINK::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
	dboList["CE_PROJECT_LINK_UC1"] = "create unique index CE_PROJECT_LINK_UC1 on CE_PROJECT_LINK (CELP_COMM_ENTITY_ID,CELP_PROJECT_ID)";  
	dboList["IND_CELP_PROJ_ID"] = "create index IND_CELP_PROJ_ID on CE_PROJECT_LINK (CELP_PROJECT_ID) ";  
	dboList["IND_CELP_ENTITY_ID"] = "create index IND_CELP_ENTITY_ID on CE_PROJECT_LINK (CELP_COMM_ENTITY_ID)";

	
}


