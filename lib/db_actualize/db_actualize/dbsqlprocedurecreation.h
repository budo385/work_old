#ifndef DBSQLPROCEDURECREATION_H
#define DBSQLPROCEDURECREATION_H



#include <QString>
#include <QHash>
#include <QMap>
#include "common/common/status.h"
#include "db/db/dbsqlquery.h"
#include "db/db/dbsqlmanager.h"
#include "db/db/dbomanager.h"





typedef QMap<QString, QString> SQLDBObjectCreateP;					///< object creation statement pair: object_name , object SQL defintion
typedef QMap<QString, SQLDBObjectCreateP> SQLDBObjectCreateP_DbType; ///< list of object creation SQL's indexed by Database Type
typedef QMapIterator<QString, QString> SQLDBObjectCreatePIterator;	///< iterator

/*!
	\class DbSqlProcedureCreation
	\brief HardCoded SQL Procedure creation statements for each DB, no version tracking
	\ingroup Db_Actualize

	When invoked, all previous Procedures are dropped, new created, only to the latest DB version

	Use:
	- Add own Procedure here in Initialize() , for each DB version
*/


class DbSqlProcedureCreation
{
public:

	void DropAllProcedures(Status &pStatus, bool bCommunicatorProc=true);
	void CreateProcedures(Status &pStatus, bool bCommunicatorProc=true);
	void InitDbSettings(DbSqlManager *pManager, QSqlDatabase *pDb,DbObjectManager *pDboManager);
	
	QStringList	GetProcedures();
	QStringList	GetGlobalTables();

protected:

	void Init_GlobalTempTables();
	
	//INIT WITH CREATE COMMANDS for each DB:
	//for each DB....
	void Init_Oracle();
	void Init_MySQL();
	void Init_FireBird();

	//not implemented:
	void Init_MS_SQL(){};
	void Init_PostgreSQL(){};
	void Init_DB2(){};

	
	DbSqlManager	*m_pManager;
	QSqlDatabase	*m_pDb;
	DbObjectManager *m_DbObjectManager;

	//always executed with drop or replace if exists (no alter)
	SQLDBObjectCreateP_DbType m_Procedures;			///< Dbo Objects for Communicator database
	SQLDBObjectCreateP_DbType m_ProceduresSPC;		///< Dbo Objects for SPC database

	SQLDBObjectCreateP_DbType m_GlobalTempTables;			///< Dbo Objects for Communicator database
	SQLDBObjectCreateP_DbType m_GlobalTempTablesSPC;		///< Dbo Objects for SPC database

	SQLDBObjectCreateP_DbType m_GlobalTempTableTriggers;			///< Dbo Objects for Communicator database
	SQLDBObjectCreateP_DbType m_GlobalTempTablesSPCTriggers;		///< Dbo Objects for SPC database


	//stored data:
	QString m_DbType;
};

#endif //DBSQLPROCEDURECREATION_H

