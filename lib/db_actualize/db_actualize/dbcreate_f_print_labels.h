#ifndef DBCREATE_F_PRINT_LABELS_H
#define DBCREATE_F_PRINT_LABELS_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_PRINT_LABELS : public DbSqlTableCreation
{
public: 
	DbCreate_F_PRINT_LABELS():DbSqlTableCreation(F_PRINT_LABELS){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_PRINT_LABELS_H
