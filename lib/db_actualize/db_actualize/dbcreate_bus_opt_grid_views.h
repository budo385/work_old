#ifndef DBCREATE_BUS_OPT_GRID_VIEWS_H
#define DBCREATE_BUS_OPT_GRID_VIEWS_H


#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_BUS_OPT_GRID_VIEWS : public DbSqlTableCreation
{
public:
	DbCreate_BUS_OPT_GRID_VIEWS():DbSqlTableCreation(BUS_OPT_GRID_VIEWS){};  //init table
	

private:
	//create generic Table def:
	void DefineFields(QStringList &lstSQL);

	//probably common to all DB's:
	void DefineIndexes(SQLDBObjectCreate &dboList);
};




#endif // DBCREATE_BUS_OPT_GRID_VIEWS_H
