#include "dbcreate_bus_email_binary.h"

//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_EMAIL_BINARY::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
											create table BUS_EMAIL_BINARY ( \
											BEB_ID INTEGER not null,\
											BEB_GLOBAL_ID VARCHAR(15) null, \
											BEB_DAT_LAST_MODIFIED TIMESTAMP not null, \
											BEB_EMAIL_ID INTEGER not null, \
											BEB_BODY LONGVARCHAR null, \
											constraint BUS_EMAIL_BINARY_PK primary key (BEB_ID))"); 
											

	lstSQL.append("\
											alter table BUS_EMAIL_BINARY\
											add constraint EMAIL_BINARY_FK1 foreign key (\
											BEB_EMAIL_ID)\
											references BUS_EMAIL (BEM_ID) \
											ON DELETE CASCADE");	//when email is deleted, this is also deleted

}



//probably common to all DB's:
void DbCreate_BUS_EMAIL_BINARY::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
	//dboList["IDX_BEB_EMAIL_ID"] = "create index IDX_BEB_EMAIL_ID on BUS_EMAIL_BINARY (BEB_EMAIL_ID)";  
}


