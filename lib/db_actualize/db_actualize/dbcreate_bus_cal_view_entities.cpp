#include "dbcreate_bus_cal_view_entities.h"

//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_CAL_VIEW_ENTITIES::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
								   create table BUS_CAL_VIEW_ENTITIES (			\
								   BCVE_ID INTEGER not null,					\
								   BCVE_GLOBAL_ID VARCHAR(15) null,				\
								   BCVE_DAT_LAST_MODIFIED TIMESTAMP not null,	\
								   BCVE_CAL_VIEW_ID INTEGER null,				\
								   BCVE_USER_ID INTEGER null,					\
								   BCVE_PROJECT_ID INTEGER null,				\
								   BCVE_CONTACT_ID INTEGER null,				\
								   BCVE_RESOURCE_ID INTEGER null,				\
								   BCVE_IS_SELECTED SMALLINT not null,			\
								   BCVE_SORT_CODE SMALLINT not null,			\
								   constraint BUS_CAL_VIEW_ENTITIES_PK primary key (BCVE_ID))");

	lstSQL.append("\
								   alter table BUS_CAL_VIEW_ENTITIES\
								   add constraint BUS_CAL_VIEW_FK1 foreign key (\
								   BCVE_CAL_VIEW_ID)\
								   references BUS_CAL_VIEW (\
								   BCALV_ID) ON DELETE CASCADE");	

	lstSQL.append("\
								   alter table BUS_CAL_VIEW_ENTITIES\
								   add constraint CALVIEW_ENTITY_PERSON_FK1 foreign key (\
								   BCVE_USER_ID)\
								   references  BUS_PERSON (\
								   BPER_ID) ON DELETE CASCADE");	

	lstSQL.append("\
								   alter table BUS_CAL_VIEW_ENTITIES\
								   add constraint CALVIEW_ENTITY_PROJECTS_FK1 foreign key (\
								   BCVE_PROJECT_ID)\
								   references BUS_PROJECTS (\
								   BUSP_ID) ON DELETE CASCADE");	

	lstSQL.append("\
								   alter table BUS_CAL_VIEW_ENTITIES\
								   add constraint CALVIEW_ENTITY_CONTACT_FK1 foreign key (\
								   BCVE_CONTACT_ID)\
								   references BUS_CM_CONTACT (\
								   BCNT_ID) ON DELETE CASCADE");	

	lstSQL.append("\
								   alter table BUS_CAL_VIEW_ENTITIES\
								   add constraint CALVIEW_ENTITY_RESOURCE_FK1 foreign key (\
								   BCVE_RESOURCE_ID)\
								   references BUS_CAL_RESOURCE (\
								   BRES_ID) ON DELETE CASCADE");	

}

void DbCreate_BUS_CAL_VIEW_ENTITIES::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList["IND_BCVE_CAL_VIEW_ID"] = "create index IND_BCVE_CAL_VIEW_ID on BUS_CAL_VIEW_ENTITIES (BCVE_CAL_VIEW_ID)";  

}
