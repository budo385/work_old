#include "dbcreate_bus_group_tree.h"


//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_GROUP_TREE::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
											create table BUS_GROUP_TREE ( \
											BGTR_ID INTEGER not null,\
											BGTR_GLOBAL_ID VARCHAR(15) null,\
											BGTR_DAT_LAST_MODIFIED TIMESTAMP not null,\
											BGTR_NAME VARCHAR(100) null,\
											BGTR_ENTITY_TYPE_ID INTEGER null, constraint BUS_GROUP_TREE_PK primary key (BGTR_ID) )");  
}



//probably common to all DB's:
void DbCreate_BUS_GROUP_TREE::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
	dboList["IND_BGTR_ENTITY_ID"] = "	create index IND_BGTR_ENTITY_ID on BUS_GROUP_TREE (	BGTR_ENTITY_TYPE_ID  )  ";  
	dboList["IND_BGTR_NAME_ENTITY_ID"] = "	create unique index IND_BGTR_NAME_ENTITY_ID on BUS_GROUP_TREE (BGTR_ENTITY_TYPE_ID  ,	BGTR_NAME  )  ";  

		

}



