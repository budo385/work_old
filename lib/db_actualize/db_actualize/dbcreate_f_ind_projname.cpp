#include "dbcreate_f_ind_projname.h"


//--------------------------------------------------
// ODBC - CREATE
//--------------------------------------------------
void DbCreate_F_IND_PROJNAME::DefineFields(QStringList &lstSQL) 
{
	//Table schema statements for ODBC.
	lstSQL.append(" 	CREATE TABLE F_IND_PROJNAME (\
										EPNI_SEQUENCE INTEGER  NOT NULL,\
										EPNI_PROJ VARCHAR(56)  NOT NULL,EPNI_NAME VARCHAR(84)  NOT NULL,\
										constraint F_IND_PROJNAME_PK primary key (EPNI_SEQUENCE))	");

}

void DbCreate_F_IND_PROJNAME::DefineIndexes(SQLDBObjectCreate &dboList) 
{
		//add index definition by its name (important).
		dboList["IND_EPNI_NAME"] = "CREATE INDEX IND_EPNI_NAME ON F_IND_PROJNAME (EPNI_NAME)";
		dboList["IND_EPNI_PROJ"] = "CREATE INDEX IND_EPNI_PROJ ON F_IND_PROJNAME (EPNI_PROJ)";

}

void DbCreate_F_IND_PROJNAME::DefineViews(SQLDBObjectCreate &dboList) 
{


	dboList["VW_F_IND_PROJNAME"] = "CREATE VIEW VW_F_IND_PROJNAME AS SELECT EPNI_SEQUENCE,EPNI_PROJ,EPNI_NAME FROM F_IND_PROJNAME";

	dboList["VWS_F_IND_PROJNAME"] = "CREATE VIEW VWS_F_IND_PROJNAME AS SELECT EPNI_SEQUENCE,EPNI_PROJ,EPNI_NAME FROM F_IND_PROJNAME";
}

void DbCreate_F_IND_PROJNAME::DefineTriggers(SQLDBObjectCreate &dboList) 
{
	if(m_DbType==DBTYPE_FIREBIRD)
	{

		dboList["TR_F_IND_PROJNAME"] = "CREATE TRIGGER TR_F_IND_PROJNAME FOR F_IND_PROJNAME BEFORE INSERT OR UPDATE AS BEGIN \
IF(NEW.EPNI_PROJ IS NULL) THEN NEW.EPNI_PROJ='';\
IF(NEW.EPNI_NAME IS NULL) THEN NEW.EPNI_NAME='';\
END;";
	}
}
