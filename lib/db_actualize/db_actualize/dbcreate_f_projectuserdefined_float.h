#ifndef DBCREATE_F_PROJECTUSERDEFINED_FLOAT_H
#define DBCREATE_F_PROJECTUSERDEFINED_FLOAT_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_PROJECTUSERDEFINED_FLOAT : public DbSqlTableCreation
{
public: 
	DbCreate_F_PROJECTUSERDEFINED_FLOAT():DbSqlTableCreation(F_PROJECTUSERDEFINED_FLOAT){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_PROJECTUSERDEFINED_FLOAT_H
