#include "dbcreate_bus_stored_project_list.h"



//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_STORED_PROJECT_LIST::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
											create table BUS_STORED_PROJECT_LIST ( \
											BSPL_ID INTEGER not null,\
											BSPL_GLOBAL_ID VARCHAR(15) null,\
											BSPL_DAT_LAST_MODIFIED TIMESTAMP not null,\
											BSPL_NAME LONGVARCHAR null,\
											BSPL_OWNER INTEGER null,\
											BSPL_USEBY_PERSON_ID INTEGER null,\
											BSPL_USEBY_DEPT_ID INTEGER null,\
											BSPL_IS_SELECTION SMALLINT null,\
											BSPL_SELECTION_DATA VARCHAR(4000) null,\
											BSPL_IS_FILTER SMALLINT null,\
											BSPL_FILTER_DATA VARCHAR(4000) null,\
											constraint BUS_SPL_PK primary key (BSPL_ID) )"); 


	lstSQL.append("\
								   alter table BUS_STORED_PROJECT_LIST\
								   add constraint BUS_STORED_PERS_FK1 foreign key (\
								   BSPL_OWNER)\
								   references BUS_PERSON (\
								   BPER_ID) ON DELETE CASCADE");

	lstSQL.append("\
				  alter table BUS_STORED_PROJECT_LIST\
				  add constraint BUS_STORED_FK2 foreign key (\
				  BSPL_USEBY_PERSON_ID)\
				  references BUS_PERSON (\
				  BPER_ID) ON DELETE CASCADE");

	lstSQL.append("\
				  alter table BUS_STORED_PROJECT_LIST\
				  add constraint BUS_STORED_FK3 foreign key (\
				  BSPL_USEBY_DEPT_ID)\
				  references BUS_DEPARTMENTS (\
				  BDEPT_ID) ON DELETE CASCADE");

}



//probably common to all DB's:
void DbCreate_BUS_STORED_PROJECT_LIST::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
	//dboList["IND_BCMJ_CONTACT_ID"] = "create index IND_BCMJ_CONTACT_ID on BUS_CM_PICTURE (BCMPC_CONTACT_ID)  ";  
}


void DbCreate_BUS_STORED_PROJECT_LIST::DefineTriggers(SQLDBObjectCreate &dboList)
{
	//create triggers TOFIX: foe each DB, must be different
	dboList["TRG_BUS_STORED_PR_LST_DELETE"]="\
										   CREATE TRIGGER TRG_BUS_STORED_PR_LST_DELETE FOR BUS_STORED_PROJECT_LIST\
										   ACTIVE AFTER DELETE AS \
										   BEGIN \
										   DELETE FROM CORE_ACC_USER_REC WHERE OLD.BSPL_ID = CUAR_RECORD_ID AND CUAR_TABLE_ID="+QVariant(BUS_STORED_PROJECT_LIST).toString()+";\
										   DELETE FROM CORE_ACC_GROUP_REC WHERE OLD.BSPL_ID = CGAR_RECORD_ID AND CGAR_TABLE_ID="+QVariant(BUS_STORED_PROJECT_LIST).toString()+";\
										   END\
										   ";
}
