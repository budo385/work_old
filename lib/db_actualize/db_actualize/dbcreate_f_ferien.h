#ifndef DBCREATE_F_FERIEN_H
#define DBCREATE_F_FERIEN_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_FERIEN : public DbSqlTableCreation
{
public: 
	DbCreate_F_FERIEN():DbSqlTableCreation(F_FERIEN){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_FERIEN_H
