#ifndef DBCREATE_F_TK_KA_KLASSEN_H
#define DBCREATE_F_TK_KA_KLASSEN_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_TK_KA_KLASSEN : public DbSqlTableCreation
{
public: 
	DbCreate_F_TK_KA_KLASSEN():DbSqlTableCreation(F_TK_KA_KLASSEN){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_TK_KA_KLASSEN_H
