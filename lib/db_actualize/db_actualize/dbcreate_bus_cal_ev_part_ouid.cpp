#include "dbcreate_bus_cal_ev_part_ouid.h"


//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_CAL_EV_PART_OUID::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
								   create table BUS_CAL_EV_PART_OUID (\
								   BCPOD_ID INTEGER not null,\
								   BCPOD_GLOBAL_ID VARCHAR(15) null,\
								   BCPOD_DAT_LAST_MODIFIED TIMESTAMP not null,\
								   BCPOD_PART_ID INTEGER not null,\
								   BCPOD_OUTLOOK_ID VARCHAR(250) null,\
								   constraint BUS_CAL_EV_PART_OUID_PK primary key (BCPOD_ID) )   ");  


	lstSQL.append("\
								   alter table BUS_CAL_EV_PART_OUID\
								   add constraint BUS_CAL_EVOUID_CAL_PART_FK1 foreign key (\
								   BCPOD_PART_ID)\
								   references BUS_CAL_EVENT_PART (\
								   BCEP_ID)	");	
}



//probably common to all DB's:
void DbCreate_BUS_CAL_EV_PART_OUID::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
}



