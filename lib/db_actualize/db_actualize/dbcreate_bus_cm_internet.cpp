#include "dbcreate_bus_cm_internet.h"




//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_CM_INTERNET::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
											create table BUS_CM_INTERNET ( \
											BCMI_ID INTEGER not null,\
											BCMI_GLOBAL_ID VARCHAR(15) null,\
											BCMI_DAT_LAST_MODIFIED TIMESTAMP not null,\
											BCMI_CONTACT_ID INTEGER not null,\
											BCMI_TYPE_ID INTEGER null,\
											BCMI_NAME VARCHAR(100) null,\
											BCMI_ADDRESS VARCHAR(200) not null,\
											BCMI_DESCRIPTION LONGVARCHAR null,\
											BCMI_EXT_ID INTEGER null, \
											BCMI_IS_DEFAULT SMALLINT not null,constraint BUS_CM_INTERNET_PK primary key (BCMI_ID) )  ");
	//constraint for table creation:
	lstSQL.append("\
											alter table BUS_CM_INTERNET\
											add constraint BUS_CONTACTS_INTERNET_FK1 foreign key (\
											BCMI_CONTACT_ID)\
											references BUS_CM_CONTACT (\
											BCNT_ID)  \
											ON DELETE CASCADE");


	//if there is one item with given type: can not delete type
	lstSQL.append("\
											alter table BUS_CM_INTERNET\
											add constraint BUS_TYPES_INTERNET_FK1 foreign key (\
											BCMI_TYPE_ID)\
											references BUS_CM_TYPES (\
											BCMT_ID)  ");
}



//probably common to all DB's:
void DbCreate_BUS_CM_INTERNET::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
	dboList["IND_BCMI_CONTACT_ID"] = "create index IND_BCMI_CONTACT_ID on BUS_CM_INTERNET (BCMI_CONTACT_ID)";  
	dboList["IND_BCMI_TYPE_ID"] = "create index IND_BCMI_TYPE_ID on BUS_CM_INTERNET (BCMI_TYPE_ID)";  
}


