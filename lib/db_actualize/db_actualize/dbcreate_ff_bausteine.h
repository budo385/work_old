#ifndef DBCREATE_FF_BAUSTEINE_H
#define DBCREATE_FF_BAUSTEINE_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_FF_BAUSTEINE : public DbSqlTableCreation
{
public: 
	DbCreate_FF_BAUSTEINE():DbSqlTableCreation(FF_BAUSTEINE){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // FF_BAUSTEINE_H
