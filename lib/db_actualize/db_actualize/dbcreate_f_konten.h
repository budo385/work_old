#ifndef DBCREATE_F_KONTEN_H
#define DBCREATE_F_KONTEN_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_KONTEN : public DbSqlTableCreation
{
public: 
	DbCreate_F_KONTEN():DbSqlTableCreation(F_KONTEN){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_KONTEN_H
