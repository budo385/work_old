#include "dbcreate_bus_storedlist.h"

//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_STOREDLIST::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
											create table BUS_STOREDLIST (			\
											BUSL_ID INTEGER not null,				\
											BUSL_GLOBAL_ID VARCHAR(15) null,			\
											BUSL_DAT_LAST_MODIFIED TIMESTAMP not null,\
											BUSL_STLIST_CODE VARCHAR(100) not null,	\
											BUSL_TREETBL_ID INTEGER not null,		\
											BUSL_OWNER INTEGER null,			\
											BUSL_DEPARTMENT INTEGER null,		\
											BUSL_FILTER_ID INTEGER null, constraint BUS_STOREDLIST_PK primary key (BUSL_ID) )");

	//no Db versioning:
	
	m_lstDeletedCols<<"BUSL_STLIST_ID";

}

//probably common to all DB's:
void DbCreate_BUS_STOREDLIST::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
	dboList["IND_TREETBL_ID"] = "create index IND_TREETBL_ID on BUS_STOREDLIST (BUSL_TREETBL_ID)";
}
