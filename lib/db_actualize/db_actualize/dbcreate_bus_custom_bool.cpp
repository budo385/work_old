#include "dbcreate_bus_custom_bool.h"



//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_CUSTOM_BOOL::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
											create table BUS_CUSTOM_BOOL ( \
											BCB_ID INTEGER not null,\
											BCB_GLOBAL_ID VARCHAR(15) null,\
											BCB_DAT_LAST_MODIFIED TIMESTAMP not null,\
											BCB_CUSTOM_FIELD_ID INTEGER not null,\
											BCB_VALUE SMALLINT not null,\
											BCB_TABLE_ID INTEGER not null,\
											BCB_RECORD_ID INTEGER not null, constraint BUS_CUSTOM_BOOL_PK primary key (BCB_ID) ) "); 
											
							lstSQL.append("\
											alter table BUS_CUSTOM_BOOL\
											add constraint BUS_CUSTOM_BOOL_FK1 foreign key (\
											BCB_CUSTOM_FIELD_ID)\
											references BUS_CUSTOM_FIELDS (\
											BCF_ID)  \
											ON DELETE CASCADE");

}

//probably common to all DB's:
void DbCreate_BUS_CUSTOM_BOOL::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
	dboList["IND_BCB_CI_TABLE_REC"] = "create index IND_BCB_CI_TABLE_REC on BUS_CUSTOM_BOOL (BCB_TABLE_ID,BCB_RECORD_ID)"; 
	dboList["IND_BCB_CI_TABLE_VAL"] = "create index IND_BCB_CI_TABLE_VAL on BUS_CUSTOM_BOOL (BCB_TABLE_ID,BCB_VALUE)"; 
	dboList["IND_BCB_CI_UNIQUE"] = "create unique index IND_BCB_CI_UNIQUE on BUS_CUSTOM_BOOL (BCB_TABLE_ID,BCB_RECORD_ID,BCB_CUSTOM_FIELD_ID)"; 

}


