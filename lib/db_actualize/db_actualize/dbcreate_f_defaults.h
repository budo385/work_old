#ifndef DBCREATE_F_DEFAULTS_H
#define DBCREATE_F_DEFAULTS_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_DEFAULTS : public DbSqlTableCreation
{
public: 
	DbCreate_F_DEFAULTS():DbSqlTableCreation(F_DEFAULTS){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_DEFAULTS_H
