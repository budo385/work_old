#ifndef DBCREATE_F_FAKT_PLAN_H
#define DBCREATE_F_FAKT_PLAN_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_FAKT_PLAN : public DbSqlTableCreation
{
public: 
	DbCreate_F_FAKT_PLAN():DbSqlTableCreation(F_FAKT_PLAN){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_FAKT_PLAN_H
