#ifndef DBCREATE_F_NK_BUDGET_H
#define DBCREATE_F_NK_BUDGET_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_NK_BUDGET : public DbSqlTableCreation
{
public: 
	DbCreate_F_NK_BUDGET():DbSqlTableCreation(F_NK_BUDGET){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_NK_BUDGET_H
