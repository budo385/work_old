#ifndef DBCREATE_CORE_USERROLE_H
#define DBCREATE_CORE_USERROLE_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_CORE_USERROLE : public DbSqlTableCreation
{
public:
	DbCreate_CORE_USERROLE():DbSqlTableCreation(CORE_USERROLE){};  //init to CORE_USERROLE table
	

private:
	//create generic Table def:
	void DefineFields(QStringList &lstSQL);

	//probably common to all DB's:
	void DefineIndexes(SQLDBObjectCreate &dboList);
};

#endif // DBCREATE_CORE_USERROLE_H

