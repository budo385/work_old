#include "dbcreate_bus_cal_resource.h"

//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_CAL_RESOURCE::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
								   create table BUS_CAL_RESOURCE (\
								   BRES_ID INTEGER not null,\
								   BRES_GLOBAL_ID VARCHAR(15) null,\
								   BRES_DAT_LAST_MODIFIED TIMESTAMP not null,\
								   BRES_ORGANIZATION_ID INTEGER null,\
								   BRES_NAME VARCHAR(250) null,\
								   BRES_VALID_FROM DATETIME null,\
								   BRES_VALID_TO DATETIME null,\
								   BRES_LOCATION VARCHAR(100) null,\
								   BRES_DESCRIPTION LONGVARCHAR null, constraint BUS_CAL_RESOURCE_PK primary key (BRES_ID) )"); 
											

	lstSQL.append("\
											alter table BUS_CAL_RESOURCE\
											add constraint BRES_ORG_K1 foreign key (\
											BRES_ORGANIZATION_ID)\
											references BUS_ORGANIZATIONS (BORG_ID)\
											");	


	//no Db versioning:
	
	//Delete columns.
	m_lstDeletedCols << "BEM_IS_PRIVATE";

	//create indexes
	SQLDBObjectCreate dboList;
	DefineIndexes(dboList);
	m_Indexes[DBTYPE_ODBC]=dboList;
	dboList.clear();
	//create triggers
	dboList["TRG_BUS_CAL_RESOURCE_DELETE"]="\
									  CREATE TRIGGER TRG_BUS_CAL_RESOURCE_DELETE AFTER DELETE ON BUS_CAL_RESOURCE\
									  FOR EACH ROW\
										 BEGIN \
										 DELETE FROM bus_nmrx_relation WHERE OLD.BRES_ID = BNMR_TABLE_KEY_ID_1 AND BNMR_TABLE_1="+QVariant(BUS_CAL_RESOURCE).toString()+";\
										 DELETE FROM bus_nmrx_relation WHERE OLD.BRES_ID = BNMR_TABLE_KEY_ID_2 AND BNMR_TABLE_2="+QVariant(BUS_CAL_RESOURCE).toString()+";\
										 END\
										 ";

	
}



//probably common to all DB's:
void DbCreate_BUS_CAL_RESOURCE::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();

}


void DbCreate_BUS_CAL_RESOURCE::DefineTriggers(SQLDBObjectCreate &dboList)
{
	//create triggers TOFIX: foe each DB, must be different
	dboList["TRG_BUS_CAL_RESOURCE_DELETE"]="\
									  CREATE TRIGGER TRG_BUS_CAL_RESOURCE_DELETE FOR BUS_CAL_RESOURCE\
									  ACTIVE AFTER DELETE AS \
									  BEGIN \
									  DELETE FROM bus_nmrx_relation WHERE OLD.BRES_ID = BNMR_TABLE_KEY_ID_1 AND BNMR_TABLE_1="+QVariant(BUS_CAL_RESOURCE).toString()+";\
									  DELETE FROM bus_nmrx_relation WHERE OLD.BRES_ID = BNMR_TABLE_KEY_ID_2 AND BNMR_TABLE_2="+QVariant(BUS_CAL_RESOURCE).toString()+";\
									  END\
									  ";
}