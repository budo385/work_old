#ifndef DBCREATE_SPC_INDUSTRY_GROUP_H
#define DBCREATE_SPC_INDUSTRY_GROUP_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_SPC_INDUSTRY_GROUP : public DbSqlTableCreation
{
public: 
	DbCreate_SPC_INDUSTRY_GROUP():DbSqlTableCreation(SPC_INDUSTRY_GROUP){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // SPC_INDUSTRY_GROUP_H
