#include "dbcreate_test_tree.h"

//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------

void DbCreate_TEST_TREE::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
											create table TEST_TREE (			\
											TEST_ID INTEGER not null,			\
											TEST_HLEVEL INTEGER null,			\
											TEST_HPARENT INTEGER null,			\
											TEST_HSIBLING INTEGER null,			\
											TEST_HHASCHILDREN INTEGER null,		\
											TEST_HCODE VARCHAR(50) null,		\
											TEST_HICON VARCHAR(50) null,		\
											TEST_MAINPARENTID VARCHAR(50) null,	\
											TEST_HNAME VARCHAR(50) null,		\
											TEST_MAINPARENTNAME INTEGER null, constraint TEST_TREE_PK primary key (TEST_ID) )");
}

/* Probably index definition is same for all DB's */
void DbCreate_TEST_TREE::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
	dboList["IND_TEST_HPARENT"] = "create index IND_TEST_HPARENT on TEST_TREE (TEST_HPARENT)";  
	dboList["IND_TEST_TREE_TREE_VIEW"] = "create index IND_TEST_TREE_TREE_VIEW on TEST_TREE (TEST_HLEVEL, TEST_HPARENT, TEST_HSIBLING)";  
}


