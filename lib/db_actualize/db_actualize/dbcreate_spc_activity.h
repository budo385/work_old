#ifndef DBCREATE_SPC_ACTIVITY_H
#define DBCREATE_SPC_ACTIVITY_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_SPC_ACTIVITY : public DbSqlTableCreation
{
public: 
	DbCreate_SPC_ACTIVITY():DbSqlTableCreation(SPC_ACTIVITY){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // SPC_ACTIVITY_H
