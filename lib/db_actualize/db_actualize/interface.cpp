#include "interface.h"
#include "dborganizer.h"

DbSqlManager m_pDbManager;
bool m_bConnectionEstablished=false;
QString m_strLastError;
QByteArray tmp;
QString m_strBackupPath;
bool m_bShutDownDatabase=false;

//for server: enable UTC, for PE client disable..

bool InitializeDatabaseConnection(const char **ppszDatabaseParams,const char *szBackupPath,bool bShutDownDatabase,char *szErrorString)
{
	//if already connected
	/*
	if (m_bConnectionEstablished)
	{
		m_strLastError=QObject::tr("Connection already established. Disconnect first!");
		strcpy(szErrorString,m_strLastError.toLocal8Bit().data());
		return false;
	}
	*/



	m_strBackupPath=szBackupPath;
	m_bShutDownDatabase=bShutDownDatabase;

	//must be 5 params:
	DbConnectionSettings ConnSettings;
	//Make DB connection:
	ConnSettings.m_strDbType=ppszDatabaseParams[0];
	ConnSettings.m_strDbHostName=ppszDatabaseParams[1];
	ConnSettings.m_strDbUserName=ppszDatabaseParams[2];
	ConnSettings.m_strDbPassword=ppszDatabaseParams[3];
	ConnSettings.m_strDbName=ppszDatabaseParams[4];

	Status err;
	m_pDbManager.Initialize(err,ConnSettings,10);
	if (!err.IsOK())
	{
		m_strLastError=err.getErrorText();
		strcpy(szErrorString,m_strLastError.toLocal8Bit().data());
		return false;
	}
	else
		m_bConnectionEstablished=true;


	

	/* BT FIREBIRD TEST */
/*
	//Status err;
	DbSqlQuery query(err,&m_pDbManager);
	DbRecordSet data;

	query.Execute(err,"SELECT * FROM FASSIGN");
	data.destroy();
	query.FetchData(data);
	if (data.getRowCount()>0)
	{
		err.setError(1,"FB Connection Successfully tested on SPC database!!!!");
	}
*/
	//PROC_CHARGE_CUMULATION
/*
	query.Prepare(err,"EXECUTE PROCEDURE PROC_CHARGE_CUMULATION ?, ?, ?, ?, ?, ? ,?, ?, ?, ?");
	QDate d1=QDate(2000,1,1);
	QDate d2=QDate::currentDate();
	QString str1="P.";
	
	//QString str1="",str2="";
	QString str3="01.01.2011,31.01.2011,01.02.2011,28.02.2011,01.03.2011,31.3.2011";

	query.bindValue(0, str1);
	query.bindValue(1, d1);
	query.bindValue(2, d2);
	query.bindValue(3, 0);
	query.bindValue(4, 0);
	query.bindValue(5, 0);
	query.bindValue(6, 0);
	query.bindValue(7, 1);
	query.bindValue(8, 0);
	query.bindValue(9, "");

	query.ExecutePrepared(err);

	query.Execute(err,"SELECT * FROM TEMP_CHARGE_CUMULATION");
	data.destroy();
	query.FetchData(data);
	_DUMP(data);

	query.Execute(err,"SELECT * FROM TEST_TABLE");
	data.destroy();
	query.FetchData(data);
	_DUMP(data);
*/

/*

	//PROC_FAKTURA_CUMULATION

	query.Prepare(err,"EXECUTE PROCEDURE PROC_FAKTURA_CUMULATION ?, ?, ?, ?, ?, ?");
	QDate d1=QDate(2000,1,1);
	QDate d2=QDate::currentDate();
	QString str1="P.";
	//QString str1="",str2="";
	//QString str3="01.01.2011,31.01.2011,01.02.2011,28.02.2011,01.03.2011,31.3.2011";
	query.bindValue(0, str1);
	query.bindValue(1, d1);
	query.bindValue(2, d2);
	query.bindValue(3, 1);
	query.bindValue(4, 0);
	query.bindValue(5, "");

	query.ExecutePrepared(err);

	query.Execute(err,"SELECT * FROM TEMP_FAKTURA_CUMULATION");
	data.destroy();
	query.FetchData(data);
	_DUMP(data);

	query.Execute(err,"SELECT * FROM TEST_TABLE");
	data.destroy();
	query.FetchData(data);
	_DUMP(data);
*/

/*
	//PROC_EXP_CUMUL_PROJ_EXPTYPE

	query.Prepare(err,"EXECUTE PROCEDURE PROC_EXP_CUMUL_PROJ_EXPTYPE ?, ?, ?, ?, ?");
	QDate d1=QDate(2000,1,1);
	QDate d2=QDate::currentDate();
	QString str1="P.";
	//QString str1="",str2="";
	//QString str3="01.01.2011,31.01.2011,01.02.2011,28.02.2011,01.03.2011,31.3.2011";
	query.bindValue(0, str1);
	query.bindValue(1, d1);
	query.bindValue(2, d2);
	query.bindValue(3, "");
	query.bindValue(4, 1);

	query.ExecutePrepared(err);

	query.Execute(err,"SELECT * FROM TEMP_EXP_CUMUL_PROJ_EXPTYPE");
	data.destroy();
	query.FetchData(data);
	_DUMP(data);

	query.Execute(err,"SELECT * FROM TEST_TABLE");
	data.destroy();
	query.FetchData(data);
	_DUMP(data);

*/


	//PROC_EXP_CUMUL_PROJ_EXPTYPE
/*
	query.Prepare(err,"EXECUTE PROCEDURE PROC_PAYMENT_CUMULATION ?, ?, ?, ?, ?, ?");
	QDate d1=QDate(2000,1,1);
	QDate d2=QDate::currentDate();
	QString str1="P.";
	//QString str1="",str2="";
	//QString str3="01.01.2011,31.01.2011,01.02.2011,28.02.2011,01.03.2011,31.3.2011";
	query.bindValue(0, str1);
	query.bindValue(1, d1);
	query.bindValue(2, d2);
	query.bindValue(3, 1);
	query.bindValue(4, "");
	query.bindValue(5, "");

	query.ExecutePrepared(err);

	query.Execute(err,"SELECT * FROM TEMP_PAYMENT_CUMULATION");
	data.destroy();
	query.FetchData(data);
	_DUMP(data);

	query.Execute(err,"SELECT * FROM TEST_TABLE");
	data.destroy();
	query.FetchData(data);
	_DUMP(data);

	*/
/*
	//PROC_LEVY_CUMULATION

	query.Prepare(err,"EXECUTE PROCEDURE PROC_LEVY_CUMULATION ?, ?, ?, ?, ?, ?");
	QDate d1=QDate(2000,1,1);
	QDate d2=QDate::currentDate();
	QString str1="P.";
	//QString str1="",str2="";
	//QString str3="01.01.2011,31.01.2011,01.02.2011,28.02.2011,01.03.2011,31.3.2011";
	query.bindValue(0, str1);
	query.bindValue(1, d1);
	query.bindValue(2, d2);
	query.bindValue(3, 1);
	query.bindValue(4, 0);
	query.bindValue(5, "");

	query.ExecutePrepared(err);

	query.Execute(err,"SELECT * FROM TEMP_LEVY_CUMULATION");
	data.destroy();
	query.FetchData(data);
	_DUMP(data);

	query.Execute(err,"SELECT * FROM TEST_TABLE");
	data.destroy();
	query.FetchData(data);
	_DUMP(data);
*/
/*
	//PROC_BF_PROJ_PERS_PERIOD_SER

	query.Prepare(err,"EXECUTE PROCEDURE PROC_BF_PROJ_PERS_PERIOD_SER ?, ?, ?, ?, ?, ?");
	QDate d1=QDate(2000,1,1);
	QDate d2=QDate::currentDate();
	//QString str1="P.";
	QString str1="P.",str2="M3245";
	//QString str1="",str2="";
	QString str3="01.01.2011,31.01.2011,01.02.2011,28.02.2011,01.03.2011,31.3.2011";

	query.bindValue(0, str1);
	query.bindValue(1, str2);
	query.bindValue(2, str3);
	query.bindValue(3, 1);
	query.bindValue(4, 0);
	query.bindValue(5, 0);

	query.ExecutePrepared(err);

	query.Execute(err,"SELECT * FROM TEMP_BF_PROJ_PERS_PERIOD_SER");
	data.destroy();
	query.FetchData(data);
	_DUMP(data);

	query.Execute(err,"SELECT * FROM TEST_TABLE");
	data.destroy();
	query.FetchData(data);
	_DUMP(data);
*/

	//PROC_CALC_WORK_CHARGE_RATES
/*
	query.Prepare(err,"EXECUTE PROCEDURE PROC_CALC_WORK_CHARGE_RATES ?, ?, ?, ?, ?, ? ,?, ?, ?, ?");
	QDate d1=QDate(2010,1,1);
	QDate d2=QDate::currentDate();
	QString str1="P.E.6711.2.941.1.",str2="M3245";
	str1="P.";
	//QString str1="",str2="";
	QString str3="01.01.2011,31.01.2011,01.02.2011,28.02.2011,01.03.2011,31.3.2011";

	query.bindValue(0, 0);
	query.bindValue(1, 0);
	query.bindValue(2, "M3245");
	query.bindValue(3, str1);
	query.bindValue(4, d1);
	query.bindValue(5, "CON");
	query.bindValue(6, "PA");
	query.bindValue(7, 0);
	query.bindValue(8, d1);
	query.bindValue(9, d2);


	//query.GetQSqlQuery()->bindValue(4, 0, QSql::Out);
	query.ExecutePrepared(err);
	//int i = query.GetQSqlQuery()->boundValue(4).toInt(); 


	query.Execute(err,"SELECT * FROM TEMP_CALC_WORK_CHARGE_RATES");
	data.destroy();
	query.FetchData(data);
	_DUMP(data);



	query.Execute(err,"SELECT * FROM TEMP_CALC_WORK_CHARGE_RATES_1");
	data.destroy();
	query.FetchData(data);
	_DUMP(data);
*/

	/* BT ORACLE TEST */

	

	//Status er;

	/*
	DbSqlQuery query(er,&m_pDbManager);
	DbRecordSet data;

	query.Execute(er,"SELECT * FROM FASSIGN");
	data.destroy();
	query.FetchData(data);
	if (data.getRowCount()>0)
	{
		err.setError(1,"ORACLE Connection Successfulyl tested on SPC database!!!!");
		m_strLastError=err.getErrorText();
		strcpy(szErrorString,m_strLastError.toLocal8Bit().data());
		return false;
	}
	//_DUMP(data);

*/
	/*
	query.Prepare(er,"CALL PROC_CALC_WORK_CHARGE_RATES(?, ?, ?, ?, ?, ? ,?, ?, ?, ?)");
	QDate d1=QDate(2010,1,1);
	QDate d2=QDate::currentDate();
	QString str1="P.E.6711.2.941.1.",str2="M3245";
	str1="P.";
	//QString str1="",str2="";
	QString str3="01.01.2011,31.01.2011,01.02.2011,28.02.2011,01.03.2011,31.3.2011";
		
	query.bindValue(0, 0);
	query.bindValue(1, 0);
	query.bindValue(2, "M3245");
	query.bindValue(3, "P.E.6711.2.951.");
	query.bindValue(4, d1);
	query.bindValue(5, "CON");
	query.bindValue(6, "PA");
	query.bindValue(7, 0);
	query.bindValue(8, d1);
	query.bindValue(9, d2);


	//query.GetQSqlQuery()->bindValue(4, 0, QSql::Out);
	query.ExecutePrepared(er);
	//int i = query.GetQSqlQuery()->boundValue(4).toInt(); 


	query.Execute(er,"SELECT * FROM TEMP_CALC_WORK_CHARGE_RATES");
	data.destroy();
	query.FetchData(data);
	_DUMP(data);
	


	query.Execute(er,"SELECT * FROM TEMP_CALC_WORK_CHARGE_RATES_1");
	data.destroy();
	query.FetchData(data);
	_DUMP(data);
*/

	


/*
	Status er;
	DbSqlQuery sql(er,&m_pDbManager);
	bool bOK=sql.GetQSqlQuery()->exec("UPDATE CORE_APP_SRV_SESSION SET COAS_DAT_LAST_MODIFIED= '2009-06-19 14.30.30'");
*/
	return true;


}
/*
bool OpenDatabaseConnection(const char **ppszDatabaseParams,const char *szBackupPath,char *szErrorString)
{
	//if already connected
	if (m_bConnectionEstablished)
	{
		m_strLastError=QObject::tr("Connection already established. Disconnect first!");
		strcpy(szErrorString,m_strLastError.toLocal8Bit().data());
		return false;
	}

	m_strBackupPath=szBackupPath;

	//must be 5 params:
	DbConnectionSettings ConnSettings;
	//Make DB connection:
	ConnSettings.m_strDbType=ppszDatabaseParams[0];
	ConnSettings.m_strDbHostName=ppszDatabaseParams[1];
	ConnSettings.m_strDbUserName=ppszDatabaseParams[2];
	ConnSettings.m_strDbPassword=ppszDatabaseParams[3];
	ConnSettings.m_strDbName=ppszDatabaseParams[4];

	Status err;
	m_pDbManager.Initialize(err,ConnSettings,10);
	if (!err.IsOK())
	{
		m_strLastError=err.getErrorText();
		strcpy(szErrorString,m_strLastError.toLocal8Bit().data());
		return false;
	}
	else
		m_bConnectionEstablished=true;

	return true;

}
*/

bool CloseDatabaseConnection(char *szErrorString)
{
	//if already connected
	if (m_bConnectionEstablished)
	{
		m_pDbManager.ShutDown();
		m_bConnectionEstablished=false;
	}
	return true;
}


bool OrganizeDatabase(char *szErrorString,bool bRebuildTables)
{
	if (!m_bConnectionEstablished)
	{
		m_strLastError=QObject::tr("Database connection not established. Connect first!");
		strcpy(szErrorString,m_strLastError.toLocal8Bit().data());
		return false;
	}

	DbOrganizer dbOrg(&m_pDbManager,m_strBackupPath,m_bShutDownDatabase);
	Status err;
	dbOrg.OrganizeDatabase(err,bRebuildTables);
	if (!err.IsOK())
	{
		m_strLastError=err.getErrorText();
		strcpy(szErrorString,m_strLastError.toLocal8Bit().data());
		return false;
	}
	return true;

}

//change backup path:
bool BackupDatabase(char *szErrorString,const char *szBackupPath,char *szBcpFileNameCreated)
{
	if (szBackupPath!=NULL)
		m_strBackupPath=szBackupPath;

	if (!m_bConnectionEstablished)
	{
		m_strLastError=QObject::tr("Database connection not established. Connect first!");
		strcpy(szErrorString,m_strLastError.toLocal8Bit().data());
		return false;
	}

	DbOrganizer dbOrg(&m_pDbManager,m_strBackupPath,m_bShutDownDatabase);
	Status err;
	QString strBcpName=dbOrg.BackupDatabase(err);
	if (!err.IsOK())
	{
		m_strLastError=err.getErrorText();
		strcpy(szErrorString,m_strLastError.toLocal8Bit().data());
		return false;
	}
	else 
	{
		if (szBcpFileNameCreated)
		{
			strcpy(szBcpFileNameCreated,strBcpName.toLocal8Bit().data());
		}
	}
	return true;


}
bool RestoreDatabase(const char *szBackupFilePath,char *szErrorString)
{
	if (!m_bConnectionEstablished)
	{
		m_strLastError=QObject::tr("Database connection not established. Connect first!");
		strcpy(szErrorString,m_strLastError.toLocal8Bit().data());
		return false;
	}



	DbOrganizer dbOrg(&m_pDbManager,m_strBackupPath,m_bShutDownDatabase);
	Status err;
	QString strPath=szBackupFilePath;

	dbOrg.RestoreDatabase(strPath,err);
	if (!err.IsOK())
	{
		m_strLastError=err.getErrorText();
		strcpy(szErrorString,m_strLastError.toLocal8Bit().data());
		return false;
	}
	return true;

}
int GetDatabaseCurrentVersion(char *szErrorString)
{
	if (!m_bConnectionEstablished)
	{
		m_strLastError=QObject::tr("Database connection not established. Connect first!");
		strcpy(szErrorString,m_strLastError.toLocal8Bit().data());
		return -1;
	}

	DbOrganizer dbOrg(&m_pDbManager,m_strBackupPath,m_bShutDownDatabase);
	Status err;

	int nVer=dbOrg.GetDatabaseCurrentVersion(err);
	if (!err.IsOK())
	{
		m_strLastError=err.getErrorText();
		strcpy(szErrorString,m_strLastError.toLocal8Bit().data());
		return -1;
	}
	return nVer;
}

int GetDatabaseCurrentVersion_SPC(char *szErrorString)
{
	if (!m_bConnectionEstablished)
	{
		m_strLastError=QObject::tr("Database connection not established. Connect first!");
		strcpy(szErrorString,m_strLastError.toLocal8Bit().data());
		return -1;
	}

	DbOrganizer dbOrg(&m_pDbManager,m_strBackupPath,m_bShutDownDatabase);
	Status err;

	int nVer=dbOrg.GetDatabaseCurrentVersion_SPC(err);
	if (!err.IsOK())
	{
		m_strLastError=err.getErrorText();
		strcpy(szErrorString,m_strLastError.toLocal8Bit().data());
		return -1;
	}
	return nVer;

}


bool CopyDatabase(char *szErrorString, const char **ppszDatabaseParamsTarget,bool bDestroyPrevious)
{
	if (!m_bConnectionEstablished)
	{
		m_strLastError=QObject::tr("Database connection not established. Connect first!");
		strcpy(szErrorString,m_strLastError.toLocal8Bit().data());
		return false;
	}

	//connect to target:
	DbSqlManager pDbManagerTarget; 

	//must be 5 params:
	DbConnectionSettings ConnSettings;
	//Make DB connection:
	ConnSettings.m_strDbType=ppszDatabaseParamsTarget[0];
	ConnSettings.m_strDbHostName=ppszDatabaseParamsTarget[1];
	ConnSettings.m_strDbUserName=ppszDatabaseParamsTarget[2];
	ConnSettings.m_strDbPassword=ppszDatabaseParamsTarget[3];
	ConnSettings.m_strDbName=ppszDatabaseParamsTarget[4];

	Status err;
	pDbManagerTarget.Initialize(err,ConnSettings,10);
	if (!err.IsOK())
	{
		m_strLastError=err.getErrorText();
		strcpy(szErrorString,m_strLastError.toLocal8Bit().data());
		return false;
	}

	DbOrganizer dbOrg(&m_pDbManager,m_strBackupPath,m_bShutDownDatabase);
	dbOrg.CopyDatabase(err,&pDbManagerTarget,bDestroyPrevious);
	if (!err.IsOK())
	{
		m_strLastError=err.getErrorText();
		strcpy(szErrorString,m_strLastError.toLocal8Bit().data());
		return false;
	}
	
	return true;

}

bool CheckForDemoDatabase(char *szErrorString, bool bEnableDemo, int nMaxSizeKb, int nWarnSizeKb)
{

	if (!m_bConnectionEstablished)
	{
		m_strLastError=QObject::tr("Database connection not established. Connect first!");
		strcpy(szErrorString,m_strLastError.toLocal8Bit().data());
		return false;
	}

	DbOrganizer dbOrg(&m_pDbManager,m_strBackupPath,m_bShutDownDatabase);
	Status err;

	dbOrg.CheckForDemoDatabase(err,bEnableDemo,nMaxSizeKb,nWarnSizeKb);
	if (!err.IsOK())
	{
		m_strLastError=err.getErrorText();
		strcpy(szErrorString,m_strLastError.toLocal8Bit().data());
		return false; 
	}

	return true;
}

bool RecalcDemoDatabaseSize(char *szErrorString, int *nCurrSizeKb)
{
	if (!m_bConnectionEstablished)
	{
		m_strLastError=QObject::tr("Database connection not established. Connect first!");
		strcpy(szErrorString,m_strLastError.toLocal8Bit().data());
		return false;
	}

	DbOrganizer dbOrg(&m_pDbManager,m_strBackupPath,m_bShutDownDatabase);
	Status err;

	dbOrg.RecalcDemoDatabaseSize(err,*nCurrSizeKb);
	if (!err.IsOK())
	{
		m_strLastError=err.getErrorText();
		strcpy(szErrorString,m_strLastError.toLocal8Bit().data());
		return false;
	}

	return true;

}

bool CreateReadOnlyUserForODBCAccess(char *szErrorString)
{
	if (!m_bConnectionEstablished)
	{
		m_strLastError=QObject::tr("Database connection not established. Connect first!");
		strcpy(szErrorString,m_strLastError.toLocal8Bit().data());
		return false;
	}

	DbOrganizer dbOrg(&m_pDbManager,m_strBackupPath,m_bShutDownDatabase);
	Status err;

	dbOrg.CreateReadOnlyUserForODBCAccess(err);
	if (!err.IsOK())
	{
		m_strLastError=err.getErrorText();
		strcpy(szErrorString,m_strLastError.toLocal8Bit().data());
		return false;
	}

	return true;
}

bool ImportDataFromOracleExport(char *szErrorString,const char *strDataFile, int *nProcessedInsertStatements, int *nProcessedSequenceUpdateStatements)
{
	if (!m_bConnectionEstablished)
	{
		m_strLastError=QObject::tr("Database connection not established. Connect first!");
		strcpy(szErrorString,m_strLastError.toLocal8Bit().data());
		return false;
	}

	QString strDataFileForParsing=strDataFile;

	DbOrganizer dbOrg(&m_pDbManager,m_strBackupPath,m_bShutDownDatabase);
	Status err;

	dbOrg.ImportDataFromOracleExport(err,strDataFileForParsing,*nProcessedInsertStatements,*nProcessedSequenceUpdateStatements);
	if (!err.IsOK())
	{
		m_strLastError=err.getErrorText();
		strcpy(szErrorString,m_strLastError.toLocal8Bit().data());
		return false;
	}

	return true;
}

bool ImportDataFromOmnisExport(char *szErrorString,const char *strDataFile, int *nProcessedInsertStatements, bool bDropExisting)
{
	if (!m_bConnectionEstablished)
	{
		m_strLastError=QObject::tr("Database connection not established. Connect first!");
		strcpy(szErrorString,m_strLastError.toLocal8Bit().data());
		return false;
	}

	QString strDataFileForParsing=strDataFile;

	DbOrganizer dbOrg(&m_pDbManager,m_strBackupPath,m_bShutDownDatabase);
	Status err;

	dbOrg.ImportDataFromOmnisExport(err,strDataFileForParsing,*nProcessedInsertStatements,bDropExisting);
	if (!err.IsOK())
	{
		m_strLastError=err.getErrorText();
		strcpy(szErrorString,m_strLastError.toLocal8Bit().data());
		return false;
	}
/*
	QFile status_file(QCoreApplication::applicationDirPath()+"/status.log");
	QString strStatus =QString("\nInterface - after ImportDataFromOmnisExport");
	if(status_file.open(QIODevice::Append))
	{
		status_file.write(strStatus.toLatin1());
		status_file.close();
	}
*/
	return true;
}




