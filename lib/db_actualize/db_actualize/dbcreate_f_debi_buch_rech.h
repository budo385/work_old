#ifndef DBCREATE_F_DEBI_BUCH_RECH_H
#define DBCREATE_F_DEBI_BUCH_RECH_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_DEBI_BUCH_RECH : public DbSqlTableCreation
{
public: 
	DbCreate_F_DEBI_BUCH_RECH():DbSqlTableCreation(F_DEBI_BUCH_RECH){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_DEBI_BUCH_RECH_H
