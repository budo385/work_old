#include "dbcreate_core_user_session.h"



//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------

void DbCreate_CORE_USER_SESSION::DefineFields(QStringList &lstSQL)
{

	//Table shema statements for ODBC.
	lstSQL.append("\
	create table CORE_USER_SESSION ( \
	COUS_ID INTEGER not null,\
	COUS_GLOBAL_ID VARCHAR(15) null,\
	COUS_DAT_LAST_MODIFIED TIMESTAMP not null,\
	COUS_USER_SESSION_ID VARCHAR(50) not null,\
	COUS_APP_SRV_SESSION_ID INTEGER not null,\
	COUS_MODULE_CODE VARCHAR(10) not null,\
	COUS_MLID VARCHAR(16) null,\
	COUS_CORE_USER_ID INTEGER not null, \
	COUS_ACTIVE_FLAG SMALLINT not null,\
	COUS_PARENT_ID  INTEGER null, \
	constraint CORE_USER_SESSION_PK primary key (COUS_ID) )");




	//constraint for table creation:
	lstSQL.append("\
											alter table CORE_USER_SESSION \
											add constraint PK_FK1 foreign key (\
											COUS_APP_SRV_SESSION_ID)\
											references CORE_APP_SRV_SESSION (\
											COAS_ID)  ON DELETE CASCADE");

	lstSQL.append("\
											alter table CORE_USER_SESSION \
											add constraint CORE_USER_SESSION_FK1 foreign key (\
											COUS_CORE_USER_ID)\
											references CORE_USER (\
											CUSR_ID)  ON DELETE CASCADE");

	lstSQL.append("\
								   alter table CORE_USER_SESSION \
								   add constraint CORE_USER_PARENT_SES_FK1 foreign key (\
								   COUS_PARENT_ID)\
								   references CORE_USER_SESSION (\
								   COUS_ID)  ON DELETE CASCADE");

}




/* Probably index defintion is same for all DB's */
void DbCreate_CORE_USER_SESSION::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index defintion by its name (important)
	dboList.clear();
	dboList["IND_USER_SESSION_ID"]		="create unique index IND_USER_SESSION_ID on CORE_USER_SESSION (COUS_USER_SESSION_ID)";  
	dboList["IND_APP_SRV_SESSION_ID"]	="create index IND_APP_SRV_SESSION_ID on CORE_USER_SESSION (COUS_APP_SRV_SESSION_ID)";  
	dboList["IND_COUS_CORE_USER_ID"]	="create index IND_COUS_CORE_USER_ID on CORE_USER_SESSION (COUS_USER_SESSION_ID)";  
	dboList["IND_MODULE_INDEX"]			="create index IND_MODULE_INDEX on CORE_USER_SESSION (COUS_MODULE_CODE,COUS_MLID)";  

}






