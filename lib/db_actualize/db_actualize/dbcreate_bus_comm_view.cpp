#include "dbcreate_bus_comm_view.h"

//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_COMM_VIEW::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
								   create table BUS_COMM_VIEW (					\
								   BUSCV_ID INTEGER not null,					\
								   BUSCV_GLOBAL_ID VARCHAR(15) null,			\
								   BUSCV_DAT_LAST_MODIFIED TIMESTAMP not null,	\
								   BUSCV_NAME VARCHAR(255) not null,			\
								   BUSCV_OPEN_ON_STARTUP SMALLINT not null,		\
								   BUSCV_IS_PUBLIC SMALLINT not null,			\
								   BUSCV_TYPE INTEGER not null,					\
								   BUSCV_OWNER_ID INTEGER null,					\
								   BUSCV_SORT_CODE VARCHAR(15) null,			\
								   BUSCV_COMMGRIDVIEW_SORT LONGVARBINARY null,	\
								   constraint BUS_COMM_VIEW_PK primary key (BUSCV_ID))");


	lstSQL.append("\
								   alter table BUS_COMM_VIEW					\
								   add constraint BUS_PERSON_BUS_COMM_VIEW_FK1	\
								   foreign key (								\
								   BUSCV_OWNER_ID)								\
								   references BUS_PERSON (						\
								   BPER_ID) ON DELETE CASCADE");
}

void DbCreate_BUS_COMM_VIEW::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList["IND_BUSCV_OWNER_ID"] = "create index IND_BUSCV_OWNER_ID on BUS_COMM_VIEW (BUSCV_OWNER_ID)";  
	dboList["IND_BUSCV_IS_PUBLIC"] = "create index IND_BUSCV_IS_PUBLIC on BUS_COMM_VIEW (BUSCV_OWNER_ID, BUSCV_IS_PUBLIC)";  
}
