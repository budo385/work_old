#ifndef DBCREATE_F_OBJEKTNUMMERN_H
#define DBCREATE_F_OBJEKTNUMMERN_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_OBJEKTNUMMERN : public DbSqlTableCreation
{
public: 
	DbCreate_F_OBJEKTNUMMERN():DbSqlTableCreation(F_OBJEKTNUMMERN){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_OBJEKTNUMMERN_H
