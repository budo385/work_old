#include "dbcreate_bus_cm_email.h"



//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_CM_EMAIL::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
											create table BUS_CM_EMAIL ( \
											BCME_ID INTEGER not null,\
											BCME_GLOBAL_ID VARCHAR(15) null,\
											BCME_DAT_LAST_MODIFIED TIMESTAMP not null,\
											BCME_CONTACT_ID INTEGER not null,\
											BCME_TYPE_ID INTEGER null,\
											BCME_NAME VARCHAR(100) null,\
											BCME_ADDRESS VARCHAR(200) not null,\
											BCME_DESCRIPTION LONGVARCHAR null,\
											BCME_EXT_ID INTEGER null, \
											BCME_IS_DEFAULT SMALLINT not null,	constraint BUS_CM_EMAIL_PK primary key (BCME_ID) ) ");

	lstSQL.append("\
											alter table BUS_CM_EMAIL\
											add constraint BUS_CONT_EMAIL_FK1 foreign key (\
											BCME_CONTACT_ID)\
											references BUS_CM_CONTACT (\
											BCNT_ID)  \
											ON DELETE CASCADE");


	//if there is one item with given type: can not delete type
	lstSQL.append("\
											alter table BUS_CM_EMAIL\
											add constraint BUS_TYPES_EMAIL_FK1 foreign key (\
											BCME_TYPE_ID)\
											references BUS_CM_TYPES (\
											BCMT_ID)  ");
}



//probably common to all DB's:
void DbCreate_BUS_CM_EMAIL::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
	dboList["IND_BCME_CONTACT_ID"] = "create index IND_BCME_CONTACT_ID on BUS_CM_EMAIL (BCME_CONTACT_ID)";  
	dboList["IND_BCME_TYPE_ID"] = "create index IND_BCME_TYPE_ID on BUS_CM_EMAIL (BCME_TYPE_ID)";  
}


