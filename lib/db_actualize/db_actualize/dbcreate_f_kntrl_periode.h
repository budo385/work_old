#ifndef DBCREATE_F_KNTRL_PERIODE_H
#define DBCREATE_F_KNTRL_PERIODE_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_KNTRL_PERIODE : public DbSqlTableCreation
{
public: 
	DbCreate_F_KNTRL_PERIODE():DbSqlTableCreation(F_KNTRL_PERIODE){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_KNTRL_PERIODE_H
