#include "dbcreate_bus_cal_event.h"


//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_CAL_EVENT::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
								   create table BUS_CAL_EVENT (\
								   BCEV_ID INTEGER not null,\
								   BCEV_GLOBAL_ID VARCHAR(15) null,\
								   BCEV_DAT_LAST_MODIFIED TIMESTAMP not null,\
								   BCEV_DAT_CREATED DATETIME null,\
								   BCEV_TITLE VARCHAR(250) not null,\
								   BCEV_FROM DATETIME null,\
								   BCEV_TO DATETIME null,\
								   BCEV_TEMPLATE_FLAG SMALLINT not null,\
								   BCEV_CATEGORY VARCHAR(250) null,\
								   BCEV_TEMPLATE_ID INTEGER null,\
								   BCEV_DESCRIPTION LONGVARCHAR null,\
								   BCEV_IS_PRIVATE SMALLINT null,\
								   BCEV_IS_PRELIMINARY SMALLINT null,\
								   BCEV_IS_INFORMATION SMALLINT null,\
								   constraint BUS_CAL_EVENT_PK primary key (BCEV_ID) )  ");  

	lstSQL.append("\
								   alter table BUS_CAL_EVENT\
								   add constraint BUS_CAL_EV_CAL_EV_FK1 foreign key (\
								   BCEV_TEMPLATE_ID)\
								   references BUS_CAL_EVENT (\
								   BCEV_ID) ON DELETE SET NULL");	
}



//probably common to all DB's:
void DbCreate_BUS_CAL_EVENT::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
}



