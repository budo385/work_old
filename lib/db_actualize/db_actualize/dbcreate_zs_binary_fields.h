#ifndef DBCREATE_ZS_BINARY_FIELDS_H
#define DBCREATE_ZS_BINARY_FIELDS_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_ZS_BINARY_FIELDS : public DbSqlTableCreation
{
public:
	DbCreate_ZS_BINARY_FIELDS():DbSqlTableCreation(ZS_BINARY_FIELDS){};  //init table
	

private:
	void DefineFields(QStringList &lstSQL);
	void DefineIndexes(SQLDBObjectCreate &dboList);
};
#endif // DBCREATE_ZS_BINARY_FIELDS_H
