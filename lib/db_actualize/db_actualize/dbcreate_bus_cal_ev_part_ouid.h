#ifndef DBCREATE_BUS_CAL_EV_PART_OUID_H
#define DBCREATE_BUS_CAL_EV_PART_OUID_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_BUS_CAL_EV_PART_OUID : public DbSqlTableCreation
{
public:
	DbCreate_BUS_CAL_EV_PART_OUID():DbSqlTableCreation(BUS_CAL_EV_PART_OUID){};  //init table

private:
	//create generic Table def:
	void DefineFields(QStringList &lstSQL);

	//probably common to all DB's:
	void DefineIndexes(SQLDBObjectCreate &dboList);
};

#endif // DBCREATE_BUS_CAL_EV_PART_OUID_H
