#ifndef DBCREATE_F_PROJEKTLISTEN_H
#define DBCREATE_F_PROJEKTLISTEN_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_PROJEKTLISTEN : public DbSqlTableCreation
{
public: 
	DbCreate_F_PROJEKTLISTEN():DbSqlTableCreation(F_PROJEKTLISTEN){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_PROJEKTLISTEN_H
