#ifndef DBCREATE_BUS_CAL_RESOURCE_H
#define DBCREATE_BUS_CAL_RESOURCE_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_BUS_CAL_RESOURCE : public DbSqlTableCreation
{
public:
	DbCreate_BUS_CAL_RESOURCE():DbSqlTableCreation(BUS_CAL_RESOURCE){};  //init table
	

private:
	//create generic Table def:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);

	//probably common to all DB's:
	void DefineIndexes(SQLDBObjectCreate &dboList);
};

#endif // DBCREATE_BUS_CAL_RESOURCE_H
