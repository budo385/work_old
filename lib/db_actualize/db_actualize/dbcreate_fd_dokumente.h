#ifndef DBCREATE_FD_DOKUMENTE_H
#define DBCREATE_FD_DOKUMENTE_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_FD_DOKUMENTE : public DbSqlTableCreation
{
public: 
	DbCreate_FD_DOKUMENTE():DbSqlTableCreation(FD_DOKUMENTE){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // FD_DOKUMENTE_H
