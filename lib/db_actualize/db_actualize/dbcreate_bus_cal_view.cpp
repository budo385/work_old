#include "dbcreate_bus_cal_view.h"

//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_CAL_VIEW::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
								   create table BUS_CAL_VIEW (					\
								   BCALV_ID INTEGER not null,					\
								   BCALV_GLOBAL_ID VARCHAR(15) null,			\
								   BCALV_DAT_LAST_MODIFIED TIMESTAMP not null,	\
								   BCALV_NAME VARCHAR(255) not null,			\
								   BCALV_OPEN_ON_STARTUP SMALLINT not null,		\
								   BCALV_IS_PUBLIC SMALLINT not null,			\
								   BCALV_TYPE INTEGER not null,					\
								   BCALV_OWNER_ID INTEGER not null,				\
								   BCALV_SORT_CODE VARCHAR(15) null,			\
								   BCALV_VALUE_BYTE LONGVARBINARY null,			\
								   BCALV_VIEW_TYPE_ID INTEGER null,				\
								   BCALV_FILTER_BY_NOTYPE SMALLINT null,		\
								   BCALV_FILTER_BY_TYPE SMALLINT null,			\
								   BCALV_SHOW_PRELIMINARY SMALLINT null,		\
								   BCALV_SHOW_INFORMATION SMALLINT null,		\
								   BCALV_TIME_SCALE SMALLINT null,				\
								   BCALV_DATE_RANGE SMALLINT null,				\
								   BCALV_SCROLL_TO_TIME DATETIME null,			\
								   BCALV_MULTIDAY_HEIGHT LONGVARBINARY null,	\
								   BCALV_COLUMN_WIDTH SMALLINT null,			\
								   BCALV_FUI_GEOMETRY LONGVARBINARY null,		\
								   BCALV_FUI_POSITION_X SMALLINT null,			\
								   BCALV_FUI_POSITION_Y SMALLINT null,			\
								   BCALV_SHOW_TASKS SMALLINT null,				\
								   BCALV_SECTIONS SMALLINT null,				\
								   BCALV_OVERVIEWLEFTCOLWIDTH SMALLINT null,	\
								   constraint BUS_CAL_VIEW_PK primary key (BCALV_ID))");

	lstSQL.append("\
								   alter table BUS_CAL_VIEW						\
								   add constraint BUS_PERSON_BUS_CAL_VIEW_FK1	\
								   foreign key (								\
								   BCALV_OWNER_ID)								\
								   references BUS_PERSON (						\
								   BPER_ID) ON DELETE CASCADE");

	lstSQL.append("\
								   alter table BUS_CAL_VIEW\
								   add constraint BUS_CAL_VIEW_TYPE_PART_FK1 foreign key (\
								   BCALV_VIEW_TYPE_ID)\
								   references BUS_CM_TYPES (\
								   BCMT_ID)	");	


	//create indexes

	//Delete column.
	//m_lstDeletedCols.append("BCALV_VALUE_BYTE");

	SQLDBObjectCreate dboList;
	DefineIndexes(dboList);
	m_Indexes[DBTYPE_ODBC]=dboList;
	dboList.clear();

	//create triggers
	m_Triggers[DBTYPE_ODBC]=dboList;
}

void DbCreate_BUS_CAL_VIEW::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList["IND_BCALV_OWNER_ID"] = "create index IND_BCALV_OWNER_ID on BUS_CAL_VIEW (BCALV_OWNER_ID)";  
	dboList["IND_OWNER_ID_IS_PUBLIC"] = "create index IND_OWNER_ID_IS_PUBLIC on BUS_CAL_VIEW (BCALV_OWNER_ID, BCALV_IS_PUBLIC)";  
}
