#ifndef DBCREATE_F_PREASSIGN_H
#define DBCREATE_F_PREASSIGN_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_PREASSIGN : public DbSqlTableCreation
{
public: 
	DbCreate_F_PREASSIGN():DbSqlTableCreation(F_PREASSIGN){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_PREASSIGN_H
