#ifndef DBCREATE_F_BATCH_JOBS_H
#define DBCREATE_F_BATCH_JOBS_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_BATCH_JOBS : public DbSqlTableCreation
{
public: 
	DbCreate_F_BATCH_JOBS():DbSqlTableCreation(F_BATCH_JOBS){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_BATCH_JOBS_H
