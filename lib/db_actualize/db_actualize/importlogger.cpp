#include "importlogger.h"
#include <QFile>
#include <QCoreApplication>

ImportLogger::ImportLogger()
{
	m_nCurrTableRec=0;
	m_nTotalRec=0;
	m_bCurrImportIsData=true;
	m_nCurrentTableIdx=0;
	m_nCurrTotalRec=0;
	m_nTableCnt=0;
}

ImportLogger::~ImportLogger()
{

}

void ImportLogger::WriteStep(QString strStep,int nCurrentTableIdx)
{
	m_strCurrStep=strStep;
	if (nCurrentTableIdx)
		m_nCurrentTableIdx=nCurrentTableIdx;
	FlushLog();
}
void ImportLogger::FlushLog()
{
	QByteArray data;
	data.append(QString::number(m_nTotalRec).toLatin1()); //total records
	data.append("\n");
	data.append(QString::number(m_nTableCnt).toLatin1()); //total tables
	data.append("\n");
	data.append(m_strCurrStep.toLatin1());
	data.append("\n");
	data.append(QString::number(m_nCurrentTableIdx).toLatin1()); //current table idx =0 
	data.append("\n");
	data.append(QString::number(m_nCurrTotalRec).toLatin1());//total records...
	data.append("\n");

	if (m_bCurrImportIsData)
		data.append(QString::number(m_nCurrTableRec).toLatin1()+"/"+QString::number(m_hshDataCnt.value(m_strCurrTable,0)).toLatin1());
	else
		data.append(QString::number(m_nCurrTableRec).toLatin1()+"/"+QString::number(m_hshBinCnt.value(m_strCurrTable,0)).toLatin1());
	data.append("\n");
	QFile status_file(m_strLogPath);
	if(status_file.open(QIODevice::WriteOnly))
	{
		status_file.write(data);
		status_file.close();
	}
}

void ImportLogger::WriteImportProgress(QString strTable, int nCurrentRec, bool bIsData, bool bEnd)
{
	if(!bEnd)
		m_nCurrTotalRec++;

	m_bCurrImportIsData=bIsData;

	if(bEnd || m_strCurrTable!=strTable || (m_nCurrTableRec+100)<nCurrentRec) //write every 100 recs or when table is changed
	{
		m_strCurrTable=strTable;
		m_nCurrTableRec=nCurrentRec;
		FlushLog();
	}
}

void ImportLogger::WriteErrLog(QString strErrName,QByteArray byteErr)
{
	QFile status_file(QCoreApplication::applicationDirPath()+"/"+strErrName);
	if(status_file.open(QIODevice::WriteOnly))
	{
		status_file.write(byteErr);
		status_file.close();
	}
}

void ImportLogger::RecalcTotal()
{
	{
		QHashIterator<QString,int> i(m_hshDataCnt);
		while (i.hasNext()) 
		{
			i.next();
			m_nTotalRec+=i.value();
		}
	}

	{
		QHashIterator<QString,int> i(m_hshBinCnt);
		while (i.hasNext()) 
		{
			i.next();
			m_nTotalRec+=i.value();
		}
	}


}