#include "dbcreate_f_proj_budgetkorrektur.h"


//--------------------------------------------------
// ODBC - CREATE
//--------------------------------------------------
void DbCreate_F_PROJ_BUDGETKORREKTUR::DefineFields(QStringList &lstSQL) 
{
	//Table schema statements for ODBC.
	lstSQL.append(" 	CREATE TABLE F_PROJ_BUDGETKORREKTUR (\
										EBK_SEQUENCE INTEGER  NOT NULL,\
										EBK_PERS_NR VARCHAR(23)  NOT NULL,EBK_PROJ_NR VARCHAR(56)  NOT NULL,\
										EBK_GRUND_CODE VARCHAR(23)  NOT NULL,EBK_GRUND_TEXT VARCHAR(1101)  NOT NULL,\
										EBK_CI_PM VARCHAR(2)  NOT NULL,EBK_DATUM DATETIME ,\
										EBK_KORR_AUFW_A DECIMAL(15,2) NOT NULL,EBK_KORR_AUFW_NK DECIMAL(15,2) NOT NULL,\
										EBK_KORR_AUFW_FK DECIMAL(15,2) NOT NULL,EBK_KORR_AUFW_FL DECIMAL(15,2) NOT NULL,\
										EBK_KORR_VERR_A DECIMAL(15,2) NOT NULL,EBK_KORR_VERR_NK DECIMAL(15,2) NOT NULL,\
										EBK_KORR_VERR_FK DECIMAL(15,2) NOT NULL,EBK_KORR_VERR_FL DECIMAL(15,2) NOT NULL,\
										EBK_KORR_VERR_BT DECIMAL(15,2) NOT NULL,EBK_GLOBAL_ID VARCHAR(14)  NOT NULL,\
										EBK_DAT_AENDERUNG DATETIME ,EBK_KORR_ZEIT DECIMAL(15,2) NOT NULL,\
										EBK_DAT_EROEFFNUNG DATETIME ,EBK_MODIFIED_BY VARCHAR(36)  NOT NULL,\
										EBK_VALID_SINCE DATETIME ,EBK_EXPIRE_DATE DATETIME ,\
										EBK_KORREKTURTYP DECIMAL(3) NOT NULL,\
										constraint F_PROJ_BUDGETKORREKTUR_PK primary key (EBK_SEQUENCE))	");

}

void DbCreate_F_PROJ_BUDGETKORREKTUR::DefineIndexes(SQLDBObjectCreate &dboList) 
{
		//add index definition by its name (important).
		dboList["IND_EBK_PERS_NR"] = "CREATE INDEX IND_EBK_PERS_NR ON F_PROJ_BUDGETKORREKTUR (EBK_PERS_NR)";
		dboList["IND_EBK_PROJ_NR"] = "CREATE INDEX IND_EBK_PROJ_NR ON F_PROJ_BUDGETKORREKTUR (EBK_PROJ_NR)";
		dboList["IND_EBK_CI_PROJ_NR_DATE"] = "CREATE INDEX IND_EBK_CI_PROJ_NR_DATE ON F_PROJ_BUDGETKORREKTUR (EBK_PROJ_NR,EBK_DATUM)";
		dboList["IND_EBK_CI_PM"] = "CREATE INDEX IND_EBK_CI_PM ON F_PROJ_BUDGETKORREKTUR (EBK_PROJ_NR,EBK_PERS_NR)";
		dboList["IND_EBK_DATUM"] = "CREATE INDEX IND_EBK_DATUM ON F_PROJ_BUDGETKORREKTUR (EBK_DATUM)";
		dboList["IND_EBK_GLOBAL_ID"] = "CREATE INDEX IND_EBK_GLOBAL_ID ON F_PROJ_BUDGETKORREKTUR (EBK_GLOBAL_ID)";
		dboList["IND_EBK_DAT_AENDERUNG"] = "CREATE INDEX IND_EBK_DAT_AENDERUNG ON F_PROJ_BUDGETKORREKTUR (EBK_DAT_AENDERUNG)";

}

void DbCreate_F_PROJ_BUDGETKORREKTUR::DefineViews(SQLDBObjectCreate &dboList) 
{


	dboList["VW_F_PROJ_BUDGETKORREKTUR"] = "CREATE VIEW VW_F_PROJ_BUDGETKORREKTUR AS SELECT EBK_SEQUENCE,EBK_PERS_NR,EBK_PROJ_NR,EBK_GRUND_CODE ,EBK_GRUND_TEXT,EBK_CI_PM,EBK_DATUM,EBK_KORR_AUFW_A ,EBK_KORR_AUFW_NK,EBK_KORR_AUFW_FK,EBK_KORR_AUFW_FL,EBK_KORR_VERR_A ,EBK_KORR_VERR_NK,EBK_KORR_VERR_FK,EBK_KORR_VERR_FL,EBK_KORR_VERR_BT ,EBK_GLOBAL_ID,EBK_DAT_AENDERUNG,EBK_KORR_ZEIT,EBK_DAT_EROEFFNUNG ,EBK_MODIFIED_BY,EBK_VALID_SINCE,EBK_EXPIRE_DATE,EBK_KORREKTURTYP FROM F_PROJ_BUDGETKORREKTUR";

	dboList["VWS_F_PROJ_BUDGETKORREKTUR"] = "CREATE VIEW VWS_F_PROJ_BUDGETKORREKTUR AS SELECT EBK_SEQUENCE,EBK_PERS_NR,EBK_PROJ_NR,EBK_GRUND_CODE ,EBK_GRUND_TEXT,EBK_CI_PM,EBK_DATUM,EBK_KORR_AUFW_A ,EBK_KORR_AUFW_NK,EBK_KORR_AUFW_FK,EBK_KORR_AUFW_FL,EBK_KORR_VERR_A ,EBK_KORR_VERR_NK,EBK_KORR_VERR_FK,EBK_KORR_VERR_FL,EBK_KORR_VERR_BT ,EBK_GLOBAL_ID,EBK_DAT_AENDERUNG,EBK_KORR_ZEIT,EBK_DAT_EROEFFNUNG ,EBK_MODIFIED_BY,EBK_VALID_SINCE,EBK_EXPIRE_DATE,EBK_KORREKTURTYP FROM F_PROJ_BUDGETKORREKTUR";
}

void DbCreate_F_PROJ_BUDGETKORREKTUR::DefineTriggers(SQLDBObjectCreate &dboList) 
{
	if(m_DbType==DBTYPE_FIREBIRD)
	{

		dboList["TR_F_PROJ_BUDGETKORREKTUR"] = "CREATE TRIGGER TR_F_PROJ_BUDGETKORREKTUR FOR F_PROJ_BUDGETKORREKTUR BEFORE INSERT OR UPDATE AS BEGIN \
IF(NEW.EBK_KORR_AUFW_A IS NULL) THEN NEW.EBK_KORR_AUFW_A=0;\
IF(NEW.EBK_KORR_AUFW_NK IS NULL) THEN NEW.EBK_KORR_AUFW_NK=0;\
IF(NEW.EBK_KORR_AUFW_FK IS NULL) THEN NEW.EBK_KORR_AUFW_FK=0;\
IF(NEW.EBK_KORR_AUFW_FL IS NULL) THEN NEW.EBK_KORR_AUFW_FL=0;\
IF(NEW.EBK_KORR_VERR_A IS NULL) THEN NEW.EBK_KORR_VERR_A=0;\
IF(NEW.EBK_KORR_VERR_NK IS NULL) THEN NEW.EBK_KORR_VERR_NK=0;\
IF(NEW.EBK_KORR_VERR_FK IS NULL) THEN NEW.EBK_KORR_VERR_FK=0;\
IF(NEW.EBK_KORR_VERR_FL IS NULL) THEN NEW.EBK_KORR_VERR_FL=0;\
IF(NEW.EBK_KORR_VERR_BT IS NULL) THEN NEW.EBK_KORR_VERR_BT=0;\
IF(NEW.EBK_KORR_ZEIT IS NULL) THEN NEW.EBK_KORR_ZEIT=0;\
IF(NEW.EBK_KORREKTURTYP IS NULL) THEN NEW.EBK_KORREKTURTYP=0;\
IF(NEW.EBK_PERS_NR IS NULL) THEN NEW.EBK_PERS_NR='';\
IF(NEW.EBK_PROJ_NR IS NULL) THEN NEW.EBK_PROJ_NR='';\
IF(NEW.EBK_GRUND_CODE IS NULL) THEN NEW.EBK_GRUND_CODE='';\
IF(NEW.EBK_GRUND_TEXT IS NULL) THEN NEW.EBK_GRUND_TEXT='';\
IF(NEW.EBK_CI_PM IS NULL) THEN NEW.EBK_CI_PM='';\
IF(NEW.EBK_GLOBAL_ID IS NULL) THEN NEW.EBK_GLOBAL_ID='';\
IF(NEW.EBK_MODIFIED_BY IS NULL) THEN NEW.EBK_MODIFIED_BY='';\
END;";
	}
}
