#ifdef _DB_ACTUALIZE_DLL
#define EXPORT_API extern "C" __declspec(dllexport)
#else
#define EXPORT_API
#endif


//PUBLIC DLL API's:
EXPORT_API bool InitializeDatabaseConnection(const char **ppszDatabaseParams,const char *szBackupPath,bool bShutDownDatabase,char *szErrorString);
//EXPORT_API bool OpenDatabaseConnection(const char **ppszDatabaseParams,const char *szBackupPath,char *szErrorString);
EXPORT_API bool CloseDatabaseConnection(char *szErrorString);
EXPORT_API bool OrganizeDatabase(char *szErrorString, bool bRebuildTables=false);
EXPORT_API bool CopyDatabase(char *szErrorString, const char **ppszDatabaseParamsTarget,bool bDestroyPrevious=false);
EXPORT_API bool BackupDatabase(char *szErrorString,const char *szBackupPath,char *szBcpFileNameCreated=0);
EXPORT_API bool RestoreDatabase(const char *szBackupFilePath,char *szErrorString);
EXPORT_API int	GetDatabaseCurrentVersion(char *szErrorString);
EXPORT_API int	GetDatabaseCurrentVersion_SPC(char *szErrorString);
EXPORT_API bool CheckForDemoDatabase(char *szErrorString, bool bEnableDemo, int nMaxSizeKb, int nWarnSizeKb); 
EXPORT_API bool RecalcDemoDatabaseSize(char *szErrorString, int *nCurrSizeKb); 
EXPORT_API bool CreateReadOnlyUserForODBCAccess(char *szErrorString); 
EXPORT_API bool ImportDataFromOracleExport(char *szErrorString,const char *strDataFile, int *nProcessedInsertStatements, int *nProcessedSequenceUpdateStatements);
EXPORT_API bool ImportDataFromOmnisExport(char *szErrorString,const char *strDataFile, int *nProcessedInsertStatements,bool bDropExisting=false);




