#ifndef DBCREATE_F_TAETIGKEITEN_H
#define DBCREATE_F_TAETIGKEITEN_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_TAETIGKEITEN : public DbSqlTableCreation
{
public: 
	DbCreate_F_TAETIGKEITEN():DbSqlTableCreation(F_TAETIGKEITEN){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_TAETIGKEITEN_H
