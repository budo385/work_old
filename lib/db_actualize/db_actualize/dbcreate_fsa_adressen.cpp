#include "dbcreate_fsa_adressen.h"


//--------------------------------------------------
// ODBC - CREATE
//--------------------------------------------------
void DbCreate_FSA_ADRESSEN::DefineFields(QStringList &lstSQL) 
{
	//Table schema statements for ODBC.
	lstSQL.append(" 	CREATE TABLE FSA_ADRESSEN (\
										EAL_SEQUENCE INTEGER  NOT NULL,\
										EAL_FIRMA VARCHAR(89)  NOT NULL,EAL_NAME VARCHAR(89)  NOT NULL,\
										EAL_RUFNAME VARCHAR(45)  NOT NULL,EAL_ABTEILUNG VARCHAR(89)  NOT NULL,\
										EAL_STELLUNG VARCHAR(89)  NOT NULL,EAL_ASSISTENT VARCHAR(89)  NOT NULL,\
										EAL_PLZ VARCHAR(12)  NOT NULL,EAL_ORT VARCHAR(89)  NOT NULL,\
										EAL_BANKVER1 VARCHAR(166)  NOT NULL,EAL_BANKVERB2 VARCHAR(166)  NOT NULL,\
										EAL_BANKVERB3 VARCHAR(166)  NOT NULL,EAL_POSTVERB VARCHAR(166)  NOT NULL,\
										EAL_BEMERKUNGEN VARCHAR(8000)  NOT NULL,EAL_KREDITLIMIT DECIMAL(15,2) NOT NULL,\
										EAL_TEL1 VARCHAR(23)  NOT NULL,EAL_FAX1 VARCHAR(23)  NOT NULL,\
										EAL_TEL2 VARCHAR(23)  NOT NULL,EAL_TELEX VARCHAR(89)  NOT NULL,\
										EAL_FAX2 VARCHAR(23)  NOT NULL,EAL_KONTAKTPER1 VARCHAR(89)  NOT NULL,\
										EAL_KONTAKTPER2 VARCHAR(89)  NOT NULL,EAL_UMSATZ_KRED DECIMAL(15,2) NOT NULL,\
										EAL_OFFEN_KRED DECIMAL(15,2) NOT NULL,EAL_LIEEFERUNG VARCHAR(3)  NOT NULL,\
										EAL_LAND VARCHAR(12)  NOT NULL,EAL_SUCHE VARCHAR(89)  NOT NULL,\
										EAL_USER_B1 VARCHAR(3)  NOT NULL,EAL_USER_B2 VARCHAR(3)  NOT NULL,\
										EAL_USER_B3 VARCHAR(3)  NOT NULL,EAL_USER_B4 VARCHAR(3)  NOT NULL,\
										EAL_USER_B5 VARCHAR(3)  NOT NULL,EAL_USER_B6 VARCHAR(3)  NOT NULL,\
										EAL_ANREDE VARCHAR(67)  NOT NULL,EAL_KUNDENRABAT DECIMAL(15,2) NOT NULL,\
										EAL_KUNDENGRUPP VARCHAR(89)  NOT NULL,EAL_WUSTCODE DECIMAL(15,1) NOT NULL,\
										EAL_SKONTO DECIMAL(15,2) NOT NULL,EAL_ABT_KONTAK1 VARCHAR(89)  NOT NULL,\
										EAL_ABT_KONTAK2 VARCHAR(89)  NOT NULL,EAL_SPRACHE VARCHAR(23)  NOT NULL,\
										EAL_UMSATZ_DEB DECIMAL(15,2) NOT NULL,EAL_SOLL_DEB DECIMAL(15,2) NOT NULL,\
										EAL_DEBI_KONTO VARCHAR(23)  NOT NULL,EAL_KREDI_KONTO VARCHAR(23)  NOT NULL,\
										EAL_MAHNSTATUS DECIMAL(15) NOT NULL,EAL_LETZE_BEST DATETIME ,\
										EAL_LETZE_LIEF DATETIME ,EAL_ANREDE_KTK1 VARCHAR(89)  NOT NULL,\
										EAL_ANREDE_KTK2 VARCHAR(89)  NOT NULL,EAL_STUCKRAB_JN VARCHAR(3)  NOT NULL,\
										EAL_USER_C1 VARCHAR(111)  NOT NULL,EAL_USER_C2 VARCHAR(111)  NOT NULL,\
										EAL_USER_C3 VARCHAR(111)  NOT NULL,EAL_USER_C4 VARCHAR(111)  NOT NULL,\
										EAL_USER_C5 VARCHAR(111)  NOT NULL,EAL_USER_N1 DECIMAL(15,2) NOT NULL,\
										EAL_USER_N2 DECIMAL(15,2) NOT NULL,EAL_USER_N3 DECIMAL(15,2) NOT NULL,\
										EAL_USER_D1 DATETIME ,EAL_USER_D2 DATETIME ,\
										EAL_USER_D3 DATETIME ,EAL_USER_C6 VARCHAR(111)  NOT NULL,\
										EAL_USER_C7 VARCHAR(111)  NOT NULL,EAL_USER_C8 VARCHAR(111)  NOT NULL,\
										EAL_USER_C9 VARCHAR(111)  NOT NULL,EAL_USER_C0 VARCHAR(111)  NOT NULL,\
										EAL_USER_N4 DECIMAL(15,2) NOT NULL,EAL_USER_N5 DECIMAL(15,2) NOT NULL,\
										EAL_USER_N6 DECIMAL(15,2) NOT NULL,EAL_USER_D4 DATETIME ,\
										EAL_USER_D5 DATETIME ,EAL_USER_D6 DATETIME ,\
										EAL_DEBITOR_NR VARCHAR(45)  NOT NULL,EAL_KREDITOR_NR VARCHAR(23)  NOT NULL,\
										EAL_KREDITOR_JN VARCHAR(3)  NOT NULL,EAL_DEBITOR_JN VARCHAR(3)  NOT NULL,\
										EAL_RECH_ADR VARCHAR(1101)  NOT NULL,EAL_LIEFER_ADR VARCHAR(1101)  NOT NULL,\
										EAL_KONT_ADR VARCHAR(1101)  NOT NULL,EAL_NOTE VARCHAR(2)  NOT NULL,\
										EAL_EROEFFNUNG DATETIME ,EAL_BILD DECIMAL(9)  NOT NULL,\
										EAL_KUNDEN_NR VARCHAR(89)  NOT NULL,EAL_GESCHLECHT DECIMAL(3) NOT NULL,\
										EAL_KURZZEICHEN VARCHAR(4)  NOT NULL,EAL_PREISFAKT_CH DECIMAL(15,2) NOT NULL,\
										EAL_PREISFAKT_AUSL DECIMAL(15,2) NOT NULL,EAL_ZAHLFRIST DECIMAL(10) NOT NULL,\
										EAL_OLD_SEQ DECIMAL(10) NOT NULL,EAL_KURZANREDE VARCHAR(34)  NOT NULL,\
										EAL_1_ADR_ZEILE VARCHAR(111)  NOT NULL,EAL_2_ADR_ZEILE VARCHAR(111)  NOT NULL,\
										EAL_STRASSE VARCHAR(111)  NOT NULL,EAL_POSTFACH VARCHAR(111)  NOT NULL,\
										EAL_PLZ_POSTFACH VARCHAR(23)  NOT NULL,EAL_GLOBAL_ID VARCHAR(14)  NOT NULL,\
										EAL_DAT_AENDERUNG DATETIME ,EAL_DAT_EROEFFNUNG DATETIME ,\
										EAL_MODIFIED_BY VARCHAR(36)  NOT NULL,EAL_VALID_SINCE DATETIME ,\
										EAL_EXPIRE_DATE DATETIME ,\
										constraint FSA_ADRESSEN_PK primary key (EAL_SEQUENCE))	");

}

void DbCreate_FSA_ADRESSEN::DefineIndexes(SQLDBObjectCreate &dboList) 
{
		//add index definition by its name (important).
		dboList["IND_EAL_FIRMA"] = "CREATE INDEX IND_EAL_FIRMA ON FSA_ADRESSEN (EAL_FIRMA)";
		dboList["IND_EAL_NAME"] = "CREATE INDEX IND_EAL_NAME ON FSA_ADRESSEN (EAL_NAME)";
		dboList["IND_EAL_ORT"] = "CREATE INDEX IND_EAL_ORT ON FSA_ADRESSEN (EAL_ORT)";
		dboList["IND_EAL_LAND"] = "CREATE INDEX IND_EAL_LAND ON FSA_ADRESSEN (EAL_LAND)";
		dboList["IND_EAL_SUCHE"] = "CREATE INDEX IND_EAL_SUCHE ON FSA_ADRESSEN (EAL_SUCHE)";
		dboList["IND_EAL_USER_B1"] = "CREATE INDEX IND_EAL_USER_B1 ON FSA_ADRESSEN (EAL_USER_B1)";
		dboList["IND_EAL_USER_C1"] = "CREATE INDEX IND_EAL_USER_C1 ON FSA_ADRESSEN (EAL_USER_C1)";
		dboList["IND_EAL_USER_D1"] = "CREATE INDEX IND_EAL_USER_D1 ON FSA_ADRESSEN (EAL_USER_D1)";
		dboList["IND_EAL_DEBITOR_NR"] = "CREATE INDEX IND_EAL_DEBITOR_NR ON FSA_ADRESSEN (EAL_DEBITOR_NR)";
		dboList["IND_EAL_KREDITOR_NR"] = "CREATE INDEX IND_EAL_KREDITOR_NR ON FSA_ADRESSEN (EAL_KREDITOR_NR)";
		dboList["IND_EAL_NOTE"] = "CREATE INDEX IND_EAL_NOTE ON FSA_ADRESSEN (EAL_NOTE)";
		dboList["IND_EAL_KUNDEN_NR"] = "CREATE INDEX IND_EAL_KUNDEN_NR ON FSA_ADRESSEN (EAL_KUNDEN_NR)";
		dboList["IND_EAL_DEBITOR_JN"] = "CREATE INDEX IND_EAL_DEBITOR_JN ON FSA_ADRESSEN (EAL_DEBITOR_JN)";
		dboList["IND_EAL_KREDITOR_JN"] = "CREATE INDEX IND_EAL_KREDITOR_JN ON FSA_ADRESSEN (EAL_KREDITOR_JN)";
		dboList["IND_EAL_KURZZEICHEN"] = "CREATE INDEX IND_EAL_KURZZEICHEN ON FSA_ADRESSEN (EAL_KURZZEICHEN)";
		dboList["IND_EAL_UMSATZ_KRED"] = "CREATE INDEX IND_EAL_UMSATZ_KRED ON FSA_ADRESSEN (EAL_UMSATZ_KRED)";
		dboList["IND_EAL_USER_N1"] = "CREATE INDEX IND_EAL_USER_N1 ON FSA_ADRESSEN (EAL_USER_N1)";
		dboList["IND_EAL_OLD_SEQ"] = "CREATE INDEX IND_EAL_OLD_SEQ ON FSA_ADRESSEN (EAL_OLD_SEQ)";
		dboList["IND_EAL_PLZ"] = "CREATE INDEX IND_EAL_PLZ ON FSA_ADRESSEN (EAL_PLZ)";
		dboList["IND_EAL_GLOBAL_ID"] = "CREATE INDEX IND_EAL_GLOBAL_ID ON FSA_ADRESSEN (EAL_GLOBAL_ID)";
		dboList["IND_EAL_DAT_AENDERUNG"] = "CREATE INDEX IND_EAL_DAT_AENDERUNG ON FSA_ADRESSEN (EAL_DAT_AENDERUNG)";
		dboList["IND_EAL_EXPIRE_DATE"] = "CREATE INDEX IND_EAL_EXPIRE_DATE ON FSA_ADRESSEN (EAL_EXPIRE_DATE)";
		dboList["IND_EAL_VALID_SINCE"] = "CREATE INDEX IND_EAL_VALID_SINCE ON FSA_ADRESSEN (EAL_VALID_SINCE)";

}

void DbCreate_FSA_ADRESSEN::DefineViews(SQLDBObjectCreate &dboList) 
{


	dboList["VW_FSA_ADRESSEN"] = "CREATE VIEW VW_FSA_ADRESSEN AS SELECT EAL_SEQUENCE,EAL_FIRMA,EAL_NAME,EAL_RUFNAME ,EAL_ABTEILUNG,EAL_STELLUNG,EAL_ASSISTENT,EAL_PLZ ,EAL_ORT,EAL_BANKVER1,EAL_BANKVERB2,EAL_BANKVERB3 ,EAL_POSTVERB,EAL_BEMERKUNGEN,EAL_KREDITLIMIT,EAL_TEL1 ,EAL_FAX1,EAL_TEL2,EAL_TELEX,EAL_FAX2 ,EAL_KONTAKTPER1,EAL_KONTAKTPER2,EAL_UMSATZ_KRED,EAL_OFFEN_KRED ,EAL_LIEEFERUNG,EAL_LAND,EAL_SUCHE,EAL_USER_B1 ,EAL_USER_B2,EAL_USER_B3,EAL_USER_B4,EAL_USER_B5 ,EAL_USER_B6,EAL_ANREDE,EAL_KUNDENRABAT,EAL_KUNDENGRUPP ,EAL_WUSTCODE,EAL_SKONTO,EAL_ABT_KONTAK1,EAL_ABT_KONTAK2 ,EAL_SPRACHE,EAL_UMSATZ_DEB,EAL_SOLL_DEB,EAL_DEBI_KONTO ,EAL_KREDI_KONTO,EAL_MAHNSTATUS,EAL_LETZE_BEST,EAL_LETZE_LIEF ,EAL_ANREDE_KTK1,EAL_ANREDE_KTK2,EAL_STUCKRAB_JN,EAL_USER_C1 ,EAL_USER_C2,EAL_USER_C3,EAL_USER_C4,EAL_USER_C5 ,EAL_USER_N1,EAL_USER_N2,EAL_USER_N3,EAL_USER_D1 ,EAL_USER_D2,EAL_USER_D3,EAL_USER_C6,EAL_USER_C7 ,EAL_USER_C8,EAL_USER_C9,EAL_USER_C0,EAL_USER_N4 ,EAL_USER_N5,EAL_USER_N6,EAL_USER_D4,EAL_USER_D5 ,EAL_USER_D6,EAL_DEBITOR_NR,EAL_KREDITOR_NR,EAL_KREDITOR_JN ,EAL_DEBITOR_JN,EAL_RECH_ADR,EAL_LIEFER_ADR,EAL_KONT_ADR ,EAL_NOTE,EAL_EROEFFNUNG,' 0 ' \"EAL_BILD\",\
							EAL_KUNDEN_NR ,EAL_GESCHLECHT,EAL_KURZZEICHEN,EAL_PREISFAKT_CH,EAL_PREISFAKT_AUSL ,EAL_ZAHLFRIST,EAL_OLD_SEQ,EAL_KURZANREDE,EAL_1_ADR_ZEILE ,EAL_2_ADR_ZEILE,EAL_STRASSE,EAL_POSTFACH,EAL_PLZ_POSTFACH ,EAL_GLOBAL_ID,EAL_DAT_AENDERUNG,EAL_DAT_EROEFFNUNG,EAL_MODIFIED_BY ,EAL_VALID_SINCE,EAL_EXPIRE_DATE FROM FSA_ADRESSEN";

	dboList["VWS_FSA_ADRESSEN"] = "CREATE VIEW VWS_FSA_ADRESSEN AS SELECT EAL_SEQUENCE,EAL_FIRMA,EAL_NAME,EAL_RUFNAME ,EAL_ABTEILUNG,EAL_STELLUNG,EAL_ASSISTENT,EAL_PLZ ,EAL_ORT,EAL_BANKVER1,EAL_BANKVERB2,EAL_BANKVERB3 ,EAL_POSTVERB,EAL_BEMERKUNGEN,EAL_KREDITLIMIT,EAL_TEL1 ,EAL_FAX1,EAL_TEL2,EAL_TELEX,EAL_FAX2 ,EAL_KONTAKTPER1,EAL_KONTAKTPER2,EAL_UMSATZ_KRED,EAL_OFFEN_KRED ,EAL_LIEEFERUNG,EAL_LAND,EAL_SUCHE,EAL_USER_B1 ,EAL_USER_B2,EAL_USER_B3,EAL_USER_B4,EAL_USER_B5 ,EAL_USER_B6,EAL_ANREDE,EAL_KUNDENRABAT,EAL_KUNDENGRUPP ,EAL_WUSTCODE,EAL_SKONTO,EAL_ABT_KONTAK1,EAL_ABT_KONTAK2 ,EAL_SPRACHE,EAL_UMSATZ_DEB,EAL_SOLL_DEB,EAL_DEBI_KONTO ,EAL_KREDI_KONTO,EAL_MAHNSTATUS,EAL_LETZE_BEST,EAL_LETZE_LIEF ,EAL_ANREDE_KTK1,EAL_ANREDE_KTK2,EAL_STUCKRAB_JN,EAL_USER_C1 ,EAL_USER_C2,EAL_USER_C3,EAL_USER_C4,EAL_USER_C5 ,EAL_USER_N1,EAL_USER_N2,EAL_USER_N3,EAL_USER_D1 ,EAL_USER_D2,EAL_USER_D3,EAL_USER_C6,EAL_USER_C7 ,EAL_USER_C8,EAL_USER_C9,EAL_USER_C0,EAL_USER_N4 ,EAL_USER_N5,EAL_USER_N6,EAL_USER_D4,EAL_USER_D5 ,EAL_USER_D6,EAL_DEBITOR_NR,EAL_KREDITOR_NR,EAL_KREDITOR_JN ,EAL_DEBITOR_JN,EAL_RECH_ADR,EAL_LIEFER_ADR,EAL_KONT_ADR ,EAL_NOTE,EAL_EROEFFNUNG,' 0 ' \"EAL_BILD\",\
							EAL_KUNDEN_NR ,EAL_GESCHLECHT,EAL_KURZZEICHEN,EAL_PREISFAKT_CH,EAL_PREISFAKT_AUSL ,EAL_ZAHLFRIST,EAL_OLD_SEQ,EAL_KURZANREDE,EAL_1_ADR_ZEILE ,EAL_2_ADR_ZEILE,EAL_STRASSE,EAL_POSTFACH,EAL_PLZ_POSTFACH ,EAL_GLOBAL_ID,EAL_DAT_AENDERUNG,EAL_DAT_EROEFFNUNG,EAL_MODIFIED_BY ,EAL_VALID_SINCE,EAL_EXPIRE_DATE FROM FSA_ADRESSEN";
}

void DbCreate_FSA_ADRESSEN::DefineTriggers(SQLDBObjectCreate &dboList) 
{
	if(m_DbType==DBTYPE_FIREBIRD)
	{

		dboList["TR_FSA_ADRESSEN"] = "CREATE TRIGGER TR_FSA_ADRESSEN FOR FSA_ADRESSEN BEFORE INSERT OR UPDATE AS BEGIN \
IF(NEW.EAL_KREDITLIMIT IS NULL) THEN NEW.EAL_KREDITLIMIT=0;\
IF(NEW.EAL_UMSATZ_KRED IS NULL) THEN NEW.EAL_UMSATZ_KRED=0;\
IF(NEW.EAL_OFFEN_KRED IS NULL) THEN NEW.EAL_OFFEN_KRED=0;\
IF(NEW.EAL_KUNDENRABAT IS NULL) THEN NEW.EAL_KUNDENRABAT=0;\
IF(NEW.EAL_WUSTCODE IS NULL) THEN NEW.EAL_WUSTCODE=0;\
IF(NEW.EAL_SKONTO IS NULL) THEN NEW.EAL_SKONTO=0;\
IF(NEW.EAL_UMSATZ_DEB IS NULL) THEN NEW.EAL_UMSATZ_DEB=0;\
IF(NEW.EAL_SOLL_DEB IS NULL) THEN NEW.EAL_SOLL_DEB=0;\
IF(NEW.EAL_MAHNSTATUS IS NULL) THEN NEW.EAL_MAHNSTATUS=0;\
IF(NEW.EAL_USER_N1 IS NULL) THEN NEW.EAL_USER_N1=0;\
IF(NEW.EAL_USER_N2 IS NULL) THEN NEW.EAL_USER_N2=0;\
IF(NEW.EAL_USER_N3 IS NULL) THEN NEW.EAL_USER_N3=0;\
IF(NEW.EAL_USER_N4 IS NULL) THEN NEW.EAL_USER_N4=0;\
IF(NEW.EAL_USER_N5 IS NULL) THEN NEW.EAL_USER_N5=0;\
IF(NEW.EAL_USER_N6 IS NULL) THEN NEW.EAL_USER_N6=0;\
IF(NEW.EAL_BILD IS NULL) THEN NEW.EAL_BILD=0;\
IF(NEW.EAL_GESCHLECHT IS NULL) THEN NEW.EAL_GESCHLECHT=0;\
IF(NEW.EAL_PREISFAKT_CH IS NULL) THEN NEW.EAL_PREISFAKT_CH=0;\
IF(NEW.EAL_PREISFAKT_AUSL IS NULL) THEN NEW.EAL_PREISFAKT_AUSL=0;\
IF(NEW.EAL_ZAHLFRIST IS NULL) THEN NEW.EAL_ZAHLFRIST=0;\
IF(NEW.EAL_OLD_SEQ IS NULL) THEN NEW.EAL_OLD_SEQ=0;\
IF(NEW.EAL_FIRMA IS NULL) THEN NEW.EAL_FIRMA='';\
IF(NEW.EAL_NAME IS NULL) THEN NEW.EAL_NAME='';\
IF(NEW.EAL_RUFNAME IS NULL) THEN NEW.EAL_RUFNAME='';\
IF(NEW.EAL_ABTEILUNG IS NULL) THEN NEW.EAL_ABTEILUNG='';\
IF(NEW.EAL_STELLUNG IS NULL) THEN NEW.EAL_STELLUNG='';\
IF(NEW.EAL_ASSISTENT IS NULL) THEN NEW.EAL_ASSISTENT='';\
IF(NEW.EAL_PLZ IS NULL) THEN NEW.EAL_PLZ='';\
IF(NEW.EAL_ORT IS NULL) THEN NEW.EAL_ORT='';\
IF(NEW.EAL_BANKVER1 IS NULL) THEN NEW.EAL_BANKVER1='';\
IF(NEW.EAL_BANKVERB2 IS NULL) THEN NEW.EAL_BANKVERB2='';\
IF(NEW.EAL_BANKVERB3 IS NULL) THEN NEW.EAL_BANKVERB3='';\
IF(NEW.EAL_POSTVERB IS NULL) THEN NEW.EAL_POSTVERB='';\
IF(NEW.EAL_BEMERKUNGEN IS NULL) THEN NEW.EAL_BEMERKUNGEN='';\
IF(NEW.EAL_TEL1 IS NULL) THEN NEW.EAL_TEL1='';\
IF(NEW.EAL_FAX1 IS NULL) THEN NEW.EAL_FAX1='';\
IF(NEW.EAL_TEL2 IS NULL) THEN NEW.EAL_TEL2='';\
IF(NEW.EAL_TELEX IS NULL) THEN NEW.EAL_TELEX='';\
IF(NEW.EAL_FAX2 IS NULL) THEN NEW.EAL_FAX2='';\
IF(NEW.EAL_KONTAKTPER1 IS NULL) THEN NEW.EAL_KONTAKTPER1='';\
IF(NEW.EAL_KONTAKTPER2 IS NULL) THEN NEW.EAL_KONTAKTPER2='';\
IF(NEW.EAL_LIEEFERUNG IS NULL) THEN NEW.EAL_LIEEFERUNG='';\
IF(NEW.EAL_LAND IS NULL) THEN NEW.EAL_LAND='';\
IF(NEW.EAL_SUCHE IS NULL) THEN NEW.EAL_SUCHE='';\
IF(NEW.EAL_USER_B1 IS NULL) THEN NEW.EAL_USER_B1='';\
IF(NEW.EAL_USER_B2 IS NULL) THEN NEW.EAL_USER_B2='';\
IF(NEW.EAL_USER_B3 IS NULL) THEN NEW.EAL_USER_B3='';\
IF(NEW.EAL_USER_B4 IS NULL) THEN NEW.EAL_USER_B4='';\
IF(NEW.EAL_USER_B5 IS NULL) THEN NEW.EAL_USER_B5='';\
IF(NEW.EAL_USER_B6 IS NULL) THEN NEW.EAL_USER_B6='';\
IF(NEW.EAL_ANREDE IS NULL) THEN NEW.EAL_ANREDE='';\
IF(NEW.EAL_KUNDENGRUPP IS NULL) THEN NEW.EAL_KUNDENGRUPP='';\
IF(NEW.EAL_ABT_KONTAK1 IS NULL) THEN NEW.EAL_ABT_KONTAK1='';\
IF(NEW.EAL_ABT_KONTAK2 IS NULL) THEN NEW.EAL_ABT_KONTAK2='';\
IF(NEW.EAL_SPRACHE IS NULL) THEN NEW.EAL_SPRACHE='';\
IF(NEW.EAL_DEBI_KONTO IS NULL) THEN NEW.EAL_DEBI_KONTO='';\
IF(NEW.EAL_KREDI_KONTO IS NULL) THEN NEW.EAL_KREDI_KONTO='';\
IF(NEW.EAL_ANREDE_KTK1 IS NULL) THEN NEW.EAL_ANREDE_KTK1='';\
IF(NEW.EAL_ANREDE_KTK2 IS NULL) THEN NEW.EAL_ANREDE_KTK2='';\
IF(NEW.EAL_STUCKRAB_JN IS NULL) THEN NEW.EAL_STUCKRAB_JN='';\
IF(NEW.EAL_USER_C1 IS NULL) THEN NEW.EAL_USER_C1='';\
IF(NEW.EAL_USER_C2 IS NULL) THEN NEW.EAL_USER_C2='';\
IF(NEW.EAL_USER_C3 IS NULL) THEN NEW.EAL_USER_C3='';\
IF(NEW.EAL_USER_C4 IS NULL) THEN NEW.EAL_USER_C4='';\
IF(NEW.EAL_USER_C5 IS NULL) THEN NEW.EAL_USER_C5='';\
IF(NEW.EAL_USER_C6 IS NULL) THEN NEW.EAL_USER_C6='';\
IF(NEW.EAL_USER_C7 IS NULL) THEN NEW.EAL_USER_C7='';\
IF(NEW.EAL_USER_C8 IS NULL) THEN NEW.EAL_USER_C8='';\
IF(NEW.EAL_USER_C9 IS NULL) THEN NEW.EAL_USER_C9='';\
IF(NEW.EAL_USER_C0 IS NULL) THEN NEW.EAL_USER_C0='';\
IF(NEW.EAL_DEBITOR_NR IS NULL) THEN NEW.EAL_DEBITOR_NR='';\
IF(NEW.EAL_KREDITOR_NR IS NULL) THEN NEW.EAL_KREDITOR_NR='';\
IF(NEW.EAL_KREDITOR_JN IS NULL) THEN NEW.EAL_KREDITOR_JN='';\
IF(NEW.EAL_DEBITOR_JN IS NULL) THEN NEW.EAL_DEBITOR_JN='';\
IF(NEW.EAL_RECH_ADR IS NULL) THEN NEW.EAL_RECH_ADR='';\
IF(NEW.EAL_LIEFER_ADR IS NULL) THEN NEW.EAL_LIEFER_ADR='';\
IF(NEW.EAL_KONT_ADR IS NULL) THEN NEW.EAL_KONT_ADR='';\
IF(NEW.EAL_NOTE IS NULL) THEN NEW.EAL_NOTE='';\
IF(NEW.EAL_KUNDEN_NR IS NULL) THEN NEW.EAL_KUNDEN_NR='';\
IF(NEW.EAL_KURZZEICHEN IS NULL) THEN NEW.EAL_KURZZEICHEN='';\
IF(NEW.EAL_KURZANREDE IS NULL) THEN NEW.EAL_KURZANREDE='';\
IF(NEW.EAL_1_ADR_ZEILE IS NULL) THEN NEW.EAL_1_ADR_ZEILE='';\
IF(NEW.EAL_2_ADR_ZEILE IS NULL) THEN NEW.EAL_2_ADR_ZEILE='';\
IF(NEW.EAL_STRASSE IS NULL) THEN NEW.EAL_STRASSE='';\
IF(NEW.EAL_POSTFACH IS NULL) THEN NEW.EAL_POSTFACH='';\
IF(NEW.EAL_PLZ_POSTFACH IS NULL) THEN NEW.EAL_PLZ_POSTFACH='';\
IF(NEW.EAL_GLOBAL_ID IS NULL) THEN NEW.EAL_GLOBAL_ID='';\
IF(NEW.EAL_MODIFIED_BY IS NULL) THEN NEW.EAL_MODIFIED_BY='';\
END;";
		dboList["TR_FSA_ADRESSEN_DELETE"] = "CREATE TRIGGER TR_FSA_ADRESSEN_DELETE FOR FSA_ADRESSEN ACTIVE AFTER DELETE \
	 AS BEGIN \
	DELETE FROM ZS_BINARY_FIELDS WHERE OLD.EAL_BILD = RID_BIN;\
	END;";
	}
}
