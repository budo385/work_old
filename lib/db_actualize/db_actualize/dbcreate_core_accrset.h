#ifndef DBCREATE_CORE_ACCRSET_H
#define DBCREATE_CORE_ACCRSET_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"

class DbCreate_CORE_ACCRSET : public DbSqlTableCreation
{
public:
	DbCreate_CORE_ACCRSET():DbSqlTableCreation(CORE_ACCRSET){};  //init to CORE_ACCRSET table
	

private:
	//create generic Table def:
	void DefineFields(QStringList &lstSQL);

	//probably common to all DB's:
	void DefineIndexes(SQLDBObjectCreate &dboList);

};

#endif // DBCREATE_CORE_ACCRSET_H
