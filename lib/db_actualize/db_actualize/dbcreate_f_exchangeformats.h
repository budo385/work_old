#ifndef DBCREATE_F_EXCHANGEFORMATS_H
#define DBCREATE_F_EXCHANGEFORMATS_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_EXCHANGEFORMATS : public DbSqlTableCreation
{
public: 
	DbCreate_F_EXCHANGEFORMATS():DbSqlTableCreation(F_EXCHANGEFORMATS){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_EXCHANGEFORMATS_H
