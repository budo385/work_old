#ifndef DBCREATE_BUS_PERSONROLE_H
#define DBCREATE_BUS_PERSONROLE_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_BUS_PERSONROLE : public DbSqlTableCreation
{
public:
	DbCreate_BUS_PERSONROLE():DbSqlTableCreation(BUS_PERSONROLE){};  //init to CORE_PERSONROLE table
	

private:
	//create generic Table def:
	void DefineFields(QStringList &lstSQL);

	//probably common to all DB's:
	void DefineIndexes(SQLDBObjectCreate &dboList);
};

#endif // DBCREATE_BUS_PERSONROLE_H