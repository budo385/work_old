#ifndef DBCREATE_BUS_TASKS_H
#define DBCREATE_BUS_TASKS_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_BUS_TASKS : public DbSqlTableCreation
{
public:
	DbCreate_BUS_TASKS():DbSqlTableCreation(BUS_TASKS){};  //init table
	

private:
	//create generic Table def:
	void DefineFields(QStringList &lstSQL);

	//probably common to all DB's:
	void DefineIndexes(SQLDBObjectCreate &dboList);
};

#endif // DBCREATE_BUS_TASKS_H
