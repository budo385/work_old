#ifndef DBCREATE_F_SYNCHRO_H
#define DBCREATE_F_SYNCHRO_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_SYNCHRO : public DbSqlTableCreation
{
public: 
	DbCreate_F_SYNCHRO():DbSqlTableCreation(F_SYNCHRO){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_SYNCHRO_H
