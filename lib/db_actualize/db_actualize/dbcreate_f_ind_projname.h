#ifndef DBCREATE_F_IND_PROJNAME_H
#define DBCREATE_F_IND_PROJNAME_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_IND_PROJNAME : public DbSqlTableCreation
{
public: 
	DbCreate_F_IND_PROJNAME():DbSqlTableCreation(F_IND_PROJNAME){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_IND_PROJNAME_H
