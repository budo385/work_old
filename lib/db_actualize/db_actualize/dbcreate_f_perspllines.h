#ifndef DBCREATE_F_PERSPLLINES_H
#define DBCREATE_F_PERSPLLINES_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_PERSPLLINES : public DbSqlTableCreation
{
public: 
	DbCreate_F_PERSPLLINES():DbSqlTableCreation(F_PERSPLLINES){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_PERSPLLINES_H
