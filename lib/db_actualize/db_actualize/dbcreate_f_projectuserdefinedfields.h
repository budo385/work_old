#ifndef DBCREATE_F_PROJECTUSERDEFINEDFIELDS_H
#define DBCREATE_F_PROJECTUSERDEFINEDFIELDS_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_PROJECTUSERDEFINEDFIELDS : public DbSqlTableCreation
{
public: 
	DbCreate_F_PROJECTUSERDEFINEDFIELDS():DbSqlTableCreation(F_PROJECTUSERDEFINEDFIELDS){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_PROJECTUSERDEFINEDFIELDS_H
