#ifndef DBCREATE_F_FAKTURA_H
#define DBCREATE_F_FAKTURA_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_FAKTURA : public DbSqlTableCreation
{
public: 
	DbCreate_F_FAKTURA():DbSqlTableCreation(F_FAKTURA){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_FAKTURA_H
