#ifndef DBCREATE_BUS_CM_CONTACT_H
#define DBCREATE_BUS_CM_CONTACT_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_BUS_CM_CONTACT : public DbSqlTableCreation
{
public:
	DbCreate_BUS_CM_CONTACT():DbSqlTableCreation(BUS_CM_CONTACT){};  //init table
	

private:
	//create generic Table def:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);

	//probably common to all DB's:
	void DefineIndexes(SQLDBObjectCreate &dboList);
};

#endif // DBCREATE_BUS_CM_CONTACT_H
