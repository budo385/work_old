#ifndef DBCREATE_F_PROJ_MITARB_H
#define DBCREATE_F_PROJ_MITARB_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_PROJ_MITARB : public DbSqlTableCreation
{
public: 
	DbCreate_F_PROJ_MITARB():DbSqlTableCreation(F_PROJ_MITARB){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_PROJ_MITARB_H
