#ifndef DBCREATE_F_PROJECTUSERDEFINEDGUIMAP_H
#define DBCREATE_F_PROJECTUSERDEFINEDGUIMAP_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_PROJECTUSERDEFINEDGUIMAP : public DbSqlTableCreation
{
public: 
	DbCreate_F_PROJECTUSERDEFINEDGUIMAP():DbSqlTableCreation(F_PROJECTUSERDEFINEDGUIMAP){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_PROJECTUSERDEFINEDGUIMAP_H
