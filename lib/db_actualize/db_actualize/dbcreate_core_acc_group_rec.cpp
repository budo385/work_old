#include "dbcreate_core_acc_group_rec.h"


//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_CORE_ACC_GROUP_REC::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
								   create table CORE_ACC_GROUP_REC (\
								   CGAR_ID INTEGER not null,\
								   CGAR_GLOBAL_ID VARCHAR(15) null,\
								   CGAR_DAT_LAST_MODIFIED TIMESTAMP not null,\
								   CGAR_TABLE_ID INTEGER not null,\
								   CGAR_RECORD_ID INTEGER not null,\
								   CGAR_GROUP_ID INTEGER not null,\
								   CGAR_PARENT_ID INTEGER null,\
								   CGAR_CHILD_INHERIT SMALLINT not null,\
								   CGAR_CHILD_COMM_INHERIT SMALLINT not null,\
								   CGAR_RIGHT SMALLINT null, constraint CORE_ACC_GROUP_REC_PK primary key (CGAR_ID) )");  


	m_lstDeletedCols<<"CGAR_GROUP_DEPENDENT";


	lstSQL.append("\
								   alter table CORE_ACC_GROUP_REC\
								   add constraint CORE_ACC_GROUP_REC_FK1 foreign key (\
								   CGAR_PARENT_ID)\
								   references CORE_ACC_GROUP_REC (\
								   CGAR_ID)  \
								   ON DELETE SET NULL");

	lstSQL.append("\
								   alter table CORE_ACC_GROUP_REC\
								   add constraint GROUP_ITEMS_ACC_GRP_FK1 foreign key (\
								   CGAR_GROUP_ID)\
								   references BUS_GROUP_ITEMS (\
								   BGIT_ID)  \
								   ON DELETE CASCADE");

	
}


//probably common to all DB's:
void DbCreate_CORE_ACC_GROUP_REC::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
	dboList["IND_CORE_ACC_GRP_UNIQUE"] = "create unique index IND_CORE_ACC_GRP_UNIQUE on CORE_ACC_GROUP_REC (CGAR_TABLE_ID,CGAR_RECORD_ID,CGAR_GROUP_ID)"; 
	dboList["IND_CORE_ACC_GRP_SELECT"] = "create index IND_CORE_ACC_GRP_SELECT on CORE_ACC_GROUP_REC (CGAR_TABLE_ID,CGAR_RECORD_ID)";  
	dboList["IND_CORE_ACC_GRP_PARENT"] = "create index IND_CORE_ACC_GRP_PARENT on CORE_ACC_GROUP_REC (CGAR_PARENT_ID)";  
	//dboList["IND_CORE_ACC_GRP_RECORD"] = "create index IND_CORE_ACC_GRP_RECORD on CORE_ACC_GROUP_REC (CGAR_TABLE_ID,CGAR_RECORD_ID)";  
}

