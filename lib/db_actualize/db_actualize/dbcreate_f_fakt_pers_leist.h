#ifndef DBCREATE_F_FAKT_PERS_LEIST_H
#define DBCREATE_F_FAKT_PERS_LEIST_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_FAKT_PERS_LEIST : public DbSqlTableCreation
{
public: 
	DbCreate_F_FAKT_PERS_LEIST():DbSqlTableCreation(F_FAKT_PERS_LEIST){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_FAKT_PERS_LEIST_H
