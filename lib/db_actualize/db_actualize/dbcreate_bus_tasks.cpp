#include "dbcreate_bus_tasks.h"

//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_TASKS::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
											create table BUS_TASKS (\
											BTKS_ID INTEGER not null,\
											BTKS_GLOBAL_ID VARCHAR(15) null,\
											BTKS_DAT_LAST_MODIFIED TIMESTAMP not null,\
											BTKS_IS_TASK_ACTIVE SMALLINT not null,\
											BTKS_SUBJECT VARCHAR(100) null,\
											BTKS_START DATETIME null,\
											BTKS_DUE DATETIME null,\
											BTKS_COMPLETED DATETIME null,\
											BTKS_TASK_TYPE_ID INTEGER null,\
											BTKS_SYSTEM_TYPE SMALLINT not null,\
											BTKS_SYSTEM_STATUS SMALLINT not null,\
											BTKS_DESCRIPTION LONGVARCHAR null,\
											BTKS_OWNER INTEGER null,\
											BTKS_ORIGINATOR INTEGER null,\
											BTKS_NOTIFY_ORIGINATOR SMALLINT null, \
											BTKS_PRIORITY SMALLINT null, constraint BUS_TASKS_PK primary key (BTKS_ID) )"); 

	lstSQL.append("\
										alter table BUS_TASKS\
										add constraint BUS_PERSON_BUS_TASKS_FK1 foreign key (\
										BTKS_ORIGINATOR)\
										references BUS_PERSON (\
										BPER_ID) ON DELETE SET NULL");

	lstSQL.append("\
										alter table BUS_TASKS\
										add constraint BUS_PERSON_BUS_TASKS_FK2 foreign key (\
										BTKS_OWNER)\
										references BUS_PERSON (\
										BPER_ID) ON DELETE SET NULL");

	lstSQL.append("\
										alter table BUS_TASKS\
										add constraint BUS_TASKS_FK1 foreign key (\
										BTKS_TASK_TYPE_ID)\
										references BUS_CM_TYPES (\
										BCMT_ID)");

	//no Db versioning:
	m_lstDeletedCols<<"BTKS_COLOR";
	m_lstDeletedCols<<"BTKS_ICON_ID";
}



//probably common to all DB's:
void DbCreate_BUS_TASKS::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();

	dboList["IND_BTKS_TYPE"] = "create index IND_BTKS_TYPE on BUS_TASKS (	BTKS_TASK_TYPE_ID  )  ";  
	dboList["IND_BTKS_ORIGINATOR"] = "create index IND_BTKS_ORIGINATOR on BUS_TASKS (	BTKS_ORIGINATOR  )  ";  
	dboList["IND_BTKS_OWNER"] = "create index IND_BTKS_OWNER on BUS_TASKS (	BTKS_OWNER  )  ";  
	dboList["IND_BTKS_STATUS"] = "create index IND_BTKS_STATUS on BUS_TASKS (		BTKS_SYSTEM_STATUS  ) ";  
	dboList["IND_BTKS_SYS_TYPE"] = "create index IND_BTKS_SYS_TYPE on BUS_TASKS (		BTKS_SYSTEM_TYPE  ) ";  
	dboList["IND_BTKS_START"] = "create index IND_BTKS_START on BUS_TASKS (		BTKS_START  ) ";  
	dboList["IND_BTKS_DUE"] = "create index IND_BTKS_DUE on BUS_TASKS (		BTKS_DUE  )  ";  
	dboList["IND_BTKS_COMPLETED"] = "create index IND_BTKS_COMPLETED on BUS_TASKS (		BTKS_COMPLETED  )  ";  
	
}


