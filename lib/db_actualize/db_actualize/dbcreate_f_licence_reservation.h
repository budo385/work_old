#ifndef DBCREATE_F_LICENCE_RESERVATION_H
#define DBCREATE_F_LICENCE_RESERVATION_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_LICENCE_RESERVATION : public DbSqlTableCreation
{
public: 
	DbCreate_F_LICENCE_RESERVATION():DbSqlTableCreation(F_LICENCE_RESERVATION){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_LICENCE_RESERVATION_H
