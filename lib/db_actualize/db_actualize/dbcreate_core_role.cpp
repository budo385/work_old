#include "dbcreate_core_role.h"

//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_CORE_ROLE::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
											create table CORE_ROLE (					\
											CROL_ID INTEGER not null,					\
											CROL_GLOBAL_ID VARCHAR(15) null,			\
											CROL_DAT_LAST_MODIFIED TIMESTAMP not null,	\
											CROL_NAME VARCHAR(255) not null,				\
											CROL_ROLE_DESC VARCHAR(255) null,			\
											CROL_ROLE_TYPE INTEGER not null,			\
											CROL_DEFAULT_FLAG INTEGER null,				\
											CROL_CODE INTEGER null,						\
											constraint CORE_ROLE_PK primary key (CROL_ID) )");
	
}

//probably common to all DB's:
void DbCreate_CORE_ROLE::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
}

