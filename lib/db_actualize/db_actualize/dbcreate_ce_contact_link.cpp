#include "dbcreate_ce_contact_link.h"



//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_CE_CONTACT_LINK::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
											create table CE_CONTACT_LINK ( \
											CELC_ID INTEGER not null,\
											CELC_GLOBAL_ID VARCHAR(15) null,\
											CELC_DAT_LAST_MODIFIED TIMESTAMP not null,\
											CELC_CONTACT_ID INTEGER not null,\
											CELC_COMM_ENTITY_ID INTEGER not null, \
											CELC_LINK_ROLE_ID INTEGER not null, \
											constraint CE_CONTACT_LINK_PK primary key (CELC_ID) )");
/*
	//constraint for table creation:
	lstSQL.append("\
											alter table CE_CONTACT_LINK\
											add constraint BUS_CNT_LINK_FK1 foreign key (\
											CELC_CONTACT_ID)\
											references BUS_CM_CONTACT (\
											BCNT_ID) \
											ON DELETE CASCADE");


	lstSQL.append("\
											alter table CE_CONTACT_LINK\
											add constraint CE_ENTITY_CONT_LK_FK1 foreign key (\
											CELC_COMM_ENTITY_ID)\
											references CE_COMM_ENTITY (\
											CENT_ID) \
											ON DELETE CASCADE");


*/
 

}



//probably common to all DB's:
void DbCreate_CE_CONTACT_LINK::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
	dboList["IND_CELC_ENTITY_ID"] = "create index IND_CELC_ENTITY_ID on CE_CONTACT_LINK (CELC_COMM_ENTITY_ID)  ";  
	dboList["IND_CELC_CONTACT_ID"] = "create index IND_CELC_CONTACT_ID on CE_CONTACT_LINK (CELC_CONTACT_ID)  ";
}
