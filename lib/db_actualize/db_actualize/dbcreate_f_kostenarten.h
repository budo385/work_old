#ifndef DBCREATE_F_KOSTENARTEN_H
#define DBCREATE_F_KOSTENARTEN_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_KOSTENARTEN : public DbSqlTableCreation
{
public: 
	DbCreate_F_KOSTENARTEN():DbSqlTableCreation(F_KOSTENARTEN){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_KOSTENARTEN_H
