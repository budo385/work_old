#ifndef DBCREATE_CORE_EVENT_LOG_H
#define DBCREATE_CORE_EVENT_LOG_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_CORE_EVENT_LOG : public DbSqlTableCreation
{
public:
	DbCreate_CORE_EVENT_LOG():DbSqlTableCreation(CORE_EVENT_LOG){};  //init to CORE_ERROR_LOG table
	

private:
	//create generic Table def:
	void DefineFields(QStringList &lstSQL);

	//probably common to all DB's:
	void DefineIndexes(SQLDBObjectCreate &dboList);
};
#endif //DbCreate_CORE_EVENT_LOG
