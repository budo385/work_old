#include "dbcreate_bus_person.h"

//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_PERSON::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
											create table BUS_PERSON (					\
											BPER_ID INTEGER not null,					\
											BPER_GLOBAL_ID VARCHAR(15) null,\
											BPER_DAT_LAST_MODIFIED TIMESTAMP not null,\
											BPER_FIRST_NAME VARCHAR(50) null,\
											BPER_LAST_NAME VARCHAR(50) not null,\
											BPER_ACTIVE_FLAG INTEGER not null,\
											BPER_CODE VARCHAR(12) not null,\
											BPER_CONTACT_ID INTEGER null,\
											BPER_SEX SMALLINT not null,\
											BPER_BIRTH_DATE DATE null,\
											BPER_DESCRIPTION LONGVARCHAR null,\
											BPER_INITIALS VARCHAR(8) not null,\
											BPER_DEPARTMENT_ID INTEGER null,\
											BPER_ORGANIZATION_ID INTEGER null,\
											BPER_DIRECT_PHONE VARCHAR(30) null,\
											BPER_INSURANCE_ID VARCHAR(30) null,\
											BPER_MAIN_FUNCTION_ID INTEGER null,\
											BPER_OCCUPATION DECIMAL(10,2) null,\
											BPER_DATE_ENTERED DATE null,\
											BPER_DATE_LEFT DATE null,\
											BPER_PICTURE BINARY(10) null,\
											BPER_BIG_PICTURE_ID INTEGER null, \
											BPER_CODE_EXT VARCHAR(12) null,\
											BPER_DEF_PROJECT_LIST_ID INTEGER null,\
											BPER_FPERSON_SEQ INTEGER null,\
											constraint BUS_PERSON_PK primary key (BPER_ID) ) ");




	//constraint for table creation:
	lstSQL.append("\
											alter table BUS_PERSON\
											add constraint BUS_DEP_BUS_PERSON_FK1 foreign key (\
											BPER_DEPARTMENT_ID)\
											references BUS_DEPARTMENTS (\
											BDEPT_ID)\
											");

	lstSQL.append("\
											alter table BUS_PERSON\
											add constraint BUS_ORG_BUS_PERSON_FK1 foreign key (\
											BPER_ORGANIZATION_ID)\
											references BUS_ORGANIZATIONS (\
											BORG_ID)\
											");

	lstSQL.append("\
											alter table BUS_PERSON\
											add constraint BUS_BIG_PIC_PERS_FK1 foreign key (\
											BPER_BIG_PICTURE_ID)\
											references BUS_BIG_PICTURE (\
											BPIC_ID)\
											");

  
	lstSQL.append("\
											alter table BUS_PERSON\
											add constraint BUS_CONT_PERS_FK1 foreign key (\
											BPER_CONTACT_ID)\
											references BUS_CM_CONTACT (\
											BCNT_ID) ON DELETE SET NULL\
											");
	lstSQL.append("\
								   alter table BUS_PERSON\
								   add constraint BUS_PROJ_LIST_PERS_FK1 foreign key (\
								   BPER_DEF_PROJECT_LIST_ID)\
								   references BUS_STORED_PROJECT_LIST (\
								   BSPL_ID) ON DELETE SET NULL\
											");

 
}

//probably common to all DB's:
void DbCreate_BUS_PERSON::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
	dboList["IND_BPER_CONTACT_ID"] = "create index IND_BPER_CONTACT_ID on BUS_PERSON (BPER_CONTACT_ID)";  
	//dboList["IND_BPER_CODE"] = "create unique index IND_BPER_CODE on BUS_PERSON (BPER_CODE)";  
	dboList["IND_BPER_DEPARTMENT_ID"] = "create index IND_BPER_DEPARTMENT_ID on BUS_PERSON (BPER_DEPARTMENT_ID)";  
	dboList["IND_BPER_ORGANIZATION_ID"] = "create index IND_BPER_ORGANIZATION_ID on BUS_PERSON (BPER_ORGANIZATION_ID)";  
	dboList["IND_BPER_FUNCTION_ID"] = "create index IND_BPER_FUNCTION_ID on BUS_PERSON (BPER_MAIN_FUNCTION_ID)";  
	dboList["IND_BPER_LAST_MODIFIED"] = "create index IND_BPER_LAST_MODIFIED on BUS_PERSON ( BPER_DAT_LAST_MODIFIED )";
	dboList["IND_BPER_FPERSON_SEQ"] = "create index IND_BPER_FPERSON_SEQ on BUS_PERSON (BPER_FPERSON_SEQ)";  
}



void DbCreate_BUS_PERSON::DefineTriggers(SQLDBObjectCreate &dboList)
{
	//create triggers TOFIX: foe each DB, must be different
	dboList["TRG_BUS_PERSON_DELETE"]="\
									 CREATE TRIGGER TRG_BUS_PERSON_DELETE FOR BUS_PERSON\
									 ACTIVE AFTER DELETE AS \
									 BEGIN \
									 DELETE FROM CORE_USER WHERE OLD.BPER_ID = CUSR_PERSON_ID;\
									 DELETE FROM BUS_BIG_PICTURE WHERE OLD.BPER_BIG_PICTURE_ID = BPIC_ID;\
									 DELETE FROM bus_nmrx_relation WHERE OLD.BPER_ID = BNMR_TABLE_KEY_ID_1 AND BNMR_TABLE_1="+QVariant(BUS_PERSON).toString()+";\
									 DELETE FROM bus_nmrx_relation WHERE OLD.BPER_ID = BNMR_TABLE_KEY_ID_2 AND BNMR_TABLE_2="+QVariant(BUS_PERSON).toString()+";\
									 END\
									 ";
	//create triggers TOFIX: foe each DB, must be different
	dboList["TRG_BUS_PERSON_UPDATE"]="\
									 CREATE TRIGGER TRG_BUS_PERSON_UPDATE FOR BUS_PERSON\
									 ACTIVE AFTER UPDATE AS \
									 BEGIN \
									 IF (NOT(OLD.BPER_BIG_PICTURE_ID IS NULL) AND NEW.BPER_BIG_PICTURE_ID IS NULL)\
									 THEN\
									 DELETE FROM BUS_BIG_PICTURE WHERE OLD.BPER_BIG_PICTURE_ID = BPIC_ID;\
									 UPDATE CORE_USER SET CUSR_FIRST_NAME=NEW.BPER_FIRST_NAME, CUSR_LAST_NAME=NEW.BPER_LAST_NAME WHERE CUSR_PERSON_ID=NEW.BPER_ID;\
									 END\
									 ";

}