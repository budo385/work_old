#include "dbcreate_bus_cal_reservation.h"

//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_CAL_RESERVATION::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
								   create table BUS_CAL_RESERVATION (\
								   BCRS_ID INTEGER not null,\
								   BCRS_GLOBAL_ID VARCHAR(15) null,\
								   BCRS_DAT_LAST_MODIFIED TIMESTAMP not null,\
								   BCRS_IS_POSSIBLE SMALLINT null,\
								   BCRS_WEEKDAY INTEGER null,\
								   BCRS_WEEKDAY_TIME_FROM DATETIME null,\
								   BCRS_WEEKDAY_TIME_TO DATETIME null,\
								   BCRS_IS_FREE SMALLINT null,\
								   BCRS_FREE_FROM DATETIME null,\
								   BCRS_FREE_TO DATETIME null,\
								   BCRS_PROJECT_ID INTEGER null,\
								   BCRS_CONTACT_ID INTEGER null,\
								   BCRS_PERSON_ID INTEGER null,\
								   BCRS_RESOURCE_ID INTEGER null, constraint BUS_CAL_RESERVATION_PK primary key (BCRS_ID) )") ; 
											

	lstSQL.append("\
									alter table BUS_CAL_RESERVATION\
									add constraint BCRS_PROJ_FK1 foreign key (\
									BCRS_PROJECT_ID)\
									references BUS_PROJECTS (BUSP_ID)\
									ON DELETE CASCADE");
	lstSQL.append("\
								   alter table BUS_CAL_RESERVATION\
								   add constraint BCRS_CONTACT_FK1 foreign key (\
								   BCRS_CONTACT_ID)\
								   references BUS_CM_CONTACT (BCNT_ID)\
								   ON DELETE CASCADE");
	lstSQL.append("\
								   alter table BUS_CAL_RESERVATION\
								   add constraint BCRS_PERS_FK1 foreign key (\
								   BCRS_PERSON_ID)\
								   references BUS_PERSON (BPER_ID)\
								   ON DELETE CASCADE");
	lstSQL.append("\
								   alter table BUS_CAL_RESERVATION\
								   add constraint BCRS_RESOURCE_FK1 foreign key (\
								   BCRS_RESOURCE_ID)\
								   references BUS_CAL_RESOURCE (BRES_ID)\
								   ON DELETE CASCADE");

}



//probably common to all DB's:
void DbCreate_BUS_CAL_RESERVATION::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
}


