#include "dbcreate_bus_cm_address_types.h"




//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_CM_ADDRESS_TYPES::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
											create table BUS_CM_ADDRESS_TYPES ( \
											BCMAT_ID INTEGER not null,\
											BCMAT_GLOBAL_ID VARCHAR(15) null,\
											BCMAT_DAT_LAST_MODIFIED TIMESTAMP not null,\
											BCMAT_ADDRESS_ID INTEGER null,\
											BCMAT_TYPE_ID INTEGER null, constraint BUS_CM_ADDRESS_TYPES_PK primary key (BCMAT_ID) )"); 



	lstSQL.append("\
											alter table BUS_CM_ADDRESS_TYPES\
											add constraint BUS_TYPES_ADDR_TYP_FK1 foreign key (\
											BCMAT_TYPE_ID)\
											references BUS_CM_TYPES (\
											BCMT_ID)\
											");


	lstSQL.append("\
											alter table BUS_CM_ADDRESS_TYPES\
											add constraint BUS_ADDR_ADDR_TYP_FK1 foreign key (\
											BCMAT_ADDRESS_ID)\
											references BUS_CM_ADDRESS (\
											BCMA_ID) ON DELETE CASCADE\
											");
}



//probably common to all DB's:
void DbCreate_BUS_CM_ADDRESS_TYPES::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
	dboList["IND_BCMAT_ADDR_TYPE_ID"] = "create unique index IND_BCMAT_ADDR_TYPE_ID on BUS_CM_ADDRESS_TYPES (	BCMAT_ADDRESS_ID,BCMAT_TYPE_ID)  ";  
	dboList["IND_BCMAT_TYPE_ID"] = "create index IND_BCMAT_TYPE_ID on BUS_CM_ADDRESS_TYPES (BCMAT_TYPE_ID)";  
	dboList["IND_BCMAT_ADDR_ID"] = "create index IND_BCMAT_ADDR_ID on BUS_CM_ADDRESS_TYPES (BCMAT_ADDRESS_ID) ";  
}


