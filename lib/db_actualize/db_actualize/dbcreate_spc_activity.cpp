#include "dbcreate_spc_activity.h"


//--------------------------------------------------
// ODBC - CREATE
//--------------------------------------------------
void DbCreate_SPC_ACTIVITY::DefineFields(QStringList &lstSQL) 
{
	//Table schema statements for ODBC.
	lstSQL.append(" 	CREATE TABLE SPC_ACTIVITY (\
										SET_ID INTEGER not null,\
										SET_GLOBAL_ID VARCHAR(15) null,\
										SET_DAT_LAST_MODIFIED TIMESTAMP not null,\
										SET_CODE VARCHAR(50) not null,\
										SET_LEVEL INTEGER not null,\
										SET_PARENT INTEGER null,\
										SET_HASCHILDREN INTEGER not null,\
										SET_ICON VARCHAR(50) null,\
										SET_MAINPARENT_ID INTEGER null,\
										SET_NAME VARCHAR(100) not null,\
										SET_STYLE INTEGER null,\
										SET_F_TAETIGKEITEN_SEQ INTEGER null,\
										constraint SPC_ACTIVITY_PK primary key (SET_ID))");


}

void DbCreate_SPC_ACTIVITY::DefineIndexes(SQLDBObjectCreate &dboList) 
{
		//add index definition by its name (important).
		dboList["IND_SET_CODE"] = "CREATE INDEX IND_SET_CODE ON SPC_ACTIVITY (SET_CODE)";
		dboList["IND_SET_CODE"] = "CREATE UNIQUE INDEX IND_SET_CODE ON SPC_ACTIVITY (SET_CODE)";
		dboList["IND_SET_PARENT"] = "create index IND_SET_PARENT on SPC_ACTIVITY (SET_PARENT)";  
}

void DbCreate_SPC_ACTIVITY::DefineViews(SQLDBObjectCreate &dboList) 
{

}

void DbCreate_SPC_ACTIVITY::DefineTriggers(SQLDBObjectCreate &dboList) 
{

}
