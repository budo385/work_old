#ifndef DBCREATE_F_WIRTSCH_GRUPPE_H
#define DBCREATE_F_WIRTSCH_GRUPPE_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_WIRTSCH_GRUPPE : public DbSqlTableCreation
{
public: 
	DbCreate_F_WIRTSCH_GRUPPE():DbSqlTableCreation(F_WIRTSCH_GRUPPE){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_WIRTSCH_GRUPPE_H
