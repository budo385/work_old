#ifndef DBCREATE_F_USER_APPLICATIONS_H
#define DBCREATE_F_USER_APPLICATIONS_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_USER_APPLICATIONS : public DbSqlTableCreation
{
public: 
	DbCreate_F_USER_APPLICATIONS():DbSqlTableCreation(F_USER_APPLICATIONS){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_USER_APPLICATIONS_H
