#ifndef DBCREATE_SPC_SECTOR_H
#define DBCREATE_SPC_SECTOR_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_SPC_SECTOR : public DbSqlTableCreation
{
public: 
	DbCreate_SPC_SECTOR():DbSqlTableCreation(SPC_SECTOR){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif //  SPC_SECTOR_H
