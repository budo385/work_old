#include "dbcreate_bus_cm_payment_conditions.h"




//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_CM_PAYMENT_CONDITIONS::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
											create table BUS_CM_PAYMENT_CONDITIONS ( \
											BCMPY_ID INTEGER not null,\
											BCMPY_GLOBAL_ID VARCHAR(15) null,\
											BCMPY_DAT_LAST_MODIFIED TIMESTAMP not null,\
											BCMPY_NAME VARCHAR(100) null,\
											BCMPY_TEXT LONGVARCHAR null,\
											BCMPY_PAYMENTPERIOD1 INTEGER null,\
											BCMPY_PAYMENTPERIOD2 INTEGER null,\
											BCMPY_PAYMENTPERIOD3 INTEGER null,\
											BCMPY_DISCOUNT1 DECIMAL(10,2) null,\
											BCMPY_DISCOUNT2 DECIMAL(10,2) null,\
											BCMPY_DISCOUNT3 DECIMAL(10,2) null, constraint BUS_CM_PAYMENT_CONDITIONS_PK primary key (BCMPY_ID) )  ");
}



//probably common to all DB's:
void DbCreate_BUS_CM_PAYMENT_CONDITIONS::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
}


