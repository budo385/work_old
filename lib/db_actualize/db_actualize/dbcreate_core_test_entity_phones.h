#ifndef DBCREATE_TEST_ENTITY_PHONES_H
#define DBCREATE_TEST_ENTITY_PHONES_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"
#include "db_versions.h"



class DbCreate_TEST_ENTITY_PHONES : public DbSqlTableCreation
{
public:
	DbCreate_TEST_ENTITY_PHONES():DbSqlTableCreation(TEST_ENTITY_PHONES){};  //init to CORE_LOCKING table

	int GetStartDbVersion(){return DBVER_1;};

private:

	//create generic Table def:
	void DefineFields(QStringList &lstSQL);

	//probably common to all DB's:
	void DefineIndexes(SQLDBObjectCreate &dboList);
};



#endif 
