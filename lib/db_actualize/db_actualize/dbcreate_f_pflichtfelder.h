#ifndef DBCREATE_F_PFLICHTFELDER_H
#define DBCREATE_F_PFLICHTFELDER_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_PFLICHTFELDER : public DbSqlTableCreation
{
public: 
	DbCreate_F_PFLICHTFELDER():DbSqlTableCreation(F_PFLICHTFELDER){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_PFLICHTFELDER_H
