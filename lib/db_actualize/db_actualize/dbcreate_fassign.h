#ifndef DBCREATE_FASSIGN_H
#define DBCREATE_FASSIGN_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_FASSIGN : public DbSqlTableCreation
{
public: 
	DbCreate_FASSIGN():DbSqlTableCreation(FASSIGN){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // FASSIGN_H
