#include "dbcreate_bus_cal_breaks.h"


//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_CAL_BREAKS::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
								   create table BUS_CAL_BREAKS (\
								   BCBL_ID INTEGER not null,\
								   BCBL_GLOBAL_ID VARCHAR(15) null,\
								   BCBL_DAT_LAST_MODIFIED TIMESTAMP not null,\
								   BCBL_FROM DATETIME null,\
								   BCBL_TO DATETIME null,\
								   BCBL_OPTIONS_ID INTEGER not null, \
								   BCBL_NAME VARCHAR(400) null, \
								   constraint BUS_CAL_BREAKS_PK primary key (BCBL_ID) )      ");  

	lstSQL.append("\
								   alter table BUS_CAL_BREAKS\
								   add constraint BUS_CAL_OPT_BREAKS_FK1 foreign key (\
								   BCBL_OPTIONS_ID)\
								   references BUS_CAL_OPTIONS (\
								   BCOL_ID) ON DELETE CASCADE 	");	
}



//probably common to all DB's:
void DbCreate_BUS_CAL_BREAKS::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
}

