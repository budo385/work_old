#ifndef DBCREATE_F_UDF_PREDEF_FILL_H
#define DBCREATE_F_UDF_PREDEF_FILL_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_UDF_PREDEF_FILL : public DbSqlTableCreation
{
public: 
	DbCreate_F_UDF_PREDEF_FILL():DbSqlTableCreation(F_UDF_PREDEF_FILL){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_UDF_PREDEF_FILL_H
