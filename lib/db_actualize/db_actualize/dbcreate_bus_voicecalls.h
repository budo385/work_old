#ifndef DBCREATE_BUS_VOICECALLS_H
#define DBCREATE_BUS_VOICECALLS_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_BUS_VOICECALLS : public DbSqlTableCreation
{
public:
	DbCreate_BUS_VOICECALLS():DbSqlTableCreation(BUS_VOICECALLS){};  //init table
	

private:
	//create generic Table def:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);

	//probably common to all DB's:
	void DefineIndexes(SQLDBObjectCreate &dboList);
};

#endif // DBCREATE_BUS_VOICECALLS_H
