#include "dbcreate_bus_nmrx_role.h"


//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_NMRX_ROLE::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
											create table BUS_NMRX_ROLE ( \
											BNRO_ID INTEGER not null,\
											BNRO_GLOBAL_ID VARCHAR(15) null,\
											BNRO_DAT_LAST_MODIFIED TIMESTAMP not null,\
											BNRO_TABLE_1 INTEGER not null,\
											BNRO_TABLE_2 INTEGER not null,\
											BNRO_NAME VARCHAR(80) not null,\
											BNRO_ASSIGMENT_TEXT VARCHAR(80) not null,\
											BNRO_DESCRIPTION LONGVARCHAR null, constraint BUS_NMRX_ROLE_PK primary key (BNRO_ID) )");  

}



//probably common to all DB's:
void DbCreate_BUS_NMRX_ROLE::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
	dboList["IND_BNRO_TABLE_1"] = "create index IND_BNRO_TABLE_1 on BUS_NMRX_ROLE (BNRO_TABLE_1) ";  
	dboList["IND_BNRO_TABLE_2"] = "create index IND_BNRO_TABLE_2 on BUS_NMRX_ROLE (BNRO_TABLE_2) ";  

}



