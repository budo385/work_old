#ifndef DBCREATE_BUS_COST_CENTERS_H
#define DBCREATE_BUS_COST_CENTERS_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_BUS_COST_CENTERS : public DbSqlTableCreation
{
public:
	DbCreate_BUS_COST_CENTERS():DbSqlTableCreation(BUS_COST_CENTER){};  //init table
	

private:
	//create generic Table def:
	void DefineFields(QStringList &lstSQL);

	//probably common to all DB's:
	void DefineIndexes(SQLDBObjectCreate &dboList);
};

#endif // DBCREATE_BUS_PERSON_H
