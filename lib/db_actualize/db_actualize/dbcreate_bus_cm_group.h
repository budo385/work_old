#ifndef DBCREATE_BUS_CM_GROUP_H
#define DBCREATE_BUS_CM_GROUP_H


#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_BUS_CM_GROUP : public DbSqlTableCreation
{
public:
	DbCreate_BUS_CM_GROUP():DbSqlTableCreation(BUS_CM_GROUP){};  //init table
	

private:
	//create generic Table def:
	void DefineFields(QStringList &lstSQL);

	//probably common to all DB's:
	void DefineIndexes(SQLDBObjectCreate &dboList);
};


#endif // DBCREATE_BUS_CM_GROUP_H
