#include "dbcreate_bus_nmrx_relation.h"


//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_NMRX_RELATION::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
											create table BUS_NMRX_RELATION ( \
											BNMR_ID INTEGER not null,\
											BNMR_GLOBAL_ID VARCHAR(15) null,\
											BNMR_DAT_LAST_MODIFIED TIMESTAMP not null,\
											BNMR_TABLE_1 INTEGER not null,\
											BNMR_TABLE_2 INTEGER not null,\
											BNMR_TABLE_KEY_ID_1 INTEGER not null,\
											BNMR_TABLE_KEY_ID_2 INTEGER not null,\
											BNMR_ROLE_ID INTEGER null,\
											BNMR_DESCRIPTION LONGVARCHAR null, \
											BNMR_SYSTEM_ROLE INTEGER null,\
											BNMR_TYPE SMALLINT null,\
											BNMR_X_TABLE_ID INTEGER null, constraint BUS_NMRX_RELATION_PK primary key (BNMR_ID) )");  

	lstSQL.append("\
											alter table BUS_NMRX_RELATION\
											add constraint BUS_ROLE_RELATION_FK1 foreign key (\
											BNMR_ROLE_ID)\
											references BUS_NMRX_ROLE (\
											BNRO_ID)");


}

//probably common to all DB's:
void DbCreate_BUS_NMRX_RELATION::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
	dboList["IND_BNMR_TABLE_1"] = "create index IND_BNMR_TABLE_1 on BUS_NMRX_RELATION (BNMR_TABLE_1  )  ";  
	dboList["IND_BNMR_TABLE_2"] = "create index IND_BNMR_TABLE_2 on BUS_NMRX_RELATION (  BNMR_TABLE_2  )";  
	dboList["IND_BNMR_TABLE_KEY_1"] = "create index IND_BNMR_TABLE_KEY_1 on BUS_NMRX_RELATION (   BNMR_TABLE_KEY_ID_1  ) ";  
	dboList["IND_BNMR_TABLE_KEY_2"] = "create index IND_BNMR_TABLE_KEY_2 on BUS_NMRX_RELATION (    BNMR_TABLE_KEY_ID_2  ) ";  
	dboList["IND_BNMR_ROLE_ID"] = "create index IND_BNMR_ROLE_ID on BUS_NMRX_RELATION (       BNMR_ROLE_ID  ) ";  
	dboList["IND_BNMR_X_TABLE_ID"] = "create index IND_BNMR_X_TABLE_ID on BUS_NMRX_RELATION (       BNMR_X_TABLE_ID  ) ";  
	dboList["IND_BNMR_SYSTEM_ROLE"] = "create index IND_BNMR_SYSTEM_ROLE on BUS_NMRX_RELATION (       BNMR_SYSTEM_ROLE  ) ";  
}

void DbCreate_BUS_NMRX_RELATION::DefineTriggers(SQLDBObjectCreate &dboList)
{
	//create triggers TOFIX: foe each DB, must be different
	dboList["TRG_NMRX_RELATION"]="\
								  CREATE TRIGGER TRG_NMRX_RELATION FOR BUS_NMRX_RELATION\
								  ACTIVE AFTER DELETE AS \
								  BEGIN \
								  DELETE FROM BUS_CAL_INVITE WHERE OLD.BNMR_X_TABLE_ID = BCIV_ID;\
								  END\
								  ";
}