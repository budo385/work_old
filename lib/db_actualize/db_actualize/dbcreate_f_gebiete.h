#ifndef DBCREATE_F_GEBIETE_H
#define DBCREATE_F_GEBIETE_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_GEBIETE : public DbSqlTableCreation
{
public: 
	DbCreate_F_GEBIETE():DbSqlTableCreation(F_GEBIETE){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_GEBIETE_H
