#include "dbcreate_bus_dm_documents.h"


//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_DM_DOCUMENTS::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
											create table BUS_DM_DOCUMENTS (\
											BDMD_ID INTEGER not null,\
											BDMD_GLOBAL_ID VARCHAR(15) null,\
											BDMD_DAT_LAST_MODIFIED TIMESTAMP not null,\
											BDMD_DAT_CREATED DATETIME not null,\
											BDMD_NAME VARCHAR(250) not null,\
											BDMD_LOCATION VARCHAR(250) null,\
											BDMD_DOC_PATH LONGVARCHAR null,\
											BDMD_OPEN_PARAMETER VARCHAR(250) null,\
											BDMD_DESCRIPTION LONGVARCHAR null,\
											BDMD_APPLICATION_ID INTEGER null,\
											BDMD_TEMPLATE_ID INTEGER null,\
											BDMD_COMM_ENTITY_ID INTEGER not null, \
											BDMD_DOC_TYPE INTEGER not null,\
											BDMD_IS_DIRECTORY SMALLINT null,\
											BDMD_IS_CHECK_OUT SMALLINT null,\
											BDMD_CHECK_OUT_USER_ID INTEGER null,\
											BDMD_CHECK_OUT_DATE DATETIME null,\
											BDMD_TEMPLATE_FLAG SMALLINT null, \
											BDMD_SIZE INTEGER null, \
											BDMD_READ_ONLY SMALLINT null, \
											BDMD_CATEGORY VARCHAR(250) null, \
											BDMD_SET_TAG_FLAG SMALLINT null, \
											constraint BUS_DM_DOCUMENTS_PK primary key (BDMD_ID) )");

	lstSQL.append("\
											alter table BUS_DM_DOCUMENTS\
											add constraint BUS_DM_APP_DM_DOC_FK1 foreign key (\
											BDMD_APPLICATION_ID)\
											references BUS_DM_APPLICATIONS (\
											BDMA_ID) ON DELETE SET NULL ");

	lstSQL.append("\
											alter table BUS_DM_DOCUMENTS\
											add constraint BUS_DM_DM_DOC_FK1 foreign key (\
											BDMD_TEMPLATE_ID)\
											references BUS_DM_DOCUMENTS (\
											BDMD_ID) ON DELETE SET NULL ");


	lstSQL.append("\
											alter table BUS_DM_DOCUMENTS\
											add constraint CE_COMM_DM_DOC_FK1 foreign key (\
											BDMD_COMM_ENTITY_ID)\
											references CE_COMM_ENTITY (\
											CENT_ID)\
											ON DELETE CASCADE");

	lstSQL.append("\
											alter table BUS_DM_DOCUMENTS\
											add constraint BUS_DM_PERSON_FK1 foreign key (\
											BDMD_CHECK_OUT_USER_ID)\
											references BUS_PERSON (\
											BPER_ID) ON DELETE SET NULL ");

	
	//Delete columns.
	m_lstDeletedCols << "BDMD_IS_PRIVATE";

}



//probably common to all DB's:
void DbCreate_BUS_DM_DOCUMENTS::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();

	dboList["IND_DM_DOC_APP_ID"] = "create index IND_DM_DOC_APP_ID on BUS_DM_DOCUMENTS (BDMD_APPLICATION_ID  )";  
	dboList["IND_DM_DOC_COMM_ID"] = "create index IND_DM_DOC_COMM_ID on BUS_DM_DOCUMENTS (	BDMD_COMM_ENTITY_ID  )  ";
	dboList["IND_BDMD_TEMPLATE"] = "create index IND_BDMD_TEMPLATE on BUS_DM_DOCUMENTS (BDMD_TEMPLATE_ID)   ";
	dboList["IND_BDMD_CATEGORY"] = "create index IND_BDMD_CATEGORY on BUS_DM_DOCUMENTS ( BDMD_CATEGORY )";
	dboList["IND_BDMD_LAST_MODIFIED"] = "create index IND_BDMD_LAST_MODIFIED on BUS_DM_DOCUMENTS ( BDMD_DAT_LAST_MODIFIED )";
	dboList["IND_BDMD_DAT_CREATED"] = "create index IND_BDMD_DAT_CREATED on BUS_DM_DOCUMENTS ( BDMD_DAT_CREATED )";
}

void DbCreate_BUS_DM_DOCUMENTS::DefineTriggers(SQLDBObjectCreate &dboList)
{
	//create triggers TOFIX: foe each DB, must be different
	dboList["TRG_BUS_DM_DOCUMENTS_DELETE"]="\
										 CREATE TRIGGER TRG_BUS_DM_DOCUMENTS_DELETE FOR BUS_DM_DOCUMENTS\
										 ACTIVE AFTER DELETE AS \
										 BEGIN \
										 DELETE FROM CORE_ACC_USER_REC WHERE OLD.BDMD_ID = CUAR_RECORD_ID AND CUAR_TABLE_ID="+QVariant(BUS_DM_DOCUMENTS).toString()+";\
										 DELETE FROM CORE_ACC_GROUP_REC WHERE OLD.BDMD_ID = CGAR_RECORD_ID AND CGAR_TABLE_ID="+QVariant(BUS_DM_DOCUMENTS).toString()+";\
										 END\
										 ";
}

