#ifndef DBCREATE_BUS_CUSTOM_DATETIME_H
#define DBCREATE_BUS_CUSTOM_DATETIME_H


#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_BUS_CUSTOM_DATETIME : public DbSqlTableCreation
{
public:
	DbCreate_BUS_CUSTOM_DATETIME():DbSqlTableCreation(BUS_CUSTOM_DATETIME){};  //init table
	

private:
	//create generic Table def:
	void DefineFields(QStringList &lstSQL);

	//probably common to all DB's:
	void DefineIndexes(SQLDBObjectCreate &dboList);
};

#endif // DBCREATE_BUS_CUSTOM_DATETIME_H
