#ifndef DBCREATE_FF_DEF_FAKT_H
#define DBCREATE_FF_DEF_FAKT_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_FF_DEF_FAKT : public DbSqlTableCreation
{
public: 
	DbCreate_FF_DEF_FAKT():DbSqlTableCreation(FF_DEF_FAKT){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // FF_DEF_FAKT_H
