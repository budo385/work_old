#ifndef DBCREATE_SPC_PR_TYPE_H
#define DBCREATE_SPC_PR_TYPE_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_SPC_PR_TYPE : public DbSqlTableCreation
{
public: 
	DbCreate_SPC_PR_TYPE():DbSqlTableCreation(SPC_PR_TYPE){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // SPC_PR_TYPE_H
