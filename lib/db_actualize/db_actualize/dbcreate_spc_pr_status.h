#ifndef DBCREATE_SPC_PR_STATUS_H
#define DBCREATE_SPC_PR_STATUS_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_SPC_PR_STATUS : public DbSqlTableCreation
{
public: 
	DbCreate_SPC_PR_STATUS():DbSqlTableCreation(SPC_PR_STATUS){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif //  SPC_PR_STATUS_H
