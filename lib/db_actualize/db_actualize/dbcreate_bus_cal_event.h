#ifndef DBCREATE_BUS_CAL_EVENT_H
#define DBCREATE_BUS_CAL_EVENT_H


#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_BUS_CAL_EVENT : public DbSqlTableCreation
{
public:
	DbCreate_BUS_CAL_EVENT():DbSqlTableCreation(BUS_CAL_EVENT){};  //init table
	

private:
	//create generic Table def:
	void DefineFields(QStringList &lstSQL);

	//probably common to all DB's:
	void DefineIndexes(SQLDBObjectCreate &dboList);
};


#endif 
