#include "dbcreate_ce_comm_entity.h"



//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_CE_COMM_ENTITY::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
											create table CE_COMM_ENTITY (\
											CENT_ID INTEGER not null,\
											CENT_GLOBAL_ID VARCHAR(15) null,\
											CENT_DAT_LAST_MODIFIED TIMESTAMP not null,\
											CENT_CE_TYPE_ID INTEGER null,\
											CENT_SYSTEM_TYPE_ID INTEGER null,\
											CENT_OWNER_ID INTEGER null,\
											CENT_TASK_ID INTEGER null,\
											CENT_TOOLTIP VARCHAR(500) null,\
											CENT_DAT_CREATED DATETIME null,\
											constraint CE_COMM_ENTITY_PK primary key (CENT_ID) ); ");

	m_lstDeletedCols << "CENT_IS_PRIVATE";

	//constraint for table creation:
	lstSQL.append("\
											alter table CE_COMM_ENTITY\
											add constraint TYPE_CE_ENTITY_FK1 foreign key (\
											CENT_CE_TYPE_ID)\
											references CE_TYPE (\
											CET_ID)\
											");

	lstSQL.append("\
											alter table CE_COMM_ENTITY \
											add constraint BUS_PERSON_CE_COMM_ENTITY_FK1 foreign key (\
												CENT_OWNER_ID)\
											 references BUS_PERSON (\
												BPER_ID) ON DELETE SET NULL\
											");

	lstSQL.append("\
											alter table CE_COMM_ENTITY\
											add constraint TASKS_ENTITY_FK1 foreign key (\
											CENT_TASK_ID)\
											references BUS_TASKS (\
											BTKS_ID)  ON DELETE SET NULL \
											");


}



//probably common to all DB's:
void DbCreate_CE_COMM_ENTITY::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
	dboList["IND_CE_TYPE_ID"] = "create index IND_CE_TYPE_ID on CE_COMM_ENTITY (CENT_CE_TYPE_ID)";  
	dboList["IND_CENT_OWNER_ID"] = "create index IND_CENT_OWNER_ID on CE_COMM_ENTITY (CENT_OWNER_ID)  ";  
	dboList["IND_CENT_SYSTEM_TYPE_ID"] = "create index IND_CENT_SYSTEM_TYPE_ID on CE_COMM_ENTITY (CENT_SYSTEM_TYPE_ID)  ";  
	dboList["IND_CENT_TASK_ID"] = "create index IND_CENT_TASK_ID on CE_COMM_ENTITY (CENT_TASK_ID)  ";  
	
}




void DbCreate_CE_COMM_ENTITY::DefineTriggers(SQLDBObjectCreate &dboList)
{
	//create triggers TOFIX: foe each DB, must be different
	dboList["TRG_CE_COMM_ENTITY"]="\
									 CREATE TRIGGER TRG_CE_COMM_ENTITY FOR CE_COMM_ENTITY\
									 ACTIVE AFTER DELETE AS \
										 BEGIN \
										 DELETE FROM BUS_TASKS WHERE OLD.CENT_TASK_ID = BTKS_ID;\
										 DELETE FROM CORE_ACC_USER_REC WHERE OLD.CENT_ID = CUAR_RECORD_ID AND CUAR_TABLE_ID="+QVariant(CE_COMM_ENTITY).toString()+";\
										 DELETE FROM bus_nmrx_relation WHERE OLD.CENT_ID = BNMR_TABLE_KEY_ID_1 AND BNMR_TABLE_1="+QVariant(CE_COMM_ENTITY).toString()+";\
										 DELETE FROM bus_nmrx_relation WHERE OLD.CENT_ID = BNMR_TABLE_KEY_ID_2 AND BNMR_TABLE_2="+QVariant(CE_COMM_ENTITY).toString()+";\
										 END\
										 ";
	
	/*
	//insert new empty record in ACC_USER_REC table with max rights
	dboList["TRG_CE_COMM_ENTITY_INSERT"]="\
									  CREATE TRIGGER TRG_CE_COMM_ENTITY_INSERT FOR CE_COMM_ENTITY\
									  ACTIVE AFTER INSERT AS \
										 BEGIN \
										 INSERT INTO CORE_ACC_USER_REC (CUAR_ID,CUAR_DAT_LAST_MODIFIED,CUAR_TABLE_ID,CUAR_RECORD_ID,CUAR_CHILD_INHERIT,CUAR_CHILD_COMM_INHERIT,CUAR_GROUP_DEPENDENT,CUAR_RIGHT)\
										 VALUES((SELECT GEN_ID(CUAR_ID_SEQ, 1) FROM RDB$DATABASE),CURRENT_TIMESTAMP,"+QVariant(CE_COMM_ENTITY).toString()+",NEW.CENT_ID,0,0,0,3);\
										 END\
										 ";

										 */

}