#include "dbcreate_bus_cm_creditor.h"



//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_CM_CREDITOR::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
											create table BUS_CM_CREDITOR ( \
											BCMC_ID INTEGER not null,\
											BCMC_GLOBAL_ID VARCHAR(15) null,\
											BCMC_DAT_LAST_MODIFIED TIMESTAMP not null,\
											BCMC_CONTACT_ID INTEGER not null,\
											BCMC_CREDITORCODE VARCHAR(50) null,\
											BCMC_CREDITORACCOUNT VARCHAR(50) null,\
											BCMC_DESCRIPTION LONGVARCHAR null, constraint BUS_CM_CREDITOR_PK primary key (BCMC_ID) )");  


	//constraint for table creation:
	lstSQL.append("\
											alter table BUS_CM_CREDITOR\
											add constraint BUS_CONT_CREDITOR_FK1 foreign key (\
											BCMC_CONTACT_ID)\
											references BUS_CM_CONTACT (\
											BCNT_ID) \
											ON DELETE CASCADE");
}



//probably common to all DB's:
void DbCreate_BUS_CM_CREDITOR::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
	dboList["IND_BUS_CM_CREDITOR"] = "create index IND_BUS_CM_CREDITOR on BUS_CM_CREDITOR (BCMC_CONTACT_ID)";  
}


