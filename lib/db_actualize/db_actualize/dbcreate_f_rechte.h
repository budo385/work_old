#ifndef DBCREATE_F_RECHTE_H
#define DBCREATE_F_RECHTE_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_RECHTE : public DbSqlTableCreation
{
public: 
	DbCreate_F_RECHTE():DbSqlTableCreation(F_RECHTE){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_RECHTE_H
