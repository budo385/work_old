#include "dbcreate_core_locking.h"



//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------

void DbCreate_CORE_LOCKING::DefineFields(QStringList &lstSQL)
{

	//Table shema statements for ODBC.
	lstSQL.append("\
	create table CORE_LOCKING (\
	CORL_ID INTEGER not null,\
	CORL_GLOBAL_ID VARCHAR(15) null,\
	CORL_DAT_LAST_MODIFIED TIMESTAMP not null,\
	CORL_RECORD_ID INTEGER not null,\
	CORL_RESOURCE_ID VARCHAR(60) not null,\
	CORL_SESSION_ID VARCHAR(50) not null,\
	CORL_TABLE_ID INTEGER not null,\
	CORL_USER_SESSION_ID INTEGER not null, constraint CORE_LOCKING_PK primary key (CORL_ID) ) ");


	//constraint for table creation:
	lstSQL.append("\
											alter table CORE_LOCKING\
											add constraint SESSION_LOCK_FK1 foreign key (\
											CORL_USER_SESSION_ID)\
											references CORE_USER_SESSION (\
											COUS_ID) ON DELETE CASCADE");

}




/* Probably index defintion is same for all DB's */
void DbCreate_CORE_LOCKING::DefineIndexes(SQLDBObjectCreate &dboList)
{

	//add index defintion by its name (important)
	dboList.clear();
	dboList["IND_RECORD_TABLE"]		="create unique index IND_RECORD_TABLE on CORE_LOCKING (CORL_TABLE_ID,CORL_RECORD_ID)";  
	dboList["IND_CORL_SESSION_ID"]	="create index IND_CORL_SESSION_ID on CORE_LOCKING (CORL_SESSION_ID)";  
	dboList["IND_CORL_RESOURCE_ID"]	="create index IND_CORL_RESOURCE_ID on CORE_LOCKING (CORL_RESOURCE_ID) ";  
}









