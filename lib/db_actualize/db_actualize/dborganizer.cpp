#include "dborganizer.h"
#include "common/common/config.h"
#include "common/common/config_version_sc.h"
#include "common/common/authenticator.h"
#include "bus_core/bus_core/accessrightsorganizer.h"
#include "bus_core/bus_core/accessrightset.h"
#include "bus_core/bus_core/accessright.h"
#include "bus_core/bus_core/optionsandsettingsorganizer.h"
#include "db/db/dbsimpleorm.h"
#include "common/common/authenticator.h"
#include "bus_core/bus_core/contacttypemanager.h"
#include "bus_core/bus_core/formatphone.h"
#include "bus_core/bus_core/hierarchicalhelper.h"
#include "accessrightsmanipulator.h"
#include "bus_core/bus_core/optionsandsettingsmanager.h"
#include "db_core/db_core/db_drivers.h"
#include "db/db/dbomanager_oracle.h"
#include "db/db/dbomanager_mysql.h"
#include "db/db/dbomanager_firebird.h"
#include "bus_core/bus_core/countries.h"
#include "trans/trans/xmlutil.h"
#include "bus_core/bus_core/globalconstants.h"
#include "common/common/datahelper.h"
#include <QSqlField>
#include <QFile>
#include <QDir>
#include <QFileInfoList>
#include <QCoreApplication>
#include <QProcess>
#include "common/common/sleeper.h"
#include "db/db/dbconnectionsetup.h"
#include "common/common/csvimportfile.h"

//MR: simple replacement for MainEntitySelectionController for two field lookup (loading only those fields)
class SimpleTableCache {
public:
	SimpleTableCache(){};
	bool Load(const QString &strTable, const QString &strKeyField, const QString &strValueField, Status &pStatus, DbSqlManager *pManager);
	QVariant GetValue(const QVariant &key);

protected:
	DbRecordSet m_data;
};

bool SimpleTableCache::Load(const QString &strTable, const QString &strKeyField, const QString &strValueField, Status &pStatus, DbSqlManager *pManager)
{
	DbSqlQuery *pQuery = new DbSqlQuery(pStatus, pManager);
	if(!pStatus.IsOK()){ 
		if(pQuery) delete pQuery; 
		return false; 
	}

	QString strQuery = QString("SELECT %1, %2 FROM %3").arg(strKeyField).arg(strValueField).arg(strTable);
	if(pQuery->Execute(pStatus, strQuery))
	{
		pQuery->FetchData(m_data);					//read all by names (if not said other).
		if(pQuery) delete pQuery;
		return true;
	}		

	if(pQuery)	delete pQuery;
	return false;
}

QVariant SimpleTableCache::GetValue(const QVariant &key)
{
	int nRow = m_data.find(0, key, true);
	if(nRow >= 0){
		return m_data.getDataRef(nRow, 1);
	}
	return QVariant();
}


/*!
Constructor: save external Db manager
\param pManager			- pointer to the Db Manager
*/
DbOrganizer::DbOrganizer(DbSqlManager *pManager,QString strBackupDirPath,bool bShutDownDatabase)
{

	m_bShutDownDatabaseWhenBackup=bShutDownDatabase;
	m_pManager=pManager;
	m_DbObjectManager=NULL;
	m_bRebuildTables=false;
	m_pDb=NULL;

	//make instance of proper DboManager:
	if(m_pManager->GetDbType()==DBTYPE_ORACLE)
		m_DbObjectManager= new DbObjectManager_Oracle(pManager->GetDbSettings());
	else if (m_pManager->GetDbType()==DBTYPE_MYSQL)
		m_DbObjectManager= new DbObjectManager_MySQL(pManager->GetDbSettings());
	else if (m_pManager->GetDbType()==DBTYPE_FIREBIRD)
		m_DbObjectManager= new DbObjectManager_FireBird(pManager->GetDbSettings());
	else
		Q_ASSERT_X(false,"DBO ORGANIZER", "Database object manager doesnt exists for specified base");

	//determine backup directory path: default: app path/backup
	if (strBackupDirPath.isEmpty())
	{
		m_strBackupDirPath=QCoreApplication::applicationDirPath();
		QDir dir(m_strBackupDirPath);
		if (!dir.exists("Backup"))
		{
			if(dir.mkdir("Backup"))
				m_strBackupDirPath+="/Backup";
		}
		else
			m_strBackupDirPath+="/Backup";
	}
	else
		m_strBackupDirPath=strBackupDirPath;

	//instance all table objects
	m_nCurrentDatabaseVersion=0;
	Initialize();
	InitializeBusinessProcedures();
	Initialize_SPC();
	InitializeBusinessProcedures_SPC();

}


/* release memory */
DbOrganizer::~DbOrganizer()
{
	int lstTableCreatorSize = m_lstTableCreator.size();
	for(int i=0; i < lstTableCreatorSize; ++i)
		delete m_lstTableCreator.at(i);

	if(m_DbObjectManager!=NULL) delete m_DbObjectManager;
}


//------------------------------------------------------
//				STORE YOUR TABLE INFO HERE
//------------------------------------------------------


/*!
	Put your table object here, choose sequence
	IMPORTANT: put your table objects in reverse order (first childs then parents)
*/
void DbOrganizer::Initialize()
{
	//---------------------
	//		SC tables
	//---------------------
	m_lstTableCreator << new DbCreate_BUS_BIG_PICTURE;
	
	m_lstTableCreator <<  new DbCreate_BUS_ORGANIZATIONS;
	m_lstTableCreator <<  new DbCreate_BUS_COST_CENTERS;
	m_lstTableCreator <<  new DbCreate_BUS_DEPARTMENTS;
	m_lstTableCreator <<  new DbCreate_BUS_PROJECTS;
	m_lstTableCreator <<  new DbCreate_CORE_APP_SRV_SESSION;
	m_lstTableCreator <<  new DbCreate_CORE_USER;
	m_lstTableCreator <<  new DbCreate_CORE_USER_SESSION;
	m_lstTableCreator <<  new DbCreate_CORE_LOCKING;
	m_lstTableCreator <<  new DbCreate_CORE_EVENT_LOG;
	m_lstTableCreator <<  new DbCreate_CORE_IPACCESSLIST;
	m_lstTableCreator <<  new DbCreate_BUS_PERSONROLE;
	m_lstTableCreator <<  new DbCreate_CORE_USERROLE;
	m_lstTableCreator <<  new DbCreate_CORE_ACCESSRIGHTS;
	m_lstTableCreator <<  new DbCreate_CORE_ACCRSETROLE;
	m_lstTableCreator <<  new DbCreate_CORE_ACCRSET;
	m_lstTableCreator <<  new DbCreate_CORE_ROLE;
	m_lstTableCreator << new DbCreate_BUS_CM_ADDRESS_TYPES;
	m_lstTableCreator << new DbCreate_BUS_CM_CREDITOR;
	m_lstTableCreator << new DbCreate_BUS_CM_ADDRESS;
	m_lstTableCreator << new DbCreate_BUS_CM_EMAIL;
	m_lstTableCreator << new DbCreate_BUS_CM_INTERNET;
	m_lstTableCreator << new DbCreate_BUS_CM_JOURNAL;
	m_lstTableCreator << new DbCreate_BUS_CM_PAYMENT_CONDITIONS;
	m_lstTableCreator << new DbCreate_BUS_CM_PHONE;
	m_lstTableCreator << new DbCreate_BUS_CM_PICTURE;
	m_nBUS_CM_PICTUREIdx=m_lstTableCreator.size()-1;		//store Idx

	m_lstTableCreator << new DbCreate_BUS_CM_DEBTOR;
	m_lstTableCreator << new DbCreate_BUS_CM_TYPES;
	m_lstTableCreator << new DbCreate_BUS_CM_ADDRESS_SCHEMAS;
	m_lstTableCreator << new DbCreate_BUS_CM_CONTACT;
	m_lstTableCreator << new DbCreate_BUS_TASKS;
	m_lstTableCreator << new DbCreate_BUS_VOICECALLS;
	m_lstTableCreator << new DbCreate_BUS_EMAIL;
	m_lstTableCreator << new DbCreate_BUS_EMAIL_ATTACHMENT;
	m_lstTableCreator << new DbCreate_CE_EVENT;
	m_lstTableCreator << new DbCreate_CE_TYPE;
	m_lstTableCreator << new DbCreate_CE_EVENT_TYPE;
	m_lstTableCreator << new DbCreate_CE_COMM_ENTITY;	
	m_lstTableCreator << new DbCreate_BUS_COMM_VIEW_SETTINGS;
	m_lstTableCreator << new DbCreate_BUS_COMM_VIEW;
	m_lstTableCreator <<  new DbCreate_BUS_PERSON;
	m_lstTableCreator <<  new DbCreate_BUS_NMRX_ROLE;
	m_lstTableCreator <<  new DbCreate_BUS_NMRX_RELATION;
	m_lstTableCreator <<  new DbCreate_BUS_OPT_GRID_VIEWS;
	m_lstTableCreator <<  new DbCreate_BUS_CM_GROUP;
	m_lstTableCreator <<  new DbCreate_BUS_GROUP_ITEMS;
	m_lstTableCreator <<  new DbCreate_BUS_GROUP_TREE;
	m_lstTableCreator <<  new DbCreate_BUS_OPT_SETTINGS;
	m_lstTableCreator <<  new DbCreate_BUS_DM_APPLICATIONS;
	m_lstTableCreator <<  new DbCreate_BUS_DM_USER_PATHS;
	m_lstTableCreator <<  new DbCreate_BUS_DM_DOCUMENTS;
	m_lstTableCreator <<  new DbCreate_BUS_DM_REVISIONS;
	m_lstTableCreator <<  new DbCreate_CORE_DATABASE_INFO;
	m_lstTableCreator <<  new DbCreate_CORE_FTP_SERVERS;
	m_lstTableCreator <<  new DbCreate_CORE_IMPORT_EXPORT;
	m_lstTableCreator <<  new DbCreate_CORE_ACC_GROUP_REC;
	m_lstTableCreator <<  new DbCreate_CORE_ACC_USER_REC;
	m_lstTableCreator <<  new DbCreate_BUS_STORED_PROJECT_LIST;
	m_lstTableCreator <<  new DbCreate_BUS_STORED_PROJECT_LIST_DATA;
	m_lstTableCreator <<  new DbCreate_BUS_SMS;
	m_lstTableCreator <<  new DbCreate_BUS_CAL_RESERVATION;
	m_lstTableCreator <<  new DbCreate_BUS_CAL_RESOURCE;
	m_lstTableCreator <<  new DbCreate_BUS_CAL_INVITE;
	m_lstTableCreator <<  new DbCreate_BUS_CAL_EVENT;
	m_lstTableCreator <<  new DbCreate_BUS_CAL_EVENT_PART;
	m_lstTableCreator <<  new DbCreate_BUS_CAL_OPTIONS;
	m_lstTableCreator <<  new DbCreate_BUS_CAL_BREAKS;
	m_lstTableCreator <<  new DbCreate_BUS_CAL_EV_PART_OUID;
	m_lstTableCreator <<  new DbCreate_BUS_REMINDERS;
	m_lstTableCreator <<  new DbCreate_BUS_DM_SERVER_FILE;
	m_lstTableCreator <<  new DbCreate_BUS_DM_META_DATA;
	m_lstTableCreator <<  new DbCreate_BUS_CAL_VIEW;
	m_lstTableCreator <<  new DbCreate_BUS_CAL_VIEW_ENTITIES;
	m_lstTableCreator <<  new DbCreate_BUS_CAL_VIEW_CE_TYPES;
	m_lstTableCreator <<  new DbCreate_SPC_ACTIVITY;
	m_lstTableCreator <<  new DbCreate_SPC_INDUSTRY_GROUP;
	m_lstTableCreator <<  new DbCreate_SPC_PR_AREA;
	m_lstTableCreator <<  new DbCreate_SPC_PR_STATUS;
	m_lstTableCreator <<  new DbCreate_SPC_PR_TYPE;
	m_lstTableCreator <<  new DbCreate_SPC_PROCESS;
	m_lstTableCreator <<  new DbCreate_SPC_RATE_CAT;
	m_lstTableCreator <<  new DbCreate_SPC_SECTOR;
	m_lstTableCreator <<  new DbCreate_SPC_SERVICE_TYPE;

	m_lstTableCreator <<  new DbCreate_BUS_CUSTOM_BOOL;
	m_lstTableCreator <<  new DbCreate_BUS_CUSTOM_DATE;
	m_lstTableCreator <<  new DbCreate_BUS_CUSTOM_DATETIME;
	m_lstTableCreator <<  new DbCreate_BUS_CUSTOM_FIELDS;
	m_lstTableCreator <<  new DbCreate_BUS_CUSTOM_FLOAT;
	m_lstTableCreator <<  new DbCreate_BUS_CUSTOM_INT;
	m_lstTableCreator <<  new DbCreate_BUS_CUSTOM_LONGTEXT;
	m_lstTableCreator <<  new DbCreate_BUS_CUSTOM_SELECTIONS;
	m_lstTableCreator <<  new DbCreate_BUS_CUSTOM_TEXT;

	m_lstTableCreator <<  new DbCreate_BUS_PUSH_MESSAGE;
	m_lstTableCreator <<  new DbCreate_BUS_PUSH_TOKEN_PERSON;
	m_lstTableCreator <<  new DbCreate_BUS_EMAIL_BINARY;
}

void DbOrganizer::Initialize_SPC()
{

	//this two must be firsT:
	m_lstTableCreatorSPC <<  new DbCreate_ZS_TEXT_FIELDS;
	m_lstTableCreatorSPC <<  new DbCreate_ZS_BINARY_FIELDS;
	

	//---------------------
	//		SPC tables
	//---------------------

	m_lstTableCreatorSPC <<  new DbCreate_F_ABT_MITARB;
	m_lstTableCreatorSPC <<  new DbCreate_F_ABT_RECHTE;
	m_lstTableCreatorSPC <<  new DbCreate_F_ABTEILUNG;
	m_lstTableCreatorSPC <<  new DbCreate_F_AGENTS;
	m_lstTableCreatorSPC <<  new DbCreate_F_BANKVERBINDUNGEN;
	m_lstTableCreatorSPC <<  new DbCreate_F_BATCH_JOBS;
	m_lstTableCreatorSPC <<  new DbCreate_F_BEL_PROJ_LISTEN;
	m_lstTableCreatorSPC <<  new DbCreate_F_COUNTERS;
	m_lstTableCreatorSPC <<  new DbCreate_F_COUNTRY;
	m_lstTableCreatorSPC <<  new DbCreate_F_DATENAKTUALISIERUNG;
	m_lstTableCreatorSPC <<  new DbCreate_F_DEBI_BUCH_RECH;
	m_lstTableCreatorSPC <<  new DbCreate_F_DEBI_BUCH_ZAHL;
	m_lstTableCreatorSPC <<  new DbCreate_F_DEFAULTS;
	m_lstTableCreatorSPC <<  new DbCreate_F_DELETE;
	m_lstTableCreatorSPC <<  new DbCreate_F_EXCHANGEFORMATS;
	m_lstTableCreatorSPC <<  new DbCreate_F_EXT_APPLICATIONS;
	m_lstTableCreatorSPC <<  new DbCreate_F_FAKT_PERS_LEIST;
	m_lstTableCreatorSPC <<  new DbCreate_F_FAKT_PLAN;
	m_lstTableCreatorSPC <<  new DbCreate_F_FAKT_PLAN_LOG;
	m_lstTableCreatorSPC <<  new DbCreate_F_FAKT_UMLAGEN;
	m_lstTableCreatorSPC <<  new DbCreate_F_FAKTURA;
	m_lstTableCreatorSPC <<  new DbCreate_F_FAKTURA_EIN;
	m_lstTableCreatorSPC <<  new DbCreate_F_FERIEN;
	m_lstTableCreatorSPC <<  new DbCreate_F_FUNKTIONEN;
	m_lstTableCreatorSPC <<  new DbCreate_F_GEBIETE;
	m_lstTableCreatorSPC <<  new DbCreate_F_HONORARKLASSE;
	m_lstTableCreatorSPC <<  new DbCreate_F_IND_PROJNAME;
	m_lstTableCreatorSPC <<  new DbCreate_F_INOUTTIMES;
	m_lstTableCreatorSPC <<  new DbCreate_F_KA_KLASSEN;
	m_lstTableCreatorSPC <<  new DbCreate_F_KNTRL_PERIODE;
	m_lstTableCreatorSPC <<  new DbCreate_F_KOMPENSATION;
	m_lstTableCreatorSPC <<  new DbCreate_F_KONTEN;
	m_lstTableCreatorSPC <<  new DbCreate_F_KONTENPLAN;
	m_lstTableCreatorSPC <<  new DbCreate_F_KONTO_NAMEN;
	m_lstTableCreatorSPC <<  new DbCreate_F_KOSTENARTEN;
	m_lstTableCreatorSPC <<  new DbCreate_F_KS_BUDGET;
	m_lstTableCreatorSPC <<  new DbCreate_F_LEISTUNGSARTEN;
	m_lstTableCreatorSPC <<  new DbCreate_F_LETZTER_EXP;
	m_lstTableCreatorSPC <<  new DbCreate_F_LICENCE_RESERVATION;
	m_lstTableCreatorSPC <<  new DbCreate_F_LOCKING;
	m_lstTableCreatorSPC <<  new DbCreate_F_MITARB_BUDGET;
	m_lstTableCreatorSPC <<  new DbCreate_F_MODULE_LICENCES;
	m_lstTableCreatorSPC <<  new DbCreate_F_NK_BUDGET;
	m_lstTableCreatorSPC <<  new DbCreate_F_OBJEKTNUMMERN;
	m_lstTableCreatorSPC <<  new DbCreate_F_ORGANISATION;
	m_lstTableCreatorSPC <<  new DbCreate_F_PERIODENART;
	m_lstTableCreatorSPC <<  new DbCreate_F_PERS_FUNKTION;
	m_lstTableCreatorSPC <<  new DbCreate_F_PERSPLLINES;
	m_lstTableCreatorSPC <<  new DbCreate_F_PERSPROJLISTS;
	m_lstTableCreatorSPC <<  new DbCreate_F_PFLICHTFELDER;
	m_lstTableCreatorSPC <<  new DbCreate_F_PR_CACHE;
	m_lstTableCreatorSPC <<  new DbCreate_F_PR_STATUS;
	m_lstTableCreatorSPC <<  new DbCreate_F_PREASSIGN;
	m_lstTableCreatorSPC <<  new DbCreate_F_PRINT_LABELS;
	m_lstTableCreatorSPC <<  new DbCreate_F_PROJ_BAUHERR;
	m_lstTableCreatorSPC <<  new DbCreate_F_PROJ_BEREICHE;
	m_lstTableCreatorSPC <<  new DbCreate_F_PROJ_BUDGETKORREKTUR;
	m_lstTableCreatorSPC <<  new DbCreate_F_PROJ_MITARB;
	m_lstTableCreatorSPC <<  new DbCreate_F_PROJ_PHASEN;
	m_lstTableCreatorSPC <<  new DbCreate_F_PROJ_RECHTE;
	m_lstTableCreatorSPC <<  new DbCreate_F_PROJ_RESTAUFWAND;
	m_lstTableCreatorSPC <<  new DbCreate_F_PROJ_STRUKT;
	m_lstTableCreatorSPC <<  new DbCreate_F_PROJECTUSERDEFINED;
	m_lstTableCreatorSPC <<  new DbCreate_F_PROJECTUSERDEFINED_CHAR;
	m_lstTableCreatorSPC <<  new DbCreate_F_PROJECTUSERDEFINED_DATE;
	m_lstTableCreatorSPC <<  new DbCreate_F_PROJECTUSERDEFINED_FLOAT;
	m_lstTableCreatorSPC <<  new DbCreate_F_PROJECTUSERDEFINEDFIELDS;
	m_lstTableCreatorSPC <<  new DbCreate_F_PROJECTUSERDEFINEDGUIMAP;
	m_lstTableCreatorSPC <<  new DbCreate_F_PROJEKTART;
	m_lstTableCreatorSPC <<  new DbCreate_F_PROJEKTLISTEN;
	m_lstTableCreatorSPC <<  new DbCreate_F_PROZESSE;
	m_lstTableCreatorSPC <<  new DbCreate_F_RECHTE;
	m_lstTableCreatorSPC <<  new DbCreate_F_RECORD_ID;
	m_lstTableCreatorSPC <<  new DbCreate_F_REPORTLISTEN;
	m_lstTableCreatorSPC <<  new DbCreate_F_REPORTPROFIL;
	m_lstTableCreatorSPC <<  new DbCreate_F_REPORTSETS;
	m_lstTableCreatorSPC <<  new DbCreate_F_SOLLZEIT_KLASSEN;
	m_lstTableCreatorSPC <<  new DbCreate_F_SOLLZEITEN;
	m_lstTableCreatorSPC <<  new DbCreate_F_STD_KONTROLLE;
	m_lstTableCreatorSPC <<  new DbCreate_F_STEMPELUHR;
	m_lstTableCreatorSPC <<  new DbCreate_F_SYNCHRO;
	m_lstTableCreatorSPC <<  new DbCreate_F_SZ_EXCEPTIONS;
	m_lstTableCreatorSPC <<  new DbCreate_F_TAET_LIST_ENTRY;
	m_lstTableCreatorSPC <<  new DbCreate_F_TAET_LIST_NAMES;
	m_lstTableCreatorSPC <<  new DbCreate_F_TAETIGKEITEN;
	m_lstTableCreatorSPC <<  new DbCreate_F_TARIFE;
	m_lstTableCreatorSPC <<  new DbCreate_F_TARIFKATEG;
	m_lstTableCreatorSPC <<  new DbCreate_F_TIMELINE_ASSOC;
	m_lstTableCreatorSPC <<  new DbCreate_F_TIMELINE_CHAR200;
	m_lstTableCreatorSPC <<  new DbCreate_F_TIMELINE_CHAR50;
	m_lstTableCreatorSPC <<  new DbCreate_F_TIMELINE_DATE;
	m_lstTableCreatorSPC <<  new DbCreate_F_TIMELINE_FLOAT;
	m_lstTableCreatorSPC <<  new DbCreate_F_TIMELINE_LONG;
	m_lstTableCreatorSPC <<  new DbCreate_F_TIMELINE_SHORT;
	m_lstTableCreatorSPC <<  new DbCreate_F_TIMESERVER;
	m_lstTableCreatorSPC <<  new DbCreate_F_TK_KA_KLASSEN;
	m_lstTableCreatorSPC <<  new DbCreate_F_TK_REFERENCES;
	m_lstTableCreatorSPC <<  new DbCreate_F_UDF_BOOLEAN;
	m_lstTableCreatorSPC <<  new DbCreate_F_UDF_CHAR;
	m_lstTableCreatorSPC <<  new DbCreate_F_UDF_DATE;
	m_lstTableCreatorSPC <<  new DbCreate_F_UDF_FIELD;
	m_lstTableCreatorSPC <<  new DbCreate_F_UDF_FLOAT;
	m_lstTableCreatorSPC <<  new DbCreate_F_UDF_INTEGER;
	m_lstTableCreatorSPC <<  new DbCreate_F_UDF_POSITION_GRID;
	m_lstTableCreatorSPC <<  new DbCreate_F_UDF_POSITION_REP;
	m_lstTableCreatorSPC <<  new DbCreate_F_UDF_POSITION_WIN;
	m_lstTableCreatorSPC <<  new DbCreate_F_UDF_PREDEF_FILL;
	m_lstTableCreatorSPC <<  new DbCreate_F_UDF_TEXT;
	m_lstTableCreatorSPC <<  new DbCreate_F_UDF_VIEW;
	m_lstTableCreatorSPC <<  new DbCreate_F_USER_APPLICATIONS;
	m_lstTableCreatorSPC <<  new DbCreate_F_USER_SESSIONS;
	m_lstTableCreatorSPC <<  new DbCreate_F_USER_SETTINGS;
	m_lstTableCreatorSPC <<  new DbCreate_F_VERSICHERUNGSKATEGORIEN;
	m_lstTableCreatorSPC <<  new DbCreate_F_WAEHRUNG;
	m_lstTableCreatorSPC <<  new DbCreate_F_WIRTSCH_GRUPPE;
	m_lstTableCreatorSPC <<  new DbCreate_F_WT_CP_SCHEMA;
	m_lstTableCreatorSPC <<  new DbCreate_F_WT_DAY_SCHEMA;
	m_lstTableCreatorSPC <<  new DbCreate_F_ZAHL_EINGANG;
	m_lstTableCreatorSPC <<  new DbCreate_F_ZAHLUNGSARTEN;
	m_lstTableCreatorSPC <<  new DbCreate_FASSIGN;
	m_lstTableCreatorSPC <<  new DbCreate_FD_DOK_VORLAGEN;
	m_lstTableCreatorSPC <<  new DbCreate_FD_DOKUMENTE;
	m_lstTableCreatorSPC <<  new DbCreate_FD_DOKUMENTENARTEN;
	m_lstTableCreatorSPC <<  new DbCreate_FF_BAUSTEINE;
	m_lstTableCreatorSPC <<  new DbCreate_FF_DEF_FAKT;
	m_lstTableCreatorSPC <<  new DbCreate_FF_DEF_FAKT_POS;
	m_lstTableCreatorSPC <<  new DbCreate_FF_RECHNUNG_POS;
	m_lstTableCreatorSPC <<  new DbCreate_FL_DEFAULT;
	m_lstTableCreatorSPC <<  new DbCreate_FL_DEFAULT2;
	m_lstTableCreatorSPC <<  new DbCreate_FPERSON;
	m_lstTableCreatorSPC <<  new DbCreate_FPROJCT;
	m_lstTableCreatorSPC <<  new DbCreate_FSA_ADRESSEN;


}

//------------------------------------------------------
//				STORE YOUR BUSINESS FUNCTS HERE
//------------------------------------------------------

/*!
	Init BO functions:
	NOTE:
	- Functions must be named as DBVER_ORDER_FunctName, where DBVER is db version and order is order in which funct is execute within one version:
	- Functions must only have 1 param (&status)
	- Functions are executed in order: version, order

	E.G: 
	1_0_CreateAdmin(Status &pStatus);     //version=1, order =0
	1_1_CreateOptions(Status &pStatus);	  //version=1, order =1	
	2_0_CreateSettings(Status &pStatus);  //version=2, order =0	
	....
*/
void DbOrganizer::InitializeBusinessProcedures()
{
	mFunctList.insert("f0001_00_CreateAdminAccount",&DbOrganizer::f0001_00_CreateAdminAccount);
	mFunctList.insert("f0001_01_CreateHiddenSystemAccount",&DbOrganizer::f0001_01_CreateHiddenSystemAccount);
	mFunctList.insert("f0001_02_CreateDefaultOrganization",&DbOrganizer::f0001_02_CreateDefaultOrganization);
	mFunctList.insert("f0001_03_CreateCoreAccessRightSet",&DbOrganizer::f0001_03_CreateCoreAccessRightSet);
	mFunctList.insert("f0014_01_UpdateCE_COMM_ENTITY",&DbOrganizer::f0014_01_UpdateCE_COMM_ENTITY);
	mFunctList.insert("f0017_01_UpdateHiddenSystemAccount",&DbOrganizer::f0017_01_UpdateHiddenSystemAccount);
	mFunctList.insert("f0017_03_UpdateDatabaseInfo",&DbOrganizer::f0017_03_UpdateDatabaseInfo);
	mFunctList.insert("f0017_04_FixSearchPhone",&DbOrganizer::f0017_04_FixSearchPhone);
	mFunctList.insert("f0021_00_FixCeTypeTable",&DbOrganizer::f0021_00_FixCeTypeTable);		
	mFunctList.insert("f0022_00_FixDuplicateBigPictureID",&DbOrganizer::f0022_00_FixDuplicateBigPictureID);		
	mFunctList.insert("f0024_00_CreateDemoKey",&DbOrganizer::f0024_00_CreateDemoKey);		
	mFunctList.insert("f0026_00_CreateContactPhonets",&DbOrganizer::f0026_00_CreateContactPhonets);		
	mFunctList.insert("f0027_00_InitEmailIDChecksum",&DbOrganizer::f0027_00_InitEmailIDChecksum);		
	mFunctList.insert("f0030_00_InitImportExportSettings",&DbOrganizer::f0030_00_InitImportExportSettings);		
	mFunctList.insert("f0032_00_FixSearchPhone_2",&DbOrganizer::f0032_00_FixSearchPhone_2);		
	mFunctList.insert("f0035_00_FixHierachicalTables",&DbOrganizer::f0035_00_FixHierachicalTables);
	mFunctList.insert("f0036_00_CreateSimpleAccessRightSet",&DbOrganizer::f0036_00_CreateSimpleAccessRightSet);
	mFunctList.insert("f0042_00_FixCodePageOnLongVarChar",&DbOrganizer::f0042_00_FixCodePageOnLongVarChar);
	mFunctList.insert("f0044_00_UpdatePersonOrganization",&DbOrganizer::f0044_00_UpdatePersonOrganization);
	mFunctList.insert("f0044_01_UpdateSystemPhoneTypes",&DbOrganizer::f0044_01_UpdateSystemPhoneTypes);
	mFunctList.insert("f0046_00_UpdatePhonets",&DbOrganizer::f0046_00_UpdatePhonets);
	mFunctList.insert("f0047_00_FixHierachicalTables",&DbOrganizer::f0047_00_FixHierachicalTables);
	mFunctList.insert("f0047_01_FixPrivateContact",&DbOrganizer::f0047_01_FixPrivateContact);
	mFunctList.insert("f0051_00_UpdateLocationPhonet",&DbOrganizer::f0051_00_UpdateLocationPhonet);
	mFunctList.insert("f0052_00_UpdateCodePredial",&DbOrganizer::f0052_00_UpdateCodePredial);
	mFunctList.insert("f0053_00_UpdateBinaryFields",&DbOrganizer::f0053_00_UpdateBinaryFields);
	mFunctList.insert("f0054_00_UpdateAddressCodeField",&DbOrganizer::f0054_00_UpdateAddressCodeField);
	mFunctList.insert("f0059_00_CreateUARRecords",&DbOrganizer::f0059_00_CreateUARRecords);
	mFunctList.insert("f0066_00_RoleUpgrade",&DbOrganizer::f0066_00_RoleUpgrade);
	mFunctList.insert("f0070_00_TemplateCleanUp",&DbOrganizer::f0070_00_TemplateCleanUp);
	mFunctList.insert("f0071_00_RoleUpgrade",&DbOrganizer::f0071_00_RoleUpgrade);
	mFunctList.insert("f0076_00_FixDocumentRevisionTable",&DbOrganizer::f0076_00_FixDocumentRevisionTable);
	mFunctList.insert("f0077_00_UpdateTaskPriorityField",&DbOrganizer::f0077_00_UpdateTaskPriorityField);
	mFunctList.insert("f0078_00_FixDocumentSize",&DbOrganizer::f0078_00_FixDocumentSize);
	mFunctList.insert("f0079_00_FixDocumentNoteSize",&DbOrganizer::f0079_00_FixDocumentNoteSize);
	mFunctList.insert("f0080_00_FixItalianPhones",&DbOrganizer::f0080_00_FixItalianPhones);
	mFunctList.insert("f0081_00_FixItalianPhones_SagaContinues",&DbOrganizer::f0081_00_FixItalianPhones_SagaContinues);
	mFunctList.insert("f0083_00_UpdateNMRXData",&DbOrganizer::f0083_00_UpdateNMRXData);
	mFunctList.insert("f0084_00_UpdateUTCDateTimeFields",&DbOrganizer::f0084_00_UpdateUTCDateTimeFields);
	mFunctList.insert("f0090_00_ChangeFromNotNull2Null_CalendarOptions",&DbOrganizer::f0090_00_ChangeFromNotNull2Null_CalendarOptions);
	mFunctList.insert("f0093_00_ChangeCalendarSettingFromStringToRecordset",&DbOrganizer::f0093_00_ChangeCalendarSettingFromStringToRecordset);
	mFunctList.insert("f0094_00_AccRSetUpgrade",&DbOrganizer::f0094_00_AccRSetUpgrade);
	mFunctList.insert("f0097_00_CalendarViewPersonalSettingUpdate",&DbOrganizer::f0097_00_CalendarViewPersonalSettingUpdate);
	mFunctList.insert("f0099_00_UpdateEmailToolTip",&DbOrganizer::f0099_00_UpdateEmailToolTip);
	mFunctList.insert("f0100_00_AccRSetUpgrade",&DbOrganizer::f0100_00_AccRSetUpgrade);
	mFunctList.insert("f0103_00_CalendarViewUpgrade",&DbOrganizer::f0103_00_CalendarViewUpgrade);
	mFunctList.insert("f0104_00_AccRSetUpgrade_Calendar",&DbOrganizer::f0104_00_AccRSetUpgrade_Calendar);
	mFunctList.insert("f0105_00_FixHierarchyStructureProjects",&DbOrganizer::f0105_00_FixHierarchyStructureProjects);
	mFunctList.insert("f0108_00_CalendarViewUpgradeOnDeleteCascade",&DbOrganizer::f0108_00_CalendarViewUpgradeOnDeleteCascade);
	mFunctList.insert("f0115_00_UpdateDateTimeFieldsBackFromUTC",&DbOrganizer::f0115_00_UpdateDateTimeFieldsBackFromUTC);
	mFunctList.insert("f0117_00_FixHierarchyLevel",&DbOrganizer::f0117_00_FixHierarchyLevel);
	mFunctList.insert("f0125_00_FixCustomFld",&DbOrganizer::f0125_00_FixCustomFld);
	mFunctList.insert("f0132_00_FixEmailExtChkSum",&DbOrganizer::f0132_00_FixEmailExtChkSum);
	mFunctList.insert("f0134_00_DeleteOldPushMessages",&DbOrganizer::f0134_00_DeleteOldPushMessages);
	
	mFunctList.insert("f9999_01_CreateApplicationOptions",&DbOrganizer::f9999_01_CreateApplicationOptions);		
}

void DbOrganizer::InitializeBusinessProcedures_SPC()
{
	//mFunctListSPC.insert("f65806_00_UpdateNewFields",&DbOrganizer::f65806_00_UpdateNewFields);
}



//-----------------------------------------------------------------------
//								ORGANIZE DB
//-----------------------------------------------------------------------

/*!
	Reorganize database to latest version: common/common/config
	WARNING: this procedure can destroy all data, make backup prior, close DB, then restore DB if something goes wrong..
	\param pStatus return error

	B.T: 25.03.2013: 
	- reorganization must include both SC and SPC parts and their common parts.
	- SC and SPC contain both separate DB versions, and reorganization is done only when version is increased, common part is always updated->increase SC DB version when changing this part
	- when importing SPC data, then database is reorganized, there is only need to drop constraints

*/
void DbOrganizer::OrganizeDatabase(Status &pStatus, bool bRebuildTables)
{
	m_bRebuildTables=bRebuildTables;

	//-----------------------------------------------------------------------------
	//		Determine what part of DB is about to be reorganized
	//-----------------------------------------------------------------------------
	//reserve exc conn.
	//QSqlDatabase *pDbConn;
	m_pManager->ReserveExclusiveDbConnection(pStatus,&m_pDb);
	if(!pStatus.IsOK()){return;}

	QString strHost=m_pManager->GetDbSettings().m_strDbHostName;
	QString strPort=m_pManager->GetDbSettings().m_nDbPort;

	//is reorganization allowed?
	bool bMakeBcp;
	bool bGo=IsUpdateNeeded(pStatus,bMakeBcp);
	if(!pStatus.IsOK()){m_pManager->ReleaseExclusiveDbConnection(&m_pDb);return;}
	bool bDatabaseWasEmpty=!bMakeBcp;

	bool bMakeBcpSPC;
	bool bGoSPC=IsUpdateNeeded_SPC(pStatus,bMakeBcpSPC);
	if(!pStatus.IsOK()){m_pManager->ReleaseExclusiveDbConnection(&m_pDb);return;}


	if (!bGo && !bGoSPC) 
	{
		qDebug()<<"Database already actualized to version: "+QVariant(m_nCurrentDatabaseVersion).toString();
		m_pManager->ReleaseExclusiveDbConnection(&m_pDb);
		return;
	}

	//-----------------------------------------------------------------------------
	//		Backup database before
	//-----------------------------------------------------------------------------

	//if DB was empty-->reorganize from scratch!
	if (!bMakeBcp)
		m_bRebuildTables=true;

#ifdef QT_NO_DEBUG
	bGo=IsDatabaseInSingleUserMode(pStatus);
	if(!pStatus.IsOK()){m_pManager->ReleaseExclusiveDbConnection(&m_pDb);return;}
	if (!bGo) 
	{
		pStatus.setError(1,QObject::tr("Database is still in use by other systems (application servers)!"));
		m_pManager->ReleaseExclusiveDbConnection(&m_pDb);
		return;
	}
#endif

	DbConnectionSettings tmpDbSettings=m_pManager->GetDbSettings();
	QString strBackupPath=m_strBackupDirPath;

#ifndef QT_NO_DEBUG
	bMakeBcp = false;
	bMakeBcpSPC = false;
#endif

	//backup will be triggered for both: Communicator and SPC
	if (bMakeBcp || bMakeBcpSPC)
	{
		int nVer=GetDatabaseCurrentVersion(pStatus);
		if(!pStatus.IsOK()){m_pManager->ReleaseExclusiveDbConnection(&m_pDb);return;}

		int nVerSPC=GetDatabaseCurrentVersion_SPC(pStatus);
		if(!pStatus.IsOK()){m_pManager->ReleaseExclusiveDbConnection(&m_pDb);return;}

		QString strDatabaseString=QString::number(nVer)+"SPC"+QString::number(nVerSPC);

		//BACKUP DATABASE:
		if (m_bShutDownDatabaseWhenBackup)
		{
			m_pManager->ReleaseExclusiveDbConnection(&m_pDb);
			m_pManager->ShutDown();
		}

		m_DbObjectManager->BackupDatabase(pStatus,tmpDbSettings.m_strDbName,tmpDbSettings.m_strDbUserName,tmpDbSettings.m_strDbPassword,strBackupPath,strDatabaseString,strHost,strPort,NULL);

		if (m_bShutDownDatabaseWhenBackup)
		{
			m_pManager->EnablePool();
			Status err;
			m_pManager->ReserveExclusiveDbConnection(err,&m_pDb);
			if(!err.IsOK()){pStatus=err;return;}
		}

		if(!pStatus.IsOK()){m_pManager->ReleaseExclusiveDbConnection(&m_pDb);return;}
	}


	//-----------------------------------------------------------------------------
	//		B.T: if SC<116 then copy to UTF8, only if was not empty
	//-----------------------------------------------------------------------------
	//Step7: create UTF8 database from SC
	//check if DB was not empty (SC data exists): then copy to UTF8 if it was SC database (and if not already UTF8):
	if(!bDatabaseWasEmpty) //only check on existing base
	{
		int nVer=GetDatabaseCurrentVersion(pStatus);
		if (pStatus.IsOK())
		{
			if (nVer<116)
			{
				CopySCDatabaseToUTF8(pStatus); //copy SC database, SC part will be reorganized later
				if (!pStatus.IsOK())
					return;
			}
		}
	}


	//-----------------------------------------------------------------------------
	//		Reorganize all
	//-----------------------------------------------------------------------------
	OrganizeDatabaseAll(pStatus,bGo,bGoSPC,m_bRebuildTables,bDatabaseWasEmpty);


	//-----------------------------------------------------------------------------
	//		Restore if fail
	//-----------------------------------------------------------------------------

	//if one fails all failed: restore from backup
	if (!pStatus.IsOK())
	{
		Status errRestore;
		if (bMakeBcp)
		{
			//dump error:
			qDebug()<<pStatus.getErrorText();
			if (m_pDb!=NULL)
				m_pManager->ReleaseExclusiveDbConnection(&m_pDb);
			//RESTORE DATABASE:
			m_pManager->ShutDown();
			m_DbObjectManager->RestoreDatabase(errRestore,tmpDbSettings.m_strDbName,tmpDbSettings.m_strDbUserName,tmpDbSettings.m_strDbPassword,strBackupPath,strHost,strPort,m_pDb);
			m_pManager->EnablePool();
		}

		if(!errRestore.IsOK())
		{
			QString strErrText=pStatus.getErrorText()+"/n/r Restore failed:"+ errRestore.getErrorText();
			pStatus.setError(pStatus.getErrorCode(),strErrText);
		}

	}

	//release connection:
	if (m_pDb!=NULL)
		m_pManager->ReleaseExclusiveDbConnection(&m_pDb);

}

//Organize All: SC and SPC part....
void DbOrganizer::OrganizeDatabaseAll(Status &pStatus, bool bReorganizeSC, bool bReorganizeSPC, bool bRebuildTables, bool bDatabaseWasEmpty)
{

	//Step0: init all object to existing Db connection
	InitDboObjects(m_pDb);

	//Step1: drop common constraints:
	m_CommonSPCSC_ConstraintCreator.DropAll(pStatus);
	if (!pStatus.IsOK())
		return;
	//Step2: reorganize SC step1
	if(bReorganizeSC)
	{
		OrganizeDatabase_SC_Step1(pStatus,bRebuildTables);
		if (!pStatus.IsOK())
			return;
	}
	//Step3: reorganize SPC step1
	if(bReorganizeSPC)
	{
		OrganizeDatabase_SPC_Step1(pStatus,bRebuildTables);
		if (!pStatus.IsOK())
			return;
	}
	//Step4: create common constraints:
	m_CommonSPCSC_ConstraintCreator.Create(pStatus);
	if (!pStatus.IsOK())
		return;
	//Step5: Communicator: execute business procedures, clean up and elevate database version:
	if(bReorganizeSC)
	{
		OrganizeDatabase_SC_Step2(pStatus);
		if (!pStatus.IsOK())
			return;
	}
	//Step6: SPC: execute business procedures, clean up and elevate database version:
	if(bReorganizeSPC)
	{
		OrganizeDatabase_SPC_Step2(pStatus);
		if (!pStatus.IsOK())
			return;
	}

	//Step8: recreate users SOKRATES and ODBC....
	CreateFBUsers(pStatus,"SYSDBA","masterkey",m_pDb);
	if(!pStatus.IsOK())
		return;

}


//Reorganize SC database part: just do till table constraints
void DbOrganizer::OrganizeDatabase_SC_Step1(Status &pStatus, bool bRebuildTables)
{
	bool bRebuildIndexes=true; //hardcoded!!!

	//-----------------------------LOW LEVEL---------------------------------------
	BeforeOrganize(pStatus);
	if(!pStatus.IsOK())return;

	//functions
	qDebug()<<"Droping functions:";
	m_FunctionCreator.DropAllFunctions(pStatus,true);
	if(!pStatus.IsOK()){return;}

	//procedures
	qDebug()<<"Droping procedures:";
	m_ProcedureCreator.DropAllProcedures(pStatus,true);
	if(!pStatus.IsOK()){return;}

	//delete constrains:
	int lstTableCreatorSize = m_lstTableCreator.size();

	//drop all views, triggers and constraints (unique, fk)
	for(int i=0; i < lstTableCreatorSize; ++i)
	{
		qDebug()<<"Deleting constraints and views for table:"<<m_lstTableCreator.at(i)->m_TableData.m_strTableName;
		m_lstTableCreator.at(i)->DeleteTableConstraints(pStatus);
		if(!pStatus.IsOK())
		{
			return;
		}
	}

	//create functions: can be used inside indexes:
	//functions
	qDebug()<<"Creating functions:";
	m_FunctionCreator.CreateFunctions(pStatus,true);
	if(!pStatus.IsOK()){return;}


	//create/upgrade tables 
	for(int i=0; i < lstTableCreatorSize; ++i)
	{
		qDebug()<<"Actualizing table:"<<m_lstTableCreator.at(i)->m_TableData.m_strTableName;
		m_lstTableCreator.at(i)->CreateTable(pStatus,bRebuildIndexes,bRebuildTables);
		if(!pStatus.IsOK())
		{
			return;
		}
	}

	//create views
	for(int i=0; i < lstTableCreatorSize; ++i)
	{
		qDebug()<<"Creating views: "<<m_lstTableCreator.at(i)->m_TableData.m_strTableName;
		m_lstTableCreator.at(i)->CreateViews(pStatus);
		if(!pStatus.IsOK())return;
	}

	//procedures: after views and before triggers as procedures can use views and triggers can use procedures and functions
	qDebug()<<"Creating procedures:";
	m_ProcedureCreator.CreateProcedures(pStatus,true);
	if(!pStatus.IsOK()){return;}

	//create triggers
	for(int i=0; i < lstTableCreatorSize; ++i)
	{
		qDebug()<<"Creating triggers: "<<m_lstTableCreator.at(i)->m_TableData.m_strTableName;
		m_lstTableCreator.at(i)->CreateTriggers(pStatus);
		if(!pStatus.IsOK())return;
	}

	//create constraints: FK's:
	for(int i=0; i < lstTableCreatorSize; ++i)
	{
		qDebug()<<"Creating constraints for table:"<<m_lstTableCreator.at(i)->m_TableData.m_strTableName;
		m_lstTableCreator.at(i)->CreateTableConstraints(pStatus);
		if(!pStatus.IsOK())
		{
			return;
		}
	}
}


//Reorganize SC database part step2: after all constraints in db (and common), execute business procs, and drop columns, tables
void DbOrganizer::OrganizeDatabase_SC_Step2(Status &pStatus)
{
	//delete constrains:
	int lstTableCreatorSize = m_lstTableCreator.size();

	//-----------------------------HIGH LEVEL--------------------------------------
	qDebug()<<"Filling default values:";
	ExecuteBusinessProcedures(pStatus,mFunctList);
	if(!pStatus.IsOK())
		return;

	//BT: Drop columns at end, thus giving chance for BusinessProcedures to copy data from them...
	//-----------------------------LOW LEVEL---------------------------------------
	for(int i=0; i < lstTableCreatorSize; ++i)
	{
		qDebug()<<"Drop columns for table:"<<m_lstTableCreator.at(i)->m_TableData.m_strTableName;
		m_lstTableCreator.at(i)->DropColumns(pStatus);
		if(!pStatus.IsOK())
		{
			return;
		}
	}

	//Drop tables at end: any FK-PK constraint should be detached
	DropTables(pStatus);
	if(!pStatus.IsOK()){return;}

	//-----------------------------SAVE NEW REVISION TO DB---------------------------------------
	//save new version:
	SaveNewDbVersion(pStatus);
}





// DB organizator for Project Control
void DbOrganizer::OrganizeDatabase_SPC_Step1(Status &pStatus,bool bRebuildTables)
{
	bool bRebuildIndexes=true; //hardcoded!!!

	//-----------------------------LOW LEVEL---------------------------------------
	BeforeOrganize_SPC(pStatus);
	if(!pStatus.IsOK())return;

	//delete constrains:
	int lstTableCreatorSize = m_lstTableCreatorSPC.size();

	//drop all views, triggers and constraints (unique, fk)
	for(int i=0; i < lstTableCreatorSize; ++i)
	{
		qDebug()<<"Deleting constraints and views for table:"<<m_lstTableCreatorSPC.at(i)->m_TableData.m_strTableName;
		m_lstTableCreatorSPC.at(i)->DeleteTableConstraints(pStatus);
		if(!pStatus.IsOK())
		{
			return;
		}
	}

	//functions
	qDebug()<<"Droping functions:";
	m_FunctionCreator.DropAllFunctions(pStatus,false);
	if(!pStatus.IsOK()){return;}

	//procedures
	qDebug()<<"Droping procedures:";
	m_ProcedureCreator.DropAllProcedures(pStatus,false);
	if(!pStatus.IsOK()){return;}


	//create functions: can be used inside indexes:
	qDebug()<<"Creating functions:";
	m_FunctionCreator.CreateFunctions(pStatus,false);
	if(!pStatus.IsOK()){return;}

	//create/upgrade tables 
	for(int i=0; i < lstTableCreatorSize; ++i)
	{
		qDebug()<<"Actualizing table:"<<m_lstTableCreatorSPC.at(i)->m_TableData.m_strTableName;
		//When table is upgraded, then all new flds that are set as null should be filled with default values to avoid nulls
		m_lstTableCreatorSPC.at(i)->CreateTable(pStatus,bRebuildIndexes,bRebuildTables,false,true);
		if(!pStatus.IsOK())
		{
			return;
		}
	}

	//create views
	for(int i=0; i < lstTableCreatorSize; ++i)
	{
		qDebug()<<"Creating views: "<<m_lstTableCreatorSPC.at(i)->m_TableData.m_strTableName;
		m_lstTableCreatorSPC.at(i)->CreateViews(pStatus);
		if(!pStatus.IsOK())return;
	}

	//procedures: after views and before triggers as procedures can use views and triggers can use procedures and functions
	qDebug()<<"Creating procedures:";
	m_ProcedureCreator.CreateProcedures(pStatus,false);
	if(!pStatus.IsOK()){return;}

	//create triggers and views after tables (can reference each other and new ones)
	for(int i=0; i < lstTableCreatorSize; ++i)
	{
		qDebug()<<"Creating triggers: "<<m_lstTableCreatorSPC.at(i)->m_TableData.m_strTableName;
		m_lstTableCreatorSPC.at(i)->CreateTriggers(pStatus);
		if(!pStatus.IsOK())return;
	}

	//create constraints: FK's:
	for(int i=0; i < lstTableCreatorSize; ++i)
	{
		qDebug()<<"Creating constraints for table:"<<m_lstTableCreatorSPC.at(i)->m_TableData.m_strTableName;
		m_lstTableCreatorSPC.at(i)->CreateTableConstraints(pStatus);
		if(!pStatus.IsOK())
		{
			return;
		}
	}

}


void DbOrganizer::OrganizeDatabase_SPC_Step2(Status &pStatus)
{
	//delete constrains:
	int lstTableCreatorSize = m_lstTableCreator.size();

	//-----------------------------HIGH LEVEL---------------------------------------
	qDebug()<<"Filling default values:";
	ExecuteBusinessProcedures(pStatus,mFunctListSPC);
	if(!pStatus.IsOK()){return;}


	//BT: Drop columns at end, thus giving chance for BusinessProcedures to copy data from them...
	//-----------------------------LOW LEVEL---------------------------------------
	for(int i=0; i < lstTableCreatorSize; ++i)
	{
		qDebug()<<"Drop columns for table:"<<m_lstTableCreatorSPC.at(i)->m_TableData.m_strTableName;
		m_lstTableCreatorSPC.at(i)->DropColumns(pStatus);
		if(!pStatus.IsOK())
		{
			return;
		}
	}

	//Drop tables at end: any FK-PK constraint should be detached
	DropTables_SPC(pStatus);
	if(!pStatus.IsOK()){return;}


	//-----------------------------SAVE NEW REVISION TO DB---------------------------------------

	//save new version:
	SaveNewDbVersion_SPC(pStatus);
}



//copy data: nCopyMode=0 (SC), nCopyMode=1 (SPC), nCopyMode=2 (All)
void DbOrganizer::CopyDatabase(Status &pStatus, DbSqlManager *pManagerTarget,bool bDestroyPrevious)
{

	//organize target:
	DbOrganizer OrgTarget(pManagerTarget,m_strBackupDirPath);

	//org target:
	if (bDestroyPrevious)
	{
		OrgTarget.OrganizeDatabase(pStatus,true);
		if(!pStatus.IsOK())
			return;
	}

	//backup target:
	OrgTarget.BackupDatabase(pStatus);
	if(!pStatus.IsOK())
		return;

	//check if version match:
	int nTargetVer=OrgTarget.GetDatabaseCurrentVersion(pStatus);
	if(!pStatus.IsOK())
		return;
	int nSourceVer=GetDatabaseCurrentVersion(pStatus);
	if(!pStatus.IsOK())
		return;
	if (nTargetVer!=nSourceVer)
	{
		pStatus.setError(1,QObject::tr("Database versions not match, you must destroy all data in the target database!"));
		return;
	}


	m_pDb=m_pManager->ReserveConnection(pStatus);
	if(!pStatus.IsOK()){return;}

	QSqlDatabase *pTargetConn=NULL;
	pManagerTarget->ReserveExclusiveDbConnection(pStatus,&pTargetConn);
	if(!pStatus.IsOK()){return;}

	//copy data:
	//muv constraints:

	//init all tables, and other objects with exclusive connection
	InitDboObjects(m_pDb);
	OrgTarget.InitDboObjects(pTargetConn);


	//-----------------------------LOW LEVEL---------------------------------------

	//delete constrains:
	int lstTableCreatorSize = m_lstTableCreator.size();

	qDebug()<<"Warning: deleting all constraints & triggers in the database: "<<pManagerTarget->GetDbName();
	OrgTarget.m_DbObjectManager->DropAllTriggers(pStatus,pTargetConn);
	if(!pStatus.IsOK())
	{m_pManager->ReleaseConnection(m_pDb); pManagerTarget->ReleaseExclusiveDbConnection(&pTargetConn); return;}
	OrgTarget.m_DbObjectManager->DropAllConstraints(pStatus,pTargetConn);
	if(!pStatus.IsOK())
	{m_pManager->ReleaseConnection(m_pDb); pManagerTarget->ReleaseExclusiveDbConnection(&pTargetConn); return;}

	//copy data on all tables
	//create tables 
	for(int i=0; i < lstTableCreatorSize; ++i)
	{
		qDebug()<<"Copying table:"<<m_lstTableCreator.at(i)->m_TableData.m_strTableName;
		m_lstTableCreator.at(i)->CopyData(pStatus,pTargetConn);
		if(!pStatus.IsOK())
		{
			m_pManager->ReleaseConnection(m_pDb); pManagerTarget->ReleaseExclusiveDbConnection(&pTargetConn);return;
		}

		//readjusting generators to start count on new values:
		OrgTarget.m_DbObjectManager->AdjustAutoIncrementAfterDbCopy(pStatus,pTargetConn,m_lstTableCreator.at(i)->m_TableData.m_strTableName,m_lstTableCreator.at(i)->m_TableData.m_strPrimaryKey);
		if(!pStatus.IsOK())
		{
			m_pManager->ReleaseConnection(m_pDb); pManagerTarget->ReleaseExclusiveDbConnection(&pTargetConn);return;
		}

	}

	//create constraints: triggers + FK's:
	for(int i=0; i < lstTableCreatorSize; ++i)
	{
		qDebug()<<"Creating constraints for table:"<<m_lstTableCreator.at(i)->m_TableData.m_strTableName;
		OrgTarget.m_lstTableCreator.at(i)->CreateTableConstraints(pStatus);
		if(!pStatus.IsOK())
		{
			m_pManager->ReleaseConnection(m_pDb); pManagerTarget->ReleaseExclusiveDbConnection(&pTargetConn);return;
		}
	}


	m_pManager->ReleaseConnection(m_pDb);
	pManagerTarget->ReleaseExclusiveDbConnection(&pTargetConn);
}

QString DbOrganizer::BackupDatabase(Status &pStatus)
{	
	int nVer=GetDatabaseCurrentVersion(pStatus);
	if(!pStatus.IsOK()){return "";}

	int nVerSPC=GetDatabaseCurrentVersion_SPC(pStatus);
	if(!pStatus.IsOK()){return "";}

	QString strDatabaseString=QString::number(nVer)+"SPC"+QString::number(nVerSPC);


	if (nVer==0)
	{
		pStatus.setError(1,QObject::tr("Database is not organized, backup creation failed!"));
		return "";
	}

	DbConnectionSettings tmpDbSettings=m_pManager->GetDbSettings();
	QString strBackupPath=m_strBackupDirPath;
	
	QString strHost=m_pManager->GetDbSettings().m_strDbHostName;
	QString strPort=m_pManager->GetDbSettings().m_nDbPort;

	if (m_bShutDownDatabaseWhenBackup)
		m_pManager->ShutDown();
	
	QString strBcpName=m_DbObjectManager->BackupDatabase(pStatus,tmpDbSettings.m_strDbName,tmpDbSettings.m_strDbUserName,tmpDbSettings.m_strDbPassword,strBackupPath,strDatabaseString,strHost,strPort,NULL);
	
	if (m_bShutDownDatabaseWhenBackup)
		m_pManager->EnablePool();

	return strBcpName;
}

void DbOrganizer::RestoreDatabase(QString strBackupFilePath,Status &pStatus)
{
	m_pManager->ReserveExclusiveDbConnection(pStatus,&m_pDb);
	if(!pStatus.IsOK()){return;}

	QString strHost=m_pManager->GetDbSettings().m_strDbHostName;
	QString strPort=m_pManager->GetDbSettings().m_nDbPort;

	bool bGo=IsDatabaseInSingleUserMode(pStatus);
	if(!pStatus.IsOK()){m_pManager->ReleaseExclusiveDbConnection(&m_pDb);return;}
	if (!bGo) 
	{
		pStatus.setError(1,QObject::tr("Database is still in use by other client. Please make sure to disconnect all clients or wait for 4 minutes (timeout for dead clients) and try again!"));
		m_pManager->ReleaseExclusiveDbConnection(&m_pDb);
		return;
	}

	m_pManager->ReleaseExclusiveDbConnection(&m_pDb);

	DbConnectionSettings tmpDbSettings=m_pManager->GetDbSettings();
	//destroy all connection that may be left in pool:
	m_pManager->ShutDown();
	m_DbObjectManager->RestoreDatabase(pStatus,tmpDbSettings.m_strDbName,tmpDbSettings.m_strDbUserName,tmpDbSettings.m_strDbPassword,strBackupFilePath,strHost,strPort,m_pDb);
	m_pManager->EnablePool();
	
}


//0 if not versioned
int DbOrganizer::GetDatabaseCurrentVersion(Status &pStatus)
{
	//reserve exc conn.
	//QSqlDatabase *pDbConn;
	bool bConnReserved=false;
	if (m_pDb==NULL)
	{
		m_pManager->ReserveExclusiveDbConnection(pStatus,&m_pDb);
		if(!pStatus.IsOK()){return -1;}
		bConnReserved=true;
	}

	//is reorganization allowed?
	bool bMakeBcp;
	bool bGo=IsUpdateNeeded(pStatus,bMakeBcp);
	if(!pStatus.IsOK()){m_pManager->ReleaseExclusiveDbConnection(&m_pDb);return -1;}

	if (bConnReserved)
	{
		m_pManager->ReleaseExclusiveDbConnection(&m_pDb);
	}
	return m_nCurrentDatabaseVersion;
}

int	DbOrganizer::GetDatabaseCurrentVersion_SPC(Status &pStatus)
{
	//reserve exc conn.
	//QSqlDatabase *pDbConn;
	bool bConnReserved=false;
	if (m_pDb==NULL)
	{
		m_pManager->ReserveExclusiveDbConnection(pStatus,&m_pDb);
		if(!pStatus.IsOK()){return -1;}
		bConnReserved=true;
	}

	//is reorganization allowed?
	bool bMakeBcp;
	bool bGo=IsUpdateNeeded_SPC(pStatus,bMakeBcp);
	if(!pStatus.IsOK()){m_pManager->ReleaseExclusiveDbConnection(&m_pDb);return -1;}

	if (bConnReserved)
	{
		m_pManager->ReleaseExclusiveDbConnection(&m_pDb);
	}
	return m_nCurrentDatabaseVersionSPC;

}


/*!
Creates database, destroys any previous data
\param pStatus return error
*/
void DbOrganizer::InitDboObjects(QSqlDatabase *pDbConn)
{
	
	//init Table objects
	int lstTableCreatorSize = m_lstTableCreator.size();
	for(int i=0; i < lstTableCreatorSize; ++i)
		m_lstTableCreator.at(i)->InitDbSettings(m_pManager,pDbConn,m_DbObjectManager);

	//init SPC Table objects
	lstTableCreatorSize = m_lstTableCreatorSPC.size();
	for(int i=0; i < lstTableCreatorSize; ++i)
		m_lstTableCreatorSPC.at(i)->InitDbSettings(m_pManager,pDbConn,m_DbObjectManager);


	//init other objects
	m_FunctionCreator.InitDbSettings(m_pManager,pDbConn,m_DbObjectManager);
	m_ProcedureCreator.InitDbSettings(m_pManager,pDbConn,m_DbObjectManager);
	m_CommonSPCSC_ConstraintCreator.InitDbSettings(m_pManager,pDbConn,m_DbObjectManager);
	
}



//reads from DB, db version
bool DbOrganizer::IsUpdateNeeded(Status &pStatus,bool &bDataBaseExists)
{
	//make query
	DbSqlQuery query(pStatus,m_pManager);
	if(!pStatus.IsOK())return false;

	m_nCurrentDatabaseVersion=0;

	//if table does not exists this is first reorganization:
	if(!m_DbObjectManager->CheckIfExists_Table(m_pDb,"CORE_DATABASE_INFO"))
	{
		bDataBaseExists=false;
		//DbTablec
		DbCreate_CORE_DATABASE_INFO tmpDbInfo;
		tmpDbInfo.InitDbSettings(m_pManager,m_pDb,m_DbObjectManager);
		tmpDbInfo.CreateTable(pStatus);
		if(!pStatus.IsOK())return false;
		return true; //version is 0:
	}
	else
	{
		
		QString strSQL="SELECT CDI_DATABASE_VERSION FROM CORE_DATABASE_INFO";
		query.Execute(pStatus,strSQL);
		if(!pStatus.IsOK()){return false;}
	
		if (query.next())
		{
			bDataBaseExists=true;
			m_nCurrentDatabaseVersion=query.value(0).toInt();
		}
		else
			bDataBaseExists=false; //db does not have any record..new db
	}
	

	//compare it:
	if (m_nCurrentDatabaseVersion<QVariant(DATABASE_VERSION).toInt())
		return true;
	else
		return false;
}

//save new version:
void DbOrganizer::SaveNewDbVersion(Status &pStatus)
{

	DbSimpleOrm TableOrm(pStatus,CORE_DATABASE_INFO,m_pManager);
	if(!pStatus.IsOK())return;

	DbRecordSet lstRead;
	TableOrm.Read(pStatus,lstRead);
	if(!pStatus.IsOK())return;

	if (lstRead.getRowCount()==0)
	{
		lstRead.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CORE_DATABASE_INFO));

		lstRead.addRow();
		//set defaults:
		lstRead.setData(0,"CDI_DATABASE_VERSION",DATABASE_VERSION);
		lstRead.setData(0,"CDI_ASK_FOR_TEMPLATE_DB",1);
		lstRead.setData(0,"CDI_START_SETUP_WIZARD",1);
		lstRead.setData(0,"CDI_DATABASE_UTF8",1); //for new DB, set as 1

		//demo key:
		QByteArray key;
		QDateTime invalidDate;
		if(!Authenticator::Demo_ResetKey(key,invalidDate,APPLICATION_VERSION,DATABASE_VERSION))
		{
			pStatus.setError(1,QObject::tr("Failed to create demo key"));
			return;
		}
		lstRead.setData(0,"CDI_SETUP_WIZARD_WIZARD_INFO",key);

	}
	else
	{
		//demo key:
		int nLastDbVer=lstRead.getDataRef(0,"CDI_DATABASE_VERSION").toInt();
		QByteArray key=lstRead.getDataRef(0,"CDI_SETUP_WIZARD_WIZARD_INFO").toByteArray();
		//demo key:
		if(!Authenticator::Demo_UpdateDbVersionKey(key,nLastDbVer,APPLICATION_VERSION,DATABASE_VERSION))
		{
			pStatus.setError(1,QObject::tr("Failed to create demo key"));
			return;
		}
		lstRead.setData(0,"CDI_SETUP_WIZARD_WIZARD_INFO",key);
		lstRead.setData(0,"CDI_DATABASE_VERSION",DATABASE_VERSION);
	}


	TableOrm.Write(pStatus,lstRead); //insert or update
}



//reads from DB, db version//FL_Default.DEF_VERSION
bool DbOrganizer::IsUpdateNeeded_SPC(Status &pStatus,bool &bDataBaseExists)
{
	//make query
	DbSqlQuery query(pStatus,m_pManager);
	if(!pStatus.IsOK())return false;

	m_nCurrentDatabaseVersionSPC=0;

	//if table does not exists this is first reorganization:
	if(!m_DbObjectManager->CheckIfExists_Table(m_pDb,"FL_Default"))
	{
		bDataBaseExists=false;
		//DbTablec
		//DbCreate_FL_DEFAULT tmpDbInfo;
		//tmpDbInfo.InitDbSettings(m_pManager,m_pDb,m_DbObjectManager);
		//tmpDbInfo.CreateTable(pStatus);
		if(!pStatus.IsOK())return false;
		return true; //version is 0:
	}
	else
	{

		QString strSQL="SELECT DEF_VERSION FROM FL_Default";
		query.Execute(pStatus,strSQL);
		if(!pStatus.IsOK()){return false;}

		if (query.next())
		{
			bDataBaseExists=true;
			m_nCurrentDatabaseVersionSPC=query.value(0).toInt();
		}
		else
			bDataBaseExists=false; //db does not have any record..new db
	}


	//compare it:
	if (m_nCurrentDatabaseVersionSPC<QVariant(DATABASE_VERSION_SPC).toInt())
		return true;
	else
		return false;
}

//save new version: //FL_Default.DEF_VERSION
void DbOrganizer::SaveNewDbVersion_SPC(Status &pStatus)
{
	//make query
	DbSqlQuery query(pStatus,m_pManager);
	if(!pStatus.IsOK())return;

	QString strSQL="SELECT COUNT(*) FROM FL_Default";
	query.Execute(pStatus,strSQL);
	if(!pStatus.IsOK()){return;}

	int nRows=0;
	if (query.next())
	{
		nRows=query.value(0).toInt();
	}

	if (nRows>0) //update
	{
		strSQL="UPDATE FL_Default SET DEF_VERSION = "+QString::number(DATABASE_VERSION_SPC);
		query.Execute(pStatus,strSQL,true);
	}
	else //insert
	{
		strSQL="INSERT INTO FL_Default (DEF_SEQUENCE,DEF_VERSION) VALUES (GEN_ID(SEQ_DEF_SEQUENCE,1),"+QString::number(DATABASE_VERSION_SPC)+")";
		query.Execute(pStatus,strSQL,true);
	}


}



//after low level org. setup business data from current version to latest
void DbOrganizer::ExecuteBusinessProcedures(Status &pStatus, QMap<QString,PFN> &lstFunct)
{
	//find starting pos:
	bool bHaveJobToDo=false;

	QMap<QString,PFN>::const_iterator i = lstFunct.constBegin();

	while (i != lstFunct.constEnd()) 
	{
		bool bOK;
		QString strKey=i.key();
		int nVer=strKey.left(strKey.indexOf("_")).remove (0,1).toInt(&bOK);  //must be in format: f0001
		Q_ASSERT(bOK);	//must be valid
		if (nVer>m_nCurrentDatabaseVersion)
		{
			bHaveJobToDo=true;
			break;
		}
		++i;
	}

	if (!bHaveJobToDo) return; //no Business Ops to do

	//do d' business jobs: //funct's are ordered by key=name (ver+order)
	while (i != lstFunct.constEnd()) 
	{
		QString strKey=i.key();
		qDebug()<<strKey;
		(this->*i.value())(pStatus);
		if(!pStatus.IsOK())
			return;
		++i;
	}
}

//
bool DbOrganizer::IsDatabaseInSingleUserMode(Status &pStatus)
{
	//if table does not exists this is first reorganization:
	if(!m_DbObjectManager->CheckIfExists_Table(m_pDb,"CORE_APP_SRV_SESSION"))
		return true;
	
	//make query
	DbSqlQuery query(pStatus,m_pManager);
	if(!pStatus.IsOK())return false;

	//select all from session table:
	QString strSQL="SELECT COAS_DAT_LAST_ACTIVITY FROM CORE_APP_SRV_SESSION";
	query.Execute(pStatus,strSQL);
	if(!pStatus.IsOK())return false;

	DbRecordSet tmpRows;
	query.FetchData(tmpRows);

	int nSize=tmpRows.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		if(tmpRows.getDataRef(i,0).toDateTime().addSecs(APP_SESSION_REPORT_INTERVAL)>QDateTime::currentDateTime())
			return false;
	}

	return true;
}

//-----------------------------------------------------------------------
//								BO FUNCTIONS
//-----------------------------------------------------------------------



/*!
	Special function:
	insert Administrator acount (person/coreuser) for startup)
*/
void DbOrganizer::f0001_00_CreateAdminAccount(Status &pStatus)
{

	DbTableKeyData tableData;
	DbSqlTableDefinition::GetKeyData(CORE_USER,tableData);
	pStatus.setError(0);


	//make local query, can fail if connection reservation fails
	DbSqlQuery query(pStatus,m_pManager);
	if(!pStatus.IsOK()) return;


	//check if exist:
	QString strSQL2="SELECT COUNT(*) FROM CORE_USER WHERE CUSR_USERNAME='Administrator'";
	query.Execute(pStatus,strSQL2);
	if(!pStatus.IsOK()) return;

	//get next:
	int nCnt=0;
	if (query.next())
	{
		nCnt=query.value(0).toInt();
	}
	if (nCnt!=0)
	{
		return; //OK:
	}


	//---------------------------------------------------------------
	//ADMIN user---------------------------------------------------
	//---------------------------------------------------------------

	//prepare insert query:
	QString strSQL=DbSqlTableView::getSQLInsert(DbSqlTableDefinition::GetFullViewID(CORE_USER));
	//m_pManager->SetSQLDefaultValues(pStatus,strSQL,m_pDb,tableData.m_strPrimaryKey,tableData.m_strLastModified);
	if(!pStatus.IsOK()) return;
	query.Prepare(pStatus,strSQL);
	if(!pStatus.IsOK()) return;

	//bind vars:
	QVariant tmpNULL;
	query.bindValue(0,"Administrator");
	query.bindValue(1,"Administrator");
	query.bindValue(2,"Administrator");		
	query.bindValue(3,Authenticator::GeneratePassword("Administrator",""));	//password
	query.bindValue(4,1);			
	query.bindValue(5,tmpNULL);		
	query.bindValue(6,0);			//not system->
	query.bindValue(7,0);			//not restricted to local access
	query.bindValue(8,1);			//Admin is default user->assign templates


	//exec & auto destroy any connection held:
	query.ExecutePrepared(pStatus);
	if(!pStatus.IsOK()) return;

	int nUserID=query.GetLastInsertedID(pStatus,"CUSR_ID");
	if(!pStatus.IsOK()) return;

	//---------------------------------------------------------------
	//ADMIN person---------------------------------------------------
	//---------------------------------------------------------------

	//prepare insert query:
	strSQL="INSERT INTO BUS_PERSON (BPER_FIRST_NAME,BPER_LAST_NAME,BPER_ACTIVE_FLAG,BPER_CODE,BPER_SEX,BPER_INITIALS) VALUES (?,?,?,?,?,?)";
	query.Prepare(pStatus,strSQL);
	if(!pStatus.IsOK()) return;

	//bind vars:
	query.bindValue(0,"Administrator");
	query.bindValue(1,"Administrator");
	query.bindValue(2,1);			
	query.bindValue(3,"ADM");			
	query.bindValue(4,0);			
	query.bindValue(5,"ADM");		

	//exec & auto destroy any connection held:
	query.ExecutePrepared(pStatus);
	if(!pStatus.IsOK()) return;
	int nPersonID=query.GetLastInsertedID(pStatus,"BPER_ID");
	if(!pStatus.IsOK()) return;


	strSQL="UPDATE CORE_USER SET CUSR_PERSON_ID="+QVariant(nPersonID).toString() +" WHERE CUSR_ID="+QVariant(nUserID).toString();
	query.Execute(pStatus,strSQL);
	if(!pStatus.IsOK()) return;

}



/*!
	Special function:
	insert hidden System acount (person/coreuser) for db bakup/etc...
*/
void DbOrganizer::f0001_01_CreateHiddenSystemAccount(Status &pStatus)
{

	DbTableKeyData tableData;
	DbSqlTableDefinition::GetKeyData(CORE_USER,tableData);
	pStatus.setError(0);


	//make local query, can fail if connection reservation fails
	DbSqlQuery query(pStatus,m_pManager);
	if(!pStatus.IsOK()) return;


	//check if exist:
	QString strSQL2="SELECT COUNT(*) FROM CORE_USER WHERE CUSR_USERNAME='system'";
	query.Execute(pStatus,strSQL2);
	if(!pStatus.IsOK()) return;

	//get next:
	int nCnt=0;
	if (query.next())
	{
		nCnt=query.value(0).toInt();
	}
	if (nCnt!=0)
	{
		return; //OK:
	}


	//---------------------------------------------------------------
	//ADMIN user---------------------------------------------------
	//---------------------------------------------------------------

	//prepare insert query:
	QString strSQL=DbSqlTableView::getSQLInsert(DbSqlTableDefinition::GetFullViewID(CORE_USER));
	//m_pManager->SetSQLDefaultValues(pStatus,strSQL,m_pDb,tableData.m_strPrimaryKey,tableData.m_strLastModified);
	if(!pStatus.IsOK()) return;
	query.Prepare(pStatus,strSQL);
	if(!pStatus.IsOK()) return;

	//bind vars:
	QVariant tmpNULL;
	query.bindValue(0,"system");
	query.bindValue(1,"system");
	query.bindValue(2,"system");		
	query.bindValue(3,Authenticator::GeneratePassword("system","balrog10"));	//password
	query.bindValue(4,1);			
	query.bindValue(5,tmpNULL);		
	query.bindValue(6,1);			//truely system acc
	query.bindValue(7,0);			//restricted to local access
	query.bindValue(8,0);			//def=0


	//exec & auto destroy any connection held:
	query.ExecutePrepared(pStatus);
	if(!pStatus.IsOK()) return;

	int nUserID=query.GetLastInsertedID(pStatus,"CUSR_ID");
	if(!pStatus.IsOK()) return;

	//---------------------------------------------------------------
	//ADMIN person---------------------------------------------------
	//---------------------------------------------------------------

	//prepare insert query:
	strSQL="INSERT INTO BUS_PERSON (BPER_FIRST_NAME,BPER_LAST_NAME,BPER_ACTIVE_FLAG,BPER_CODE,BPER_SEX,BPER_INITIALS) VALUES (?,?,?,?,?,?)";
	query.Prepare(pStatus,strSQL);
	if(!pStatus.IsOK()) return;

	//bind vars:
	query.bindValue(0,"system");
	query.bindValue(1,"system");
	query.bindValue(2,1);			
	query.bindValue(3,"sys");			
	query.bindValue(4,0);			
	query.bindValue(5,"sys");		

	//exec & auto destroy any connection held:
	query.ExecutePrepared(pStatus);
	if(!pStatus.IsOK()) return;
	int nPersonID=query.GetLastInsertedID(pStatus,"BPER_ID");
	if(!pStatus.IsOK()) return;


	strSQL="UPDATE CORE_USER SET CUSR_PERSON_ID="+QVariant(nPersonID).toString() +" WHERE CUSR_ID="+QVariant(nUserID).toString();
	query.Execute(pStatus,strSQL);
	if(!pStatus.IsOK()) return;

}


//clear system flag from administrator (acc can be deleted)->sets def flag-> for db setup
//deletes LSA, creates sysadmin acc, system flag=1, ->hidden
void DbOrganizer::f0017_01_UpdateHiddenSystemAccount(Status &pStatus)
{

	DbTableKeyData tableData;
	DbSqlTableDefinition::GetKeyData(CORE_USER,tableData);
	pStatus.setError(0);


	//make local query, can fail if connection reservation fails
	DbSqlQuery query(pStatus,m_pManager);
	if(!pStatus.IsOK()) return;

	//---------------------------------
	//		delete LSA:
	//----------------------------------

	//check if exist:
	QString strSQL2="SELECT CUSR_ID,CUSR_PERSON_ID FROM CORE_USER WHERE CUSR_USERNAME='LocalSystem'";
	query.Execute(pStatus,strSQL2);
	if(!pStatus.IsOK()) return;

	//delete LSA:
	if (query.next())
	{
		int nUsrID=query.value(0).toInt();
		int nPersonID=query.value(0).toInt();
		QString strSQL="DELETE FROM CORE_USER WHERE CUSR_ID="+QVariant(nUsrID).toString();
		query.Execute(pStatus,strSQL);
		if(!pStatus.IsOK()) return;
		strSQL="DELETE FROM BUS_PERSON WHERE BPER_ID="+QVariant(nPersonID).toString();
		query.Execute(pStatus,strSQL);
		if(!pStatus.IsOK()) return;
	}

	//---------------------------------
	//		delete sysadmin:
	//----------------------------------
	//check if exist:
	strSQL2="SELECT CUSR_ID,CUSR_PERSON_ID FROM CORE_USER WHERE CUSR_USERNAME='sysadmin'";
	query.Execute(pStatus,strSQL2);
	if(!pStatus.IsOK()) return;

	//delete LSA:
	if (query.next())
	{
		int nUsrID=query.value(0).toInt();
		int nPersonID=query.value(0).toInt();
		QString strSQL="DELETE FROM CORE_USER WHERE CUSR_ID="+QVariant(nUsrID).toString();
		query.Execute(pStatus,strSQL);
		if(!pStatus.IsOK()) return;
	}

	//---------------------------------
	//		delete sysappserver:
	//----------------------------------
	//check if exist:
	strSQL2="SELECT CUSR_ID,CUSR_PERSON_ID FROM CORE_USER WHERE CUSR_USERNAME='sysappserver'";
	query.Execute(pStatus,strSQL2);
	if(!pStatus.IsOK()) return;

	//delete sysappserver:
	if (query.next())
	{
		int nUsrID=query.value(0).toInt();
		int nPersonID=query.value(0).toInt();
		QString strSQL="DELETE FROM CORE_USER WHERE CUSR_ID="+QVariant(nUsrID).toString();
		query.Execute(pStatus,strSQL);
		if(!pStatus.IsOK()) return;
	}

	//---------------------------------
	//		update admin account = default & not system:
	//----------------------------------
	//check if exist:
	strSQL2="UPDATE CORE_USER SET CUSR_IS_SYSTEM=0,CUSR_IS_DEFAULT_ACC=1 WHERE CUSR_USERNAME='Administrator'";
	query.Execute(pStatus,strSQL2);
	if(!pStatus.IsOK()) return;

	//create hidden (if exists it will skip):
	f0001_01_CreateHiddenSystemAccount(pStatus);
}

/*
	Special function:
	insert one organization: ORG. 
*/
void DbOrganizer::f0001_02_CreateDefaultOrganization(Status &pStatus)
{
	//if not first version (create) exit
	DbTableKeyData tableData;
	DbSqlTableDefinition::GetKeyData(BUS_ORGANIZATION,tableData);
	pStatus.setError(0);

	//make local query, can fail if connection reservation fails
	DbSqlQuery query(pStatus,m_pManager);
	if(!pStatus.IsOK()) return;

	//check if exist:
	QString strSQL2="SELECT COUNT(*) FROM bus_organizations WHERE BORG_CODE='ORG.'";
	query.Execute(pStatus,strSQL2);
	if(!pStatus.IsOK()) return;

	//get next:
	int nCnt=0;
	if (query.next())
	{
		nCnt=query.value(0).toInt();
	}
	if (nCnt!=0)
	{
		return; //OK:
	}


	//prepare insert query:
	QString strSQL=DbSqlTableView::getSQLInsert(DbSqlTableDefinition::GetFullViewID(BUS_ORGANIZATION));
	//m_pManager->SetSQLDefaultValues(pStatus,strSQL,m_pDb,tableData.m_strPrimaryKey,tableData.m_strLastModified);
	if(!pStatus.IsOK()) return;
	query.Prepare(pStatus,strSQL);
	if(!pStatus.IsOK()) return;

	QVariant tmpNULL;
	//bind vars:

	query.bindValue(0,"ORG.");
	query.bindValue(1,"Standard Organization");
	query.bindValue(2,1);		
	query.bindValue(3,tmpNULL);
	query.bindValue(4,tmpNULL);			
	query.bindValue(5,0);		
	query.bindValue(6,tmpNULL);			
	query.bindValue(7,0);			
	query.bindValue(8,tmpNULL);
	//query.bindValue(9,tmpNULL);
	query.bindValue(10,"");
	query.bindValue(11,0);

	//exec & auto destroy any connection held:
	query.ExecutePrepared(pStatus);
	if(!pStatus.IsOK()) return;

	
	int nOrg=query.GetLastInsertedID(pStatus,"ORG_ID");
	if(!pStatus.IsOK()) return;

	//---------------------------------
	//		update org:
	//---------------------------------

	//check if exist:
	strSQL2="SELECT CUSR_ID,CUSR_PERSON_ID FROM CORE_USER WHERE CUSR_USERNAME='Administrator'";
	query.Execute(pStatus,strSQL2);
	if(!pStatus.IsOK()) return;

	//delete LSA:
	if (query.next())
	{
		int nUsrID=query.value(0).toInt();
		int nPersonID=query.value(0).toInt();
		QString strSQL="UPDATE BUS_PERSON SET BPER_ORGANIZATION_ID = "+QVariant(nOrg).toString()+" WHERE BPER_ID="+QVariant(nPersonID).toString();
		query.Execute(pStatus,strSQL);
		if(!pStatus.IsOK()) return;
	}


}

void DbOrganizer::f0001_03_CreateCoreAccessRightSet(Status &pStatus)
{
	DbSimpleOrm TableOrm(pStatus, CORE_ACCRSET, m_pManager); 
	if(!pStatus.IsOK()) 
		return;

	//Instantiate Access right organizer.
	AccessRightsOrganizer ACCOrganizer;

	//Get access right set hash.
	QHash<int, AccessRightSet *> *AccRSetHash = ACCOrganizer.GetAccessRightSetHash();
	Status status;

	QHashIterator<int, AccessRightSet *> iter(*AccRSetHash);
	while (iter.hasNext())
	{
		iter.next();
		DbRecordSet AccRSetRecordSet;
		AccessRightSet *AccRSet = iter.value();

		//Get access right recordset.
		AccRSet->GetARSRecordSet(AccRSetRecordSet);

		//Write access right set to DB.
		TableOrm.Write(status, AccRSetRecordSet);
		if (!status.IsOK())
			qDebug() << "Access Right Set: " << iter.key() << "not loaded.";
	}
}

//Insert _IS_PRIVATE from voice call, email and document tables.
void DbOrganizer::f0014_01_UpdateCE_COMM_ENTITY(Status &pStatus)
{
	/*
	if (m_bRebuildTables)
		return;
	QString strWhere;

	int nRowCount;
	DbRecordSet recTmp;

	DbSqlQuery query(pStatus, m_pManager, m_pDb);
	if(!pStatus.IsOK()) return;
	
	//Emails.
	strWhere = "SELECT CENT_ID, BEM_IS_PRIVATE FROM CE_COMM_ENTITY, BUS_EMAIL WHERE BEM_COMM_ENTITY_ID = CENT_ID";
	
	query.Execute(pStatus, strWhere);
	if(!pStatus.IsOK()) return;
	query.FetchData(recTmp);

	nRowCount = recTmp.getRowCount();
	for (int i = 0; i < nRowCount; ++i)
	{
		QString strCENT_ID = recTmp.getDataRef(i, "CENT_ID").toString();
		QString strBEM_IS_PRIVATE = recTmp.getDataRef(i, "BEM_IS_PRIVATE").toString();
		strWhere = "UPDATE CE_COMM_ENTITY SET CENT_IS_PRIVATE = " + strBEM_IS_PRIVATE + " WHERE CENT_ID = " + strCENT_ID;
		query.Execute(pStatus, strWhere);
		if(!pStatus.IsOK()) return;
	}

	//Voice calls.
	recTmp.destroy();
	strWhere = "SELECT CENT_ID, BVC_IS_PRIVATE FROM CE_COMM_ENTITY, BUS_VOICECALLS WHERE BVC_COMM_ENTITY_ID = CENT_ID";

	query.Execute(pStatus, strWhere);
	if(!pStatus.IsOK()) return;
	query.FetchData(recTmp);

	nRowCount = recTmp.getRowCount();
	for (int i = 0; i < nRowCount; ++i)
	{
		QString strCENT_ID = recTmp.getDataRef(i, "CENT_ID").toString();
		QString strBVC_IS_PRIVATE = recTmp.getDataRef(i, "BVC_IS_PRIVATE").toString();
		strWhere = "UPDATE CE_COMM_ENTITY SET CENT_IS_PRIVATE = " + strBVC_IS_PRIVATE + " WHERE CENT_ID = " + strCENT_ID;
		query.Execute(pStatus, strWhere);
		if(!pStatus.IsOK()) return;
	}

	//Documents.
	recTmp.destroy();
	strWhere = "SELECT CENT_ID, BDMD_IS_PRIVATE FROM CE_COMM_ENTITY, BUS_DM_DOCUMENTS WHERE BDMD_COMM_ENTITY_ID = CENT_ID";

	query.Execute(pStatus, strWhere);
	if(!pStatus.IsOK()) return;
	query.FetchData(recTmp);

	nRowCount = recTmp.getRowCount();
	for (int i = 0; i < nRowCount; ++i)
	{
		QString strCENT_ID = recTmp.getDataRef(i, "CENT_ID").toString();
		QString strBDMD_IS_PRIVATE = recTmp.getDataRef(i, "BDMD_IS_PRIVATE").toString();
		strWhere = "UPDATE CE_COMM_ENTITY SET CENT_IS_PRIVATE = " + strBDMD_IS_PRIVATE + " WHERE CENT_ID = " + strCENT_ID;
		query.Execute(pStatus, strWhere);
		if(!pStatus.IsOK()) return;
	}
	*/
}

void DbOrganizer::f0017_03_UpdateDatabaseInfo(Status &pStatus)
{
	DbSqlQuery query(pStatus, m_pManager);
	if(!pStatus.IsOK()) return;

	//reset template flag=0, do not ask for template
	QString strSQL = "UPDATE CORE_DATABASE_INFO SET CDI_ASK_FOR_TEMPLATE_DB=1, CDI_START_SETUP_WIZARD=1";
	query.Execute(pStatus, strSQL);
}

/*!
	Fill application options.
*/
void DbOrganizer::f9999_01_CreateApplicationOptions(Status &pStatus)
{
	//Fill application options.
	OptionsAndSettingsOrganizer OptionsOrganizer;
	DbRecordSet recOptions = OptionsOrganizer.GetOptions();

	//recOptions.Dump();

	DbRecordSet tmp;
	DbSimpleOrm OptionsORM(pStatus, BUS_OPT_SETTINGS, m_pManager);
	if (!pStatus.IsOK()) return;

	//B.T: delete all existing (quick patch):
	//OptionsORM.GetDbSqlQuery()->Execute(pStatus,"DELETE FROM BUS_OPT_SETTINGS");
	//if (!pStatus.IsOK()) return;


	QString strWhere = " WHERE BOUS_PERSON_ID IS NULL";
	OptionsORM.Read(pStatus, tmp, strWhere);
	if (!pStatus.IsOK()) return;

	//tmp.Dump();

	//Delete already inserted options.
	int nRowCount = recOptions.getRowCount();
	for (int i = 0; i < nRowCount; ++i)
	{
		int nOptionID = recOptions.getDataRef(i, "BOUS_SETTING_ID").toInt();
		if (tmp.find("BOUS_SETTING_ID", nOptionID))
		{
			recOptions.selectRow(i);
		}
	}
	recOptions.deleteSelectedRows();

	OptionsORM.Write(pStatus, recOptions);
	if (!pStatus.IsOK()) return;
}



/*!
	Correct SERACH field inside phones of skype type
*/
void DbOrganizer::f0017_04_FixSearchPhone(Status &pStatus)
{

	DbSqlQuery query(pStatus, m_pManager);
	if(!pStatus.IsOK()) return;


	//7->is fixed!!!
	QString strSQL="UPDATE bus_cm_phone SET BCMP_SEARCH=BCMP_FULLNUMBER  WHERE BCMP_TYPE_ID IN (SELECT BCMT_ID FROM bus_cm_types WHERE BCMT_SYSTEM_TYPE=7)";
	query.Execute(pStatus, strSQL);
}


//all types with CET_COMM_ENTITY_TYPE_ID=0-> set to 1
void DbOrganizer::f0021_00_FixCeTypeTable(Status &pStatus)
{
	DbSqlQuery query(pStatus, m_pManager);
	if(!pStatus.IsOK()) return;

	QString strSQL="UPDATE ce_type SET CET_COMM_ENTITY_TYPE_ID=1 WHERE CET_COMM_ENTITY_TYPE_ID=0";
	query.Execute(pStatus, strSQL);

}


void DbOrganizer::f0022_00_FixDuplicateBigPictureID(Status &pStatus)
{
	DbSqlQuery query(pStatus, m_pManager);
	if(!pStatus.IsOK()) return;


	QString strSQL="SELECT DISTINCT A1.BCMPC_ID FROM BUS_CM_PICTURE A1 WHERE (SELECT COUNT(*) FROM BUS_CM_PICTURE B1 WHERE B1.BCMPC_BIG_PICTURE_ID=A1.BCMPC_BIG_PICTURE_ID)>1";
	query.Execute(pStatus, strSQL);
	if(!pStatus.IsOK()) return;
	DbRecordSet lstData;
	query.FetchData(lstData);
	lstData.setColumn(0,QVariant::Int,"BCMPC_ID");

	if (lstData.getRowCount()==0)
		return;


	QString strSqlStart = "DELETE FROM BUS_CM_PICTURE WHERE BCMPC_ID IN (";

	int nChunk = 200;	//chunks of 200 max
	QString strWhere;

	//drop all:
	m_lstTableCreator.at(m_nBUS_CM_PICTUREIdx)->DropTriggers(pStatus);
	if(!pStatus.IsOK()) return;

	int nSize=lstData.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		strWhere+= lstData.getDataRef(i,0).toString()+",";

		//execute chunk:
		if(i>nChunk||i==nSize-1)
		{
			strWhere.chop(1);
			strSQL=strSqlStart+strWhere+QString(") ");	
			nChunk+=200; //next 200
			if(!query.Execute(pStatus,strSQL))
			{
				return;							
			}
			strWhere="";
		}
	}

	//drop all:
	m_lstTableCreator.at(m_nBUS_CM_PICTUREIdx)->CreateTriggers(pStatus);


}

void DbOrganizer::f0024_00_CreateDemoKey(Status &pStatus)
{
	DbSimpleOrm TableOrm(pStatus,CORE_DATABASE_INFO,m_pManager);
	if(!pStatus.IsOK())return;

	DbRecordSet lstRead;
	TableOrm.Read(pStatus,lstRead);
	if(!pStatus.IsOK())return;

	if (lstRead.getRowCount()>0)
	{
		QByteArray key;
		QDateTime invalidDate;
	
		if(!Authenticator::Demo_ResetKey(key,invalidDate,APPLICATION_VERSION,DATABASE_VERSION))
		{
			pStatus.setError(1,QObject::tr("Failed to create demo key"));
			return;
		}

		lstRead.setData(0,"CDI_SETUP_WIZARD_WIZARD_INFO",key);
		TableOrm.Write(pStatus,lstRead); //update
	}
}


void DbOrganizer::f0026_00_CreateContactPhonets(Status &pStatus)
{
	DbSimpleOrm TableOrm(pStatus,BUS_CM_CONTACT,m_pManager);
	if(!pStatus.IsOK())return;

	DbRecordSet lstContacts;
	TableOrm.Read(pStatus,lstContacts,"",DbSqlTableView::TVIEW_BUS_CM_CONTACT_SELECT_PHONETS);
	if(!pStatus.IsOK())return;


	//lstContacts.Dump();

	//update all phonets:
	ContactTypeManager::SetPhonets(lstContacts);

	//lstContacts.Dump();

	//save them:
	TableOrm.Write(pStatus,lstContacts,DbSqlTableView::TVIEW_BUS_CM_CONTACT_SELECT_PHONETS);
}

void DbOrganizer::f0027_00_InitEmailIDChecksum(Status &pStatus)
{
	DbSimpleOrm TableOrm(pStatus,BUS_EMAIL,m_pManager);
	if(!pStatus.IsOK())return;

	DbRecordSet lstEmails;
	TableOrm.Read(pStatus,lstEmails,"",DbSqlTableView::TVIEW_BUS_EMAIL);
	if(!pStatus.IsOK())return;

	//lstEmails.Dump();

	//update all "checksums" (base64 actually)
	int nChksumCol = lstEmails.getColumnIdx("BEM_EXT_ID_CHKSUM");
	int nExtIDCol = lstEmails.getColumnIdx("BEM_EXT_ID");
	int nCount = lstEmails.getRowCount();
	for(int i=0; i<nCount; i++){
		QByteArray arExtID = lstEmails.getDataRef(i, nExtIDCol).toByteArray();
		if(arExtID.length() > 0)
			lstEmails.setData(i, nChksumCol, QString(arExtID.toBase64().constData()));
	}

	//lstEmails.Dump();

	//save them:
	TableOrm.Write(pStatus,lstEmails,DbSqlTableView::TVIEW_BUS_EMAIL);
}


void DbOrganizer::f0030_00_InitImportExportSettings(Status &pStatus)
{
	DbSimpleOrm TableOrm(pStatus,CORE_IMPORT_EXPORT,m_pManager);
	if(!pStatus.IsOK())return;

	DbRecordSet lstData;
	TableOrm.Read(pStatus,lstData);
	if(!pStatus.IsOK())return;


	if (lstData.getRowCount()>0)
		return;

	lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CORE_IMPORT_EXPORT));
	lstData.addRow();
	//disable all sync:
	lstData.setData(0,"CIE_GLOBAL_SCAN_PERIOD",30);
	lstData.setData(0,"CIE_PROJ_ENABLED",0);
	lstData.setData(0,"CIE_PROJ_SCAN_PERIOD",1);
	lstData.setData(0,"CIE_PROJ_FILE_EXT","sok");
	lstData.setData(0,"CIE_PROJ_FILE_PREFIX_PROCESS","old");

	lstData.setData(0,"CIE_USER_ENABLED",0);
	lstData.setData(0,"CIE_USER_SCAN_PERIOD",1);
	lstData.setData(0,"CIE_USER_FILE_EXT","sok");
	lstData.setData(0,"CIE_USER_FILE_PREFIX_PROCESS","old");

	lstData.setData(0,"CIE_DEBT_ENABLED",0);
	lstData.setData(0,"CIE_DEBT_SCAN_PERIOD",1);
	lstData.setData(0,"CIE_DEBT_FILE_EXT","sok");
	lstData.setData(0,"CIE_DEBT_FILE_PREFIX_PROCESS","old");

	lstData.setData(0,"CIE_CONT_ENABLED",0);
	lstData.setData(0,"CIE_CONT_SCAN_PERIOD",1);
	lstData.setData(0,"CIE_CONT_FILE_EXT","sok");
	lstData.setData(0,"CIE_CONT_FILE_PREFIX_PROCESS","old");


	TableOrm.Write(pStatus,lstData);
}

void DbOrganizer::BeforeOrganize(Status &pStatus)
{
	if (m_nCurrentDatabaseVersion==29)
	{
		m_DbObjectManager->DropIfExists_Table(pStatus,m_pDb,"CORE_IMPORT_EXPORT");
		if(!pStatus.IsOK())return;
	}

	DropAllSequencesFromOldSCDatabase(pStatus);
}

void DbOrganizer::DropTables(Status &pStatus)
{
		m_DbObjectManager->DropIfExists_Table(pStatus,m_pDb,"CE_CONTACT_LINK");
		if(!pStatus.IsOK())return;
		m_DbObjectManager->DropIfExists_Table(pStatus,m_pDb,"CE_PROJECT_LINK");
		if(!pStatus.IsOK())return;
		m_DbObjectManager->DropIfExists_Table(pStatus,m_pDb,"BUS_STOREDLIST_ITEMS");
		if(!pStatus.IsOK())return;
		m_DbObjectManager->DropIfExists_Table(pStatus,m_pDb,"BUS_STOREDLIST");
		if(!pStatus.IsOK())return;
		m_DbObjectManager->DropIfExists_Table(pStatus,m_pDb,"CE_CHARGES_LINK");
		if(!pStatus.IsOK())return;

		m_DbObjectManager->DropIfExists_Table(pStatus,m_pDb,"TEST_TREE");
		if(!pStatus.IsOK())return;

}


void DbOrganizer::BeforeOrganize_SPC(Status &pStatus)
{



}

void DbOrganizer::DropTables_SPC(Status &pStatus)
{


}




void DbOrganizer::f0032_00_FixSearchPhone_2(Status &pStatus)
{

	DbSimpleOrm TableOrm(pStatus,BUS_CM_PHONE,m_pManager);
	if(!pStatus.IsOK())return;

	DbRecordSet lstPhones;
	TableOrm.Read(pStatus,lstPhones);
	if(!pStatus.IsOK())return;

	FormatPhone PhoneFormatter;
	QString strPhone,strCountry,strLocal,strISO,strArea,strSearch;

	int nSize=lstPhones.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		QString strNumber=lstPhones.getDataRef(i,"BCMP_FULLNUMBER").toString();
		PhoneFormatter.SplitPhoneNumber(strNumber,strCountry,strArea,strLocal,strSearch,strISO);
		lstPhones.setData(i,"BCMP_SEARCH",strSearch);
	}

	TableOrm.Write(pStatus,lstPhones);
	
}

//Issue #955 - simple Access rights system and basic roles insertion.
//Delete all from access rights sets and role tables - this can be done only here 
//because AR system is not in use.
void DbOrganizer::f0036_00_CreateSimpleAccessRightSet(Status &pStatus)
{	
	DbSimpleOrm TableOrm(pStatus, CORE_ACCRSET, m_pManager); 
	if(!pStatus.IsOK()) 
		return;

	//--------------------------------------
	//START TRANSACTION.
	//--------------------------------------
	m_pManager->BeginTransaction(pStatus, TableOrm.GetDbConnection());
	if(!pStatus.IsOK()) 
	{
		m_pManager->RollbackTransaction(pStatus, TableOrm.GetDbConnection());
		return;
	}
	
	//First delete existing roles.
	DbSimpleOrm ROLEOrm(pStatus, CORE_ROLE, m_pManager); 
	if(!pStatus.IsOK()) 
	{
		m_pManager->RollbackTransaction(pStatus, TableOrm.GetDbConnection());
		return;
	}

	DbRecordSet recRoles;
	ROLEOrm.Read(pStatus, recRoles);
	if(!pStatus.IsOK()) 
	{
		m_pManager->RollbackTransaction(pStatus, TableOrm.GetDbConnection());
		return;
	}

	//Delete existing roles.
	if (recRoles.getRowCount() > 0)
	{
		ROLEOrm.DeleteFast(pStatus, recRoles);
		if(!pStatus.IsOK()) 
		{
			m_pManager->RollbackTransaction(pStatus, TableOrm.GetDbConnection());
			return;
		}
	}

	//Then ...
	//Get existing access right sets. If there is something inside delete it and write in only simple access right set (issue #955).
	DbRecordSet recAccrSet;
	TableOrm.Read(pStatus, recAccrSet);
	if(!pStatus.IsOK()) 
	{
		m_pManager->RollbackTransaction(pStatus, TableOrm.GetDbConnection());
		return;
	}
	
	if (recAccrSet.getRowCount() > 0)
	{
		TableOrm.DeleteFast(pStatus, recAccrSet);
		if(!pStatus.IsOK()) 
		{
			m_pManager->RollbackTransaction(pStatus, TableOrm.GetDbConnection());
			return;
		}
	}

	//Instantiate Access right organizer.
	AccessRightsOrganizer ACCOrganizer;

	//Get access right set hash.
	QHash<int, AccessRightSet *> AccRSetHash = ACCOrganizer.GetSimpleAccessRightSetHash();
	Status status;

	QHashIterator<int, AccessRightSet *> iter(AccRSetHash);
	while (iter.hasNext())
	{
		iter.next();
		DbRecordSet AccRSetRecordSet;
		AccessRightSet *AccRSet = iter.value();

		//Get access right recordset.
		AccRSet->GetARSRecordSet(AccRSetRecordSet);

		//Write access right set to DB.
		TableOrm.Write(status, AccRSetRecordSet);
		if(!pStatus.IsOK()) 
		{
			m_pManager->RollbackTransaction(pStatus, TableOrm.GetDbConnection());
			return;
		}
	}

	//Read once more access rights sets for later adding them to default roles.
	TableOrm.Read(pStatus, recAccrSet);
	if(!pStatus.IsOK()) 
	{
		m_pManager->RollbackTransaction(pStatus, TableOrm.GetDbConnection());
		return;
	}

	//
	//
	//Insert default roles.
	//Read recordset to rewrite.
	DbRecordSet RoleRecordSet;
	RoleRecordSet.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CORE_ROLE));

	//Insert roles.
	RoleRecordSet.addRow();
	RoleRecordSet.setData(0, "CROL_NAME", "Administrator");
	RoleRecordSet.setData(0, "CROL_ROLE_DESC", "Administrator Role");
	RoleRecordSet.setData(0, "CROL_ROLE_TYPE", 2);
	RoleRecordSet.setData(0, "CROL_CODE", ADMINISTRATOR_ROLE);
	RoleRecordSet.addRow();
	RoleRecordSet.setData(1, "CROL_NAME", "Project Manager");
	RoleRecordSet.setData(1, "CROL_ROLE_DESC", "Project Manager Role");
	RoleRecordSet.setData(1, "CROL_ROLE_TYPE", 2);
	RoleRecordSet.setData(1, "CROL_CODE", PROJECT_MANAGER_BE_ROLE);
	RoleRecordSet.addRow();
	RoleRecordSet.setData(2, "CROL_NAME", "Secretary");
	RoleRecordSet.setData(2, "CROL_ROLE_DESC", "Secretary Role");
	RoleRecordSet.setData(2, "CROL_ROLE_TYPE", 2);
	RoleRecordSet.setData(2, "CROL_CODE", SECRETARY_BE_ROLE);
	RoleRecordSet.addRow();
	RoleRecordSet.setData(3, "CROL_NAME", "User");
	RoleRecordSet.setData(3, "CROL_ROLE_DESC", "User Role");
	RoleRecordSet.setData(3, "CROL_ROLE_TYPE", 2);
	RoleRecordSet.setData(3, "CROL_CODE", USER_BE_ROLE);
	RoleRecordSet.addRow();
	RoleRecordSet.setData(4, "CROL_NAME", "User");
	RoleRecordSet.setData(4, "CROL_ROLE_DESC", "User Role");
	RoleRecordSet.setData(4, "CROL_ROLE_TYPE", 2);
	RoleRecordSet.setData(4, "CROL_CODE", USER_TE_ROLE);

	//Write.
	ROLEOrm.Write(pStatus, RoleRecordSet);
	if(!pStatus.IsOK()) 
	{
		m_pManager->RollbackTransaction(pStatus, TableOrm.GetDbConnection());
		return;
	}

	//Get role ID's based on their codes.
	int nRoleCount = RoleRecordSet.getRowCount();
	QHash<int, int> hshRoleCode2RoleID;
	for (int i = 0; i < nRoleCount; ++i)
		hshRoleCode2RoleID.insert(RoleRecordSet.getDataRef(i, "CROL_CODE").toInt(), RoleRecordSet.getDataRef(i, "CROL_ID").toInt());

	//Now add access rights sets to default roles.
	//Make flag to mark insert dirty if something went wrong - to rollback.
	bool bDirty = false;
	AccessRightsManipulator AccRghtManipulator(m_pManager);
	AccRghtManipulator.InsertNewAccRSetToRole(pStatus, hshRoleCode2RoleID.value(ADMINISTRATOR_ROLE), recAccrSet.getDataRef(recAccrSet.find("CAST_CODE", PROJECT_MANAGER, true), "CAST_ID").toInt());
	if(!pStatus.IsOK()) bDirty = true;
	AccRghtManipulator.InsertNewAccRSetToRole(pStatus, hshRoleCode2RoleID.value(ADMINISTRATOR_ROLE), recAccrSet.getDataRef(recAccrSet.find("CAST_CODE", ADMINISTRATOR, true), "CAST_ID").toInt());
	if(!pStatus.IsOK()) bDirty = true;
	AccRghtManipulator.InsertNewAccRSetToRole(pStatus, hshRoleCode2RoleID.value(ADMINISTRATOR_ROLE), recAccrSet.getDataRef(recAccrSet.find("CAST_CODE", HUMAN_RESOURCES, true), "CAST_ID").toInt());
	if(!pStatus.IsOK()) bDirty = true;
	AccRghtManipulator.InsertNewAccRSetToRole(pStatus, hshRoleCode2RoleID.value(ADMINISTRATOR_ROLE), recAccrSet.getDataRef(recAccrSet.find("CAST_CODE", MAIN_DATA, true), "CAST_ID").toInt());
	if(!pStatus.IsOK()) bDirty = true;
	AccRghtManipulator.InsertNewAccRSetToRole(pStatus, hshRoleCode2RoleID.value(ADMINISTRATOR_ROLE), recAccrSet.getDataRef(recAccrSet.find("CAST_CODE", CONTACT_MANAGER, true), "CAST_ID").toInt());
	if(!pStatus.IsOK()) bDirty = true;
	AccRghtManipulator.InsertNewAccRSetToRole(pStatus, hshRoleCode2RoleID.value(ADMINISTRATOR_ROLE), recAccrSet.getDataRef(recAccrSet.find("CAST_CODE", COMMUNICATOR, true), "CAST_ID").toInt());
	if(!pStatus.IsOK()) bDirty = true;
	AccRghtManipulator.InsertNewAccRSetToRole(pStatus, hshRoleCode2RoleID.value(ADMINISTRATOR_ROLE), recAccrSet.getDataRef(recAccrSet.find("CAST_CODE", GROUP_MANAGEMENT, true), "CAST_ID").toInt());
	if(!pStatus.IsOK()) bDirty = true;
	AccRghtManipulator.InsertNewAccRSetToRole(pStatus, hshRoleCode2RoleID.value(ADMINISTRATOR_ROLE), recAccrSet.getDataRef(recAccrSet.find("CAST_CODE", TEMPLATE_MANAGER, true), "CAST_ID").toInt());
	if(!pStatus.IsOK()) bDirty = true;

	AccRghtManipulator.InsertNewAccRSetToRole(pStatus, hshRoleCode2RoleID.value(SECRETARY_BE_ROLE), recAccrSet.getDataRef(recAccrSet.find("CAST_CODE", PROJECT_MANAGER, true), "CAST_ID").toInt());
	if(!pStatus.IsOK()) bDirty = true;
	AccRghtManipulator.InsertNewAccRSetToRole(pStatus, hshRoleCode2RoleID.value(SECRETARY_BE_ROLE), recAccrSet.getDataRef(recAccrSet.find("CAST_CODE", HUMAN_RESOURCES, true), "CAST_ID").toInt());
	if(!pStatus.IsOK()) bDirty = true;
	AccRghtManipulator.InsertNewAccRSetToRole(pStatus, hshRoleCode2RoleID.value(SECRETARY_BE_ROLE), recAccrSet.getDataRef(recAccrSet.find("CAST_CODE", MAIN_DATA, true), "CAST_ID").toInt());
	if(!pStatus.IsOK()) bDirty = true;
	AccRghtManipulator.InsertNewAccRSetToRole(pStatus, hshRoleCode2RoleID.value(SECRETARY_BE_ROLE), recAccrSet.getDataRef(recAccrSet.find("CAST_CODE", CONTACT_MANAGER, true), "CAST_ID").toInt());
	if(!pStatus.IsOK()) bDirty = true;
	AccRghtManipulator.InsertNewAccRSetToRole(pStatus, hshRoleCode2RoleID.value(SECRETARY_BE_ROLE), recAccrSet.getDataRef(recAccrSet.find("CAST_CODE", COMMUNICATOR, true), "CAST_ID").toInt());
	if(!pStatus.IsOK()) bDirty = true;
	AccRghtManipulator.InsertNewAccRSetToRole(pStatus, hshRoleCode2RoleID.value(SECRETARY_BE_ROLE), recAccrSet.getDataRef(recAccrSet.find("CAST_CODE", GROUP_MANAGEMENT, true), "CAST_ID").toInt());
	if(!pStatus.IsOK()) bDirty = true;
	AccRghtManipulator.InsertNewAccRSetToRole(pStatus, hshRoleCode2RoleID.value(SECRETARY_BE_ROLE), recAccrSet.getDataRef(recAccrSet.find("CAST_CODE", TEMPLATE_MANAGER, true), "CAST_ID").toInt());
	if(!pStatus.IsOK()) bDirty = true;

	AccRghtManipulator.InsertNewAccRSetToRole(pStatus, hshRoleCode2RoleID.value(PROJECT_MANAGER_BE_ROLE), recAccrSet.getDataRef(recAccrSet.find("CAST_CODE", PROJECT_MANAGER, true), "CAST_ID").toInt());
	if(!pStatus.IsOK()) bDirty = true;
	AccRghtManipulator.InsertNewAccRSetToRole(pStatus, hshRoleCode2RoleID.value(PROJECT_MANAGER_BE_ROLE), recAccrSet.getDataRef(recAccrSet.find("CAST_CODE", CONTACT_MANAGER, true), "CAST_ID").toInt());
	if(!pStatus.IsOK()) bDirty = true;
	AccRghtManipulator.InsertNewAccRSetToRole(pStatus, hshRoleCode2RoleID.value(PROJECT_MANAGER_BE_ROLE), recAccrSet.getDataRef(recAccrSet.find("CAST_CODE", COMMUNICATOR, true), "CAST_ID").toInt());
	if(!pStatus.IsOK()) bDirty = true;
	AccRghtManipulator.InsertNewAccRSetToRole(pStatus, hshRoleCode2RoleID.value(PROJECT_MANAGER_BE_ROLE), recAccrSet.getDataRef(recAccrSet.find("CAST_CODE", GROUP_MANAGEMENT, true), "CAST_ID").toInt());
	if(!pStatus.IsOK()) bDirty = true;
	AccRghtManipulator.InsertNewAccRSetToRole(pStatus, hshRoleCode2RoleID.value(PROJECT_MANAGER_BE_ROLE), recAccrSet.getDataRef(recAccrSet.find("CAST_CODE", TEMPLATE_MANAGER, true), "CAST_ID").toInt());
	if(!pStatus.IsOK()) bDirty = true;
	
	AccRghtManipulator.InsertNewAccRSetToRole(pStatus, hshRoleCode2RoleID.value(USER_BE_ROLE), recAccrSet.getDataRef(recAccrSet.find("CAST_CODE", COMMUNICATOR, true), "CAST_ID").toInt());
	if(!pStatus.IsOK()) bDirty = true;
	AccRghtManipulator.InsertNewAccRSetToRole(pStatus, hshRoleCode2RoleID.value(USER_BE_ROLE), recAccrSet.getDataRef(recAccrSet.find("CAST_CODE", TEMPLATE_MANAGER, true), "CAST_ID").toInt());
	if(!pStatus.IsOK()) bDirty = true;

	AccRghtManipulator.InsertNewAccRSetToRole(pStatus, hshRoleCode2RoleID.value(USER_TE_ROLE), recAccrSet.getDataRef(recAccrSet.find("CAST_CODE", PROJECT_MANAGER, true), "CAST_ID").toInt());
	if(!pStatus.IsOK()) bDirty = true;
	AccRghtManipulator.InsertNewAccRSetToRole(pStatus, hshRoleCode2RoleID.value(USER_TE_ROLE), recAccrSet.getDataRef(recAccrSet.find("CAST_CODE", HUMAN_RESOURCES, true), "CAST_ID").toInt());
	if(!pStatus.IsOK()) bDirty = true;
	AccRghtManipulator.InsertNewAccRSetToRole(pStatus, hshRoleCode2RoleID.value(USER_TE_ROLE), recAccrSet.getDataRef(recAccrSet.find("CAST_CODE", MAIN_DATA, true), "CAST_ID").toInt());
	if(!pStatus.IsOK()) bDirty = true;
	AccRghtManipulator.InsertNewAccRSetToRole(pStatus, hshRoleCode2RoleID.value(USER_TE_ROLE), recAccrSet.getDataRef(recAccrSet.find("CAST_CODE", CONTACT_MANAGER, true), "CAST_ID").toInt());
	if(!pStatus.IsOK()) bDirty = true;
	AccRghtManipulator.InsertNewAccRSetToRole(pStatus, hshRoleCode2RoleID.value(USER_TE_ROLE), recAccrSet.getDataRef(recAccrSet.find("CAST_CODE", COMMUNICATOR, true), "CAST_ID").toInt());
	if(!pStatus.IsOK()) bDirty = true;
	AccRghtManipulator.InsertNewAccRSetToRole(pStatus, hshRoleCode2RoleID.value(USER_TE_ROLE), recAccrSet.getDataRef(recAccrSet.find("CAST_CODE", GROUP_MANAGEMENT, true), "CAST_ID").toInt());
	if(!pStatus.IsOK()) bDirty = true;
	AccRghtManipulator.InsertNewAccRSetToRole(pStatus, hshRoleCode2RoleID.value(USER_TE_ROLE), recAccrSet.getDataRef(recAccrSet.find("CAST_CODE", TEMPLATE_MANAGER, true), "CAST_ID").toInt());
	if(!pStatus.IsOK()) bDirty = true;

	if(bDirty) 
	{
		m_pManager->RollbackTransaction(pStatus, TableOrm.GetDbConnection());
		return;
	}

	//--------------------------------------
	//Get all persons and assign them administrator privileges.
	//--------------------------------------
	//First delete existing roles.
	DbSimpleOrm BUS_PERSONOrm(pStatus, BUS_PERSON, m_pManager); 
	if(!pStatus.IsOK()) 
	{
		m_pManager->RollbackTransaction(pStatus, TableOrm.GetDbConnection());
		return;
	}

	DbRecordSet recPersons;
	BUS_PERSONOrm.Read(pStatus, recPersons);
	if(!pStatus.IsOK()) 
	{
		m_pManager->RollbackTransaction(pStatus, TableOrm.GetDbConnection());
		return;
	}

	int nPersonsCount = recPersons.getRowCount();
	for (int i = 0; i < nPersonsCount; ++i)
	{
		int nPersonID = recPersons.getDataRef(i, "BPER_ID").toInt();
		AccRghtManipulator.InsertNewRoleToPerson(pStatus, nPersonID, hshRoleCode2RoleID.value(ADMINISTRATOR_ROLE));
		if(!pStatus.IsOK()) 
		{
			m_pManager->RollbackTransaction(pStatus, TableOrm.GetDbConnection());
			return;
		}
	}
	
	//--------------------------------------
	//If all good commit.
	//--------------------------------------
	m_pManager->CommitTransaction(pStatus, TableOrm.GetDbConnection());
}


void DbOrganizer::f0035_00_FixHierachicalTables(Status &pStatus)
{
	FixHierachicalTable(pStatus, BUS_PROJECT);
	FixHierachicalTable(pStatus, BUS_ORGANIZATION);
	FixHierachicalTable(pStatus, BUS_DEPARTMENT);
}



void DbOrganizer::FixHierachicalTable(Status &pStatus, int nTableID)
{
	DbSimpleOrm TableOrm(pStatus, nTableID, m_pManager); 
	if(!pStatus.IsOK()) 
		return;

	DbRecordSet lstRead;
	TableOrm.Read(pStatus,lstRead);
	if(!pStatus.IsOK())return;

	int nRowCount = lstRead.getRowCount();
	if (nRowCount>0)
	{
		//find tree errors and fix the data
		DbTableKeyData KeyData;
		DbSqlTableDefinition::GetKeyData(nTableID,KeyData);
		QString strPrefix = KeyData.m_strFieldPrefix;

		//debugging log
		bool bLog = false;
		QFile file("C:\\admin_tool.log");
		//if (!file.open(QIODevice::WriteOnly|QIODevice::Text|QIODevice::Append)){
		//	bLog = false;
			//QMessageBox::information(NULL, "", tr("Failed to open log file!"));
		//}

		qDebug() << "Analyzing table:" << KeyData.m_strTableName;
		if(bLog)
			file.write(QString("Analyzing table: " + KeyData.m_strTableName + "\n").toLatin1());

		int nIDCol = lstRead.getColumnIdx(strPrefix+"_ID");						Q_ASSERT(nIDCol >= 0);
		int nCodeCol = lstRead.getColumnIdx(strPrefix+"_CODE");					Q_ASSERT(nCodeCol >= 0);
		int nLevelCol = lstRead.getColumnIdx(strPrefix+"_LEVEL");				Q_ASSERT(nLevelCol >= 0);
		int nParentCol = lstRead.getColumnIdx(strPrefix+"_PARENT");				Q_ASSERT(nParentCol >= 0);
		//int nSiblingCol = lstRead.getColumnIdx(strPrefix+"_SIBLING");			Q_ASSERT(nSiblingCol >= 0);
		int nHasChildrenCol = lstRead.getColumnIdx(strPrefix+"_HASCHILDREN");	Q_ASSERT(nHasChildrenCol >= 0);

		//sort by code to speed up the process
		lstRead.sort(nCodeCol);

	#ifdef _DEBUG
		//lstRead.Dump();
	#endif

		for(int i=0; i<nRowCount; i++)
		{
			QString strCodeCur	  = lstRead.getDataRef(i, nCodeCol).toString();
			QString strCodeParent = HierarchicalHelper::CalcParentCode(strCodeCur);

			//if the row has parent, it must be in one of the rows before it
			int nParentIdx  = HierarchicalHelper::SortedListFindParentNodeIdx(lstRead, strCodeCur, nCodeCol, i-1);
			int nPrevSibIdx = HierarchicalHelper::SortedListFindPrevSiblingNodeIdx(lstRead, strCodeCur, nCodeCol, i-1, nParentIdx);

			//
			// check/fix that row has correct parent ID and level
			//
			if(nParentIdx >= 0)
			{
				QString strRealParent = lstRead.getDataRef(nParentIdx, nCodeCol).toString();

				if( lstRead.getDataRef(i, nParentCol) != 
					lstRead.getDataRef(nParentIdx, nIDCol))
				{
					int nCurParentID = lstRead.getDataRef(i, nParentCol).toInt();
					int nCurParentIdx = lstRead.find(nIDCol, nCurParentID, true);
					QString strCurParent = lstRead.getDataRef(nCurParentIdx, nCodeCol).toString();
					
					qDebug() << "Code" << strCodeCur << "has invalid parent: " << nCurParentID  << "(" << strCurParent << ")" << "correct is: " << lstRead.getDataRef(nParentIdx, nIDCol).toInt() << " (" << strRealParent << ")";
					if(bLog)
						file.write(QString("Code [" + strCodeCur + "] has invalid parent: " + QString("%1").arg(nCurParentID)  + " (" + strCurParent + ")" + " correct is: " + QString("%1").arg(lstRead.getDataRef(nParentIdx, nIDCol).toInt()) + " (" + strRealParent + ")\n").toLatin1());

					lstRead.setData(i, nParentCol, lstRead.getDataRef(nParentIdx, nIDCol));
				}
				if( lstRead.getDataRef(i, nLevelCol).toInt() != 
					(lstRead.getDataRef(nParentIdx, nLevelCol).toInt() + 1))
				{
					qDebug() << "Code" << strCodeCur << "has invalid level" << lstRead.getDataRef(i, nLevelCol).toInt() << "correct is:" << lstRead.getDataRef(nParentIdx, nLevelCol).toInt() + 1;
					if(bLog)
						file.write(QString("Code [" + strCodeCur + "] has invalid level: " + QString("%1").arg(lstRead.getDataRef(i, nLevelCol).toInt()) + " correct is:" + QString("%1").arg(lstRead.getDataRef(nParentIdx, nLevelCol).toInt() + 1) + "\n").toLatin1());

					lstRead.setData(i, nLevelCol, lstRead.getDataRef(nParentIdx, nLevelCol).toInt() + 1);
				}
			}
			else{
				//root node
				if(!lstRead.getDataRef(i, nParentCol).isNull())
				{
					qDebug() << "Code" << strCodeCur << "has invalid parent (correct is NULL)";
					if(bLog)
						file.write(QString("Code [" + strCodeCur + "] has invalid parent (correct is NULL)\n").toLatin1());

					lstRead.setData(i, nParentCol, QVariant(QVariant::Int));
				}
				if(lstRead.getDataRef(i, nLevelCol).toInt() != 1)
				{
					qDebug() << "Code" << strCodeCur << "has invalid level" << lstRead.getDataRef(i, nLevelCol).toInt() <<  "(correct is 1)";
					if(bLog)
						file.write(QString("Code [" + strCodeCur + "] has invalid level " + QString("%1").arg(lstRead.getDataRef(i, nLevelCol).toInt()) +  " (correct is 1)\n").toLatin1());

					lstRead.setData(i, nLevelCol, 1);
				}
			}

			//
			// check/fix that row has correct sibling index
			//

			/*
			if(nPrevSibIdx >= 0)
			{
				if( lstRead.getDataRef(i, nSiblingCol) != 
					(lstRead.getDataRef(nPrevSibIdx, nSiblingCol).toInt() + 1))
				{
					qDebug() << "Code" << strCodeCur << "has invalid sibling" << lstRead.getDataRef(i, nSiblingCol).toInt() << "correct is " << (lstRead.getDataRef(nPrevSibIdx, nSiblingCol).toInt() + 1);
					qDebug() << "Previous sibling is " << lstRead.getDataRef(nPrevSibIdx, nCodeCol).toString() << " (index: " << lstRead.getDataRef(nPrevSibIdx, nSiblingCol).toInt() << ")";
					if(bLog)
						file.write(QString("Code [" + strCodeCur + "] has invalid sibling " + QString("%1").arg(lstRead.getDataRef(i, nSiblingCol).toInt()) +  " (correct is " + QString("%1").arg(lstRead.getDataRef(nPrevSibIdx, nSiblingCol).toInt() + 1) + ")\n").toLatin1());

					lstRead.setData(i, nSiblingCol, (lstRead.getDataRef(nPrevSibIdx, nSiblingCol).toInt() + 1));
				}
			}
			else
			{
				//first sibling in the branch
				if( lstRead.getDataRef(i, nSiblingCol).isNull() ||
					lstRead.getDataRef(i, nSiblingCol).toInt() != 0)
				{
					qDebug() << "Code" << strCodeCur << "has invalid sibling" << lstRead.getDataRef(i, nSiblingCol) << "(correct is 0)";
					if(bLog)
						file.write(QString("Code [" + strCodeCur + "] has invalid sibling " + QString("%1").arg(lstRead.getDataRef(i, nSiblingCol).toInt()) + " (correct is 0)\n").toLatin1());

					lstRead.setData(i, nSiblingCol, 0);
				}
			}
			*/
		}

		TableOrm.Write(pStatus,lstRead); //write fixed data
	}
}



//test flag: CDI_IS_DEMO_DATABASE if true and bEnableDemo=false, delete triggers
//test flag: CDI_IS_DEMO_DATABASE if false and bEnableDemo=true create triggers
//test flag: CDI_IS_DEMO_DATABASE if true and bEnableDemo=true or false & false, do nothing

//warning: tested for FIRBIRD only!!!!

void DbOrganizer::CheckForDemoDatabase(Status &pStatus, bool bEnableDemo, int nMaxSizeKb, int nWarnSizeKb)
{
	int nVer=GetDatabaseCurrentVersion(pStatus);
	if (!pStatus.IsOK()) return;
	if (nVer<41)
	{
		pStatus.setError(StatusCodeSet::ERR_GENERAL,"Could not prepare demo database, database version must be at least 41!");
		return;
	}

	m_pManager->ReserveExclusiveDbConnection(pStatus,&m_pDb);
	if(!pStatus.IsOK()){return;}

	DbRecordSet rec_DbInfo;
	DbSimpleOrm TableORM(pStatus, CORE_DATABASE_INFO, m_pManager);
	if (!pStatus.IsOK()) return;

	TableORM.Read(pStatus,rec_DbInfo);
	if (rec_DbInfo.getRowCount()!=1)
	{
		pStatus.setError(StatusCodeSet::ERR_GENERAL,"Could not prepare demo database, database info table does not exists!");
		return;
	}


	if (rec_DbInfo.getDataRef(0,"CDI_IS_DEMO_DATABASE").toInt()==0 && bEnableDemo) //create
	{
		m_DbObjectManager->PrepareDemoDatabase(pStatus,m_pDb);
		if (!pStatus.IsOK())
		{
			m_pManager->ReleaseExclusiveDbConnection(&m_pDb);
			return;
		}

		rec_DbInfo.setData(0,"CDI_IS_DEMO_DATABASE",1);
		rec_DbInfo.setData(0,"CDI_DEMO_DB_MAX_SIZE",nMaxSizeKb);
		rec_DbInfo.setData(0,"CDI_DEMO_DB_WARN_SIZE",nWarnSizeKb);
		TableORM.Write(pStatus,rec_DbInfo);
		if (!pStatus.IsOK())
		{
			m_pManager->ReleaseExclusiveDbConnection(&m_pDb);
			return;
		}

		int nCurr;
		RecalcDemoDatabaseSize(pStatus,nCurr);
		if (!pStatus.IsOK())
		{
			m_pManager->ReleaseExclusiveDbConnection(&m_pDb);
			return;
		}


		if (nCurr>nMaxSizeKb)
		{
			pStatus.setError(StatusCodeSet::ERR_GENERAL,"Could not prepare demo database, current database size is greater then limit set!");
			m_pManager->ReleaseExclusiveDbConnection(&m_pDb);
			return;
		}
	}
	else if (rec_DbInfo.getDataRef(0,"CDI_IS_DEMO_DATABASE").toInt()>0 && !bEnableDemo) //delete
	{
		m_DbObjectManager->ClearDemoDatabase(pStatus,m_pDb);
		if (!pStatus.IsOK())
		{
			m_pManager->ReleaseExclusiveDbConnection(&m_pDb);
			return;
		}
		rec_DbInfo.setData(0,"CDI_IS_DEMO_DATABASE",0);
		rec_DbInfo.setData(0,"CDI_DEMO_DB_MAX_SIZE",0);
		rec_DbInfo.setData(0,"CDI_DEMO_DB_WARN_SIZE",0);
		rec_DbInfo.setData(0,"CDI_DEMO_DB_CURR_SIZE",0);
		TableORM.Write(pStatus,rec_DbInfo);
		if (!pStatus.IsOK())
		{
			m_pManager->ReleaseExclusiveDbConnection(&m_pDb);
			return;
		}
	}

	m_pManager->ReleaseExclusiveDbConnection(&m_pDb);
	return;

	//if nothing changed, exit
}



//sum all blob field size into CDI_DEMO_DB_MAX_SIZE
void DbOrganizer::RecalcDemoDatabaseSize(Status &pStatus, int &nCurrSizeKb)
{
	
	DbSqlQuery query(pStatus,m_pManager);
	if(!pStatus.IsOK())return;
	nCurrSizeKb=0;

	//BUS_EMAIL.BEM_BODY
	query.Execute(pStatus,"SELECT sum(octet_length(BEM_BODY))/1024 FROM BUS_EMAIL");
	if(!pStatus.IsOK())return;
	if (query.next()) 
		nCurrSizeKb+=query.value(0).toInt();

	//BUS_DM_REVISIONS.BDMR_CONTENT
	query.Execute(pStatus,"SELECT sum(octet_length(BDMR_CONTENT))/1024 FROM BUS_DM_REVISIONS");
	if(!pStatus.IsOK())return;
	if (query.next()) 
		nCurrSizeKb+=query.value(0).toInt();

	//BUS_CM_PICTURE.BCMPC_PICTURE
	query.Execute(pStatus,"SELECT sum(octet_length(BCMPC_PICTURE))/1024 FROM BUS_CM_PICTURE");
	if(!pStatus.IsOK())return;
	if (query.next()) 
		nCurrSizeKb+=query.value(0).toInt();

	//BUS_BIG_PICTURE.BPIC_PICTURE
	query.Execute(pStatus,"SELECT sum(octet_length(BPIC_PICTURE))/1024 FROM BUS_BIG_PICTURE");
	if(!pStatus.IsOK())return;
	if (query.next()) 
		nCurrSizeKb+=query.value(0).toInt();

	//BUS_EMAIL_ATTACHMENT.BEA_CONTENT
	query.Execute(pStatus,"SELECT sum(octet_length(BEA_CONTENT))/1024 FROM BUS_EMAIL_ATTACHMENT");
	if(!pStatus.IsOK())return;
	if (query.next()) 
		nCurrSizeKb+=query.value(0).toInt();

	//WRITE
	query.Execute(pStatus,"UPDATE CORE_DATABASE_INFO SET CDI_DEMO_DB_CURR_SIZE ="+QVariant(nCurrSizeKb).toString());

}


void DbOrganizer::f0042_00_FixCodePageOnLongVarChar(Status &pStatus)
{
	DbSqlQuery query(pStatus,m_pManager);
	if(!pStatus.IsOK())return;

	query.BeginTransaction(pStatus);
	if(!pStatus.IsOK())return;


	UpdateLongVarChar(pStatus,"BUS_CM_ADDRESS","BCMA_FORMATEDADDRESS","BCMA_ID");
	if(!pStatus.IsOK())
	{
		query.Rollback();
		return;
	}
	UpdateLongVarChar(pStatus,"BUS_CM_ADDRESS_SCHEMAS","BCMAS_SCHEMA","BCMAS_ID");
	if(!pStatus.IsOK())
	{
		query.Rollback();
		return;
	}
	UpdateLongVarChar(pStatus,"BUS_CM_CONTACT","BCNT_DESCRIPTION","BCNT_ID");
	if(!pStatus.IsOK())
	{
		query.Rollback();
		return;
	}
	UpdateLongVarChar(pStatus,"BUS_CM_CREDITOR","BCMC_DESCRIPTION","BCMC_ID");
	if(!pStatus.IsOK())
	{
		query.Rollback();
		return;
	}
	UpdateLongVarChar(pStatus,"BUS_CM_DEBTOR","BCMD_DESCRIPTION","BCMD_ID");
	if(!pStatus.IsOK())
	{
		query.Rollback();
		return;
	}
	UpdateLongVarChar(pStatus,"BUS_CM_DEBTOR","BCMD_INVOICE_TEXT","BCMD_ID");
	if(!pStatus.IsOK())
	{
		query.Rollback();
		return;
	}
	UpdateLongVarChar(pStatus,"BUS_CM_EMAIL","BCME_DESCRIPTION","BCME_ID");
	if(!pStatus.IsOK())
	{
		query.Rollback();
		return;
	}
	UpdateLongVarChar(pStatus,"BUS_CM_INTERNET","BCMI_DESCRIPTION","BCMI_ID");
	if(!pStatus.IsOK())
	{
		query.Rollback();
		return;
	}
	UpdateLongVarChar(pStatus,"BUS_CM_JOURNAL","BCMJ_TEXT","BCMJ_ID");
	if(!pStatus.IsOK())
	{
		query.Rollback();
		return;
	}
	UpdateLongVarChar(pStatus,"BUS_CM_PAYMENT_CONDITIONS","BCMPY_TEXT","BCMPY_ID");
	if(!pStatus.IsOK())
	{
		query.Rollback();
		return;
	}
	UpdateLongVarChar(pStatus,"BUS_CM_PICTURE","BCMPC_TEXT","BCMPC_ID");
	if(!pStatus.IsOK())
	{
		query.Rollback();
		return;
	}
	UpdateLongVarChar(pStatus,"BUS_COST_CENTERS","BCTC_DESCRIPTION","BCTC_ID");
	if(!pStatus.IsOK())
	{
		query.Rollback();
		return;
	}
	UpdateLongVarChar(pStatus,"BUS_DEPARTMENTS","BDEPT_DESCRIPTION","BDEPT_ID");
	if(!pStatus.IsOK())
	{
		query.Rollback();
		return;
	}
	UpdateLongVarChar(pStatus,"BUS_DM_APPLICATIONS","BDMA_DESCRIPTION","BDMA_ID");
	if(!pStatus.IsOK())
	{
		query.Rollback();
		return;
	}
	UpdateLongVarChar(pStatus,"BUS_DM_DOCUMENTS","BDMD_DOC_PATH","BDMD_ID");
	if(!pStatus.IsOK())
	{
		query.Rollback();
		return;
	}
	UpdateLongVarChar(pStatus,"BUS_DM_DOCUMENTS","BDMD_DESCRIPTION","BDMD_ID");
	if(!pStatus.IsOK())
	{
		query.Rollback();
		return;
	}
	UpdateLongVarChar(pStatus,"BUS_DM_USER_PATHS","BDMU_APP_PATH","BDMU_ID");
	if(!pStatus.IsOK())
	{
		query.Rollback();
		return;
	}
	UpdateLongVarChar(pStatus,"BUS_EMAIL","BEM_FROM","BEM_ID");
	if(!pStatus.IsOK())
	{
		query.Rollback();
		return;
	}
	UpdateLongVarChar(pStatus,"BUS_EMAIL","BEM_TO","BEM_ID");
	if(!pStatus.IsOK())
	{
		query.Rollback();
		return;
	}
	UpdateLongVarChar(pStatus,"BUS_EMAIL","BEM_CC","BEM_ID");
	if(!pStatus.IsOK())
	{
		query.Rollback();
		return;
	}
	UpdateLongVarChar(pStatus,"BUS_EMAIL","BEM_BCC","BEM_ID");
	if(!pStatus.IsOK())
	{
		query.Rollback();
		return;
	}
	UpdateLongVarChar(pStatus,"BUS_EMAIL","BEM_SUBJECT","BEM_ID");
	if(!pStatus.IsOK())
	{
		query.Rollback();
		return;
	}
	UpdateLongVarChar(pStatus,"BUS_EMAIL","BEM_BODY","BEM_ID");
	if(!pStatus.IsOK())
	{
		query.Rollback();
		return;
	}
	UpdateLongVarChar(pStatus,"BUS_EMAIL","BEM_TEMPLATE_DESCRIPTION","BEM_ID");
	if(!pStatus.IsOK())
	{
		query.Rollback();
		return;
	}
	UpdateLongVarChar(pStatus,"BUS_GROUP_ITEMS","BGIT_DESCRIPTION","BGIT_ID");
	if(!pStatus.IsOK())
	{
		query.Rollback();
		return;
	}
	UpdateLongVarChar(pStatus,"BUS_NMRX_RELATION","BNMR_DESCRIPTION","BNMR_ID");
	if(!pStatus.IsOK())
	{
		query.Rollback();
		return;
	}
	UpdateLongVarChar(pStatus,"BUS_NMRX_ROLE","BNRO_DESCRIPTION","BNRO_ID");
	if(!pStatus.IsOK())
	{
		query.Rollback();
		return;
	}
	UpdateLongVarChar(pStatus,"BUS_ORGANIZATIONS","BORG_DESCRIPTION","BORG_ID");
	if(!pStatus.IsOK())
	{
		query.Rollback();
		return;
	}
	UpdateLongVarChar(pStatus,"BUS_PERSON","BPER_DESCRIPTION","BPER_ID");
	if(!pStatus.IsOK())
	{
		query.Rollback();
		return;
	}
	UpdateLongVarChar(pStatus,"BUS_PROJECTS","BUSP_DESCRIPTION","BUSP_ID");
	if(!pStatus.IsOK())
	{
		query.Rollback();
		return;
	}
	UpdateLongVarChar(pStatus,"BUS_PROJECTS","BUSP_LOCATION","BUSP_ID");
	if(!pStatus.IsOK())
	{
		query.Rollback();
		return;
	}
	UpdateLongVarChar(pStatus,"BUS_TASKS","BTKS_DESCRIPTION","BTKS_ID");
	if(!pStatus.IsOK())
	{
		query.Rollback();
		return;
	}
	UpdateLongVarChar(pStatus,"BUS_VOICECALLS","BVC_DESCRIPTION","BVC_ID");
	if(!pStatus.IsOK())
	{
		query.Rollback();
		return;
	}
	UpdateLongVarChar(pStatus,"CE_EVENT","CEV_DESCRIPTION","CEV_ID");
	if(!pStatus.IsOK())
	{
		query.Rollback();
		return;
	}
	UpdateLongVarChar(pStatus,"CE_EVENT_TYPE","CEVT_DESCRIPTION","CEVT_ID");
	if(!pStatus.IsOK())
	{
		query.Rollback();
		return;
	}
	UpdateLongVarChar(pStatus,"CE_TYPE","CET_DESCRIPTION","CET_ID");
	if(!pStatus.IsOK())
	{
		query.Rollback();
		return;
	}
	UpdateLongVarChar(pStatus,"CORE_EVENT_LOG","CREL_EVENT_TEXT","CREL_ID");
	if(!pStatus.IsOK())
	{
		query.Rollback();
		return;
	}


	query.Commit(pStatus);


}

void DbOrganizer::UpdateLongVarChar(Status &pStatus,QString strTableName,QString strFieldName,QString strIDName)
{

	DbSqlQuery X(pStatus,m_pManager);
	if(!pStatus.IsOK())return;

	QSqlQuery *query=X.GetQSqlQuery();

	QString strSQL=QString("SELECT %2,%1 FROM %3 ").arg(strFieldName).arg(strIDName).arg(strTableName);
	if(!query->exec(strSQL))
	{
		pStatus=m_pManager->MapSqlError(query);
		return;
	}
	QHash<int,QByteArray> lst; //read into and convert: rec_id/byte!
	int k=0;
	while (query->next())
	{
		QString strData=query->value(1).toString();
		if (strData.isEmpty())
			continue;
		lst[query->value(0).toInt()]=strData.toUtf8();
		k++;
	}

	//qDebug()<<k;
	//qDebug()<<lst.count();

	QString strInsert=QString("UPDATE %1 SET %2 = ? WHERE %3=?").arg(strTableName).arg(strFieldName).arg(strIDName);
	if (!query->prepare(strInsert))
	{
		pStatus=m_pManager->MapSqlError(query);
		return;
	}

	//save
	QHashIterator<int, QByteArray> i(lst);
	while (i.hasNext()) 
	{
		i.next();
		query->bindValue(0,i.value());
		query->bindValue(1,i.key());
		if (!query->exec())
		{
			pStatus=m_pManager->MapSqlError(query);
			return;
		}
	}
}


//for all users without default ORG-> set default org:
void DbOrganizer::f0044_00_UpdatePersonOrganization(Status &pStatus)
{
	//insert/define new options (including def. org) //no matter if called later on..it will do nothing
	f9999_01_CreateApplicationOptions(pStatus);
	if(!pStatus.IsOK()) return;

	DbRecordSet Ret_recOptions;
	DbSimpleOrm OPTIONS_SETTINGS_ORM(pStatus, BUS_OPT_SETTINGS, m_pManager); 
	if(!pStatus.IsOK()) return;
	DbSimpleOrm Org_ORM(pStatus, BUS_ORGANIZATION, m_pManager); 
	if(!pStatus.IsOK()) return;
	DbSimpleOrm Pers_ORM(pStatus, BUS_PERSON, m_pManager); 
	if(!pStatus.IsOK()) return;

	//read opt rec
	QString strWhere = "WHERE BOUS_PERSON_ID IS NULL ";
	OPTIONS_SETTINGS_ORM.Read(pStatus, Ret_recOptions, strWhere);
	if(!pStatus.IsOK()) return;

	//get def org. code & name
	OptionsAndSettingsManager settings;
	settings.SetOptionsDataSource(&Ret_recOptions);

	int nID=settings.GetApplicationOption(APP_DEFAULT_ORGANIZATION_ID).toInt();
	if (nID<=0) //we need to set one org as default
	{
		//read all orgs:
		DbRecordSet lstOrgs;
		Org_ORM.Read(pStatus,lstOrgs, " ORDER BY BORG_ID"); //get oldest inserted
		if(!pStatus.IsOK()) return;

		if (lstOrgs.getRowCount()>0)
		{
			int nOrgDefaultOrg=lstOrgs.getDataRef(0,"BORG_ID").toInt();
			//save back
			settings.SetApplicationOption(APP_DEFAULT_ORGANIZATION_ID,nOrgDefaultOrg);
			DbRecordSet recSettings;
			settings.GetDataSource(Ret_recOptions,recSettings);
			OPTIONS_SETTINGS_ORM.Write(pStatus,Ret_recOptions);
			if(!pStatus.IsOK()) return;

			//reassign all persons to default organization if not already:
			DbRecordSet lstUsers;
			strWhere = "WHERE BPER_ORGANIZATION_ID IS NULL ";
			Pers_ORM.Read(pStatus, lstUsers, strWhere);
			if(!pStatus.IsOK()) return;
			
			if (lstUsers.getRowCount()>0)
			{
				lstUsers.setColValue(lstUsers.getColumnIdx("BPER_ORGANIZATION_ID"),nOrgDefaultOrg);
				Pers_ORM.Write(pStatus, lstUsers);
				if(!pStatus.IsOK()) return;
			}
		}
	}
}



//add direkt and internal
void DbOrganizer::f0044_01_UpdateSystemPhoneTypes(Status &pStatus)
{
	//determine if german or eng
	DbRecordSet recPhoneTypes;
	QString strWhere = "WHERE BCMT_ENTITY_TYPE = " + QVariant(ContactTypeManager::TYPE_PHONE).toString();
	DbSimpleOrm TableOrm(pStatus, BUS_CM_TYPES, m_pManager); 
	if(!pStatus.IsOK()) return;

	TableOrm.Read(pStatus,recPhoneTypes,strWhere);
	if(!pStatus.IsOK()) return;


	DbRecordSet newData;
	newData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_TYPES));
	newData.addRow();
	newData.setData(0,"BCMT_ENTITY_TYPE",QVariant(ContactTypeManager::TYPE_PHONE).toInt());
	newData.setData(0,"BCMT_SYSTEM_TYPE",QVariant(ContactTypeManager::SYSTEM_PHONE_DIRECT).toInt());
	newData.setData(0,"BCMT_IS_FORMATTING_ALLOWED",1);
	newData.setData(0,"BCMT_TYPE_NAME","Business Direct");
	newData.addRow();
	newData.setData(1,"BCMT_ENTITY_TYPE",QVariant(ContactTypeManager::TYPE_PHONE).toInt());
	newData.setData(1,"BCMT_SYSTEM_TYPE",QVariant(ContactTypeManager::SYSTEM_PHONE_INTERNAL).toInt());
	newData.setData(1,"BCMT_IS_FORMATTING_ALLOWED",1);
	newData.setData(1,"BCMT_TYPE_NAME","Internal");


//a) Is there any phone type containing "Gesch�ft" or "Privat" (without an e at the end) --> Is German
	bool bUseGerman=false;
	int nRow=recPhoneTypes.find("BCMT_TYPE_NAME",QString("Gesch�ft"),true,false,true,true);
	if (nRow>=0)
		bUseGerman=true;
	if (!bUseGerman)
	{
		nRow=recPhoneTypes.find("BCMT_TYPE_NAME",QString("Privat"),true,false,true,true);
		if (nRow>=0)
			bUseGerman=true;
	}
	if (bUseGerman)
	{
		newData.setData(0,"BCMT_TYPE_NAME","Gesch�ft direkt");
		newData.setData(1,"BCMT_TYPE_NAME","Intern");
	}

//b) if not: find the first user with admin rights. Check his personal settings: Look at his default language --> DB language
//??? cant do that


	//check if already exists:
	newData.clearSelection();
	if (recPhoneTypes.find("BCMT_SYSTEM_TYPE",QVariant(ContactTypeManager::SYSTEM_PHONE_DIRECT).toInt()))
		newData.selectRow(0);
	if (recPhoneTypes.find("BCMT_SYSTEM_TYPE",QVariant(ContactTypeManager::SYSTEM_PHONE_INTERNAL).toInt()))
		newData.selectRow(1);
	newData.deleteSelectedRows();

	//if not, then write down:
	if (newData.getRowCount()>0)
	{
		recPhoneTypes.merge(newData);
		TableOrm.Write(pStatus,recPhoneTypes);
	}


}

//if empty phonet fields inside contacts, update them
void DbOrganizer::f0046_00_UpdatePhonets(Status &pStatus)
{
	DbRecordSet recData;
	QString strWhere = "where BCNT_LASTNAME_PHONET IS NULL or BCNT_FIRSTNAME_PHONET IS NULL or BCNT_ORGANIZATIONNAME_PHONET IS NULL";
	DbSimpleOrm TableOrm(pStatus, BUS_CM_CONTACT, m_pManager); 
	if(!pStatus.IsOK()) return;

	TableOrm.Read(pStatus,recData,strWhere);
	if(!pStatus.IsOK()) return;

	if (recData.getRowCount()>0)
	{
		ContactTypeManager::SetPhonets(recData);
		TableOrm.Write(pStatus,recData);
	}
}

void DbOrganizer::f0047_00_FixHierachicalTables(Status &pStatus)
{
	FixHierachicalTable(pStatus, BUS_GROUP_ITEMS);
}


//if empty phonet fields inside contacts, update them
void DbOrganizer::f0047_01_FixPrivateContact(Status &pStatus)
{
	/*
	DbRecordSet recData;
	QString strWhere = "UPDATE BUS_CM_CONTACT SET BCNT_ACCESS = 2 where BCNT_DAT_LAST_MODIFIED > ? AND BCNT_ACCESS=0";
	DbSqlQuery X(pStatus,m_pManager;
	if(!pStatus.IsOK())return;
	X.Prepare(pStatus,strWhere);
	if(!pStatus.IsOK())return;
	QDateTime dateStart=QDateTime::fromString("19.03.2008","dd.MM.yyyy");
	X.bindValue(0,dateStart);
	X.ExecutePrepared(pStatus);
	*/
}


//if empty phonet fields inside contacts, update them
void DbOrganizer::f0051_00_UpdateLocationPhonet(Status &pStatus)
{
	DbRecordSet recData;
	//QString strWhere = "where BCNT_LASTNAME_PHONET IS NULL or BCNT_FIRSTNAME_PHONET IS NULL or BCNT_ORGANIZATIONNAME_PHONET IS NULL or BCNT_LOCATION_PHONET IS NULL";
	DbSimpleOrm TableOrm(pStatus, BUS_CM_CONTACT, m_pManager); 
	if(!pStatus.IsOK()) return;

	TableOrm.Read(pStatus,recData);//;,strWhere); //reset all phonets
	if(!pStatus.IsOK()) return;

	if (recData.getRowCount()>0)
	{
		ContactTypeManager::SetPhonets(recData);
		TableOrm.Write(pStatus,recData);
	}
}

//get all pers setting: CURRENT_COUNTRY_ISO_CODE
void DbOrganizer::f0052_00_UpdateCodePredial(Status &pStatus)
{

	DbRecordSet recData;
	//QString strWhere = "where BCNT_LASTNAME_PHONET IS NULL or BCNT_FIRSTNAME_PHONET IS NULL or BCNT_ORGANIZATIONNAME_PHONET IS NULL or BCNT_LOCATION_PHONET IS NULL";
	DbSimpleOrm TableOrm(pStatus, BUS_OPT_SETTINGS, m_pManager); 
	if(!pStatus.IsOK()) return;
	QString strWhere = " where BOUS_SETTING_ID = " +QVariant(CURRENT_COUNTRY_ISO_CODE).toString();
	TableOrm.Read(pStatus,recData,strWhere);
	if(!pStatus.IsOK()) return;

	if (recData.getRowCount()>0)
	{
		int nSize=recData.getRowCount();
		for(int i=0;i<nSize;i++)
		{
			QString strCountry=Countries::GetCountryISOFromPredial(recData.getDataRef(i,"BOUS_VALUE_STRING").toString());
			recData.setData(i,"BOUS_VALUE_STRING",strCountry);
		}
	}

	TableOrm.Write(pStatus,recData); //save back:
	
}


void DbOrganizer::f0053_00_UpdateBinaryFields(Status &pStatus)
{
	
	//SETTINGS VARS:
	DbRecordSet recData;
	DbSimpleOrm TableOrm(pStatus, BUS_OPT_SETTINGS, m_pManager); 
	if(!pStatus.IsOK()) return;
	QString strWhere = " where BOUS_SETTING_ID IN (" +QVariant(APPEAR_WINDOW_PROPERTIES).toString()+",";
	strWhere += QVariant(IMPORT_EMAIL_OUTLOOK_FOLDERS).toString()+",";
	strWhere += QVariant(IMPORT_CONTACT_OUTLOOK_FOLDERS).toString()+",";
	strWhere += QVariant(OUTLOOK_SETTINGS_SCAN_FOLDERS).toString()+",";
	strWhere += QVariant(THUNDERBIRD_SETTINGS_SCAN_FOLDERS).toString()+",";
	strWhere += QVariant(IMPORT_EMAIL_THUNDERBIRD_FOLDERS).toString()+")";
	

	TableOrm.Read(pStatus,recData,strWhere);
	if(!pStatus.IsOK()) return;

	if (recData.getRowCount()>0)
	{
		int nSize=recData.getRowCount();
		for(int i=0;i<nSize;i++)
		{
			if (recData.getDataRef(i,"BOUS_VALUE_BYTE").toByteArray().size()>0)
			{
				DbRecordSet lst=XmlUtil::ConvertByteArray2RecordSet(recData.getDataRef(i,"BOUS_VALUE_BYTE").toByteArray());
				QByteArray byte=XmlUtil::ConvertRecordSet2ByteArray_Fast(lst);
				recData.setData(i,"BOUS_VALUE_BYTE",byte);
			}
		}
	}
	TableOrm.Write(pStatus,recData); //save back:


	//BUSCS_VALUE_BYTE
	DbSimpleOrm TableOrm2(pStatus, BUS_COMM_VIEW_SETTINGS, m_pManager); 
	if(!pStatus.IsOK()) return;
	TableOrm2.Read(pStatus,recData);
	if(!pStatus.IsOK()) return;

	if (recData.getRowCount()>0)
	{
		int nSize=recData.getRowCount();
		for(int i=0;i<nSize;i++)
		{
			if (recData.getDataRef(i,"BUSCS_VALUE_BYTE").toByteArray().size()>0)
			{
				DbRecordSet lst=XmlUtil::ConvertByteArray2RecordSet(recData.getDataRef(i,"BUSCS_VALUE_BYTE").toByteArray());
				QByteArray byte=XmlUtil::ConvertRecordSet2ByteArray_Fast(lst);
				recData.setData(i,"BUSCS_VALUE_BYTE",byte);
			}
		}
	}
	TableOrm2.Write(pStatus,recData); //save back:


}


//get all pers setting: CURRENT_COUNTRY_ISO_CODE
void DbOrganizer::f0054_00_UpdateAddressCodeField(Status &pStatus)
{

	DbRecordSet recData;
	//QString strWhere = "where BCNT_LASTNAME_PHONET IS NULL or BCNT_FIRSTNAME_PHONET IS NULL or BCNT_ORGANIZATIONNAME_PHONET IS NULL or BCNT_LOCATION_PHONET IS NULL";
	DbSimpleOrm TableOrm(pStatus, BUS_CM_ADDRESS, m_pManager); 
	if(!pStatus.IsOK()) return;
	TableOrm.Read(pStatus,recData);
	if(!pStatus.IsOK()) return;

	if (recData.getRowCount()>0)
	{
		int nSize=recData.getRowCount();
		for(int i=0;i<nSize;i++)
		{
			QString strCountryCode=Countries::GetISOCodeFromDs(recData.getDataRef(i,"BCMA_COUNTRY_CODE").toString());
			QString strCountryName=recData.getDataRef(i,"BCMA_COUNTRY_NAME").toString();
			if (strCountryName.isEmpty())
				strCountryName=Countries::GetCountryFromISOCode(strCountryCode);
			recData.setData(i,"BCMA_COUNTRY_CODE",strCountryCode);
			recData.setData(i,"BCMA_COUNTRY_NAME",strCountryName);
		}
	}

	TableOrm.Write(pStatus,recData); //save back:

}

//for contacts and all comm entity transfer private flag to UAR record
void DbOrganizer::f0059_00_CreateUARRecords(Status &pStatus)
{
	//assert if version = 0
	pStatus.setError(0);
	if(!m_DbObjectManager->CheckIfExists_Column(m_pDb,"BUS_CM_CONTACT","BCNT_ACCESS"))
		return;


	DbSqlQuery query(pStatus,m_pManager);
	if(!pStatus.IsOK())return;


	//------------------------------
	//		CONTACT
	//------------------------------
	//ACC_RIGHT_PRIVATE=0,		//private
	//ACC_RIGHT_PUBLIC_READ=1,	//public read
	//ACC_RIGHT_PUBLIC_WRITE=2		//public write
	//select where =0: private: 2uar: owner_id = 3, owner_id=NULL = 0
	//select where =0: read: 2uar: owner_id = 3, owner_id=NULL = 1

	DbRecordSet lstData;
	query.Execute(pStatus,"SELECT BCNT_ID,BCNT_OWNER_ID, BCNT_ACCESS FROM BUS_CM_CONTACT WHERE BCNT_ACCESS<2 AND NOT(BCNT_OWNER_ID IS NULL)");
	if(!pStatus.IsOK()) return;
	query.FetchData(lstData);
	if (lstData.getRowCount()>0)
	{
		QString strSQL="INSERT INTO CORE_ACC_USER_REC (CUAR_TABLE_ID,CUAR_PERSON_ID,CUAR_RECORD_ID,CUAR_CHILD_INHERIT,CUAR_CHILD_COMM_INHERIT,CUAR_GROUP_DEPENDENT,CUAR_RIGHT)\
					   VALUES("+QVariant(BUS_CM_CONTACT).toString()+",?,?,0,0,0,?);";

		query.Prepare(pStatus,strSQL);
		if(!pStatus.IsOK()) return;

		int nSize=lstData.getRowCount();
		for(int i=0;i<nSize;i++)
		{
			//for owner full access:
			qDebug()<<lstData.getDataRef(i,"BCNT_OWNER_ID").toInt();
			qDebug()<<lstData.getDataRef(i,"BCNT_ID").toInt();

			query.bindValue(0,lstData.getDataRef(i,"BCNT_OWNER_ID").toInt());
			query.bindValue(1,lstData.getDataRef(i,"BCNT_ID").toInt());
			query.bindValue(2,3);
			query.ExecutePrepared(pStatus);
			if(!pStatus.IsOK()) 
				return;

			//for all users=NULL, restricted access, if 0 = 0, if 1 = 1
			query.bindValue(0,QVariant(QVariant::Int)); //NULL
			query.bindValue(1,lstData.getDataRef(i,"BCNT_ID").toInt());
			query.bindValue(2,lstData.getDataRef(i,"BCNT_ACCESS").toInt());
			query.ExecutePrepared(pStatus);
			if(!pStatus.IsOK()) return;
		}
	}

	//------------------------------
	//		DOCUMENTS
	//------------------------------

	//CENT_IS_PRIVATE = 0 ->ok
	//CENT_IS_PRIVATE = 1:private: 2uar: owner_id = 3, owner_id=NULL = 0

	QString strSQL ="SELECT BDMD_ID,CENT_OWNER_ID,CENT_IS_PRIVATE FROM CE_COMM_ENTITY ";
	strSQL +=" INNER JOIN BUS_DM_DOCUMENTS ON CENT_ID=BDMD_COMM_ENTITY_ID";
	strSQL +=" WHERE NOT(CENT_OWNER_ID IS NULL) AND CENT_IS_PRIVATE>0 AND CENT_SYSTEM_TYPE_ID= "+QVariant(GlobalConstants::CE_TYPE_DOCUMENT).toString();
	query.Execute(pStatus,strSQL);
	if(!pStatus.IsOK()) return;
	lstData.destroy();
	query.FetchData(lstData);
	UpdateUARRecords(pStatus,BUS_DM_DOCUMENTS,lstData,&query);
	if(!pStatus.IsOK()) 
		return;

	//------------------------------
	//		emails
	//------------------------------

	//CENT_IS_PRIVATE = 0 ->ok
	//CENT_IS_PRIVATE = 1:private: 2uar: owner_id = 3, owner_id=NULL = 0

	strSQL ="SELECT BEM_ID,CENT_OWNER_ID,CENT_IS_PRIVATE FROM CE_COMM_ENTITY";
	strSQL +=" INNER JOIN BUS_EMAIL ON CENT_ID=BEM_COMM_ENTITY_ID";
	strSQL +=" WHERE NOT(CENT_OWNER_ID IS NULL) AND CENT_IS_PRIVATE>0 AND CENT_SYSTEM_TYPE_ID= "+QVariant(GlobalConstants::CE_TYPE_EMAIL).toString();
	query.Execute(pStatus,strSQL);
	if(!pStatus.IsOK()) return;
	lstData.destroy();
	query.FetchData(lstData);
	UpdateUARRecords(pStatus,BUS_EMAIL,lstData,&query);
	if(!pStatus.IsOK()) 
		return;

	//------------------------------
	//		DOCUMENTS
	//------------------------------

	//CENT_IS_PRIVATE = 0 ->ok
	//CENT_IS_PRIVATE = 1:private: 2uar: owner_id = 3, owner_id=NULL = 0

	strSQL ="SELECT BVC_ID,CENT_OWNER_ID,CENT_IS_PRIVATE FROM CE_COMM_ENTITY";
	strSQL +=" INNER JOIN BUS_VOICECALLS ON CENT_ID=BVC_COMM_ENTITY_ID";
	strSQL +="  WHERE NOT(CENT_OWNER_ID IS NULL) AND CENT_IS_PRIVATE>0 AND CENT_SYSTEM_TYPE_ID= "+QVariant(GlobalConstants::CE_TYPE_VOICE_CALL).toString();
	query.Execute(pStatus,strSQL);
	if(!pStatus.IsOK()) return;
	lstData.destroy();
	query.FetchData(lstData);
	UpdateUARRecords(pStatus,BUS_VOICECALLS,lstData,&query);
	if(!pStatus.IsOK()) 
		return;

}

//Issue 1939 & 1941.
void DbOrganizer::f0066_00_RoleUpgrade(Status &pStatus)
{
	//Instantiate Access right manipulator.
	AccessRightsManipulator AccRghtManipulator(m_pManager);

	AccRghtManipulator.InsertNewAccRSetToDatabase(pStatus, PROJECT_LIST_MANAGEMENT);

	AccRghtManipulator.InsertNewAccessRightToAccRSet(pStatus, ADMINISTRATOR, MODIFY_FOREIGN_PERSONAL_SETTINGS);
}

void DbOrganizer::f0071_00_RoleUpgrade(Status &pStatus)
{
	//Instantiate Access right manipulator.
	AccessRightsManipulator AccRghtManipulator(m_pManager);

	AccRghtManipulator.InsertNewAccRSetToDefaultRole(pStatus, ADMINISTRATOR_ROLE, PROJECT_LIST_MANAGEMENT);
	AccRghtManipulator.InsertNewAccRSetToDefaultRole(pStatus, PROJECT_MANAGER_BE_ROLE, PROJECT_LIST_MANAGEMENT);
}

void DbOrganizer::UpdateUARRecords(Status &pStatus, int nTableID,DbRecordSet &lstData,DbSqlQuery *query)
{
	pStatus.setError(0);
	if (lstData.getRowCount()>0)
	{
		QString strSQL="INSERT INTO CORE_ACC_USER_REC (CUAR_TABLE_ID,CUAR_PERSON_ID,CUAR_RECORD_ID,CUAR_CHILD_INHERIT,CUAR_CHILD_COMM_INHERIT,CUAR_GROUP_DEPENDENT,CUAR_RIGHT)\
					   VALUES("+QVariant(nTableID).toString()+",?,?,0,0,0,?);";

		query->Prepare(pStatus,strSQL);
		if(!pStatus.IsOK()) return;

		int nSize=lstData.getRowCount();
		for(int i=0;i<nSize;i++)
		{
			//for owner full access:
			query->bindValue(0,lstData.getDataRef(i,"CENT_OWNER_ID").toInt());
			query->bindValue(1,lstData.getDataRef(i,0).toInt()); //ID
			query->bindValue(2,3);
			query->ExecutePrepared(pStatus);
			if(!pStatus.IsOK()) return;

			//for all users=NULL, restricted access, if 0 = 0, if 1 = 1
			query->bindValue(0,QVariant(QVariant::Int)); //NULL
			query->bindValue(1,lstData.getDataRef(i,0).toInt()); //ID
			query->bindValue(2,0);
			query->ExecutePrepared(pStatus);
			if(!pStatus.IsOK()) return;
		}
	}
}


//clean all application & document records
void DbOrganizer::f0070_00_TemplateCleanUp(Status &pStatus)
{
	DbSqlQuery query(pStatus,m_pManager);
	if(!pStatus.IsOK())return;

	QString strSQL="UPDATE BUS_DM_DOCUMENTS A SET A.BDMD_TEMPLATE_ID=NULL WHERE A.BDMD_TEMPLATE_ID IN ( SELECT B.BDMD_ID FROM BUS_DM_DOCUMENTS B WHERE A.BDMD_TEMPLATE_ID=B.BDMD_ID AND B.BDMD_TEMPLATE_FLAG=0)";
	query.Execute(pStatus,strSQL);
	if(!pStatus.IsOK())return;

	strSQL="UPDATE BUS_DM_APPLICATIONS A SET A.BDMA_TEMPLATE_ID=NULL WHERE A.BDMA_TEMPLATE_ID IN ( SELECT B.BDMD_ID FROM BUS_DM_DOCUMENTS B WHERE A.BDMA_TEMPLATE_ID=B.BDMD_ID AND B.BDMD_TEMPLATE_FLAG=0)";
	query.Execute(pStatus,strSQL);
	if(!pStatus.IsOK())return;
	strSQL="UPDATE BUS_DM_APPLICATIONS A SET A.BDMA_INSTALL_APP_TEMPL_ID=NULL WHERE A.BDMA_INSTALL_APP_TEMPL_ID IN ( SELECT B.BDMD_ID FROM BUS_DM_DOCUMENTS B WHERE A.BDMA_INSTALL_APP_TEMPL_ID=B.BDMD_ID AND B.BDMD_TEMPLATE_FLAG=0)";
	query.Execute(pStatus,strSQL);
	if(!pStatus.IsOK())return;
	strSQL="UPDATE BUS_DM_APPLICATIONS A SET A.BDMA_INSTALL_VIEWER_TEMPL_ID=NULL WHERE A.BDMA_INSTALL_VIEWER_TEMPL_ID IN ( SELECT B.BDMD_ID FROM BUS_DM_DOCUMENTS B WHERE A.BDMA_INSTALL_VIEWER_TEMPL_ID=B.BDMD_ID AND B.BDMD_TEMPLATE_FLAG=0)";
	query.Execute(pStatus,strSQL);
	if(!pStatus.IsOK())return;

}

//update new fields: size() and is_compressed:
void DbOrganizer::f0076_00_FixDocumentRevisionTable(Status &pStatus)
{
	DbSqlQuery query(pStatus,m_pManager);
	if(!pStatus.IsOK())return;

	DbSqlQuery queryUpdate(pStatus,m_pManager);
	if(!pStatus.IsOK())return;

	QString strSQL="UPDATE BUS_DM_REVISIONS SET BDMR_IS_COMPRESSED=1";
	query.Execute(pStatus,strSQL);
	if(!pStatus.IsOK())return;

	strSQL="SELECT BDMR_ID,BDMR_CONTENT FROM BUS_DM_REVISIONS";
	query.Execute(pStatus,strSQL);
	if(!pStatus.IsOK())return;

	while (query.next())
	{
		int nID=query.value(0).toInt();
		QByteArray content=qUncompress(query.value(1).toByteArray());

		queryUpdate.Execute(pStatus,"UPDATE BUS_DM_REVISIONS SET BDMR_SIZE="+QString::number(content.size())+" WHERE BDMR_ID="+QString::number(nID));
		if(!pStatus.IsOK())return;
	}
}

void DbOrganizer::f0077_00_UpdateTaskPriorityField(Status &pStatus)
{
	DbSqlQuery query(pStatus,m_pManager);
	if(!pStatus.IsOK())return;

	QString strSQL="UPDATE BUS_TASKS SET BTKS_PRIORITY=5";
	query.Execute(pStatus,strSQL);
	if(!pStatus.IsOK())return;
}


//update old field: size() inside document: get latest rev, get size, copy size
void DbOrganizer::f0078_00_FixDocumentSize(Status &pStatus)
{
	DbSqlQuery query(pStatus,m_pManager);
	if(!pStatus.IsOK())return;

	DbSqlQuery queryUpdate(pStatus,m_pManager);
	if(!pStatus.IsOK())return;

	QString strSQL="SELECT A.BDMR_DOCUMENT_ID, A.BDMR_SIZE FROM BUS_DM_REVISIONS A WHERE A.BDMR_DAT_CHECK_IN = (SELECT MAX(B.BDMR_DAT_CHECK_IN) FROM BUS_DM_REVISIONS B WHERE B.BDMR_DOCUMENT_ID=A.BDMR_DOCUMENT_ID)";
	query.Execute(pStatus,strSQL);
	if(!pStatus.IsOK())return;

	while (query.next())
	{
		int nID=query.value(0).toInt();
		int nSize=query.value(1).toInt();
		queryUpdate.Execute(pStatus,"UPDATE BUS_DM_DOCUMENTS SET BDMD_SIZE="+QString::number(nSize)+" WHERE BDMD_ID="+QString::number(nID));
		if(!pStatus.IsOK())return;
	}
}

//update old field: size() inside document: get latest rev, get size, copy size
void DbOrganizer::f0079_00_FixDocumentNoteSize(Status &pStatus)
{
	DbSqlQuery query(pStatus,m_pManager);
	if(!pStatus.IsOK())return;

	DbSqlQuery queryUpdate(pStatus,m_pManager);
	if(!pStatus.IsOK())return;

	QString strSQL="SELECT BDMD_ID, BDMD_DESCRIPTION FROM BUS_DM_DOCUMENTS WHERE BDMD_DOC_TYPE = "+QString::number(GlobalConstants::DOC_TYPE_NOTE);
	query.Execute(pStatus,strSQL);
	if(!pStatus.IsOK())return;

	while (query.next())
	{
		int nID=query.value(0).toInt();
		int nSize=query.value(1).toString().size();
		queryUpdate.Execute(pStatus,"UPDATE BUS_DM_DOCUMENTS SET BDMD_SIZE="+QString::number(nSize)+" WHERE BDMD_ID="+QString::number(nID));
		if(!pStatus.IsOK())return;
	}
}

//update old field: size() inside document: get latest rev, get size, copy size
void DbOrganizer::f0080_00_FixItalianPhones(Status &pStatus)
{

	DbSimpleOrm TableOrm(pStatus,BUS_CM_PHONE,m_pManager);
	if(!pStatus.IsOK())return;

	DbRecordSet lstPhones;
	TableOrm.Read(pStatus,lstPhones," WHERE BCMP_COUNTRY = '0039'");
	if(!pStatus.IsOK())return;

	FormatPhone PhoneFormatter;
	QString strCountry,strLocal,strISO,strArea;

	//lstPhones.Dump();
	int nSize=lstPhones.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		QString strCountry=lstPhones.getDataRef(i,"BCMP_COUNTRY").toString();
		QString strLocal=lstPhones.getDataRef(i,"BCMP_LOCAL").toString();
		QString strArea=lstPhones.getDataRef(i,"BCMP_AREA").toString();

		QString strPhone	=	PhoneFormatter.FormatPhoneNumber(strCountry,strArea,strLocal);
		QString strSearch	=	PhoneFormatter.GenerateSearchNumber(strCountry,strArea,strLocal);
		lstPhones.setData(i,"BCMP_FULLNUMBER",strPhone);
		lstPhones.setData(i,"BCMP_SEARCH",strSearch);
	}

	//lstPhones.Dump();
	TableOrm.Write(pStatus,lstPhones);

}


//issue:2142
void DbOrganizer::f0081_00_FixItalianPhones_SagaContinues(Status &pStatus)
{
	f0080_00_FixItalianPhones(pStatus); //just recall prev function as there is new algorithm in formatphone...funny..
}


//issue:2142
void DbOrganizer::f0083_00_UpdateNMRXData(Status &pStatus)
{

	//only if table exists then copy from it:
	if (!m_DbObjectManager->CheckIfExists_Table(m_pDb,"CE_PROJECT_LINK"))
		return;

	DbSimpleOrm TableOrm_NMRX(pStatus,BUS_NMRX_RELATION,m_pManager);
	if(!pStatus.IsOK())return;


	//------------------------PROJECT LINK CONVERT------------------------------------
	DbSimpleOrm TableOrm(pStatus,CE_PROJECT_LINK,m_pManager);
	if(!pStatus.IsOK())return;

	DbRecordSet lstData;
	TableOrm.Read(pStatus,lstData);
	if(!pStatus.IsOK())return;

	int nSize=lstData.getRowCount();
	if (nSize>0)
	{
		DbRecordSet lstNewNMRX;
		lstNewNMRX.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION));
		lstNewNMRX.addRow(nSize);
		lstNewNMRX.setColValue("BNMR_TABLE_1",CE_COMM_ENTITY);
		lstNewNMRX.setColValue("BNMR_TABLE_2",BUS_PROJECT);

		for(int i=0;i<nSize;i++)
		{
			lstNewNMRX.setData(i,"BNMR_TABLE_KEY_ID_1",lstData.getDataRef(i,"CELP_COMM_ENTITY_ID").toInt());
			lstNewNMRX.setData(i,"BNMR_TABLE_KEY_ID_2",lstData.getDataRef(i,"CELP_PROJECT_ID").toInt());
		}

		TableOrm_NMRX.Write(pStatus,lstNewNMRX);
		if(!pStatus.IsOK())return;
	}

	//------------------------CONTACT LINK CONVERT------------------------------------
	DbSimpleOrm TableOrm_Contact(pStatus,CE_CONTACT_LINK,m_pManager);
	if(!pStatus.IsOK())return;

	TableOrm_Contact.Read(pStatus,lstData);
	if(!pStatus.IsOK())return;

	nSize=lstData.getRowCount();
	if (nSize>0)
	{
		DbRecordSet lstNewNMRX;
		lstNewNMRX.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION));
		lstNewNMRX.addRow(nSize);
		lstNewNMRX.setColValue("BNMR_TABLE_1",CE_COMM_ENTITY);
		lstNewNMRX.setColValue("BNMR_TABLE_2",BUS_CM_CONTACT);

		for(int i=0;i<nSize;i++)
		{
			lstNewNMRX.setData(i,"BNMR_TABLE_KEY_ID_1",lstData.getDataRef(i,"CELC_COMM_ENTITY_ID").toInt());
			lstNewNMRX.setData(i,"BNMR_TABLE_KEY_ID_2",lstData.getDataRef(i,"CELC_CONTACT_ID").toInt());
			lstNewNMRX.setData(i,"BNMR_SYSTEM_ROLE",lstData.getDataRef(i,"CELC_LINK_ROLE_ID").toInt());
		}

		TableOrm_NMRX.Write(pStatus,lstNewNMRX);
		if(!pStatus.IsOK())return;
	}


	//------------------------REST TYPE to 0------------------------------------
	QString strSQL="UPDATE BUS_NMRX_RELATION SET BNMR_TYPE=0";
	TableOrm_NMRX.GetDbSqlQuery()->Execute(pStatus,strSQL);
	if(!pStatus.IsOK())return;

}

void DbOrganizer::f0084_00_UpdateUTCDateTimeFields(Status &pStatus)
{
	//get all tables
	QHash<int, DbTableKeyData>   hshTableKeyData;
	DbSqlTableDefinition::GetAllTableData(hshTableKeyData);	

	//iterate through hash:
	QHashIterator<int, DbTableKeyData> i(hshTableKeyData);
	while (i.hasNext()) 
	{
		QString strSQL,strUpdate;
		i.next();
		QString strLastModified=i.value().m_strLastModified;
		if(i.value().m_nViewID<=0)
			continue;
		DbView view = DbSqlTableView::getView(i.value().m_nViewID);

		int nSize=view.size();
		for(int k=0;k<nSize;k++)
		{
			if (view.at(k).m_nType==QVariant::DateTime && view.at(k).m_strName!=strLastModified)
			{
				strSQL.append(view.at(k).m_strName);
				strSQL.append(",");
				strUpdate.append(view.at(k).m_strName);
				strUpdate.append("=?,");
			}
			
		}
		if (strSQL.isEmpty())
			continue;

		//qDebug()<<"UTC convert for "<<i.value().m_strTableName<<"for cols: " <<strSQL;

		strSQL.chop(1);
		strUpdate.chop(1);
		strSQL="SELECT "+i.value().m_strPrimaryKey+","+strLastModified+","+strSQL+" FROM "+i.value().m_strTableName;
		strUpdate="UPDATE "+i.value().m_strTableName+" SET "+strUpdate+" WHERE "+i.value().m_strPrimaryKey+"=?";

		DbSqlQuery query(pStatus,m_pManager);
		if(!pStatus.IsOK())return;
		query.Execute(pStatus,strSQL);
		if(!pStatus.IsOK())return;
		DbRecordSet lstData;
		query.FetchData(lstData);
		//lstData.Dump();
		//convert to UTC:

		
		nSize=lstData.getRowCount();
		int nColSize=lstData.getColumnCount();
		for(int k=0;k<nSize;k++)
		{
			for(int z=2;z<nColSize;z++) //skip ID & last modif at pos 0
			{
				if(lstData.getDataRef(k,z).isNull())
					continue;

				QDateTime tmp=lstData.getDataRef(k,z).toDateTime().toUTC();
				//qDebug()<<tmp;
				tmp.setTimeSpec(Qt::LocalTime);
				//qDebug()<<tmp;
				lstData.setData(k,z,tmp);
			}
		}

		//save back:
		query.Prepare(pStatus,strUpdate);
		if(!pStatus.IsOK())return;
		
		for(int k=0;k<nSize;k++)
		{
			for(int z=2;z<nColSize;z++) //skip ID & last modif at pos 0
			{
				query.bindValue(z-2,lstData.getDataRef(k,z));
			}
			query.bindValue(nColSize-2,lstData.getDataRef(k,0)); //ID
			query.ExecutePrepared(pStatus);
			if(!pStatus.IsOK())return;
		}

	}


}



//update old field: size() inside document: get latest rev, get size, copy size
void DbOrganizer::f0090_00_ChangeFromNotNull2Null_CalendarOptions(Status &pStatus)
{

	QStringList lstFields;
	QStringList lstFieldsDataTypes;

	lstFields<<"BCOL_FROM";
	lstFields<<"BCOL_TO";
	lstFieldsDataTypes<<"TIMESTAMP";
	lstFieldsDataTypes<<"TIMESTAMP";

	ChangeFromNotNull2NullFields(pStatus,"BUS_CAL_OPTIONS","BCOL_ID",lstFields,lstFieldsDataTypes);

	lstFields.clear();
	lstFieldsDataTypes.clear();
	lstFields<<"BCEV_FROM";
	lstFields<<"BCEV_TO";
	lstFieldsDataTypes<<"TIMESTAMP";
	lstFieldsDataTypes<<"TIMESTAMP";

	ChangeFromNotNull2NullFields(pStatus,"BUS_CAL_EVENT","BCEV_ID",lstFields,lstFieldsDataTypes);
}

void DbOrganizer::f0093_00_ChangeCalendarSettingFromStringToRecordset(Status &pStatus)
{	
	QString strSQL = "UPDATE BUS_OPT_SETTINGS SET BOUS_SETTING_TYPE = 3, BOUS_VALUE_STRING = NULL WHERE BOUS_SETTING_ID = " + QVariant(CALENDAR_VIEW_SETTINGS).toString();
	DbSqlQuery query(pStatus,m_pManager);
	if(!pStatus.IsOK())return;
	query.Execute(pStatus,strSQL);
	if(!pStatus.IsOK())return;
}

void DbOrganizer::f0094_00_AccRSetUpgrade(Status &pStatus)
{
	//Instantiate Access right manipulator.
	AccessRightsManipulator AccRghtManipulator(m_pManager);

	AccRghtManipulator.InsertNewAccessRightToAccRSet(pStatus, MAIN_DATA, MODIFY_CALENDAR_CATEGORY);
	AccRghtManipulator.InsertNewAccessRightToAccRSet(pStatus, MAIN_DATA, MODIFY_CALENDAR_SHOW_TIME_AS);
	AccRghtManipulator.InsertNewAccessRightToAccRSet(pStatus, MAIN_DATA, MODIFY_DOCUMENTS_CATEGORY);
	AccRghtManipulator.InsertNewAccessRightToAccRSet(pStatus, MAIN_DATA, MODIFY_NMRX_ROLES);
}

void DbOrganizer::f0097_00_CalendarViewPersonalSettingUpdate(Status &pStatus)
{
	DbRecordSet recData;
	DbSimpleOrm TableOrm(pStatus, BUS_OPT_SETTINGS, m_pManager); 
	if(!pStatus.IsOK()) return;
	QString strWhere = " where BOUS_SETTING_ID = " +QVariant(CALENDAR_VIEW_SETTINGS).toString();
	TableOrm.Read(pStatus,recData,strWhere);
	if(!pStatus.IsOK()) return;

	if (recData.getRowCount()>0)
	{
		int nSize=recData.getRowCount();
		for(int i=0;i<nSize;i++)
		{
			recData.setData(i,"BOUS_SETTING_TYPE",0);
			recData.setData(i,"BOUS_VALUE",-1);
			//recData.setData(i,"BOUS_VALUE_BYTE",NULL);
		}
	}

	TableOrm.Write(pStatus,recData); //save back:

	//Update byte field to null.	
	DbSqlQuery *query = TableOrm.GetDbSqlQuery();
	QString strSQL="UPDATE BUS_OPT_SETTINGS SET BOUS_VALUE_BYTE = NULL WHERE BOUS_SETTING_ID =" +QVariant(CALENDAR_VIEW_SETTINGS).toString();
	query->Execute(pStatus,strSQL);
	if(!pStatus.IsOK()) return;
}

void DbOrganizer::f0099_00_UpdateEmailToolTip(Status &pStatus)
{
	DbRecordSet recData;
	DbSimpleOrm TableOrm(pStatus, BUS_EMAIL, m_pManager); 
	if(!pStatus.IsOK()) return;
	TableOrm.Read(pStatus,recData,"",DbSqlTableView::TVIEW_BUS_EMAIL_COMM_ENTITY);
	if(!pStatus.IsOK()) return;
	DbSimpleOrm CE_COMM_ENTITY_TableORM(pStatus, CE_COMM_ENTITY, m_pManager);
	if(!pStatus.IsOK()) return;

	int nCharsNumber=250;

	int nRowCount = recData.getRowCount();
	for (int i = 0; i < nRowCount; i++)
	{
		QString strBody = recData.getDataRef(i, "BEM_BODY").toString();
		//If html - BEM_EMAIL_TYPE = 1, if text BEM_EMAIL_TYPE = 0.
		if (recData.getDataRef(i, "BEM_EMAIL_TYPE").toInt())
		{
			strBody = DataHelper::ExtractTextFromHTML(strBody);
			strBody = strBody.left(nCharsNumber);
			strBody = strBody.remove("&nbsp;", Qt::CaseInsensitive);
			strBody = strBody.remove("&#8217;", Qt::CaseInsensitive);
			strBody = strBody.remove("&#8230;", Qt::CaseInsensitive);
		}
		else
		{
			strBody = strBody.left(nCharsNumber);
		}

		//Put body back to recordset.
		recData.setData(i, "CENT_TOOLTIP", strBody);
	}


	if (recData.getRowCount()>0)
	{
		DbRecordSet rowCommEntity;
		rowCommEntity.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CE_COMM_ENTITY));
		rowCommEntity.merge(recData);

		CE_COMM_ENTITY_TableORM.Write(pStatus, rowCommEntity);
		if (!pStatus.IsOK())
			return;
	}

}

void DbOrganizer::f0100_00_AccRSetUpgrade(Status &pStatus)
{
	//Instantiate Access right manipulator.
	AccessRightsManipulator AccRghtManipulator(m_pManager);
	AccRghtManipulator.InsertNewAccessRightToAccRSet(pStatus, CONTACT_MANAGER, ENTER_MODIFY_CONTACT_ASSIGNMENT_ROLE);

	//again to make sure it's all right:
	AccRghtManipulator.InsertNewAccessRightToAccRSet(pStatus, MAIN_DATA, MODIFY_CALENDAR_CATEGORY);
	AccRghtManipulator.InsertNewAccessRightToAccRSet(pStatus, MAIN_DATA, MODIFY_CALENDAR_SHOW_TIME_AS);
	AccRghtManipulator.InsertNewAccessRightToAccRSet(pStatus, MAIN_DATA, MODIFY_DOCUMENTS_CATEGORY);
	AccRghtManipulator.InsertNewAccessRightToAccRSet(pStatus, MAIN_DATA, MODIFY_NMRX_ROLES);
	
}

void DbOrganizer::f0103_00_CalendarViewUpgrade(Status &pStatus)
{
	//----------------------------------------------
	//				Get views and update.
	//----------------------------------------------
	//Contact data Table ORM.
	DbSimpleOrm BUS_CAL_VIEW_ORM(pStatus, BUS_CAL_VIEW, m_pManager); 
	if(!pStatus.IsOK()) return;
	//Read.
	DbRecordSet recCalendarViews;
 	BUS_CAL_VIEW_ORM.Read(pStatus, recCalendarViews);

	int nRows=recCalendarViews.getRowCount();
	for (int i=0; i<nRows; i++)
	{
		QByteArray bytView = recCalendarViews.getDataRef(i, "BCALV_VALUE_BYTE").toByteArray();
		DbRecordSet recSettings;
		recSettings = XmlUtil::ConvertByteArray2RecordSet_Fast(bytView);

		//Check does saved setting has the same column count as defined in table view (somebody added some setting).
		DbRecordSet testSettings;
		//_DUMP(recSettings);
		//_DUMP(testSettings);
		testSettings.defineFromView(DbSqlTableView::getView(DbSqlTableView::MVIEW_CALENDAR_PERSONAL_SETTINGS));
		if (recSettings.getColumnCount() != testSettings.getColumnCount())
		{
			DbRecordSet tmpRec;
			tmpRec=testSettings;
			tmpRec.addRow();
			int nViewColumnCount=recSettings.getColumnCount();
			int nTestColumnCount=testSettings.getColumnCount();
			for (int j=0; j<nTestColumnCount; j++)
			{
				if(j<nViewColumnCount)
				{
					tmpRec.setData(0,j,recSettings.getDataRef(0,j));
				}
				else
				{
					if(j==nViewColumnCount)
					{
						//CALENDAR_SECTIONS -> 0-SHOW BOTH, 1-SHOW UPPER, 2-SHOW LOWER.
						tmpRec.setData(0,j,0);
					}
					else
					{
						//Upper calendar left section witdth.
						tmpRec.setData(0,j,200);
					}
				}
			}

			QByteArray newViewByte=XmlUtil::ConvertRecordSet2ByteArray_Fast(tmpRec);
			recCalendarViews.setData(i, "BCALV_VALUE_BYTE",newViewByte);
		}
	}

	//----------------------------------------------
	//					Save view.
	//----------------------------------------------
	//--------------------------------------
	//START TRANSACTION
	//--------------------------------------
	BUS_CAL_VIEW_ORM.GetDbSqlQuery()->BeginTransaction(pStatus);
	if(!pStatus.IsOK()) return;

	//Write view.
	BUS_CAL_VIEW_ORM.Write(pStatus, recCalendarViews);
	if (!pStatus.IsOK())
	{
		BUS_CAL_VIEW_ORM.GetDbSqlQuery()->Rollback();
		return;
	}

	//--------------------------------------
	//COMMIT TRANSACTION
	//--------------------------------------
	BUS_CAL_VIEW_ORM.GetDbSqlQuery()->Commit(pStatus);
}

void DbOrganizer::f0104_00_AccRSetUpgrade_Calendar(Status &pStatus)
{

	//Instantiate Access right manipulator.
	AccessRightsManipulator AccRghtManipulator(m_pManager);
	AccRghtManipulator.InsertNewAccRSetToDatabase(pStatus, CALENDAR_ACCESS);

	AccRghtManipulator.InsertNewAccessRightToAccRSet(pStatus, CALENDAR_ACCESS, OPEN_CALENDAR);
	AccRghtManipulator.InsertNewAccessRightToAccRSet(pStatus, CALENDAR_ACCESS, ENTER_MODIFY_CALENDAR);
}



//all fields from lstFields are copied to temp table, dropped, recreate as not null, then filled again: THIS IS ONLY NEEDED FOR FIREBIRD!!!
void DbOrganizer::ChangeFromNotNull2NullFields(Status &pStatus, QString strTableName, QString strIDColumnName, QStringList lstFields, QStringList lstFieldsDataTypes)
{
	if (m_pManager->GetDbType()!=DBTYPE_FIREBIRD)
		return;

	DbSqlQuery query(pStatus,m_pManager);
	if(!pStatus.IsOK())return;

	//get fields:
	QString strSQLOnlyFields=strIDColumnName+",";
	int nSize=lstFields.size();
	for(int i=0;i<nSize;i++)
	{
		strSQLOnlyFields.append(lstFields.at(i));
		strSQLOnlyFields.append(",");
	}
	strSQLOnlyFields.chop(1);

	//SELECT DATA
	QString strSQL="SELECT "+strSQLOnlyFields+" FROM "+strTableName;
	query.Execute(pStatus,strSQL);
	if(!pStatus.IsOK())return;
	DbRecordSet lstData;
	query.FetchData(lstData);


	//DROP COLUMNS:
	m_DbObjectManager->DropColumns(pStatus,strTableName,lstFields,m_pDb);
	if(!pStatus.IsOK())return;

	//CREATE NEW FIELDS  (one by one):
	for(int i=0;i<nSize;i++)
	{
		strSQL="ALTER TABLE "+strTableName+" ADD "+lstFields.at(i)+" "+lstFieldsDataTypes.at(i)+" ";
		query.Execute(pStatus,strSQL);
		if(!pStatus.IsOK())return;
	}

	//COPY BACK DATA: 
	strSQL="UPDATE "+strTableName+" SET ";
	for(int i=0;i<nSize;i++)
	{
		strSQL+=lstFields.at(i)+" = ?,";
	}
	strSQL.chop(1);
	strSQL+=" WHERE "+strIDColumnName+"=?";
	query.Prepare(pStatus,strSQL);
	if(!pStatus.IsOK())return;

	int nSize2=lstData.getRowCount();
	for(int i=0;i<nSize2;i++)
	{
		for(int k=0;k<nSize;k++)
			query.bindValue(k,lstData.getDataRef(i,k+1));

		query.bindValue(nSize,lstData.getDataRef(i,0));

		query.ExecutePrepared(pStatus);
		if(!pStatus.IsOK())return;
	}

	
}


void DbOrganizer::f0105_00_FixHierarchyStructureProjects(Status &pStatus)
{
	//HC flag fix
	//LEVEL flag fix

	if (m_pManager->GetDbType()!=DBTYPE_FIREBIRD)
	{
		Q_ASSERT(false); //triba napravit
		return;
	}

	DbSqlQuery query(pStatus,m_pManager);
	if(!pStatus.IsOK())return;

	//update BUSP_HASCHILDREN=1
	QString strSQL="UPDATE BUS_PROJECTS SET BUSP_HASCHILDREN=1 WHERE BUSP_ID IN	(SELECT DISTINCT A.BUSP_ID FROM BUS_PROJECTS AS A INNER JOIN BUS_PROJECTS AS B ON SUBSTRING(B.BUSP_CODE FROM 1 FOR CHAR_LENGTH(A.BUSP_CODE))=A.BUSP_CODE  AND CHAR_LENGTH(A.BUSP_CODE)<CHAR_LENGTH(B.BUSP_CODE))";
	query.Execute(pStatus,strSQL);
	if(!pStatus.IsOK())return;

	//update BUSP_HASCHILDREN=0
	strSQL="UPDATE BUS_PROJECTS SET BUSP_HASCHILDREN=0 WHERE BUSP_ID NOT IN	(SELECT DISTINCT A.BUSP_ID FROM BUS_PROJECTS AS A INNER JOIN BUS_PROJECTS AS B ON SUBSTRING(B.BUSP_CODE FROM 1 FOR CHAR_LENGTH(A.BUSP_CODE))=A.BUSP_CODE  AND CHAR_LENGTH(A.BUSP_CODE)<CHAR_LENGTH(B.BUSP_CODE))";
	query.Execute(pStatus,strSQL);
	if(!pStatus.IsOK())return;

	//update BUSP_LEVEL
	strSQL="UPDATE BUS_PROJECTS SET BUSP_LEVEL=(CHAR_LENGTH(BUSP_CODE) - CHAR_LENGTH(REPLACE (BUSP_CODE,'.','' )))";
	query.Execute(pStatus,strSQL);
	if(!pStatus.IsOK())return;

}

//B.T. 2013.02.10: due to import of projects from the SPC, we must change level logic in the SC: level = 0 is first level, then so on...
void DbOrganizer::f0117_00_FixHierarchyLevel(Status &pStatus)
{
	DbSqlQuery query(pStatus,m_pManager);
	if(!pStatus.IsOK())return;

	//update BUS_PROJECTS
	QString strSQL="UPDATE BUS_PROJECTS SET BUSP_LEVEL=(CHAR_LENGTH(BUSP_CODE) - CHAR_LENGTH(REPLACE (BUSP_CODE,'.','' ))-1)";
	query.Execute(pStatus,strSQL);
	if(!pStatus.IsOK())return;

	//update BUS_ORGANIZATION
	strSQL="UPDATE BUS_ORGANIZATIONS SET BORG_LEVEL=(CHAR_LENGTH(BORG_CODE) - CHAR_LENGTH(REPLACE (BORG_CODE,'.','' ))-1)";
	query.Execute(pStatus,strSQL);
	if(!pStatus.IsOK())return;

	//update BUS_DEPARTMENT
	strSQL="UPDATE BUS_DEPARTMENTS SET BDEPT_LEVEL=(CHAR_LENGTH(BDEPT_CODE) - CHAR_LENGTH(REPLACE (BDEPT_CODE,'.','' ))-1)";
	query.Execute(pStatus,strSQL);
	if(!pStatus.IsOK())return;

	//update BUS_GROUP_ITEMS
	strSQL="UPDATE BUS_GROUP_ITEMS SET BGIT_LEVEL=(CHAR_LENGTH(BGIT_CODE) - CHAR_LENGTH(REPLACE (BGIT_CODE,'.','' ))-1)";
	query.Execute(pStatus,strSQL);
	if(!pStatus.IsOK())return;

	//update BUS_COST_CENTERS
	strSQL="UPDATE BUS_COST_CENTERS SET BCTC_LEVEL=(CHAR_LENGTH(BCTC_CODE) - CHAR_LENGTH(REPLACE (BCTC_CODE,'.','' ))-1)";
	query.Execute(pStatus,strSQL);
	if(!pStatus.IsOK())return;

}

void DbOrganizer::f0108_00_CalendarViewUpgradeOnDeleteCascade(Status &pStatus)
{
	//----------------------------------------------
	//				Get views and update.
	//----------------------------------------------
	//Contact data Table ORM.
	DbSimpleOrm BUS_CAL_VIEW_ORM(pStatus, BUS_CAL_VIEW, m_pManager); 
	if(!pStatus.IsOK()) return;
	//Read.
	DbRecordSet recCalendarViews;
	BUS_CAL_VIEW_ORM.Read(pStatus, recCalendarViews);

	//_DUMP(recCalendarViews);

	int nRows=recCalendarViews.getRowCount();
	for (int i=0; i<nRows; i++)
	{
		int nCalendarViewID= recCalendarViews.getDataRef(i, "BCALV_ID").toInt();

		//Get data from byte array.
		//_DUMP(recCalendarViews.getRow(i));
		QByteArray bytView = recCalendarViews.getDataRef(i, "BCALV_VALUE_BYTE").toByteArray();
		DbRecordSet recSettings;
		recSettings = XmlUtil::ConvertByteArray2RecordSet_Fast(bytView);

		//_DUMP(recSettings);
		if (recSettings.getRowCount()==0)
		{
			continue;
		}


		//Set calendar view data from old data.	
		recCalendarViews.setData(i, "BCALV_FILTER_BY_NOTYPE",	recSettings.getDataRef(0, "FILTER_BY_NO_TYPE").toInt());
		recCalendarViews.setData(i, "BCALV_FILTER_BY_TYPE",		recSettings.getDataRef(0, "FILTER_BY_TYPE").toInt());
		recCalendarViews.setData(i, "BCALV_SHOW_PRELIMINARY",	recSettings.getDataRef(0, "SHOW_PRELIMINARY").toInt());
		recCalendarViews.setData(i, "BCALV_SHOW_INFORMATION",	recSettings.getDataRef(0, "SHOW_INFORMATION").toInt());
		recCalendarViews.setData(i, "BCALV_TIME_SCALE",			recSettings.getDataRef(0, "CALENDAR_TIME_SCALE").toInt());
		recCalendarViews.setData(i, "BCALV_DATE_RANGE",			recSettings.getDataRef(0, "CALENDAR_DATE_RANGE").toInt());
		recCalendarViews.setData(i, "BCALV_SCROLL_TO_TIME",		recSettings.getDataRef(0, "CALENDAR_SCROLL_TO_TIME").toDateTime());

		QByteArray calHeightByte = recSettings.getDataRef(0, "MULTIDAY_CALENDAR_HEIGHT").toByteArray();
		recCalendarViews.setData(i, "BCALV_MULTIDAY_HEIGHT",calHeightByte);

//		recCalendarViews.setData(i, "BCALV_COLUMN_WIDTH",		recSettings.getDataRef(0, "CALENDAR_COLUMN_WIDTH").toInt());

		QByteArray calGeometryByte = recSettings.getDataRef(0, "CALENDAR_FUI_GEOMETRY").toByteArray();
		recCalendarViews.setData(i, "BCALV_FUI_GEOMETRY",		calGeometryByte);
		recCalendarViews.setData(i, "BCALV_FUI_POSITION_X",		recSettings.getDataRef(0, "CALENDAR_FUI_POSITION_X").toInt());
		recCalendarViews.setData(i, "BCALV_FUI_POSITION_Y",		recSettings.getDataRef(0, "CALENDAR_FUI_POSITION_Y").toInt());
		recCalendarViews.setData(i, "BCALV_SHOW_TASKS",			recSettings.getDataRef(0, "CALENDAR_SHOW_TASKS").toInt());
		recCalendarViews.setData(i, "BCALV_SECTIONS",			recSettings.getDataRef(0, "CALENDAR_SECTIONS").toInt());
		if (recSettings.getColumnIdx("CALENDAR_OVERVIEW_LEFT_COLUMN_WIDTH")>=0)
		{
			recCalendarViews.setData(i, "BCALV_OVERVIEWLEFTCOLWIDTH",recSettings.getDataRef(0, "CALENDAR_OVERVIEW_LEFT_COLUMN_WIDTH").toInt());
		}

		//Get checked calendar types.
		DbRecordSet recTVIEW_BUS_CAL_VIEW_CE_TYPES;
		recTVIEW_BUS_CAL_VIEW_CE_TYPES.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_VIEW_CE_TYPES));

		QByteArray bytTypeIDs = recSettings.getDataRef(0, "TASK_TYPE_CHECKED_ITEMS").toByteArray();
		DbRecordSet recCheckedItems = XmlUtil::ConvertByteArray2RecordSet_Fast(bytTypeIDs);

		DbSqlQuery query(pStatus, m_pManager);
		if(!pStatus.IsOK()) return;
		QString strWhere = "SELECT * FROM BUS_CM_TYPES WHERE BCMT_ID = ?";

		//Write them in BUS_CAL_VIEW_CE_TYPES table.
		int nTypeItemsCount=recCheckedItems.getRowCount();
		for (int j=0; j<nTypeItemsCount; j++)
		{
			DbRecordSet recTmp;
			int nID = recCheckedItems.getDataRef(j, "CHECKED_ITEM_ID").toInt();
			//Check foreign keys.
			query.Prepare(pStatus, strWhere);
			if(!pStatus.IsOK()) return;
			query.bindValue(0, recCheckedItems.getDataRef(j, "CHECKED_ITEM_ID").toInt());
			query.ExecutePrepared(pStatus);
			if(!pStatus.IsOK()) return;
			query.FetchData(recTmp);

			if (!recTmp.getRowCount())
				continue;

			//_DUMP(recCheckedItems);
			recTVIEW_BUS_CAL_VIEW_CE_TYPES.addRow();
			int nRow=recTVIEW_BUS_CAL_VIEW_CE_TYPES.getRowCount()-1;
			recTVIEW_BUS_CAL_VIEW_CE_TYPES.setData(nRow, "BCCT_CAL_VIEW_ID",nCalendarViewID);
			recTVIEW_BUS_CAL_VIEW_CE_TYPES.setData(nRow, "BCCT_TYPE_ID",recCheckedItems.getDataRef(j, "CHECKED_ITEM_ID").toInt());
			recTVIEW_BUS_CAL_VIEW_CE_TYPES.setData(nRow, "BCCT_IS_SELECTED",recCheckedItems.getDataRef(j, "CHECKED_ITEM_STATE").toInt()); //Qt::CheckedState.
		}

		//Get calendar entities.
		DbRecordSet recTVIEW_BUS_CAL_VIEW_ENTITIES;
		recTVIEW_BUS_CAL_VIEW_ENTITIES.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_VIEW_ENTITIES));

		QByteArray bytEntities = recSettings.getDataRef(0, "CALENDAR_ENTITIES").toByteArray();
		DbRecordSet recEntities = XmlUtil::ConvertByteArray2RecordSet_Fast(bytEntities);

		//Get current selected item.
		int nCurrentEntityRow = recSettings.getDataRef(0, "CALENDAR_CURRENT_ENTITY").toInt();

		//Write them in BUS_CAL_VIEW_ENTITIES table.
		bool bSelectedFound=false;
		int nEntityCount=recEntities.getRowCount();
		for (int j=0; j<nEntityCount; j++)
		{
			int nEntityType=recEntities.getDataRef(j, "ENTITY_TYPE").toInt();

			//Check foreign keys.
			if(nEntityType==GlobalConstants::TYPE_CAL_USER_SELECT)
			{
				strWhere = "SELECT * FROM BUS_PERSON WHERE BPER_ID = ?";
			}
			else if (nEntityType==GlobalConstants::TYPE_CAL_PROJECT_SELECT)
			{
				strWhere = "SELECT * FROM BUS_PROJECTS WHERE BUSP_ID = ?";
			}
			else if (nEntityType==GlobalConstants::TYPE_CAL_CONTACT_SELECT)
			{
				strWhere = "SELECT * FROM BUS_CM_CONTACT WHERE BCNT_ID = ?";
			}
			else if (nEntityType==GlobalConstants::TYPE_CAL_RESOURCE_SELECT)
			{
				strWhere = "SELECT * FROM BUS_CAL_RESOURCE WHERE BRES_ID = ?";
			}

			query.Prepare(pStatus, strWhere);
			if(!pStatus.IsOK()) return;

			if(nEntityType==GlobalConstants::TYPE_CAL_USER_SELECT)
			{
				query.bindValue(0, recEntities.getDataRef(j, "ENTITY_PERSON_ID").toInt());
			}
			else
			{
				query.bindValue(0, recEntities.getDataRef(j, "ENTITY_ID").toInt());
			}

			query.ExecutePrepared(pStatus);
			if(!pStatus.IsOK()) return;
			DbRecordSet recTmp;
			query.FetchData(recTmp);

			if (!recTmp.getRowCount())
				continue;

			//Now add.
			recTVIEW_BUS_CAL_VIEW_ENTITIES.addRow();
			int nRow=recTVIEW_BUS_CAL_VIEW_ENTITIES.getRowCount()-1;
			recTVIEW_BUS_CAL_VIEW_ENTITIES.setData(nRow, "BCVE_CAL_VIEW_ID", nCalendarViewID);
			recTVIEW_BUS_CAL_VIEW_ENTITIES.setData(nRow, "BCVE_SORT_CODE", j);

			if (nCurrentEntityRow==j)
			{
				bSelectedFound=true;
				recTVIEW_BUS_CAL_VIEW_ENTITIES.setData(nRow, "BCVE_IS_SELECTED", 1);
			}
			else
			{
				recTVIEW_BUS_CAL_VIEW_ENTITIES.setData(nRow, "BCVE_IS_SELECTED", 0);
			}

			//Check is there a selected row.
			if (j==(nEntityCount-1) && !bSelectedFound)
			{
				recTVIEW_BUS_CAL_VIEW_ENTITIES.setData(nRow, "BCVE_IS_SELECTED", 1);
			}

			if(nEntityType==GlobalConstants::TYPE_CAL_USER_SELECT)
			{
				//User has stored both user and contact data.
				recTVIEW_BUS_CAL_VIEW_ENTITIES.setData(nRow, "BCVE_CONTACT_ID", recEntities.getDataRef(j, "ENTITY_ID").toInt());
				recTVIEW_BUS_CAL_VIEW_ENTITIES.setData(nRow, "BCVE_USER_ID", recEntities.getDataRef(j, "ENTITY_PERSON_ID").toInt());
			}
			else if (nEntityType==GlobalConstants::TYPE_CAL_PROJECT_SELECT)
			{
				recTVIEW_BUS_CAL_VIEW_ENTITIES.setData(nRow, "BCVE_PROJECT_ID", recEntities.getDataRef(j, "ENTITY_ID").toInt());
			}
			else if (nEntityType==GlobalConstants::TYPE_CAL_CONTACT_SELECT)
			{
				recTVIEW_BUS_CAL_VIEW_ENTITIES.setData(nRow, "BCVE_CONTACT_ID", recEntities.getDataRef(j, "ENTITY_ID").toInt());
			}
			else if (nEntityType==GlobalConstants::TYPE_CAL_RESOURCE_SELECT)
			{
				recTVIEW_BUS_CAL_VIEW_ENTITIES.setData(nRow, "BCVE_RESOURCE_ID", recEntities.getDataRef(j, "ENTITY_ID").toInt());
			}
		}

		QByteArray byteEmpty;
		recCalendarViews.setData(i, "BCALV_VALUE_BYTE", byteEmpty);


		//----------------------------------------------
		//		Save view, entities and type id's.
		//----------------------------------------------
		//--------------------------------------
		//START TRANSACTION
		//--------------------------------------

		DbSimpleOrm BUS_CAL_VIEW_ENTITIES_ORM(pStatus, BUS_CAL_VIEW_ENTITIES, m_pManager); 
		if(!pStatus.IsOK()) return;

		if (recTVIEW_BUS_CAL_VIEW_ENTITIES.getRowCount()>0)
		{
			//Write entities.
			BUS_CAL_VIEW_ENTITIES_ORM.Write(pStatus, recTVIEW_BUS_CAL_VIEW_ENTITIES);
			if (!pStatus.IsOK())
			{
				return;
			}
		}

		DbSimpleOrm BUS_CAL_VIEW_CE_TYPES_ORM(pStatus, BUS_CAL_VIEW_CE_TYPES, m_pManager); 
		if(!pStatus.IsOK()) return;

		if (recTVIEW_BUS_CAL_VIEW_CE_TYPES.getRowCount()>0)
		{
			//Write Type id's.
			BUS_CAL_VIEW_CE_TYPES_ORM.Write(pStatus, recTVIEW_BUS_CAL_VIEW_CE_TYPES);
			if (!pStatus.IsOK())
			{
				return;
			}
		}

		//Write view.
		BUS_CAL_VIEW_ORM.Write(pStatus, recCalendarViews);
		if (!pStatus.IsOK())
		{
			return;
		}
	}
}


//MB: 2013.11.26 = MB said that this is no longer needed, just for SPC see users: CreateFBUsers()

void DbOrganizer::CreateReadOnlyUserForODBCAccess(Status &pStatus)
{
	/*
	//reserve exc conn.
	bool bConnReserved=false;
	if (m_pDb==NULL)
	{
		m_pManager->ReserveExclusiveDbConnection(pStatus,&m_pDb);
		if(!pStatus.IsOK()){return;}
		bConnReserved=true;
	}

	//create user
	m_DbObjectManager->CreateReadOnlyUser_Level1(pStatus,"ODBCREADER","sokrep24","ODBC Access", "Read Only User","SYSDBA","masterkey",m_pDb);

	//release connection
	if (bConnReserved)
	{
		m_pManager->ReleaseExclusiveDbConnection(&m_pDb);
	}*/
	
}

//file with insert into statements from SPC ORACLE Oracle dump database into theFB
//Note: before import data, execute U:\Oracle Migration\Data SPCT\DDL\Converted2FB\A_BeforeImportData.SQL
//call this function with file: U:\Oracle Migration\Data SPCT\DDL\Converted2FB\_Data\original_export3.sql
//after: execute U:\Oracle Migration\Data SPCT\DDL\Converted2FB\B_AfterImportData.SQL

void DbOrganizer::ImportDataFromOracleExport(Status &pStatus,QString strDataFile, int &nProcessedInsertStatements, int &nProcessedSequenceUpdateStatements)
{
	QByteArray buffer_err;
	bool bErrOccured=false;

	pStatus.setError(0);
	nProcessedInsertStatements=0;
	nProcessedSequenceUpdateStatements=0;
	int nErrorsDetected=0;

	QFileInfo info(strDataFile);
	QString strStatus_file_path=info.absolutePath()+"/status.log";
	QFile status_file(strStatus_file_path);

	//reserve exc conn.
	bool bConnReserved=false;
	if (m_pDb==NULL)
	{
		m_pManager->ReserveExclusiveDbConnection(pStatus,&m_pDb);
		if(!pStatus.IsOK()){return;}
		bConnReserved=true;
	}

	DbSqlQuery query(pStatus,m_pManager);
	if(!pStatus.IsOK())return;

	//first re-arrange decimal format from '8,25' to '8.25'
	//remove REM
	if(!strDataFile.isEmpty())
	{
		QByteArray buffer;
		QFile filek(strDataFile);
		if(filek.open(QIODevice::ReadOnly))
		{
			buffer = filek.readAll();
			filek.close();

			//-----------------------------------------------
			//purge REM's
			//-----------------------------------------------
			//buffer.replace("REM ","--- ");

			//-----------------------------------------------
			//purge empty strings ' '
			//-----------------------------------------------
			//buffer.replace("' '","''");

			//-----------------------------------------------
			//replace decimals with dot(8,25 to 8.25)
			//-----------------------------------------------
			int nSize=buffer.size();
			int nPos=0;
			int nOcc=0;
			do 
			{
				nPos=buffer.indexOf("'",nPos);
				if (nPos>=0)
				{
					int nPos2=buffer.indexOf("'",nPos+1);
					if (nPos2>nPos)
					{
						int nComma=buffer.indexOf(",",nPos);
						if (nComma>nPos && nComma<nPos2)
						{
							//test if numbeR:
							QString str_test(buffer.mid(nPos+1,nPos2-nPos-1));
							bool bOK=false;
							float x = str_test.toDouble(&bOK); 
							if (bOK)
							{
								buffer.replace(nComma,1,".");
								nOcc++;
							}
						}
						nPos=nPos2;
					}
					else
						nPos++;
				}
			} while (nPos>=0);


			QTime time_test;
			time_test.start();


			//-----------------------------------------------
			//isolate insert into statements and execute 'em
			//-----------------------------------------------
			QStringList lstTables; //list of tables with data
			QStringList lstSequences; //list of sequence names

			QString strTableName="";
			QString strSequence="";

			nPos=0;
			int nInserts=0;
			do 
			{
				nPos=buffer.indexOf("Insert into",nPos);
				if (nPos>=0)
				{
					int nPos2=buffer.indexOf(");",nPos+1);
					if (nPos2>nPos)
					{
						QString strSQL(buffer.mid(nPos,nPos2-nPos+1).trimmed());

						//table name
						QString strTableName_Local;
						int nLen1=QString("Insert into ").length();
						int nPos3=strSQL.indexOf(" ",nLen1+1);
						if (nPos3>=0)
							strTableName_Local=strSQL.mid(nLen1,nPos3-nLen1).trimmed();

						//just add new if changes:
						if (strTableName_Local!=strTableName)
						{
							strTableName=strTableName_Local;

							if (!(strTableName=="LONG_FIELDS" || strTableName=="LONGR_FIELDS"))
							{
								//sequence name
								QStringList lstColumns=strSQL.mid(strSQL.indexOf("(")+1,strSQL.indexOf(")")-strSQL.indexOf("(")-1).split(",");
								for (int i=0;i<lstColumns.size();i++)
								{
									if (lstColumns.at(i).toLower().contains("_sequence"))
									{
										strSequence=lstColumns.at(i);
										break;
									}
								}

								if (!strSequence.isEmpty())
								{
									lstTables<<strTableName;
									lstSequences<<strSequence;
								}

							}

							//on every table change write status:
							int nSecs = time_test.elapsed()/1000;
							QString strStatus=QString("Processed %1 inserts, encounter %2 errors, now processing %3, time elapsed (sec): %4").arg(nProcessedInsertStatements).arg(nErrorsDetected).arg(strTableName).arg(nSecs);
							if(status_file.open(QIODevice::WriteOnly))
							{
								status_file.write(strStatus.toLatin1());
								status_file.close();
							}

						}


						
						//-----------------------------------------------
						//special for LONGR_FIELDS:
						//-----------------------------------------------
						if (strTableName=="LONGR_FIELDS")
						{
							QString strRIDstrSQL =strSQL.section("'",1,1);
							if (strRIDstrSQL.length()>0)
							{
								//test if table is LONGR_FIELDS, if so then...break to more then 1 statement
								//find start of value tag:
								int nPosBinStart=strSQL.indexOf("','");

								//qDebug()<<strRIDstrSQL<<" byte insert ALL: "<<strSQL.section("'",3,3).size();
								if (nPosBinStart<0)
								{
									Status err;
									query.Execute(err,strSQL,true);
									if (!err.IsOK())
									{
										bErrOccured=true;
										buffer_err.append(QString("Error \"%1\" while executing statement: %2\n\r").arg(err.getErrorText()).arg(strSQL).toLatin1());
										nErrorsDetected++;
									}
									else
										nProcessedInsertStatements++;
								}
								else
								{
									bool bFirstChunk=true;
									QString strSQLBin;
									int nPosEnd=strSQL.length()-1; //exclude ')' at end of SQL
									nPosBinStart+=3;
									while(nPosBinStart+4096<nPosEnd)
									{
										if (bFirstChunk)
										{
											strSQLBin=strSQL.left(nPosBinStart+4096)+"')";
											strSQLBin.insert(nPosBinStart-1,"x");
											bFirstChunk=false;
											//qDebug()<<strRIDstrSQL<<" byte insert 1st: "<<strSQLBin.section("'",3,3).size();
										}
										else
										{
											strSQLBin="UPDATE LONGR_FIELDS SET TEXT=TEXT || x'"+strSQL.mid(nPosBinStart,4096)+"'";
											strSQLBin+=" WHERE RID="+strRIDstrSQL;
											//qDebug()<<strRIDstrSQL<<" byte update: "<<strSQLBin.section("'",1,1).size();
										}

										Status err;
										query.Execute(err,strSQLBin,true);
										if (!err.IsOK())
										{
											bErrOccured=true;
											buffer_err.append(QString("Error \"%1\" while executing statement: %2\n\r").arg(err.getErrorText()).arg(strSQLBin).toLatin1());
											nErrorsDetected++;
										}
										else
											nProcessedInsertStatements++;

										nPosBinStart+=4096;
									}

									//execute last statement (or first if data <4096:
									if (bFirstChunk) //first
									{
										strSQLBin=strSQL;
										strSQLBin.insert(nPosBinStart-1,"x");
									}
									else
									{
										if (!strSQL.mid(nPosBinStart,4096).trimmed().isEmpty())
										{
											strSQLBin="UPDATE LONGR_FIELDS SET TEXT=TEXT || x'"+strSQL.mid(nPosBinStart,4096); //get all string till end 
											strSQLBin.chop(1);//then chop last char:")"
											strSQLBin+=" WHERE RID="+strRIDstrSQL; 
											//qDebug()<<strRIDstrSQL<<" byte update last: "<<strSQLBin.section("'",1,1).size();
										}
										else
											strSQLBin="";
									}

									if (!strSQLBin.isEmpty()) //if non empty then execute
									{
										Status err;
										query.Execute(err,strSQLBin,true);
										if (!err.IsOK())
										{
											bErrOccured=true;
											buffer_err.append(QString("Error \"%1\" while executing statement: %2\n\r").arg(err.getErrorText()).arg(strSQLBin).toLatin1());
											nErrorsDetected++;
										}
										else
											nProcessedInsertStatements++;
									}

								}
							}
						}
						else
						{
							
							//-----------------------------------------------
							//	Normal INSERT
							//-----------------------------------------------
							Status err;
							query.Execute(err,strSQL,true);
							if (!err.IsOK())
							{
								bErrOccured=true;
								buffer_err.append(QString("Error \"%1\" while executing statement: %2\n\r").arg(err.getErrorText()).arg(strSQL).toLatin1());
								nErrorsDetected++;
							}
							else
								nProcessedInsertStatements++;
							
						}
						nPos=nPos2;
					}
					else
						nPos++;
				}
			} while (nPos>=0);
		

			//-----------------------------------------------
			//parse through all tables and update sequences to max values of tables
			//-----------------------------------------------

			//on every table change write status:
			int nSecs = time_test.elapsed()/1000;
			QString strStatus=QString("Processed %1 inserts, encounter %2 errors, now processing sequences, time elapsed (sec): %3").arg(nProcessedInsertStatements).arg(nErrorsDetected).arg(nSecs);
			if(status_file.open(QIODevice::WriteOnly))
			{
				status_file.write(strStatus.toLatin1());
				status_file.close();
			}

			nSize=lstSequences.size();
			for (int i=0;i<nSize;i++)
			{
				QString strSeq="SELECT MAX("+lstSequences.at(i)+") FROM "+lstTables.at(i);
				Status err;
				query.Execute(err,strSeq,true);
				if (!err.IsOK())
				{
					bErrOccured=true;
					buffer_err.append(QString("Error \"%1\" while executing statement: %2\n\r").arg(err.getErrorText()).arg(strSeq).toLatin1());
					nErrorsDetected++;
					continue;
				}

				int nNextID=1;
				if (query.next())
					nNextID=query.value(0).toInt();
				
				strSeq="ALTER SEQUENCE SEQ_"+lstSequences.at(i)+" RESTART WITH "+QString::number(nNextID);
				query.Execute(err,strSeq,true);
				if (!err.IsOK())
				{
					bErrOccured=true;
					buffer_err.append(QString("Error \"%1\" while executing statement: %2\n\r").arg(err.getErrorText()).arg(strSeq).toLatin1());
					nErrorsDetected++;
				}
				else
					nProcessedSequenceUpdateStatements++;
			}

			//-----------------------------------------------
			//special for long and longr fields:
			//-----------------------------------------------

			{
				int nNextID_1=1;
				int nNextID_2=1;

				QString strSeq="SELECT MAX(RID) FROM LONGR_FIELDS";
				Status err;
				query.Execute(err,strSeq,true);
				if (!err.IsOK())
				{
					bErrOccured=true;
					buffer_err.append(QString("Error \"%1\" while executing statement: %2\n\r").arg(err.getErrorText()).arg(strSeq).toLatin1());
					nErrorsDetected++;
				}
				else
					if (query.next())
						nNextID_1=query.value(0).toInt();


				strSeq="SELECT MAX(RID) FROM LONG_FIELDS";
				query.Execute(err,strSeq,true);
				if (!err.IsOK())
				{
					bErrOccured=true;
					buffer_err.append(QString("Error \"%1\" while executing statement: %2\n\r").arg(err.getErrorText()).arg(strSeq).toLatin1());
					nErrorsDetected++;
				}
				else
					if (query.next())
						nNextID_2=query.value(0).toInt();

				int nNextID=nNextID_2;
				if (nNextID_1>nNextID_2)
					nNextID=nNextID_1;

				strSeq="ALTER SEQUENCE SEQ_LONG_RID RESTART WITH "+QString::number(nNextID);
				query.Execute(err,strSeq,true);
				if (!err.IsOK())
				{
					bErrOccured=true;
					buffer_err.append(QString("Error \"%1\" while executing statement: %2\n\r").arg(err.getErrorText()).arg(strSeq).toLatin1());
					nErrorsDetected++;
				}
				else
					nProcessedSequenceUpdateStatements++;
			}

			//on every table change write status:
			nSecs = time_test.elapsed()/1000;
			strStatus=QString("Processed %1 inserts, encounter %2 errors, finished all in (sec): %3").arg(nProcessedInsertStatements).arg(nErrorsDetected).arg(nSecs);
			if(status_file.open(QIODevice::WriteOnly))
			{
				status_file.write(strStatus.toLatin1());
				status_file.close();
			}


			if (buffer_err.size()>0)
			{
				QFileInfo info(strDataFile);
				QString strErrorFile=info.absolutePath()+"/error.log";
				QFile err_file(strErrorFile);
				if(err_file.open(QIODevice::WriteOnly))
				{
					err_file.write(buffer_err);
					err_file.close();
				}

				pStatus.setError(1,QString("Some errors occurred during import. Check error log: %1)").arg(strErrorFile));
			}
		
		 }
	}

	//release connection
	if (bConnReserved)
	{
		m_pManager->ReleaseExclusiveDbConnection(&m_pDb);
	}

}

//Note: function is used to import data from DataMigration directory of Omnis export (for Oracle)
//This function will be called only once at initialization of the FB database for SPC data
//This function will: organize database with SPC tables, import data, adjust data, create seq generators
void DbOrganizer::ImportDataFromOmnisExport(Status &pStatus,QString strDataMigrationDir, int &nProcessedInsertStatements, bool bDropExisting)
{

	QByteArray byteErrLog;
	int nErrorsDetected=0;
	m_StatusLogger.SetLogPath(QCoreApplication::applicationDirPath()+"/status.log");
	m_StatusLogger.WriteStep("Step 1/10: Calculating total records for import");

	//----------------------------------------------
	//	INIT LOG 
	//----------------------------------------------

	
	//get all records from all tables:

	QDir dirCTL(strDataMigrationDir+"/CtrlFiles");
	QStringList lstFilter;
	lstFilter.append("*.ctl");
	QFileInfoList lstFiles=dirCTL.entryInfoList(lstFilter,QDir::Files);
	int nSize=lstFiles.size();
	for(int i=0;i<nSize;++i)
	{
		QString strTableName=lstFiles.at(i).completeBaseName();
		if (strTableName=="FPROJCT")
		{
			qDebug()<<1;
		}

		QString strDataFile=strDataMigrationDir+"/AsciiFiles/"+strTableName+".dat";

		int nCntData=0,nCntBinData=0;
		O2F_CountRecordsFromFile(strDataFile,nCntData);
		m_StatusLogger.SetDataRecords(strTableName,nCntData);

		QStringList lstBinFilter;
		lstBinFilter.append("*.bin");
		lstBinFilter.append("*.txt");
		QDir dirBin(strDataMigrationDir+"/BinFiles/"+strTableName);
		QFileInfoList lstBinFiles=dirBin.entryInfoList(lstBinFilter,QDir::Files);
		int nSizeBin=lstBinFiles.size();
		for (int j=0;j<nSizeBin;j++)
		{
			O2F_CountRecordsFromFile(lstBinFiles.at(j).absoluteFilePath(),nCntBinData);
		}

		m_StatusLogger.SetBinRecords(strTableName,nCntBinData);
	}
	m_StatusLogger.RecalcTotal();
	m_StatusLogger.SetTableCnt(nSize);



	//----------------------------------------------
	//	REORGANIZE & PREPARE DATABASE
	//----------------------------------------------

	m_StatusLogger.WriteStep("Step 2/10: Organize Database");
	//reorganize database:
	OrganizeDatabase(pStatus,bDropExisting);
	if(!pStatus.IsOK())
		return;

	//reserve exc conn.
	bool bConnReserved=false;
	if (m_pDb==NULL)
	{
		m_pManager->ReserveExclusiveDbConnection(pStatus,&m_pDb);
		if(!pStatus.IsOK()){return;}
		bConnReserved=true;
	}

	InitDboObjects(m_pDb);

	//Delete row from FL_DEFAULT
	Status err;
	DbSqlQuery query(err,m_pManager);
	if(!err.IsOK())
		return;

	QString strSQL="DELETE FROM FL_DEFAULT";
	query.Execute(err,strSQL);
	if(!err.IsOK())
		return;


	m_StatusLogger.WriteStep("Step 3/10: Delete table indexes, views, triggers before importing data");

	int lstTableCreatorSize = m_lstTableCreatorSPC.size();
	//drop all views, triggers and constraints (unique, fk)
	for(int i=0; i < lstTableCreatorSize; ++i)
	{
		qDebug()<<"Deleting constraints and views for table:"<<m_lstTableCreatorSPC.at(i)->m_TableData.m_strTableName;
		m_lstTableCreatorSPC.at(i)->DeleteTableConstraints(pStatus,true);
		if(!pStatus.IsOK())
		{
			return;
		}
	}


	//----------------------------------------------
	//	IMPORT DATA, CREATE SEQ, ADJUST BINARY DATA....
	//----------------------------------------------

	//find fprojct: must be first, before charges, faktura zehlung...for triggers..
	for(int i=0;i<nSize;++i)
	{
		if (lstFiles.at(i).completeBaseName().toUpper()=="FPROJCT")
		{
			QFileInfo fprojct=lstFiles.takeAt(i);
			lstFiles.prepend(fprojct);
		}
		if (lstFiles.at(i).completeBaseName().toUpper()=="FASSIGN")
		{
			QFileInfo fassign=lstFiles.takeAt(i);
			lstFiles.prepend(fassign);
		}
	}

	bool bErrorOccured=false;
	for(int i=0;i<nSize;++i)
	{
		QString strTableName=lstFiles.at(i).completeBaseName();

		if (strTableName.toUpper().left(5)=="F_CM_") //MB said to skip F_CM tables
			continue;

		if (strTableName.toUpper()=="F_PR_CACHE") 
			continue;

		if (nErrorsDetected>0 || byteErrLog.size()>0)
		{
			bErrorOccured=true;
			if (byteErrLog.size()==0)
				byteErrLog.append("Errors detected: "+QString::number(nErrorsDetected).toLatin1());
			m_StatusLogger.WriteErrLog(QCoreApplication::applicationDirPath()+"/err_"+strTableName+".log",byteErrLog);
		}

		m_StatusLogger.WriteStep(QString("Step 4/10: Importing %1 data: parsing CTL file").arg(strTableName),i);
		qDebug()<<"Importing data for table: "<<strTableName;

		//get sizes & col names
		QString strSequence;
		QList<int> lstColTypes;
		QStringList lstColNames;
		QList<int> lstColSizes;
		if(!O2F_ParseCTLFile(lstFiles.at(i).absoluteFilePath(),lstColSizes,lstColNames,lstColTypes,strSequence))
		{
			nErrorsDetected++;
			byteErrLog.append("Failed to open/parse file: "+lstFiles.at(i).absoluteFilePath()+"\n\r");
			continue;
		}

		m_StatusLogger.WriteStep(QString("Step 4/10: Importing %1").arg(strTableName),i);
		//select cast ('1960.12.13 12:30' as timestamp) from rdb$database;
		//get data: column by column by width: get into string, put ' ' if date...use "cast ('1960.12.13' as date)" or "cast ('1960.12.13 12:30' as timestamp)"
		QString strDataFile=strDataMigrationDir+"/AsciiFiles/"+strTableName+".dat";
		if(!O2F_InsertTableData(strTableName,strDataFile,lstColSizes,lstColNames,lstColTypes,byteErrLog,nErrorsDetected, nProcessedInsertStatements))
		{
			nErrorsDetected++;
			byteErrLog.append("Failed to open/parse file: "+strDataFile+"\n\r");
			continue;
		}

		//----------------------------------------------
		//	CREATE SEQUENCE and UPDATE to the MAX+1 value
		//----------------------------------------------
		m_StatusLogger.WriteStep(QString("Step 4/10: Importing %1 data: recreate sequences").arg(strTableName),i);
		if (!strSequence.isEmpty())
		{
			m_DbObjectManager->CreateAutoIncrementID(pStatus,m_pDb,strTableName,strSequence);
			if (!pStatus.IsOK())
			{
				nErrorsDetected++;
				byteErrLog.append("Failed to create sequence: "+strSequence+"\n\r");
				continue;
			}
			m_DbObjectManager->AdjustAutoIncrementAfterDbCopy(pStatus,m_pDb,strTableName,strSequence);
			if (!pStatus.IsOK())
			{
				nErrorsDetected++;
				byteErrLog.append("Failed to create sequence: "+strSequence+"\n\r");
				continue;
			}
		}

		//----------------------------------------------
		//	IMPORT DATA from BIN dir then connect FK by PK id, but warning PK id is not SEQUENCE.............!!????
		//----------------------------------------------
		QStringList lstBinFilter;
		lstBinFilter.append("*.bin");
		lstBinFilter.append("*.txt");
		QDir dirBin(strDataMigrationDir+"/BinFiles/"+strTableName);


		//temporary to create triggers for keeping clean binary fields:
		m_StatusLogger.WriteStep(QString("Step 4/10: Importing %1 data: import binary data").arg(strTableName),i);
		QFileInfoList lstBinFiles=dirBin.entryInfoList(lstBinFilter,QDir::Files);
		int nSizeBin=lstBinFiles.size();
		for (int j=0;j<nSizeBin;j++)
		{
			if(!O2F_InsertBinaryTableData(strTableName, lstBinFiles.at(j).absoluteFilePath(), byteErrLog, nErrorsDetected, nProcessedInsertStatements))
			{
				nErrorsDetected++;
				byteErrLog.append("Failed to open/parse file: "+lstBinFiles.at(j).absoluteFilePath()+"\n\r");
				continue;
			}
		}

	}

	qDebug()<<"All data imported, adjusting constraints and finishing import";

	//----------------------------------------------
	//	CREATE ALL OTHER LINKS: FK-PK, TRIGGERS, ETC...
	//----------------------------------------------
	m_StatusLogger.WriteStep(QString("Step 5/10: Adjust foreign key constraints"));
	O2F_AdjustFKConstraints(pStatus);
	if(!pStatus.IsOK())
	{
		m_pManager->ReleaseExclusiveDbConnection(&m_pDb);
		return;
	}

	m_StatusLogger.WriteStep(QString("Step 6/10: Reset SC Wizards"));
	O2F_ResetCommunicatorWizards(pStatus);
	if(!pStatus.IsOK())
	{
		m_pManager->ReleaseExclusiveDbConnection(&m_pDb);
		return;
	}

	m_StatusLogger.WriteStep(QString("Step 7/10: Update Hierarhical Data"));
	O2F_UpdateHierarhicalData(pStatus);
	if(!pStatus.IsOK())
	{
		m_pManager->ReleaseExclusiveDbConnection(&m_pDb);
		return;
	}

	m_StatusLogger.WriteStep(QString("Step 8/10: Importing Stored Project List data"));

	//import CSV data, files must be in same dir as Admintool
	if (!QFile::exists(strDataMigrationDir+"/AsciiFiles/SPL_HEADERS.dat") || !QFile::exists(strDataMigrationDir+"/AsciiFiles/SPL_DETAILS.dat"))
	{
		pStatus.setError(1,"AsciiFiles/SPL_HEADERS.dat and AsciiFiles/SPL_DETAILS.dat are missing.");
		m_pManager->ReleaseExclusiveDbConnection(&m_pDb);
		return;
	}

	O2F_ImportStoredProjectListsCSV(strDataMigrationDir+"/AsciiFiles/SPL_HEADERS.dat", strDataMigrationDir+"/AsciiFiles/SPL_DETAILS.dat", pStatus);
	if(!pStatus.IsOK())
	{
		m_pManager->ReleaseExclusiveDbConnection(&m_pDb);
		return;
	}


	m_StatusLogger.WriteStep(QString("Step 9/10: Create all table constraints (indexes, foreign keys, triggers, views)"));
	//create constraints: FK's:
	for(int i=0; i < lstTableCreatorSize; ++i)
	{
		qDebug()<<"Creating constraints for table:"<<m_lstTableCreatorSPC.at(i)->m_TableData.m_strTableName;
		m_lstTableCreatorSPC.at(i)->CreateTableConstraints(pStatus);
		if(!pStatus.IsOK())
		{
			m_pManager->ReleaseExclusiveDbConnection(&m_pDb);
			return;
		}
	}

	m_StatusLogger.WriteStep(QString("Step 10/10: Creating users for database access"));
	//create admin user for FB Database:
	//SOKRATES/xan09 full admin
	//ODBCREADER/sokreport24 - read only za reporte...
	CreateFBUsers(pStatus,"SYSDBA","masterkey",m_pDb);
	if(!pStatus.IsOK())
		return;


	//write error log:
	if (bErrorOccured)
	{
		pStatus.setError(1,QString("Some errors occurred during import. Check error logs."));
	}

	//release connection
	if (bConnReserved)
	{
		m_pManager->ReleaseExclusiveDbConnection(&m_pDb);
	}
	
}


//parses CTL file and returns column sizes and names by which ascii file will be decoded
//if size is 0->then insert NULL
//false if size can not be parsed or file can not be open
bool DbOrganizer::O2F_ParseCTLFile(QString strCTLFile,QList<int> &lstColSizes,QStringList &lstColNames,QList<int> &lstColTypes, QString &strSequenceColumnName)
{
	QFile file_ctl(strCTLFile);
	if(!file_ctl.open(QIODevice::ReadOnly))
		return false;

	QByteArray bufferCTL = file_ctl.readAll();
	int nPos0=bufferCTL.indexOf("(");
	if (nPos0<0)
		return false;

	bufferCTL=bufferCTL.mid(nPos0+1);
	QList<QByteArray> lstCols=bufferCTL.split(',');

	strSequenceColumnName="";
	int nSize2=lstCols.size();
	for (int j=0;j<nSize2;j++)
	{
		//get size:
		QByteArray colData=lstCols.at(j);

		int nPos1= colData.indexOf("(");
		int nPos2= colData.indexOf(")",nPos1);
		int nSize=QString(colData.mid(nPos1+1,nPos2-nPos1-1)).toInt();

		bool bTermCol=false;
		if (colData.indexOf("TERMINATED BY '|'")>0)
			bTermCol=true;

		//skip insert (indication to insert NULL value):
		if (colData.indexOf("FILLER POSITION(*)")>0)
			nSize=-1;
		
		//lstColumnName:
		QString strCol=QString(colData).section(" ",0,0).trimmed();
		if (strCol.isEmpty())
			return false;

		if (strCol.indexOf("_SEQUENCE")>0 && strSequenceColumnName.isEmpty()) //get first one
			strSequenceColumnName=strCol;

		//type: only detect DATE-> for parsing YYYYMMDD if timestamp -> YYYYMMDDHHMM
		QString strType=QString(colData).section(" ",1,1);
		if (strType=="DATE")
			lstColTypes.append(QVariant::Date);
		else if (strType.contains("CHAR"))
			lstColTypes.append(QVariant::String);
		else
		{
			if (bTermCol)
				nSize=-2; //indication that you search for "|"
			lstColTypes.append(QVariant::Invalid);
		}

		//nSize=0->date, -1 -> null, -2 ->all others terminated by "|"
		lstColNames.append(strCol);
		lstColSizes.append(nSize);
	}

	return true;
}

/*
//insert into table from dat file which is formatted by params:
bool DbOrganizer::O2F_InsertTableData(QString strTableName, QString strDATFile,QList<int> &lstColSizes,QStringList &lstColNames,QList<int> &lstColTypes, QByteArray &buffer_err,int &nErrorsDetected, int &nSQLProcessed)
{
	Status err;
	DbSqlQuery query(err,m_pManager);
	if(!err.IsOK())
		return false;

	QFile file(strDATFile);
	if(!file.open(QIODevice::ReadOnly))
		return false;
	QByteArray buffer = file.readAll();

	//form insert column statement:
	int nColCnt=lstColSizes.size();
	QString strSQLInsert="INSERT INTO "+strTableName+" (";
	QString strExc;
	for (int i=0;i<nColCnt;i++)
	{
		strSQLInsert.append(lstColNames.at(i));
		strSQLInsert.append(",");
		strExc.append("?,");
	}
	strSQLInsert.chop(1);
	strExc.chop(1);
	strSQLInsert.append(") VALUES("+strExc+")");

	//prepare insert
	query.Prepare(err,strSQLInsert,true);
	if (!err.IsOK())
	{
		buffer_err.append(QString("Error \"%1\" while executing statement: %2\n\r").arg(err.getErrorText()).arg(strSQLInsert).toLatin1());
		nErrorsDetected++;
		return false;
	}



	int nPos=0;
	int nCurrCol=0;
	QString strSQL;
	bool bSQLPrepared=false;
	int nBindPos=0;

	QStringList lstStringRowValues;
	
	while (nPos<buffer.size())
	{
		int nColSize=lstColSizes.at(nCurrCol);
		//QByteArray val;
		if (nColSize==-1) //NULL
		{
			query.bindValue(nBindPos,QVariant());
			//val.append("null,");
			nPos++;
			nBindPos++;
		}
		else if (nColSize==-2)//terminated by "|" but not date
		{
			QString val;
			int nEndPos=buffer.indexOf("|",nPos);
			if (nEndPos<0)
				return false;
			int nLen=nEndPos-nPos;
			val=buffer.mid(nPos,nLen).trimmed();
			if (val=="") //fix empty string to one space-> for Omnis
				val=" ";

			query.bindValue(nBindPos,val);
			nBindPos++;

			nPos=nEndPos+1;
		}
		else 
		{
			if (lstColTypes.at(nCurrCol)==QVariant::Date) //date
			{
				QByteArray val;
				int nEndPos=buffer.indexOf("|",nPos);
				if (nEndPos<0)
					return false;
				int nLen=nEndPos-nPos;
				if (nLen==0)
				{
					//insert null date...
					//val="null," ;
					query.bindValue(nBindPos,QVariant());
					nBindPos++;
				}
				else
				{
					QByteArray val=buffer.mid(nPos,nLen);
					QString strDate;
					QByteArray year=val.left(4);
					if (year=="0001")
						year="2001";
					else if (year=="0000")
						year="2000";

					QDateTime date;

					//date is in format: YYYYMMDDHHMM for timestamp and YYYYMMDD + "|" as delimiter char
					//"cast ('1960.12.13' as date)" or "cast ('1960.12.13 12:30' as timestamp)"
					if (nLen==8)
					{
						strDate=year+"."+val.mid(4,2)+"."+val.mid(6,2);
						date= QDateTime::fromString(strDate,"yyyy.MM.dd");
					}
					else if (nLen==10) //only time part is valid and format is different then it should be: complete mess
					{
						strDate="2000.01.01 "+val.mid(6,2)+":"+val.mid(8,2);
						date= QDateTime::fromString(strDate,"yyyy.MM.dd hh:mm");
					}
					else if (nLen==12)
					{
						strDate=year+"."+val.mid(4,2)+"."+val.mid(6,2)+" "+val.mid(8,2)+":"+val.mid(10,2);
						date = QDateTime::fromString(strDate,"yyyy.MM.dd hh:mm");
					}
					else
					{
						strDate=year+".01.01 00:00";
						qDebug()<<"DateTime field is messed up: " << val;
						date= QDateTime::fromString(strDate,"yyyy.MM.dd hh:mm");
						
					}

					qDebug()<<date.toString();
					query.bindValue(nBindPos,date);
					nBindPos++;
				}

				//if date then pos is +1 (terminated char |)
				nPos=nEndPos+1;
			}
			else if (lstColTypes.at(nCurrCol)==QVariant::String) //umlaut problems:
			{
				
				QString strValue=QString::fromLatin1(buffer.mid(nPos,nColSize).trimmed());
				if (strValue.isEmpty()) //fix empty string to one space-> for Omnis
					strValue=" ";
				query.bindValue(nBindPos,strValue);
				nBindPos++;
				nPos+=nColSize;
			}
			else //all others
			{
				QString val;
				val=buffer.mid(nPos,nColSize).trimmed();
				if (val=="") //fix empty string to one space-> for Omnis
					val=" ";
				query.bindValue(nBindPos,val);
				nPos+=nColSize;
			}
		}



		//inc and reset column position if reached end
		nCurrCol++;
		if (nCurrCol>=nColCnt)
		{
			//execute statement: so god help us!
			query.ExecutePrepared(err,-1,true);
			if (!err.IsOK())
			{
				buffer_err.append(QString("Error \"%1\" while executing statement: %2\n\r").arg(err.getErrorText()).arg(strSQL).toLatin1());
				nErrorsDetected++;
			}
	
			nSQLProcessed++;
			strSQL.clear();
			nCurrCol=0;
			nBindPos=0;
			nPos+=2; //move position by two chars
			lstStringRowValues.clear();
		}
	}

	return true;
}
*/







/*
//insert into table from dat file which is formatted by params:
bool DbOrganizer::O2F_InsertTableData(QString strTableName, QString strDATFile,QList<int> &lstColSizes,QStringList &lstColNames,QList<int> &lstColTypes, QByteArray &buffer_err,int &nErrorsDetected, int &nSQLProcessed)
{
	Status err;
	DbSqlQuery query(err,m_pManager);
	if(!err.IsOK())
		return false;

	QFile file(strDATFile);
	if(!file.open(QIODevice::ReadOnly))
		return false;
	QByteArray buffer = file.readAll();

	//form insert column statement:
	int nColCnt=lstColSizes.size();
	QString strSQLInsert="INSERT INTO "+strTableName+" (";
	for (int i=0;i<nColCnt;i++)
	{
		strSQLInsert.append(lstColNames.at(i));
		strSQLInsert.append(",");
	}
	strSQLInsert.chop(1);
	strSQLInsert.append(") VALUES(");


	int nPos=0;
	int nCurrCol=0;
	QString strSQL;
	bool bSQLPrepared=false;

	QStringList lstStringRowValues;

	while (nPos<buffer.size())
	{

		int nColSize=lstColSizes.at(nCurrCol);
		QByteArray val;
		if (nColSize==-1) //NULL
		{
			val.append("null,");
			nPos++;
		}
		else if (nColSize==-2)//terminated by "|" but not date
		{
			int nEndPos=buffer.indexOf("|",nPos);
			if (nEndPos<0)
				return false;
			int nLen=nEndPos-nPos;
			val="'"+buffer.mid(nPos,nLen).trimmed()+"',";
			if (val=="'',") //fix empty string to one space-> for Omnis
				val="' ',";
			nPos=nEndPos+1;
		}
		else 
		{
			if (lstColTypes.at(nCurrCol)==QVariant::Date) //date
			{
				int nEndPos=buffer.indexOf("|",nPos);
				if (nEndPos<0)
					return false;
				int nLen=nEndPos-nPos;
				if (nLen==0)
				{
					//insert null date...
					val="null," ;
				}
				else
				{
					val=buffer.mid(nPos,nLen);
					QByteArray year=val.left(4);
					if (year=="0001")
						year="2001";
					else if (year=="0000")
						year="2000";

					//date is in format: YYYYMMDDHHMM for timestamp and YYYYMMDD + "|" as delimiter char
					//"cast ('1960.12.13' as date)" or "cast ('1960.12.13 12:30' as timestamp)"
					if (nLen==8)
						val="cast ('"+year+"."+val.mid(4,2)+"."+val.mid(6,2)+"' as date)," ;
					else if (nLen==10) //only time part is valid and format is different then it should be: complete mess
						val="cast ('2000.01.01 "+val.mid(6,2)+":"+val.mid(8,2)+"' as timestamp)," ;
					else if (nLen==12)
						val="cast ('"+year+"."+val.mid(4,2)+"."+val.mid(6,2)+" "+val.mid(8,2)+":"+val.mid(10,2)+"' as timestamp)," ;
					else
					{
						val="cast ('"+year+".01.01 00:00' as timestamp)," ;
						qDebug()<<"DateTime field is messed up: " << val;
						//return false;
					}

				}

				//if date then pos is +1 (terminated char |)
				nPos=nEndPos+1;
			}
			else if (lstColTypes.at(nCurrCol)==QVariant::String) //umlaut problems:
			{
				QString strValue; //no trimming on computed indexes
				if (lstColNames.at(nCurrCol)=="ETR_CODE")
					strValue=QString::fromLatin1(buffer.mid(nPos,nColSize));
				else
					strValue=QString::fromLatin1(buffer.mid(nPos,nColSize).trimmed());

				//BT 17.08 reverted back: now expell all empty spaces from base, only leave as is, empty string..
				//if (strValue.isEmpty()) //fix empty string to one space-> for Omnis
				//	strValue=" ";

				lstStringRowValues.append(strValue);
				strSQL.append("?,");
				nPos+=nColSize;
			}
			else //all others
			{
				val="'"+buffer.mid(nPos,nColSize).trimmed()+"',";
				if (val=="'',") //fix empty string to one space-> for Omnis
					val="' ',";
				nPos+=nColSize;
			}
		}

		if (lstColTypes.at(nCurrCol)!=QVariant::String)
			strSQL.append(val);


		//inc and reset column position if reached end
		nCurrCol++;
		if (nCurrCol>=nColCnt)
		{
			//execute statement: so god help us!
			strSQL.chop(1);
			strSQL=strSQLInsert+strSQL+")";

			if (lstStringRowValues.size()>0)
			{
				query.Prepare(err,strSQL,true);
				if (!err.IsOK())
				{
					buffer_err.append(QString("Error \"%1\" while executing statement: %2\n\r").arg(err.getErrorText()).arg(strSQL).toLatin1());
					nErrorsDetected++;
				}

				int nStrSize=lstStringRowValues.size();
				for (int z=0;z<nStrSize;z++)
				{
					query.bindValue(z,lstStringRowValues.at(z));
				}
				query.ExecutePrepared(err,-1,true);
				if (!err.IsOK())
				{
					buffer_err.append(QString("Error \"%1\" while executing statement: %2\n\r").arg(err.getErrorText()).arg(strSQL).toLatin1());
					nErrorsDetected++;
				}
			}
			else
			{

				query.Execute(err,strSQL,true);
				if (!err.IsOK())
				{
					buffer_err.append(QString("Error \"%1\" while executing statement: %2\n\r").arg(err.getErrorText()).arg(strSQL).toLatin1());
					nErrorsDetected++;
				}
			}

			nSQLProcessed++;
			strSQL.clear();
			nCurrCol=0;
			nPos+=2; //move position by two chars
			lstStringRowValues.clear();
		}
	}

	return true;
}
*/

#define CHUNK_DATA_READ 1024*1024*5 //5Mb
#define CHUNK_POS_BEFORE_NEXT_READ 1024*200 //assume that 1 line is less then 200Kb data worst case

//insert into table from dat file which is formatted by params:
bool DbOrganizer::O2F_InsertTableData(QString strTableName, QString strDATFile,QList<int> &lstColSizes,QStringList &lstColNames,QList<int> &lstColTypes, QByteArray &buffer_err,int &nErrorsDetected, int &nSQLProcessed)
{
	Status err;
	DbSqlQuery query(err,m_pManager);
	if(!err.IsOK())
		return false;

	QFile file(strDATFile);
	if(!file.open(QIODevice::ReadOnly))
		return false;

	int nTotalReadBufferSize=0;
	int nTotalBufferSize=file.size();
	

	//form insert column statement:
	int nColCnt=lstColSizes.size();
	QString strSQLInsert="INSERT INTO "+strTableName+" (";
	for (int i=0;i<nColCnt;i++)
	{
		strSQLInsert.append(lstColNames.at(i));
		strSQLInsert.append(",");
	}
	strSQLInsert.chop(1);
	strSQLInsert.append(") VALUES(");

	int nRecordsProcessed=0;
	int nPos=0;
	int nCurrCol=0;
	QString strSQL;
	bool bSQLPrepared=false;

	QStringList lstStringRowValues;

	QByteArray buffer;
	while(file.bytesAvailable()>0)
	{
		QByteArray buffer_read = file.read(CHUNK_DATA_READ);
		nTotalReadBufferSize+=buffer_read.size();
		buffer += buffer_read;
		
		int nStopPos=0;
		if (nTotalReadBufferSize>=nTotalBufferSize)
			nStopPos=buffer.size();
		else
			nStopPos=buffer.size()-CHUNK_POS_BEFORE_NEXT_READ;
		
		while (nPos<nStopPos)
		{

			int nColSize=lstColSizes.at(nCurrCol);
			QByteArray val;
			if (nColSize==-1) //NULL
			{
				val.append("null,");
				nPos++;
			}
			else if (nColSize==-2)//terminated by "|" but not date
			{
				int nEndPos=buffer.indexOf("|",nPos);
				if (nEndPos<0)
					return false;
				int nLen=nEndPos-nPos;
				val="'"+buffer.mid(nPos,nLen).trimmed()+"',";
				if (val=="'',") //fix empty string to one space-> for Omnis
					val="' ',";
				nPos=nEndPos+1;
			}
			else 
			{
				if (lstColTypes.at(nCurrCol)==QVariant::Date) //date
				{
					int nEndPos=buffer.indexOf("|",nPos);
					if (nEndPos<0)
						return false;
					int nLen=nEndPos-nPos;
					if (nLen==0)
					{
						//insert null date...
						val="null," ;
					}
					else
					{
						val=buffer.mid(nPos,nLen);
						QByteArray year=val.left(4);
						if (year=="0001")
							year="2001";
						else if (year=="0000")
							year="2000";

						//date is in format: YYYYMMDDHHMM for timestamp and YYYYMMDD + "|" as delimiter char
						//"cast ('1960.12.13' as date)" or "cast ('1960.12.13 12:30' as timestamp)"
						if (nLen==8)
							val="cast ('"+year+"."+val.mid(4,2)+"."+val.mid(6,2)+"' as date)," ;
						else if (nLen==10) //only time part is valid and format is different then it should be: complete mess
							val="cast ('2000.01.01 "+val.mid(6,2)+":"+val.mid(8,2)+"' as timestamp)," ;
						else if (nLen==12)
							val="cast ('"+year+"."+val.mid(4,2)+"."+val.mid(6,2)+" "+val.mid(8,2)+":"+val.mid(10,2)+"' as timestamp)," ;
						else
						{
							val="cast ('"+year+".01.01 00:00' as timestamp)," ;
							qDebug()<<"DateTime field is messed up: " << val;
							//return false;
						}

					}

					//if date then pos is +1 (terminated char |)
					nPos=nEndPos+1;
				}
				else if (lstColTypes.at(nCurrCol)==QVariant::String) //umlaut problems:
				{
					QString strValue; //no trimming on computed indexes
					if (lstColNames.at(nCurrCol)=="ETR_CODE")
						strValue=QString::fromLatin1(buffer.mid(nPos,nColSize));
					else
						strValue=QString::fromLatin1(buffer.mid(nPos,nColSize).trimmed());

					//BT 17.08 reverted back: now expell all empty spaces from base, only leave as is, empty string..
					//if (strValue.isEmpty()) //fix empty string to one space-> for Omnis
					//	strValue=" ";

					lstStringRowValues.append(strValue);
					strSQL.append("?,");
					nPos+=nColSize;
				}
				else //all others
				{
					val="'"+buffer.mid(nPos,nColSize).trimmed()+"',";
					if (val=="'',") //fix empty string to one space-> for Omnis
						val="' ',";
					nPos+=nColSize;
				}
			}

			if (lstColTypes.at(nCurrCol)!=QVariant::String)
				strSQL.append(val);


			//inc and reset column position if reached end
			nCurrCol++;
			if (nCurrCol>=nColCnt)
			{
				//execute statement: so god help us!
				strSQL.chop(1);
				strSQL=strSQLInsert+strSQL+")";

				if (lstStringRowValues.size()>0)
				{
					query.Prepare(err,strSQL,true);
					if (!err.IsOK())
					{
						buffer_err.append(QString("Error \"%1\" while executing statement: %2\n\r").arg(err.getErrorText()).arg(strSQL).toLatin1());
						nErrorsDetected++;
					}

					int nStrSize=lstStringRowValues.size();
					for (int z=0;z<nStrSize;z++)
					{
						query.bindValue(z,lstStringRowValues.at(z));
					}
					query.ExecutePrepared(err,-1,true);
					if (!err.IsOK())
					{
						buffer_err.append(QString("Error \"%1\" while executing statement: %2\n\r").arg(err.getErrorText()).arg(strSQL).toLatin1());
						nErrorsDetected++;
					}
				}
				else
				{

					query.Execute(err,strSQL,true);
					if (!err.IsOK())
					{
						buffer_err.append(QString("Error \"%1\" while executing statement: %2\n\r").arg(err.getErrorText()).arg(strSQL).toLatin1());
						nErrorsDetected++;
					}
				}

				nRecordsProcessed++;
				nSQLProcessed++;
				strSQL.clear();
				nCurrCol=0;
				nPos+=2; //move position by two chars
				lstStringRowValues.clear();
				m_StatusLogger.WriteImportProgress(strTableName,nRecordsProcessed,true);
			}
		}


		buffer=buffer.mid(nPos);
		nPos=0;
		
	}

	m_StatusLogger.WriteImportProgress(strTableName,nRecordsProcessed,true,true);
	return true;
}


bool DbOrganizer::O2F_CountRecordsFromFile(QString strFile, int &nCnt)
{
	QFile file(strFile);
	if(!file.open(QIODevice::ReadOnly))
		return false;

	QByteArray buffer;
	while(file.bytesAvailable()>0)
	{
		buffer = file.read(CHUNK_DATA_READ);

		nCnt+=buffer.count("\n");
	}

	return true;
}



bool DbOrganizer::O2F_InsertBinaryTableData(QString strTableName, QString strBinFile, QByteArray &buffer_err,int &nErrorsDetected, int &nSQLProcessed)
{
	Status err;
	DbSqlQuery query(err,m_pManager);
	if(!err.IsOK())
		return false;

	QFile file(strBinFile);
	if(!file.open(QIODevice::ReadOnly))
		return false;
	QByteArray buffer = file.readAll();

	QFileInfo file_info(strBinFile);
	QString strFieldName = file_info.completeBaseName();
	DbTableKeyData TableData;
	DbSqlTableDefinition::GetKeyData(DbSqlTableDefinition::GetTableID(strTableName), TableData);
	QString strTableSequenceName=TableData.m_strPrimaryKey;
	QString strSuffix = file_info.completeSuffix().toLower(); //txt or bin
	QString strSQLInsert;
	QString strSQLUpdate;
	QString strSQLWhere;
	QString strBinSequenceName;
	bool	bUpdateTextDataDirectly=false;
	bool	bUpdateBinDataDirectly=false;
	QString strSQLTextUpdate;
	

	if (strSuffix=="bin")
	{
		strSQLInsert="Insert into ZS_BINARY_FIELDS (RID_BIN,TEXT) values ";
		strSQLUpdate="UPDATE ZS_BINARY_FIELDS SET TEXT=TEXT ||";
		strBinSequenceName ="RID_BIN";

		QString strSQL="SELECT FIRST 1 "+strFieldName+" FROM "+strTableName;
		query.Execute(err,strSQL,true);
		if(!err.IsOK())
		{
			buffer_err.append(QString("Error \"%1\" while executing statement: %2\n\r").arg(err.getErrorText()).arg(strSQL).toLatin1());
			nErrorsDetected++;
			return false;
		}

		if(query.GetQSqlQuery()->record().field(0).type()==QVariant::ByteArray)
		{
			bUpdateBinDataDirectly=true;
		}
		else
		{
			//Q_ASSERT_X(false,"O2F_InsertBinaryTableData"," BIN blob is not in game any more");
			//qDebug()<<"Table with BIN blob " <<strTableName << " fld: "<<strFieldName;
		}


	}
	else
	{
		strSQLInsert="Insert into ZS_TEXT_FIELDS (RID_TXT,TEXT) values ";
		strSQLUpdate="UPDATE ZS_TEXT_FIELDS SET TEXT=TEXT ||";
		strBinSequenceName ="RID_TXT";

		//MB: requested: 


		//Note:
		//Go to DB and test if field is VARCHAR, if so then insert TEXT into field directly
		//Else: insert into binary table and write down ID....
		QString strSQL="SELECT FIRST 1 "+strFieldName+" FROM "+strTableName;
		query.Execute(err,strSQL,true);
		if(!err.IsOK())
		{
			buffer_err.append(QString("Error \"%1\" while executing statement: %2\n\r").arg(err.getErrorText()).arg(strSQL).toLatin1());
			nErrorsDetected++;
			return false;
		}

		if(query.GetQSqlQuery()->record().field(0).type()==QVariant::String)
		{
			bUpdateTextDataDirectly=true;
		}
		else
		{
			Q_ASSERT_X(false,"O2F_InsertBinaryTableData"," TEXT blob is not in game any more");
			qDebug()<<"Table Name with TEXT blob " <<strTableName << " fld: "<<strFieldName;
		}

	}

	/*
	a) Imena tablica da budu za BIN: BINARY_FIELDS, a za txt TEXT_FIELDS
	b) primary key umjesto RID da budu za BIN: RID_BIN, a za TEXT:   RID_TXT
	c) i da sequence budu posebne za obje tablice: SEQ_RID_BIN i SEQ_RID_TXT 
	*/

	//--------------------------------
	// insert BIN/TEXT fields
	//--------------------------------
	int nPos=0;
	int nRecordsProcessed=0;
	bool bSQLPrepared=false;
	
	while (nPos<buffer.size())
	{
		//first 9 chars are seq_id
		int nSeqID=buffer.mid(nPos,9).trimmed().toInt();
		if (nSeqID==0)
			return false;
		nPos+=9;

		//first 9 chars are seq_id
		int nDataLength=buffer.mid(nPos,9).trimmed().toInt();
		if (nDataLength==0)
			return false;
		nPos+=9;

		//now data goes: get it chunk by chunk and insert/update at same time...
		bool bFirstChunk=true;
		QString strRIDstrSQL;
		
		//convert base64
		//B.T.: bindata will be inserted into DB
		QByteArray bindata=QByteArray::fromBase64(buffer.mid(nPos,nDataLength));
		nPos+=nDataLength; //align to next data row value
		//continue;

		//for TEXT only (less then 4Kb, so do not split to smaller parts):
		if (bUpdateTextDataDirectly)
		{
			if (!bSQLPrepared)
			{
				strSQLTextUpdate="UPDATE "+strTableName+" SET "+strFieldName+" = ? WHERE "+strTableSequenceName + " = ?";
				query.Prepare(err,strSQLTextUpdate,true);
				if (!err.IsOK())
				{
					buffer_err.append(QString("Error \"%1\" while executing statement: %2\n\r").arg(err.getErrorText()).arg(strSQLTextUpdate).toLatin1());
					nErrorsDetected++;
					continue;
				}
				else
					bSQLPrepared=true;
			}
			//try to solve UTF8 problem:
			QString strUTF8=QString::fromLatin1(bindata);
			query.bindValue(0,strUTF8);
			query.bindValue(1,nSeqID);
			query.ExecutePrepared(err,-1,true);
			if (!err.IsOK())
			{
				buffer_err.append(QString("Error \"%1\" while executing statement: %2\n\r").arg(err.getErrorText()).arg(strSQLTextUpdate).toLatin1());
				nErrorsDetected++;
			}
			else
			{
				nSQLProcessed++;
				nRecordsProcessed++;
				m_StatusLogger.WriteImportProgress(strTableName,nRecordsProcessed,false);
			}

			
			continue;
		}

		//for BIN from EUS table from TS:
		if (bUpdateBinDataDirectly)
		{
			if (!bSQLPrepared)
			{
				strSQLTextUpdate="UPDATE "+strTableName+" SET "+strFieldName+" = ? WHERE "+strTableSequenceName + " = ?";
				query.Prepare(err,strSQLTextUpdate,true);
				if (!err.IsOK())
				{
					buffer_err.append(QString("Error \"%1\" while executing statement: %2\n\r").arg(err.getErrorText()).arg(strSQLTextUpdate).toLatin1());
					nErrorsDetected++;
					continue;
				}
				else
					bSQLPrepared=true;
			}


			query.bindValue(0,bindata);
			query.bindValue(0,nSeqID);
			query.ExecutePrepared(err,-1,true);
			if (!err.IsOK())
			{
				buffer_err.append(QString("Error \"%1\" while executing statement: %2\n\r").arg(err.getErrorText()).arg(strSQLTextUpdate).toLatin1());
				nErrorsDetected++;
			}
			else
			{
				nSQLProcessed++;
				nRecordsProcessed++;
				m_StatusLogger.WriteImportProgress(strTableName,nRecordsProcessed,false);
			}

			continue;
		}



		int nBinPos=0;
		bindata=bindata.toHex();
		int nBinPosEnd=bindata.size();

		while(nBinPos<nBinPosEnd)
		{
			QString strSQLBin;

			int nBinLength=0;
			if (nBinPos+4096>nBinPosEnd)
				nBinLength=nBinPosEnd-nBinPos;
			else
				nBinLength=4096;

			if (bFirstChunk)
			{
				Status err;
				strRIDstrSQL=QString::number(m_pManager->getNextInsertId(err,m_pDb,strBinSequenceName));
				if (!err.IsOK())
				{
					strRIDstrSQL="";
					nErrorsDetected++;
					buffer_err.append("Failed to inc sequence: "+strBinSequenceName+"\n\r");
					break;
				}
				strSQLBin=strSQLInsert+"('"+strRIDstrSQL+"',x'"+bindata.mid(nBinPos,nBinLength)+"')";
				bFirstChunk=false;
			}
			else
			{
				strSQLBin=strSQLUpdate+" x'"+bindata.mid(nBinPos,nBinLength)+"'";
				strSQLBin+=" WHERE "+strBinSequenceName+"="+strRIDstrSQL;
			}

			Status err;
			query.Execute(err,strSQLBin,true);
			if (!err.IsOK())
			{
				buffer_err.append(QString("Error \"%1\" while executing statement: %2\n\r").arg(err.getErrorText()).arg(strSQLBin).toLatin1());
				nErrorsDetected++;
			}
			else
			{
				nSQLProcessed++;
				nRecordsProcessed++;
				m_StatusLogger.WriteImportProgress(strTableName,nRecordsProcessed,false);
			}

			nBinPos+=nBinLength;
		}


		//-------------------------------------------
		// connect id's -> RID to the table record
		//-------------------------------------------
		if (!strRIDstrSQL.isEmpty())
		{
			QString strSQLConnect="UPDATE "+strTableName+" SET "+strFieldName+ "="+strRIDstrSQL +" WHERE "+strTableSequenceName+ "="+QString::number(nSeqID);
			Status err;
			query.Execute(err,strSQLConnect,true);
			if (!err.IsOK())
			{
				buffer_err.append(QString("Error \"%1\" while executing statement: %2\n\r").arg(err.getErrorText()).arg(strSQLConnect).toLatin1());
				nErrorsDetected++;
			}
			else
				nSQLProcessed++;
		}
	}

	m_StatusLogger.WriteImportProgress(strTableName,nRecordsProcessed,false, true);
	return true;
}


//from SC version 113 to 114 all sequences names are changed from NAME_SEQ to SEQ_NAME to be compatible with Omnis SPC tables
void DbOrganizer::DropAllSequencesFromOldSCDatabase(Status &pStatus)
{

	if (m_nCurrentDatabaseVersion>114)
		return;

	qDebug()<<"Deleting all sequences with SEQ_primarykeyname prefix. Will be replaces with primarykeyname_SEQ suffix";

	DbSqlQuery query(pStatus,m_pManager);

	//delete constrains:
	int lstTableCreatorSize = m_lstTableCreator.size();
	for(int i=0; i < lstTableCreatorSize; ++i)
	{
		QString strSeqName=m_lstTableCreator.at(i)->m_TableData.m_strPrimaryKey+"_SEQ";
		QString strSQL="DROP SEQUENCE "+strSeqName;
		query.Execute(pStatus,strSQL); //do not chek errors..just do it
	}

	pStatus.setError(0);

}

//Arturo missed to update SEQUENCE_FK fields -> connection to the other tables. I'll do it:
bool DbOrganizer::O2F_AdjustFKConstraints(Status &pStatus)
{
	DbSqlQuery query(pStatus,m_pManager);

	QString strSQL="UPDATE F_Pers_Funktion SET FPF_EFK_SEQUENCE  = (SELECT  FIRST 1 EFK_SEQUENCE from F_Funktionen WHERE EFK_CODE = FPF_FUNKTION)";
	query.Execute(pStatus,strSQL,true);
	if (!pStatus.IsOK())
		return false;

	//F_Pers_Funktion->F_Funktionen (FPF_FUNKTION -> EFK_CODE) + (FPF_EFK_SEQUENCE  ->  EFK_SEQUENCE)
	strSQL="UPDATE F_Pers_Funktion SET FPF_EFK_SEQUENCE  = (SELECT  FIRST 1 EFK_SEQUENCE from F_Funktionen WHERE EFK_CODE = FPF_FUNKTION)";
		query.Execute(pStatus,strSQL,true);
	if (!pStatus.IsOK())
		return false;

	//F_Mitarb_Budget->FPERSON (EBM_MITARB -> FP_PERS_NR) + (EBM_FP_SEQUENCE  ->  FP_SEQUENCE)
	strSQL="UPDATE F_Mitarb_Budget SET EBM_FP_SEQUENCE  = (SELECT  FIRST 1 FP_SEQUENCE from FPERSON WHERE FP_PERS_NR = EBM_MITARB)";
		query.Execute(pStatus,strSQL,true);
	if (!pStatus.IsOK())
		return false;

	//F_Mitarb_Budget->FPROJCT (EBM_PROJEKT -> PR_CODE) + (EBM_PR_SEQUENCE  ->  PR_SEQUENCE)
	strSQL="UPDATE F_Mitarb_Budget SET EBM_PR_SEQUENCE  = (SELECT  FIRST 1 PR_SEQUENCE from FPROJCT WHERE EBM_PROJEKT = PR_CODE)";
		query.Execute(pStatus,strSQL,true);
	if (!pStatus.IsOK())
		return false;

	//F_Proj_Bauherr->FPROJCT (FPB_PROJEKT -> PR_CODE) + (FPB_PR_SEQUENCE  ->  PR_SEQUENCE)
	strSQL="UPDATE F_Proj_Bauherr SET FPB_PR_SEQUENCE  = (SELECT  FIRST 1 PR_SEQUENCE from FPROJCT WHERE FPB_PROJEKT = PR_CODE)";
		query.Execute(pStatus,strSQL,true);
	if (!pStatus.IsOK())
		return false;

	//F_Proj_Bauherr->FSA_Adressen (FPB_AUFTRAGGEBER -> EAL_SUCHE) + (FPB_EAL_SEQUENCE  ->  EAL_SEQUENCE)
	strSQL="UPDATE F_Proj_Bauherr SET FPB_EAL_SEQUENCE  = (SELECT FIRST 1 EAL_SEQUENCE from FSA_Adressen WHERE FPB_AUFTRAGGEBER = EAL_SUCHE)";
		query.Execute(pStatus,strSQL,true);
	if (!pStatus.IsOK())
		return false;

	//FASSIGN->FPERSON (FB_PERS_NR -> FP_PERS_NR) + (FB_FP_SEQUENCE  ->  FP_SEQUENCE)
	strSQL="UPDATE FASSIGN SET FB_FP_SEQUENCE  = (SELECT  FIRST 1  FP_SEQUENCE from FPERSON WHERE FP_PERS_NR = FB_PERS_NR)";
		query.Execute(pStatus,strSQL,true);
	if (!pStatus.IsOK())
		return false;

	//FASSIGN->FPROJCT (FB_PROJ_NR -> PR_CODE) + (FB_PR_SEQUENCE  ->  PR_SEQUENCE)
	strSQL="UPDATE FASSIGN SET FB_PR_SEQUENCE  = (SELECT  FIRST 1  PR_SEQUENCE from FPROJCT WHERE FB_PROJ_NR = PR_CODE)";
		query.Execute(pStatus,strSQL,true);
	if (!pStatus.IsOK())
		return false;

	//F_KS_Budget->FPROJCT (EKB_PROJEKT -> PR_CODE) + (EKB_PR_SEQUENCE  ->  PR_SEQUENCE)
	strSQL="UPDATE F_KS_Budget SET EKB_PR_SEQUENCE  = (SELECT  FIRST 1  PR_SEQUENCE from FPROJCT WHERE EKB_PROJEKT = PR_CODE)";
		query.Execute(pStatus,strSQL,true);
	if (!pStatus.IsOK())
		return false;

	return true;
}

/*
	B.T.: passwords are now stored in base64 format. Login procedure of SC must detect this. For initial setup, all passwords, both in BUS_PERSON & FPERSON will be reseted to ""
*/
bool DbOrganizer::O2F_ResetPasswords(Status &pStatus)
{
	DbSqlQuery query(pStatus,m_pManager);
	QString strSQL="UPDATE FPERSON SET FP_PASSWORD=''";
	query.Execute(pStatus,strSQL,true);
	if (!pStatus.IsOK())
		return false;

	strSQL="UPDATE CORE_USER SET CUSR_PASSWORD='' WHERE CUSR_IS_SYSTEM=0 AND CUSR_IS_DEFAULT_ACC=0";
	query.Execute(pStatus,strSQL,true);
	if (!pStatus.IsOK())
		return false;
	return true;
}

//SET BDPET_PARENT and BUSP_PARENT to correct values, as trigger is not enough as records are not sorted
bool DbOrganizer::O2F_UpdateHierarhicalData(Status &pStatus)
{
	DbSqlQuery query(pStatus,m_pManager);
	QString strSQL="UPDATE BUS_DEPARTMENTS A SET BDEPT_PARENT= (SELECT FIRST 1 B.BDEPT_ID FROM BUS_DEPARTMENTS B WHERE SUBSTRING(A.BDEPT_CODE FROM 1 FOR CHAR_LENGTH(B.BDEPT_CODE))=B.BDEPT_CODE AND B.BDEPT_LEVEL < A.BDEPT_LEVEL ORDER BY B.BDEPT_CODE DESC)";
	query.Execute(pStatus,strSQL,true);
	if (!pStatus.IsOK())
		return false;

	strSQL="UPDATE BUS_PROJECTS A SET BUSP_PARENT= (SELECT FIRST 1 B.BUSP_ID FROM BUS_PROJECTS B WHERE SUBSTRING(A.BUSP_CODE FROM 1 FOR CHAR_LENGTH(B.BUSP_CODE))=B.BUSP_CODE AND B.BUSP_LEVEL < A.BUSP_LEVEL ORDER BY B.BUSP_CODE DESC)";
	query.Execute(pStatus,strSQL,true);
	if (!pStatus.IsOK())
		return false;

	/*
	//use this for all new tables: F_Proj_Bereiche,	F_Gebiete,	F_Leistungsarten,F_Prozesse,F_Projektart,F_Taetigkeiten,F_WIRTSCH_GRUPPE
	strSQL="UPDATE F_PROJ_BEREICHE A SET EPB_PARENT= (SELECT FIRST 1 B.EPB_ID FROM F_PROJ_BEREICHE B WHERE SUBSTRING(A.EPB_CODE FROM 1 FOR CHAR_LENGTH(B.EPB_CODE))=B.EPB_CODE AND B.EPB_LEVEL < A.EPB_LEVEL ORDER BY B.EPB_CODE DESC)";
	query.Execute(pStatus,strSQL,true);
	if (!pStatus.IsOK())
		return false;

	strSQL="UPDATE F_GEBIETE A SET EFG_PARENT= (SELECT FIRST 1 B.EFG_ID FROM F_GEBIETE B WHERE SUBSTRING(A.EFG_CODE FROM 1 FOR CHAR_LENGTH(B.EFG_CODE))=B.EFG_CODE AND B.EFG_LEVEL < A.EFG_LEVEL ORDER BY B.EFG_CODE DESC)";
	query.Execute(pStatus,strSQL,true);
	if (!pStatus.IsOK())
		return false;


	strSQL="UPDATE F_LEISTUNGSARTEN A SET  ELA_PARENT= (SELECT FIRST 1 B. ELA_ID FROM F_LEISTUNGSARTEN B WHERE SUBSTRING(A. ELA_CODE FROM 1 FOR CHAR_LENGTH(B. ELA_CODE))=B. ELA_CODE AND B. ELA_LEVEL < A. ELA_LEVEL ORDER BY B. ELA_CODE DESC)";
	query.Execute(pStatus,strSQL,true);
	if (!pStatus.IsOK())
		return false;

	strSQL="UPDATE F_PROZESSE A SET  EPZ_PARENT= (SELECT FIRST 1 B. EPZ_ID FROM F_PROZESSE B WHERE SUBSTRING(A. EPZ_CODE FROM 1 FOR CHAR_LENGTH(B. EPZ_CODE))=B. EPZ_CODE AND B. EPZ_LEVEL < A. EPZ_LEVEL ORDER BY B. EPZ_CODE DESC)";
	query.Execute(pStatus,strSQL,true);
	if (!pStatus.IsOK())
		return false;

	strSQL="UPDATE F_PROJEKTART A SET  EAP_PARENT= (SELECT FIRST 1 B. EAP_ID FROM F_PROJEKTART B WHERE SUBSTRING(A. EAP_CODE FROM 1 FOR CHAR_LENGTH(B. EAP_CODE))=B. EAP_CODE AND B. EAP_LEVEL < A. EAP_LEVEL ORDER BY B. EAP_CODE DESC)";
	query.Execute(pStatus,strSQL,true);
	if (!pStatus.IsOK())
		return false;

	strSQL="UPDATE F_TAETIGKEITEN A SET  ET_PARENT= (SELECT FIRST 1 B. ET_ID FROM F_TAETIGKEITEN B WHERE SUBSTRING(A. ET_CODE FROM 1 FOR CHAR_LENGTH(B. ET_CODE))=B. ET_CODE AND B. ET_LEVEL < A. ET_LEVEL ORDER BY B. ET_CODE DESC)";
	query.Execute(pStatus,strSQL,true);
	if (!pStatus.IsOK())
		return false;

	strSQL="UPDATE F_WIRTSCH_GRUPPE A SET  EWG_PARENT= (SELECT FIRST 1 B. EWG_ID FROM F_WIRTSCH_GRUPPE B WHERE SUBSTRING(A. EWG_CODE FROM 1 FOR CHAR_LENGTH(B. EWG_CODE))=B. EWG_CODE AND B. EWG_LEVEL < A. EWG_LEVEL ORDER BY B. EWG_CODE DESC)";
	query.Execute(pStatus,strSQL,true);
	if (!pStatus.IsOK())
		return false;

*/
	return true;

}


/*
	B.T.: reset wizards if SPC import
*/
bool DbOrganizer::O2F_ResetCommunicatorWizards(Status &pStatus)
{
	DbSqlQuery query(pStatus,m_pManager);
	QString strSQL="UPDATE CORE_DATABASE_INFO SET CDI_ASK_FOR_TEMPLATE_DB=0, CDI_START_SETUP_WIZARD=0";
	query.Execute(pStatus,strSQL,true);
	if (!pStatus.IsOK())
		return false;
	return true;
}

bool DbOrganizer::O2F_ImportStoredProjectListsCSV(QString strHeaderFile, QString strDetailsFile, Status &pStatus)
{
	CsvImportFile file;

	//STEP 1: load files

	DbRecordSet lstHeaderData;
	file.Load(pStatus, strHeaderFile, lstHeaderData, QStringList(), QStringList(), true);
	if(!pStatus.IsOK())	return false;

	int nColCnt = lstHeaderData.getColumnCount();
	if(nColCnt != 8){
		pStatus.setError(1, QString("Invalid column count in header file (%1 instead of 8): %2").arg(nColCnt).arg(strHeaderFile));
		return false;
	}

	DbRecordSet lstDetailsData;
	file.Load(pStatus, strDetailsFile, lstDetailsData, QStringList(), QStringList(), true);
	if(!pStatus.IsOK())	return false;

	nColCnt = lstDetailsData.getColumnCount();
	if(nColCnt != 9){
		pStatus.setError(1, QString("Invalid column count in details file (%1 instead of 9): %2").arg(nColCnt).arg(strDetailsFile));
		return false;
	}

	//STEP 2: write header data

	//SPL Main Data: AKA "header" file import is defined by MB like this:
	//PL_CODE			Code		Mostly, this is a name. Spaces etc. are allowed
	//PL_NAME						Mostly empty, unless the list was defined in Timesheet
	//PL_PERSON			Pers.No.	If a pers.no. is defined, only this user can see this SPL. It is hidden for all others.
	//PL_ABTEILUNG		Department	If a pers.no. is defined, only users from this department or subdepartments of it can see this SPL. It is hidden for all others.
	//PL_INKL_SEL		1: Is Selection
	//PL_SEL_KRIT		Selection criteria	XML format, to be converted to SQL when needed.
	//PL_SAMMELLISTE	1: Is Filter
	//PL_LST_SUBLISTS	List of sub-SPLs	XML format
	//-------------------------------------------------------------------------------
	//This input is to be stored into BUS_STORED_PROJECT_LIST as follows:
	//
	//BSPL_NAME -> trimmed(PL_CODE +" "+ PL_NAME)  /* PL_NAME je najcesce prazan */
	//BSPL_USEBY_PERSON_ID -> PL_PERSON (search BUS_PERSON by BPER_CODE = PL_PERSON) if not found then set NULL, but report error on console (qdebug)
	//BSPL_USEBY_DEPT_ID -> PL_ABTEILUNG (search BUS_DEPARTMENTS by BDEPT_CODE = PL_ABTEILUNG) if not found then set NULL
	//BSPL_IS_SELECTION -> PL_INKL_SEL
	//BSPL_SELECTION_DATA -> PL_SEL_KRIT
	//BSPL_IS_FILTER -> PL_SAMMELLISTE
	//BSPL_FILTER_DATA -> PL_LST_SUBLISTS
	
	DbRecordSet lstData;
	lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_STORED_PROJECT_LIST));

	SimpleTableCache PersonCache;
	PersonCache.Load("BUS_PERSON", "BPER_CODE", "BPER_ID", pStatus, m_pManager);

	SimpleTableCache DepartmentCache;
	DepartmentCache.Load("BUS_DEPARTMENTS", "BDEPT_CODE", "BDEPT_ID", pStatus, m_pManager);

	int nRowCnt = lstHeaderData.getRowCount();
	int i;
	int nSkipped = 0;
	for(i=0; i<nRowCnt; i++)
	{
		//TOFIX set BSPL_OWNER to some value?
		QString strName = lstHeaderData.getDataRef(i, 0).toString();
		if(!lstHeaderData.getDataRef(i, 1).toString().isEmpty()){
				strName += " ";
				strName += lstHeaderData.getDataRef(i, 1).toString();
		}
		strName = strName.trimmed();	//trimmed(PL_CODE +" "+ PL_NAME)
		if(strName.isEmpty()){
			qDebug() << "ERROR: Skip importing header line:" << i << "(Empty code + name)";
			nSkipped ++;
			continue;
		}

		int nPersonID = -1;	//search BUS_PERSON by BPER_CODE = PL_PERSON
		QString strPersonCode = lstHeaderData.getDataRef(i, 2).toString();
		if(!strPersonCode.isEmpty()){
			nPersonID = PersonCache.GetValue(strPersonCode).toInt();
		}
		int nDepartmentID = -1;
		QString strDepartmentCode = lstHeaderData.getDataRef(i, 3).toString();
		if(!strDepartmentCode.isEmpty()){
			nDepartmentID = DepartmentCache.GetValue(strDepartmentCode).toInt();
		}

		if (nPersonID < 1)
		{
			qDebug() << "ERROR: Could not find person id with code: " <<  strPersonCode;
		}
		if (nDepartmentID < 1)
		{
			qDebug() << "ERROR: Could not find department id with code: " <<  strDepartmentCode;
		}


		lstData.addRow();
		int nIdx = i-nSkipped;

		lstData.setData(nIdx, "BSPL_NAME",				strName);
		if (nPersonID > 0)			lstData.setData(nIdx, "BSPL_USEBY_PERSON_ID",	nPersonID);
		if (nDepartmentID > 0)		lstData.setData(nIdx, "BSPL_USEBY_DEPT_ID",	nDepartmentID);
		lstData.setData(nIdx, "BSPL_IS_SELECTION",		lstHeaderData.getDataRef(i, 4).toInt());
		lstData.setData(nIdx, "BSPL_SELECTION_DATA",	lstHeaderData.getDataRef(i, 5).toString());
		lstData.setData(nIdx, "BSPL_IS_FILTER",			lstHeaderData.getDataRef(i, 6).toInt());
		lstData.setData(nIdx, "BSPL_FILTER_DATA",		lstHeaderData.getDataRef(i, 7).toString());
	}

	//write data into the database
	DbSimpleOrm TableOrm(pStatus,BUS_STORED_PROJECT_LIST,m_pManager);
	if(!pStatus.IsOK()) return false;
	TableOrm.Write(pStatus,lstData);
	if(!pStatus.IsOK()) return false;

	//STEP 3: write details data

	//SPL Details: AKA "details" file import is defined by MB like this:
	//PL_CODE			Code		Mostly, this is a name. Spaces etc. are allowed
	//PL_NAME						Mostly empty, unless the list was defined in Timesheet
	//PR_CODE			Project Code
	//PR_AKTIV			0=not active 1=active	Currently used to mark inactive projects in the tree
	//T_LIST_ELEM_STATE	0=no subelements 1=Collapsed (+) 2=Expanded (-)
	//PR_STUFE			Level					Starts with 0
	//PR_EX_UNTERPROJ	0=no subelements in DB	1=subelements exist in DB
	//T_MAIN_NODE		Project code of parent node
	//PR_STYLE			Styling					Used to display icons etc. depending on project type (not yet used in SPC-Q7)
	//----------------------------------------------------------------------------
	//These records are copied directly to BUS_STORED_PROJECT_LIST_DATA with following algorithm
	//PR_CODE -> BSPLD_PROJECT_ID (find BUSP_ID by PR_CODE in BUS_PROJECT table and set it here)
	//T_LIST_ELEM_STATE-> BSPLD_EXPANDED

	lstData.destroy();
	lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_STORED_PROJECT_LIST_DATA));

	SimpleTableCache ProjectListCache;
	ProjectListCache.Load("BUS_STORED_PROJECT_LIST", "BSPL_NAME", "BSPL_ID", pStatus, m_pManager);

	SimpleTableCache ProjectCache;
	ProjectCache.Load("BUS_PROJECTS", "BUSP_CODE", "BUSP_ID", pStatus, m_pManager);

	nSkipped = 0;
	nRowCnt = lstDetailsData.getRowCount();

	for(int z=0; z<nRowCnt; z++)
	{
		QString strName = lstDetailsData.getDataRef(z, 0).toString();	
		if(!lstDetailsData.getDataRef(z, 1).toString().isEmpty()){
				strName += " ";
				strName += lstDetailsData.getDataRef(z, 1).toString();
		}
		strName = strName.trimmed();	//trimmed(PL_CODE +" "+ PL_NAME)
		if(strName.isEmpty()){
			qDebug() << "ERROR: Skip importing detail line:" << z << "(Empty code + name)";
			nSkipped ++;
			continue;
		}

		int nTreeID = -1;	//search BUS_PERSON by BPER_CODE = PL_PERSON
		QString strProjListName = strName;
		if(!strProjListName.isEmpty()){
			nTreeID = ProjectListCache.GetValue(strProjListName).toInt();
		}
		if (nTreeID < 1){
			qDebug() << "ERROR: Skip importing detail line:" << z << QString("(project list not found using name: [%1])").arg(strProjListName);
			nSkipped ++;
			continue;
		}

		int nProjectID = -1;
		QString strProjectCode = lstDetailsData.getDataRef(z, 2).toString();
		if(!strProjectCode.isEmpty()){
			nProjectID = ProjectCache.GetValue(strProjectCode).toInt();
		}
		if (nProjectID < 1){
			qDebug() << "ERROR: Skip importing detail line:" << z << QString("(project not found using code: [%1])").arg(strProjectCode);
			nSkipped ++;
			continue;
		}

		lstData.addRow();
		
		int nIdx = lstData.getRowCount()-1;
		lstData.setData(nIdx, "BSPLD_TREE_ID",		nTreeID);
		lstData.setData(nIdx, "BSPLD_PROJECT_ID",	nProjectID);
		lstData.setData(nIdx, "BSPLD_EXPANDED",		lstDetailsData.getDataRef(z, 4).toInt());

		//B.T: breaks on large import:
		//go by chunks of 10000
		if (nIdx==10000 || z==nRowCnt-1)
		{
			qDebug()<<"Writing chunk to "<<z;
			//write data into the database
			DbSimpleOrm TableOrm1(pStatus,BUS_STORED_PROJECT_LIST_DATA,m_pManager);
			if(!pStatus.IsOK()) return false;
			TableOrm1.Write(pStatus,lstData);
			if(!pStatus.IsOK()) return false;

			lstData.clear();
		}
	}




	return true;
}

void DbOrganizer::f0115_00_UpdateDateTimeFieldsBackFromUTC(Status &pStatus)
{
	//get all tables
	QHash<int, DbTableKeyData>   hshTableKeyData;
	DbSqlTableDefinition::GetAllTableData(hshTableKeyData);	

	//iterate through hash:
	QHashIterator<int, DbTableKeyData> i(hshTableKeyData);
	while (i.hasNext()) 
	{
		QString strSQL,strUpdate;
		i.next();
		QString strLastModified=i.value().m_strLastModified;
		if (i.value().m_nViewID<=0) //those are SPC tables!!
			continue;
		DbView view = DbSqlTableView::getView(i.value().m_nViewID);

		int nSize=view.size();
		for(int k=0;k<nSize;k++)
		{
			if (view.at(k).m_nType==QVariant::DateTime && view.at(k).m_strName!=strLastModified)
			{
				strSQL.append(view.at(k).m_strName);
				strSQL.append(",");
				strUpdate.append(view.at(k).m_strName);
				strUpdate.append("=?,");
			}

		}
		if (strSQL.isEmpty())
			continue;

		//qDebug()<<"UTC convert for "<<i.value().m_strTableName<<"for cols: " <<strSQL;

		strSQL.chop(1);
		strUpdate.chop(1);
		strSQL="SELECT "+i.value().m_strPrimaryKey+","+strLastModified+","+strSQL+" FROM "+i.value().m_strTableName;
		strUpdate="UPDATE "+i.value().m_strTableName+" SET "+strUpdate+" WHERE "+i.value().m_strPrimaryKey+"=?";

		DbSqlQuery query(pStatus,m_pManager);
		if(!pStatus.IsOK())return;
		query.Execute(pStatus,strSQL);
		if(!pStatus.IsOK())return;
		DbRecordSet lstData;
		query.FetchData(lstData);
		//lstData.Dump();
		//convert to UTC:


		nSize=lstData.getRowCount();
		int nColSize=lstData.getColumnCount();
		for(int k=0;k<nSize;k++)
		{
			for(int z=2;z<nColSize;z++) //skip ID & last modif at pos 0
			{
				if(lstData.getDataRef(k,z).isNull())
					continue;

				QDateTime tmp=lstData.getDataRef(k,z).toDateTime();
				//qDebug()<<tmp;
				tmp.setTimeSpec(Qt::UTC);
				tmp = tmp.toLocalTime();
				//qDebug()<<tmp;
				lstData.setData(k,z,tmp);
			}
		}

		//save back:
		query.Prepare(pStatus,strUpdate,true);
		if(!pStatus.IsOK())return;

		for(int k=0;k<nSize;k++)
		{
			for(int z=2;z<nColSize;z++) //skip ID & last modif at pos 0
			{
				query.bindValue(z-2,lstData.getDataRef(k,z));
			}
			query.bindValue(nColSize-2,lstData.getDataRef(k,0)); //ID
			query.ExecutePrepared(pStatus,-1,true);
			if(!pStatus.IsOK())return;
		}

	}


}

//check field CDI_DATABASE_UTF8 if exists and set to 1 then it's ok
bool DbOrganizer::TestIfUTF8Database(Status &pStatus)
{
	//make query
	DbSqlQuery query(pStatus,m_pManager);
	if(!pStatus.IsOK())return false;

	//if table does not exists this is first reorganization:
	if(!m_DbObjectManager->CheckIfExists_Table(m_pDb,"CORE_DATABASE_INFO"))
	{
		return false;
	}
	else
	{
		QString strSQL="SELECT * FROM CORE_DATABASE_INFO";
		query.Execute(pStatus,strSQL);
		if(!pStatus.IsOK()){return false;}
		DbRecordSet data;
		query.FetchData(data);

		if (data.getColumnIdx("CDI_DATABASE_UTF8")>=0 && data.getRowCount()>0)
		{
			if (data.getDataRef(0,"CDI_DATABASE_UTF8").isNull()) //if NULL -> then it is new SPC database, correct it to 1:
			{
				query.Execute(pStatus,"UPDATE CORE_DATABASE_INFO SET CDI_DATABASE_UTF8=1");
				return true; //DB is UTF8 already...
			}

			if (data.getDataRef(0,"CDI_DATABASE_UTF8").toInt()>0)
			{
				return true;
			}
		}
	}

	return false;
}

void DbOrganizer::CopySCDatabaseToUTF8(Status &pStatus)
{
	//create new database: new_utf8_oldname
	DbConnectionSettings OldDbSettings=m_pManager->GetDbSettings();
	QString strPathOld=OldDbSettings.m_strDbName;
	QFileInfo info_db(strPathOld);
	QString strPathNew=QDir::cleanPath(info_db.absolutePath()+"/new_utf8_"+info_db.fileName());
	QFile::remove(strPathNew);

	DbConnectionSetup::CreateFireBirdDatabase(pStatus,m_pManager->GetDbSettings().m_strDbUserName,m_pManager->GetDbSettings().m_strDbPassword,strPathNew);
	if (!pStatus.IsOK())
		return;

	DbConnectionSettings NewDbSettings=OldDbSettings;
	NewDbSettings.m_strDbName=strPathNew;
	
	DbSqlManager m_DbManagerTarget;
	m_DbManagerTarget.Initialize(pStatus,NewDbSettings);
	if (!pStatus.IsOK())
	{
		QFile::remove(strPathNew);
		return;
	}

	//Organize empty target database:
	DbOrganizer OrgTarget(&m_DbManagerTarget,m_strBackupDirPath);
	OrgTarget.OrganizeSCDatabaseForUTF8(pStatus);
	if (!pStatus.IsOK())
	{
		QFile::remove(strPathNew);
		return;
	}

	QSqlDatabase *pTargetConn=NULL;
	m_DbManagerTarget.ReserveExclusiveDbConnection(pStatus,&pTargetConn);
	if (!pStatus.IsOK())
	{
		QFile::remove(strPathNew);
		return;
	}


	//copy data:
	//muv constraints:

	//init all tables, and other objects with exclusive connection
	InitDboObjects(m_pDb);
	OrgTarget.InitDboObjects(pTargetConn);

	

	//-----------------------------LOW LEVEL---------------------------------------
	// SC
	//-----------------------------LOW LEVEL---------------------------------------

	//delete constrains:
	int lstTableCreatorSize = m_lstTableCreator.size();
	
	//drop all common points in target db:
	OrgTarget.m_CommonSPCSC_ConstraintCreator.DropAll(pStatus);
	if(!pStatus.IsOK())
	{
		m_DbManagerTarget.ReleaseExclusiveDbConnection(&pTargetConn);
		QFile::remove(strPathNew);
		return;
	}

	//drop all indexes, views, triggers and constraints (unique, fk)
	for(int i=0; i < lstTableCreatorSize; ++i)
	{
		//if(!m_DbObjectManager->CheckIfExists_Table(m_pDb,m_lstTableCreator.at(i)->m_TableData.m_strTableName))
		//	continue;

		qDebug()<<"Deleting constraints and views for table:"<<m_lstTableCreator.at(i)->m_TableData.m_strTableName;
		OrgTarget.m_lstTableCreator.at(i)->DeleteTableConstraints(pStatus);
		if(!pStatus.IsOK())
		{
			m_DbManagerTarget.ReleaseExclusiveDbConnection(&pTargetConn);
			QFile::remove(strPathNew);
			return;
		}
	}

	//copy data on all tables
	//create tables 
	for(int i=0; i < lstTableCreatorSize; ++i)
	{
		if(!m_DbObjectManager->CheckIfExists_Table(m_pDb,m_lstTableCreator.at(i)->m_TableData.m_strTableName))
			continue;
		
		qDebug()<<"Copying table:"<<m_lstTableCreator.at(i)->m_TableData.m_strTableName;

		m_lstTableCreator.at(i)->CopyData(pStatus,pTargetConn);
		if(!pStatus.IsOK())
		{
			m_DbManagerTarget.ReleaseExclusiveDbConnection(&pTargetConn);
			QFile::remove(strPathNew);
			return;
		}

		//readjusting generators to start count on new values:
		OrgTarget.m_DbObjectManager->AdjustAutoIncrementAfterDbCopy(pStatus,pTargetConn,m_lstTableCreator.at(i)->m_TableData.m_strTableName,m_lstTableCreator.at(i)->m_TableData.m_strPrimaryKey);
		if(!pStatus.IsOK())
		{
			m_DbManagerTarget.ReleaseExclusiveDbConnection(&pTargetConn);
			QFile::remove(strPathNew);
			return;
		}

	}

	//create constraints: triggers + FK's:
	//create triggers and views after tables (can reference each other and new ones)
	for(int i=0; i < lstTableCreatorSize; ++i)
	{
		//if(!m_DbObjectManager->CheckIfExists_Table(m_pDb,m_lstTableCreator.at(i)->m_TableData.m_strTableName))
		//	continue;

		qDebug()<<"Creating triggers: "<<m_lstTableCreator.at(i)->m_TableData.m_strTableName;
		OrgTarget.m_lstTableCreator.at(i)->CreateTriggers(pStatus);
		if(!pStatus.IsOK())
		{
			m_DbManagerTarget.ReleaseExclusiveDbConnection(&pTargetConn);
			QFile::remove(strPathNew);
			return;
		}
		qDebug()<<"Creating views: "<<m_lstTableCreator.at(i)->m_TableData.m_strTableName;
		OrgTarget.m_lstTableCreator.at(i)->CreateViews(pStatus);
		if(!pStatus.IsOK())
		{
			m_DbManagerTarget.ReleaseExclusiveDbConnection(&pTargetConn);
			QFile::remove(strPathNew);
			return;
		}
		qDebug()<<"Creating indexes: "<<m_lstTableCreator.at(i)->m_TableData.m_strTableName;
		OrgTarget.m_lstTableCreator.at(i)->CreateIndexes(pStatus);
		if(!pStatus.IsOK())
		{
			m_DbManagerTarget.ReleaseExclusiveDbConnection(&pTargetConn);
			QFile::remove(strPathNew);
			return;
		}
	}

	//create constraints: FK's:
	for(int i=0; i < lstTableCreatorSize; ++i)
	{
		//if(!m_DbObjectManager->CheckIfExists_Table(m_pDb,m_lstTableCreator.at(i)->m_TableData.m_strTableName))
		//	continue;

		qDebug()<<"Creating constraints for table:"<<m_lstTableCreator.at(i)->m_TableData.m_strTableName;
		OrgTarget.m_lstTableCreator.at(i)->CreateTableConstraints(pStatus);
		if(!pStatus.IsOK())
		{
			m_DbManagerTarget.ReleaseExclusiveDbConnection(&pTargetConn);
			QFile::remove(strPathNew);
			return;
		}
	}

	//create all common costraints in target db:
	/*
	OrgTarget.m_CommonSPCSC_ConstraintCreator.Create(pStatus);
	if(!pStatus.IsOK())
	{
		m_DbManagerTarget.ReleaseExclusiveDbConnection(&pTargetConn);
		QFile::remove(strPathNew);
		return;
	}
	*/

	//make query
	DbSqlQuery query(pStatus,&m_DbManagerTarget);
	if(!pStatus.IsOK())
	{
		m_DbManagerTarget.ReleaseExclusiveDbConnection(&pTargetConn);
		QFile::remove(strPathNew);
		return;
	}

	QString strSQL="UPDATE CORE_DATABASE_INFO SET CDI_DATABASE_UTF8=1";
	query.Execute(pStatus,strSQL);
	if(!pStatus.IsOK())
	{
		m_DbManagerTarget.ReleaseExclusiveDbConnection(&pTargetConn);
		QFile::remove(strPathNew);
		return;
	}

	m_DbManagerTarget.ReleaseExclusiveDbConnection(&pTargetConn);

	//disconnect from old one and rename new one and connect to new one
	//release connection:
	if (m_pDb!=NULL)
		m_pManager->ReleaseExclusiveDbConnection(&m_pDb);

	int nCnt = m_pManager->GetReservedConnections();

	m_pManager->ShutDown();
	m_DbManagerTarget.ShutDown();

	nCnt = m_pManager->GetReservedConnections();

	if(!QFile::remove(strPathOld))
	{
		
		//B.T.: wait some time then again:
		ThreadSleeper::Sleep(5000);
		if(!QFile::remove(strPathOld))
		{
			pStatus.setError(1,QString("Database is reorganized and stored in new UTF8 file: %1, but failed to remove old database file %2 (probably locked, manually delete 2nd file then rename 1st file)").arg(strPathNew).arg(strPathOld));
			return;
		}


	}

	if(!QFile::rename(strPathNew,strPathOld))
	{
		pStatus.setError(1,"Failed to rename file: "+strPathOld);
		return;
	}

	//if all ok then switch settings and reconnect to new DB:
	m_pManager->Initialize(pStatus,OldDbSettings);
	if (!pStatus.IsOK())
		return;

	//get back connection
	m_pManager->ReserveExclusiveDbConnection(pStatus,&m_pDb);
}





/*
	Marin requested this:
	3 access rights u bazi:
	- SYSDBA kao i do sada
	- SOKRATES/xan09 full admin user za SPC-korisnike (novo) --> To se treba ugraditi u ODBC installer i mora biti automatski u svakoj bazi
	//Note ne moze admin role jer ne mos to sjebat da radi..mora preko konekcije iskazati da je admin role, a mos mu puvat u guzicu kad ide preko ODBC-a
	//Ne mozes uopce ROLE koristiti jer isti je kurac kao i kod admin role...
*/

void DbOrganizer::CreateFBUsers(Status &pStatus, QString strMasterKey, QString strMasterPW, QSqlDatabase *pDbConnection)
{
	QString strUserName="SOKRATES";
	QString strPassword="xan09";
	
	//--------------------------------------
	//		drop user first, ignore errors
	//--------------------------------------
	QStringList lstArgs;
	/*
	lstArgs<<"-user";
	lstArgs<<strMasterKey;
	lstArgs<<"-password";
	lstArgs<<strMasterPW;
	lstArgs<<"-delete";
	lstArgs<<strUserName;

	
	QString strDir=QCoreApplication::applicationDirPath()+"/firebird/bin";
	mysqldump.setWorkingDirectory(strDir);
	mysqldump.start(strDir+"/gsec.exe",lstArgs);
	mysqldump.waitForFinished(-1);
	*/

	//--------------------------------------
	//		create user
	//--------------------------------------
	lstArgs.clear();

	lstArgs<<"-user";
	lstArgs<<strMasterKey;
	lstArgs<<"-password";
	lstArgs<<strMasterPW;
	lstArgs<<"-add";
	lstArgs<<strUserName;
	lstArgs<<"-pw";
	lstArgs<<strPassword;

	QProcess mysqldump;
	QString strDir=QCoreApplication::applicationDirPath()+"/firebird/bin";
	mysqldump.setWorkingDirectory(strDir);
	mysqldump.start(strDir+"/gsec.exe",lstArgs);
	if (!mysqldump.waitForFinished(-1)) //wait indefently
	{		
		pStatus.setError(1,QObject::tr("Failed to execute gsec. Make sure it is in /firebird/bin subdirectory!"));
		return;
	}
	int nExitCode=mysqldump.exitCode();
	QByteArray errContent=mysqldump.readAllStandardError();
	if (errContent.size()!=0)
	{
		if (!errContent.contains("violation of PRIMARY"))
		{
			pStatus.setError(1,QObject::tr("Failed to execute gsec! Reason:"+errContent.left(4095))); //max buff=4kb
			return;
		}
	}

	//ADMIN ROLE:
	QSqlQuery query(*pDbConnection);
	QStringList lstTables, lstViews,lstProcedcures, lstFunctions;
	O2F_GetAllObjectsFromDatabase(lstTables,lstViews,lstProcedcures,lstFunctions);

	//--------------------------------------
	// create role and assign 'em to user (we need sql query)
	//--------------------------------------
		
	pDbConnection->transaction();

	QString strSQL;

	/*
	QString strSQL="CREATE ROLE SOKRATES_ROLE";

	//first drop role:
	QString strSQL_Delete="DROP ROLE SOKRATES_ROLE";
	query.exec(strSQL_Delete);
	if(!query.exec(strSQL))
	{
		//error executing query
		pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, QObject::tr("Failed to create role: SOKRATES_ROLE")+query.lastError().text());
		pDbConnection->rollback();
		return;
	}
	*/

	int nSize=lstTables.size();
	for (int i=0;i<nSize;i++)
	{
		strSQL = "GRANT ALL ON " +lstTables.at(i) + " TO SOKRATES";
		if(!query.exec(strSQL))
		{
			//error executing query
			pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, QObject::tr("Failed to grant select on role: ")+strSQL+" - "+query.lastError().text());
			pDbConnection->rollback();
			return;
		}
	}

	nSize=lstViews.size();
	for (int i=0;i<nSize;i++)
	{
		strSQL = "GRANT SELECT ON " +lstViews.at(i) + " TO SOKRATES";
		if(!query.exec(strSQL))
		{
			//error executing query
			pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, QObject::tr("Failed to grant select on role: ")+strSQL+" - "+query.lastError().text());
			pDbConnection->rollback();
			return;
		}
	}

	nSize=lstProcedcures.size();
	for (int i=0;i<nSize;i++)
	{
		strSQL = "GRANT EXECUTE ON PROCEDURE " +lstProcedcures.at(i) + " TO SOKRATES";
		if(!query.exec(strSQL))
		{
			//error executing query
			pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, QObject::tr("Failed to grant select on role: ")+strSQL+" - "+query.lastError().text());
			pDbConnection->rollback();
			return;
		}
	}
	/*
	nSize=lstFunctions.size();
	for (int i=0;i<nSize;i++)
	{
		strSQL = "GRANT EXECUTE ON " +lstFunctions.at(i) + " TO ROLE SOKRATES_ROLE";
		if(!query.exec(strSQL))
		{
			//error executing query
			pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, QObject::tr("Failed to grant select on role: ")+strSQL+" - "+query.lastError().text());
			pDbConnection->rollback();
			return;
		}
	}
	*/
	/*
	strSQL ="GRANT SOKRATES_ROLE TO "+strUserName;

	if(!query.exec(strSQL))
	{
		//error executing query
		pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, QObject::tr("Failed to grant role on user: ")+strSQL+" - "+query.lastError().text());
		pDbConnection->rollback();
		return;
	}
	*/
	pDbConnection->commit();


	strUserName="ODBCREAD";
	strPassword="sokrep24";

	//--------------------------------------
	//		drop user first, ignore errors
	//--------------------------------------
	lstArgs.clear();

	/*
	lstArgs<<"-user";
	lstArgs<<strMasterKey;
	lstArgs<<"-password";
	lstArgs<<strMasterPW;
	lstArgs<<"-delete";
	lstArgs<<strUserName;

	//QProcess mysqldump;
	strDir=QCoreApplication::applicationDirPath()+"/firebird/bin";
	mysqldump.setWorkingDirectory(strDir);
	mysqldump.start(strDir+"/gsec.exe",lstArgs);
	mysqldump.waitForFinished(-1);

	*/

	//WARNING in system with more databases, deleting old users can be fatal!!!!!

	//--------------------------------------
	//		create user
	//--------------------------------------
	lstArgs.clear();

	lstArgs<<"-user";
	lstArgs<<strMasterKey;
	lstArgs<<"-password";
	lstArgs<<strMasterPW;
	lstArgs<<"-add";
	lstArgs<<strUserName;
	lstArgs<<"-pw";
	lstArgs<<strPassword;


	strDir=QCoreApplication::applicationDirPath()+"/firebird/bin";
	mysqldump.setWorkingDirectory(strDir);
	mysqldump.start(strDir+"/gsec.exe",lstArgs);
	if (!mysqldump.waitForFinished(-1)) //wait indecently
	{		
		pStatus.setError(1,QObject::tr("Failed to execute gsec. Make sure it is in /firebird/bin subdirectory!"));
		return;
	}
	nExitCode=mysqldump.exitCode();
	errContent=mysqldump.readAllStandardError();
	if (errContent.size()!=0)
	{
		if (!errContent.contains("violation of PRIMARY"))
		{
			pStatus.setError(1,QObject::tr("Failed to execute gsec! Reason:"+errContent.left(4095))); //max buff=4kb
			return;
		}
	}



	//--------------------------------------
	// create role and assign 'em to user (we need sql query)
	//--------------------------------------

	pDbConnection->transaction();
	/*
	strSQL="CREATE ROLE ODBCREADER_ROLE";

	//first drop role:
	strSQL_Delete="DROP ROLE ODBCREADER_ROLE";
	query.exec(strSQL_Delete);
	if(!query.exec(strSQL))
	{
		//error executing query
		pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, QObject::tr("Failed to create role: ODBCREADER_ROLE")+query.lastError().text());
		pDbConnection->rollback();
		return;
	}
	*/


	nSize=lstTables.size();
	for (int i=0;i<nSize;i++)
	{
		strSQL = "GRANT ALL ON " +lstTables.at(i) + " TO ODBCREADER";
		if(!query.exec(strSQL))
		{
			//error executing query
			pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, QObject::tr("Failed to grant select on role: ")+strSQL+" - "+query.lastError().text());
			pDbConnection->rollback();
			return;
		}
	}

	nSize=lstViews.size();
	for (int i=0;i<nSize;i++)
	{
		strSQL = "GRANT SELECT ON " +lstViews.at(i) + " TO ODBCREADER";
		if(!query.exec(strSQL))
		{
			//error executing query
			pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, QObject::tr("Failed to grant select on role: ")+strSQL+" - "+query.lastError().text());
			pDbConnection->rollback();
			return;
		}
	}

	nSize=lstProcedcures.size();
	for (int i=0;i<nSize;i++)
	{
		strSQL = "GRANT EXECUTE ON PROCEDURE " +lstProcedcures.at(i) + " TO ODBCREADER";
		if(!query.exec(strSQL))
		{
			//error executing query
			pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, QObject::tr("Failed to grant select on role: ")+strSQL+" - "+query.lastError().text());
			pDbConnection->rollback();
			return;
		}
	}

	/*
	nSize=lstFunctions.size();
	for (int i=0;i<nSize;i++)
	{
		strSQL = "GRANT EXECUTE ON " +lstFunctions.at(i) + " TO ROLE ODBCREADER_ROLE";
		if(!query.exec(strSQL))
		{
			//error executing query
			pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, QObject::tr("Failed to grant select on role: ")+strSQL+" - "+query.lastError().text());
			pDbConnection->rollback();
			return;
		}
	}
	*/

	/*
	strSQL ="GRANT ODBCREADER_ROLE TO "+strUserName;

	if(!query.exec(strSQL))
	{
		//error executing query
		pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL, QObject::tr("Failed to grant role on user: ")+strSQL+" - "+query.lastError().text());
		pDbConnection->rollback();
		return;
	}
	*/
	pDbConnection->commit();

	
}

void DbOrganizer::f0125_00_FixCustomFld(Status &pStatus)
{
	DbSqlQuery query(pStatus,m_pManager);
	if(!pStatus.IsOK())return;

	QString strSQL ="DROP INDEX IND_BCFL_TABLE_VAL";
	query.Execute(pStatus,strSQL,true);
	if(!pStatus.IsOK())return;

	strSQL ="ALTER TABLE BUS_CUSTOM_FLOAT DROP BCFL_VALUE";
	query.Execute(pStatus,strSQL,true);
	if(!pStatus.IsOK())return;

	strSQL ="ALTER TABLE BUS_CUSTOM_FLOAT ADD BCFL_VALUE VARCHAR(100)"; //float is strangly stored..use it as char...
	query.Execute(pStatus,strSQL,true);
	if(!pStatus.IsOK())return;

	strSQL ="create index IND_BCFL_TABLE_VAL on BUS_CUSTOM_FLOAT (BCFL_VALUE)";
	query.Execute(pStatus,strSQL,true);
	if(!pStatus.IsOK())return;

}
/*
void DbOrganizer::f65806_00_UpdateNewFields(Status &pStatus)
{
	//new flds for fprojct and fl_default2: to avoid NULL's, fill 'em with some values....
	DbSqlQuery query(pStatus,m_pManager);
	if(!pStatus.IsOK())return;

	QString strSQL ="UPDATE FPROJCT SET PR_BELTEXT_MUSS=0,PR_LOC_X=0,PR_LOC_Y=0,PR_LOC_REF=''";
	query.Execute(pStatus,strSQL,true);
	if(!pStatus.IsOK())return;

	strSQL ="UPDATE FL_DEFAULT2 SET DEF_HHM_DEB_INT='',DEF_UNIFLOW_VERSION=0,DEF_VIRTSERVER_ISALIVE=0,DEF_BR_KORR_BLOCK=0,DEF_NAV_FK_EXT=0,DEF_NAV_KA_KREDFAKT=''";
	query.Execute(pStatus,strSQL,true);
	if(!pStatus.IsOK())return;
}
*/


//get all objects:
void DbOrganizer::O2F_GetAllObjectsFromDatabase(QStringList &lstTables, QStringList &lstViews,QStringList &lstProcedcures,QStringList &lstFunctions)
{
	//tables, views
	int nSize = m_lstTableCreator.size();
	for(int i=0; i < nSize; ++i)
	{
		lstTables.append(m_lstTableCreator.at(i)->m_TableData.m_strTableName);
		lstViews.append(m_lstTableCreator.at(i)->GetViews());
	}

	//tables, views
	nSize = m_lstTableCreatorSPC.size();
	for(int i=0; i < nSize; ++i)
	{
		lstTables.append(m_lstTableCreatorSPC.at(i)->m_TableData.m_strTableName);
		lstViews.append(m_lstTableCreatorSPC.at(i)->GetViews());
	}

	//procedures:
	lstProcedcures = m_ProcedureCreator.GetProcedures();
	lstTables +=m_ProcedureCreator.GetGlobalTables();
	//functions:
	lstFunctions = m_FunctionCreator.GetFunctions();
	
}

void DbOrganizer::OrganizeSCDatabaseForUTF8(Status &pStatus)
{
	m_pManager->ReserveExclusiveDbConnection(pStatus,&m_pDb);
	if(!pStatus.IsOK()){m_pManager->ReleaseExclusiveDbConnection(&m_pDb);return;}

	//Step0: init all object to existing Db connection
	InitDboObjects(m_pDb);

	//Step1: drop common constraints:
	//m_CommonSPCSC_ConstraintCreator.DropAll(pStatus);
	//if(!pStatus.IsOK()){m_pManager->ReleaseExclusiveDbConnection(&m_pDb);return;}

	//Step2: reorganize SC step1
	OrganizeDatabase_SC_Step1(pStatus);
	if(!pStatus.IsOK()){m_pManager->ReleaseExclusiveDbConnection(&m_pDb);return;}

	//Step3: create common constraints:
	//m_CommonSPCSC_ConstraintCreator.Create(pStatus);
	//if(!pStatus.IsOK()){m_pManager->ReleaseExclusiveDbConnection(&m_pDb);return;}

	//Step4: Communicator: execute business procedures, clean up and elevate database version:
	OrganizeDatabase_SC_Step2(pStatus);

	m_pManager->ReleaseExclusiveDbConnection(&m_pDb);
}

void DbOrganizer::f0132_00_FixEmailExtChkSum( Status &pStatus )
{
	DbSqlQuery query(pStatus,m_pManager);
	if(!pStatus.IsOK())return;

	QString strSQL ="SELECT BEM_ID, BEM_DAT_LAST_MODIFIED, BEM_MAILBOX_TYPE,BEM_TEMPLATE_FLAG,BEM_EXT_ID, BEM_EXT_ID_CHKSUM, BEM_TO_EMAIL_ACCOUNT, BEM_TO, CHAR_LENGTH(BEM_BODY) BODY_SIZE, CENT_OWNER_ID FROM BUS_EMAIL INNER JOIN CE_COMM_ENTITY ON BEM_COMM_ENTITY_ID=CENT_ID";
	query.Execute(pStatus,strSQL,true);
	if(!pStatus.IsOK())return;

	DbRecordSet lstData;
	query.FetchData(lstData);

	int nSize=lstData.getRowCount();
	//lstData.Dump();

	for (int i=0;i<nSize;i++)
	{
		QString strToAcc=lstData.getDataRef(i,"BEM_TO_EMAIL_ACCOUNT").toString().left(120);
		if (strToAcc.isEmpty())
			strToAcc=lstData.getDataRef(i,"BEM_TO").toString().left(120);

		QString strChkSum=QString::number(lstData.getDataRef(i,"CENT_OWNER_ID").toInt())+"-";
		strChkSum+=strToAcc+"-";
		strChkSum+=QString::number(lstData.getDataRef(i,"BODY_SIZE").toInt())+"-";
		strChkSum+=lstData.getDataRef(i,"BEM_EXT_ID").toByteArray().toBase64();

		lstData.setData(i,"BEM_EXT_ID_CHKSUM",strChkSum);
	}

	lstData.removeColumn(lstData.getColumnIdx("CENT_OWNER_ID"));
	lstData.removeColumn(lstData.getColumnIdx("BODY_SIZE"));
	lstData.setColValue("BEM_MAILBOX_TYPE",1); //set mailbox to inbox
	//lstData.setColValue("BEM_TEMPLATE_FLAG",0); //reset template flag no templates for now -> MB stores them in separate space
	
	
	query.WriteData(pStatus,"BUS_EMAIL",lstData);
	if(!pStatus.IsOK())return;
	
	//copy email body to new table:
	strSQL ="INSERT INTO BUS_EMAIL_BINARY (BEB_ID, BEB_DAT_LAST_MODIFIED, BEB_EMAIL_ID, BEB_BODY) SELECT GEN_ID(SEQ_BEB_ID,1),CURRENT_TIMESTAMP, BEM_ID, BEM_BODY FROM BUS_EMAIL";
	query.Execute(pStatus,strSQL,true);
	if(!pStatus.IsOK())return;

	strSQL ="UPDATE BUS_EMAIL SET BEM_BODY = NULL";
	query.Execute(pStatus,strSQL,true);
	if(!pStatus.IsOK())return;
	
}


void DbOrganizer::f0134_00_DeleteOldPushMessages(Status &pStatus)
{
	DbSqlQuery query(pStatus, m_pManager);	
	if(!pStatus.IsOK())return;

	//delete old ones:
	QDateTime datOldPushTokens=QDateTime::currentDateTime().addDays(-1); //-1days
	QString strSQL=QString("DELETE FROM BUS_PUSH_MESSAGE WHERE BPM_CREATED<?" );
	query.Prepare(pStatus,strSQL);
	if(!pStatus.IsOK())return;

	query.bindValue(0,datOldPushTokens);
	query.ExecutePrepared(pStatus);

}