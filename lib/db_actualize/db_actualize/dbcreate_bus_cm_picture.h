#ifndef DBCREATE_BUS_CM_PICTURE_H
#define DBCREATE_BUS_CM_PICTURE_H


#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_BUS_CM_PICTURE : public DbSqlTableCreation
{
public:
	DbCreate_BUS_CM_PICTURE():DbSqlTableCreation(BUS_CM_PICTURE){};  //init table
	

private:
	//create generic Table def:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);

	//probably common to all DB's:
	void DefineIndexes(SQLDBObjectCreate &dboList);
};

#endif // DBCREATE_BUS_CM_PICTURE_H
