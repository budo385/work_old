#include "dbcreate_core_accrsetrole.h"

//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_CORE_ACCRSETROLE::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
											create table CORE_ACCRSETROLE (				\
											CASTR_ID INTEGER not null,					\
											CASTR_GLOBAL_ID VARCHAR(15) null,			\
											CASTR_DAT_LAST_MODIFIED TIMESTAMP not null,	\
											CASTR_ROLE_ID INTEGER not null,				\
											CASTR_ACCRSET_ID INTEGER not null,			\
											constraint CORE_ACCRSETROLE_PK primary key (CASTR_ID) ) ");

	//no Db versioning:
	lstSQL.append("alter table CORE_ACCRSETROLE						\
							   add constraint CORE_ARS_ACCRSROLE_FK1	\
							   foreign key (CASTR_ACCRSET_ID)						\
							   references CORE_ACCRSET (CAST_ID)");

	lstSQL.append("alter table CORE_ACCRSETROLE						\
							   add constraint CORE_ROLE_CORE_ACCRSETROLE_FK1		\
							   foreign key (CASTR_ROLE_ID)							\
							   references CORE_ROLE (CROL_ID) ON DELETE CASCADE");


}

//probably common to all DB's:
void DbCreate_CORE_ACCRSETROLE::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();

	dboList["IND_CASTR_ROLE_ID"]	= "create index IND_CASTR_ROLE_ID on CORE_ACCRSETROLE (CASTR_ROLE_ID)";
	dboList["IND_CASTR_ACCRSET_ID"] = "create index IND_CASTR_ACCRSET_ID on CORE_ACCRSETROLE (CASTR_ACCRSET_ID)";
}
