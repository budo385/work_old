#include "dbcreate_bus_push_token_person.h"

//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_PUSH_TOKEN_PERSON::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
											create table BUS_PUSH_TOKEN_PERSON (					\
											BPT_ID INTEGER not null,					\
											BPT_GLOBAL_ID VARCHAR(15) null,			\
											BPT_DAT_LAST_MODIFIED TIMESTAMP not null,	\
											BPT_PUSH_TOKEN VARCHAR(2000) not null,		\
											BPT_OS_TYPE INTEGER not null,				\
											BPT_DAT_LAST_SUCCESS_SEND TIMESTAMP null,	\
											BPT_PERSON_FK_ID INTEGER not null,			\
											constraint BUS_PUSH_TOKEN_PERSON_PK primary key (BPT_ID) )");

	lstSQL.append("\
											alter table BUS_PUSH_TOKEN_PERSON\
											add constraint BUS_PUSH_TOKEN_PERSON_FK1 foreign key (\
											BPT_PERSON_FK_ID)\
											references BUS_PERSON (\
											BPER_ID)  ON DELETE CASCADE ");
}

//probably common to all DB's:
void DbCreate_BUS_PUSH_TOKEN_PERSON::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
	//dboList["IND_BPM_STATUS"] = "create index IND_BPM_STATUS on BUS_PUSH_MESSAGE (BPM_STATUS)";
}


