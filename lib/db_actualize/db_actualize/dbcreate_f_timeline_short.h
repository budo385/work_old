#ifndef DBCREATE_F_TIMELINE_SHORT_H
#define DBCREATE_F_TIMELINE_SHORT_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_TIMELINE_SHORT : public DbSqlTableCreation
{
public: 
	DbCreate_F_TIMELINE_SHORT():DbSqlTableCreation(F_TIMELINE_SHORT){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_TIMELINE_SHORT_H
