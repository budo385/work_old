#ifndef DBCREATE_BUS_CAL_BREAKS_H
#define DBCREATE_BUS_CAL_BREAKS_H


#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_BUS_CAL_BREAKS : public DbSqlTableCreation
{
public:
	DbCreate_BUS_CAL_BREAKS():DbSqlTableCreation(BUS_CAL_BREAKS){};  //init table
	

private:
	//create generic Table def:
	void DefineFields(QStringList &lstSQL);

	//probably common to all DB's:
	void DefineIndexes(SQLDBObjectCreate &dboList);
};


#endif 
