#include "dbcreate_ce_event.h"



//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_CE_EVENT::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
											create table CE_EVENT (\
											CEV_ID INTEGER not null,\
											CEV_GLOBAL_ID VARCHAR(15) null,\
											CEV_DAT_LAST_MODIFIED TIMESTAMP not null,\
											CEV_LOGGE_USER_ID INTEGER not null,\
											CEV_ENTITY_ID INTEGER not null,\
											CEV_EVENT_TYPE_ID INTEGER not null,\
											CEV_DESCRIPTION LONGVARCHAR null,\
											CEV_DAT_EVENT_OCCURED DATETIME not null, constraint CE_EVENT_PK primary key (CEV_ID))");


	//constraint for table creation:
	lstSQL.append("\
											alter table CE_EVENT\
											add constraint CE_COM_ENT_CE_EV_FK1 foreign key (\
											CEV_ENTITY_ID)\
											references CE_COMM_ENTITY (\
											CENT_ID) \
											ON DELETE CASCADE"); //when entity is deleted, delete its events

	//constraint for table creation:
	lstSQL.append("\
											alter table CE_EVENT\
											add constraint CE_EV_TYP_CE_EV_FK1 foreign key (\
											CEV_EVENT_TYPE_ID)\
											references CE_EVENT_TYPE (\
											CEVT_ID)\
											"); //when type is deleted, forbid if exists in events..

 
	//constraint for table creation:
	lstSQL.append("\
								   alter table CE_EVENT\
								   add constraint CE_EV_TYP_PERS_FK1 foreign key (\
								   CEV_LOGGE_USER_ID)\
								   references BUS_PERSON (\
								   BPER_ID)\
								   "); //when type is deleted, forbid if exists in events..

}

//probably common to all DB's:
void DbCreate_CE_EVENT::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
	dboList["IND_CEV_ENTITY_ID"] = "create index IND_CEV_ENTITY_ID on CE_EVENT (CEV_ENTITY_ID)";  
	dboList["IND_CEV_EVENT_TYPE_ID"] = "create index IND_CEV_EVENT_TYPE_ID on CE_EVENT (CEV_EVENT_TYPE_ID)";  
}


