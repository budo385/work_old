#ifndef DBCREATE_F_HONORARKLASSE_H
#define DBCREATE_F_HONORARKLASSE_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_HONORARKLASSE : public DbSqlTableCreation
{
public: 
	DbCreate_F_HONORARKLASSE():DbSqlTableCreation(F_HONORARKLASSE){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_HONORARKLASSE_H
