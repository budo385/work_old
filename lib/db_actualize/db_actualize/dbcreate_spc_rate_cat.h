#ifndef DBCREATE_SPC_RATE_CAT_H
#define DBCREATE_SPC_RATE_CAT_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_SPC_RATE_CAT : public DbSqlTableCreation
{
public: 
	DbCreate_SPC_RATE_CAT():DbSqlTableCreation(SPC_RATE_CAT){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // SPC_RATE_CAT_H
