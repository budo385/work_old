#include "dbsqltablecreation.h"
#include "common/common/stringhelper.h"
#include "db_core/db_core/db_drivers.h"
#include "db/db/dbsqlquery.h"

/*!
	Constructor: inits with table data
*/

DbSqlTableCreation::DbSqlTableCreation(int TableID)
{
	DbSqlTableDefinition::GetKeyData(TableID, m_TableData);

}



/*!
	Create table (from scratch) based on last version.

	\param pManager		- pointer to the Db Manager
	\param pDb			- pointer to the valid DbConnection 
	\param pDboManager	- pointer to the Db object manager

*/
void DbSqlTableCreation::InitDbSettings(DbSqlManager *pManager, QSqlDatabase *pDb,DbObjectManager *pDboManager )
{
	//set & test DB pointers
	m_pManager = pManager;
	m_pDb = pDb;
	m_DbObjectManager = pDboManager;
	m_DbType=pManager->GetDbType();

	//asserter
	Q_ASSERT_X(m_pDb!=NULL,"Init Create Table", "NULL pointers detected");
	Q_ASSERT_X(m_pManager!=NULL,"Init Create Table", "NULL pointers detected");

	//call init method:
	m_TableDDL.clear();
	Initialize(m_DbType);
}


/*!
	Based on DB type, initialize table object DDL

	\param strDbType	- Db Type

*/
void DbSqlTableCreation::Initialize(QString strDbType)
{

	if(strDbType==DBTYPE_ORACLE)
		Init_Oracle();
	else if (strDbType==DBTYPE_MYSQL)
		Init_MySQL();
	else if (strDbType==DBTYPE_MS_SQL)
		Init_MS_SQL();
	else if (strDbType==DBTYPE_FIREBIRD)
		Init_FireBird();
	//else if (strDbType==DBTYPE_POSTGRESQL)
	//	Init_PostgreSQL();
	//else if (strDbType==DBTYPE_DB2)
	//	Init_DB2();
	else
	   Q_ASSERT_X(false,"TABLE CREATE", "Database type not supported");


}





/*!
	Create table (from scratch or drop existing) based on last version and all other associated table objects.
	
	\param pStatus			returned status
	\param bToLastVersion	if 1 then create with last version defintion, else create as for first Db version
*/
void DbSqlTableCreation::CreateTable(Status &pStatus, bool bRebuildIndexes,bool bDropTable, bool bSkipCreateIndexes, bool bFillDefaultValuesForNewFields)
{

	//engage transaction:
	DbSqlQuery query(pStatus,m_pManager);

	if (bDropTable)
	{
		m_DbObjectManager->DropIfExists_Table(pStatus,m_pDb,m_TableData.m_strTableName);
		if(!pStatus.IsOK()) 
			return;
	}

	//if table exists, create, else upgrade
	if(!m_DbObjectManager->CheckIfExists_Table(m_pDb,m_TableData.m_strTableName))
	{
		CreateFields(pStatus);
			if(!pStatus.IsOK()) 
				return;
	
		CreateAutoIncrementField(pStatus);
			if(!pStatus.IsOK())
				return;
	}
	else
	{
		if (bRebuildIndexes)
		{
			//drop all indexes in DB for this table:
			m_DbObjectManager->DropAllIndexes(pStatus,m_TableData.m_strTableName,m_pDb);
			if (!pStatus.IsOK())
				return;
		}

		//check sequences: if not exists create and update to max value
		CheckAutoIncrementField(pStatus);
			if(!pStatus.IsOK()) 
				return;

		//truncate fields:
		DbRecordSet lstData;
		TruncateFields(pStatus,lstData);
		if (!pStatus.IsOK()) 
			return;

		//alter table:
		UpgradeFields(pStatus,bFillDefaultValuesForNewFields);
			if (!pStatus.IsOK()) 
				return;

		//save truncated data:
		SaveTruncateFields(pStatus,lstData);
		if (!pStatus.IsOK()) 
			return;
	}


	//create indexes (only new ones):
	qDebug()<<"Create indexes for "<<m_TableData.m_strTableName;
	CreateIndexes(pStatus);
}

//drops columns (at end of upgrade process)
void DbSqlTableCreation::DropColumns(Status &pStatus)
{
	//if nothing to drop exit
	pStatus.setError(0);
	if (m_lstDeletedCols.size()==0)
		return;

	//drop it (all indexes not in use are already dropped):
	m_DbObjectManager->DropColumns(pStatus,m_TableData.m_strTableName,m_lstDeletedCols,m_pDb);

}



//------------------------------------------------------------------------------
//								TABLE FIELDS
//------------------------------------------------------------------------------

/*!
	Drop & create table based on info in m_Table
	NOTE: no transaction, use it on higher level!

	\param pStatus &pStatus
	\param bToLastVersion  - true means crate table to the latest version
*/
void DbSqlTableCreation::CreateFields(Status &pStatus)
{
	QString strSQL;
	QStringList lstSQLs;

	//check if DB is supported:
	if (!m_TableDDL.contains(m_DbType))
		{pStatus.setError(StatusCodeSet::ERR_SQL_UNKNOWN_DBTYPE);return;}

	lstSQLs= m_TableDDL[m_DbType];

	//Create query:
	DbSqlQuery query(pStatus,m_pManager);

	//first statement must be CREATE table:
	strSQL=lstSQLs.at(0);

	//add storage space:
	strSQL+=m_DbObjectManager->GetTableSpace();

	//Drop object if exists:
	m_DbObjectManager->DropIfExists_Table(pStatus,m_pDb,m_TableData.m_strTableName);
	if(!pStatus.IsOK())
		return;

	//create table:
	query.Execute(pStatus,strSQL.trimmed().simplified());
	
	if(!pStatus.IsOK())
		return;
}


/*!
	Create indexes, triggers and PK-FK
	In create table list, any statement beyond first is constraint statement (PK-FK)
	This must be separated, because it must be executed after all tables are created in DB!

	\param pStatus &pStatus
	\param bToLastVersion  - true means crate table to the latest version
*/
void DbSqlTableCreation::CreateTableConstraints(Status &pStatus)
{
	/* //must be left out and called separate as in import data triggers and views must go first, data second then constraints at end
	//create triggers:
	CreateTriggers(pStatus);
	if(!pStatus.IsOK()) 
		return;

	//create views:
	CreateViews(pStatus);
	if(!pStatus.IsOK()) 
		return;
	*/

	//create PK-FK links:
	QString strSQL;
	QStringList lstConstraints;

	//check if DB is supported:
	if (!m_TableDDL.contains(m_DbType))
		{pStatus.setError(StatusCodeSet::ERR_SQL_UNKNOWN_DBTYPE);return;}

	lstConstraints= m_TableDDL[m_DbType];

	//Create query:
	DbSqlQuery query(pStatus,m_pManager);

	//start from 1
	int lstConstraintsSize = lstConstraints.size();
	for (int i=1; i < lstConstraintsSize; ++i)
	{
		strSQL=lstConstraints.at(i).simplified();
		if (!query.Execute(pStatus,strSQL))
			return;	//execute each SQL
	}
}


//drops all FK's && triggers
void DbSqlTableCreation::DeleteTableConstraints(Status &pStatus, bool bDropOnlyConstraints)
{
	if (!bDropOnlyConstraints)
	{
		//delete triggers and views as they can reference other tables:;
		DropTriggers(pStatus);
		if(!pStatus.IsOK()) return;

		//drop views
		DropViews(pStatus);
		if(!pStatus.IsOK()) return;

		//drop all indexes for this table:
		m_DbObjectManager->DropAllIndexes(pStatus,m_TableData.m_strTableName,m_pDb);
		if (!pStatus.IsOK()) return;
	}

	//indexes are automatically deleted with table, no danger
	QString strSQL;
	QStringList lstConstraints;

	pStatus.setError(0);
	bool bExists=m_DbObjectManager->CheckIfExists_Table(m_pDb, m_TableData.m_strTableName);
	if(!bExists)
		return; //table doesn't exists

	//check if DB is supported:
	if (!m_TableDDL.contains(m_DbType))
		{pStatus.setError(StatusCodeSet::ERR_SQL_UNKNOWN_DBTYPE);return;}

	lstConstraints= m_TableDDL[m_DbType];

	//Create query:
	DbSqlQuery query(pStatus,m_pManager);

	//start from 1
	int lstConstraintsSize = lstConstraints.size();
	for (int i=1; i < lstConstraintsSize; ++i)
	{
		strSQL=lstConstraints.at(i);

		//------------------------PARSE FK NAME---------------------
		//parse FK name:
		int nPos2=strSQL.indexOf("foreign",Qt::CaseInsensitive);
		if(nPos2==-1) continue;
		QString strFK=strSQL;
		strFK.truncate(nPos2);

		int nPos1=strSQL.indexOf("add constraint",Qt::CaseInsensitive);
		if(nPos1==-1) continue;
		nPos1=nPos1+QString("add constraint").size()+1;

		strFK=strFK.right(strFK.size()-nPos1).trimmed();

		if(m_pManager->GetDbType()==DBTYPE_FIREBIRD) //hardcoded DB dependency
			strFK="ALTER TABLE "+m_TableData.m_strTableName+" DROP CONSTRAINT "+strFK;
		else
			strFK="ALTER TABLE "+m_TableData.m_strTableName+" DROP FOREIGN KEY "+strFK;
		
		//------------------------PARSE FK NAME---------------------
		
		query.Execute(pStatus,strFK); //skip status check: if not exist just go-on
	}

	pStatus.setError(0);
}

/*!
	ALTER table, preserve data
*/
void DbSqlTableCreation::UpgradeFields(Status &pStatus,bool bFillDefaultValuesForNewFields)
{
	//alter table: must exists prior, indexes must be destroyed before
	QStringList lstEmpty; //skip dropping cols here, call special API DropColumns
	m_DbObjectManager->AlterTable(pStatus,m_TableData.m_strTableName,m_TableDDL[m_DbType].at(0),lstEmpty,m_lstChangedNameCols,m_pDb,bFillDefaultValuesForNewFields);
}


/*!
	Create AutoInc fields, creates seq on autoinc field + insert trigger
	If not supported by DB egine, it will be not executed.
	Override to customize. 

	\param pStatus &pStatus
*/
void DbSqlTableCreation::CreateAutoIncrementField(Status &pStatus)
{
	m_DbObjectManager->CreateAutoIncrementID(pStatus, m_pDb,m_TableData.m_strTableName, m_TableData.m_strPrimaryKey);
}

void DbSqlTableCreation::CheckAutoIncrementField(Status &pStatus)
{
	//check sequences (if not exist create and adjust to max value):
	if(!m_DbObjectManager->CheckIfExists_Sequence(m_pDb,"SEQ_"+m_TableData.m_strPrimaryKey))
	{

		m_DbObjectManager->CreateAutoIncrementID(pStatus, m_pDb,m_TableData.m_strTableName, m_TableData.m_strPrimaryKey);
		if (pStatus.IsOK())
			m_DbObjectManager->AdjustAutoIncrementAfterDbCopy(pStatus, m_pDb,m_TableData.m_strTableName, m_TableData.m_strPrimaryKey);
	}
}





//------------------------------------------------------------------------------
//							ASSOCIATED TABLE OBJECTS
//------------------------------------------------------------------------------
/*!
	Creates indexes, only stored latest version by Dbtype.
	Old ones are not dropped!!
	Procedure for dropping indexes: just add DROP index statement in alter table/version array dboUpgrade

	\param pStatus &pStatus
*/
void DbSqlTableCreation::CreateIndexes(Status &pStatus)
{
	QString strSQL,strIndexName;

	//reset status:
	pStatus.setError(0);

	//check if DB is supported:
	if (!m_Indexes.contains(m_DbType))
		{pStatus.setError(StatusCodeSet::ERR_SQL_UNKNOWN_DBTYPE);return;}

	//prepare query
	DbSqlQuery query(pStatus,m_pManager);

	//loop through indexes
	SQLDBObjectCreateIterator i(m_Indexes.value(m_DbType));


	while (i.hasNext()) 
		{
			i.next();
			//extract  SQL
			strSQL=i.value();
			strIndexName=i.key();

			strSQL+=m_DbObjectManager->GetIndexSpace();
			if(m_DbObjectManager->CheckIfExists_Index(m_pDb,m_TableData.m_strTableName,strIndexName)) 
				continue; //skip if exists
			/*
			if (m_TableData.m_strTableName == "BUS_DM_SERVER_FILE")
			{
					QString strCheck="SELECT COUNT(*) FROM RDB$INDICES WHERE RDB$RELATION_NAME='BUS_DM_SERVER_FILE'";
					if (query.Execute(pStatus,strCheck))	
					{
						query.next();
						qDebug() << query.value(0).toInt();
					}
			}
			*/

			//Create new:
			if (!query.Execute(pStatus,strSQL))	
			{
				return;
			}
	}
}




/*!
	Creates triggers

	\param pStatus - error
*/
void DbSqlTableCreation::CreateTriggers(Status &pStatus)
{
	QString strSQL,strTriggerName;

	//reset status:
	pStatus.setError(0);

	//check if DB is supported:
	if (!m_Triggers.contains(m_DbType))
		{pStatus.setError(StatusCodeSet::ERR_SQL_UNKNOWN_DBTYPE);return;}

	//prepare query
	DbSqlQuery query(pStatus,m_pManager);

	//loop through indexes
	SQLDBObjectCreateIterator i(m_Triggers.value(m_DbType));

	while (i.hasNext()) 
		{
			i.next();
			
			//extract  SQL
			strSQL=m_Triggers.value(m_DbType).value(i.key()).simplified();
			strTriggerName=i.key();
			
			//Drop object if exists:
			m_DbObjectManager->DropIfExists_Trigger(pStatus,m_pDb,strTriggerName);
			if(!pStatus.IsOK())
				{return;}
			
			//Create new:
			if (!query.Execute(pStatus,strSQL.trimmed().simplified()))	
				{return;}
	}
}



/*!
	Delete them all

	\param pStatus - error
*/
void DbSqlTableCreation::DropTriggers(Status &pStatus)
{
	QString strSQL,strTriggerName;

	//reset status:
	pStatus.setError(0);
	//check if DB is supported:
	if (!m_Triggers.contains(m_DbType))
	{pStatus.setError(StatusCodeSet::ERR_SQL_UNKNOWN_DBTYPE);return;}
	//prepare query
	DbSqlQuery query(pStatus,m_pManager);
	//loop through indexes
	SQLDBObjectCreateIterator i(m_Triggers.value(m_DbType));

	while (i.hasNext()) 
	{
		i.next();
		//extract  SQL
		strSQL=m_Triggers.value(m_DbType).value(i.key());
		strTriggerName=i.key();

		//Drop object if exists:
		m_DbObjectManager->DropIfExists_Trigger(pStatus,m_pDb,strTriggerName);
		if(!pStatus.IsOK())
			return;
	}

	//for some databases there is trigger for inserting new value of sequence: leave that trigger
}


void DbSqlTableCreation::CreateViews(Status &pStatus)
{
	QString strSQL,strViewName;

	//reset status:
	pStatus.setError(0);

	//check if DB is supported:
	if (!m_Views.contains(m_DbType))
	{pStatus.setError(StatusCodeSet::ERR_SQL_UNKNOWN_DBTYPE);return;}
	//prepare query
	DbSqlQuery query(pStatus,m_pManager);
	//loop through indexes
	SQLDBObjectCreateIterator i(m_Views.value(m_DbType));

	while (i.hasNext()) 
	{
		i.next();
		//extract  SQL
		strSQL=m_Views.value(m_DbType).value(i.key()).simplified();
		strViewName=i.key();

		//Drop object if exists:
		m_DbObjectManager->DropIfExists_View(pStatus,m_pDb,strViewName);
		if(!pStatus.IsOK())
		{return;}

		//Create new:
		if (!query.Execute(pStatus,strSQL.trimmed().simplified()))	 
		{return;}
	}
}

void DbSqlTableCreation::DropViews(Status &pStatus)
{
	QString strSQL,strViewName;

	//reset status:
	pStatus.setError(0);
	//check if DB is supported:
	if (!m_Views.contains(m_DbType))
	{pStatus.setError(StatusCodeSet::ERR_SQL_UNKNOWN_DBTYPE);return;}
	//prepare query
	DbSqlQuery query(pStatus,m_pManager);
	//loop through indexes
	SQLDBObjectCreateIterator i(m_Views.value(m_DbType));

	while (i.hasNext()) 
	{
		i.next();
		//extract  SQL
		strSQL=m_Views.value(m_DbType).value(i.key());
		strViewName=i.key();

		//Drop object if exists:
		m_DbObjectManager->DropIfExists_View(pStatus,m_pDb,strViewName);
		if(!pStatus.IsOK())
			return;
	}
}


void DbSqlTableCreation::DropIndexes(Status &pStatus)
{
	//drop all indexes for this table:
	m_DbObjectManager->DropAllIndexes(pStatus,m_TableData.m_strTableName,m_pDb);
	if (!pStatus.IsOK()) return;
}



//------------------------------------------------------------------------------
//								HELPER
//------------------------------------------------------------------------------





/*!
	Maps Data types from ODBC generic driver to specified DB type

	\param inputDLL		   - DDL SQL string for table creation with generic ODBC datatypes
	\param outputDDL	   - returned DDL SQL string in specified DbType format
	\param strDbType	   - database type of returned string
*/
void DbSqlTableCreation::MapTableDataTypes(QString inputDLL, QString& outputDDL,QString strDbType)
{

	if(strDbType==DBTYPE_MYSQL)
	{
		//WARNING: replaces first occurence of not null in sQL CREATE TABLE with auto increment statement
		if (inputDLL.indexOf("CREATE TABLE",0,Qt::CaseInsensitive)!=-1)
		{	
			if (inputDLL.indexOf("not null")!=-1)
				outputDDL=inputDLL.replace(inputDLL.indexOf("not null"),8,"AUTO_INCREMENT not null");
		}
		else
			outputDDL=inputDLL;

		outputDDL=outputDDL.replace(" NUMERIC"," DECIMAL",Qt::CaseInsensitive);
		outputDDL=outputDDL.replace(" BIT "," SMALLINT ",Qt::CaseInsensitive);
		outputDDL=outputDDL.replace(" BINARY(10)"," MEDIUMBLOB ",Qt::CaseInsensitive);
		outputDDL=outputDDL.replace(" BINARY "," MEDIUMBLOB ",Qt::CaseInsensitive);
		outputDDL=outputDDL.replace(" LONGVARCHAR "," MEDIUMTEXT ",Qt::CaseInsensitive);
		outputDDL=outputDDL.replace(" LONGVARBINARY "," MEDIUMBLOB ",Qt::CaseInsensitive);
		//outputDDL=StringHelper::RemovSpecialChars(outputDDL);
		return;
	}

	if(strDbType==DBTYPE_ORACLE)
	{
		//outputDDL=inputDLL.replace("INTEGER","NUMBER(11,0)",Qt::CaseInsensitive);
		//outputDDL=outputDDL.replace("TINYINT","NUMBER(3,0)",Qt::CaseInsensitive);
		//outputDDL=outputDDL.replace("SMALLINT","NUMBER(6,0)",Qt::CaseInsensitive);
		//outputDDL=outputDDL.replace("VARCHAR","VARCHAR2",Qt::CaseInsensitive);
		outputDDL=outputDDL.replace("BINARY(10)","BLOB",Qt::CaseInsensitive);
		outputDDL=outputDDL.replace("DATETIME","DATE",Qt::CaseInsensitive);
		outputDDL=outputDDL.replace("TIME","DATE",Qt::CaseInsensitive);
		//outputDDL=StringHelper::RemovSpecialChars(outputDDL);
		return;
	}

	if(strDbType==DBTYPE_MS_SQL)
	{
		//WARNING: replaces first occurence of comma in sQL with auto increment statement
		if (inputDLL.indexOf(",")!=-1)
			outputDDL=inputDLL.replace(inputDLL.indexOf(","),1," IDENTITY (1,1),");
		else
			outputDDL=inputDLL;
		outputDDL=outputDDL.replace("BINARY(10)","IMAGE",Qt::CaseInsensitive);
		//outputDDL=StringHelper::RemovSpecialChars(outputDDL);
		return;
	}

	if(strDbType==DBTYPE_FIREBIRD)
	{
		//remove NULL as default is NULL:
		//outputDDL=inputDLL.replace("DAT_LAST_MODIFIED TIMESTAMP not null","DAT_LAST_MODIFIED TIMESTAMP DEFAULT CURRENT_TIMESTAMP",Qt::CaseInsensitive);
		outputDDL=inputDLL.replace(" not null"," AJME_MENI_122",Qt::CaseInsensitive);
		outputDDL=outputDDL.replace(" null"," ",Qt::CaseInsensitive);
		outputDDL=outputDDL.replace(" AJME_MENI_122"," not null",Qt::CaseInsensitive);

		outputDDL=outputDDL.replace(" DATETIME"," TIMESTAMP",Qt::CaseInsensitive); 
		outputDDL=outputDDL.replace(" NUMERIC"," DECIMAL",Qt::CaseInsensitive);
		outputDDL=outputDDL.replace(" TINYINT"," SMALLINT",Qt::CaseInsensitive);
		outputDDL=outputDDL.replace(" BIT "," SMALLINT ",Qt::CaseInsensitive);
		outputDDL=outputDDL.replace(" BINARY(10)"," BLOB ",Qt::CaseInsensitive);
		outputDDL=outputDDL.replace(" BINARY "," BLOB ",Qt::CaseInsensitive);
		outputDDL=outputDDL.replace(" LONGVARCHAR "," BLOB sub_type TEXT ",Qt::CaseInsensitive);
		outputDDL=outputDDL.replace(" LONGVARBINARY "," BLOB ",Qt::CaseInsensitive);

		//outputDDL=StringHelper::RemovSpecialChars(outputDDL);
		return;
	}

	
	Q_ASSERT_X(false,"DB not supproted","Make your own data type mapping here, or add new table DDL in table object itself");

}


/*!
	Maps whole Table DDL data from ODBC generic to specified DB
	Why: 
	- All fields for all tables and constraints (FK,PK) ared defined in generic SQL datatypes (ODBC) and then mapped to the specific database
	- All other objects (indexes, triggers, views) are defined already for specific database through functions: DefineIndexes, DefineTriggers, DefineViews
	- Those functions must have IF(m_DbType) then ...else ... etc....

	\param strDbType	   - database type of target DB
*/
void DbSqlTableCreation::MapTableDDLData(QString strDbType)
{
	QStringList lstMappedSQLs,lstSQLs;
	DefineFields(lstSQLs);
	
	QString strNewSQL;

	//map CREATE TABLE STATEMENTS (field data types):

	// map only first: create table:
	MapTableDataTypes(lstSQLs.at(0),strNewSQL,strDbType);
	lstSQLs[0]=strNewSQL;
	m_TableDDL[strDbType]=lstSQLs;

	SQLDBObjectCreate dboList;
	DefineIndexes(dboList);
	m_Indexes[strDbType]=dboList;

	dboList.clear();
	DefineTriggers(dboList);
	m_Triggers[strDbType]=dboList;

	dboList.clear();
	DefineViews(dboList);
	m_Views[strDbType]=dboList;
}


/*!
	Get's table row count. Used in filling default data.
*/
int DbSqlTableCreation::GetTableRowCount()
{
	Status pStatus;
	//make local query, can fail if connection reservation fails
	DbSqlQuery query(pStatus,m_pManager);
	if(!pStatus.IsOK()) return 100;

	QString strSQL = "SELECT COUNT(*) FROM " + m_TableData.m_strTableName + ";";

	query.Execute(pStatus, strSQL);
	if(!pStatus.IsOK()) return 100;

	query.next();
	return query.value(0).toInt();
}




//------------------------------------------------------------------------------
//				DB SPECIFIC MAPPERS (override for your table object)
//------------------------------------------------------------------------------




/* override this for your specific DB implementations. This function only maps DDL table creation statement*/
void DbSqlTableCreation::Init_MySQL()
{
	MapTableDDLData(DBTYPE_MYSQL);
}

/* override this for your specific DB implementations. This function only maps DDL table creation statement*/
void DbSqlTableCreation::Init_Oracle()
{
	MapTableDDLData(DBTYPE_ORACLE);
}

/* override this for your specific DB implementations. This function only maps DDL table creation statement*/
void DbSqlTableCreation::Init_MS_SQL()
{
	MapTableDDLData(DBTYPE_MS_SQL);
}


/* override this for your specific DB implementations. This function only maps DDL table creation statement*/
void DbSqlTableCreation::Init_FireBird()
{
	MapTableDDLData(DBTYPE_FIREBIRD);
}


/* select * from 1 insert into 2nd-> disable autoincrement */
void DbSqlTableCreation::CopyData(Status &pStatus,QSqlDatabase *pDbTargetConnection)
{

	//prepare queris
	DbSqlQuery querySource(pStatus,m_pManager);
	if (!pStatus.IsOK())
		return;
	QSqlQuery queryTarget(*pDbTargetConnection);

	//sql
	QString strDelete="DELETE FROM " +m_TableData.m_strTableName;
	//QString strSelect="SELECT " +DbSqlTableView::getSQLColumnsFromView(DbSqlTableView::getView(m_TableData.m_nViewID))+ " FROM "+m_TableData.m_strTableName;
	QString strSelect="SELECT * FROM "+m_TableData.m_strTableName;
	//QString strInsert=DbSqlTableView::getFullSQLInsert(m_TableData.m_nViewID);
	
	QStringList* lstCols = DbSqlTableView::getAllLongVarCharFields();

	//read from source
	querySource.Execute(pStatus,strSelect);
	if (!pStatus.IsOK())
		return;
	DbRecordSet data;
	//data.defineFromView(DbSqlTableView::getView(m_TableData.m_nViewID));
	//BT: 17.01.2008: to avoid conversion between blob/char, data will be automatically defined by source data
	//querySource.FetchData(data);

	//delete target:
	if(!queryTarget.exec(strDelete))
	{
		pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL,queryTarget.lastError().text());
		return;
	}

	bool bPrepared=false;

	//insert data on target:
	int nRow=0;
	while(querySource.FetchNextRow(data,true))
	{
		//insert on target:
		int nSize=data.getColumnCount();

		if (!bPrepared) //just insert on most common columns:
		{
			QString strInsert="INSERT INTO "+m_TableData.m_strTableName+" (";
			DbRecordSet recTemp;
			recTemp.defineFromView(DbSqlTableView::getView(m_TableData.m_nViewID));
			for(int i=0;i<nSize;++i)
			{
				if (recTemp.getColumnIdx(data.getColumnName(i))>=0)
					strInsert+=data.getColumnName(i)+",";
			}
			strInsert.chop(1);
			strInsert+=") VALUES (";
			for(int i=0;i<nSize;++i)
			{
				strInsert+="?,";
			}
			strInsert.chop(1);
			strInsert+=") ";

			//prepare target:
			if(!queryTarget.prepare(strInsert))
			{
				pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL,queryTarget.lastError().text());
				return;
			}

			bPrepared=true;
		}

		//data.Dump();
		for(int i=0;i<nSize;++i)
		{
			if(lstCols->indexOf(data.getColumnName(i))>=0)
			{
				QByteArray binData=data.getDataRef(nRow,i).toString().toUtf8();
				queryTarget.bindValue(i,binData);
			}
			else
				queryTarget.bindValue(i,data.getDataRef(nRow,i));
		}
		if(!queryTarget.exec())
		{
			data.Dump();
			
			//qDebug()<<data.getColumnType(data.getColumnIdx("BUSP_DESCRIPTION"));
			qDebug()<<strSelect;
			//qDebug()<<strInsert;
			qDebug()<<queryTarget.lastError().number();
			qDebug()<<queryTarget.lastError().databaseText();
			qDebug()<<queryTarget.lastError().driverText();
			pStatus.setError(StatusCodeSet::ERR_SQL_STATEMENT_FAIL,queryTarget.lastError().text());
			return;
		}

		//nRow++;
	}


}


QStringList DbSqlTableCreation::GetViews()
{
	QStringList lstViews;
	if (!m_Views.contains(m_DbType))
		{return lstViews;}

	//loop through indexes
	SQLDBObjectCreateIterator i(m_Views.value(m_DbType));
	while (i.hasNext()) 
	{
		i.next();
		lstViews.append(i.key());
	}

	return lstViews;
}

//when column is about to truncate, this is impossible with alter statement: so load data, drop it first then recreate and insert data..
//done before recreate table flds
//truncated columns are stored in format: DATABASE_VERSION;COLUMN_NAME and for SPC tables: SPCDATABASEVERSION;COLUMN_NAME, e.g.: 130;BEM_TO ili SPC6500;FL_DEF4
void DbSqlTableCreation::TruncateFields(Status &pStatus, DbRecordSet &lstOldData)
{
	//if nothing to drop exit
	pStatus.setError(0);
	if (m_lstTruncatedCols.size()==0)
		return;

	DbSqlQuery query(pStatus,m_pManager);
	if(!pStatus.IsOK())return;
	query.Execute(pStatus,"SELECT FIRST 1 * FROM "+ m_TableData.m_strTableName);
	if(!pStatus.IsOK())return;

	QSqlRecord rec = query.GetQSqlQuery()->record();

	DbRecordSet lstData;
	QString strSQL;
	query.FetchData(lstData);

	//check if REPLY_TO fld exists, if so then drop and recreate:

	int nDbVersion=m_pManager->GetDatabaseVersion(m_pDb);
	int nSPCDbVersion=m_pManager->GetDatabaseVersion_SPC(m_pDb);

	QStringList lstCols;
	for (int i=0;i<m_lstTruncatedCols.size();i++)
	{
		QString strDBVer=m_lstTruncatedCols.at(i).left(m_lstTruncatedCols.at(i).indexOf(";"));
		if (strDBVer.left(3)=="SPC")
		{
			if (strDBVer.mid(3).toInt()<nSPCDbVersion)
				continue;
		}
		else
		{
			if (strDBVer.toInt()<nDbVersion)
				continue;
		}

		QString strColName=m_lstTruncatedCols.at(i).mid(m_lstTruncatedCols.at(i).indexOf(";")+1);

		if(rec.indexOf(strColName)>=0) //skip if not exists:
		{
			strSQL.append(strColName+",");
			lstCols.append(strColName);
		}
	}
	if (strSQL.isEmpty())
		return;

	strSQL.chop(1);
	strSQL.prepend("SELECT "+m_TableData.m_strPrimaryKey+", "+m_TableData.m_strLastModified+", ");
	strSQL.append(" FROM "+m_TableData.m_strTableName);

	
	query.Execute(pStatus,strSQL);
	if(!pStatus.IsOK())return;

	query.FetchData(lstOldData);

	//drop it (all indexes not in use are already dropped):
	m_DbObjectManager->DropColumns(pStatus,m_TableData.m_strTableName,lstCols,m_pDb);
}

void DbSqlTableCreation::SaveTruncateFields( Status &pStatus, DbRecordSet &lstData )
{
	if (lstData.getRowCount()>0)
	{
		DbSqlQuery query(pStatus,m_pManager);
		if(!pStatus.IsOK())return;

		query.WriteData(pStatus,m_TableData.m_strTableName,lstData);
		if(!pStatus.IsOK())return;
	}
}