#include "dbcreate_core_database_info.h"

//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_CORE_DATABASE_INFO::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
								   create table CORE_DATABASE_INFO ( \
								   CDI_ID INTEGER not null,\
								   CDI_GLOBAL_ID VARCHAR(15) null,\
								   CDI_DAT_LAST_MODIFIED TIMESTAMP null,\
								   CDI_DATABASE_VERSION INTEGER not null, \
								   CDI_LAST_RESTORE_FILE VARCHAR(250) null, \
								   CDI_TEMPLATE_DB_USED VARCHAR(250) null, \
								   CDI_ASK_FOR_TEMPLATE_DB SMALLINT not null, \
								   CDI_START_SETUP_WIZARD SMALLINT not null, \
								   CDI_SETUP_WIZARD_WIZARD_INFO LONGVARBINARY null, \
								   CDI_SERVER_UPDATE_DATE DATE null, \
								   CDI_IS_DEMO_DATABASE SMALLINT null, \
								   CDI_DEMO_DB_MAX_SIZE INTEGER null, \
								   CDI_DEMO_DB_WARN_SIZE INTEGER null, \
								   CDI_DEMO_DB_CURR_SIZE INTEGER null, \
								   CDI_DATABASE_UNIQUE_KEY VARCHAR(250) null, \
								   CDI_DATABASE_UTF8 SMALLINT null, \
								   constraint CORE_DATABASE_INFO_PK primary key (CDI_ID) )");  

}



//probably common to all DB's:
void DbCreate_CORE_DATABASE_INFO::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
}


