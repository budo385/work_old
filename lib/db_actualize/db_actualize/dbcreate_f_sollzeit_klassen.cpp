#include "dbcreate_f_sollzeit_klassen.h"


//--------------------------------------------------
// ODBC - CREATE
//--------------------------------------------------
void DbCreate_F_SOLLZEIT_KLASSEN::DefineFields(QStringList &lstSQL) 
{
	//Table schema statements for ODBC.
	lstSQL.append(" 	CREATE TABLE F_SOLLZEIT_KLASSEN (\
										ESK_SEQUENCE INTEGER  NOT NULL,\
										ESK_CODE VARCHAR(34)  NOT NULL,ESK_TEXT VARCHAR(1101)  NOT NULL,\
										ESK_GLOBAL_ID VARCHAR(14)  NOT NULL,ESK_DAT_AENDERUNG DATETIME ,\
										ESK_DAT_EROEFFNUNG DATETIME ,ESK_MODIFIED_BY VARCHAR(36)  NOT NULL,\
										ESK_VALID_SINCE DATETIME ,ESK_EXPIRE_DATE DATETIME ,\
										constraint F_SOLLZEIT_KLASSEN_PK primary key (ESK_SEQUENCE))	");

}

void DbCreate_F_SOLLZEIT_KLASSEN::DefineIndexes(SQLDBObjectCreate &dboList) 
{
		//add index definition by its name (important).
		dboList["IND_ESK_CODE"] = "CREATE UNIQUE INDEX IND_ESK_CODE ON F_SOLLZEIT_KLASSEN (ESK_CODE)";
		dboList["IND_ESK_GLOBAL_ID"] = "CREATE INDEX IND_ESK_GLOBAL_ID ON F_SOLLZEIT_KLASSEN (ESK_GLOBAL_ID)";
		dboList["IND_ESK_DAT_AENDERUNG"] = "CREATE INDEX IND_ESK_DAT_AENDERUNG ON F_SOLLZEIT_KLASSEN (ESK_DAT_AENDERUNG)";
		dboList["IND_ESK_EXPIRE_DATE"] = "CREATE INDEX IND_ESK_EXPIRE_DATE ON F_SOLLZEIT_KLASSEN (ESK_EXPIRE_DATE)";
		dboList["IND_ESK_VALID_SINCE"] = "CREATE INDEX IND_ESK_VALID_SINCE ON F_SOLLZEIT_KLASSEN (ESK_VALID_SINCE)";

}

void DbCreate_F_SOLLZEIT_KLASSEN::DefineViews(SQLDBObjectCreate &dboList) 
{


	dboList["VW_F_SOLLZEIT_KLASSEN"] = "CREATE VIEW VW_F_SOLLZEIT_KLASSEN AS SELECT ESK_SEQUENCE,ESK_CODE,ESK_TEXT,ESK_GLOBAL_ID ,ESK_DAT_AENDERUNG,ESK_DAT_EROEFFNUNG,ESK_MODIFIED_BY,ESK_VALID_SINCE ,ESK_EXPIRE_DATE FROM F_SOLLZEIT_KLASSEN";

	dboList["VWS_F_SOLLZEIT_KLASSEN"] = "CREATE VIEW VWS_F_SOLLZEIT_KLASSEN AS SELECT ESK_SEQUENCE,ESK_CODE,ESK_TEXT,ESK_GLOBAL_ID ,ESK_DAT_AENDERUNG,ESK_DAT_EROEFFNUNG,ESK_MODIFIED_BY,ESK_VALID_SINCE ,ESK_EXPIRE_DATE FROM F_SOLLZEIT_KLASSEN";
}

void DbCreate_F_SOLLZEIT_KLASSEN::DefineTriggers(SQLDBObjectCreate &dboList) 
{
	if(m_DbType==DBTYPE_FIREBIRD)
	{

		dboList["TR_F_SOLLZEIT_KLASSEN"] = "CREATE TRIGGER TR_F_SOLLZEIT_KLASSEN FOR F_SOLLZEIT_KLASSEN BEFORE INSERT OR UPDATE AS BEGIN \
IF(NEW.ESK_CODE IS NULL) THEN NEW.ESK_CODE='';\
IF(NEW.ESK_TEXT IS NULL) THEN NEW.ESK_TEXT='';\
IF(NEW.ESK_GLOBAL_ID IS NULL) THEN NEW.ESK_GLOBAL_ID='';\
IF(NEW.ESK_MODIFIED_BY IS NULL) THEN NEW.ESK_MODIFIED_BY='';\
END;";
	}
}
