#ifndef DBCREATE_F_PERS_FUNKTION_H
#define DBCREATE_F_PERS_FUNKTION_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_PERS_FUNKTION : public DbSqlTableCreation
{
public: 
	DbCreate_F_PERS_FUNKTION():DbSqlTableCreation(F_PERS_FUNKTION){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_PERS_FUNKTION_H
