#ifndef DBCREATE_F_DELETE_H
#define DBCREATE_F_DELETE_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_DELETE : public DbSqlTableCreation
{
public: 
	DbCreate_F_DELETE():DbSqlTableCreation(F_DELETE){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_DELETE_H
