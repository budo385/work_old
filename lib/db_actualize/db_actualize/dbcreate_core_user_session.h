#ifndef DBCREATE_CORE_USER_SESSION_H
#define DBCREATE_CORE_USER_SESSION_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"




class DbCreate_CORE_USER_SESSION : public DbSqlTableCreation
{
public:
	DbCreate_CORE_USER_SESSION():DbSqlTableCreation(CORE_USER_SESSION){};  //init to CORE_LOCKING table

	

private:

	//create generic Table def:
	void DefineFields(QStringList &lstSQL);

	//probably common to all DB's:
	void DefineIndexes(SQLDBObjectCreate &dboList);
};



#endif //DBCREATE_CORE_USER_SESSION_H
