#ifndef DBCREATE_BUS_DM_USER_PATHS_H
#define DBCREATE_BUS_DM_USER_PATHS_H


#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"




class DbCreate_BUS_DM_USER_PATHS : public DbSqlTableCreation
{
public:
	DbCreate_BUS_DM_USER_PATHS():DbSqlTableCreation(BUS_DM_USER_PATHS){};  //init table
	

private:
	//create generic Table def:
	void DefineFields(QStringList &lstSQL);

	//probably common to all DB's:
	void DefineIndexes(SQLDBObjectCreate &dboList);
};

#endif // DBCREATE_BUS_DM_USER_PATHS_H
