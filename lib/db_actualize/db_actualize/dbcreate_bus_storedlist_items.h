#ifndef DBCREATE_BUS_STOREDLIST_ITEMS_H
#define DBCREATE_BUS_STOREDLIST_ITEMS_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_BUS_STOREDLIST_ITEMS : public DbSqlTableCreation
{
public:
    DbCreate_BUS_STOREDLIST_ITEMS():DbSqlTableCreation(BUS_STOREDLIST_ITEMS){};  //init to BUS_STOREDLIST_ITEMS table
	

private:
	//create generic Table def:
	void DefineFields(QStringList &lstSQL);

	//probably common to all DB's:
	void DefineIndexes(SQLDBObjectCreate &dboList);
};

#endif // DBCREATE_BUS_STOREDLIST_ITEMS_H
