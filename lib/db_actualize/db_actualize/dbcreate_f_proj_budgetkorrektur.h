#ifndef DBCREATE_F_PROJ_BUDGETKORREKTUR_H
#define DBCREATE_F_PROJ_BUDGETKORREKTUR_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_PROJ_BUDGETKORREKTUR : public DbSqlTableCreation
{
public: 
	DbCreate_F_PROJ_BUDGETKORREKTUR():DbSqlTableCreation(F_PROJ_BUDGETKORREKTUR){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_PROJ_BUDGETKORREKTUR_H
