#include "dbcreate_bus_comm_view_settings.h"

void DbCreate_BUS_COMM_VIEW_SETTINGS::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
								   create table BUS_COMM_VIEW_SETTINGS (		\
								   BUSCS_ID INTEGER not null,					\
								   BUSCS_GLOBAL_ID VARCHAR(15) null,			\
								   BUSCS_DAT_LAST_MODIFIED TIMESTAMP not null,	\
								   BUSCS_VIEW_ID INTEGER not null,				\
								   BUSCS_SETTING_ID INTEGER not null,			\
								   BUSCS_SETTING_TYPE INTEGER not null,			\
								   BUSCS_VALUE INTEGER null,					\
								   BUSCS_VALUE_STRING VARCHAR(255) null,		\
								   BUSCS_VALUE_BYTE LONGVARBINARY null,			\
								   BUSCS_VALUE_DATETIME TIMESTAMP null,			\
								   constraint BUS_COMM_VIEW_SETTINGS_PK primary key (BUSCS_ID) ) ");


	lstSQL.append("\
								   alter table BUS_COMM_VIEW_SETTINGS	\
								   add constraint BUS_VIEW_SETTINGS_FK1 foreign key ( \
								   BUSCS_VIEW_ID)						\
								   references BUS_COMM_VIEW (			\
								   BUSCV_ID) ON DELETE CASCADE" );

	//no Db versioning:

	//Delete column.
	m_lstDeletedCols.append("BUSCS_SETTING_SET_ID");

}

void DbCreate_BUS_COMM_VIEW_SETTINGS::DefineIndexes(SQLDBObjectCreate &dboList)
{
	dboList.clear();
	dboList["IND_BUSCS_PERSON_SETTING_ID"] = "create unique index IND_BUSCS_PERSON_SETTING_ID	\
												 on BUS_COMM_VIEW_SETTINGS								\
												 (BUSCS_VIEW_ID, BUSCS_SETTING_ID)";  
}
