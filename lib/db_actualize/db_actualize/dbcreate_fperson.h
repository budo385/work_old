#ifndef DBCREATE_FPERSON_H
#define DBCREATE_FPERSON_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_FPERSON : public DbSqlTableCreation
{
public: 
	DbCreate_FPERSON():DbSqlTableCreation(FPERSON){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // FPERSON_H
