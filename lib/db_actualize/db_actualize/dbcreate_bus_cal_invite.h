#ifndef DBCREATE_BUS_CAL_INVITE_H
#define DBCREATE_BUS_CAL_INVITE_H


#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_BUS_CAL_INVITE : public DbSqlTableCreation
{
public:
	DbCreate_BUS_CAL_INVITE():DbSqlTableCreation(BUS_CAL_INVITE){};  //init table
	

private:
	//create generic Table def:
	void DefineFields(QStringList &lstSQL);

	//probably common to all DB's:
	void DefineIndexes(SQLDBObjectCreate &dboList);
};


#endif // DBCREATE_BUS_CAL_INVITE_H
