#include "dbcreate_core_user.h"


//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_CORE_USER::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
											create table CORE_USER (					\
											CUSR_ID INTEGER not null,					\
											CUSR_GLOBAL_ID VARCHAR(15) null,			\
											CUSR_DAT_LAST_MODIFIED TIMESTAMP not null,	\
											CUSR_FIRST_NAME VARCHAR(50) null,			\
											CUSR_LAST_NAME VARCHAR(50) not null,		\
											CUSR_USERNAME VARCHAR(50) not null,			\
											CUSR_PASSWORD BINARY null,					\
											CUSR_ACTIVE_FLAG INTEGER not null,			\
											CUSR_PERSON_ID INTEGER null,				\
											CUSR_IS_SYSTEM SMALLINT not null,			\
											CUSR_LOCAL_ACCESS_ONLY SMALLINT not null,	\
											CUSR_IS_DEFAULT_ACC SMALLINT null,			\
											constraint CORE_USER_PK primary key (CUSR_ID) )");

	lstSQL.append("\
											alter table CORE_USER\
											add constraint CORE_PERS_FK1 foreign key (\
											CUSR_PERSON_ID)\
											references BUS_PERSON (\
											BPER_ID) \
											");
}

//probably common to all DB's:
void DbCreate_CORE_USER::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
	dboList["IND_CUSR_PERSON_ID"] = "create index IND_CUSR_PERSON_ID on CORE_USER (CUSR_PERSON_ID)";
	dboList["IND_CUSR_USERNAME"] = "create unique index IND_CUSR_USERNAME on CORE_USER (CUSR_USERNAME)";
}


