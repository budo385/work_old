#ifndef DBCREATE_F_STD_KONTROLLE_H
#define DBCREATE_F_STD_KONTROLLE_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_STD_KONTROLLE : public DbSqlTableCreation
{
public: 
	DbCreate_F_STD_KONTROLLE():DbSqlTableCreation(F_STD_KONTROLLE){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_STD_KONTROLLE_H
