#ifndef DBCREATE_F_TARIFKATEG_H
#define DBCREATE_F_TARIFKATEG_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_TARIFKATEG : public DbSqlTableCreation
{
public: 
	DbCreate_F_TARIFKATEG():DbSqlTableCreation(F_TARIFKATEG){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_TARIFKATEG_H
