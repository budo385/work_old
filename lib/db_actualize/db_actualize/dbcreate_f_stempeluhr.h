#ifndef DBCREATE_F_STEMPELUHR_H
#define DBCREATE_F_STEMPELUHR_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_STEMPELUHR : public DbSqlTableCreation
{
public: 
	DbCreate_F_STEMPELUHR():DbSqlTableCreation(F_STEMPELUHR){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_STEMPELUHR_H
