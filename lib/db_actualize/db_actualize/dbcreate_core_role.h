#ifndef DBCREATE_CORE_ROLE_H
#define DBCREATE_CORE_ROLE_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_CORE_ROLE : public DbSqlTableCreation
{
public:
	DbCreate_CORE_ROLE():DbSqlTableCreation(CORE_ROLE){};  //init to CORE_ROLE table
	

private:
	//create generic Table def:
	void DefineFields(QStringList &lstSQL);

	//probably common to all DB's:
	void DefineIndexes(SQLDBObjectCreate &dboList);
};

#endif // DBCREATE_CORE_ROLE_H
