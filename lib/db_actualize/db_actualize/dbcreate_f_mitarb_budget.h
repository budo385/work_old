#ifndef DBCREATE_F_MITARB_BUDGET_H
#define DBCREATE_F_MITARB_BUDGET_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_MITARB_BUDGET : public DbSqlTableCreation
{
public: 
	DbCreate_F_MITARB_BUDGET():DbSqlTableCreation(F_MITARB_BUDGET){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_MITARB_BUDGET_H
