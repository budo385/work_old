#ifndef DBCREATE_CORE_LOCKING_H
#define DBCREATE_CORE_LOCKING_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"




class DbCreate_CORE_LOCKING : public DbSqlTableCreation
{
public:
	DbCreate_CORE_LOCKING():DbSqlTableCreation(CORE_LOCKING){};  //init to CORE_LOCKING table

	

private:

	//create generic Table def:
	void DefineFields(QStringList &lstSQL);




	//for each DB override....
	//void Init_Oracle();
	//void Init_MySQL();
	//void Init_MS_SQL();
	//not implemented:
	//void DefineTriggers(SQLDBObjectCreate &dboList){};
	//void Init_PostgreSQL(){};
	//void Init_DB2(){};


	//probably common to all DB's:
	void DefineIndexes(SQLDBObjectCreate &dboList);
};



#endif // DbCreate_CORE_LOCKING_H
