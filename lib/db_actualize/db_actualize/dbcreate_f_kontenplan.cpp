#include "dbcreate_f_kontenplan.h"


//--------------------------------------------------
// ODBC - CREATE
//--------------------------------------------------
void DbCreate_F_KONTENPLAN::DefineFields(QStringList &lstSQL) 
{
	//Table schema statements for ODBC.
	lstSQL.append(" 	CREATE TABLE F_KONTENPLAN (\
										EKP_SEQUENCE INTEGER  NOT NULL,\
										EKP_CODE VARCHAR(34)  NOT NULL,EKP_NAME VARCHAR(111)  NOT NULL,\
										constraint F_KONTENPLAN_PK primary key (EKP_SEQUENCE))	");

}

void DbCreate_F_KONTENPLAN::DefineIndexes(SQLDBObjectCreate &dboList) 
{
		//add index definition by its name (important).
		dboList["IND_EKP_CODE"] = "CREATE INDEX IND_EKP_CODE ON F_KONTENPLAN (EKP_CODE)";

}

void DbCreate_F_KONTENPLAN::DefineViews(SQLDBObjectCreate &dboList) 
{


	dboList["VW_F_KONTENPLAN"] = "CREATE VIEW VW_F_KONTENPLAN AS SELECT EKP_SEQUENCE,EKP_CODE,EKP_NAME FROM F_KONTENPLAN";

	dboList["VWS_F_KONTENPLAN"] = "CREATE VIEW VWS_F_KONTENPLAN AS SELECT EKP_SEQUENCE,EKP_CODE,EKP_NAME FROM F_KONTENPLAN";
}

void DbCreate_F_KONTENPLAN::DefineTriggers(SQLDBObjectCreate &dboList) 
{
	if(m_DbType==DBTYPE_FIREBIRD)
	{

		dboList["TR_F_KONTENPLAN"] = "CREATE TRIGGER TR_F_KONTENPLAN FOR F_KONTENPLAN BEFORE INSERT OR UPDATE AS BEGIN \
IF(NEW.EKP_CODE IS NULL) THEN NEW.EKP_CODE='';\
IF(NEW.EKP_NAME IS NULL) THEN NEW.EKP_NAME='';\
END;";
	}
}
