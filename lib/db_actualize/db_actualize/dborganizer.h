#ifndef DBORGANIZER_H
#define DBORGANIZER_H


#include <QString>
#include <QHash>
#include "common/common/status.h"
#include "db/db/dbsqlquery.h"
#include "db/db/dbsqlmanager.h"
#include "importlogger.h"

//all DB specific managers:
#include "db/db/dbomanager.h"

//all DB object creators:
#include "dbsqltablecreation.h"
#include "dbsqlfunctioncreation.h"
#include "dbsqlprocedurecreation.h"
#include "dbsql_spc_sc_common_ddl_creation.h"

//include all object tables:
#include "table_collection.h"



/*!
	\class DbOrganizer
	\brief Main object for reorganizing/actualize database
	\ingroup Db_Actualize

	Holds exclusive DB connection, create or upgrade DB: see RS Database Actualization V1.0 2007-02-20.html
	How to add/alter own table/dbo object in the DB:
	- write own table object, inherit from DbSqlTableCreation (see others
	- in this object write any changes for any DbVersion and any DbType
	- add your table object header in table_collection.h
	- for new function/view/proc fill specified functions in DbSqlFunctionCreation,
	DbSqlProcedureCreation objects
	- add business procedures for filling default data inside InitializeBusinessProcedures
*/

class DbOrganizer
{
	typedef void (DbOrganizer::* PFN)(Status &pStatus);
	
public:
	DbOrganizer(DbSqlManager *pManager,QString strBackupDirPath="",bool bShutDownDatabase=false); 
	~DbOrganizer();

	//public interface through dbactualize.dll which is shipped with appserver.
	void OrganizeDatabase(Status &pStatus, bool bRebuildTables=false);
	void CopyDatabase(Status &pStatus, DbSqlManager *pManagerTarget,bool bDestroyPrevious=false);

	//demo helper:
	void CheckForDemoDatabase(Status &pStatus, bool bEnableDemo, int nMaxSizeKb, int nWarnSizeKb); 
	void RecalcDemoDatabaseSize(Status &pStatus, int &nCurrSizeKb); 

	//helper:
	int		GetDatabaseCurrentVersion(Status &pStatus); 
	int		GetDatabaseCurrentVersion_SPC(Status &pStatus); 

	QString BackupDatabase(Status &pStatus); 
	void	RestoreDatabase(QString strBackupFilePath,Status &pStatus); 

	//create ODBC user
	void CreateReadOnlyUserForODBCAccess(Status &pStatus);
	void ImportDataFromOracleExport(Status &pStatus,QString strDataFile, int &nProcessedInsertStatements, int &nProcessedSequenceUpdateStatements);
	void ImportDataFromOmnisExport(Status &pStatus,QString strDataMigrationDir, int &nProcessedInsertStatements, bool bDropExisting=false);

	//
	void OrganizeSCDatabaseForUTF8(Status &pStatus);
private:

	void OrganizeDatabaseAll(Status &pStatus, bool bReorganizeSC, bool bReorganizeSPC, bool bRebuildTables, bool bDatabaseWasEmpty);
		
	
	//Communicator
	//-------------------------------------------------
	void OrganizeDatabase_SC_Step1(Status &pStatus, bool bRebuildTables=false);
	void OrganizeDatabase_SC_Step2(Status &pStatus);
	void Initialize();
	void InitializeBusinessProcedures();
	void BeforeOrganize(Status &pStatus);
	void DropTables(Status &pStatus); //<- add own drops here (executed at end of reorganiztion)

	bool IsUpdateNeeded(Status &pStatus,bool &bDataBaseExists);
	void SaveNewDbVersion(Status &pStatus);
	bool IsDatabaseInSingleUserMode(Status &pStatus);
	bool TestIfUTF8Database(Status &pStatus);
	void CopySCDatabaseToUTF8(Status &pStatus);
	

	//Project Control
	//-------------------------------------------------
	void OrganizeDatabase_SPC_Step1(Status &pStatus, bool bRebuildTables=false);
	void OrganizeDatabase_SPC_Step2(Status &pStatus);
	void Initialize_SPC();
	void InitializeBusinessProcedures_SPC();
	void BeforeOrganize_SPC(Status &pStatus);
	void DropTables_SPC(Status &pStatus); //<- add own drops here (executed at end of reorganiztion)
	bool IsUpdateNeeded_SPC(Status &pStatus,bool &bDataBaseExists);
	void SaveNewDbVersion_SPC(Status &pStatus);
	void CreateFBUsers(Status &pStatus, QString strMasterKey, QString strMasterPW, QSqlDatabase *pDbConnection);


	
	//--------------------------------Business procedures for defaults-----------------------------
	//		Communicator
	//--------------------------------Business procedures for defaults-----------------------------
	void f0001_00_CreateAdminAccount(Status &pStatus);
	void f0001_01_CreateHiddenSystemAccount(Status &pStatus);
	void f0001_02_CreateDefaultOrganization(Status &pStatus);
	void f0001_03_CreateCoreAccessRightSet(Status &pStatus);
	void f0014_01_UpdateCE_COMM_ENTITY(Status &pStatus);
	void f0017_01_UpdateHiddenSystemAccount(Status &pStatus);
	void f0017_03_UpdateDatabaseInfo(Status &pStatus);
	void f0017_04_FixSearchPhone(Status &pStatus);
	void f0021_00_FixCeTypeTable(Status &pStatus);
	void f0022_00_FixDuplicateBigPictureID(Status &pStatus);
	void f0024_00_CreateDemoKey(Status &pStatus);
	void f0026_00_CreateContactPhonets(Status &pStatus);
	void f0027_00_InitEmailIDChecksum(Status &pStatus);
	void f0030_00_InitImportExportSettings(Status &pStatus);
	void f0032_00_FixSearchPhone_2(Status &pStatus);
	void f0035_00_FixHierachicalTables(Status &pStatus);
	void f0036_00_CreateSimpleAccessRightSet(Status &pStatus);
	void f0042_00_FixCodePageOnLongVarChar(Status &pStatus);
	void f0044_00_UpdatePersonOrganization(Status &pStatus);
	void f0044_01_UpdateSystemPhoneTypes(Status &pStatus);
	void f0046_00_UpdatePhonets(Status &pStatus);
	void f0047_00_FixHierachicalTables(Status &pStatus);
	void f0047_01_FixPrivateContact(Status &pStatus);
	void f0051_00_UpdateLocationPhonet(Status &pStatus);
	void f0052_00_UpdateCodePredial(Status &pStatus);
	void f0053_00_UpdateBinaryFields(Status &pStatus);
	void f0054_00_UpdateAddressCodeField(Status &pStatus);
	void f0059_00_CreateUARRecords(Status &pStatus);
	void f0066_00_RoleUpgrade(Status &pStatus);
	void f0070_00_TemplateCleanUp(Status &pStatus);
	void f0071_00_RoleUpgrade(Status &pStatus);
	void f0076_00_FixDocumentRevisionTable(Status &pStatus);
	void f0077_00_UpdateTaskPriorityField(Status &pStatus);
	void f0078_00_FixDocumentSize(Status &pStatus);
	void f0079_00_FixDocumentNoteSize(Status &pStatus);
	void f0080_00_FixItalianPhones(Status &pStatus);
	void f0081_00_FixItalianPhones_SagaContinues(Status &pStatus);
	void f0083_00_UpdateNMRXData(Status &pStatus);
	void f0084_00_UpdateUTCDateTimeFields(Status &pStatus);
	void f0090_00_ChangeFromNotNull2Null_CalendarOptions(Status &pStatus);
	void f0093_00_ChangeCalendarSettingFromStringToRecordset(Status &pStatus);
	void f0094_00_AccRSetUpgrade(Status &pStatus);
	void f0097_00_CalendarViewPersonalSettingUpdate(Status &pStatus);
	void f0099_00_UpdateEmailToolTip(Status &pStatus);
	void f0100_00_AccRSetUpgrade(Status &pStatus);
	void f0103_00_CalendarViewUpgrade(Status &pStatus);
	void f0104_00_AccRSetUpgrade_Calendar(Status &pStatus);
	void f0105_00_FixHierarchyStructureProjects(Status &pStatus);
	void f0108_00_CalendarViewUpgradeOnDeleteCascade(Status &pStatus);
	void f0115_00_UpdateDateTimeFieldsBackFromUTC(Status &pStatus);
	void f0117_00_FixHierarchyLevel(Status &pStatus);
	void f0125_00_FixCustomFld(Status &pStatus);
	void f0132_00_FixEmailExtChkSum(Status &pStatus);
	void f0134_00_DeleteOldPushMessages(Status &pStatus);

	
	void f9999_01_CreateApplicationOptions(Status &pStatus);


	//--------------------------------Business procedures for defaults-----------------------------
	//		SPC
	//--------------------------------Business procedures for defaults-----------------------------
	//void f65806_00_UpdateNewFields(Status &pStatus);

	



	//-------------------------------------------------
	//Common:
	//-------------------------------------------------
	void InitDboObjects(QSqlDatabase *pDbConn);
	void ExecuteBusinessProcedures(Status &pStatus, QMap<QString,PFN> &lstFunct);

	//-------------------------------Omnis 2 FB---------------------------------------------------
	bool O2F_ParseCTLFile(QString strCTLFile,QList<int> &lstColSizes,QStringList &lstColNames,QList<int> &lstColTypes, QString &strSequenceColumnName);
	bool O2F_InsertTableData(QString strTableName, QString strDATFile,QList<int> &lstColSizes,QStringList &lstColNames,QList<int> &lstColTypes, QByteArray &buffer_err,int &nErrorsDetected, int &nSQLProcessed);
	bool O2F_InsertBinaryTableData(QString strTableName, QString strBinFile, QByteArray &buffer_err,int &nErrorsDetected, int &nSQLProcessed);
	bool O2F_AdjustFKConstraints(Status &pStatus);
	bool O2F_ResetPasswords(Status &pStatus);
	bool O2F_ResetCommunicatorWizards(Status &pStatus);
	bool O2F_ImportStoredProjectListsCSV(QString strHeaderFile, QString strDetailsFile, Status &pStatus);
	bool O2F_UpdateHierarhicalData(Status &pStatus);
	void O2F_GetAllObjectsFromDatabase(QStringList &lstTables, QStringList &lstViews,QStringList &lstProcedcures,QStringList &lstFunctions);
	bool O2F_CountRecordsFromFile(QString strFile, int &nCnt);

	//-------------------------------Helper Funct--------------------------------------------------
	void FixHierachicalTable(Status &pStatus, int nTableID);
	void UpdateLongVarChar(Status &pStatus,QString strTableName,QString strFieldName,QString strIDName);
	void UpdateUARRecords(Status &pStatus, int nTableID,DbRecordSet &lstData,DbSqlQuery *query);
	void ChangeFromNotNull2NullFields(Status &pStatus, QString strTableName,QString strIDColumnName,QStringList lstFields, QStringList lstFieldsDataTypes);
	void DropAllSequencesFromOldSCDatabase(Status &pStatus);



	//--------------------------------PRIVATE VARIABLES-----------------------------

	DbSqlManager	*m_pManager;			//db manager (extern)
	QSqlDatabase	*m_pDb;					//exclusive connection
	DbObjectManager *m_DbObjectManager;		//db dependent object manager

	//Communicator:
	int							m_nCurrentDatabaseVersion;			//version from DB, 0=not found, recreate DB from zero.
	QList<DbSqlTableCreation* > m_lstTableCreator;
	QMap<QString,PFN>			mFunctList;
	bool						m_bRebuildTables;

	//Project Control
	int							m_nCurrentDatabaseVersionSPC;		//version from DB, 0=not found, recreate DB from zero.
	QList<DbSqlTableCreation* > m_lstTableCreatorSPC;
	QMap<QString,PFN>			mFunctListSPC;
	bool						m_bRebuildTablesSPC;
	
	//Coomon (functions and procedures)
	DbSqlFunctionCreation m_FunctionCreator;
	DbSqlProcedureCreation m_ProcedureCreator;
	DbSql_SPC_SC_Common_DDL_Creation m_CommonSPCSC_ConstraintCreator;

	QString		m_strBackupDirPath;
	bool		m_bShutDownDatabaseWhenBackup;
	int			m_nBUS_CM_PICTUREIdx;

	ImportLogger	m_StatusLogger;

};


#endif //DBORGANIZER_H

