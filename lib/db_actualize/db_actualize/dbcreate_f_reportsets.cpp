#include "dbcreate_f_reportsets.h"


//--------------------------------------------------
// ODBC - CREATE
//--------------------------------------------------
void DbCreate_F_REPORTSETS::DefineFields(QStringList &lstSQL) 
{
	//Table schema statements for ODBC.
	lstSQL.append(" 	CREATE TABLE F_REPORTSETS (\
										ERS_SEQUENCE INTEGER  NOT NULL,\
										ERS_CODE VARCHAR(23)  NOT NULL,ERS_NAME VARCHAR(111)  NOT NULL,\
										ERS_SETLIST  BLOB  ,\
										constraint F_REPORTSETS_PK primary key (ERS_SEQUENCE))	");

}

void DbCreate_F_REPORTSETS::DefineIndexes(SQLDBObjectCreate &dboList) 
{
		//add index definition by its name (important).
		dboList["IND_ERS_CODE"] = "CREATE INDEX IND_ERS_CODE ON F_REPORTSETS (ERS_CODE)";

}

void DbCreate_F_REPORTSETS::DefineViews(SQLDBObjectCreate &dboList) 
{


	dboList["VW_F_REPORTSETS"] = "CREATE VIEW VW_F_REPORTSETS AS SELECT ERS_SEQUENCE,ERS_CODE,ERS_NAME,ERS_SETLIST FROM F_REPORTSETS";

	dboList["VWS_F_REPORTSETS"] = "CREATE VIEW VWS_F_REPORTSETS AS SELECT ERS_SEQUENCE,ERS_CODE,ERS_NAME,ERS_SETLIST FROM F_REPORTSETS";
}

void DbCreate_F_REPORTSETS::DefineTriggers(SQLDBObjectCreate &dboList) 
{
	if(m_DbType==DBTYPE_FIREBIRD)
	{

		dboList["TR_F_REPORTSETS"] = "CREATE TRIGGER TR_F_REPORTSETS FOR F_REPORTSETS BEFORE INSERT OR UPDATE AS BEGIN \
IF(NEW.ERS_CODE IS NULL) THEN NEW.ERS_CODE='';\
IF(NEW.ERS_NAME IS NULL) THEN NEW.ERS_NAME='';\
END;";
	}
}
