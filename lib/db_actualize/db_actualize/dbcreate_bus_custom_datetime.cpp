#include "dbcreate_bus_custom_datetime.h"



//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_CUSTOM_DATETIME::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
											create table BUS_CUSTOM_DATETIME ( \
											BCDT_ID INTEGER not null,\
											BCDT_GLOBAL_ID VARCHAR(15) null,\
											BCDT_DAT_LAST_MODIFIED TIMESTAMP not null,\
											BCDT_CUSTOM_FIELD_ID INTEGER not null,\
											BCDT_VALUE DATETIME not null,\
											BCDT_TABLE_ID INTEGER not null,\
											BCDT_RECORD_ID INTEGER not null, constraint BUS_CUSTOM_DATETIME_PK primary key (BCDT_ID) ) "); 
											
							lstSQL.append("\
											alter table BUS_CUSTOM_DATETIME\
											add constraint BUS_CUSTOM_DATETIME_FK1 foreign key (\
											BCDT_CUSTOM_FIELD_ID)\
											references BUS_CUSTOM_FIELDS (\
											BCF_ID)  \
											ON DELETE CASCADE");

}

//probably common to all DB's:
void DbCreate_BUS_CUSTOM_DATETIME::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
	dboList["IND_BCDT_CI_TABLE_REC"] = "create index IND_BCDT_CI_TABLE_REC on BUS_CUSTOM_DATETIME (BCDT_TABLE_ID,BCDT_RECORD_ID)"; 
	dboList["IND_BCDT_TABLE_VAL"] = "create index IND_BCDT_TABLE_VAL on BUS_CUSTOM_DATETIME (BCDT_VALUE)"; 
	dboList["IND_BCDT_CI_UNIQUE"] = "create unique index IND_BCDT_CI_UNIQUE on BUS_CUSTOM_DATETIME (BCDT_TABLE_ID,BCDT_RECORD_ID,BCDT_CUSTOM_FIELD_ID)";

}


