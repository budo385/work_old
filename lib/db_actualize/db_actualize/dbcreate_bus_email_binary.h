#ifndef DBCREATE_BUS_EMAIL_BINARY_H
#define DBCREATE_BUS_EMAIL_BINARY_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_BUS_EMAIL_BINARY : public DbSqlTableCreation
{
public:
	DbCreate_BUS_EMAIL_BINARY():DbSqlTableCreation(BUS_EMAIL_BINARY){};  //init table
	

private:
	//create generic Table def:
	void DefineFields(QStringList &lstSQL);

	//probably common to all DB's:
	void DefineIndexes(SQLDBObjectCreate &dboList);
};

#endif // DBCREATE_BUS_EMAIL_BINARY_H
