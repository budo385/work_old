#include "dbcreate_spc_sector.h"


//--------------------------------------------------
// ODBC - CREATE
//--------------------------------------------------
void DbCreate_SPC_SECTOR::DefineFields(QStringList &lstSQL) 
{
	//Table schema statements for ODBC.
	lstSQL.append(" 	CREATE TABLE SPC_SECTOR (\
										SEFG_ID INTEGER not null,\
										SEFG_GLOBAL_ID VARCHAR(15) null,\
										SEFG_DAT_LAST_MODIFIED TIMESTAMP not null,\
										SEFG_CODE VARCHAR(50) not null,\
										SEFG_LEVEL INTEGER not null,\
										SEFG_PARENT INTEGER null,\
										SEFG_HASCHILDREN INTEGER not null,\
										SEFG_ICON VARCHAR(50) null,\
										SEFG_MAINPARENT_ID INTEGER null,\
										SEFG_NAME VARCHAR(100) not null,\
										SEFG_STYLE INTEGER null,\
										SEFG_F_GEBIETE_SEQ INTEGER null,\
										constraint SPC_SECTOR_PK primary key (SEFG_ID))");

}

void DbCreate_SPC_SECTOR::DefineIndexes(SQLDBObjectCreate &dboList) 
{
		//add index definition by its name (important).
		dboList["IND_SEFG_NAME"] = "CREATE INDEX IND_SEFG_NAME ON SPC_SECTOR (SEFG_NAME)";
		dboList["IND_SEFG_CODE"] = "CREATE UNIQUE INDEX IND_SEFG_CODE ON SPC_SECTOR (SEFG_CODE)";
		dboList["IND_SEFG_PARENT"] = "create index IND_SEFG_PARENT on SPC_SECTOR (SEFG_PARENT)";  
}

void DbCreate_SPC_SECTOR::DefineViews(SQLDBObjectCreate &dboList) 
{

}

void DbCreate_SPC_SECTOR::DefineTriggers(SQLDBObjectCreate &dboList) 
{

}
