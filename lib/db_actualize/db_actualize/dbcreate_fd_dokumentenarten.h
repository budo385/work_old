#ifndef DBCREATE_FD_DOKUMENTENARTEN_H
#define DBCREATE_FD_DOKUMENTENARTEN_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_FD_DOKUMENTENARTEN : public DbSqlTableCreation
{
public: 
	DbCreate_FD_DOKUMENTENARTEN():DbSqlTableCreation(FD_DOKUMENTENARTEN){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // FD_DOKUMENTENARTEN_H
