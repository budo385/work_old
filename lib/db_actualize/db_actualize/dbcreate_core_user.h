#ifndef DBCREATE_CORE_USER_H
#define DBCREATE_CORE_USER_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"
#include "db/db/dbsqlquery.h"


class DbCreate_CORE_USER : public  DbSqlTableCreation
{
public:
	DbCreate_CORE_USER():DbSqlTableCreation(CORE_USER){};  //init to CORE_USER table
	

private:
	//create generic Table def:
	void DefineFields(QStringList &lstSQL);
	//probably common to all DB's:
	void DefineIndexes(SQLDBObjectCreate &dboList);
};

#endif // DBCREATE_CORE_USER_H

