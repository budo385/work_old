#include "dbcreate_bus_cm_phone.h"



//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_CM_PHONE::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
											create table BUS_CM_PHONE ( \
											BCMP_ID INTEGER not null,\
											BCMP_GLOBAL_ID VARCHAR(15) null,\
											BCMP_DAT_LAST_MODIFIED TIMESTAMP not null,\
											BCMP_CONTACT_ID INTEGER not null,\
											BCMP_NAME VARCHAR(100) null,\
											BCMP_TYPE_ID INTEGER null,\
											BCMP_FULLNUMBER VARCHAR(100) not null,\
											BCMP_LOCAL VARCHAR(50) null,\
											BCMP_COUNTRY VARCHAR(50) null,\
											BCMP_AREA VARCHAR(50) null,\
											BCMP_EXT_ID INTEGER null,\
											BCMP_IS_DEFAULT SMALLINT not null,\
											BCMP_SEARCH VARCHAR(100) null, \
											BCMP_COUNTRY_ISO VARCHAR(5) null,\
											constraint BUS_CM_PHONE_PK primary key (BCMP_ID) )");  

	//constraint for table creation:
	lstSQL.append("\
											alter table BUS_CM_PHONE\
											add constraint BUS_CONTACTS_PHONE_FK1 foreign key (\
											BCMP_CONTACT_ID)\
											references BUS_CM_CONTACT (\
											BCNT_ID) \
											ON DELETE CASCADE");


	//if there is one item with given type: can not delete type
	lstSQL.append("\
											alter table BUS_CM_PHONE\
											add constraint BUS_TYPES_PHONE_FK1 foreign key (\
											BCMP_TYPE_ID)\
											references BUS_CM_TYPES (\
											BCMT_ID) ");
}



//probably common to all DB's:
void DbCreate_BUS_CM_PHONE::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
	dboList["IND_BCMP_CONTACT_ID"] = "create index IND_BCMP_CONTACT_ID on BUS_CM_PHONE (BCMP_CONTACT_ID) ";  
	dboList["IND_BCMP_SEARCH"] = "create index IND_BCMP_SEARCH on BUS_CM_PHONE (BCMP_SEARCH )";  
	dboList["IND_BCMP_TYPE_ID"] = "create index IND_BCMP_TYPE_ID on BUS_CM_PHONE (BCMP_TYPE_ID)  ";  
}


