#include "dbcreate_bus_custom_longtext.h"



//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_CUSTOM_LONGTEXT::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
											create table BUS_CUSTOM_LONGTEXT ( \
											BCLT_ID INTEGER not null,\
											BCLT_GLOBAL_ID VARCHAR(15) null,\
											BCLT_DAT_LAST_MODIFIED TIMESTAMP not null,\
											BCLT_CUSTOM_FIELD_ID INTEGER not null,\
											BCLT_VALUE LONGVARCHAR not null,\
											BCLT_TABLE_ID INTEGER not null,\
											BCLT_RECORD_ID INTEGER not null, constraint BUS_CUSTOM_LONGTEXT_PK primary key (BCLT_ID) ) "); 
											
							lstSQL.append("\
											alter table BUS_CUSTOM_LONGTEXT\
											add constraint BUS_CUSTOM_LONGTEXT_FK1 foreign key (\
											BCLT_CUSTOM_FIELD_ID)\
											references BUS_CUSTOM_FIELDS (\
											BCF_ID)  \
											ON DELETE CASCADE");

}

//probably common to all DB's:
void DbCreate_BUS_CUSTOM_LONGTEXT::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
	dboList["IND_BCLT_CI_TABLE_REC"] = "create index IND_BCLT_CI_TABLE_REC on BUS_CUSTOM_LONGTEXT (BCLT_TABLE_ID,BCLT_RECORD_ID)"; 
	dboList["IND_BCLT_CI_UNIQUE"] = "create unique index IND_BCLT_CI_UNIQUE on BUS_CUSTOM_LONGTEXT (BCLT_TABLE_ID,BCLT_RECORD_ID,BCLT_CUSTOM_FIELD_ID)"; 

}


