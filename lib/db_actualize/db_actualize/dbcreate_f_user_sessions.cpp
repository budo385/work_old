#include "dbcreate_f_user_sessions.h"


//--------------------------------------------------
// ODBC - CREATE
//--------------------------------------------------
void DbCreate_F_USER_SESSIONS::DefineFields(QStringList &lstSQL) 
{
	//Table schema statements for ODBC.
	lstSQL.append(" 	CREATE TABLE F_USER_SESSIONS (\
										USR_SEQUENCE INTEGER  NOT NULL,\
										USR_SESSION_ID VARCHAR(56)  NOT NULL,USR_PERS_CODE VARCHAR(23)  NOT NULL,\
										USR_START_TIME DATETIME ,USR_LAST_ALIVE DATETIME ,\
										USR_SERIAL_ID VARCHAR(56)  NOT NULL,USR_SERVER_IP VARCHAR(34)  NOT NULL,\
										USR_SERVER_PORT DECIMAL(10) NOT NULL,USR_DAT_AENDERUNG DATETIME ,\
										USR_TIME_OFFSET DECIMAL(10) NOT NULL,USR_GLOBAL_ID VARCHAR(14)  NOT NULL,\
										USR_DAT_EROEFFNUNG DATETIME ,USR_MODIFIED_BY VARCHAR(36)  NOT NULL,\
										USR_VALID_SINCE DATETIME ,USR_EXPIRE_DATE DATETIME ,\
										constraint F_USER_SESSIONS_PK primary key (USR_SEQUENCE))	");

}

void DbCreate_F_USER_SESSIONS::DefineIndexes(SQLDBObjectCreate &dboList) 
{
		//add index definition by its name (important).
		dboList["IND_USR_SESSION_ID"] = "CREATE UNIQUE INDEX IND_USR_SESSION_ID ON F_USER_SESSIONS (USR_SESSION_ID)";
		dboList["IND_USR_GLOBAL_ID"] = "CREATE INDEX IND_USR_GLOBAL_ID ON F_USER_SESSIONS (USR_GLOBAL_ID)";
		dboList["IND_USR_DAT_AENDERUNG"] = "CREATE INDEX IND_USR_DAT_AENDERUNG ON F_USER_SESSIONS (USR_DAT_AENDERUNG)";
		dboList["IND_USR_EXPIRE_DATE"] = "CREATE INDEX IND_USR_EXPIRE_DATE ON F_USER_SESSIONS (USR_EXPIRE_DATE)";
		dboList["IND_USR_VALID_SINCE"] = "CREATE INDEX IND_USR_VALID_SINCE ON F_USER_SESSIONS (USR_VALID_SINCE)";

}

void DbCreate_F_USER_SESSIONS::DefineViews(SQLDBObjectCreate &dboList) 
{


	dboList["VW_F_USER_SESSIONS"] = "CREATE VIEW VW_F_USER_SESSIONS AS SELECT USR_SEQUENCE,USR_SESSION_ID,USR_PERS_CODE,USR_START_TIME ,USR_LAST_ALIVE,USR_SERIAL_ID,USR_SERVER_IP,USR_SERVER_PORT ,USR_DAT_AENDERUNG,USR_TIME_OFFSET,USR_GLOBAL_ID,USR_DAT_EROEFFNUNG ,USR_MODIFIED_BY,USR_VALID_SINCE,USR_EXPIRE_DATE FROM F_USER_SESSIONS";

	dboList["VWS_F_USER_SESSIONS"] = "CREATE VIEW VWS_F_USER_SESSIONS AS SELECT USR_SEQUENCE,USR_SESSION_ID,USR_PERS_CODE,USR_START_TIME ,USR_LAST_ALIVE,USR_SERIAL_ID,USR_SERVER_IP,USR_SERVER_PORT ,USR_DAT_AENDERUNG,USR_TIME_OFFSET,USR_GLOBAL_ID,USR_DAT_EROEFFNUNG ,USR_MODIFIED_BY,USR_VALID_SINCE,USR_EXPIRE_DATE FROM F_USER_SESSIONS";
}

void DbCreate_F_USER_SESSIONS::DefineTriggers(SQLDBObjectCreate &dboList) 
{
	if(m_DbType==DBTYPE_FIREBIRD)
	{

		dboList["TR_F_USER_SESSIONS"] = "CREATE TRIGGER TR_F_USER_SESSIONS FOR F_USER_SESSIONS BEFORE INSERT OR UPDATE AS BEGIN \
IF(NEW.USR_SERVER_PORT IS NULL) THEN NEW.USR_SERVER_PORT=0;\
IF(NEW.USR_TIME_OFFSET IS NULL) THEN NEW.USR_TIME_OFFSET=0;\
IF(NEW.USR_SESSION_ID IS NULL) THEN NEW.USR_SESSION_ID='';\
IF(NEW.USR_PERS_CODE IS NULL) THEN NEW.USR_PERS_CODE='';\
IF(NEW.USR_SERIAL_ID IS NULL) THEN NEW.USR_SERIAL_ID='';\
IF(NEW.USR_SERVER_IP IS NULL) THEN NEW.USR_SERVER_IP='';\
IF(NEW.USR_GLOBAL_ID IS NULL) THEN NEW.USR_GLOBAL_ID='';\
IF(NEW.USR_MODIFIED_BY IS NULL) THEN NEW.USR_MODIFIED_BY='';\
END;";
	}
}
