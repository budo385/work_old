#ifndef DBCREATE_CORE_ACCESSRIGHTS_H
#define DBCREATE_CORE_ACCESSRIGHTS_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_CORE_ACCESSRIGHTS : public  DbSqlTableCreation
{
public:
	DbCreate_CORE_ACCESSRIGHTS():DbSqlTableCreation(CORE_ACCESSRIGHTS){};  //init to CORE_ACCESSRIGHTS table
	

private:
	//create generic Table def:
	void DefineFields(QStringList &lstSQL);

	//probably common to all DB's:
	void DefineIndexes(SQLDBObjectCreate &dboList);
};

#endif // DBCREATE_CORE_ACCESSRIGHTS_H
