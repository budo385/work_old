#include "dbcreate_bus_storedlist_items.h"

//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_STOREDLIST_ITEMS::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("											\
											create table BUS_STOREDLIST_ITEMS (			\
											BUSLI_ID INTEGER not null,					\
											BUSLI_GLOBAL_ID VARCHAR(15) null,				\
											BUSLI_DAT_LAST_MODIFIED TIMESTAMP not null,	\
											BUSLI_STLIST_ID INTEGER not null,			\
											BUSLI_STRDLIST_PARENT_ID INTEGER not null,	\
											BUSLI_TREETBL_ROWID INTEGER not null,		\
											BUSLI_ITEMSTATE INTEGER not null,			\
											BUSLI_STRDLIST_LEVEL INTEGER not null, constraint BUS_STOREDLIST_ITEMS_PK primary key (BUSLI_ID) )");


	m_lstDeletedCols<<"BUSLI_STRDLIST_SIBLING_ID";

}

//probably common to all DB's:
void DbCreate_BUS_STOREDLIST_ITEMS::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
	dboList["IND_STLIST_ID_ITEM"] = "create index IND_STLIST_ID_ITEM on BUS_STOREDLIST_ITEMS (BUSLI_STLIST_ID)";
	dboList["IND_TREETBL_ROWID"] = "create index IND_TREETBL_ROWID on BUS_STOREDLIST_ITEMS (BUSLI_TREETBL_ROWID)";
	dboList["IND_STLIST_SORT"] = "create index IND_STLIST_SORT on BUS_STOREDLIST_ITEMS (BUSLI_STRDLIST_LEVEL, BUSLI_STRDLIST_PARENT_ID)";
}
