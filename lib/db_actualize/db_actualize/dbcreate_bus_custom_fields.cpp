#include "dbcreate_bus_custom_fields.h"



//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_CUSTOM_FIELDS::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
											create table BUS_CUSTOM_FIELDS ( \
											BCF_ID INTEGER not null,\
											BCF_GLOBAL_ID VARCHAR(15) null,\
											BCF_DAT_LAST_MODIFIED TIMESTAMP not null,\
											BCF_TABLE_ID INTEGER not null,\
											BCF_NAME VARCHAR(300) not null,\
											BCF_DATA_TYPE SMALLINT not null,\
											BCF_SELECTION_TYPE SMALLINT not null,\
											BCF_SORT VARCHAR(20) null,\
											BCF_GROUP VARCHAR(100) null, constraint BUS_CUSTOM_FIELDS_PK primary key (BCF_ID) ) "); 

}

//probably common to all DB's:
void DbCreate_BUS_CUSTOM_FIELDS::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
	dboList["IND_BCF_TABLE_ID"] = "create index IND_BCF_TABLE_ID on BUS_CUSTOM_FIELDS (BCF_TABLE_ID)";  
	dboList["IND_BCF_TYPE"] = "create index IND_BCF_TYPE on BUS_CUSTOM_FIELDS (BCF_DATA_TYPE)";  
}


