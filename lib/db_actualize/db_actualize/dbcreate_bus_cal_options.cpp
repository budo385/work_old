#include "dbcreate_bus_cal_options.h"


//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_CAL_OPTIONS::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
								   create table BUS_CAL_OPTIONS (\
								   BCOL_ID INTEGER not null,\
								   BCOL_GLOBAL_ID VARCHAR(15) null,\
								   BCOL_DAT_LAST_MODIFIED TIMESTAMP not null,\
								   BCOL_EVENT_PART_ID INTEGER not null,\
								   BCOL_FROM DATETIME null,\
								   BCOL_TO DATETIME null,\
								   BCOL_IS_ACTIVE SMALLINT not null,\
								   BCOL_SUBJECT VARCHAR(250) null,\
								   BCOL_LOCATION VARCHAR(250) null,\
								   BCOL_DESCRIPTION LONGVARCHAR null, constraint BUS_CAL_OPTIONS_PK primary key (BCOL_ID) )     ");  


	lstSQL.append("\
								   alter table BUS_CAL_OPTIONS\
								   add constraint BUS_CAL_EV_PART_OPT_FK1 foreign key (\
								   BCOL_EVENT_PART_ID)\
								   references BUS_CAL_EVENT_PART (\
								   BCEP_ID) ON DELETE CASCADE 	");	
}



//probably common to all DB's:
void DbCreate_BUS_CAL_OPTIONS::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
}



