#include "dbcreate_core_ftp_servers.h"



//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_CORE_FTP_SERVERS::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
								   create table CORE_FTP_SERVERS ( \
								   CFTP_ID INTEGER not null,\
								   CFTP_GLOBAL_ID VARCHAR(15) null,\
								   CFTP_DAT_LAST_MODIFIED DATETIME null,\
								   CFTP_HOST VARCHAR(250) not null,\
								   CFTP_PORT INTEGER not null,\
								   CFTP_USER VARCHAR(250) not null,\
								   CFTP_PASSWORD VARCHAR(250) null,\
								   CFTP_NAME VARCHAR(250) not null,\
								   CFTP_START_DIRECTORY VARCHAR(250) null,\
								   CFTP_IS_PASSIVE_MODE TINYINT null,\
								   CFTP_USE_PROXY TINYINT null,\
								   CFTP_PROXY_HOST VARCHAR(250) null,\
								   CFTP_PROXY_PORT INTEGER null,\
								   CFTP_PROXY_TYPE TINYINT null,\
								   CFTP_PROXY_USER VARCHAR(250) null,\
								   CFTP_PROXY_PASSWORD VARCHAR(250) null, constraint CORE_FTP_SERVERS_PK primary key (CFTP_ID) )") ;


}



//probably common to all DB's:
void DbCreate_CORE_FTP_SERVERS::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
}

void DbCreate_CORE_FTP_SERVERS::DefineTriggers(SQLDBObjectCreate &dboList)
{
	//create triggers TOFIX: foe each DB, must be different
	dboList["TRG_CORE_FTP_SERVERS_DEL"]="\
									 CREATE TRIGGER TRG_CORE_FTP_SERVERS_DEL FOR CORE_FTP_SERVERS\
									 ACTIVE AFTER DELETE AS \
										 BEGIN \
										 UPDATE CORE_IMPORT_EXPORT SET CIE_USER_SOURCE_FTP_ID =NULL WHERE OLD.CFTP_ID = CIE_USER_SOURCE_FTP_ID;\
										 UPDATE CORE_IMPORT_EXPORT SET CIE_DEBT_SOURCE_FTP_ID =NULL WHERE OLD.CFTP_ID = CIE_DEBT_SOURCE_FTP_ID;\
										 UPDATE CORE_IMPORT_EXPORT SET CIE_CONT_SOURCE_FTP_ID =NULL WHERE OLD.CFTP_ID = CIE_CONT_SOURCE_FTP_ID;\
										 END\
										 ";
}

