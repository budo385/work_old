#ifndef DBCREATE_BUS_CM_ADDRESS_SCHEMAS_H
#define DBCREATE_BUS_CM_ADDRESS_SCHEMAS_H


#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_BUS_CM_ADDRESS_SCHEMAS : public DbSqlTableCreation
{
public:
	DbCreate_BUS_CM_ADDRESS_SCHEMAS():DbSqlTableCreation(BUS_CM_ADDRESS_SCHEMAS){};  //init table
	

private:
	//create generic Table def:
	void DefineFields(QStringList &lstSQL);

	//probably common to all DB's:
	void DefineIndexes(SQLDBObjectCreate &dboList);
};

#endif // DBCREATE_BUS_CM_ADDRESS_SCHEMAS_H
