#ifndef DBCREATE_F_AGENTS_H
#define DBCREATE_F_AGENTS_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_AGENTS : public DbSqlTableCreation
{
public: 
	DbCreate_F_AGENTS():DbSqlTableCreation(F_AGENTS){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_AGENTS_H
