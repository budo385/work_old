#ifndef DBCREATE_BUS_PERSON_H
#define DBCREATE_BUS_PERSON_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_BUS_PERSON : public DbSqlTableCreation
{
public:
	DbCreate_BUS_PERSON():DbSqlTableCreation(BUS_PERSON){};  //init to CORE_PERSON table
	

private:
	//create generic Table def:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);

	//probably common to all DB's:
	void DefineIndexes(SQLDBObjectCreate &dboList);
};

#endif // DBCREATE_BUS_PERSON_H
