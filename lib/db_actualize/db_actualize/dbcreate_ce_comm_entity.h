#ifndef DBCREATE_CE_COMM_ENTITY_H
#define DBCREATE_CE_COMM_ENTITY_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_CE_COMM_ENTITY : public DbSqlTableCreation
{
public:
	DbCreate_CE_COMM_ENTITY():DbSqlTableCreation(CE_COMM_ENTITY){};  //init table
	

private:
	//create generic Table def:
	void DefineFields(QStringList &lstSQL);

	//probably common to all DB's:
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineTriggers(SQLDBObjectCreate &dboList);
};
#endif // DBCREATE_CE_COMM_ENTITY_H
