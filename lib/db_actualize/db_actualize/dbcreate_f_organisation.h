#ifndef DBCREATE_F_ORGANISATION_H
#define DBCREATE_F_ORGANISATION_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_ORGANISATION : public DbSqlTableCreation
{
public: 
	DbCreate_F_ORGANISATION():DbSqlTableCreation(F_ORGANISATION){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_ORGANISATION_H
