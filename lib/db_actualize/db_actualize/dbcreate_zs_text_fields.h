#ifndef DBCREATE_ZS_TEXT_FIELDS_H
#define DBCREATE_ZS_TEXT_FIELDS_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_ZS_TEXT_FIELDS : public DbSqlTableCreation
{
public:
	DbCreate_ZS_TEXT_FIELDS():DbSqlTableCreation(ZS_TEXT_FIELDS){};  //init table
	

private:
	//create generic Table def:
	void DefineFields(QStringList &lstSQL);

	//probably common to all DB's:
	void DefineIndexes(SQLDBObjectCreate &dboList);
};
#endif // DBCREATE_ZS_TEXT_FIELDS_H
