#include "dbcreate_zs_binary_fields.h"



//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_ZS_BINARY_FIELDS::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
											create table ZS_BINARY_FIELDS ( \
											RID_BIN INTEGER not null,\
											TEXT LONGVARBINARY null, constraint ZS_BINARY_FIELDS_PK primary key (RID_BIN) )"); 
}



//probably common to all DB's:
void DbCreate_ZS_BINARY_FIELDS::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
}


