#ifndef DBCREATE_BUS_GROUP_TREE_H
#define DBCREATE_BUS_GROUP_TREE_H


#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_BUS_GROUP_TREE : public DbSqlTableCreation
{
public:
	DbCreate_BUS_GROUP_TREE():DbSqlTableCreation(BUS_GROUP_TREE){};  //init table
	

private:
	//create generic Table def:
	void DefineFields(QStringList &lstSQL);

	//probably common to all DB's:
	void DefineIndexes(SQLDBObjectCreate &dboList);
};

#endif // DBCREATE_BUS_GROUP_TREE_H
