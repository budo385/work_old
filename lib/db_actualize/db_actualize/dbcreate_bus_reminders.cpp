#include "dbcreate_bus_reminders.h"


//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_REMINDERS::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
								   create table BUS_REMINDERS ( \
								   BRE_ID INTEGER not null,\
								   BRE_GLOBAL_ID BINARY(10) null,\
								   BRE_DAT_LAST_MODIFIED TIMESTAMP not null,\
								   BRE_EVENT_TABLE_ID INTEGER null,\
								   BRE_EVENT_RECORD_ID INTEGER null,\
								   BRE_EVENT_DATA VARCHAR(100) null,\
								   BRE_SUBJECT VARCHAR(1000) not null,\
								   BRE_CATEGORY INTEGER not null,\
								   BRE_DAT_DUE DATETIME not null,\
								   BRE_DAT_SEND_REMIND DATETIME not null,\
								   BRE_DAT_EXPIRE DATETIME not null,\
								   BRE_ONE_SHOOT_REMIND SMALLINT not null,\
								   BRE_SC_NOTIFY_FAILED SMALLINT not null,\
								   BRE_OWNER_ID INTEGER not null,\
								   BRE_RECV_CONTACT_ID INTEGER not null, constraint BUS_REMINDERS_PK primary key (BRE_ID) )  ");  


	lstSQL.append("\
								   alter table BUS_REMINDERS\
								   add constraint BUS_CM_CONTACT_REM_FK1 foreign key (\
								   BRE_RECV_CONTACT_ID)\
								   references BUS_CM_CONTACT (\
								   BCNT_ID)  ");

	lstSQL.append("\
								   alter table BUS_REMINDERS\
								   add constraint BUS_PERSON_REM_FK1 foreign key (\
								   BRE_OWNER_ID)\
								   references BUS_PERSON (\
								   BPER_ID)    ");
}



//probably common to all DB's:
void DbCreate_BUS_REMINDERS::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
}



