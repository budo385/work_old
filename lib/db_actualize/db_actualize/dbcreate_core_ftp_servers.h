#ifndef DBCREATE_CORE_FTP_SERVERS_H
#define DBCREATE_CORE_FTP_SERVERS_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"

class DbCreate_CORE_FTP_SERVERS  : public DbSqlTableCreation 
{
public:
	DbCreate_CORE_FTP_SERVERS():DbSqlTableCreation(CORE_FTP_SERVERS){};  //init table

private:
	//create generic Table def:
	void DefineFields(QStringList &lstSQL);

	//probably common to all DB's:
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	
};

#endif // DBCREATE_CORE_FTP_SERVERS_H
