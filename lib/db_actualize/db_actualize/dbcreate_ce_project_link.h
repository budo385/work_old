#ifndef DBCREATE_CE_PROJECT_LINK_H
#define DBCREATE_CE_PROJECT_LINK_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_CE_PROJECT_LINK : public DbSqlTableCreation
{
public:
	DbCreate_CE_PROJECT_LINK():DbSqlTableCreation(CE_PROJECT_LINK){};  //init table
	

private:
	//create generic Table def:
	void DefineFields(QStringList &lstSQL);

	//probably common to all DB's:
	void DefineIndexes(SQLDBObjectCreate &dboList);
};
#endif // DBCREATE_CE_PROJECT_LINK_H
