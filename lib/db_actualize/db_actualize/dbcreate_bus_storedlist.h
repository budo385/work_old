#ifndef DBCREATE_BUS_STOREDLIST_H
#define DBCREATE_BUS_STOREDLIST_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_BUS_STOREDLIST : public DbSqlTableCreation
{
public:
    DbCreate_BUS_STOREDLIST():DbSqlTableCreation(BUS_STOREDLIST){};  //init to BUS_STOREDLIST table
	

private:
	//create generic Table def:
	void DefineFields(QStringList &lstSQL);

	//probably common to all DB's:
	void DefineIndexes(SQLDBObjectCreate &dboList);
};

#endif // DBCREATE_BUS_STOREDLIST_H
