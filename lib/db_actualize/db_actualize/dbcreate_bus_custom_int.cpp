#include "dbcreate_bus_custom_int.h"



//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_CUSTOM_INT::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
											create table BUS_CUSTOM_INT ( \
											BCI_ID INTEGER not null,\
											BCI_GLOBAL_ID VARCHAR(15) null,\
											BCI_DAT_LAST_MODIFIED TIMESTAMP not null,\
											BCI_CUSTOM_FIELD_ID INTEGER not null,\
											BCI_VALUE INTEGER not null,\
											BCI_TABLE_ID INTEGER not null,\
											BCI_RECORD_ID INTEGER not null, constraint BUS_CUSTOM_INT_PK primary key (BCI_ID) ) "); 
											
							lstSQL.append("\
											alter table BUS_CUSTOM_INT\
											add constraint BUS_CUSTOM_INT_FK1 foreign key (\
											BCI_CUSTOM_FIELD_ID)\
											references BUS_CUSTOM_FIELDS (\
											BCF_ID)  \
											ON DELETE CASCADE");

}

//probably common to all DB's:
void DbCreate_BUS_CUSTOM_INT::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
	dboList["IND_BCI_CI_TABLE_REC"] = "create index IND_BCI_CI_TABLE_REC on BUS_CUSTOM_INT (BCI_TABLE_ID,BCI_RECORD_ID)"; 
	dboList["IND_BCI_TABLE_VAL"] = "create index IND_BCI_TABLE_VAL on BUS_CUSTOM_INT (BCI_VALUE)"; 
	dboList["IND_BCI_CI_UNIQUE"] = "create unique index IND_BCI_CI_UNIQUE on BUS_CUSTOM_INT (BCI_TABLE_ID,BCI_RECORD_ID,BCI_CUSTOM_FIELD_ID)"; 

}


