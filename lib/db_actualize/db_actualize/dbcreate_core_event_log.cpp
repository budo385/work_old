#include "dbcreate_core_event_log.h"

//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------

void DbCreate_CORE_EVENT_LOG::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append(\
							"create table CORE_EVENT_LOG ( \
							CREL_ID INTEGER not null,\
							CREL_GLOBAL_ID VARCHAR(15) null,\
							CREL_DAT_LAST_MODIFIED TIMESTAMP not null,\
							CREL_EVENT_CODE INTEGER not null,\
							CREL_EVENT_TEXT LONGVARCHAR not null,\
							CREL_EVENT_TYPE INTEGER not null,\
							CREL_EVENT_CATEGORY_ID INTEGER not null,\
							CREL_EVENT_DATETIME DATETIME not null, constraint CORE_EVENT_LOG_PK primary key (CREL_ID) )");  


}

/* Probably index definition is same for all DB's */
void DbCreate_CORE_EVENT_LOG::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
	dboList["IND_EVENT_CAT_ID"]	  ="create index IND_EVENT_CAT_ID	on CORE_EVENT_LOG (CREL_EVENT_CATEGORY_ID)";
	dboList["IND_EVENT_TYPE"]	  ="create index IND_EVENT_TYPE		on CORE_EVENT_LOG (CREL_EVENT_TYPE)";
	dboList["IND_EVENT_DATETIME"] ="create index IND_EVENT_DATETIME on CORE_EVENT_LOG (CREL_EVENT_DATETIME)";
}

