#ifndef DBCREATE_F_TIMELINE_LONG_H
#define DBCREATE_F_TIMELINE_LONG_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_TIMELINE_LONG : public DbSqlTableCreation
{
public: 
	DbCreate_F_TIMELINE_LONG():DbSqlTableCreation(F_TIMELINE_LONG){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_TIMELINE_LONG_H
