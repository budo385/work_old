#ifndef DBCREATE_CORE_ACC_USER_REC_H
#define DBCREATE_CORE_ACC_USER_REC_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_CORE_ACC_USER_REC : public DbSqlTableCreation
{
public:
	DbCreate_CORE_ACC_USER_REC():DbSqlTableCreation(CORE_ACC_USER_REC){};  //init table
	

private:
	//create generic Table def:
	void DefineFields(QStringList &lstSQL);

	//probably common to all DB's:
	void DefineIndexes(SQLDBObjectCreate &dboList);
};

#endif // DBCREATE_BUS_CM_CONTACT_H
