#ifndef DBCREATE_F_WAEHRUNG_H
#define DBCREATE_F_WAEHRUNG_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_WAEHRUNG : public DbSqlTableCreation
{
public: 
	DbCreate_F_WAEHRUNG():DbSqlTableCreation(F_WAEHRUNG){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_WAEHRUNG_H
