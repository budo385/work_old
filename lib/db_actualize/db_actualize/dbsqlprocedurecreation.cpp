#include "dbsqlprocedurecreation.h"
#include "db_core/db_core/db_drivers.h"
#include "common/common/datahelper.h"

/*!
	Create table (from scratch) based on last version.

	\param pManager		- pointer to the Db Manager
	\param pDb			- pointer to the valid DbConnection 
	\param pDboManager	- pointer to the Db object manager

*/
void DbSqlProcedureCreation::InitDbSettings(DbSqlManager *pManager, QSqlDatabase *pDb,DbObjectManager *pDboManager )
{
	//set & test DB pointers
	m_pManager = pManager;
	m_pDb = pDb;
	m_DbObjectManager = pDboManager;
	m_DbType=pManager->GetDbType();

	//asserter
	Q_ASSERT_X(m_pDb!=NULL,"Init Create Table", "NULL pointers detected");
	Q_ASSERT_X(m_pManager!=NULL,"Init Create Table", "NULL pointers detected");


	//call init method:
	Init_Oracle();
	Init_MySQL();
	Init_FireBird();
	Init_GlobalTempTables();
}


/*!
	Creates Procedures, drop if exists

	\param pStatus &pStatus
*/
void DbSqlProcedureCreation::CreateProcedures(Status &pStatus, bool bCommunicatorProc)
{
	/*
		NOTE: 
		1. drop procedures first
		2. then recreate tables
		3. create procedures then...
	*/

	//Drop all procedures first to be able to drop tables that are used from procedures";
	//qDebug()<<"Drop all procedures";
	//DropAllProcedures(pStatus,bCommunicatorProc);
	//if(!pStatus.IsOK())
	//	return;
	
	QString strSQL,strName;
	pStatus.setError(0);

	//FIRST CREATE TEMP TABLES:

	qDebug()<<"Create global tables";

	if (bCommunicatorProc)
	{
		//check if DB is supported:
		if (!m_GlobalTempTables.contains(m_DbType))
		{pStatus.setError(StatusCodeSet::ERR_SQL_UNKNOWN_DBTYPE);return;}
		DbSqlQuery query(pStatus,m_pManager);
		SQLDBObjectCreatePIterator i(m_GlobalTempTables.value(m_DbType));
		while (i.hasNext()) 
		{
			i.next();
			strSQL=i.value();
			strName=i.key();
			qDebug()<<"Creating global temp table: "<<strName;
			m_DbObjectManager->DropIfExists_Table(pStatus,m_pDb,strName);
			if(!pStatus.IsOK())
				return;
			if (!query.Execute(pStatus,strSQL))	
				return;
		}

		//check if DB is supported:
		if (!m_GlobalTempTableTriggers.contains(m_DbType))
		{pStatus.setError(StatusCodeSet::ERR_SQL_UNKNOWN_DBTYPE);return;}
		//DbSqlQuery query(pStatus,m_pManager);
		SQLDBObjectCreatePIterator k(m_GlobalTempTableTriggers.value(m_DbType));
		while (k.hasNext()) 
		{
			k.next();
			strSQL=k.value();
			strName=k.key();
			qDebug()<<"Creating global temp table trigger: "<<strName;
			m_DbObjectManager->DropIfExists_Table(pStatus,m_pDb,strName);
			if(!pStatus.IsOK())
				return;
			if (!query.Execute(pStatus,strSQL))	
				return;
		}

	}
	else
	{
		//check if DB is supported:
		if (!m_GlobalTempTablesSPC.contains(m_DbType))
		{
			pStatus.setError(StatusCodeSet::ERR_SQL_UNKNOWN_DBTYPE);
			return;
		}
		DbSqlQuery query(pStatus,m_pManager);
		SQLDBObjectCreatePIterator i(m_GlobalTempTablesSPC.value(m_DbType));
		while (i.hasNext()) 
		{
			i.next();
			strSQL=i.value();
			strName=i.key();
			qDebug()<<"Creating global temp table: "<<strName;
			m_DbObjectManager->DropIfExists_Table(pStatus,m_pDb,strName);
			if(!pStatus.IsOK())
				return;
			if (!query.Execute(pStatus,strSQL))	
				return;	
		}

		//check if DB is supported:
		if (!m_GlobalTempTablesSPCTriggers.contains(m_DbType))
		{
			pStatus.setError(StatusCodeSet::ERR_SQL_UNKNOWN_DBTYPE);
			return;
		}
		//DbSqlQuery query(pStatus,m_pManager);
		SQLDBObjectCreatePIterator k(m_GlobalTempTablesSPCTriggers.value(m_DbType));
		while (k.hasNext()) 
		{
			k.next();
			strSQL=k.value();
			strName=k.key();
			qDebug()<<"Creating global temp table trigger: "<<strName;
			m_DbObjectManager->DropIfExists_Table(pStatus,m_pDb,strName);
			if(!pStatus.IsOK())
				return;
			if (!query.Execute(pStatus,strSQL))	
				return;	
		}

	}


	//PROCEDURES
	qDebug()<<"Create procedures";

	if (bCommunicatorProc)
	{
		//check if DB is supported:
		if (!m_Procedures.contains(m_DbType))
		{pStatus.setError(StatusCodeSet::ERR_SQL_UNKNOWN_DBTYPE);return;}
		DbSqlQuery query(pStatus,m_pManager);


		//procs
		QStringList lstProcNames;
		SQLDBObjectCreatePIterator i(m_Procedures.value(m_DbType));
		while (i.hasNext()) 
		{
			i.next();
			lstProcNames.append(i.key());
		}

		//sort asc:
		DataHelper::SortList(lstProcNames,true);
		int nSize=lstProcNames.size();
		for (int i=0;i<nSize;i++)
		{
			strName=lstProcNames.at(i).split(",").at(1).trimmed();
			strSQL = m_Procedures.value(m_DbType).value(lstProcNames.at(i));
			qDebug()<<"Creating procedure: "<<strName;
			m_DbObjectManager->DropIfExists_Proc(pStatus,m_pDb,strName);
			if(!pStatus.IsOK())
				return;
			if (!query.Execute(pStatus,strSQL))	
				return;
		}
	}
	else
	{
		//check if DB is supported:
		if (!m_ProceduresSPC.contains(m_DbType))
		{
			pStatus.setError(StatusCodeSet::ERR_SQL_UNKNOWN_DBTYPE);
			return;
		}
		DbSqlQuery query(pStatus,m_pManager);

		//procs
		QStringList lstProcNames;
		SQLDBObjectCreatePIterator i(m_ProceduresSPC.value(m_DbType));
		while (i.hasNext()) 
		{
			i.next();
			lstProcNames.append(i.key());
		}


		//sort asc:
		DataHelper::SortList(lstProcNames,true);
		int nSize=lstProcNames.size();
		for (int i=0;i<nSize;i++)
		{
			strName=lstProcNames.at(i).split(",").at(1).trimmed();
			strSQL = m_ProceduresSPC.value(m_DbType).value(lstProcNames.at(i));
			qDebug()<<"Creating procedure: "<<strName;
			m_DbObjectManager->DropIfExists_Proc(pStatus,m_pDb,strName);
			if(!pStatus.IsOK())
				return;
			if (!query.Execute(pStatus,strSQL))	
				return;
		}
	}
}

void DbSqlProcedureCreation::DropAllProcedures(Status &pStatus, bool bCommunicatorProc)
{
	QString strSQL,strName;
	pStatus.setError(0);

	if (bCommunicatorProc)
	{
		//check if DB is supported:
		if (!m_Procedures.contains(m_DbType))
		{pStatus.setError(StatusCodeSet::ERR_SQL_UNKNOWN_DBTYPE);return;}

		//drop triggers
		SQLDBObjectCreatePIterator k(m_GlobalTempTableTriggers.value(m_DbType));
		while (k.hasNext()) 
		{
			k.next();
			strName=k.key();
			qDebug()<<"Drop trigger: "<<strName;
			m_DbObjectManager->DropIfExists_Trigger(pStatus,m_pDb,strName);
			if(!pStatus.IsOK())
				return;
		}

		//procs
		QStringList lstProcNames;
		DbSqlQuery query(pStatus,m_pManager);
		SQLDBObjectCreatePIterator i(m_Procedures.value(m_DbType));
		while (i.hasNext()) 
		{
			i.next();
			lstProcNames.append(i.key());
		}

		//sort descending:
		DataHelper::SortList(lstProcNames,false);
		int nSize=lstProcNames.size();
		for (int i=0;i<nSize;i++)
		{
			strName=lstProcNames.at(i).split(",").at(1).trimmed();
			qDebug()<<"Drop procedure: "<<strName;
			m_DbObjectManager->DropIfExists_Proc(pStatus,m_pDb,strName);
			if(!pStatus.IsOK())
				return;
		}


		//drop global tables:
		SQLDBObjectCreatePIterator z(m_GlobalTempTables.value(m_DbType));
		while (z.hasNext()) 
		{
			z.next();
			strName=z.key();
			qDebug()<<"Drop global table: "<<strName;
			m_DbObjectManager->DropIfExists_Table(pStatus,m_pDb,strName);
			if(!pStatus.IsOK())
				return;
		}
	}
	else
	{
		//check if DB is supported:
		if (!m_ProceduresSPC.contains(m_DbType))
			{pStatus.setError(StatusCodeSet::ERR_SQL_UNKNOWN_DBTYPE);return;}

		//drop triggers
		SQLDBObjectCreatePIterator k(m_GlobalTempTablesSPCTriggers.value(m_DbType));
		while (k.hasNext()) 
		{
			k.next();
			strName=k.key();
			qDebug()<<"Drop trigger: "<<strName;
			m_DbObjectManager->DropIfExists_Trigger(pStatus,m_pDb,strName);
			if(!pStatus.IsOK())
				return;
		}

		
		//procs
		QStringList lstProcNames;
		DbSqlQuery query(pStatus,m_pManager);
		SQLDBObjectCreatePIterator i(m_ProceduresSPC.value(m_DbType));
		while (i.hasNext()) 
		{
			i.next();
			lstProcNames.append(i.key());
		}
		
		//sort desceding:
		DataHelper::SortList(lstProcNames,false);
		int nSize=lstProcNames.size();
		for (int i=0;i<nSize;i++)
		{
			strName=lstProcNames.at(i).split(",").at(1).trimmed();
			qDebug()<<"Drop procedure: "<<strName;
			m_DbObjectManager->DropIfExists_Proc(pStatus,m_pDb,strName);
			if(!pStatus.IsOK())
				return;
		}

		//drop global tables:
		SQLDBObjectCreatePIterator z(m_GlobalTempTablesSPC.value(m_DbType));
		while (z.hasNext()) 
		{
			z.next();
			strName=z.key();
			qDebug()<<"Drop global table: "<<strName;
			m_DbObjectManager->DropIfExists_Table(pStatus,m_pDb,strName);
			if(!pStatus.IsOK())
				return;
		}
	}

}



QStringList	DbSqlProcedureCreation::GetProcedures()
{		
	QStringList lstProcNames;
	{
		SQLDBObjectCreatePIterator i(m_Procedures.value(m_DbType));
		while (i.hasNext()) 
		{
			i.next();
			QString strName=i.key().split(",").at(1).trimmed();
			lstProcNames.append(strName);
		}
	}
	{
		SQLDBObjectCreatePIterator i(m_ProceduresSPC.value(m_DbType));
		while (i.hasNext()) 
		{
			i.next();
			QString strName=i.key().split(",").at(1).trimmed();
			lstProcNames.append(strName);
		}
	}
	return lstProcNames;
}

QStringList	DbSqlProcedureCreation::GetGlobalTables()
{
	QStringList lstNames;
	{
		SQLDBObjectCreatePIterator i(m_GlobalTempTables.value(m_DbType));
		while (i.hasNext()) 
		{
			i.next();
			lstNames.append(i.key());
		}
	}
	{
		SQLDBObjectCreatePIterator i(m_GlobalTempTablesSPC.value(m_DbType));
		while (i.hasNext()) 
		{
			i.next();
			lstNames.append(i.key());
		}
	}
	return lstNames;

}


//---------------------------------------------------
//					ORACLE
//---------------------------------------------------

void DbSqlProcedureCreation::Init_Oracle()
{

	SQLDBObjectCreateP dboList;
/*
	dboList["Funct_1"]="\
		create Procedure Funct_1(\
		xxxyyy,\
		bzzzzzzzz)";  

	dboList["Funct_2"]="\
		create Procedure Funct_2 on CORE_LOCKING (\
		CORL_TABLE_ID,\
		CORL_RECORD_ID)";  

*/
	m_Procedures[DBTYPE_ORACLE]=dboList;

}



//---------------------------------------------------
//					MYSQL
//---------------------------------------------------

void DbSqlProcedureCreation::Init_MySQL()
{


	SQLDBObjectCreateP dboList;

/*
	dboList["Funct_1"]="\
		create Procedure Funct_1(\
		xxxyyy,\
		bzzzzzzzz)";  

	dboList["Funct_2"]="\
		create unique index Funct_2 on CORE_LOCKING (\
		CORL_TABLE_ID,\
		CORL_RECORD_ID)";  

*/
	m_Procedures[DBTYPE_MYSQL]=dboList;

}


void DbSqlProcedureCreation::Init_GlobalTempTables()
{
	SQLDBObjectCreateP dboList;
	m_GlobalTempTables[DBTYPE_FIREBIRD]=dboList;


	dboList.clear();
	dboList["TEMP_BUDGET_CORRECTION"]="CREATE GLOBAL TEMPORARY TABLE TEMP_BUDGET_CORRECTION\
									  (\
									  PROJ_CODE				VARCHAR(100),\
									  DATE_FROM				TIMESTAMP,\
									  DATE_TO				TIMESTAMP,\
									  NEW					INTEGER,\
									  TB_B_BUD_H            DECIMAL(15,2),\
									  TB_B_BUD_NH           DECIMAL(15,2),\
									  TB_B_BUD_FH           DECIMAL(15,2),\
									  TB_B_BUD_V            DECIMAL(15,2),\
									  TB_B_BUD_N            DECIMAL(15,2),\
									  TB_B_BUD_A 	        DECIMAL(15,2),\
									  TB_B_BUD_F 	        DECIMAL(15,2),\
									  TB_B_BUD_Z            DECIMAL(15,2)\
									  ) ON COMMIT PRESERVE ROWS";

	dboList["TEMP_REST_CORRECTION"]="CREATE GLOBAL TEMPORARY TABLE TEMP_REST_CORRECTION\
									(\
									PROJ_CODE				VARCHAR(100),\
									DATE_FROM				TIMESTAMP,\
									DATE_TO				TIMESTAMP,\
									NEW					INTEGER,\
									TB_B_KORR_H           DECIMAL(15,2),\
									TB_B_KORR_V           DECIMAL(15,2),\
									TB_B_KORR_N           DECIMAL(15,2),\
									TB_B_KORR_F           DECIMAL(15,2),\
									TB_B_KORR_A           DECIMAL(15,2),\
									TB_B_KORR_Z 	        DECIMAL(15,2),\
									TB_B_KORR_VN 	        DECIMAL(15,2),\
									TB_B_KORR_VF          DECIMAL(15,2),\
									TB_N_RK 	    		DECIMAL(15,2)\
									) ON COMMIT PRESERVE ROWS";

	dboList["TEMP_LST_PROJECTS"]="CREATE GLOBAL TEMPORARY TABLE TEMP_LST_PROJECTS\
								 (\
								 PROJ_CODE				VARCHAR(100)\
								 ) ON COMMIT PRESERVE ROWS";

	dboList["TEMP_CHARGE_CUMULATION"]="CREATE GLOBAL TEMPORARY TABLE TEMP_CHARGE_CUMULATION\
									  (\
									  PROJ_CODE				VARCHAR(100),\
									  DATE_FROM				TIMESTAMP,\
									  DATE_TO				TIMESTAMP,\
									  NEW					INTEGER,\
									  T_KUM_ZEIT_ZT         DECIMAL(15,2),\
									  T_KUM_ZEIT_KT         DECIMAL(15,2),\
									  T_KUM_ZEIT            DECIMAL(15,2),\
									  T_KUM_HON_ZT          DECIMAL(15,2),\
									  T_KUM_HON_KT          DECIMAL(15,2),\
									  T_KUM_HON 	        DECIMAL(15,2),\
									  T_KUM_NEBEN 	        DECIMAL(15,2),\
									  T_KUM_AUFW_ZT         DECIMAL(15,2),\
									  T_KUM_AUFW_KT 	    DECIMAL(15,2),\
									  T_KUM_AUFW_HON        DECIMAL(15,2),\
									  T_KUM_AUFW_NEBEN 	    DECIMAL(15,2),\
									  T_KUM_AUFG_FR_KOST 	DECIMAL(15,2),\
									  T_KUM_AUFW_FR_KOST 	DECIMAL(15,2),\
									  T_KUM_AUFG_FR_LEIST 	DECIMAL(15,2),\
									  T_KUM_AUFW_FR_LEIST 	DECIMAL(15,2),\
									  T_KUM_ZEIT_FR_LEIST 	DECIMAL(15,2),\
									  T_KUM_ZT_UNFAKT 	    DECIMAL(15,2),\
									  T_KUM_KT_UNFAKT       DECIMAL(15,2),\
									  T_KUM_NK_UNFAKT       DECIMAL(15,2),\
									  T_KUM_AUFG_HON_NF     DECIMAL(15,2),\
									  T_KUM_AUFG_NK_NF      DECIMAL(15,2),\
									  T_KUM_AUFG_FK_NF      DECIMAL(15,2),\
									  T_KUM_AUFW_HON_NF     DECIMAL(15,2),\
									  T_KUM_AUFW_NK_NF      DECIMAL(15,2),\
									  T_KUM_AUFW_FK_NF      DECIMAL(15,2),\
									  T_KUM_AUFG_ZEIT_NF    DECIMAL(15,2),\
									  T_KUM_AUFG_SPES       DECIMAL(15,2),\
									  T_KUM_AUFW_SPES       DECIMAL(15,2),\
									  T_KUM_ARB_AUFG_FAKT   DECIMAL(15,2),\
									  T_KUM_ARB_AUFW_FAKT   DECIMAL(15,2),\
									  T_KUM_NK_AUFG_FAKT   DECIMAL(15,2),\
									  T_KUM_NK_AUFW_FAKT   DECIMAL(15,2),\
									  T_KUM_FK_AUFG_FAKT   DECIMAL(15,2),\
									  T_KUM_FK_AUFW_FAKT   DECIMAL(15,2)\
									  ) ON COMMIT PRESERVE ROWS";

	dboList["TEMP_FAKTURA_CUMULATION"]="CREATE GLOBAL TEMPORARY TABLE TEMP_FAKTURA_CUMULATION\
									   (\
									   PROJ_CODE			VARCHAR(100),\
									   DATE_FROM			TIMESTAMP,\
									   DATE_TO			TIMESTAMP,\
									   NEW				INTEGER,\
									   T_KUM_F_HONORAR   DECIMAL(15,2),\
									   T_KUM_F_NEBENKOST DECIMAL(15,2),\
									   T_KUM_F_ZUSCHLAG  DECIMAL(15,2),\
									   T_KUM_F_RABATT    DECIMAL(15,2),\
									   T_KUM_F_RAB_ARB   DECIMAL(15,2),\
									   T_KUM_F_RAB_NK 	DECIMAL(15,2),\
									   T_KUM_F_MWST 	    DECIMAL(15,2),\
									   T_KUM_F_BETRAG    DECIMAL(15,2),\
									   T_KUM_F_BET_MWST0 DECIMAL(15,2),\
									   T_KUM_F_BET_MWSTA DECIMAL(15,2),\
									   T_KUM_F_BET_MWSTB DECIMAL(15,2),\
									   T_KUM_F_OFFEN 	DECIMAL(15,2),\
									   T_KUM_F_FK 		DECIMAL(15,2),\
									   T_KUM_F_NK_O_FK 	DECIMAL(15,2),\
									   T_KUM_F_EXT_HON 	DECIMAL(15,2),\
									   T_KUM_F_EXT_NK 	DECIMAL(15,2)\
									   ) ON COMMIT PRESERVE ROWS";

	dboList["TEMP_EXP_CUMUL_PROJ_EXPTYPE"]="CREATE GLOBAL TEMPORARY TABLE TEMP_EXP_CUMUL_PROJ_EXPTYPE\
										   (\
										   PROJ_CODE					VARCHAR(100),\
										   DATE_FROM					TIMESTAMP,\
										   DATE_TO					TIMESTAMP,\
										   NEW						INTEGER,\
										   T_NK_AUFG_ANZ_KUM   		DECIMAL(15,2),\
										   T_NK_AUFG_ANZ_UNKUM 		DECIMAL(15,2),\
										   T_NK_AUFG_INT_KUM  		DECIMAL(15,2),\
										   T_NK_AUFG_EXT_KUM   	 	DECIMAL(15,2),\
										   T_NK_AUFG_INT_UNKUM   	DECIMAL(15,2),\
										   T_NK_AUFG_EXT_UNKUM 		DECIMAL(15,2),\
										   T_NK_AUFG_INT_UNKUM_FK 	DECIMAL(15,2),\
										   T_NK_AUFG_EXT_UNKUM_FK    DECIMAL(15,2)\
										   ) ON COMMIT PRESERVE ROWS";

	dboList["TEMP_PAYMENT_CUMULATION"]="CREATE GLOBAL TEMPORARY TABLE TEMP_PAYMENT_CUMULATION\
									   (\
									   PROJ_CODE				VARCHAR(100),\
									   DATE_FROM				TIMESTAMP,\
									   DATE_TO				TIMESTAMP,\
									   NEW					INTEGER,\
									   T_KUM_Z_BETRAG   		DECIMAL(15,2),\
									   T_KUM_Z_SKONTO 		DECIMAL(15,2),\
									   T_KUM_Z_BET_MWSTA  	DECIMAL(15,2),\
									   T_KUM_Z_BET_MWSTB   	DECIMAL(15,2),\
									   T_KUM_Z_TOTAL   		DECIMAL(15,2)\
									   ) ON COMMIT PRESERVE ROWS";

	dboList["TEMP_LEVY_CUMULATION"]="CREATE GLOBAL TEMPORARY TABLE TEMP_LEVY_CUMULATION\
									(\
									PROJ_CODE					VARCHAR(100),\
									DATE_FROM					TIMESTAMP,\
									DATE_TO					TIMESTAMP,\
									NEW						INTEGER,\
									T_UML_FAKT_BEL_HON   		DECIMAL(15,2),\
									T_UML_FAKT_BEL_NK 		DECIMAL(15,2),\
									T_UML_FAKT_ENT_HON  		DECIMAL(15,2),\
									T_UML_FAKT_ENT_NK   		DECIMAL(15,2),\
									T_UML_FAKT_FKT_HON   		DECIMAL(15,2),\
									T_UML_FAKT_FKT_NK   		DECIMAL(15,2)\
									) ON COMMIT PRESERVE ROWS";

	dboList["TEMP_BF_PROJ_PERS_PERIOD_SER"]="CREATE GLOBAL TEMPORARY TABLE TEMP_BF_PROJ_PERS_PERIOD_SER\
											(\
											T_D1					TIMESTAMP,\
											T_D2					TIMESTAMP,\
											T_KUM_ZEIT   			DECIMAL(15,2),\
											T_KUM_ZEIT_TOTAL 		DECIMAL(15,2),\
											T_KUM_HON  			DECIMAL(15,2),\
											T_KUM_AUFW_HON   		DECIMAL(15,2),\
											T_KUM_AUFG_SPES   	DECIMAL(15,2),\
											T_KUM_AUFW_SPES   	DECIMAL(15,2)\
											) ON COMMIT PRESERVE ROWS";

	dboList["TEMP_CALC_WORK_CHARGE_RATES"]="CREATE GLOBAL TEMPORARY TABLE TEMP_CALC_WORK_CHARGE_RATES\
										   (\
										   RATE					DECIMAL(15,4),\
										   TIME_UNIT				DECIMAL(15,2),\
										   RATE_TYPE				DECIMAL(15,2),\
										   RATE_CAT				VARCHAR(30),\
										   FEE_CLASS				VARCHAR(30),\
										   RATE_SHORTCUT			VARCHAR(30),\
										   FIXED					DECIMAL(15,2)\
										   ) ON COMMIT PRESERVE ROWS";

	dboList["TEMP_CALC_WORK_CHARGE_RATES_1"]="CREATE GLOBAL TEMPORARY TABLE TEMP_CALC_WORK_CHARGE_RATES_1\
											 (\
											 ETR_CODE					VARCHAR(74),\
											 ETR_CODE_PROJ				VARCHAR(50),\
											 ETR_CODE_FUNK				VARCHAR(12),\
											 ETR_CODE_PERS				VARCHAR(12),\
											 ETR_TARIF_KOST			DECIMAL(15,2),\
											 ETR_TARIF_ZEIT			DECIMAL(15,2),\
											 ETR_TARIF_AUFW			DECIMAL(15,2),\
											 ETR_KOST_NULL				DECIMAL(15,2),\
											 ETR_ZEIT_NULL				DECIMAL(15,2),\
											 ETR_AUFW_NULL				DECIMAL(15,2),\
											 RT_SEARCH_CODE			DECIMAL(15,2),\
											 RT_SORT_CODE_EXT			DECIMAL(15,2),\
											 RT_SORT_CODE_INT			DECIMAL(15,2)\
											 ) ON COMMIT PRESERVE ROWS";







	
	m_GlobalTempTablesSPC[DBTYPE_FIREBIRD]=dboList;
	dboList.clear();
	m_GlobalTempTableTriggers[DBTYPE_FIREBIRD]=dboList;

	dboList["TR_TEMP_BUDGET_CORRECTION"]="CREATE TRIGGER TR_TEMP_BUDGET_CORRECTION FOR TEMP_BUDGET_CORRECTION BEFORE INSERT OR UPDATE AS BEGIN\
										 IF(NEW.NEW IS NULL) THEN NEW.NEW=0;\
										 IF(NEW.TB_B_BUD_H IS NULL) THEN NEW.TB_B_BUD_H=0;\
										 IF(NEW.TB_B_BUD_NH IS NULL) THEN NEW.TB_B_BUD_NH=0;\
										 IF(NEW.TB_B_BUD_FH IS NULL) THEN NEW.TB_B_BUD_FH=0;\
										 IF(NEW.TB_B_BUD_V IS NULL) THEN NEW.TB_B_BUD_V=0;\
										 IF(NEW.TB_B_BUD_N IS NULL) THEN NEW.TB_B_BUD_N=0;\
										 IF(NEW.TB_B_BUD_A IS NULL) THEN NEW.TB_B_BUD_A=0;\
										 IF(NEW.TB_B_BUD_F IS NULL) THEN NEW.TB_B_BUD_F=0;\
										 IF(NEW.TB_B_BUD_Z IS NULL) THEN NEW.TB_B_BUD_Z=0;\
										 END;";

	dboList["TR_TEMP_REST_CORRECTION"]="CREATE TRIGGER TR_TEMP_REST_CORRECTION FOR TEMP_REST_CORRECTION BEFORE INSERT OR UPDATE AS BEGIN\
									   IF(NEW.NEW IS NULL) THEN NEW.NEW=0;\
									   IF(NEW.TB_B_KORR_H IS NULL) THEN NEW.TB_B_KORR_H=0;\
									   IF(NEW.TB_B_KORR_V IS NULL) THEN NEW.TB_B_KORR_V=0;\
									   IF(NEW.TB_B_KORR_N IS NULL) THEN NEW.TB_B_KORR_N=0;\
									   IF(NEW.TB_B_KORR_F IS NULL) THEN NEW.TB_B_KORR_F=0;\
									   IF(NEW.TB_B_KORR_A IS NULL) THEN NEW.TB_B_KORR_A=0;\
									   IF(NEW.TB_B_KORR_Z IS NULL) THEN NEW.TB_B_KORR_Z=0;\
									   IF(NEW.TB_B_KORR_VN IS NULL) THEN NEW.TB_B_KORR_VN=0;\
									   IF(NEW.TB_B_KORR_VF IS NULL) THEN NEW.TB_B_KORR_VF=0;\
									   IF(NEW.TB_N_RK IS NULL) THEN NEW.TB_N_RK=0;\
									   END;";

	dboList["TR_TEMP_CHARGE_CUMULATION"]="CREATE TRIGGER TR_TEMP_CHARGE_CUMULATION FOR TEMP_CHARGE_CUMULATION BEFORE INSERT OR UPDATE AS BEGIN\
										 IF(NEW.NEW IS NULL) THEN NEW.NEW=0;\
										 IF(NEW.T_KUM_ZEIT_ZT IS NULL) THEN NEW.T_KUM_ZEIT_ZT=0;\
										 IF(NEW.T_KUM_ZEIT_KT IS NULL) THEN NEW.T_KUM_ZEIT_KT=0;\
										 IF(NEW.T_KUM_ZEIT IS NULL) THEN NEW.T_KUM_ZEIT=0;\
										 IF(NEW.T_KUM_HON_ZT IS NULL) THEN NEW.T_KUM_HON_ZT=0;\
										 IF(NEW.T_KUM_HON_KT IS NULL) THEN NEW.T_KUM_HON_KT=0;\
										 IF(NEW.T_KUM_HON IS NULL) THEN NEW.T_KUM_HON=0;\
										 IF(NEW.T_KUM_NEBEN IS NULL) THEN NEW.T_KUM_NEBEN=0;\
										 IF(NEW.T_KUM_AUFW_ZT IS NULL) THEN NEW.T_KUM_AUFW_ZT=0;\
										 IF(NEW.T_KUM_AUFW_KT IS NULL) THEN NEW.T_KUM_AUFW_KT=0;\
										 IF(NEW.T_KUM_AUFW_HON IS NULL) THEN NEW.T_KUM_AUFW_HON=0;\
										 IF(NEW.T_KUM_AUFW_NEBEN IS NULL) THEN NEW.T_KUM_AUFW_NEBEN=0;\
										 IF(NEW.T_KUM_AUFG_FR_KOST IS NULL) THEN NEW.T_KUM_AUFG_FR_KOST=0;\
										 IF(NEW.T_KUM_AUFW_FR_KOST IS NULL) THEN NEW.T_KUM_AUFW_FR_KOST=0;\
										 IF(NEW.T_KUM_AUFG_FR_LEIST IS NULL) THEN NEW.T_KUM_AUFG_FR_LEIST=0;\
										 IF(NEW.T_KUM_AUFW_FR_LEIST IS NULL) THEN NEW.T_KUM_AUFW_FR_LEIST=0;\
										 IF(NEW.T_KUM_ZEIT_FR_LEIST IS NULL) THEN NEW.T_KUM_ZEIT_FR_LEIST=0;\
										 IF(NEW.T_KUM_ZT_UNFAKT IS NULL) THEN NEW.T_KUM_ZT_UNFAKT=0;\
										 IF(NEW.T_KUM_KT_UNFAKT IS NULL) THEN NEW.T_KUM_KT_UNFAKT=0;\
										 IF(NEW.T_KUM_NK_UNFAKT IS NULL) THEN NEW.T_KUM_NK_UNFAKT=0;\
										 IF(NEW.T_KUM_AUFG_HON_NF IS NULL) THEN NEW.T_KUM_AUFG_HON_NF=0;\
										 IF(NEW.T_KUM_AUFG_NK_NF IS NULL) THEN NEW.T_KUM_AUFG_NK_NF=0;\
										 IF(NEW.T_KUM_AUFG_FK_NF IS NULL) THEN NEW.T_KUM_AUFG_FK_NF=0;\
										 IF(NEW.T_KUM_AUFW_HON_NF IS NULL) THEN NEW.T_KUM_AUFW_HON_NF=0;\
										 IF(NEW.T_KUM_AUFW_NK_NF IS NULL) THEN NEW.T_KUM_AUFW_NK_NF=0;\
										 IF(NEW.T_KUM_AUFW_FK_NF IS NULL) THEN NEW.T_KUM_AUFW_FK_NF=0;\
										 IF(NEW.T_KUM_AUFG_ZEIT_NF IS NULL) THEN NEW.T_KUM_AUFG_ZEIT_NF=0;\
										 IF(NEW.T_KUM_AUFG_SPES IS NULL) THEN NEW.T_KUM_AUFG_SPES=0;\
										 IF(NEW.T_KUM_AUFW_SPES IS NULL) THEN NEW.T_KUM_AUFW_SPES=0;\
										 IF(NEW.T_KUM_ARB_AUFG_FAKT IS NULL) THEN NEW.T_KUM_ARB_AUFG_FAKT=0;\
										 IF(NEW.T_KUM_ARB_AUFW_FAKT IS NULL) THEN NEW.T_KUM_ARB_AUFW_FAKT=0;\
										 IF(NEW.T_KUM_NK_AUFG_FAKT IS NULL) THEN NEW.T_KUM_NK_AUFG_FAKT=0;\
										 IF(NEW.T_KUM_NK_AUFW_FAKT IS NULL) THEN NEW.T_KUM_NK_AUFW_FAKT=0;\
										 IF(NEW.T_KUM_FK_AUFG_FAKT IS NULL) THEN NEW.T_KUM_FK_AUFG_FAKT=0;\
										 IF(NEW.T_KUM_FK_AUFW_FAKT IS NULL) THEN NEW.T_KUM_FK_AUFW_FAKT=0;\
										 END;";

	dboList["TR_TEMP_FAKTURA_CUMULATION"]="CREATE TRIGGER TR_TEMP_FAKTURA_CUMULATION FOR TEMP_FAKTURA_CUMULATION BEFORE INSERT OR UPDATE AS BEGIN\
										  IF(NEW.NEW IS NULL) THEN NEW.NEW=0;\
										  IF(NEW.T_KUM_F_HONORAR IS NULL) THEN NEW.T_KUM_F_HONORAR=0;\
										  IF(NEW.T_KUM_F_NEBENKOST IS NULL) THEN NEW.T_KUM_F_NEBENKOST=0;\
										  IF(NEW.T_KUM_F_ZUSCHLAG IS NULL) THEN NEW.T_KUM_F_ZUSCHLAG=0;\
										  IF(NEW.T_KUM_F_RABATT IS NULL) THEN NEW.T_KUM_F_RABATT=0;\
										  IF(NEW.T_KUM_F_RAB_ARB IS NULL) THEN NEW.T_KUM_F_RAB_ARB=0;\
										  IF(NEW.T_KUM_F_RAB_NK IS NULL) THEN NEW.T_KUM_F_RAB_NK=0;\
										  IF(NEW.T_KUM_F_MWST IS NULL) THEN NEW.T_KUM_F_MWST=0;\
										  IF(NEW.T_KUM_F_BETRAG IS NULL) THEN NEW.T_KUM_F_BETRAG=0;\
										  IF(NEW.T_KUM_F_BET_MWST0 IS NULL) THEN NEW.T_KUM_F_BET_MWST0=0;\
										  IF(NEW.T_KUM_F_BET_MWSTA IS NULL) THEN NEW.T_KUM_F_BET_MWSTA=0;\
										  IF(NEW.T_KUM_F_BET_MWSTB IS NULL) THEN NEW.T_KUM_F_BET_MWSTB=0;\
										  IF(NEW.T_KUM_F_OFFEN IS NULL) THEN NEW.T_KUM_F_OFFEN=0;\
										  IF(NEW.T_KUM_F_FK IS NULL) THEN NEW.T_KUM_F_FK=0;\
										  IF(NEW.T_KUM_F_NK_O_FK IS NULL) THEN NEW.T_KUM_F_NK_O_FK=0;\
										  IF(NEW.T_KUM_F_EXT_HON IS NULL) THEN NEW.T_KUM_F_EXT_HON=0;\
										  IF(NEW.T_KUM_F_EXT_NK IS NULL) THEN NEW.T_KUM_F_EXT_NK=0;\
										  END;";

	dboList["TR_TEMP_EXP_CUMUL_PROJ_EXPTYPE"]="CREATE TRIGGER TR_TEMP_EXP_CUMUL_PROJ_EXPTYPE FOR TEMP_EXP_CUMUL_PROJ_EXPTYPE BEFORE INSERT OR UPDATE AS BEGIN\
											  IF(NEW.NEW IS NULL) THEN NEW.NEW=0;\
											  IF(NEW.T_NK_AUFG_ANZ_KUM IS NULL) THEN NEW.T_NK_AUFG_ANZ_KUM=0;\
											  IF(NEW.T_NK_AUFG_ANZ_UNKUM IS NULL) THEN NEW.T_NK_AUFG_ANZ_UNKUM=0;\
											  IF(NEW.T_NK_AUFG_INT_KUM IS NULL) THEN NEW.T_NK_AUFG_INT_KUM=0;\
											  IF(NEW.T_NK_AUFG_EXT_KUM IS NULL) THEN NEW.T_NK_AUFG_EXT_KUM=0;\
											  IF(NEW.T_NK_AUFG_INT_UNKUM IS NULL) THEN NEW.T_NK_AUFG_INT_UNKUM=0;\
											  IF(NEW.T_NK_AUFG_EXT_UNKUM IS NULL) THEN NEW.T_NK_AUFG_EXT_UNKUM=0;\
											  IF(NEW.T_NK_AUFG_INT_UNKUM_FK IS NULL) THEN NEW.T_NK_AUFG_INT_UNKUM_FK=0;\
											  IF(NEW.T_NK_AUFG_EXT_UNKUM_FK IS NULL) THEN NEW.T_NK_AUFG_EXT_UNKUM_FK=0;\
											  END;";

	dboList["TR_TEMP_PAYMENT_CUMULATION"]="CREATE TRIGGER TR_TEMP_PAYMENT_CUMULATION FOR TEMP_PAYMENT_CUMULATION BEFORE INSERT OR UPDATE AS BEGIN\
										  IF(NEW.NEW IS NULL) THEN NEW.NEW=0;\
										  IF(NEW.T_KUM_Z_BETRAG IS NULL) THEN NEW.T_KUM_Z_BETRAG=0;\
										  IF(NEW.T_KUM_Z_SKONTO IS NULL) THEN NEW.T_KUM_Z_SKONTO=0;\
										  IF(NEW.T_KUM_Z_BET_MWSTA IS NULL) THEN NEW.T_KUM_Z_BET_MWSTA=0;\
										  IF(NEW.T_KUM_Z_BET_MWSTB IS NULL) THEN NEW.T_KUM_Z_BET_MWSTB=0;\
										  IF(NEW.T_KUM_Z_TOTAL IS NULL) THEN NEW.T_KUM_Z_TOTAL=0;\
										  END;";

	dboList["TR_TEMP_LEVY_CUMULATION"]="CREATE TRIGGER TR_TEMP_LEVY_CUMULATION FOR TEMP_LEVY_CUMULATION BEFORE INSERT OR UPDATE AS BEGIN\
									   IF(NEW.NEW IS NULL) THEN NEW.NEW=0;\
									   IF(NEW.T_UML_FAKT_BEL_HON IS NULL) THEN NEW.T_UML_FAKT_BEL_HON=0;\
									   IF(NEW.T_UML_FAKT_BEL_NK IS NULL) THEN NEW.T_UML_FAKT_BEL_NK=0;\
									   IF(NEW.T_UML_FAKT_ENT_HON IS NULL) THEN NEW.T_UML_FAKT_ENT_HON=0;\
									   IF(NEW.T_UML_FAKT_ENT_NK IS NULL) THEN NEW.T_UML_FAKT_ENT_NK=0;\
									   IF(NEW.T_UML_FAKT_FKT_HON IS NULL) THEN NEW.T_UML_FAKT_FKT_HON=0;\
									   IF(NEW.T_UML_FAKT_FKT_NK IS NULL) THEN NEW.T_UML_FAKT_FKT_NK=0;\
									   END;";

	dboList["TR_TEMP_BF_PROJ_PERS_PERIOD_SER"]="CREATE TRIGGER TR_TEMP_BF_PROJ_PERS_PERIOD_SER FOR TEMP_BF_PROJ_PERS_PERIOD_SER BEFORE INSERT OR UPDATE AS BEGIN\
											   IF(NEW.T_KUM_ZEIT IS NULL) THEN NEW.T_KUM_ZEIT=0;\
											   IF(NEW.T_KUM_ZEIT_TOTAL IS NULL) THEN NEW.T_KUM_ZEIT_TOTAL=0;\
											   IF(NEW.T_KUM_HON IS NULL) THEN NEW.T_KUM_HON=0;\
											   IF(NEW.T_KUM_AUFW_HON IS NULL) THEN NEW.T_KUM_AUFW_HON=0;\
											   IF(NEW.T_KUM_AUFG_SPES IS NULL) THEN NEW.T_KUM_AUFG_SPES=0;\
											   IF(NEW.T_KUM_AUFW_SPES IS NULL) THEN NEW.T_KUM_AUFW_SPES=0;\
											   END;";

	dboList["TR_TEMP_CALC_WORK_CHARGE_RATES"]="CREATE TRIGGER TR_TEMP_CALC_WORK_CHARGE_RATES FOR TEMP_CALC_WORK_CHARGE_RATES BEFORE INSERT OR UPDATE AS BEGIN\
											  IF(NEW.RATE IS NULL) THEN NEW.RATE=0;\
											  IF(NEW.TIME_UNIT IS NULL) THEN NEW.TIME_UNIT=0;\
											  IF(NEW.RATE_TYPE IS NULL) THEN NEW.RATE_TYPE=0;\
											  IF(NEW.RATE_CAT IS NULL) THEN NEW.RATE_CAT='';\
											  IF(NEW.FEE_CLASS IS NULL) THEN NEW.FEE_CLASS='';\
											  IF(NEW.RATE_SHORTCUT IS NULL) THEN NEW.RATE_SHORTCUT='';\
											  IF(NEW.FIXED IS NULL) THEN NEW.FIXED=0;\
											  END;";

	dboList["TR_TEMP_CALC_WORK_CHARGE_RATES"]="CREATE TRIGGER TR_TEMP_CALC_WORK_CHARGE_RATES FOR TEMP_CALC_WORK_CHARGE_RATES BEFORE INSERT OR UPDATE AS BEGIN\
											  IF(NEW.RATE IS NULL) THEN NEW.RATE=0;\
											  IF(NEW.TIME_UNIT IS NULL) THEN NEW.TIME_UNIT=0;\
											  IF(NEW.RATE_TYPE IS NULL) THEN NEW.RATE_TYPE=0;\
											  IF(NEW.RATE_CAT IS NULL) THEN NEW.RATE_CAT='';\
											  IF(NEW.FEE_CLASS IS NULL) THEN NEW.FEE_CLASS='';\
											  IF(NEW.RATE_SHORTCUT IS NULL) THEN NEW.RATE_SHORTCUT='';\
											  IF(NEW.FIXED IS NULL) THEN NEW.FIXED=0;\
											  END;";





	m_GlobalTempTablesSPCTriggers[DBTYPE_FIREBIRD]=dboList;
}


void DbSqlProcedureCreation::Init_FireBird()
{
	SQLDBObjectCreateP dboList;
	m_Procedures[DBTYPE_FIREBIRD]=dboList;


	dboList.clear();

	dboList["00001,PROC_UPDATE_TEXT_FIELDS"]="RECREATE PROCEDURE  PROC_UPDATE_TEXT_FIELDS(STRTEXT BLOB SUB_TYPE TEXT,NID INTEGER) \
RETURNS (NNEWID INTEGER) \
AS \
BEGIN \
   NNEWID = NULL; \
    \
   /* CHECK ID FIELD IF 0 THEN SET NULL */ \
   IF (NID IS NOT NULL) THEN \
      IF(NID = 0) THEN \
        NID=0; \
 \
   IF (NID IS NULL AND STRTEXT<>' ') THEN \
   BEGIN \
      NNEWID = NEXT VALUE FOR SEQ_RID_TXT; \
      INSERT INTO ZS_TEXT_FIELDS (RID_TXT,TEXT) VALUES (:NNEWID,:STRTEXT); \
   END \
   ELSE IF (NID IS NOT NULL AND STRTEXT<>' ') THEN \
   BEGIN \
      UPDATE ZS_TEXT_FIELDS SET TEXT=:STRTEXT WHERE RID_TXT=:NID; \
      NNEWID = NID; \
   END \
   ELSE IF (NID IS NOT NULL AND (STRTEXT=' ' OR STRTEXT IS NULL)) THEN \
      DELETE FROM ZS_TEXT_FIELDS WHERE RID_TXT=:NID; \
END ";

dboList["00002,PROC_UPDATE_BINARY_FIELDS"]="RECREATE PROCEDURE  PROC_UPDATE_BINARY_FIELDS(STRBINARY BLOB,NID INTEGER) \
RETURNS (NNEWID INTEGER) \
AS \
BEGIN \
   NNEWID = NULL; \
    \
      /* CHECK ID FIELD IF 0 THEN SET NULL */ \
   IF (NID IS NOT NULL) THEN \
      IF(NID = 0) THEN \
        NID=0; \
         \
   IF (NID IS NULL AND STRBINARY<>' ') THEN \
   BEGIN \
      NNEWID = NEXT VALUE FOR SEQ_RID_BIN; \
      INSERT INTO ZS_BINARY_FIELDS (RID_BIN,TEXT) VALUES (:NNEWID,:STRBINARY); \
   END \
   ELSE IF (NID IS NOT NULL AND STRBINARY<>' ') THEN \
   BEGIN \
      UPDATE ZS_BINARY_FIELDS SET TEXT=:STRBINARY WHERE RID_BIN=:NID; \
      NNEWID = NID; \
   END \
   ELSE IF (NID IS NOT NULL AND (STRBINARY=' ' OR STRBINARY IS NULL)) THEN \
      DELETE FROM ZS_BINARY_FIELDS WHERE RID_BIN=:NID; \
END  \
";

dboList["00003,PROC_UPDATE_PAYMENT_SUM"]="RECREATE PROCEDURE  PROC_UPDATE_PAYMENT_SUM(OPER INTEGER, FZ_BETRAG DECIMAL(15,2), FZ_MWST_A DECIMAL(15,2), FZ_MWST_B DECIMAL(15,2), FZ_SKONTO DECIMAL(15,2),FZ_RECH_NR DECIMAL(15), FZ_PROJEKT VARCHAR(56)) \
AS \
 \
/* LOCAL VARS FROM DB */ \
DECLARE FV_DEF_MWST_AB TIMESTAMP; \
 \
DECLARE LV_CASE_1 INTEGER = 0; \
DECLARE LV_CASE_2 INTEGER = 0; \
DECLARE LV_CASE_3 INTEGER = 0; \
DECLARE LV_CASE_4 INTEGER = 0; \
 \
DECLARE BETRAG DECIMAL(15,2) = 0; \
DECLARE HONORAR DECIMAL(15,2) = 0; \
DECLARE NEBEN DECIMAL(15,2) = 0; \
 \
DECLARE EF_HON_VOLL DECIMAL(15,2)=0; \
DECLARE EF_NEB_VOLL DECIMAL(15,2)=0; \
DECLARE EF_HONORAR DECIMAL(15,2)=0; \
DECLARE EF_NEBENKOSTEN DECIMAL(15,2)=0; \
DECLARE EF_TOTAL_BETRAG DECIMAL(15,2)=0; \
DECLARE EF_TARIFART DECIMAL(15)=0; \
DECLARE LV_PR_ZAHL_MWST DECIMAL(15,2)=0; \
DECLARE LV_TEMP_CALC DECIMAL(15,2)=0; BEGIN \
 \
BETRAG = FZ_BETRAG-FZ_MWST_A-FZ_MWST_B+FZ_SKONTO-IIF(ABS(FZ_BETRAG)>0.01,FZ_SKONTO/FZ_BETRAG*(FZ_MWST_A+FZ_MWST_B),0);    /* DIFERENT FROM PICK() */ \
	 \
SELECT FIRST 1 EF_TARIFART, EF_HON_VOLL, EF_NEB_VOLL, EF_HONORAR, EF_NEBENKOSTEN, EF_TOTAL_BETRAG FROM F_ZAHL_EINGANG,F_FAKTURA WHERE EF_FAKT_NR = :FZ_RECH_NR INTO EF_TARIFART, EF_HON_VOLL,EF_NEB_VOLL,EF_HONORAR,EF_NEBENKOSTEN,EF_TOTAL_BETRAG; \
 \
IF ((EF_HON_VOLL+EF_NEB_VOLL)<>0.00) THEN \
	BEGIN \
		HONORAR = (BETRAG/(EF_HON_VOLL+EF_NEB_VOLL))*EF_HON_VOLL; \
		NEBEN = (BETRAG/(EF_HON_VOLL+EF_NEB_VOLL))*EF_NEB_VOLL; \
	END \
ELSE IF ((EF_HONORAR+EF_NEBENKOSTEN)<>0.00) THEN \
	BEGIN \
		HONORAR = (BETRAG/(EF_HONORAR+EF_NEBENKOSTEN))*EF_HONORAR; \
		NEBEN = (BETRAG/(EF_HONORAR+EF_NEBENKOSTEN))*EF_NEBENKOSTEN; \
	END \
 \
IF(FZ_BETRAG<>0.00) THEN  \
BEGIN \
	LV_PR_ZAHL_MWST = FZ_MWST_A+FZ_MWST_B+IIF(ABS(FZ_BETRAG)>0.01,(FZ_SKONTO/FZ_BETRAG)*(FZ_MWST_A+FZ_MWST_B),0); \
END \
 \
IF(EF_TOTAL_BETRAG<>0.00) THEN  \
BEGIN \
	LV_TEMP_CALC = ROUND((HONORAR/EF_TOTAL_BETRAG)*FZ_BETRAG,1); \
END /* PR_ZAHL_ZEIT CASE */ \
IF (EF_TARIFART=0)  THEN \
	LV_CASE_1=1; \
ELSE IF (EF_TARIFART=6 AND FZ_BETRAG=(EF_HONORAR+EF_NEBENKOSTEN))  THEN \
	LV_CASE_1=2; \
ELSE IF (EF_TARIFART=6 AND FZ_BETRAG<>(EF_HONORAR+EF_NEBENKOSTEN))  THEN \
	LV_CASE_1=3; \
	 \
/* PR_ZAHL_HONOR CASE */ \
IF (EF_TARIFART=0 OR EF_TARIFART=1 OR EF_TARIFART=2)  THEN \
	LV_CASE_2=1; \
ELSE IF ((EF_TARIFART=6 OR EF_TARIFART=7) AND FZ_BETRAG=(EF_HONORAR+EF_NEBENKOSTEN))  THEN \
	LV_CASE_2=2; \
ELSE IF ((EF_TARIFART=6 OR EF_TARIFART=7) AND FZ_BETRAG<>(EF_HONORAR+EF_NEBENKOSTEN))  THEN \
	LV_CASE_2=3; \
 \
/* PR_ZAHL_KOST CASE */ \
IF (EF_TARIFART=1)  THEN \
	LV_CASE_3=1; \
ELSE IF ((EF_TARIFART=7) AND FZ_BETRAG=(EF_HONORAR+EF_NEBENKOSTEN))  THEN \
	LV_CASE_3=2; \
ELSE IF ((EF_TARIFART=7) AND FZ_BETRAG<>(EF_HONORAR+EF_NEBENKOSTEN))  THEN \
	LV_CASE_3=3; \
	 \
/* PR_ZAHL_NEBEN CASE */ \
IF (EF_TARIFART=3)  THEN \
	LV_CASE_4=1; \
ELSE IF ((EF_TARIFART=6 OR EF_TARIFART=7) AND FZ_BETRAG=(EF_HONORAR+EF_NEBENKOSTEN))  THEN \
	LV_CASE_4=2; \
ELSE IF ((EF_TARIFART=6 OR EF_TARIFART=7) AND FZ_BETRAG<>(EF_HONORAR+EF_NEBENKOSTEN))  THEN \
	LV_CASE_4=3; \
 \
IF(OPER=1) THEN \
 BEGIN \
	 BETRAG=-BETRAG; \
	 NEBEN=-NEBEN; \
	 HONORAR=-HONORAR; \
	 LV_TEMP_CALC=-LV_TEMP_CALC; \
 END \
 \
 UPDATE F_PR_CACHE SET \
 FPC_DAT_AENDERUNG = CURRENT_TIMESTAMP, \
 FPC_ZAHL_ZEIT = (CASE :LV_CASE_1 WHEN 1 THEN FPC_ZAHL_ZEIT+:BETRAG WHEN 2 THEN FPC_ZAHL_ZEIT+:HONORAR WHEN 3 THEN FPC_ZAHL_ZEIT+:LV_TEMP_CALC ELSE FPC_ZAHL_ZEIT END), \
 FPC_ZAHL_HONOR = (CASE :LV_CASE_2 WHEN 1 THEN FPC_ZAHL_HONOR+:BETRAG WHEN 2 THEN FPC_ZAHL_HONOR+:HONORAR WHEN 3 THEN FPC_ZAHL_HONOR+:LV_TEMP_CALC ELSE FPC_ZAHL_HONOR END), \
 FPC_ZAHL_TOTAL = (CASE WHEN (:EF_TARIFART>=0 AND :EF_TARIFART<=7) THEN FPC_ZAHL_TOTAL+:BETRAG ELSE FPC_ZAHL_TOTAL END), \
 FPC_ZAHL_KOST = (CASE :LV_CASE_3 WHEN 1 THEN FPC_ZAHL_KOST+:BETRAG WHEN 2 THEN FPC_ZAHL_KOST+:HONORAR WHEN 3 THEN FPC_ZAHL_KOST+:LV_TEMP_CALC ELSE FPC_ZAHL_KOST END), \
 FPC_ZAHL_ZEITM = (CASE WHEN :EF_TARIFART=2 THEN FPC_ZAHL_ZEITM+:BETRAG ELSE FPC_ZAHL_ZEITM END), \
 FPC_ZAHL_NEBEN = (CASE :LV_CASE_4 WHEN 1 THEN FPC_ZAHL_NEBEN+:BETRAG WHEN 2 THEN FPC_ZAHL_NEBEN+:NEBEN WHEN 3 THEN FPC_ZAHL_NEBEN+:BETRAG-:LV_TEMP_CALC ELSE FPC_ZAHL_NEBEN END), \
 FPC_ZAHL_SPEZ = (CASE WHEN :EF_TARIFART=4 THEN FPC_ZAHL_SPEZ+:BETRAG ELSE FPC_ZAHL_SPEZ END), \
 FPC_ZAHL_MWST = FPC_ZAHL_MWST+:LV_PR_ZAHL_MWST \
 WHERE FPC_PROJ = :FZ_PROJEKT; \
	 \
 \
END  \
";

dboList["00004,PROC_UPDATE_INVOICE_SUM"]="RECREATE PROCEDURE  PROC_UPDATE_INVOICE_SUM(OPER INTEGER, EF_TARIFART DECIMAL(15), EF_HON_VOLL DECIMAL(15,2), EF_VOLLBETRAG DECIMAL(15,2), EF_NEB_VOLL DECIMAL(15,2), \
EF_FDATUM TIMESTAMP, EF_WUST DECIMAL(15,2), EF_MWST_1 DECIMAL(15,2),EF_MWST_2  DECIMAL(15,2),EF_MIN_RABATT DECIMAL(15,2),EF_PROJEKT VARCHAR(56), EF_AUFWAND DECIMAL(15,2), \
EF_NET_RAB_MWST_0 DECIMAL(15,2),EF_NET_RAB_MWST_1 DECIMAL(15,2),EF_NET_RAB_MWST_2 DECIMAL(15,2),EF_NET_RAB_WUST DECIMAL(15,2)) \
AS \
 \
/* LOCAL VARS FROM DB */ \
DECLARE LV_DEF_MWST_AB TIMESTAMP; \
 \
DECLARE LV_CASE_1 INTEGER = 0; \
DECLARE LV_CASE_2 INTEGER = 0; \
DECLARE LV_CASE_3 INTEGER = 0; \
DECLARE LV_CASE_4 INTEGER = 0; \
 \
BEGIN \
 \
/* TEST IF PR_CODE, PERS_CODE AND SPESEN CODE ARE ACTUAL, IF NOT GO AHEAD AND ONLY ABORT ALL IF PROJECT CODE IS EMPTY */ \
IF (EF_PROJEKT='' OR EF_PROJEKT IS NULL) THEN \
	EXIT; \
 \
/* PR_FAKT_ZEIT CASE */ \
IF ((EF_TARIFART=0 AND EF_HON_VOLL<>0) OR (EF_TARIFART=6) OR (EF_TARIFART=8 AND EF_HON_VOLL<>0))  THEN \
	LV_CASE_1=1; \
ELSE IF ((EF_TARIFART=0 AND EF_HON_VOLL=0) OR (EF_TARIFART=8 AND EF_HON_VOLL=0))  THEN \
	LV_CASE_1=2; \
	 \
/* PR_FAKT_KOSTEN CASE */ \
IF ((EF_TARIFART=1 AND EF_HON_VOLL<>0) OR (EF_TARIFART=7) OR (EF_TARIFART=5 AND EF_HON_VOLL<>0))  THEN \
	LV_CASE_2=1; \
ELSE IF ((EF_TARIFART=1 AND EF_HON_VOLL=0) OR (EF_TARIFART=5 AND EF_HON_VOLL=0))  THEN \
	LV_CASE_2=2; \
	 \
/* PR_FAKT_ZMITT CASE */ \
IF (EF_TARIFART=2 AND EF_HON_VOLL<>0)  THEN \
	LV_CASE_3=1; \
ELSE IF (EF_TARIFART=2 AND EF_HON_VOLL=0) THEN \
	LV_CASE_3=2; \
 \
/* PR_FAKT_NEBEN CASE */ \
IF ((EF_TARIFART=3 AND EF_HON_VOLL<>0) OR EF_TARIFART=6 OR EF_TARIFART=7)  THEN \
	LV_CASE_4=1; \
ELSE IF (EF_TARIFART=3 AND EF_HON_VOLL=0) THEN \
	LV_CASE_4=2; \
	 \
 \
 IF(OPER=0) THEN \
 BEGIN \
	 UPDATE F_PR_CACHE SET \
     FPC_DAT_AENDERUNG = CURRENT_TIMESTAMP, \
	 FPC_FAKT_ZEIT = (CASE :LV_CASE_1 WHEN 1 THEN FPC_FAKT_ZEIT+:EF_HON_VOLL WHEN 2 THEN FPC_FAKT_ZEIT+:EF_VOLLBETRAG ELSE FPC_FAKT_ZEIT END), \
	 FPC_FAKT_KOSTEN = (CASE :LV_CASE_2 WHEN 1 THEN FPC_FAKT_KOSTEN+:EF_HON_VOLL WHEN 2 THEN FPC_FAKT_KOSTEN+:EF_VOLLBETRAG ELSE FPC_FAKT_KOSTEN END), \
	 FPC_FAKT_ZMITT = (CASE :LV_CASE_3 WHEN 1 THEN FPC_FAKT_ZMITT+:EF_HON_VOLL WHEN 2 THEN FPC_FAKT_ZMITT+:EF_VOLLBETRAG ELSE FPC_FAKT_ZMITT END), \
	 FPC_FAKT_NEBEN = (CASE :LV_CASE_4 WHEN 1 THEN FPC_FAKT_NEBEN+:EF_NEB_VOLL WHEN 2 THEN FPC_FAKT_NEBEN+:EF_VOLLBETRAG ELSE FPC_FAKT_NEBEN END), \
	 FPC_FAKT_SPEZ = (CASE WHEN :EF_TARIFART=4 THEN FPC_FAKT_SPEZ+:EF_VOLLBETRAG ELSE FPC_FAKT_SPEZ END), \
	 FPC_FAKT_DAT = :EF_FDATUM, \
	 FPC_FAKT_TOTAL = FPC_FAKT_TOTAL+:EF_VOLLBETRAG, \
	 FPC_FAKT_AUFW = FPC_FAKT_AUFW+:EF_AUFWAND, \
	 FPC_RABATTE = FPC_RABATTE + :EF_VOLLBETRAG- :EF_MIN_RABATT \
	 WHERE FPC_PROJ = :EF_PROJEKT; \
 END \
 ELSE \
 BEGIN \
 	 UPDATE F_PR_CACHE SET \
	 FPC_DAT_AENDERUNG = CURRENT_TIMESTAMP, \
	 FPC_FAKT_ZEIT = (CASE WHEN (:EF_TARIFART=0 OR :EF_TARIFART=6 OR :EF_TARIFART=8) THEN FPC_FAKT_ZEIT-:EF_HON_VOLL ELSE FPC_FAKT_ZEIT END), \
	 FPC_FAKT_KOSTEN = (CASE WHEN (:EF_TARIFART=1 OR :EF_TARIFART=7 OR :EF_TARIFART=5) THEN FPC_FAKT_KOSTEN-:EF_HON_VOLL ELSE FPC_FAKT_KOSTEN END), \
	 FPC_FAKT_ZMITT = (CASE WHEN :EF_TARIFART=2 THEN FPC_FAKT_ZMITT-:EF_HON_VOLL ELSE FPC_FAKT_ZMITT END), \
	 FPC_FAKT_NEBEN = (CASE WHEN (:EF_TARIFART=3 OR :EF_TARIFART=6 OR :EF_TARIFART=7)  THEN FPC_FAKT_NEBEN-:EF_NEB_VOLL ELSE FPC_FAKT_NEBEN END), \
	 FPC_FAKT_SPEZ = (CASE WHEN :EF_TARIFART=4 THEN FPC_FAKT_SPEZ-:EF_VOLLBETRAG ELSE FPC_FAKT_SPEZ END), \
	 FPC_FAKT_DAT = :EF_FDATUM, \
	 FPC_FAKT_TOTAL = FPC_FAKT_TOTAL-:EF_VOLLBETRAG, \
	 FPC_FAKT_AUFW = FPC_FAKT_AUFW-:EF_AUFWAND, \
	 FPC_RABATTE = FPC_RABATTE -:EF_NET_RAB_MWST_0-:EF_NET_RAB_MWST_1-:EF_NET_RAB_MWST_2-:EF_NET_RAB_WUST \
	 WHERE FPC_PROJ = :EF_PROJEKT; \
 END \
 \
END  \
";

dboList["00005,PROC_UPDATE_CHARGE_SUM"]="RECREATE PROCEDURE  PROC_UPDATE_CHARGE_SUM(OPER INTEGER, FB_KOSTEN DECIMAL(15,2), FB_SPESEN DECIMAL(15,2), FB_AUFWAND DECIMAL(15,2), FB_SPES_AUFW DECIMAL(15,2), \
FB_ARBEITSBEL DECIMAL(3), FB_ZEIT DECIMAL(15,2), FB_NICHT_FAKTURIERBAR DECIMAL(3),FB_TARIFART  DECIMAL(15),FB_SPESENBEL DECIMAL(3),FB_PERS_NR VARCHAR(14), FB_SPESENCODE VARCHAR(23), FB_PR_SEQUENCE DECIMAL (18,0)) \
AS \
 \
/* LOCAL VARS FROM DB */ \
DECLARE LV_DEF_HS_FL INTEGER; \
DECLARE LV_DEF_PR_NF_NA INTEGER; \
DECLARE LV_DEF_NK_FK DECIMAL(3); \
DECLARE LV_FP_FREMDLEISTUNG DECIMAL(3); \
DECLARE LV_FEK_CODE VARCHAR(23) = ''; \
DECLARE LV_FEK_FREMDAUFW DECIMAL(3); \
DECLARE LV_PR_VERRECHENBAR DECIMAL(3); \
DECLARE LV_PR_CODE VARCHAR(56); DECLARE LV_CASE_1 INTEGER = 0; \
DECLARE LV_CASE_2 INTEGER = 0; \
DECLARE LV_CASE_3 INTEGER = 0; \
DECLARE LV_CASE_4 INTEGER = 0; \
 \
BEGIN \
 \
/* TEST IF PR_CODE, PERS_CODE AND SPESEN CODE ARE ACTUAL, IF NOT GO AHEAD AND ONLY ABORT ALL IF PROJECT CODE IS EMPTY */ \
FB_SPESENCODE = TRIM(FB_SPESENCODE); \
IF (FB_SPESENCODE='' OR FB_SPESENCODE IS NULL) THEN \
	BEGIN \
	 LV_FEK_CODE=''; \
	 LV_FEK_FREMDAUFW=0; \
	END \
ELSE \
	SELECT FIRST 1 FEK_CODE, FEK_FREMDAUFW FROM F_KOSTENARTEN WHERE FEK_CODE=:FB_SPESENCODE INTO LV_FEK_CODE,LV_FEK_FREMDAUFW; \
 \
FB_PERS_NR = TRIM(FB_PERS_NR); \
IF (FB_PERS_NR='' OR FB_PERS_NR IS NULL) THEN \
	 LV_FP_FREMDLEISTUNG=0; \
ELSE \
	 SELECT  FIRST 1 FP_FREMDLEISTUNG FROM FPERSON WHERE FP_PERS_NR=:FB_PERS_NR INTO LV_FP_FREMDLEISTUNG; \
 \
IF (FB_PR_SEQUENCE=0 OR FB_PR_SEQUENCE IS NULL) THEN \
	EXIT; \
ELSE \
	SELECT  FIRST 1 PR_CODE, PR_VERRECHENBAR FROM FPROJCT WHERE PR_SEQUENCE=:FB_PR_SEQUENCE INTO LV_PR_CODE, LV_PR_VERRECHENBAR; \
	 \
	 \
SELECT DEF_HS_FL,DEF_PR_NF_NA,DEF_NK_FK FROM FL_DEFAULT INTO LV_DEF_HS_FL,LV_DEF_PR_NF_NA, LV_DEF_NK_FK;  \
 IF(:FB_ARBEITSBEL=1 AND :FB_NICHT_FAKTURIERBAR<>0 OR (NOT(:LV_PR_VERRECHENBAR<>0) AND NOT(:LV_DEF_PR_NF_NA<>0))) THEN \
  LV_CASE_1 = 1; \
 \
 IF(:FB_ARBEITSBEL=1) THEN \
 BEGIN \
	IF(NOT(:LV_FP_FREMDLEISTUNG<>0)) THEN \
		BEGIN \
			IF(FB_TARIFART=1) THEN \
				LV_CASE_2=1; \
			ELSE \
				LV_CASE_2=2; \
		END \
	ELSE \
		BEGIN \
			IF(NOT(:LV_DEF_HS_FL<>0)) THEN \
				IF(FB_TARIFART=1) THEN \
					LV_CASE_2=3; \
				ELSE \
					LV_CASE_2=4; \
			ELSE \
				LV_CASE_2=5; \
		END \
 END \
 \
 IF (:FB_SPESENBEL=1 AND :FB_NICHT_FAKTURIERBAR<>0 OR (NOT(LV_PR_VERRECHENBAR<>0) AND NOT(LV_DEF_PR_NF_NA<>0))) THEN \
  LV_CASE_3 = 1; \
 \
 IF (:FB_SPESENBEL=1) THEN \
  BEGIN \
	IF (:LV_FEK_CODE<>'') THEN \
	BEGIN \
		IF (:LV_FEK_FREMDAUFW<>0) THEN \
		BEGIN \
			IF (NOT(:LV_DEF_NK_FK<>0)) THEN \
				LV_CASE_4 = 1; \
			ELSE \
				LV_CASE_4 = 2; \
		END \
     	ELSE \
			LV_CASE_4 = 3; \
    END \
	ELSE \
		LV_CASE_4 = 3; \
 END  IF(OPER=1) THEN \
 BEGIN \
	FB_KOSTEN = - FB_KOSTEN; \
	FB_SPESEN = - FB_SPESEN; \
	FB_AUFWAND = - FB_AUFWAND; \
	FB_SPES_AUFW = - FB_SPES_AUFW; \
	FB_ZEIT = - FB_ZEIT; \
 END \
 \
 UPDATE F_PR_CACHE SET \
 FPC_DAT_AENDERUNG = CURRENT_TIMESTAMP, \
 FPC_AUFGELAUFEN = FPC_AUFGELAUFEN+:FB_KOSTEN+:FB_SPESEN, \
 FPC_AUFG_AUFW=FPC_AUFG_AUFW+:FB_AUFWAND+:FB_SPES_AUFW, \
 FPC_AUFWAND_A = (CASE :FB_ARBEITSBEL WHEN 1 THEN FPC_AUFWAND_A+:FB_AUFWAND ELSE FPC_AUFWAND_A END), \
 FPC_AUFG_A = (CASE :FB_ARBEITSBEL WHEN 1 THEN FPC_AUFG_A+:FB_KOSTEN ELSE FPC_AUFG_A END), \
 FPC_AUFG_ARB_H = (CASE :FB_ARBEITSBEL WHEN 1 THEN FPC_AUFG_ARB_H+:FB_ZEIT ELSE FPC_AUFG_ARB_H END), \
 FPC_AUFG_NF_HON_EXT = (CASE :LV_CASE_1 WHEN 1 THEN FPC_AUFG_NF_HON_EXT+:FB_KOSTEN ELSE FPC_AUFG_NF_HON_EXT END), \
 FPC_AUFG_NF_HON_INT = (CASE :LV_CASE_1 WHEN 1 THEN FPC_AUFG_NF_HON_EXT+:FB_AUFWAND ELSE FPC_AUFG_NF_HON_INT END), \
 FPC_AUFG_NF_ZEIT = (CASE :LV_CASE_1 WHEN 1 THEN FPC_AUFG_NF_ZEIT+:FB_ZEIT ELSE FPC_AUFG_NF_ZEIT END), \
 FPC_AUFG_ZEIT = (CASE WHEN :LV_CASE_2=1 OR :LV_CASE_2=3 THEN FPC_AUFG_ZEIT+:FB_KOSTEN ELSE FPC_AUFG_ZEIT END), \
 FPC_AUFW_ZEIT = (CASE WHEN :LV_CASE_2=1 OR :LV_CASE_2=3 THEN FPC_AUFW_ZEIT+:FB_AUFWAND ELSE FPC_AUFW_ZEIT END), \
 FPC_AUFG_KOST = (CASE WHEN :LV_CASE_2=2 OR :LV_CASE_2=4 THEN FPC_AUFG_KOST+:FB_KOSTEN ELSE FPC_AUFG_KOST END), \
 FPC_AUFW_KOST = (CASE WHEN :LV_CASE_2=2 OR :LV_CASE_2=4 THEN FPC_AUFW_KOST+:FB_AUFWAND ELSE FPC_AUFW_KOST END), \
 FPC_AUFG_FREMDLEIST = (CASE WHEN :LV_CASE_2>2 THEN FPC_AUFG_FREMDLEIST+:FB_KOSTEN ELSE FPC_AUFG_FREMDLEIST END), \
 FPC_AUFW_FREMDLEIST = (CASE WHEN :LV_CASE_2>2 THEN FPC_AUFW_FREMDLEIST+:FB_KOSTEN ELSE FPC_AUFW_FREMDLEIST END), \
 FPC_AUFG_NF_NK_EXT = (CASE :LV_CASE_3 WHEN 1 THEN FPC_AUFG_NF_NK_EXT+:FB_SPESEN ELSE FPC_AUFG_NF_NK_EXT END), \
 FPC_AUFG_NF_NK_INT = (CASE :LV_CASE_3 WHEN 1 THEN FPC_AUFG_NF_NK_INT+:FB_SPES_AUFW ELSE FPC_AUFG_NF_NK_INT END), \
 FPC_AUFG_FREMD = (CASE WHEN (:LV_CASE_4=1 OR :LV_CASE_4=2) THEN FPC_AUFG_FREMD+:FB_SPESEN ELSE FPC_AUFG_FREMD END), \
 FPC_AUFW_FREMD = (CASE WHEN (:LV_CASE_4=1 OR :LV_CASE_4=2) THEN FPC_AUFW_FREMD+:FB_SPES_AUFW ELSE FPC_AUFW_FREMD END), \
 FPC_AUFG_SK = (CASE WHEN (:LV_CASE_4=1 OR :LV_CASE_4=3) THEN FPC_AUFG_SK+:FB_SPESEN ELSE FPC_AUFG_SK END), \
 FPC_AUFWAND_SK = (CASE WHEN (:LV_CASE_4=1 OR :LV_CASE_4=3) THEN FPC_AUFWAND_SK+:FB_SPES_AUFW ELSE FPC_AUFWAND_SK END) \
 WHERE FPC_PROJ = :LV_PR_CODE; END  \
";

dboList["00006,PROC_CHARGE_CUMULATION"]="RECREATE PROCEDURE  PROC_CHARGE_CUMULATION \
( \
  PV_PROJECT VARCHAR(100), \
  PV_FROM DATE, \
  PV_TO DATE, \
  P_UZ_ONLY INTEGER, \
  P_UZ_STUFE INTEGER, \
  P_ABT_FREMD_ONLY INTEGER, \
  P_INIT INTEGER, \
  P_INCLUDE_SP INTEGER, \
  P_ABT_EQUAL INTEGER, \
  P_DEPT VARCHAR(100), \
  P_DEL_OLD_CONTENT INTEGER \
) AS \
 \
DECLARE LV_PROJECT2 VARCHAR(100); \
DECLARE ABT VARCHAR(100) = ''; \
DECLARE LV_INCL_PROJ_CODE INTEGER = 0; \
DECLARE LV_EMPTY_STRING VARCHAR(100); \
 \
DECLARE FV_DEF_HS_FL INTEGER; \
DECLARE FV_DEF_PR_NF_NA INTEGER; \
DECLARE FV_DEF_NK_FK INTEGER; \
DECLARE FV_DEF_KUM_NO_INAKT INTEGER; DECLARE STR_SQL VARCHAR(4000); \
DECLARE STR_SQL_2 VARCHAR(2000); \
DECLARE STR_WHERE VARCHAR(2000); \
 \
BEGIN \
 \
/* PREPARE VALUES FOR SELECT */ \
 \
  SELECT FIRST 1 DEF_HS_FL,DEF_PR_NF_NA,DEF_NK_FK,DEF_KUM_NO_INAKT FROM FL_DEFAULT INTO FV_DEF_HS_FL,FV_DEF_PR_NF_NA,FV_DEF_NK_FK,FV_DEF_KUM_NO_INAKT; \
 \
  IF (P_ABT_EQUAL = 0) THEN \
    ABT=''; \
  ELSE IF (P_ABT_EQUAL = 1) THEN \
    SELECT PR_ABTEILUNG FROM FPROJCT WHERE PR_CODE=:PV_PROJECT INTO ABT; \
  ELSE IF (P_ABT_EQUAL = 2) THEN \
    ABT=P_DEPT; \
  ELSE \
    ABT=''; \
 \
  IF (P_INCLUDE_SP = 1) THEN \
    LV_PROJECT2 = PV_PROJECT || '~'; \
 \
 /* WHERE STATEMENTS */ \
IF (P_INCLUDE_SP<>0) THEN \
  STR_WHERE =' AND FASSIGN.FB_PROJ_NR BETWEEN ''' || PV_PROJECT || ''' AND ''' || LV_PROJECT2|| ''''; \
ELSE \
  STR_WHERE =' AND FASSIGN.FB_PROJ_NR = ''' || PV_PROJECT || ''''; IF (P_ABT_FREMD_ONLY<>0) THEN \
  STR_WHERE = STR_WHERE || ' AND FPERSON.FP_ABTEILUNG != SUBSTRING(FPROJCT.PR_ABTEILUNG FROM 1 FOR CHAR_LENGTH(FPERSON.FP_ABTEILUNG))'; IF (P_UZ_ONLY<>0) THEN \
  STR_WHERE = STR_WHERE || ' AND FB_UEBERZEIT_STU = ' || P_UZ_STUFE; IF (ABT<>'') THEN \
  STR_WHERE = STR_WHERE || ' AND ''' || ABT || '''= SUBSTRING(FPROJCT.PR_ABTEILUNG FROM 1 FOR CHAR_LENGTH(''' || ABT || '''))'; IF (FV_DEF_KUM_NO_INAKT<>0) THEN \
 STR_WHERE = STR_WHERE ||' AND FPROJCT.PR_AKTIV = 1'; \
  \
 /*B.T.: ENABLE TO KEEP OLD CONTENT */ \
IF (P_DEL_OLD_CONTENT=1)THEN \
	DELETE FROM TEMP_CHARGE_CUMULATION; \
 \
IF (LV_INCL_PROJ_CODE<>0) THEN \
BEGIN \
 STR_WHERE = STR_WHERE || ' GROUP BY FB_PROJ_NR'; \
 STR_WHERE = STR_WHERE || ' ORDER BY FB_PROJ_NR'; \
END /*  --------------CHARGES-------------- */ \
STR_SQL = \
'INSERT INTO TEMP_CHARGE_CUMULATION( \
  PROJ_CODE, \
  DATE_FROM, \
  DATE_TO, \
  T_KUM_ZEIT_ZT, \
  T_KUM_ZEIT_KT, \
  T_KUM_ZEIT, \
  T_KUM_HON_ZT, \
  T_KUM_HON_KT, \
  T_KUM_HON, \
  T_KUM_NEBEN, \
  T_KUM_AUFW_ZT, \
  T_KUM_AUFW_KT, \
  T_KUM_AUFW_HON, \
  T_KUM_AUFW_NEBEN, \
  T_KUM_AUFG_FR_KOST, \
  T_KUM_AUFW_FR_KOST, \
  T_KUM_AUFG_FR_LEIST, \
  T_KUM_AUFW_FR_LEIST, \
  T_KUM_ZEIT_FR_LEIST, \
  T_KUM_ZT_UNFAKT, \
  T_KUM_KT_UNFAKT, \
  T_KUM_NK_UNFAKT, \
  T_KUM_AUFG_HON_NF, \
  T_KUM_AUFG_NK_NF, \
  T_KUM_AUFG_FK_NF, \
  T_KUM_AUFW_HON_NF, \
  T_KUM_AUFW_NK_NF, \
  T_KUM_AUFW_FK_NF, \
  T_KUM_AUFG_ZEIT_NF, \
  T_KUM_AUFG_SPES, \
  T_KUM_AUFW_SPES, \
  T_KUM_ARB_AUFG_FAKT, \
  T_KUM_ARB_AUFW_FAKT, \
  T_KUM_NK_AUFG_FAKT, \
  T_KUM_NK_AUFW_FAKT, \
  T_KUM_FK_AUFG_FAKT, \
  T_KUM_FK_AUFW_FAKT) \
  SELECT ''' || PV_PROJECT || ''', ''' || PV_FROM || ''', ''' || PV_TO || ''',  \
SUM(CASE FB_TARIFART WHEN 1 THEN FB_ZEIT ELSE 0 END), \
SUM(CASE FB_TARIFART WHEN 0 THEN FB_ZEIT ELSE 0 END), \
SUM(FB_ZEIT), \
SUM(CASE WHEN (FB_TARIFART = 1) AND ((FP_FREMDLEISTUNG=0) OR (' || FV_DEF_HS_FL || '= 0)) THEN FB_KOSTEN ELSE 0 END), \
SUM(CASE WHEN (FB_TARIFART = 0) AND ((FP_FREMDLEISTUNG=0) OR (' || FV_DEF_HS_FL || ' = 0)) THEN FB_KOSTEN ELSE 0 END), \
SUM(CASE WHEN (FP_FREMDLEISTUNG=0) OR (' || FV_DEF_HS_FL || ' = 0) THEN FB_KOSTEN ELSE 0 END), \
0, \
SUM(CASE WHEN (FB_TARIFART = 1) AND ((FP_FREMDLEISTUNG=0) OR (' || FV_DEF_HS_FL || ' = 0)) THEN FB_AUFWAND ELSE 0 END), \
SUM(CASE WHEN (FB_TARIFART = 0) AND ((FP_FREMDLEISTUNG=0) OR (' || FV_DEF_HS_FL || ' = 0)) THEN FB_AUFWAND ELSE 0 END), \
SUM(CASE WHEN (FP_FREMDLEISTUNG=0) OR (' || FV_DEF_HS_FL || ' = 0) THEN FB_AUFWAND ELSE 0 END), \
0, \
0, \
0, \
SUM(CASE WHEN (FP_FREMDLEISTUNG=1) THEN FB_KOSTEN ELSE 0 END), \
SUM(CASE WHEN (FP_FREMDLEISTUNG=1) THEN FB_AUFWAND ELSE 0 END), \
SUM(CASE WHEN (FP_FREMDLEISTUNG=1) THEN FB_ZEIT ELSE 0 END), \
SUM(CASE WHEN (FB_TARIFART = 1) AND ((FB_NICHT_FAKTURIERBAR=1) OR ((PR_VERRECHENBAR=0) AND (' || FV_DEF_PR_NF_NA || '=0))) THEN FB_KOSTEN ELSE 0 END), \
SUM(CASE WHEN (FB_TARIFART = 0) AND ((FB_NICHT_FAKTURIERBAR=1) OR ((PR_VERRECHENBAR=0) AND (' || FV_DEF_PR_NF_NA || '=0))) THEN FB_KOSTEN ELSE 0 END), \
0, \
SUM(CASE WHEN ((FB_NICHT_FAKTURIERBAR=1) OR ((PR_VERRECHENBAR=0) AND (' || FV_DEF_PR_NF_NA || '=0)))THEN FB_KOSTEN ELSE 0 END), \
0, \
0, \
SUM(CASE WHEN ((FB_NICHT_FAKTURIERBAR=1) OR ((PR_VERRECHENBAR=0) AND (' || FV_DEF_PR_NF_NA || '=0)))THEN FB_AUFWAND ELSE 0 END), \
0, \
0, \
SUM(CASE WHEN ((FB_NICHT_FAKTURIERBAR=1) OR ((PR_VERRECHENBAR=0) AND (' || FV_DEF_PR_NF_NA || '=0)))THEN FB_ZEIT ELSE 0 END), \
0, \
0, \
SUM(CASE WHEN ((FB_NICHT_FAKTURIERBAR=0) AND (FB_FAKTURIERT=1))THEN FB_KOSTEN ELSE 0 END), \
SUM(CASE WHEN ((FB_NICHT_FAKTURIERBAR=0) AND (FB_FAKTURIERT=1))THEN FB_AUFWAND ELSE 0 END), \
0, \
0, \
0, \
0 \
FROM FASSIGN,FPROJCT,FPERSON \
WHERE FASSIGN.FB_DATUM >=''' || PV_FROM || ''' AND FASSIGN.FB_DATUM <= ''' || PV_TO || ''' AND FASSIGN.FB_PROJ_NR=FPROJCT.PR_CODE AND FASSIGN.FB_PERS_NR=FPERSON.FP_PERS_NR' || \
STR_WHERE || 'PLAN (FASSIGN INDEX(IND_FB_CI_PD), FPROJCT INDEX(IND_PR_CODE), FPERSON INDEX(IND_FP_PERS_NR))'; EXECUTE STATEMENT STR_SQL;  \
 \
/*  --------------EXPENSES-------------- */ \
/* STR_WHERE  IS SAME */ STR_SQL = \
'INSERT INTO TEMP_CHARGE_CUMULATION( \
  PROJ_CODE, \
  DATE_FROM, \
  DATE_TO, \
  T_KUM_ZEIT_ZT, \
  T_KUM_ZEIT_KT, \
  T_KUM_ZEIT, \
  T_KUM_HON_ZT, \
  T_KUM_HON_KT, \
  T_KUM_HON, \
  T_KUM_NEBEN, \
  T_KUM_AUFW_ZT, \
  T_KUM_AUFW_KT, \
  T_KUM_AUFW_HON, \
  T_KUM_AUFW_NEBEN, \
  T_KUM_AUFG_FR_KOST, \
  T_KUM_AUFW_FR_KOST, \
  T_KUM_AUFG_FR_LEIST, \
  T_KUM_AUFW_FR_LEIST, \
  T_KUM_ZEIT_FR_LEIST, \
  T_KUM_ZT_UNFAKT, \
  T_KUM_KT_UNFAKT, \
  T_KUM_NK_UNFAKT, \
  T_KUM_AUFG_HON_NF, \
  T_KUM_AUFG_NK_NF, \
  T_KUM_AUFG_FK_NF, \
  T_KUM_AUFW_HON_NF, \
  T_KUM_AUFW_NK_NF, \
  T_KUM_AUFW_FK_NF, \
  T_KUM_AUFG_ZEIT_NF, \
  T_KUM_AUFG_SPES, \
  T_KUM_AUFW_SPES, \
  T_KUM_ARB_AUFG_FAKT, \
  T_KUM_ARB_AUFW_FAKT, \
  T_KUM_NK_AUFG_FAKT, \
  T_KUM_NK_AUFW_FAKT, \
  T_KUM_FK_AUFG_FAKT, \
  T_KUM_FK_AUFW_FAKT) \
  SELECT ''' || PV_PROJECT || ''', ''' || PV_FROM || ''', ''' || PV_TO || ''',  \
  0,0,0,0,0,0, \
SUM(CASE \
WHEN (FEK_CODE IS NOT NULL) AND (FEK_FREMDAUFW = 1) AND (' || FV_DEF_NK_FK || ' = 1) \
THEN 0 \
ELSE FB_SPESEN \
END), \
0,0,0, \
SUM(CASE \
WHEN (FEK_CODE IS NOT NULL) AND (FEK_FREMDAUFW = 1) AND (' || FV_DEF_NK_FK || ' = 1) \
THEN 0 \
ELSE FB_SPES_AUFW \
END), \
SUM(CASE \
WHEN  (FEK_CODE IS NOT NULL) AND (FEK_FREMDAUFW = 1) \
THEN FB_SPESEN \
ELSE 0 \
END), \
SUM(CASE \
WHEN  (FEK_CODE IS NOT NULL) AND (FEK_FREMDAUFW = 1) \
THEN FB_SPES_AUFW \
ELSE 0 \
END), \
0,0,0,0,0, \
SUM(CASE WHEN FB_FAKTURIERT=1 THEN FB_SPESEN ELSE 0 END), \
0, \
SUM(CASE \
WHEN (FEK_CODE IS NULL) AND ((FB_NICHT_FAKTURIERBAR=1) OR ((PR_VERRECHENBAR = 0) AND (' || FV_DEF_PR_NF_NA || ') = 0)) \
THEN FB_SPESEN \
WHEN (FEK_CODE IS NOT NULL) AND (FEK_FREMDAUFW = 1) AND (FB_NICHT_FAKTURIERBAR=1) AND (' || FV_DEF_PR_NF_NA || ' = 0) \
THEN FB_SPESEN \
WHEN (FEK_CODE IS NOT NULL) AND (FEK_FREMDAUFW = 0) AND (FB_NICHT_FAKTURIERBAR=1) \
THEN FB_SPESEN \
ELSE 0 \
END), \
SUM(CASE \
WHEN  (FEK_CODE IS NOT NULL) AND (FEK_FREMDAUFW = 1) AND (FB_NICHT_FAKTURIERBAR = 0) \
THEN FB_SPESEN \
ELSE 0 \
END), \
0, \
SUM(CASE \
WHEN (FEK_CODE IS NULL) AND ((FB_NICHT_FAKTURIERBAR=1) OR ((PR_VERRECHENBAR = 0) AND (' || FV_DEF_PR_NF_NA || ') = 0)) \
THEN FB_SPES_AUFW \
WHEN (FEK_CODE IS NOT NULL) AND (FEK_FREMDAUFW = 1) AND (FB_NICHT_FAKTURIERBAR=1) AND (' || FV_DEF_PR_NF_NA || ' = 0) \
THEN FB_SPES_AUFW \
WHEN (FEK_CODE IS NOT NULL) AND (FEK_FREMDAUFW = 0) AND (FB_NICHT_FAKTURIERBAR=1) \
THEN FB_SPES_AUFW \
ELSE 0 \
END), \
SUM(CASE \
WHEN  (FEK_CODE IS NOT NULL) AND (FEK_FREMDAUFW = 1) AND (FB_NICHT_FAKTURIERBAR = 0) \
THEN FB_SPES_AUFW \
ELSE 0 \
END), \
0, \
SUM(CASE \
WHEN (FB_PERS_NR != '''') \
THEN FB_SPESEN \
ELSE 0 \
END), \
SUM(CASE \
WHEN (FB_PERS_NR != '''') \
THEN FB_SPES_AUFW \
ELSE 0 \
END), \
0,0, \
SUM(CASE \
WHEN (FEK_CODE IS NULL) AND (FB_NICHT_FAKTURIERBAR = 0) AND (FB_FAKTURIERT = 1) \
THEN FB_SPESEN \
WHEN (FEK_CODE IS NOT NULL) AND (FEK_FREMDAUFW = 1) AND (' || FV_DEF_NK_FK || ' = 0) AND (FB_NICHT_FAKTURIERBAR = 0) AND (FB_FAKTURIERT = 1) \
THEN FB_SPESEN \
WHEN  (FEK_CODE IS NOT NULL) AND (FEK_FREMDAUFW = 0) AND (FB_NICHT_FAKTURIERBAR = 0) AND (FB_FAKTURIERT = 1) \
THEN FB_SPESEN \
ELSE 0 \
END), \
SUM(CASE \
WHEN (FEK_CODE IS NULL) AND (FB_NICHT_FAKTURIERBAR = 0) AND (FB_FAKTURIERT = 1) \
THEN FB_SPES_AUFW \
WHEN (FEK_CODE IS NOT NULL) AND (FEK_FREMDAUFW = 1) AND (' || FV_DEF_NK_FK || ' = 0) AND (FB_NICHT_FAKTURIERBAR = 0) AND (FB_FAKTURIERT = 1) \
THEN FB_SPES_AUFW \
WHEN  (FEK_CODE IS NOT NULL) AND (FEK_FREMDAUFW = 0) AND (FB_NICHT_FAKTURIERBAR = 0) AND (FB_FAKTURIERT = 1) \
THEN FB_SPES_AUFW \
ELSE 0 \
END), \
SUM(CASE \
WHEN  (FEK_CODE IS NOT NULL) AND (FEK_FREMDAUFW = 1) AND (FB_NICHT_FAKTURIERBAR = 0) AND (FB_FAKTURIERT = 1) \
THEN FB_SPESEN \
ELSE 0 \
END), \
SUM(CASE \
WHEN  (FEK_CODE IS NOT NULL) AND (FEK_FREMDAUFW = 1) AND (FB_NICHT_FAKTURIERBAR = 0) AND (FB_FAKTURIERT = 1) \
THEN FB_SPES_AUFW \
ELSE 0 \
END) \
FROM FASSIGN \
INNER JOIN FPROJCT ON FASSIGN.FB_PROJ_NR=FPROJCT.PR_CODE \
LEFT OUTER JOIN F_KOSTENARTEN ON FB_SPESENCODE=FEK_CODE \
LEFT OUTER JOIN FPERSON ON FB_PERS_NR=FP_PERS_NR \
WHERE FASSIGN.FB_DATUM >=''' || PV_FROM || ''' AND FASSIGN.FB_DATUM <= ''' || PV_TO || '''' || \
STR_WHERE || 'PLAN (FASSIGN INDEX(IND_FB_CI_PD), FPROJCT INDEX(IND_PR_CODE), FPERSON INDEX(IND_FP_PERS_NR),F_KOSTENARTEN INDEX (IND_FEK_CODE))'; \
 \
EXECUTE STATEMENT STR_SQL; END   \
";

dboList["00007,PROC_FAKTURA_CUMULATION "]="RECREATE PROCEDURE  PROC_FAKTURA_CUMULATION \
										  ( \
										  PV_PROJECT VARCHAR(100), \
										  PV_FROM DATE, \
										  PV_TO DATE, \
										  P_INCLUDE_SP INTEGER, \
										  P_ABT_EQUAL INTEGER, \
										  P_DEPT VARCHAR(100), \
										  P_DEL_OLD_CONTENT INTEGER \
										  ) AS DECLARE LV_DEF_FULL_FAKT_KZ INTEGER; \
										  DECLARE LV_DEF_KUM_NO_INAKT INTEGER; \
										  \
										  DECLARE ABT VARCHAR(100) = ''; \
										  DECLARE LV_PROJECT2 VARCHAR(100); \
										  \
										  DECLARE STR_SQL VARCHAR(4000); \
										  DECLARE STR_WHERE VARCHAR(2000); \
										  \
										  BEGIN \
										  \
										  SELECT FIRST 1 DEF_KUM_NO_INAKT FROM FL_DEFAULT INTO LV_DEF_KUM_NO_INAKT; \
										  SELECT FIRST 1 DEF_FULL_FAKT_KZ FROM FL_DEFAULT2 INTO LV_DEF_FULL_FAKT_KZ; \
										  \
										  IF (P_ABT_EQUAL = 0) THEN \
										  ABT=''; \
										  ELSE IF (P_ABT_EQUAL = 1) THEN \
										  SELECT PR_ABTEILUNG FROM FPROJCT WHERE PR_CODE=:PV_PROJECT INTO ABT; \
										  ELSE IF (P_ABT_EQUAL = 2) THEN \
										  ABT=P_DEPT; \
										  ELSE \
										  ABT=''; \
										  \
										  IF (P_INCLUDE_SP = 1) THEN \
										  LV_PROJECT2 = PV_PROJECT || '~'; \
										  \
										  IF (P_INCLUDE_SP<>0) THEN \
										  STR_WHERE =' AND EF_PROJEKT BETWEEN ''' || PV_PROJECT || ''' AND ''' || LV_PROJECT2|| ''''; \
										  ELSE \
										  STR_WHERE =' AND EF_PROJEKT = ''' || PV_PROJECT || ''''; \
										  \
										  IF (ABT<>'') THEN \
										  STR_WHERE = STR_WHERE || ' AND ''' || ABT || '''= SUBSTRING(FPROJCT.PR_ABTEILUNG FROM 1 FOR CHAR_LENGTH(''' || ABT || '''))'; \
										  \
										  IF (P_DEL_OLD_CONTENT=1)THEN \
										  DELETE FROM TEMP_FAKTURA_CUMULATION; IF (LV_DEF_KUM_NO_INAKT>0) THEN \
										  BEGIN \
										  STR_WHERE = STR_WHERE ||' AND FPROJCT.PR_AKTIV = 1 '; \
										  END \
										  \
										  STR_SQL = \
										  'INSERT INTO TEMP_FAKTURA_CUMULATION( \
										  PROJ_CODE, \
										  DATE_FROM, \
										  DATE_TO, \
										  T_KUM_F_HONORAR, \
										  T_KUM_F_NEBENKOST, \
										  T_KUM_F_ZUSCHLAG, \
										  T_KUM_F_RABATT, \
										  T_KUM_F_RAB_ARB, \
										  T_KUM_F_RAB_NK, \
										  T_KUM_F_MWST, \
										  T_KUM_F_BETRAG, \
										  T_KUM_F_BET_MWST0, \
										  T_KUM_F_BET_MWSTA, \
										  T_KUM_F_BET_MWSTB, \
										  T_KUM_F_OFFEN, \
										  T_KUM_F_FK, \
										  T_KUM_F_NK_O_FK, \
										  T_KUM_F_EXT_HON, \
										  T_KUM_F_EXT_NK \
										  ) \
										  SELECT ''' || PV_PROJECT || ''', ''' || PV_FROM || ''', ''' || PV_TO || ''', \
										  SUM(EF_HON_VOLL), \
										  SUM(EF_NEB_VOLL), \
										  SUM(EF_ZUSCHLAG), \
										  SUM(EF_NET_RAB_MWST_0+EF_NET_RAB_MWST_1+EF_NET_RAB_MWST_2+EF_NET_RAB_WUST), \
										  SUM(CASE WHEN ABS(EF_HON_VOLL+EF_NEB_VOLL)>0.01 THEN (EF_ZUSCHLAG+EF_NET_RAB_MWST_0+EF_NET_RAB_MWST_1+EF_NET_RAB_MWST_2+EF_NET_RAB_WUST)*EF_HON_VOLL/(EF_HON_VOLL+EF_NEB_VOLL) ELSE 0 END), \
										  SUM(CASE WHEN ABS(EF_HON_VOLL+EF_NEB_VOLL)>0.01 THEN (EF_ZUSCHLAG+EF_NET_RAB_MWST_0+EF_NET_RAB_MWST_1+EF_NET_RAB_MWST_2+EF_NET_RAB_WUST)*EF_NEB_VOLL/(EF_HON_VOLL+EF_NEB_VOLL) ELSE 0 END), \
										  SUM(EF_MWST_1+EF_MWST_2), \
										  SUM(EF_TOTAL_BETRAG), \
										  SUM(EF_BETRAG_0), \
										  SUM(EF_BETRAG_1), \
										  SUM(EF_BETRAG_2), \
										  SUM(EF_OFFEN), \
										  SUM(CASE WHEN (' || LV_DEF_FULL_FAKT_KZ || '= 1 AND ABS(EF_NEB_VOLL)>0.01) THEN (SELECT SUM (FB_SPESEN) FROM FASSIGN INNER JOIN F_KOSTENARTEN ON FEK_CODE=FB_SPESENCODE WHERE FB_FAKTUR_NR=EF_FAKT_NR AND FB_SPESENBEL > 0 AND FEK_FREMDAUFW > 0 \
										  PLAN (FASSIGN INDEX (IND_FB_FAKTUR_NR),F_KOSTENARTEN INDEX (IND_FEK_CODE))) ELSE 0 END), \
										  SUM(CASE WHEN (' || LV_DEF_FULL_FAKT_KZ || '= 1 AND ABS(EF_NEB_VOLL)>0.01) THEN EF_NEB_VOLL-(SELECT SUM (FB_SPESEN) FROM FASSIGN INNER JOIN F_KOSTENARTEN ON FEK_CODE=FB_SPESENCODE WHERE FB_FAKTUR_NR=EF_FAKT_NR AND FB_SPESENBEL > 0 AND FEK_FREMDAUFW > 0 \
										  PLAN (FASSIGN INDEX (IND_FB_FAKTUR_NR),F_KOSTENARTEN INDEX (IND_FEK_CODE))) ELSE 0 END), \
										  SUM(CASE WHEN (PR_STARTDAT >=  ''' || PV_FROM || ''') AND (PR_STARTDAT<=''' || PV_TO || ''') THEN PR_FAKT_EXT_HON ELSE 0 END), \
										  SUM(CASE WHEN (PR_STARTDAT >=  ''' || PV_FROM || ''') AND (PR_STARTDAT<=''' || PV_TO || ''') THEN PR_FAKT_EXT_NK ELSE 0 END) \
										  FROM F_FAKTURA \
										  INNER JOIN FPROJCT ON PR_CODE=EF_PROJEKT \
										  WHERE EF_FDATUM >= ''' || PV_FROM || ''' AND EF_FDATUM <= ''' || PV_TO || ''' AND (EF_VERBUCHT=1 OR EF_FORMULAR=0)' || STR_WHERE || \
										  ' PLAN (F_FAKTURA INDEX(IND_EF_CI_PD), FPROJCT INDEX(IND_PR_CODE))'; \
										  \
										  EXECUTE STATEMENT STR_SQL; \
										  END  \
										  \
										  ";

dboList["00008,PROC_EXP_CUMUL_PROJ_EXPTYPE"]="RECREATE PROCEDURE  PROC_EXP_CUMUL_PROJ_EXPTYPE \
( \
  PV_PROJECT VARCHAR(100), \
  PV_FROM DATE, \
  PV_TO DATE, \
  PV_EXP_TYPE VARCHAR(100), \
  P_INCLUDE_SP INTEGER, \
  P_DEL_OLD_CONTENT INTEGER \
)AS \
 \
DECLARE LV_PROJECT2 VARCHAR(100); \
DECLARE LV_EXP_TYPE2 VARCHAR(100); \
DECLARE DEF_KUM_NO_INAKT INTEGER; \
 \
DECLARE STR_SQL VARCHAR(4000); \
DECLARE STR_WHERE VARCHAR(4000); \
DECLARE N_WHERE1 INTEGER =0; \
 \
BEGIN /* LOAD DEFAULTS */ \
SELECT FIRST 1 DEF_KUM_NO_INAKT FROM FL_DEFAULT INTO DEF_KUM_NO_INAKT; \
 \
/* PREPARE FOR CUMULATION WITH TILDA */ \
LV_PROJECT2 = PV_PROJECT || '~'; \
LV_EXP_TYPE2 = PV_EXP_TYPE || '~'; /* WHERE STATEMENTS */ \
IF (P_INCLUDE_SP<>0) THEN \
  STR_WHERE =' AND FASSIGN.FB_PROJ_NR BETWEEN ''' || PV_PROJECT || ''' AND ''' || LV_PROJECT2|| ''''; \
ELSE \
  STR_WHERE =' AND FASSIGN.FB_PROJ_NR = ''' || PV_PROJECT || ''''; IF ((PV_FROM> CAST('1.1.1900' AS DATE)) AND (PV_TO>CAST('1.1.1900' AS DATE))) THEN \
BEGIN \
  STR_WHERE = STR_WHERE || ' AND FASSIGN.FB_DATUM >= ''' || PV_FROM || ''' AND FASSIGN.FB_DATUM <= ''' || PV_TO || ''''; \
  N_WHERE1 =1; \
END \
 \
IF (DEF_KUM_NO_INAKT<>0) THEN \
 STR_WHERE = STR_WHERE ||' AND FPROJCT.PR_AKTIV = 1'; \
 \
 /*B.T.: ENABLE TO KEEP OLD CONTENT */ \
IF (P_DEL_OLD_CONTENT=1)THEN \
	DELETE FROM TEMP_EXP_CUMUL_PROJ_EXPTYPE; \
 \
/* ASSEMBLE QUERY */ \
STR_SQL = \
'INSERT INTO TEMP_EXP_CUMUL_PROJ_EXPTYPE( \
  PROJ_CODE, \
  DATE_FROM, \
  DATE_TO, \
  T_NK_AUFG_ANZ_KUM, \
  T_NK_AUFG_ANZ_UNKUM, \
  T_NK_AUFG_INT_KUM, \
  T_NK_AUFG_EXT_KUM, \
  T_NK_AUFG_INT_UNKUM, \
  T_NK_AUFG_EXT_UNKUM, \
  T_NK_AUFG_INT_UNKUM_FK, \
  T_NK_AUFG_EXT_UNKUM_FK \
) \
  SELECT ''' || PV_PROJECT || ''', ''' || PV_FROM || ''', ''' || PV_TO || ''', \
SUM(FB_SPES_AUFW), \
SUM(FB_SPESEN), \
SUM(FB_ANZAHL), \
SUM(CASE WHEN FB_SPESENCODE = ''' || PV_EXP_TYPE || ''' THEN FB_SPES_AUFW ELSE 0 END), \
SUM(CASE WHEN FB_SPESENCODE = ''' || PV_EXP_TYPE || ''' THEN FB_SPESEN ELSE 0 END), \
SUM(CASE WHEN FB_SPESENCODE = ''' || PV_EXP_TYPE || ''' THEN FB_ANZAHL ELSE 0 END), \
SUM(CASE WHEN (FEK_CODE IS NOT NULL) AND (FEK_FREMDAUFW = 1) AND (FB_SPESENCODE=''' || PV_EXP_TYPE || ''') THEN FB_SPES_AUFW ELSE 0 END), \
SUM(CASE WHEN (FEK_CODE IS NOT NULL) AND (FEK_FREMDAUFW = 1) AND (FB_SPESENCODE=''' || PV_EXP_TYPE || ''') THEN FB_SPESEN ELSE 0 END) \
FROM FASSIGN \
INNER JOIN FPROJCT ON FB_PROJ_NR=PR_CODE \
LEFT OUTER JOIN F_KOSTENARTEN ON FB_SPESENCODE=FEK_CODE \
LEFT OUTER JOIN FPERSON ON FB_PERS_NR=FP_PERS_NR \
WHERE FB_SPESENBEL = 1 AND FB_SPESENCODE BETWEEN ''' || PV_EXP_TYPE || ''' AND ''' || LV_EXP_TYPE2 || '''' || STR_WHERE || \
' PLAN (FPERSON INDEX(IND_FP_PERS_NR), FPROJCT INDEX(IND_PR_CODE), FASSIGN INDEX (IND_FB_CI_PK),F_KOSTENARTEN INDEX (IND_FEK_CODE))'; \
 \
EXECUTE STATEMENT STR_SQL; END  \
";

dboList["00009,PROC_PAYMENT_CUMULATION"]="RECREATE PROCEDURE  PROC_PAYMENT_CUMULATION \
( \
  PV_PROJECT VARCHAR(100), \
  PV_FROM DATE, \
  PV_TO DATE, \
  P_INCLUDE_SP INTEGER, \
  PV_ABT_EQUAL INTEGER, \
  PV_DEPT VARCHAR(100), \
  P_DEL_OLD_CONTENT INTEGER \
)AS \
 \
DECLARE ABT VARCHAR(100) = ''; \
DECLARE STR_SQL VARCHAR(4000); \
DECLARE STR_WHERE VARCHAR(2000); \
DECLARE LV_PROJECT2 VARCHAR(100); \
DECLARE DEF_KUM_NO_INAKT INTEGER; \
 \
BEGIN \
 \
	/* LOAD DEFAULTS */ \
	SELECT FIRST 1 DEF_KUM_NO_INAKT FROM FL_DEFAULT INTO DEF_KUM_NO_INAKT; \
 \
  /* WHERE STATEMENT */ \
  IF (PV_ABT_EQUAL = 0) THEN \
    ABT=''; \
  ELSE IF (PV_ABT_EQUAL = 1) THEN \
    SELECT PR_ABTEILUNG FROM FPROJCT WHERE PR_CODE=:PV_PROJECT INTO ABT; \
  ELSE IF (PV_ABT_EQUAL = 2) THEN \
    ABT=PV_DEPT; \
  ELSE \
    ABT=''; \
	 \
  IF (P_INCLUDE_SP = 1) THEN \
    LV_PROJECT2 = PV_PROJECT || '~'; \
 \
	/* WHERE STATEMENTS */ \
	IF (P_INCLUDE_SP<>0) THEN \
	  STR_WHERE =' AND FZ_PROJEKT BETWEEN ''' || PV_PROJECT || ''' AND ''' || LV_PROJECT2|| ''''; \
	ELSE \
	  STR_WHERE =' AND FZ_PROJEKT = ''' || PV_PROJECT || ''''; \
 \
  IF (ABT<>'') THEN \
   STR_WHERE = STR_WHERE || ' AND ''' || ABT || '''= SUBSTRING(FPROJCT.PR_ABTEILUNG FROM 1 FOR CHAR_LENGTH(''' || ABT || '''))'; \
 \
  IF (DEF_KUM_NO_INAKT <>0) THEN \
   STR_WHERE = STR_WHERE || ' AND FPROJCT.PR_AKTIV = 1';  \
 /*B.T.: ENABLE TO KEEP OLD CONTENT */ \
IF (P_DEL_OLD_CONTENT=1)THEN \
	DELETE FROM TEMP_PAYMENT_CUMULATION; \
 \
/* ASSEMBLE QUERY */ \
STR_SQL = \
'INSERT INTO TEMP_PAYMENT_CUMULATION ( \
  PROJ_CODE, \
  DATE_FROM, \
  DATE_TO, \
  T_KUM_Z_BETRAG, \
  T_KUM_Z_SKONTO, \
  T_KUM_Z_BET_MWSTA, \
  T_KUM_Z_BET_MWSTB, \
  T_KUM_Z_TOTAL \
) \
    SELECT ''' || PV_PROJECT || ''', ''' || PV_FROM || ''', ''' || PV_TO || ''', \
SUM(FZ_BETRAG), \
SUM(FZ_SKONTO), \
SUM(FZ_MWST_A), \
SUM(FZ_MWST_B), \
SUM(FZ_BETRAG+FZ_SKONTO) \
FROM F_ZAHL_EINGANG \
INNER JOIN FPROJCT ON FZ_PROJEKT=PR_CODE \
WHERE FZ_DATUM >= ''' || PV_FROM || ''' AND FZ_DATUM <=''' || PV_TO || '''' || STR_WHERE || \
' PLAN (FPROJCT INDEX(IND_PR_CODE), F_ZAHL_EINGANG INDEX (IND_FZ_CI_PD))'; \
 \
EXECUTE STATEMENT STR_SQL; \
 \
END  \
";

dboList["00010,PROC_LEVY_CUMULATION"]="RECREATE PROCEDURE  PROC_LEVY_CUMULATION  \
( \
  PV_PROJECT VARCHAR(100), \
  PV_FROM DATE, \
  PV_TO DATE, \
  P_INCLUDE_SP INTEGER, \
  PV_ABT_EQUAL INTEGER, \
  PV_ABT VARCHAR(100), \
  P_DEL_OLD_CONTENT INTEGER \
)AS  \
 \
DECLARE ABT VARCHAR(100) = ''; \
DECLARE STR_SQL VARCHAR(4000); \
DECLARE STR_WHERE VARCHAR(2000); \
DECLARE LV_PROJECT2 VARCHAR(100); \
DECLARE DEF_KUM_NO_INAKT INTEGER; \
 \
BEGIN \
   \
   \
	/* LOAD DEFAULTS */ \
	SELECT FIRST 1 DEF_KUM_NO_INAKT FROM FL_DEFAULT INTO DEF_KUM_NO_INAKT; \
 \
  /* WHERE STATEMENT */ \
  IF (PV_ABT_EQUAL = 0) THEN \
    ABT=''; \
  ELSE IF (PV_ABT_EQUAL = 1) THEN \
    SELECT PR_ABTEILUNG FROM FPROJCT WHERE PR_CODE=:PV_PROJECT INTO ABT; \
  ELSE IF (PV_ABT_EQUAL = 2) THEN \
    ABT=PV_ABT; \
  ELSE \
    ABT=''; \
	 \
  IF (P_INCLUDE_SP = 1) THEN \
    LV_PROJECT2 = PV_PROJECT || '~'; \
 \
	/* WHERE STATEMENTS */ \
	IF (P_INCLUDE_SP<>0) THEN \
	  STR_WHERE =' AND EFU_PROJ_NR BETWEEN ''' || PV_PROJECT || ''' AND ''' || LV_PROJECT2|| ''''; \
	ELSE \
	  STR_WHERE =' AND EFU_PROJ_NR = ''' || PV_PROJECT || ''''; \
 \
  IF (ABT<>'') THEN \
   STR_WHERE = STR_WHERE || ' AND ''' || ABT || '''= SUBSTRING(FPROJCT.PR_ABTEILUNG FROM 1 FOR CHAR_LENGTH(''' || ABT || '''))'; \
 \
  IF (DEF_KUM_NO_INAKT <>0) THEN \
   STR_WHERE = STR_WHERE || ' AND FPROJCT.PR_AKTIV = 1'; \
    \
       \
 \
/* DELETE PREVIOUS VALUES FOR SAME SESISON */ \
IF (P_DEL_OLD_CONTENT=1)THEN \
	DELETE FROM TEMP_LEVY_CUMULATION; \
 \
/* ASSEMBLE QUERY */ \
STR_SQL = \
'INSERT INTO TEMP_LEVY_CUMULATION ( \
  PROJ_CODE, \
  DATE_FROM, \
  DATE_TO, \
  T_UML_FAKT_BEL_HON, \
  T_UML_FAKT_BEL_NK, \
  T_UML_FAKT_ENT_HON, \
  T_UML_FAKT_ENT_NK, \
  T_UML_FAKT_FKT_HON, \
  T_UML_FAKT_FKT_NK \
) \
    SELECT ''' || PV_PROJECT || ''', ''' || PV_FROM || ''', ''' || PV_TO || ''', \
SUM(EFU_BELAST_HON), \
SUM(EFU_BELAST_NK), \
SUM(EFU_ENTLAST_HON), \
SUM(EFU_ENTLAST_NK), \
SUM(CASE WHEN (EFU_PROJ_NR = ''' || PV_PROJECT || ''') THEN EFU_ENT_HON_UNV ELSE 0 END), \
SUM(CASE WHEN (EFU_PROJ_NR = ''' || PV_PROJECT || ''') THEN EFU_ENT_NK_UNV ELSE 0 END) \
FROM F_FAKT_UMLAGEN \
INNER JOIN FPROJCT ON EFU_PROJ_NR=PR_CODE \
WHERE EFU_DATUM >= ''' || PV_FROM || ''' AND EFU_DATUM <=''' || PV_TO || '''' || STR_WHERE ||  \
' PLAN (FPROJCT INDEX(IND_PR_CODE), F_FAKT_UMLAGEN INDEX (IND_EFU_CI_PROJ_NR_DATE))'; \
 \
EXECUTE STATEMENT STR_SQL; \
 \
END ";

dboList["00011,PROC_BF_PROJ_PERS_PERIOD_SER"]="RECREATE PROCEDURE  PROC_BF_PROJ_PERS_PERIOD_SER \
( \
  PV_PROJECT VARCHAR(100), \
  PV_PERSON VARCHAR(100), \
  PV_PERIOD_SERIES VARCHAR(1000), /*STRING WITH PAIRS OF DATES SEPARATED BY A COMMA IN THE FORMAT DD.MM.YYYY. EXAMPLE: 01.01.2011,31.01.2011,01.02.2011,28.02.2011,01.03.2011,31.3.2011*/ \
  P_INCLUDE_SP INTEGER, \
  P_INVOICEABILITY INTEGER, /*0: ALL CHARGES   1:ONLY FB_NICHT_FAKTURIERBAR=1   2:ONLY FB_NICHT_FAKTURIERBAR=0*/ \
  P_INVOICED_STATE INTEGER  /*0: ALL CHARGES   1:ONLY FB_FAKTURIERT=1   2:ONLY FB_FAKTURIERT=0 */ \
) AS \
 \
DECLARE LV_PROJECT2 VARCHAR(100); \
DECLARE STR_SQL VARCHAR(4000); \
DECLARE STR_WHERE VARCHAR(2000); \
DECLARE DATE_FROM DATE; \
DECLARE DATE_TO DATE; \
DECLARE STR_DATE_FROM VARCHAR(100); \
DECLARE STR_DATE_TO VARCHAR(100); \
DECLARE N_CNT INTEGER =0; \
DECLARE N_POS_END INTEGER =1; \
DECLARE N_POS_START INTEGER =1; \
 \
BEGIN \
	/* PREPARE VALUES FOR SELECT */ \
	IF (P_INCLUDE_SP = 1) THEN \
		LV_PROJECT2 = PV_PROJECT || '~'; \
	 \
	/* WHERE STATEMENTS */ \
	IF (P_INCLUDE_SP<>0) THEN \
	  STR_WHERE =' AND FASSIGN.FB_PROJ_NR BETWEEN ''' || PV_PROJECT || ''' AND ''' || LV_PROJECT2|| ''''; \
	ELSE \
	  STR_WHERE =' AND FASSIGN.FB_PROJ_NR = ''' || PV_PROJECT || ''''; \
	 \
 	DELETE FROM TEMP_BF_PROJ_PERS_PERIOD_SER; \
 \
	/* GO INTO LOOP */ \
WHILE (N_POS_END<CHAR_LENGTH(PV_PERIOD_SERIES)) DO \
BEGIN \
 \
  /* CALCULATE NEW TARGET: NEW COMMA OR END OF STRING */ \
  N_POS_END = POSITION (',',PV_PERIOD_SERIES, N_POS_START); \
   \
  IF (N_POS_END = 0 OR N_POS_END = 1) THEN \
    N_POS_END = CHAR_LENGTH(PV_PERIOD_SERIES)+1; \
	 \
  IF (N_POS_END<=N_POS_START) THEN \
    BREAK; \
	 \
  STR_DATE_FROM = SUBSTRING(PV_PERIOD_SERIES FROM N_POS_START FOR N_POS_END-N_POS_START); \
	 \
  N_POS_START = N_POS_END+1; \
 \
  /* CALCULATE NEW TARGET: NEW COMMA OR END OF STRING */ \
  N_POS_END = POSITION ( ',', PV_PERIOD_SERIES, N_POS_START); \
  IF (N_POS_END = 0 OR N_POS_END = 1) THEN \
    N_POS_END = CHAR_LENGTH(PV_PERIOD_SERIES)+1; \
	 \
  IF (N_POS_END<=N_POS_START) THEN \
    BREAK; \
	 \
  STR_DATE_TO = SUBSTRING(PV_PERIOD_SERIES FROM N_POS_START FOR N_POS_END-N_POS_START); \
   \
  N_POS_START = N_POS_END+1; \
 \
  DATE_FROM = CAST (STR_DATE_FROM AS DATE); \
  DATE_TO = CAST (STR_DATE_TO AS DATE); /*  --------------CALCULATE-------------- */ \
STR_SQL = \
'INSERT INTO TEMP_BF_PROJ_PERS_PERIOD_SER( \
  T_D1, \
  T_D2, \
  T_KUM_ZEIT, \
  T_KUM_ZEIT_TOTAL, \
  T_KUM_HON, \
  T_KUM_AUFW_HON, \
  T_KUM_AUFG_SPES, \
  T_KUM_AUFW_SPES) \
  SELECT \
''' || DATE_FROM || ''', \
''' || DATE_TO || ''', \
SUM(CASE WHEN  ' || P_INVOICEABILITY || '<>0 AND ' || P_INVOICED_STATE || '<>0 THEN FB_ZEIT \
WHEN ((' || P_INVOICED_STATE || '=0) OR ((' || P_INVOICED_STATE || '=1) AND FB_FAKTURIERT<>0) OR ((' || P_INVOICED_STATE || '=2) AND FB_FAKTURIERT<>0)) \
AND ((' || P_INVOICEABILITY || '=0) OR ((' || P_INVOICEABILITY || '=2) AND FB_NICHT_FAKTURIERBAR<>0) OR ((' || P_INVOICEABILITY || '=1) AND FB_NICHT_FAKTURIERBAR<>0)) THEN FB_ZEIT ELSE 0 END), \
 \
SUM(CASE WHEN  ' || P_INVOICEABILITY || '<>0 AND ' || P_INVOICED_STATE || '<>0 THEN FB_ZEIT_TOTAL \
WHEN ((' || P_INVOICED_STATE || '=0) OR ((' || P_INVOICED_STATE || '=1) AND FB_FAKTURIERT<>0) OR ((' || P_INVOICED_STATE || '=2) AND FB_FAKTURIERT<>0)) \
AND ((' || P_INVOICEABILITY || '=0) OR ((' || P_INVOICEABILITY || '=2) AND FB_NICHT_FAKTURIERBAR<>0) OR ((' || P_INVOICEABILITY || '=1) AND FB_NICHT_FAKTURIERBAR<>0)) THEN FB_ZEIT_TOTAL ELSE 0 END), \
 \
SUM(CASE WHEN  ' || P_INVOICEABILITY || '<>0 AND ' || P_INVOICED_STATE || '<>0 THEN FB_KOSTEN \
WHEN ((' || P_INVOICED_STATE || '=0) OR ((' || P_INVOICED_STATE || '=1) AND FB_FAKTURIERT<>0) OR ((' || P_INVOICED_STATE || '=2) AND FB_FAKTURIERT<>0)) \
AND ((' || P_INVOICEABILITY || '=0) OR ((' || P_INVOICEABILITY || '=2) AND FB_NICHT_FAKTURIERBAR<>0) OR ((' || P_INVOICEABILITY || '=1) AND FB_NICHT_FAKTURIERBAR<>0)) THEN FB_KOSTEN ELSE 0 END), \
 \
SUM(CASE WHEN  ' || P_INVOICEABILITY || '<>0 AND ' || P_INVOICED_STATE || '<>0 THEN FB_AUFWAND \
WHEN ((' || P_INVOICED_STATE || '=0) OR ((' || P_INVOICED_STATE || '=1) AND FB_FAKTURIERT<>0) OR ((' || P_INVOICED_STATE || '=2) AND FB_FAKTURIERT<>0)) \
AND ((' || P_INVOICEABILITY || '=0) OR ((' || P_INVOICEABILITY || '=2) AND FB_NICHT_FAKTURIERBAR<>0) OR ((' || P_INVOICEABILITY || '=1) AND FB_NICHT_FAKTURIERBAR<>0)) THEN FB_AUFWAND ELSE 0 END), \
 \
SUM(CASE WHEN  ' || P_INVOICEABILITY || '<>0 AND ' || P_INVOICED_STATE || '<>0 THEN FB_SPESEN \
WHEN ((' || P_INVOICED_STATE || '=0) OR ((' || P_INVOICED_STATE || '=1) AND FB_FAKTURIERT<>0) OR ((' || P_INVOICED_STATE || '=2) AND FB_FAKTURIERT<>0)) \
AND ((' || P_INVOICEABILITY || '=0) OR ((' || P_INVOICEABILITY || '=2) AND FB_NICHT_FAKTURIERBAR<>0) OR ((' || P_INVOICEABILITY || '=1) AND FB_NICHT_FAKTURIERBAR<>0)) THEN FB_SPESEN ELSE 0 END), \
 \
SUM(CASE WHEN  ' || P_INVOICEABILITY || '<>0 AND ' || P_INVOICED_STATE || '<>0 THEN FB_SPES_AUFW \
WHEN ((' || P_INVOICED_STATE || '=0) OR ((' || P_INVOICED_STATE || '=1) AND FB_FAKTURIERT<>0) OR ((' || P_INVOICED_STATE || '=2) AND FB_FAKTURIERT<>0)) \
AND ((' || P_INVOICEABILITY || '=0) OR ((' || P_INVOICEABILITY || '=2) AND FB_NICHT_FAKTURIERBAR<>0) OR ((' || P_INVOICEABILITY || '=1) AND FB_NICHT_FAKTURIERBAR<>0)) THEN FB_SPES_AUFW ELSE 0 END) \
FROM FASSIGN \
INNER JOIN FPERSON ON FB_PERS_NR=FP_PERS_NR \
INNER JOIN FPROJCT ON FB_PROJ_NR=PR_CODE \
WHERE FASSIGN.FB_DATUM >=''' || DATE_FROM || ''' AND FASSIGN.FB_DATUM <= ''' || DATE_TO || '''AND FP_PERS_NR = ''' || PV_PERSON || '''' || \
STR_WHERE || 'PLAN (FASSIGN INDEX(IND_FB_CI_PD), FPROJCT INDEX(IND_PR_CODE), FPERSON INDEX(IND_FP_PERS_NR))'; EXECUTE STATEMENT STR_SQL; \
 \
END \
 \
END  \
";

dboList["00012,PROC_RESOLVE_RATE_CAT"]="RECREATE PROCEDURE  PROC_RESOLVE_RATE_CAT(P_RATE_CAT VARCHAR(100), P_DATE DATE) RETURNS (RATE_CAT VARCHAR(100)) AS \
 \
DECLARE STR_RATE_SEARCH VARCHAR(100); \
DECLARE STR_WHERE VARCHAR(2000); \
DECLARE STR_SQL VARCHAR(4000); \
DECLARE ETK_CODE VARCHAR(23); \
DECLARE ETK_SAMMEL_KT DECIMAL(3); \
DECLARE ETKR_TARIFKATEGORIE VARCHAR(23); \
DECLARE ETKR_TARIFKAT_REF VARCHAR(34); \
DECLARE ETKR_DATUM_AB TIMESTAMP; \
 \
BEGIN \
 \
/* THIS SQL WILL RETURN LATEST ENTRY WITH SAME DATE OR DATE BEFORE GIVEN DATE IF RECORD EXISTS. IF RECORD DOES NOT EXISTS, FIELDS WITH ETKR PREFIX WILL BE NULL */ \
SELECT FIRST 1 ETK_CODE,ETK_SAMMEL_KT,ETKR_TARIFKATEGORIE,ETKR_TARIFKAT_REF,ETKR_DATUM_AB FROM F_TARIFKATEG \
LEFT OUTER JOIN F_TK_REFERENCES ON ETKR_TARIFKATEGORIE=ETK_CODE AND ETKR_DATUM_AB<=:P_DATE \
WHERE ETK_CODE = :P_RATE_CAT ORDER BY ETKR_DATUM_AB DESC \
INTO ETK_CODE,ETK_SAMMEL_KT,ETKR_TARIFKATEGORIE,ETKR_TARIFKAT_REF,ETKR_DATUM_AB; \
 \
/* THIS IS DEFAULT */ \
RATE_CAT = ETK_CODE; \
 \
IF (ETK_SAMMEL_KT<>0) THEN \
  IF (ETKR_DATUM_AB IS NOT NULL) THEN \
	  BEGIN \
		IF (ETKR_TARIFKAT_REF IS NOT NULL AND ETKR_TARIFKAT_REF <> P_RATE_CAT) THEN \
			BEGIN \
				EXECUTE PROCEDURE PROC_RESOLVE_RATE_CAT :ETKR_TARIFKAT_REF, :P_DATE RETURNING_VALUES :RATE_CAT; \
				EXIT; \
			END \
	  END  \
END  \
";

dboList["00013,PROC_TEST_FEE_CLASS"]="RECREATE PROCEDURE  PROC_TEST_FEE_CLASS \
( \
  P_EXT_INT DECIMAL, \
  P_RATE_TYPE DECIMAL, \
  P_PERSON VARCHAR(100), \
  P_PROJECT VARCHAR(100), \
  P_DATE DATE, \
  P_FUNCTION VARCHAR(100), \
  P_ACTIVITY VARCHAR(100), \
  P_RATE_CAT VARCHAR(100), \
  P_FIXED_FEE_CLASS INTEGER, \
  P_FEE_CLASS VARCHAR(100) \
) \
RETURNS (N_RATE_FOUND INTEGER) AS \
 \
DECLARE EH_CODE VARCHAR(7); \
DECLARE EH_TARIFKATEGORIE VARCHAR(23); \
DECLARE EH_TARIF_KOST DECIMAL(15,2); \
DECLARE EH_TARIF_ZEIT DECIMAL(15,2); \
DECLARE EH_TARIF_AUFW DECIMAL(15,2); \
DECLARE STR_SQL VARCHAR(4000); \
DECLARE N_CNT_RECORDS INTEGER = 0; \
DECLARE TMP1 INTEGER = 0; \
 \
BEGIN \
 \
N_RATE_FOUND = 0; \
 \
/* WARNING: THIS FUNCTION CHANGES VALUES INSIDE TEMP_CALC_WORK_CHARGE_RATES IF RETURNS 1 */ \
SELECT COUNT(*) FROM F_HONORARKLASSE \
WHERE EH_CODE = :P_FEE_CLASS AND EH_TARIFKATEGORIE= :P_RATE_CAT INTO N_CNT_RECORDS; IF (CHAR_LENGTH(P_FIXED_FEE_CLASS)>0 AND (CHAR_LENGTH(P_RATE_CAT)>0)) THEN \
BEGIN \
  IF (P_EXT_INT = 0) THEN \
    UPDATE TEMP_CALC_WORK_CHARGE_RATES SET RATE = :P_FIXED_FEE_CLASS; \
  ELSE \
    UPDATE TEMP_CALC_WORK_CHARGE_RATES SET RATE = 0;   IF (N_CNT_RECORDS>0) THEN \
  BEGIN \
 \
    SELECT FIRST 1 EH_CODE,EH_TARIFKATEGORIE,EH_TARIF_KOST,EH_TARIF_ZEIT,EH_TARIF_AUFW FROM F_HONORARKLASSE \
    WHERE EH_CODE = :P_FEE_CLASS AND EH_TARIFKATEGORIE= :P_RATE_CAT INTO \
    EH_CODE,EH_TARIFKATEGORIE,EH_TARIF_KOST,EH_TARIF_ZEIT,EH_TARIF_AUFW; \
 \
    N_RATE_FOUND =1; \
 \
    IF (P_EXT_INT = 1) THEN \
 \
        IF (EH_TARIF_AUFW<>0.00) THEN \
          UPDATE TEMP_CALC_WORK_CHARGE_RATES SET RATE = :EH_TARIF_AUFW; \
        ELSE \
          N_RATE_FOUND =0; \
    ELSE \
        IF ( IIF( (:P_RATE_TYPE-1)=0,  EH_TARIF_KOST, EH_TARIF_ZEIT)<>0) THEN \
          UPDATE TEMP_CALC_WORK_CHARGE_RATES SET RATE = IIF ((:P_RATE_TYPE-1)=0, :EH_TARIF_KOST, :EH_TARIF_ZEIT); \
        ELSE \
          N_RATE_FOUND =0; \
 \
    IF (N_RATE_FOUND>0) THEN \
	BEGIN \
      UPDATE TEMP_CALC_WORK_CHARGE_RATES SET RATE_SHORTCUT = 'R(FC)'; \
      IF (P_EXT_INT = 0) THEN \
	  BEGIN \
        UPDATE TEMP_CALC_WORK_CHARGE_RATES SET RATE_CAT = :P_RATE_CAT; \
        UPDATE TEMP_CALC_WORK_CHARGE_RATES SET FEE_CLASS = :P_FEE_CLASS; \
	  END \
    END \
 \
  END \
END \
END  \
 \
";

dboList["00014,PROC_CALC_WORK_CHARGE_RATES"]="RECREATE PROCEDURE  PROC_CALC_WORK_CHARGE_RATES \
( \
  P_EXT_INT DECIMAL, \
  P_RATE_TYPE DECIMAL, \
  P_PERSON VARCHAR(100), \
  P_PROJECT VARCHAR(100), \
  P_DATE DATE, \
  P_FUNCTION VARCHAR(100), \
  P_ACTIVITY VARCHAR(100), \
  P_DURATION DECIMAL, \
  P_FROM DATE, \
  P_TO DATE, \
  P_PRIOR_EXT VARCHAR(100) = '754612380', \
  P_PRIOR_INT VARCHAR(100) = '745162380' \
) AS \
 \
DECLARE DEF_TARIFBERECHNUNG DECIMAL(3); \
DECLARE DEF_TARIFKATEG VARCHAR(23); \
DECLARE DEF_UZ1_ARB_ZEIT DECIMAL(3); \
DECLARE DEF_UZ2_ARB_ZEIT DECIMAL(3); \
DECLARE DEF_UZ3_ARB_ZEIT DECIMAL(3); \
DECLARE DEF_UZ4_ARB_ZEIT DECIMAL(3); \
DECLARE DEF_UZ5_ARB_ZEIT DECIMAL(3); \
DECLARE DEF_UZ1_AUFWAND DECIMAL(3); \
DECLARE DEF_UZ2_AUFWAND DECIMAL(3); \
DECLARE DEF_UZ3_AUFWAND DECIMAL(3); \
DECLARE DEF_UZ4_AUFWAND DECIMAL(3); \
DECLARE DEF_UZ5_AUFWAND DECIMAL(3); \
DECLARE DEF_UZ1_KOSTEN DECIMAL(3); \
DECLARE DEF_UZ2_KOSTEN DECIMAL(3); \
DECLARE DEF_UZ3_KOSTEN DECIMAL(3); \
DECLARE DEF_UZ4_KOSTEN DECIMAL(3); \
DECLARE DEF_UZ5_KOSTEN DECIMAL(3); \
DECLARE DEF_UEBERST_FAKT1 DECIMAL(15,2); \
DECLARE DEF_UEBERST_FAKT2 DECIMAL(15,2); \
DECLARE DEF_UEBERST_FAKT3 DECIMAL(15,2); \
DECLARE DEF_UEBERST_FAKT4 DECIMAL(15,2); \
DECLARE DEF_UEBERST_FAKT5 DECIMAL(15,2); DECLARE PR_FAKTURART DECIMAL(3); \
DECLARE PR_TARIFKATEGORIE VARCHAR(23); \
DECLARE PR_TARIFBERECHNUNG DECIMAL(3); \
DECLARE PR_FAKTOR DECIMAL(15,4); \
 \
DECLARE EFK_CODE VARCHAR(14); \
DECLARE EFK_HONOR_KL VARCHAR(7); \
DECLARE EFK_FIX DECIMAL(3); \
 \
DECLARE ET_CODE VARCHAR(23); \
DECLARE ET_HONOR_KL VARCHAR(7); \
DECLARE ET_FIX DECIMAL(3); \
 \
DECLARE STR_RATE_SEARCH VARCHAR(100); \
DECLARE STR_WHERE VARCHAR(4000); \
DECLARE STR_SQL VARCHAR(4000); \
 \
DECLARE STR_RATE_CAT VARCHAR(100); \
DECLARE N_FIXED_FEE_CLASS INTEGER = 0; \
DECLARE STR_FEE_CLASS VARCHAR(100)  = ''; \
DECLARE LOCAL_P_RATE_TYPE DECIMAL; \
DECLARE P_STATUS_CODE INTEGER; \
DECLARE P_STATUS_TEXT VARCHAR(1000); \
 \
DECLARE ETR_CODE		VARCHAR(82); \
DECLARE ETR_CODE_PROJ	VARCHAR(56); \
DECLARE ETR_CODE_FUNK	VARCHAR(14); \
DECLARE ETR_CODE_PERS	VARCHAR(14); \
DECLARE ETR_TARIF_KOST	DECIMAL(15,2); \
DECLARE ETR_TARIF_ZEIT	DECIMAL(15,2); \
DECLARE ETR_TARIF_AUFW	DECIMAL(15,2); \
DECLARE ETR_KOST_NULL	DECIMAL(3); \
DECLARE ETR_ZEIT_NULL	DECIMAL(3); \
DECLARE ETR_AUFW_NULL	DECIMAL(3); \
DECLARE RT_SEARCH_CODE		DECIMAL(15,2); \
DECLARE RT_SORT_CODE_EXT	DECIMAL(15,2); \
DECLARE RT_SORT_CODE_INT	DECIMAL(15,2); \
 \
DECLARE N_CALC_MODEL INTEGER =0; \
DECLARE N_RATE_FOUND INTEGER; \
DECLARE N_FC_TESTED INTEGER; \
DECLARE N_PRIOR_FEE_CLASS INTEGER; \
DECLARE N_LOOP_SIZE INTEGER; \
DECLARE N_LOOP_POS INTEGER; \
DECLARE STR_PRIOR_EXT VARCHAR(100) = '754612380'; \
DECLARE STR_PRIOR_INT VARCHAR(100) = '745162380'; \
DECLARE N_MEAN_RATE_CNT INTEGER; \
 \
BEGIN \
 \
/* SET DEFAULT PARAMETER VALUES */ \
 \
/* READ DEFAULTS */ \
SELECT FIRST 1 \
DEF_TARIFBERECHNUNG,DEF_TARIFKATEG, \
DEF_UZ1_ARB_ZEIT,DEF_UZ2_ARB_ZEIT,DEF_UZ3_ARB_ZEIT,DEF_UZ4_ARB_ZEIT,DEF_UZ5_ARB_ZEIT, \
DEF_UZ1_AUFWAND,DEF_UZ2_AUFWAND,DEF_UZ3_AUFWAND,DEF_UZ4_AUFWAND,DEF_UZ5_AUFWAND, \
DEF_UZ1_KOSTEN,DEF_UZ2_KOSTEN,DEF_UZ3_KOSTEN,DEF_UZ4_KOSTEN,DEF_UZ5_KOSTEN, \
DEF_UEBERST_FAKT1,DEF_UEBERST_FAKT2,DEF_UEBERST_FAKT3,DEF_UEBERST_FAKT4,DEF_UEBERST_FAKT5 \
FROM FL_DEFAULT INTO \
DEF_TARIFBERECHNUNG,DEF_TARIFKATEG, \
DEF_UZ1_ARB_ZEIT,DEF_UZ2_ARB_ZEIT,DEF_UZ3_ARB_ZEIT,DEF_UZ4_ARB_ZEIT,DEF_UZ5_ARB_ZEIT, \
DEF_UZ1_AUFWAND,DEF_UZ2_AUFWAND,DEF_UZ3_AUFWAND,DEF_UZ4_AUFWAND,DEF_UZ5_AUFWAND, \
DEF_UZ1_KOSTEN,DEF_UZ2_KOSTEN,DEF_UZ3_KOSTEN,DEF_UZ4_KOSTEN,DEF_UZ5_KOSTEN, \
DEF_UEBERST_FAKT1,DEF_UEBERST_FAKT2,DEF_UEBERST_FAKT3,DEF_UEBERST_FAKT4,DEF_UEBERST_FAKT5; /* READ PROJECT DATA */ \
SELECT PR_FAKTURART,PR_TARIFKATEGORIE,PR_TARIFBERECHNUNG,PR_FAKTOR FROM FPROJCT WHERE PR_CODE=:P_PROJECT INTO PR_FAKTURART,PR_TARIFKATEGORIE,PR_TARIFBERECHNUNG,PR_FAKTOR ; /* INIT RATE ROW: ADD ONE EMPTY ROW WITH EMPTY STRINGS AND ZEROS */ \
DELETE FROM TEMP_CALC_WORK_CHARGE_RATES; \
DELETE FROM TEMP_CALC_WORK_CHARGE_RATES_1; \
INSERT INTO TEMP_CALC_WORK_CHARGE_RATES (RATE,TIME_UNIT,RATE_TYPE,RATE_CAT,FEE_CLASS,RATE_SHORTCUT,FIXED) VALUES (0,0,0,'','','',0); \
 \
/* \
  ----------------------BUILD SPECIAL RATES LIST-------------------------------------------------------- \
*/ \
 \
/* DEFINE WHERE STATEMENT */ \
STR_WHERE = ' WHERE ETR_CODE = ' || '''' || RPAD(' ', 74, ' ') || ''''; \
 \
/* VARCHAR <> '' NE RADI */ \
 \
IF (CHAR_LENGTH(P_PROJECT)>0) THEN \
BEGIN \
  STR_WHERE = STR_WHERE || ' OR ETR_CODE = ' || '''' || RPAD(' ', 12, ' ')  || RPAD(' ', 12, ' ') || RPAD(:P_PROJECT, 50, ' ') || ''''; \
  IF (CHAR_LENGTH(P_FUNCTION)>0) THEN \
  BEGIN \
    STR_WHERE = STR_WHERE || ' OR ETR_CODE = ' || '''' || RPAD(:P_FUNCTION, 12, ' ')  || RPAD(' ', 12, ' ') || RPAD(:P_PROJECT, 50, ' ') || ''''; \
    IF (CHAR_LENGTH(P_PERSON)>0) THEN \
      STR_WHERE = STR_WHERE || ' OR ETR_CODE = ' || '''' || RPAD(:P_FUNCTION, 12, ' ')  || RPAD(:P_PERSON, 12, ' ') || RPAD(:P_PROJECT, 50, ' ') || ''''; \
  END \
  IF (CHAR_LENGTH(P_PERSON)>0) THEN \
    STR_WHERE = STR_WHERE || ' OR ETR_CODE = ' || '''' || RPAD(' ', 12, ' ')  || RPAD(:P_PERSON, 12, ' ') || RPAD(:P_PROJECT, 50, ' ') || ''''; \
END \
 \
IF (CHAR_LENGTH(P_FUNCTION)>0) THEN \
BEGIN \
  STR_WHERE = STR_WHERE || ' OR ETR_CODE = ' || '''' || RPAD(:P_FUNCTION, 12, ' ')  || RPAD(' ', 12, ' ') || RPAD(' ', 50, ' ') || ''''; \
  IF (CHAR_LENGTH(P_PERSON)>0) THEN \
    STR_WHERE = STR_WHERE || ' OR ETR_CODE = ' || '''' || RPAD(:P_FUNCTION, 12, ' ')  || RPAD(:P_PERSON, 12, ' ') || RPAD(' ', 50, ' ') || ''''; \
END \
 \
IF (CHAR_LENGTH(P_PERSON)>0) THEN \
  STR_WHERE = STR_WHERE || ' OR ETR_CODE = ' || '''' || RPAD(' ', 12, ' ')  || RPAD(:P_PERSON, 12, ' ') || RPAD(' ', 50, ' ') || ''''; STR_SQL = \
'INSERT INTO TEMP_CALC_WORK_CHARGE_RATES_1 \
(ETR_CODE,ETR_CODE_PROJ,ETR_CODE_FUNK,ETR_CODE_PERS,ETR_TARIF_KOST,ETR_TARIF_ZEIT,ETR_TARIF_AUFW,ETR_KOST_NULL,ETR_ZEIT_NULL,ETR_AUFW_NULL,RT_SEARCH_CODE,RT_SORT_CODE_EXT,RT_SORT_CODE_INT) \
SELECT ETR_CODE,ETR_CODE_PROJ,ETR_CODE_FUNK,ETR_CODE_PERS,ETR_TARIF_KOST,ETR_TARIF_ZEIT,ETR_TARIF_AUFW,ETR_KOST_NULL,ETR_ZEIT_NULL,ETR_AUFW_NULL, \
CASE \
(CASE WHEN (SUBSTRING(ETR_CODE FROM 1 FOR 12)<>RPAD('' '',12,'' '')) THEN ''1'' ELSE ''0'' END || \
CASE WHEN (SUBSTRING(ETR_CODE FROM 13 FOR 12)<>RPAD('' '',12,'' '')) THEN ''1'' ELSE ''0'' END || \
CASE WHEN (SUBSTRING(ETR_CODE FROM 25 FOR 50)<>RPAD('' '',50,'' '')) THEN ''1'' ELSE ''0'' END) \
WHEN ''000'' THEN 0 \
WHEN ''001'' THEN 1 \
WHEN ''010'' THEN 2 \
WHEN ''100'' THEN 3 \
WHEN ''011'' THEN 4 \
WHEN ''101'' THEN 5 \
WHEN ''110'' THEN 6 \
WHEN ''111'' THEN 7 \
END, \
IIF(CHAR_LENGTH(''' || P_PRIOR_EXT || ''')>0, \
CAST(SUBSTRING(''' || P_PRIOR_EXT || ''' FROM \
1+CASE \
(CASE WHEN (SUBSTRING(ETR_CODE FROM 1 FOR 12)<>RPAD('' '',12,'' '')) THEN ''1'' ELSE ''0'' END || \
CASE WHEN (SUBSTRING(ETR_CODE FROM 13 FOR 12)<>RPAD('' '',12,'' '')) THEN ''1'' ELSE ''0'' END || \
CASE WHEN (SUBSTRING(ETR_CODE FROM 25 FOR 50)<>RPAD('' '',50,'' '')) THEN ''1'' ELSE ''0'' END) \
WHEN ''000'' THEN 0 \
WHEN ''001'' THEN 1 \
WHEN ''010'' THEN 2 \
WHEN ''100'' THEN 3 \
WHEN ''011'' THEN 4 \
WHEN ''101'' THEN 5 \
WHEN ''110'' THEN 6 \
WHEN ''111'' THEN 7 \
END FOR 1) AS INTEGER),0), \
IIF(CHAR_LENGTH(''' || P_PRIOR_INT || ''')>0, \
CAST(SUBSTRING(''' || P_PRIOR_INT || ''' FROM \
1+CASE \
(CASE WHEN (SUBSTRING(ETR_CODE FROM 1 FOR 12)<>RPAD('' '',12,'' '')) THEN ''1'' ELSE ''0'' END || \
CASE WHEN (SUBSTRING(ETR_CODE FROM 13 FOR 12)<>RPAD('' '',12,'' '')) THEN ''1'' ELSE ''0'' END || \
CASE WHEN (SUBSTRING(ETR_CODE FROM 25 FOR 50)<>RPAD('' '',50,'' '')) THEN ''1'' ELSE ''0'' END) \
WHEN ''000'' THEN 0 \
WHEN ''001'' THEN 1 \
WHEN ''010'' THEN 2 \
WHEN ''100'' THEN 3 \
WHEN ''011'' THEN 4 \
WHEN ''101'' THEN 5 \
WHEN ''110'' THEN 6 \
WHEN ''111'' THEN 7 \
END FOR 1) AS INTEGER),0) \
FROM F_TARIFE' || STR_WHERE; \
 \
/*INSERT INTO TEST_TABLE VALUES (:STR_SQL);*/ \
 \
EXECUTE STATEMENT STR_SQL; \
 \
/* DETERMINE RATE CLASS */ \
IF (CHAR_LENGTH(PR_TARIFKATEGORIE)>0) THEN \
  EXECUTE PROCEDURE PROC_RESOLVE_RATE_CAT :PR_TARIFKATEGORIE, :P_DATE RETURNING_VALUES :STR_RATE_CAT; \
ELSE \
  EXECUTE PROCEDURE PROC_RESOLVE_RATE_CAT :DEF_TARIFKATEG, :P_DATE RETURNING_VALUES :STR_RATE_CAT; \
 \
/* DETERMINE FEE CLASS */ \
IF(PR_TARIFBERECHNUNG = 0) THEN \
BEGIN \
  SELECT EFK_CODE,EFK_HONOR_KL,EFK_FIX FROM F_FUNKTIONEN WHERE EFK_CODE = :P_FUNCTION INTO EFK_CODE,EFK_HONOR_KL,EFK_FIX; \
  IF (CHAR_LENGTH(EFK_HONOR_KL)>0) THEN \
  BEGIN \
      N_FIXED_FEE_CLASS = EFK_FIX; \
      STR_FEE_CLASS = EFK_HONOR_KL; \
  END \
END \
ELSE IF (PR_TARIFBERECHNUNG = 1) THEN \
BEGIN \
  SELECT ET_CODE,ET_HONOR_KL,ET_FIX FROM F_TAETIGKEITEN WHERE ET_CODE = :P_ACTIVITY INTO ET_CODE,ET_HONOR_KL,ET_FIX; \
  IF (CHAR_LENGTH(ET_HONOR_KL)>0) THEN \
  BEGIN \
      N_FIXED_FEE_CLASS = ET_FIX; \
      STR_FEE_CLASS = ET_HONOR_KL; \
  END \
END \
 \
/* START TO PROCESS */ \
LOCAL_P_RATE_TYPE = P_RATE_TYPE; \
IF (P_RATE_TYPE=0) THEN \
  IF (PR_FAKTURART<>0) THEN \
    LOCAL_P_RATE_TYPE =  1; \
  ELSE \
    LOCAL_P_RATE_TYPE =  2; /* INIT  RATE VARS */ \
UPDATE TEMP_CALC_WORK_CHARGE_RATES SET \
RATE_TYPE = :LOCAL_P_RATE_TYPE-1, \
TIME_UNIT=0, \
RATE_CAT='', \
FEE_CLASS='', \
FIXED=0; /*---------------------------------------------*/ \
/*            CALC MODEL BASED ON  FEE CLASSES AND RATE CATEGORIES \
/*---------------------------------------------*/ \
 \
IF  (N_CALC_MODEL = 0) THEN \
BEGIN \
	N_RATE_FOUND =0; \
	N_FC_TESTED = 0; \
	N_LOOP_POS = 1; \
 \
	N_PRIOR_FEE_CLASS = POSITION(IIF(P_EXT_INT=0, STR_PRIOR_EXT, STR_PRIOR_INT),'8'); \
	SELECT COUNT(*) FROM TEMP_CALC_WORK_CHARGE_RATES_1 INTO N_LOOP_SIZE; \
 \
	/*---------------------------------------------*/ \
	/*            START LOOP \
	/*---------------------------------------------*/ \
 \
	IF (N_LOOP_SIZE>0) THEN \
	BEGIN \
	 \
	  WHILE (N_LOOP_POS>N_LOOP_SIZE) DO \
	  BEGIN \
	  /* READ LINE BY LINE WITH EACH SQL /*/ \
	  IF (P_EXT_INT = 0) THEN \
	  BEGIN \
	    STR_SQL = 'SELECT FIRST ' || N_LOOP_POS || ' SKIP ' || (N_LOOP_POS-1) || 'ETR_CODE,ETR_CODE_PROJ,ETR_CODE_FUNK,ETR_CODE_PERS,ETR_TARIF_KOST,ETR_TARIF_ZEIT,ETR_TARIF_AUFW,ETR_KOST_NULL,ETR_ZEIT_NULL,ETR_AUFW_NULL,RT_SEARCH_CODE,RT_SORT_CODE_EXT,RT_SORT_CODE_INT \
		FROM TEMP_CALC_WORK_CHARGE_RATES_1 ORDER BY RT_SORT_CODE_EXT'; \
		EXECUTE STATEMENT STR_SQL INTO ETR_CODE,ETR_CODE_PROJ,ETR_CODE_FUNK,ETR_CODE_PERS,ETR_TARIF_KOST,ETR_TARIF_ZEIT,ETR_TARIF_AUFW,ETR_KOST_NULL,ETR_ZEIT_NULL,ETR_AUFW_NULL,RT_SEARCH_CODE,RT_SORT_CODE_EXT,RT_SORT_CODE_INT; \
   	  END \
	  ELSE \
	  BEGIN \
	    STR_SQL = 'SELECT FIRST ' || N_LOOP_POS || ' SKIP ' || (N_LOOP_POS-1) || 'ETR_CODE,ETR_CODE_PROJ,ETR_CODE_FUNK,ETR_CODE_PERS,ETR_TARIF_KOST,ETR_TARIF_ZEIT,ETR_TARIF_AUFW,ETR_KOST_NULL,ETR_ZEIT_NULL,ETR_AUFW_NULL,RT_SEARCH_CODE,RT_SORT_CODE_EXT,RT_SORT_CODE_INT \
		FROM TEMP_CALC_WORK_CHARGE_RATES_1 ORDER BY RT_SORT_CODE_INT'; \
		EXECUTE STATEMENT STR_SQL INTO ETR_CODE,ETR_CODE_PROJ,ETR_CODE_FUNK,ETR_CODE_PERS,ETR_TARIF_KOST,ETR_TARIF_ZEIT,ETR_TARIF_AUFW,ETR_KOST_NULL,ETR_ZEIT_NULL,ETR_AUFW_NULL,RT_SEARCH_CODE,RT_SORT_CODE_EXT,RT_SORT_CODE_INT; \
	  END \
 \
	  IF (IIF(P_EXT_INT=0, RT_SORT_CODE_EXT, RT_SORT_CODE_INT)>N_PRIOR_FEE_CLASS AND N_FC_TESTED=0) THEN \
	  BEGIN \
		N_FC_TESTED = 1; \
		EXECUTE PROCEDURE PROC_TEST_FEE_CLASS :P_EXT_INT, :LOCAL_P_RATE_TYPE, :P_PERSON, :P_PROJECT, :P_DATE, :P_FUNCTION, :P_ACTIVITY, :STR_RATE_CAT, :N_FIXED_FEE_CLASS, :STR_FEE_CLASS RETURNING_VALUES :N_RATE_FOUND; \
 \
		IF (N_RATE_FOUND<>0) THEN \
		BEGIN \
		  UPDATE TEMP_CALC_WORK_CHARGE_RATES SET RATE_SHORTCUT = 'R(FC)'; \
		  BREAK;/* EXIT LOOP */ \
		END \
	  END \
 \
	  IF (N_RATE_FOUND=0) THEN /* INTERNAL */ \
	  BEGIN \
		N_RATE_FOUND =1; \
		IF (P_EXT_INT=1) THEN \
		  IF (ETR_TARIF_AUFW<>0) THEN \
			UPDATE TEMP_CALC_WORK_CHARGE_RATES SET RATE = :ETR_TARIF_AUFW; \
		  ELSE IF (ETR_AUFW_NULL<>0) THEN \
			UPDATE TEMP_CALC_WORK_CHARGE_RATES SET RATE = 0; \
		  ELSE \
			N_RATE_FOUND =0; \
	  END \
	  ELSE /* EXTERNAL */ \
	  BEGIN \
 \
		IF (IIF((LOCAL_P_RATE_TYPE-1)=0, ETR_TARIF_KOST, ETR_TARIF_ZEIT )<>0) THEN \
		  UPDATE TEMP_CALC_WORK_CHARGE_RATES SET RATE = (IIF( (:LOCAL_P_RATE_TYPE-1)=0, :ETR_TARIF_KOST, :ETR_TARIF_ZEIT)); \
		ELSE IF (IIF( (LOCAL_P_RATE_TYPE-1)=0, ETR_KOST_NULL, ETR_ZEIT_NULL)<>0) THEN \
		  UPDATE TEMP_CALC_WORK_CHARGE_RATES SET RATE = 0; \
		ELSE \
		  N_RATE_FOUND =0; \
 \
	  END \
 \
	  IF (N_RATE_FOUND<>0) THEN \
	  BEGIN \
		UPDATE TEMP_CALC_WORK_CHARGE_RATES SET RATE_SHORTCUT = CASE :RT_SEARCH_CODE \
		WHEN 0 THEN 'R()' \
		WHEN 1 THEN 'R(PRJ)' \
		WHEN 2 THEN 'R(PRS)' \
		WHEN 3 THEN 'R(FNC)' \
		WHEN 4 THEN 'R(PRS,PRJ)' \
		WHEN 5 THEN 'R(FNC,PRJ)' \
		WHEN 6 THEN 'R(PRS,FNC)' \
		WHEN 7 THEN 'R(PRS,PRJ,FNC)' \
		END; \
 \
		BREAK; /* EXIT LOOP */ \
	  END \
 \
		N_LOOP_POS = N_LOOP_POS+1; \
 \
	  END /* END OF LOOP */ \
	END \
 \
	/*---------------------------------------------*/ \
	/*            END LOOP \
	/*---------------------------------------------*/ 	IF (N_FC_TESTED=0 AND N_RATE_FOUND=0) THEN \
	BEGIN \
	  EXECUTE PROCEDURE PROC_TEST_FEE_CLASS :P_EXT_INT, :LOCAL_P_RATE_TYPE, :P_PERSON, :P_PROJECT, :P_DATE, :P_FUNCTION, :P_ACTIVITY, :STR_RATE_CAT, :N_FIXED_FEE_CLASS, :STR_FEE_CLASS RETURNING_VALUES :N_RATE_FOUND; \
	  IF (N_RATE_FOUND<>0) THEN \
		UPDATE TEMP_CALC_WORK_CHARGE_RATES SET RATE_SHORTCUT = 'R(FC)'; \
	END \
 \
	/* PROJECT DEFINED MULTIPLIER */ \
	IF (P_EXT_INT=0) THEN \
	  UPDATE TEMP_CALC_WORK_CHARGE_RATES SET RATE = (SELECT FIRST 1 RATE FROM TEMP_CALC_WORK_CHARGE_RATES)*:PR_FAKTOR; \
 \
	P_STATUS_CODE =0; \
	P_STATUS_TEXT =''; \
END \
ELSE \
BEGIN \
/*---------------------------------------------*/ \
/*            CALC MODEL BASED ON GLOBAL OR PERSONAL MEAN RATE \
/*---------------------------------------------*/ \
 \
  /* READ MEAN RATE AND COUNT */ \
  SELECT FIRST 1 ETR_CODE_PERS,ETR_TARIF_KOST,ETR_TARIF_ZEIT,ETR_TARIF_AUFW,ETR_KOST_NULL,ETR_ZEIT_NULL,ETR_AUFW_NULL \
  FROM F_TARIFE WHERE ETR_CODE_PERS = :P_PERSON AND ETR_CODE_PROJ = '' AND ETR_CODE_FUNK = '' \
  INTO ETR_CODE_PERS,ETR_TARIF_KOST,ETR_TARIF_ZEIT,ETR_TARIF_AUFW,ETR_KOST_NULL,ETR_ZEIT_NULL,ETR_AUFW_NULL; \
 \
  SELECT COUNT(*) FROM F_TARIFE WHERE ETR_CODE_PERS = :P_PERSON AND ETR_CODE_PROJ = '' AND ETR_CODE_FUNK = '' INTO N_MEAN_RATE_CNT; \
 \
  IF (N_MEAN_RATE_CNT=0) THEN /* NOT FOUND */ \
  BEGIN \
    IF (N_CALC_MODEL = 1) THEN \
	BEGIN \
        P_STATUS_CODE = 1; \
		P_STATUS_TEXT = 'NO PERSONAL MEAN RATE T(M) DEFINED!'; \
	END \
    ELSE IF (N_CALC_MODEL = 2) THEN \
	BEGIN \
        P_STATUS_CODE =1; \
		P_STATUS_TEXT = 'NO GLOBAL MEAN RATE RT DEFINED!'; \
    END \
 \
      /* PROJECT DEFINED MULTIPLIER */ \
    IF (P_EXT_INT=0) THEN \
      UPDATE TEMP_CALC_WORK_CHARGE_RATES SET RATE = (SELECT FIRST 1 RATE FROM TEMP_CALC_WORK_CHARGE_RATES)*:PR_FAKTOR; \
 \
  END \
  ELSE \
  BEGIN \
 \
    IF (P_EXT_INT=0) THEN \
	BEGIN \
      UPDATE TEMP_CALC_WORK_CHARGE_RATES SET RATE = (IIF((:LOCAL_P_RATE_TYPE-1)=0, :ETR_TARIF_KOST, :ETR_TARIF_ZEIT)); \
      UPDATE TEMP_CALC_WORK_CHARGE_RATES SET RATE_TYPE = :LOCAL_P_RATE_TYPE-1; \
	END \
    ELSE IF (P_EXT_INT=1) THEN \
	BEGIN \
      UPDATE TEMP_CALC_WORK_CHARGE_RATES SET RATE = :ETR_TARIF_AUFW; \
      UPDATE TEMP_CALC_WORK_CHARGE_RATES SET RATE_TYPE = 0; \
    END \
 \
    UPDATE TEMP_CALC_WORK_CHARGE_RATES SET \
    TIME_UNIT=0, \
    RATE_CAT='', \
    FEE_CLASS='', \
    RATE_SHORTCUT= (CASE WHEN (:N_CALC_MODEL-1=0) THEN 'R(PERS)' ELSE 'R()' END); \
 \
  END END \
END  \
";

dboList["00015,PROC_CALC_UNCUMULATED_BF"]="RECREATE PROCEDURE  PROC_CALC_UNCUMULATED_BF \
( \
  PV_PROJECT VARCHAR(100), \
  PV_PERIOD_SERIES VARCHAR(3000), /*STRING WITH PAIRS OF DATES SEPARATED BY A COMMA IN THE FORMAT DD.MM.YYYY. EXAMPLE: 01.01.2011,31.01.2011,01.02.2011,28.02.2011,01.03.2011,31.3.2011*/ \
  P_INCLUDE_SP INTEGER, \
  P_ABT_EQUAL INTEGER, \
  P_DEPT VARCHAR(100) \
) AS \
 \
DECLARE STR_DATE_FROM VARCHAR(100); \
DECLARE STR_DATE_TO VARCHAR(100); \
DECLARE DATE_FROM DATE; \
DECLARE DATE_TO DATE; \
DECLARE N_CNT INTEGER =0; \
DECLARE N_POS_END INTEGER =1; \
DECLARE N_POS_START INTEGER =1; \
 \
DECLARE STR_PROJECT_CODE VARCHAR(100); \
DECLARE ABT VARCHAR(100) = ''; \
DECLARE FV_DEF_KUM_NO_INAKT INTEGER; \
DECLARE STR_SQL VARCHAR(4000); \
DECLARE STR_WHERE VARCHAR(2000); \
 \
BEGIN \
 \
/*--------------------- \
MB: ASSMEBLE PROJECT LIST THEN HIT ON 'EM \
--------------------*/ \
 \
/* PREPARE VALUES FOR SELECT */ \
  SELECT FIRST 1 DEF_KUM_NO_INAKT FROM FL_DEFAULT INTO FV_DEF_KUM_NO_INAKT; \
  IF (P_ABT_EQUAL = 0) THEN \
    ABT=''; \
  ELSE IF (P_ABT_EQUAL = 1) THEN \
    SELECT PR_ABTEILUNG FROM FPROJCT WHERE PR_CODE=:PV_PROJECT INTO ABT; \
  ELSE IF (P_ABT_EQUAL = 2) THEN \
    ABT=P_DEPT; \
  ELSE \
    ABT=''; \
 \
 /* WHERE STATEMENTS */ \
  IF (P_INCLUDE_SP<>0) THEN \
	STR_WHERE =' PR_CODE BETWEEN ''' || PV_PROJECT || ''' AND ''' || PV_PROJECT || '~' || ''''; \
  ELSE \
	STR_WHERE =' PR_CODE = ''' || PV_PROJECT || ''''; \
 \
  IF (ABT<>'') THEN \
    STR_WHERE = STR_WHERE || ' AND ''' || ABT || '''= SUBSTRING(PR_ABTEILUNG FROM 1 FOR CHAR_LENGTH(''' || ABT || '''))'; \
 \
  IF (FV_DEF_KUM_NO_INAKT<>0) THEN \
   STR_WHERE = STR_WHERE ||' AND PR_AKTIV = 1'; \
  \
 STR_SQL = 'INSERT INTO TEMP_LST_PROJECTS(PROJ_CODE) SELECT PR_CODE FROM FPROJCT WHERE ' || STR_WHERE; \
  \
 DELETE FROM TEMP_LST_PROJECTS; \
 EXECUTE STATEMENT STR_SQL; \
  \
 DELETE FROM TEMP_CHARGE_CUMULATION; \
 DELETE FROM TEMP_FAKTURA_CUMULATION; \
 DELETE FROM TEMP_PAYMENT_CUMULATION; \
 DELETE FROM TEMP_LEVY_CUMULATION; \
 \
  FOR SELECT PROJ_CODE FROM TEMP_LST_PROJECTS INTO :STR_PROJECT_CODE \
  DO \
  BEGIN \
     \
	N_CNT =0; \
	N_POS_END  =1; \
	N_POS_START =1; \
 \
	/* LOOP BY PROJECT */ \
	WHILE (N_POS_END<CHAR_LENGTH(PV_PERIOD_SERIES)) DO \
	BEGIN \
 \
	  N_POS_END = POSITION (',',PV_PERIOD_SERIES, N_POS_START); \
	   \
	  IF (N_POS_END = 0 OR N_POS_END = 1) THEN \
		N_POS_END = CHAR_LENGTH(PV_PERIOD_SERIES)+1; \
		 \
	  IF (N_POS_END<=N_POS_START) THEN \
		BREAK; \
		 \
	  STR_DATE_FROM = SUBSTRING(PV_PERIOD_SERIES FROM N_POS_START FOR N_POS_END-N_POS_START); \
		 \
	  N_POS_START = N_POS_END+1; \
 \
	  N_POS_END = POSITION ( ',', PV_PERIOD_SERIES, N_POS_START); \
	  IF (N_POS_END = 0 OR N_POS_END = 1) THEN \
		N_POS_END = CHAR_LENGTH(PV_PERIOD_SERIES)+1; \
		 \
	  IF (N_POS_END<=N_POS_START) THEN \
		BREAK; \
		 \
	  STR_DATE_TO = SUBSTRING(PV_PERIOD_SERIES FROM N_POS_START FOR N_POS_END-N_POS_START); \
	   \
	  N_POS_START = N_POS_END+1; \
 \
	  DATE_FROM = CAST (STR_DATE_FROM AS DATE); \
	  DATE_TO = CAST (STR_DATE_TO AS DATE); \
	   \
	  EXECUTE PROCEDURE PROC_CHARGE_CUMULATION :STR_PROJECT_CODE,:DATE_FROM, :DATE_TO, 0,0,0,0,0,0,'',0; \
	  EXECUTE PROCEDURE PROC_FAKTURA_CUMULATION :STR_PROJECT_CODE,:DATE_FROM, :DATE_TO, 0,0,'',0; \
	  EXECUTE PROCEDURE PROC_PAYMENT_CUMULATION :STR_PROJECT_CODE,:DATE_FROM, :DATE_TO, 0,0,'',0; \
	  EXECUTE PROCEDURE PROC_LEVY_CUMULATION :STR_PROJECT_CODE,:DATE_FROM, :DATE_TO, 0,0,'',0; \
	END \
	 \
 END \
END  \
";


dboList["00016,PROC_SUM_REST_CORRECTION"]="RECREATE PROCEDURE  PROC_SUM_REST_CORRECTION \
										   ( \
										   PV_PROJECT VARCHAR(100), \
										   PV_TO DATE, \
										   P_INCLUDE_SP INTEGER, \
										   P_ABT_EQUAL INTEGER, \
										   P_DEPT VARCHAR(100) \
										   ) AS \
										   \
										   DECLARE LV_DEF_KUM_NO_INAKT INTEGER; \
										   DECLARE ABT VARCHAR(100) = ''; \
										   DECLARE LV_PROJECT2 VARCHAR(100); \
										   DECLARE STR_SQL VARCHAR(4000); \
										   DECLARE STR_WHERE VARCHAR(2000); \
										   \
										   BEGIN \
										   \
										   /* PREPARE VALUES FOR SELECT */ \
										   SELECT FIRST 1 DEF_KUM_NO_INAKT FROM FL_DEFAULT INTO LV_DEF_KUM_NO_INAKT; \
										   \
										   /* WHERE STATEMENT */ \
										   IF (P_ABT_EQUAL = 0) THEN \
										   ABT=''; \
										   ELSE IF (P_ABT_EQUAL = 1) THEN \
										   SELECT PR_ABTEILUNG FROM FPROJCT WHERE PR_CODE=:PV_PROJECT INTO ABT; \
										   ELSE IF (P_ABT_EQUAL = 2) THEN \
										   ABT=P_DEPT; \
										   ELSE \
										   ABT=''; \
										   \
										   IF (P_INCLUDE_SP = 1) THEN \
										   LV_PROJECT2 = PV_PROJECT || '~'; \
										   \
										   /* WHERE STATEMENTS */ \
										   IF (P_INCLUDE_SP<>0) THEN \
										   STR_WHERE =' AND ERA_PROJ_NR BETWEEN ''' || PV_PROJECT || ''' AND ''' || LV_PROJECT2|| ''''; \
										   ELSE \
										   STR_WHERE =' AND ERA_PROJ_NR = ''' || PV_PROJECT || ''''; \
										   \
										   IF (ABT<>'') THEN \
										   STR_WHERE = STR_WHERE || ' AND ''' || ABT || '''= SUBSTRING(FPROJCT.PR_ABTEILUNG FROM 1 FOR CHAR_LENGTH(''' || ABT || '''))'; \
										   \
										   IF (LV_DEF_KUM_NO_INAKT=1) THEN \
										   STR_WHERE = STR_WHERE ||' AND FPROJCT.PR_AKTIV = 1 '; \
										   \
										   /* IF INVALID TO DATE, THEN SET HIGH IN FUTURE: GET ALL RECORDS */ \
										   IF (PV_TO <= CAST ('1980-01-01' AS DATE)) THEN \
										   PV_TO = CAST ('2200-01-01' AS DATE); \
										   \
										   DELETE FROM TEMP_REST_CORRECTION; \
										   \
										   STR_SQL = \
										   'INSERT INTO TEMP_REST_CORRECTION( \
										   PROJ_CODE, \
										   DATE_FROM, \
										   DATE_TO, \
										   TB_B_KORR_H, \
										   TB_B_KORR_V, \
										   TB_B_KORR_N, \
										   TB_B_KORR_F, \
										   TB_B_KORR_A, \
										   TB_B_KORR_Z, \
										   TB_B_KORR_VN, \
										   TB_B_KORR_VF, \
										   TB_N_RK \
										   ) \
										   SELECT ''' || PV_PROJECT || ''', ''1980-01-01'', ''' || PV_TO || ''',  \
										   SUM(ERA_KORR_VERR_HS), \
										   SUM(ERA_KORR_VERR_BT), \
										   SUM(ERA_KORR_AUFW_NK), \
										   SUM(ERA_KORR_AUFW_FK), \
										   SUM(ERA_KORR_AUFW_A), \
										   SUM(ERA_KORR_ZEIT), \
										   SUM(ERA_KORR_VERR_NK), \
										   SUM(ERA_KORR_VERR_FK), \
										   COUNT(ERA_SEQUENCE) \
										   FROM F_PROJ_RESTAUFWAND \
										   INNER JOIN FPROJCT ON PR_CODE=ERA_PROJ_NR \
										   WHERE ERA_DATUM <= ''' || PV_TO || ''' ' || STR_WHERE || \
										   ' PLAN (F_PROJ_RESTAUFWAND INDEX(IND_ERA_CI_PROJ_NR_DATE), FPROJCT INDEX(IND_PR_CODE))'; \
										   \
										   EXECUTE STATEMENT STR_SQL; END  \
										   \
										   ";

dboList["00017,PROC_SUM_BUDGET_CORRECTION"]="RECREATE PROCEDURE  PROC_SUM_BUDGET_CORRECTION \
											 ( \
											 PV_PROJECT VARCHAR(100), \
											 PV_TO DATE, \
											 P_INCLUDE_SP INTEGER, \
											 P_ABT_EQUAL INTEGER, \
											 P_DEPT VARCHAR(100), \
											 PV_KUMART INTEGER, \
											 PV_KUM_STUFE INTEGER, \
											 PV_KUM_PA VARCHAR(100) \
											 ) AS \
											 \
											 DECLARE LV_DEF_KUM_NO_INAKT INTEGER; \
											 DECLARE ABT VARCHAR(100) = ''; \
											 DECLARE LV_PROJECT2 VARCHAR(100); \
											 DECLARE STR_SQL VARCHAR(4000); \
											 DECLARE STR_CALC VARCHAR(4000); \
											 DECLARE STR_WHERE VARCHAR(2000); \
											 \
											 BEGIN \
											 \
											 /* PREPARE VALUES FOR SELECT */ \
											 SELECT FIRST 1 DEF_KUM_NO_INAKT FROM FL_DEFAULT INTO LV_DEF_KUM_NO_INAKT; \
											 \
											 /* WHERE STATEMENT */ \
											 IF (P_ABT_EQUAL = 0) THEN \
											 ABT=''; \
											 ELSE IF (P_ABT_EQUAL = 1) THEN \
											 SELECT PR_ABTEILUNG FROM FPROJCT WHERE PR_CODE=:PV_PROJECT INTO ABT; \
											 ELSE IF (P_ABT_EQUAL = 2) THEN \
											 ABT=P_DEPT; \
											 ELSE \
											 ABT=''; \
											 \
											 IF (P_INCLUDE_SP = 1) THEN \
											 LV_PROJECT2 = PV_PROJECT || '~'; \
											 \
											 /* WHERE STATEMENTS */ \
											 IF (P_INCLUDE_SP<>0) THEN \
											 STR_WHERE =' AND EBK_PROJ_NR BETWEEN ''' || PV_PROJECT || ''' AND ''' || LV_PROJECT2|| ''''; \
											 ELSE \
											 STR_WHERE =' AND EBK_PROJ_NR = ''' || PV_PROJECT || ''''; \
											 \
											 IF (ABT<>'') THEN \
											 STR_WHERE = STR_WHERE || ' AND ''' || ABT || '''= SUBSTRING(FPROJCT.PR_ABTEILUNG FROM 1 FOR CHAR_LENGTH(''' || ABT || '''))'; \
											 \
											 IF (LV_DEF_KUM_NO_INAKT=1) THEN \
											 STR_WHERE = STR_WHERE ||' AND FPROJCT.PR_AKTIV = 1 '; \
											 \
											 /* IF INVALID TO DATE, THEN EXIT */ \
											 IF (PV_TO <= CAST ('1980-01-01' AS DATE)) THEN \
											 EXIT; \
											 \
											 DELETE FROM TEMP_BUDGET_CORRECTION; \
											 \
											 IF(PV_KUMART=0 OR PV_KUMART=1) THEN \
											 BEGIN \
											 STR_CALC =  \
											 'SUM(CASE WHEN (PR_HONSUM_PASSIV=0) THEN EBK_KORR_VERR_A ELSE 0 END), \
											 SUM(CASE WHEN (PR_BUDGETKS_PASSIV=0) THEN EBK_KORR_VERR_NK ELSE 0 END), \
											 SUM(CASE WHEN (PR_FREMDAUFW_PASSIV=0) THEN EBK_KORR_VERR_FK ELSE 0 END), \
											 SUM(CASE WHEN (PR_BUDGET_PASSIV=0) THEN EBK_KORR_VERR_BT ELSE 0 END), \
											 SUM(CASE WHEN (PR_AUFW_NK_BUD_PASS=0) THEN EBK_KORR_AUFW_NK ELSE 0 END), \
											 SUM(CASE WHEN (PR_AUFW_HON_BUD_PASS=0) THEN EBK_KORR_AUFW_A ELSE 0 END), \
											 SUM(CASE WHEN (PR_AUFW_FK_BUD_PASS=0) THEN EBK_KORR_AUFW_FK ELSE 0 END), \
											 SUM(CASE WHEN (PR_BUDGET_ZEIT_PASSIV=0) THEN EBK_KORR_ZEIT ELSE 0 END)'; \
											 END \
											 ELSE IF (PV_KUMART=2) THEN \
											 BEGIN \
											 STR_CALC =  \
											 'SUM(EBK_KORR_VERR_A), \
											 SUM(EBK_KORR_VERR_NK), \
											 SUM(EBK_KORR_VERR_FK), \
											 SUM(EBK_KORR_VERR_BT), \
											 SUM(EBK_KORR_AUFW_NK), \
											 SUM(EBK_KORR_AUFW_A), \
											 SUM(EBK_KORR_AUFW_FK), \
											 SUM(EBK_KORR_ZEIT)'; \
											 \
											 STR_WHERE = STR_WHERE ||' AND FPROJCT.PR_STUFE=' || PV_KUM_STUFE; \
											 END \
											 ELSE IF (PV_KUMART=3) THEN \
											 BEGIN \
											 STR_CALC =  \
											 'SUM(EBK_KORR_VERR_A), \
											 SUM(EBK_KORR_VERR_NK), \
											 SUM(EBK_KORR_VERR_FK), \
											 SUM(EBK_KORR_VERR_BT), \
											 SUM(EBK_KORR_AUFW_NK), \
											 SUM(EBK_KORR_AUFW_A), \
											 SUM(EBK_KORR_AUFW_FK), \
											 SUM(EBK_KORR_ZEIT)'; \
											 \
											 STR_WHERE = STR_WHERE ||' AND FPROJCT.PR_PROJEKTART=''' || PV_KUM_PA || ''' '; \
											 END \
											 \
											 \
											 STR_SQL = \
											 'INSERT INTO TEMP_BUDGET_CORRECTION( \
											 PROJ_CODE, \
											 DATE_FROM, \
											 DATE_TO, \
											 TB_B_BUD_H, \
											 TB_B_BUD_NH, \
											 TB_B_BUD_FH, \
											 TB_B_BUD_V, \
											 TB_B_BUD_N, \
											 TB_B_BUD_A, \
											 TB_B_BUD_F, \
											 TB_B_BUD_Z \
											 ) \
											 SELECT ''' || PV_PROJECT || ''', ''1980-01-01'', ''' || PV_TO || ''', ' || STR_CALC || \
											 ' FROM F_PROJ_BUDGETKORREKTUR \
											 INNER JOIN FPROJCT ON PR_CODE=EBK_PROJ_NR \
											 WHERE EBK_DATUM <= ''' || PV_TO || ''' ' || STR_WHERE || \
											 ' PLAN (F_PROJ_BUDGETKORREKTUR INDEX(IND_EBK_CI_PROJ_NR_DATE), FPROJCT INDEX(IND_PR_CODE))'; \
											 \
											 EXECUTE STATEMENT STR_SQL; END  \
											 \
											 ";




	m_ProceduresSPC[DBTYPE_FIREBIRD]=dboList;
}


