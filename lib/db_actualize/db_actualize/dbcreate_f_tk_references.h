#ifndef DBCREATE_F_TK_REFERENCES_H
#define DBCREATE_F_TK_REFERENCES_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_TK_REFERENCES : public DbSqlTableCreation
{
public: 
	DbCreate_F_TK_REFERENCES():DbSqlTableCreation(F_TK_REFERENCES){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_TK_REFERENCES_H
