#include "dbcreate_bus_cm_address_schemas.h"


//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_CM_ADDRESS_SCHEMAS::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
											create table BUS_CM_ADDRESS_SCHEMAS ( \
											BCMAS_ID INTEGER not null,\
											BCMAS_GLOBAL_ID VARCHAR(15) null,\
											BCMAS_DAT_LAST_MODIFIED TIMESTAMP not null,\
											BCMAS_SCHEMA_NAME VARCHAR(100) not null,\
											BCMAS_SCHEMA LONGVARCHAR not null, constraint BUS_CM_ADDRESS_SCHEMAS_PK primary key (BCMAS_ID) )  ");
}


//probably common to all DB's:
void DbCreate_BUS_CM_ADDRESS_SCHEMAS::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
	dboList["IND_BCMAS_SCHEMA_NAME"] = "create index IND_BCMAS_SCHEMA_NAME on BUS_CM_ADDRESS_SCHEMAS (BCMAS_SCHEMA_NAME)";  
}



