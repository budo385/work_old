#include "dbcreate_f_waehrung.h"


//--------------------------------------------------
// ODBC - CREATE
//--------------------------------------------------
void DbCreate_F_WAEHRUNG::DefineFields(QStringList &lstSQL) 
{
	//Table schema statements for ODBC.
	lstSQL.append(" 	CREATE TABLE F_WAEHRUNG (\
										FW_SEQUENCE INTEGER  NOT NULL,\
										FW_DATUM DATETIME ,FW_NAME VARCHAR(45)  NOT NULL,\
										FW_W_CODE VARCHAR(9)  NOT NULL,FW_REF_FREMD DECIMAL NOT NULL,\
										FW_FREMD_REF DECIMAL NOT NULL,FW_VALID_SINCE DATETIME ,\
										FW_EXPIRE_DATE DATETIME ,FW_GLOBAL_ID VARCHAR(14)  NOT NULL,\
										FW_DAT_AENDERUNG DATETIME ,FW_DAT_EROEFFNUNG DATETIME ,\
										FW_MODIFIED_BY VARCHAR(36)  NOT NULL,FW_CODE_BH VARCHAR(12)  NOT NULL,\
										constraint F_WAEHRUNG_PK primary key (FW_SEQUENCE))	");

}

void DbCreate_F_WAEHRUNG::DefineIndexes(SQLDBObjectCreate &dboList) 
{
		//add index definition by its name (important).
		dboList["IND_FW_DATUM"] = "CREATE INDEX IND_FW_DATUM ON F_WAEHRUNG (FW_DATUM)";
		dboList["IND_FW_W_CODE"] = "CREATE INDEX IND_FW_W_CODE ON F_WAEHRUNG (FW_W_CODE)";
		dboList["IND_FW_GLOBAL_ID"] = "CREATE INDEX IND_FW_GLOBAL_ID ON F_WAEHRUNG (FW_GLOBAL_ID)";
		dboList["IND_FW_DAT_AENDERUNG"] = "CREATE INDEX IND_FW_DAT_AENDERUNG ON F_WAEHRUNG (FW_DAT_AENDERUNG)";
		dboList["IND_FW_EXPIRE_DATE"] = "CREATE INDEX IND_FW_EXPIRE_DATE ON F_WAEHRUNG (FW_EXPIRE_DATE)";
		dboList["IND_FW_VALID_SINCE"] = "CREATE INDEX IND_FW_VALID_SINCE ON F_WAEHRUNG (FW_VALID_SINCE)";

}

void DbCreate_F_WAEHRUNG::DefineViews(SQLDBObjectCreate &dboList) 
{


	dboList["VW_F_WAEHRUNG"] = "CREATE VIEW VW_F_WAEHRUNG AS SELECT FW_SEQUENCE,FW_DATUM,FW_NAME,FW_W_CODE ,FW_REF_FREMD,FW_FREMD_REF,FW_VALID_SINCE,FW_EXPIRE_DATE ,FW_GLOBAL_ID,FW_DAT_AENDERUNG,FW_DAT_EROEFFNUNG,FW_MODIFIED_BY ,FW_CODE_BH FROM F_WAEHRUNG";

	dboList["VWS_F_WAEHRUNG"] = "CREATE VIEW VWS_F_WAEHRUNG AS SELECT FW_SEQUENCE,FW_DATUM,FW_NAME,FW_W_CODE ,FW_REF_FREMD,FW_FREMD_REF,FW_VALID_SINCE,FW_EXPIRE_DATE ,FW_GLOBAL_ID,FW_DAT_AENDERUNG,FW_DAT_EROEFFNUNG,FW_MODIFIED_BY ,FW_CODE_BH FROM F_WAEHRUNG";
}

void DbCreate_F_WAEHRUNG::DefineTriggers(SQLDBObjectCreate &dboList) 
{
	if(m_DbType==DBTYPE_FIREBIRD)
	{

		dboList["TR_F_WAEHRUNG"] = "CREATE TRIGGER TR_F_WAEHRUNG FOR F_WAEHRUNG BEFORE INSERT OR UPDATE AS BEGIN \
IF(NEW.FW_REF_FREMD IS NULL) THEN NEW.FW_REF_FREMD=0;\
IF(NEW.FW_FREMD_REF IS NULL) THEN NEW.FW_FREMD_REF=0;\
IF(NEW.FW_NAME IS NULL) THEN NEW.FW_NAME='';\
IF(NEW.FW_W_CODE IS NULL) THEN NEW.FW_W_CODE='';\
IF(NEW.FW_GLOBAL_ID IS NULL) THEN NEW.FW_GLOBAL_ID='';\
IF(NEW.FW_MODIFIED_BY IS NULL) THEN NEW.FW_MODIFIED_BY='';\
IF(NEW.FW_CODE_BH IS NULL) THEN NEW.FW_CODE_BH='';\
END;";
	}
}
