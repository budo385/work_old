#include "dbcreate_core_userrole.h"


//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_CORE_USERROLE::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
											create table CORE_USERROLE (				\
											CURL_ID INTEGER not null,					\
											CURL_GLOBAL_ID VARCHAR(15) null,			\
											CURL_DAT_LAST_MODIFIED TIMESTAMP not null,	\
											CURL_USER_ID INTEGER not null,				\
											CURL_ROLE_ID INTEGER not null,				\
											constraint CORE_USERROLE_PK primary key (CURL_ID) )");

	
	lstSQL.append("alter table CORE_USERROLE					\
								   add constraint CORE_USER_CORE_USERROLE_FK1	\
								   foreign key (CURL_USER_ID)					\
								   references CORE_USER (CUSR_ID) ON DELETE CASCADE");

	lstSQL.append("alter table CORE_USERROLE					\
								   add constraint CORE_ROLE_CORE_USERROLE_FK1	\
								   foreign key (CURL_ROLE_ID)					\
								   references CORE_ROLE (CROL_ID) ON DELETE CASCADE");
	
}

//probably common to all DB's:
void DbCreate_CORE_USERROLE::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
	dboList["IND_CURL_USER_ID"] = "create index IND_CURL_USER_ID on CORE_USERROLE (CURL_USER_ID)";
	dboList["IND_CURL_ROLE_ID"] = "create index IND_CURL_ROLE_ID on CORE_USERROLE (CURL_ROLE_ID)";
}


