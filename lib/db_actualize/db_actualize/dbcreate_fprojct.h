#ifndef DBCREATE_FPROJCT_H
#define DBCREATE_FPROJCT_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_FPROJCT : public DbSqlTableCreation
{
public: 
	DbCreate_FPROJCT():DbSqlTableCreation(FPROJCT){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // FPROJCT_H
