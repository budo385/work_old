#ifndef DBCREATE_BUS_SERVER_FILE_H
#define DBCREATE_BUS_SERVER_FILE_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_BUS_DM_SERVER_FILE : public DbSqlTableCreation
{
public:
	DbCreate_BUS_DM_SERVER_FILE():DbSqlTableCreation(BUS_DM_SERVER_FILE){};  //init table
	
private:
	//create generic Table def:
	void DefineFields(QStringList &lstSQL);

	//probably common to all DB's:
	void DefineIndexes(SQLDBObjectCreate &dboList);
};


#endif // DBCREATE_BUS_SERVER_FILE_H
