#include "dbcreate_bus_cal_view_ce_types.h"

//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_CAL_VIEW_CE_TYPES::DefineFields(QStringList &lstSQL)
{
	lstSQL.append("\
								   create table BUS_CAL_VIEW_CE_TYPES (			\
								   BCCT_ID INTEGER not null,					\
								   BCCT_GLOBAL_ID VARCHAR(15) null,				\
								   BCCT_DAT_LAST_MODIFIED TIMESTAMP not null,	\
								   BCCT_CAL_VIEW_ID INTEGER null,			\
								   BCCT_TYPE_ID INTEGER not null,				\
								   BCCT_IS_SELECTED SMALLINT not null,			\
								   constraint BUS_CAL_VIEW_CE_TYPES_PK primary key (BCCT_ID))");

	lstSQL.append("\
								   alter table BUS_CAL_VIEW_CE_TYPES\
								   add constraint CALVIEWCE_TYPES_CAL_VIEW_FK1 foreign key (\
								   BCCT_CAL_VIEW_ID)\
								   references BUS_CAL_VIEW (\
								   BCALV_ID) ON DELETE CASCADE");	

	lstSQL.append("\
								   alter table BUS_CAL_VIEW_CE_TYPES\
								   add constraint CALVIEWCE_TYPES_CMTYPES_FK1 foreign key (\
								   BCCT_TYPE_ID)\
								   references  BUS_CM_TYPES (\
								   BCMT_ID) ON DELETE CASCADE");	

	//create indexes

	//Delete column.
	//m_lstDeletedCols.append("BUSCV_IS_PRIVATE");

	SQLDBObjectCreate dboList;
	DefineIndexes(dboList);
	m_Indexes[DBTYPE_ODBC]=dboList;
	dboList.clear();

	//create triggers
	m_Triggers[DBTYPE_ODBC]=dboList;
}

void DbCreate_BUS_CAL_VIEW_CE_TYPES::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList["IND_BCCT_CAL_VIEW_ID"] = "create index IND_BCCT_CAL_VIEW_ID on BUS_CAL_VIEW_CE_TYPES (BCCT_CAL_VIEW_ID)";  
}
