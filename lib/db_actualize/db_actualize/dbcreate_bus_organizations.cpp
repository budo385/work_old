#include "dbcreate_bus_organizations.h"


//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_ORGANIZATIONS::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
											create table BUS_ORGANIZATIONS ( \
											BORG_ID INTEGER not null, \
											BORG_GLOBAL_ID VARCHAR(15) null, \
											BORG_DAT_LAST_MODIFIED TIMESTAMP not null, \
											BORG_CODE VARCHAR(50) not null, \
											BORG_NAME VARCHAR(100) not null, \
											BORG_ACTIVE_FLAG INTEGER not null, \
											BORG_DESCRIPTION LONGVARCHAR null, \
											BORG_CONTACT_ID INTEGER null, \
											BORG_LEVEL INTEGER not null, \
											BORG_PARENT INTEGER null, \
											BORG_HASCHILDREN INTEGER not null, \
											BORG_MAINPARENT_ID INTEGER null, \
											BORG_ICON VARCHAR(50) null, \
											BORG_STYLE INTEGER not null, constraint BUS_ORGANIZATIONS_PK primary key (BORG_ID) )");


	m_lstDeletedCols<<"BORG_SIBLING";

	//constraint for table creation:
	lstSQL.append("\
											alter table BUS_ORGANIZATIONS\
											add constraint BUS_ORGANIZATIONS_PK_FK_PARENT foreign key (\
											BORG_PARENT)\
											references BUS_ORGANIZATIONS (\
											BORG_ID) ON DELETE CASCADE");


	lstSQL.append("\
											alter table BUS_ORGANIZATIONS\
											add constraint BUS_ORG_CONTACT_FK1 foreign key (\
											BORG_CONTACT_ID)\
											references BUS_CM_CONTACT (\
											BCNT_ID) \
											");

	lstSQL.append("\
											alter table BUS_ORGANIZATIONS\
											add constraint BUS_ORG_FK_MAINPARENT foreign key (\
											BORG_MAINPARENT_ID)\
											references BUS_ORGANIZATIONS (\
											BORG_ID)");

}

//probably common to all DB's:
void DbCreate_BUS_ORGANIZATIONS::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
	dboList["IND_BORG_CONTACT_ID"] = "create index IND_BORG_CONTACT_ID on BUS_ORGANIZATIONS(BORG_CONTACT_ID)";  
	dboList["IND_BORG_CODE"] = "create unique index IND_BORG_CODE on BUS_ORGANIZATIONS (BORG_CODE)";    
	dboList["IND_BORG_PARENT"] = "create index IND_BORG_PARENT on BUS_ORGANIZATIONS (BORG_PARENT)";    
	dboList["IND_BORG_HIER"] = "create index IND_BORG_HIER on BUS_ORGANIZATIONS (BORG_LEVEL,BORG_PARENT)";  

}

