#include "dbcreate_spc_pr_area.h"


//--------------------------------------------------
// ODBC - CREATE
//--------------------------------------------------
void DbCreate_SPC_PR_AREA::DefineFields(QStringList &lstSQL) 
{
	//Table schema statements for ODBC.
	lstSQL.append(" 	CREATE TABLE SPC_PR_AREA (\
										SEPB_ID INTEGER not null,\
										SEPB_GLOBAL_ID VARCHAR(15) null,\
										SEPB_DAT_LAST_MODIFIED TIMESTAMP not null,\
										SEPB_CODE VARCHAR(50) not null,\
										SEPB_NAME VARCHAR(100) not null,\
										SEPB_LEVEL INTEGER not null,\
										SEPB_PARENT INTEGER null,\
										SEPB_HASCHILDREN INTEGER not null,\
										SEPB_ICON VARCHAR(50) null,\
										SEPB_MAINPARENT_ID INTEGER null,\
										SEPB_STYLE INTEGER null,\
										SEPB_F_PROJ_BEREICHE_SEQ INTEGER null,\
										constraint SPC_PR_AREA_PK primary key (SEPB_ID))");

}

void DbCreate_SPC_PR_AREA::DefineIndexes(SQLDBObjectCreate &dboList) 
{
		//add index definition by its name (important).
		dboList["IND_SEPB_NAME"] = "CREATE INDEX IND_SEPB_NAME ON SPC_PR_AREA (SEPB_NAME)";
		dboList["IND_SEPB_CODE"] = "CREATE UNIQUE INDEX IND_SEPB_CODE ON SPC_PR_AREA (SEPB_CODE)";
		dboList["IND_SEPB_PARENT"] = "create index IND_SEPB_PARENT on SPC_PR_AREA (SEPB_PARENT)";  
}

void DbCreate_SPC_PR_AREA::DefineViews(SQLDBObjectCreate &dboList) 
{
}

void DbCreate_SPC_PR_AREA::DefineTriggers(SQLDBObjectCreate &dboList) 
{
}
