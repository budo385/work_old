#include "dbcreate_f_taetigkeiten.h"


//--------------------------------------------------
// ODBC - CREATE
//--------------------------------------------------
void DbCreate_F_TAETIGKEITEN::DefineFields(QStringList &lstSQL) 
{
	//Table schema statements for ODBC.
	lstSQL.append(" 	CREATE TABLE F_TAETIGKEITEN (\
				  ET_SEQUENCE INTEGER  NOT NULL,\
				  ET_CODE VARCHAR(23)  NOT NULL,ET_BEZEICHNUNG VARCHAR(89)  NOT NULL,\
				  ET_TEXT VARCHAR(551)  NOT NULL,ET_TITEL VARCHAR(23)  NOT NULL,\
				  ET_STUFE DECIMAL(3) NOT NULL,ET_EX_UT DECIMAL(3) NOT NULL,\
				  ET_HONOR_KL VARCHAR(7)  NOT NULL,ET_FIX DECIMAL(3) NOT NULL,\
				  ET_FIXPREIS DECIMAL(15,2) NOT NULL,ET_INAKTIV DECIMAL(3) NOT NULL,\
				  ET_NICHT_BEL DECIMAL(3) NOT NULL,ET_RUNDEN DECIMAL(15) NOT NULL,\
				  ET_AUFRUNDEN DECIMAL(3) NOT NULL,ET_CODE_EXT VARCHAR(23)  NOT NULL,\
				  ET_GLOBAL_ID VARCHAR(14)  NOT NULL,ET_DAT_AENDERUNG DATETIME ,\
				  ET_DAT_EROEFFNUNG DATETIME ,ET_VALID_SINCE DATETIME ,\
				  ET_EXPIRE_DATE DATETIME ,ET_MODIFIED_BY VARCHAR(36)  NOT NULL,\
				  ET_DEFAULT_TIME DECIMAL(15,2) NOT NULL,\
				  constraint F_TAETIGKEITEN_PK primary key (ET_SEQUENCE))	");


}

void DbCreate_F_TAETIGKEITEN::DefineIndexes(SQLDBObjectCreate &dboList) 
{
		//add index definition by its name (important).
		dboList["IND_ET_CODE"] = "CREATE INDEX IND_ET_CODE ON F_TAETIGKEITEN (ET_CODE)";
		dboList["IND_ET_BEZEICHNUNG"] = "CREATE INDEX IND_ET_BEZEICHNUNG ON F_TAETIGKEITEN (ET_BEZEICHNUNG)";
		dboList["IND_ET_TITEL"] = "CREATE INDEX IND_ET_TITEL ON F_TAETIGKEITEN (ET_TITEL)";
		dboList["IND_ET_CODE_EXT"] = "CREATE INDEX IND_ET_CODE_EXT ON F_TAETIGKEITEN (ET_CODE_EXT)";
		dboList["IND_ET_DAT_AENDERUNG"] = "CREATE INDEX IND_ET_DAT_AENDERUNG ON F_TAETIGKEITEN (ET_DAT_AENDERUNG)";
		dboList["IND_ET_GLOBAL_ID"] = "CREATE INDEX IND_ET_GLOBAL_ID ON F_TAETIGKEITEN (ET_GLOBAL_ID)";
		dboList["IND_ET_EXPIRE_DATE"] = "CREATE INDEX IND_ET_EXPIRE_DATE ON F_TAETIGKEITEN (ET_EXPIRE_DATE)";
		dboList["IND_ET_VALID_SINCE"] = "CREATE INDEX IND_ET_VALID_SINCE ON F_TAETIGKEITEN (ET_VALID_SINCE)";
}

void DbCreate_F_TAETIGKEITEN::DefineViews(SQLDBObjectCreate &dboList) 
{


	dboList["VW_F_TAETIGKEITEN"] = "CREATE VIEW VW_F_TAETIGKEITEN AS SELECT ET_SEQUENCE,ET_CODE,ET_BEZEICHNUNG,ET_TEXT ,ET_TITEL,ET_STUFE,ET_EX_UT,ET_HONOR_KL ,ET_FIX,ET_FIXPREIS,ET_INAKTIV,ET_NICHT_BEL ,ET_RUNDEN,ET_AUFRUNDEN,ET_CODE_EXT,ET_GLOBAL_ID ,ET_DAT_AENDERUNG,ET_DAT_EROEFFNUNG,ET_VALID_SINCE,ET_EXPIRE_DATE ,ET_MODIFIED_BY,ET_DEFAULT_TIME FROM F_TAETIGKEITEN";

	dboList["VWS_F_TAETIGKEITEN"] = "CREATE VIEW VWS_F_TAETIGKEITEN AS SELECT ET_SEQUENCE,ET_CODE,ET_BEZEICHNUNG,ET_TEXT ,ET_TITEL,ET_STUFE,ET_EX_UT,ET_HONOR_KL ,ET_FIX,ET_FIXPREIS,ET_INAKTIV,ET_NICHT_BEL ,ET_RUNDEN,ET_AUFRUNDEN,ET_CODE_EXT,ET_GLOBAL_ID ,ET_DAT_AENDERUNG,ET_DAT_EROEFFNUNG,ET_VALID_SINCE,ET_EXPIRE_DATE ,ET_MODIFIED_BY,ET_DEFAULT_TIME FROM F_TAETIGKEITEN";
}

void DbCreate_F_TAETIGKEITEN::DefineTriggers(SQLDBObjectCreate &dboList) 
{

}
