#include "dbcreate_bus_voicecalls.h"



//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_VOICECALLS::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
											create table BUS_VOICECALLS ( \
											BVC_ID INTEGER not null,\
											BVC_GLOBAL_ID VARCHAR(15) null,\
											BVC_DAT_LAST_MODIFIED TIMESTAMP not null,\
											BVC_COMM_ENTITY_ID INTEGER not null,\
											BVC_START DATETIME not null,\
											BVC_END DATETIME null,\
											BVC_DURATION INTEGER null,\
											BVC_CALLER_ID VARCHAR(50) null,\
											BVC_CALL_ID INTEGER null,\
											BVC_DIRECTION INTEGER null,\
											BVC_DESCRIPTION LONGVARCHAR null,\
											BVC_INTERFACE_ID INTEGER null,\
											BVC_PROJECT_ID INTEGER null,\
											BVC_CONTACT_ID INTEGER null,\
											BVC_CALL_STATUS INTEGER null,\
											BVC_DEVICE_CODE INTEGER null,\
											constraint BUS_VOICECALLS_PK primary key (BVC_ID) ); "); 
											

	lstSQL.append("\
											alter table BUS_VOICECALLS\
											add constraint CE_ENTITY_VOICEC_FK1 foreign key (\
											BVC_COMM_ENTITY_ID)\
											references CE_COMM_ENTITY (CENT_ID)\
											ON DELETE CASCADE");	//when comm entity is deleted, voice calls are deleted


/*
	//if there is one item with given type: can not delete type
	lstSQL.append("\
											alter table BUS_VOICECALLS \
											add constraint BUS_VOICECALLS_INTERFACES_BUS_VOICECALLS_FK1 foreign key (\
											BVC_INTERFACE_ID)\
											references BUS_VOICECALLS_INTERFACES (BVCI_ID)");
*/

	lstSQL.append("\
										alter table BUS_VOICECALLS\
										add constraint BUS_CONTACT_VOICECAL_FK1 foreign key (\
										BVC_CONTACT_ID)\
										references BUS_CM_CONTACT (BCNT_ID)\
										ON DELETE CASCADE");


	lstSQL.append("\
										alter table BUS_VOICECALLS\
										add constraint BUS_PROJ_VOICECAL_FK1 foreign key (\
										BVC_PROJECT_ID)\
										references BUS_PROJECTS (BUSP_ID)\
										ON DELETE CASCADE");

	//no Db versioning:
	
	//Delete columns.
	m_lstDeletedCols << "BVC_IS_PRIVATE";


}



//probably common to all DB's:
void DbCreate_BUS_VOICECALLS::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();

	dboList["IND_BVC_COMM_ENTITY_ID"] = "create index IND_BVC_COMM_ENTITY_ID on BUS_VOICECALLS (BVC_COMM_ENTITY_ID  )  ";  
	dboList["IND_BVC_PROJECT_ID"] = "create index IND_BVC_PROJECT_ID on BUS_VOICECALLS (BVC_PROJECT_ID  )  ";  
	dboList["IND_BVC_CONTACT_ID"] = "create index IND_BVC_CONTACT_ID on BUS_VOICECALLS (BVC_CONTACT_ID  )  ";  
	dboList["IND_BVC_LAST_MODIFIED"] = "create index IND_BVC_LAST_MODIFIED on BUS_VOICECALLS ( BVC_DAT_LAST_MODIFIED )";
	dboList["IND_BVC_START"] = "create index IND_BVC_START on BUS_VOICECALLS ( BVC_START )";
	dboList["IND_BVC_CALL_STATUS"] = "create index IND_BVC_CALL_STATUS on BUS_VOICECALLS ( BVC_CALL_STATUS )";
	
}


void DbCreate_BUS_VOICECALLS::DefineTriggers(SQLDBObjectCreate &dboList)
{
	//create triggers TOFIX: foe each DB, must be different
	dboList["TRG_BUS_VOICECALLS_DELETE"]="\
									CREATE TRIGGER TRG_BUS_VOICECALLS_DELETE FOR BUS_VOICECALLS\
									ACTIVE AFTER DELETE AS \
									BEGIN \
									DELETE FROM CORE_ACC_USER_REC WHERE OLD.BVC_ID = CUAR_RECORD_ID AND CUAR_TABLE_ID="+QVariant(BUS_VOICECALLS).toString()+";\
									DELETE FROM CORE_ACC_GROUP_REC WHERE OLD.BVC_ID = CGAR_RECORD_ID AND CGAR_TABLE_ID="+QVariant(BUS_VOICECALLS).toString()+";\
									END\
									";
}