#include "dbcreate_zs_text_fields.h"



//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_ZS_TEXT_FIELDS::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
				  create table ZS_TEXT_FIELDS ( \
				  RID_TXT INTEGER not null,\
				  TEXT LONGVARCHAR null, constraint ZS_TEXT_FIELDS_PK primary key (RID_TXT))"); 
}



//probably common to all DB's:
void DbCreate_ZS_TEXT_FIELDS::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
}


