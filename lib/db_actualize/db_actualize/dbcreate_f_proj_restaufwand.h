#ifndef DBCREATE_F_PROJ_RESTAUFWAND_H
#define DBCREATE_F_PROJ_RESTAUFWAND_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_PROJ_RESTAUFWAND : public DbSqlTableCreation
{
public: 
	DbCreate_F_PROJ_RESTAUFWAND():DbSqlTableCreation(F_PROJ_RESTAUFWAND){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_PROJ_RESTAUFWAND_H
