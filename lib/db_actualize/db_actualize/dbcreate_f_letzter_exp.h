#ifndef DBCREATE_F_LETZTER_EXP_H
#define DBCREATE_F_LETZTER_EXP_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_LETZTER_EXP : public DbSqlTableCreation
{
public: 
	DbCreate_F_LETZTER_EXP():DbSqlTableCreation(F_LETZTER_EXP){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_LETZTER_EXP_H
