#ifndef DBCREATE_F_USER_SETTINGS_H
#define DBCREATE_F_USER_SETTINGS_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_USER_SETTINGS : public DbSqlTableCreation
{
public: 
	DbCreate_F_USER_SETTINGS():DbSqlTableCreation(F_USER_SETTINGS){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_USER_SETTINGS_H
