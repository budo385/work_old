<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_CH">
<context>
    <name>QObject</name>
    <message>
        <location filename="dborganizer.cpp" line="975"/>
        <source>Database versions not match, you must destroy all data in the target database!</source>
        <translation>Database versions not match, you must destroy all data in the target database!</translation>
    </message>
    <message>
        <source>Failed to delete table: </source>
        <translation type="obsolete">Failed to delete table: </translation>
    </message>
    <message>
        <source>Failed to delete view: </source>
        <translation type="obsolete">Failed to delete view: </translation>
    </message>
    <message>
        <source>Failed to delete index: </source>
        <translation type="obsolete">Failed to delete index: </translation>
    </message>
    <message>
        <source>Failed to delete trigger: </source>
        <translation type="obsolete">Failed to delete trigger: </translation>
    </message>
    <message>
        <source>Failed to delete sequence: </source>
        <translation type="obsolete">Failed to delete sequence: </translation>
    </message>
    <message>
        <source>Sequence creation failed: </source>
        <translation type="obsolete">Sequence creation failed: </translation>
    </message>
    <message>
        <source>Failed to delete procedure: </source>
        <translation type="obsolete">Failed to delete procedure: </translation>
    </message>
    <message>
        <source>Failed to create generator: </source>
        <translation type="obsolete">Failed to create generator: </translation>
    </message>
    <message>
        <source>Failed to adjust Sequence start value: </source>
        <translation type="obsolete">Failed to adjust Sequence start value: </translation>
    </message>
    <message>
        <source>Failed to delete triggers: </source>
        <translation type="obsolete">Failed to delete triggers: </translation>
    </message>
    <message>
        <source>Failed to delete constraints: </source>
        <translation type="obsolete">Failed to delete constraints: </translation>
    </message>
    <message>
        <source>Failed to fetch table info from database for table: </source>
        <translation type="obsolete">Failed to fetch table info from database for table: </translation>
    </message>
    <message>
        <source>Failed to parse create table statement for table: </source>
        <translation type="obsolete">Failed to parse create table statement for table: </translation>
    </message>
    <message>
        <source>Can not determine new backup file name, backup creation failed!</source>
        <translation type="obsolete">Can not determine new backup file name, backup creation failed!</translation>
    </message>
    <message>
        <source>Failed to execute gbak!</source>
        <translation type="obsolete">Failed to execute gbak!</translation>
    </message>
    <message>
        <source>Failed to execute gbak! Reason:</source>
        <translation type="obsolete">Failed to execute gbak! Reason:</translation>
    </message>
    <message>
        <source>Database restore from </source>
        <translation type="obsolete">Database restore from </translation>
    </message>
    <message>
        <source> failed! File not found!</source>
        <translation type="obsolete"> failed! File not found!</translation>
    </message>
    <message>
        <source> failed! Can not unzip file: </source>
        <translation type="obsolete"> failed! Can not unzip file: </translation>
    </message>
    <message>
        <source>Failed to delete triggers</source>
        <translation type="obsolete">Failed to delete triggers</translation>
    </message>
    <message>
        <source>Failed to delete constraints</source>
        <translation type="obsolete">Failed to delete constraints</translation>
    </message>
    <message>
        <source>Failed to execute mysldump!</source>
        <translation type="obsolete">Failed to execute mysldump!</translation>
    </message>
    <message>
        <source>Failed to execute mysldump! Reason:</source>
        <translation type="obsolete">Failed to execute mysldump! Reason:</translation>
    </message>
    <message>
        <source>Backup file can not be written to disk!</source>
        <translation type="obsolete">Backup file can not be written to disk!</translation>
    </message>
    <message>
        <source>Failed to execute mysql!</source>
        <translation type="obsolete">Failed to execute mysql!</translation>
    </message>
    <message>
        <source>Failed to execute mysql! Reason:</source>
        <translation type="obsolete">Failed to execute mysql! Reason:</translation>
    </message>
    <message>
        <source>Table does not exists: </source>
        <translation type="obsolete">Table does not exists: </translation>
    </message>
    <message>
        <source>Trigger creation failed: </source>
        <translation type="obsolete">Trigger creation failed: </translation>
    </message>
    <message>
        <source>Database is still in use by other systems (application servers). Wait for 4 minutes and try again!</source>
        <translation type="obsolete">Database is still in use by other systems (application servers). Wait for 4 minutes and try again!</translation>
    </message>
    <message>
        <location filename="dborganizer.cpp" line="1057"/>
        <source>Database is not organized, backup creation failed!</source>
        <translation>Database is not organized, backup creation failed!</translation>
    </message>
    <message>
        <location filename="dborganizer.cpp" line="1090"/>
        <source>Database is still in use by other client. Please make sure to disconnect all clients or wait for 4 minutes (timeout for dead clients) and try again!</source>
        <translation>Database is still in use by other client. Please make sure to disconnect all clients or wait for 4 minutes (timeout for dead clients) and try again!</translation>
    </message>
    <message>
        <location filename="interface.cpp" line="416"/>
        <location filename="interface.cpp" line="442"/>
        <location filename="interface.cpp" line="471"/>
        <location filename="interface.cpp" line="496"/>
        <location filename="interface.cpp" line="518"/>
        <location filename="interface.cpp" line="542"/>
        <location filename="interface.cpp" line="586"/>
        <location filename="interface.cpp" line="609"/>
        <location filename="interface.cpp" line="633"/>
        <location filename="interface.cpp" line="656"/>
        <location filename="interface.cpp" line="681"/>
        <source>Database connection not established. Connect first!</source>
        <translation>Database connection not established. Connect first!</translation>
    </message>
    <message>
        <location filename="dborganizer.cpp" line="1255"/>
        <location filename="dborganizer.cpp" line="1269"/>
        <location filename="dborganizer.cpp" line="2040"/>
        <source>Failed to create demo key</source>
        <translation>Demo-Schlüssel konnte nicht erzeugt werden</translation>
    </message>
    <message>
        <location filename="dborganizer.cpp" line="544"/>
        <source>Database is still in use by other systems (application servers)!</source>
        <translation>Datenbank wird noch von anderen Systemen (Application-Servern) verwendet!</translation>
    </message>
    <message>
        <source>Failed to execute nbackup. Make sure it is in /firebird/bin subdirectory!</source>
        <translation type="obsolete">Das Backup (nbackup) konnte nicht gestartet werden. Stellen Sie sicher, dass es sich im /firebird/bin Unterverzeichnis befindet!</translation>
    </message>
    <message>
        <source>Failed to execute nbackup! Reason:</source>
        <translation type="obsolete">Das Backup (nbackup) konnte nicht gestartet werden. Ursache:</translation>
    </message>
    <message>
        <source>Failed to execute gbak. Make sure it is in /firebird/bin subdirectory!</source>
        <translation type="obsolete">Das Backup (gbak) konnte nicht gestartet werden. Stellen Sie sicher, dass es sich im /firebird/bin Unterverzeichnis befindet!</translation>
    </message>
    <message>
        <location filename="dborganizer.cpp" line="6872"/>
        <location filename="dborganizer.cpp" line="7026"/>
        <source>Failed to execute gsec. Make sure it is in /firebird/bin subdirectory!</source>
        <translation>Das Backup (gsek) konnte nicht gestartet werden. Stellen Sie sicher, dass es sich im /firebird/bin Unterverzeichnis befindet!</translation>
    </message>
    <message>
        <location filename="dborganizer.cpp" line="6881"/>
        <location filename="dborganizer.cpp" line="7035"/>
        <source>Failed to execute gsec! Reason:</source>
        <translation>Das Backup (gsec) konnte nicht ausgeführt werden. Ursache:</translation>
    </message>
    <message>
        <source>Failed to create role: </source>
        <translation type="obsolete">Rolle konnte nicht erstellt werden: </translation>
    </message>
    <message>
        <location filename="dborganizer.cpp" line="6921"/>
        <location filename="dborganizer.cpp" line="6934"/>
        <location filename="dborganizer.cpp" line="6947"/>
        <location filename="dborganizer.cpp" line="7070"/>
        <location filename="dborganizer.cpp" line="7083"/>
        <location filename="dborganizer.cpp" line="7096"/>
        <source>Failed to grant select on role: </source>
        <translation>SELECT-Rechte konnten der Rolle nicht gewährt werden: </translation>
    </message>
    <message>
        <source>Failed to grant role on user: </source>
        <translation type="obsolete">Rolle konnte User nicht zugewiesen werden: </translation>
    </message>
    <message>
        <source>Database already have Sokrates Project tables. Operation aborted!</source>
        <translation type="obsolete">Die Datenbank enthält bereits Tabellen für SOKRATES Project Control. Ausführung abgebrochen!</translation>
    </message>
    <message>
        <source>Failed to grant admin role on user: </source>
        <translation type="obsolete">Administratorenrechte konnten nicht zugewiesen werden. Benutzer: </translation>
    </message>
</context>
</TS>
