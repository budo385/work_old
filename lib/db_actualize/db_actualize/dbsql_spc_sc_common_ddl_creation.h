#ifndef DBSQL_SPC_SC_COMMON_DDL_CREATION_H
#define DBSQL_SPC_SC_COMMON_DDL_CREATION_H



#include <QString>
#include <QHash>
#include "common/common/status.h"
#include "db/db/dbsqlquery.h"
#include "db/db/dbsqlmanager.h"
#include "db/db/dbomanager.h"





typedef QHash<QString, QString> SQLDBObjectCreate;					///< object creation statement pair: object_name , object SQL defintion
typedef QHash<QString, SQLDBObjectCreate> SQLDBObjectCreate_DbType; ///< list of object creation SQL's indexed by Database Type
typedef QHashIterator<QString, QString> SQLDBObjectCreateIterator;	///< iterator

/*!
	\class DbSql_SPC_SC_Common_DDL_Creation
	\brief HardCoded SQL function creation statements for each DB, no version tracking
	\ingroup Db_Actualize

	When invoked, all previous functions are dropped, new created, only to the latest DB version

	Use:
	- Add own function here in Initialize() , for each DB version
*/


class DbSql_SPC_SC_Common_DDL_Creation
{
public:

	void DropAll(Status &pStatus);
	void Create(Status &pStatus);
	void InitDbSettings(DbSqlManager *pManager, QSqlDatabase *pDb,DbObjectManager *pDboManager);

protected:
	//INIT WITH CREATE COMMANDS for each DB:
	//for each DB....
	void Init_Oracle();
	void Init_MySQL();
	void Init_FireBird();

	//not implemented:
	void Init_MS_SQL(){};
	void Init_PostgreSQL(){};
	void Init_DB2(){};

	DbSqlManager	*m_pManager;
	QSqlDatabase	*m_pDb;
	DbObjectManager *m_DbObjectManager;

	//always executed with drop or replace if exists (no alter)
	SQLDBObjectCreate_DbType m_Constraints;					///< Dbo Objects for Communicator database: FK constraints
	SQLDBObjectCreate_DbType m_Triggers;

	//stored data:
	QString m_DbType;
};


#endif //DBSQL_SPC_SC_COMMON_DDL_CREATION_H

