#include "dbcreate_spc_process.h"


//--------------------------------------------------
// ODBC - CREATE
//--------------------------------------------------
void DbCreate_SPC_PROCESS::DefineFields(QStringList &lstSQL) 
{
	//Table schema statements for ODBC.
	lstSQL.append(" 	CREATE TABLE SPC_PROCESS (\
										SEPZ_ID INTEGER not null,\
										SEPZ_GLOBAL_ID VARCHAR(15) null,\
										SEPZ_DAT_LAST_MODIFIED TIMESTAMP not null,\
										SEPZ_CODE VARCHAR(50) not null,\
										SEPZ_LEVEL INTEGER not null,\
										SEPZ_PARENT INTEGER null,\
										SEPZ_HASCHILDREN INTEGER not null,\
										SEPZ_ICON VARCHAR(50) null,\
										SEPZ_MAINPARENT_ID INTEGER null,\
										SEPZ_NAME VARCHAR(100) not null,\
										SEPZ_STYLE INTEGER null,\
										SEPZ_F_PROZESSE_SEQ INTEGER null,\
										constraint SPC_PROCESS_PK primary key (SEPZ_ID)	)");
}

void DbCreate_SPC_PROCESS::DefineIndexes(SQLDBObjectCreate &dboList) 
{
		//add index definition by its name (important).
		dboList["IND_SEPZ_NAME"] = "CREATE INDEX IND_SEPZ_NAME ON SPC_PROCESS (SEPZ_NAME)";
		dboList["IND_SEPZ_CODE"] = "CREATE UNIQUE INDEX IND_SEPZ_CODE ON SPC_PROCESS (SEPZ_CODE)";
		dboList["IND_SEPZ_PARENT"] = "create index IND_SEPZ_PARENT on SPC_PROCESS (SEPZ_PARENT)";  
}

void DbCreate_SPC_PROCESS::DefineViews(SQLDBObjectCreate &dboList) 
{

}

void DbCreate_SPC_PROCESS::DefineTriggers(SQLDBObjectCreate &dboList) 
{
	
}
