#include "dbcreate_bus_opt_grid_views.h"



//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_OPT_GRID_VIEWS::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
											create table BUS_OPT_GRID_VIEWS ( \
											BOGW_ID INTEGER not null,\
											BOGW_GLOBAL_ID VARCHAR(15) null,\
											BOGW_DAT_LAST_MODIFIED TIMESTAMP not null,\
											BOGW_GRID_ID INTEGER not null,\
											BOGW_VIEW_NAME VARCHAR(80) null,\
											BOGW_HEADER_TEXT VARCHAR(80) null,\
											BOGW_COLUMN_NAME VARCHAR(80) null,\
											BOGW_COLUMN_TYPE INTEGER null,\
											BOGW_COLUMN_SIZE INTEGER null,\
											BOGW_COLUMN_POSITION INTEGER null,\
											BOGW_EDITABLE SMALLINT null,\
											BOGW_SORT_ORDER INTEGER null,\
											BOGW_DATASOURCE VARCHAR(100) null,\
											BOGW_REPORT_WIDTH INTEGER null, constraint BUS_OPT_GRID_VIEWS_PK primary key (BOGW_ID) ) ");
}



//probably common to all DB's:
void DbCreate_BUS_OPT_GRID_VIEWS::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
	dboList["IND_BOGW_GRID_ID"] = "create index IND_BOGW_GRID_ID on BUS_OPT_GRID_VIEWS (BOGW_GRID_ID)  ";  
	dboList["IND_BOGW_GRID_NAME_ID"] = "create index IND_BOGW_GRID_NAME_ID on BUS_OPT_GRID_VIEWS (	BOGW_GRID_ID  ,	BOGW_VIEW_NAME  )  ";  
}

