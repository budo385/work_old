#include "dbcreate_core_app_srv_session.h"



//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------

void DbCreate_CORE_APP_SRV_SESSION::DefineFields(QStringList &lstSQL)
{

	//Table shema statements for ODBC.
	lstSQL.append("\
	create table CORE_APP_SRV_SESSION (\
	COAS_ID INTEGER not null,\
	COAS_GLOBAL_ID VARCHAR(15) null,\
	COAS_DAT_LAST_MODIFIED TIMESTAMP not null,\
	COAS_APP_SRV_SERIAL_ID VARCHAR(30) null,\
	COAS_APP_SRV_NAME VARCHAR(30) null,\
	COAS_UNIQUE_LICENSE_ID INTEGER not null,\
	COAS_IP_ADDRESS VARCHAR(20) null,\
	COAS_DAT_STARTED DATETIME not null,\
	COAS_DAT_LAST_ACTIVITY DATETIME not null,\
	COAS_ROLE_ID INTEGER not null,\
	COAS_CURRENT_CONNECTIONS INTEGER not null,\
	COAS_MAX_CONNECTIONS INTEGER not null, \
	COAS_IS_LBO SMALLINT not null,\
	COAS_IS_MASTER SMALLINT not null, constraint CORE_APP_SRV_SESSION_PK primary key (COAS_ID) )" )  ;
	
	
}




/* Probably index defintion is same for all DB's */
void DbCreate_CORE_APP_SRV_SESSION::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index defintion by its name (important)
	dboList.clear();
	dboList["IND_COAS_DAT_LAST_ACTIVITY"]	="create index IND_COAS_DAT_LAST_ACTIVITY on CORE_APP_SRV_SESSION (COAS_DAT_LAST_ACTIVITY)";  
	dboList["IND_COAS_ROLE_ID"]	="create index IND_COAS_ROLE_ID on CORE_APP_SRV_SESSION (COAS_ROLE_ID)";  
	dboList["IND_COMB_ROLE_LICENSE"]	="create index IND_COMB_ROLE_LICENSE on CORE_APP_SRV_SESSION (COAS_ROLE_ID,COAS_UNIQUE_LICENSE_ID)";  
}






