#include "dbcreate_bus_cm_contact.h"


//--------------------------------------------------
// ODBC - CREATE
//--------------------------------------------------
void DbCreate_BUS_CM_CONTACT::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
											create table BUS_CM_CONTACT ( \
											BCNT_ID INTEGER not null,\
											BCNT_GLOBAL_ID VARCHAR(15) null,\
											BCNT_DAT_LAST_MODIFIED TIMESTAMP not null,\
											BCNT_TYPE INTEGER not null,\
											BCNT_DESCRIPTION LONGVARCHAR null,\
											BCNT_LANGUAGECODE VARCHAR(50) null,\
											BCNT_SUPPRESS_MAILING SMALLINT not null,\
											BCNT_EXT_ID_STR VARCHAR(255) null,\
											BCNT_EXT_APP VARCHAR(8) null,\
											BCNT_OLD_CODE VARCHAR(80) null,\
											BCNT_OWNER_ID INTEGER null,\
											BCNT_SHORTNAME VARCHAR(200) null,\
											BCNT_LASTNAME VARCHAR(80) null,\
											BCNT_FIRSTNAME VARCHAR(80) null,\
											BCNT_MIDDLENAME VARCHAR(80) null,\
											BCNT_NICKNAME VARCHAR(80) null,\
											BCNT_BIRTHDAY DATE null,\
											BCNT_DEPARTMENTNAME VARCHAR(200) null,\
											BCNT_ORGANIZATIONNAME VARCHAR(200) null,\
											BCNT_ORGANIZATIONNAME_2 VARCHAR(200) null,\
											BCNT_PROFESSION VARCHAR(200) null,\
											BCNT_FUNCTION VARCHAR(200) null,\
											BCNT_SEX SMALLINT null,\
											BCNT_FOUNDATIONDATE DATE null, \
											BCNT_LOCATION VARCHAR(100) null,\
											BCNT_PICTURE LONGVARBINARY null,\
											BCNT_BIG_PICTURE_ID INTEGER null, \
											BCNT_LASTNAME_PHONET VARCHAR(200) null,\
											BCNT_FIRSTNAME_PHONET VARCHAR(200) null,\
											BCNT_ORGANIZATIONNAME_PHONET VARCHAR(200) null,\
											BCNT_DO_NOT_SYNC_MAIL SMALLINT null,\
											BCNT_SYNC_MAIL_AS_PRIV SMALLINT null,\
											BCNT_OUT_MAIL_AS_PRIV SMALLINT null,\
											BCNT_LOCATION_PHONET VARCHAR(150) null,\
											BCNT_DEFAULT_GRID_VIEW_ID INTEGER null,\
											BCNT_DAT_CREATED DATETIME null,\
											constraint BUS_CM_CONTACT_PK primary key (BCNT_ID) )  ");


	//no Db versioning:
	m_lstDeletedCols<<"BCNT_DEFAULT_GRID_VIEW";
	m_lstDeletedCols<<"BCNT_ACCESS";


	lstSQL.append("\
											alter table BUS_CM_CONTACT\
											add constraint BIG_PIC_CONT_FK1 foreign key (\
											BCNT_BIG_PICTURE_ID)\
											references BUS_BIG_PICTURE (\
											BPIC_ID) \
											");

	lstSQL.append("\
										    alter table BUS_CM_CONTACT\
										    add constraint BCNT_DEF_GRID_VIEW_FK foreign key (\
										    BCNT_DEFAULT_GRID_VIEW_ID)\
										    references BUS_COMM_VIEW (\
										    BUSCV_ID) \
											");
	

	lstSQL.append("\
								   alter table BUS_CM_CONTACT\
								   add constraint PERSON_CONT_FK1 foreign key (\
								   BCNT_OWNER_ID)\
								   references BUS_PERSON (\
								   BPER_ID)\
								   ON DELETE SET NULL");
}


//probably common to all DB's:
void DbCreate_BUS_CM_CONTACT::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
	dboList["IND_BCNT_TYPE"] = "create index IND_BCNT_TYPE on BUS_CM_CONTACT (BCNT_TYPE)";  
	dboList["IND_BCNT_OWNER_ID"] = "create index IND_BCNT_OWNER_ID on BUS_CM_CONTACT (BCNT_OWNER_ID )";  
	dboList["IND_BCNT_ORGANIZATIONNAME"] = "create index IND_BCNT_ORGANIZATIONNAME on BUS_CM_CONTACT (BCNT_ORGANIZATIONNAME )";  
	dboList["IND_BCNT_LAST_NAME"] = "create index IND_BCNT_LAST_NAME on BUS_CM_CONTACT (BCNT_LASTNAME )";  
	dboList["IND_BCNT_DEPARTMENTNAME"] = "create index IND_BCNT_DEPARTMENTNAME on BUS_CM_CONTACT (BCNT_DEPARTMENTNAME)  ";  
	dboList["IND_BCNT_PROFESSION"] = "create index IND_BCNT_PROFESSION on BUS_CM_CONTACT (BCNT_PROFESSION ) ";  
	dboList["IND_BCNT_FUNCTION"] = "create index IND_BCNT_FUNCTION on BUS_CM_CONTACT (BCNT_FUNCTION )";  
	dboList["IND_LASTNAME_PHONET"] = "create index IND_LASTNAME_PHONET on BUS_CM_CONTACT (BCNT_LASTNAME_PHONET )";  
	dboList["IND_FIRSTNAME_PHONET"] = "create index IND_FIRSTNAME_PHONET on BUS_CM_CONTACT (BCNT_FIRSTNAME_PHONET )";  
	dboList["IND_ORGNAME_PHONET"] = "create index IND_ORGNAME_PHONET on BUS_CM_CONTACT (BCNT_ORGANIZATIONNAME_PHONET )";  
	dboList["IND_BCNT_LAST_MODIFIED"] = "create index IND_BCNT_LAST_MODIFIED on BUS_CM_CONTACT ( BCNT_DAT_LAST_MODIFIED )";
	dboList["IND_LOCATION_PHONET"] = "create index IND_LOCATION_PHONET on BUS_CM_CONTACT (BCNT_LOCATION_PHONET )";  
}





void DbCreate_BUS_CM_CONTACT::DefineTriggers(SQLDBObjectCreate &dboList)
{
	//create triggers TOFIX: foe each DB, must be different
	dboList["TRG_BUS_CONTACT_DELETE"]="\
									 CREATE TRIGGER TRG_BUS_CONTACT_DELETE FOR BUS_CM_CONTACT\
									 ACTIVE AFTER DELETE AS \
									 BEGIN \
									 DELETE FROM BUS_BIG_PICTURE WHERE OLD.BCNT_BIG_PICTURE_ID = BPIC_ID;\
									 DELETE FROM bus_nmrx_relation WHERE OLD.BCNT_ID = BNMR_TABLE_KEY_ID_1 AND BNMR_TABLE_1="+QVariant(BUS_CM_CONTACT).toString()+";\
									 DELETE FROM bus_nmrx_relation WHERE OLD.BCNT_ID = BNMR_TABLE_KEY_ID_2 AND BNMR_TABLE_2="+QVariant(BUS_CM_CONTACT).toString()+";\
									 DELETE FROM CORE_ACC_USER_REC WHERE OLD.BCNT_ID = CUAR_RECORD_ID AND CUAR_TABLE_ID="+QVariant(BUS_CM_CONTACT).toString()+";\
									 DELETE FROM CORE_ACC_GROUP_REC WHERE OLD.BCNT_ID = CGAR_RECORD_ID AND CGAR_TABLE_ID="+QVariant(BUS_CM_CONTACT).toString()+";\
									 DELETE FROM BUS_CUSTOM_BOOL WHERE OLD.BCNT_ID = BCB_RECORD_ID AND BCB_TABLE_ID="+QVariant(BUS_CM_CONTACT).toString()+";\
									 DELETE FROM BUS_CUSTOM_DATE WHERE OLD.BCNT_ID = BCD_RECORD_ID AND BCD_TABLE_ID="+QVariant(BUS_CM_CONTACT).toString()+";\
									 DELETE FROM BUS_CUSTOM_DATETIME WHERE OLD.BCNT_ID = BCDT_RECORD_ID AND BCDT_TABLE_ID="+QVariant(BUS_CM_CONTACT).toString()+";\
									 DELETE FROM BUS_CUSTOM_FLOAT WHERE OLD.BCNT_ID = BCFL_RECORD_ID AND BCFL_TABLE_ID="+QVariant(BUS_CM_CONTACT).toString()+";\
									 DELETE FROM BUS_CUSTOM_INT WHERE OLD.BCNT_ID = BCI_RECORD_ID AND BCI_TABLE_ID="+QVariant(BUS_CM_CONTACT).toString()+";\
									 DELETE FROM BUS_CUSTOM_LONGTEXT WHERE OLD.BCNT_ID = BCLT_RECORD_ID AND BCLT_TABLE_ID="+QVariant(BUS_CM_CONTACT).toString()+";\
									 DELETE FROM BUS_CUSTOM_TEXT WHERE OLD.BCNT_ID = BCT_RECORD_ID AND BCT_TABLE_ID="+QVariant(BUS_CM_CONTACT).toString()+";\
									 END\
									 ";

}