#ifndef DBCREATE_F_FAKTURA_EIN_H
#define DBCREATE_F_FAKTURA_EIN_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_FAKTURA_EIN : public DbSqlTableCreation
{
public: 
	DbCreate_F_FAKTURA_EIN():DbSqlTableCreation(F_FAKTURA_EIN){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_FAKTURA_EIN_H
