#include "dbcreate_core_ipaccesslist.h"



//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------

void DbCreate_CORE_IPACCESSLIST::DefineFields(QStringList &lstSQL)
{

	//Table shema statements for ODBC.
	lstSQL.append("\
											create table CORE_IPACCESSLIST ( \
											CIPR_ID INTEGER not null,\
											CIPR_GLOBAL_ID VARCHAR(15) null,\
											CIPR_DAT_LAST_MODIFIED TIMESTAMP null,\
											CIPR_LIST_NAME VARCHAR(30) not null,\
											CIPR_IP_ADDRESS INTEGER not null,\
											CIPR_FILTER_BITS INTEGER not null,\
											CIPR_ORDER INTEGER not null,\
											CIPR_ALLOW SMALLINT not null, constraint CORE_IPACCESSLIST_PK primary key (CIPR_ID) )");  

}




/* Probably index defintion is same for all DB's */
void DbCreate_CORE_IPACCESSLIST::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index defintion by its name (important)
	dboList.clear();
	dboList["IND_LIST_NAME"]	="create index IND_LIST_NAME on CORE_IPACCESSLIST (CIPR_ID,CIPR_LIST_NAME)";  
}





