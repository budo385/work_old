#ifndef DBCREATE_BUS_PUSH_TOKEN_PERSON_H
#define DBCREATE_BUS_PUSH_TOKEN_PERSON_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"
#include "db/db/dbsqlquery.h"


class DbCreate_BUS_PUSH_TOKEN_PERSON : public  DbSqlTableCreation
{
public:
	DbCreate_BUS_PUSH_TOKEN_PERSON():DbSqlTableCreation(BUS_PUSH_TOKEN_PERSON){};  //init to CORE_USER table
	

private:
	//create generic Table def:
	void DefineFields(QStringList &lstSQL);
	//probably common to all DB's:
	void DefineIndexes(SQLDBObjectCreate &dboList);
};

#endif // DBCREATE_BUS_PUSH_MESSAGE_H

