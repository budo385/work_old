#include "dbcreate_bus_email_attachment.h"

//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_EMAIL_ATTACHMENT::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
											create table BUS_EMAIL_ATTACHMENT ( \
											BEA_ID INTEGER not null,\
											BEA_GLOBAL_ID VARCHAR(15) null, \
											BEA_DAT_LAST_MODIFIED TIMESTAMP not null, \
											BEA_EMAIL_ID INTEGER not null, \
											BEA_NAME VARCHAR(150) null, \
											BEA_CONTENT LONGVARBINARY null,\
											BEA_CID_LINK VARCHAR(70) null,\
											constraint BUS_EMAIL_ATTACHMENT_PK primary key (BEA_ID))"); 
											

	lstSQL.append("\
											alter table BUS_EMAIL_ATTACHMENT\
											add constraint EMAIL_ATTACHMENT_FK1 foreign key (\
											BEA_EMAIL_ID)\
											references BUS_EMAIL (BEM_ID) \
											ON DELETE CASCADE");	//when email is deleted, attachments are deleted

}



//probably common to all DB's:
void DbCreate_BUS_EMAIL_ATTACHMENT::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
	//dboList["IDX_BEA_EMAIL_ID"] = "create index IDX_BEA_EMAIL_ID on BUS_EMAIL_ATTACHMENT (BEA_EMAIL_ID)";  
}


