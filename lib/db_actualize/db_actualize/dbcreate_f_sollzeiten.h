#ifndef DBCREATE_F_SOLLZEITEN_H
#define DBCREATE_F_SOLLZEITEN_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_SOLLZEITEN : public DbSqlTableCreation
{
public: 
	DbCreate_F_SOLLZEITEN():DbSqlTableCreation(F_SOLLZEITEN){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_SOLLZEITEN_H
