#ifndef DBCREATE_F_RECORD_ID_H
#define DBCREATE_F_RECORD_ID_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_RECORD_ID : public DbSqlTableCreation
{
public: 
	DbCreate_F_RECORD_ID():DbSqlTableCreation(F_RECORD_ID){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_RECORD_ID_H
