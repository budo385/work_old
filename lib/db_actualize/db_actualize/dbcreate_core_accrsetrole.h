#ifndef DBCREATE_CORE_ACCRSETROLE_H
#define DBCREATE_CORE_ACCRSETROLE_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_CORE_ACCRSETROLE : public DbSqlTableCreation
{
public:
	DbCreate_CORE_ACCRSETROLE():DbSqlTableCreation(CORE_ACCRSETROLE){};  //init to CORE_PERSON table
	

private:
	//create generic Table def:
	void DefineFields(QStringList &lstSQL);

	//probably common to all DB's:
	void DefineIndexes(SQLDBObjectCreate &dboList);
};

#endif // DBCREATE_CORE_ACCRSETROLE_H
