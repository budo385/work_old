#include "dbcreate_f_record_id.h"


//--------------------------------------------------
// ODBC - CREATE
//--------------------------------------------------
void DbCreate_F_RECORD_ID::DefineFields(QStringList &lstSQL) 
{
	//Table schema statements for ODBC.
	lstSQL.append(" 	CREATE TABLE F_RECORD_ID (\
										RID_TABLE_NAME VARCHAR(45)  NOT NULL,\
										RID_NEXT_ID DECIMAL(10) NOT NULL,RID_COL_NAME VARCHAR(56)  NOT NULL,\
										RID_COUNTER DECIMAL(10) NOT NULL,RID_SEQUENCE INTEGER  NOT NULL,\
										constraint F_RECORD_ID_PK primary key (RID_SEQUENCE))	");

}

void DbCreate_F_RECORD_ID::DefineIndexes(SQLDBObjectCreate &dboList) 
{
		//add index definition by its name (important).
		dboList["IND_RID_TABLE_NAME"] = "CREATE UNIQUE INDEX IND_RID_TABLE_NAME ON F_RECORD_ID (RID_TABLE_NAME)";

}

void DbCreate_F_RECORD_ID::DefineViews(SQLDBObjectCreate &dboList) 
{


	dboList["VW_F_RECORD_ID"] = "CREATE VIEW VW_F_RECORD_ID AS SELECT RID_TABLE_NAME,RID_NEXT_ID,RID_COL_NAME,RID_COUNTER ,RID_SEQUENCE FROM F_RECORD_ID";

	dboList["VWS_F_RECORD_ID"] = "CREATE VIEW VWS_F_RECORD_ID AS SELECT RID_TABLE_NAME,RID_NEXT_ID,RID_COL_NAME,RID_COUNTER ,RID_SEQUENCE FROM F_RECORD_ID";
}

void DbCreate_F_RECORD_ID::DefineTriggers(SQLDBObjectCreate &dboList) 
{
	if(m_DbType==DBTYPE_FIREBIRD)
	{

		dboList["TR_F_RECORD_ID"] = "CREATE TRIGGER TR_F_RECORD_ID FOR F_RECORD_ID BEFORE INSERT OR UPDATE AS BEGIN \
IF(NEW.RID_NEXT_ID IS NULL) THEN NEW.RID_NEXT_ID=0;\
IF(NEW.RID_COUNTER IS NULL) THEN NEW.RID_COUNTER=0;\
IF(NEW.RID_TABLE_NAME IS NULL) THEN NEW.RID_TABLE_NAME='';\
IF(NEW.RID_COL_NAME IS NULL) THEN NEW.RID_COL_NAME='';\
END;";
	}
}
