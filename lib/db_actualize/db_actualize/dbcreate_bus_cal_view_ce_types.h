#ifndef DBCREATE_BUS_CAL_VIEW_CE_TYPES_H
#define DBCREATE_BUS_CAL_VIEW_CE_TYPES_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"

class DbCreate_BUS_CAL_VIEW_CE_TYPES : public DbSqlTableCreation
{
public:
	DbCreate_BUS_CAL_VIEW_CE_TYPES():DbSqlTableCreation(BUS_CAL_VIEW_CE_TYPES){};  //init table

private:
	//create generic Table def:
	void DefineFields(QStringList &lstSQL);

	//probably common to all DB's:
	void DefineIndexes(SQLDBObjectCreate &dboList);
};

#endif // DBCREATE_BUS_CAL_VIEW_CE_TYPES_H
