#ifndef DBCREATE_F_BEL_PROJ_LISTEN_H
#define DBCREATE_F_BEL_PROJ_LISTEN_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_BEL_PROJ_LISTEN : public DbSqlTableCreation
{
public: 
	DbCreate_F_BEL_PROJ_LISTEN():DbSqlTableCreation(F_BEL_PROJ_LISTEN){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_BEL_PROJ_LISTEN_H
