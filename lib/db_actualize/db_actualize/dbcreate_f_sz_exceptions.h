#ifndef DBCREATE_F_SZ_EXCEPTIONS_H
#define DBCREATE_F_SZ_EXCEPTIONS_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_SZ_EXCEPTIONS : public DbSqlTableCreation
{
public: 
	DbCreate_F_SZ_EXCEPTIONS():DbSqlTableCreation(F_SZ_EXCEPTIONS){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_SZ_EXCEPTIONS_H
