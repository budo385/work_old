#include "dbcreate_f_konto_namen.h"


//--------------------------------------------------
// ODBC - CREATE
//--------------------------------------------------
void DbCreate_F_KONTO_NAMEN::DefineFields(QStringList &lstSQL) 
{
	//Table schema statements for ODBC.
	lstSQL.append(" 	CREATE TABLE F_KONTO_NAMEN (\
										KN_SEQUENCE INTEGER  NOT NULL,\
										KN_KONTO VARCHAR(34)  NOT NULL,KN_NAME VARCHAR(45)  NOT NULL,\
										KN_KONTO_FIBU VARCHAR(12)  NOT NULL,KN_LEITER VARCHAR(7)  NOT NULL,\
										KN_GRUPPE VARCHAR(12)  NOT NULL,KN_STUFE DECIMAL(3) NOT NULL,\
										KN_EX_UKS DECIMAL(3) NOT NULL,KN_VALID_SINCE DATETIME ,\
										KN_EXPIRE_DATE DATETIME ,KN_GLOBAL_ID VARCHAR(14)  NOT NULL,\
										KN_DAT_AENDERUNG DATETIME ,KN_DAT_EROEFFNUNG DATETIME ,\
										KN_MODIFIED_BY VARCHAR(36)  NOT NULL,\
										constraint F_KONTO_NAMEN_PK primary key (KN_SEQUENCE))	");

}

void DbCreate_F_KONTO_NAMEN::DefineIndexes(SQLDBObjectCreate &dboList) 
{
		//add index definition by its name (important).
		dboList["IND_KN_KONTO"] = "CREATE INDEX IND_KN_KONTO ON F_KONTO_NAMEN (KN_KONTO)";
		dboList["IND_KN_GRUPPE"] = "CREATE INDEX IND_KN_GRUPPE ON F_KONTO_NAMEN (KN_GRUPPE)";
		dboList["IND_KN_LEITER"] = "CREATE INDEX IND_KN_LEITER ON F_KONTO_NAMEN (KN_LEITER)";
		dboList["IND_KN_GLOBAL_ID"] = "CREATE INDEX IND_KN_GLOBAL_ID ON F_KONTO_NAMEN (KN_GLOBAL_ID)";
		dboList["IND_KN_DAT_AENDERUNG"] = "CREATE INDEX IND_KN_DAT_AENDERUNG ON F_KONTO_NAMEN (KN_DAT_AENDERUNG)";
		dboList["IND_KN_EXPIRE_DATE"] = "CREATE INDEX IND_KN_EXPIRE_DATE ON F_KONTO_NAMEN (KN_EXPIRE_DATE)";
		dboList["IND_KN_VALID_SINCE"] = "CREATE INDEX IND_KN_VALID_SINCE ON F_KONTO_NAMEN (KN_VALID_SINCE)";

}

void DbCreate_F_KONTO_NAMEN::DefineViews(SQLDBObjectCreate &dboList) 
{


	dboList["VW_F_KONTO_NAMEN"] = "CREATE VIEW VW_F_KONTO_NAMEN AS SELECT KN_SEQUENCE,KN_KONTO,KN_NAME,KN_KONTO_FIBU ,KN_LEITER,KN_GRUPPE,KN_STUFE,KN_EX_UKS ,KN_VALID_SINCE,KN_EXPIRE_DATE,KN_GLOBAL_ID,KN_DAT_AENDERUNG ,KN_DAT_EROEFFNUNG,KN_MODIFIED_BY FROM F_KONTO_NAMEN";

	dboList["VWS_F_KONTO_NAMEN"] = "CREATE VIEW VWS_F_KONTO_NAMEN AS SELECT KN_SEQUENCE,KN_KONTO,KN_NAME,KN_KONTO_FIBU ,KN_LEITER,KN_GRUPPE,KN_STUFE,KN_EX_UKS ,KN_VALID_SINCE,KN_EXPIRE_DATE,KN_GLOBAL_ID,KN_DAT_AENDERUNG ,KN_DAT_EROEFFNUNG,KN_MODIFIED_BY FROM F_KONTO_NAMEN";
}

void DbCreate_F_KONTO_NAMEN::DefineTriggers(SQLDBObjectCreate &dboList) 
{
	if(m_DbType==DBTYPE_FIREBIRD)
	{

		dboList["TR_F_KONTO_NAMEN"] = "CREATE TRIGGER TR_F_KONTO_NAMEN FOR F_KONTO_NAMEN BEFORE INSERT OR UPDATE AS BEGIN \
IF(NEW.KN_STUFE IS NULL) THEN NEW.KN_STUFE=0;\
IF(NEW.KN_EX_UKS IS NULL) THEN NEW.KN_EX_UKS=0;\
IF(NEW.KN_KONTO IS NULL) THEN NEW.KN_KONTO='';\
IF(NEW.KN_NAME IS NULL) THEN NEW.KN_NAME='';\
IF(NEW.KN_KONTO_FIBU IS NULL) THEN NEW.KN_KONTO_FIBU='';\
IF(NEW.KN_LEITER IS NULL) THEN NEW.KN_LEITER='';\
IF(NEW.KN_GRUPPE IS NULL) THEN NEW.KN_GRUPPE='';\
IF(NEW.KN_GLOBAL_ID IS NULL) THEN NEW.KN_GLOBAL_ID='';\
IF(NEW.KN_MODIFIED_BY IS NULL) THEN NEW.KN_MODIFIED_BY='';\
END;";
	}
}
