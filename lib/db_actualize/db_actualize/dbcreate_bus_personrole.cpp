#include "dbcreate_bus_personrole.h"

//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_PERSONROLE::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
											create table BUS_PERSONROLE (				\
											BPRL_ID INTEGER not null,					\
											BPRL_GLOBAL_ID VARCHAR(15) null,			\
											BPRL_DAT_LAST_MODIFIED TIMESTAMP not null,	\
											BPRL_PERSON_ID INTEGER not null,			\
											BPRL_ROLE_ID INTEGER not null,				\
											constraint BUS_PERSONROLE_PK primary key (BPRL_ID) ) ");

	lstSQL.append("alter table BUS_PERSONROLE						\
							   add constraint BUS_PERSON_BUS_PERSONROLE_FK1	\
							   foreign key (BPRL_PERSON_ID)					\
							   references BUS_PERSON (BPER_ID) ON DELETE CASCADE");

	lstSQL.append("alter table BUS_PERSONROLE					\
							   add constraint CORE_ROLE_BUS_PERSONROLE_FK1 \
							   foreign key (BPRL_ROLE_ID)					\
							   references CORE_ROLE (CROL_ID) ON DELETE CASCADE");
	
}

//probably common to all DB's:
void DbCreate_BUS_PERSONROLE::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();

	dboList["IND_BPRL_ROLE_ID"] = "create index IND_BPRL_ROLE_ID on BUS_PERSONROLE (BPRL_ROLE_ID)";
	dboList["IND_BPRL_PERSON_ID"] = "create index IND_BPRL_PERSON_ID on BUS_PERSONROLE (BPRL_PERSON_ID)";
}
