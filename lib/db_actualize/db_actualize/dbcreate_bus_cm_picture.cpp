#include "dbcreate_bus_cm_picture.h"


//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_CM_PICTURE::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
											create table BUS_CM_PICTURE ( \
											BCMPC_ID INTEGER not null,\
											BCMPC_GLOBAL_ID VARCHAR(15) null,\
											BCMPC_DAT_LAST_MODIFIED TIMESTAMP not null,\
											BCMPC_CONTACT_ID INTEGER not null,\
											BCMPC_DATE DATE null,\
											BCMPC_TEXT LONGVARCHAR null,\
											BCMPC_NAME VARCHAR(100) null,\
											BCMPC_PICTURE BINARY(10) null,\
											BCMPC_IS_DEFAULT SMALLINT not null,\
											BCMPC_BIG_PICTURE_ID INTEGER not null,\
											constraint BUS_CM_PICTURE_PK primary key (BCMPC_ID) ) "); 
	//constraint for table creation:
	lstSQL.append("\
											alter table BUS_CM_PICTURE\
											add constraint BUS_CONTACTS_PICTURE_FK1 foreign key (\
											BCMPC_CONTACT_ID)\
											references BUS_CM_CONTACT (\
											BCNT_ID)  \
											ON DELETE CASCADE");

	//constraint for table creation:
	lstSQL.append("\
											alter table BUS_CM_PICTURE\
											add constraint BUS_BIG_PICT_PICTURE_FK1 foreign key (\
											BCMPC_BIG_PICTURE_ID)\
											references BUS_BIG_PICTURE (\
											BPIC_ID) ");

}



//probably common to all DB's:
void DbCreate_BUS_CM_PICTURE::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
	dboList["IND_BCMPC_CONTACT_ID"] = "create index IND_BCMPC_CONTACT_ID on BUS_CM_PICTURE (BCMPC_CONTACT_ID)  ";  
}




void DbCreate_BUS_CM_PICTURE::DefineTriggers(SQLDBObjectCreate &dboList)
{
	//create triggers TOFIX: foe each DB, must be different

	dboList["TRG_BUS_CM_PICTURE_DELETE"]="\
										 CREATE TRIGGER TRG_BUS_CM_PICTURE_DELETE FOR BUS_CM_PICTURE\
										 ACTIVE AFTER DELETE AS \
										 BEGIN \
										 DELETE FROM BUS_BIG_PICTURE WHERE OLD.BCMPC_BIG_PICTURE_ID = BPIC_ID;\
										 END\
										 ";
}