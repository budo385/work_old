#include "dbcreate_f_bel_proj_listen.h"


//--------------------------------------------------
// ODBC - CREATE
//--------------------------------------------------
void DbCreate_F_BEL_PROJ_LISTEN::DefineFields(QStringList &lstSQL) 
{
	//Table schema statements for ODBC.
	lstSQL.append(" 	CREATE TABLE F_BEL_PROJ_LISTEN (\
										EBP_SEQUENCE INTEGER  NOT NULL,\
										EBP_PERS_NR VARCHAR(14)  NOT NULL,EBP_LISTE  BLOB  ,\
										EBP_PROZ DECIMAL(15,2) NOT NULL,\
										constraint F_BEL_PROJ_LISTEN_PK primary key (EBP_SEQUENCE))	");

}

void DbCreate_F_BEL_PROJ_LISTEN::DefineIndexes(SQLDBObjectCreate &dboList) 
{
		//add index definition by its name (important).
		dboList["IND_EBP_PERS_NR"] = "CREATE INDEX IND_EBP_PERS_NR ON F_BEL_PROJ_LISTEN (EBP_PERS_NR)";

}

void DbCreate_F_BEL_PROJ_LISTEN::DefineViews(SQLDBObjectCreate &dboList) 
{


	dboList["VW_F_BEL_PROJ_LISTEN"] = "CREATE VIEW VW_F_BEL_PROJ_LISTEN AS SELECT EBP_SEQUENCE,EBP_PERS_NR,EBP_LISTE,EBP_PROZ FROM F_BEL_PROJ_LISTEN";

	dboList["VWS_F_BEL_PROJ_LISTEN"] = "CREATE VIEW VWS_F_BEL_PROJ_LISTEN AS SELECT EBP_SEQUENCE,EBP_PERS_NR,EBP_LISTE,EBP_PROZ FROM F_BEL_PROJ_LISTEN";
}

void DbCreate_F_BEL_PROJ_LISTEN::DefineTriggers(SQLDBObjectCreate &dboList) 
{
	if(m_DbType==DBTYPE_FIREBIRD)
	{

		dboList["TR_F_BEL_PROJ_LISTEN"] = "CREATE TRIGGER TR_F_BEL_PROJ_LISTEN FOR F_BEL_PROJ_LISTEN BEFORE INSERT OR UPDATE AS BEGIN \
IF(NEW.EBP_PROZ IS NULL) THEN NEW.EBP_PROZ=0;\
IF(NEW.EBP_PERS_NR IS NULL) THEN NEW.EBP_PERS_NR='';\
END;";
	}
}
