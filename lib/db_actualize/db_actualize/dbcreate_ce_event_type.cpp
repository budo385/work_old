#include "dbcreate_ce_event_type.h"

//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_CE_EVENT_TYPE::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
											create table CE_EVENT_TYPE ( \
											CEVT_ID INTEGER not null,\
											CEVT_GLOBAL_ID VARCHAR(15) null,\
											CEVT_DAT_LAST_MODIFIED TIMESTAMP not null,\
											CEVT_CODE VARCHAR(50) not null,\
											CEVT_NAME VARCHAR(80) not null,\
											CEVT_SYSTEM_TYPE_ID INTEGER not null,\
											CEVT_STATUS_ID INTEGER null,\
											CEVT_PRIORITY INTEGER null,\
											CEVT_DIRECTION INTEGER null,\
											CEVT_DESCRIPTION LONGVARCHAR null,\
											constraint CE_EVENT_TYPE_PK primary key (CEVT_ID))");
}



//probably common to all DB's:
void DbCreate_CE_EVENT_TYPE::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
}


