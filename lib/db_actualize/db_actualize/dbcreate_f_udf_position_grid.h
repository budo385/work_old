#ifndef DBCREATE_F_UDF_POSITION_GRID_H
#define DBCREATE_F_UDF_POSITION_GRID_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_UDF_POSITION_GRID : public DbSqlTableCreation
{
public: 
	DbCreate_F_UDF_POSITION_GRID():DbSqlTableCreation(F_UDF_POSITION_GRID){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_UDF_POSITION_GRID_H
