#ifndef DBCREATE_F_UDF_DATE_H
#define DBCREATE_F_UDF_DATE_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_UDF_DATE : public DbSqlTableCreation
{
public: 
	DbCreate_F_UDF_DATE():DbSqlTableCreation(F_UDF_DATE){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_UDF_DATE_H
