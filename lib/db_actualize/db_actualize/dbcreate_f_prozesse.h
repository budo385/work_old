#ifndef DBCREATE_F_PROZESSE_H
#define DBCREATE_F_PROZESSE_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_PROZESSE : public DbSqlTableCreation
{
public: 
	DbCreate_F_PROZESSE():DbSqlTableCreation(F_PROZESSE){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_PROZESSE_H
