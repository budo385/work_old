#ifndef DBCREATE_F_PERSPROJLISTS_H
#define DBCREATE_F_PERSPROJLISTS_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_PERSPROJLISTS : public DbSqlTableCreation
{
public: 
	DbCreate_F_PERSPROJLISTS():DbSqlTableCreation(F_PERSPROJLISTS){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_PERSPROJLISTS_H
