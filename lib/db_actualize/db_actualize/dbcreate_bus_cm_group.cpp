#include "dbcreate_bus_cm_group.h"


//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_CM_GROUP::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
											create table BUS_CM_GROUP ( \
											BGCN_ID INTEGER not null,\
											BGCN_GLOBAL_ID VARCHAR(15) null,\
											BGCN_DAT_LAST_MODIFIED TIMESTAMP not null,\
											BGCN_CONTACT_ID INTEGER not null,\
											BGCN_ITEM_ID INTEGER not null,\
											BGCN_CMCA_VALID_FROM DATE null,\
											BGCN_CMCA_VALID_TO DATE null, constraint BUS_CM_GROUP_PK primary key (BGCN_ID) ) ");


	lstSQL.append("\
											alter table BUS_CM_GROUP\
											add constraint BUS_ITEMS_CM_GROUP_FK1 foreign key (\
											BGCN_ITEM_ID)\
											references BUS_GROUP_ITEMS (\
											BGIT_ID) \
											ON DELETE CASCADE");

	lstSQL.append("\
											alter table BUS_CM_GROUP\
											add constraint BUS_CONT_CM_GROUP_FK1 foreign key (\
											BGCN_CONTACT_ID)\
											references BUS_CM_CONTACT (\
											BCNT_ID) \
											ON DELETE CASCADE");
}



//probably common to all DB's:
void DbCreate_BUS_CM_GROUP::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
	dboList["IND_BGCN_ITEM_ID"] = "create index IND_BGCN_ITEM_ID on BUS_CM_GROUP (	BGCN_ITEM_ID  )  ";  
	dboList["IND_BGCN_ITEM_CONT_ID"] = "create unique index IND_BGCN_ITEM_CONT_ID on BUS_CM_GROUP (BGCN_ITEM_ID,BGCN_CONTACT_ID)  ";  
	dboList["IND_BGCN_CONT_ID"] = "create index IND_BGCN_CONT_ID on BUS_CM_GROUP (	BGCN_CONTACT_ID  )  ";  
		
}




