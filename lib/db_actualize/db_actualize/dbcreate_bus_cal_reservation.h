#ifndef DBCREATE_BUS_CAL_RESERVATION_H
#define DBCREATE_BUS_CAL_RESERVATION_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_BUS_CAL_RESERVATION : public DbSqlTableCreation
{
public:
	DbCreate_BUS_CAL_RESERVATION():DbSqlTableCreation(BUS_CAL_RESERVATION){};  //init table
	

private:
	//create generic Table def:
	void DefineFields(QStringList &lstSQL);

	//probably common to all DB's:
	void DefineIndexes(SQLDBObjectCreate &dboList);
};

#endif // DBCREATE_BUS_CAL_RESERVATION_H
