#ifndef DBCREATE_F_LEISTUNGSARTEN_H
#define DBCREATE_F_LEISTUNGSARTEN_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_LEISTUNGSARTEN : public DbSqlTableCreation
{
public: 
	DbCreate_F_LEISTUNGSARTEN():DbSqlTableCreation(F_LEISTUNGSARTEN){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_LEISTUNGSARTEN_H
