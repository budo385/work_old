#ifndef DBCREATE_CORE_DATABASE_INFO_H
#define DBCREATE_CORE_DATABASE_INFO_H


#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_CORE_DATABASE_INFO : public DbSqlTableCreation
{
public:
	DbCreate_CORE_DATABASE_INFO():DbSqlTableCreation(CORE_DATABASE_INFO){};  //init table


private:
	//create generic Table def:
	void DefineFields(QStringList &lstSQL);
	void DefineIndexes(SQLDBObjectCreate &dboList);
};

#endif // DBCREATE_CORE_DATABASE_INFO_H
