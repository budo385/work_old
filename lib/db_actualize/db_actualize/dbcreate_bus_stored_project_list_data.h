#ifndef DBCREATE_BUS_STORED_PROJECT_LIST_DATA_H
#define DBCREATE_BUS_STORED_PROJECT_LIST_DATA_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_BUS_STORED_PROJECT_LIST_DATA : public DbSqlTableCreation
{
public:
	DbCreate_BUS_STORED_PROJECT_LIST_DATA():DbSqlTableCreation(BUS_STORED_PROJECT_LIST_DATA){};  //init table
	

private:
	//create generic Table def:
	void DefineFields(QStringList &lstSQL);

	//probably common to all DB's:
	void DefineIndexes(SQLDBObjectCreate &dboList);
};
#endif // DBCREATE_BUS_STORED_PROJECT_LIST_DATA_H
