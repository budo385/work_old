#ifndef DBCREATE_TEST_TREE_H
#define DBCREATE_TEST_TREE_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_TEST_TREE : public DbSqlTableCreation
{

public:
	DbCreate_TEST_TREE():DbSqlTableCreation(TEST_TREE){};  //init to TEST_TREE table

	

private:

	//create generic Table def:
	void DefineFields(QStringList &lstSQL);

	//probably common to all DB's:
	void DefineIndexes(SQLDBObjectCreate &dboList);
    
};

#endif // DBCREATE_TEST_TREE_H
