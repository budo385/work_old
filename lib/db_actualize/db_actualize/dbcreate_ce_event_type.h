#ifndef DBCREATE_CE_EVENT_TYPE_H
#define DBCREATE_CE_EVENT_TYPE_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_CE_EVENT_TYPE : public DbSqlTableCreation
{
public:
	DbCreate_CE_EVENT_TYPE():DbSqlTableCreation(CE_EVENT_TYPE){};  //init table
	

private:
	//create generic Table def:
	void DefineFields(QStringList &lstSQL);

	//probably common to all DB's:
	void DefineIndexes(SQLDBObjectCreate &dboList);
};
#endif // DBCREATE_CE_EVENT_TYPE_H
