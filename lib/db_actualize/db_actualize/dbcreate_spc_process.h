#ifndef DBCREATE_SPC_PROCESS_H
#define DBCREATE_SPC_PROCESS_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_SPC_PROCESS : public DbSqlTableCreation
{
public: 
	DbCreate_SPC_PROCESS():DbSqlTableCreation(SPC_PROCESS){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // SPC_PROCESS_H
