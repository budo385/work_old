#ifndef DBCREATE_F_PROJEKTART_H
#define DBCREATE_F_PROJEKTART_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_PROJEKTART : public DbSqlTableCreation
{
public: 
	DbCreate_F_PROJEKTART():DbSqlTableCreation(F_PROJEKTART){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_PROJEKTART_H
