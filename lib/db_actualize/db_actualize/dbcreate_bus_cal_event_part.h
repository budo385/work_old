#ifndef DBCREATE_BUS_CAL_EVENT_PART_H
#define DBCREATE_BUS_CAL_EVENT_PART_H


#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_BUS_CAL_EVENT_PART : public DbSqlTableCreation
{
public:
	DbCreate_BUS_CAL_EVENT_PART():DbSqlTableCreation(BUS_CAL_EVENT_PART){};  //init table
	

private:
	//create generic Table def:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);


	//probably common to all DB's:
	void DefineIndexes(SQLDBObjectCreate &dboList);
};


#endif 
