#ifndef DBCREATE_F_MODULE_LICENCES_H
#define DBCREATE_F_MODULE_LICENCES_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_MODULE_LICENCES : public DbSqlTableCreation
{
public: 
	DbCreate_F_MODULE_LICENCES():DbSqlTableCreation(F_MODULE_LICENCES){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_MODULE_LICENCES_H
