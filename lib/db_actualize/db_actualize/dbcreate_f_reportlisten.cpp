#include "dbcreate_f_reportlisten.h"


//--------------------------------------------------
// ODBC - CREATE
//--------------------------------------------------
void DbCreate_F_REPORTLISTEN::DefineFields(QStringList &lstSQL) 
{
	//Table schema statements for ODBC.
	lstSQL.append(" 	CREATE TABLE F_REPORTLISTEN (\
										ERL_SEQUENCE INTEGER  NOT NULL,\
										ERL_CODE VARCHAR(23)  NOT NULL,ERL_NAME VARCHAR(111)  NOT NULL,\
										ERL_LIST VARCHAR(1101)  NOT NULL,\
										constraint F_REPORTLISTEN_PK primary key (ERL_SEQUENCE))	");

}

void DbCreate_F_REPORTLISTEN::DefineIndexes(SQLDBObjectCreate &dboList) 
{
		//add index definition by its name (important).
		dboList["IND_ERL_CODE"] = "CREATE INDEX IND_ERL_CODE ON F_REPORTLISTEN (ERL_CODE)";

}

void DbCreate_F_REPORTLISTEN::DefineViews(SQLDBObjectCreate &dboList) 
{


	dboList["VW_F_REPORTLISTEN"] = "CREATE VIEW VW_F_REPORTLISTEN AS SELECT ERL_SEQUENCE,ERL_CODE,ERL_NAME,ERL_LIST FROM F_REPORTLISTEN";

	dboList["VWS_F_REPORTLISTEN"] = "CREATE VIEW VWS_F_REPORTLISTEN AS SELECT ERL_SEQUENCE,ERL_CODE,ERL_NAME,ERL_LIST FROM F_REPORTLISTEN";
}

void DbCreate_F_REPORTLISTEN::DefineTriggers(SQLDBObjectCreate &dboList) 
{
	if(m_DbType==DBTYPE_FIREBIRD)
	{

		dboList["TR_F_REPORTLISTEN"] = "CREATE TRIGGER TR_F_REPORTLISTEN FOR F_REPORTLISTEN BEFORE INSERT OR UPDATE AS BEGIN \
IF(NEW.ERL_CODE IS NULL) THEN NEW.ERL_CODE='';\
IF(NEW.ERL_NAME IS NULL) THEN NEW.ERL_NAME='';\
IF(NEW.ERL_LIST IS NULL) THEN NEW.ERL_LIST='';\
END;";
	}
}
