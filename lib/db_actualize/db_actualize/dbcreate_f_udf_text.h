#ifndef DBCREATE_F_UDF_TEXT_H
#define DBCREATE_F_UDF_TEXT_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_UDF_TEXT : public DbSqlTableCreation
{
public: 
	DbCreate_F_UDF_TEXT():DbSqlTableCreation(F_UDF_TEXT){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_UDF_TEXT_H
