#include "dbcreate_bus_dm_user_paths.h"


//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_DM_USER_PATHS::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
											create table BUS_DM_USER_PATHS ( \
											BDMU_ID INTEGER not null,\
											BDMU_GLOBAL_ID VARCHAR(15) null,\
											BDMU_DAT_LAST_MODIFIED TIMESTAMP not null,\
											BDMU_APP_PATH LONGVARCHAR null,\
											BDMU_PRIORITY SMALLINT null,\
											BDMU_OWNER_ID INTEGER not null,\
											BDMU_APPLICATION_ID INTEGER not null, constraint BUS_DM_USER_PATHS_PK primary key (BDMU_ID) ) ");

	//if user is deleted: delete his paths:
	lstSQL.append("\
											alter table BUS_DM_USER_PATHS\
											add constraint BUS_PER_DM_US_PATH_FK1 foreign key (\
											BDMU_OWNER_ID)\
											references BUS_PERSON (\
											BPER_ID) \
											ON DELETE CASCADE"); 

	lstSQL.append("\
											alter table BUS_DM_USER_PATHS\
											add constraint BUS_DM_APP_DM_USR_PATH_FK1 foreign key (\
											BDMU_APPLICATION_ID)\
											references BUS_DM_APPLICATIONS (\
											BDMA_ID) ON DELETE CASCADE"); 

}



//probably common to all DB's:
void DbCreate_BUS_DM_USER_PATHS::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
	dboList["IND_DM_USR_OWNER_ID"] = "create index IND_DM_USR_OWNER_ID on BUS_DM_USER_PATHS (		BDMU_OWNER_ID ) ";
	dboList["IND_DM_USR_APP_ID"] = "create index IND_DM_USR_APP_ID on BUS_DM_USER_PATHS (		BDMU_APPLICATION_ID ) "; 
	dboList["IND_DM_USR_PRIORITY"] = "create index IND_DM_USR_PRIORITY on BUS_DM_USER_PATHS (		BDMU_PRIORITY ) "; 
	dboList["IND_DM_USR_COMBINE"] = "create index IND_DM_USR_COMBINE on BUS_DM_USER_PATHS (		BDMU_OWNER_ID,BDMU_APPLICATION_ID )";  
}
