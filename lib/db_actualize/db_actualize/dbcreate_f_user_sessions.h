#ifndef DBCREATE_F_USER_SESSIONS_H
#define DBCREATE_F_USER_SESSIONS_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_USER_SESSIONS : public DbSqlTableCreation
{
public: 
	DbCreate_F_USER_SESSIONS():DbSqlTableCreation(F_USER_SESSIONS){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_USER_SESSIONS_H
