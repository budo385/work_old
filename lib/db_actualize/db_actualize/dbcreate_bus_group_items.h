#ifndef DBCREATE_BUS_GROUP_ITEMS_H
#define DBCREATE_BUS_GROUP_ITEMS_H


#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_BUS_GROUP_ITEMS : public DbSqlTableCreation
{
public:
	DbCreate_BUS_GROUP_ITEMS():DbSqlTableCreation(BUS_GROUP_ITEMS){};  //init table
	

private:
	//create generic Table def:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);

	//probably common to all DB's:
	void DefineIndexes(SQLDBObjectCreate &dboList);
};


#endif // DBCREATE_BUS_GROUP_ITEMS_H
