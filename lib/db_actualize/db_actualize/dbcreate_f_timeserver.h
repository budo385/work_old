#ifndef DBCREATE_F_TIMESERVER_H
#define DBCREATE_F_TIMESERVER_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_TIMESERVER : public DbSqlTableCreation
{
public: 
	DbCreate_F_TIMESERVER():DbSqlTableCreation(F_TIMESERVER){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_TIMESERVER_H
