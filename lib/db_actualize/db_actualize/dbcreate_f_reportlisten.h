#ifndef DBCREATE_F_REPORTLISTEN_H
#define DBCREATE_F_REPORTLISTEN_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_REPORTLISTEN : public DbSqlTableCreation
{
public: 
	DbCreate_F_REPORTLISTEN():DbSqlTableCreation(F_REPORTLISTEN){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_REPORTLISTEN_H
