#ifndef DBCREATE_F_UDF_VIEW_H
#define DBCREATE_F_UDF_VIEW_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_UDF_VIEW : public DbSqlTableCreation
{
public: 
	DbCreate_F_UDF_VIEW():DbSqlTableCreation(F_UDF_VIEW){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_UDF_VIEW_H
