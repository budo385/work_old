#ifndef DBCREATE_BUS_NMRX_ROLE_H
#define DBCREATE_BUS_NMRX_ROLE_H


#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_BUS_NMRX_ROLE : public DbSqlTableCreation
{
public:
	DbCreate_BUS_NMRX_ROLE():DbSqlTableCreation(BUS_NMRX_ROLE){};  //init table
	

private:
	//create generic Table def:
	void DefineFields(QStringList &lstSQL);

	//probably common to all DB's:
	void DefineIndexes(SQLDBObjectCreate &dboList);
};


#endif // DBCREATE_BUS_NMRX_ROLE_H
