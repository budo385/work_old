#include "dbcreate_f_periodenart.h"


//--------------------------------------------------
// ODBC - CREATE
//--------------------------------------------------
void DbCreate_F_PERIODENART::DefineFields(QStringList &lstSQL) 
{
	//Table schema statements for ODBC.
	lstSQL.append(" 	CREATE TABLE F_PERIODENART (\
										EPA_SEQUENCE INTEGER  NOT NULL,\
										EPA_CODE DECIMAL(3) NOT NULL,EPA_NAME VARCHAR(89)  NOT NULL,\
										EPA_SORT VARCHAR(12)  NOT NULL,\
										constraint F_PERIODENART_PK primary key (EPA_SEQUENCE))	");

}

void DbCreate_F_PERIODENART::DefineIndexes(SQLDBObjectCreate &dboList) 
{
		//add index definition by its name (important).
		dboList["IND_EPA_CODE"] = "CREATE INDEX IND_EPA_CODE ON F_PERIODENART (EPA_CODE)";

}

void DbCreate_F_PERIODENART::DefineViews(SQLDBObjectCreate &dboList) 
{


	dboList["VW_F_PERIODENART"] = "CREATE VIEW VW_F_PERIODENART AS SELECT EPA_SEQUENCE,EPA_CODE,EPA_NAME,EPA_SORT FROM F_PERIODENART";

	dboList["VWS_F_PERIODENART"] = "CREATE VIEW VWS_F_PERIODENART AS SELECT EPA_SEQUENCE,EPA_CODE,EPA_NAME,EPA_SORT FROM F_PERIODENART";
}

void DbCreate_F_PERIODENART::DefineTriggers(SQLDBObjectCreate &dboList) 
{
	if(m_DbType==DBTYPE_FIREBIRD)
	{

		dboList["TR_F_PERIODENART"] = "CREATE TRIGGER TR_F_PERIODENART FOR F_PERIODENART BEFORE INSERT OR UPDATE AS BEGIN \
IF(NEW.EPA_CODE IS NULL) THEN NEW.EPA_CODE=0;\
IF(NEW.EPA_NAME IS NULL) THEN NEW.EPA_NAME='';\
IF(NEW.EPA_SORT IS NULL) THEN NEW.EPA_SORT='';\
END;";
	}
}
