#include "dbcreate_bus_cm_types.h"

//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_CM_TYPES::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
											create table BUS_CM_TYPES ( \
											BCMT_ID INTEGER not null,\
											BCMT_GLOBAL_ID VARCHAR(15) null,\
											BCMT_DAT_LAST_MODIFIED TIMESTAMP not null,\
											BCMT_TYPE_NAME VARCHAR(100) not null,\
											BCMT_ENTITY_TYPE INTEGER not null,\
											BCMT_IS_DEFAULT SMALLINT null,\
											BCMT_SYSTEM_TYPE INTEGER null, \
											BCMT_IS_FORMATTING_ALLOWED SMALLINT null,\
											BCMT_COLOR VARCHAR(100) null, \
											BCMT_PICTURE LONGVARBINARY null, \
											BCMT_EMAIL_TEMPLATE_ID INTEGER null,\
											constraint BUS_CM_TYPES_PK primary key (BCMT_ID) ) "); 


	
	lstSQL.append("\
								   alter table BUS_CM_TYPES\
								   add constraint BUS_TYPES_EMAIL_TEMP_FK1 foreign key (\
								   BCMT_EMAIL_TEMPLATE_ID)\
								   references BUS_EMAIL (\
								   BEM_ID) ON DELETE SET NULL");
}


//probably common to all DB's:
void DbCreate_BUS_CM_TYPES::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
	dboList["IND_BCMT_ENTITY_TYPE"] = "create index IND_BCMT_ENTITY_TYPE on BUS_CM_TYPES (BCMT_ENTITY_TYPE) ";  
}



