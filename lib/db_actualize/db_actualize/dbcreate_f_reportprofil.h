#ifndef DBCREATE_F_REPORTPROFIL_H
#define DBCREATE_F_REPORTPROFIL_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_REPORTPROFIL : public DbSqlTableCreation
{
public: 
	DbCreate_F_REPORTPROFIL():DbSqlTableCreation(F_REPORTPROFIL){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_REPORTPROFIL_H
