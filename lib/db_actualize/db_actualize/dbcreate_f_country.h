#ifndef DBCREATE_F_COUNTRY_H
#define DBCREATE_F_COUNTRY_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_COUNTRY : public DbSqlTableCreation
{
public: 
	DbCreate_F_COUNTRY():DbSqlTableCreation(F_COUNTRY){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_COUNTRY_H
