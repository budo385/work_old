#ifndef DBCREATE_F_PROJECTUSERDEFINED_CHAR_H
#define DBCREATE_F_PROJECTUSERDEFINED_CHAR_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_PROJECTUSERDEFINED_CHAR : public DbSqlTableCreation
{
public: 
	DbCreate_F_PROJECTUSERDEFINED_CHAR():DbSqlTableCreation(F_PROJECTUSERDEFINED_CHAR){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_PROJECTUSERDEFINED_CHAR_H
