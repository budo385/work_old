#include "dbcreate_bus_dm_applications.h"


//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_DM_APPLICATIONS::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
											create table BUS_DM_APPLICATIONS (\
											BDMA_ID INTEGER not null,\
											BDMA_GLOBAL_ID VARCHAR(15) null,\
											BDMA_DAT_LAST_MODIFIED TIMESTAMP not null,\
											BDMA_CODE VARCHAR(50) not null,\
											BDMA_NAME VARCHAR(250) not null,\
											BDMA_TEMPLATE_ID INTEGER null,\
											BDMA_ICON LONGVARBINARY null,\
											BDMA_EXTENSIONS VARCHAR(250) null,\
											BDMA_DESCRIPTION LONGVARCHAR null,\
											BDMA_OPEN_PARAMETER VARCHAR(250) null,\
											BDMA_APPLICATION_TYPE SMALLINT null,\
											BDMA_READ_ONLY SMALLINT null, \
											BDMA_NO_FILE_DOCUMENT SMALLINT null,\
											BDMA_INSTALL_APP_TEMPL_ID INTEGER null,\
											BDMA_INSTALL_VIEWER_TEMPL_ID INTEGER null,\
											BDMA_ENCODE_PARAMETERS SMALLINT null,\
											BDMA_SET_READ_ONLY_FLAG SMALLINT null, \
											constraint BUS_DM_APPLICATIONS_PK primary key (BDMA_ID) )");  

	lstSQL.append("\
											alter table BUS_DM_APPLICATIONS\
											add constraint BUS_DM_DOC_APP_FK1 foreign key (\
											BDMA_TEMPLATE_ID)\
											references BUS_DM_DOCUMENTS (\
											BDMD_ID) ON DELETE SET NULL ");

	lstSQL.append("\
								   alter table BUS_DM_APPLICATIONS\
								   add constraint BUS_DM_APPLICATIONS_FK2 foreign key (\
								   BDMA_INSTALL_APP_TEMPL_ID)\
								   references BUS_DM_DOCUMENTS (\
								   BDMD_ID)  ON DELETE SET NULL \
								   ");


	lstSQL.append("\
								   alter table BUS_DM_APPLICATIONS\
								   add constraint BUS_DM_APPLICATIONS_FK3 foreign key (\
								   BDMA_INSTALL_VIEWER_TEMPL_ID)\
								   references BUS_DM_DOCUMENTS (\
								   BDMD_ID)  ON DELETE SET NULL \
								   ");


}



//probably common to all DB's:
void DbCreate_BUS_DM_APPLICATIONS::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
	dboList["IND_DM_APP_NAME"] = "create index IND_DM_APP_NAME on BUS_DM_APPLICATIONS (BDMA_NAME) ";  
	dboList["IND_BDMA_TEMPLATE_ID"] = "create index IND_BDMA_TEMPLATE_ID on BUS_DM_APPLICATIONS (BDMA_TEMPLATE_ID) ";  
	dboList["IND_BDMA_INSTALL_APP"] = "create index IND_BDMA_INSTALL_APP on BUS_DM_APPLICATIONS (BDMA_INSTALL_APP_TEMPL_ID) ";  
	dboList["IND_BDMA_INSTALL_VIEW"] = "create index IND_BDMA_INSTALL_VIEW on BUS_DM_APPLICATIONS (BDMA_TEMPLATE_ID) ";  
}


