#include "dbcreate_bus_cm_address.h"



//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_CM_ADDRESS::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
											create table BUS_CM_ADDRESS ( \
											BCMA_ID INTEGER not null,\
											BCMA_GLOBAL_ID VARCHAR(15) null,\
											BCMA_DAT_LAST_MODIFIED TIMESTAMP not null,\
											BCMA_CONTACT_ID INTEGER not null,\
											BCMA_FORMATEDADDRESS LONGVARCHAR null,\
											BCMA_NAME VARCHAR(80) null,\
											BCMA_FIRSTNAME VARCHAR(80) null,\
											BCMA_LASTNAME VARCHAR(80) null,\
											BCMA_MIDDLENAME VARCHAR(80) null,\
											BCMA_ORGANIZATIONNAME VARCHAR(200) null,\
											BCMA_ORGANIZATIONNAME_2 VARCHAR(200) null,\
											BCMA_TITLE VARCHAR(30) null,\
											BCMA_SALUTATION VARCHAR(50) null,\
											BCMA_LANGUGAGE_CODE VARCHAR(50) null,\
											BCMA_COUNTRY_CODE VARCHAR(10) null,\
											BCMA_COUNTRY_NAME VARCHAR(50) null,\
											BCMA_STREET VARCHAR(200) null,\
											BCMA_CITY VARCHAR(200) null,\
											BCMA_ZIP VARCHAR(15) null,\
											BCMA_REGION VARCHAR(50) null,\
											BCMA_POBOX VARCHAR(50) null,\
											BCMA_POBOXZIP VARCHAR(50) null,\
											BCMA_OFFICECODE VARCHAR(50) null,\
											BCMA_EXT_ID INTEGER null,\
											BCMA_IS_DEFAULT SMALLINT not null,\
											BCMA_FORMATSCHEMA_ID INTEGER null,\
											BCMA_PARSESCHEMA_ID INTEGER null,\
											BCMA_SHORT_SALUTATION VARCHAR(50) null, constraint BUS_CM_ADDRESS_PK primary key (BCMA_ID) ) "); 
											

	lstSQL.append("\
											alter table BUS_CM_ADDRESS\
											add constraint BUS_CONT_ADDR_FK1 foreign key (\
											BCMA_CONTACT_ID)\
											references BUS_CM_CONTACT (\
											BCNT_ID)  \
											ON DELETE CASCADE");


	//if there is one item with given type: can not delete type
	lstSQL.append("\
											alter table BUS_CM_ADDRESS\
											add constraint BUS_ADDR_SCHEMAS_ADDR_FK1 foreign key (\
											BCMA_FORMATSCHEMA_ID)\
											references BUS_CM_ADDRESS_SCHEMAS (\
											BCMAS_ID)  \
											");


	lstSQL.append("\
											alter table BUS_CM_ADDRESS\
											add constraint BUS_ADDR_SCHEMAS_ADDR_FK2 foreign key (\
											BCMA_PARSESCHEMA_ID)\
											references BUS_CM_ADDRESS_SCHEMAS (\
											BCMAS_ID)  \
											");
}

//probably common to all DB's:
void DbCreate_BUS_CM_ADDRESS::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
	dboList["IND_BCMA_CONTACT_ID"] = "create index IND_BCMA_CONTACT_ID on BUS_CM_ADDRESS (BCMA_CONTACT_ID)";  
	dboList["IND_BCMA_FORMATSCHEMA_ID"] = "create index IND_BCMA_FORMATSCHEMA_ID on BUS_CM_ADDRESS (BCMA_FORMATSCHEMA_ID)  ";  
	dboList["IND_BCMA_PARSESCHEMA_ID"] = "create index IND_BCMA_PARSESCHEMA_ID on BUS_CM_ADDRESS (BCMA_PARSESCHEMA_ID)";  
}


