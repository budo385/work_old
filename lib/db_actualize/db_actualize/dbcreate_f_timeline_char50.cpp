#include "dbcreate_f_timeline_char50.h"


//--------------------------------------------------
// ODBC - CREATE
//--------------------------------------------------
void DbCreate_F_TIMELINE_CHAR50::DefineFields(QStringList &lstSQL) 
{
	//Table schema statements for ODBC.
	lstSQL.append(" 	CREATE TABLE F_TIMELINE_CHAR50 (\
										ETLC5_SEQUENCE INTEGER  NOT NULL,\
										ETLC5_ENTITY_ID VARCHAR(84)  NOT NULL,ETLC5_ENTITY_TABLE VARCHAR(56)  NOT NULL,\
										ETLC5_ENTITY_FIELD VARCHAR(78)  NOT NULL,ETLC5_FROM_DATE DATETIME ,\
										ETLC5_DATA VARCHAR(56)  NOT NULL,ETLC5_DAT_AENDERUNG DATETIME ,\
										ETLC5_MODIFIED_BY VARCHAR(23)  NOT NULL,ETLC5_GLOBAL_ID VARCHAR(14)  NOT NULL,\
										ETLC5_DAT_EROEFFNUNG DATETIME ,ETLC5_COMB_FI VARCHAR(84)  NOT NULL,\
										constraint F_TIMELINE_CHAR50_PK primary key (ETLC5_SEQUENCE))	");

}

void DbCreate_F_TIMELINE_CHAR50::DefineIndexes(SQLDBObjectCreate &dboList) 
{
		//add index definition by its name (important).
		dboList["IND_ETLC5_SEQUENCE"] = "CREATE UNIQUE INDEX IND_ETLC5_SEQUENCE ON F_TIMELINE_CHAR50 (ETLC5_SEQUENCE)";
		dboList["IND_ETLC5_ENTITY_ID"] = "CREATE INDEX IND_ETLC5_ENTITY_ID ON F_TIMELINE_CHAR50 (ETLC5_ENTITY_ID)";
		dboList["IND_ETLC5_ENTITY_TABLE"] = "CREATE INDEX IND_ETLC5_ENTITY_TABLE ON F_TIMELINE_CHAR50 (ETLC5_ENTITY_TABLE)";
		dboList["IND_ETLC5_ENTITY_FIELD"] = "CREATE INDEX IND_ETLC5_ENTITY_FIELD ON F_TIMELINE_CHAR50 (ETLC5_ENTITY_FIELD)";
		dboList["IND_ETLC5_DAT_AENDERUNG"] = "CREATE INDEX IND_ETLC5_DAT_AENDERUNG ON F_TIMELINE_CHAR50 (ETLC5_DAT_AENDERUNG)";
		dboList["IND_ETLC5_GLOBAL_ID"] = "CREATE INDEX IND_ETLC5_GLOBAL_ID ON F_TIMELINE_CHAR50 (ETLC5_GLOBAL_ID)";
		dboList["IND_ETLC5_COMB_FI"] = "CREATE INDEX IND_ETLC5_COMB_FI ON F_TIMELINE_CHAR50 (ETLC5_COMB_FI)";

}

void DbCreate_F_TIMELINE_CHAR50::DefineViews(SQLDBObjectCreate &dboList) 
{


	dboList["VW_F_TIMELINE_CHAR50"] = "CREATE VIEW VW_F_TIMELINE_CHAR50 AS SELECT ETLC5_SEQUENCE,ETLC5_ENTITY_ID,ETLC5_ENTITY_TABLE,ETLC5_ENTITY_FIELD ,ETLC5_FROM_DATE,ETLC5_DATA,ETLC5_DAT_AENDERUNG,ETLC5_MODIFIED_BY ,ETLC5_GLOBAL_ID,ETLC5_DAT_EROEFFNUNG,ETLC5_COMB_FI FROM F_TIMELINE_CHAR50";

	dboList["VWS_F_TIMELINE_CHAR50"] = "CREATE VIEW VWS_F_TIMELINE_CHAR50 AS SELECT ETLC5_SEQUENCE,ETLC5_ENTITY_ID,ETLC5_ENTITY_TABLE,ETLC5_ENTITY_FIELD ,ETLC5_FROM_DATE,ETLC5_DATA,ETLC5_DAT_AENDERUNG,ETLC5_MODIFIED_BY ,ETLC5_GLOBAL_ID,ETLC5_DAT_EROEFFNUNG,ETLC5_COMB_FI FROM F_TIMELINE_CHAR50";
}

void DbCreate_F_TIMELINE_CHAR50::DefineTriggers(SQLDBObjectCreate &dboList) 
{
	if(m_DbType==DBTYPE_FIREBIRD)
	{

		dboList["TR_F_TIMELINE_CHAR50"] = "CREATE TRIGGER TR_F_TIMELINE_CHAR50 FOR F_TIMELINE_CHAR50 BEFORE INSERT OR UPDATE AS BEGIN \
IF(NEW.ETLC5_ENTITY_ID IS NULL) THEN NEW.ETLC5_ENTITY_ID='';\
IF(NEW.ETLC5_ENTITY_TABLE IS NULL) THEN NEW.ETLC5_ENTITY_TABLE='';\
IF(NEW.ETLC5_ENTITY_FIELD IS NULL) THEN NEW.ETLC5_ENTITY_FIELD='';\
IF(NEW.ETLC5_DATA IS NULL) THEN NEW.ETLC5_DATA='';\
IF(NEW.ETLC5_MODIFIED_BY IS NULL) THEN NEW.ETLC5_MODIFIED_BY='';\
IF(NEW.ETLC5_GLOBAL_ID IS NULL) THEN NEW.ETLC5_GLOBAL_ID='';\
IF(NEW.ETLC5_COMB_FI IS NULL) THEN NEW.ETLC5_COMB_FI='';\
END;";
	}
}
