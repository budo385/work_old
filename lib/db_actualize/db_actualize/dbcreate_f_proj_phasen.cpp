#include "dbcreate_f_proj_phasen.h"


//--------------------------------------------------
// ODBC - CREATE
//--------------------------------------------------
void DbCreate_F_PROJ_PHASEN::DefineFields(QStringList &lstSQL) 
{
	//Table schema statements for ODBC.
	lstSQL.append(" 	CREATE TABLE F_PROJ_PHASEN (\
										EPP_SEQUENCE INTEGER  NOT NULL,\
										EPP_DATUM DATETIME ,EPP_BESCHREIB VARCHAR(8000)  NOT NULL,\
										EPP_PROJ_NR VARCHAR(56)  NOT NULL,EPP_VISUM VARCHAR(10)  NOT NULL,\
										EPP_ERLEDIGT DECIMAL(3) NOT NULL,EPP_ERLEDIGT_AM DATETIME ,\
										EPP_CI_EV VARCHAR(2)  NOT NULL,EPP_STAND DECIMAL(15,2) NOT NULL,\
										EPP_MEILENSTEIN DECIMAL(3) NOT NULL,EPP_DAT_AENDERUNG DATETIME ,\
										EPP_GLOBAL_ID VARCHAR(14)  NOT NULL,EPP_RELATIV DECIMAL(3) NOT NULL,\
										EPP_REL_DAT DECIMAL(3) NOT NULL,EPP_REL_PER_TYP DECIMAL(3) NOT NULL,\
										EPP_REL_DAUER DECIMAL(15) NOT NULL,EPP_CI_PD VARCHAR(2)  NOT NULL,\
										constraint F_PROJ_PHASEN_PK primary key (EPP_SEQUENCE))	");

}

void DbCreate_F_PROJ_PHASEN::DefineIndexes(SQLDBObjectCreate &dboList) 
{
		//add index definition by its name (important).
		dboList["IND_EPP_DATUM"] = "CREATE INDEX IND_EPP_DATUM ON F_PROJ_PHASEN (EPP_DATUM)";
		dboList["IND_EPP_PROJ_NR"] = "CREATE INDEX IND_EPP_PROJ_NR ON F_PROJ_PHASEN (EPP_PROJ_NR)";
		dboList["IND_EPP_ERLEDIGT"] = "CREATE INDEX IND_EPP_ERLEDIGT ON F_PROJ_PHASEN (EPP_ERLEDIGT)";
		dboList["IND_EPP_CI_EV"] = "CREATE INDEX IND_EPP_CI_EV ON F_PROJ_PHASEN (EPP_VISUM,EPP_ERLEDIGT)";
		dboList["IND_EPP_VISUM"] = "CREATE INDEX IND_EPP_VISUM ON F_PROJ_PHASEN (EPP_VISUM)";
		dboList["IND_EPP_GLOBAL_ID"] = "CREATE INDEX IND_EPP_GLOBAL_ID ON F_PROJ_PHASEN (EPP_GLOBAL_ID)";
		dboList["IND_EPP_DAT_AENDERUNG"] = "CREATE INDEX IND_EPP_DAT_AENDERUNG ON F_PROJ_PHASEN (EPP_DAT_AENDERUNG)";
		dboList["IND_EPP_REL_DAT"] = "CREATE INDEX IND_EPP_REL_DAT ON F_PROJ_PHASEN (EPP_REL_DAT)";
		dboList["IND_EPP_CI_PD"] = "CREATE INDEX IND_EPP_CI_PD ON F_PROJ_PHASEN (EPP_PROJ_NR,EPP_REL_DAT)";

}

void DbCreate_F_PROJ_PHASEN::DefineViews(SQLDBObjectCreate &dboList) 
{


	dboList["VW_F_PROJ_PHASEN"] = "CREATE VIEW VW_F_PROJ_PHASEN AS SELECT EPP_SEQUENCE,EPP_DATUM,EPP_BESCHREIB,EPP_PROJ_NR ,EPP_VISUM,EPP_ERLEDIGT,EPP_ERLEDIGT_AM,EPP_CI_EV ,EPP_STAND,EPP_MEILENSTEIN,EPP_DAT_AENDERUNG,EPP_GLOBAL_ID ,EPP_RELATIV,EPP_REL_DAT,EPP_REL_PER_TYP,EPP_REL_DAUER ,EPP_CI_PD FROM F_PROJ_PHASEN";

	dboList["VWS_F_PROJ_PHASEN"] = "CREATE VIEW VWS_F_PROJ_PHASEN AS SELECT EPP_SEQUENCE,EPP_DATUM,EPP_BESCHREIB,EPP_PROJ_NR ,EPP_VISUM,EPP_ERLEDIGT,EPP_ERLEDIGT_AM,EPP_CI_EV ,EPP_STAND,EPP_MEILENSTEIN,EPP_DAT_AENDERUNG,EPP_GLOBAL_ID ,EPP_RELATIV,EPP_REL_DAT,EPP_REL_PER_TYP,EPP_REL_DAUER ,EPP_CI_PD FROM F_PROJ_PHASEN";
}

void DbCreate_F_PROJ_PHASEN::DefineTriggers(SQLDBObjectCreate &dboList) 
{
	if(m_DbType==DBTYPE_FIREBIRD)
	{

		dboList["TR_F_PROJ_PHASEN"] = "CREATE TRIGGER TR_F_PROJ_PHASEN FOR F_PROJ_PHASEN BEFORE INSERT OR UPDATE AS BEGIN \
IF(NEW.EPP_ERLEDIGT IS NULL) THEN NEW.EPP_ERLEDIGT=0;\
IF(NEW.EPP_STAND IS NULL) THEN NEW.EPP_STAND=0;\
IF(NEW.EPP_MEILENSTEIN IS NULL) THEN NEW.EPP_MEILENSTEIN=0;\
IF(NEW.EPP_RELATIV IS NULL) THEN NEW.EPP_RELATIV=0;\
IF(NEW.EPP_REL_DAT IS NULL) THEN NEW.EPP_REL_DAT=0;\
IF(NEW.EPP_REL_PER_TYP IS NULL) THEN NEW.EPP_REL_PER_TYP=0;\
IF(NEW.EPP_REL_DAUER IS NULL) THEN NEW.EPP_REL_DAUER=0;\
IF(NEW.EPP_BESCHREIB IS NULL) THEN NEW.EPP_BESCHREIB='';\
IF(NEW.EPP_PROJ_NR IS NULL) THEN NEW.EPP_PROJ_NR='';\
IF(NEW.EPP_VISUM IS NULL) THEN NEW.EPP_VISUM='';\
IF(NEW.EPP_CI_EV IS NULL) THEN NEW.EPP_CI_EV='';\
IF(NEW.EPP_GLOBAL_ID IS NULL) THEN NEW.EPP_GLOBAL_ID='';\
IF(NEW.EPP_CI_PD IS NULL) THEN NEW.EPP_CI_PD='';\
END;";
	}
}
