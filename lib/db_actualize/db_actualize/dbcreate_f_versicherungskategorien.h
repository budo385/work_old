#ifndef DBCREATE_F_VERSICHERUNGSKATEGORIEN_H
#define DBCREATE_F_VERSICHERUNGSKATEGORIEN_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_VERSICHERUNGSKATEGORIEN : public DbSqlTableCreation
{
public: 
	DbCreate_F_VERSICHERUNGSKATEGORIEN():DbSqlTableCreation(F_VERSICHERUNGSKATEGORIEN){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_VERSICHERUNGSKATEGORIEN_H
