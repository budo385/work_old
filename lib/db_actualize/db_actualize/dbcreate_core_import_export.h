#ifndef DBCREATE_CORE_IMPORT_EXPORT_H
#define DBCREATE_CORE_IMPORT_EXPORT_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_CORE_IMPORT_EXPORT : public DbSqlTableCreation
{
public:
	DbCreate_CORE_IMPORT_EXPORT():DbSqlTableCreation(CORE_IMPORT_EXPORT){};  //init table


private:
	//create generic Table def:
	void DefineFields(QStringList &lstSQL);
	void DefineIndexes(SQLDBObjectCreate &dboList);
};

#endif // DBCREATE_CORE_IMPORT_EXPORT_H
