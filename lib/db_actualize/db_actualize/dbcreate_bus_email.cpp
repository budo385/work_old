#include "dbcreate_bus_email.h"

//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_EMAIL::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
											create table BUS_EMAIL (						\
											BEM_ID INTEGER not null,						\
											BEM_GLOBAL_ID VARCHAR(15) null,					\
											BEM_DAT_LAST_MODIFIED TIMESTAMP not null,		\
											BEM_COMM_ENTITY_ID INTEGER not null,			\
											BEM_PROJECT_ID INTEGER null,					\
											BEM_EXT_ID LONGVARBINARY null,					\
											BEM_EXT_APP_DB_INSTANCE VARCHAR(50) null,		\
											BEM_EXT_APP VARCHAR(8) null,					\
											BEM_FROM LONGVARCHAR null,						\
											BEM_TO LONGVARCHAR null,						\
											BEM_CC LONGVARCHAR null,						\
											BEM_BCC LONGVARCHAR null,						\
											BEM_SUBJECT LONGVARCHAR null,					\
											BEM_BODY LONGVARCHAR null,						\
											BEM_RECV_TIME DATETIME null,					\
											BEM_UNREAD_FLAG SMALLINT null,					\
											BEM_EMAIL_TYPE SMALLINT not null,				\
											BEM_TEMPLATE_FLAG SMALLINT null,				\
											BEM_OUTGOING SMALLINT null,						\
											BEM_TEMPLATE_NAME VARCHAR(250) null,			\
											BEM_TEMPLATE_DESCRIPTION LONGVARCHAR null,		\
											BEM_IS_SYNC_IN_PROGRESS SMALLINT null,			\
											BEM_SIZE INTEGER null,							\
											BEM_EXT_ID_CHKSUM VARCHAR(200) null,			\
											BEM_CATEGORY VARCHAR(250) null,					\
											BEM_FIX_HTML SMALLINT null,						\
											BEM_REPLY_TO VARCHAR(250) null,					\
											BEM_TRUE_FROM VARCHAR(250) null,				\
											BEM_TO_EMAIL_ACCOUNT VARCHAR(250) null,			\
											BEM_MAILBOX_TYPE SMALLINT null,					\
											BEM_REMINDER_DUE_DATE DATETIME null,				\
											BEM_REMINDER_TYPE VARCHAR(250) null,			\
											constraint BUS_EMAIL_PK primary key (BEM_ID) )"); 
											

	lstSQL.append("\
											alter table BUS_EMAIL\
											add constraint CE_ENTITY_EMAIL_FK1 foreign key (\
											BEM_COMM_ENTITY_ID)\
											references CE_COMM_ENTITY (CENT_ID)\
											ON DELETE CASCADE");	//when comm entity is deleted, voice calls are deleted


	lstSQL.append("\
										alter table BUS_EMAIL\
										add constraint BUS_PROJ_EMAIL_FK1 foreign key (\
										BEM_PROJECT_ID)\
										references BUS_PROJECTS (BUSP_ID)\
										ON DELETE CASCADE");

	//no Db versioning:
	
	//Delete columns.
	m_lstDeletedCols << "BEM_IS_PRIVATE";

	//truncated columns <Database_ver,column_name>: database_ver where alter will occur
	m_lstTruncatedCols << "131;BEM_REPLY_TO";
	m_lstTruncatedCols << "131;BEM_TRUE_FROM";
	m_lstTruncatedCols << "131;BEM_TO_EMAIL_ACCOUNT";
	m_lstTruncatedCols << "133;BEM_REMINDER_DUE_DATE";

}



//probably common to all DB's:
void DbCreate_BUS_EMAIL::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();

	dboList["IND_BEM_COMM_ENTITY_ID"] = "create index IND_BEM_COMM_ENTITY_ID on BUS_EMAIL (BEM_COMM_ENTITY_ID  )  ";  
	dboList["IND_BEM_PROJECT_ID"] = "create index IND_BEM_PROJECT_ID on BUS_EMAIL (BEM_PROJECT_ID  )  ";  
	dboList["IND_BEM_EXT_ID_CHKSUM"] = "create index IND_BEM_EXT_ID_CHKSUM on BUS_EMAIL ( BEM_EXT_ID_CHKSUM )";
	dboList["IND_BEM_CATEGORY"] = "create index IND_BEM_CATEGORY on BUS_EMAIL ( BEM_CATEGORY )";
	dboList["IND_BEM_LAST_MODIFIED"] = "create index IND_BEM_LAST_MODIFIED on BUS_EMAIL ( BEM_DAT_LAST_MODIFIED )";
	dboList["IND_BEM_RECV_TIME"] = "create index IND_BEM_RECV_TIME on BUS_EMAIL (BEM_RECV_TIME)";

	dboList["IND_BEM_OUTGOING"] = "create index IND_BEM_OUTGOING on BUS_EMAIL (BEM_OUTGOING)";
	dboList["IND_BEM_TEMPLATE_FLAG"] = "create index IND_BEM_TEMPLATE_FLAG on BUS_EMAIL (BEM_TEMPLATE_FLAG)";
	
	dboList["IND_BEM_REPLY_TO"] = "create index IND_BEM_REPLY_TO on BUS_EMAIL (BEM_REPLY_TO)";
	dboList["IND_BEM_TRUE_FROM"] = "create index IND_BEM_TRUE_FROM on BUS_EMAIL (BEM_TRUE_FROM)";
	dboList["IND_BEM_TO_EMAIL_ACCOUNT"] = "create index IND_BEM_TO_EMAIL_ACCOUNT on BUS_EMAIL (BEM_TO_EMAIL_ACCOUNT)";
	//dboList["IND_CMB_TO_ACC_TEMP_FLAG"] = "create index IND_CMB_TO_ACC_TEMP_FLAG on BUS_EMAIL (BEM_TEMPLATE_FLAG, BEM_TO_EMAIL_ACCOUNT)";

	dboList["IND_BEM_MAILBOX_TYPE"] = "create index IND_BEM_MAILBOX_TYPE on BUS_EMAIL (BEM_MAILBOX_TYPE)";
	dboList["IND_BEM_REMINDER_DUE_DATE"] = "create index IND_BEM_REMINDER_DUE_DATE on BUS_EMAIL (BEM_REMINDER_DUE_DATE)";
	
	
	
}


void DbCreate_BUS_EMAIL::DefineTriggers(SQLDBObjectCreate &dboList)
{
	//create triggers TOFIX: foe each DB, must be different
	dboList["TRG_BUS_EMAIL_DELETE"]="\
									  CREATE TRIGGER TRG_BUS_EMAIL_DELETE FOR BUS_EMAIL\
									  ACTIVE AFTER DELETE AS \
									  BEGIN \
									  DELETE FROM CORE_ACC_USER_REC WHERE OLD.BEM_ID = CUAR_RECORD_ID AND CUAR_TABLE_ID="+QVariant(BUS_EMAIL).toString()+";\
									  DELETE FROM CORE_ACC_GROUP_REC WHERE OLD.BEM_ID = CGAR_RECORD_ID AND CGAR_TABLE_ID="+QVariant(BUS_EMAIL).toString()+";\
									  END\
									  ";

}