#include "dbcreate_bus_cost_centers.h"

//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_COST_CENTERS::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
											create table BUS_COST_CENTERS ( \
											BCTC_ID INTEGER not null, \
											BCTC_GLOBAL_ID VARCHAR(15) null, \
											BCTC_DAT_LAST_MODIFIED TIMESTAMP not null, \
											BCTC_CODE VARCHAR(50) not null, \
											BCTC_NAME VARCHAR(100) not null, \
											BCTC_ACTIVE_FLAG INTEGER not null, \
											BCTC_DESCRIPTION LONGVARCHAR null, \
											BCTC_ORGANIZATION_ID INTEGER null, \
											BCTC_LEVEL INTEGER not null, \
											BCTC_PARENT INTEGER null, \
											BCTC_HASCHILDREN INTEGER not null, \
											BCTC_ICON VARCHAR(50) not null, \
											BCTC_MAINPARENT_ID INTEGER null, \
											BCTC_STYLE INTEGER not null, constraint BUS_COST_CENTERS_PK primary key (BCTC_ID) )");


	m_lstDeletedCols<<"BCTC_SIBLING";

	//constraint for table creation:
	/* TOFIX this causes crash when editing an item in the FUI !!!!!*/
	lstSQL.append("\
											alter table BUS_COST_CENTERS\
											add constraint BUS_COST_CENTERS_PK_FK_PARENT foreign key (\
											BCTC_PARENT)\
											references BUS_COST_CENTERS (\
											BCTC_ID) ON DELETE CASCADE");

	lstSQL.append("\
								   alter table BUS_COST_CENTERS\
								   add constraint BUS_COST_ORG_K1 foreign key (\
								   BCTC_ORGANIZATION_ID)\
								   references BUS_ORGANIZATIONS (BORG_ID)\
											");	

	lstSQL.append("\
								   alter table BUS_COST_CENTERS\
								   add constraint BUS_COST_PK_FK_MAINPARENT foreign key (\
								   BCTC_MAINPARENT_ID)\
								   references BUS_COST_CENTERS (\
								   BCTC_ID)");
}


//probably common to all DB's:
void DbCreate_BUS_COST_CENTERS::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
	dboList["IND_BCTC_ORG_ID"] = "create index IND_BCTC_ORG_ID on BUS_COST_CENTERS (BCTC_ORGANIZATION_ID)";  
	dboList["IND_BCTC_CODE_ORG"] = "create unique index IND_BCTC_CODE_ORG on BUS_COST_CENTERS (BCTC_CODE, BCTC_ORGANIZATION_ID) ";  
	dboList["IND_BCTC_PARENT"] = "create index IND_BCTC_PARENT on BUS_COST_CENTERS (BCTC_PARENT) ";  
	dboList["IND_BCTC_HIER"] = "create index IND_BCTC_HIER on BUS_COST_CENTERS (BCTC_LEVEL,BCTC_PARENT) ";  

}

