#include "dbcreate_bus_stored_project_list_data.h"



//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_STORED_PROJECT_LIST_DATA::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
											create table BUS_STORED_PROJECT_LIST_DATA ( \
											BSPLD_ID INTEGER not null,\
											BSPLD_GLOBAL_ID VARCHAR(15) null,\
											BSPLD_DAT_LAST_MODIFIED TIMESTAMP not null,\
											BSPLD_TREE_ID INTEGER not null,\
											BSPLD_PROJECT_ID INTEGER not null,\
											BSPLD_EXPANDED SMALLINT null,\
											constraint BUS_SPLD_PK primary key (BSPLD_ID) )"); 

	//constraint for table creation:
	lstSQL.append("\
											alter table BUS_STORED_PROJECT_LIST_DATA\
											add constraint BUS_SPL_BUS_SPLD_FK1 foreign key (\
											BSPLD_TREE_ID)\
											references BUS_STORED_PROJECT_LIST (\
											BSPL_ID) ON DELETE CASCADE");

	lstSQL.append("\
											alter table BUS_STORED_PROJECT_LIST_DATA\
											add constraint BUS_PROJ_BUS_SPLD_FK1 foreign key (\
											BSPLD_PROJECT_ID)\
											references BUS_PROJECTS (\
											BUSP_ID) ON DELETE CASCADE");
}



//probably common to all DB's:
void DbCreate_BUS_STORED_PROJECT_LIST_DATA::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
	//dboList["IND_BCMJ_CONTACT_ID"] = "create index IND_BCMJ_CONTACT_ID on BUS_CM_PICTURE (BCMPC_CONTACT_ID)  ";  

}


