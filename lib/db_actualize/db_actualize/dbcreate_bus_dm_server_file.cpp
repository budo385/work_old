#include "dbcreate_bus_dm_server_file.h"


//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_DM_SERVER_FILE::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
								   create table BUS_DM_SERVER_FILE (\
								   BDSF_ID INTEGER not null,\
								   BDSF_GLOBAL_ID VARCHAR(15) null,\
								   BDSF_DAT_LAST_MODIFIED TIMESTAMP not null,\
								   BDSF_DISK_REL_PATH VARCHAR(500) not null,\
								   BDSF_DISK_FILENAME VARCHAR(500) not null,\
								   BDSF_FILENAME VARCHAR(500) not null,\
								   BDSF_EXTENSION VARCHAR(50) not null,\
								   BDSF_CREATED DATETIME null,\
								   BDSF_MODIFIED DATETIME null,\
								   BDSF_SIZE INTEGER null,\
								   BDSF_THUMB_PIC_32 LONGVARBINARY null,\
								   BDSF_THUMB_PIC_64 LONGVARBINARY null,\
								   BDSF_THUMB_PIC_128 LONGVARBINARY null,\
								   BDSF_THUMB_PIC_256 LONGVARBINARY null,\
								   BDSF_TAGS VARCHAR(500) null, constraint BUS_DM_SERVER_FILE_PK primary key (BDSF_ID) )    ");  

}



//probably common to all DB's:
void DbCreate_BUS_DM_SERVER_FILE::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
	//dboList["IND_BDSF_FILENAME_2"] = "create index IND_BDSF_FILENAME_2 on BUS_DM_SERVER_FILE (BDSF_FILENAME)";  
	//dboList["IND_BDSF_EXTENSION"] = "create index IND_BDSF_EXTENSION on BUS_DM_SERVER_FILE (BDSF_EXTENSION)";  
	//dboList["IND_BDSF_TAGS"] = "create index IND_BDSF_TAGS on BUS_DM_SERVER_FILE (BDSF_TAGS)";  
}



