#include "dbcreate_core_test_entity_phones.h"



//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------

void DbCreate_TEST_ENTITY_PHONES::DefineFields(QStringList &lstSQL)
{

	//Table shema statements for ODBC.
	m_TableDDL[DBTYPE_ODBC]["First"].append("\
	create table TEST_ENTITY_PHONES ( \
	TEP_ID INTEGER not null,\
	TEP_GLOBAL_ID VARCHAR(15) null,\
	TEP_DAT_LAST_MODIFIED TIMESTAMP not null,\
	TEP_CONTACT_ID INTEGER not null,\
	TEP_PHONE VARCHAR(50) not null,\
	TEP_PHONE_TYPE INTEGER not null, constraint TEST_ENTITY_PHONES_PK primary key (TEP_ID) )");


	//constraint for table creation:
	m_TableDDL[DBTYPE_ODBC]["First"].append("\
	alter table TEST_ENTITY_PHONES\
	add constraint TEST_ENTITY_PHONES_FK1 foreign key (\
	TEP_CONTACT_ID)\
	references TEST_ENTITY_CONTACT (\
	TEC_ID) ON DELETE CASCADE" );

	//no Db versioning:
	m_TableDDL[DBTYPE_ODBC]["Last"]=m_TableDDL[DBTYPE_ODBC]["First"];

	
	//create indexes
	SQLDBObjectCreate dboList;
	DefineIndexes(dboList);
	m_Indexes[DBTYPE_ODBC]=dboList;
	dboList.clear();
	
	//create triggers
	m_Triggers[DBTYPE_ODBC]=dboList;


	//--------------------------------------------------
	// ODBC - UPGRADE DEFS
	//---------------------------------------------------
	
	//Now for each Db_version upgrade, set alternations or init to empty list:
	//Init to empty other lists:
	SQLDBObjectUpdate dboUpgrade;

	m_Fields_Upgrade[DBTYPE_ODBC]=dboUpgrade;
}




/* Probably index defintion is same for all DB's */
void DbCreate_TEST_ENTITY_PHONES::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index defintion by its name (important)
	dboList.clear();
	//dboList["IND_TEP_CONTACT_ID"]		="create index IND_TEP_CONTACT_ID on TEST_ENTITY_PHONES (TEP_CONTACT_ID) ";  
}





