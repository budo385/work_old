#include "dbcreate_bus_cm_journal.h"





//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_CM_JOURNAL::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
											create table BUS_CM_JOURNAL ( \
											BCMJ_ID INTEGER not null,\
											BCMJ_GLOBAL_ID VARCHAR(15) null,\
											BCMJ_DAT_LAST_MODIFIED TIMESTAMP not null,\
											BCMJ_CONTACT_ID INTEGER not null,\
											BCMJ_PERSON_ID INTEGER null,\
											BCMJ_DATE DATE null,\
											BCMJ_TEXT LONGVARCHAR null, \
											BCMJ_IS_PRIVATE INTEGER null,\
											constraint BUS_CM_JOURNAL_PK primary key (BCMJ_ID) )  ");
	//constraint for table creation:
	lstSQL.append("\
											alter table BUS_CM_JOURNAL\
											add constraint BUS_CONTACTS_JOURNAL_FK1 foreign key (\
											BCMJ_CONTACT_ID)\
											references BUS_CM_CONTACT (\
											BCNT_ID)  \
											ON DELETE CASCADE");

	//constraint for table creation:
	lstSQL.append("\
								   alter table BUS_CM_JOURNAL\
								   add constraint PERSON_JOURNAL_FK1 foreign key (\
								   BCMJ_PERSON_ID)\
								   references BUS_PERSON (\
								   BPER_ID)  \
								   ON DELETE SET NULL");
}



//probably common to all DB's:
void DbCreate_BUS_CM_JOURNAL::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
	dboList["IND_BCMJ_CONTACT_ID"] = "create index IND_BCMJ_CONTACT_ID on BUS_CM_JOURNAL (BCMJ_CONTACT_ID)  ";  
}


