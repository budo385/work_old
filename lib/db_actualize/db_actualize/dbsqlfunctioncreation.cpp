#include "dbsqlfunctioncreation.h"
#include "db_core/db_core/db_drivers.h"


/*!
	Create table (from scratch) based on last version.

	\param pManager		- pointer to the Db Manager
	\param pDb			- pointer to the valid DbConnection 
	\param pDboManager	- pointer to the Db object manager

*/
void DbSqlFunctionCreation::InitDbSettings(DbSqlManager *pManager, QSqlDatabase *pDb,DbObjectManager *pDboManager )
{
	//set & test DB pointers
	m_pManager = pManager;
	m_pDb = pDb;
	m_DbObjectManager = pDboManager;
	m_DbType=pManager->GetDbType();

	//asserter
	Q_ASSERT_X(m_pDb!=NULL,"Init Create Table", "NULL pointers detected");
	Q_ASSERT_X(m_pManager!=NULL,"Init Create Table", "NULL pointers detected");


	//call init method:
	Init_Oracle();
	Init_MySQL();
	Init_FireBird();
}


/*!
	Creates functions, drop if exists

	\param pStatus &pStatus
*/
void DbSqlFunctionCreation::CreateFunctions(Status &pStatus, bool bCommunicatorFunct)
{
	QString strSQL,strName;
	pStatus.setError(0);

	if (bCommunicatorFunct)
	{
		//check if DB is supported:
		if (!m_Functions.contains(m_DbType))
			{pStatus.setError(StatusCodeSet::ERR_SQL_UNKNOWN_DBTYPE);return;}

		DbSqlQuery query(pStatus,m_pManager);
		SQLDBObjectCreateIterator i(m_Functions.value(m_DbType));
		while (i.hasNext()) 
		{
			i.next();
			strSQL=i.value();
			strName=i.key();
			qDebug()<<"Creating function: "<<strName;
			m_DbObjectManager->DropIfExists_Funct(pStatus,m_pDb,strName);
			if(!pStatus.IsOK())
				return;
			if (!query.Execute(pStatus,strSQL))	
				return;
		}
	}
	else
	{
		//check if DB is supported:
		if (!m_FunctionsSPC.contains(m_DbType))
		{pStatus.setError(StatusCodeSet::ERR_SQL_UNKNOWN_DBTYPE);return;}

		DbSqlQuery query(pStatus,m_pManager);
		SQLDBObjectCreateIterator i(m_FunctionsSPC.value(m_DbType));
		while (i.hasNext()) 
		{
			i.next();
			strSQL=i.value();
			strName=i.key();
			qDebug()<<"Creating function: "<<strName;
			m_DbObjectManager->DropIfExists_Funct(pStatus,m_pDb,strName);
			if(!pStatus.IsOK())
				return;
			if (!query.Execute(pStatus,strSQL))	
				return;
		}
	}
}


void DbSqlFunctionCreation::DropAllFunctions(Status &pStatus, bool bCommunicatorFunct)
{
	QString strSQL,strName;
	pStatus.setError(0);

	if (bCommunicatorFunct)
	{
		//check if DB is supported:
		if (!m_Functions.contains(m_DbType))
			{pStatus.setError(StatusCodeSet::ERR_SQL_UNKNOWN_DBTYPE);return;}

		DbSqlQuery query(pStatus,m_pManager);
		SQLDBObjectCreateIterator i(m_Functions.value(m_DbType));
		while (i.hasNext()) 
		{
			i.next();
			strName=i.key();
			qDebug()<<"Drop function: "<<strName;
			m_DbObjectManager->DropIfExists_Funct(pStatus,m_pDb,strName);
			if(!pStatus.IsOK())
				return;
		}
	}
	else
	{
		//check if DB is supported:
		if (!m_FunctionsSPC.contains(m_DbType))
			{pStatus.setError(StatusCodeSet::ERR_SQL_UNKNOWN_DBTYPE);return;}

		DbSqlQuery query(pStatus,m_pManager);
		SQLDBObjectCreateIterator i(m_FunctionsSPC.value(m_DbType));
		while (i.hasNext()) 
		{
			i.next();
			strName=i.key();
			qDebug()<<"Drop function: "<<strName;
			m_DbObjectManager->DropIfExists_Funct(pStatus,m_pDb,strName);
			if(!pStatus.IsOK())
				return;
		}
	}
}

QStringList	DbSqlFunctionCreation::GetFunctions()
{
	QStringList lstNames;
	{
		SQLDBObjectCreateIterator i(m_FunctionsSPC.value(m_DbType));
		while (i.hasNext()) 
		{
			i.next();
			lstNames.append(i.key());
		}
	}
	return lstNames;
}

//---------------------------------------------------
//					ORACLE
//---------------------------------------------------

void DbSqlFunctionCreation::Init_Oracle()
{

	SQLDBObjectCreate dboList;
/*
	dboList["Funct_1"]="\
		create function Funct_1(\
		xxxyyy,\
		bzzzzzzzz)";  

	dboList["Funct_2"]="\
		create function Funct_2 on CORE_LOCKING (\
		CORL_TABLE_ID,\
		CORL_RECORD_ID)";  

*/
	m_Functions[DBTYPE_ORACLE]=dboList;

}



//---------------------------------------------------
//					MYSQL
//---------------------------------------------------

void DbSqlFunctionCreation::Init_MySQL()
{

	
	SQLDBObjectCreate dboList;

	/*
	dboList["FUNCT_DATE_INTERVAL_ADD"]="\
	CREATE FUNCTION FUNCT_DATE_INTERVAL_ADD(pDate DATETIME,pInterval INTEGER, pDatePart VARCHAR(30)) RETURNS datetime\
	BEGIN\
	DECLARE dateNew DATETIME;\
	SELECT CASE pDatePart\
		WHEN 'second' THEN ADDDATE(pDate,INTERVAL pInterval  SECOND)\
		WHEN 'hour' THEN ADDDATE(pDate,INTERVAL pInterval  HOUR)\
		WHEN 'day' THEN ADDDATE(pDate,INTERVAL pInterval  DAY)\
		END INTO dateNew;\
	RETURN dateNew;\
	END";
	*/

	m_Functions[DBTYPE_MYSQL]=dboList;
	

}



//---------------------------------------------------
//					FIREBIRD
//---------------------------------------------------

void DbSqlFunctionCreation::Init_FireBird()
{
	SQLDBObjectCreate dboList;
	m_Functions[DBTYPE_FIREBIRD]=dboList;


	dboList.clear();
	dboList["DATETOSTR"]="DECLARE EXTERNAL FUNCTION DATETOSTR\
						  TIMESTAMP, CSTRING(255)\
						  RETURNS CSTRING(255) FREE_IT\
						  ENTRY_POINT 'fn_datetostr'  MODULE_NAME 'rfunc';";

	dboList["N_DATE"]="DECLARE EXTERNAL FUNCTION N_DATE\
						  DATE\
						  RETURNS DATE FREE_IT\
						  ENTRY_POINT 'fn_nvl_date' MODULE_NAME 'rfunc';";

	dboList["N_STR"]="DECLARE EXTERNAL FUNCTION N_STR\
				      CSTRING(255)\
					  RETURNS CSTRING(255) FREE_IT\
					  ENTRY_POINT 'fn_nvl_string' MODULE_NAME 'rfunc';";

	dboList["N_INT"]="DECLARE EXTERNAL FUNCTION N_INT\
					 CSTRING(1020)\
					 RETURNS CSTRING(1020) FREE_IT\
					 ENTRY_POINT 'fn_nvl_integer' MODULE_NAME 'rfunc';";
	
	dboList["F_ENCODE_BASE64"]="DECLARE EXTERNAL FUNCTION F_ENCODE_BASE64\
					 CSTRING(255)\
					 RETURNS CSTRING(255) FREE_IT\
					 ENTRY_POINT 'fn_str_encodeToBase64' MODULE_NAME 'rfunc';";

	dboList["F_DECODE_BASE64"]="DECLARE EXTERNAL FUNCTION F_DECODE_BASE64\
   				     CSTRING(255)\
					 RETURNS CSTRING(255) FREE_IT\
					 ENTRY_POINT 'fn_str_decodeFromBase64' MODULE_NAME 'rfunc';";

	m_FunctionsSPC[DBTYPE_FIREBIRD]=dboList;
}