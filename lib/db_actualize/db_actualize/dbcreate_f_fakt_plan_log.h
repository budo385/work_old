#ifndef DBCREATE_F_FAKT_PLAN_LOG_H
#define DBCREATE_F_FAKT_PLAN_LOG_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_FAKT_PLAN_LOG : public DbSqlTableCreation
{
public: 
	DbCreate_F_FAKT_PLAN_LOG():DbSqlTableCreation(F_FAKT_PLAN_LOG){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_FAKT_PLAN_LOG_H
