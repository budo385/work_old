#include "dbcreate_bus_custom_text.h"



//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_CUSTOM_TEXT::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
											create table BUS_CUSTOM_TEXT ( \
											BCT_ID INTEGER not null,\
											BCT_GLOBAL_ID VARCHAR(15) null,\
											BCT_DAT_LAST_MODIFIED TIMESTAMP not null,\
											BCT_CUSTOM_FIELD_ID INTEGER not null,\
											BCT_VALUE VARCHAR(200) not null,\
											BCT_TABLE_ID INTEGER not null,\
											BCT_RECORD_ID INTEGER not null, constraint BUS_CUSTOM_TEXT_PK primary key (BCT_ID) ) "); 
											
							lstSQL.append("\
											alter table BUS_CUSTOM_TEXT\
											add constraint BUS_CUSTOM_TEXT_FK1 foreign key (\
											BCT_CUSTOM_FIELD_ID)\
											references BUS_CUSTOM_FIELDS (\
											BCF_ID)  \
											ON DELETE CASCADE");

}

//probably common to all DB's:
void DbCreate_BUS_CUSTOM_TEXT::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
	dboList["IND_BCT_CI_TABLE_REC"] = "create index IND_BCT_CI_TABLE_REC on BUS_CUSTOM_TEXT (BCT_TABLE_ID,BCT_RECORD_ID)"; 
	dboList["IND_BCT_TABLE_VAL"] = "create index IND_BCT_TABLE_VAL on BUS_CUSTOM_TEXT (BCT_VALUE)"; 
	dboList["IND_BCT_CI_UNIQUE"] = "create unique index IND_BCT_CI_UNIQUE on BUS_CUSTOM_TEXT (BCT_TABLE_ID,BCT_RECORD_ID,BCT_CUSTOM_FIELD_ID)"; 
}


