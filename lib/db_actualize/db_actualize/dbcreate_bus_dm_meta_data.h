#ifndef DBCREATE_BUS_DM_META_DATA_H
#define DBCREATE_BUS_DM_META_DATA_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_BUS_DM_META_DATA : public DbSqlTableCreation
{
public:
	DbCreate_BUS_DM_META_DATA():DbSqlTableCreation(BUS_DM_META_DATA){};  //init table
	
private:
	//create generic Table def:
	void DefineFields(QStringList &lstSQL);

	//probably common to all DB's:
	void DefineIndexes(SQLDBObjectCreate &dboList);
};


#endif // DBCREATE_BUS_DM_META_DATA_H
