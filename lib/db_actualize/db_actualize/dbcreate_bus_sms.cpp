#include "dbcreate_bus_sms.h"

//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_BUS_SMS::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
									create table BUS_SMS (\
									BSMS_ID INTEGER not null,\
									BSMS_GLOBAL_ID VARCHAR(15) null,\
									BSMS_DAT_LAST_MODIFIED TIMESTAMP not null,\
									BSMS_COMM_ENTITY_ID INTEGER not null,\
									BSMS_PROJECT_ID INTEGER null,\
									BSMS_CONTACT_ID INTEGER null,\
									BSMS_TEXT LONGVARCHAR null,\
									BSMS_PHONE_NUMBER VARCHAR(20) null,\
									BSMS_DATE_SENT DATETIME null,\
									BSMS_TEMPLATE_FLAG SMALLINT null,\
									BSMS_TEMPLATE_NAME VARCHAR(250) null,\
									BSMS_TEMPLATE_DESCRIPTION LONGVARCHAR null,\
									BSMS_CATEGORY VARCHAR(250) null, constraint BUS_SMS_PK primary key (BSMS_ID) )");

	lstSQL.append("\
										alter table BUS_SMS\
										add constraint BUS_SMS_COMM_SMS_FK1 foreign key (\
											BSMS_COMM_ENTITY_ID)\
										 references CE_COMM_ENTITY (\
											CENT_ID)");

	lstSQL.append("\
								   alter table BUS_SMS\
								   add constraint BUS_SMS_CONT_FK1 foreign key (\
								   BSMS_CONTACT_ID)\
								   references BUS_CM_CONTACT (\
								   BCNT_ID)\
									");
	
	lstSQL.append("\
								   alter table BUS_SMS\
								   add constraint BUS_SMS_PERS_FK1 foreign key (\
								   BSMS_PROJECT_ID)\
								   references BUS_PROJECTS (\
								   BUSP_ID)\
								   ");


}

//probably common to all DB's:
void DbCreate_BUS_SMS::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();
	dboList["IND_BSMS_LAST_MODIFIED"] = "create index IND_BSMS_LAST_MODIFIED on BUS_SMS ( BSMS_DAT_LAST_MODIFIED )";
}

void DbCreate_BUS_SMS::DefineTriggers(SQLDBObjectCreate &dboList)
{
	//create triggers TOFIX: foe each DB, must be different
	dboList["TRG_BUS_SMS_DELETE"]="\
									 CREATE TRIGGER TRG_BUS_SMS_DELETE FOR BUS_SMS\
									 ACTIVE AFTER DELETE AS \
									 BEGIN \
									 DELETE FROM CORE_ACC_USER_REC WHERE OLD.BSMS_ID = CUAR_RECORD_ID AND CUAR_TABLE_ID="+QVariant(BUS_SMS).toString()+";\
									 DELETE FROM CORE_ACC_GROUP_REC WHERE OLD.BSMS_ID = CGAR_RECORD_ID AND CGAR_TABLE_ID="+QVariant(BUS_SMS).toString()+";\
									 END\
									 ";
}

