#ifndef DBCREATE_FL_DEFAULT_H
#define DBCREATE_FL_DEFAULT_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_FL_DEFAULT : public DbSqlTableCreation
{
public: 
	DbCreate_FL_DEFAULT():DbSqlTableCreation(FL_DEFAULT){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // FL_DEFAULT_H
