#ifndef DBCREATE_F_PROJECTUSERDEFINED_H
#define DBCREATE_F_PROJECTUSERDEFINED_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_PROJECTUSERDEFINED : public DbSqlTableCreation
{
public: 
	DbCreate_F_PROJECTUSERDEFINED():DbSqlTableCreation(F_PROJECTUSERDEFINED){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_PROJECTUSERDEFINED_H
