#ifndef DBCREATE_F_INOUTTIMES_H
#define DBCREATE_F_INOUTTIMES_H

#include "dbsqltablecreation.h"
#include "db_core/db_core/dbtableiddefinition.h"


class DbCreate_F_INOUTTIMES : public DbSqlTableCreation
{
public: 
	DbCreate_F_INOUTTIMES():DbSqlTableCreation(F_INOUTTIMES){};


private:
	void DefineFields(QStringList &lstSQL);
	void DefineTriggers(SQLDBObjectCreate &dboList);
	void DefineIndexes(SQLDBObjectCreate &dboList);
	void DefineViews(SQLDBObjectCreate &dboList);
};

#endif // F_INOUTTIMES_H
