#include "dbcreate_ce_type.h"

//--------------------------------------------------
// ODBC - CREATE
//---------------------------------------------------
void DbCreate_CE_TYPE::DefineFields(QStringList &lstSQL)
{
	//Table schema statements for ODBC.
	lstSQL.append("\
											create table CE_TYPE ( \
											CET_ID INTEGER not null,\
											CET_GLOBAL_ID VARCHAR(15) null,\
											CET_DAT_LAST_MODIFIED TIMESTAMP null,\
											CET_TYPE_NAME VARCHAR(100) not null,\
											CET_COMM_ENTITY_TYPE_ID INTEGER not null,\
											CET_CODE VARCHAR(50) null,\
											CET_NAME VARCHAR(100) null,\
											CET_LEVEL INTEGER null,\
											CET_PARENT INTEGER null,\
											CET_HASCHILDREN INTEGER null,\
											CET_MAINPARENT_ID INTEGER null,\
											CET_ICON VARCHAR(50) null,\
											CET_STYLE INTEGER null,\
											CET_DESCRIPTION LONGVARCHAR null,\
											CET_MAPPED_NAME VARCHAR(100) null,\
											CET_ICON_BINARY LONGVARBINARY null,\
											constraint CE_TYPE_PK primary key (CET_ID) )");


	m_lstDeletedCols<<"CET_SIBLING";

	lstSQL.append("\
								   alter table CE_TYPE\
								   add constraint CE_TYPE_PARENT_FK1 foreign key (\
								   CET_PARENT)\
								   references CE_TYPE (\
								   CET_ID) \
								   ON DELETE CASCADE");

	/*
	lstSQL.append("\
								   alter table CE_TYPE\
								   add constraint CE_TYPE_FK_MAINPARENT foreign key (\
								   CET_MAINPARENT_ID)\
								   references CE_TYPE (\
								   CET_ID)");
*/

	
}



//probably common to all DB's:
void DbCreate_CE_TYPE::DefineIndexes(SQLDBObjectCreate &dboList)
{
	//add index definition by its name (important)
	dboList.clear();

	dboList["IND_ID_COMM_ENTITY_ID"] = "create unique index IND_ID_COMM_ENTITY_ID on CE_TYPE (CET_COMM_ENTITY_TYPE_ID,CET_CODE)";  
	dboList["IND_CET_LEVEL"] = "create index IND_CET_LEVEL on CE_TYPE (CET_LEVEL)";  
	dboList["IND_CET_PARENT"] = "create index IND_CET_PARENT on CE_TYPE (CET_PARENT)";  
	dboList["IND_UNQ_CET_HIER"] = "create index IND_UNQ_CET_HIER on CE_TYPE (CET_LEVEL,CET_PARENT)";  

}


