TEMPLATE	= lib
CONFIG		= qt warn_on release staticlib
HEADERS		= db_actualize/db_versions.h \
		  db_actualize/dbcreate_bus_person.h \
		  db_actualize/dbcreate_bus_personrole.h \
		  db_actualize/dbcreate_bus_storedlist.h \
		  db_actualize/dbcreate_bus_storedlist_items.h \
		  db_actualize/dbcreate_core_accessrights.h \
		  db_actualize/dbcreate_core_accrset.h \
		  db_actualize/dbcreate_core_accrsetrole.h \
		  db_actualize/dbcreate_core_app_srv_session.h \
		  db_actualize/dbcreate_core_event_log.h \
		  db_actualize/dbcreate_core_locking.h \
		  db_actualize/dbcreate_core_role.h \
		  db_actualize/dbcreate_core_test_entity_contact.h \
		  db_actualize/dbcreate_core_test_entity_info.h \
		  db_actualize/dbcreate_core_test_entity_phones.h \
		  db_actualize/dbcreate_core_user.h \
		  db_actualize/dbcreate_core_user_session.h \
		  db_actualize/dbcreate_core_userrole.h \
		  db_actualize/dbcreate_test_tree.h \
		  db_actualize/dbomanager.h \
		  db_actualize/dbomanager_mysql.h \
		  db_actualize/dbomanager_oracle.h \
		  db_actualize/dborganizer.h \
		  db_actualize/dbsqlfunctioncreation.h \
		  db_actualize/dbsqlprocedurecreation.h \
		  db_actualize/dbsqltablecreation.h \
		  db_actualize/dbsqlviewcreation.h \
		  db_actualize/table_collection.h
SOURCES		= db_actualize/dbcreate_bus_person.cpp \
		  db_actualize/dbcreate_bus_personrole.cpp \
		  db_actualize/dbcreate_bus_storedlist.cpp \
		  db_actualize/dbcreate_bus_storedlist_items.cpp \
		  db_actualize/dbcreate_core_accessrights.cpp \
		  db_actualize/dbcreate_core_accrset.cpp \
		  db_actualize/dbcreate_core_accrsetrole.cpp \
		  db_actualize/dbcreate_core_app_srv_session.cpp \
		  db_actualize/dbcreate_core_event_log.cpp \
		  db_actualize/dbcreate_core_locking.cpp \
		  db_actualize/dbcreate_core_role.cpp \
		  db_actualize/dbcreate_core_test_entity_contact.cpp \
		  db_actualize/dbcreate_core_test_entity_info.cpp \
		  db_actualize/dbcreate_core_test_entity_phones.cpp \
		  db_actualize/dbcreate_core_user.cpp \
		  db_actualize/dbcreate_core_user_session.cpp \
		  db_actualize/dbcreate_core_userrole.cpp \
		  db_actualize/dbcreate_test_tree.cpp \
		  db_actualize/dbomanager.cpp \
		  db_actualize/dbomanager_mysql.cpp \
		  db_actualize/dbomanager_oracle.cpp \
		  db_actualize/dborganizer.cpp \
		  db_actualize/dbsqlfunctioncreation.cpp \
		  db_actualize/dbsqlprocedurecreation.cpp \
		  db_actualize/dbsqltablecreation.cpp \
		  db_actualize/dbsqlviewcreation.cpp
INTERFACES	= 
TARGET		= db_actualize
INCLUDEPATH	+= ../	/usr/lib/qt4/include/QtSql
