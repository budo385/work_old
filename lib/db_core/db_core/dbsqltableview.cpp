#include "dbsqltableview.h"
#include "dbsqltabledefinition.h"
#include "common/common/entity_id_collection.h"


bool DbSqlTableView::bInitialized=false;
QList<DbView> DbSqlTableView::s_lstDbViews;
bool DbSqlTableView::s_bAllLongVarCharFieldsInitialized=false;
QStringList DbSqlTableView::s_lstAllLongVarCharFields;


//in this function insert own views. NOTE: Every table must have at least one FULL view (all cols)
void DbSqlTableView::Initialize()
{
	if (bInitialized)	return;
	bInitialized=true;


	//-----------------------
	// TVIEW_CORE_LOCKING
	//-----------------------

	//add new sample view
	DbView view;
	view.m_nViewID		= TVIEW_CORE_LOCKING;
	
	//This is for SQL generation:
	//By default based on recordset view (below), select, update & insert statements will be generated
	//How many cols from begin to skip when insert/update sql: (-1 first, leave empty if u want 1:1, or see above for complex mapping)
	view.m_nSkipFirstColsInsert = 3;
	view.m_nSkipFirstColsUpdate = 3;
	view.m_strTables="CORE_LOCKING"; //if more then one table separate by comma "TABLE,TABLE2"
	
	//Record set defintion
	view.append(DbColumnEx("CORL_ID", QVariant::Int));
	view.append(DbColumnEx("CORL_GLOBAL_ID" ,QVariant::String));
	view.append(DbColumnEx("CORL_DAT_LAST_MODIFIED" ,QVariant::DateTime));
	view.append(DbColumnEx("CORL_RECORD_ID",  QVariant::Int));
	view.append(DbColumnEx("CORL_RESOURCE_ID",  QVariant::String));
	view.append(DbColumnEx("CORL_SESSION_ID",  QVariant::String));
	view.append(DbColumnEx("CORL_TABLE_ID",  QVariant::Int));
	view.append(DbColumnEx("CORL_USER_SESSION_ID",  QVariant::Int));


	s_lstDbViews.append(view);
	view.clear();


	//-----------------------
	// TVIEW_CORE_APP_SERVER
	//-----------------------
	view.m_nViewID		= TVIEW_CORE_APP_SRV_SESSION;
	view.m_nSkipFirstColsInsert = 3; //skip Id & global
	view.m_nSkipFirstColsUpdate = 3;
	view.m_strTables="CORE_APP_SRV_SESSION"; //if more then one table separate by comma "TABLE,TABLE2"
	view.append(DbColumnEx( "COAS_ID",  QVariant::Int));
	view.append(DbColumnEx( "COAS_GLOBAL_ID",  QVariant::String));
	view.append(DbColumnEx( "COAS_DAT_LAST_MODIFIED" ,QVariant::DateTime));
	view.append(DbColumnEx( "COAS_APP_SRV_SERIAL_ID", QVariant::String));
	view.append(DbColumnEx( "COAS_APP_SRV_NAME", QVariant::String));
	view.append(DbColumnEx( "COAS_UNIQUE_LICENSE_ID", QVariant::Int));
	view.append(DbColumnEx( "COAS_IP_ADDRESS",  QVariant::String));
	view.append(DbColumnEx( "COAS_DAT_STARTED",  QVariant::DateTime));
	view.append(DbColumnEx( "COAS_DAT_LAST_ACTIVITY",  QVariant::DateTime));
	view.append(DbColumnEx( "COAS_ROLE_ID",  QVariant::Int));
	view.append(DbColumnEx( "COAS_CURRENT_CONNECTIONS",  QVariant::Int));
	view.append(DbColumnEx( "COAS_MAX_CONNECTIONS", QVariant::Int));
	view.append(DbColumnEx( "COAS_IS_LBO", QVariant::Int));
	view.append(DbColumnEx( "COAS_IS_MASTER", QVariant::Int));
	

	s_lstDbViews.append(view);
	view.clear();


	//-----------------------
	// TVIEW_CORE_APP_SRV_SESSION_STATUS
	//-----------------------
	view.m_nViewID		= TVIEW_CORE_APP_SRV_SESSION_STATUS;
	view.m_strTables="CORE_APP_SRV_SESSION"; 
	view.m_strSelectSQL="SELECT COAS_ID,COAS_IP_ADDRESS,COAS_CURRENT_CONNECTIONS,COAS_MAX_CONNECTIONS FROM CORE_APP_SRV_SESSION";
	view.append(DbColumnEx( "COAS_ID",  QVariant::Int));
	view.append(DbColumnEx( "COAS_IP_ADDRESS",  QVariant::String));
	view.append(DbColumnEx( "COAS_CURRENT_CONNECTIONS",  QVariant::Int));
	view.append(DbColumnEx( "COAS_MAX_CONNECTIONS", QVariant::Int));
	view.append(DbColumnEx( "LOAD_FACTOR", QVariant::Double));

	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// TVIEW_CORE_USER_SESSION
	//-----------------------
	view.m_nViewID		= TVIEW_CORE_USER_SESSION;
	view.m_nSkipFirstColsInsert = 3;	//skip Id & global
	view.m_nSkipFirstColsUpdate = 3;
	view.m_strTables="CORE_USER_SESSION"; //if more then one table separate by comma "TABLE,TABLE2"
	view.append(DbColumnEx("COUS_ID",  QVariant::Int));
	view.append(DbColumnEx("COUS_GLOBAL_ID",  QVariant::String));
	view.append(DbColumnEx("COUS_DAT_LAST_MODIFIED" ,QVariant::DateTime));
	view.append(DbColumnEx("COUS_USER_SESSION_ID",  QVariant::String));
	view.append(DbColumnEx("COUS_APP_SRV_SESSION_ID",  QVariant::Int));
	view.append(DbColumnEx("COUS_MODULE_CODE",  QVariant::String));
	view.append(DbColumnEx("COUS_MLID",  QVariant::String));
	view.append(DbColumnEx("COUS_CORE_USER_ID",  QVariant::Int));
	view.append(DbColumnEx("COUS_ACTIVE_FLAG",  QVariant::Int));
	view.append(DbColumnEx("COUS_PARENT_ID",  QVariant::Int));
	
	


	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// TVIEW_CORE_EVENT_LOG
	//-----------------------
	view.m_nViewID		= TVIEW_CORE_EVENT_LOG;
	view.m_nSkipFirstColsInsert = 3;	//skip Id & global
	view.m_nSkipFirstColsUpdate = 3;
	view.m_strTables="CORE_EVENT_LOG"; //if more then one table separate by comma "TABLE,TABLE2"
	view.append(DbColumnEx("CREL_ID",				QVariant::Int));
	view.append(DbColumnEx("CREL_GLOBAL_ID",		QVariant::String));
	view.append(DbColumnEx("CREL_DAT_LAST_MODIFIED",QVariant::DateTime));
	view.append(DbColumnEx("CREL_EVENT_CODE",		QVariant::Int));
	view.append(DbColumnEx("CREL_EVENT_TEXT",		QVariant::String,true));
	view.append(DbColumnEx("CREL_EVENT_TYPE",		QVariant::Int));
	view.append(DbColumnEx("CREL_EVENT_CATEGORY_ID",QVariant::Int));
	view.append(DbColumnEx("CREL_EVENT_DATETIME",	QVariant::DateTime));

	s_lstDbViews.append(view);
	view.clear();


	//-----------------------
	// TVIEW_CORE_IP_ACCESS_LIST
	//-----------------------
	view.m_nViewID		= TVIEW_CORE_IPACCESSLIST;
	view.m_nSkipFirstColsInsert = 3;
	view.m_nSkipFirstColsUpdate = 3;
	view.m_strTables="CORE_IPACCESSLIST";

	//Record set definition
	view.append(DbColumnEx("CIPR_ID", QVariant::Int));
	view.append(DbColumnEx("CIPR_GLOBAL_ID", QVariant::String));
	view.append(DbColumnEx("CIPR_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("CIPR_LIST_NAME", QVariant::String));
	view.append(DbColumnEx("CIPR_IP_ADDRESS", QVariant::UInt));
	view.append(DbColumnEx("CIPR_FILTER_BITS", QVariant::Int));
	view.append(DbColumnEx("CIPR_ORDER", QVariant::Int));
	view.append(DbColumnEx("CIPR_ALLOW", QVariant::Int));


	s_lstDbViews.append(view);
	view.clear();


	//-----------------------
	// TVIEW_CORE_DATABASE_INFO
	//-----------------------
	view.m_nViewID		= TVIEW_CORE_DATABASE_INFO;
	view.m_nSkipFirstColsInsert = 3;
	view.m_nSkipFirstColsUpdate = 3;
	view.m_strTables="CORE_DATABASE_INFO";

	//Record set definition
	view.append(DbColumnEx("CDI_ID", QVariant::Int));
	view.append(DbColumnEx("CDI_GLOBAL_ID", QVariant::String));
	view.append(DbColumnEx("CDI_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("CDI_DATABASE_VERSION", QVariant::Int));
	view.append(DbColumnEx("CDI_LAST_RESTORE_FILE", QVariant::String));
	view.append(DbColumnEx("CDI_TEMPLATE_DB_USED", QVariant::String));
	view.append(DbColumnEx("CDI_ASK_FOR_TEMPLATE_DB", QVariant::Int));
	view.append(DbColumnEx("CDI_START_SETUP_WIZARD", QVariant::Int));
	view.append(DbColumnEx("CDI_SETUP_WIZARD_WIZARD_INFO", QVariant::ByteArray));
	view.append(DbColumnEx("CDI_SERVER_UPDATE_DATE", QVariant::Date));
	view.append(DbColumnEx("CDI_IS_DEMO_DATABASE", QVariant::Int));
	view.append(DbColumnEx("CDI_DEMO_DB_CURR_SIZE", QVariant::Int));
	view.append(DbColumnEx("CDI_DEMO_DB_WARN_SIZE", QVariant::Int));
	view.append(DbColumnEx("CDI_DEMO_DB_MAX_SIZE", QVariant::Int));
	view.append(DbColumnEx("CDI_DATABASE_UNIQUE_KEY", QVariant::String));
	view.append(DbColumnEx("CDI_DATABASE_UTF8", QVariant::Int));
	
	
	
	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// TVIEW_CORE_IMPORT_EXPORT
	//-----------------------

	view.m_nViewID		= TVIEW_CORE_IMPORT_EXPORT;
	view.m_nSkipFirstColsInsert	= 3;
	view.m_nSkipFirstColsUpdate	= 3;
	view.m_strTables="CORE_IMPORT_EXPORT";

	//Record set definition
	view.append(DbColumnEx("CIE_ID", QVariant::Int));
	view.append(DbColumnEx("CIE_GLOBAL_ID", QVariant::String));
	view.append(DbColumnEx("CIE_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("CIE_GLOBAL_SCAN_PERIOD", QVariant::Int));
	view.append(DbColumnEx("CIE_PROJ_ENABLED", QVariant::Int));
	view.append(DbColumnEx("CIE_PROJ_IMPORT", QVariant::Int));
	view.append(DbColumnEx("CIE_PROJ_SCAN_TYPE", QVariant::Int));
	view.append(DbColumnEx("CIE_PROJ_SCAN_PERIOD", QVariant::Int));
	view.append(DbColumnEx("CIE_PROJ_SOURCE", QVariant::Int));
	view.append(DbColumnEx("CIE_PROJ_SOURCE_DIR", QVariant::String));
	view.append(DbColumnEx("CIE_PROJ_SOURCE_FTP_ID", QVariant::Int));
	view.append(DbColumnEx("CIE_PROJ_SOURCE_FTP_DIR", QVariant::String));
	view.append(DbColumnEx("CIE_PROJ_FILE_EXT", QVariant::String));
	view.append(DbColumnEx("CIE_PROJ_FILE_PREFIX", QVariant::String));
	view.append(DbColumnEx("CIE_PROJ_FILE_PREFIX_PROCESS", QVariant::String));
	view.append(DbColumnEx("CIE_PROJ_EXPORT_TYPE", QVariant::Int));
	view.append(DbColumnEx("CIE_PROJ_EXPORT_LAST_N", QVariant::Int));
	view.append(DbColumnEx("CIE_PROJ_ADD_DATETIME", QVariant::Int));
	view.append(DbColumnEx("CIE_PROJ_STATUS_DATE", QVariant::DateTime));
	view.append(DbColumnEx("CIE_PROJ_STATUS_ERROR", QVariant::String));
	view.append(DbColumnEx("CIE_PROJ_STATUS_LAST_FILE", QVariant::String));
	view.append(DbColumnEx("CIE_PROJ_STATUS_REC_OK", QVariant::Int));
	view.append(DbColumnEx("CIE_PROJ_STATUS_REC_FAIL", QVariant::Int));
	view.append(DbColumnEx("CIE_USER_ENABLED", QVariant::Int));
	view.append(DbColumnEx("CIE_USER_IMPORT", QVariant::Int));
	view.append(DbColumnEx("CIE_USER_SCAN_TYPE", QVariant::Int));
	view.append(DbColumnEx("CIE_USER_SCAN_PERIOD", QVariant::Int));
	view.append(DbColumnEx("CIE_USER_SOURCE", QVariant::Int));
	view.append(DbColumnEx("CIE_USER_SOURCE_DIR", QVariant::String));
	view.append(DbColumnEx("CIE_USER_SOURCE_FTP_ID", QVariant::Int));
	view.append(DbColumnEx("CIE_USER_SOURCE_FTP_DIR", QVariant::String));
	view.append(DbColumnEx("CIE_USER_FILE_EXT", QVariant::String));
	view.append(DbColumnEx("CIE_USER_FILE_PREFIX", QVariant::String));
	view.append(DbColumnEx("CIE_USER_FILE_PREFIX_PROCESS", QVariant::String));
	view.append(DbColumnEx("CIE_USER_EXPORT_TYPE", QVariant::Int));
	view.append(DbColumnEx("CIE_USER_EXPORT_LAST_N", QVariant::Int));
	view.append(DbColumnEx("CIE_USER_ADD_DATETIME", QVariant::Int));
	view.append(DbColumnEx("CIE_USER_STATUS_DATE", QVariant::DateTime));
	view.append(DbColumnEx("CIE_USER_STATUS_ERROR", QVariant::String));
	view.append(DbColumnEx("CIE_USER_STATUS_LAST_FILE", QVariant::String));
	view.append(DbColumnEx("CIE_USER_STATUS_REC_OK", QVariant::Int));
	view.append(DbColumnEx("CIE_USER_STATUS_REC_FAIL", QVariant::Int));
	view.append(DbColumnEx("CIE_USER_DEF_ROLE_ID", QVariant::Int));
	view.append(DbColumnEx("CIE_DEBT_ENABLED", QVariant::Int));
	view.append(DbColumnEx("CIE_DEBT_IMPORT", QVariant::Int));
	view.append(DbColumnEx("CIE_DEBT_SCAN_TYPE", QVariant::Int));
	view.append(DbColumnEx("CIE_DEBT_SCAN_PERIOD", QVariant::Int));
	view.append(DbColumnEx("CIE_DEBT_SOURCE", QVariant::Int));
	view.append(DbColumnEx("CIE_DEBT_SOURCE_DIR", QVariant::String));
	view.append(DbColumnEx("CIE_DEBT_SOURCE_FTP_ID", QVariant::Int));
	view.append(DbColumnEx("CIE_DEBT_SOURCE_FTP_DIR", QVariant::String));
	view.append(DbColumnEx("CIE_DEBT_FILE_EXT", QVariant::String));
	view.append(DbColumnEx("CIE_DEBT_FILE_PREFIX", QVariant::String));
	view.append(DbColumnEx("CIE_DEBT_FILE_PREFIX_PROCESS", QVariant::String));
	view.append(DbColumnEx("CIE_DEBT_EXPORT_TYPE", QVariant::Int));
	view.append(DbColumnEx("CIE_DEBT_EXPORT_LAST_N", QVariant::Int));
	view.append(DbColumnEx("CIE_DEBT_ADD_DATETIME", QVariant::Int));
	view.append(DbColumnEx("CIE_DEBT_STATUS_DATE", QVariant::DateTime));
	view.append(DbColumnEx("CIE_DEBT_STATUS_ERROR", QVariant::String));
	view.append(DbColumnEx("CIE_DEBT_STATUS_LAST_FILE", QVariant::String));
	view.append(DbColumnEx("CIE_DEBT_STATUS_REC_OK", QVariant::Int));
	view.append(DbColumnEx("CIE_DEBT_STATUS_REC_FAIL", QVariant::Int));
	view.append(DbColumnEx("CIE_DEBT_ADDRESS_TYPE_ID", QVariant::Int));
	view.append(DbColumnEx("CIE_CONT_ENABLED", QVariant::Int));
	view.append(DbColumnEx("CIE_CONT_IMPORT", QVariant::Int));
	view.append(DbColumnEx("CIE_CONT_SCAN_TYPE", QVariant::Int));
	view.append(DbColumnEx("CIE_CONT_SCAN_PERIOD", QVariant::Int));
	view.append(DbColumnEx("CIE_CONT_SOURCE", QVariant::Int));
	view.append(DbColumnEx("CIE_CONT_SOURCE_DIR", QVariant::String));
	view.append(DbColumnEx("CIE_CONT_SOURCE_FTP_ID", QVariant::Int));
	view.append(DbColumnEx("CIE_CONT_SOURCE_FTP_DIR", QVariant::String));
	view.append(DbColumnEx("CIE_CONT_FILE_EXT", QVariant::String));
	view.append(DbColumnEx("CIE_CONT_FILE_PREFIX", QVariant::String));
	view.append(DbColumnEx("CIE_CONT_FILE_PREFIX_PROCESS", QVariant::String));
	view.append(DbColumnEx("CIE_CONT_EXPORT_TYPE", QVariant::Int));
	view.append(DbColumnEx("CIE_CONT_EXPORT_LAST_N", QVariant::Int));
	view.append(DbColumnEx("CIE_CONT_ADD_DATETIME", QVariant::Int));
	view.append(DbColumnEx("CIE_CONT_STATUS_DATE", QVariant::DateTime));
	view.append(DbColumnEx("CIE_CONT_STATUS_ERROR", QVariant::String));
	view.append(DbColumnEx("CIE_CONT_STATUS_LAST_FILE", QVariant::String));
	view.append(DbColumnEx("CIE_CONT_STATUS_REC_OK", QVariant::Int));
	view.append(DbColumnEx("CIE_CONT_STATUS_REC_FAIL", QVariant::Int));
	view.append(DbColumnEx("CIE_USE_UNICODE", QVariant::Int));
	view.append(DbColumnEx("CIE_SPLST_ENABLED", QVariant::Int));
	view.append(DbColumnEx("CIE_SPLST_IMPORT", QVariant::Int));
	view.append(DbColumnEx("CIE_SPLST_SCAN_TYPE", QVariant::Int));
	view.append(DbColumnEx("CIE_SPLST_SCAN_PERIOD", QVariant::Int));
	view.append(DbColumnEx("CIE_SPLST_SOURCE", QVariant::Int));
	view.append(DbColumnEx("CIE_SPLST_SOURCE_DIR", QVariant::String));
	view.append(DbColumnEx("CIE_SPLST_SOURCE_FTP_ID", QVariant::Int));
	view.append(DbColumnEx("CIE_SPLST_SOURCE_FTP_DIR", QVariant::String));
	view.append(DbColumnEx("CIE_SPLST_FILE_EXT", QVariant::String));
	view.append(DbColumnEx("CIE_SPLST_FILE_PREFIX", QVariant::String));
	view.append(DbColumnEx("CIE_SPLST_FILE_PREFIX_PROCESS", QVariant::String));
	view.append(DbColumnEx("CIE_SPLST_EXPORT_TYPE", QVariant::Int));
	view.append(DbColumnEx("CIE_SPLST_EXPORT_LAST_N", QVariant::Int));
	view.append(DbColumnEx("CIE_SPLST_ADD_DATETIME", QVariant::Int));
	view.append(DbColumnEx("CIE_SPLST_STATUS_DATE", QVariant::DateTime));
	view.append(DbColumnEx("CIE_SPLST_STATUS_ERROR", QVariant::String));
	view.append(DbColumnEx("CIE_SPLST_STATUS_LAST_FILE", QVariant::String));
	view.append(DbColumnEx("CIE_SPLST_STATUS_REC_OK", QVariant::Int));
	view.append(DbColumnEx("CIE_SPLST_STATUS_REC_FAIL", QVariant::Int));


	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// MVIEW_BOENTITYRECORD_WRITE (dynamic or memory view)
	//-----------------------
	view.m_nViewID		= MVIEW_BOENTITYRECORD_WRITE;
	view.append(DbColumnEx( "lstWrite",							DbRecordSet::GetVariantType()));
	view.append(DbColumnEx( "lstDelete",						DbRecordSet::GetVariantType()));
	view.append(DbColumnEx( "lstUnlock",						DbRecordSet::GetVariantType()));
	view.append(DbColumnEx( "datParentLastModificationTime",	QVariant::DateTime));

	s_lstDbViews.append(view);
	view.clear();
	
	//-----------------------
	// MVIEW_BOENTITYRECORD_READ (dynamic or memory view)
	//-----------------------
	view.m_nViewID		= MVIEW_BOENTITYRECORD_READ;
	view.append(DbColumnEx( "nEntityID",  QVariant::Int));
	view.append(DbColumnEx( "lstRecords",  DbRecordSet::GetVariantType()));
	view.append(DbColumnEx( "lstFilter",  DbRecordSet::GetVariantType()));

	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// MVIEW_BOENTITYGROUP_DATA (dynamic or memory view)
	//-----------------------
	view.m_nViewID		= MVIEW_BOENTITYGROUP_DATA;
	view.append(DbColumnEx( "nGroupID",  QVariant::Int));
	view.append(DbColumnEx( "lstGroupData",  DbRecordSet::GetVariantType()));
	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// MVIEW_BOENTITYRECORD_PREPAREFOREDIT (dynamic or memory view)
	//-----------------------
	view.m_nViewID		= MVIEW_BOENTITYRECORD_PREPAREFOREDIT;
	view.append(DbColumnEx( "nEntityID",  QVariant::Int));
	view.append(DbColumnEx( "datLastModificationTime" ,QVariant::DateTime));
	view.append(DbColumnEx( "bReloadFlag",  QVariant::Bool));
	view.append(DbColumnEx( "strLockedResourceID",  QVariant::String));
	view.append(DbColumnEx( "lstRead",  DbRecordSet::GetVariantType()));
	//view.append(DbColumnEx( "lstFilter",  DbRecordSet::GetVariantType()));

	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// MVIEW_BOENTITYRECORD_GROUPS (dynamic or memory view)
	//-----------------------
	view.m_nViewID		= MVIEW_BOENTITYRECORD_GROUPS;
	view.append(DbColumnEx( "nLevel",  QVariant::Int));
	view.append(DbColumnEx( "nParentGroupID",  QVariant::Int));
	view.append(DbColumnEx( "nGroupID",  QVariant::Int));
	view.append(DbColumnEx( "strGroupName" ,QVariant::String));
	view.append(DbColumnEx( "nGroupViewID",  QVariant::Int));

	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// MVIEW_BOENTITYRECORD_SUBGROUPS (dynamic or memory view)
	//-----------------------
	view.m_nViewID		= MVIEW_BOENTITYRECORD_SUBGROUPS;
	view.append(DbColumnEx( "nGroupID",  QVariant::Int));
	view.append(DbColumnEx( "strGroupName" ,QVariant::String));
	view.append(DbColumnEx( "nGroupViewID",  QVariant::Int));

	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// MVIEW_GRID_COLUMN_SETUP
	// For universal table setup, defines each column:
	//-----------------------
	view.m_nViewID		= MVIEW_GRID_COLUMN_SETUP;
	view.append(DbColumnEx("BOGW_HEADER_TEXT",			QVariant::String));
	view.append(DbColumnEx("BOGW_COLUMN_NAME",			QVariant::String));
	view.append(DbColumnEx("BOGW_COLUMN_TYPE",			QVariant::Int));		//0-edit text,1 - checkbox, 2- dropdownlist(combo), 3- listwidget, 4 -picture
	view.append(DbColumnEx("BOGW_COLUMN_SIZE",			QVariant::Int));
	view.append(DbColumnEx("BOGW_COLUMN_POSITION",		QVariant::Int));
	view.append(DbColumnEx("BOGW_EDITABLE",				QVariant::Bool));
	view.append(DbColumnEx("BOGW_SORT_ORDER",			QVariant::Int));		//false ASC (default), else DESC
	view.append(DbColumnEx("BOGW_TOOLTIP",				QVariant::String));		
	view.append(DbColumnEx("BOGW_DATASOURCE",			QVariant::String));		
	view.append(DbColumnEx("BOGW_REPORT_WIDTH",			QVariant::Int));		
	view.append(DbColumnEx("BOGW_SECTION_RESIZE_MODE",	QVariant::Int));

	//if column requires special data source calculation, format: "COLUMN_1<<'Separator'<<COLUMN_2<<COLUMN_3 ... where Separator is custom defined text printed as is


	s_lstDbViews.append(view);
	view.clear();

	//--------------------------------------------------------------------------------------------
	//									Stored list.
	//--------------------------------------------------------------------------------------------
	//-----------------------
	// TVIEW_BUS_STOREDLIST
	// Stored list table.
	//-----------------------
	view.m_nViewID		= TVIEW_BUS_STOREDLIST;
	view.m_nSkipFirstColsInsert = 3;	//skip Id & global
	view.m_nSkipFirstColsUpdate = 3;
	view.m_strTables="BUS_STOREDLIST"; //if more then one table separate by comma "TABLE,TABLE2"
	view.append(DbColumnEx("BUSL_ID",				QVariant::Int));
	view.append(DbColumnEx("BUSL_GLOBAL_ID",		QVariant::String));
	view.append(DbColumnEx("BUSL_DAT_LAST_MODIFIED",QVariant::DateTime));
	view.append(DbColumnEx("BUSL_STLIST_CODE",		QVariant::String));
	view.append(DbColumnEx("BUSL_TREETBL_ID",		QVariant::Int));
	view.append(DbColumnEx("BUSL_OWNER",			QVariant::Int));
	view.append(DbColumnEx("BUSL_DEPARTMENT",		QVariant::Int));
	view.append(DbColumnEx("BUSL_FILTER_ID",		QVariant::Int));

	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// TVIEW_BUS_STOREDLIST_ITEMS
	// Stored list table.
	//-----------------------
	view.m_nViewID		= TVIEW_BUS_STOREDLIST_ITEMS;
	view.m_nSkipFirstColsInsert = 2;	//skip Id & global
	view.m_nSkipFirstColsUpdate =2;
	view.m_strTables="BUS_STOREDLIST_ITEMS"; //if more then one table separate by comma "TABLE,TABLE2"
	view.append(DbColumnEx("BUSLI_ID",				QVariant::Int));
	view.append(DbColumnEx("BUSLI_GLOBAL_ID",		QVariant::String));
	view.append(DbColumnEx("BUSLI_DAT_LAST_MODIFIED",QVariant::DateTime));
	view.append(DbColumnEx("BUSLI_STLIST_ID",		QVariant::Int));
	view.append(DbColumnEx("BUSLI_TREETBL_ROWID",	QVariant::Int));
	view.append(DbColumnEx("BUSLI_ITEMSTATE",		QVariant::Int));
	view.append(DbColumnEx("BUSLI_STRDLIST_LEVEL",	QVariant::Int));

	s_lstDbViews.append(view);
	view.clear();

	//--------------------------------------------------------------------------------------------
	//									Access Rights.
	//--------------------------------------------------------------------------------------------
	//-----------------------
	// TVIEW_CORE_ROLE
	// For universal table setup, defines each column:
	//-----------------------
	view.m_nViewID		= TVIEW_CORE_ROLE;
	view.m_nSkipFirstColsInsert = 3;	//skip Id & global
	view.m_nSkipFirstColsUpdate = 3;
	view.m_strTables="CORE_ROLE";
	view.append(DbColumnEx("CROL_ID",				QVariant::Int));
	view.append(DbColumnEx("CROL_GLOBAL_ID",		QVariant::String));
	view.append(DbColumnEx("CROL_DAT_LAST_MODIFIED",QVariant::DateTime));
	view.append(DbColumnEx("CROL_NAME",				QVariant::String));
	view.append(DbColumnEx("CROL_ROLE_DESC",		QVariant::String));
	view.append(DbColumnEx("CROL_ROLE_TYPE",		QVariant::Int));
	view.append(DbColumnEx("CROL_DEFAULT_FLAG",		QVariant::Int));
	view.append(DbColumnEx("CROL_CODE",				QVariant::Int));

	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// TVIEW_BUS_PERSON
	// For universal table setup, defines each column:
	//-----------------------
	view.m_nViewID		= TVIEW_BUS_PERSON;
	view.m_nSkipFirstColsInsert = 3;	//skip Id & global & last modify
	view.m_nSkipFirstColsUpdate = 3;	//skip Id
	view.m_strTables="BUS_PERSON";
	view.append(DbColumnEx("BPER_ID",				QVariant::Int));
	view.append(DbColumnEx("BPER_GLOBAL_ID",		QVariant::String));
	view.append(DbColumnEx("BPER_DAT_LAST_MODIFIED",QVariant::DateTime));
	view.append(DbColumnEx("BPER_FIRST_NAME",		QVariant::String));
	view.append(DbColumnEx("BPER_LAST_NAME",		QVariant::String));
	view.append(DbColumnEx("BPER_ACTIVE_FLAG",		QVariant::Int));
	view.append(DbColumnEx("BPER_CODE",				QVariant::String));
	view.append(DbColumnEx("BPER_CONTACT_ID",		QVariant::Int));
	view.append(DbColumnEx("BPER_SEX",				QVariant::Int));
	view.append(DbColumnEx("BPER_BIRTH_DATE",		QVariant::Date));
	view.append(DbColumnEx("BPER_DESCRIPTION",		QVariant::String,true));
	view.append(DbColumnEx("BPER_INITIALS",			QVariant::String));
	view.append(DbColumnEx("BPER_DEPARTMENT_ID",	QVariant::Int));
	view.append(DbColumnEx("BPER_ORGANIZATION_ID",	QVariant::Int));
	view.append(DbColumnEx("BPER_DIRECT_PHONE",		QVariant::String));
	view.append(DbColumnEx("BPER_INSURANCE_ID",		QVariant::String));
	view.append(DbColumnEx("BPER_MAIN_FUNCTION_ID",	QVariant::String));
	view.append(DbColumnEx("BPER_OCCUPATION",		QVariant::Double));
	view.append(DbColumnEx("BPER_DATE_ENTERED",		QVariant::Date));
	view.append(DbColumnEx("BPER_DATE_LEFT",		QVariant::Date));
	view.append(DbColumnEx("BPER_PICTURE",			QVariant::ByteArray));
	view.append(DbColumnEx("BPER_BIG_PICTURE_ID",	QVariant::Int));
	view.append(DbColumnEx("BPER_CODE_EXT",			QVariant::String));
	view.append(DbColumnEx("BPER_DEF_PROJECT_LIST_ID",	QVariant::Int));
	view.append(DbColumnEx("BPER_FPERSON_SEQ",		QVariant::Int));
	
	s_lstDbViews.append(view);
	view.clear();



	//-----------------------
	// TVIEW_BUS_PERSONROLE
	// For universal table setup, defines each column:
	//-----------------------
	view.m_nViewID		= TVIEW_BUS_PERSONROLE;
	view.m_nSkipFirstColsInsert = 3;	//skip Id & global
	view.m_nSkipFirstColsUpdate = 3;
	view.m_strTables="BUS_PERSONROLE";
	view.append(DbColumnEx("BPRL_ID",				QVariant::Int));
	view.append(DbColumnEx("BPRL_GLOBAL_ID",		QVariant::String));
	view.append(DbColumnEx("BPRL_DAT_LAST_MODIFIED",QVariant::DateTime));
	view.append(DbColumnEx("BPRL_PERSON_ID",		QVariant::Int));
	view.append(DbColumnEx("BPRL_ROLE_ID",			QVariant::Int));

	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// TVIEW_CORE_USER
	// For universal table setup, defines each column:
	//-----------------------
	view.m_nViewID		= TVIEW_CORE_USER;
	view.m_nSkipFirstColsInsert = 3;	//skip Id & global
	view.m_nSkipFirstColsUpdate = 3;
	view.m_strTables="CORE_USER";
	view.append(DbColumnEx("CUSR_ID",				QVariant::Int));
	view.append(DbColumnEx("CUSR_GLOBAL_ID",		QVariant::String));
	view.append(DbColumnEx("CUSR_DAT_LAST_MODIFIED",QVariant::DateTime));
	view.append(DbColumnEx("CUSR_FIRST_NAME",		QVariant::String));
	view.append(DbColumnEx("CUSR_LAST_NAME",		QVariant::String));
	view.append(DbColumnEx("CUSR_USERNAME",			QVariant::String));
	view.append(DbColumnEx("CUSR_PASSWORD",			QVariant::ByteArray));
	view.append(DbColumnEx("CUSR_ACTIVE_FLAG",		QVariant::Int));
	view.append(DbColumnEx("CUSR_PERSON_ID",		QVariant::Int));
	view.append(DbColumnEx("CUSR_IS_SYSTEM",		QVariant::Int));
	view.append(DbColumnEx("CUSR_LOCAL_ACCESS_ONLY",QVariant::Int));
	view.append(DbColumnEx("CUSR_IS_DEFAULT_ACC",	QVariant::Int));
	

	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// TVIEW_CORE_USERROLE
	// For universal table setup, defines each column:
	//-----------------------
	view.m_nViewID		= TVIEW_CORE_USERROLE;
	view.m_nSkipFirstColsInsert = 3;	//skip Id & global
	view.m_nSkipFirstColsUpdate = 3;
	view.m_strTables="CORE_USERROLE";
	view.append(DbColumnEx("CURL_ID",				QVariant::Int));
	view.append(DbColumnEx("CURL_GLOBAL_ID",		QVariant::String));
	view.append(DbColumnEx("CURL_DAT_LAST_MODIFIED",QVariant::DateTime));
	view.append(DbColumnEx("CURL_USER_ID",			QVariant::Int));
	view.append(DbColumnEx("CURL_ROLE_ID",			QVariant::Int));

	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// TVIEW_CORE_ACCESSRIGHTS
	// For universal table setup, defines each column:
	//-----------------------
	view.m_nViewID		= TVIEW_CORE_ACCESSRIGHTS;
	view.m_nSkipFirstColsInsert = 3;	//skip Id & global
	view.m_nSkipFirstColsUpdate = 3;
	view.m_strTables="CORE_ACCESSRIGHTS";
	view.append(DbColumnEx("CAR_ID",				QVariant::Int));
	view.append(DbColumnEx("CAR_GLOBAL_ID",			QVariant::String));
	view.append(DbColumnEx("CAR_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("CAR_ACCRSET_ID",		QVariant::Int));
	view.append(DbColumnEx("CAR_ROLE_ID",			QVariant::Int));
	view.append(DbColumnEx("CAR_NAME",				QVariant::String));
	view.append(DbColumnEx("CAR_TYPE",				QVariant::Int));
	view.append(DbColumnEx("CAR_VALUE",				QVariant::Int));
	view.append(DbColumnEx("CAR_VALUE_DESCR",		QVariant::String));
	view.append(DbColumnEx("CAR_CODE",				QVariant::Int));

	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// TVIEW_CORE_ACCRSET
	// For universal table setup, defines each column:
	//-----------------------
	view.m_nViewID		= TVIEW_CORE_ACCRSET;
	view.m_nSkipFirstColsInsert = 3;	//skip Id & global
	view.m_nSkipFirstColsUpdate = 3;
	view.m_strTables="CORE_ACCRSET";
	view.append(DbColumnEx("CAST_ID",				QVariant::Int));
	view.append(DbColumnEx("CAST_GLOBAL_ID",		QVariant::String));
	view.append(DbColumnEx("CAST_DAT_LAST_MODIFIED",QVariant::DateTime));
	view.append(DbColumnEx("CAST_NAME",				QVariant::String));
	view.append(DbColumnEx("CAST_ACCRSET_DESC",		QVariant::String));
	view.append(DbColumnEx("CAST_ACCRSET_TYPE",		QVariant::Int));
	view.append(DbColumnEx("CAST_CODE",				QVariant::Int));

	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// TVIEW_CORE_ACCRSETROLE
	// For universal table setup, defines each column:
	//-----------------------
	view.m_nViewID		= TVIEW_CORE_ACCRSETROLE;
	view.m_nSkipFirstColsInsert = 3;	//skip Id & global
	view.m_nSkipFirstColsUpdate = 3;
	view.m_strTables="CORE_ACCRSETROLE";
	view.append(DbColumnEx("CASTR_ID",				QVariant::Int));
	view.append(DbColumnEx("CASTR_GLOBAL_ID",		QVariant::String));
	view.append(DbColumnEx("CASTR_DAT_LAST_MODIFIED",QVariant::DateTime));
	view.append(DbColumnEx("CASTR_ROLE_ID",			QVariant::Int));
	view.append(DbColumnEx("CASTR_ACCRSET_ID",		QVariant::Int));

	s_lstDbViews.append(view);
	view.clear();

	//--------------------------------------------------------------------------------------------
	//									Access Rights views.
	//--------------------------------------------------------------------------------------------
	//-----------------------
	// TVIEW_CORE_ROLE_VIEW
	// For universal table setup, defines each column:
	//-----------------------
	view.m_nViewID		= TVIEW_CORE_ROLE_VIEW;
	view.m_nSkipFirstColsInsert = 3;	//skip Id & global
	view.m_nSkipFirstColsUpdate = 3;
	view.m_strTables="CORE_ROLE";
	view.append(DbColumnEx("CROL_ID",				QVariant::Int));
	view.append(DbColumnEx("CROL_NAME",				QVariant::String));
	view.append(DbColumnEx("CROL_ROLE_DESC",		QVariant::String));
	view.append(DbColumnEx("CROL_ROLE_TYPE",		QVariant::Int));
	view.append(DbColumnEx("CROL_CODE",				QVariant::Int));

	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// TVIEW_PERSON_TREE_VIEW
	// Person Access Rights tree view.
	//-----------------------
	view.m_nViewID		= TVIEW_PERSON_TREE_VIEW;
	view.m_nSkipFirstColsInsert = 3;	//skip Id & global
	view.m_nSkipFirstColsUpdate = 3;
	view.m_strTables="BUS_PERSON"; //if more then one table separate by comma "TABLE,TABLE2"
	view.append(DbColumnEx("BPER_ID",					QVariant::Int));		//Row ID.
	view.append(DbColumnEx("BPER_FIRST_NAME",			QVariant::String));		//First Name.
	view.append(DbColumnEx("BPER_LAST_NAME",			QVariant::String));		//Last Name.
	view.append(DbColumnEx("BPER_ACTIVE_FLAG",			QVariant::Int));		//Is active flag.

	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// TVIEW_USER_TREE_VIEW
	// User Access Rights tree view.
	//-----------------------
	view.m_nViewID		= TVIEW_USER_TREE_VIEW;
	view.m_nSkipFirstColsInsert = 3;	//skip Id & global
	view.m_nSkipFirstColsUpdate = 3;
	view.m_strTables="CORE_USER"; //if more then one table separate by comma "TABLE,TABLE2"
	view.append(DbColumnEx("CUSR_ID",					QVariant::Int));		//Row ID.
	view.append(DbColumnEx("CUSR_FIRST_NAME",			QVariant::String));		//First Name.
	view.append(DbColumnEx("CUSR_LAST_NAME",			QVariant::String));		//Last Name.
	view.append(DbColumnEx("CUSR_ACTIVE_FLAG",			QVariant::Int));		//Is active flag.

	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// TVIEW_PERSON_ROLE_LIST
	// One person role list view.
	//-----------------------
	view.m_nViewID		= TVIEW_PERSON_ROLE_LIST;
	view.m_nSkipFirstColsInsert = 3;	//skip Id & global
	view.m_nSkipFirstColsUpdate = 3;
	view.m_strTables="CORE_ROLE, BUS_PERSONROLE"; //if more then one table separate by comma "TABLE,TABLE2"
	view.append(DbColumnEx("CROL_ID",				QVariant::Int));
	view.append(DbColumnEx("CROL_NAME",				QVariant::String));
	view.append(DbColumnEx("CROL_ROLE_DESC",		QVariant::String));
	view.append(DbColumnEx("BPRL_PERSON_ID",		QVariant::Int));
	view.append(DbColumnEx("CROL_ROLE_TYPE",		QVariant::Int));

	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// TVIEW_USER_ROLE_LIST
	// One user role list view.
	//-----------------------
	view.m_nViewID		= TVIEW_USER_ROLE_LIST;
	view.m_nSkipFirstColsInsert = 3;	//skip Id & global
	view.m_nSkipFirstColsUpdate = 3;
	view.m_strTables="CORE_ROLE, CORE_USERROLE"; //if more then one table separate by comma "TABLE,TABLE2"
	view.append(DbColumnEx("CROL_ID",				QVariant::Int));
	view.append(DbColumnEx("CROL_NAME",				QVariant::String));
	view.append(DbColumnEx("CROL_ROLE_DESC",		QVariant::String));
	view.append(DbColumnEx("CURL_USER_ID",			QVariant::Int));
	view.append(DbColumnEx("CROL_ROLE_TYPE",		QVariant::Int));

	s_lstDbViews.append(view);
	view.clear();



	
	view.m_nViewID		= TVIEW_BUS_PERSON_SELECTION;
	view.m_strTables="BUS_PERSON";
	view.m_nSkipLastCols=3; //skip two cols at end (calculated)
	view.append(DbColumnEx("BPER_ID",				QVariant::Int));
	view.append(DbColumnEx("BPER_CODE",				QVariant::String));
	view.append(DbColumnEx("BPER_FIRST_NAME",		QVariant::String));
	view.append(DbColumnEx("BPER_LAST_NAME",		QVariant::String));
	view.append(DbColumnEx("BPER_DEPARTMENT_ID",	QVariant::Int));
	view.append(DbColumnEx("BPER_ORGANIZATION_ID",	QVariant::Int));
	view.append(DbColumnEx("BPER_CONTACT_ID",		QVariant::Int));
	view.append(DbColumnEx("BDEPT_CODE",			QVariant::String));
	view.append(DbColumnEx("BDEPT_NAME",			QVariant::String));
	view.append(DbColumnEx("CUSR_IS_SYSTEM",		QVariant::Int));
	view.append(DbColumnEx("CUSR_IS_DEFAULT_ACC",	QVariant::Int));
	view.append(DbColumnEx("CUSR_USERNAME",			QVariant::String));
	view.append(DbColumnEx("BPER_INITIALS",			QVariant::String));
	view.append(DbColumnEx("CUSR_ACTIVE_FLAG",		QVariant::Int));
	view.append(DbColumnEx("BPER_ACTIVE_FLAG",		QVariant::Int));
	//calc fields:
	view.append(DbColumnEx("BPER_NAME",				QVariant::String));			//calc field
	view.append(DbColumnEx("BPER_DEPT_NAME",		QVariant::String));			//calc field
	view.append(DbColumnEx("IS_LOGIN_ACC",			QVariant::Int));
	
	view.m_strSelectSQL="SELECT "+getSQLColumnsFromView(view)+" FROM BUS_PERSON LEFT OUTER JOIN BUS_DEPARTMENTS ON BPER_DEPARTMENT_ID=BDEPT_ID";
	view.m_strSelectSQL+=" LEFT OUTER JOIN CORE_USER ON BPER_ID=CUSR_PERSON_ID";
	s_lstDbViews.append(view);
	view.clear();



	//-----------------------
	// TVIEW_CORE_USER_SELECTION
	// For universal table setup, defines each column:
	//-----------------------
	view.m_nViewID		= TVIEW_CORE_USER_SELECTION;
	view.m_strTables="CORE_USER,BUS_PERSON";
	view.append(DbColumnEx("CUSR_ID",				QVariant::Int));
	view.append(DbColumnEx("CUSR_FIRST_NAME",		QVariant::String));
	view.append(DbColumnEx("CUSR_LAST_NAME",		QVariant::String));
	view.append(DbColumnEx("CUSR_USERNAME",			QVariant::String));
	view.append(DbColumnEx("CUSR_PASSWORD",			QVariant::ByteArray));
	view.append(DbColumnEx("CUSR_ACTIVE_FLAG",		QVariant::Int));
	view.append(DbColumnEx("CUSR_PERSON_ID",		QVariant::Int));
	view.append(DbColumnEx("CUSR_IS_SYSTEM",		QVariant::Int));
	view.append(DbColumnEx("CUSR_LOCAL_ACCESS_ONLY",QVariant::Int));
	view.append(DbColumnEx("BPER_ID",				QVariant::Int));
	view.append(DbColumnEx("BPER_ACTIVE_FLAG",		QVariant::Int));
	view.append(DbColumnEx("BPER_CODE",				QVariant::String));
	view.append(DbColumnEx("BPER_CONTACT_ID",		QVariant::Int));
	view.append(DbColumnEx("BPER_SEX",				QVariant::Int));
	view.append(DbColumnEx("BPER_BIRTH_DATE",		QVariant::Date));
	view.append(DbColumnEx("BPER_INITIALS",			QVariant::String));
	view.append(DbColumnEx("BPER_DEPARTMENT_ID",	QVariant::Int));
	view.append(DbColumnEx("BPER_ORGANIZATION_ID",	QVariant::Int));
	view.append(DbColumnEx("BPER_DIRECT_PHONE",		QVariant::String));
	view.append(DbColumnEx("BPER_INSURANCE_ID",		QVariant::String));
	view.append(DbColumnEx("BPER_MAIN_FUNCTION_ID",	QVariant::Int));
	view.append(DbColumnEx("BPER_OCCUPATION",		QVariant::Double));
	view.append(DbColumnEx("BPER_DATE_ENTERED",		QVariant::Date));
	view.append(DbColumnEx("BPER_DATE_LEFT",		QVariant::Date));
	view.append(DbColumnEx("BPER_FIRST_NAME",		QVariant::String));
	view.append(DbColumnEx("BPER_LAST_NAME",		QVariant::String));
	view.append(DbColumnEx("BPER_DEF_PROJECT_LIST_ID",	QVariant::Int));
	view.append(DbColumnEx("BPER_FPERSON_SEQ",		QVariant::Int));
	view.append(DbColumnEx("BDEPT_CODE",			QVariant::String));
	view.append(DbColumnEx("BDEPT_NAME",			QVariant::String));
	
	view.m_strSelectSQL="SELECT "+ getSQLColumnsFromView(view)+" FROM CORE_USER LEFT OUTER JOIN BUS_PERSON ON CUSR_PERSON_ID=BPER_ID";
	view.m_strSelectSQL+=" LEFT OUTER JOIN BUS_DEPARTMENTS ON BPER_DEPARTMENT_ID=BDEPT_ID";
	s_lstDbViews.append(view);
	view.clear();


	//-----------------------
	// TVIEW_CORE_USER_SELECTION
	// For universal table setup, defines each column:
	//-----------------------
	view.m_nViewID		= TVIEW_CORE_USER_SELECTION_SHORT;
	view.m_strSelectSQL="SELECT CUSR_ID,CUSR_FIRST_NAME,CUSR_LAST_NAME,CUSR_USERNAME FROM CORE_USER";
	view.m_strTables="CORE_USER";
	view.append(DbColumnEx("CUSR_ID",				QVariant::Int));
	view.append(DbColumnEx("CUSR_FIRST_NAME",		QVariant::String));
	view.append(DbColumnEx("CUSR_LAST_NAME",		QVariant::String));
	view.append(DbColumnEx("CUSR_USERNAME",			QVariant::String));
	view.append(DbColumnEx("CUSR_NAME",				QVariant::String));

	s_lstDbViews.append(view);
	view.clear();

	

	//-----------------------
	// TVIEW_BUS_ORGANIZATION
	//-----------------------
	view.m_nViewID		= TVIEW_BUS_ORGANIZATION;
	view.m_nSkipFirstColsInsert = 3;
	view.m_nSkipFirstColsUpdate = 3;
	view.m_strTables="BUS_ORGANIZATIONS";

	//Record set definition
	view.append(DbColumnEx("BORG_ID", QVariant::Int));
	view.append(DbColumnEx("BORG_GLOBAL_ID", QVariant::String));
	view.append(DbColumnEx("BORG_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("BORG_CODE", QVariant::String));
	view.append(DbColumnEx("BORG_NAME", QVariant::String));
	view.append(DbColumnEx("BORG_ACTIVE_FLAG", QVariant::Int));
	view.append(DbColumnEx("BORG_DESCRIPTION", QVariant::String,true));
	view.append(DbColumnEx("BORG_CONTACT_ID", QVariant::Int));
	view.append(DbColumnEx("BORG_LEVEL", QVariant::Int));
	view.append(DbColumnEx("BORG_PARENT", QVariant::Int));
	view.append(DbColumnEx("BORG_HASCHILDREN", QVariant::Int));
	view.append(DbColumnEx("BORG_MAINPARENT_ID", QVariant::Int));
	view.append(DbColumnEx("BORG_ICON", QVariant::String));
	view.append(DbColumnEx("BORG_STYLE", QVariant::Int));

	s_lstDbViews.append(view);
	view.clear();

	//automatically create selection view
	InitTreeSelectionView(BUS_ORGANIZATION, view);
	view.m_nViewID		= TVIEW_BUS_ORGANIZATION_SELECTION;
	s_lstDbViews.append(view);
	view.clear();


	//-----------------------
	// TVIEW_BUS_DEPARTMENT
	//-----------------------
	view.m_nViewID		= TVIEW_BUS_DEPARTMENT;
	view.m_nSkipFirstColsInsert = 3;
	view.m_nSkipFirstColsUpdate = 3;
	view.m_strTables="BUS_DEPARTMENTS";

	//Record set definition
	view.append(DbColumnEx("BDEPT_ID", QVariant::Int));
	view.append(DbColumnEx("BDEPT_GLOBAL_ID", QVariant::String));
	view.append(DbColumnEx("BDEPT_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("BDEPT_CODE", QVariant::String));
	view.append(DbColumnEx("BDEPT_LEVEL", QVariant::Int));
	view.append(DbColumnEx("BDEPT_PARENT", QVariant::Int));
	view.append(DbColumnEx("BDEPT_HASCHILDREN", QVariant::Int));
	view.append(DbColumnEx("BDEPT_ICON", QVariant::String));
	view.append(DbColumnEx("BDEPT_STYLE", QVariant::Int));
	view.append(DbColumnEx("BDEPT_MAINPARENT_ID", QVariant::Int));
	view.append(DbColumnEx("BDEPT_NAME", QVariant::String));
	view.append(DbColumnEx("BDEPT_ACTIVE_FLAG", QVariant::Int));
	view.append(DbColumnEx("BDEPT_DESCRIPTION", QVariant::String,true));
	view.append(DbColumnEx("BDEPT_CONTACT_ID", QVariant::Int));
	view.append(DbColumnEx("BDEPT_ORGANIZATION_ID", QVariant::Int));
	view.append(DbColumnEx("BDEPT_COST_CENTER_ID", QVariant::Int));
	view.append(DbColumnEx("BDEPT_F_ABTEILUNG_SEQ", QVariant::Int));
	

	s_lstDbViews.append(view);
	view.clear();

	//automatically create selection view
	InitTreeSelectionView(BUS_DEPARTMENT, view);
	view.append(DbColumnEx("BDEPT_ORGANIZATION_ID", QVariant::Int));
	view.m_nViewID		= TVIEW_BUS_DEPARTMENT_SELECTION;
	s_lstDbViews.append(view);
	view.clear();

	

	//-----------------------
	// TVIEW_BUS_COST_CENTER
	//-----------------------
	view.m_nViewID		= TVIEW_BUS_COST_CENTER;
	view.m_nSkipFirstColsInsert = 3;
	view.m_nSkipFirstColsUpdate = 3;
	view.m_strTables="BUS_COST_CENTERS";

	//Record set definition
	view.append(DbColumnEx("BCTC_ID", QVariant::Int));
	view.append(DbColumnEx("BCTC_GLOBAL_ID", QVariant::String));
	view.append(DbColumnEx("BCTC_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("BCTC_CODE", QVariant::String));
	view.append(DbColumnEx("BCTC_NAME", QVariant::String));
	view.append(DbColumnEx("BCTC_ACTIVE_FLAG", QVariant::Int));
	view.append(DbColumnEx("BCTC_DESCRIPTION", QVariant::String,true));
	view.append(DbColumnEx("BCTC_ORGANIZATION_ID", QVariant::Int));
	view.append(DbColumnEx("BCTC_LEVEL", QVariant::Int));
	view.append(DbColumnEx("BCTC_PARENT", QVariant::Int));
	view.append(DbColumnEx("BCTC_HASCHILDREN", QVariant::Int));
	view.append(DbColumnEx("BCTC_ICON", QVariant::String));
	view.append(DbColumnEx("BCTC_MAINPARENT_ID", QVariant::Int));
	view.append(DbColumnEx("BCTC_STYLE", QVariant::Int));

	s_lstDbViews.append(view);
	view.clear();

	//automatically create selection view
	InitTreeSelectionView(BUS_COST_CENTER, view);
	view.append(DbColumnEx("BCTC_ORGANIZATION_ID", QVariant::Int)); //for filters
	view.m_nViewID		= TVIEW_BUS_COST_CENTER_SELECTION;
	s_lstDbViews.append(view);
	view.clear();

	
	//--------------------------------------------------------------------------------------------
	//							CONTACTS
	//--------------------------------------------------------------------------------------------

	//-----------------------
	// TVIEW_BUS_CM_EMAIL
	//-----------------------


	view.m_nViewID		= TVIEW_BUS_CM_EMAIL;
	view.m_nSkipFirstColsInsert = 3;
	view.m_nSkipFirstColsUpdate = 3;
	view.m_strTables="BUS_CM_EMAIL";

	//Record set definition
	view.append(DbColumnEx("BCME_ID", QVariant::Int));
	view.append(DbColumnEx("BCME_GLOBAL_ID", QVariant::String));
	view.append(DbColumnEx("BCME_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("BCME_CONTACT_ID", QVariant::Int));
	view.append(DbColumnEx("BCME_TYPE_ID", QVariant::Int));
	view.append(DbColumnEx("BCME_NAME", QVariant::String));
	view.append(DbColumnEx("BCME_ADDRESS", QVariant::String));
	view.append(DbColumnEx("BCME_DESCRIPTION", QVariant::String,true));
	view.append(DbColumnEx("BCME_EXT_ID", QVariant::Int));
	view.append(DbColumnEx("BCME_IS_DEFAULT", QVariant::Int));

	s_lstDbViews.append(view);
	view.clear();
	


	//-----------------------
	// TVIEW_BUS_CM_PICTURE
	//-----------------------

	
	view.m_nViewID		= TVIEW_BUS_CM_PICTURE;
	view.m_nSkipFirstColsInsert = 3;
	view.m_nSkipFirstColsUpdate = 3;
	view.m_strTables="BUS_CM_PICTURE";

	//Record set definition
	view.append(DbColumnEx("BCMPC_ID", QVariant::Int));
	view.append(DbColumnEx("BCMPC_GLOBAL_ID", QVariant::String));
	view.append(DbColumnEx("BCMPC_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("BCMPC_CONTACT_ID", QVariant::Int));
	view.append(DbColumnEx("BCMPC_DATE", QVariant::Date));
	view.append(DbColumnEx("BCMPC_TEXT", QVariant::String,true));
	view.append(DbColumnEx("BCMPC_NAME", QVariant::String));
	view.append(DbColumnEx("BCMPC_PICTURE", QVariant::ByteArray));
	view.append(DbColumnEx("BCMPC_IS_DEFAULT", QVariant::Int));
	view.append(DbColumnEx("BCMPC_BIG_PICTURE_ID", QVariant::Int));
	
	s_lstDbViews.append(view);
	view.clear();




	//-----------------------
	// TVIEW_BUS_CM_PAYMENT_CONDITIONS
	//-----------------------

	view.m_nViewID		= TVIEW_BUS_CM_PAYMENT_CONDITIONS;	
	view.m_nSkipFirstColsInsert = 3;
	view.m_nSkipFirstColsUpdate = 3;
	view.m_strTables="BUS_CM_PAYMENT_CONDITIONS";

	//Record set definition
	view.append(DbColumnEx("BCMPY_ID", QVariant::Int));
	view.append(DbColumnEx("BCMPY_GLOBAL_ID", QVariant::String));
	view.append(DbColumnEx("BCMPY_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("BCMPY_NAME", QVariant::String));
	view.append(DbColumnEx("BCMPY_TEXT", QVariant::String,true));
	view.append(DbColumnEx("BCMPY_PAYMENTPERIOD1", QVariant::Int));
	view.append(DbColumnEx("BCMPY_PAYMENTPERIOD2", QVariant::Int));
	view.append(DbColumnEx("BCMPY_PAYMENTPERIOD3", QVariant::Int));
	view.append(DbColumnEx("BCMPY_DISCOUNT1", QVariant::Double));
	view.append(DbColumnEx("BCMPY_DISCOUNT2", QVariant::Double));
	view.append(DbColumnEx("BCMPY_DISCOUNT3", QVariant::Double));

	s_lstDbViews.append(view);
	view.clear();


	//-----------------------
	// TVIEW_BUS_CM_PAYMENT_CONDITIONS_SELECT
	//-----------------------

	view.m_nViewID		= TVIEW_BUS_CM_PAYMENT_CONDITIONS_SELECT;	
	view.m_strTables	= "BUS_CM_PAYMENT_CONDITIONS";

	//Record set definition
	view.append(DbColumnEx("BCMPY_ID", QVariant::Int));
	view.append(DbColumnEx("BCMPY_NAME", QVariant::String));
	s_lstDbViews.append(view);
	view.clear();





	//-----------------------
	// TVIEW_BUS_CM_JOURNAL
	//-----------------------

	view.m_nViewID		= TVIEW_BUS_CM_JOURNAL;	
	view.m_nSkipFirstColsInsert = 3;
	view.m_nSkipFirstColsUpdate = 3;
	view.m_strTables="BUS_CM_JOURNAL";

	//Record set definition
	view.append(DbColumnEx("BCMJ_ID", QVariant::Int));
	view.append(DbColumnEx("BCMJ_GLOBAL_ID", QVariant::String));
	view.append(DbColumnEx("BCMJ_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("BCMJ_CONTACT_ID", QVariant::Int));
	view.append(DbColumnEx("BCMJ_PERSON_ID", QVariant::Int));
	view.append(DbColumnEx("BCMJ_DATE", QVariant::Date));
	view.append(DbColumnEx("BCMJ_TEXT", QVariant::String,true));
	view.append(DbColumnEx("BCMJ_IS_PRIVATE", QVariant::Int));

	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// TVIEW_BUS_CM_JOURNAL_SELECT
	//-----------------------

	view.m_nViewID		= TVIEW_BUS_CM_JOURNAL_SELECT;	
	view.m_nSkipFirstColsInsert = 3;
	view.m_nSkipFirstColsUpdate = 3;
	view.m_nSkipLastCols = 1; //read
	view.m_nSkipLastColsWrite = 4; //write

	view.m_strTables="BUS_CM_JOURNAL";

	//Record set definition
	view.append(DbColumnEx("BCMJ_ID", QVariant::Int));
	view.append(DbColumnEx("BCMJ_GLOBAL_ID", QVariant::String));
	view.append(DbColumnEx("BCMJ_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("BCMJ_CONTACT_ID", QVariant::Int));
	view.append(DbColumnEx("BCMJ_PERSON_ID", QVariant::Int));
	view.append(DbColumnEx("BCMJ_DATE", QVariant::Date));
	view.append(DbColumnEx("BCMJ_TEXT", QVariant::String, true));
	view.append(DbColumnEx("BCMJ_IS_PRIVATE", QVariant::Int));
	view.append(DbColumnEx("BPER_CODE",				QVariant::String));
	view.append(DbColumnEx("BPER_FIRST_NAME",		QVariant::String));
	view.append(DbColumnEx("BPER_LAST_NAME",		QVariant::String));
	view.append(DbColumnEx("BPER_NAME",				QVariant::String));

	view.m_strSelectSQL="SELECT "+getSQLColumnsFromView(view)+" FROM BUS_CM_JOURNAL INNER JOIN bus_person ON BCMJ_PERSON_ID=BPER_ID";

	//Add extra column info
	s_lstDbViews.append(view);
	view.clear();





	//-----------------------
	// TVIEW_BUS_CM_DEBTOR
	//-----------------------

	view.m_nViewID		= TVIEW_BUS_CM_DEBTOR;	
	view.m_nSkipFirstColsInsert = 3;
	view.m_nSkipFirstColsUpdate = 3;
	view.m_strTables="BUS_CM_DEBTOR";

	//Record set definition
	view.append(DbColumnEx("BCMD_ID", QVariant::Int));
	view.append(DbColumnEx("BCMD_GLOBAL_ID", QVariant::String));
	view.append(DbColumnEx("BCMD_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("BCMD_CONTACT_ID", QVariant::Int));
	view.append(DbColumnEx("BCMD_DEBTORCODE", QVariant::String));
	view.append(DbColumnEx("BCMD_DEBTORACCOUNT", QVariant::String));
	view.append(DbColumnEx("BCMD_DESCRIPTION", QVariant::String,true));
	view.append(DbColumnEx("BCMD_CUSTOMERCODE", QVariant::String));
	view.append(DbColumnEx("BCMD_INVOICE_TEXT", QVariant::String,true));
	view.append(DbColumnEx("BCMD_PAYMENT_ID", QVariant::Int));
	view.append(DbColumnEx("BCMD_INTERNAL_DEBTOR", QVariant::Int));
	
	s_lstDbViews.append(view);
	view.clear();






	//-----------------------
	// TVIEW_BUS_CM_CREDITOR
	//-----------------------

	view.m_nViewID		= TVIEW_BUS_CM_CREDITOR;	
	view.m_nSkipFirstColsInsert = 3;
	view.m_nSkipFirstColsUpdate = 3;
	view.m_strTables="BUS_CM_CREDITOR";

	//Record set definition
	view.append(DbColumnEx("BCMC_ID", QVariant::Int));
	view.append(DbColumnEx("BCMC_GLOBAL_ID", QVariant::String));
	view.append(DbColumnEx("BCMC_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("BCMC_CONTACT_ID", QVariant::Int));
	view.append(DbColumnEx("BCMC_CREDITORCODE", QVariant::String));
	view.append(DbColumnEx("BCMC_CREDITORACCOUNT", QVariant::String));
	view.append(DbColumnEx("BCMC_DESCRIPTION", QVariant::String,true));

	s_lstDbViews.append(view);
	view.clear();


	//-----------------------
	// TVIEW_BUS_CM_ADDRESS
	//-----------------------

	view.m_nViewID		= TVIEW_BUS_CM_ADDRESS;	
	view.m_nSkipFirstColsInsert = 3;
	view.m_nSkipFirstColsUpdate = 3;
	view.m_strTables="BUS_CM_ADDRESS";

	//Record set definition
	view.append(DbColumnEx("BCMA_ID", QVariant::Int));
	view.append(DbColumnEx("BCMA_GLOBAL_ID", QVariant::String));
	view.append(DbColumnEx("BCMA_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("BCMA_CONTACT_ID", QVariant::Int));
	view.append(DbColumnEx("BCMA_FORMATEDADDRESS", QVariant::String,true));
	view.append(DbColumnEx("BCMA_NAME", QVariant::String));
	view.append(DbColumnEx("BCMA_FIRSTNAME", QVariant::String));
	view.append(DbColumnEx("BCMA_LASTNAME", QVariant::String));
	view.append(DbColumnEx("BCMA_MIDDLENAME", QVariant::String));
	view.append(DbColumnEx("BCMA_ORGANIZATIONNAME", QVariant::String));
	view.append(DbColumnEx("BCMA_ORGANIZATIONNAME_2", QVariant::String));
	view.append(DbColumnEx("BCMA_TITLE", QVariant::String));
	view.append(DbColumnEx("BCMA_SALUTATION", QVariant::String));
	view.append(DbColumnEx("BCMA_LANGUGAGE_CODE", QVariant::String));
	view.append(DbColumnEx("BCMA_COUNTRY_CODE", QVariant::String));
	view.append(DbColumnEx("BCMA_COUNTRY_NAME", QVariant::String));
	view.append(DbColumnEx("BCMA_STREET", QVariant::String));
	view.append(DbColumnEx("BCMA_CITY", QVariant::String));
	view.append(DbColumnEx("BCMA_ZIP", QVariant::String));
	view.append(DbColumnEx("BCMA_REGION", QVariant::String));
	view.append(DbColumnEx("BCMA_POBOX", QVariant::String));
	view.append(DbColumnEx("BCMA_POBOXZIP", QVariant::String));
	view.append(DbColumnEx("BCMA_OFFICECODE", QVariant::String));
	view.append(DbColumnEx("BCMA_EXT_ID", QVariant::Int));
	view.append(DbColumnEx("BCMA_IS_DEFAULT", QVariant::Int));
	view.append(DbColumnEx("BCMA_FORMATSCHEMA_ID", QVariant::Int));
	view.append(DbColumnEx("BCMA_PARSESCHEMA_ID", QVariant::Int));
	view.append(DbColumnEx("BCMA_SHORT_SALUTATION", QVariant::String));
	
	s_lstDbViews.append(view);
	view.clear();



	//-----------------------
	// TVIEW_BUS_CM_PHONE
	//-----------------------

	view.m_nViewID		= TVIEW_BUS_CM_PHONE;	
	view.m_nSkipFirstColsInsert = 3;
	view.m_nSkipFirstColsUpdate = 3;
	view.m_strTables="BUS_CM_PHONE";

	//Record set definition
	view.append(DbColumnEx("BCMP_ID", QVariant::Int));
	view.append(DbColumnEx("BCMP_GLOBAL_ID", QVariant::String));
	view.append(DbColumnEx("BCMP_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("BCMP_CONTACT_ID", QVariant::Int));
	view.append(DbColumnEx("BCMP_NAME", QVariant::String));
	view.append(DbColumnEx("BCMP_TYPE_ID", QVariant::Int));
	view.append(DbColumnEx("BCMP_FULLNUMBER", QVariant::String));
	view.append(DbColumnEx("BCMP_LOCAL", QVariant::String));
	view.append(DbColumnEx("BCMP_COUNTRY", QVariant::String));
	view.append(DbColumnEx("BCMP_AREA", QVariant::String));
	view.append(DbColumnEx("BCMP_EXT_ID", QVariant::Int));
	view.append(DbColumnEx("BCMP_IS_DEFAULT", QVariant::Int));
	view.append(DbColumnEx("BCMP_SEARCH", QVariant::String));
	view.append(DbColumnEx("BCMP_COUNTRY_ISO", QVariant::String));

	s_lstDbViews.append(view);
	view.clear();





	//-----------------------
	// TVIEW_BUS_CM_CONTACTS
	//-----------------------

	view.m_nViewID		= TVIEW_BUS_CM_CONTACT;	
	view.m_nSkipFirstColsInsert = 4;
	view.m_nSkipFirstColsUpdate = 4;
	view.m_strTables="BUS_CM_CONTACT";

	//Record set definition
	view.append(DbColumnEx("BCNT_ID", QVariant::Int));
	view.append(DbColumnEx("BCNT_GLOBAL_ID", QVariant::String));
	view.append(DbColumnEx("BCNT_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("BCNT_DAT_CREATED", QVariant::DateTime));
	view.append(DbColumnEx("BCNT_TYPE", QVariant::Int));
	view.append(DbColumnEx("BCNT_DESCRIPTION", QVariant::String,true));
	view.append(DbColumnEx("BCNT_LANGUAGECODE", QVariant::String));
	view.append(DbColumnEx("BCNT_SUPPRESS_MAILING", QVariant::Int));
	view.append(DbColumnEx("BCNT_EXT_ID_STR", QVariant::String));
	view.append(DbColumnEx("BCNT_EXT_APP", QVariant::String));
	view.append(DbColumnEx("BCNT_OLD_CODE", QVariant::String));
	view.append(DbColumnEx("BCNT_OWNER_ID", QVariant::Int));
	view.append(DbColumnEx("BCNT_SHORTNAME", QVariant::String));
	view.append(DbColumnEx("BCNT_LASTNAME", QVariant::String));
	view.append(DbColumnEx("BCNT_FIRSTNAME", QVariant::String));
	view.append(DbColumnEx("BCNT_MIDDLENAME", QVariant::String));
	view.append(DbColumnEx("BCNT_NICKNAME", QVariant::String));
	view.append(DbColumnEx("BCNT_BIRTHDAY", QVariant::Date));
	view.append(DbColumnEx("BCNT_DEPARTMENTNAME", QVariant::String));
	view.append(DbColumnEx("BCNT_ORGANIZATIONNAME", QVariant::String));
	view.append(DbColumnEx("BCNT_ORGANIZATIONNAME_2", QVariant::String));
	view.append(DbColumnEx("BCNT_PROFESSION", QVariant::String));
	view.append(DbColumnEx("BCNT_FUNCTION", QVariant::String));
	view.append(DbColumnEx("BCNT_SEX", QVariant::Int));
	view.append(DbColumnEx("BCNT_FOUNDATIONDATE", QVariant::Date));
	view.append(DbColumnEx("BCNT_LOCATION", QVariant::String));
	view.append(DbColumnEx("BCNT_PICTURE", QVariant::ByteArray));
	view.append(DbColumnEx("BCNT_BIG_PICTURE_ID", QVariant::Int));
	view.append(DbColumnEx("BCNT_LASTNAME_PHONET", QVariant::String));
	view.append(DbColumnEx("BCNT_FIRSTNAME_PHONET", QVariant::String));
	view.append(DbColumnEx("BCNT_ORGANIZATIONNAME_PHONET", QVariant::String));
	view.append(DbColumnEx("BCNT_DO_NOT_SYNC_MAIL", QVariant::Int));
	view.append(DbColumnEx("BCNT_SYNC_MAIL_AS_PRIV", QVariant::Int));
	view.append(DbColumnEx("BCNT_OUT_MAIL_AS_PRIV", QVariant::Int));
	view.append(DbColumnEx("BCNT_LOCATION_PHONET", QVariant::String));
	view.append(DbColumnEx("BCNT_DEFAULT_GRID_VIEW_ID", QVariant::Int));

	s_lstDbViews.append(view);

	//do not clear, I need this view for query below
	//view.clear();

	//-----------------------
	// TVIEW_BUS_CONTACT_FULL
	//-----------------------
	view.m_nViewID				= TVIEW_BUS_CONTACT_FULL;	
	view.m_nSkipFirstColsInsert = 4;
	view.m_nSkipFirstColsUpdate = 4;
	view.m_nSkipLastCols		= 25; //skip all these calculated fields 
	view.m_nSkipLastColsWrite	= 25; //skip all these calculated fields 

	//Record set definition: all calc fields or from other tables-> use for read/write contact in one row
	//all LST and BCT_NAME fields are filled on sever->BusContact ReadData.
	view.append(DbColumnEx("LST_PHONE", DbRecordSet::GetVariantType()));	//defined as TVIEW_BUS_CM_PHONE + BCMT_TYPE_NAME
	view.append(DbColumnEx("LST_ADDRESS", DbRecordSet::GetVariantType()));	//defined as TVIEW_BUS_CM_ADDRESS + LST_TYPES(+BCMT_TYPE_NAME) = TVIEW_BUS_CM_ADDRESS_SELECT
	view.append(DbColumnEx("LST_EMAIL", DbRecordSet::GetVariantType()));	//defined as TVIEW_BUS_CM_EMAIL + BCMT_TYPE_NAME
	view.append(DbColumnEx("LST_INTERNET", DbRecordSet::GetVariantType()));	//defined as TVIEW_BUS_CM_INTERNET + BCMT_TYPE_NAME
	view.append(DbColumnEx("LST_JOURNAL", DbRecordSet::GetVariantType()));	//defined as TVIEW_BUS_CM_JOURNAL_SELECT
	view.append(DbColumnEx("LST_GROUPS", DbRecordSet::GetVariantType()));	//<derived from CLC_GROUPS (TVIEW_BUS_CM_GROUP)> TVIEW_BUS_CM_GROUP_SELECT
	view.append(DbColumnEx("LST_CREDITOR", DbRecordSet::GetVariantType()));	//defined as TVIEW_BUS_CM_CREDITOR
	view.append(DbColumnEx("LST_DEBTOR", DbRecordSet::GetVariantType()));	//defined as TVIEW_BUS_CM_DEBTOR
	view.append(DbColumnEx("LST_PICTURES", DbRecordSet::GetVariantType()));	//defined as TVIEW_BUS_CM_PICTURE+BPIC_PICTURE
	view.append(DbColumnEx("LST_CUSTOM_FIELDS", DbRecordSet::GetVariantType()));	//defined as TVIEW_BUS_CUSTOM_FIELDS_DATA
	view.append(DbColumnEx("BCNT_NAME", QVariant::String));
	//not readed, used for import/export
	view.append(DbColumnEx("STATUS_CODE", QVariant::Int));
	view.append(DbColumnEx("STATUS_TEXT", QVariant::String));
	view.append(DbColumnEx("CLC_GROUPS", QVariant::String));				//groups are separated with semicolon ;
	//used to write new big picture, when reading it is left empty as small 'preview' pic is already loaded with main contact record
	view.append(DbColumnEx("BPIC_PICTURE", QVariant::ByteArray));			//new issue: import pictures->from QCW
	//-------------- groups --------------------not readed used only to display in contact/group grid
	view.append(DbColumnEx("BGCN_ID", QVariant::Int));						//id of group/cnt assoc
	view.append(DbColumnEx("BGCN_ITEM_ID", QVariant::Int));					//group id
	view.append(DbColumnEx("BGCN_CONTACT_ID", QVariant::Int));				//cont id (to save time)
	view.append(DbColumnEx("BGCN_CMCA_VALID_FROM", QVariant::Date));
	view.append(DbColumnEx("BGCN_CMCA_VALID_TO", QVariant::Date));
	//-------------- debtor --------------------used to write debtor when import
	view.append(DbColumnEx("BCMD_DEBTORCODE", QVariant::String));
	view.append(DbColumnEx("BCMD_DEBTORACCOUNT", QVariant::String));
	view.append(DbColumnEx("BCMD_CUSTOMERCODE", QVariant::String));
	view.append(DbColumnEx("BCMD_DESCRIPTION", QVariant::String, true));
	view.append(DbColumnEx("BCMD_INVOICE_TEXT", QVariant::String, true));

	s_lstDbViews.append(view);
	view.clear();


	//-----------------------
	// TVIEW_BUS_CM_CONTACT_SELECT_PHONETS
	//-----------------------

	view.m_nViewID		= TVIEW_BUS_CM_CONTACT_SELECT_PHONETS;	
	view.m_nSkipFirstColsInsert = 3;
	view.m_nSkipFirstColsUpdate = 3;
	view.m_strTables="BUS_CM_CONTACT";

	//Record set definition
	view.append(DbColumnEx("BCNT_ID", QVariant::Int));
	view.append(DbColumnEx("BCNT_GLOBAL_ID", QVariant::String));
	view.append(DbColumnEx("BCNT_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("BCNT_TYPE", QVariant::Int));
	view.append(DbColumnEx("BCNT_LASTNAME", QVariant::String));
	view.append(DbColumnEx("BCNT_FIRSTNAME", QVariant::String));
	view.append(DbColumnEx("BCNT_ORGANIZATIONNAME", QVariant::String));
	view.append(DbColumnEx("BCNT_LASTNAME_PHONET", QVariant::String));
	view.append(DbColumnEx("BCNT_FIRSTNAME_PHONET", QVariant::String));
	view.append(DbColumnEx("BCNT_ORGANIZATIONNAME_PHONET", QVariant::String));
	view.append(DbColumnEx("BCNT_LOCATION_PHONET", QVariant::String));

	s_lstDbViews.append(view);
	view.clear();


	//-----------------------
	// TVIEW_BUS_CM_CONTACT_SELECT_FUI
	//-----------------------

	view.m_nViewID		= TVIEW_BUS_CM_CONTACT_SELECT_FUI;	
	view.m_nSkipFirstColsInsert = 3;
	view.m_nSkipFirstColsUpdate = 3;
	view.m_nSkipLastCols =2;
	view.m_strTables="BUS_CM_CONTACT";

	//Record set definition
	view.append(DbColumnEx("BCNT_ID", QVariant::Int));
	view.append(DbColumnEx("BCNT_GLOBAL_ID", QVariant::String));
	view.append(DbColumnEx("BCNT_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("BCNT_TYPE", QVariant::Int));
	view.append(DbColumnEx("BCNT_DESCRIPTION", QVariant::String,true));
	view.append(DbColumnEx("BCNT_LANGUAGECODE", QVariant::String));
	view.append(DbColumnEx("BCNT_SUPPRESS_MAILING", QVariant::Int));
	view.append(DbColumnEx("BCNT_EXT_ID_STR", QVariant::String));
	view.append(DbColumnEx("BCNT_EXT_APP", QVariant::String));
	view.append(DbColumnEx("BCNT_OLD_CODE", QVariant::String));
	view.append(DbColumnEx("BCNT_OWNER_ID", QVariant::Int));
	view.append(DbColumnEx("BCNT_SHORTNAME", QVariant::String));
	view.append(DbColumnEx("BCNT_LASTNAME", QVariant::String));
	view.append(DbColumnEx("BCNT_FIRSTNAME", QVariant::String));
	view.append(DbColumnEx("BCNT_MIDDLENAME", QVariant::String));
	view.append(DbColumnEx("BCNT_NICKNAME", QVariant::String));
	view.append(DbColumnEx("BCNT_BIRTHDAY", QVariant::Date));
	view.append(DbColumnEx("BCNT_DEPARTMENTNAME", QVariant::String));
	view.append(DbColumnEx("BCNT_ORGANIZATIONNAME", QVariant::String));
	view.append(DbColumnEx("BCNT_ORGANIZATIONNAME_2", QVariant::String));
	view.append(DbColumnEx("BCNT_PROFESSION", QVariant::String));
	view.append(DbColumnEx("BCNT_FUNCTION", QVariant::String));
	view.append(DbColumnEx("BCNT_SEX", QVariant::Int));
	view.append(DbColumnEx("BCNT_FOUNDATIONDATE", QVariant::Date));
	view.append(DbColumnEx("BCNT_LOCATION", QVariant::String));
	view.append(DbColumnEx("BCNT_PICTURE", QVariant::ByteArray));
	view.append(DbColumnEx("BCNT_BIG_PICTURE_ID", QVariant::Int));
	view.append(DbColumnEx("BCNT_LASTNAME_PHONET", QVariant::String));
	view.append(DbColumnEx("BCNT_FIRSTNAME_PHONET", QVariant::String));
	view.append(DbColumnEx("BCNT_ORGANIZATIONNAME_PHONET", QVariant::String));
	view.append(DbColumnEx("BCNT_DO_NOT_SYNC_MAIL", QVariant::Int));
	view.append(DbColumnEx("BCNT_SYNC_MAIL_AS_PRIV", QVariant::Int));
	view.append(DbColumnEx("BCNT_OUT_MAIL_AS_PRIV", QVariant::Int));
	view.append(DbColumnEx("BCNT_LOCATION_PHONET", QVariant::String));
	view.append(DbColumnEx("BCNT_DEFAULT_GRID_VIEW_ID", QVariant::Int));
	view.append(DbColumnEx("BPER_FIRST_NAME", QVariant::String));
	view.append(DbColumnEx("BPER_LAST_NAME", QVariant::String));
	view.append(DbColumnEx("BDEPT_CODE", QVariant::String));
	view.append(DbColumnEx("BDEPT_NAME", QVariant::String));

	view.m_strSelectSQL="SELECT "+getSQLColumnsFromView(view)+" FROM BUS_CM_CONTACT LEFT OUTER JOIN BUS_PERSON ON BCNT_OWNER_ID=BPER_ID";
	s_lstDbViews.append(view);
	view.clear();


	//-----------------------
	// TVIEW_BUS_CONTACT_FULL_TEXT_IMPORT
	//
	//  Do not change the order of the fields!!! OR else ...
	//-----------------------
	view.m_nViewID		= TVIEW_BUS_CONTACT_FULL_TEXT_IMPORT;	
	view.m_strTables="BUS_CM_CONTACT";
	
	view.append(DbColumnEx("CLC_IS_ADD_ON", QVariant::Int));	//"Follow-up" record
	
	//---------contact--------------
	view.append(DbColumnEx("BCNT_FIRSTNAME", QVariant::String));
	view.append(DbColumnEx("BCNT_LASTNAME", QVariant::String));
	view.append(DbColumnEx("BCNT_MIDDLENAME", QVariant::String));
	view.append(DbColumnEx("BCNT_NICKNAME", QVariant::String));
	view.append(DbColumnEx("BCNT_BIRTHDAY", QVariant::Date));
	view.append(DbColumnEx("BCNT_SEX", QVariant::Int));
	view.append(DbColumnEx("BCNT_DEPARTMENTNAME", QVariant::String));
	view.append(DbColumnEx("BCNT_PROFESSION", QVariant::String));
	view.append(DbColumnEx("BCNT_FUNCTION", QVariant::String));
	view.append(DbColumnEx("BCNT_ORGANIZATIONNAME", QVariant::String));
	view.append(DbColumnEx("BCNT_FOUNDATIONDATE", QVariant::Date));
	view.append(DbColumnEx("BCNT_DESCRIPTION", QVariant::String, true));
	view.append(DbColumnEx("BCNT_LANGUAGECODE", QVariant::String));
	view.append(DbColumnEx("BCNT_SUPPRESS_MAILING", QVariant::Int));
	view.append(DbColumnEx("BCNT_OLD_CODE", QVariant::String));		//external code

	//---------address--------------
	view.append(DbColumnEx("BCMA_FORMATEDADDRESS", QVariant::String,true));
	view.append(DbColumnEx("BCMA_FIRSTNAME", QVariant::String));
	view.append(DbColumnEx("BCMA_LASTNAME", QVariant::String));
	view.append(DbColumnEx("BCMA_MIDDLENAME", QVariant::String));
	view.append(DbColumnEx("BCMA_TITLE", QVariant::String));
	view.append(DbColumnEx("BCMA_SALUTATION", QVariant::String));
	view.append(DbColumnEx("BCMA_ORGANIZATIONNAME", QVariant::String));
	view.append(DbColumnEx("BCMA_ORGANIZATIONNAME_2", QVariant::String));
	view.append(DbColumnEx("BCMA_STREET", QVariant::String));
	view.append(DbColumnEx("BCMA_ZIP", QVariant::String));
	view.append(DbColumnEx("BCMA_CITY", QVariant::String));
	view.append(DbColumnEx("BCMA_COUNTRY_CODE", QVariant::String));
	view.append(DbColumnEx("BCMA_COUNTRY_NAME", QVariant::String));
	view.append(DbColumnEx("BCMA_REGION", QVariant::String));
	view.append(DbColumnEx("BCMA_POBOX", QVariant::String));
	view.append(DbColumnEx("BCMA_POBOXZIP", QVariant::String));
	view.append(DbColumnEx("BCMA_OFFICECODE", QVariant::String));

	//---------calculated fields--------------
	view.append(DbColumnEx("CLC_EMAIL_ADDRESS", QVariant::String));
	view.append(DbColumnEx("CLC_EMAIL_ADDRESS_DESC", QVariant::String));
	view.append(DbColumnEx("CLC_WEB_ADDRESS", QVariant::String));
	view.append(DbColumnEx("CLC_WEB_ADDRESS_DESC", QVariant::String));
	view.append(DbColumnEx("CLC_PHONE_BUSINESS_CENTRAL", QVariant::String));
	view.append(DbColumnEx("CLC_PHONE_BUSINESS_DIRECT", QVariant::String));
	view.append(DbColumnEx("CLC_PHONE_MOBILE", QVariant::String));
	view.append(DbColumnEx("CLC_PHONE_FAX", QVariant::String));
	view.append(DbColumnEx("CLC_PHONE_PRIVATE", QVariant::String));
	view.append(DbColumnEx("CLC_PHONE_PRIVATE_MOBILE", QVariant::String));
	view.append(DbColumnEx("CLC_PHONE_SKYPE", QVariant::String));
	view.append(DbColumnEx("CLC_PHONE_GENERIC",		 QVariant::String));
	view.append(DbColumnEx("CLC_PHONE_GENERIC_NAME", QVariant::String));

	//-------------- journal --------------------
	view.append(DbColumnEx("BCMJ_PERSON_CODE", QVariant::String));
	view.append(DbColumnEx("BCMJ_DATE", QVariant::Date));
	view.append(DbColumnEx("BCMJ_TEXT", QVariant::String, true));
	view.append(DbColumnEx("BCMJ_IS_PRIVATE", QVariant::Int));

	//
	view.append(DbColumnEx("CLC_GROUPS", QVariant::String));

	//-------------- debtor --------------------
	view.append(DbColumnEx("BCMD_DEBTORCODE", QVariant::String));
	view.append(DbColumnEx("BCMD_DEBTORACCOUNT", QVariant::String));
	view.append(DbColumnEx("BCMD_CUSTOMERCODE", QVariant::String));
	view.append(DbColumnEx("BCMD_DESCRIPTION", QVariant::String,true));
	view.append(DbColumnEx("BCMD_INVOICE_TEXT", QVariant::String, true));

	// bus person
	view.append(DbColumnEx("BPER_CODE", QVariant::String));
	view.append(DbColumnEx("BPER_CODE_EXT", QVariant::String));
	view.append(DbColumnEx("BPER_INITIALS", QVariant::String));
	view.append(DbColumnEx("BPER_ACTIVE_FLAG", QVariant::Int));
	view.append(DbColumnEx("BPER_DATE_ENTERED", QVariant::Date));
	view.append(DbColumnEx("BPER_DATE_LEFT", QVariant::Date));
	view.append(DbColumnEx("BPER_ORGANIZATION_CODE", QVariant::String));
	view.append(DbColumnEx("BPER_DEPARTMENT_CODE", QVariant::String));
	view.append(DbColumnEx("BPER_INSURANCE_ID", QVariant::String));
	view.append(DbColumnEx("BPER_OCCUPATION", QVariant::Double));

	// unclear - not defined in doc
	view.append(DbColumnEx("BCNT_ORGANIZATIONNAME_2", QVariant::String));
	s_lstDbViews.append(view);
	view.clear();




	//-----------------------
	// TVIEW_BUS_CM_INTERNET
	//-----------------------

	view.m_nViewID		= TVIEW_BUS_CM_INTERNET;	
	view.m_nSkipFirstColsInsert = 3;
	view.m_nSkipFirstColsUpdate = 3;
	view.m_strTables="BUS_CM_INTERNET";

	//Record set definition
	view.append(DbColumnEx("BCMI_ID", QVariant::Int));
	view.append(DbColumnEx("BCMI_GLOBAL_ID", QVariant::String));
	view.append(DbColumnEx("BCMI_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("BCMI_CONTACT_ID", QVariant::Int));
	view.append(DbColumnEx("BCMI_TYPE_ID", QVariant::Int));
	view.append(DbColumnEx("BCMI_NAME", QVariant::String));
	view.append(DbColumnEx("BCMI_ADDRESS", QVariant::String));
	view.append(DbColumnEx("BCMI_DESCRIPTION", QVariant::String,true));
	view.append(DbColumnEx("BCMI_EXT_ID", QVariant::Int));
	view.append(DbColumnEx("BCMI_IS_DEFAULT", QVariant::Int));

	s_lstDbViews.append(view);
	view.clear();


	//-----------------------
	// TVIEW_BUS_CM_ADDRESS_SCHEMAS
	//-----------------------

	view.m_nViewID		= TVIEW_BUS_CM_ADDRESS_SCHEMAS;	
	view.m_nSkipFirstColsInsert = 3;
	view.m_nSkipFirstColsUpdate = 3;
	view.m_strTables="BUS_CM_ADDRESS_SCHEMAS";

	//Record set definition
	view.append(DbColumnEx("BCMAS_ID", QVariant::Int));
	view.append(DbColumnEx("BCMAS_GLOBAL_ID", QVariant::String));
	view.append(DbColumnEx("BCMAS_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("BCMAS_SCHEMA_NAME", QVariant::String, true));
	view.append(DbColumnEx("BCMAS_SCHEMA", QVariant::String,true));

	s_lstDbViews.append(view);
	view.clear();





	//-----------------------
	// TVIEW_BUS_CM_TYPES
	//-----------------------

	view.m_nViewID		= TVIEW_BUS_CM_TYPES;	
	view.m_nSkipFirstColsInsert = 3;
	view.m_nSkipFirstColsUpdate = 3;
	view.m_strTables="BUS_CM_TYPES";

	//Record set definition
	view.append(DbColumnEx("BCMT_ID",					QVariant::Int));
	view.append(DbColumnEx("BCMT_GLOBAL_ID",			QVariant::String));
	view.append(DbColumnEx("BCMT_DAT_LAST_MODIFIED",	QVariant::DateTime));
	view.append(DbColumnEx("BCMT_TYPE_NAME",			QVariant::String));
	view.append(DbColumnEx("BCMT_ENTITY_TYPE",			QVariant::Int));
	view.append(DbColumnEx("BCMT_IS_DEFAULT",			QVariant::Int));
	view.append(DbColumnEx("BCMT_SYSTEM_TYPE",			QVariant::Int));
	view.append(DbColumnEx("BCMT_IS_FORMATTING_ALLOWED", QVariant::Int));
	view.append(DbColumnEx("BCMT_COLOR",				QVariant::String));
	view.append(DbColumnEx("BCMT_PICTURE",				QVariant::ByteArray));
	view.append(DbColumnEx("BCMT_EMAIL_TEMPLATE_ID",	QVariant::Int));


	//Add extra column info
	s_lstDbViews.append(view);
	view.clear();
	
	//-----------------------
	// TVIEW_BUS_CM_SUBSET_SELECTION
	//-----------------------
	view.m_nViewID		= TVIEW_BUS_CM_SUBSET_SELECTION;	
	view.m_strTables=	"BUS_CM_CONTACT";
	view.append(DbColumnEx("ID", QVariant::Int));
	view.append(DbColumnEx("NAME", QVariant::String));
	s_lstDbViews.append(view);
	view.clear();


	//-----------------------
	// TVIEW_BUS_CONTACT_SELECTION
	//-----------------------
	view.m_nViewID		= TVIEW_BUS_CONTACT_SELECTION;	
	view.m_nSkipLastCols = 1;
	view.m_strTables="BUS_CM_CONTACT";
	view.append(DbColumnEx("BCNT_ID", QString("Contact_ID"), QVariant::Int,false,"Contact ID"));
	view.append(DbColumnEx("BCNT_LASTNAME", QString("LastName"),QVariant::String,false,"Last Name"));
	view.append(DbColumnEx("BCNT_FIRSTNAME", QString("FirstName"),QVariant::String,false,"First Name"));
	view.append(DbColumnEx("BCNT_ORGANIZATIONNAME", QString("OrganizationName"),QVariant::String,false,"Organization Name"));
	view.append(DbColumnEx("BCNT_TYPE", QString("Type"),QVariant::Int,false,"Contact Type: 1-person, 2-organization"));
	view.append(DbColumnEx("BCNT_NAME", QString("CalculatedName"),QVariant::String,false,"Name+Organization"));
	
	//Add extra column info
	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// TVIEW_BUS_CONTACT_SELECTION_WITH_ACTIVE_GROUP_PERIOD
	//-----------------------
	view.m_nViewID		= TVIEW_BUS_CONTACT_SELECTION_WITH_ACTIVE_GROUP_PERIOD;	
	view.m_nSkipLastCols = 1;
	view.m_strTables="BUS_CM_CONTACT";
	view.append(DbColumnEx("BCNT_ID", QString("Contact_ID"), QVariant::Int,false,"Contact ID"));
	view.append(DbColumnEx("BCNT_LASTNAME", QString("LastName"),QVariant::String,false,"Last Name"));
	view.append(DbColumnEx("BCNT_FIRSTNAME", QString("FirstName"),QVariant::String,false,"First Name"));
	view.append(DbColumnEx("BCNT_ORGANIZATIONNAME", QString("OrganizationName"),QVariant::String,false,"Organization Name"));
	view.append(DbColumnEx("BCNT_TYPE", QString("Type"),QVariant::Int,false,"Contact Type: 1-person, 2-organization"));
	view.append(DbColumnEx("BGCN_CMCA_VALID_FROM",QVariant::Date));
	view.append(DbColumnEx("BGCN_CMCA_VALID_TO", QVariant::Date));
	view.append(DbColumnEx("BCNT_NAME", QString("CalculatedName"),QVariant::String,false,"Name+Organization"));
	

	//Add extra column info
	s_lstDbViews.append(view);
	view.clear();
	
	
	//-----------------------
	// TVIEW_BUS_CONTACT_SELECTION_WITH_PICS
	//-----------------------
	view.m_nViewID		= TVIEW_BUS_CONTACT_SELECTION_WITH_PICS;	
	view.m_nSkipLastCols = 1;
	view.m_strTables="BUS_CM_CONTACT";
	view.append(DbColumnEx("BCNT_ID", QString("Contact_ID"), QVariant::Int,false,"Contact ID"));
	view.append(DbColumnEx("BCNT_LASTNAME", QString("LastName"),QVariant::String,false,"Last Name"));
	view.append(DbColumnEx("BCNT_FIRSTNAME", QString("FirstName"),QVariant::String,false,"First Name"));
	view.append(DbColumnEx("BCNT_ORGANIZATIONNAME", QString("OrganizationName"),QVariant::String,false,"Organization Name"));
	view.append(DbColumnEx("BCNT_TYPE", QString("Type"),QVariant::Int,false,"Contact Type: 1-person, 2-organization"));
	view.append(DbColumnEx("BCNT_PICTURE", QString("ContactPicture"),QVariant::ByteArray,false,"Picture"));
	view.append(DbColumnEx("BCNT_NAME", QString("CalculatedName"),QVariant::String,false,"Name+Organization"));
	

	//Add extra column info
	s_lstDbViews.append(view);
	view.clear();
	



	//-----------------------
	// TVIEW_BUS_CM_ADDRESS_TYPES
	//-----------------------

	view.m_nViewID		= TVIEW_BUS_CM_ADDRESS_TYPES;	
	view.m_nSkipFirstColsInsert = 3;
	view.m_nSkipFirstColsUpdate = 3;
	view.m_strTables="BUS_CM_ADDRESS_TYPES";

	//Record set definition
	view.append(DbColumnEx("BCMAT_ID", QVariant::Int));
	view.append(DbColumnEx("BCMAT_GLOBAL_ID", QVariant::String));
	view.append(DbColumnEx("BCMAT_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("BCMAT_ADDRESS_ID", QVariant::Int));
	view.append(DbColumnEx("BCMAT_TYPE_ID", QVariant::Int));

	//Add extra column info
	s_lstDbViews.append(view);
	view.clear();


	


	//-----------------------
	// MVIEW_DATE_SELECTOR
	//-----------------------

	view.m_nViewID		= MVIEW_DATE_SELECTOR;	
	view.m_strTables="BUS_CM_CONTACT";
	view.append(DbColumnEx("TYPE", QVariant::Int));
	view.append(DbColumnEx("DATE", QVariant::Date));
	//Add extra column info
	s_lstDbViews.append(view);
	view.clear();
	
	//-----------------------
	// TVIEW_ORG_DEPT_REPORT
	// Organization departments report view.
	//-----------------------
	view.m_nViewID		= TVIEW_ORG_DEPT_REPORT;
	view.m_nSkipFirstColsInsert = 3;	//skip Id & global
	view.m_nSkipFirstColsUpdate = 3;
	view.m_strTables="BUS_ORGANIZATIONS, BUS_DEPARTMENTS"; //if more then one table separate by comma "TABLE,TABLE2"
	view.append(DbColumnEx("BORG_CODE",				QVariant::String));
	view.append(DbColumnEx("BORG_NAME",				QVariant::String));
	view.append(DbColumnEx("BORG_DESCRIPTION",		QVariant::String, true));
	view.append(DbColumnEx("BDEPT_CODE",			QVariant::String));
	view.append(DbColumnEx("BDEPT_NAME",			QVariant::String));
	view.append(DbColumnEx("BDEPT_COST_CENTER_ID",	QVariant::Int));
	view.append(DbColumnEx("BDEPT_DESCRIPTION",		QVariant::String,true));

	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// TVIEW_RPT_TABLE_VIEW
	// Report selection wizard tree view.
	//-----------------------
	view.m_nViewID		= TVIEW_RPT_TABLE_VIEW;
	view.m_nSkipFirstColsInsert = 3;	//skip Id & global
	view.m_nSkipFirstColsUpdate = 3;
	view.m_strTables=""; //if more then one table separate by comma "TABLE,TABLE2"
	view.append(DbColumnEx("RPT_ID",			QVariant::Int));
	view.append(DbColumnEx("RPT_CODE",			QVariant::String));
	view.append(DbColumnEx("RPT_LEVEL",		QVariant::Int));
	view.append(DbColumnEx("RPT_PARENT",		QVariant::Int));
	view.append(DbColumnEx("RPT_HASCHILDREN",	QVariant::Int));
	view.append(DbColumnEx("RPT_ICON",			QVariant::String));
	view.append(DbColumnEx("RPT_MAINPARENT_ID",QVariant::Int));
	view.append(DbColumnEx("RPT_NAME",			QVariant::String));
	view.append(DbColumnEx("RPT_STYLE",		QVariant::Int));

	s_lstDbViews.append(view);
	view.clear();
	
	//-----------------------
	// TVIEW_BUS_VOICECALLS
	//-----------------------
	view.m_nViewID		= TVIEW_BUS_VOICECALLS;
	view.m_nSkipFirstColsInsert = 3;	//skip Id & global
	view.m_nSkipFirstColsUpdate = 3;
	view.m_strTables="BUS_VOICECALLS";
	view.append(DbColumnEx("BVC_ID",				QVariant::Int));
	view.append(DbColumnEx("BVC_GLOBAL_ID",			QVariant::String));
	view.append(DbColumnEx("BVC_DAT_LAST_MODIFIED",	QVariant::DateTime));
	view.append(DbColumnEx("BVC_COMM_ENTITY_ID",	QVariant::Int));
	view.append(DbColumnEx("BVC_START",			QVariant::DateTime));
	view.append(DbColumnEx("BVC_END",			QVariant::DateTime));
	view.append(DbColumnEx("BVC_DURATION",		QVariant::Int));
	view.append(DbColumnEx("BVC_CALLER_ID",		QVariant::String));
	view.append(DbColumnEx("BVC_CALL_ID",		QVariant::Int));
	view.append(DbColumnEx("BVC_DIRECTION",		QVariant::Int));
	view.append(DbColumnEx("BVC_DESCRIPTION",	QVariant::String,true));
	view.append(DbColumnEx("BVC_INTERFACE_ID",	QVariant::Int));
	view.append(DbColumnEx("BVC_PROJECT_ID",	QVariant::Int));
	view.append(DbColumnEx("BVC_CONTACT_ID",	QVariant::Int));
	view.append(DbColumnEx("BVC_CALL_STATUS",	QVariant::Int));
	view.append(DbColumnEx("BVC_DEVICE_CODE",	QVariant::Int));

	s_lstDbViews.append(view);
	view.clear();



	//-----------------------
	// TVIEW_CE_EVENT
	//-----------------------
	view.m_strTables="CE_EVENT";
	view.m_nSkipFirstColsInsert = 3;	//skip Id & global
	view.m_nSkipFirstColsUpdate = 3;
	view.append(DbColumnEx("CEV_ID",	QVariant::Int));
	view.append(DbColumnEx("CEV_GLOBAL_ID",	QVariant::String));
	view.append(DbColumnEx("CEV_DAT_LAST_MODIFIED",	QVariant::DateTime));
	view.append(DbColumnEx("CEV_LOGGE_USER_ID",	QVariant::Int));
	view.append(DbColumnEx("CEV_ENTITY_ID",	QVariant::Int));
	view.append(DbColumnEx("CEV_EVENT_TYPE_ID",	QVariant::Int));
	view.append(DbColumnEx("CEV_DESCRIPTION",	QVariant::String,true));
	view.append(DbColumnEx("CEV_DAT_EVENT_OCCURED",	QVariant::DateTime));
	view.m_nViewID		= TVIEW_CE_EVENT;

	s_lstDbViews.append(view);
	view.clear();


	//-----------------------
	// TVIEW_CE_COMM_ENTITY
	//-----------------------
	view.m_nViewID		= TVIEW_CE_COMM_ENTITY;
	view.m_nSkipFirstColsInsert = 4;	//skip Id & global
	view.m_nSkipFirstColsUpdate = 4;
	view.m_strTables="CE_COMM_ENTITY";
	view.append(DbColumnEx("CENT_ID",					QVariant::Int));
	view.append(DbColumnEx("CENT_GLOBAL_ID",			QVariant::String));
	view.append(DbColumnEx("CENT_DAT_LAST_MODIFIED",	QVariant::DateTime));
	view.append(DbColumnEx("CENT_DAT_CREATED",			QVariant::DateTime));
	view.append(DbColumnEx("CENT_CE_TYPE_ID",			QVariant::Int));
	view.append(DbColumnEx("CENT_SYSTEM_TYPE_ID",		QVariant::Int));
	view.append(DbColumnEx("CENT_OWNER_ID",				QVariant::Int));
	view.append(DbColumnEx("CENT_TASK_ID",				QVariant::Int));
	view.append(DbColumnEx("CENT_TOOLTIP",				QVariant::String));
	//view.append(DbColumnEx("CENT_IS_PRIVATE",			QVariant::Int));

	s_lstDbViews.append(view);
	view.clear();

	
	//-----------------------
	// TVIEW_CE_PROJECT_LINK
	//-----------------------
	view.m_nViewID		= TVIEW_CE_PROJECT_LINK;
	view.m_nSkipFirstColsInsert	= 3;
	view.m_nSkipFirstColsUpdate	= 3;
	view.m_strTables="CE_PROJECT_LINK";

	//Record set definition
	view.append(DbColumnEx("CELP_ID", QVariant::Int));
	view.append(DbColumnEx("CELP_GLOBAL_ID", QVariant::String));
	view.append(DbColumnEx("CELP_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("CELP_PROJECT_ID", QVariant::Int));
	view.append(DbColumnEx("CELP_COMM_ENTITY_ID", QVariant::Int));
	view.append(DbColumnEx("CELP_LINK_ROLE_ID", QVariant::Int));

	s_lstDbViews.append(view);
	view.clear();


	//-----------------------
	// CE_TYPE
	//-----------------------
	view.m_nSkipFirstColsInsert = 3;	//skip Id & global
	view.m_nSkipFirstColsUpdate = 3;

	view.m_strTables="CE_TYPE";
	view.m_nViewID		= TVIEW_CE_TYPE;
	view.append(DbColumnEx("CET_ID",	QVariant::Int));
	view.append(DbColumnEx("CET_GLOBAL_ID",	QVariant::String));
	view.append(DbColumnEx("CET_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("CET_CODE", QVariant::String));
	view.append(DbColumnEx("CET_LEVEL", QVariant::Int));
	view.append(DbColumnEx("CET_PARENT", QVariant::Int));
	view.append(DbColumnEx("CET_HASCHILDREN", QVariant::Int));
	view.append(DbColumnEx("CET_ICON", QVariant::String));
	view.append(DbColumnEx("CET_MAINPARENT_ID", QVariant::Int));
	view.append(DbColumnEx("CET_NAME", QVariant::String));
	view.append(DbColumnEx("CET_STYLE", QVariant::Int));
	view.append(DbColumnEx("CET_TYPE_NAME",	QVariant::String));
	view.append(DbColumnEx("CET_COMM_ENTITY_TYPE_ID", QVariant::Int));
	view.append(DbColumnEx("CET_DESCRIPTION", QVariant::String,true));
	view.append(DbColumnEx("CET_MAPPED_NAME", QVariant::String));
	view.append(DbColumnEx("CET_ICON_BINARY", QVariant::ByteArray));

	
	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// CE_TYPE
	//-----------------------
	view.m_nSkipFirstColsInsert = 3;	//skip Id & global
	view.m_nSkipFirstColsUpdate = 3;
	view.m_nSkipLastCols = 1;
	view.m_nSkipLastColsWrite = 1;

	view.m_strTables="CE_TYPE";
	view.m_nViewID		= TVIEW_CE_TYPE_SELECT;
	view.append(DbColumnEx("CET_ID",	QVariant::Int));
	view.append(DbColumnEx("CET_GLOBAL_ID",	QVariant::String));
	view.append(DbColumnEx("CET_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("CET_CODE", QVariant::String));
	view.append(DbColumnEx("CET_LEVEL", QVariant::Int));
	view.append(DbColumnEx("CET_PARENT", QVariant::Int));
	view.append(DbColumnEx("CET_HASCHILDREN", QVariant::Int));
	view.append(DbColumnEx("CET_ICON", QVariant::String));
	view.append(DbColumnEx("CET_MAINPARENT_ID", QVariant::Int));
	view.append(DbColumnEx("CET_NAME", QVariant::String));
	view.append(DbColumnEx("CET_STYLE", QVariant::Int));
	view.append(DbColumnEx("CET_TYPE_NAME",	QVariant::String));
	view.append(DbColumnEx("CET_COMM_ENTITY_TYPE_ID", QVariant::Int));
	view.append(DbColumnEx("CET_DESCRIPTION", QVariant::String,true));
	view.append(DbColumnEx("CET_MAPPED_NAME", QVariant::String));
	view.append(DbColumnEx("CET_ICON_BINARY", QVariant::ByteArray));
	view.append(DbColumnEx("CET_EXPANDED", QVariant::Int));

	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// CE_EVENT_TYPE
	//-----------------------
	view.m_strTables="CE_EVENT_TYPE";
	view.m_nSkipFirstColsInsert = 3;	//skip Id & global
	view.m_nSkipFirstColsUpdate = 3;
	view.append(DbColumnEx("CEVT_ID",	QVariant::Int));
	view.append(DbColumnEx("CEVT_GLOBAL_ID",	QVariant::String));
	view.append(DbColumnEx("CEVT_DAT_LAST_MODIFIED",	QVariant::DateTime));
	view.append(DbColumnEx("CEVT_CODE",	QVariant::String));
	view.append(DbColumnEx("CEVT_NAME",	QVariant::String));
	view.append(DbColumnEx("CEVT_SYSTEM_TYPE_ID",	QVariant::Int));
	view.append(DbColumnEx("CEVT_STATUS_ID",	QVariant::Int));
	view.append(DbColumnEx("CEVT_PRIORITY",	QVariant::Int));
	view.append(DbColumnEx("CEVT_DIRECTION",	QVariant::Int));
	view.append(DbColumnEx("CEVT_DESCRIPTION",	QVariant::String,true));
	view.m_nViewID		= TVIEW_CE_EVENT_TYPE;

	s_lstDbViews.append(view);
	view.clear();


	//-----------------------
	// CE_CONTACT_LINK
	//-----------------------
	view.m_nViewID		= TVIEW_CE_CONTACT_LINK;
	view.m_nSkipFirstColsInsert	= 3;
	view.m_nSkipFirstColsUpdate	= 3;
	view.m_strTables="CE_CONTACT_LINK";

	//Record set definition
	view.append(DbColumnEx("CELC_ID", QVariant::Int));
	view.append(DbColumnEx("CELC_GLOBAL_ID", QVariant::String));
	view.append(DbColumnEx("CELC_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("CELC_CONTACT_ID", QVariant::Int));
	view.append(DbColumnEx("CELC_COMM_ENTITY_ID", QVariant::Int));
	view.append(DbColumnEx("CELC_LINK_ROLE_ID", QVariant::Int));

	s_lstDbViews.append(view);
	view.clear();


	//-----------------------
	// CE_CHARGES_LINK
	//-----------------------
	view.m_nViewID		= TVIEW_CE_CHARGES_LINK;
	view.m_nSkipFirstColsInsert	= 3;
	view.m_nSkipFirstColsUpdate	= 3;
	view.m_strTables="CE_CHARGES_LINK";

	//Record set definition
	view.append(DbColumnEx("CECH_ID", QVariant::Int));
	view.append(DbColumnEx("CECH_GLOBAL_ID", QVariant::String));
	view.append(DbColumnEx("CECH_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("CECH_CHARGE_ID", QVariant::Int));
	view.append(DbColumnEx("CECH_COMM_ENTITY_ID", QVariant::Int));

	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// BUS_PROJECTS
	//-----------------------
	view.m_nViewID		= TVIEW_BUS_PROJECT;
	view.m_nSkipFirstColsInsert = 4;	//skip Id & global
	view.m_nSkipFirstColsUpdate = 4;
	view.m_strTables="BUS_PROJECTS";

	//Record set definition
	view.append(DbColumnEx("BUSP_ID", QVariant::Int));
	view.append(DbColumnEx("BUSP_GLOBAL_ID", QVariant::String));
	view.append(DbColumnEx("BUSP_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("BUSP_DAT_CREATED", QVariant::DateTime));
	view.append(DbColumnEx("BUSP_CODE", QVariant::String));
	view.append(DbColumnEx("BUSP_LEVEL", QVariant::Int));
	view.append(DbColumnEx("BUSP_PARENT", QVariant::Int));
	view.append(DbColumnEx("BUSP_HASCHILDREN", QVariant::Int));
	view.append(DbColumnEx("BUSP_ICON", QVariant::String));
	view.append(DbColumnEx("BUSP_MAINPARENT_ID", QVariant::Int));
	view.append(DbColumnEx("BUSP_NAME", QVariant::String));
	view.append(DbColumnEx("BUSP_STYLE", QVariant::Int));
	view.append(DbColumnEx("BUSP_START_DATE", QVariant::Date));
	view.append(DbColumnEx("BUSP_DEADLINE_DATE", QVariant::Date));
	view.append(DbColumnEx("BUSP_COMPLETION_DATE", QVariant::Date));
	view.append(DbColumnEx("BUSP_LATEST_COMPLETION_DATE", QVariant::Date));
	view.append(DbColumnEx("BUSP_OFFER_DATE", QVariant::Date));
	view.append(DbColumnEx("BUSP_OFFER_VALID_DATE", QVariant::Date));
	view.append(DbColumnEx("BUSP_ORDER_DATE", QVariant::Date));
	view.append(DbColumnEx("BUSP_CONTRACT_DATE", QVariant::Date));
	view.append(DbColumnEx("BUSP_LEADER_ID", QVariant::Int));
	view.append(DbColumnEx("BUSP_RESPONSIBLE_ID", QVariant::Int));
	view.append(DbColumnEx("BUSP_DESCRIPTION", QVariant::String,true));
	view.append(DbColumnEx("BUSP_ACTIVE_FLAG", QVariant::Int));
	view.append(DbColumnEx("BUSP_TEMPLATE_FLAG", QVariant::Int));
	view.append(DbColumnEx("BUSP_LOCATION", QVariant::String,true));
	view.append(DbColumnEx("BUSP_TIME_BUDGET", QVariant::Double));
	view.append(DbColumnEx("BUSP_FIXED_FEE", QVariant::Double));
	view.append(DbColumnEx("BUSP_COST_BUDGET", QVariant::Double));
	view.append(DbColumnEx("BUSP_ORGANIZATION_ID", QVariant::Int));
	view.append(DbColumnEx("BUSP_DEPARTMENT_ID", QVariant::Int));
	view.append(DbColumnEx("BUSP_DEFAULT_GRID_VIEW_ID", QVariant::Int));
	view.append(DbColumnEx("BUSP_FPROJCT_SEQ", QVariant::Int));

	view.append(DbColumnEx("BUSP_INVOICING_TYPE", QVariant::Int));
	view.append(DbColumnEx("BUSP_PRODUCTIVE", QVariant::Int));
	view.append(DbColumnEx("BUSP_NOTCHARGEABLE", QVariant::Int));
	view.append(DbColumnEx("BUSP_INVOICEABLE", QVariant::Int));
	view.append(DbColumnEx("BUSP_ABSENTTIME", QVariant::Int));
	view.append(DbColumnEx("BUSP_HOLIDAYS", QVariant::Int));
	view.append(DbColumnEx("BUSP_STATUS_ID", QVariant::Int));
	view.append(DbColumnEx("BUSP_AREA_ID", QVariant::Int));
	view.append(DbColumnEx("BUSP_SECTOR_ID", QVariant::Int));
	view.append(DbColumnEx("BUSP_INDUSTRY_ID", QVariant::Int));
	view.append(DbColumnEx("BUSP_SERVICE_TYPE_ID", QVariant::Int));
	view.append(DbColumnEx("BUSP_PROCESS_ID", QVariant::Int));
	view.append(DbColumnEx("BUSP_RATE_CAT_ID", QVariant::Int));
	view.append(DbColumnEx("BUSP_EXP_RATE_CAT_ID", QVariant::Int));
	view.append(DbColumnEx("BUSP_PLAN_COST_CAT_ID", QVariant::Int));
	view.append(DbColumnEx("BUSP_TYPE_ID", QVariant::Int));
	view.append(DbColumnEx("BUSP_DEF_ACTIVITY_ID", QVariant::Int));


	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// TVIEW_BUS_PROJECT_IMPORT
	//-----------------------
	view.m_nViewID		= TVIEW_BUS_PROJECT_IMPORT;
	view.m_strTables="BUS_PROJECTS";

	//Record set definition
	view.append(DbColumnEx("BUSP_CODE", QVariant::String));
	view.append(DbColumnEx("BUSP_NAME", QVariant::String));
	view.append(DbColumnEx("BUSP_START_DATE", QVariant::Date));
	view.append(DbColumnEx("BUSP_DEADLINE_DATE", QVariant::Date));
	view.append(DbColumnEx("BUSP_COMPLETION_DATE", QVariant::Date));
	view.append(DbColumnEx("BUSP_LATEST_COMPLETION_DATE", QVariant::Date));
	view.append(DbColumnEx("BUSP_OFFER_DATE", QVariant::Date));
	view.append(DbColumnEx("BUSP_OFFER_VALID_DATE", QVariant::Date));
	view.append(DbColumnEx("BUSP_ORDER_DATE", QVariant::Date));
	view.append(DbColumnEx("BUSP_CONTRACT_DATE", QVariant::Date));
	view.append(DbColumnEx("BUSP_LEADER_CODE", QVariant::String));
	view.append(DbColumnEx("BUSP_RESPONSIBLE_CODE", QVariant::String));
	view.append(DbColumnEx("BUSP_DESCRIPTION", QVariant::String, true));
	view.append(DbColumnEx("BUSP_ACTIVE_FLAG", QVariant::Int));
	view.append(DbColumnEx("BUSP_TEMPLATE_FLAG", QVariant::Int));
	view.append(DbColumnEx("BUSP_LOCATION", QVariant::String, true));
	view.append(DbColumnEx("BUSP_TIME_BUDGET", QVariant::Double));
	view.append(DbColumnEx("BUSP_FIXED_FEE", QVariant::Double));
	view.append(DbColumnEx("BUSP_COST_BUDGET", QVariant::Double));
	view.append(DbColumnEx("BUSP_ORGANIZATION_CODE", QVariant::String));
	view.append(DbColumnEx("BUSP_DEPARTMENT_CODE", QVariant::String));

	
	view.append(DbColumnEx("BUSP_INVOICING_TYPE", QVariant::Int));
	view.append(DbColumnEx("BUSP_PRODUCTIVE", QVariant::Int));
	view.append(DbColumnEx("BUSP_NOTCHARGEABLE", QVariant::Int));
	view.append(DbColumnEx("BUSP_INVOICEABLE", QVariant::Int));
	view.append(DbColumnEx("BUSP_ABSENTTIME", QVariant::Int));
	view.append(DbColumnEx("BUSP_HOLIDAYS", QVariant::Int));

	view.append(DbColumnEx("BUSP_STATUS_CODE", QVariant::String));
	view.append(DbColumnEx("BUSP_AREA_CODE", QVariant::String));
	view.append(DbColumnEx("BUSP_SECTOR_CODE", QVariant::String));
	view.append(DbColumnEx("BUSP_INDUSTRY_CODE", QVariant::String));
	view.append(DbColumnEx("BUSP_SERVICE_TYPE_CODE", QVariant::String));
	view.append(DbColumnEx("BUSP_PROCESS_CODE", QVariant::String));
	view.append(DbColumnEx("BUSP_RATE_CAT_CODE", QVariant::String));
	view.append(DbColumnEx("BUSP_EXP_RATE_CAT_CODE", QVariant::String));
	view.append(DbColumnEx("BUSP_PLAN_COST_CAT_CODE", QVariant::String));
	view.append(DbColumnEx("BUSP_TYPE_CODE", QVariant::String));
	view.append(DbColumnEx("BUSP_DEF_ACTIVITY_CODE", QVariant::String));
	

	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// BUS_PROJECTS_READ
	//-----------------------
	view.m_nViewID		= TVIEW_BUS_PROJECT_READ;
	view.m_strTables="BUS_PROJECTS";

	//Record set definition
	InitTreeSelectionView(BUS_PROJECT, view);
	view.append(DbColumnEx("BUSP_START_DATE", QVariant::Date));
	view.append(DbColumnEx("BUSP_DEADLINE_DATE", QVariant::Date));
	view.append(DbColumnEx("BUSP_COMPLETION_DATE", QVariant::Date));
	view.append(DbColumnEx("BUSP_LATEST_COMPLETION_DATE", QVariant::Date));
	view.append(DbColumnEx("BUSP_OFFER_DATE", QVariant::Date));
	view.append(DbColumnEx("BUSP_OFFER_VALID_DATE", QVariant::Date));
	view.append(DbColumnEx("BUSP_ORDER_DATE", QVariant::Date));
	view.append(DbColumnEx("BUSP_CONTRACT_DATE", QVariant::Date));
	view.append(DbColumnEx("BUSP_LEADER_ID", QVariant::Int));
	view.append(DbColumnEx("BUSP_RESPONSIBLE_ID", QVariant::Int));
	view.append(DbColumnEx("BUSP_DESCRIPTION", QVariant::String, true));
	view.append(DbColumnEx("BUSP_ACTIVE_FLAG", QVariant::Int));
	view.append(DbColumnEx("BUSP_TEMPLATE_FLAG", QVariant::Int));
	view.append(DbColumnEx("BUSP_LOCATION", QVariant::String, true));
	view.append(DbColumnEx("BUSP_TIME_BUDGET", QVariant::Double));
	view.append(DbColumnEx("BUSP_FIXED_FEE", QVariant::Double));
	view.append(DbColumnEx("BUSP_COST_BUDGET", QVariant::Double));
	view.append(DbColumnEx("BUSP_ORGANIZATION_ID", QVariant::Int));
	view.append(DbColumnEx("BUSP_DEPARTMENT_ID", QVariant::Int));

	s_lstDbViews.append(view);
	view.clear();


	//-----------------------
	// SELECT ONLY
	//-----------------------

	view.m_nViewID		= TVIEW_BUS_PROJECT_SELECT;
	view.m_strTables="BUS_PROJECTS";
	view.m_nSkipLastCols=1;
	view.m_nSkipLastColsWrite=1;

	//Record set definition
	InitTreeSelectionView(BUS_PROJECT, view);
	view.append(DbColumnEx("BUSP_TEMPLATE_FLAG", QVariant::Int));
	view.append(DbColumnEx("BUSP_ACTIVE_FLAG", QVariant::Int));
	view.append(DbColumnEx("BUSP_EXPANDED", QVariant::Int));


	s_lstDbViews.append(view);
	view.clear();

	

	//-----------------------
	// BUS_PROJECTS_READ
	//-----------------------
	view.m_nViewID		= TVIEW_BUS_BIG_PICTURE;
	view.m_nSkipFirstColsInsert = 3;
	view.m_nSkipFirstColsUpdate = 3;
	view.m_strTables="BUS_BIG_PICTURE";

	//Record set definition
	view.append(DbColumnEx("BPIC_ID", QVariant::Int));
	view.append(DbColumnEx("BPIC_GLOBAL_ID", QVariant::String));
	view.append(DbColumnEx("BPIC_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("BPIC_PICTURE", QVariant::ByteArray));
	s_lstDbViews.append(view);
	view.clear();


	//-----------------------
	// TVIEW_BUS_CONTACT_QUICK_INFO
	//-----------------------
	view.m_nViewID		= TVIEW_BUS_CONTACT_QUICK_INFO;
	view.m_strTables="BUS_CM_CONTACT";
	view.append(DbColumnEx("BCNT_ID",				QVariant::Int));
	view.append(DbColumnEx("BCMP_ID",				QVariant::Int));
	view.append(DbColumnEx("BCMP_FULLNUMBER",		QVariant::String));
	view.append(DbColumnEx("BCME_ID",				QVariant::Int));
	view.append(DbColumnEx("BCME_ADDRESS",			QVariant::String));
	view.append(DbColumnEx("BCMA_ID",				QVariant::Int));			
	view.append(DbColumnEx("BCMA_FORMATEDADDRESS",	QVariant::String,true));
	
	view.m_strSelectSQL="SELECT "+getSQLColumnsFromView(view)+" FROM BUS_CM_CONTACT LEFT OUTER JOIN bus_cm_phone ON BCMP_CONTACT_ID=BCNT_ID AND BCMP_IS_DEFAULT=1";
	view.m_strSelectSQL+=" LEFT OUTER JOIN bus_cm_email ON BCME_CONTACT_ID=BCNT_ID AND BCME_IS_DEFAULT=1";
	view.m_strSelectSQL+=" LEFT OUTER JOIN bus_cm_address ON BCMA_CONTACT_ID=BCNT_ID AND BCMA_IS_DEFAULT=1";
	s_lstDbViews.append(view);
	view.clear();



	//-----------------------
	// TVIEW_BUS_CM_PHONE_SELECT
	//-----------------------
	view.m_nViewID		= TVIEW_BUS_CM_PHONE_SELECT;
	view.m_nSkipFirstColsInsert = 3;
	view.m_nSkipFirstColsUpdate = 3;
	view.m_nSkipLastColsWrite = 1;
	view.m_strTables="BUS_CM_PHONE";

	//Record set definition
	view.append(DbColumnEx("BCMP_ID", QVariant::Int));
	view.append(DbColumnEx("BCMP_GLOBAL_ID", QVariant::String));
	view.append(DbColumnEx("BCMP_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("BCMP_CONTACT_ID", QVariant::Int));
	view.append(DbColumnEx("BCMP_NAME", QVariant::String));
	view.append(DbColumnEx("BCMP_TYPE_ID", QVariant::Int));
	view.append(DbColumnEx("BCMP_FULLNUMBER", QVariant::String));
	view.append(DbColumnEx("BCMP_LOCAL", QVariant::String));
	view.append(DbColumnEx("BCMP_COUNTRY", QVariant::String));
	view.append(DbColumnEx("BCMP_AREA", QVariant::String));
	view.append(DbColumnEx("BCMP_EXT_ID", QVariant::Int));
	view.append(DbColumnEx("BCMP_IS_DEFAULT", QVariant::Int));
	view.append(DbColumnEx("BCMP_SEARCH", QVariant::String));
	view.append(DbColumnEx("BCMP_COUNTRY_ISO", QVariant::String));
	view.append(DbColumnEx("BCMT_TYPE_NAME", QVariant::String));
	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// TVIEW_BUS_CM_PHONE_SELECT_EXT
	//-----------------------
	view.m_nViewID		= TVIEW_BUS_CM_PHONE_SELECT_EXT;
	view.m_nSkipFirstColsInsert = 3;
	view.m_nSkipFirstColsUpdate = 3;
	view.m_nSkipLastColsWrite = 2;
	view.m_strTables="BUS_CM_PHONE";

	//Record set definition
	view.append(DbColumnEx("BCMP_ID", QVariant::Int));
	view.append(DbColumnEx("BCMP_GLOBAL_ID", QVariant::String));
	view.append(DbColumnEx("BCMP_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("BCMP_CONTACT_ID", QVariant::Int));
	view.append(DbColumnEx("BCMP_NAME", QVariant::String));
	view.append(DbColumnEx("BCMP_TYPE_ID", QVariant::Int));
	view.append(DbColumnEx("BCMP_FULLNUMBER", QVariant::String));
	view.append(DbColumnEx("BCMP_LOCAL", QVariant::String));
	view.append(DbColumnEx("BCMP_COUNTRY", QVariant::String));
	view.append(DbColumnEx("BCMP_AREA", QVariant::String));
	view.append(DbColumnEx("BCMP_EXT_ID", QVariant::Int));
	view.append(DbColumnEx("BCMP_IS_DEFAULT", QVariant::Int));
	view.append(DbColumnEx("BCMP_SEARCH", QVariant::String));
	view.append(DbColumnEx("BCMP_COUNTRY_ISO", QVariant::String));
	view.append(DbColumnEx("BCMT_TYPE_NAME", QVariant::String));
	view.append(DbColumnEx("BCMT_SYSTEM_TYPE", QVariant::Int));

	view.m_strSelectSQL="SELECT "+getSQLColumnsFromView(view)+" FROM BUS_CM_PHONE LEFT OUTER JOIN BUS_CM_TYPES ON BCMT_ID=BCMP_TYPE_ID";

	s_lstDbViews.append(view);
	view.clear();

	
	//-----------------------
	// TVIEW_BUS_CM_EMAIL_SELECT
	//-----------------------
	view.m_nViewID		= TVIEW_BUS_CM_EMAIL_SELECT;
	view.m_nSkipFirstColsInsert = 3;
	view.m_nSkipFirstColsUpdate = 3;
	view.m_nSkipLastColsWrite = 1;
	view.m_nSkipLastCols = 1;
	view.m_strTables="BUS_CM_EMAIL";

	//Record set definition
	view.append(DbColumnEx("BCME_ID", QVariant::Int));
	view.append(DbColumnEx("BCME_GLOBAL_ID", QVariant::String));
	view.append(DbColumnEx("BCME_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("BCME_CONTACT_ID", QVariant::Int));
	view.append(DbColumnEx("BCME_TYPE_ID", QVariant::Int));
	view.append(DbColumnEx("BCME_NAME", QVariant::String));
	view.append(DbColumnEx("BCME_ADDRESS", QVariant::String));
	view.append(DbColumnEx("BCME_DESCRIPTION", QVariant::String, true));
	view.append(DbColumnEx("BCME_EXT_ID", QVariant::Int));
	view.append(DbColumnEx("BCME_IS_DEFAULT", QVariant::Int));
	view.append(DbColumnEx("BCMT_TYPE_NAME", QVariant::String));
	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// TVIEW_BUS_CM_INTERNET_SELECT
	//-----------------------
	view.m_nViewID		= TVIEW_BUS_CM_INTERNET_SELECT;
	view.m_nSkipFirstColsInsert = 3;
	view.m_nSkipFirstColsUpdate = 3;
	view.m_nSkipLastColsWrite = 1;
	view.m_strTables="BUS_CM_INTERNET";

	//Record set definition
	view.append(DbColumnEx("BCMI_ID", QVariant::Int));
	view.append(DbColumnEx("BCMI_GLOBAL_ID", QVariant::String));
	view.append(DbColumnEx("BCMI_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("BCMI_CONTACT_ID", QVariant::Int));
	view.append(DbColumnEx("BCMI_TYPE_ID", QVariant::Int));
	view.append(DbColumnEx("BCMI_NAME", QVariant::String));
	view.append(DbColumnEx("BCMI_ADDRESS", QVariant::String));
	view.append(DbColumnEx("BCMI_DESCRIPTION", QVariant::String, true));
	view.append(DbColumnEx("BCMI_EXT_ID", QVariant::Int));
	view.append(DbColumnEx("BCMI_IS_DEFAULT", QVariant::Int));
	view.append(DbColumnEx("BCMT_TYPE_NAME", QVariant::String));
	s_lstDbViews.append(view);
	view.clear();


	//-----------------------
	// TVIEW_BUS_CM_ADDRESS_SELECT
	//-----------------------
	view.m_nViewID		= TVIEW_BUS_CM_ADDRESS_SELECT;
	view.m_nSkipFirstColsInsert = 3;
	view.m_nSkipFirstColsUpdate = 3;
	view.m_nSkipLastCols = 2; //read
	view.m_nSkipLastColsWrite = 2; //write
	view.m_strTables="BUS_CM_ADDRESS";

	//Record set definition
	view.append(DbColumnEx("BCMA_ID", QVariant::Int));
	view.append(DbColumnEx("BCMA_GLOBAL_ID", QVariant::String));
	view.append(DbColumnEx("BCMA_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("BCMA_CONTACT_ID", QVariant::Int));
	view.append(DbColumnEx("BCMA_FORMATEDADDRESS", QVariant::String,true));
	view.append(DbColumnEx("BCMA_NAME", QVariant::String));
	view.append(DbColumnEx("BCMA_FIRSTNAME", QVariant::String));
	view.append(DbColumnEx("BCMA_LASTNAME", QVariant::String));
	view.append(DbColumnEx("BCMA_MIDDLENAME", QVariant::String));
	view.append(DbColumnEx("BCMA_ORGANIZATIONNAME", QVariant::String));
	view.append(DbColumnEx("BCMA_ORGANIZATIONNAME_2", QVariant::String));
	view.append(DbColumnEx("BCMA_TITLE", QVariant::String));
	view.append(DbColumnEx("BCMA_SALUTATION", QVariant::String));
	view.append(DbColumnEx("BCMA_LANGUGAGE_CODE", QVariant::String));
	view.append(DbColumnEx("BCMA_COUNTRY_CODE", QVariant::String));
	view.append(DbColumnEx("BCMA_COUNTRY_NAME", QVariant::String));
	view.append(DbColumnEx("BCMA_STREET", QVariant::String));
	view.append(DbColumnEx("BCMA_CITY", QVariant::String));
	view.append(DbColumnEx("BCMA_ZIP", QVariant::String));
	view.append(DbColumnEx("BCMA_REGION", QVariant::String));
	view.append(DbColumnEx("BCMA_POBOX", QVariant::String));
	view.append(DbColumnEx("BCMA_POBOXZIP", QVariant::String));
	view.append(DbColumnEx("BCMA_OFFICECODE", QVariant::String));
	view.append(DbColumnEx("BCMA_EXT_ID", QVariant::Int));
	view.append(DbColumnEx("BCMA_IS_DEFAULT", QVariant::Int));
	view.append(DbColumnEx("BCMA_FORMATSCHEMA_ID", QVariant::Int));
	view.append(DbColumnEx("BCMA_PARSESCHEMA_ID", QVariant::Int));
	view.append(DbColumnEx("BCMA_SHORT_SALUTATION", QVariant::String));
	view.append(DbColumnEx("BCMT_TYPE_NAME", QVariant::String));
	view.append(DbColumnEx("LST_TYPES", DbRecordSet::GetVariantType()));		//calculated list

	s_lstDbViews.append(view);
	view.clear();


	//-----------------------
	// TVIEW_BUS_CM_ADDRESS_SELECT
	//-----------------------
	view.m_nViewID		= TVIEW_BUS_CM_ADDRESS_TYPES_SELECT;
	view.m_nSkipFirstColsInsert = 3;
	view.m_nSkipFirstColsUpdate = 3;
	view.m_nSkipLastColsWrite = 1;
	view.m_strTables="BUS_CM_ADDRESS_TYPES";

	//Record set definition
	view.append(DbColumnEx("BCMAT_ID", QVariant::Int));
	view.append(DbColumnEx("BCMAT_GLOBAL_ID", QVariant::String));
	view.append(DbColumnEx("BCMAT_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("BCMAT_ADDRESS_ID", QVariant::Int));
	view.append(DbColumnEx("BCMAT_TYPE_ID", QVariant::Int));
	view.append(DbColumnEx("BCMT_TYPE_NAME", QVariant::String));

	s_lstDbViews.append(view);
	view.clear();

	

	//-----------------------
	// TVIEW_BUS_OPT_GRID_VIEWS
	//-----------------------
	view.m_nViewID		= TVIEW_BUS_OPT_GRID_VIEWS;
	view.m_nSkipFirstColsInsert	= 3;
	view.m_nSkipFirstColsUpdate	= 3;
	view.m_strTables="BUS_OPT_GRID_VIEWS";

	//Record set definition
	view.append(DbColumnEx("BOGW_ID", QVariant::Int));
	view.append(DbColumnEx("BOGW_GLOBAL_ID", QVariant::String));
	view.append(DbColumnEx("BOGW_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("BOGW_GRID_ID", QVariant::Int));
	view.append(DbColumnEx("BOGW_VIEW_NAME", QVariant::String));
	view.append(DbColumnEx("BOGW_HEADER_TEXT", QVariant::String));
	view.append(DbColumnEx("BOGW_COLUMN_NAME", QVariant::String));
	view.append(DbColumnEx("BOGW_COLUMN_TYPE", QVariant::Int));
	view.append(DbColumnEx("BOGW_COLUMN_SIZE", QVariant::Int));
	view.append(DbColumnEx("BOGW_COLUMN_POSITION", QVariant::Int));
	view.append(DbColumnEx("BOGW_EDITABLE", QVariant::Int));
	view.append(DbColumnEx("BOGW_SORT_ORDER", QVariant::Int));
	view.append(DbColumnEx("BOGW_DATASOURCE", QVariant::String));
	view.append(DbColumnEx("BOGW_REPORT_WIDTH", QVariant::Int));

	//Add extra column info
	s_lstDbViews.append(view);
	view.clear();




	//-----------------------
	// TVIEW_BUS_NMRX_RELATION
	//-----------------------
	view.m_nViewID		= TVIEW_BUS_NMRX_RELATION;
	view.m_nSkipFirstColsInsert	= 3;
	view.m_nSkipFirstColsUpdate	= 3;
	view.m_strTables="BUS_NMRX_RELATION";

	//Record set definition
	view.append(DbColumnEx("BNMR_ID", QVariant::Int));
	view.append(DbColumnEx("BNMR_GLOBAL_ID", QVariant::String));
	view.append(DbColumnEx("BNMR_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("BNMR_TABLE_1", QVariant::Int));
	view.append(DbColumnEx("BNMR_TABLE_2", QVariant::Int));
	view.append(DbColumnEx("BNMR_TABLE_KEY_ID_1", QVariant::Int));
	view.append(DbColumnEx("BNMR_TABLE_KEY_ID_2", QVariant::Int));
	view.append(DbColumnEx("BNMR_ROLE_ID", QVariant::Int));
	view.append(DbColumnEx("BNMR_DESCRIPTION", QVariant::String,true));
	view.append(DbColumnEx("BNMR_SYSTEM_ROLE", QVariant::Int));
	view.append(DbColumnEx("BNMR_TYPE", QVariant::Int));
	view.append(DbColumnEx("BNMR_X_TABLE_ID", QVariant::Int));
	
	s_lstDbViews.append(view);
	view.clear();


	//-----------------------
	// TVIEW_BUS_NMRX_ROLE
	//-----------------------
	view.m_nViewID		= TVIEW_BUS_NMRX_ROLE;
	view.m_nSkipFirstColsInsert	= 3;
	view.m_nSkipFirstColsUpdate	= 3;
	view.m_strTables="BUS_NMRX_ROLE";

	//Record set definition
	view.append(DbColumnEx("BNRO_ID", QVariant::Int));
	view.append(DbColumnEx("BNRO_GLOBAL_ID", QVariant::String));
	view.append(DbColumnEx("BNRO_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("BNRO_TABLE_1", QVariant::Int));
	view.append(DbColumnEx("BNRO_TABLE_2", QVariant::Int));
	view.append(DbColumnEx("BNRO_NAME", QVariant::String));
	view.append(DbColumnEx("BNRO_ASSIGMENT_TEXT", QVariant::String));
	view.append(DbColumnEx("BNRO_DESCRIPTION", QVariant::String, true));

	//Add extra column info


	//Add extra column info
	s_lstDbViews.append(view);
	view.clear();



	//-----------------------
	// TVIEW_BUS_OPT_SETTINGS
	//-----------------------
	view.m_nViewID		= TVIEW_BUS_OPT_SETTINGS;
	view.m_nSkipFirstColsInsert	= 3;
	view.m_nSkipFirstColsUpdate	= 3;
	view.m_strTables="BUS_OPT_SETTINGS";

	//Record set definition
	view.append(DbColumnEx("BOUS_ID",				QVariant::Int));
	view.append(DbColumnEx("BOUS_GLOBAL_ID",		QVariant::String));
	view.append(DbColumnEx("BOUS_DAT_LAST_MODIFIED",QVariant::DateTime));
	view.append(DbColumnEx("BOUS_SETTING_SET_ID",	QVariant::Int));
	view.append(DbColumnEx("BOUS_SETTING_ID",		QVariant::Int));
	view.append(DbColumnEx("BOUS_SETTING_VISIBLE",	QVariant::Int));
	view.append(DbColumnEx("BOUS_SETTING_TYPE",		QVariant::Int));
	view.append(DbColumnEx("BOUS_VALUE",			QVariant::Int));
	view.append(DbColumnEx("BOUS_VALUE_STRING",		QVariant::String));
	view.append(DbColumnEx("BOUS_VALUE_BYTE",		QVariant::ByteArray));
	view.append(DbColumnEx("BOUS_PERSON_ID",		QVariant::Int));
	s_lstDbViews.append(view);
	view.clear();



	//-----------------------
	// TVIEW_BUS_GROUP_TREE
	//-----------------------
	view.m_nViewID		= TVIEW_BUS_GROUP_TREE;
	view.m_nSkipFirstColsInsert	= 3;
	view.m_nSkipFirstColsUpdate	= 3;
	view.m_strTables="BUS_GROUP_TREE";

	//Record set definition
	view.append(DbColumnEx("BGTR_ID", QVariant::Int));
	view.append(DbColumnEx("BGTR_GLOBAL_ID", QVariant::String));
	view.append(DbColumnEx("BGTR_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("BGTR_NAME", QVariant::String));
	view.append(DbColumnEx("BGTR_ENTITY_TYPE_ID", QVariant::Int));
	s_lstDbViews.append(view);
	view.clear();



	//-----------------------
	// TVIEW_BUS_GROUP_ITEMS
	//-----------------------
	view.m_nViewID		= TVIEW_BUS_GROUP_ITEMS;
	view.m_nSkipFirstColsInsert	= 3;
	view.m_nSkipFirstColsUpdate	= 3;
	view.m_strTables="BUS_GROUP_ITEMS";

	//Record set definition
	view.append(DbColumnEx("BGIT_ID", QVariant::Int));
	view.append(DbColumnEx("BGIT_GLOBAL_ID", QVariant::String));
	view.append(DbColumnEx("BGIT_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("BGIT_CODE", QVariant::String));
	view.append(DbColumnEx("BGIT_LEVEL", QVariant::Int));
	view.append(DbColumnEx("BGIT_PARENT", QVariant::Int));
	view.append(DbColumnEx("BGIT_HASCHILDREN", QVariant::Int));
	view.append(DbColumnEx("BGIT_ICON", QVariant::String));
	view.append(DbColumnEx("BGIT_MAINPARENT_ID", QVariant::Int));
	view.append(DbColumnEx("BGIT_NAME", QVariant::String));
	view.append(DbColumnEx("BGIT_STYLE", QVariant::Int));
	view.append(DbColumnEx("BGIT_DESCRIPTION", QVariant::String,true));
	view.append(DbColumnEx("BGIT_EXTERNAL_CATEGORY", QVariant::String));
	view.append(DbColumnEx("BGIT_TREE_ID", QVariant::Int));

	s_lstDbViews.append(view);
	view.clear();


	//-----------------------
	// TVIEW_BUS_CM_GROUP
	//-----------------------
	view.m_nViewID		= TVIEW_BUS_CM_GROUP;
	view.m_nSkipFirstColsInsert	= 3;
	view.m_nSkipFirstColsUpdate	= 3;
	view.m_strTables="BUS_CM_GROUP";

	//Record set definition
	view.append(DbColumnEx("BGCN_ID", QVariant::Int));
	view.append(DbColumnEx("BGCN_GLOBAL_ID", QVariant::String));
	view.append(DbColumnEx("BGCN_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("BGCN_CONTACT_ID", QVariant::Int));
	view.append(DbColumnEx("BGCN_ITEM_ID", QVariant::Int));
	view.append(DbColumnEx("BGCN_CMCA_VALID_FROM", QVariant::Date));
	view.append(DbColumnEx("BGCN_CMCA_VALID_TO", QVariant::Date));
	s_lstDbViews.append(view);
	view.clear();


	//-----------------------
	// TVIEW_BUS_CM_GROUP
	//-----------------------
	view.m_nViewID		= TVIEW_BUS_CM_GROUP_SELECT;
	view.m_nSkipFirstColsInsert	= 3;
	view.m_nSkipFirstColsUpdate	= 3;
	view.m_strTables="BUS_CM_GROUP";

	//Record set definition
	view.append(DbColumnEx("BGCN_ID", QVariant::Int));
	view.append(DbColumnEx("BGCN_GLOBAL_ID", QVariant::String));
	view.append(DbColumnEx("BGCN_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("BGCN_CONTACT_ID", QVariant::Int));
	view.append(DbColumnEx("BGCN_ITEM_ID", QVariant::Int));
	view.append(DbColumnEx("BGCN_CMCA_VALID_FROM", QVariant::Date));
	view.append(DbColumnEx("BGCN_CMCA_VALID_TO", QVariant::Date));
	view.append(DbColumnEx("BGIT_ID", QVariant::Int));
	view.append(DbColumnEx("BGIT_CODE", QVariant::String));
	view.append(DbColumnEx("BGIT_NAME", QVariant::String));
	view.append(DbColumnEx("BGIT_TREE_ID", QVariant::Int));
	view.append(DbColumnEx("BGTR_NAME", QVariant::String));
	view.m_strSelectSQL="SELECT "+getSQLColumnsFromView(view)+" FROM BUS_CM_GROUP \
															  INNER JOIN BUS_GROUP_ITEMS ON BGCN_ITEM_ID=BGIT_ID\
															  INNER JOIN BUS_GROUP_TREE ON BGIT_TREE_ID=BGTR_ID ";

	s_lstDbViews.append(view);
	view.clear();


	//-----------------------
	// TVIEW_BUS_NMRX_RELATION_SELECT
	//-----------------------
	view.m_nViewID		= TVIEW_BUS_NMRX_RELATION_SELECT;
	view.m_nSkipFirstColsInsert	= 3;
	view.m_nSkipFirstColsUpdate	= 3;
	view.m_nSkipLastCols		= 3;
	view.m_nSkipLastColsWrite	= 6;

	view.m_strTables="BUS_NMRX_RELATION";

	//Record set definition
	view.append(DbColumnEx("BNMR_ID", QVariant::Int));
	view.append(DbColumnEx("BNMR_GLOBAL_ID", QVariant::String));
	view.append(DbColumnEx("BNMR_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("BNMR_TABLE_1", QVariant::Int));
	view.append(DbColumnEx("BNMR_TABLE_2", QVariant::Int));
	view.append(DbColumnEx("BNMR_TABLE_KEY_ID_1", QVariant::Int));
	view.append(DbColumnEx("BNMR_TABLE_KEY_ID_2", QVariant::Int));
	view.append(DbColumnEx("BNMR_ROLE_ID", QVariant::Int));
	view.append(DbColumnEx("BNMR_DESCRIPTION", QVariant::String, true));
	view.append(DbColumnEx("BNMR_SYSTEM_ROLE", QVariant::Int));
	view.append(DbColumnEx("BNMR_TYPE", QVariant::Int));
	view.append(DbColumnEx("BNMR_X_TABLE_ID", QVariant::Int));
	view.append(DbColumnEx("BNRO_NAME", QVariant::String));
	view.append(DbColumnEx("BNRO_ASSIGMENT_TEXT", QVariant::String));
	view.append(DbColumnEx("BNRO_DESCRIPTION", QVariant::String, true));
	view.append(DbColumnEx("NAME_1", QVariant::String));					//calc field: name of entity table 1
	view.append(DbColumnEx("NAME_2", QVariant::String));					//calc field: name of entity table 2
	view.append(DbColumnEx("X_RECORD", DbRecordSet::GetVariantType()));		//calc field: name of entity table 2

	view.m_strSelectSQL="SELECT "+getSQLColumnsFromView(view)+" FROM BUS_NMRX_RELATION LEFT JOIN BUS_NMRX_ROLE ON BNMR_ROLE_ID=BNRO_ID";

	//Add extra column info
	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// TVIEW_BUS_EMAIL
	//-----------------------
	view.m_nViewID		= TVIEW_BUS_EMAIL;
	view.m_nSkipFirstColsInsert = 3;	//skip Id & global
	view.m_nSkipFirstColsUpdate = 3;
	view.m_strTables="BUS_EMAIL";
	view.append(DbColumnEx("BEM_ID",				QVariant::Int));
	view.append(DbColumnEx("BEM_GLOBAL_ID",			QVariant::String));
	view.append(DbColumnEx("BEM_DAT_LAST_MODIFIED",	QVariant::DateTime));
	view.append(DbColumnEx("BEM_COMM_ENTITY_ID",	QVariant::Int));
	view.append(DbColumnEx("BEM_PROJECT_ID",		QVariant::Int));
	view.append(DbColumnEx("BEM_EXT_ID",			QVariant::ByteArray));
	view.append(DbColumnEx("BEM_EXT_APP_DB_INSTANCE",QVariant::String));
	view.append(DbColumnEx("BEM_EXT_APP",			QVariant::String));
	view.append(DbColumnEx("BEM_FROM",				QVariant::String,true));
	view.append(DbColumnEx("BEM_TO",				QVariant::String,true));
	view.append(DbColumnEx("BEM_CC",				QVariant::String,true));
	view.append(DbColumnEx("BEM_BCC",				QVariant::String,true));
	view.append(DbColumnEx("BEM_SUBJECT",			QVariant::String,true));
	view.append(DbColumnEx("BEM_BODY",				QVariant::String,true));
	view.append(DbColumnEx("BEM_RECV_TIME",			QVariant::DateTime));
	view.append(DbColumnEx("BEM_EMAIL_TYPE",		QVariant::Int));
	view.append(DbColumnEx("BEM_TEMPLATE_FLAG",		QVariant::Int));
	view.append(DbColumnEx("BEM_OUTGOING",			QVariant::Int));
	view.append(DbColumnEx("BEM_TEMPLATE_NAME",		QVariant::String));
	view.append(DbColumnEx("BEM_TEMPLATE_DESCRIPTION",QVariant::String,true));
	view.append(DbColumnEx("BEM_UNREAD_FLAG",		QVariant::Int));
	view.append(DbColumnEx("BEM_IS_SYNC_IN_PROGRESS",	QVariant::Int));
	view.append(DbColumnEx("BEM_SIZE",					QVariant::Int));
	view.append(DbColumnEx("BEM_EXT_ID_CHKSUM",		QVariant::String));
	view.append(DbColumnEx("BEM_CATEGORY",			QVariant::String));
	view.append(DbColumnEx("BEM_FIX_HTML",			QVariant::Int));	//MR issue #2459
	view.append(DbColumnEx("BEM_REPLY_TO",			QVariant::String));	
	view.append(DbColumnEx("BEM_TRUE_FROM",			QVariant::String));
	view.append(DbColumnEx("BEM_TO_EMAIL_ACCOUNT",	QVariant::String));
	view.append(DbColumnEx("BEM_MAILBOX_TYPE",		QVariant::Int));
	view.append(DbColumnEx("BEM_REMINDER_DUE_DATE",	QVariant::DateTime));
	view.append(DbColumnEx("BEM_REMINDER_TYPE",		QVariant::String));
	view.m_strSelectSQL="SELECT " +getSQLColumnsFromView(view)+" FROM BUS_EMAIL LEFT OUTER JOIN BUS_EMAIL_BINARY ON BEB_EMAIL_ID = BEM_ID";
	view.m_strSelectSQL.replace("BEM_BODY","BEB_BODY as BEM_BODY");
		
	s_lstDbViews.append(view);

	//-----------------------
	// TVIEW_BUS_EMAIL_COMM_ENTITY  ++com entity fields
	//-----------------------
	view.m_nViewID		= TVIEW_BUS_EMAIL_COMM_ENTITY;
	view.append(DbColumnEx("CENT_ID",					QVariant::Int));
	view.append(DbColumnEx("CENT_GLOBAL_ID",			QVariant::String));
	view.append(DbColumnEx("CENT_DAT_LAST_MODIFIED",	QVariant::DateTime));
	view.append(DbColumnEx("CENT_CE_TYPE_ID",			QVariant::Int));
	view.append(DbColumnEx("CENT_SYSTEM_TYPE_ID",		QVariant::Int));
	view.append(DbColumnEx("CENT_OWNER_ID",				QVariant::Int));
	view.append(DbColumnEx("CENT_TASK_ID",				QVariant::Int));
	view.append(DbColumnEx("CENT_TOOLTIP",				QVariant::String));
	//view.append(DbColumnEx("CENT_IS_PRIVATE",			QVariant::Int));
	view.m_strSelectSQL="SELECT " +getSQLColumnsFromView(view)+" FROM BUS_EMAIL INNER JOIN CE_COMM_ENTITY ON CENT_ID=BEM_COMM_ENTITY_ID";

	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// TVIEW_BUS_EMAIL_TEMPLATES 
	//-----------------------
	view.m_nViewID		= TVIEW_BUS_EMAIL_TEMPLATES;
	view.m_nSkipFirstColsInsert = 3;	//skip Id & global
	view.m_nSkipFirstColsUpdate = 3;
	view.m_strTables="BUS_EMAIL";
	view.append(DbColumnEx("BEM_ID",				QVariant::Int));
	view.append(DbColumnEx("BEM_GLOBAL_ID",			QVariant::String));
	view.append(DbColumnEx("BEM_DAT_LAST_MODIFIED",	QVariant::DateTime));
	view.append(DbColumnEx("BEM_COMM_ENTITY_ID",	QVariant::Int));
	view.append(DbColumnEx("BEM_TEMPLATE_NAME",		QVariant::String));
	view.append(DbColumnEx("BEM_TEMPLATE_DESCRIPTION",QVariant::String,true));
	view.append(DbColumnEx("BEM_CATEGORY",			QVariant::String));
	view.m_strSelectSQL="SELECT " +getSQLColumnsFromView(view)+" FROM BUS_EMAIL INNER JOIN CE_COMM_ENTITY ON CENT_ID=BEM_COMM_ENTITY_ID";

	s_lstDbViews.append(view);
	view.clear();

	

	//-----------------------
	// TVIEW_BUS_EMAIL_UPDATE 
	//-----------------------
	view.m_nViewID		= TVIEW_BUS_EMAIL_UPDATE;
	view.m_nSkipFirstColsInsert = 3;	//skip Id & global
	view.m_nSkipFirstColsUpdate = 3;
	view.m_strTables="BUS_EMAIL";
	view.append(DbColumnEx("BEM_ID",				QVariant::Int));
	view.append(DbColumnEx("BEM_DAT_LAST_MODIFIED",	QVariant::DateTime));
	view.append(DbColumnEx("CENT_ID",				QVariant::Int));		//special non existent column, filled at client
	view.append(DbColumnEx("BEM_EXT_ID",			QVariant::ByteArray));	//ext id
	view.append(DbColumnEx("BEM_FROM",				QVariant::String,true));
	view.append(DbColumnEx("BEM_TO",				QVariant::String,true));
	view.append(DbColumnEx("BEM_CC",				QVariant::String,true));
	view.append(DbColumnEx("BEM_BCC",				QVariant::String,true));
	view.append(DbColumnEx("BEM_SUBJECT",			QVariant::String,true));
	view.append(DbColumnEx("BEM_BODY",				QVariant::String,true));
	view.append(DbColumnEx("BEM_RECV_TIME",			QVariant::DateTime));
	view.append(DbColumnEx("BEM_EMAIL_TYPE",		QVariant::Int));
	view.append(DbColumnEx("BEM_OUTGOING",			QVariant::Int));
	view.append(DbColumnEx("BEM_UNREAD_FLAG",		QVariant::Int));
	view.append(DbColumnEx("BEM_IS_SYNC_IN_PROGRESS", QVariant::Int));
	view.append(DbColumnEx("BEM_REPLY_TO",			QVariant::String));	
	view.append(DbColumnEx("BEM_TRUE_FROM",			QVariant::String));
	view.append(DbColumnEx("BEM_TO_EMAIL_ACCOUNT",	QVariant::String));
	view.append(DbColumnEx("BEM_MAILBOX_TYPE",		QVariant::Int));
	view.append(DbColumnEx("BEM_REMINDER_DUE_DATE",	QVariant::DateTime));
	view.append(DbColumnEx("BEM_REMINDER_TYPE",		QVariant::String));

	s_lstDbViews.append(view);
	view.clear();


	//-----------------------
	// TVIEW_BUS_EMAIL_ATTACHMENT
	//-----------------------
	view.m_nViewID		= TVIEW_BUS_EMAIL_ATTACHMENT;
	view.m_nSkipFirstColsInsert = 3;	//skip Id & global
	view.m_nSkipFirstColsUpdate = 3;
	view.m_strTables="BUS_EMAIL_ATTACHMENT";

	view.append(DbColumnEx("BEA_ID",				QVariant::Int));
	view.append(DbColumnEx("BEA_GLOBAL_ID",			QVariant::String));
	view.append(DbColumnEx("BEA_DAT_LAST_MODIFIED",	QVariant::DateTime));
	view.append(DbColumnEx("BEA_EMAIL_ID",			QVariant::Int));
	view.append(DbColumnEx("BEA_NAME",				QVariant::String));
	view.append(DbColumnEx("BEA_CONTENT",			QVariant::ByteArray));
	view.append(DbColumnEx("BEA_CID_LINK",			QVariant::String));

	s_lstDbViews.append(view);
	view.clear();


	//-----------------------
	// TVIEW_BUS_TASKS
	//-----------------------
	view.m_nViewID		= TVIEW_BUS_TASKS;
	view.m_nSkipFirstColsInsert = 3;	//skip Id & global
	view.m_nSkipFirstColsUpdate = 3;
	view.m_strTables="BUS_TASKS";
	view.append(DbColumnEx("BTKS_ID",				QVariant::Int));
	view.append(DbColumnEx("BTKS_GLOBAL_ID",		QVariant::String));
	view.append(DbColumnEx("BTKS_DAT_LAST_MODIFIED",QVariant::DateTime));
	view.append(DbColumnEx("BTKS_IS_TASK_ACTIVE",	QVariant::Int));
	view.append(DbColumnEx("BTKS_SUBJECT",			QVariant::String));
	view.append(DbColumnEx("BTKS_START",			QVariant::DateTime));
	view.append(DbColumnEx("BTKS_DUE",				QVariant::DateTime));
	view.append(DbColumnEx("BTKS_COMPLETED",		QVariant::DateTime));
	view.append(DbColumnEx("BTKS_TASK_TYPE_ID",		QVariant::Int));
	view.append(DbColumnEx("BTKS_SYSTEM_TYPE",		QVariant::Int));
	view.append(DbColumnEx("BTKS_SYSTEM_STATUS",	QVariant::Int));
	view.append(DbColumnEx("BTKS_DESCRIPTION",		QVariant::String,true));
	view.append(DbColumnEx("BTKS_OWNER",			QVariant::Int));
	view.append(DbColumnEx("BTKS_ORIGINATOR",		QVariant::Int));
	view.append(DbColumnEx("BTKS_NOTIFY_ORIGINATOR",QVariant::Int));
	view.append(DbColumnEx("BTKS_PRIORITY",			QVariant::Int));

	s_lstDbViews.append(view);
	view.clear();


	//-----------------------
	// TVIEW_BUS_CM_DEAFULTS
	//-----------------------
	view.m_nViewID		= TVIEW_BUS_CM_DEAFULTS;
	view.m_strTables="BUS_CM_CONTACT";

	view.append(DbColumnEx("BCNT_ID", QVariant::Int));
	view.append(DbColumnEx("BCME_NAME", QVariant::String));
	view.append(DbColumnEx("BCME_ADDRESS", QVariant::String));
	view.append(DbColumnEx("BCME_TYPE_ID", QVariant::Int));
	view.append(DbColumnEx("BCMI_ADDRESS", QVariant::String));
	view.append(DbColumnEx("BCMI_TYPE_ID", QVariant::Int));
	view.append(DbColumnEx("BCMP_FULLNUMBER", QVariant::String));
	view.append(DbColumnEx("BCMP_SEARCH", QVariant::String));
	view.append(DbColumnEx("BCMA_FORMATEDADDRESS", QVariant::String,true));
	view.append(DbColumnEx("BCNT_DO_NOT_SYNC_MAIL", QVariant::Int));
	view.append(DbColumnEx("BCNT_SYNC_MAIL_AS_PRIV", QVariant::Int));
	view.append(DbColumnEx("BCNT_OUT_MAIL_AS_PRIV", QVariant::Int));



	s_lstDbViews.append(view);
	view.clear();
	

	//----------------------------------------------------------------------
	//						DOCUMENTS
	//----------------------------------------------------------------------


	//-----------------------
	// TVIEW_BUS_DM_USER_PATHS
	//-----------------------
	view.m_nViewID		= TVIEW_BUS_DM_USER_PATHS;
	view.m_nSkipFirstColsInsert	= 3;
	view.m_nSkipFirstColsUpdate	= 3;
	view.m_strTables="BUS_DM_USER_PATHS";

	//Record set definition
	view.append(DbColumnEx("BDMU_ID", QVariant::Int));
	view.append(DbColumnEx("BDMU_GLOBAL_ID", QVariant::String));
	view.append(DbColumnEx("BDMU_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("BDMU_APP_PATH", QVariant::String,true));
	view.append(DbColumnEx("BDMU_PRIORITY", QVariant::Int));
	view.append(DbColumnEx("BDMU_OWNER_ID", QVariant::Int));
	view.append(DbColumnEx("BDMU_APPLICATION_ID", QVariant::Int));

	s_lstDbViews.append(view);
	view.clear();


	//-----------------------
	// TVIEW_BUS_DM_APPLICATIONS
	//-----------------------
	view.m_nViewID		= TVIEW_BUS_DM_APPLICATIONS;
	view.m_nSkipFirstColsInsert	= 3;
	view.m_nSkipFirstColsUpdate	= 3;
	view.m_strTables="BUS_DM_APPLICATIONS";

	//Record set definition
	view.append(DbColumnEx("BDMA_ID", QVariant::Int));
	view.append(DbColumnEx("BDMA_GLOBAL_ID", QVariant::String));
	view.append(DbColumnEx("BDMA_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("BDMA_CODE", QVariant::String));
	view.append(DbColumnEx("BDMA_NAME", QVariant::String));
	view.append(DbColumnEx("BDMA_TEMPLATE_ID", QVariant::Int));
	view.append(DbColumnEx("BDMA_ICON", QVariant::ByteArray));
	view.append(DbColumnEx("BDMA_EXTENSIONS", QVariant::String));
	view.append(DbColumnEx("BDMA_DESCRIPTION", QVariant::String,true));
	view.append(DbColumnEx("BDMA_OPEN_PARAMETER", QVariant::String));
	view.append(DbColumnEx("BDMA_APPLICATION_TYPE", QVariant::Int));
	view.append(DbColumnEx("BDMA_READ_ONLY", QVariant::Int));
	view.append(DbColumnEx("BDMA_NO_FILE_DOCUMENT", QVariant::Int));
	view.append(DbColumnEx("BDMA_INSTALL_APP_TEMPL_ID", QVariant::Int));
	view.append(DbColumnEx("BDMA_INSTALL_VIEWER_TEMPL_ID", QVariant::Int));
	view.append(DbColumnEx("BDMA_ENCODE_PARAMETERS", QVariant::Int));
	view.append(DbColumnEx("BDMA_SET_READ_ONLY_FLAG", QVariant::Int));
		
	s_lstDbViews.append(view);
	view.clear();
	


	//-----------------------
	// TVIEW_BUS_DM_DOCUMENTS
	//-----------------------
	view.m_nViewID		= TVIEW_BUS_DM_DOCUMENTS;
	view.m_nSkipFirstColsInsert	= 3;
	view.m_nSkipFirstColsUpdate	= 3;
	view.m_strTables="BUS_DM_DOCUMENTS";

	//Record set definition
	view.append(DbColumnEx("BDMD_ID", QVariant::Int));
	view.append(DbColumnEx("BDMD_GLOBAL_ID", QVariant::String));
	view.append(DbColumnEx("BDMD_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("BDMD_DAT_CREATED", QVariant::DateTime));
	view.append(DbColumnEx("BDMD_NAME", QVariant::String));
	view.append(DbColumnEx("BDMD_LOCATION", QVariant::String));
	view.append(DbColumnEx("BDMD_DOC_PATH", QVariant::String,true));
	view.append(DbColumnEx("BDMD_OPEN_PARAMETER", QVariant::String));
	view.append(DbColumnEx("BDMD_DESCRIPTION", QVariant::String,true));
	view.append(DbColumnEx("BDMD_APPLICATION_ID", QVariant::Int));
	view.append(DbColumnEx("BDMD_TEMPLATE_ID", QVariant::Int));
	view.append(DbColumnEx("BDMD_COMM_ENTITY_ID", QVariant::Int));
	view.append(DbColumnEx("BDMD_DOC_TYPE", QVariant::Int));
	view.append(DbColumnEx("BDMD_IS_DIRECTORY", QVariant::Int));
	view.append(DbColumnEx("BDMD_IS_CHECK_OUT",			QVariant::Int));
	view.append(DbColumnEx("BDMD_CHECK_OUT_USER_ID",	QVariant::Int));
	view.append(DbColumnEx("BDMD_CHECK_OUT_DATE",		QVariant::DateTime));
	view.append(DbColumnEx("BDMD_TEMPLATE_FLAG",		QVariant::Int));
	view.append(DbColumnEx("BDMD_SIZE",					QVariant::Int));
	view.append(DbColumnEx("BDMD_READ_ONLY",			QVariant::Int));
	view.append(DbColumnEx("BDMD_CATEGORY",				QVariant::String));
	view.append(DbColumnEx("BDMD_SET_TAG_FLAG",			QVariant::Int));

	s_lstDbViews.append(view);

	//WARNING: Do not insert nothing between this two views!!!
	//WARNING: Do not insert nothing between this two views!!!
	//WARNING: Do not insert nothing between this two views!!!

	//-----------------------
	// TVIEW_BUS_DM_DOCUMENTS_COMM_ENTITY  ++com entity fields
	//-----------------------
	view.m_nViewID		= TVIEW_BUS_DM_DOCUMENTS_COMM_ENTITY;
	view.append(DbColumnEx("CENT_ID",					QVariant::Int));
	view.append(DbColumnEx("CENT_GLOBAL_ID",			QVariant::String));
	view.append(DbColumnEx("CENT_DAT_LAST_MODIFIED",	QVariant::DateTime));
	view.append(DbColumnEx("CENT_CE_TYPE_ID",			QVariant::Int));
	view.append(DbColumnEx("CENT_SYSTEM_TYPE_ID",		QVariant::Int));
	view.append(DbColumnEx("CENT_OWNER_ID",				QVariant::Int));
	view.append(DbColumnEx("CENT_TASK_ID",				QVariant::Int));
	view.append(DbColumnEx("CENT_TOOLTIP",				QVariant::String));
	//view.append(DbColumnEx("CENT_IS_PRIVATE",			QVariant::Int));
	view.m_strSelectSQL="SELECT " +getSQLColumnsFromView(view)+" FROM BUS_DM_DOCUMENTS INNER JOIN CE_COMM_ENTITY ON CENT_ID=BDMD_COMM_ENTITY_ID";

	s_lstDbViews.append(view);
	view.clear();


	//-----------------------
	// TVIEW_BUS_DM_USER_PATHS_SELECT
	//-----------------------
	view.m_nViewID		=	TVIEW_BUS_DM_USER_PATHS_SELECT;
	view.m_strTables	=	"BUS_DM_USER_PATHS";
	view.append(DbColumnEx("BDMU_ID", QVariant::Int));
	view.append(DbColumnEx("BDMU_APP_PATH", QVariant::String, true));
	view.append(DbColumnEx("BDMU_PRIORITY", QVariant::Int));
	view.append(DbColumnEx("BDMU_OWNER_ID", QVariant::Int));
	view.append(DbColumnEx("BDMU_APPLICATION_ID", QVariant::Int));
	view.append(DbColumnEx("BDMA_CODE", QVariant::String));
	view.append(DbColumnEx("BDMA_NAME", QVariant::String));
	view.m_strSelectSQL="SELECT " +getSQLColumnsFromView(view)+" FROM BUS_DM_USER_PATHS INNER JOIN BUS_DM_APPLICATIONS ON BDMU_APPLICATION_ID=BDMA_ID";
	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// TVIEW_BUS_DM_DOCUMENTS_UPDATE_APP
	//-----------------------
	view.m_nViewID		= TVIEW_BUS_DM_DOCUMENTS_UPDATE_APP;
	view.m_nSkipFirstColsInsert	= 3;
	view.m_nSkipFirstColsUpdate	= 3;
	view.m_strTables="BUS_DM_DOCUMENTS";

	//Record set definition
	view.append(DbColumnEx("BDMD_ID", QVariant::Int));
	view.append(DbColumnEx("BDMD_GLOBAL_ID", QVariant::String));
	view.append(DbColumnEx("BDMD_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("BDMD_APPLICATION_ID", QVariant::Int));

	s_lstDbViews.append(view);
	view.clear();


	//-----------------------
	// TVIEW_COMM_GRID_EMAIL_VIEW
	//-----------------------
	view.m_nViewID		= TVIEW_COMM_GRID_EMAIL_VIEW;
	view.m_strTables	= "CE_COMM_ENTITY";
	view.m_nSkipLastCols = 4;
	view.append(DbColumnEx("CENT_ID",					QVariant::Int));
	view.append(DbColumnEx("CENT_DAT_LAST_MODIFIED",	QVariant::DateTime));
	view.append(DbColumnEx("CENT_SYSTEM_TYPE_ID",		QVariant::Int));
	view.append(DbColumnEx("CENT_TOOLTIP",				QVariant::String));
	//view.append(DbColumnEx("CENT_IS_PRIVATE",			QVariant::Int));
	view.append(DbColumnEx("BTKS_ID",					QVariant::Int));
	view.append(DbColumnEx("BTKS_IS_TASK_ACTIVE",		QVariant::Int));
	view.append(DbColumnEx("BTKS_SUBJECT",				QVariant::String));
	view.append(DbColumnEx("BTKS_START",				QVariant::DateTime));
	view.append(DbColumnEx("BTKS_DUE",					QVariant::DateTime));
	view.append(DbColumnEx("BTKS_COMPLETED",			QVariant::DateTime));
	view.append(DbColumnEx("BTKS_SYSTEM_STATUS",		QVariant::Int));
	view.append(DbColumnEx("BTKS_NOTIFY_ORIGINATOR",	QVariant::Int));
	view.append(DbColumnEx("BTKS_DESCRIPTION",			QVariant::String, true));
	view.append(DbColumnEx("BTKS_ORIGINATOR",			QVariant::Int));
	view.append(DbColumnEx("BTKS_PRIORITY",				QVariant::Int));
	view.append(DbColumnEx("BTKS_TASK_TYPE_ID",			QVariant::Int));
	view.append(DbColumnEx("BCMT_COLOR",				QVariant::String));
	view.append(DbColumnEx("BCMT_PICTURE",				QVariant::ByteArray));
	view.append(DbColumnEx("BEM_ID",					QVariant::Int));
	view.append(DbColumnEx("BEM_SUBJECT",				QVariant::String, true));
	view.append(DbColumnEx("BEM_BODY",					QVariant::String, true));	//-->> commented by MP on 21.5.2008 because Herzog had 20 mails with 1 MB body in a view.
	//BT. 01.03.2011 commented: new CE_TOOLTIP in action (body used only for tooltip)
	//BT. 14.03.2011 uncommented: BEM_BODY is used in full text search -> server crash if not exists
	view.append(DbColumnEx("BEM_RECV_TIME",				QVariant::DateTime));
	view.append(DbColumnEx("BEM_UNREAD_FLAG",			QVariant::Int));
	view.append(DbColumnEx("BEM_EMAIL_TYPE",			QVariant::Int));
	view.append(DbColumnEx("BEM_OUTGOING",				QVariant::Int));
	view.append(DbColumnEx("BEM_TEMPLATE_FLAG",			QVariant::Int));
	view.append(DbColumnEx("BEM_EXT_ID",				QVariant::ByteArray));
	view.append(DbColumnEx("BEM_EXT_APP",				QVariant::String));
	view.append(DbColumnEx("BEM_SIZE",					QVariant::Int));
	view.append(DbColumnEx("A.BPER_ID",					QVariant::Int));
	view.append(DbColumnEx("A.BPER_FIRST_NAME",			QVariant::String));
	view.append(DbColumnEx("A.BPER_LAST_NAME",			QVariant::String));
	view.append(DbColumnEx("B.BORG_ID",					QVariant::Int));
	view.append(DbColumnEx("B.BORG_NAME",				QVariant::String));
	view.append(DbColumnEx("B1.BORG_ID",				QVariant::Int));
	view.append(DbColumnEx("B1.BORG_NAME",				QVariant::String));
	view.append(DbColumnEx("BUSP_ID",					QVariant::Int));
	view.append(DbColumnEx("BUSP_CODE",					QVariant::String));
	view.append(DbColumnEx("BUSP_NAME",					QVariant::String));
	view.append(DbColumnEx("A1.BPER_FIRST_NAME",		QVariant::String));
	view.append(DbColumnEx("A1.BPER_LAST_NAME",			QVariant::String));
	view.append(DbColumnEx("A1.BPER_INITIALS",			QVariant::String));
	view.append(DbColumnEx("A2.BPER_ID",				QVariant::Int));
	view.append(DbColumnEx("A2.BPER_FIRST_NAME",		QVariant::String));
	view.append(DbColumnEx("A2.BPER_LAST_NAME",			QVariant::String));
	view.append(DbColumnEx("A2.BPER_INITIALS",			QVariant::String));
	view.append(DbColumnEx("BCNT_ID",					QVariant::Int));
	view.append(DbColumnEx("BCNT_LASTNAME",				QVariant::String));
	view.append(DbColumnEx("BCNT_FIRSTNAME",			QVariant::String));
	view.append(DbColumnEx("BCNT_ORGANIZATIONNAME",		QVariant::String));
	view.m_strSelectSQL = "SELECT " + getSQLColumnsFromView(view) +
						  " FROM BUS_EMAIL \
							LEFT OUTER JOIN BUS_PROJECTS ON BEM_PROJECT_ID = BUSP_ID \
							INNER JOIN CE_COMM_ENTITY ON BEM_COMM_ENTITY_ID = CENT_ID \
							LEFT OUTER JOIN BUS_EMAIL_BINARY ON BEB_EMAIL_ID = BEM_ID \
							LEFT OUTER JOIN BUS_TASKS ON CENT_TASK_ID = BTKS_ID	\
							LEFT OUTER JOIN BUS_PERSON A  ON CENT_OWNER_ID = A.BPER_ID \
							LEFT OUTER JOIN BUS_PERSON A1 ON BTKS_ORIGINATOR = A1.BPER_ID \
							LEFT OUTER JOIN BUS_PERSON A2 ON BTKS_OWNER = A2.BPER_ID \
							LEFT OUTER JOIN BUS_ORGANIZATIONS B ON A.BPER_ORGANIZATION_ID = B.BORG_ID \
							LEFT OUTER JOIN BUS_CM_TYPES ON BCMT_ID = BTKS_TASK_TYPE_ID \
							LEFT OUTER JOIN BUS_ORGANIZATIONS B1 ON A2.BPER_ORGANIZATION_ID = B1.BORG_ID ";
	view.m_strSelectSQL.replace("BEM_BODY","BEB_BODY as BEM_BODY");

	s_lstDbViews.append(view);
	view.clear();
	
	//-----------------------
	// TVIEW_CONTACT_COMM_GRID_EMAIL_VIEW
	//-----------------------
	view.m_nViewID		= TVIEW_CONTACT_COMM_GRID_EMAIL_VIEW;
	view.m_strTables	= "CE_COMM_ENTITY";
	view.m_nSkipLastCols = 4;
	view.append(DbColumnEx("CENT_ID",					QVariant::Int));
	view.append(DbColumnEx("CENT_DAT_LAST_MODIFIED",	QVariant::DateTime));
	view.append(DbColumnEx("CENT_SYSTEM_TYPE_ID",		QVariant::Int));
	view.append(DbColumnEx("CENT_TOOLTIP",				QVariant::String));
	//view.append(DbColumnEx("CENT_IS_PRIVATE",			QVariant::Int));
	view.append(DbColumnEx("BTKS_ID",					QVariant::Int));
	view.append(DbColumnEx("BTKS_IS_TASK_ACTIVE",		QVariant::Int));
	view.append(DbColumnEx("BTKS_SUBJECT",				QVariant::String));
	view.append(DbColumnEx("BTKS_START",				QVariant::DateTime));
	view.append(DbColumnEx("BTKS_DUE",					QVariant::DateTime));
	view.append(DbColumnEx("BTKS_COMPLETED",			QVariant::DateTime));
	view.append(DbColumnEx("BTKS_SYSTEM_STATUS",		QVariant::Int));
	view.append(DbColumnEx("BTKS_DESCRIPTION",			QVariant::String, true));
	view.append(DbColumnEx("BTKS_ORIGINATOR",			QVariant::Int));
	view.append(DbColumnEx("BTKS_NOTIFY_ORIGINATOR",	QVariant::Int));
	view.append(DbColumnEx("BTKS_PRIORITY",				QVariant::Int));
	view.append(DbColumnEx("BTKS_TASK_TYPE_ID",			QVariant::Int));
	view.append(DbColumnEx("BCMT_COLOR",				QVariant::String));
	view.append(DbColumnEx("BCMT_PICTURE",				QVariant::ByteArray));
	view.append(DbColumnEx("BEM_ID",					QVariant::Int));
	view.append(DbColumnEx("BEM_SUBJECT",				QVariant::String, true));
	view.append(DbColumnEx("BEM_BODY",					QVariant::String));	//-->> commented by MP on 21.5.2008 because Herzog had 20 mails with 1 MB body in a view.
	//BT. 01.03.2011 commented: new CE_TOOLTIP in action (body used only for tooltip)
	//BT. 14.03.2011 uncommented: BEM_BODY is used in full text search -> server crash if not exists
	view.append(DbColumnEx("BEM_RECV_TIME",				QVariant::DateTime));
	view.append(DbColumnEx("BEM_UNREAD_FLAG",			QVariant::Int));
	view.append(DbColumnEx("BEM_EMAIL_TYPE",			QVariant::Int));
	view.append(DbColumnEx("BEM_OUTGOING",				QVariant::Int));
	view.append(DbColumnEx("BEM_TEMPLATE_FLAG",			QVariant::Int));
	view.append(DbColumnEx("BEM_EXT_ID",				QVariant::ByteArray));
	view.append(DbColumnEx("BEM_EXT_APP",				QVariant::String));
	view.append(DbColumnEx("BEM_SIZE",					QVariant::Int));
	view.append(DbColumnEx("A.BPER_ID",					QVariant::Int));
	view.append(DbColumnEx("A.BPER_FIRST_NAME",			QVariant::String));
	view.append(DbColumnEx("A.BPER_LAST_NAME",			QVariant::String));
	view.append(DbColumnEx("B.BORG_ID",					QVariant::Int));
	view.append(DbColumnEx("B.BORG_NAME",				QVariant::String));
	view.append(DbColumnEx("B1.BORG_ID",				QVariant::Int));
	view.append(DbColumnEx("B1.BORG_NAME",				QVariant::String));
	view.append(DbColumnEx("BUSP_ID",					QVariant::Int));
	view.append(DbColumnEx("BUSP_CODE",					QVariant::String));
	view.append(DbColumnEx("BUSP_NAME",					QVariant::String));
	view.append(DbColumnEx("A1.BPER_FIRST_NAME",		QVariant::String));
	view.append(DbColumnEx("A1.BPER_LAST_NAME",			QVariant::String));
	view.append(DbColumnEx("A1.BPER_INITIALS",			QVariant::String));
	view.append(DbColumnEx("A2.BPER_ID",				QVariant::Int));
	view.append(DbColumnEx("A2.BPER_FIRST_NAME",		QVariant::String));
	view.append(DbColumnEx("A2.BPER_LAST_NAME",			QVariant::String));
	view.append(DbColumnEx("A2.BPER_INITIALS",			QVariant::String));
	view.append(DbColumnEx("BCNT_ID",					QVariant::Int));
	view.append(DbColumnEx("BCNT_LASTNAME",				QVariant::String));
	view.append(DbColumnEx("BCNT_FIRSTNAME",			QVariant::String));
	view.append(DbColumnEx("BCNT_ORGANIZATIONNAME",		QVariant::String));
	view.m_strSelectSQL = "SELECT " + getSQLColumnsFromView(view) +
		" FROM BUS_EMAIL \
		LEFT OUTER JOIN BUS_PROJECTS ON BEM_PROJECT_ID = BUSP_ID \
		LEFT OUTER JOIN BUS_EMAIL_BINARY ON BEB_EMAIL_ID = BEM_ID \
		INNER JOIN CE_COMM_ENTITY ON BEM_COMM_ENTITY_ID = CENT_ID \
		INNER JOIN BUS_NMRX_RELATION ON CENT_ID = BNMR_TABLE_KEY_ID_1 AND BNMR_TABLE_1 =35 AND BNMR_TABLE_2=22 AND BNMR_TYPE=0\
		LEFT OUTER JOIN BUS_TASKS ON CENT_TASK_ID = BTKS_ID \
		LEFT OUTER JOIN BUS_PERSON A  ON CENT_OWNER_ID = A.BPER_ID \
		LEFT OUTER JOIN BUS_PERSON A1 ON BTKS_ORIGINATOR = A1.BPER_ID \
		LEFT OUTER JOIN BUS_PERSON A2 ON BTKS_OWNER = A2.BPER_ID \
		LEFT OUTER JOIN BUS_ORGANIZATIONS B ON A.BPER_ORGANIZATION_ID = B.BORG_ID \
		LEFT OUTER JOIN BUS_CM_TYPES ON BCMT_ID = BTKS_TASK_TYPE_ID \
		LEFT OUTER JOIN BUS_ORGANIZATIONS B1 ON A2.BPER_ORGANIZATION_ID = B1.BORG_ID ";
	view.m_strSelectSQL.replace("BEM_BODY","BEB_BODY as BEM_BODY");
	s_lstDbViews.append(view);
	view.clear();
	
	//-----------------------
	// TVIEW_COMM_CONTACT_DOCUMENT_VIEW
	//-----------------------
	view.m_nViewID		= TVIEW_COMM_CONTACT_DOCUMENT_VIEW;
	view.m_strTables	= "BUS_CM_CONTACT, BUS_NMRX_RELATION, BUS_DM_DOCUMENTS";
	view.append(DbColumnEx("BCNT_ID",					QVariant::Int));
	view.append(DbColumnEx("BCNT_LASTNAME",				QVariant::String));
	view.append(DbColumnEx("BCNT_FIRSTNAME",			QVariant::String));
	view.append(DbColumnEx("BCNT_ORGANIZATIONNAME",		QVariant::String));
	view.append(DbColumnEx("BNMR_TABLE_KEY_ID_1", 		QVariant::Int));
	view.append(DbColumnEx("BNMR_DAT_LAST_MODIFIED",	QVariant::DateTime));
	s_lstDbViews.append(view);
	view.clear();
	
	//-----------------------
	// TVIEW_COMM_CONTACT_EMAIL_VIEW1
	//-----------------------
	view.m_nViewID		= TVIEW_COMM_CONTACT_EMAIL_VIEW;
	view.m_strTables	= "BUS_CM_CONTACT, BUS_NMRX_RELATION, BUS_EMAIL";
	view.append(DbColumnEx("BCNT_ID",					QVariant::Int));
	view.append(DbColumnEx("BCNT_LASTNAME",				QVariant::String));
	view.append(DbColumnEx("BCNT_FIRSTNAME",			QVariant::String));
	view.append(DbColumnEx("BCNT_ORGANIZATIONNAME",		QVariant::String));
	view.append(DbColumnEx("BNMR_TABLE_KEY_ID_1", 		QVariant::Int));
	view.append(DbColumnEx("BNMR_DAT_LAST_MODIFIED",	QVariant::DateTime));
	view.append(DbColumnEx("BEM_OUTGOING",				QVariant::Int));
	s_lstDbViews.append(view);
	view.clear();
	
	//-----------------------
	// TVIEW_COMM_GRID_VOICECALL_VIEW
	//-----------------------
	view.m_nViewID		= TVIEW_COMM_GRID_VOICECALL_VIEW;
	view.m_strTables	= "CE_COMM_ENTITY";
	view.append(DbColumnEx("CENT_ID",					QVariant::Int));
	view.append(DbColumnEx("CENT_DAT_LAST_MODIFIED",	QVariant::DateTime));
	view.append(DbColumnEx("CENT_SYSTEM_TYPE_ID",		QVariant::Int));
	view.append(DbColumnEx("CENT_TOOLTIP",				QVariant::String));
	//view.append(DbColumnEx("CENT_IS_PRIVATE",			QVariant::Int));
	view.append(DbColumnEx("BTKS_ID",					QVariant::Int));
	view.append(DbColumnEx("BTKS_IS_TASK_ACTIVE",		QVariant::Int));
	view.append(DbColumnEx("BTKS_SUBJECT",				QVariant::String));
	view.append(DbColumnEx("BTKS_START",				QVariant::DateTime));
	view.append(DbColumnEx("BTKS_DUE",					QVariant::DateTime));
	view.append(DbColumnEx("BTKS_COMPLETED",			QVariant::DateTime));
	view.append(DbColumnEx("BTKS_SYSTEM_STATUS",		QVariant::Int));
	view.append(DbColumnEx("BTKS_DESCRIPTION",			QVariant::String, true));
	view.append(DbColumnEx("BTKS_ORIGINATOR",			QVariant::Int));
	view.append(DbColumnEx("BTKS_NOTIFY_ORIGINATOR",	QVariant::Int));
	view.append(DbColumnEx("BTKS_PRIORITY",				QVariant::Int));
	view.append(DbColumnEx("BTKS_TASK_TYPE_ID",			QVariant::Int));
	view.append(DbColumnEx("BCMT_COLOR",				QVariant::String));
	view.append(DbColumnEx("BCMT_PICTURE",				QVariant::ByteArray));
	view.append(DbColumnEx("BVC_ID",					QVariant::Int));
	view.append(DbColumnEx("BVC_START",					QVariant::DateTime));
	view.append(DbColumnEx("BVC_DURATION",				QVariant::Int));
	view.append(DbColumnEx("BVC_DIRECTION",				QVariant::Int));
	view.append(DbColumnEx("BVC_CALLER_ID",				QVariant::String));
	view.append(DbColumnEx("BVC_DESCRIPTION",			QVariant::String, true));
	view.append(DbColumnEx("BVC_CALL_STATUS",			QVariant::Int));
	view.append(DbColumnEx("A.BPER_ID",					QVariant::Int));
	view.append(DbColumnEx("A.BPER_FIRST_NAME",			QVariant::String));
	view.append(DbColumnEx("A.BPER_LAST_NAME",			QVariant::String));
//	view.append(DbColumnEx("A.BPER_INITIALS",			QVariant::String));
	view.append(DbColumnEx("B.BORG_ID",					QVariant::Int));
	view.append(DbColumnEx("B.BORG_NAME",				QVariant::String));
	view.append(DbColumnEx("B1.BORG_ID",				QVariant::Int));
	view.append(DbColumnEx("B1.BORG_NAME",				QVariant::String));
	view.append(DbColumnEx("BUSP_ID",					QVariant::Int));
	view.append(DbColumnEx("BUSP_CODE",					QVariant::String));
	view.append(DbColumnEx("BUSP_NAME",					QVariant::String));
	view.append(DbColumnEx("A1.BPER_FIRST_NAME",		QVariant::String));
	view.append(DbColumnEx("A1.BPER_LAST_NAME",			QVariant::String));
	view.append(DbColumnEx("A1.BPER_INITIALS",			QVariant::String));
	view.append(DbColumnEx("A2.BPER_ID",				QVariant::Int));
	view.append(DbColumnEx("A2.BPER_FIRST_NAME",		QVariant::String));
	view.append(DbColumnEx("A2.BPER_LAST_NAME",			QVariant::String));
	view.append(DbColumnEx("A2.BPER_INITIALS",			QVariant::String));
	view.append(DbColumnEx("BCNT_ID",					QVariant::Int));
	view.append(DbColumnEx("BCNT_LASTNAME",				QVariant::String));
	view.append(DbColumnEx("BCNT_FIRSTNAME",			QVariant::String));
	view.append(DbColumnEx("BCNT_ORGANIZATIONNAME",		QVariant::String));
	view.m_strSelectSQL = "SELECT " + getSQLColumnsFromView(view) +
						  " FROM BUS_VOICECALLS \
							LEFT OUTER JOIN BUS_PROJECTS ON BVC_PROJECT_ID = BUSP_ID \
							LEFT OUTER JOIN BUS_CM_CONTACT ON BVC_CONTACT_ID = BCNT_ID \
							INNER JOIN CE_COMM_ENTITY ON BVC_COMM_ENTITY_ID = CENT_ID \
							LEFT OUTER JOIN BUS_TASKS  ON CENT_TASK_ID = BTKS_ID \
							LEFT OUTER JOIN BUS_PERSON A ON CENT_OWNER_ID = A.BPER_ID \
							LEFT OUTER JOIN BUS_PERSON A1 ON BTKS_ORIGINATOR = A1.BPER_ID \
							LEFT OUTER JOIN BUS_PERSON A2 ON BTKS_OWNER = A2.BPER_ID \
							LEFT OUTER JOIN BUS_ORGANIZATIONS B ON A.BPER_ORGANIZATION_ID = B.BORG_ID \
							LEFT OUTER JOIN BUS_CM_TYPES ON BCMT_ID = BTKS_TASK_TYPE_ID \
							LEFT OUTER JOIN BUS_ORGANIZATIONS B1 ON A2.BPER_ORGANIZATION_ID = B1.BORG_ID ";
	s_lstDbViews.append(view);
	view.clear();
	
	//-----------------------
	// TVIEW_COMM_GRID_DOCUMENT_VIEW
	//-----------------------
	view.m_nViewID		= TVIEW_COMM_GRID_DOCUMENT_VIEW;
	view.m_strTables	= "CE_COMM_ENTITY";
	view.m_nSkipLastCols = 4;
	view.append(DbColumnEx("CENT_ID",					QVariant::Int));
	view.append(DbColumnEx("CENT_DAT_LAST_MODIFIED",	QVariant::DateTime));
	view.append(DbColumnEx("CENT_SYSTEM_TYPE_ID",		QVariant::Int));
	view.append(DbColumnEx("CENT_TOOLTIP",				QVariant::String));
	//view.append(DbColumnEx("CENT_IS_PRIVATE",			QVariant::Int));
	view.append(DbColumnEx("CET_ID",					QVariant::Int));
	view.append(DbColumnEx("CET_CODE",					QVariant::String));
	view.append(DbColumnEx("CET_NAME",					QVariant::String));
	view.append(DbColumnEx("BTKS_ID",					QVariant::Int));
	view.append(DbColumnEx("BTKS_IS_TASK_ACTIVE",		QVariant::Int));
	view.append(DbColumnEx("BTKS_SUBJECT",				QVariant::String));
	view.append(DbColumnEx("BTKS_START",				QVariant::DateTime));
	view.append(DbColumnEx("BTKS_DUE",					QVariant::DateTime));
	view.append(DbColumnEx("BTKS_COMPLETED",			QVariant::DateTime));
	view.append(DbColumnEx("BTKS_SYSTEM_STATUS",		QVariant::Int));
	view.append(DbColumnEx("BTKS_DESCRIPTION",			QVariant::String, true));
	view.append(DbColumnEx("BTKS_ORIGINATOR",			QVariant::Int));
	view.append(DbColumnEx("BTKS_NOTIFY_ORIGINATOR",	QVariant::Int));
	view.append(DbColumnEx("BTKS_PRIORITY",				QVariant::Int));
	view.append(DbColumnEx("BTKS_TASK_TYPE_ID",			QVariant::Int));
	view.append(DbColumnEx("BCMT_COLOR",				QVariant::String));
	view.append(DbColumnEx("BCMT_PICTURE",				QVariant::ByteArray));
	view.append(DbColumnEx("BDMD_ID",					QVariant::Int));
	view.append(DbColumnEx("BDMD_DAT_LAST_MODIFIED",	QVariant::DateTime));
	view.append(DbColumnEx("BDMD_NAME",					QVariant::String));
	view.append(DbColumnEx("BDMD_DESCRIPTION",			QVariant::String, true));
	view.append(DbColumnEx("BDMD_DOC_PATH",				QVariant::String, true));
	view.append(DbColumnEx("BDMD_APPLICATION_ID",		QVariant::Int));
	view.append(DbColumnEx("BDMD_OPEN_PARAMETER",		QVariant::String));
	view.append(DbColumnEx("BDMD_DOC_TYPE",				QVariant::Int));
	view.append(DbColumnEx("BDMD_LOCATION",				QVariant::String));
	view.append(DbColumnEx("BDMD_IS_CHECK_OUT",			QVariant::Int));
	view.append(DbColumnEx("BDMD_CHECK_OUT_USER_ID",	QVariant::Int));
	view.append(DbColumnEx("BDMD_TEMPLATE_FLAG",		QVariant::Int));
	view.append(DbColumnEx("BDMD_SIZE",					QVariant::Int));
	view.append(DbColumnEx("BDMD_READ_ONLY",			QVariant::Int));
	view.append(DbColumnEx("A.BPER_ID",					QVariant::Int));
	view.append(DbColumnEx("A.BPER_FIRST_NAME",			QVariant::String));
	view.append(DbColumnEx("A.BPER_LAST_NAME",			QVariant::String));
	view.append(DbColumnEx("B.BORG_ID",					QVariant::Int));
	view.append(DbColumnEx("B.BORG_NAME",				QVariant::String));
	view.append(DbColumnEx("B1.BORG_ID",				QVariant::Int));
	view.append(DbColumnEx("B1.BORG_NAME",				QVariant::String));
	view.append(DbColumnEx("BUSP_ID",					QVariant::Int));
	view.append(DbColumnEx("BUSP_CODE",					QVariant::String));
	view.append(DbColumnEx("BUSP_NAME",					QVariant::String));
	view.append(DbColumnEx("A1.BPER_FIRST_NAME",		QVariant::String));
	view.append(DbColumnEx("A1.BPER_LAST_NAME",			QVariant::String));
	view.append(DbColumnEx("A1.BPER_INITIALS",			QVariant::String));
	view.append(DbColumnEx("A2.BPER_ID",				QVariant::Int));
	view.append(DbColumnEx("A2.BPER_FIRST_NAME",		QVariant::String));
	view.append(DbColumnEx("A2.BPER_LAST_NAME",			QVariant::String));
	view.append(DbColumnEx("A2.BPER_INITIALS",			QVariant::String));
	view.append(DbColumnEx("BCNT_ID",					QVariant::Int));
	view.append(DbColumnEx("BCNT_LASTNAME",				QVariant::String));
	view.append(DbColumnEx("BCNT_FIRSTNAME",			QVariant::String));
	view.append(DbColumnEx("BCNT_ORGANIZATIONNAME",		QVariant::String));
	view.m_strSelectSQL = "SELECT " + getSQLColumnsFromView(view) +
						  " FROM BUS_DM_DOCUMENTS \
							INNER JOIN CE_COMM_ENTITY ON BDMD_COMM_ENTITY_ID = CENT_ID\
							LEFT OUTER JOIN BUS_TASKS  ON CENT_TASK_ID = BTKS_ID \
							LEFT OUTER JOIN CE_TYPE	   ON CENT_CE_TYPE_ID = CET_ID \
							LEFT OUTER JOIN BUS_PERSON A ON CENT_OWNER_ID = A.BPER_ID \
							LEFT OUTER JOIN BUS_ORGANIZATIONS B ON A.BPER_ORGANIZATION_ID = B.BORG_ID \
							LEFT OUTER JOIN BUS_PERSON A2 ON BTKS_OWNER = A2.BPER_ID \
							LEFT OUTER JOIN BUS_ORGANIZATIONS B1 ON A2.BPER_ORGANIZATION_ID = B1.BORG_ID \
							LEFT OUTER JOIN BUS_NMRX_RELATION ON BNMR_TABLE_KEY_ID_1 = CENT_ID AND BNMR_TABLE_1 =35 AND BNMR_TABLE_2=42 AND BNMR_TYPE=0\
							LEFT OUTER JOIN BUS_PERSON A1 ON BTKS_ORIGINATOR = A1.BPER_ID \
							LEFT OUTER JOIN BUS_CM_TYPES ON BCMT_ID = BTKS_TASK_TYPE_ID \
							LEFT OUTER JOIN BUS_PROJECTS ON BNMR_TABLE_KEY_ID_2 = BUSP_ID";
	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// TVIEW_COMM_GRID_LAST_N_DOCUMENT_VIEW
	//-----------------------
	view.m_nViewID		= TVIEW_COMM_GRID_LAST_N_DOCUMENT_VIEW;
	view.m_strTables	= "CE_COMM_ENTITY";
	view.m_nSkipLastCols = 0;
	view.append(DbColumnEx("CENT_ID",					QVariant::Int));
	view.m_strSelectSQL = "SELECT " + getSQLColumnsFromView(view) +
		" FROM BUS_DM_DOCUMENTS \
		INNER JOIN CE_COMM_ENTITY ON BDMD_COMM_ENTITY_ID = CENT_ID\
		LEFT OUTER JOIN BUS_TASKS  ON CENT_TASK_ID = BTKS_ID \
		LEFT OUTER JOIN CE_TYPE	   ON CENT_CE_TYPE_ID = CET_ID \
		LEFT OUTER JOIN BUS_PERSON A ON CENT_OWNER_ID = A.BPER_ID \
		LEFT OUTER JOIN BUS_ORGANIZATIONS B ON A.BPER_ORGANIZATION_ID = B.BORG_ID \
		LEFT OUTER JOIN BUS_PERSON A2 ON BTKS_OWNER = A2.BPER_ID \
		LEFT OUTER JOIN BUS_ORGANIZATIONS B1 ON A2.BPER_ORGANIZATION_ID = B1.BORG_ID \
		LEFT OUTER JOIN BUS_NMRX_RELATION ON BNMR_TABLE_KEY_ID_1 = CENT_ID AND BNMR_TABLE_1 =35 AND BNMR_TABLE_2=42 AND BNMR_TYPE=0\
		LEFT OUTER JOIN BUS_PERSON A1 ON BTKS_ORIGINATOR = A1.BPER_ID \
		LEFT OUTER JOIN BUS_PROJECTS ON BNMR_TABLE_KEY_ID_2 = BUSP_ID";
	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// TVIEW_CONTACT_COMM_GRID_DOCUMENT_VIEW
	//-----------------------
	view.m_nViewID		= TVIEW_CONTACT_COMM_GRID_DOCUMENT_VIEW;
	view.m_strTables	= "CE_COMM_ENTITY";
	view.m_nSkipLastCols = 4;
	view.append(DbColumnEx("CENT_ID",					QVariant::Int));
	view.append(DbColumnEx("CENT_DAT_LAST_MODIFIED",	QVariant::DateTime));
	view.append(DbColumnEx("CENT_SYSTEM_TYPE_ID",		QVariant::Int));
	view.append(DbColumnEx("CENT_TOOLTIP",				QVariant::String));
	//view.append(DbColumnEx("CENT_IS_PRIVATE",			QVariant::Int));
	view.append(DbColumnEx("CET_ID",					QVariant::Int));
	view.append(DbColumnEx("CET_CODE",					QVariant::String));
	view.append(DbColumnEx("CET_NAME",					QVariant::String));
	view.append(DbColumnEx("BTKS_ID",					QVariant::Int));
	view.append(DbColumnEx("BTKS_IS_TASK_ACTIVE",		QVariant::Int));
	view.append(DbColumnEx("BTKS_SUBJECT",				QVariant::String));
	view.append(DbColumnEx("BTKS_START",				QVariant::DateTime));
	view.append(DbColumnEx("BTKS_DUE",					QVariant::DateTime));
	view.append(DbColumnEx("BTKS_COMPLETED",			QVariant::DateTime));
	view.append(DbColumnEx("BTKS_SYSTEM_STATUS",		QVariant::Int));
	view.append(DbColumnEx("BTKS_DESCRIPTION",			QVariant::String, true));
	view.append(DbColumnEx("BTKS_ORIGINATOR",			QVariant::Int));
	view.append(DbColumnEx("BTKS_NOTIFY_ORIGINATOR",	QVariant::Int));
	view.append(DbColumnEx("BTKS_PRIORITY",				QVariant::Int));
	view.append(DbColumnEx("BTKS_TASK_TYPE_ID",			QVariant::Int));
	view.append(DbColumnEx("BCMT_COLOR",				QVariant::String));
	view.append(DbColumnEx("BCMT_PICTURE",				QVariant::ByteArray));
	view.append(DbColumnEx("BDMD_ID",					QVariant::Int));
	view.append(DbColumnEx("BDMD_DAT_LAST_MODIFIED",	QVariant::DateTime));
	view.append(DbColumnEx("BDMD_NAME",					QVariant::String));
	view.append(DbColumnEx("BDMD_DESCRIPTION",			QVariant::String, true));
	view.append(DbColumnEx("BDMD_DOC_PATH",				QVariant::String, true));
	view.append(DbColumnEx("BDMD_APPLICATION_ID",		QVariant::Int));
	view.append(DbColumnEx("BDMD_OPEN_PARAMETER",		QVariant::String));
	view.append(DbColumnEx("BDMD_DOC_TYPE",				QVariant::Int));
	view.append(DbColumnEx("BDMD_LOCATION",				QVariant::String));
	view.append(DbColumnEx("BDMD_IS_CHECK_OUT",			QVariant::Int));
	view.append(DbColumnEx("BDMD_CHECK_OUT_USER_ID",	QVariant::Int));
	view.append(DbColumnEx("BDMD_TEMPLATE_FLAG",		QVariant::Int));
	view.append(DbColumnEx("BDMD_SIZE",					QVariant::Int));
	view.append(DbColumnEx("BDMD_READ_ONLY",			QVariant::Int));
	view.append(DbColumnEx("A.BPER_ID",					QVariant::Int));
	view.append(DbColumnEx("A.BPER_FIRST_NAME",			QVariant::String));
	view.append(DbColumnEx("A.BPER_LAST_NAME",			QVariant::String));
	view.append(DbColumnEx("B.BORG_ID",					QVariant::Int));
	view.append(DbColumnEx("B.BORG_NAME",				QVariant::String));
	view.append(DbColumnEx("B1.BORG_ID",				QVariant::Int));
	view.append(DbColumnEx("B1.BORG_NAME",				QVariant::String));
	view.append(DbColumnEx("BUSP_ID",					QVariant::Int));
	view.append(DbColumnEx("BUSP_CODE",					QVariant::String));
	view.append(DbColumnEx("BUSP_NAME",					QVariant::String));
	view.append(DbColumnEx("A1.BPER_FIRST_NAME",		QVariant::String));
	view.append(DbColumnEx("A1.BPER_LAST_NAME",			QVariant::String));
	view.append(DbColumnEx("A1.BPER_INITIALS",			QVariant::String));
	view.append(DbColumnEx("A2.BPER_ID",				QVariant::Int));
	view.append(DbColumnEx("A2.BPER_FIRST_NAME",		QVariant::String));
	view.append(DbColumnEx("A2.BPER_LAST_NAME",			QVariant::String));
	view.append(DbColumnEx("A2.BPER_INITIALS",			QVariant::String));
	view.append(DbColumnEx("BCNT_ID",					QVariant::Int));
	view.append(DbColumnEx("BCNT_LASTNAME",				QVariant::String));
	view.append(DbColumnEx("BCNT_FIRSTNAME",			QVariant::String));
	view.append(DbColumnEx("BCNT_ORGANIZATIONNAME",		QVariant::String));
	view.m_strSelectSQL = "SELECT " + getSQLColumnsFromView(view) +
		" FROM BUS_DM_DOCUMENTS \
		INNER JOIN CE_COMM_ENTITY ON BDMD_COMM_ENTITY_ID = CENT_ID \
		INNER JOIN BUS_NMRX_RELATION K2 ON CENT_ID = K2.BNMR_TABLE_KEY_ID_1 AND K2.BNMR_TABLE_1 =35 AND K2.BNMR_TABLE_2=22 AND K2.BNMR_TYPE=0\
		LEFT OUTER JOIN BUS_TASKS  ON CENT_TASK_ID = BTKS_ID \
		LEFT OUTER JOIN CE_TYPE	   ON CENT_CE_TYPE_ID = CET_ID \
		LEFT OUTER JOIN BUS_PERSON A ON CENT_OWNER_ID = A.BPER_ID \
		LEFT OUTER JOIN BUS_ORGANIZATIONS B ON A.BPER_ORGANIZATION_ID = B.BORG_ID \
		LEFT OUTER JOIN BUS_PERSON A2 ON BTKS_OWNER = A2.BPER_ID \
		LEFT OUTER JOIN BUS_ORGANIZATIONS B1 ON A2.BPER_ORGANIZATION_ID = B1.BORG_ID \
		LEFT OUTER JOIN BUS_NMRX_RELATION K1 ON K1.BNMR_TABLE_KEY_ID_1 = CENT_ID AND K1.BNMR_TABLE_1 =35 AND K1.BNMR_TABLE_2=42 AND K1.BNMR_TYPE=0\
		LEFT OUTER JOIN BUS_PERSON A1 ON BTKS_ORIGINATOR = A1.BPER_ID \
		LEFT OUTER JOIN BUS_CM_TYPES ON BCMT_ID = BTKS_TASK_TYPE_ID \
		LEFT OUTER JOIN BUS_PROJECTS ON K1.BNMR_TABLE_KEY_ID_2 = BUSP_ID";
	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// TVIEW_PROJECT_COMM_GRID_DOCUMENT_VIEW
	//-----------------------
	view.m_nViewID		= TVIEW_PROJECT_COMM_GRID_DOCUMENT_VIEW;
	view.m_strTables	= "CE_COMM_ENTITY";
	view.m_nSkipLastCols = 4;
	view.append(DbColumnEx("CENT_ID",					QVariant::Int));
	view.append(DbColumnEx("CENT_DAT_LAST_MODIFIED",	QVariant::DateTime));
	view.append(DbColumnEx("CENT_SYSTEM_TYPE_ID",		QVariant::Int));
	view.append(DbColumnEx("CENT_TOOLTIP",				QVariant::String));
	//view.append(DbColumnEx("CENT_IS_PRIVATE",			QVariant::Int));
	view.append(DbColumnEx("CET_ID",					QVariant::Int));
	view.append(DbColumnEx("CET_CODE",					QVariant::String));
	view.append(DbColumnEx("CET_NAME",					QVariant::String));
	view.append(DbColumnEx("BTKS_ID",					QVariant::Int));
	view.append(DbColumnEx("BTKS_IS_TASK_ACTIVE",		QVariant::Int));
	view.append(DbColumnEx("BTKS_SUBJECT",				QVariant::String));
	view.append(DbColumnEx("BTKS_START",				QVariant::DateTime));
	view.append(DbColumnEx("BTKS_DUE",					QVariant::DateTime));
	view.append(DbColumnEx("BTKS_COMPLETED",			QVariant::DateTime));
	view.append(DbColumnEx("BTKS_SYSTEM_STATUS",		QVariant::Int));
	view.append(DbColumnEx("BTKS_DESCRIPTION",			QVariant::String, true));
	view.append(DbColumnEx("BTKS_ORIGINATOR",			QVariant::Int));
	view.append(DbColumnEx("BTKS_NOTIFY_ORIGINATOR",	QVariant::Int));
	view.append(DbColumnEx("BTKS_PRIORITY",				QVariant::Int));
	view.append(DbColumnEx("BTKS_TASK_TYPE_ID",			QVariant::Int));
	view.append(DbColumnEx("BCMT_COLOR",				QVariant::String));
	view.append(DbColumnEx("BCMT_PICTURE",				QVariant::ByteArray));
	view.append(DbColumnEx("BDMD_ID",					QVariant::Int));
	view.append(DbColumnEx("BDMD_DAT_LAST_MODIFIED",	QVariant::DateTime));
	view.append(DbColumnEx("BDMD_NAME",					QVariant::String));
	view.append(DbColumnEx("BDMD_DESCRIPTION",			QVariant::String, true));
	view.append(DbColumnEx("BDMD_DOC_PATH",				QVariant::String, true));
	view.append(DbColumnEx("BDMD_APPLICATION_ID",		QVariant::Int));
	view.append(DbColumnEx("BDMD_OPEN_PARAMETER",		QVariant::String));
	view.append(DbColumnEx("BDMD_DOC_TYPE",				QVariant::Int));
	view.append(DbColumnEx("BDMD_LOCATION",				QVariant::String));
	view.append(DbColumnEx("BDMD_IS_CHECK_OUT",			QVariant::Int));
	view.append(DbColumnEx("BDMD_CHECK_OUT_USER_ID",	QVariant::Int));
	view.append(DbColumnEx("BDMD_TEMPLATE_FLAG",		QVariant::Int));
	view.append(DbColumnEx("BDMD_SIZE",					QVariant::Int));
	view.append(DbColumnEx("A.BPER_ID",					QVariant::Int));
	view.append(DbColumnEx("A.BPER_FIRST_NAME",			QVariant::String));
	view.append(DbColumnEx("A.BPER_LAST_NAME",			QVariant::String));
	view.append(DbColumnEx("B.BORG_ID",					QVariant::Int));
	view.append(DbColumnEx("B.BORG_NAME",				QVariant::String));
	view.append(DbColumnEx("B1.BORG_ID",				QVariant::Int));
	view.append(DbColumnEx("B1.BORG_NAME",				QVariant::String));
	view.append(DbColumnEx("BUSP_ID",					QVariant::Int));
	view.append(DbColumnEx("BUSP_CODE",					QVariant::String));
	view.append(DbColumnEx("BUSP_NAME",					QVariant::String));
	view.append(DbColumnEx("A1.BPER_FIRST_NAME",		QVariant::String));
	view.append(DbColumnEx("A1.BPER_LAST_NAME",			QVariant::String));
	view.append(DbColumnEx("A1.BPER_INITIALS",			QVariant::String));
	view.append(DbColumnEx("A2.BPER_ID",				QVariant::Int));
	view.append(DbColumnEx("A2.BPER_FIRST_NAME",		QVariant::String));
	view.append(DbColumnEx("A2.BPER_LAST_NAME",			QVariant::String));
	view.append(DbColumnEx("A2.BPER_INITIALS",			QVariant::String));
	view.append(DbColumnEx("BCNT_ID",					QVariant::Int));
	view.append(DbColumnEx("BCNT_LASTNAME",				QVariant::String));
	view.append(DbColumnEx("BCNT_FIRSTNAME",			QVariant::String));
	view.append(DbColumnEx("BCNT_ORGANIZATIONNAME",		QVariant::String));
	view.m_strSelectSQL = "SELECT " + getSQLColumnsFromView(view) +
		" FROM BUS_DM_DOCUMENTS, \
		BUS_NMRX_RELATION, \
		CE_COMM_ENTITY \
		LEFT OUTER JOIN BUS_TASKS  ON CENT_TASK_ID = BTKS_ID \
		LEFT OUTER JOIN CE_TYPE	   ON CENT_CE_TYPE_ID = CET_ID \
		LEFT OUTER JOIN BUS_PERSON A ON CENT_OWNER_ID = A.BPER_ID \
		LEFT OUTER JOIN BUS_ORGANIZATIONS B ON A.BPER_ORGANIZATION_ID = B.BORG_ID \
		LEFT OUTER JOIN BUS_PERSON A2 ON BTKS_OWNER = A2.BPER_ID \
		LEFT OUTER JOIN BUS_ORGANIZATIONS B1 ON A2.BPER_ORGANIZATION_ID = B1.BORG_ID \
		LEFT OUTER JOIN BUS_PERSON A1 ON BTKS_ORIGINATOR = A1.BPER_ID \
		LEFT OUTER JOIN BUS_CM_TYPES ON BCMT_ID = BTKS_TASK_TYPE_ID \
		LEFT OUTER JOIN BUS_PROJECTS ON BNMR_TABLE_KEY_ID_2 = BUSP_ID";
	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// MVIEW_COMM_GRID_SORT_RECORDSET
	//-----------------------
	view.m_nViewID		= MVIEW_COMM_GRID_SORT_RECORDSET;
	view.append(DbColumnEx( "ROW_IN_TABLE",		QVariant::Int));
	view.append(DbColumnEx( "CONTACT",			QVariant::String));
	view.append(DbColumnEx( "PROJECT",			QVariant::String));
	view.append(DbColumnEx( "DATE_1",			QVariant::Date));
	view.append(DbColumnEx( "DATE_2",			QVariant::Date));
	view.append(DbColumnEx( "STATUS",			QVariant::Int));
	view.append(DbColumnEx( "APP_TYPE",			QVariant::Int));
	view.append(DbColumnEx( "SUBTYPE",			QVariant::Int));
	view.append(DbColumnEx( "MEDIA",			QVariant::Int));
	view.append(DbColumnEx( "ORIGINATOR",		QVariant::String));
	view.append(DbColumnEx( "OWNER",			QVariant::String));
	view.append(DbColumnEx( "TIME_1",			QVariant::Time));
	view.append(DbColumnEx( "TIME_2",			QVariant::Time));
	view.append(DbColumnEx( "COLOR",			QVariant::Int));
	view.append(DbColumnEx( "SUBJECT",			QVariant::String));
	view.append(DbColumnEx( "TASK_PRIORITY",	QVariant::Int));
	view.append(DbColumnEx( "TASK_TYPE_ID",		QVariant::Int));

	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// MVIEW_COMPLEX_COMM_FILTER
	//-----------------------
	view.m_nViewID		= MVIEW_COMPLEX_COMM_FILTER;
	view.append(DbColumnEx( "UNASSIGNEDVOICECALLS",							QVariant::Int));
	view.append(DbColumnEx( "MISSEDVOICECALLS",								QVariant::Int));
	view.append(DbColumnEx( "SCHEDULEDVOICECALLS",							QVariant::Int));
	view.append(DbColumnEx( "DUEVOICECALLS",								QVariant::Int));
	view.append(DbColumnEx( "OVERDUEVOICECALLS",							QVariant::Int));
	
	view.append(DbColumnEx( "UNREADEMAILS",									QVariant::Int));
	view.append(DbColumnEx( "UNASSIGNEDEMAILS",								QVariant::Int));
	view.append(DbColumnEx( "SCHEDULEDEMAILS",								QVariant::Int));
	view.append(DbColumnEx( "DUEEMAILS",									QVariant::Int));
	view.append(DbColumnEx( "OVERDUEEMAILS",								QVariant::Int));
	view.append(DbColumnEx( "TEMPLATESEMAILS",								QVariant::Int));

	view.append(DbColumnEx( "CHECKEDOUTDOCUMENTS",							QVariant::Int));
	view.append(DbColumnEx( "UNASSIGNEDDOCUMENTS",							QVariant::Int));
	view.append(DbColumnEx( "SCHEDULEDDOCUMENTS",							QVariant::Int));
	view.append(DbColumnEx( "DUEDOCUMENTS",									QVariant::Int));
	view.append(DbColumnEx( "OVERDUEDOCUMENTS",								QVariant::Int));
	view.append(DbColumnEx( "TEMPLATESDOCUMENTS",							QVariant::Int));

	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// MVIEW_CE_TYPES_FILTER_CHECKED_ITEMS
	//-----------------------
	view.m_nViewID		= MVIEW_CE_TYPES_FILTER_CHECKED_ITEMS;
	view.append(DbColumnEx( "CHECKED_ITEM_ID",								QVariant::Int));
	view.append(DbColumnEx( "CHECKED_ITEM_STATE",							QVariant::Int)); //Qt::CheckedState.

	s_lstDbViews.append(view);
	view.clear();

	//------------------------------
	// TVIEW_BUS_DM_REVISIONS
	//------------------------------

	view.m_nViewID		= TVIEW_BUS_DM_REVISIONS;
	view.m_nSkipFirstColsInsert	= 3;
	view.m_nSkipFirstColsUpdate	= 3;
	view.m_strTables="BUS_DM_REVISIONS";

	//Record set definition
	view.append(DbColumnEx("BDMR_ID",				QVariant::Int));
	view.append(DbColumnEx("BDMR_GLOBAL_ID",		QVariant::String));
	view.append(DbColumnEx("BDMR_DAT_LAST_MODIFIED",QVariant::DateTime));
	view.append(DbColumnEx("BDMR_DOCUMENT_ID",		QVariant::Int));
	view.append(DbColumnEx("BDMR_DAT_CHECK_IN",		QVariant::DateTime));
	view.append(DbColumnEx("BDMR_NAME",				QVariant::String));
	view.append(DbColumnEx("BDMR_REVISION_TAG",		QVariant::String));
	view.append(DbColumnEx("BDMR_CONTENT",			QVariant::ByteArray));
	view.append(DbColumnEx("BDMR_CHECK_IN_USER_ID", QVariant::Int));
	view.append(DbColumnEx("BDMR_IS_COMPRESSED",	QVariant::Int));
	view.append(DbColumnEx("BDMR_SIZE",				QVariant::Int));

	s_lstDbViews.append(view);
	view.clear();


	//------------------------------
	// TVIEW_BUS_DM_REVISIONS_SELECT (does not have content)
	//------------------------------

	view.m_nViewID		= TVIEW_BUS_DM_REVISIONS_SELECT;
	view.m_nSkipFirstColsInsert	= 3;
	view.m_nSkipFirstColsUpdate	= 3;
	view.m_nSkipLastCols=2;
	view.m_strTables="BUS_DM_REVISIONS";

	//Record set definition
	view.append(DbColumnEx("BDMR_ID",				QVariant::Int));
	view.append(DbColumnEx("BDMR_GLOBAL_ID",		QVariant::String));
	view.append(DbColumnEx("BDMR_DAT_LAST_MODIFIED",QVariant::DateTime));
	view.append(DbColumnEx("BDMR_DOCUMENT_ID",		QVariant::Int));
	view.append(DbColumnEx("BDMR_DAT_CHECK_IN",		QVariant::DateTime));
	view.append(DbColumnEx("BDMR_NAME",				QVariant::String));
	view.append(DbColumnEx("BDMR_REVISION_TAG",		QVariant::String));
	view.append(DbColumnEx("BDMR_CHECK_IN_USER_ID", QVariant::Int));
	view.append(DbColumnEx("BDMR_IS_COMPRESSED",	QVariant::Int));
	view.append(DbColumnEx("BDMR_SIZE",				QVariant::Int));
	view.append(DbColumnEx("BPER_FIRST_NAME",		QVariant::String));
	view.append(DbColumnEx("BPER_LAST_NAME",		QVariant::String));
	view.append(DbColumnEx("USER_NAME",				QVariant::String));
	view.append(DbColumnEx("SIZE",					QVariant::String));

	view.m_strSelectSQL="SELECT " +getSQLColumnsFromView(view)+" FROM BUS_DM_REVISIONS LEFT OUTER JOIN BUS_PERSON ON BDMR_CHECK_IN_USER_ID=BPER_ID";
	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// TVIEW_BUS_DM_CHECK_OUT_INFO
	//-----------------------
	view.m_nViewID		= TVIEW_BUS_DM_CHECK_OUT_INFO;
	view.m_nSkipFirstColsInsert	 = 3;
	view.m_nSkipFirstColsUpdate	 = 3;
	view.m_nSkipLastColsWrite	 = 2;
	view.m_strTables="BUS_DM_DOCUMENTS";
	view.append(DbColumnEx("BDMD_ID",					QVariant::Int));
	view.append(DbColumnEx("BDMD_GLOBAL_ID",			QVariant::String));
	view.append(DbColumnEx("BDMD_DAT_LAST_MODIFIED",	QVariant::DateTime));
	view.append(DbColumnEx("BDMD_IS_CHECK_OUT",			QVariant::Int));
	view.append(DbColumnEx("BDMD_CHECK_OUT_USER_ID",	QVariant::Int));
	view.append(DbColumnEx("BDMD_CHECK_OUT_DATE",		QVariant::DateTime));
	view.append(DbColumnEx("BDMD_DOC_PATH",				QVariant::String,true));
	view.append(DbColumnEx("BDMD_SIZE",					QVariant::Int));
	view.append(DbColumnEx("BDMD_APPLICATION_ID",		QVariant::Int));
	view.append(DbColumnEx("BPER_FIRST_NAME",			QVariant::String));
	view.append(DbColumnEx("BPER_LAST_NAME",			QVariant::String));
	view.m_strSelectSQL="SELECT " +getSQLColumnsFromView(view)+" FROM BUS_DM_DOCUMENTS LEFT OUTER JOIN BUS_PERSON ON BDMD_CHECK_OUT_USER_ID=BPER_ID";

	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// TVIEW_BUS_COMM_VIEW
	//-----------------------
	view.m_nViewID		= TVIEW_BUS_COMM_VIEW;
	view.m_nSkipFirstColsInsert	 = 3;
	view.m_nSkipFirstColsUpdate	 = 3;
	view.m_strTables="BUS_COMM_VIEW";
	view.append(DbColumnEx("BUSCV_ID",					QVariant::Int));
	view.append(DbColumnEx("BUSCV_GLOBAL_ID",			QVariant::String));
	view.append(DbColumnEx("BUSCV_DAT_LAST_MODIFIED",	QVariant::DateTime));
	view.append(DbColumnEx("BUSCV_NAME",				QVariant::String));
	view.append(DbColumnEx("BUSCV_OPEN_ON_STARTUP",		QVariant::Int));
	view.append(DbColumnEx("BUSCV_IS_PUBLIC",			QVariant::Int));
	view.append(DbColumnEx("BUSCV_TYPE",				QVariant::Int));
	view.append(DbColumnEx("BUSCV_OWNER_ID",			QVariant::Int));
	view.append(DbColumnEx("BUSCV_SORT_CODE",			QVariant::String));
	view.append(DbColumnEx("BUSCV_COMMGRIDVIEW_SORT",	QVariant::ByteArray));

	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// TVIEW_BUS_COMM_VIEW_SETTINGS
	//-----------------------
	view.m_nViewID		= TVIEW_BUS_COMM_VIEW_SETTINGS;
	view.m_nSkipFirstColsInsert	 = 3;
	view.m_nSkipFirstColsUpdate	 = 3;
	view.m_strTables="BUS_COMM_VIEW_SETTINGS";
	view.append(DbColumnEx("BUSCS_ID",					QVariant::Int));
	view.append(DbColumnEx("BUSCS_GLOBAL_ID",			QVariant::String));
	view.append(DbColumnEx("BUSCS_DAT_LAST_MODIFIED",	QVariant::DateTime));
	view.append(DbColumnEx("BUSCS_VIEW_ID",				QVariant::Int));
	view.append(DbColumnEx("BUSCS_SETTING_ID",			QVariant::Int));
	view.append(DbColumnEx("BUSCS_SETTING_TYPE",		QVariant::Int));
	view.append(DbColumnEx("BUSCS_VALUE",				QVariant::Int));
	view.append(DbColumnEx("BUSCS_VALUE_STRING",		QVariant::String));
	view.append(DbColumnEx("BUSCS_VALUE_BYTE",			QVariant::ByteArray));
	view.append(DbColumnEx("BUSCS_VALUE_DATETIME",		QVariant::DateTime));
	
	s_lstDbViews.append(view);
	view.clear();

	
	//-----------------------
	// MVIEW_EMAIL_RECIPIENTS
	//-----------------------
	view.m_nViewID		= MVIEW_EMAIL_RECIPIENTS;
	view.append(DbColumnEx("CONTACT_ID",				QVariant::Int));
	view.append(DbColumnEx("EMAIL_ID",					QVariant::Int));
	view.append(DbColumnEx("CONTACT_NAME",				QVariant::String));
	view.append(DbColumnEx("CONTACT_EMAIL",				QVariant::String));
	view.append(DbColumnEx("RECIPIENT_TYPE",			QVariant::Int));		//0 - To, 1 - CC, 2 - BCC.

	s_lstDbViews.append(view);
	view.clear();
	

	//-----------------------
	// TVIEW_BUS_PERSON_IMPORT
	//-----------------------
	view.m_nViewID		= TVIEW_BUS_PERSON_IMPORT;
	view.m_nSkipFirstColsInsert = 3;	//skip Id & global & last modify
	view.m_nSkipFirstColsUpdate = 3;	//skip Id
	view.m_strTables="BUS_PERSON";
	view.append(DbColumnEx("BPER_CODE",				QVariant::String));
	view.append(DbColumnEx("GENERATE_CONTACT",		QVariant::Int));
	view.append(DbColumnEx("BPER_CODE_EXT",			QVariant::String));
	view.append(DbColumnEx("BPER_FIRST_NAME",		QVariant::String));
	view.append(DbColumnEx("BPER_LAST_NAME",		QVariant::String));
	view.append(DbColumnEx("BPER_INITIALS",			QVariant::String));
	view.append(DbColumnEx("BORG_CODE",				QVariant::String));
	view.append(DbColumnEx("BDEPT_CODE",			QVariant::String));
	view.append(DbColumnEx("BPER_DATE_ENTERED",		QVariant::Date));
	view.append(DbColumnEx("BPER_DATE_LEFT",		QVariant::Date));
	view.append(DbColumnEx("BPER_ACTIVE_FLAG",		QVariant::Int));
	view.append(DbColumnEx("BPER_SEX",				QVariant::Int));
	view.append(DbColumnEx("BPER_DIRECT_PHONE",		QVariant::String));
	view.append(DbColumnEx("BPER_INSURANCE_ID",		QVariant::String));
	view.append(DbColumnEx("BPER_BIRTH_DATE",		QVariant::Date));
	view.append(DbColumnEx("BPER_OCCUPATION",		QVariant::Double));
	view.append(DbColumnEx("CLC_FUNCTION_CODE",		QVariant::String));	// ignored, no function table
	view.append(DbColumnEx("BPER_DESCRIPTION",		QVariant::String, true));

	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// MVIEW_REPORT_PROJECTS_VIEW
	//-----------------------
	view.m_nViewID		= MVIEW_REPORT_PROJECTS_VIEW;
	view.m_strTables="BUS_PROJECTS, BUS_PERSON";
	//Record set definition
	view.append(DbColumnEx("BUSP_ID",				QVariant::Int));
	view.append(DbColumnEx("BUSP_CODE",				QVariant::String));
	view.append(DbColumnEx("BUSP_LEVEL",			QVariant::Int));
	view.append(DbColumnEx("BUSP_PARENT",			QVariant::Int));
	view.append(DbColumnEx("BUSP_HASCHILDREN",		QVariant::Int));
	view.append(DbColumnEx("BUSP_NAME",				QVariant::String));
	view.append(DbColumnEx("BUSP_START_DATE",		QVariant::Date));
	view.append(DbColumnEx("BUSP_DEADLINE_DATE",	QVariant::Date));
	view.append(DbColumnEx("BUSP_COMPLETION_DATE",	QVariant::Date));
	view.append(DbColumnEx("BPER_ID",				QVariant::Int));
	view.append(DbColumnEx("BPER_FIRST_NAME",		QVariant::String));
	view.append(DbColumnEx("BPER_LAST_NAME",		QVariant::String));
	view.append(DbColumnEx("BPER_INITIALS",			QVariant::String));

	view.m_strSelectSQL = "SELECT " + getSQLColumnsFromView(view) +
						  " FROM BUS_PROJECTS \
							LEFT OUTER JOIN BUS_PERSON ON BUSP_LEADER_ID = BPER_ID ";
	s_lstDbViews.append(view);
	view.clear();

	//---------------------------------------------
	// MVIEW_COMMGRIDFILTER_WIZARD_VIEW
	//---------------------------------------------
	view.m_nViewID		= MVIEW_COMMGRIDFILTER_WIZARD_VIEW;
	view.append(DbColumnEx("VIEW_ID",					QVariant::Int));
	view.append(DbColumnEx("VIEW_DATA_RECORDSET",		QVariant::ByteArray));

	s_lstDbViews.append(view);
	view.clear();


	

	//-----------------------
	// TVIEW_CORE_FTP_SERVERS
	//-----------------------
	view.m_nViewID		= TVIEW_CORE_FTP_SERVERS;
	view.m_nSkipFirstColsInsert	= 3;
	view.m_nSkipFirstColsUpdate	= 3;
	view.m_strTables="CORE_FTP_SERVERS";

	//Record set definition
	view.append(DbColumnEx("CFTP_ID", QVariant::Int));
	view.append(DbColumnEx("CFTP_GLOBAL_ID", QVariant::String));
	view.append(DbColumnEx("CFTP_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("CFTP_HOST", QVariant::String));
	view.append(DbColumnEx("CFTP_PORT", QVariant::Int));
	view.append(DbColumnEx("CFTP_USER", QVariant::String));
	view.append(DbColumnEx("CFTP_PASSWORD", QVariant::String));
	view.append(DbColumnEx("CFTP_NAME", QVariant::String));
	view.append(DbColumnEx("CFTP_START_DIRECTORY", QVariant::String));
	view.append(DbColumnEx("CFTP_IS_PASSIVE_MODE", QVariant::Int));
	view.append(DbColumnEx("CFTP_USE_PROXY", QVariant::Int));
	view.append(DbColumnEx("CFTP_PROXY_HOST", QVariant::String));
	view.append(DbColumnEx("CFTP_PROXY_PORT", QVariant::Int));
	view.append(DbColumnEx("CFTP_PROXY_TYPE", QVariant::Int));
	view.append(DbColumnEx("CFTP_PROXY_USER", QVariant::String));
	view.append(DbColumnEx("CFTP_PROXY_PASSWORD", QVariant::String));

	s_lstDbViews.append(view);
	view.clear();


	//-----------------------
	// TVIEW_BUS_CM_CONTACTS
	//-----------------------

	view.m_nViewID		= TVIEW_BUS_CM_CONTACT_SELECT_SHORT;	
	view.m_nSkipFirstColsInsert = 3;
	view.m_nSkipFirstColsUpdate = 3;
	view.m_strTables="BUS_CM_CONTACT";

	//Record set definition
	view.append(DbColumnEx("BCNT_ID", QVariant::Int));
	view.append(DbColumnEx("BCNT_GLOBAL_ID", QVariant::String));
	view.append(DbColumnEx("BCNT_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("BCNT_TYPE", QVariant::Int));
	view.append(DbColumnEx("BCNT_SUPPRESS_MAILING", QVariant::Int));
	view.append(DbColumnEx("BCNT_DESCRIPTION", QVariant::String,true));
	view.append(DbColumnEx("BCNT_LASTNAME", QVariant::String));
	view.append(DbColumnEx("BCNT_FIRSTNAME", QVariant::String));
	view.append(DbColumnEx("BCNT_MIDDLENAME", QVariant::String));
	view.append(DbColumnEx("BCNT_NICKNAME", QVariant::String));
	view.append(DbColumnEx("BCNT_BIRTHDAY", QVariant::Date));
	view.append(DbColumnEx("BCNT_SEX", QVariant::Int));
	view.append(DbColumnEx("BCNT_PICTURE", QVariant::ByteArray));
	view.append(DbColumnEx("BCNT_DEPARTMENTNAME", QVariant::String));
	view.append(DbColumnEx("BCNT_ORGANIZATIONNAME", QVariant::String));
	view.append(DbColumnEx("BCNT_ORGANIZATIONNAME_2", QVariant::String));
	view.append(DbColumnEx("BPIC_PICTURE", QVariant::ByteArray));
	view.append(DbColumnEx("BPER_ID", QVariant::Int));
	view.m_strSelectSQL = "SELECT " + getSQLColumnsFromView(view) + " FROM BUS_CM_CONTACT LEFT OUTER JOIN BUS_BIG_PICTURE ON BCNT_BIG_PICTURE_ID = BPIC_ID ";
	view.m_strSelectSQL += " LEFT OUTER JOIN BUS_PERSON ON BCNT_ID=BPER_CONTACT_ID";
	
	s_lstDbViews.append(view);


	view.clear();


	//-----------------------
	// TVIEW_CORE_ACC_USER_REC
	//-----------------------

	view.m_nViewID		= TVIEW_CORE_ACC_USER_REC;	
	view.m_nSkipFirstColsInsert	= 3;
	view.m_nSkipFirstColsUpdate	= 3;
	view.m_strTables="CORE_ACC_USER_REC";

	//Record set definition
	view.append(DbColumnEx("CUAR_ID", QVariant::Int));
	view.append(DbColumnEx("CUAR_GLOBAL_ID", QVariant::String));
	view.append(DbColumnEx("CUAR_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("CUAR_TABLE_ID", QVariant::Int));
	view.append(DbColumnEx("CUAR_RECORD_ID", QVariant::Int));
	view.append(DbColumnEx("CUAR_PERSON_ID", QVariant::Int));
	view.append(DbColumnEx("CUAR_PARENT_ID", QVariant::Int));
	view.append(DbColumnEx("CUAR_CHILD_INHERIT", QVariant::Int));
	view.append(DbColumnEx("CUAR_CHILD_COMM_INHERIT", QVariant::Int));
	view.append(DbColumnEx("CUAR_GROUP_DEPENDENT", QVariant::Int));
	view.append(DbColumnEx("CUAR_RIGHT", QVariant::Int));

	s_lstDbViews.append(view);
	view.clear();


	//-----------------------
	// TVIEW_CORE_ACC_GROUP_REC
	//-----------------------

	view.m_nViewID		= TVIEW_CORE_ACC_GROUP_REC;	
	view.m_nSkipFirstColsInsert	= 3;
	view.m_nSkipFirstColsUpdate	= 3;
	view.m_strTables="CORE_ACC_GROUP_REC";

	//Record set definition
	view.append(DbColumnEx("CGAR_ID", QVariant::Int));
	view.append(DbColumnEx("CGAR_GLOBAL_ID", QVariant::String));
	view.append(DbColumnEx("CGAR_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("CGAR_TABLE_ID", QVariant::Int));
	view.append(DbColumnEx("CGAR_RECORD_ID", QVariant::Int));
	view.append(DbColumnEx("CGAR_GROUP_ID", QVariant::Int));
	view.append(DbColumnEx("CGAR_PARENT_ID", QVariant::Int));
	view.append(DbColumnEx("CGAR_CHILD_INHERIT", QVariant::Int));
	view.append(DbColumnEx("CGAR_CHILD_COMM_INHERIT", QVariant::Int));
	view.append(DbColumnEx("CGAR_RIGHT", QVariant::Int));

	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// TVIEW_BUS_STORED_PROJECT_LIST
	//-----------------------

	view.m_nViewID		= TVIEW_BUS_STORED_PROJECT_LIST;	
	view.m_nSkipFirstColsInsert	= 3;
	view.m_nSkipFirstColsUpdate	= 3;
	view.m_strTables="BUS_STORED_PROJECT_LIST";

	//Record set definition
	view.append(DbColumnEx("BSPL_ID", QVariant::Int));
	view.append(DbColumnEx("BSPL_GLOBAL_ID", QVariant::String));
	view.append(DbColumnEx("BSPL_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("BSPL_NAME", QVariant::String, true));
	view.append(DbColumnEx("BSPL_OWNER", QVariant::Int));
	view.append(DbColumnEx("BSPL_USEBY_PERSON_ID", QVariant::Int));
	view.append(DbColumnEx("BSPL_USEBY_DEPT_ID", QVariant::Int));
	view.append(DbColumnEx("BSPL_IS_SELECTION", QVariant::Int));
	view.append(DbColumnEx("BSPL_SELECTION_DATA", QVariant::String));
	view.append(DbColumnEx("BSPL_IS_FILTER", QVariant::Int));
	view.append(DbColumnEx("BSPL_FILTER_DATA", QVariant::String));
	

	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// TVIEW_BUS_STORED_PROJECT_LIST_DATA
	//-----------------------

	view.m_nViewID		= TVIEW_BUS_STORED_PROJECT_LIST_DATA;	
	view.m_nSkipFirstColsInsert	= 3;
	view.m_nSkipFirstColsUpdate	= 3;
	view.m_strTables="BUS_STORED_PROJECT_LIST_DATA";

	//Record set definition
	view.append(DbColumnEx("BSPLD_ID", QVariant::Int));
	view.append(DbColumnEx("BSPLD_GLOBAL_ID", QVariant::String));
	view.append(DbColumnEx("BSPLD_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("BSPLD_TREE_ID", QVariant::Int));
	view.append(DbColumnEx("BSPLD_PROJECT_ID", QVariant::Int));
	view.append(DbColumnEx("BSPLD_EXPANDED", QVariant::Int));

	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// TVIEW_BUS_SMS
	//-----------------------

	view.m_nViewID		= TVIEW_BUS_SMS;	
	view.m_nSkipFirstColsInsert	= 3;
	view.m_nSkipFirstColsUpdate	= 3;
	view.m_strTables="BUS_SMS";

	//Record set definition
	view.append(DbColumnEx("BSMS_ID", QVariant::Int));
	view.append(DbColumnEx("BSMS_GLOBAL_ID", QVariant::String));
	view.append(DbColumnEx("BSMS_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("BSMS_COMM_ENTITY_ID", QVariant::Int));
	view.append(DbColumnEx("BSMS_PROJECT_ID", QVariant::Int));
	view.append(DbColumnEx("BSMS_CONTACT_ID", QVariant::Int));
	view.append(DbColumnEx("BSMS_TEXT", QVariant::String, true));
	view.append(DbColumnEx("BSMS_PHONE_NUMBER", QVariant::String));
	view.append(DbColumnEx("BSMS_DATE_SENT", QVariant::DateTime));
	view.append(DbColumnEx("BSMS_TEMPLATE_FLAG", QVariant::Int));
	view.append(DbColumnEx("BSMS_TEMPLATE_NAME", QVariant::String));
	view.append(DbColumnEx("BSMS_TEMPLATE_DESCRIPTION", QVariant::String, true));
	view.append(DbColumnEx("BSMS_CATEGORY", QVariant::String));

	s_lstDbViews.append(view);
	view.clear();



	//-----------------------
	// BUS_CAL_RESERVATION
	//-----------------------

	view.m_nViewID		= TVIEW_BUS_CAL_RESERVATION;	
	view.m_nSkipFirstColsInsert	= 3;
	view.m_nSkipFirstColsUpdate	= 3;
	view.m_strTables="BUS_CAL_RESERVATION";

	//Record set definition
	view.append(DbColumnEx("BCRS_ID", QVariant::Int));
	view.append(DbColumnEx("BCRS_GLOBAL_ID", QVariant::String));
	view.append(DbColumnEx("BCRS_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("BCRS_IS_POSSIBLE", QVariant::Int));
	view.append(DbColumnEx("BCRS_WEEKDAY", QVariant::Int));
	view.append(DbColumnEx("BCRS_WEEKDAY_TIME_FROM", QVariant::DateTime));
	view.append(DbColumnEx("BCRS_WEEKDAY_TIME_TO", QVariant::DateTime));
	view.append(DbColumnEx("BCRS_IS_FREE", QVariant::Int));
	view.append(DbColumnEx("BCRS_FREE_FROM", QVariant::DateTime));
	view.append(DbColumnEx("BCRS_FREE_TO", QVariant::DateTime));
	view.append(DbColumnEx("BCRS_PROJECT_ID", QVariant::Int));
	view.append(DbColumnEx("BCRS_CONTACT_ID", QVariant::Int));
	view.append(DbColumnEx("BCRS_PERSON_ID", QVariant::Int));
	view.append(DbColumnEx("BCRS_RESOURCE_ID", QVariant::Int));

	s_lstDbViews.append(view);
	view.clear();


	//-----------------------
	// BUS_CAL_RESERVATION
	//-----------------------

	view.m_nViewID		= TVIEW_BUS_CAL_RESOURCE;	
	view.m_nSkipFirstColsInsert	= 3;
	view.m_nSkipFirstColsUpdate	= 3;
	view.m_strTables="BUS_CAL_RESOURCE";

	//Record set definition
	view.append(DbColumnEx("BRES_ID", QVariant::Int));
	view.append(DbColumnEx("BRES_GLOBAL_ID", QVariant::String));
	view.append(DbColumnEx("BRES_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("BRES_ORGANIZATION_ID", QVariant::Int));
	view.append(DbColumnEx("BRES_NAME", QVariant::String));
	view.append(DbColumnEx("BRES_VALID_FROM", QVariant::DateTime));
	view.append(DbColumnEx("BRES_VALID_TO", QVariant::DateTime));
	view.append(DbColumnEx("BRES_LOCATION", QVariant::String));
	view.append(DbColumnEx("BRES_DESCRIPTION", QVariant::String,true));

	s_lstDbViews.append(view);
	view.clear();
	
	//-----------------------
	// TVIEW_BUS_CAL_RESOURCE_SELECT
	//-----------------------

	view.m_nViewID		= TVIEW_BUS_CAL_RESOURCE_SELECT;	
	view.m_nSkipFirstColsInsert	= 0;
	view.m_nSkipFirstColsUpdate	= 0;
	view.m_strTables="BUS_CAL_RESOURCE";

	//Record set definition
	view.append(DbColumnEx("BRES_ID", QVariant::Int));
	view.append(DbColumnEx("BRES_ORGANIZATION_ID", QVariant::Int));
	view.append(DbColumnEx("BRES_NAME", QVariant::String));

	s_lstDbViews.append(view);
	view.clear();


	//-----------------------
	// TVIEW_BUS_CAL_INVITE
	//-----------------------

	view.m_nViewID		= TVIEW_BUS_CAL_INVITE;	
	view.m_nSkipFirstColsInsert	= 3;
	view.m_nSkipFirstColsUpdate	= 3;
	view.m_strTables="BUS_CAL_INVITE";

	//Record set definition
	view.append(DbColumnEx("BCIV_ID", QVariant::Int));
	view.append(DbColumnEx("BCIV_GLOBAL_ID", QVariant::String));
	view.append(DbColumnEx("BCIV_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("BCIV_INVITE", QVariant::Int));
	view.append(DbColumnEx("BCIV_IS_SENT", QVariant::Int));
	view.append(DbColumnEx("BCIV_STATUS", QVariant::Int));
	view.append(DbColumnEx("BCIV_BY_EMAIL", QVariant::Int));
	view.append(DbColumnEx("BCIV_BY_SMS", QVariant::Int));
	view.append(DbColumnEx("BCIV_BY_NOTIFICATION", QVariant::Int));
	view.append(DbColumnEx("BCIV_EMAIL_ADDRESS", QVariant::String));
	view.append(DbColumnEx("BCIV_SMS_NUMBERS", QVariant::String));
	view.append(DbColumnEx("BCIV_EMAIL_TEMPLATE_ID", QVariant::Int));
	view.append(DbColumnEx("BCIV_ANSWER_TEXT", QVariant::String,true));
	view.append(DbColumnEx("BCIV_OUID", QVariant::String));
	view.append(DbColumnEx("BCIV_SUBJECT_ID", QVariant::String));


	s_lstDbViews.append(view);
	//view.clear();
	/////WARNING---->don't put anything here, views are joined

	//-----------------------
	// TVIEW_BUS_CAL_INVITE_SELECT
	//-----------------------

	view.m_nViewID		= TVIEW_BUS_CAL_INVITE_SELECT;	
	view.m_nSkipFirstColsInsert	= 3;
	view.m_nSkipFirstColsUpdate	= 3;
	view.m_nSkipLastCols		= 5;
	view.m_nSkipLastColsWrite	= 5;
	view.m_strTables="BUS_CAL_INVITE";

	//Record set definition
	view.append(DbColumnEx("BCEP_ID", QVariant::Int));
	view.append(DbColumnEx("BCEP_COMM_ENTITY_ID", QVariant::Int));
	view.append(DbColumnEx("BPER_ID", QVariant::Int));
	view.append(DbColumnEx("CENT_OWNER_ID", QVariant::Int));
	view.append(DbColumnEx("RECIPIENT_CONTACT_ID", QVariant::Int));			// for mails I've gotta have contact and BPER can be NULL

	s_lstDbViews.append(view);
	view.clear();


	//-----------------------
	// TVIEW_BUS_CAL_INVITE_FIELDS
	//-----------------------

	view.m_nViewID		= TVIEW_BUS_CAL_INVITE_ICALENDAR_FIELDS;	
	view.m_nSkipFirstColsInsert	= 0;
	view.m_nSkipFirstColsUpdate	= 0;
	view.m_nSkipLastCols = 0;
	view.m_nSkipLastColsWrite =0;
	view.m_strTables="BUS_CAL_OPTIONS";

	//Record set definition
	view.append(DbColumnEx("BCEV_TITLE", QVariant::String));
	view.append(DbColumnEx("BCOL_FROM", QVariant::DateTime));
	view.append(DbColumnEx("BCOL_TO", QVariant::DateTime));
	view.append(DbColumnEx("BCOL_SUBJECT", QVariant::String));
	view.append(DbColumnEx("BCOL_LOCATION", QVariant::String));
	view.append(DbColumnEx("BCOL_DESCRIPTION", QVariant::String, true));
	view.append(DbColumnEx("CENT_CE_TYPE_ID", QVariant::Int));
	view.append(DbColumnEx("CENT_OWNER_ID", QVariant::Int)); 
	view.append(DbColumnEx("BCEV_CATEGORY", QVariant::String));
	view.append(DbColumnEx("BCEP_STATUS", QVariant::Int)); 
	view.append(DbColumnEx("BCIV_EMAIL_TEMPLATE_ID", QVariant::Int));
	view.append(DbColumnEx("BCIV_EMAIL_ADDRESS", QVariant::String));

	//fill before send:
	view.append(DbColumnEx("EMAIL_TEMPLATE_SUBJECT", QVariant::String));
	view.append(DbColumnEx("EMAIL_TEMPLATE_BODY", QVariant::String));
	view.append(DbColumnEx("OWNER_NAME", QVariant::String));
	view.append(DbColumnEx("TYPE_NAME", QVariant::String));

	//id's
	view.append(DbColumnEx("BCEV_ID", QVariant::Int));
	view.append(DbColumnEx("BCEP_ID", QVariant::Int));
	view.append(DbColumnEx("BCEP_COMM_ENTITY_ID", QVariant::Int));
	view.append(DbColumnEx("BCOL_ID", QVariant::Int));
	view.append(DbColumnEx("BCIV_ID", QVariant::Int));
	view.append(DbColumnEx("DB_UNIQUE_ID", QVariant::Int));
	//host/recipient:
	view.append(DbColumnEx("ORGANIZER_MAIL", QVariant::String));
	view.append(DbColumnEx("ORGANIZER_CONTACT_ID", QVariant::Int));
	view.append(DbColumnEx("RECIPIENT_MAIL", QVariant::String));
	view.append(DbColumnEx("RECIPIENT_CONTACT_ID", QVariant::Int));
	//generated/extern IDs:
	//view.append(DbColumnEx("OUTLOOK_UID", QVariant::String));
	view.append(DbColumnEx("BCIV_OUID", QVariant::String));
	view.append(DbColumnEx("BCIV_SUBJECT_ID", QVariant::String));
	//when parsing emails
	view.append(DbColumnEx("BCIV_ANSWER_TEXT", QVariant::String, true));
	view.append(DbColumnEx("BCIV_STATUS", QVariant::Int)); //GlobalConstants::STATUS_CAL_INVITE_CONFIRMED =1 , GlobalConstants::STATUS_CAL_INVITE_REJECTED =2
	view.append(DbColumnEx("BCIV_BY_SMS", QVariant::Int));
	view.append(DbColumnEx("BCIV_SMS_NUMBERS", QVariant::String));
	

	view.append(DbColumnEx("CONTACT_ASSIGN_LIST", DbRecordSet::GetVariantType())); //<IN DbSqlTableView::TVIEW_BUS_NMRX_RELATION

	s_lstDbViews.append(view);
	view.clear();


	//-----------------------
	// TVIEW_BUS_CAL_EVENT
	//-----------------------

	view.m_nViewID		= TVIEW_BUS_CAL_EVENT;	
	view.m_nSkipFirstColsInsert	= 3;
	view.m_nSkipFirstColsUpdate	= 3;
	view.m_strTables="BUS_CAL_EVENT";

	//Record set definition
	view.append(DbColumnEx("BCEV_ID", QVariant::Int));
	view.append(DbColumnEx("BCEV_GLOBAL_ID", QVariant::String));
	view.append(DbColumnEx("BCEV_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("BCEV_DAT_CREATED", QVariant::DateTime));
	view.append(DbColumnEx("BCEV_TITLE", QVariant::String));
	view.append(DbColumnEx("BCEV_FROM", QVariant::DateTime));
	view.append(DbColumnEx("BCEV_TO", QVariant::DateTime));
	view.append(DbColumnEx("BCEV_TEMPLATE_FLAG", QVariant::Int));
	view.append(DbColumnEx("BCEV_CATEGORY", QVariant::String));
	view.append(DbColumnEx("BCEV_TEMPLATE_ID", QVariant::Int));
	view.append(DbColumnEx("BCEV_DESCRIPTION", QVariant::String, true));
	view.append(DbColumnEx("BCEV_IS_PRIVATE", QVariant::Int));
	view.append(DbColumnEx("BCEV_IS_PRELIMINARY", QVariant::Int));
	view.append(DbColumnEx("BCEV_IS_INFORMATION", QVariant::Int));

	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// TVIEW_BUS_CAL_EVENT_PART
	//-----------------------

	view.m_nViewID		= TVIEW_BUS_CAL_EVENT_PART;	
	view.m_nSkipFirstColsInsert	= 3;
	view.m_nSkipFirstColsUpdate	= 3;
	view.m_strTables="BUS_CAL_EVENT_PART";

	//Record set definition
	view.append(DbColumnEx("BCEP_ID", QVariant::Int));
	view.append(DbColumnEx("BCEP_GLOBAL_ID", QVariant::String));
	view.append(DbColumnEx("BCEP_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("BCEP_EVENT_ID", QVariant::Int));
	view.append(DbColumnEx("BCEP_COMM_ENTITY_ID", QVariant::Int));
	view.append(DbColumnEx("BCEP_STATUS", QVariant::Int));
	view.append(DbColumnEx("BCEP_PRESENCE_TYPE_ID", QVariant::Int));
	view.append(DbColumnEx("BCEP_TYPE_ID", QVariant::Int));
	view.append(DbColumnEx("BCEP_DESCRIPTION", QVariant::String, true));
	view.append(DbColumnEx("BCEP_ASSIGN_NEW_CRP", QVariant::Int));
	

	s_lstDbViews.append(view);
	//view.clear();
	/////WARNING---->don't put anything here, views are joined

	//-----------------------
	// TVIEW_BUS_CAL_EVENT_PART_COMM_ENTITY  ++com entity fields
	//-----------------------
	view.m_nViewID		= TVIEW_BUS_CAL_EVENT_PART_COMM_ENTITY;
	view.m_nSkipLastCols = 6;
	view.m_nSkipLastColsWrite =13;
	view.append(DbColumnEx("CENT_ID",					QVariant::Int));
	view.append(DbColumnEx("CENT_GLOBAL_ID",			QVariant::String));
	view.append(DbColumnEx("CENT_DAT_LAST_MODIFIED",	QVariant::DateTime));
	view.append(DbColumnEx("CENT_CE_TYPE_ID",			QVariant::Int));
	view.append(DbColumnEx("CENT_SYSTEM_TYPE_ID",		QVariant::Int));
	view.append(DbColumnEx("CENT_OWNER_ID",				QVariant::Int));
	view.append(DbColumnEx("CENT_TASK_ID",				QVariant::Int));

	view.append(DbColumnEx("OPTION_LIST", DbRecordSet::GetVariantType()));
	view.append(DbColumnEx("NMRX_PERSON_LIST", DbRecordSet::GetVariantType()));
	view.append(DbColumnEx("NMRX_PROJECT_LIST", DbRecordSet::GetVariantType()));
	view.append(DbColumnEx("NMRX_RESOURCE_LIST", DbRecordSet::GetVariantType()));
	view.append(DbColumnEx("PART_FROM", QVariant::DateTime)); //use for sort
	view.append(DbColumnEx("PART_TO", QVariant::DateTime));

	view.m_strSelectSQL="SELECT " +getSQLColumnsFromView(view)+" FROM BUS_CAL_EVENT_PART INNER JOIN CE_COMM_ENTITY ON CENT_ID=BCEP_COMM_ENTITY_ID";

	s_lstDbViews.append(view);
	view.clear();


	//-----------------------
	// TVIEW_BUS_CAL_EV_PART_OUID
	//-----------------------

	view.m_nViewID		= TVIEW_BUS_CAL_EV_PART_OUID;	
	view.m_nSkipFirstColsInsert	= 3;
	view.m_nSkipFirstColsUpdate	= 3;
	view.m_strTables="BUS_CAL_EV_PART_OUID";

	//Record set definition
	view.append(DbColumnEx("BCPOD_ID", QVariant::Int));
	view.append(DbColumnEx("BCPOD_GLOBAL_ID", QVariant::String));
	view.append(DbColumnEx("BCPOD_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("BCPOD_PART_ID", QVariant::Int));
	view.append(DbColumnEx("BCPOD_OUTLOOK_ID", QVariant::String));

	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// TVIEW_BUS_CAL_OPTIONS
	//-----------------------

	view.m_nViewID		= TVIEW_BUS_CAL_OPTIONS;	
	view.m_nSkipFirstColsInsert	= 3;
	view.m_nSkipFirstColsUpdate	= 3;
	view.m_strTables="BUS_CAL_OPTIONS";

	//Record set definition
	view.append(DbColumnEx("BCOL_ID", QVariant::Int));
	view.append(DbColumnEx("BCOL_GLOBAL_ID", QVariant::String));
	view.append(DbColumnEx("BCOL_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("BCOL_EVENT_PART_ID", QVariant::Int));
	view.append(DbColumnEx("BCOL_FROM", QVariant::DateTime));
	view.append(DbColumnEx("BCOL_TO", QVariant::DateTime));
	view.append(DbColumnEx("BCOL_IS_ACTIVE", QVariant::Int));
	view.append(DbColumnEx("BCOL_SUBJECT", QVariant::String));
	view.append(DbColumnEx("BCOL_LOCATION", QVariant::String));
	view.append(DbColumnEx("BCOL_DESCRIPTION", QVariant::String, true));

	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// TVIEW_BUS_CAL_BREAKS
	//-----------------------

	view.m_nViewID		= TVIEW_BUS_CAL_BREAKS;	
	view.m_nSkipFirstColsInsert	= 3;
	view.m_nSkipFirstColsUpdate	= 3;
	view.m_strTables="BUS_CAL_BREAKS";

	//Record set definition
	view.append(DbColumnEx("BCBL_ID", QVariant::Int));
	view.append(DbColumnEx("BCBL_GLOBAL_ID", QVariant::String));
	view.append(DbColumnEx("BCBL_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("BCBL_FROM", QVariant::DateTime));
	view.append(DbColumnEx("BCBL_TO", QVariant::DateTime));
	view.append(DbColumnEx("BCBL_OPTIONS_ID", QVariant::Int));
	view.append(DbColumnEx("BCBL_NAME", QVariant::String));
	
	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// TVIEW_BUS_CAL_EVENT_SELECT
	//-----------------------

	view.m_nViewID		= TVIEW_BUS_CAL_EVENT_SELECT;	
	view.m_nSkipFirstColsInsert	= 3;
	view.m_nSkipFirstColsUpdate	= 3;
	view.m_strTables="BUS_CAL_EVENT";

	//Record set definition
	view.append(DbColumnEx("BCEV_ID", QVariant::Int));
	view.append(DbColumnEx("BCEV_GLOBAL_ID", QVariant::String));
	view.append(DbColumnEx("BCEV_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("BCEV_DAT_CREATED", QVariant::DateTime));
	view.append(DbColumnEx("BCEV_TITLE", QVariant::String));
	view.append(DbColumnEx("BCEV_FROM", QVariant::DateTime));
	view.append(DbColumnEx("BCEV_TO", QVariant::DateTime));
	view.append(DbColumnEx("BCEV_TEMPLATE_FLAG", QVariant::Int));
	view.append(DbColumnEx("BCEV_CATEGORY", QVariant::String));
	view.append(DbColumnEx("BCEV_TEMPLATE_ID", QVariant::Int));

	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// TVIEW_BUS_CAL_OPTIONS_SELECT
	//-----------------------

	view.m_nViewID		= TVIEW_BUS_CAL_OPTIONS_SELECT;	
	view.m_nSkipFirstColsInsert	= 3;
	view.m_nSkipFirstColsUpdate	= 3;
	view.m_nSkipLastCols = 1;
	view.m_nSkipLastColsWrite =1;
	view.m_strTables="BUS_CAL_OPTIONS";

	//Record set definition
	view.append(DbColumnEx("BCOL_ID", QVariant::Int));
	view.append(DbColumnEx("BCOL_GLOBAL_ID", QVariant::String));
	view.append(DbColumnEx("BCOL_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("BCOL_EVENT_PART_ID", QVariant::Int));
	view.append(DbColumnEx("BCOL_FROM", QVariant::DateTime));
	view.append(DbColumnEx("BCOL_TO", QVariant::DateTime));
	view.append(DbColumnEx("BCOL_IS_ACTIVE", QVariant::Int));
	view.append(DbColumnEx("BCOL_SUBJECT", QVariant::String));
	view.append(DbColumnEx("BCOL_LOCATION", QVariant::String));
	view.append(DbColumnEx("BCOL_DESCRIPTION", QVariant::String, true));
	view.append(DbColumnEx("BREAK_LIST", DbRecordSet::GetVariantType()));

	s_lstDbViews.append(view);
	view.clear();



	//-----------------------
	// MVIEW_CALENDAR_EVENT_PARTS
	//-----------------------
	//INNER JOIN BUS_CAL_OPTIONS ON BCOL_EVENT_PART_ID = BCEP_ID AND BCOL_IS_ACTIVE = 1
	view.m_nViewID		= MVIEW_CALENDAR_EVENT_PARTS;
	view.m_strTables	= "CE_COMM_ENTITY";
	view.append(DbColumnEx("CENT_ID",					QVariant::Int));
	view.append(DbColumnEx("BCEP_ID",					QVariant::Int));
	view.append(DbColumnEx("BCEP_STATUS",				QVariant::Int));
	view.append(DbColumnEx("BCEV_ID",					QVariant::Int));
	view.append(DbColumnEx("BCEV_TITLE",				QVariant::String));
	view.append(DbColumnEx("BCEV_IS_INFORMATION",		QVariant::Int));
	view.append(DbColumnEx("BCOL_ID",					QVariant::Int));
	view.append(DbColumnEx("BCOL_SUBJECT",				QVariant::String));
	view.append(DbColumnEx("BCOL_FROM",					QVariant::DateTime));
	view.append(DbColumnEx("BCOL_TO",					QVariant::DateTime));
	view.append(DbColumnEx("BCOL_LOCATION",				QVariant::String));
	view.append(DbColumnEx("BCOL_EVENT_PART_ID",		QVariant::Int));
	view.append(DbColumnEx("CENT_OWNER_ID",				QVariant::Int));		//Owner.
	view.append(DbColumnEx("BPER_FIRST_NAME",			QVariant::String));
	view.append(DbColumnEx("BPER_LAST_NAME",			QVariant::String));
	view.append(DbColumnEx("BPER_CONTACT_ID",			QVariant::Int));		//Owner contact ID.
	view.append(DbColumnEx("BCOL_DESCRIPTION",			QVariant::String, true));		//Description.
	view.append(DbColumnEx("BCMT_COLOR",				QVariant::String));		//Type.
	view.append(DbColumnEx("CET_ICON_BINARY",			QVariant::ByteArray));
	view.m_strSelectSQL = "SELECT " + getSQLColumnsFromView(view) +
		" FROM BUS_CAL_EVENT_PART \
		INNER JOIN BUS_CAL_OPTIONS ON BCOL_EVENT_PART_ID = BCEP_ID  \
		INNER JOIN CE_COMM_ENTITY ON CENT_ID = BCEP_COMM_ENTITY_ID \
		INNER JOIN BUS_PERSON ON CENT_OWNER_ID = BPER_ID \
		INNER JOIN BUS_CAL_EVENT ON BCEV_ID = BCEP_EVENT_ID \
		INNER JOIN BUS_NMRX_RELATION ON BNMR_TABLE_1 = 35 AND BNMR_TABLE_KEY_ID_1 = CENT_ID \
		LEFT OUTER JOIN BUS_CM_TYPES ON BCMT_ID = BCEP_PRESENCE_TYPE_ID \
		LEFT OUTER JOIN CE_TYPE ON CET_ID = CENT_CE_TYPE_ID \
		";
	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// MVIEW_CALENDAR_ONE_EVENT_PARTS
	//-----------------------
	//INNER JOIN BUS_CAL_OPTIONS ON BCOL_EVENT_PART_ID = BCEP_ID AND BCOL_IS_ACTIVE = 1
	view.m_nViewID		= MVIEW_CALENDAR_ONE_EVENT_PARTS;
	view.m_strTables	= "CE_COMM_ENTITY";
	view.append(DbColumnEx("CENT_ID",					QVariant::Int));
	view.append(DbColumnEx("BCEP_ID",					QVariant::Int));
	view.append(DbColumnEx("BCEP_STATUS",				QVariant::Int));
	view.append(DbColumnEx("BCEV_ID",					QVariant::Int));
	view.append(DbColumnEx("BCEV_TITLE",				QVariant::String));
	view.append(DbColumnEx("BCEV_IS_INFORMATION",		QVariant::Int));
	view.append(DbColumnEx("BCOL_ID",					QVariant::Int));
	view.append(DbColumnEx("BCOL_SUBJECT",				QVariant::String));
	view.append(DbColumnEx("BCOL_FROM",					QVariant::DateTime));
	view.append(DbColumnEx("BCOL_TO",					QVariant::DateTime));
	view.append(DbColumnEx("BCOL_LOCATION",				QVariant::String));
	view.append(DbColumnEx("BCOL_EVENT_PART_ID",		QVariant::Int));
	view.append(DbColumnEx("CENT_OWNER_ID",				QVariant::Int));		//Owner.
	view.append(DbColumnEx("BPER_FIRST_NAME",			QVariant::String));
	view.append(DbColumnEx("BPER_LAST_NAME",			QVariant::String));
	view.append(DbColumnEx("BPER_CONTACT_ID",			QVariant::Int));		//Owner contact ID.
	view.append(DbColumnEx("BCOL_DESCRIPTION",			QVariant::String, true));		//Description.
	view.append(DbColumnEx("BCMT_COLOR",				QVariant::String));		//Type.
	view.append(DbColumnEx("CET_ICON_BINARY",			QVariant::ByteArray));
	view.m_strSelectSQL = "SELECT " + getSQLColumnsFromView(view) +
		" FROM BUS_CAL_EVENT_PART \
		INNER JOIN BUS_CAL_OPTIONS ON BCOL_EVENT_PART_ID = BCEP_ID  \
		INNER JOIN CE_COMM_ENTITY ON CENT_ID = BCEP_COMM_ENTITY_ID \
		INNER JOIN BUS_PERSON ON CENT_OWNER_ID = BPER_ID \
		INNER JOIN BUS_CAL_EVENT ON BCEV_ID = BCEP_EVENT_ID \
		LEFT OUTER JOIN BUS_CM_TYPES ON BCMT_ID = BCEP_PRESENCE_TYPE_ID \
		LEFT OUTER JOIN CE_TYPE ON CET_ID = CENT_CE_TYPE_ID \
		";
	s_lstDbViews.append(view);
	view.clear();
	
	//-----------------------
	// MVIEW_CALENDAR_FILTER_VIEW
	//-----------------------
	view.m_nViewID		= MVIEW_CALENDAR_FILTER_VIEW;
	view.m_strTables	= "BUS_CM_CONTACT ";
	view.append(DbColumnEx("SHOW_PRELIMINARY_EVENTS",	QVariant::Int));
	view.append(DbColumnEx("SHOW_INFORMATION_EVENTS",	QVariant::Int));
	view.append(DbColumnEx("FILTER_BY_CALENDAR_TYPE",	QVariant::Int));
	view.append(DbColumnEx("CALENDAR_TYPE_RECORDSET",	QVariant::ByteArray));
	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// MVIEW_CALENDAR_EVENT_CONTACT_VIEW
	//-----------------------
	view.m_nViewID		= MVIEW_CALENDAR_EVENT_CONTACT_VIEW;
	view.m_strTables	= "BUS_CM_CONTACT, BUS_NMRX_RELATION, CE_COMM_ENTITY ";
	view.append(DbColumnEx("CENT_ID",					QVariant::Int));
	view.append(DbColumnEx("BCNT_ID",					QVariant::Int));
	view.append(DbColumnEx("BCNT_LASTNAME",				QVariant::String));
	view.append(DbColumnEx("BCNT_FIRSTNAME",			QVariant::String));
	view.append(DbColumnEx("BCNT_ORGANIZATIONNAME",		QVariant::String));
	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// MVIEW_CALENDAR_EVENT_PROJECT_VIEW
	//-----------------------
	view.m_nViewID		= MVIEW_CALENDAR_EVENT_PROJECT_VIEW;
	view.m_strTables	= "BUS_PROJECTS, BUS_NMRX_RELATION, CE_COMM_ENTITY ";
	view.append(DbColumnEx("CENT_ID",					QVariant::Int));
	view.append(DbColumnEx("BUSP_ID",					QVariant::Int));
	view.append(DbColumnEx("BUSP_NAME",					QVariant::String));
	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// MVIEW_CALENDAR_EVENT_RESOURCE_VIEW
	//-----------------------
	view.m_nViewID		= MVIEW_CALENDAR_EVENT_RESOURCE_VIEW;
	view.m_strTables	= "BUS_CAL_RESOURCE, BUS_NMRX_RELATION, CE_COMM_ENTITY ";
	view.append(DbColumnEx("CENT_ID",					QVariant::Int));
	view.append(DbColumnEx("BRES_ID",					QVariant::Int));
	view.append(DbColumnEx("BRES_NAME",					QVariant::String));
	s_lstDbViews.append(view);
	view.clear();
	
	//-----------------------
	// MVIEW_CALENDAR_EVENT_BREAK_VIEW
	//-----------------------
	view.m_nViewID		= MVIEW_CALENDAR_EVENT_BREAK_VIEW;
	view.m_strTables	= "BUS_CAL_OPTIONS, BUS_CAL_BREAKS ";
	view.append(DbColumnEx("BCBL_ID",					QVariant::Int));
	view.append(DbColumnEx("BCBL_FROM",					QVariant::DateTime));
	view.append(DbColumnEx("BCBL_TO",					QVariant::DateTime));
	s_lstDbViews.append(view);
	view.clear();
	
	//-----------------------
	// MVIEW_CALENDAR_ENTITY_VIEW
	//-----------------------
	view.m_nViewID		= MVIEW_CALENDAR_ENTITY_VIEW;
	view.m_strTables	= "BUS_PERSON ";
	view.append(DbColumnEx("ENTITY_ID",					QVariant::Int));
	view.append(DbColumnEx("ENTITY_NAME",				QVariant::String));
	view.append(DbColumnEx("ENTITY_TYPE",				QVariant::Int));
	view.append(DbColumnEx("ENTITY_PERSON_ID",			QVariant::Int));
	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// MVIEW_CALENDAR_ITEM_ASSIGNED_RESOURCES_VIEW
	//-----------------------
	view.m_nViewID		= MVIEW_CALENDAR_ITEM_ASSIGNED_RESOURCES_VIEW;
	view.m_strTables	= "BUS_PERSON ";
	view.append(DbColumnEx("BPER_CONTACT_ID",			QVariant::Int));	//Owner.
	view.append(DbColumnEx("BCNT_ID",					QVariant::Int));	//Contacts.
	view.append(DbColumnEx("BUSP_ID",					QVariant::Int));	//Projects.
	view.append(DbColumnEx("BRES_ID",					QVariant::Int));	//Resources.
	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// MVIEW_BUS_CAL_RESERVATION
	//-----------------------
	view.m_nViewID		= MVIEW_BUS_CAL_RESERVATION;
	view.m_strTables	= "BUS_CAL_RESERVATION ";
	view.append(DbColumnEx("BCRS_ID",					QVariant::Int));
	view.append(DbColumnEx("DATE_FROM",					QVariant::DateTime));
	view.append(DbColumnEx("DATE_TO",					QVariant::DateTime));
	view.append(DbColumnEx("BCRS_IS_POSSIBLE",			QVariant::Int));
	view.append(DbColumnEx("BCRS_IS_FREE",				QVariant::Int));
	s_lstDbViews.append(view);
	view.clear();




	//-----------------------
	// TVIEW_WEBSERVICE_BUS_CM_PHONE_SELECT
	//-----------------------
	view.m_nViewID		= TVIEW_WEBSERVICE_BUS_CM_PHONE_SELECT;
	view.m_nSkipFirstColsInsert = 3;
	view.m_nSkipFirstColsUpdate = 3;
	view.m_nSkipLastColsWrite = 1;
	view.m_strTables="BUS_CM_PHONE";

	//Record set definition
	view.append(DbColumnEx("BCMP_ID", QString("Phone_ID"),QVariant::Int));
	view.append(DbColumnEx("BCMP_NAME", QString("Name"),QVariant::String));
	view.append(DbColumnEx("BCMP_FULLNUMBER", QString("PhoneNumber"),QVariant::String));
	view.append(DbColumnEx("BCMP_SEARCH", QString("PhoneNumber_Truncated"),QVariant::String));
	view.append(DbColumnEx("BCMP_IS_DEFAULT", QString("IsDefault"),QVariant::Int));
	view.append(DbColumnEx("BCMT_TYPE_NAME", QString("Type"),QVariant::String));
	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// TVIEW_WEBSERVICE_BUS_CM_SKYPE_SELECT
	//-----------------------
	view.m_nViewID		= TVIEW_WEBSERVICE_BUS_CM_SKYPE_SELECT;
	view.m_nSkipFirstColsInsert = 3;
	view.m_nSkipFirstColsUpdate = 3;
	view.m_nSkipLastColsWrite = 1;
	view.m_strTables="BUS_CM_PHONE";

	//Record set definition
	view.append(DbColumnEx("BCMP_ID", QString("Phone_ID"),QVariant::Int));
	view.append(DbColumnEx("BCMP_NAME", QString("Name"),QVariant::String));
	view.append(DbColumnEx("BCMP_FULLNUMBER", QString("SykpeUserName"),QVariant::String));
	view.append(DbColumnEx("BCMP_IS_DEFAULT", QString("IsDefault"),QVariant::Int));
	view.append(DbColumnEx("BCMT_TYPE_NAME", QString("Type"),QVariant::String));
	s_lstDbViews.append(view);
	view.clear();



	//-----------------------
	// TVIEW_WEBSERVICE_BUS_CM_EMAIL_SELECT
	//-----------------------
	view.m_nViewID		= TVIEW_WEBSERVICE_BUS_CM_EMAIL_SELECT;
	view.m_nSkipFirstColsInsert = 3;
	view.m_nSkipFirstColsUpdate = 3;
	view.m_nSkipLastColsWrite = 1;
	view.m_strTables="BUS_CM_EMAIL";

	//Record set definition
	view.append(DbColumnEx("BCME_ID", QString("Email_ID"),QVariant::Int));
	view.append(DbColumnEx("BCME_NAME", QString("Name"),QVariant::String));
	view.append(DbColumnEx("BCME_ADDRESS", QString("EmailAddress"),QVariant::String));
	view.append(DbColumnEx("BCME_IS_DEFAULT", QString("IsDefault"),QVariant::Int));
	view.append(DbColumnEx("BCMT_TYPE_NAME", QString("Type"),QVariant::String));
	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// TVIEW_WEBSERVICE_BUS_CM_INTERNET_SELECT
	//-----------------------
	view.m_nViewID		= TVIEW_WEBSERVICE_BUS_CM_INTERNET_SELECT;
	view.m_nSkipFirstColsInsert = 3;
	view.m_nSkipFirstColsUpdate = 3;
	view.m_nSkipLastColsWrite = 1;
	view.m_strTables="BUS_CM_INTERNET";

	//Record set definition
	view.append(DbColumnEx("BCMI_ID", QString("Web_ID"),QVariant::Int));
	view.append(DbColumnEx("BCMI_NAME", QString("Name"),QVariant::String));
	view.append(DbColumnEx("BCMI_ADDRESS",QString("WebAddress"),QVariant::String));
	view.append(DbColumnEx("BCMI_IS_DEFAULT", QString("IsDefault"),QVariant::Int));
	view.append(DbColumnEx("BCMT_TYPE_NAME", QString("Type"),QVariant::String));
	s_lstDbViews.append(view);
	view.clear();


	//-----------------------
	// TVIEW_WEBSERVICE_BUS_CM_ADDRESS_SELECT
	//-----------------------
	view.m_nViewID		= TVIEW_WEBSERVICE_BUS_CM_ADDRESS_SELECT;
	view.m_nSkipLastCols = 2; //read
	view.m_nSkipLastColsWrite = 2; //write
	view.m_strTables="BUS_CM_ADDRESS";

	//Record set definition
	view.append(DbColumnEx("BCMA_ID", QString("Address_ID"),QVariant::Int));
	view.append(DbColumnEx("BCMA_FORMATEDADDRESS", QString("CompleteAddress"),QVariant::String,true));
	view.append(DbColumnEx("BCMA_IS_DEFAULT", QString("IsDefault"),QVariant::Int));
	view.append(DbColumnEx("BCMT_TYPE_NAME", QString("Type"),QVariant::String));
	view.append(DbColumnEx("BCMA_COUNTRY_CODE", QString("CountryCode"),QVariant::String));
	view.append(DbColumnEx("BCMA_COUNTRY_NAME", QString("CountryName"),QVariant::String));
	view.append(DbColumnEx("BCMA_STREET", QString("Street"),QVariant::String));
	view.append(DbColumnEx("BCMA_CITY", QString("City"),QVariant::String));
	view.append(DbColumnEx("BCMA_ZIP", QString("ZIP"),QVariant::String));
	view.append(DbColumnEx("ADDITIONAL_ADDRESS_TYPE_NAMES", QString("AdditonalTypes"),QVariant::String,false,"Comma delimited additonal address types"));

	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// MVIEW_CALENDAR_PERSONAL_SETTINGS
	//-----------------------
	view.m_nViewID		= MVIEW_CALENDAR_PERSONAL_SETTINGS;
	view.m_strTables	= " ";
	view.append(DbColumnEx("FILTER_BY_NO_TYPE",			QVariant::Int));
	view.append(DbColumnEx("FILTER_BY_TYPE",			QVariant::Int));
	view.append(DbColumnEx("SHOW_PRELIMINARY",			QVariant::Int));
	view.append(DbColumnEx("SHOW_INFORMATION",			QVariant::Int));
	view.append(DbColumnEx("CALENDAR_TIME_SCALE",		QVariant::Int));
	view.append(DbColumnEx("CALENDAR_DATE_RANGE",		QVariant::Int));
	view.append(DbColumnEx("CALENDAR_SCROLL_TO_TIME",	QVariant::Time));
	view.append(DbColumnEx("MULTIDAY_CALENDAR_HEIGHT",	QVariant::ByteArray));
	view.append(DbColumnEx("TASK_TYPE_CHECKED_ITEMS",	QVariant::ByteArray));	//MVIEW_CE_TYPES_FILTER_CHECKED_ITEMS.
	view.append(DbColumnEx("CALENDAR_ENTITIES",			QVariant::ByteArray));	//MVIEW_CALENDAR_ENTITY_VIEW.
	view.append(DbColumnEx("CALENDAR_CURRENT_ENTITY",	QVariant::Int));
	view.append(DbColumnEx("CALENDAR_COLUMN_WIDTH",		QVariant::Int));
	view.append(DbColumnEx("CALENDAR_FUI_GEOMETRY",		QVariant::ByteArray));
	view.append(DbColumnEx("CALENDAR_FUI_POSITION_X",	QVariant::Int));
	view.append(DbColumnEx("CALENDAR_FUI_POSITION_Y",	QVariant::Int));
	view.append(DbColumnEx("CALENDAR_SHOW_TASKS",		QVariant::Int));
	view.append(DbColumnEx("CALENDAR_SECTIONS",			QVariant::Int));	//0-SHOW BOTH, 1-SHOW UPPER, 2-SHOW LOWER.
	view.append(DbColumnEx("CALENDAR_OVERVIEW_LEFT_COLUMN_WIDTH", QVariant::Int));	//Width of the upper calendar left column.
	s_lstDbViews.append(view);
	view.clear();


	//-----------------------
	// TVIEW_WEBSERVICE_BUS_DM_DOCUMENT
	//-----------------------
	view.m_nViewID		= TVIEW_WEBSERVICE_BUS_DM_DOCUMENT_SELECT;
	view.m_nSkipFirstColsInsert	= 3;
	view.m_nSkipFirstColsUpdate	= 3;
	view.m_nSkipLastCols = 1;
	view.m_strTables="BUS_DM_DOCUMENTS";

	//Record set definition
	view.append(DbColumnEx("BDMD_ID",QString("Document_ID"), QVariant::Int));
	view.append(DbColumnEx("BDMD_DAT_LAST_MODIFIED",QString("LastModified"), QVariant::DateTime));
	view.append(DbColumnEx("BDMD_DAT_CREATED",QString("Created"), QVariant::DateTime));
	view.append(DbColumnEx("BDMD_NAME",QString("Name"), QVariant::String));
	view.append(DbColumnEx("BDMD_DOC_PATH",QString("LastPath"), QVariant::String,true));
	view.append(DbColumnEx("BDMD_DOC_TYPE",QString("nDocType"), QVariant::Int));
	view.append(DbColumnEx("BDMD_SIZE",	QString("Size"),	QVariant::Int));
	view.append(DbColumnEx("BPER_INITIALS",QString("Owner"),	QVariant::String));
	view.append(DbColumnEx("CEVT_NAME",QString("UserDocumentTypeName"),		QVariant::String));
	view.append(DbColumnEx("EXTENSION",QString("Extension"),	QVariant::String));
	
	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// TVIEW_WEBSERVICE_BUS_DM_DOCUMENT_DETAILS
	//-----------------------
	view.m_nViewID		= TVIEW_WEBSERVICE_BUS_DM_DOCUMENT_DETAILS;
	view.m_nSkipFirstColsInsert	= 3;
	view.m_nSkipFirstColsUpdate	= 3;
	view.m_strTables="BUS_DM_DOCUMENTS";

	//Record set definition
	view.append(DbColumnEx("BDMD_ID",QString("Document_ID"), QVariant::Int));
	view.append(DbColumnEx("BDMD_DAT_LAST_MODIFIED",QString("LastModified"), QVariant::DateTime));
	view.append(DbColumnEx("BDMD_DAT_CREATED",QString("Created"), QVariant::DateTime));
	view.append(DbColumnEx("BDMD_NAME",QString("Name"), QVariant::String));
	view.append(DbColumnEx("BDMD_DOC_PATH",QString("LastPath"), QVariant::String,true));
	view.append(DbColumnEx("BDMD_DOC_TYPE",QString("nDocType"), QVariant::Int));
	view.append(DbColumnEx("BDMD_SIZE",	QString("Size"),	QVariant::Int));
	view.append(DbColumnEx("BDMD_DESCRIPTION", QString("TextField"),QVariant::String,true));
	view.append(DbColumnEx("BPER_INITIALS",QString("Owner"),	QVariant::String));
	view.append(DbColumnEx("CEVT_NAME",QString("UserDocumentTypeName"),		QVariant::String));
	
	s_lstDbViews.append(view);
	view.clear();


	//-----------------------
	// TVIEW_WEBSERVICE_BUS_EMAIL_SELECT
	//-----------------------
	view.m_nViewID		= TVIEW_WEBSERVICE_BUS_EMAIL_SELECT;
	view.m_nSkipFirstColsInsert	= 3;
	view.m_nSkipFirstColsUpdate	= 3;
	view.m_nSkipLastCols = 5;
	view.m_strTables="BUS_EMAIL";

	//Record set definition
	view.append(DbColumnEx("BEM_ID",QString("Email_ID"), QVariant::Int));
	view.append(DbColumnEx("BEM_DAT_LAST_MODIFIED",QString("LastModified"), QVariant::DateTime));
	view.append(DbColumnEx("BEM_RECV_TIME",QString("SendRecvDateTime"), QVariant::DateTime));
	view.append(DbColumnEx("BEM_PROJECT_ID",QString("Project_ID"), QVariant::Int));
	view.append(DbColumnEx("BEM_FROM",QString("From"), QVariant::String, true));
	view.append(DbColumnEx("BEM_TO",QString("To"), QVariant::String, true));
	view.append(DbColumnEx("BEM_CC",QString("CC"),	QVariant::String, true));
	view.append(DbColumnEx("BEM_BCC",QString("BCC"),	QVariant::String, true));
	view.append(DbColumnEx("BEM_SUBJECT",QString("Subject"),		QVariant::String));
	view.append(DbColumnEx("BEM_OUTGOING",QString("IsOutgoing"),	QVariant::Int));
	view.append(DbColumnEx("BEM_REPLY_TO",			QVariant::String));	
	view.append(DbColumnEx("BEM_TRUE_FROM",			QVariant::String));
	view.append(DbColumnEx("BEM_TO_EMAIL_ACCOUNT",	QVariant::String));
	view.append(DbColumnEx("BEM_UNREAD_FLAG",		QVariant::Int));	
	view.append(DbColumnEx("BEM_MAILBOX_TYPE",QString("MailBox"),		QVariant::Int));
	view.append(DbColumnEx("BEM_REMINDER_DUE_DATE",	QString("DueDate"), QVariant::DateTime));
	view.append(DbColumnEx("BEM_REMINDER_TYPE",	QString("ReminderType"),QVariant::String));
	view.append(DbColumnEx("CENT_ID",QString("Email_Comm_ID"), QVariant::Int));
	view.append(DbColumnEx("CENT_TOOLTIP",QString("Body_Preview"),	QVariant::String));
	
	view.append(DbColumnEx("CONTACT_FROM_ID",QString("Contact_From_ID"),	QVariant::Int));
	view.append(DbColumnEx("CONTACT_FROM_NAME",QString("Contact_From_Name"),		QVariant::String));
	view.append(DbColumnEx("CONTACT_TO_ID",QString("Contact_To_ID"),	QVariant::Int));
	view.append(DbColumnEx("CONTACT_TO_NAME",QString("Contact_To_Name"),QVariant::String));
	view.append(DbColumnEx("NUMBER_OF_ATTACHMENTS",QString("NumberOfAttachments"),	QVariant::Int));

	s_lstDbViews.append(view);
	view.clear();


	//-----------------------
	// TVIEW_WEBSERVICE_BUS_EMAIL_ATTACHMENT_LIST
	//-----------------------
	view.m_nViewID		= TVIEW_WEBSERVICE_BUS_EMAIL_ATTACHMENT_LIST;
	view.m_nSkipFirstColsInsert	= 3;
	view.m_nSkipFirstColsUpdate	= 3;
	view.m_nSkipLastCols = 3;
	view.m_strTables="BUS_EMAIL_ATTACHMENT";

	//Record set definition
	view.append(DbColumnEx("BEA_ID",QString("Attachment_ID"), QVariant::Int));
	view.append(DbColumnEx("BEA_NAME",QString("Name"), QVariant::String));
	view.append(DbColumnEx("EXT",QString("Extension"), QVariant::String));
	view.append(DbColumnEx("IS_ZIP",QString("IsZipped"), QVariant::Int));
	view.append(DbColumnEx("URL",QString("Url"), QVariant::String));
	
	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// TVIEW_WEBSERVICE_BUS_PROJECT_LIST
	//-----------------------

	view.m_nViewID		= TVIEW_WEBSERVICE_BUS_PROJECT_LIST;	
	view.m_nSkipFirstColsInsert	= 3;
	view.m_nSkipFirstColsUpdate	= 3;
	view.m_strTables="BUS_STORED_PROJECT_LIST";

	//Record set definition
	view.append(DbColumnEx("BSPL_ID", QString("List_ID"),QVariant::Int));
	view.append(DbColumnEx("BSPL_NAME", QString("List_Name"), QVariant::String, true));

	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// TVIEW_WEBSERVICE_BUS_PROJECT_SELECT
	//-----------------------

	view.m_nViewID		= TVIEW_WEBSERVICE_BUS_PROJECT_SELECT;	
	view.m_nSkipFirstColsInsert	= 3;
	view.m_nSkipFirstColsUpdate	= 3;
	view.m_strTables="BUS_PROJECTS";

	//Record set definition
	view.append(DbColumnEx("BUSP_ID", QString("Project_ID"),QVariant::Int));
	view.append(DbColumnEx("BUSP_CODE", QString("Code"), QVariant::String));
	view.append(DbColumnEx("BUSP_NAME", QString("Name"), QVariant::String));
	view.append(DbColumnEx("BUSP_EXPANDED", QString("Expanded"), QVariant::Int));
	view.append(DbColumnEx("BUSP_PARENT", QString("Parent_Project_ID"), QVariant::Int));
	view.append(DbColumnEx("BUSP_HASCHILDREN", QString("HasChildren"), QVariant::Int));
	

	s_lstDbViews.append(view);
	view.clear();


	//-----------------------
	// TVIEW_WEBSERVICE_BUS_COMM_VIEWS
	//-----------------------
	view.m_nViewID		= TVIEW_WEBSERVICE_BUS_COMM_VIEWS;
	view.m_strTables="BUS_COMM_VIEW";

	//Record set definition
	view.append(DbColumnEx("BUSCV_ID",QString("Id"), QVariant::Int));
	view.append(DbColumnEx("BUSCV_NAME",QString("Name"), QVariant::String));
	view.append(DbColumnEx("DATE_RANGE",QString("Date_Range"), QVariant::Int));
	view.append(DbColumnEx("USE_FROM_TO",QString("Use_From_To"), QVariant::Int));
	view.append(DbColumnEx("DATE_FROM",QString("Date_From"), QVariant::DateTime));
	view.append(DbColumnEx("DATE_TO",QString("Date_To"), QVariant::DateTime));

	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// TVIEW_WEBSERVICE_BUS_COMM_DATA
	//-----------------------
	view.m_nViewID		= TVIEW_WEBSERVICE_BUS_COMM_DATA;
	view.m_strTables="BUS_COMM_VIEW";

	//Record set definition
	
	view.append(DbColumnEx("CENT_ID",QString("ID"), QVariant::Int));
	view.append(DbColumnEx("BTKS_ID",QString("BTKS_ID"), QVariant::Int));
	view.append(DbColumnEx("Color",QString("Color"), QVariant::Int));
	view.append(DbColumnEx("Pix1",QString("Pix1"), QVariant::String));
	view.append(DbColumnEx("Pix2",QString("Pix2"), QVariant::String));
	view.append(DbColumnEx("Pix3",QString("Pix3"), QVariant::String));
	view.append(DbColumnEx("Pix4",QString("Pix4"), QVariant::String));
	view.append(DbColumnEx("Date1",QString("Date1"), QVariant::String));
	view.append(DbColumnEx("Date2",QString("Date2"), QVariant::String));
	view.append(DbColumnEx("Autor",QString("Autor"), QVariant::String));
	view.append(DbColumnEx("Subject",QString("Subject"), QVariant::String));
	view.append(DbColumnEx("Description",QString("Description"), QVariant::String));
	view.append(DbColumnEx("SecondColDisplay",QString("SecondColDisplay"), QVariant::String));
	view.append(DbColumnEx("Owner",QString("Owner"), QVariant::String));
	view.append(DbColumnEx("Contact",QString("Contact"), QVariant::String));
	view.append(DbColumnEx("Project",QString("Project"), QVariant::String));
	view.append(DbColumnEx("TaskColor",QString("TaskColor"), QVariant::String));
	view.append(DbColumnEx("TaskIcon",QString("TaskIcon"), QVariant::ByteArray));
	view.append(DbColumnEx("TaskPriority",QString("TaskPriority"), QVariant::Int));
	view.append(DbColumnEx("Pix4ByteArray",QString("Pix4ByteArray"), QVariant::ByteArray));
	view.append(DbColumnEx("Document_Type",QString("Document_Type"), QVariant::Int));
	view.append(DbColumnEx("Document_ID",QString("Document_ID"), QVariant::Int));
	view.append(DbColumnEx("EXTENSION",QString("Extension"),	QVariant::String));
	view.append(DbColumnEx("URL_PATH",QString("Url_Path"),	QVariant::String));
	


	s_lstDbViews.append(view);
	view.clear();

	
	

	//-----------------------
	// TVIEW_BUS_USER_CONTACT_RELATIONSHIP_IMPORT
	//-----------------------
	view.m_nViewID		= TVIEW_BUS_USER_CONTACT_RELATIONSHIP_IMPORT;
	view.m_nSkipFirstColsInsert = 3;	//skip Id & global & last modify
	view.m_nSkipFirstColsUpdate = 3;	//skip Id
	view.m_strTables="";
	view.append(DbColumnEx("BCNT_FIRSTNAME", QVariant::String));
	view.append(DbColumnEx("BCNT_LASTNAME", QVariant::String));
	view.append(DbColumnEx("BCNT_ORGANIZATIONNAME", QVariant::String));
	view.append(DbColumnEx("BPER_FIRST_NAME",		QVariant::String));
	view.append(DbColumnEx("BPER_LAST_NAME",		QVariant::String));
	view.append(DbColumnEx("BPER_CODE",				QVariant::String));
	view.append(DbColumnEx("BPER_INITIALS",			QVariant::String));
	view.append(DbColumnEx("BUCR_ROLE",				QVariant::String));
	view.append(DbColumnEx("BUCR_DESCRIPTION",		QVariant::String,true));

	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// TVIEW_BUS_CONTACT_CONTACT_RELATIONSHIP_IMPORT
	//-----------------------
	view.m_nViewID		= TVIEW_BUS_CONTACT_CONTACT_RELATIONSHIP_IMPORT;
	view.m_nSkipFirstColsInsert = 3;	//skip Id & global & last modify
	view.m_nSkipFirstColsUpdate = 3;	//skip Id
	view.m_strTables="";
	view.append(DbColumnEx("BCNT1_FIRSTNAME", QVariant::String));
	view.append(DbColumnEx("BCNT1_LASTNAME", QVariant::String));
	view.append(DbColumnEx("BCNT1_ORGANIZATIONNAME", QVariant::String));
	view.append(DbColumnEx("BCNT2_FIRSTNAME", QVariant::String));
	view.append(DbColumnEx("BCNT2_LASTNAME", QVariant::String));
	view.append(DbColumnEx("BCNT2_ORGANIZATIONNAME", QVariant::String));
	view.append(DbColumnEx("BUCR_ROLE",				QVariant::String));
	view.append(DbColumnEx("BUCR_DESCRIPTION",		QVariant::String,true));

	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// TVIEW_BUS_CONTACT_CONTACT_RELATIONSHIP_EXPORT
	//
	//  Do not change the order of the fields!!! OR else ...
	//-----------------------
	view.m_nViewID		= TVIEW_BUS_CONTACT_CONTACT_RELATIONSHIP_EXPORT;	
	view.m_strTables="";
	
	view.append(DbColumnEx("CLC_IS_ADD_ON", QVariant::Int));	//"Follow-up" record
	
	//---------contact--------------
	view.append(DbColumnEx("BCNT_FIRSTNAME", QVariant::String));
	view.append(DbColumnEx("BCNT_LASTNAME", QVariant::String));
	view.append(DbColumnEx("BCNT_MIDDLENAME", QVariant::String));
	view.append(DbColumnEx("BCNT_NICKNAME", QVariant::String));
	view.append(DbColumnEx("BCNT_BIRTHDAY", QVariant::Date));
	view.append(DbColumnEx("BCNT_SEX", QVariant::Int));
	view.append(DbColumnEx("BCNT_DEPARTMENTNAME", QVariant::String));
	view.append(DbColumnEx("BCNT_PROFESSION", QVariant::String));
	view.append(DbColumnEx("BCNT_FUNCTION", QVariant::String));
	view.append(DbColumnEx("BCNT_ORGANIZATIONNAME", QVariant::String));
	view.append(DbColumnEx("BCNT_FOUNDATIONDATE", QVariant::Date));
	view.append(DbColumnEx("BCNT_DESCRIPTION", QVariant::String, true));
	view.append(DbColumnEx("BCNT_LANGUAGECODE", QVariant::String));
	view.append(DbColumnEx("BCNT_SUPPRESS_MAILING", QVariant::Int));
	view.append(DbColumnEx("BCNT_OLD_CODE", QVariant::String));		//external code

	//---------address--------------
	view.append(DbColumnEx("BCMA_FORMATEDADDRESS", QVariant::String,true));
	view.append(DbColumnEx("BCMA_FIRSTNAME", QVariant::String));
	view.append(DbColumnEx("BCMA_LASTNAME", QVariant::String));
	view.append(DbColumnEx("BCMA_MIDDLENAME", QVariant::String));
	view.append(DbColumnEx("BCMA_TITLE", QVariant::String));
	view.append(DbColumnEx("BCMA_SALUTATION", QVariant::String));
	view.append(DbColumnEx("BCMA_ORGANIZATIONNAME", QVariant::String));
	view.append(DbColumnEx("BCMA_ORGANIZATIONNAME_2", QVariant::String));
	view.append(DbColumnEx("BCMA_STREET", QVariant::String));
	view.append(DbColumnEx("BCMA_ZIP", QVariant::String));
	view.append(DbColumnEx("BCMA_CITY", QVariant::String));
	view.append(DbColumnEx("BCMA_COUNTRY_CODE", QVariant::String));
	view.append(DbColumnEx("BCMA_COUNTRY_NAME", QVariant::String));
	view.append(DbColumnEx("BCMA_REGION", QVariant::String));
	view.append(DbColumnEx("BCMA_POBOX", QVariant::String));
	view.append(DbColumnEx("BCMA_POBOXZIP", QVariant::String));
	view.append(DbColumnEx("BCMA_OFFICECODE", QVariant::String));

	//---------calculated fields--------------
	view.append(DbColumnEx("CLC_EMAIL_ADDRESS", QVariant::String));
	view.append(DbColumnEx("CLC_EMAIL_ADDRESS_DESC", QVariant::String));
	view.append(DbColumnEx("CLC_WEB_ADDRESS", QVariant::String));
	view.append(DbColumnEx("CLC_WEB_ADDRESS_DESC", QVariant::String));
	view.append(DbColumnEx("CLC_PHONE_BUSINESS_CENTRAL", QVariant::String));
	view.append(DbColumnEx("CLC_PHONE_BUSINESS_DIRECT", QVariant::String));
	view.append(DbColumnEx("CLC_PHONE_MOBILE", QVariant::String));
	view.append(DbColumnEx("CLC_PHONE_FAX", QVariant::String));
	view.append(DbColumnEx("CLC_PHONE_PRIVATE", QVariant::String));
	view.append(DbColumnEx("CLC_PHONE_PRIVATE_MOBILE", QVariant::String));
	view.append(DbColumnEx("CLC_PHONE_SKYPE", QVariant::String));
	view.append(DbColumnEx("CLC_PHONE_GENERIC",		 QVariant::String));
	view.append(DbColumnEx("CLC_PHONE_GENERIC_NAME", QVariant::String));

	//-------------- journal --------------------
    view.append(DbColumnEx("BCMJ_PERSON_CODE", QVariant::String));
    view.append(DbColumnEx("BCMJ_DATE", QVariant::Date));
    view.append(DbColumnEx("BCMJ_TEXT", QVariant::String, true));
    view.append(DbColumnEx("BCMJ_IS_PRIVATE", QVariant::Int));
    view.append(DbColumnEx("CLC_GROUPS", QVariant::String));

	//-------------- debtor --------------------
	view.append(DbColumnEx("BCMD_DEBTORCODE", QVariant::String));
	view.append(DbColumnEx("BCMD_DEBTORACCOUNT", QVariant::String));
	view.append(DbColumnEx("BCMD_CUSTOMERCODE", QVariant::String));
	view.append(DbColumnEx("BCMD_DESCRIPTION", QVariant::String,true));
	view.append(DbColumnEx("BCMD_INVOICE_TEXT", QVariant::String, true));

	// bus person
	view.append(DbColumnEx("BPER_CODE", QVariant::String));
	view.append(DbColumnEx("BPER_CODE_EXT", QVariant::String));
	view.append(DbColumnEx("BPER_INITIALS", QVariant::String));
	view.append(DbColumnEx("BPER_ACTIVE_FLAG", QVariant::Int));
	view.append(DbColumnEx("BPER_DATE_ENTERED", QVariant::Date));
	view.append(DbColumnEx("BPER_DATE_LEFT", QVariant::Date));
	view.append(DbColumnEx("BPER_ORGANIZATION_CODE", QVariant::String));
	view.append(DbColumnEx("BPER_DEPARTMENT_CODE", QVariant::String));
	view.append(DbColumnEx("BPER_INSURANCE_ID", QVariant::String));
	view.append(DbColumnEx("BPER_OCCUPATION", QVariant::Double));

	//-------------- role data --------------------
	view.append(DbColumnEx("BUCR_ROLE", QVariant::String));
	view.append(DbColumnEx("BCNT2_FIRSTNAME", QVariant::String));
	view.append(DbColumnEx("BCNT2_LASTNAME", QVariant::String));
	view.append(DbColumnEx("BCNT2_ORGANIZATIONNAME", QVariant::String));
	view.append(DbColumnEx("BUCR_DESCRIPTION",		QVariant::String,true));

	view.append(DbColumnEx("BUCR_ROLE_DIRECTION", QVariant::String));	// one character field, ">" or "<"

	view.append(DbColumnEx("BCMA_SHORT_SALUTATION", QVariant::String));	//issue #2419
	
	// unclear - not defined in doc
	view.append(DbColumnEx("BCNT_ORGANIZATIONNAME_2", QVariant::String));
	s_lstDbViews.append(view);
	view.clear();


	//-----------------------
	// TVIEW_BUS_REMINDERS
	//-----------------------
	view.m_nViewID	= TVIEW_BUS_REMINDERS;
	view.m_nSkipFirstColsInsert	= 3;
	view.m_nSkipFirstColsUpdate	= 3;
	view.m_strTables="BUS_REMINDERS";

	//Record set definition
	view.append(DbColumnEx("BRE_ID", QVariant::Int));
	view.append(DbColumnEx("BRE_GLOBAL_ID", QVariant::String));
	view.append(DbColumnEx("BRE_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("BRE_EVENT_TABLE_ID", QVariant::Int));
	view.append(DbColumnEx("BRE_EVENT_RECORD_ID", QVariant::Int));
	view.append(DbColumnEx("BRE_EVENT_DATA", QVariant::String));
	view.append(DbColumnEx("BRE_SUBJECT", QVariant::String));
	view.append(DbColumnEx("BRE_CATEGORY", QVariant::Int));
	view.append(DbColumnEx("BRE_DAT_DUE", QVariant::DateTime));
	view.append(DbColumnEx("BRE_DAT_SEND_REMIND", QVariant::DateTime));
	view.append(DbColumnEx("BRE_DAT_EXPIRE", QVariant::DateTime));
	view.append(DbColumnEx("BRE_ONE_SHOOT_REMIND", QVariant::Int));
	view.append(DbColumnEx("BRE_SC_NOTIFY_FAILED", QVariant::Int));
	view.append(DbColumnEx("BRE_OWNER_ID", QVariant::Int));
	view.append(DbColumnEx("BRE_RECV_CONTACT_ID", QVariant::Int));
	

	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// MVIEW_REMINDER_SETTINGS
	//-----------------------
	view.m_nViewID		= MVIEW_REMINDER_SETTINGS;
	view.append(DbColumnEx("REM_ONCE_BEFORE",			QVariant::Int));
	view.append(DbColumnEx("REM_EVERY_N_BEFORE",		QVariant::Int));
	view.append(DbColumnEx("REM_EVERY_N_AFTER",			QVariant::Int));
	view.append(DbColumnEx("REM_DELETE_IF_OVERDUE",		QVariant::Int));
	view.append(DbColumnEx("REM_SEND_BY_SOKRATES",		QVariant::Int));
	view.append(DbColumnEx("REM_SEND_BY_EMAIL",			QVariant::Int));
	view.append(DbColumnEx("REM_SEND_BY_SMS",			QVariant::Int));
	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// TVIEW_BUS_DM_SERVER_FILE
	//-----------------------
	view.m_nViewID	= TVIEW_BUS_DM_SERVER_FILE;
	view.m_nSkipFirstColsInsert	= 3;
	view.m_nSkipFirstColsUpdate	= 3;
	view.m_strTables="BUS_DM_SERVER_FILE";

	//Record set definition
	view.append(DbColumnEx("BDSF_ID", QVariant::Int));
	view.append(DbColumnEx("BDSF_GLOBAL_ID", QVariant::String));
	view.append(DbColumnEx("BDSF_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("BDSF_DISK_REL_PATH", QVariant::String));
	view.append(DbColumnEx("BDSF_DISK_FILENAME", QVariant::String));
	view.append(DbColumnEx("BDSF_FILENAME", QVariant::String));
	view.append(DbColumnEx("BDSF_EXTENSION", QVariant::String));
	view.append(DbColumnEx("BDSF_CREATED", QVariant::DateTime));
	view.append(DbColumnEx("BDSF_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("BDSF_SIZE", QVariant::Int));
	view.append(DbColumnEx("BDSF_THUMB_PIC_32", QVariant::ByteArray));
	view.append(DbColumnEx("BDSF_THUMB_PIC_64", QVariant::ByteArray));
	view.append(DbColumnEx("BDSF_THUMB_PIC_128", QVariant::ByteArray));
	view.append(DbColumnEx("BDSF_THUMB_PIC_256", QVariant::ByteArray));
	view.append(DbColumnEx("BDSF_TAGS", QVariant::String));
	s_lstDbViews.append(view);
	view.clear();



	//-----------------------
	// TVIEW_BUS_DM_META_DATA
	//-----------------------
	view.m_nViewID	= TVIEW_BUS_DM_META_DATA;
	view.m_nSkipFirstColsInsert	= 3;
	view.m_nSkipFirstColsUpdate	= 3;
	view.m_strTables="BUS_DM_META_DATA";

	//Record set definition
	view.append(DbColumnEx("BDME_ID", QVariant::Int));
	view.append(DbColumnEx("BDME_GLOBAL_ID", QVariant::String));
	view.append(DbColumnEx("BDME_DAT_LAST_MODIFIED", QVariant::DateTime));
	view.append(DbColumnEx("BDME_SERVER_FILE_ID", QVariant::Int));
	view.append(DbColumnEx("BDME_VALUE_TYPE", QVariant::Int));
	view.append(DbColumnEx("BDME_VALUE", QVariant::String));

	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// TVIEW_BUS_CAL_VIEW
	//-----------------------

	view.m_nViewID		= TVIEW_BUS_CAL_VIEW;
	view.m_nSkipFirstColsInsert	 = 3;
	view.m_nSkipFirstColsUpdate	 = 3;
	view.m_strTables="BUS_CAL_VIEW";
	//Record set definition
	view.append(DbColumnEx("BCALV_ID",					QVariant::Int));
	view.append(DbColumnEx("BCALV_GLOBAL_ID",			QVariant::String));
	view.append(DbColumnEx("BCALV_DAT_LAST_MODIFIED",	QVariant::DateTime));
	view.append(DbColumnEx("BCALV_NAME",				QVariant::String));
	view.append(DbColumnEx("BCALV_OPEN_ON_STARTUP",		QVariant::Int));
	view.append(DbColumnEx("BCALV_IS_PUBLIC",			QVariant::Int));
	view.append(DbColumnEx("BCALV_TYPE",				QVariant::Int));
	view.append(DbColumnEx("BCALV_OWNER_ID",			QVariant::Int));
	view.append(DbColumnEx("BCALV_SORT_CODE",			QVariant::String));
	view.append(DbColumnEx("BCALV_VALUE_BYTE",			QVariant::ByteArray));
	view.append(DbColumnEx("BCALV_VIEW_TYPE_ID",		QVariant::Int));
	view.append(DbColumnEx("BCALV_FILTER_BY_NOTYPE",	QVariant::Int));
	view.append(DbColumnEx("BCALV_FILTER_BY_TYPE",		QVariant::Int));
	view.append(DbColumnEx("BCALV_SHOW_PRELIMINARY",	QVariant::Int));
	view.append(DbColumnEx("BCALV_SHOW_INFORMATION",	QVariant::Int));
	view.append(DbColumnEx("BCALV_TIME_SCALE",			QVariant::Int));
	view.append(DbColumnEx("BCALV_DATE_RANGE",			QVariant::Int));
	view.append(DbColumnEx("BCALV_SCROLL_TO_TIME",		QVariant::DateTime));
	view.append(DbColumnEx("BCALV_MULTIDAY_HEIGHT",		QVariant::ByteArray));
	view.append(DbColumnEx("BCALV_COLUMN_WIDTH",		QVariant::Int));
	view.append(DbColumnEx("BCALV_FUI_GEOMETRY",		QVariant::ByteArray));
	view.append(DbColumnEx("BCALV_FUI_POSITION_X",		QVariant::Int));
	view.append(DbColumnEx("BCALV_FUI_POSITION_Y",		QVariant::Int));
	view.append(DbColumnEx("BCALV_SHOW_TASKS",			QVariant::Int));
	view.append(DbColumnEx("BCALV_SECTIONS",			QVariant::Int));
	view.append(DbColumnEx("BCALV_OVERVIEWLEFTCOLWIDTH",QVariant::Int));

	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// TVIEW_GROUP_IMPORT
	//-----------------------

	view.m_nViewID		= TVIEW_GROUP_IMPORT;
	view.m_nSkipFirstColsInsert	 = 3;
	view.m_nSkipFirstColsUpdate	 = 3;
	view.m_strTables="BUS_CAL_VIEW";
	//Record set definition
	view.append(DbColumnEx("BCNT_FIRSTNAME",			QVariant::String));
	view.append(DbColumnEx("BCNT_LASTNAME",				QVariant::String));
	view.append(DbColumnEx("BCNT_ORGANIZATIONNAME",		QVariant::String));
	view.append(DbColumnEx("BGIT_CODE",					QVariant::String));
	view.append(DbColumnEx("BGIT_EXTERNAL_CATEGORY",	QVariant::String));
	view.append(DbColumnEx("MODE",						QVariant::Int));
	view.append(DbColumnEx("UPDATE_MODE",				QVariant::Int));
	view.append(DbColumnEx("BGCN_CMCA_VALID_FROM",		QVariant::Date));
	view.append(DbColumnEx("BGCN_CMCA_VALID_TO",		QVariant::Date));

	s_lstDbViews.append(view);
	view.clear();

	//----------------------------------------------
	// TVIEW_WEBSERVICE_BUS_CALENDAR_SELECT
	//----------------------------------------------

	view.m_nViewID		= TVIEW_WEBSERVICE_BUS_CALENDAR_SELECT;	
	view.m_strTables	= "BUS_CAL_EVENT_PART";
	view.m_nSkipLastCols	 = 7;

	//Record set definition
	view.append(DbColumnEx("BCEP_ID",					QVariant::Int));
	view.append(DbColumnEx("BCEP_DAT_LAST_MODIFIED",	QVariant::DateTime));
	view.append(DbColumnEx("BCEP_EVENT_ID",				QVariant::Int));
	view.append(DbColumnEx("CENT_ID",					QVariant::Int));
	//view.append(DbColumnEx("BCEP_COMM_ENTITY_ID",		QVariant::Int));
	view.append(DbColumnEx("BCEP_STATUS",				QVariant::Int));
	view.append(DbColumnEx("BCEP_PRESENCE_TYPE_ID",		QVariant::Int));
	view.append(DbColumnEx("BCEP_TYPE_ID",				QVariant::Int));
	//view.append(DbColumnEx("BCEP_DESCRIPTION",			QVariant::String, true));
	view.append(DbColumnEx("BCEV_ID",					QVariant::Int));
	view.append(DbColumnEx("BCEV_TITLE",				QVariant::String));
	view.append(DbColumnEx("BCEV_IS_INFORMATION",		QVariant::Int));
	view.append(DbColumnEx("BCEV_IS_PRELIMINARY",		QVariant::Int));
	view.append(DbColumnEx("BCOL_SUBJECT",				QVariant::String));
	view.append(DbColumnEx("BCOL_FROM",					QVariant::DateTime));
	view.append(DbColumnEx("BCOL_TO",					QVariant::DateTime));
	view.append(DbColumnEx("BCOL_LOCATION",				QVariant::String));
	view.append(DbColumnEx("BCOL_DESCRIPTION",			QVariant::String, true));		//Description.

	view.append(DbColumnEx("EVENT_TITLE",		QVariant::String));
	view.append(DbColumnEx("TIME_FROM", QVariant::String));
	view.append(DbColumnEx("TIME_TO",	QVariant::String));
	view.append(DbColumnEx("DATE",		QVariant::Date));
	view.append(DbColumnEx("STATUS",	QVariant::Int));
	view.append(DbColumnEx("ASSIGNED_LIST", QVariant::String));
	view.append(DbColumnEx("DATE_LOCAL_STR", QVariant::String));
	//view.append(DbColumnEx("NMRX_PERSON_LIST", DbRecordSet::GetVariantType()));

	view.m_strSelectSQL = "SELECT " + getSQLColumnsFromView(view) +
		" FROM BUS_CAL_EVENT_PART \
		INNER JOIN BUS_CAL_OPTIONS ON BCOL_EVENT_PART_ID = BCEP_ID  \
		INNER JOIN CE_COMM_ENTITY ON CENT_ID = BCEP_COMM_ENTITY_ID \
		INNER JOIN BUS_CAL_EVENT ON BCEV_ID = BCEP_EVENT_ID \
		INNER JOIN BUS_NMRX_RELATION ON BNMR_TABLE_1 = 35 AND BNMR_TABLE_KEY_ID_1 = CENT_ID \
		";
	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// TVIEW_BUS_CAL_VIEW_ENTITIES
	//-----------------------

	view.m_nViewID = TVIEW_BUS_CAL_VIEW_ENTITIES;
	view.m_nSkipFirstColsInsert	 = 3;
	view.m_nSkipFirstColsUpdate	 = 3;
	view.m_strTables="BUS_CAL_VIEW_ENTITIES";
	//Record set definition
	view.append(DbColumnEx("BCVE_ID",					QVariant::Int));
	view.append(DbColumnEx("BCVE_GLOBAL_ID",			QVariant::String));
	view.append(DbColumnEx("BCVE_DAT_LAST_MODIFIED",	QVariant::DateTime));
	view.append(DbColumnEx("BCVE_CAL_VIEW_ID",			QVariant::Int));
	view.append(DbColumnEx("BCVE_USER_ID",				QVariant::Int));
	view.append(DbColumnEx("BCVE_PROJECT_ID",			QVariant::Int));
	view.append(DbColumnEx("BCVE_CONTACT_ID",			QVariant::Int));
	view.append(DbColumnEx("BCVE_RESOURCE_ID",			QVariant::Int));
	view.append(DbColumnEx("BCVE_IS_SELECTED",			QVariant::Int));
	view.append(DbColumnEx("BCVE_SORT_CODE",			QVariant::Int));

	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// TVIEW_BUS_CAL_VIEW_CE_TYPES
	//-----------------------

	view.m_nViewID = TVIEW_BUS_CAL_VIEW_CE_TYPES;
	view.m_nSkipFirstColsInsert	 = 3;
	view.m_nSkipFirstColsUpdate	 = 3;
	view.m_strTables="BUS_CAL_VIEW_CE_TYPES";
	//Record set definition
	view.append(DbColumnEx("BCCT_ID",					QVariant::Int));
	view.append(DbColumnEx("BCCT_GLOBAL_ID",			QVariant::String));
	view.append(DbColumnEx("BCCT_DAT_LAST_MODIFIED",	QVariant::DateTime));
	view.append(DbColumnEx("BCCT_CAL_VIEW_ID",			QVariant::Int));
	view.append(DbColumnEx("BCCT_TYPE_ID",				QVariant::Int));
	view.append(DbColumnEx("BCCT_IS_SELECTED",			QVariant::Int));

	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// TVIEW_BUS_CAL_VIEW_ENTITIES_SELECT
	//-----------------------

	view.m_nViewID = TVIEW_BUS_CAL_VIEW_ENTITIES_SELECT;	
	view.m_nSkipFirstColsInsert = 3;
	view.m_nSkipFirstColsUpdate = 3;
	view.m_nSkipLastCols=2; //skip 2 cols at end (calculated)
	view.m_strTables="BUS_CAL_VIEW_ENTITIES";

	//Record set definition
	view.append(DbColumnEx("BCVE_ID",					QVariant::Int));
	view.append(DbColumnEx("BCVE_GLOBAL_ID",			QVariant::String));
	view.append(DbColumnEx("BCVE_DAT_LAST_MODIFIED",	QVariant::DateTime));
	view.append(DbColumnEx("BCVE_CAL_VIEW_ID",			QVariant::Int));
	view.append(DbColumnEx("BCVE_USER_ID",				QVariant::Int));
	view.append(DbColumnEx("BCVE_PROJECT_ID",			QVariant::Int));
	view.append(DbColumnEx("BCVE_CONTACT_ID",			QVariant::Int));
	view.append(DbColumnEx("BCVE_RESOURCE_ID",			QVariant::Int));
	view.append(DbColumnEx("BCVE_IS_SELECTED",			QVariant::Int));
	view.append(DbColumnEx("BCVE_SORT_CODE",			QVariant::Int));
	view.append(DbColumnEx("BUSP_NAME",					QVariant::String));
	view.append(DbColumnEx("BRES_NAME",					QVariant::String));
	view.append(DbColumnEx("BCNT_LASTNAME",				QVariant::String));
	view.append(DbColumnEx("BCNT_FIRSTNAME",			QVariant::String));
	view.append(DbColumnEx("BCNT_ORGANIZATIONNAME",		QVariant::String));
	view.append(DbColumnEx("BPER_FIRST_NAME",			QVariant::String));
	view.append(DbColumnEx("BPER_LAST_NAME",			QVariant::String));
	view.append(DbColumnEx("BDEPT_CODE",				QVariant::String));
	view.append(DbColumnEx("BDEPT_NAME",				QVariant::String));
	view.append(DbColumnEx("BPER_NAME",					QVariant::String));
	view.append(DbColumnEx("BCNT_NAME",					QVariant::String));
	
	view.m_strSelectSQL="SELECT "+getSQLColumnsFromView(view)+" \
						FROM BUS_CAL_VIEW_ENTITIES \
						LEFT OUTER JOIN BUS_PERSON ON BCVE_USER_ID=BPER_ID \
						LEFT OUTER JOIN BUS_DEPARTMENTS ON BPER_DEPARTMENT_ID=BDEPT_ID \
						LEFT OUTER JOIN BUS_PROJECTS ON BCVE_PROJECT_ID=BUSP_ID \
						LEFT OUTER JOIN BUS_CM_CONTACT ON BCVE_CONTACT_ID=BCNT_ID \
						LEFT OUTER JOIN BUS_CAL_RESOURCE ON BCVE_RESOURCE_ID=BRES_ID \
						";

	s_lstDbViews.append(view);

	view.clear();

	//-----------------------
	// MVIEW_CALENDAR_EVENT_PARTS_SIMPLE
	//-----------------------
	//INNER JOIN BUS_CAL_OPTIONS ON BCOL_EVENT_PART_ID = BCEP_ID AND BCOL_IS_ACTIVE = 1
	view.m_nViewID		= MVIEW_CALENDAR_EVENT_PARTS_SIMPLE;
	view.m_strTables	= "BUS_NMRX_RELATION";
	view.append(DbColumnEx("CENT_ID", QVariant::Int));
	view.append(DbColumnEx("BNMR_TABLE_2", QVariant::Int));
	view.append(DbColumnEx("BNMR_TABLE_KEY_ID_2", QVariant::Int));
//	view.m_strSelectSQL = "SELECT " + getSQLColumnsFromView(view) +
	view.m_strSelectSQL = "SELECT CENT_ID, BNMR_TABLE_2, BNMR_TABLE_KEY_ID_2 \
		FROM BUS_CAL_EVENT_PART \
		INNER JOIN BUS_CAL_OPTIONS ON BCOL_EVENT_PART_ID = BCEP_ID  \
		INNER JOIN CE_COMM_ENTITY ON CENT_ID = BCEP_COMM_ENTITY_ID \
		INNER JOIN BUS_PERSON ON CENT_OWNER_ID = BPER_ID \
		INNER JOIN BUS_CAL_EVENT ON BCEV_ID = BCEP_EVENT_ID \
		INNER JOIN BUS_NMRX_RELATION ON BNMR_TABLE_1 = 35 AND BNMR_TABLE_KEY_ID_1 = CENT_ID \
		LEFT OUTER JOIN BUS_CM_TYPES ON BCMT_ID = BCEP_PRESENCE_TYPE_ID \
		LEFT OUTER JOIN CE_TYPE ON CET_ID = CENT_CE_TYPE_ID \
		";
	s_lstDbViews.append(view);
	view.clear();

	view.m_nViewID = TVIEW_SPC_ACTIVITY;
	view.m_nSkipFirstColsInsert	 = 3;
	view.m_nSkipFirstColsUpdate	 = 3;
	view.m_strTables="SPC_ACTIVITY";
	//Record set definition
	InitTreeSelectionView(SPC_ACTIVITY, view,true);
	view.append(DbColumnEx("SET_F_TAETIGKEITEN_SEQ", QVariant::Int));

	s_lstDbViews.append(view);
	view.clear();

	view.m_nViewID = TVIEW_SPC_INDUSTRY_GROUP;
	view.m_nSkipFirstColsInsert	 = 3;
	view.m_nSkipFirstColsUpdate	 = 3;
	view.m_strTables="SPC_INDUSTRY_GROUP";
	//Record set definition
	InitTreeSelectionView(SPC_INDUSTRY_GROUP, view,true);
	view.append(DbColumnEx("SEWG_F_WIRTSCH_GRUPPE_SEQ", QVariant::Int));

	s_lstDbViews.append(view);
	view.clear();

	view.m_nViewID = TVIEW_SPC_PR_AREA;
	view.m_nSkipFirstColsInsert	 = 3;
	view.m_nSkipFirstColsUpdate	 = 3;
	view.m_strTables="SPC_PR_AREA";
	//Record set definition
	InitTreeSelectionView(SPC_PR_AREA, view,true);
	view.append(DbColumnEx("SEPB_F_PROJ_BEREICHE_SEQ", QVariant::Int));

	s_lstDbViews.append(view);
	view.clear();

	view.m_nViewID = TVIEW_SPC_PR_STATUS;
	view.m_nSkipFirstColsInsert	 = 3;
	view.m_nSkipFirstColsUpdate	 = 3;
	view.m_strTables="SPC_PR_STATUS";
	//Record set definition
	view.append(DbColumnEx("SFPS_ID", QVariant::Int));
	view.append(DbColumnEx("SFPS_GLOBAL_ID" ,QVariant::String));
	view.append(DbColumnEx("SFPS_DAT_LAST_MODIFIED" ,QVariant::DateTime));
	view.append(DbColumnEx("SFPS_CODE", QVariant::String));
	view.append(DbColumnEx("SFPS_NAME", QVariant::String));
	view.append(DbColumnEx("SFPS_F_PR_STATUS_SEQ", QVariant::Int));

	s_lstDbViews.append(view);
	view.clear();

	view.m_nViewID = TVIEW_SPC_PR_TYPE;
	view.m_nSkipFirstColsInsert	 = 3;
	view.m_nSkipFirstColsUpdate	 = 3;
	view.m_strTables="SPC_PR_TYPE";
	//Record set definition
	InitTreeSelectionView(SPC_PR_TYPE, view,true);
	view.append(DbColumnEx("SEAP_F_PROJEKTART_SEQ", QVariant::Int));

	s_lstDbViews.append(view);
	view.clear();

	view.m_nViewID = TVIEW_SPC_PROCESS;
	view.m_nSkipFirstColsInsert	 = 3;
	view.m_nSkipFirstColsUpdate	 = 3;
	view.m_strTables="SPC_PROCESS";
	//Record set definition
	InitTreeSelectionView(SPC_PROCESS, view,true);
	view.append(DbColumnEx("SEPZ_F_PROZESSE_SEQ", QVariant::Int));

	s_lstDbViews.append(view);
	view.clear();

	view.m_nViewID = TVIEW_SPC_RATE_CAT;
	view.m_nSkipFirstColsInsert	 = 3;
	view.m_nSkipFirstColsUpdate	 = 3;
	view.m_strTables="SPC_RATE_CAT";
	//Record set definition
	view.append(DbColumnEx("SETK_ID", QVariant::Int));
	view.append(DbColumnEx("SETK_GLOBAL_ID" ,QVariant::String));
	view.append(DbColumnEx("SETK_DAT_LAST_MODIFIED" ,QVariant::DateTime));
	view.append(DbColumnEx("SETK_CODE", QVariant::String));
	view.append(DbColumnEx("SETK_NAME", QVariant::String));
	view.append(DbColumnEx("SETK_F_TARIFKATEG_SEQ", QVariant::Int));

	s_lstDbViews.append(view);
	view.clear();


	view.m_nViewID = TVIEW_SPC_SECTOR;
	view.m_nSkipFirstColsInsert	 = 3;
	view.m_nSkipFirstColsUpdate	 = 3;
	view.m_strTables="SPC_SECTOR";
	//Record set definition
	InitTreeSelectionView(SPC_SECTOR, view,true);
	view.append(DbColumnEx("SEFG_F_GEBIETE_SEQ", QVariant::Int));

	s_lstDbViews.append(view);
	view.clear();

	view.m_nViewID = TVIEW_SPC_SERVICE_TYPE;
	view.m_nSkipFirstColsInsert	 = 3;
	view.m_nSkipFirstColsUpdate	 = 3;
	view.m_strTables="SPC_SERVICE_TYPE";
	//Record set definition
	InitTreeSelectionView(SPC_SERVICE_TYPE, view,true);
	view.append(DbColumnEx("SELA_F_LEISTUNGSARTEN_SEQ", QVariant::Int));

	s_lstDbViews.append(view);
	view.clear();



	//-----------------------
	// TVIEW_BUS_CUSTOM_BOOL
	//-----------------------

	view.m_nViewID = TVIEW_BUS_CUSTOM_BOOL;
	view.m_nSkipFirstColsInsert	 = 3;
	view.m_nSkipFirstColsUpdate	 = 3;
	view.m_strTables="BUS_CUSTOM_BOOL";
	//Record set definition
	view.append(DbColumnEx("BCB_ID",				QVariant::Int));
	view.append(DbColumnEx("BCB_GLOBAL_ID",			QVariant::String));
	view.append(DbColumnEx("BCB_DAT_LAST_MODIFIED",	QVariant::DateTime));
	view.append(DbColumnEx("BCB_CUSTOM_FIELD_ID",	QVariant::Int));
	view.append(DbColumnEx("BCB_VALUE",				QVariant::Int));
	view.append(DbColumnEx("BCB_TABLE_ID",			QVariant::Int));
	view.append(DbColumnEx("BCB_RECORD_ID",			QVariant::Int));

	s_lstDbViews.append(view);
	view.clear();


	//-----------------------
	// TVIEW_BUS_CUSTOM_DATE
	//-----------------------

	view.m_nViewID = TVIEW_BUS_CUSTOM_DATE;
	view.m_nSkipFirstColsInsert	 = 3;
	view.m_nSkipFirstColsUpdate	 = 3;
	view.m_strTables="BUS_CUSTOM_DATE";
	//Record set definition
	view.append(DbColumnEx("BCD_ID",				QVariant::Int));
	view.append(DbColumnEx("BCD_GLOBAL_ID",			QVariant::String));
	view.append(DbColumnEx("BCD_DAT_LAST_MODIFIED",	QVariant::DateTime));
	view.append(DbColumnEx("BCD_CUSTOM_FIELD_ID",	QVariant::Int));
	view.append(DbColumnEx("BCD_VALUE",				QVariant::Date));
	view.append(DbColumnEx("BCD_TABLE_ID",			QVariant::Int));
	view.append(DbColumnEx("BCD_RECORD_ID",			QVariant::Int));

	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// TVIEW_BUS_CUSTOM_DATETIME
	//-----------------------

	view.m_nViewID = TVIEW_BUS_CUSTOM_DATETIME;
	view.m_nSkipFirstColsInsert	 = 3;
	view.m_nSkipFirstColsUpdate	 = 3;
	view.m_strTables="BUS_CUSTOM_DATETIME";
	//Record set definition
	view.append(DbColumnEx("BCDT_ID",				QVariant::Int));
	view.append(DbColumnEx("BCDT_GLOBAL_ID",		QVariant::String));
	view.append(DbColumnEx("BCDT_DAT_LAST_MODIFIED",QVariant::DateTime));
	view.append(DbColumnEx("BCDT_CUSTOM_FIELD_ID",	QVariant::Int));
	view.append(DbColumnEx("BCDT_VALUE",			QVariant::DateTime));
	view.append(DbColumnEx("BCDT_TABLE_ID",			QVariant::Int));
	view.append(DbColumnEx("BCDT_RECORD_ID",		QVariant::Int));

	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// TVIEW_BUS_CUSTOM_FIELDS
	//-----------------------
	view.m_nViewID = TVIEW_BUS_CUSTOM_FIELDS;
	view.m_nSkipFirstColsInsert	 = 3;
	view.m_nSkipFirstColsUpdate	 = 3;
	view.m_strTables="BUS_CUSTOM_FIELDS";
	//Record set definition
	view.append(DbColumnEx("BCF_ID",				QVariant::Int));
	view.append(DbColumnEx("BCF_GLOBAL_ID",			QVariant::String));
	view.append(DbColumnEx("BCF_DAT_LAST_MODIFIED",	QVariant::DateTime));
	view.append(DbColumnEx("BCF_TABLE_ID",			QVariant::Int));
	view.append(DbColumnEx("BCF_NAME",				QVariant::String));
	view.append(DbColumnEx("BCF_DATA_TYPE",			QVariant::Int));
	view.append(DbColumnEx("BCF_SELECTION_TYPE",	QVariant::Int));
	view.append(DbColumnEx("BCF_SORT",				QVariant::String));
	view.append(DbColumnEx("BCF_GROUP",				QVariant::String));

	s_lstDbViews.append(view);
	//view.clear(); //do not uncomment this...


	//-----------------------
	// TVIEW_BUS_CUSTOM_FIELDS_DATA
	//-----------------------
	view.m_nViewID = TVIEW_BUS_CUSTOM_FIELDS_DATA;
	view.m_nSkipFirstColsInsert	 = 3;
	view.m_nSkipFirstColsUpdate	 = 3;
	view.m_nSkipLastCols	 = 8;
	view.m_strTables="BUS_CUSTOM_FIELDS";
	//Record set definition
	view.append(DbColumnEx("LST_SELECTIONS",		DbRecordSet::GetVariantType()));
	view.append(DbColumnEx("REC_BOOL_DATA",			DbRecordSet::GetVariantType()));
	view.append(DbColumnEx("REC_INT_DATA",			DbRecordSet::GetVariantType()));
	view.append(DbColumnEx("REC_FLOAT_DATA",			DbRecordSet::GetVariantType()));
	view.append(DbColumnEx("REC_TEXT_DATA",			DbRecordSet::GetVariantType()));
	view.append(DbColumnEx("REC_LONGTEXT_DATA",			DbRecordSet::GetVariantType()));
	view.append(DbColumnEx("REC_DATE_DATA",			DbRecordSet::GetVariantType()));
	view.append(DbColumnEx("REC_DATETIME_DATA",			DbRecordSet::GetVariantType()));


	s_lstDbViews.append(view);
	view.clear();


	


	//-----------------------
	// TVIEW_BUS_CUSTOM_FIELDS_SELECT
	//-----------------------

	view.m_nViewID = TVIEW_BUS_CUSTOM_FIELDS_SELECT;
	view.m_nSkipLastCols	 = 1;
	view.m_strTables="BUS_CUSTOM_FIELDS";
	//Record set definition
	view.append(DbColumnEx("BCF_ID",				QVariant::Int));
	view.append(DbColumnEx("BCF_TABLE_ID",			QVariant::Int));
	view.append(DbColumnEx("BCF_NAME",				QVariant::String));
	view.append(DbColumnEx("TABLE_NAME",			QVariant::String));

	s_lstDbViews.append(view);
	view.clear();


	//-----------------------
	// TVIEW_BUS_CUSTOM_FLOAT
	//-----------------------

	view.m_nViewID = TVIEW_BUS_CUSTOM_FLOAT;
	view.m_nSkipFirstColsInsert	 = 3;
	view.m_nSkipFirstColsUpdate	 = 3;
	view.m_strTables="BUS_CUSTOM_FLOAT";
	//Record set definition
	view.append(DbColumnEx("BCFL_ID",				QVariant::Int));
	view.append(DbColumnEx("BCFL_GLOBAL_ID",		QVariant::String));
	view.append(DbColumnEx("BCFL_DAT_LAST_MODIFIED",QVariant::DateTime));
	view.append(DbColumnEx("BCFL_CUSTOM_FIELD_ID",	QVariant::Int));
	view.append(DbColumnEx("BCFL_VALUE",			QVariant::String));
	view.append(DbColumnEx("BCFL_TABLE_ID",			QVariant::Int));
	view.append(DbColumnEx("BCFL_RECORD_ID",		QVariant::Int));

	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// TVIEW_BUS_CUSTOM_INT
	//-----------------------

	view.m_nViewID = TVIEW_BUS_CUSTOM_INT;
	view.m_nSkipFirstColsInsert	 = 3;
	view.m_nSkipFirstColsUpdate	 = 3;
	view.m_strTables="BUS_CUSTOM_INT";
	//Record set definition
	view.append(DbColumnEx("BCI_ID",				QVariant::Int));
	view.append(DbColumnEx("BCI_GLOBAL_ID",			QVariant::String));
	view.append(DbColumnEx("BCI_DAT_LAST_MODIFIED",	QVariant::DateTime));
	view.append(DbColumnEx("BCI_CUSTOM_FIELD_ID",	QVariant::Int));
	view.append(DbColumnEx("BCI_VALUE",				QVariant::Int));
	view.append(DbColumnEx("BCI_TABLE_ID",			QVariant::Int));
	view.append(DbColumnEx("BCI_RECORD_ID",			QVariant::Int));

	s_lstDbViews.append(view);
	view.clear();


	//-----------------------
	// TVIEW_BUS_CUSTOM_LONGTEXT
	//-----------------------

	view.m_nViewID = TVIEW_BUS_CUSTOM_LONGTEXT;
	view.m_nSkipFirstColsInsert	 = 3;
	view.m_nSkipFirstColsUpdate	 = 3;
	view.m_strTables="BUS_CUSTOM_LONGTEXT";
	//Record set definition
	view.append(DbColumnEx("BCLT_ID",					QVariant::Int));
	view.append(DbColumnEx("BCLT_GLOBAL_ID",			QVariant::String));
	view.append(DbColumnEx("BCLT_DAT_LAST_MODIFIED",	QVariant::DateTime));
	view.append(DbColumnEx("BCLT_CUSTOM_FIELD_ID",		QVariant::Int));
	view.append(DbColumnEx("BCLT_VALUE",				QVariant::String,true));
	view.append(DbColumnEx("BCLT_TABLE_ID",				QVariant::Int));
	view.append(DbColumnEx("BCLT_RECORD_ID",			QVariant::Int));

	s_lstDbViews.append(view);
	view.clear();


	//-----------------------
	// TVIEW_BUS_CUSTOM_TEXT
	//-----------------------

	view.m_nViewID = TVIEW_BUS_CUSTOM_TEXT;
	view.m_nSkipFirstColsInsert	 = 3;
	view.m_nSkipFirstColsUpdate	 = 3;
	view.m_strTables="BUS_CUSTOM_TEXT";
	//Record set definition
	view.append(DbColumnEx("BCT_ID",				QVariant::Int));
	view.append(DbColumnEx("BCT_GLOBAL_ID",			QVariant::String));
	view.append(DbColumnEx("BCT_DAT_LAST_MODIFIED",	QVariant::DateTime));
	view.append(DbColumnEx("BCT_CUSTOM_FIELD_ID",	QVariant::Int));
	view.append(DbColumnEx("BCT_VALUE",				QVariant::String));
	view.append(DbColumnEx("BCT_TABLE_ID",			QVariant::Int));
	view.append(DbColumnEx("BCT_RECORD_ID",			QVariant::Int));

	s_lstDbViews.append(view);
	view.clear();


	//-----------------------
	// TVIEW_BUS_CUSTOM_SELECTIONS
	//-----------------------

	view.m_nViewID = TVIEW_BUS_CUSTOM_SELECTIONS;
	view.m_nSkipFirstColsInsert	 = 3;
	view.m_nSkipFirstColsUpdate	 = 3;
	view.m_strTables="BUS_CUSTOM_SELECTIONS";
	//Record set definition
	view.append(DbColumnEx("BCS_ID",				QVariant::Int));
	view.append(DbColumnEx("BCS_GLOBAL_ID",			QVariant::String));
	view.append(DbColumnEx("BCS_DAT_LAST_MODIFIED",	QVariant::DateTime));
	view.append(DbColumnEx("BCS_CUSTOM_FIELD_ID",	QVariant::Int));
	view.append(DbColumnEx("BCS_VALUE",				QVariant::String));
	view.append(DbColumnEx("BCS_RADIO_BUTTON_LABEL",QVariant::String));
	view.append(DbColumnEx("BCS_IS_DEFAULT",		QVariant::Int));

	s_lstDbViews.append(view);
	view.clear();
	

	//-----------------------
	// TVIEW_BUS_CUSTOM_CONTACT_DATA_IMPORT
	//-----------------------
	view.m_nViewID		= TVIEW_BUS_CUSTOM_CONTACT_DATA_IMPORT;
	view.m_strTables="";
	view.append(DbColumnEx("BCNT_FIRSTNAME",		QVariant::String));
	view.append(DbColumnEx("BCNT_LASTNAME",			QVariant::String));
	view.append(DbColumnEx("BCNT_ORGANIZATIONNAME", QVariant::String));
	view.append(DbColumnEx("BCF_VALUE",				QVariant::String));
	view.append(DbColumnEx("STATUS_CODE",			QVariant::Int));			//0-ok, 1-matching contact not found, 2-matching custom field not found, 3 - wrong data type, 4-other error-> check STATUS_TEXT
	view.append(DbColumnEx("STATUS_TEXT",			QVariant::String));

	s_lstDbViews.append(view);
	view.clear();


	//-----------------------
	// TVIEW_BUS_PUSH_MESSAGE
	//-----------------------
	view.m_nViewID		= TVIEW_BUS_PUSH_MESSAGE;
	view.m_nSkipFirstColsInsert = 3;	//skip Id & global
	view.m_nSkipFirstColsUpdate = 3;
	view.m_strTables="BUS_PUSH_MESSAGE"; //if more then one table separate by comma "TABLE,TABLE2"
	view.append(DbColumnEx("BPM_ID",  QVariant::Int));
	view.append(DbColumnEx("BPM_GLOBAL_ID",  QVariant::String));
	view.append(DbColumnEx("BPM_DAT_LAST_MODIFIED" ,QVariant::DateTime));
	view.append(DbColumnEx("BPM_CREATED" ,QVariant::DateTime));
	view.append(DbColumnEx("BPM_MESSAGE_BODY" ,QVariant::String));
	view.append(DbColumnEx("BPM_PUSH_TOKEN" ,QVariant::String));
	view.append(DbColumnEx("BPM_OS_TYPE" ,QVariant::Int));
	view.append(DbColumnEx("BPM_BADGE" ,QVariant::Int));
	view.append(DbColumnEx("BPM_SOUNDNAME" ,QVariant::String));
	view.append(DbColumnEx("BPM_STATUS" ,QVariant::Int));
	view.append(DbColumnEx("BPM_ERROR_TEXT" ,QVariant::String));
	view.append(DbColumnEx("BPM_PERSON_FK_ID" ,QVariant::Int));
	view.append(DbColumnEx("BPM_SEND_TRIES" ,QVariant::Int));
	view.append(DbColumnEx("BPM_CUSTOM_DATA_LIST" ,QVariant::String));
	view.append(DbColumnEx("BPM_DAT_SENT" ,QVariant::DateTime));
		

	s_lstDbViews.append(view);
	view.clear();

	//-----------------------
	// TVIEW_BUS_PUSH_TOKEN_PERSON
	//-----------------------
	view.m_nViewID		= TVIEW_BUS_PUSH_TOKEN_PERSON;
	view.m_nSkipFirstColsInsert = 3;	//skip Id & global
	view.m_nSkipFirstColsUpdate = 3;
	view.m_strTables="BUS_PUSH_TOKEN_PERSON"; //if more then one table separate by comma "TABLE,TABLE2"
	view.append(DbColumnEx("BPT_ID",  QVariant::Int));
	view.append(DbColumnEx("BPT_GLOBAL_ID",  QVariant::String));
	view.append(DbColumnEx("BPT_DAT_LAST_MODIFIED" ,QVariant::DateTime));
	view.append(DbColumnEx("BPT_PUSH_TOKEN" ,QVariant::String));
	view.append(DbColumnEx("BPT_OS_TYPE" ,QVariant::Int));
	view.append(DbColumnEx("BPT_DAT_LAST_SUCCESS_SEND" ,QVariant::DateTime));
	view.append(DbColumnEx("BPT_PERSON_FK_ID" ,QVariant::Int));

	s_lstDbViews.append(view);
	view.clear();


	//-----------------------
	// TVIEW_SPC_BUDGET_VALS
	//-----------------------
	view.m_nViewID		= TVIEW_SPC_BUDGET_VALS;
	view.m_strTables="";
	view.append(DbColumnEx("T_ZEIT_AUFG",			QVariant::Double));
	view.append(DbColumnEx("T_ZEIT_BUDGET",			QVariant::Double));
	view.append(DbColumnEx("T_HONORAR_AUFG",		QVariant::Double));
	view.append(DbColumnEx("T_HONORAR_BUDGET",		QVariant::Double));
	view.append(DbColumnEx("T_STAND_GEW",			QVariant::Double));
	view.append(DbColumnEx("T_NK_AUFG",				QVariant::Double));
	view.append(DbColumnEx("T_NK_BUDGET",			QVariant::Double));
	view.append(DbColumnEx("T_FAKT_HON",			QVariant::Double));
	view.append(DbColumnEx("T_FAKT_NK",				QVariant::Double));
	view.append(DbColumnEx("T_ZAHL_HON",			QVariant::Double));
	view.append(DbColumnEx("T_ZAHL_NK",				QVariant::Double));
	view.append(DbColumnEx("T_AUFW_HON",			QVariant::Double));
	view.append(DbColumnEx("T_AUFW_NK",				QVariant::Double));
	view.append(DbColumnEx("T_BUDGET",				QVariant::Double));
	view.append(DbColumnEx("T_MWST",				QVariant::Double));
	view.append(DbColumnEx("T_RABATT",				QVariant::Double));
	view.append(DbColumnEx("T_FREMD_BUDGET",		QVariant::Double));
	view.append(DbColumnEx("T_FREMDLEIST_BUDGET",	QVariant::Double));
	view.append(DbColumnEx("T_AUFW_FL_BUDGET",		QVariant::Double));
	view.append(DbColumnEx("T_AUFW_FK_BUDGET",		QVariant::Double));
	view.append(DbColumnEx("T_HONSUM_TOT",			QVariant::Double));
	view.append(DbColumnEx("T_KOSTENDACH",			QVariant::Double));
	view.append(DbColumnEx("T_HONSUM_STAND",		QVariant::Double));
	view.append(DbColumnEx("T_HONSUM_INC_STAND",	QVariant::Double));
	view.append(DbColumnEx("T_FAKT_EXTERN",			QVariant::Double));

	view.append(DbColumnEx("T_FREMDKOST_AUFG",		QVariant::Double));
	view.append(DbColumnEx("T_FREMDLEIST_AUFG",		QVariant::Double));
	view.append(DbColumnEx("T_FREMDLEIST_BUDGET",	QVariant::Double));
	view.append(DbColumnEx("T_FREMDLEIST_AUFW",		QVariant::Double));
	view.append(DbColumnEx("T_FREMDKOST_AUFW",		QVariant::Double));

	view.append(DbColumnEx("T_AKOS_KT_PER1",		QVariant::Double));
	view.append(DbColumnEx("T_AKOS_KT_PER2",		QVariant::Double));
	view.append(DbColumnEx("T_AKOS_ZT_PER1",		QVariant::Double));
	view.append(DbColumnEx("T_AKOS_ZT_PER2",		QVariant::Double));
	view.append(DbColumnEx("T_AKOS_SP_PER1",		QVariant::Double));
	view.append(DbColumnEx("T_AKOS_SP_PER2",		QVariant::Double));
	view.append(DbColumnEx("T_AAUF_KT_PER1",		QVariant::Double));
	view.append(DbColumnEx("T_AAUF_KT_PER2",		QVariant::Double));
	view.append(DbColumnEx("T_AAUF_ZT_PER1",		QVariant::Double));
	view.append(DbColumnEx("T_AAUF_ZT_PER2",		QVariant::Double));
	view.append(DbColumnEx("T_AAUF_SP_PER1",		QVariant::Double));
	view.append(DbColumnEx("T_AAUF_SP_PER2",		QVariant::Double));
	view.append(DbColumnEx("T_AZEI_KT_PER1",		QVariant::Double));
	view.append(DbColumnEx("T_AZEI_KT_PER2",		QVariant::Double));
	view.append(DbColumnEx("T_AZEI_ZT_PER1",		QVariant::Double));
	view.append(DbColumnEx("T_AZEI_ZT_PER2",		QVariant::Double));

	view.append(DbColumnEx("T_FAKT_KOSTEN",		QVariant::Double));
	view.append(DbColumnEx("T_FAKT_ZEIT",		QVariant::Double));
	view.append(DbColumnEx("T_FAKT_ZMITT",		QVariant::Double));
	view.append(DbColumnEx("T_FAKT_NEBEN",		QVariant::Double));
	view.append(DbColumnEx("T_FAKT_EXT_HON",	QVariant::Double));
	view.append(DbColumnEx("T_FAKT_EXT_NK",		QVariant::Double));
	view.append(DbColumnEx("T_ZAHL_KOSTEN",		QVariant::Double));
	view.append(DbColumnEx("T_ZAHL_ZEIT",		QVariant::Double));
	view.append(DbColumnEx("T_ZAHL_ZMITT",		QVariant::Double));

	view.append(DbColumnEx("T_AUFW_HON_BUDGET",		QVariant::Double));
	view.append(DbColumnEx("T_AUFW_NK_BUDGET",		QVariant::Double));
	view.append(DbColumnEx("T_PER1",				QVariant::Int));	//??
	view.append(DbColumnEx("T_PER2",				QVariant::Int));

	view.append(DbColumnEx("T_AUFG_ZEIT_NF",		QVariant::Double));  //??
	view.append(DbColumnEx("T_PROG_AUFW_HON",		QVariant::Double));
	view.append(DbColumnEx("T_PROG_ZEIT",		QVariant::Double));
	view.append(DbColumnEx("T_WERT_KORR",		QVariant::Double));
	view.append(DbColumnEx("T_FAKT_SPEZ",		QVariant::Double));
	view.append(DbColumnEx("T_AUFG_HON_KT",		QVariant::Double));
	view.append(DbColumnEx("T_AUFG_HON_ZT",		QVariant::Double));
	view.append(DbColumnEx("T_AUFW_HON_KT",		QVariant::Double));
	view.append(DbColumnEx("T_AUFW_HON_ZT",		QVariant::Double));

	view.append(DbColumnEx("T_WERT_DIR_KUM",		QVariant::Double));
	view.append(DbColumnEx("T_WERTP_DIR_KUM",		QVariant::Double));
	view.append(DbColumnEx("T_WERTD_DIR_KUM",		QVariant::Double));
	view.append(DbColumnEx("T_FAKT_RAB_ARB",		QVariant::Double));
	view.append(DbColumnEx("T_FAKT_RAB_NK",		QVariant::Double));
	view.append(DbColumnEx("T_KUM_GWJ_PROG",		QVariant::Double));
	view.append(DbColumnEx("T_KUM_GWJ_PROG_SUM",		QVariant::Double));

	view.append(DbColumnEx("T_FAKT_NK_O_FK",	QVariant::Double));
	view.append(DbColumnEx("T_FAKT_FK",			QVariant::Double));
	view.append(DbColumnEx("T_AUFG_FK_NF_NULL",	QVariant::Double));

	view.append(DbColumnEx("T_NF_NK_INT",	QVariant::Double));
	view.append(DbColumnEx("T_NF_NK_EXT",	QVariant::Double));
	view.append(DbColumnEx("T_NF_FK_INT",	QVariant::Double));
	view.append(DbColumnEx("T_NF_FK_EXT",	QVariant::Double));

	view.append(DbColumnEx("T_ARB_AUFG_FAKT",	QVariant::Double));
	view.append(DbColumnEx("T_ARB_AUFW_FAKT",	QVariant::Double));

	
	//from _151:
	view.append(DbColumnEx("T_AUFG_HON_NF",	QVariant::Double));
	view.append(DbColumnEx("T_AUFW_HON_NF",	QVariant::Double));
	view.append(DbColumnEx("T_AUFG_NK_NF",	QVariant::Double));
	view.append(DbColumnEx("T_AUFW_NK_NF",	QVariant::Double));
	view.append(DbColumnEx("T_AUFW_NK_NF",	QVariant::Double));

	//from 110:
	//---------IMPORTANT: do not shuffle order of this section
	view.append(DbColumnEx("T_KUM_F_HONORAR",	QVariant::Double));
	view.append(DbColumnEx("T_KUM_F_NEBENKOST",	QVariant::Double));
	view.append(DbColumnEx("T_KUM_F_ZUSCHLAG",	QVariant::Double));
	view.append(DbColumnEx("T_KUM_F_RABATT",	QVariant::Double));
	view.append(DbColumnEx("T_KUM_F_MWST",	QVariant::Double));
	view.append(DbColumnEx("T_KUM_F_BETRAG",	QVariant::Double));
	
	view.append(DbColumnEx("T_KUM_F_BET_MWST0",	QVariant::Double));
	view.append(DbColumnEx("T_KUM_F_BET_MWSTA",	QVariant::Double));
	view.append(DbColumnEx("T_KUM_F_BET_MWSTB",	QVariant::Double));

	view.append(DbColumnEx("T_KUM_F_EXT_HON",	QVariant::Double));
	view.append(DbColumnEx("T_KUM_F_EXT_NK",	QVariant::Double));

	view.append(DbColumnEx("T_KUM_F_OFFEN",	QVariant::Double));
	view.append(DbColumnEx("T_KUM_F_RAB_ARB",	QVariant::Double));
	view.append(DbColumnEx("T_KUM_F_RAB_NK",	QVariant::Double));
	view.append(DbColumnEx("T_KUM_F_FK",	QVariant::Double));
	view.append(DbColumnEx("T_KUM_F_NK_O_FK",	QVariant::Double));
	//---------IMPORTANT: do not shuffle order of this section

	//for Routine_104_SP
	view.append(DbColumnEx("T_PC_PROJECT",	QVariant::String));
	view.append(DbColumnEx("T_PC_DATE_FROM",	QVariant::DateTime));
	view.append(DbColumnEx("T_PC_DATE_TO",	QVariant::DateTime));
	view.append(DbColumnEx("T_PC_NEW",	QVariant::Int));

	view.append(DbColumnEx("T_KUM_Z_BETRAG",	QVariant::Double));
	view.append(DbColumnEx("T_KUM_Z_SKONTO",	QVariant::Double));
	view.append(DbColumnEx("T_KUM_Z_BET_MWSTA",	QVariant::Double));
	view.append(DbColumnEx("T_KUM_Z_BET_MWSTB",	QVariant::Double));
	view.append(DbColumnEx("T_KUM_Z_TOTAL",	QVariant::Double));

	//for Routine_105_SP
	view.append(DbColumnEx("T_KUM_ZEIT_ZT",	QVariant::Double));
	view.append(DbColumnEx("T_KUM_ZEIT_KT",	QVariant::Double));
	view.append(DbColumnEx("T_KUM_ZEIT",	QVariant::Double));
	view.append(DbColumnEx("T_KUM_HON_ZT",	QVariant::Double));
	view.append(DbColumnEx("T_KUM_HON_KT",	QVariant::Double));
	view.append(DbColumnEx("T_KUM_HON",	QVariant::Double));
	view.append(DbColumnEx("T_KUM_NEBEN",	QVariant::Double));

	view.append(DbColumnEx("T_KUM_AUFG_FR_KOST",	QVariant::Double));
	view.append(DbColumnEx("T_KUM_AUFW_FR_KOST",	QVariant::Double));
	view.append(DbColumnEx("T_KUM_AUFG_FR_LEIST",	QVariant::Double));
	view.append(DbColumnEx("T_KUM_AUFW_FR_LEIST",	QVariant::Double));

	view.append(DbColumnEx("T_KUM_ZEIT_FR_LEIST",	QVariant::Double));
	view.append(DbColumnEx("T_KUM_ZT_UNFAKT",	QVariant::Double));
	view.append(DbColumnEx("T_KUM_KT_UNFAKT",	QVariant::Double));
	view.append(DbColumnEx("T_KUM_NK_UNFAKT",	QVariant::Double));
	
	view.append(DbColumnEx("T_KUM_AUFG_HON_NF",	QVariant::Double));
	
	view.append(DbColumnEx("T_KUM_AUFG_NK_NF",	QVariant::Double));
	view.append(DbColumnEx("T_KUM_AUFG_FK_NF",	QVariant::Double));
	view.append(DbColumnEx("T_KUM_AUFW_HON_NF",	QVariant::Double));
	view.append(DbColumnEx("T_KUM_AUFW_NK_NF",	QVariant::Double));
	view.append(DbColumnEx("T_KUM_AUFW_FK_NF",	QVariant::Double));

	view.append(DbColumnEx("T_KUM_AUFG_ZEIT_NF",	QVariant::Double));
	view.append(DbColumnEx("T_KUM_AUFG_SPES",	QVariant::Double));
	view.append(DbColumnEx("T_KUM_AUFW_SPES",	QVariant::Double));

	view.append(DbColumnEx("T_KUM_ARB_AUFG_FAKT",	QVariant::Double));
	view.append(DbColumnEx("T_KUM_ARB_AUFW_FAKT",	QVariant::Double));
	view.append(DbColumnEx("T_KUM_NK_AUFG_FAKT",	QVariant::Double));
	view.append(DbColumnEx("T_KUM_NK_AUFW_FAKT",	QVariant::Double));
	view.append(DbColumnEx("T_KUM_FK_AUFG_FAKT",	QVariant::Double));
	view.append(DbColumnEx("T_KUM_FK_AUFW_FAKT",	QVariant::Double));

	view.append(DbColumnEx("T_FIRST_CHARGE",	QVariant::DateTime));
		
	s_lstDbViews.append(view);
	view.clear();



	//-----------------------
	// TVIEW_BUS_EMAIL_BINARY
	//-----------------------

	view.m_nViewID = TVIEW_BUS_EMAIL_BINARY;
	view.m_nSkipFirstColsInsert	 = 3;
	view.m_nSkipFirstColsUpdate	 = 3;
	view.m_strTables="BUS_EMAIL_BINARY";
	//Record set definition
	view.append(DbColumnEx("BEB_ID",				QVariant::Int));
	view.append(DbColumnEx("BEB_GLOBAL_ID",			QVariant::String));
	view.append(DbColumnEx("BEB_DAT_LAST_MODIFIED",	QVariant::DateTime));
	view.append(DbColumnEx("BEB_EMAIL_ID",			QVariant::Int));
	view.append(DbColumnEx("BEB_BODY",				QVariant::String,true));

	s_lstDbViews.append(view);
	view.clear();


}

//-----------------------------------------------------
//				PUBLIC MEMEBERS
//-----------------------------------------------------



/*!
	Returns Recordset defintion view for given view ID
	\param  nViewID		- view id
	\return DbView		- view def
*/
DbView& DbSqlTableView::getView(int nViewID)
{
	Initialize();
	//if(!m_bInitialised)Initialize();		//automatic initialization
	return s_lstDbViews[findView(nViewID)]; //return view
}

/*!
	Returns user defined SQL view or generates SQL view based on recordset defintion
	\param  nViewID		- view id
	\return SQL			- SQL SELECT STATEMENT in format SELECT X1,X1 FROM TABLE
*/
QString DbSqlTableView::getSQLView(int nViewID, bool bDistinct /*= false*/)
{
	Initialize();
	QString strSql; 

	//find view, if SQL defined return
	int nIdx = findView(nViewID);
	strSql=s_lstDbViews[nIdx].m_strSelectSQL;
	if(!strSql.isEmpty()) return strSql;

	//if not defined, generate automatic, store back to list :)
	DbView RecordSetView = s_lstDbViews[nIdx];

	//loop through
	if (bDistinct)
		strSql="SELECT DISTINCT " ;
	else
		strSql="SELECT " ;


	
	int RecordSetViewSize = RecordSetView.size()-RecordSetView.m_nSkipLastCols;
	for(int i=0; i < RecordSetViewSize ;++i)
	{
		strSql+=RecordSetView.at(i).m_strName+",";
		//if(i!=RecordSetViewSize-1)strSql+=",";//add comma between
	}
	strSql.chop(1); //remove comma at end
	strSql+=" FROM "  + RecordSetView.m_strTables;

	//store back to skip init again:
	s_lstDbViews[nIdx].m_strSelectSQL=strSql;
	return strSql;
}

/*!
	Returns user defined SQL INSERT or generates SQL INSERT based on recordset defintion.
	Takes mapping into account if negative, or non exist, else, user must manually create sql
	\param  nViewID		- view id
	\return SQL			- SQL INSERT STATEMENT in format INSERT INTO XX (X1,X1) VALUES(?,?)
*/
QString DbSqlTableView::getSQLInsert(int nViewID,QSet<int> *pLstIsLongVarcharField)
{
	Initialize();

	QString strSql; 
	int nStart;

	//find view, if SQL definded return
	int nIdx = findView(nViewID);
	strSql=s_lstDbViews[nIdx].m_strInsertSQL;
	if(!strSql.isEmpty()) 
	{
		if (pLstIsLongVarcharField)
			*pLstIsLongVarcharField=s_lstDbViews[nIdx].m_LstIsLongVarcharField;
		return strSql;
	}

	//if not definded, generate automatic, store back to list :)
	DbView RecordSetView = s_lstDbViews[nIdx];

	//if map.size=1 and neg take it:
	nStart=s_lstDbViews[nIdx].m_nSkipFirstColsInsert;

	//clear
	QSet<int> lstIsLongVarcharField;

	//loop through
	strSql="INSERT INTO "  + RecordSetView.m_strTables+" ( ";
	int RecordSetViewSize = RecordSetView.size()-s_lstDbViews[nIdx].m_nSkipLastColsWrite;
	for(int i=nStart; i < RecordSetViewSize; ++i)
	{
		strSql+=RecordSetView.at(i).m_strName+",";

		//if (pLstIsLongVarcharField) //BT added for testing if longvarchar, for FB conversion from char to blob
		if (RecordSetView.at(i).m_bIsLongVarChar)
				lstIsLongVarcharField.insert(i);
	}
	strSql.chop(1); //remove comma at end
	strSql+=") VALUES (";
	for(int i=nStart; i < RecordSetViewSize; ++i)
	{
		strSql+="?,";
		//if(i!=RecordSetViewSize-1)strSql+=",";//add comma between
	}
	strSql.chop(1); //remove comma at end
	strSql+=") ";

	//store back to skip init again:
	s_lstDbViews[nIdx].m_strInsertSQL=strSql;
	s_lstDbViews[nIdx].m_LstIsLongVarcharField=lstIsLongVarcharField;
	if (pLstIsLongVarcharField)
		*pLstIsLongVarcharField=lstIsLongVarcharField;
	return strSql;
}

//ignore skip cols, same as insert-> for full data copy
QString DbSqlTableView::getFullSQLInsert(int nViewID,QSet<int> *pLstIsLongVarcharField)
{
	Initialize();
	QString strSql; 
	int nStart;

	//find view
	int nIdx = findView(nViewID);

	//if not definded, generate automatic, store back to list :)
	DbView RecordSetView = s_lstDbViews[nIdx];

	//if map.size=1 and neg take it:
	nStart=0;
	int RecordSetViewSize = RecordSetView.size();

	//loop through
	strSql="INSERT INTO "  + RecordSetView.m_strTables+" ( ";
	
	for(int i=nStart; i < RecordSetViewSize; ++i)
	{
		strSql+=RecordSetView.at(i).m_strName+",";

		if (pLstIsLongVarcharField) //BT added for testing if longvarchar, for FB conversion from char to blob
			if (RecordSetView.at(i).m_bIsLongVarChar)
					pLstIsLongVarcharField->insert(i);
	}
	strSql.chop(1); //remove comma at end

	strSql+=") VALUES (";
	for(int i=nStart; i < RecordSetViewSize; ++i)
	{
		strSql+="?,";
	}
	strSql.chop(1); //remove comma at end
	strSql+=") ";

	return strSql;
}

/*!
	Returns user defined SQL UPDATE or generates SQL UPDATE based on recordset defintion.
	Takes mapping into account if negative, or non exist, else, user must manually create sql
	\param  nViewID		- view id
	\return SQL			- SQL UPDATE STATEMENT in format UPDATE XX SET X1=?,X2=? (no where clause)
*/
QString DbSqlTableView::getSQLUpdate(int nViewID,QSet<int> *pLstIsLongVarcharField)
{
	Initialize();

	QString strSql; 
	int nStart;

	//find view, if SQL defined return
	int nIdx = findView(nViewID);
	strSql=s_lstDbViews[nIdx].m_strUpdateSQL;
	if(!strSql.isEmpty()) 
	{
		if (pLstIsLongVarcharField)
			*pLstIsLongVarcharField=s_lstDbViews[nIdx].m_LstIsLongVarcharField;
		return strSql;
	}

	//if not definded, generate automatic, store back to list :)
	DbView RecordSetView = s_lstDbViews[nIdx];

	//if map.size=1 and neg take it:
	nStart=s_lstDbViews[nIdx].m_nSkipFirstColsUpdate;

	//clear
	QSet<int> lstIsLongVarcharField;

	//loop through
	strSql="UPDATE "  + RecordSetView.m_strTables+" SET ";
	int RecordSetViewSize = RecordSetView.size()-s_lstDbViews[nIdx].m_nSkipLastColsWrite;
	for(int i=nStart;i<RecordSetViewSize;++i)
	{
		strSql+=RecordSetView.at(i).m_strName+" = ?,";
		//if (pLstIsLongVarcharField) //BT added for testing if longvarchar, for FB conversion from char to blob
		if (RecordSetView.at(i).m_bIsLongVarChar)
			lstIsLongVarcharField.insert(i);
	}
	strSql.chop(1); //remove comma at end

	

	//store back to skip init again:
	s_lstDbViews[nIdx].m_strUpdateSQL=strSql;
	s_lstDbViews[nIdx].m_LstIsLongVarcharField=lstIsLongVarcharField;
	if (pLstIsLongVarcharField)
		*pLstIsLongVarcharField=lstIsLongVarcharField;
	return strSql;
}

/*!
	Returns list of mapping (for SQL insert) between position in RecordSet View and SQL statemnt.
	Mapping can be define with view or it can be left default emopty in which case mapping is 1:1
	(from first to last column)
	if Mapping contains -1 or -2 or other negative values, it means that mapping list will be generated
	but first column will be skipped (-1) or first two (-2), and so on

	\param  nViewID		- view ID
	\return list		- Map list
*/
int DbSqlTableView::getInsertListMapping(int nViewID)
{
	Initialize();

	Q_ASSERT_X(findView(nViewID)>=0, "return of insert list", "Unknown ViewID");
	return s_lstDbViews[findView(nViewID)].m_nSkipFirstColsInsert;
}

/*!
	Returns list of mapping (for SQL update) between position in RecordSet View and SQL statemnt.
	Mapping can be define with view or it can be left default emopty in which case mapping is 1:1
	(from first to last column)
	if Mapping contains -1 or -2 or other negative values, it means that mapping list will be generated
	but first column will be skipped (-1) or first two (-2), and so on

	\param  nViewID		- view ID
	\return list		- Map list
*/

int DbSqlTableView::getUpdateListMapping(int nViewID)
{
	Initialize();

	Q_ASSERT_X(findView(nViewID)>=0, "return of update list", "Unknown ViewID");
	return s_lstDbViews[findView(nViewID)].m_nSkipFirstColsUpdate;
}

/* private: find that view */
int DbSqlTableView::findView(int nViewID)
{
	Initialize();

	int DbViewSize = s_lstDbViews.size();
	for(int i=0; i < DbViewSize; i++)
		if( s_lstDbViews[i].m_nViewID == nViewID)
			return i;

	Q_ASSERT_X(false,"VIEW","VIEW DOES NOT EXIST, ABORT ABORT");
	return -1;
}




/*!
	In debug mode, call this to check if there are view duplicates
*/
bool DbSqlTableView::CheckUniqueView()
{

	Initialize();

	int nSize=s_lstDbViews.size();
	for(int i=0;i<nSize;++i)
	{
		int nOccur=0;
		int nViewID=s_lstDbViews.at(i).m_nViewID;
		for(int j=0;j<nSize;++j)
		{
			if(nViewID==s_lstDbViews.at(j).m_nViewID)
				nOccur++;
		}
		if(nOccur>1)
		{
			Q_ASSERT(false);
			return false;
		}
	}
	return true;
}

/*!
	Helper functions to get mandatory field names (no prefixes)
	\param nTableID			- table id
	\param strFieldSuffix	- ID , GLOBAL_ID, or other mandatory

*/
QString DbSqlTableView::getFieldName(int nTableID,QString strFieldSuffix)
{
	return DbSqlTableDefinition::GetTablePrefix(nTableID)+"_"+strFieldSuffix;
}

/*!
	Helper functions to get generic TREE VIEW:
*/
void DbSqlTableView::InitTreeSelectionView(int nTableID, DbView &view, bool bOrderColumnsFromStart)
{
	DbTableKeyData KeyData;
	DbSqlTableDefinition::GetKeyData(nTableID,KeyData);
	QString strPrefix = KeyData.m_strFieldPrefix;	

	view.m_strTables = KeyData.m_strTableName;
	if (bOrderColumnsFromStart) //B.T. added for SPC-SC transition: SC must have all SC fields available
	{
		view.append(DbColumnEx(strPrefix+"_ID",	QVariant::Int));
		view.append(DbColumnEx(strPrefix+"_GLOBAL_ID" ,QVariant::String));
		view.append(DbColumnEx(strPrefix+"_DAT_LAST_MODIFIED" ,QVariant::DateTime));
		view.append(DbColumnEx(strPrefix+"_CODE", QVariant::String));
		view.append(DbColumnEx(strPrefix+"_LEVEL", QVariant::Int));
		view.append(DbColumnEx(strPrefix+"_PARENT", QVariant::Int));
		view.append(DbColumnEx(strPrefix+"_HASCHILDREN", QVariant::Int));
		view.append(DbColumnEx(strPrefix+"_ICON", QVariant::String));
		view.append(DbColumnEx(strPrefix+"_MAINPARENT_ID", QVariant::Int));
		view.append(DbColumnEx(strPrefix+"_NAME", QVariant::String));
		view.append(DbColumnEx(strPrefix+"_STYLE", QVariant::Int));
	}
	else
	{
		view.append(DbColumnEx(strPrefix+"_ID",	QVariant::Int));
		view.append(DbColumnEx(strPrefix+"_CODE", QVariant::String));
		view.append(DbColumnEx(strPrefix+"_LEVEL", QVariant::Int));
		view.append(DbColumnEx(strPrefix+"_PARENT", QVariant::Int));
		view.append(DbColumnEx(strPrefix+"_HASCHILDREN", QVariant::Int));
		view.append(DbColumnEx(strPrefix+"_ICON", QVariant::String));
		view.append(DbColumnEx(strPrefix+"_MAINPARENT_ID", QVariant::Int));
		view.append(DbColumnEx(strPrefix+"_NAME", QVariant::String));
		view.append(DbColumnEx(strPrefix+"_STYLE", QVariant::Int));
		view.append(DbColumnEx(strPrefix+"_DAT_LAST_MODIFIED" ,QVariant::DateTime));
	}

}


//returns columns from view, comma separated:
QString DbSqlTableView::getSQLColumnsFromView(DbView &RecordSetView)
{
	//if not defined, generate automatic, store back to list :)
	QString strSql;

	int RecordSetViewSize = RecordSetView.size()-RecordSetView.m_nSkipLastCols;
	for(int i=0; i < RecordSetViewSize ;++i)
	{
		strSql+=RecordSetView.at(i).m_strName;
		strSql+=",";//add comma between
	}

	strSql.chop(1);
	//store back to skip init again:
	return strSql;
}


QStringList* DbSqlTableView::getAllLongVarCharFields()
{
	if(s_bAllLongVarCharFieldsInitialized)
		return &s_lstAllLongVarCharFields;

	Initialize();

	int nSize=s_lstDbViews.size();
	for(int i=0;i<nSize;++i)
	{
		DbView view=s_lstDbViews.at(i);
		int nColCnt=view.size();
		for(int j=0;j<nColCnt;++j)
		{
			if (view.at(j).m_bIsLongVarChar)
				s_lstAllLongVarCharFields<<view.at(j).m_strName;
		}
	}

	s_bAllLongVarCharFieldsInitialized=true;
	return &s_lstAllLongVarCharFields;
}

