<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="1.1" language="de_CH">
<context>
    <name>QObject</name>
    <message>
        <location filename="" line="4368828"/>
        <source>First Name</source>
        <translation type="obsolete">Vorname</translation>
    </message>
    <message>
        <location filename="" line="4368828"/>
        <source>First Name(s)</source>
        <translation type="obsolete">Vorname(n)</translation>
    </message>
    <message>
        <location filename="" line="4368828"/>
        <source>Last Name</source>
        <translation type="obsolete">Nachname</translation>
    </message>
    <message>
        <location filename="" line="4368828"/>
        <source>Last Name(s)</source>
        <translation type="obsolete">Nachname(n)</translation>
    </message>
    <message>
        <location filename="" line="4368828"/>
        <source>Active</source>
        <translation type="obsolete">Aktiv</translation>
    </message>
    <message>
        <location filename="" line="4368828"/>
        <source>Checked if the Person is Active</source>
        <translation type="obsolete">Markiert wenn Benutzer aktiv ist</translation>
    </message>
    <message>
        <location filename="" line="4368828"/>
        <source>Gender</source>
        <translation type="obsolete">Geschlecht</translation>
    </message>
    <message>
        <location filename="" line="4368828"/>
        <source>Birth Date</source>
        <translation type="obsolete">Geburtstag</translation>
    </message>
    <message>
        <location filename="" line="4368828"/>
        <source>Date of Birth</source>
        <translation type="obsolete">Geburtstag</translation>
    </message>
    <message>
        <location filename="" line="4368828"/>
        <source>Notes</source>
        <translation type="obsolete">Bemerkungen</translation>
    </message>
    <message>
        <location filename="" line="4368828"/>
        <source>Department</source>
        <translation type="obsolete">Abteilung</translation>
    </message>
    <message>
        <location filename="" line="4368828"/>
        <source>Assigned Department</source>
        <translation type="obsolete">Zugewiesene Abteilung</translation>
    </message>
    <message>
        <location filename="" line="4368828"/>
        <source>Company</source>
        <translation type="obsolete">Organisation</translation>
    </message>
    <message>
        <location filename="" line="4368828"/>
        <source>Assigned Company</source>
        <translation type="obsolete">Organisation zuweisen</translation>
    </message>
    <message>
        <location filename="" line="4368828"/>
        <source>Direct Phone</source>
        <translation type="obsolete">Telefon direkt</translation>
    </message>
    <message>
        <location filename="" line="4368828"/>
        <source>Direct Phone Number</source>
        <translation type="obsolete">Telefonnummer direkt</translation>
    </message>
    <message>
        <location filename="" line="4368828"/>
        <source>Insurance ID</source>
        <translation type="obsolete">Versicherungsnummer</translation>
    </message>
    <message>
        <location filename="" line="4368828"/>
        <source>Social Security Number, AHV Number ...</source>
        <translation type="obsolete">Sozialversicherungsnr. AHV-Nummer...</translation>
    </message>
    <message>
        <location filename="" line="4368828"/>
        <source>Main Function</source>
        <translation type="obsolete">Hauptfunktion</translation>
    </message>
    <message>
        <location filename="" line="4368828"/>
        <source>Assigned Main Function</source>
        <translation type="obsolete">Zugewiesene Hauptfunktion</translation>
    </message>
    <message>
        <location filename="" line="4368828"/>
        <source>Occupation %</source>
        <translation type="obsolete">Beschäftigung in %</translation>
    </message>
    <message>
        <location filename="" line="4368828"/>
        <source>Entered</source>
        <translation type="obsolete">Eintritt</translation>
    </message>
    <message>
        <location filename="" line="4368828"/>
        <source>Date of Entry</source>
        <translation type="obsolete">Datum Eintritt</translation>
    </message>
    <message>
        <location filename="" line="4368828"/>
        <source>Left</source>
        <translation type="obsolete">Austritt</translation>
    </message>
    <message>
        <location filename="" line="4368828"/>
        <source>Date of Leaving</source>
        <translation type="obsolete">Datum Austritt</translation>
    </message>
    <message>
        <location filename="" line="4368828"/>
        <source>Picture</source>
        <translation type="obsolete">Bild</translation>
    </message>
    <message>
        <location filename="" line="4368828"/>
        <source>Name</source>
        <translation type="obsolete">Name</translation>
    </message>
    <message>
        <location filename="" line="4368828"/>
        <source>Name of the Organization / Company</source>
        <translation type="obsolete">Name der Organisation / Firma</translation>
    </message>
    <message>
        <location filename="" line="4368828"/>
        <source>Checked if the Organization is Active</source>
        <translation type="obsolete">Markiert wenn Organisation aktiv ist</translation>
    </message>
    <message>
        <location filename="" line="4368828"/>
        <source>Contact</source>
        <translation type="obsolete">Kontakt</translation>
    </message>
    <message>
        <location filename="" line="4368828"/>
        <source>Assigned Contact Data</source>
        <translation type="obsolete">Zugewiesene Kontaktdaten</translation>
    </message>
    <message>
        <location filename="" line="4368828"/>
        <source>Organization (with timeline)</source>
        <translation type="obsolete">Organisation (mit Zeitlinie)</translation>
    </message>
    <message>
        <location filename="" line="4368828"/>
        <source>Assigned Organization</source>
        <translation type="obsolete">Zugewiesene Organisation</translation>
    </message>
    <message>
        <location filename="" line="4368828"/>
        <source>Cost Center (with timeline)</source>
        <translation type="obsolete">Kostenstelle (mit Zeitlinie)</translation>
    </message>
    <message>
        <location filename="" line="4368828"/>
        <source>Assigned Cost Center</source>
        <translation type="obsolete">Zugewiesene Kostenstelle</translation>
    </message>
    <message>
        <location filename="" line="4368828"/>
        <source>Name of the Cost Center</source>
        <translation type="obsolete">Name der Kostenstelle</translation>
    </message>
    <message>
        <location filename="" line="4368828"/>
        <source>Checked if the Cost Center is Active</source>
        <translation type="obsolete">Markiert wenn Kostenstelle aktiv ist</translation>
    </message>
    <message>
        <location filename="" line="4368828"/>
        <source>1st Payment Period in days</source>
        <translation type="obsolete">1. Zahlfrist in Tagen</translation>
    </message>
    <message>
        <location filename="" line="4368828"/>
        <source>2nd Payment Period in days</source>
        <translation type="obsolete">2. Zahlfrist in Tagen</translation>
    </message>
    <message>
        <location filename="" line="4368828"/>
        <source>3th Payment Period in days</source>
        <translation type="obsolete">3. Zahlfrist in Tagen</translation>
    </message>
    <message>
        <location filename="" line="4368828"/>
        <source>Creditor Code for accounting system</source>
        <translation type="obsolete">Kreditoren-Code für Buchhaltung</translation>
    </message>
    <message>
        <location filename="" line="4368828"/>
        <source>Creditor Account for accounting system</source>
        <translation type="obsolete">Kreditorenkonto für Buchhaltung</translation>
    </message>
    <message>
        <location filename="" line="4368828"/>
        <source>General description</source>
        <translation type="obsolete">Allgemeine Beschreibung</translation>
    </message>
    <message>
        <location filename="" line="4368828"/>
        <source>Creditor Code</source>
        <translation type="obsolete">Kreditorencode</translation>
    </message>
    <message>
        <location filename="" line="4368828"/>
        <source>Creditor Account</source>
        <translation type="obsolete">Kreditorenkonto</translation>
    </message>
    <message>
        <location filename="" line="4368828"/>
        <source>Local</source>
        <translation type="obsolete">Lokale Nummer</translation>
    </message>
    <message>
        <location filename="" line="4368828"/>
        <source>Country predial</source>
        <translation type="obsolete">Landesvorwahl</translation>
    </message>
    <message>
        <location filename="" line="4368828"/>
        <source>City/Area Code</source>
        <translation type="obsolete">Ortsvorwahl</translation>
    </message>
    <message>
        <location filename="" line="4368828"/>
        <source>Suppress mailing</source>
        <translation type="obsolete">In Mailings unterdrücken</translation>
    </message>
    <message>
        <location filename="" line="4368828"/>
        <source>Checked when contact receives no mail</source>
        <translation type="obsolete">Markiert wenn Kontakt keine E-Mails erhalten soll</translation>
    </message>
</context>
</TS>
