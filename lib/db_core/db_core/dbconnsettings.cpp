#include "dbconnsettings.h"

DbConnectionSettings::~DbConnectionSettings()
{
}

DbConnectionSettings::DbConnectionSettings(const DbConnectionSettings &that)
{
	operator = (that);
}
void DbConnectionSettings::operator =(const DbConnectionSettings &that)
{
	m_strCustomName	 = that.m_strCustomName;
	m_strDbType		 = that.m_strDbType;
	m_strDbHostName  = that.m_strDbHostName ;
	m_strDbUserName	 = that.m_strDbUserName;
	m_strDbPassword	 = that.m_strDbPassword;
	m_strDbOptions	 = that.m_strDbOptions;
	m_strDbName		 = that.m_strDbName;
	m_strTableSpace	 = that.m_strTableSpace;
	m_strIndexSpace	 = that.m_strIndexSpace;
	m_nDbPort		 = that.m_nDbPort;
	m_nDbPoolTimeout = that.m_nDbPoolTimeout;
}


