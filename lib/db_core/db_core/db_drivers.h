#ifndef DB_DRIVERS_H
#define DB_DRIVERS_H


//DEFINE DATABASE TYPES:
//This is important because QT supports multiple DB drivers in same time e.g. MYSQL and MYSQL3
//Its probably best to enumerate all those type to latest version...



#define DBTYPE_MYSQL		"QMYSQL3"		
#define DBTYPE_ORACLE		"QOCI"
#define DBTYPE_DB2			"QDB2"
#define DBTYPE_FIREBIRD		"QIBASE"
#define DBTYPE_ODBC			"QODBC3"
#define DBTYPE_MS_SQL		"QODBC3"
#define DBTYPE_POSTGRESQL	"QPSQL7"

#endif //DB_DRIVERS_H
