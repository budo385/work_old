#ifndef DB_CONNECTIONS_LIST_H
#define DB_CONNECTIONS_LIST_H

#include <QList>
#include "dbconnsettings.h"
#include "common/common/encryptedinifile.h"
#include "common/common/connectionslistabstract.h"

class DbConnectionsList: public ConnectionsListAbstract
{
public:
	DbConnectionsList();
	~DbConnectionsList();

	DbConnectionsList(const DbConnectionsList &other) : ConnectionsListAbstract() { operator = (other); }
	void operator = (const DbConnectionsList &other){
		if(this != &other){
			m_lstConnections = other.m_lstConnections;
		}
	}
	
	void Clear();
	bool Load(QString strFile, QString strPassword);
	bool Save(QString strFile, QString strPassword);

	//should be called after Load() call, before Save()
	QString GetPasswordHash();
	bool MatchUserPassword(QString strPassword);
	bool IsUserPasswordSet();
	bool SetUserPasswordHash(QString strPassHash);
	void ClearUserPassword();
	
	bool GetDbSettings(QString strSettingsName,DbConnectionSettings &info);


public:
	QList<DbConnectionSettings> m_lstConnections;

protected:
	EncryptedIniFile m_objIni;

	QString GetHashString(QString strPassword);
};

#endif //NETWORK_CONNECTIONS_H


