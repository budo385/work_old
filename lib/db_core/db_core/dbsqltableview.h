#ifndef DBSQLTABLEVIEW_H
#define DBSQLTABLEVIEW_H

#include <QString>
#include <QStringList>
#include "common/common/dbrecordsetdefintion.h"


/*!
	\class DbSqlTableView
	\brief TableView is static data container of all views on all tables
	\ingroup Db_Core

	- holds view defintions for DbRecordSet class
	- holds INSERT, SELECT, UPDATE statements for easier SQL operations
	Use:
	- define own VIEW_ID in ViewID enumerator
	- define VIEW data in initialize function
*/

class DbSqlTableView 
{

public:
	enum ViewID							///< ViewID - ID type definition enum
	{
		TVIEW_CORE_LOCKING,
		TVIEW_CORE_APP_SRV_SESSION,
		TVIEW_CORE_USER_SESSION,
		TVIEW_CORE_APP_SRV_SESSION_STATUS,
		TVIEW_CORE_EVENT_LOG,
		TVIEW_CORE_IPACCESSLIST,
		TVIEW_CORE_DATABASE_INFO,
		TVIEW_CORE_IMPORT_EXPORT,

		//MVIEW_BOENTITY_DATA					
		MVIEW_BOENTITYRECORD_WRITE,
		MVIEW_BOENTITYRECORD_READ,
		MVIEW_BOENTITYGROUP_DATA,
		
		MVIEW_BOENTITYRECORD_PREPAREFOREDIT,
		
		//Stored list
		TVIEW_BUS_STOREDLIST,
		TVIEW_BUS_STOREDLIST_ITEMS,
		
		MVIEW_BOENTITYRECORD_SUBGROUPS,
		MVIEW_BOENTITYRECORD_GROUPS,
		MVIEW_GRID_COLUMN_SETUP,
		MVIEW_DATE_SELECTOR,
		
		//Access Rights
		TVIEW_CORE_ROLE,
		TVIEW_BUS_PERSONROLE,
		TVIEW_BUS_PERSON,
		TVIEW_CORE_USER,
		TVIEW_CORE_USERROLE,
		TVIEW_CORE_ACCESSRIGHTS,
		TVIEW_CORE_ACCRSET,
		TVIEW_CORE_ROLE_VIEW,
		TVIEW_PERSON_TREE_VIEW,
		TVIEW_USER_TREE_VIEW,
		TVIEW_PERSON_ROLE_LIST,
		TVIEW_USER_ROLE_LIST,
		TVIEW_CORE_ACCRSETROLE,

		//BUS PERSON ENTITY:
		TVIEW_BUS_PERSON_SELECTION,
		TVIEW_BUS_PERSON_IMPORT,

		TVIEW_CORE_USER_SELECTION,
		TVIEW_CORE_USER_SELECTION_SHORT,

		//organizational structures
		TVIEW_BUS_ORGANIZATION,
		TVIEW_BUS_DEPARTMENT,
		TVIEW_BUS_COST_CENTER,

		TVIEW_BUS_ORGANIZATION_SELECTION,
		TVIEW_BUS_DEPARTMENT_SELECTION,
		TVIEW_BUS_COST_CENTER_SELECTION,


		//CONTACTS:		
		TVIEW_BUS_CM_CONTACT,
		TVIEW_BUS_CM_CREDITOR,
		TVIEW_BUS_CM_PHONE,
		TVIEW_BUS_CM_DEBTOR,
		TVIEW_BUS_CM_PAYMENT_CONDITIONS,
		TVIEW_BUS_CM_INTERNET,
		TVIEW_BUS_CM_EMAIL,
		TVIEW_BUS_CM_ADDRESS,
		TVIEW_BUS_CM_PICTURE,
		TVIEW_BUS_CM_JOURNAL,
		TVIEW_BUS_CM_TYPES,
		TVIEW_BUS_CM_ADDRESS_SCHEMAS,
		TVIEW_BUS_CM_ADDRESS_TYPES,

		TVIEW_BUS_BIG_PICTURE,

		TVIEW_BUS_CM_SUBSET_SELECTION,
		TVIEW_BUS_CONTACT_SELECTION,
		TVIEW_BUS_CONTACT_SELECTION_WITH_ACTIVE_GROUP_PERIOD,
		TVIEW_BUS_CONTACT_SELECTION_WITH_PICS,
		TVIEW_BUS_CONTACT_QUICK_INFO,
		TVIEW_BUS_CONTACT_FULL,
		TVIEW_BUS_CONTACT_FULL_TEXT_IMPORT,
		TVIEW_BUS_CM_CONTACT_SELECT_FUI,
		TVIEW_BUS_CM_CONTACT_SELECT_PHONETS,
		TVIEW_BUS_CM_CONTACT_SELECT_SHORT,

		//special lists for contact:
		TVIEW_BUS_CM_PHONE_SELECT,
		TVIEW_BUS_CM_PHONE_SELECT_EXT,
		TVIEW_BUS_CM_EMAIL_SELECT,
		TVIEW_BUS_CM_INTERNET_SELECT,
		TVIEW_BUS_CM_ADDRESS_SELECT,
		TVIEW_BUS_CM_ADDRESS_TYPES_SELECT,
		TVIEW_BUS_CM_JOURNAL_SELECT,
		TVIEW_BUS_CM_DEAFULTS,			

		TVIEW_BUS_VOICECALLS,
		TVIEW_CE_EVENT,
		TVIEW_CE_COMM_ENTITY,
		TVIEW_CE_PROJECT_LINK,
		TVIEW_CE_TYPE,
		TVIEW_CE_TYPE_SELECT,
		TVIEW_CE_EVENT_TYPE,
		TVIEW_CE_CONTACT_LINK,
		TVIEW_CE_CHARGES_LINK,

		TVIEW_BUS_PROJECT,
		TVIEW_BUS_PROJECT_READ,
		TVIEW_BUS_PROJECT_IMPORT,
		TVIEW_BUS_PROJECT_SELECT,

		//REPORTS.
		TVIEW_ORG_DEPT_REPORT,				
		TVIEW_RPT_TABLE_VIEW,

		TVIEW_BUS_OPT_GRID_VIEWS,
		TVIEW_BUS_OPT_SETTINGS,
		TVIEW_BUS_NMRX_RELATION,
		TVIEW_BUS_NMRX_ROLE,
		TVIEW_BUS_GROUP_TREE,
		TVIEW_BUS_GROUP_ITEMS,
		TVIEW_BUS_CM_GROUP,
		TVIEW_BUS_CM_GROUP_SELECT,
		TVIEW_BUS_NMRX_RELATION_SELECT,

		TVIEW_BUS_EMAIL,
		TVIEW_BUS_EMAIL_ATTACHMENT,
		TVIEW_BUS_EMAIL_COMM_ENTITY,
		TVIEW_BUS_EMAIL_UPDATE,
		TVIEW_BUS_EMAIL_TEMPLATES,
		TVIEW_BUS_TASKS,

		TVIEW_BUS_DM_USER_PATHS,
		TVIEW_BUS_DM_APPLICATIONS,
		TVIEW_BUS_DM_DOCUMENTS,
		TVIEW_BUS_DM_DOCUMENTS_UPDATE_APP,
		TVIEW_BUS_DM_USER_PATHS_SELECT,
		TVIEW_BUS_DM_DOCUMENTS_COMM_ENTITY,			//doc+com_entity
		TVIEW_BUS_DM_REVISIONS,
		TVIEW_BUS_DM_REVISIONS_SELECT,
		TVIEW_BUS_DM_CHECK_OUT_INFO,

		TVIEW_COMM_GRID_EMAIL_VIEW,
		TVIEW_CONTACT_COMM_GRID_EMAIL_VIEW, 
		TVIEW_COMM_GRID_VOICECALL_VIEW,
		TVIEW_COMM_GRID_DOCUMENT_VIEW, 
		TVIEW_COMM_GRID_LAST_N_DOCUMENT_VIEW, 
		TVIEW_CONTACT_COMM_GRID_DOCUMENT_VIEW, 
		TVIEW_PROJECT_COMM_GRID_DOCUMENT_VIEW, 
		TVIEW_COMM_CONTACT_DOCUMENT_VIEW,
		TVIEW_COMM_CONTACT_EMAIL_VIEW,
		MVIEW_COMM_GRID_SORT_RECORDSET,
		MVIEW_COMM_GRID_MODEL_RECORDSET, 
		MVIEW_COMPLEX_COMM_FILTER,
		MVIEW_CE_TYPES_FILTER_CHECKED_ITEMS, 

		TVIEW_BUS_COMM_VIEW, 
		TVIEW_BUS_COMM_VIEW_SETTINGS, 
		MVIEW_EMAIL_RECIPIENTS, 
		MVIEW_REPORT_PROJECTS_VIEW, 
		MVIEW_COMMGRIDFILTER_WIZARD_VIEW,
		TVIEW_CORE_FTP_SERVERS,
		TVIEW_CORE_ACC_USER_REC,
		TVIEW_CORE_ACC_GROUP_REC,
		
		TVIEW_BUS_STORED_PROJECT_LIST,
		TVIEW_BUS_STORED_PROJECT_LIST_DATA,
		TVIEW_BUS_SMS,
		TVIEW_BUS_CAL_RESERVATION,
		TVIEW_BUS_CAL_RESOURCE,
		TVIEW_BUS_CAL_RESOURCE_SELECT,
		TVIEW_BUS_CAL_INVITE,
		TVIEW_BUS_CAL_INVITE_SELECT,
		TVIEW_BUS_CAL_INVITE_ICALENDAR_FIELDS,
		TVIEW_BUS_CAL_EVENT,
		TVIEW_BUS_CAL_EVENT_PART,
		TVIEW_BUS_CAL_EV_PART_OUID,
		TVIEW_BUS_CAL_OPTIONS,
		TVIEW_BUS_CAL_BREAKS,
		TVIEW_BUS_CAL_EVENT_SELECT,
		TVIEW_BUS_CAL_EVENT_PART_COMM_ENTITY,
		TVIEW_BUS_CAL_OPTIONS_SELECT, 
		MVIEW_CALENDAR_EVENT_PARTS,
		MVIEW_CALENDAR_ONE_EVENT_PARTS,
		MVIEW_CALENDAR_FILTER_VIEW,
		MVIEW_CALENDAR_EVENT_CONTACT_VIEW,
		MVIEW_CALENDAR_EVENT_PROJECT_VIEW,
		MVIEW_CALENDAR_EVENT_RESOURCE_VIEW,
		MVIEW_CALENDAR_EVENT_BREAK_VIEW,
		MVIEW_CALENDAR_ENTITY_VIEW,
		MVIEW_CALENDAR_ITEM_ASSIGNED_RESOURCES_VIEW,
		MVIEW_BUS_CAL_RESERVATION,

		//web services:
		TVIEW_WEBSERVICE_BUS_CM_PHONE_SELECT,
		TVIEW_WEBSERVICE_BUS_CM_SKYPE_SELECT,
		TVIEW_WEBSERVICE_BUS_CM_EMAIL_SELECT,
		TVIEW_WEBSERVICE_BUS_CM_INTERNET_SELECT,
		TVIEW_WEBSERVICE_BUS_CM_ADDRESS_SELECT, 
		TVIEW_WEBSERVICE_BUS_DM_DOCUMENT_SELECT,
		TVIEW_WEBSERVICE_BUS_DM_DOCUMENT_DETAILS,
		TVIEW_WEBSERVICE_BUS_EMAIL_SELECT,
		TVIEW_WEBSERVICE_BUS_EMAIL_ATTACHMENT_LIST,
		TVIEW_WEBSERVICE_BUS_PROJECT_LIST,
		TVIEW_WEBSERVICE_BUS_PROJECT_SELECT,
		TVIEW_WEBSERVICE_BUS_CALENDAR_SELECT,
		TVIEW_WEBSERVICE_BUS_COMM_VIEWS,
		TVIEW_WEBSERVICE_BUS_COMM_DATA,

		MVIEW_CALENDAR_PERSONAL_SETTINGS,
		TVIEW_BUS_USER_CONTACT_RELATIONSHIP_IMPORT,
		TVIEW_BUS_CONTACT_CONTACT_RELATIONSHIP_IMPORT,
		TVIEW_BUS_CONTACT_CONTACT_RELATIONSHIP_EXPORT,
		TVIEW_BUS_REMINDERS,
		MVIEW_REMINDER_SETTINGS,
		TVIEW_BUS_DM_SERVER_FILE,
		TVIEW_BUS_DM_META_DATA, 
		TVIEW_BUS_CAL_VIEW,
		TVIEW_GROUP_IMPORT,
		TVIEW_BUS_CAL_VIEW_ENTITIES,
		TVIEW_BUS_CAL_VIEW_CE_TYPES,
		TVIEW_BUS_CAL_VIEW_ENTITIES_SELECT,
		TVIEW_BUS_CM_PAYMENT_CONDITIONS_SELECT,
		MVIEW_CALENDAR_EVENT_PARTS_SIMPLE,

		//SPC clone tables:
		TVIEW_SPC_ACTIVITY,
		TVIEW_SPC_INDUSTRY_GROUP,
		TVIEW_SPC_PR_AREA,
		TVIEW_SPC_PR_STATUS,
		TVIEW_SPC_PR_TYPE,
		TVIEW_SPC_PROCESS,
		TVIEW_SPC_RATE_CAT,
		TVIEW_SPC_SECTOR,
		TVIEW_SPC_SERVICE_TYPE,

		TVIEW_BUS_CUSTOM_BOOL,
		TVIEW_BUS_CUSTOM_DATE,
		TVIEW_BUS_CUSTOM_DATETIME,
		TVIEW_BUS_CUSTOM_FIELDS,
		TVIEW_BUS_CUSTOM_FLOAT,
		TVIEW_BUS_CUSTOM_INT,
		TVIEW_BUS_CUSTOM_LONGTEXT,
		TVIEW_BUS_CUSTOM_TEXT,
		TVIEW_BUS_CUSTOM_SELECTIONS,
		TVIEW_BUS_CUSTOM_FIELDS_SELECT,
		TVIEW_BUS_CUSTOM_FIELDS_DATA,
		TVIEW_BUS_CUSTOM_CONTACT_DATA_IMPORT,

		TVIEW_SPC_BUDGET_VALS,
		TVIEW_BUS_PUSH_MESSAGE,
		TVIEW_BUS_PUSH_TOKEN_PERSON,
		TVIEW_BUS_EMAIL_BINARY

	};

	static DbView& getView(int nViewID);
	static QString getSQLView(int nViewID, bool bDistinct = false);
	static QString getSQLInsert(int nViewID,QSet<int> *pLstIsLongVarcharField=NULL);
	static QString getFullSQLInsert(int nViewID,QSet<int> *pLstIsLongVarcharField=NULL);
	static QString getSQLUpdate(int nViewID,QSet<int> *pLstIsLongVarcharField=NULL);
	static QString getSQLColumnsFromView(DbView &view);
	static QStringList* getAllLongVarCharFields();



	//Table Mosting view
	static QString getFieldName(int nTableID,QString strFieldSuffix);
	static void InitTreeSelectionView(int nViewID, DbView &view, bool bOrderColumnsFromStart=false);

	
	static int getInsertListMapping(int nViewID);
	static int getUpdateListMapping(int nViewID);


protected:
	static void Initialize();
	static int findView(int nViewID);
	static bool CheckUniqueView();
	static QList<DbView>	s_lstDbViews;
	static bool				bInitialized;				///<if false first one to use this class will init
	static QStringList		s_lstAllLongVarCharFields;
	static bool				s_bAllLongVarCharFieldsInitialized;
	
};


#endif



