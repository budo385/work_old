#ifndef DBCONNSETTINGS_H
#define DBCONNSETTINGS_H

#include <QString>


/*!
\class DbConnectionSettings
\brief Sql connection settings class
\ingroup SQLModule

Data container for storing connection settings to one Database.
*/
class DbConnectionSettings
{
public:
	DbConnectionSettings(): 
	 m_nDbPort(0), 
	 m_nDbPoolTimeout(0),
	 m_strTableSpace("SOKRATES_TABLE"),
	 m_strIndexSpace("SOKRATES_INDEX"){};
	
	~DbConnectionSettings();
	DbConnectionSettings(const DbConnectionSettings &that);
	void operator =(const DbConnectionSettings &that); 
	
	QString m_strCustomName;	///< connection custom name "My sql database 1"
	QString m_strDbType;		///< Database type "QMYSQL", "QOCI", ...
	QString m_strDbHostName;	///< Host name.
	QString m_strDbUserName;	///< DB User name.
	QString m_strDbPassword;	///< DB passsword.
	QString m_strDbOptions;		///< DB connection options.
	QString m_strDbName;		///< DB name.
	QString m_strTableSpace;	///< table space	only for some DB's
	QString m_strIndexSpace;	///< index space    only for some DB's
	int m_nDbPort;				///< DB connection port.
	int m_nDbPoolTimeout;		///< DB pool connection timeout.
};


#endif //DBCONNSETTINGS_H


