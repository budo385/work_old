#ifndef DBSQLTABLEDEFINITION_H
#define DBSQLTABLEDEFINITION_H

#include <QString>
#include <QHash>
#include <QList>
#include <QListIterator>
#include "common/common/status.h"
#include "dbsqltableview.h"
#include "common/common/dbrecordset.h"
#include "dbtableiddefinition.h"

/*!
	\class DbTableKeyData
	\brief Table key data definition class, used in DbSqlTableDefinition for describing one table
	\ingroup Db_Core

	Class for storing table informations
*/
class DbTableKeyData
{
public:
	DbTableKeyData(){};
	/*
	DbTableKeyData(QString strTableName, QString strPrimaryKey, QString strGlobalID, int nViewID, 
				   int nParentTableID = -1, QString strParentForeignField = "", bool bCheckParent = true, bool bHasActiveFlag = true);
				   */

	DbTableKeyData(QString strTableName, QString strPrimaryKey, int nViewID, 
		int nParentTableID = -1, QString strParentForeignField = "", bool bCheckParent = true, bool bHasActiveFlag = true);

	DbTableKeyData(const DbTableKeyData &that);
	void operator =(const DbTableKeyData &that);

public:
	QString m_strTableName;			///< Table Name.
	int		m_nViewID;				///< Table View ID (enum ViewID).
	int		m_nParentTableID;		///< Parent table ID.
	QString m_strParentForeignField;///< Parent foreign field name.
	bool	m_bCheckParent;			///< Check parent tables switch.
	bool	m_bHasActiveFlag;		///< Table uses "_ACTIVE_FLAG" field to hide old records

	//--------mandatory fields:
	QString m_strPrimaryKey;		///< Table primary key.
	QString m_strGlobalID;			///< Global ID column
	QString m_strLastModified;		///< LastModified column

	QString m_strFieldPrefix;		///< Table Prefix

	QString m_strEntityName;		///< Entity name: e.g. Contact for BUS_CM_CONTACT
	QString m_strEntityNamePlural;	///< Entity name plural: e.g. Contacts for BUS_CM_CONTACT

private:
	QString CalculateTablePrefix(QString strPK);
};



/*!
\class DbSqlTableDefinition
\brief Global data container for all key informations about all tables in SOKRATES_XP database
\ingroup Db_Core

Abstract class for instances of table definitions per database type.
*/
class DbSqlTableDefinition
{
public:
	DbSqlTableDefinition(){};
	~DbSqlTableDefinition(){};
	static  void	GetKeyData(int TableID, DbTableKeyData &KeyData);
	static  int		GetFullViewID(int TableID);
	static  void	CopyFromList2RecordSet(int TableID, const QList<int> &lstPKs,DbRecordSet &lstFull);
	static  void	CopyFromRecordSet2List(int TableID, QList<int> &lstPKs,const DbRecordSet &lstFull);
	static	QString GetParentSqlRecordLockString(int TableID);
	static  int		GetParentID(int TableID);

	static int	   GetTableID(QString strTableName);
	static QString GetTablePrefix(int TableID);
	static QString GetTableName(int TableID);
	static QString GetTablePrimaryKey(int TableID);
	static void GetEntityName(QString strTableName,QString& strEntityName,QString& strEntityNamePlural);
	static void GetAllTableData(QHash<int, DbTableKeyData> &data){data=s_hshTableKeyData;}

	//SQL helper:
	static int GenerateChunkedWhereStatement(const DbRecordSet &pLstOfID,QString strColIDName,QString &strWhere,int nStartFrom=0, int nChunkSize=400,bool bAddQuotes=false);
	static bool GetTableDataFromSQL(bool bInsertSQL,QString strSQLInsertStatement, QString &strTableName, QString &strPrimaryKey,QString &strLastModified, QString &strCreated);
	

protected:
	static void							Initialize();
	static QString						CalculateTablePrefix(QString strPK);
	static void							FillParentSqlHash(int TableID, QHash<int, int> &hshParentHash, int &nParentNumber, bool &bLastItem);
	static QHash<int, DbTableKeyData>   s_hshTableKeyData;
	static bool							bInitialized;				///<if false first one to use this class will init

};

#endif


