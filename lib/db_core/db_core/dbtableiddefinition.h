/*!
\brief Definitions of Table IDs
\ingroup Db_Core

Header file with definitions for TableName to TableID conversion.

*/
#ifndef DBTABLEIDDEFINITION_H
#define DBTABLEIDDEFINITION_H

#define CORE_LOCKING				0	//<Locking table
#define CORE_APP_SRV_SESSION		1	//<app server session table
#define CORE_USER_SESSION			2	//<user session table
#define CORE_EVENT_LOG				3	//<user session table
#define CORE_IPACCESSLIST			21	//<IP kick ban list
#define CORE_DATABASE_INFO			59
#define CORE_IMPORT_EXPORT			62

//Test table for tree view
#define TEST_TREE					4	//<TEST_TREE table

//Stored List
#define BUS_STOREDLIST				6	//<Stored list table.
#define BUS_STOREDLIST_ITEMS		7	//<Stored list table items.

//Access Rights
#define CORE_ROLE					8	//<Acces Rights Role description table.
#define BUS_PERSONROLE				9	//<Acces Rights Role to Person table.
#define BUS_PERSON					10	//<Acces Rights Person table.
#define CORE_USER					11	//<Acces Rights User table.
#define CORE_USERROLE				12	//<Acces Rights User table.
#define CORE_ACCESSRIGHTS			13	//<Acces Rights Access Rights table.
#define CORE_ACCRSET				14	//<Acces Rights Access Rights Set table.
#define CORE_ACCRSETROLE			5	//<Acces Rights Access Rights Set to Role table.

//Le grande test d' Bo entity (OBSOLETE):
#define TEST_ENTITY_CONTACT			15	//< Main entity table 1:1
#define TEST_ENTITY_PHONES			16	//< 1:N related, phones
#define TEST_ENTITY_INFO			17	//< 1:N big binary fields

//organizational structures
#define BUS_ORGANIZATION			18
#define BUS_DEPARTMENT				19
#define BUS_COST_CENTER				20
#define BUS_PROJECT					42

//Contacts:
#define BUS_CM_CONTACT				22
#define BUS_CM_CREDITOR				23
#define BUS_CM_PHONE				24
#define BUS_CM_DEBTOR				25
#define BUS_CM_PAYMENT_CONDITIONS	26
#define BUS_CM_INTERNET				27
#define BUS_CM_EMAIL				28
#define BUS_CM_ADDRESS				29
#define BUS_CM_PICTURE				30
#define BUS_CM_JOURNAL				31
#define BUS_CM_TYPES				32
#define BUS_CM_ADDRESS_SCHEMAS		33

#define BUS_CM_ADDRESS_TYPES		43
#define BUS_BIG_PICTURE				44

#define BUS_VOICECALLS				34
#define CE_COMM_ENTITY				35
#define CE_EVENT					36
#define CE_PROJECT_LINK				37
#define CE_TYPE						38
#define CE_CHARGES_LINK				39
#define CE_CONTACT_LINK				40
#define CE_EVENT_TYPE				41
#define BUS_OPT_GRID_VIEWS			45
#define BUS_NMRX_RELATION			46
#define BUS_NMRX_ROLE				47
#define BUS_OPT_SETTINGS			48
#define BUS_GROUP_TREE				49
#define BUS_GROUP_ITEMS				50
#define BUS_CM_GROUP				51
#define BUS_EMAIL					52
#define BUS_TASKS					53
#define BUS_DM_USER_PATHS			54
#define BUS_DM_APPLICATIONS			55
#define BUS_DM_DOCUMENTS			56
#define BUS_EMAIL_ATTACHMENT		57
#define BUS_DM_REVISIONS			58

#define BUS_COMM_VIEW				60
#define BUS_COMM_VIEW_SETTINGS		61
#define CORE_FTP_SERVERS			63
#define CORE_ACC_USER_REC			64
#define CORE_ACC_GROUP_REC			65

#define BUS_STORED_PROJECT_LIST		66
#define BUS_STORED_PROJECT_LIST_DATA 67

#define BUS_SMS						68
#define BUS_CAL_RESERVATION			69
#define BUS_CAL_RESOURCE			70
#define BUS_CAL_INVITE				71
#define BUS_CAL_EVENT				72
#define BUS_CAL_EVENT_PART			73
#define BUS_CAL_OPTIONS				74
#define BUS_CAL_BREAKS				75
#define BUS_CAL_EV_PART_OUID		76
#define BUS_REMINDERS				77
#define BUS_DM_SERVER_FILE			78
#define BUS_DM_SERVER_DIR			79
#define BUS_DM_META_DATA			80
#define BUS_CAL_VIEW				81
#define BUS_CAL_VIEW_ENTITIES		82
#define BUS_CAL_VIEW_CE_TYPES		83

#define F_ABT_MITARB				84
#define F_ABT_RECHTE				85
#define F_ABTEILUNG					86
#define F_AGENTS					87
#define F_BANKVERBINDUNGEN			88
#define F_BATCH_JOBS				89
#define F_BEL_PROJ_LISTEN			90
#define F_COUNTERS					91
#define F_COUNTRY					92
#define F_DATENAKTUALISIERUNG		93
#define F_DEBI_BUCH_RECH			94
#define F_DEBI_BUCH_ZAHL			95
#define F_DEFAULTS					96
#define F_DELETE					97
#define F_EXCHANGEFORMATS			98
#define F_EXT_APPLICATIONS			99
#define F_FAKT_PERS_LEIST			100
#define F_FAKT_PLAN					101
#define F_FAKT_PLAN_LOG				102
#define F_FAKT_UMLAGEN				103
#define F_FAKTURA					104
#define F_FAKTURA_EIN				105
#define F_FERIEN					106
#define F_FUNKTIONEN				107
#define F_GEBIETE					108
#define F_HONORARKLASSE				109
#define F_IND_PROJNAME				110
#define F_INOUTTIMES				111
#define F_KA_KLASSEN				112
#define F_KNTRL_PERIODE				113
#define F_KOMPENSATION				114
#define F_KONTEN					115
#define F_KONTENPLAN				116
#define F_KONTO_NAMEN				117
#define F_KOSTENARTEN				118
#define F_KS_BUDGET					119
#define F_LEISTUNGSARTEN			120
#define F_LETZTER_EXP				121
#define F_LICENCE_RESERVATION		122
#define F_LOCKING					123
#define F_MITARB_BUDGET				124
#define F_MODULE_LICENCES			125
#define F_NK_BUDGET					126
#define F_OBJEKTNUMMERN				127
#define F_ORGANISATION				128
#define F_PERIODENART				129
#define F_PERS_FUNKTION				130
#define F_PERSPLLINES				131
#define F_PERSPROJLISTS				132
#define F_PFLICHTFELDER				133
#define F_PR_CACHE					134
#define F_PR_STATUS					135
#define F_PREASSIGN					136
#define F_PRINT_LABELS				137
#define F_PROJ_BAUHERR				138
#define F_PROJ_BEREICHE				139
#define F_PROJ_BUDGETKORREKTUR		140
#define F_PROJ_MITARB				141
#define F_PROJ_PHASEN				142
#define F_PROJ_RECHTE				143
#define F_PROJ_RESTAUFWAND			144
#define F_PROJ_STRUKT				145
#define F_PROJECTUSERDEFINED		146
#define F_PROJECTUSERDEFINED_CHAR	147
#define F_PROJECTUSERDEFINED_DATE	148
#define F_PROJECTUSERDEFINED_FLOAT	149
#define F_PROJECTUSERDEFINEDFIELDS	150
#define F_PROJECTUSERDEFINEDGUIMAP	151
#define F_PROJEKTART				152
#define F_PROJEKTLISTEN				153
#define F_PROZESSE					154
#define F_RECHTE					155
#define F_RECORD_ID					156
#define F_REPORTLISTEN				157
#define F_REPORTPROFIL				158
#define F_REPORTSETS				159
#define F_SOLLZEIT_KLASSEN			160
#define F_SOLLZEITEN				161
#define F_STD_KONTROLLE				162
#define F_STEMPELUHR				163
#define F_SYNCHRO					164
#define F_SZ_EXCEPTIONS				165
#define F_TAET_LIST_ENTRY			166
#define F_TAET_LIST_NAMES			167
#define F_TAETIGKEITEN				168
#define F_TARIFE					169
#define F_TARIFKATEG				170
#define F_TIMELINE_ASSOC			171
#define F_TIMELINE_CHAR200			172
#define F_TIMELINE_CHAR50			173
#define F_TIMELINE_DATE				174
#define F_TIMELINE_FLOAT			175
#define F_TIMELINE_LONG				176
#define F_TIMELINE_SHORT			177
#define F_TIMESERVER				178
#define F_TK_KA_KLASSEN				179
#define F_TK_REFERENCES				180
#define F_UDF_BOOLEAN				181
#define F_UDF_CHAR					182
#define F_UDF_DATE					183
#define F_UDF_FIELD					184
#define F_UDF_FLOAT					185
#define F_UDF_INTEGER				186
#define F_UDF_POSITION_GRID			187
#define F_UDF_POSITION_REP			188
#define F_UDF_POSITION_WIN			189
#define F_UDF_PREDEF_FILL			190
#define F_UDF_TEXT					191
#define F_UDF_VIEW					192
#define F_USER_APPLICATIONS			193
#define F_USER_SESSIONS				194
#define F_USER_SETTINGS				195
#define F_VERSICHERUNGSKATEGORIEN	196
#define F_WAEHRUNG					197
#define F_WIRTSCH_GRUPPE			198
#define F_WT_CP_SCHEMA				199
#define F_WT_DAY_SCHEMA				200
#define F_ZAHL_EINGANG				201
#define F_ZAHLUNGSARTEN				202
#define FASSIGN						203
#define FD_DOK_VORLAGEN				204
#define FD_DOKUMENTE				205
#define FD_DOKUMENTENARTEN			206
#define FF_BAUSTEINE				207
#define FF_DEF_FAKT					208
#define FF_DEF_FAKT_POS				209
#define FF_RECHNUNG_POS				210
#define FL_DEFAULT					211
#define FL_DEFAULT2					212
#define FPERSON						213
#define FPROJCT						214
#define FSA_ADRESSEN				215

#define	ZS_BINARY_FIELDS			216
#define	ZS_TEXT_FIELDS				217


#define	SPC_ACTIVITY				218
#define	SPC_INDUSTRY_GROUP			219
#define	SPC_PR_AREA					220
#define	SPC_PR_STATUS				221
#define	SPC_PR_TYPE					222
#define	SPC_PROCESS					223
#define	SPC_RATE_CAT				224
#define	SPC_SECTOR					225
#define	SPC_SERVICE_TYPE			226


#define	BUS_CUSTOM_BOOL				227
#define	BUS_CUSTOM_DATE				228
#define	BUS_CUSTOM_DATETIME			229
#define	BUS_CUSTOM_FIELDS			230
#define	BUS_CUSTOM_FLOAT			231
#define	BUS_CUSTOM_INT				232
#define	BUS_CUSTOM_LONGTEXT			233
#define	BUS_CUSTOM_SELECTIONS		234
#define	BUS_CUSTOM_TEXT				235

#define	BUS_PUSH_MESSAGE			236
#define	BUS_PUSH_TOKEN_PERSON		237
#define	BUS_EMAIL_BINARY			238



//WARNING: when adding new table ID, get latest number, increase it, and put it below:
//NEXT NUMBER: 235

#endif



