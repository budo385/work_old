#include "dbconnectionslist.h"
#include "common/common/sha256hash.h"
#include "db_drivers.h"

DbConnectionsList::DbConnectionsList()
{
}

DbConnectionsList::~DbConnectionsList()
{
}

void DbConnectionsList::Clear()
{
	m_lstConnections.clear();
}

bool DbConnectionsList::Load(QString strFile, QString strPassword)
{
	Clear();

	m_objIni.SetPassword(strPassword);
	m_objIni.SetPath(strFile);
	if(!m_objIni.Load())
		return false;

	//load a list of network connections
	int nCount = 0;
	m_objIni.GetValue("DbConnections", "Count", nCount);
	for(int i=0; i<nCount; i++)
	{
		//load a single DbConnectionSettings info
		QString strKey, strName, strHost, strDbName, strType, strUser, strPass;
		QString strDbOptions, strTableSpace, strIndexSpace;
		int nPort, nTimeout;

		strKey.sprintf("Name%d", i+1);
		m_objIni.GetValue("DbConnections", strKey, strName);

		strKey.sprintf("DbName%d", i+1);
		m_objIni.GetValue("DbConnections", strKey, strDbName);

		strKey.sprintf("Host%d", i+1);
		m_objIni.GetValue("DbConnections", strKey, strHost);

		strKey.sprintf("Port%d", i+1);
		m_objIni.GetValue("DbConnections", strKey, nPort, 1111);

		strKey.sprintf("Type%d", i+1);
		m_objIni.GetValue("DbConnections", strKey, strType, DBTYPE_MYSQL);
		
		strKey.sprintf("User%d", i+1);
		m_objIni.GetValue("DbConnections", strKey, strUser, "");

		strKey.sprintf("Pass%d", i+1);
		m_objIni.GetValue("DbConnections", strKey, strPass, "");

		strKey.sprintf("DbOptions%d", i+1);
		m_objIni.GetValue("DbConnections", strKey, strDbOptions, "");

		strKey.sprintf("Timeout%d", i+1);
		m_objIni.GetValue("DbConnections", strKey, nTimeout, 0);
	
		strKey.sprintf("TableSpace%d", i+1);
		m_objIni.GetValue("DbConnections", strKey, strTableSpace, "");

		strKey.sprintf("IndexSpace%d", i+1);
		m_objIni.GetValue("DbConnections", strKey, strIndexSpace, "");

		DbConnectionSettings info;
		info.m_strCustomName  = strName;
		info.m_strDbName	  = strDbName;
		info.m_strDbHostName  = strHost;
		info.m_nDbPort		  = nPort;
		info.m_strDbType	  = strType;
		info.m_strDbUserName  = strUser;
		info.m_strDbPassword  = strPass;
		info.m_strDbOptions   = strDbOptions;
		info.m_strTableSpace  = strTableSpace;
		info.m_strIndexSpace  = strIndexSpace;
		info.m_nDbPoolTimeout = nTimeout;
		
		m_lstConnections.append(info);
	}

	return true;
}

bool DbConnectionsList::Save(QString strFile, QString strPassword)
{
	m_objIni.SetPassword(strPassword);
	m_objIni.SetPath(strFile);

	//fill the data inside
	m_objIni.RemoveSection("DbConnections");	//cleanup existing data

	QString strKey;

	int nCnt = m_lstConnections.size();
	m_objIni.SetValue("DbConnections", "Count", nCnt);

	for(int i=0; i<nCnt; i++)
	{
		//save a single NetworkConnection info
		strKey.sprintf("Name%d", i+1);
		m_objIni.SetValue("DbConnections", strKey, m_lstConnections[i].m_strCustomName);
	
		strKey.sprintf("Host%d", i+1);
		m_objIni.SetValue("DbConnections", strKey, m_lstConnections[i].m_strDbHostName);

		strKey.sprintf("Port%d", i+1);
		m_objIni.SetValue("DbConnections", strKey, m_lstConnections[i].m_nDbPort);

		strKey.sprintf("DbName%d", i+1);
		m_objIni.SetValue("DbConnections", strKey, m_lstConnections[i].m_strDbName);

		strKey.sprintf("Type%d", i+1);
		m_objIni.SetValue("DbConnections", strKey, m_lstConnections[i].m_strDbType);

		strKey.sprintf("User%d", i+1);
		m_objIni.SetValue("DbConnections", strKey, m_lstConnections[i].m_strDbUserName);
		
		strKey.sprintf("Pass%d", i+1);
		m_objIni.SetValue("DbConnections", strKey, m_lstConnections[i].m_strDbPassword);

		strKey.sprintf("DbOptions%d", i+1);
		m_objIni.SetValue("DbConnections", strKey, m_lstConnections[i].m_strDbOptions);

		strKey.sprintf("Timeout%d", i+1);
		m_objIni.SetValue("DbConnections", strKey, m_lstConnections[i].m_nDbPoolTimeout);

		strKey.sprintf("TableSpace%d", i+1);
		m_objIni.SetValue("DbConnections", strKey, m_lstConnections[i].m_strTableSpace);

		strKey.sprintf("IndexSpace%d", i+1);
		m_objIni.SetValue("DbConnections", strKey, m_lstConnections[i].m_strIndexSpace);
	}

	return m_objIni.Save();
}

void DbConnectionsList::ClearUserPassword()
{
	m_objIni.SetValue("UserInfo", "UsePassword", 0);
	m_objIni.SetValue("UserInfo", "PasswordHash", "");
}

QString DbConnectionsList::GetPasswordHash()
{
	QString strPassHash;
	m_objIni.GetValue("UserInfo", "PasswordHash", strPassHash);
	return strPassHash;
}

bool DbConnectionsList::MatchUserPassword(QString strPassword)
{
	if(IsUserPasswordSet())
	{
		//compare password hashes (converted to hex)
		QString strPassHash = GetPasswordHash();
		return strPassHash == GetHashString(strPassword);
	}
	return false;
}

bool DbConnectionsList::IsUserPasswordSet()
{
	int nUsePass = 0;
	m_objIni.GetValue("UserInfo", "UsePassword", nUsePass, 0);
	return (nUsePass > 0);
}

bool DbConnectionsList::SetUserPasswordHash(QString strPassHash)
{
	m_objIni.SetValue("UserInfo", "UsePassword", 1);
	m_objIni.SetValue("UserInfo", "PasswordHash", strPassHash);
	return true;
}

QString DbConnectionsList::GetHashString(QString strPassword)
{
	QByteArray data;
	data.append(strPassword);

	//calculate hash value
	QByteArray hash;
	Sha256Hash Hasher;
	hash = Hasher.GetHash(data);

	//convert it into the string (binary to Base64)
	QString strResult;
	strResult = hash.toBase64();
	return strResult;
}




//gets connection settings by name: false if not found
bool DbConnectionsList::GetDbSettings(QString strSettingsName,DbConnectionSettings &info)
{

	for(int i=0;i<m_lstConnections.size();++i)
	{
		if(m_lstConnections.at(i).m_strCustomName==strSettingsName.trimmed())
		{
			info=m_lstConnections.at(i);
			return true;
		}
	}
	return false;
}
