#include "dbsqltabledefinition.h"

bool DbSqlTableDefinition::bInitialized=false;
QHash<int, DbTableKeyData> DbSqlTableDefinition::s_hshTableKeyData;



//------------------------------------------------------
//			DbTableKeyData
//------------------------------------------------------



//automatic field calculation based on PK:
DbTableKeyData::DbTableKeyData(QString strTableName, QString strPrimaryKey, int nViewID, 
			   int nParentTableID, QString strParentForeignField, bool bCheckParent, bool bHasActiveFlag)
{

	m_strTableName			= strTableName;
	m_strPrimaryKey			= strPrimaryKey;
	m_strFieldPrefix		= CalculateTablePrefix(m_strPrimaryKey);
	m_strGlobalID			= m_strFieldPrefix+"_GLOBAL_ID";
	m_strLastModified		= m_strFieldPrefix+"_DAT_LAST_MODIFIED";
	m_nViewID				= nViewID;
	m_nParentTableID		= nParentTableID;
	m_strParentForeignField	= strParentForeignField;
	m_bCheckParent			= bCheckParent;
	m_bHasActiveFlag		= bHasActiveFlag;
	m_strEntityName			= "";
	m_strEntityNamePlural	= "";

}


DbTableKeyData::DbTableKeyData(const DbTableKeyData &that)
{
	operator =(that);
}
void DbTableKeyData::operator =(const DbTableKeyData &that)
{
	m_strTableName			= that.m_strTableName;
	m_strPrimaryKey			= that.m_strPrimaryKey;
	m_strGlobalID			= that.m_strGlobalID;
	m_strLastModified		= that.m_strLastModified;
	m_nViewID				= that.m_nViewID;
	m_nParentTableID		= that.m_nParentTableID;
	m_strParentForeignField	= that.m_strParentForeignField;
	m_bCheckParent			= that.m_bCheckParent;
	m_bHasActiveFlag		= that.m_bHasActiveFlag;
	m_strFieldPrefix		= that.m_strFieldPrefix;
	m_strEntityName			= that.m_strEntityName;
	m_strEntityNamePlural	= that.m_strEntityNamePlural;
}



/*!
Calculated prefix: search first: "_"

\param lstPKs	- primary key
\return prefix without _
*/
QString DbTableKeyData::CalculateTablePrefix(QString strPK)
{
	//getFieldName:	
	int nIdx=strPK.indexOf("_");
	if(nIdx==-1) 
		return "";
	else
		return strPK.left(nIdx);

}


//------------------------------------------------------
//			DbSqlTableDefinition
//------------------------------------------------------

//init static data:

//in this function insert own key data. NOTE: Every table must have one and only one ENTRY in this class
void DbSqlTableDefinition::Initialize()
{
	if (bInitialized)	return;
	bInitialized=true;

	DbTableKeyData TableKeyData;

	//CORE_LOCKING
	TableKeyData=DbTableKeyData("CORE_LOCKING", "CORL_ID", DbSqlTableView::TVIEW_CORE_LOCKING);
	s_hshTableKeyData.insert(CORE_LOCKING, TableKeyData);		//TABLE_ID, TABLE KEY DATA.
	

	//CORE_APP_SERVER
	TableKeyData=DbTableKeyData("CORE_APP_SRV_SESSION", "COAS_ID",  DbSqlTableView::TVIEW_CORE_APP_SRV_SESSION/*, 0, "COAS_ROLE_ID", false*/);
	s_hshTableKeyData.insert(CORE_APP_SRV_SESSION, TableKeyData);

	//CORE_USER_SESSION
	TableKeyData=DbTableKeyData("CORE_USER_SESSION", "COUS_ID", DbSqlTableView::TVIEW_CORE_USER_SESSION, CORE_APP_SRV_SESSION, "COUS_APP_SRV_SESSION_ID");
	s_hshTableKeyData.insert(CORE_USER_SESSION, TableKeyData);

	//CORE_EVENT_LOG
	TableKeyData=DbTableKeyData("CORE_EVENT_LOG", "CREL_ID",  DbSqlTableView::TVIEW_CORE_EVENT_LOG);
	s_hshTableKeyData.insert(CORE_EVENT_LOG, TableKeyData);

	//CORE_IPACCESSLIST
	TableKeyData=DbTableKeyData("CORE_IPACCESSLIST", "CIPR_ID",  DbSqlTableView::TVIEW_CORE_IPACCESSLIST);
	s_hshTableKeyData.insert(CORE_IPACCESSLIST, TableKeyData);

	//CORE_DATABASE_INFO
	TableKeyData=DbTableKeyData("CORE_DATABASE_INFO", "CDI_ID",  DbSqlTableView::TVIEW_CORE_DATABASE_INFO);
	s_hshTableKeyData.insert(CORE_DATABASE_INFO, TableKeyData);

	//CORE_DATABASE_INFO
	TableKeyData=DbTableKeyData("CORE_IMPORT_EXPORT", "CIE_ID",  DbSqlTableView::TVIEW_CORE_IMPORT_EXPORT);
	s_hshTableKeyData.insert(CORE_IMPORT_EXPORT, TableKeyData);

	//-------------------------------------------------------------------------------------
	//								Stored list
	//-------------------------------------------------------------------------------------
	//BUS_STOREDLIST
	TableKeyData=DbTableKeyData("BUS_STOREDLIST", "BUSL_ID", DbSqlTableView::TVIEW_BUS_STOREDLIST);
	s_hshTableKeyData.insert(BUS_STOREDLIST, TableKeyData);

	//BUS_STOREDLIST_ITEMS
	TableKeyData=DbTableKeyData("BUS_STOREDLIST_ITEMS", "BUSLI_ID",  DbSqlTableView::TVIEW_BUS_STOREDLIST_ITEMS);
	s_hshTableKeyData.insert(BUS_STOREDLIST_ITEMS, TableKeyData);

	//-------------------------------------------------------------------------------------
	//								Access Rights
	//-------------------------------------------------------------------------------------
	//CORE_ROLE
	TableKeyData=DbTableKeyData("CORE_ROLE", "CROL_ID", DbSqlTableView::TVIEW_CORE_ROLE);
	s_hshTableKeyData.insert(CORE_ROLE, TableKeyData);

	//CORE_PERSONROLE - CORE_PERSON IS PARENT AND IS CHECKING IT.
	TableKeyData=DbTableKeyData("BUS_PERSONROLE", "BPRL_ID", DbSqlTableView::TVIEW_BUS_PERSONROLE, BUS_PERSON, "BPRL_PERSON_ID");
	s_hshTableKeyData.insert(BUS_PERSONROLE, TableKeyData);

	//CORE_PERSON
	TableKeyData=DbTableKeyData("BUS_PERSON", "BPER_ID",  DbSqlTableView::TVIEW_BUS_PERSON);
	TableKeyData.m_strEntityName="User";
	TableKeyData.m_strEntityNamePlural="Users";
	s_hshTableKeyData.insert(BUS_PERSON, TableKeyData);

	//CORE_USER
	TableKeyData=DbTableKeyData("CORE_USER", "CUSR_ID", DbSqlTableView::TVIEW_CORE_USER);
	TableKeyData.m_strLastModified="CUSR_DAT_LAST_MODIFIED";
	TableKeyData.m_strEntityName="Login Account";
	TableKeyData.m_strEntityNamePlural="Login Accounts";
	s_hshTableKeyData.insert(CORE_USER, TableKeyData);

	//CORE_USERROLE
	TableKeyData=DbTableKeyData("CORE_USERROLE", "CURL_ID", DbSqlTableView::TVIEW_CORE_USERROLE, CORE_USER, "CURL_USER_ID");
	s_hshTableKeyData.insert(CORE_USERROLE, TableKeyData);

	//CORE_ACCESSRIGHTS
	TableKeyData=DbTableKeyData("CORE_ACCESSRIGHTS", "CAR_ID", DbSqlTableView::TVIEW_CORE_ACCESSRIGHTS, CORE_ROLE, "CAR_ROLE_ID");
	s_hshTableKeyData.insert(CORE_ACCESSRIGHTS, TableKeyData);

	//CORE_ACCRSET
	TableKeyData=DbTableKeyData("CORE_ACCRSET", "CAST_ID", DbSqlTableView::TVIEW_CORE_ACCRSET);
	s_hshTableKeyData.insert(CORE_ACCRSET, TableKeyData);

	//CORE_ACCRSETROLE
	TableKeyData=DbTableKeyData("CORE_ACCRSETROLE", "CASTR_ID", DbSqlTableView::TVIEW_CORE_ACCRSETROLE, CORE_ROLE, "CASTR_ROLE_ID");
	s_hshTableKeyData.insert(CORE_ACCRSETROLE, TableKeyData);



	
	//-------------------------------------------------------------------------------------
	//								organizational structures
	//-------------------------------------------------------------------------------------
	TableKeyData=DbTableKeyData("BUS_ORGANIZATIONS", "BORG_ID", DbSqlTableView::TVIEW_BUS_ORGANIZATION);
	TableKeyData.m_strLastModified="BORG_DAT_LAST_MODIFIED";
	TableKeyData.m_strEntityName="Organization";
	TableKeyData.m_strEntityNamePlural="Organizations";
	s_hshTableKeyData.insert(BUS_ORGANIZATION, TableKeyData);

	TableKeyData=DbTableKeyData("BUS_DEPARTMENTS", "BDEPT_ID", DbSqlTableView::TVIEW_BUS_DEPARTMENT);
	TableKeyData.m_strLastModified="BDEPT_DAT_LAST_MODIFIED";
	TableKeyData.m_strEntityName="Department";
	TableKeyData.m_strEntityNamePlural="Departments";
	s_hshTableKeyData.insert(BUS_DEPARTMENT, TableKeyData);

	TableKeyData=DbTableKeyData("BUS_COST_CENTERS", "BCTC_ID", DbSqlTableView::TVIEW_BUS_COST_CENTER);
	TableKeyData.m_strLastModified="BCTC_DAT_LAST_MODIFIED";
	TableKeyData.m_strEntityName="Cost Center";
	TableKeyData.m_strEntityNamePlural="Cost Centers";
	s_hshTableKeyData.insert(BUS_COST_CENTER, TableKeyData);

	TableKeyData=DbTableKeyData("BUS_PROJECTS", "BUSP_ID", DbSqlTableView::TVIEW_BUS_PROJECT);
	TableKeyData.m_strLastModified="BUSP_DAT_LAST_MODIFIED";
	TableKeyData.m_strEntityName="Project";
	TableKeyData.m_strEntityNamePlural="Projects";
	s_hshTableKeyData.insert(BUS_PROJECT, TableKeyData);



	//-------------------------------------------------------------------------------------
	//								CONTACTS
	//-------------------------------------------------------------------------------------

	TableKeyData=DbTableKeyData("BUS_CM_CONTACT", "BCNT_ID", DbSqlTableView::TVIEW_BUS_CM_CONTACT);
	TableKeyData.m_strEntityName="Contact";
	TableKeyData.m_strEntityNamePlural="Contacts";
	s_hshTableKeyData.insert(BUS_CM_CONTACT, TableKeyData);

	TableKeyData=DbTableKeyData("BUS_CM_CREDITOR", "BCMC_ID",  DbSqlTableView::TVIEW_BUS_CM_CREDITOR,BUS_CM_CONTACT,"BCMC_CONTACT_ID");
	TableKeyData.m_strEntityName="Creditor";
	TableKeyData.m_strEntityNamePlural="Creditors";
	s_hshTableKeyData.insert(BUS_CM_CREDITOR, TableKeyData);

	TableKeyData=DbTableKeyData("BUS_CM_PHONE", "BCMP_ID", DbSqlTableView::TVIEW_BUS_CM_PHONE,BUS_CM_CONTACT,"BCMP_CONTACT_ID");
	TableKeyData.m_strEntityName="Phone";
	TableKeyData.m_strEntityNamePlural="Phones";
	s_hshTableKeyData.insert(BUS_CM_PHONE, TableKeyData);

	TableKeyData=DbTableKeyData("BUS_CM_DEBTOR", "BCMD_ID",  DbSqlTableView::TVIEW_BUS_CM_DEBTOR,BUS_CM_CONTACT,"BCMD_CONTACT_ID");
	TableKeyData.m_strEntityName="Debtor";
	TableKeyData.m_strEntityNamePlural="Debtors";
	s_hshTableKeyData.insert(BUS_CM_DEBTOR, TableKeyData);

	//parent is DEBTOR
	TableKeyData=DbTableKeyData("BUS_CM_PAYMENT_CONDITIONS", "BCMPY_ID", DbSqlTableView::TVIEW_BUS_CM_PAYMENT_CONDITIONS,BUS_CM_DEBTOR,"BCMPY_DEBTOR_ID");
	s_hshTableKeyData.insert(BUS_CM_PAYMENT_CONDITIONS, TableKeyData);

	TableKeyData=DbTableKeyData("BUS_CM_INTERNET", "BCMI_ID", DbSqlTableView::TVIEW_BUS_CM_INTERNET,BUS_CM_CONTACT,"BCMI_CONTACT_ID");
	s_hshTableKeyData.insert(BUS_CM_INTERNET, TableKeyData);

	TableKeyData=DbTableKeyData("BUS_CM_EMAIL", "BCME_ID", DbSqlTableView::TVIEW_BUS_CM_EMAIL,BUS_CM_CONTACT,"BCME_CONTACT_ID");
	TableKeyData.m_strEntityName="Email";
	TableKeyData.m_strEntityNamePlural="Emails";
	s_hshTableKeyData.insert(BUS_CM_EMAIL, TableKeyData);

	TableKeyData=DbTableKeyData("BUS_CM_ADDRESS", "BCMA_ID", DbSqlTableView::TVIEW_BUS_CM_ADDRESS,BUS_CM_CONTACT,"BCMA_CONTACT_ID");
	s_hshTableKeyData.insert(BUS_CM_ADDRESS, TableKeyData);

	TableKeyData=DbTableKeyData("BUS_CM_PICTURE", "BCMPC_ID", DbSqlTableView::TVIEW_BUS_CM_PICTURE,BUS_CM_CONTACT,"BCMPC_CONTACT_ID");
	s_hshTableKeyData.insert(BUS_CM_PICTURE, TableKeyData);


	TableKeyData=DbTableKeyData("BUS_CM_JOURNAL", "BCMJ_ID", DbSqlTableView::TVIEW_BUS_CM_JOURNAL,BUS_CM_CONTACT,"BCMJ_CONTACT_ID");
	s_hshTableKeyData.insert(BUS_CM_JOURNAL, TableKeyData);

	//no parent:
	TableKeyData=DbTableKeyData("BUS_CM_TYPES", "BCMT_ID", DbSqlTableView::TVIEW_BUS_CM_TYPES);
	TableKeyData.m_strEntityName="Type Definition";
	TableKeyData.m_strEntityNamePlural="Type Definitions";
	s_hshTableKeyData.insert(BUS_CM_TYPES, TableKeyData);

	TableKeyData=DbTableKeyData("BUS_CM_ADDRESS_SCHEMAS", "BCMAS_ID", DbSqlTableView::TVIEW_BUS_CM_ADDRESS_SCHEMAS);
	s_hshTableKeyData.insert(BUS_CM_ADDRESS_SCHEMAS, TableKeyData);


	TableKeyData=DbTableKeyData("BUS_CM_ADDRESS_TYPES", "BCMAT_ID",  DbSqlTableView::TVIEW_BUS_CM_ADDRESS_TYPES);
	s_hshTableKeyData.insert(BUS_CM_ADDRESS_TYPES, TableKeyData);


	TableKeyData=DbTableKeyData("BUS_BIG_PICTURE", "BPIC_ID",  DbSqlTableView::TVIEW_BUS_BIG_PICTURE);
	s_hshTableKeyData.insert(BUS_BIG_PICTURE, TableKeyData);


	TableKeyData=DbTableKeyData("BUS_TASKS", "BTKS_ID", DbSqlTableView::TVIEW_BUS_TASKS);
	TableKeyData.m_strEntityName="Scheduled Task";
	TableKeyData.m_strEntityNamePlural="Scheduled Tasks";
	s_hshTableKeyData.insert(BUS_TASKS, TableKeyData);

	//-------------------------------------------------------------------------------------
	//								CE TYPES
	//-------------------------------------------------------------------------------------


	TableKeyData=DbTableKeyData("CE_COMM_ENTITY", "CENT_ID", DbSqlTableView::TVIEW_CE_COMM_ENTITY);
	TableKeyData.m_strEntityName="Communication Entity";
	TableKeyData.m_strEntityNamePlural="Communication Entities";
	s_hshTableKeyData.insert(CE_COMM_ENTITY, TableKeyData);

	TableKeyData=DbTableKeyData("BUS_VOICECALLS", "BVC_ID", DbSqlTableView::TVIEW_BUS_VOICECALLS, CE_COMM_ENTITY, "CENT_ID");
	s_hshTableKeyData.insert(BUS_VOICECALLS, TableKeyData);

	TableKeyData=DbTableKeyData("BUS_EMAIL", "BEM_ID", DbSqlTableView::TVIEW_BUS_EMAIL, CE_COMM_ENTITY, "CENT_ID");
	s_hshTableKeyData.insert(BUS_EMAIL, TableKeyData);

	TableKeyData=DbTableKeyData("BUS_EMAIL_ATTACHMENT", "BEA_ID", DbSqlTableView::TVIEW_BUS_EMAIL_ATTACHMENT, BUS_EMAIL, "BEA_EMAIL_ID");
	s_hshTableKeyData.insert(BUS_EMAIL_ATTACHMENT, TableKeyData);
	
	TableKeyData=DbTableKeyData("CE_EVENT", "CEV_ID", DbSqlTableView::TVIEW_CE_EVENT, CE_COMM_ENTITY, "CENT_ID");
	s_hshTableKeyData.insert(CE_EVENT, TableKeyData);

	TableKeyData=DbTableKeyData("CE_PROJECT_LINK", "CELP_ID", DbSqlTableView::TVIEW_CE_PROJECT_LINK, CE_COMM_ENTITY, "CENT_ID");
	s_hshTableKeyData.insert(CE_PROJECT_LINK, TableKeyData);

	TableKeyData=DbTableKeyData("CE_TYPE", "CET_ID", DbSqlTableView::TVIEW_CE_TYPE, -1, "", true, false);
	s_hshTableKeyData.insert(CE_TYPE, TableKeyData);

	TableKeyData=DbTableKeyData("CE_EVENT_TYPE", "CEVT_ID", DbSqlTableView::TVIEW_CE_EVENT_TYPE, CE_COMM_ENTITY, "CENT_ID");
	s_hshTableKeyData.insert(CE_EVENT_TYPE, TableKeyData);

	TableKeyData=DbTableKeyData("CE_CONTACT_LINK", "CELC_ID", DbSqlTableView::TVIEW_CE_CONTACT_LINK, CE_COMM_ENTITY, "CENT_ID");
	s_hshTableKeyData.insert(CE_CONTACT_LINK, TableKeyData);

	TableKeyData=DbTableKeyData("CE_CHARGES_LINK", "CECH_ID", DbSqlTableView::TVIEW_CE_CHARGES_LINK, CE_COMM_ENTITY, "CENT_ID");
	s_hshTableKeyData.insert(CE_CHARGES_LINK, TableKeyData);


	//-------------------------------------------------------------------------------------
	//								NMR & OPT & GROUP
	//-------------------------------------------------------------------------------------

	TableKeyData=DbTableKeyData("BUS_OPT_GRID_VIEWS", "BOGW_ID", DbSqlTableView::TVIEW_BUS_OPT_GRID_VIEWS);
	s_hshTableKeyData.insert(BUS_OPT_GRID_VIEWS, TableKeyData);

	TableKeyData=DbTableKeyData("BUS_OPT_SETTINGS", "BOUS_ID", DbSqlTableView::TVIEW_BUS_OPT_SETTINGS);
	s_hshTableKeyData.insert(BUS_OPT_SETTINGS, TableKeyData);

	TableKeyData=DbTableKeyData("BUS_NMRX_RELATION", "BNMR_ID", DbSqlTableView::TVIEW_BUS_NMRX_RELATION);
	s_hshTableKeyData.insert(BUS_NMRX_RELATION, TableKeyData);

	TableKeyData=DbTableKeyData("BUS_NMRX_ROLE", "BNRO_ID", DbSqlTableView::TVIEW_BUS_NMRX_ROLE);
	s_hshTableKeyData.insert(BUS_NMRX_ROLE, TableKeyData);

	TableKeyData=DbTableKeyData("BUS_GROUP_TREE", "BGTR_ID", DbSqlTableView::TVIEW_BUS_GROUP_TREE);
	s_hshTableKeyData.insert(BUS_GROUP_TREE, TableKeyData);

	TableKeyData=DbTableKeyData("BUS_GROUP_ITEMS", "BGIT_ID", DbSqlTableView::TVIEW_BUS_GROUP_ITEMS,-1,"",false,false);
	s_hshTableKeyData.insert(BUS_GROUP_ITEMS, TableKeyData);

	TableKeyData=DbTableKeyData("BUS_CM_GROUP", "BGCN_ID", DbSqlTableView::TVIEW_BUS_CM_GROUP);
	s_hshTableKeyData.insert(BUS_CM_GROUP, TableKeyData);


	//-------------------------------------------------------------------------------------
	//								DOCUMENTS
	//-------------------------------------------------------------------------------------

	TableKeyData=DbTableKeyData("BUS_DM_USER_PATHS", "BDMU_ID", DbSqlTableView::TVIEW_BUS_DM_USER_PATHS);
	s_hshTableKeyData.insert(BUS_DM_USER_PATHS, TableKeyData);

	TableKeyData=DbTableKeyData("BUS_DM_APPLICATIONS", "BDMA_ID", DbSqlTableView::TVIEW_BUS_DM_APPLICATIONS);
	TableKeyData.m_strEntityName="Application";
	TableKeyData.m_strEntityNamePlural="Applications";
	s_hshTableKeyData.insert(BUS_DM_APPLICATIONS, TableKeyData);


	TableKeyData=DbTableKeyData("BUS_DM_DOCUMENTS", "BDMD_ID", DbSqlTableView::TVIEW_BUS_DM_DOCUMENTS);
	TableKeyData.m_strEntityName="Document";
	TableKeyData.m_strEntityNamePlural="Documents";
	s_hshTableKeyData.insert(BUS_DM_DOCUMENTS, TableKeyData);


	TableKeyData=DbTableKeyData("BUS_DM_REVISIONS", "BDMR_ID", DbSqlTableView::TVIEW_BUS_DM_REVISIONS);
	TableKeyData.m_strEntityName="Document Revision";
	TableKeyData.m_strEntityNamePlural="Document Revisions";
	s_hshTableKeyData.insert(BUS_DM_REVISIONS, TableKeyData);


	//------------------------ COMMUNICATION GRID VIEW ------------------------------------------
	TableKeyData=DbTableKeyData("BUS_COMM_VIEW", "BUSCV_ID", DbSqlTableView::TVIEW_BUS_COMM_VIEW, BUS_PERSON, "BUSCV_OWNER_ID");
	s_hshTableKeyData.insert(BUS_COMM_VIEW, TableKeyData);

	TableKeyData=DbTableKeyData("BUS_COMM_VIEW_SETTINGS", "BUSCS_ID", DbSqlTableView::TVIEW_BUS_COMM_VIEW_SETTINGS, BUS_COMM_VIEW, "BUSCS_VIEW_ID");
	s_hshTableKeyData.insert(BUS_COMM_VIEW_SETTINGS, TableKeyData);

	//------------------------		CALENDAR VIEWS		----------------------------------------
	TableKeyData=DbTableKeyData("BUS_CAL_VIEW", "BCALV_ID", DbSqlTableView::TVIEW_BUS_CAL_VIEW, BUS_PERSON, "BCALV_OWNER_ID");
	s_hshTableKeyData.insert(BUS_CAL_VIEW, TableKeyData);

	//------------------------ FTP ------------------------------------------
	TableKeyData=DbTableKeyData("CORE_FTP_SERVERS", "CFTP_ID", DbSqlTableView::TVIEW_CORE_FTP_SERVERS);
	TableKeyData.m_strEntityName="FTP Server";
	TableKeyData.m_strEntityNamePlural="FTP Servers";
	s_hshTableKeyData.insert(CORE_FTP_SERVERS, TableKeyData);


	TableKeyData=DbTableKeyData("CORE_ACC_USER_REC", "CUAR_ID", DbSqlTableView::TVIEW_CORE_ACC_USER_REC);
	s_hshTableKeyData.insert(CORE_ACC_USER_REC, TableKeyData);

	TableKeyData=DbTableKeyData("CORE_ACC_GROUP_REC", "CGAR_ID", DbSqlTableView::TVIEW_CORE_ACC_GROUP_REC);
	s_hshTableKeyData.insert(CORE_ACC_GROUP_REC, TableKeyData);

	TableKeyData=DbTableKeyData("BUS_STORED_PROJECT_LIST", "BSPL_ID", DbSqlTableView::TVIEW_BUS_STORED_PROJECT_LIST);
	s_hshTableKeyData.insert(BUS_STORED_PROJECT_LIST, TableKeyData);

	TableKeyData=DbTableKeyData("BUS_STORED_PROJECT_LIST_DATA", "BSPLD_ID", DbSqlTableView::TVIEW_BUS_STORED_PROJECT_LIST_DATA);
	s_hshTableKeyData.insert(BUS_STORED_PROJECT_LIST_DATA, TableKeyData);

	TableKeyData=DbTableKeyData("BUS_SMS", "BSMS_ID", DbSqlTableView::TVIEW_BUS_SMS);
	s_hshTableKeyData.insert(BUS_SMS, TableKeyData);

	TableKeyData=DbTableKeyData("BUS_CAL_RESERVATION", "BCRS_ID", DbSqlTableView::TVIEW_BUS_CAL_RESERVATION);
	s_hshTableKeyData.insert(BUS_CAL_RESERVATION, TableKeyData);
	
	TableKeyData=DbTableKeyData("BUS_CAL_RESOURCE", "BRES_ID", DbSqlTableView::TVIEW_BUS_CAL_RESOURCE);
	s_hshTableKeyData.insert(BUS_CAL_RESOURCE, TableKeyData);

	TableKeyData=DbTableKeyData("BUS_CAL_INVITE", "BCIV_ID", DbSqlTableView::TVIEW_BUS_CAL_INVITE);
	s_hshTableKeyData.insert(BUS_CAL_INVITE, TableKeyData);

	TableKeyData=DbTableKeyData("BUS_CAL_EVENT", "BCEV_ID", DbSqlTableView::TVIEW_BUS_CAL_EVENT);
	s_hshTableKeyData.insert(BUS_CAL_EVENT, TableKeyData);

	TableKeyData=DbTableKeyData("BUS_CAL_EVENT_PART", "BCEP_ID", DbSqlTableView::TVIEW_BUS_CAL_EVENT_PART);
	s_hshTableKeyData.insert(BUS_CAL_EVENT_PART, TableKeyData);

	TableKeyData=DbTableKeyData("BUS_CAL_OPTIONS", "BCOL_ID", DbSqlTableView::TVIEW_BUS_CAL_OPTIONS);
	s_hshTableKeyData.insert(BUS_CAL_OPTIONS, TableKeyData);

	TableKeyData=DbTableKeyData("BUS_CAL_BREAKS", "BCBL_ID", DbSqlTableView::TVIEW_BUS_CAL_BREAKS);
	s_hshTableKeyData.insert(BUS_CAL_BREAKS, TableKeyData);

	TableKeyData=DbTableKeyData("BUS_CAL_EV_PART_OUID", "BCPOD_ID", DbSqlTableView::TVIEW_BUS_CAL_EV_PART_OUID);
	s_hshTableKeyData.insert(BUS_CAL_EV_PART_OUID, TableKeyData);

	TableKeyData=DbTableKeyData("BUS_REMINDERS", "BRE_ID", DbSqlTableView::TVIEW_BUS_REMINDERS);
	s_hshTableKeyData.insert(BUS_REMINDERS, TableKeyData);

	TableKeyData=DbTableKeyData("BUS_DM_SERVER_FILE", "BDSF_ID", DbSqlTableView::TVIEW_BUS_DM_SERVER_FILE);
	s_hshTableKeyData.insert(BUS_DM_SERVER_FILE, TableKeyData);

	TableKeyData=DbTableKeyData("BUS_DM_META_DATA", "BDME_ID", DbSqlTableView::TVIEW_BUS_DM_META_DATA);
	s_hshTableKeyData.insert(BUS_DM_META_DATA, TableKeyData);

	//------------------------		CALENDAR VIEWS	ENTITIES AND TYPES	----------------------------------------
	TableKeyData=DbTableKeyData("BUS_CAL_VIEW_ENTITIES", "BCVE_ID", DbSqlTableView::TVIEW_BUS_CAL_VIEW_ENTITIES, BUS_CAL_VIEW, "BCVE_CAL_VIEW_ID");
	s_hshTableKeyData.insert(BUS_CAL_VIEW_ENTITIES, TableKeyData);

	TableKeyData=DbTableKeyData("BUS_CAL_VIEW_CE_TYPES", "BCCT_ID", DbSqlTableView::TVIEW_BUS_CAL_VIEW_CE_TYPES, BUS_CAL_VIEW, "BCCT_CAL_VIEW_ID");
	s_hshTableKeyData.insert(BUS_CAL_VIEW_CE_TYPES, TableKeyData);


	//------------------------ SPC CLONES ----------------------------------------

	TableKeyData=DbTableKeyData("SPC_ACTIVITY", "SET_ID", DbSqlTableView::TVIEW_SPC_ACTIVITY);
	TableKeyData.m_bHasActiveFlag=false;
	s_hshTableKeyData.insert(SPC_ACTIVITY, TableKeyData);

	TableKeyData=DbTableKeyData("SPC_INDUSTRY_GROUP", "SEWG_ID", DbSqlTableView::TVIEW_SPC_INDUSTRY_GROUP);
	TableKeyData.m_bHasActiveFlag=false;
	s_hshTableKeyData.insert(SPC_INDUSTRY_GROUP, TableKeyData);

	TableKeyData=DbTableKeyData("SPC_PR_AREA", "SEPB_ID", DbSqlTableView::TVIEW_SPC_PR_AREA);
	TableKeyData.m_bHasActiveFlag=false;
	s_hshTableKeyData.insert(SPC_PR_AREA, TableKeyData);

	TableKeyData=DbTableKeyData("SPC_PR_STATUS", "SFPS_ID", DbSqlTableView::TVIEW_SPC_PR_STATUS);
	TableKeyData.m_bHasActiveFlag=false;
	s_hshTableKeyData.insert(SPC_PR_STATUS, TableKeyData);

	TableKeyData=DbTableKeyData("SPC_PR_TYPE", "SEAP_ID", DbSqlTableView::TVIEW_SPC_PR_TYPE);
	TableKeyData.m_bHasActiveFlag=false;
	s_hshTableKeyData.insert(SPC_PR_TYPE, TableKeyData);

	TableKeyData=DbTableKeyData("SPC_PROCESS", "SEPZ_ID", DbSqlTableView::TVIEW_SPC_PROCESS);
	TableKeyData.m_bHasActiveFlag=false;
	s_hshTableKeyData.insert(SPC_PROCESS, TableKeyData);

	TableKeyData=DbTableKeyData("SPC_RATE_CAT", "SETK_ID", DbSqlTableView::TVIEW_SPC_RATE_CAT);
	TableKeyData.m_bHasActiveFlag=false;
	s_hshTableKeyData.insert(SPC_RATE_CAT, TableKeyData);

	TableKeyData=DbTableKeyData("SPC_SECTOR", "SEFG_ID", DbSqlTableView::TVIEW_SPC_SECTOR);
	TableKeyData.m_bHasActiveFlag=false;
	s_hshTableKeyData.insert(SPC_SECTOR, TableKeyData);

	TableKeyData=DbTableKeyData("SPC_SERVICE_TYPE", "SELA_ID", DbSqlTableView::TVIEW_SPC_SERVICE_TYPE);
	TableKeyData.m_bHasActiveFlag=false;
	s_hshTableKeyData.insert(SPC_SERVICE_TYPE, TableKeyData);

	TableKeyData=DbTableKeyData("BUS_CUSTOM_BOOL", "BCB_ID", DbSqlTableView::TVIEW_BUS_CUSTOM_BOOL);
	TableKeyData.m_bHasActiveFlag=false;
	s_hshTableKeyData.insert(BUS_CUSTOM_BOOL, TableKeyData);

	TableKeyData=DbTableKeyData("BUS_CUSTOM_DATE", "BCD_ID", DbSqlTableView::TVIEW_BUS_CUSTOM_DATE);
	TableKeyData.m_bHasActiveFlag=false;
	s_hshTableKeyData.insert(BUS_CUSTOM_DATE, TableKeyData);

	TableKeyData=DbTableKeyData("BUS_CUSTOM_DATETIME", "BCDT_ID", DbSqlTableView::TVIEW_BUS_CUSTOM_DATETIME);
	TableKeyData.m_bHasActiveFlag=false;
	s_hshTableKeyData.insert(BUS_CUSTOM_DATETIME, TableKeyData);

	TableKeyData=DbTableKeyData("BUS_CUSTOM_FIELDS", "BCF_ID", DbSqlTableView::TVIEW_BUS_CUSTOM_FIELDS);
	TableKeyData.m_bHasActiveFlag=false;
	s_hshTableKeyData.insert(BUS_CUSTOM_FIELDS, TableKeyData);

	TableKeyData=DbTableKeyData("BUS_CUSTOM_FLOAT", "BCFL_ID", DbSqlTableView::TVIEW_BUS_CUSTOM_FLOAT);
	TableKeyData.m_bHasActiveFlag=false;
	s_hshTableKeyData.insert(BUS_CUSTOM_FLOAT, TableKeyData);

	TableKeyData=DbTableKeyData("BUS_CUSTOM_INT", "BCI_ID", DbSqlTableView::TVIEW_BUS_CUSTOM_INT);
	TableKeyData.m_bHasActiveFlag=false;
	s_hshTableKeyData.insert(BUS_CUSTOM_INT, TableKeyData);

	TableKeyData=DbTableKeyData("BUS_CUSTOM_LONGTEXT", "BCLT_ID", DbSqlTableView::TVIEW_BUS_CUSTOM_LONGTEXT);
	TableKeyData.m_bHasActiveFlag=false;
	s_hshTableKeyData.insert(BUS_CUSTOM_LONGTEXT, TableKeyData);

	TableKeyData=DbTableKeyData("BUS_CUSTOM_TEXT", "BCT_ID", DbSqlTableView::TVIEW_BUS_CUSTOM_TEXT);
	TableKeyData.m_bHasActiveFlag=false;
	s_hshTableKeyData.insert(BUS_CUSTOM_TEXT, TableKeyData);

	TableKeyData=DbTableKeyData("BUS_CUSTOM_SELECTIONS", "BCS_ID", DbSqlTableView::TVIEW_BUS_CUSTOM_SELECTIONS);
	TableKeyData.m_bHasActiveFlag=false;
	s_hshTableKeyData.insert(BUS_CUSTOM_SELECTIONS, TableKeyData);

	TableKeyData=DbTableKeyData("BUS_PUSH_MESSAGE", "BPM_ID", DbSqlTableView::TVIEW_BUS_PUSH_MESSAGE);
	TableKeyData.m_bHasActiveFlag=false;
	s_hshTableKeyData.insert(BUS_PUSH_MESSAGE, TableKeyData);

	TableKeyData=DbTableKeyData("BUS_PUSH_TOKEN_PERSON", "BPT_ID", DbSqlTableView::TVIEW_BUS_PUSH_TOKEN_PERSON);
	TableKeyData.m_bHasActiveFlag=false;
	s_hshTableKeyData.insert(BUS_PUSH_TOKEN_PERSON, TableKeyData);

	TableKeyData=DbTableKeyData("BUS_EMAIL_BINARY", "BEB_ID", DbSqlTableView::TVIEW_BUS_EMAIL_BINARY);
	TableKeyData.m_bHasActiveFlag=false;
	s_hshTableKeyData.insert(BUS_EMAIL_BINARY, TableKeyData);

	
	//------------------------ SPC ----------------------------------------

	TableKeyData=DbTableKeyData("F_ABT_MITARB", "EAM_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_ABT_MITARB, TableKeyData);

	TableKeyData=DbTableKeyData("F_ABT_RECHTE", "EAR_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_ABT_RECHTE, TableKeyData);

	TableKeyData=DbTableKeyData("F_ABTEILUNG", "FAB_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_ABTEILUNG, TableKeyData);

	TableKeyData=DbTableKeyData("F_AGENTS", "EAGN_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_AGENTS, TableKeyData);

	TableKeyData=DbTableKeyData("F_BANKVERBINDUNGEN", "EBV_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_BANKVERBINDUNGEN, TableKeyData);

	TableKeyData=DbTableKeyData("F_BATCH_JOBS", "BJ_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_BATCH_JOBS, TableKeyData);

	TableKeyData=DbTableKeyData("F_BEL_PROJ_LISTEN", "EBP_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_BEL_PROJ_LISTEN, TableKeyData);

	TableKeyData=DbTableKeyData("F_COUNTERS", "ECN_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_COUNTERS, TableKeyData);

	TableKeyData=DbTableKeyData("F_COUNTRY", "CTRY_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_COUNTRY, TableKeyData);

	TableKeyData=DbTableKeyData("F_DATENAKTUALISIERUNG", "DBS_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_DATENAKTUALISIERUNG, TableKeyData);

	TableKeyData=DbTableKeyData("F_DEBI_BUCH_RECH", "EDE_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_DEBI_BUCH_RECH, TableKeyData);

	TableKeyData=DbTableKeyData("F_DEBI_BUCH_ZAHL", "EDZ_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_DEBI_BUCH_ZAHL, TableKeyData);

	TableKeyData=DbTableKeyData("F_DEFAULTS", "DFF_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_DEFAULTS, TableKeyData);

	TableKeyData=DbTableKeyData("F_DELETE", "DEL_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_DELETE, TableKeyData);

	TableKeyData=DbTableKeyData("F_EXCHANGEFORMATS", "EX_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_EXCHANGEFORMATS, TableKeyData);

	TableKeyData=DbTableKeyData("F_EXT_APPLICATIONS", "EAPP_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_EXT_APPLICATIONS, TableKeyData);

	TableKeyData=DbTableKeyData("F_FAKT_PERS_LEIST", "EFPL_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_FAKT_PERS_LEIST, TableKeyData);

	TableKeyData=DbTableKeyData("F_FAKT_PLAN", "EFP_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_FAKT_PLAN, TableKeyData);

	TableKeyData=DbTableKeyData("F_FAKT_PLAN_LOG", "EFPLG_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_FAKT_PLAN_LOG, TableKeyData);

	TableKeyData=DbTableKeyData("F_FAKT_UMLAGEN", "EFU_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_FAKT_UMLAGEN, TableKeyData);

	TableKeyData=DbTableKeyData("F_FAKTURA", "EF_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_FAKTURA, TableKeyData);

	TableKeyData=DbTableKeyData("F_FAKTURA_EIN", "EKR_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_FAKTURA_EIN, TableKeyData);

	TableKeyData=DbTableKeyData("F_FERIEN", "FFK_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_FERIEN, TableKeyData);

	TableKeyData=DbTableKeyData("F_FUNKTIONEN", "EFK_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_FUNKTIONEN, TableKeyData);

	TableKeyData=DbTableKeyData("F_GEBIETE", "EFG_SEQUENCE", -1);
	TableKeyData.m_bHasActiveFlag=false;
	s_hshTableKeyData.insert(F_GEBIETE, TableKeyData);

	TableKeyData=DbTableKeyData("F_HONORARKLASSE", "EH_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_HONORARKLASSE, TableKeyData);

	TableKeyData=DbTableKeyData("F_IND_PROJNAME", "EPNI_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_IND_PROJNAME, TableKeyData);

	TableKeyData=DbTableKeyData("F_INOUTTIMES", "STU_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_INOUTTIMES, TableKeyData);

	TableKeyData=DbTableKeyData("F_KA_KLASSEN", "EKK_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_KA_KLASSEN, TableKeyData);

	TableKeyData=DbTableKeyData("F_KNTRL_PERIODE", "FKP_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_KNTRL_PERIODE, TableKeyData);

	TableKeyData=DbTableKeyData("F_KOMPENSATION", "EK_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_KOMPENSATION, TableKeyData);

	TableKeyData=DbTableKeyData("F_KONTEN", "EKT_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_KONTEN, TableKeyData);

	TableKeyData=DbTableKeyData("F_KONTENPLAN", "EKP_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_KONTENPLAN, TableKeyData);

	TableKeyData=DbTableKeyData("F_KONTO_NAMEN", "KN_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_KONTO_NAMEN, TableKeyData);

	TableKeyData=DbTableKeyData("F_KOSTENARTEN", "FEK_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_KOSTENARTEN, TableKeyData);

	TableKeyData=DbTableKeyData("F_KS_BUDGET", "EKB_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_KS_BUDGET, TableKeyData);

	TableKeyData=DbTableKeyData("F_LEISTUNGSARTEN", "ELA_SEQUENCE", -1);
	TableKeyData.m_bHasActiveFlag=false;
	s_hshTableKeyData.insert(F_LEISTUNGSARTEN, TableKeyData);

	TableKeyData=DbTableKeyData("F_LETZTER_EXP", "ELE_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_LETZTER_EXP, TableKeyData);

	TableKeyData=DbTableKeyData("F_LICENCE_RESERVATION", "LRV_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_LICENCE_RESERVATION, TableKeyData);

	TableKeyData=DbTableKeyData("F_LOCKING", "ELK_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_LOCKING, TableKeyData);

	TableKeyData=DbTableKeyData("F_MITARB_BUDGET", "EBM_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_MITARB_BUDGET, TableKeyData);

	TableKeyData=DbTableKeyData("F_MODULE_LICENCES", "MLC_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_MODULE_LICENCES, TableKeyData);

	TableKeyData=DbTableKeyData("F_NK_BUDGET", "ENKB_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_NK_BUDGET, TableKeyData);

	TableKeyData=DbTableKeyData("F_OBJEKTNUMMERN", "EON_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_OBJEKTNUMMERN, TableKeyData);

	TableKeyData=DbTableKeyData("F_ORGANISATION", "FJO_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_ORGANISATION, TableKeyData);

	TableKeyData=DbTableKeyData("F_PERIODENART", "EPA_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_PERIODENART, TableKeyData);

	TableKeyData=DbTableKeyData("F_PERS_FUNKTION", "FPF_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_PERS_FUNKTION, TableKeyData);

	TableKeyData=DbTableKeyData("F_PERSPLLINES", "EPLL_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_PERSPLLINES, TableKeyData);

	TableKeyData=DbTableKeyData("F_PERSPROJLISTS", "EPPL_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_PERSPROJLISTS, TableKeyData);

	TableKeyData=DbTableKeyData("F_PFLICHTFELDER", "PF_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_PFLICHTFELDER, TableKeyData);

	TableKeyData=DbTableKeyData("F_PR_CACHE", "FPC_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_PR_CACHE, TableKeyData);

	TableKeyData=DbTableKeyData("F_PR_STATUS", "FPS_SEQUENCE", -1);
	TableKeyData.m_bHasActiveFlag=false;
	s_hshTableKeyData.insert(F_PR_STATUS, TableKeyData);

	TableKeyData=DbTableKeyData("F_PREASSIGN", "EVE_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_PREASSIGN, TableKeyData);

	TableKeyData=DbTableKeyData("F_PRINT_LABELS", "PLAB_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_PRINT_LABELS, TableKeyData);

	TableKeyData=DbTableKeyData("F_PROJ_BAUHERR", "FPB_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_PROJ_BAUHERR, TableKeyData);

	TableKeyData=DbTableKeyData("F_PROJ_BEREICHE", "EPB_SEQUENCE", -1);
	TableKeyData.m_bHasActiveFlag=false;
	s_hshTableKeyData.insert(F_PROJ_BEREICHE, TableKeyData);

	TableKeyData=DbTableKeyData("F_PROJ_BUDGETKORREKTUR", "EBK_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_PROJ_BUDGETKORREKTUR, TableKeyData);

	TableKeyData=DbTableKeyData("F_PROJ_MITARB", "EPM_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_PROJ_MITARB, TableKeyData);

	TableKeyData=DbTableKeyData("F_PROJ_PHASEN", "EPP_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_PROJ_PHASEN, TableKeyData);

	TableKeyData=DbTableKeyData("F_PROJ_RECHTE", "EPR_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_PROJ_RECHTE, TableKeyData);

	TableKeyData=DbTableKeyData("F_PROJ_RESTAUFWAND", "ERA_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_PROJ_RESTAUFWAND, TableKeyData);

	TableKeyData=DbTableKeyData("F_PROJ_STRUKT", "EPS_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_PROJ_STRUKT, TableKeyData);

	TableKeyData=DbTableKeyData("F_PROJECTUSERDEFINED", "EPU_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_PROJECTUSERDEFINED, TableKeyData);

	TableKeyData=DbTableKeyData("F_PROJECTUSERDEFINED_CHAR", "EPUC_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_PROJECTUSERDEFINED_CHAR, TableKeyData);

	TableKeyData=DbTableKeyData("F_PROJECTUSERDEFINED_DATE", "EPUD_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_PROJECTUSERDEFINED_DATE, TableKeyData);

	TableKeyData=DbTableKeyData("F_PROJECTUSERDEFINED_FLOAT", "EPUF_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_PROJECTUSERDEFINED_FLOAT, TableKeyData);

	TableKeyData=DbTableKeyData("F_PROJECTUSERDEFINEDFIELDS", "EPUL_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_PROJECTUSERDEFINEDFIELDS, TableKeyData);

	TableKeyData=DbTableKeyData("F_PROJECTUSERDEFINEDGUIMAP", "EPUG_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_PROJECTUSERDEFINEDGUIMAP, TableKeyData);

	TableKeyData=DbTableKeyData("F_PROJEKTART", "EAP_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_PROJEKTART, TableKeyData);

	TableKeyData=DbTableKeyData("F_PROJEKTLISTEN", "PL_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_PROJEKTLISTEN, TableKeyData);

	TableKeyData=DbTableKeyData("F_PROZESSE", "EPZ_SEQUENCE", -1);
	TableKeyData.m_bHasActiveFlag=false;
	s_hshTableKeyData.insert(F_PROZESSE, TableKeyData);

	TableKeyData=DbTableKeyData("F_RECHTE", "ZR_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_RECHTE, TableKeyData);

	TableKeyData=DbTableKeyData("F_RECORD_ID", "RID_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_RECORD_ID, TableKeyData);

	TableKeyData=DbTableKeyData("F_REPORTLISTEN", "ERL_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_REPORTLISTEN, TableKeyData);

	TableKeyData=DbTableKeyData("F_REPORTPROFIL", "ERP_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_REPORTPROFIL, TableKeyData);

	TableKeyData=DbTableKeyData("F_REPORTSETS", "ERS_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_REPORTSETS, TableKeyData);

	TableKeyData=DbTableKeyData("F_SOLLZEIT_KLASSEN", "ESK_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_SOLLZEIT_KLASSEN, TableKeyData);

	TableKeyData=DbTableKeyData("F_SOLLZEITEN", "ESZ_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_SOLLZEITEN, TableKeyData);

	TableKeyData=DbTableKeyData("F_STD_KONTROLLE", "FSK_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_STD_KONTROLLE, TableKeyData);

	TableKeyData=DbTableKeyData("F_STEMPELUHR", "ESU_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_STEMPELUHR, TableKeyData);

	TableKeyData=DbTableKeyData("F_SYNCHRO", "ESY_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_SYNCHRO, TableKeyData);

	TableKeyData=DbTableKeyData("F_SZ_EXCEPTIONS", "ESZE_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_SZ_EXCEPTIONS, TableKeyData);

	TableKeyData=DbTableKeyData("F_TAET_LIST_ENTRY", "TE_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_TAET_LIST_ENTRY, TableKeyData);

	TableKeyData=DbTableKeyData("F_TAET_LIST_NAMES", "TN_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_TAET_LIST_NAMES, TableKeyData);

	TableKeyData=DbTableKeyData("F_TAETIGKEITEN", "ET_SEQUENCE", -1);
	TableKeyData.m_bHasActiveFlag=false;
	s_hshTableKeyData.insert(F_TAETIGKEITEN, TableKeyData);

	TableKeyData=DbTableKeyData("F_TARIFE", "ETR_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_TARIFE, TableKeyData);

	TableKeyData=DbTableKeyData("F_TARIFKATEG", "ETK_SEQUENCE", -1);
	TableKeyData.m_bHasActiveFlag=false;
	s_hshTableKeyData.insert(F_TARIFKATEG, TableKeyData);

	TableKeyData=DbTableKeyData("F_TIMELINE_ASSOC", "ETLA_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_TIMELINE_ASSOC, TableKeyData);

	TableKeyData=DbTableKeyData("F_TIMELINE_CHAR200", "ETLC2_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_TIMELINE_CHAR200, TableKeyData);

	TableKeyData=DbTableKeyData("F_TIMELINE_CHAR50", "ETLC5_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_TIMELINE_CHAR50, TableKeyData);

	TableKeyData=DbTableKeyData("F_TIMELINE_DATE", "ETLD_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_TIMELINE_DATE, TableKeyData);

	TableKeyData=DbTableKeyData("F_TIMELINE_FLOAT", "ETLF_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_TIMELINE_FLOAT, TableKeyData);

	TableKeyData=DbTableKeyData("F_TIMELINE_LONG", "ETLL_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_TIMELINE_LONG, TableKeyData);

	TableKeyData=DbTableKeyData("F_TIMELINE_SHORT", "ETLS_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_TIMELINE_SHORT, TableKeyData);

	TableKeyData=DbTableKeyData("F_TIMESERVER", "TSR_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_TIMESERVER, TableKeyData);

	TableKeyData=DbTableKeyData("F_TK_KA_KLASSEN", "ETA_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_TK_KA_KLASSEN, TableKeyData);

	TableKeyData=DbTableKeyData("F_TK_REFERENCES", "ETKR_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_TK_REFERENCES, TableKeyData);

	TableKeyData=DbTableKeyData("F_UDF_BOOLEAN", "UDFB_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_UDF_BOOLEAN, TableKeyData);

	TableKeyData=DbTableKeyData("F_UDF_CHAR", "UDFC_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_UDF_CHAR, TableKeyData);

	TableKeyData=DbTableKeyData("F_UDF_DATE", "UDFD_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_UDF_DATE, TableKeyData);

	TableKeyData=DbTableKeyData("F_UDF_FIELD", "UDF_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_UDF_FIELD, TableKeyData);

	TableKeyData=DbTableKeyData("F_UDF_FLOAT", "UDFF_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_UDF_FLOAT, TableKeyData);

	TableKeyData=DbTableKeyData("F_UDF_INTEGER", "UDFI_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_UDF_INTEGER, TableKeyData);

	TableKeyData=DbTableKeyData("F_UDF_POSITION_GRID", "UDFG_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_UDF_POSITION_GRID, TableKeyData);

	TableKeyData=DbTableKeyData("F_UDF_POSITION_REP", "UDFR_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_UDF_POSITION_REP, TableKeyData);

	TableKeyData=DbTableKeyData("F_UDF_POSITION_WIN", "UDFP_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_UDF_POSITION_WIN, TableKeyData);

	TableKeyData=DbTableKeyData("F_UDF_PREDEF_FILL", "UDFCF_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_UDF_PREDEF_FILL, TableKeyData);

	TableKeyData=DbTableKeyData("F_UDF_TEXT", "UDFT_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_UDF_TEXT, TableKeyData);

	TableKeyData=DbTableKeyData("F_UDF_VIEW", "UDFV_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_UDF_VIEW, TableKeyData);

	TableKeyData=DbTableKeyData("F_USER_APPLICATIONS", "UA_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_USER_APPLICATIONS, TableKeyData);

	TableKeyData=DbTableKeyData("F_USER_SESSIONS", "USR_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_USER_SESSIONS, TableKeyData);

	TableKeyData=DbTableKeyData("F_USER_SETTINGS", "EUS_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_USER_SETTINGS, TableKeyData);

	TableKeyData=DbTableKeyData("F_VERSICHERUNGSKATEGORIEN", "EVK_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_VERSICHERUNGSKATEGORIEN, TableKeyData);

	TableKeyData=DbTableKeyData("F_WAEHRUNG", "FW_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_WAEHRUNG, TableKeyData);

	TableKeyData=DbTableKeyData("F_WIRTSCH_GRUPPE", "EWG_SEQUENCE", -1);
	TableKeyData.m_bHasActiveFlag=false;
	s_hshTableKeyData.insert(F_WIRTSCH_GRUPPE, TableKeyData);

	TableKeyData=DbTableKeyData("F_WT_CP_SCHEMA", "EWS_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_WT_CP_SCHEMA, TableKeyData);

	TableKeyData=DbTableKeyData("F_WT_DAY_SCHEMA", "EDW_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_WT_DAY_SCHEMA, TableKeyData);

	TableKeyData=DbTableKeyData("F_ZAHL_EINGANG", "FZ_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_ZAHL_EINGANG, TableKeyData);

	TableKeyData=DbTableKeyData("F_ZAHLUNGSARTEN", "EZA_SEQUENCE", -1);
	s_hshTableKeyData.insert(F_ZAHLUNGSARTEN, TableKeyData);

	TableKeyData=DbTableKeyData("FASSIGN", "FB_SEQUENCE", -1);
	s_hshTableKeyData.insert(FASSIGN, TableKeyData);

	TableKeyData=DbTableKeyData("FD_DOK_VORLAGEN", "EDV_SEQUENCE", -1);
	s_hshTableKeyData.insert(FD_DOK_VORLAGEN, TableKeyData);

	TableKeyData=DbTableKeyData("FD_DOKUMENTE", "EDC_SEQUENCE", -1);
	s_hshTableKeyData.insert(FD_DOKUMENTE, TableKeyData);

	TableKeyData=DbTableKeyData("FD_DOKUMENTENARTEN", "EDA_SEQUENCE", -1);
	s_hshTableKeyData.insert(FD_DOKUMENTENARTEN, TableKeyData);

	TableKeyData=DbTableKeyData("FF_BAUSTEINE", "FBS_SEQUENCE", -1);
	s_hshTableKeyData.insert(FF_BAUSTEINE, TableKeyData);

	TableKeyData=DbTableKeyData("FF_DEF_FAKT", "DF_SEQUENCE", -1);
	s_hshTableKeyData.insert(FF_DEF_FAKT, TableKeyData);

	TableKeyData=DbTableKeyData("FF_DEF_FAKT_POS", "DFB_SEQUENCE", -1);
	s_hshTableKeyData.insert(FF_DEF_FAKT_POS, TableKeyData);

	TableKeyData=DbTableKeyData("FF_RECHNUNG_POS", "FRP_SEQUENCE", -1);
	s_hshTableKeyData.insert(FF_RECHNUNG_POS, TableKeyData);

	TableKeyData=DbTableKeyData("FL_DEFAULT", "DEF_SEQUENCE", -1);
	s_hshTableKeyData.insert(FL_DEFAULT, TableKeyData);

	TableKeyData=DbTableKeyData("FL_DEFAULT2", "DEF_2_SEQUENCE", -1);
	s_hshTableKeyData.insert(FL_DEFAULT2, TableKeyData);

	TableKeyData=DbTableKeyData("FPERSON", "FP_SEQUENCE", -1);
	s_hshTableKeyData.insert(FPERSON, TableKeyData);

	TableKeyData=DbTableKeyData("FPROJCT", "PR_SEQUENCE", -1);
	s_hshTableKeyData.insert(FPROJCT, TableKeyData);

	TableKeyData=DbTableKeyData("FSA_ADRESSEN", "EAL_SEQUENCE", -1);
	s_hshTableKeyData.insert(FSA_ADRESSEN, TableKeyData);

	TableKeyData=DbTableKeyData("ZS_TEXT_FIELDS", "RID_TXT", -1);
	s_hshTableKeyData.insert(ZS_TEXT_FIELDS, TableKeyData);

	TableKeyData=DbTableKeyData("ZS_BINARY_FIELDS", "RID_BIN", -1);
	s_hshTableKeyData.insert(ZS_BINARY_FIELDS, TableKeyData);

	//m_bInitialised = true;
	return;
}

/*!
	Get table key data.
	\param TableID	- table id defined in the dbtableiddefinition.h
	\param KeyData	- returned table data
*/
void DbSqlTableDefinition::GetKeyData(int TableID, DbTableKeyData &KeyData)
{
	Initialize();
	Q_ASSERT(s_hshTableKeyData.contains(TableID));
	KeyData = s_hshTableKeyData.value(TableID);

}

/*!
	Get table full view ID.
	\param	TableID - table id defined in the dbtableiddefinition.h
	\return			- full view ID
*/
int DbSqlTableDefinition::GetFullViewID(int TableID)
{
	Initialize();
	Q_ASSERT(s_hshTableKeyData.contains(TableID));
	return s_hshTableKeyData.value(TableID).m_nViewID;

}

/*!
	Get table prefix:
	\param	TableID - table id defined in the dbtableiddefinition.h
	\return			- prefix
*/
QString DbSqlTableDefinition::GetTablePrefix(int TableID)
{
	Initialize();
	Q_ASSERT(s_hshTableKeyData.contains(TableID));
	return s_hshTableKeyData.value(TableID).m_strFieldPrefix;
}

int	DbSqlTableDefinition::GetTableID(QString strTableName)
{
	Initialize();

	strTableName=strTableName.toLower();
	QHashIterator<int, DbTableKeyData> i(s_hshTableKeyData);
	while (i.hasNext()) 
	{
		i.next();
		if (i.value().m_strTableName.toLower()==strTableName)
		{
			return	 i.key();
		}
	}

	return -1;
}

/*!
	Get table name from table id
*/
QString DbSqlTableDefinition::GetTableName(int TableID)
{
	Initialize();
	Q_ASSERT(s_hshTableKeyData.contains(TableID));
	return s_hshTableKeyData.value(TableID).m_strTableName;
}


/*!
	Get table name from table id
*/
QString DbSqlTableDefinition::GetTablePrimaryKey(int TableID)
{
	Initialize();
	Q_ASSERT(s_hshTableKeyData.contains(TableID));
	return s_hshTableKeyData.value(TableID).m_strPrimaryKey;
}







/*!
	Copies PK key values from QList<int> to recordset defintion full of given table
	\param TableID	- table id
	\param lstPKs	- source list of PK
	\param lstFull	- returned filled list in full view format with filled PK column
*/
void DbSqlTableDefinition::CopyFromList2RecordSet(int TableID, const QList<int> &lstPKs,DbRecordSet& lstFull)
{

	//prepare:
	lstFull.defineFromView(DbSqlTableView::getView(GetFullViewID(TableID)));
	DbTableKeyData tabledata;
	GetKeyData(TableID,tabledata);
	int nPKCol=lstFull.getColumnIdx(tabledata.m_strPrimaryKey);

	//copy from QList to lstTemp:
	for(int i=0;i<lstPKs.size();++i)
	{
		lstFull.addRow();
		lstFull.setData(i,nPKCol,lstPKs.at(i));
	}
}


/*!
	Copies PK key values from recordset to QList<int> 
	\param TableID	- table id
	\param lstPKs	- source list of PK
	\param lstFull	- returned filled list in full view format with filled PK column
*/
void DbSqlTableDefinition::CopyFromRecordSet2List(int TableID, QList<int> &lstPKs,const DbRecordSet& lstFull)
{
	//define temp list:
	lstPKs.clear();
	DbTableKeyData tabledata;
	GetKeyData(TableID,tabledata);
	int nPKCol=lstFull.getColumnIdx(tabledata.m_strPrimaryKey);

	//copy from QList to lstTemp:
	for(int i=0;i<lstFull.getRowCount();++i)
	{
		lstPKs.append(lstFull.getDataRef(i,nPKCol).toInt());
	}


}

void DbSqlTableDefinition::FillParentSqlHash(int TableID, QHash<int, int> &hshParentHash, int &nParentNumber, bool &bLastItem)
{
	//If TableID < 0 then there is no parent.
	if (TableID < 0)
		return;

	//Now go and find your parent.
	DbTableKeyData KeyData = DbTableKeyData();
	GetKeyData(TableID, KeyData);
	
	//Don't forget it (write him down).s
	hshParentHash.insert(nParentNumber, TableID);
	nParentNumber++;

	//See does parent have a parent.
	FillParentSqlHash(KeyData.m_nParentTableID, hshParentHash, nParentNumber, bLastItem);
}

QString DbSqlTableDefinition::GetParentSqlRecordLockString(int TableID)
{
	//Declare variables for parent hash.
	QHash<int, int> hshParentHash;
	int ParentNumber = 0;
	bool LastItem = false;
	
	//Declare local parameters.
	DbTableKeyData		 KeyData;
	DbTableKeyData		 ParentKeyData;
	FillParentSqlHash(TableID, hshParentHash, ParentNumber, LastItem);
	QString StrSql = QString();

	//Start to create SQL statement.
	if (!hshParentHash.isEmpty())
	{
		int hshItem = hshParentHash.count();
		
		//We are going from behind to create SQL statement.
		//If m_hshParentSqlHash has e.g. 4 members, first from behind is 3rd.
		hshItem--;
		
		//Find table id and parent key data.
		int tableID = hshParentHash.value(hshItem);
		GetKeyData(tableID, KeyData);

		//Create parents SQL - select his index.
		StrSql = "Select ";
		StrSql += QString("%1.%2").arg(KeyData.m_strTableName).arg(KeyData.m_strPrimaryKey);
		StrSql += " FROM ";
		StrSql += KeyData.m_strTableName;
		
		//Join all child's on parent.
		while (hshItem > 0)
		{
			hshItem--;
			tableID = hshParentHash.value(hshItem);
			GetKeyData(tableID, KeyData);
			
			StrSql += " INNER JOIN ";
			StrSql += KeyData.m_strTableName;
			StrSql += " ON ";
			StrSql += QString("%1.%2").arg(KeyData.m_strTableName).arg(KeyData.m_strParentForeignField);
			StrSql += " = ";

			GetKeyData(KeyData.m_nParentTableID, ParentKeyData);
			StrSql += QString("%1.%2").arg(ParentKeyData.m_strTableName).arg(ParentKeyData.m_strPrimaryKey);
		}
		
		//Where clause is on begining table id.
		GetKeyData(tableID, KeyData);
		StrSql += QString(" WHERE %1.%2 IN ").arg(KeyData.m_strTableName).arg(KeyData.m_strPrimaryKey);
	}

	return StrSql;
}

int	DbSqlTableDefinition::GetParentID(int TableID)
{
	//Set default value and initialize variables.
	int ParentTableId = -1;
	DbTableKeyData	KeyData;
	GetKeyData(TableID, KeyData);

	//If m_bCheckParent == false (table definition is initialized with no parent check)
	//then return itself as a parent (e.g. return -1).
	if (!KeyData.m_bCheckParent)
		return ParentTableId;

	//Loop till parent is >= 0.
	while (KeyData.m_nParentTableID >= 0)
	{
		ParentTableId = KeyData.m_nParentTableID;
		GetKeyData(KeyData.m_nParentTableID, KeyData);
	}

	return ParentTableId;
}




// Prepare chunked statement in format (n1,n2,n3....) where n is from pLstOfID
// NOTE: if nChunkSize == -1 then no chunk:it will to to end of list
// pLstOfID must have strColIDName column inside filled with n's
// return -1 if strSQL is empty, else it returns actually chunked rows
int DbSqlTableDefinition::GenerateChunkedWhereStatement(const DbRecordSet &pLstOfID,QString strColIDName,QString &strWhere,int nStartFrom,int nChunkSize,bool bAddQuotes)
{
	Q_ASSERT(nStartFrom>=0);

	int nSize=pLstOfID.getRowCount();
	if(nStartFrom>=nSize) return -1;		//reach end

	int nChunk;
	if(nChunkSize==-1)
		nChunk=pLstOfID.getRowCount();
	else
		nChunk=nStartFrom+nChunkSize;		//chunks of 200 max from offset

	//limit to size:
	if(nChunk>nSize)
		nChunk=nSize;


	strWhere.clear();	//clear string

	//ID must be present
	int nIDColIdx=pLstOfID.getColumnIdx(strColIDName);
	Q_ASSERT(nIDColIdx!=-1);

	int i;

	//set first (as must be at least one if we are here):
	if (!bAddQuotes)
		strWhere = " ("+pLstOfID.getDataRef(nStartFrom, nIDColIdx).toString();
	else
		strWhere = " ('"+pLstOfID.getDataRef(nStartFrom, nIDColIdx).toString()+"'";

	//start from 2nd:
	for(i=nStartFrom+1;i<nChunk;i++)
	{
		if (!bAddQuotes)
			strWhere+= ","+pLstOfID.getDataRef(i, nIDColIdx).toString();
		else
			strWhere+= ",'"+pLstOfID.getDataRef(i, nIDColIdx).toString()+"'";
	}

	strWhere+=") ";		//close bracket


	return i;			//return no rows actually added
}


//get table data by table name
void DbSqlTableDefinition::GetEntityName(QString strTableName,QString& strEntityName,QString& strEntityNamePlural)
{
	Initialize();

	QHashIterator<int, DbTableKeyData> i(s_hshTableKeyData);
	while (i.hasNext()) 
	{
		i.next();
		if (i.value().m_strTableName==strTableName)
		{
			strEntityName=i.value().m_strEntityName;
			strEntityNamePlural=i.value().m_strEntityNamePlural;
			return;
		}
	}

	strEntityName="";
	strEntityNamePlural="";
}



//insert: INSERT INTO table (ID,LAST,...) VALUES(?,CURRENT_TIMESTAMP(),...)
bool DbSqlTableDefinition::GetTableDataFromSQL(bool bInsertSQL,QString strSQLInsertStatement, QString &strTableName, QString &strPrimaryKey,QString &strLastModified, QString &strCreated)
{
	if (bInsertSQL)
	{
		//get table name:
		int nTable1=strSQLInsertStatement.indexOf("INTO");
		int nTable2=strSQLInsertStatement.indexOf("(");
		int nComa1=strSQLInsertStatement.indexOf(",");
		strTableName=strSQLInsertStatement.mid(nTable1+5,nTable2-nTable1-5).trimmed();
		QString strCol=strSQLInsertStatement.mid(nTable2+1,nComa1-nTable2-1).trimmed();
		int nPrefix=strCol.indexOf("_");
		if (nPrefix>=0)
		{
			strPrimaryKey=strCol.left(nPrefix)+"_ID";
			strLastModified=strCol.left(nPrefix)+"_DAT_LAST_MODIFIED";
			strCreated=strCol.left(nPrefix)+"_DAT_CREATED";
			return true;
		}
		if (strTableName.isEmpty() || nPrefix==-1)
			return false;
	}
	else //update
	{
		//get table name:
		strSQLInsertStatement=strSQLInsertStatement.simplified();
		//int nTable1=strSQLInsertStatement.indexOf("UPDATE");
		int nTable2=strSQLInsertStatement.indexOf(" ",9);
		int nComa1=strSQLInsertStatement.indexOf("SET ");
		int nEq=strSQLInsertStatement.indexOf("=");
		strTableName=strSQLInsertStatement.mid(7,nTable2-7).trimmed();
		QString strCol=strSQLInsertStatement.mid(nComa1+4,nEq-nComa1-4).trimmed();
		int nPrefix=strCol.indexOf("_");
		if (nPrefix>=0)
		{
			strPrimaryKey=strCol.left(nPrefix)+"_ID";
			strLastModified=strCol.left(nPrefix)+"_DAT_LAST_MODIFIED";
			strCreated=strCol.left(nPrefix)+"_DAT_CREATED";
			return true;
		}
		if (strTableName.isEmpty() || nPrefix==-1)
			return false;


	}


	return true;
}
