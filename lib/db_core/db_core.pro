TEMPLATE	= lib
CONFIG		= qt warn_on release staticlib
HEADERS		= db_core/dbcore_connectionfaker.h \
		  db_core/dbcore_include.h \
		  db_core/dbrecordset.h \
		  db_core/dbrecordsetdefintion.h \
		  db_core/dbsqltabledefinition.h \
		  db_core/dbsqltableview.h \
		  db_core/dbtableiddefinition.h
SOURCES		= db_core/dbrecordset.cpp \
		  db_core/dbrecordsetdefintion.cpp \
		  db_core/dbsqltabledefinition.cpp \
		  db_core/dbsqltableview.cpp
INTERFACES	= 
TARGET		= db_core
INCLUDEPATH	+= ../
