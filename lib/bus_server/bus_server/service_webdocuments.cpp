#include "service_webdocuments.h"
#include "bus_core/bus_core/globalconstants.h"

#include "bus_core/bus_core/usersessionmanager.h"
extern UserSessionManager	*g_UserSessionManager;
#include "bus_core/bus_core/useraccessright.h"
extern UserAccessRight *g_AccessRight;	
#include "businessserviceset.h"
#include "common/common/datahelper.h"
#include "common/common/userstoragehelper.h"
#include "trans/trans/tcphelper.h"
extern BusinessServiceSet* g_BusinessServiceSet;


//get all documents for logged user/contact
void Service_WebDocuments::ReadDocuments(Status &Ret_pStatus, DbRecordSet &Ret_Documents)
{
	int nContactID=g_UserSessionManager->GetContactID();
	int Ret_nTotalCount;
	SearchDocuments(Ret_pStatus,Ret_nTotalCount,Ret_Documents,-1,-1,-1,nContactID);
}

//get all documents for logged user/contact
void Service_WebDocuments::ReadDocumentsWithFilter(Status &Ret_pStatus, int &Ret_nTotalCount,DbRecordSet &Ret_Documents,  int nDocType, int nFromN,int nToN)
{
	int nContactID=g_UserSessionManager->GetContactID();
	SearchDocuments(Ret_pStatus,Ret_nTotalCount,Ret_Documents,nDocType,nFromN,nToN,nContactID);
}



//filter: per doc type (notes, internet file, etc..)=default=all, per contact/project + per owner+ last N option + name starts with +created from+modified from 
//nSortOrder: 0- by name (def), 1-by create date, 2- by ext, 3 by type (+by last modified if LAST N filter is activated)
//nFromN from 0 - nToN, etc..till nsize
void Service_WebDocuments::SearchDocuments(Status &Ret_pStatus, int &Ret_nTotalCount,DbRecordSet &Ret_Documents,  int nDocType, int nFromN, int nToN, int nContactID, int nProjectID, int nOwnerUserID, QString strSearchByName, QDateTime datFromLastModified, QDateTime datToLastModified, QDateTime datFromCreated, QDateTime datToCreated)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_DM_DOCUMENTS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	QString strSQL=" INNER JOIN CE_COMM_ENTITY ON BDMD_COMM_ENTITY_ID = CENT_ID";
	strSQL+=" INNER JOIN BUS_PERSON ON BPER_ID = CENT_OWNER_ID";
	strSQL+=" LEFT OUTER JOIN CE_EVENT_TYPE ON CEVT_ID = CENT_CE_TYPE_ID";

	//-----------------------------WHERE----------------------------
	
	QString strWhere;
	int nBindCounter=-1;
	int nBindBy_Name_Idx=-1;
	int nBindBy_FromLastModified_Idx=-1;
	int nBindBy_ToLastModified_Idx=-1;
	int nBindBy_FromCreated_Idx=-1;
	int nBindBy_ToCreated_Idx=-1;

	if (nContactID>0)
	{
		strSQL+=QString(" INNER JOIN BUS_NMRX_RELATION K2 ON CENT_ID = K2.BNMR_TABLE_KEY_ID_1 AND ")+QString(NMRX_CE_CONTACT_WHERE_AS_K2);
		strWhere+=QString(" AND K2.BNMR_TABLE_KEY_ID_2=")+QString::number(nContactID);
	}
	else if (nProjectID>0) //nContact and project are mutually exclusive..
	{
		strSQL+=QString(" INNER JOIN BUS_NMRX_RELATION K2 ON CENT_ID = K2.BNMR_TABLE_KEY_ID_1 AND ") +QString(NMRX_CE_PROJECT_WHERE);
		strWhere+=QString(" AND K2.BNMR_TABLE_KEY_ID_2=")+QString::number(nProjectID);
	}
	else if (nOwnerUserID>0)
	{
		//reset all sql:
		strSQL=" INNER JOIN CE_COMM_ENTITY ON BDMD_COMM_ENTITY_ID = CENT_ID";
		strSQL+=" INNER JOIN BUS_PERSON ON BPER_ID = CENT_OWNER_ID";
		strSQL+=QString(" AND CENT_OWNER_ID=")+QString::number(nOwnerUserID);
		strSQL+=" LEFT OUTER JOIN CE_EVENT_TYPE ON CEVT_ID = CENT_CE_TYPE_ID";
	}


	if (nDocType>=0) //owner id= logged person id
	{
		strWhere+=" AND BDMD_DOC_TYPE="+QString::number(nDocType);
	}
	if (!strSearchByName.isEmpty()) //owner id= logged person id
	{
		nBindCounter++;
		nBindBy_Name_Idx=nBindCounter;
		strWhere+=" AND BDMD_NAME LIKE ?";
	}
	if (datFromLastModified.isValid())
	{
		nBindCounter++;
		nBindBy_FromLastModified_Idx=nBindCounter;
		strWhere+=" AND BDMD_DAT_LAST_MODIFIED >=?";
	}
	if (datToLastModified.isValid())
	{
		nBindCounter++;
		nBindBy_ToLastModified_Idx=nBindCounter;
		strWhere+=" AND BDMD_DAT_LAST_MODIFIED =<?";
	}
	if (datFromCreated.isValid())
	{
		nBindCounter++;
		nBindBy_FromCreated_Idx=nBindCounter;
		strWhere+=" AND BDMD_DAT_CREATED >=?";
	}
	if (datToCreated.isValid())
	{
		nBindCounter++;
		nBindBy_ToCreated_Idx=nBindCounter;
		strWhere+=" AND BDMD_DAT_CREATED =<?";
	}
	strWhere=" WHERE "+strWhere.mid(4); //remove AND at start
	QString strSQL_Cnt="SELECT COUNT(*) FROM BUS_DM_DOCUMENTS "+strSQL+strWhere; //for total cnt prepare another statement with same where statement
	g_AccessRight->SQLFilterRecords(BUS_DM_DOCUMENTS,strSQL_Cnt);

	strSQL=DbSqlTableView::getSQLView(DbSqlTableView::TVIEW_WEBSERVICE_BUS_DM_DOCUMENT_SELECT)+strSQL+strWhere;
	GetDbManager()->GetFunction()->PrepareLimit(strSQL,nFromN,nToN);

	//-----------------------------bind if needed: byname, bydate----------------------------
	g_AccessRight->SQLFilterRecords(BUS_DM_DOCUMENTS,strSQL);
	DbSqlQuery *query=TableOrm.GetDbSqlQuery();

	strSQL +=" ORDER BY BDMD_DAT_LAST_MODIFIED DESC, BDMD_NAME DESC";

	query->Prepare(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK()) return;

	if (!strSearchByName.isEmpty()) //owner id= logged person id
	{
		strSearchByName+="%";
		query->bindValue(nBindBy_Name_Idx,strSearchByName);
	}
	if (datFromLastModified.isValid())
	{
		query->bindValue(nBindBy_FromLastModified_Idx,datFromLastModified);
	}
	if (datToLastModified.isValid())
	{
		query->bindValue(nBindBy_ToLastModified_Idx,datToLastModified);
	}
	if (datFromCreated.isValid())
	{
		query->bindValue(nBindBy_FromCreated_Idx,datFromCreated);
	}
	if (datToCreated.isValid())
	{
		query->bindValue(nBindBy_ToLastModified_Idx,datToCreated);
	}

	query->ExecutePrepared(Ret_pStatus);
	if(!Ret_pStatus.IsOK()) return;

	query->FetchData(Ret_Documents);

	//Ret_Documents.Dump();

	query->Execute(Ret_pStatus,strSQL_Cnt);
	if(!Ret_pStatus.IsOK()) return;
	if(query->next())
		Ret_nTotalCount=query->value(0).toInt();
	else
		Ret_nTotalCount=Ret_Documents.getRowCount();
	
	
	Ret_Documents.addColumn(QVariant::String,"EXTENSION");


	//-----------------------------calc extension:----------------------------
	int nSize=Ret_Documents.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		if (!Ret_Documents.getDataRef(i,"BDMD_DOC_PATH").toString().isEmpty())
		{
			QFileInfo info(Ret_Documents.getDataRef(i,"BDMD_DOC_PATH").toString());
			Ret_Documents.setData(i,"EXTENSION",info.suffix());
		}
	}

	//SortDataList lstSort;
	/* PM
		// sort by ext+name (fixed)
		lstSort<<SortData(Ret_Documents.getColumnIdx("EXTENSION"),0);
		lstSort<<SortData(Ret_Documents.getColumnIdx("BDMD_NAME"),0);
	*/ 

	//Ret_Documents.Dump();  
	// PM: new sorting by last modified date, descending
	//lstSort<<SortData(Ret_Documents.getColumnIdx("BDMD_DAT_LAST_MODIFIED"),1);  //0 asc, 1 desc
	//lstSort<<SortData(Ret_Documents.getColumnIdx("BDMD_NAME"),0);		

	//Ret_Documents.sortMulti(lstSort);
	//Ret_Documents.Dump();

	//----------------FROM -- TO test ---------------------
	//DataHelper::GetPartOfRecordSet(Ret_Documents,nFromN,nToN);

	Ret_Documents.renameColumnsFromAlias(DbSqlTableView::getView(DbSqlTableView::TVIEW_WEBSERVICE_BUS_DM_DOCUMENT_SELECT));
}


void Service_WebDocuments::ReadDocumentDetails(Status &Ret_pStatus, int nDocumentID, DbRecordSet &Ret_Document)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_DM_DOCUMENTS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	QString strWhere=" INNER JOIN CE_COMM_ENTITY ON BDMD_COMM_ENTITY_ID = CENT_ID";
	strWhere+=" INNER JOIN BUS_PERSON ON BPER_ID = CENT_OWNER_ID";
	strWhere+=" LEFT OUTER JOIN CE_EVENT_TYPE ON CEVT_ID = CENT_CE_TYPE_ID";
	strWhere+=" WHERE BDMD_ID="+QString::number(nDocumentID);

	g_AccessRight->SQLFilterRecords(BUS_DM_DOCUMENTS,strWhere);
	TableOrm.Read(Ret_pStatus,Ret_Document,strWhere,DbSqlTableView::TVIEW_WEBSERVICE_BUS_DM_DOCUMENT_DETAILS);
	if(!Ret_pStatus.IsOK()) return;

	Ret_Document.renameColumnsFromAlias(DbSqlTableView::getView(DbSqlTableView::TVIEW_WEBSERVICE_BUS_DM_DOCUMENT_DETAILS));
}

//performs check in user storage into the database........
void Service_WebDocuments::CheckIn(Status &Ret_pStatus, int nDocumentID)
{
	int nPersonID=g_UserSessionManager->GetPersonID();
	DbRecordSet Ret_DocumentRevision;
	DbRecordSet Ret_CheckOutInfo;

	g_BusinessServiceSet->BusDocuments->CheckInFromUserStorage(Ret_pStatus, nDocumentID, nPersonID,true,Ret_DocumentRevision,Ret_CheckOutInfo);
}

//performs check out from database to the user storage
void Service_WebDocuments::CheckOut(Status &Ret_pStatus, int nDocumentID,QString &Ret_strDocumentURL,bool bReadOnly)
{
	int nPersonID=g_UserSessionManager->GetPersonID();
	DbRecordSet Ret_DocumentRevision;
	DbRecordSet Ret_CheckOutInfo;

	g_BusinessServiceSet->BusDocuments->CheckOutToUserStorage(Ret_pStatus, nDocumentID, nPersonID,bReadOnly,Ret_DocumentRevision,Ret_CheckOutInfo);

	QString strServerFilePath=Ret_CheckOutInfo.getDataRef(0,"BDMD_DOC_PATH").toString();

	//based on logged user determine encoded user request string for file and return to the user:
	DbRecordSet rowUserData;
	g_UserSessionManager->GetUserData(rowUserData);

	QString strUserName=rowUserData.getDataRef(0,"CUSR_USERNAME").toString();
	QByteArray bytePassword=rowUserData.getDataRef(0,"CUSR_PASSWORD").toByteArray();
	QString strCurrentSession=g_UserSessionManager->GetSessionID();

	//QString strAuth=UserStorageHelper::GenerateAuthQueryString(strUserName,bytePassword,strCurrentSession);
	Ret_strDocumentURL=UserStorageHelper::GenerateRequestQueryString(strServerFilePath);

}
void Service_WebDocuments::CheckOutCopy(Status &Ret_pStatus, int nDocumentID,QString &Ret_strDocumentURL)
{
	CheckOut(Ret_pStatus,nDocumentID,Ret_strDocumentURL,true);
}

void Service_WebDocuments::ReadNoteDocument(Status &Ret_pStatus, int nDocumentID,QString &Ret_strDocumentURL)
{
	DbRecordSet Ret_Document;

	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_DM_DOCUMENTS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	QString strWhere=" WHERE BDMD_ID="+QString::number(nDocumentID);

	g_AccessRight->SQLFilterRecords(BUS_DM_DOCUMENTS,strWhere);
	TableOrm.Read(Ret_pStatus,Ret_Document,strWhere);
	if(!Ret_pStatus.IsOK()) return;

	if (Ret_Document.getRowCount()!=1)
	{
		Ret_pStatus.setError(1,"No document found");
		return;
	}

	//test document type:
	if (Ret_Document.getDataRef(0,"BDMD_DOC_TYPE").toInt()!=GlobalConstants::DOC_TYPE_NOTE)
	{
		Ret_pStatus.setError(1,"Wrong document type");
		return;
	}

	QString byteContent=Ret_Document.getDataRef(0,"BDMD_DESCRIPTION").toString();
	QString strName=Ret_Document.getDataRef(0,"BDMD_NAME").toString();

	//QString strSession=g_UserSessionManager->GetSessionID();
	//QString strUserSubDir=DataHelper::EncodeSession2Directory(strSession);
	//QString strDirPath=g_UserSessionManager->InitializeUserStorageDirectory(Ret_pStatus,strUserSubDir);
	//if(!Ret_pStatus.IsOK()) return;


	QString strDirectory=DataHelper::EncodeSession2Directory(g_UserSessionManager->GetSessionID());
	QString strServerFilePath=g_UserSessionManager->InitializeUserStorageDirectory(Ret_pStatus,strDirectory);
	if(!Ret_pStatus.IsOK()) 	
		return;

	QString strAttachmentSubDirAlone="_note_body_"+QString::number(nDocumentID);
	QDir attach(strServerFilePath+"/"+strAttachmentSubDirAlone);
	if (!attach.exists())
	{
		QDir attach_create(strServerFilePath);
		if(!attach_create.mkdir(strAttachmentSubDirAlone))
		{
			Ret_pStatus.setError(StatusCodeSet::ERR_BUS_DOCUMENT_FROM_ATTACH,"Failed to create directory "+strAttachmentSubDirAlone);
			return;
		}
	}
	QString strAttachmentSubDir=strServerFilePath+"/"+strAttachmentSubDirAlone;

	QStringList lstTempFiles;
	UnembedHtmlPix(byteContent,lstTempFiles,strAttachmentSubDir);
	
	//replace all link of CIDs with url link: /
	byteContent = byteContent.replace(strAttachmentSubDir+"/","");

	strName=QUrl::toPercentEncoding(strName);
	QString strInFile=strAttachmentSubDir+"/"+strName+".html";

	//write html:
	QFile html_file(strInFile);
	if(!html_file.open(QIODevice::WriteOnly))
	{
		Ret_pStatus.setError(1,"Error while trying to convert to html");
		return;
	}
	html_file.write(byteContent.toUtf8());
	html_file.close();

	//return link:
	Ret_strDocumentURL=UserStorageHelper::GenerateRequestQueryString(strInFile,"download",strAttachmentSubDirAlone);
}

void Service_WebDocuments::UnembedHtmlPixJob(QString &strHtmlData, QStringList &lstResTempFiles, QString strTempDir, QString strTagStart, QString strSettingStart)
{
	//find and unembed each picture in the HTML
	int nPos = strHtmlData.indexOf(strTagStart, 0, Qt::CaseInsensitive);
	while(nPos >= 0)
	{
		//extract file path
		int nSrcStart = strHtmlData.indexOf(strSettingStart, nPos, Qt::CaseInsensitive);
		if(nSrcStart > 0)
		{
			int nSrcStart1 = strHtmlData.indexOf(";base64,", nSrcStart);
			if(nSrcStart1 < 0){
				nPos = nSrcStart + 1;

				//search next
				nPos = strHtmlData.indexOf(strTagStart, nPos+1, Qt::CaseInsensitive);
				continue;
			}
			nSrcStart1 += 8; //strlen(";base64,")
			int nSrcEnd = strHtmlData.indexOf("\"", nSrcStart1);
			if(nSrcEnd > 0)
			{
				QString strBase64 = strHtmlData.mid(nSrcStart1, nSrcEnd-nSrcStart1);
				//convert from base64
				QByteArray arData = QByteArray::fromBase64(strBase64.toLatin1());

				//extract extension from mime type
				const static int nHdrLen = strSettingStart.length() + strlen("data:image/");
				QString strExt = strHtmlData.mid(nSrcStart+nHdrLen, nSrcStart1-nSrcStart-nHdrLen-8);	//8 -> strlen(";base64,")

				// save the image contents into the temp file
				QString strName = "SokratesImage." + strExt;
				QFileInfo attInfo(strName);
				QString strTmpPath;
				//generate unique temp name
				int nTry = 0; bool bOK = false;
				while(nTry < 1000 && !bOK){
					strTmpPath = strTempDir;
					strTmpPath += "/";
					strTmpPath += attInfo.completeBaseName();
					strTmpPath += QVariant(nTry).toString();
					strTmpPath += ".";
					strTmpPath += attInfo.completeSuffix();
					nTry ++;
					bOK = !QFile::exists(strTmpPath);
				}
				if(!bOK){
					Q_ASSERT(false);	//could not create unused temp path
					//search next
					nPos = strHtmlData.indexOf(strTagStart, nPos+1, Qt::CaseInsensitive);
					continue;
				}

				//save to file
				QFile file(strTmpPath);
				if (file.open(QIODevice::WriteOnly|QIODevice::Truncate))
				{
					file.write(arData);
					file.close();

					//write back into the string (replace old "src" value with file url)
					strHtmlData.remove(nSrcStart + strSettingStart.length(), nSrcEnd-nSrcStart-strSettingStart.length());
					strHtmlData.insert(nSrcStart + strSettingStart.length(), strTmpPath);

					lstResTempFiles.append(strTmpPath);
				}
			}
		}

		//search next
		nPos = strHtmlData.indexOf(strTagStart, nPos+1, Qt::CaseInsensitive);
	}
}

void Service_WebDocuments::UnembedHtmlPix(QString &strHtmlData, QStringList &lstResTempFiles, QString strTempDir)
{
	lstResTempFiles.clear();

	UnembedHtmlPixJob(strHtmlData, lstResTempFiles, strTempDir, "<img", "src=\"");
	//MB: 2014.03.07 new CSS properties that can contain CID embedded image
	UnembedHtmlPixJob(strHtmlData, lstResTempFiles, strTempDir, "<table", "background=\"");
	UnembedHtmlPixJob(strHtmlData, lstResTempFiles, strTempDir, "<div", "background=\"");
	UnembedHtmlPixJob(strHtmlData, lstResTempFiles, strTempDir, "<td", "background=\"");
}