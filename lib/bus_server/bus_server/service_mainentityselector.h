#ifndef SERVICE_MAINENTITYSELECTOR_H
#define SERVICE_MAINENTITYSELECTOR_H

#include "businessservice.h"
#include "bus_interface/bus_interface/interface_mainentityselector.h"




/*!
	\class Service_MainEntitySelector
	\brief Data selection business object for all main entity data
	\ingroup Bus_Services

	
	Entity is resolved by entity ID defined in the bus_core/entity_id_collection.h
	Data is selected in predefined view, filtered by filter.

*/

class Service_MainEntitySelector : public Interface_MainEntitySelector, public BusinessService
{
public:
	void ReadData(Status &Ret_pStatus, int nEntityID, DbRecordSet Filter, DbRecordSet &Ret_Data);
	void ReadDataBatch(Status &Ret_pStatus, DbRecordSet lstEntityIDs, DbRecordSet lstFilters, DbRecordSet &Ret_Data);
	//special use by SAPNE patterns:
	void ReadDataAll(Status &Ret_pStatus, int nEntityID,int nCountInClientCache, QDateTime datClientCacheTime, DbRecordSet &Ret_Data);
	void ReadDataOne(Status &Ret_pStatus, int nEntityID,DbRecordSet Filter, DbRecordSet &Ret_Data);


private:
	void ReadBusPerson(Status &Ret_pStatus, int nEntityID, DbRecordSet Filter, DbRecordSet &Ret_Data);
	void ReadCoreUser(Status &Ret_pStatus, int nEntityID, DbRecordSet Filter, DbRecordSet &Ret_Data);
	void ReadBusOrganization(Status &Ret_pStatus, int nEntityID, DbRecordSet Filter, DbRecordSet &Ret_Data);
	void ReadContactTypes(Status &Ret_pStatus, int nEntityID, DbRecordSet Filter, DbRecordSet &Ret_Data);
	void ReadBusDepartment(Status &Ret_pStatus, int nEntityID, DbRecordSet Filter, DbRecordSet &Ret_Data);
	void ReadBusCostCenter(Status &Ret_pStatus, int nEntityID, DbRecordSet Filter, DbRecordSet &Ret_Data);
	void ReadCeTypes(Status &Ret_pStatus, int nEntityID, DbRecordSet Filter, DbRecordSet &Ret_Data);
	void ReadProjects(Status &Ret_pStatus, int nEntityID, DbRecordSet Filter, DbRecordSet &Ret_Data);
	void ReadCeEventTypes(Status &Ret_pStatus, int nEntityID, DbRecordSet Filter, DbRecordSet &Ret_Data);
	void ReadGridViews(Status &Ret_pStatus, int nEntityID, DbRecordSet Filter, DbRecordSet &Ret_Data);
	void ReadDMApplications(Status &Ret_pStatus, int nEntityID, DbRecordSet Filter, DbRecordSet &Ret_Data);
	void ReadCategoryDoc(Status &Ret_pStatus, int nEntityID, DbRecordSet Filter, DbRecordSet &Ret_Data);
	void ReadCategoryEmail(Status &Ret_pStatus, int nEntityID, DbRecordSet Filter, DbRecordSet &Ret_Data);
	void ReadCategoryCalendar(Status &Ret_pStatus, int nEntityID, DbRecordSet Filter, DbRecordSet &Ret_Data);
	void ReadResources(Status &Ret_pStatus, int nEntityID, DbRecordSet Filter, DbRecordSet &Ret_Data);
	void ReadEmailTemplates(Status &Ret_pStatus, int nEntityID, DbRecordSet Filter, DbRecordSet &Ret_Data);
	void ReadPersonEmailAddresses(Status &Ret_pStatus, int nEntityID, DbRecordSet Filter, DbRecordSet &Ret_Data);
	void ReadCalendarViews(Status &Ret_pStatus, int nEntityID, DbRecordSet Filter, DbRecordSet &Ret_Data);
	
	//Contacts:
	void ReadContactOrganization(Status &Ret_pStatus, int nEntityID, DbRecordSet Filter, DbRecordSet &Ret_Data);
	void ReadContactDepartment(Status &Ret_pStatus, int nEntityID, DbRecordSet Filter, DbRecordSet &Ret_Data);
	void ReadContactFunction(Status &Ret_pStatus, int nEntityID, DbRecordSet Filter, DbRecordSet &Ret_Data);
	void ReadContactProfession(Status &Ret_pStatus, int nEntityID, DbRecordSet Filter, DbRecordSet &Ret_Data);
	void ReadContactGroups(Status &Ret_pStatus, int nEntityID, DbRecordSet Filter, DbRecordSet &Ret_Data);
	void ReadGroupItems(Status &Ret_pStatus, int nEntityID, DbRecordSet Filter, DbRecordSet &Ret_Data);
	void ReadPaymentConditions(Status &Ret_pStatus, int nEntityID, DbRecordSet Filter, DbRecordSet &Ret_Data);
	void ReadCustomFields(Status &Ret_pStatus, int nEntityID, DbRecordSet Filter, DbRecordSet &Ret_Data);

	//Email:
	void ReadAllEmails(Status &Ret_pStatus, int nEntityID, DbRecordSet Filter, DbRecordSet &Ret_Data);
	
	//special use:
	QDateTime GetLastModifyDateForTable(Status &err,int nEntityID);
	int GetCountForTable(Status &err,int nEntityID);

};

#endif // SERVICE_MAINENTITYSELECTOR_H
