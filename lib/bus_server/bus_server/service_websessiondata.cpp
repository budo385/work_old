#include "service_websessiondata.h"

#include "bus_core/bus_core/accessrightsid.h"
#include "bus_core/bus_core/optionsandsettingsid.h"
#include "bus_core/bus_core/customavailability.h"

#include "bus_core/bus_core/usersessionmanager.h"
extern UserSessionManager	*g_UserSessionManager;
#include "privateserviceset.h"
extern PrivateServiceSet* g_PrivateServiceSet; 
#include "businessserviceset.h"
extern BusinessServiceSet* g_BusinessServiceSet;


//get user session data:
void Service_WebSessionData::GetUserSessionData(Status &Ret_Status,DbRecordSet &Ret_ARSet,DbRecordSet &Ret_UserData, DbRecordSet &Ret_ContactData,  QString &Ret_strEmailCalConfAddress,  int &Ret_nIsAnestezistClient,  int &Ret_nCSID)
{

	DbRecordSet lstAr;
	//AR set:
	g_BusinessServiceSet->AccessRights->GetUserAccRightsRecordSet(Ret_Status,lstAr,m_SessionManager->GetUserID());
	if(!Ret_Status.IsOK()) return;

	//filter only 4our: show contacts, calendar, tasks, projects, documents:
	Ret_ARSet.copyDefinition(lstAr);

	lstAr.find("CAR_CODE", (int)OPEN_CALENDAR);
	lstAr.find("CAR_CODE", (int)ENTER_MODIFY_PROJECT_MAIN_DATA,false,false,false);
	lstAr.find("CAR_CODE", (int)ENTER_MODIFY_CONTACTS,false,false,false);
	lstAr.find("CAR_CODE", (int)ASSIGN_CE_ENTITIES_TO_PROJECTS_AND_CONTACTS,false,false,false);

	Ret_ARSet.merge(lstAr,true);
	//Ret_ARSet.Dump();


	//--------------------------
	//OPTIONS AND SETTINGS.
	//--------------------------
	DbRecordSet Ret_recSettings;
	DbRecordSet Ret_recOptions;
	GetOptionsAndSettings(Ret_Status, m_SessionManager->GetPersonID(), Ret_recSettings, Ret_recOptions);
	if(!Ret_Status.IsOK()) return;

	Ret_recOptions.find("BOUS_SETTING_SET_ID",(int)CALENDAR_OPTIONS);
	int nRow=Ret_recOptions.find("BOUS_SETTING_ID",(int)CALENDAR_EMAIL_CONFIRMATION_ADDRESS,true,true);
	if (nRow>0)
	{
		Ret_strEmailCalConfAddress=Ret_recOptions.getDataRef(nRow,"BOUS_VALUE_STRING").toString();
	}



	//--------------------------------------------------------
	// GET USER DATA: BPER_FIRST_NAME & LAST NAME + email + etc....
	//--------------------------------------------------------
	//load from DB username, password, group, 
	//user data
	int nUserID=m_SessionManager->GetUserID();
	g_PrivateServiceSet->CoreUser->GetUserByID(Ret_Status,nUserID,Ret_UserData); //TVIEW_CORE_USER_SELECTION format
	if(!Ret_Status.IsOK())return;

	//Ret_UserData.Dump();


	//user contact data:
	int nContactID=Ret_UserData.getDataRef(0,"BPER_CONTACT_ID").toInt();
	if(nContactID!=0)
	{
		g_BusinessServiceSet->BusContact->ReadContactDefaults(Ret_Status,nContactID,Ret_ContactData); //TVIEW_BUS_CM_DEAFULTS
		if(!Ret_Status.IsOK()) return;
	}

	//Ret_ContactData.Dump();

	Ret_nCSID = CustomAvailability::GetCustomSolutionID();

	if (Ret_nCSID==CUSTOM_SOL_AS)
		Ret_nIsAnestezistClient = 1;
	else
		Ret_nIsAnestezistClient = 0;

	
	
}




void Service_WebSessionData::GetOptionsAndSettings(Status &pStatus, int nPersonID, DbRecordSet &recSettings, DbRecordSet &recOptions)
{
	//Get person ID.
	QString strPersonID = QVariant(nPersonID).toString();

	DbSimpleOrm OPTIONS_SETTINGS_ORM(pStatus, BUS_OPT_SETTINGS, GetDbManager()); 
	if(!pStatus.IsOK()) return;

	QString strWhere = "WHERE BOUS_PERSON_ID = " + strPersonID;
	if (nPersonID>0)
	{
		//First person settings.
		OPTIONS_SETTINGS_ORM.Read(pStatus, recSettings, strWhere);
		if(!pStatus.IsOK()) return;
	}
	else
	{
		recSettings.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_OPT_SETTINGS));
	}

	//_DUMP(recSettings);

	strWhere = "WHERE BOUS_PERSON_ID IS NULL ";
	OPTIONS_SETTINGS_ORM.Read(pStatus, recOptions, strWhere);
	if(!pStatus.IsOK()) return;
}