#include "service_usersession.h"
#include "db/db/dbsimpleorm.h"
#include "bus_core/bus_core/usersessionmanager.h"
extern UserSessionManager	*g_UserSessionManager;
#include "systemserviceset.h"
extern SystemServiceSet* g_SystemServiceSet;	


/*!
	Reads all session belonging to one app. server

	\param pStatus				- if err occur, returned here
	\param nAppServerSessionID	- app. server id 
	\param lstSessions			- full view of readed session table (record set) in this case one row only!!
	\param pDbConn				- db conn
*/
void Service_UserSession::ReadSessionByAppSrvID(Status &pStatus,int nAppServerSessionID,DbRecordSet& lstSessions)
{
	//make local query, can fail if connection reservation fails
	DbSimpleOrm TableOrm(pStatus,CORE_USER_SESSION,GetDbManager()); if(!pStatus.IsOK()) return;

	//read:
	TableOrm.Read(pStatus,lstSessions,"WHERE COUS_APP_SRV_SESSION_ID= "+QVariant(nAppServerSessionID).toString());

}

void Service_UserSession::ReadActiveSessionByAppSrvID(Status &pStatus,int nAppServerSessionID,DbRecordSet& lstSessions)
{
	DbSimpleOrm TableOrm(pStatus,CORE_USER_SESSION,GetDbManager()); if(!pStatus.IsOK()) return;
	TableOrm.Read(pStatus,lstSessions,"WHERE COUS_APP_SRV_SESSION_ID= "+QVariant(nAppServerSessionID).toString()+" AND COUS_ACTIVE_FLAG=1");
}



/*!
	Read row of data from session table for given session id string

	\param pStatus				- if err occur, returned here
	\param strSessionID			- session ID (string) - COUS_USER_SESSION_ID
	\param lstSessions			- full view of readed session table (record set) in this case one row only!!
	\param pDbConn				- db conn
*/
void Service_UserSession::ReadSessionBySessionID(Status &pStatus,QString strSessionID,DbRecordSet& lstSessions)
{
	//make local query, can fail if connection reservation fails
	DbSimpleOrm TableOrm(pStatus,CORE_USER_SESSION,GetDbManager()); if(!pStatus.IsOK()) return;

	//read:
	TableOrm.Read(pStatus,lstSessions,"WHERE COUS_USER_SESSION_ID= '"+strSessionID+"'");

}



/*!
	Insert new session in table, if not unique, SQL error returned

	\param pStatus				if err occur, returned here
	\param strSessionID			new session id (32byte string) - COUS_USER_SESSION_ID
	\param nAppServerSessionID	app server id
	\param nMaxSessionCount		maximum sessions, must be tested first before inserting
	\param strModuleCode		client module code
	\param strMLIID				MLIID
	\param nUserID				link to CORE_USER table (must exists)
	\param nSessionRecordID		returned session record id
	\param pDbConn				db conn
*/
void Service_UserSession::InsertSession(Status &pStatus,QString strSessionID,int nAppServerSessionID,int nMaxSessionCount,QString strModuleCode, QString strMLIID,int nUserID,int &nSessionRecordID, int nParentSessionID)
{
	//make local query, can fail if connection reservation fails
	DbSimpleOrm TableOrm(pStatus,CORE_USER_SESSION,GetDbManager());
	if(!pStatus.IsOK()) return;

	//-----------------------------------------------------
	//			CHECK LICENSE IN USE
	//-----------------------------------------------------
	if (nParentSessionID==0)
	{
		CheckLicenseCount(pStatus,nMaxSessionCount,strModuleCode,strMLIID);
		if(!pStatus.IsOK())	return;
	}


	//-----------------------------------------------------
	//			TRANSACTION: INSERT USER SESSION
	//-----------------------------------------------------
	TableOrm.GetDbSqlQuery()->BeginTransaction(pStatus);
	if(!pStatus.IsOK()) return;

	//prepare insert query:
	DbRecordSet rowUser;
	rowUser.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CORE_USER_SESSION));

	//bind vars:
	int nActiveFlag=1;
	rowUser.addRow();
	rowUser.setData(0,"COUS_USER_SESSION_ID",strSessionID);
	rowUser.setData(0,"COUS_APP_SRV_SESSION_ID",nAppServerSessionID);
	rowUser.setData(0,"COUS_MODULE_CODE",strModuleCode);
	rowUser.setData(0,"COUS_MLID",strMLIID);
	rowUser.setData(0,"COUS_CORE_USER_ID",nUserID);
	rowUser.setData(0,"COUS_ACTIVE_FLAG",nActiveFlag);
	if (nParentSessionID>0)
		rowUser.setData(0,"COUS_PARENT_ID",nParentSessionID);

	TableOrm.Write(pStatus,rowUser);
	if(!pStatus.IsOK()) 
	{
		TableOrm.GetDbSqlQuery()->Rollback();
		return;
	}

	//store session ID:
	nSessionRecordID=rowUser.getDataRef(0,0).toInt();

	//---------------------------------------------------------------------
	//			TRANSACTION: INCREASE NUMBER OF USED CONNECTIONS/SESSIONS
	//---------------------------------------------------------------------
	g_SystemServiceSet->AppSrvSession->UpdateCurrentConnection(pStatus,nAppServerSessionID);
	if(pStatus.IsOK())
		TableOrm.GetDbSqlQuery()->Commit(pStatus);
	else
	{
		TableOrm.GetDbSqlQuery()->Rollback();
		return;
	}

	
}




/*!
	Deletes list of sessions

	\param pStatus				if err occur, returned here
	\param nAppServerSessionID	App server session ID(int) to which session belong to
	\param lstSessions			single column list of COUS_ID (integers) or record session
	\param pDbConn				db conn
*/
void Service_UserSession::DeleteSession(Status &pStatus,int nAppServerSessionID,QList<int> lstSessions)
{
	//make local query, can fail if connection reservation fails
	DbSimpleOrm TableOrm(pStatus,CORE_USER_SESSION,GetDbManager()); if(!pStatus.IsOK()) return;

	//exit if empty
	if(lstSessions.size()==0) return;

	TableOrm.GetDbSqlQuery()->BeginTransaction(pStatus);
	if(!pStatus.IsOK()) 
		return;


	//delete but only if they belong to app. server (in meantime some1 can take them)
	TableOrm.DeleteFast(pStatus,lstSessions,"AND COUS_APP_SRV_SESSION_ID="+QVariant(nAppServerSessionID).toString());
	if(!pStatus.IsOK()) 
	{
		qDebug()<<"Delete Fast failed inside DeleteSession";
		TableOrm.GetDbSqlQuery()->Rollback();
		return;
	}

	g_SystemServiceSet->AppSrvSession->UpdateCurrentConnection(pStatus,nAppServerSessionID);
	if(pStatus.IsOK())
		TableOrm.GetDbSqlQuery()->Commit(pStatus);
	else
	{
		qDebug()<<"UpdateCurrentConnection failed inside DeleteSession";
		TableOrm.GetDbSqlQuery()->Rollback();
		return;
	}


}

/*!
	Deletes user sessions by app. server id

	\param pStatus				if err occur, returned here
	\param nAppServerSessionID	App server session ID(int)
	\param pDbConn				db conn
*/
void Service_UserSession::DeleteSessionByAppSrvID(Status &pStatus,int nAppServerSessionID)
{
	//make local query, can fail if connection reservation fails
	DbSqlQuery query(pStatus,GetDbManager());if(!pStatus.IsOK()) return;


	//prepare insert query:
	QString strSQL="DELETE FROM CORE_USER_SESSION WHERE COUS_APP_SRV_SESSION_ID="+QVariant(nAppServerSessionID).toString();
	//exec & auto destroy any connection held:
	query.Execute(pStatus,strSQL);
	if(!pStatus.IsOK()) return;

	//update session count:
	g_SystemServiceSet->AppSrvSession->UpdateCurrentConnection(pStatus,nAppServerSessionID);

}


/*!
	Deletes user sessions by app. server id and ACTIVE FLAG = false and only if EXPIRED period exceeded!!

	\param pStatus				- if err occur, returned here
	\param nAppServerSessionID	- App server session ID(int)
	\param pDbConn				- db conn
*/
void Service_UserSession::DeleteInactiveSessions(Status &pStatus,int nAppServerSessionID)
{
	//make local query, can fail if connection reservation fails
	DbSqlQuery query(pStatus,GetDbManager());
	if(!pStatus.IsOK()) return;

	int nSecondsForLife=g_UserSessionManager->GetIsAliveCheckInterval();
	QDateTime datExpired=QDateTime::currentDateTime().addSecs(-nSecondsForLife);

	//prepare insert query:
	QString strSQL="DELETE FROM CORE_USER_SESSION WHERE COUS_APP_SRV_SESSION_ID=? AND COUS_ACTIVE_FLAG=0 AND COUS_DAT_LAST_MODIFIED<?";
	query.Prepare(pStatus,strSQL);
	if(!pStatus.IsOK()) return;
	query.bindValue(0,nAppServerSessionID);
	query.bindValue(1,datExpired);

	//exec & auto destroy any connection held:
	query.ExecutePrepared(pStatus);
	if(!pStatus.IsOK()) return;

	//update session count:
	g_SystemServiceSet->AppSrvSession->UpdateCurrentConnection(pStatus,nAppServerSessionID);

}

/*!
	Update to new app.srv.id session ID, sets active

	\param pStatus				if err occur, returned here
	\param nAppServerSessionID	app. server id 
	\param strSessionID			new session id (32byte string) - COUS_USER_SESSION_ID
	\param nSessionRecordID		returned session record id of updated session
	\param pDbConn				db conn
*/

void Service_UserSession::ActivateSession(Status &pStatus,int nAppServerSessionID,QString strSessionID,int &nSessionRecordID,int nMaxSessionCount)
{

	//make local query, can fail if connection reservation fails
	DbSqlQuery query(pStatus,GetDbManager());if(!pStatus.IsOK()) return;
	DbRecordSet recTemp;

	//-----------------------------------------------------
	//	TEST IF SESSION ACTUALLY EXISTS
	//-----------------------------------------------------
	//first read ID:
	ReadSessionBySessionID(pStatus,strSessionID,recTemp);
	if(!pStatus.IsOK())return;
	//if not found error: session expired...
	if(recTemp.getRowCount()!=1)
	{pStatus.setError(StatusCodeSet::ERR_SYSTEM_SESSION_EXPIRED);return;}


	//-----------------------------------------------------
	//			CHECK LICENSE IN USE
	//-----------------------------------------------------
	QString strModuleCode=recTemp.getDataRef(0,"COUS_MODULE_CODE").toString();
	QString strMLIID=recTemp.getDataRef(0,"COUS_MLID").toString();
	CheckLicenseCount(pStatus,nMaxSessionCount,strModuleCode,strMLIID);
	if(!pStatus.IsOK())	return;

	//return back ID:
	recTemp.getData(0,recTemp.getColumnIdx("COUS_ID"),nSessionRecordID);


	//UPDATE SESSION:
	QString strSQL="UPDATE CORE_USER_SESSION SET COUS_ACTIVE_FLAG=1, COUS_APP_SRV_SESSION_ID= "+QVariant(nAppServerSessionID).toString()+" WHERE  COUS_USER_SESSION_ID='"+strSessionID+"'";

	//exec & auto destroy any connection held:
	query.Execute(pStatus,strSQL);
	if(!pStatus.IsOK())return;

	//update current session count (session can be transfered to new app. server), old one doesn't matter, coz it's probably dead:
	g_SystemServiceSet->AppSrvSession->UpdateCurrentConnection(pStatus,nAppServerSessionID);



}



/*!
	Takes all user sessions and transfers from one server to another, marks them inactive

	\param pStatus					if err occur, returned here
	\param nAppServerSessionID		app. server id 
	\param nOldAppServerSessionID	old app. server session id (must be valid)
	\param pDbConn					db conn
*/
void Service_UserSession::TransferSessions(Status &pStatus,int nAppServerSessionID,int nOldAppServerSessionID)
{
	//make local query, can fail if connection reservation fails
	DbSqlQuery query(pStatus,GetDbManager());
	if(!pStatus.IsOK()) return;

	//prepare insert query:
	QString strSQL="UPDATE CORE_USER_SESSION SET COUS_ACTIVE_FLAG=0, COUS_APP_SRV_SESSION_ID= "+QVariant(nAppServerSessionID).toString()+" WHERE  COUS_APP_SRV_SESSION_ID="+QVariant(nOldAppServerSessionID).toString();

	//exec & auto destroy any connection held:
	query.Execute(pStatus,strSQL);
	if(!pStatus.IsOK())return;

	//update current session count:
	g_SystemServiceSet->AppSrvSession->UpdateCurrentConnection(pStatus,nAppServerSessionID);
}


/*!
	Sets active flag to false to every session within one app server(used by garbage collector when server crashes)

	\param pStatus				if err occur, returned here
	\param nAppServerSessionID	app. server id 
	\param pDbConn				db conn
*/
void Service_UserSession::InactivateSessions(Status &pStatus,int nAppServerSessionID,int nSessionRecordID)
{
	//make local query, can fail if connection reservation fails
	DbSqlQuery query(pStatus,GetDbManager());
	if(!pStatus.IsOK()) return;

	QString strSQL;
	if (nSessionRecordID==-1)
		strSQL="UPDATE CORE_USER_SESSION SET COUS_ACTIVE_FLAG=0 WHERE  COUS_APP_SRV_SESSION_ID="+QVariant(nAppServerSessionID).toString();
	else
		strSQL="UPDATE CORE_USER_SESSION SET COUS_ACTIVE_FLAG=0 WHERE  COUS_APP_SRV_SESSION_ID="+QVariant(nAppServerSessionID).toString()+" AND COUS_ID="+QVariant(nSessionRecordID).toString();
	

	//exec & auto destroy any connection held:
	query.Execute(pStatus,strSQL);
	if(!pStatus.IsOK()) return;

	//update session count:
	g_SystemServiceSet->AppSrvSession->UpdateCurrentConnection(pStatus,nAppServerSessionID);


}




/*!
	Checks if session with given ID exists in user session table: for preventing misuse of license count

	\param pStatus				if err occur, returned here, if session does not exists, set error here
	\param nSessionRecordID		Session record ID
	\param pDbConn				db conn
*/
void Service_UserSession::CheckSession(Status &pStatus,int nSessionRecordID)
{
	//make local query, can fail if connection reservation fails
	DbSqlQuery query(pStatus,GetDbManager());if(!pStatus.IsOK()) return;

	//prepare insert query:
	QString strSQL="SELECT COUNT(*) FROM CORE_USER_SESSION WHERE COUS_ID="+QVariant(nSessionRecordID).toString();

	//exec & auto destroy any connection held:
	query.Execute(pStatus,strSQL);
	
	//return if error
	if(!pStatus.IsOK()) return;
	
	//get count:
	int nCount=0;
	if(query.GetQSqlQuery()->next())
	{
		nCount=query.GetQSqlQuery()->value(0).toInt();
	}

	//set as session is expired: there is no session in DB
	if(nCount==0)
	{
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_SESSION_EXPIRED);
	}

}

/*!
	Before insert or session activation, check session count: now this is not 100% safe

	\param pStatus			- if err occur, returned here, if session does not exists, set error here
	\param nMaxSessionCount	- maximum sessions, must be tested first before inserting
	\param strModuleCode	- client module code
	\param strMLIID			- MLIID
	\param pDbConn			- db conn
*/
void Service_UserSession::CheckLicenseCount(Status &pStatus,int nMaxSessionCount,QString strModuleCode, QString strMLIID)
{
	//if unlimited, go away:
	if (nMaxSessionCount<0)
	{
		pStatus.setError(0);
		return;
	}
	else if (nMaxSessionCount==0) //issue 1951: 0 users:_ no access
	{
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_MOD_LICENSE_MAX_REACHED);
		return;
	}

	//make local query, can fail if connection reservation fails
	DbSqlQuery query(pStatus,GetDbManager());if(!pStatus.IsOK()) return;


	int nCurrentSessionCount;

	//select count (as system will have no module code, then it will not be count in code/mlid combination):
	QString strSQL="SELECT COUNT(*) FROM CORE_USER_SESSION WHERE COUS_ACTIVE_FLAG=1 AND COUS_MODULE_CODE='"+strModuleCode+"' AND COUS_MLID='"+strMLIID+"' AND COUS_PARENT_ID IS NULL";
	query.Execute(pStatus,strSQL);
	if(!pStatus.IsOK())
		return;
	if(query.GetQSqlQuery()->next())		//get date, if record does not exists: unlock, set general error
		nCurrentSessionCount=query.GetQSqlQuery()->value(0).toInt();
	else
	{
		pStatus.setError(StatusCodeSet::ERR_BUS_FETCHING_LICENSE);
		return;
	}

	//if all licenses are used:
	if(nCurrentSessionCount>=nMaxSessionCount)
	{
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_MOD_LICENSE_MAX_REACHED);
		return;
	}
}