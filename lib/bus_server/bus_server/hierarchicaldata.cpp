#include "hierarchicaldata.h"
#include "businesslocker.h"
#include "common/common/statuscodeset.h"
#include "bus_core/bus_core/hierarchicalhelper.h"
#include "db/db/dbsimpleorm.h"
#include "db/db/dbsqlmanager.h"
extern DbSqlManager *g_DbManager;


#define _EXIT_ON_ERROR_ROLLBACK_AND_UNLOCK	\
	TableOrm.GetDbSqlQuery()->Rollback(); \
	Status err; \
	if (bLockedFromHere && !pLockResourceID.isEmpty())\
	Locker.UnLock(err, pLockResourceID);\
	return;\

#define _EXIT_ON_ERROR_ROLLBACK_AND_UNLOCK_QUERY \
	query.Rollback(); \
	Status err; \
	if (bLockedFromHere && !pLockResourceID.isEmpty())\
	Locker.UnLock(err, pLockResourceID);\
	return;\


HierarchicalData::HierarchicalData()
:m_nTableID(-1)
{

}

HierarchicalData::~HierarchicalData()
{

}

void HierarchicalData::Initialize(int nTableID,DbSqlManager* pDatabaseManager,int nWriteView,bool bReturnShortViewAfterWrite)
{
	m_pDbManager=pDatabaseManager;
	m_nTableID=nTableID;
	DbSqlTableDefinition::GetKeyData(m_nTableID,m_pTableKeyData);
	m_nWriteView=nWriteView;
	m_bReturnShortViewAfterWrite=bReturnShortViewAfterWrite;
}



/*!
	Insert, update, deletes record of one entity, in transaction
	Removes any lock after success!

	\param Ret_pStatus	- returns error if only one fails

	\param Data		- data to be written
	\param pLockResourceID	- if <>'' unlock after write, else records are locked inside
	\param strSQLWhereFilter- filter for tree if more then 1 tree items are in same table
	\param pDbConn db	- connection

	NOTE: 
	To save network traffic, Data is returned in short format or if m_bReturnShortViewAfterWrite=false, lstWrite will be returned as is.
*/
void HierarchicalData::Write(Status &Ret_pStatus, DbRecordSet &Data, QString pLockResourceID, QString strSQLWhereFilter) const
{
	Ret_pStatus.setError(0);
	DbSimpleOrm TableOrm(Ret_pStatus,m_nTableID,m_pDbManager); 
	if(!Ret_pStatus.IsOK()) return;

	//if not already locked lock records:
	bool bLockedFromHere = false;
	BusinessLocker Locker(Ret_pStatus, m_nTableID, m_pDbManager);
	if(!Ret_pStatus.IsOK()) 
		return;
	if (pLockResourceID.isEmpty())
	{
		Locker.Lock(Ret_pStatus, Data, pLockResourceID);
		if(!Ret_pStatus.IsOK()) 
			return;
		bLockedFromHere = true;
	}

	//
	// HCT specific processing for insert mode (new records):
	//	- calculate parent node from code
	//	- calculate node level and sibling
	//	- update exiting nodes from database if they should become children of the new node
	//
	// Similar processing goes in edit mode if user changed the code!
	//
	int nIdCol = Data.getColumnIdx(m_pTableKeyData.m_strFieldPrefix + "_ID");
	Q_ASSERT(nIdCol >= 0);
	int nCodeCol = Data.getColumnIdx(m_pTableKeyData.m_strFieldPrefix + "_CODE");
	Q_ASSERT(nCodeCol >= 0);
	int nPidCol = Data.getColumnIdx(m_pTableKeyData.m_strFieldPrefix + "_PARENT");
	Q_ASSERT(nPidCol >= 0);
	int nLevelCol = Data.getColumnIdx(m_pTableKeyData.m_strFieldPrefix + "_LEVEL");
	Q_ASSERT(nLevelCol >= 0);
	int nHasChildrenCol = Data.getColumnIdx(m_pTableKeyData.m_strFieldPrefix + "_HASCHILDREN");
	Q_ASSERT(nHasChildrenCol >= 0);
	int nStyleCol = Data.getColumnIdx(m_pTableKeyData.m_strFieldPrefix + "_STYLE");
	Q_ASSERT(nStyleCol >= 0);

	//start transaction
	//------------------------------------------
	TableOrm.GetDbSqlQuery()->BeginTransaction(Ret_pStatus);
	if(!Ret_pStatus.IsOK())
	{
		Status err;
		if (bLockedFromHere && !pLockResourceID.isEmpty())
			Locker.UnLock(err, pLockResourceID);
		return;
	}

	bool bInsertMode = false; 
	int nParentID = -1;

	//Data.Dump();

	int nCount = Data.getRowCount();
	for(int i=0; i<nCount; i++)
	{
		int nRecordID = 0;
		Data.getData(i,nIdCol, nRecordID);
		QString strCode;
		Data.getData(i,nCodeCol, strCode);
		int nOldParentID = 0;
		Data.getData(i,nPidCol, nOldParentID);

		// calculate (new) parent and the level
		int nParentLevel = -1;
		nParentID = CalcParent(strCode, nParentLevel, strSQLWhereFilter);
		if(nParentID >= 0)
		{
			if(nParentID != nRecordID)
			{
				Data.setData(i, nPidCol, nParentID);
			}
		}
		else{
			Data.setData(i, nPidCol, QVariant(QVariant::Int));
		}

		//B.T: 2013.03.11: Level is always number of dots:
		int nLevel=strCode.split(".",QString::SkipEmptyParts).count()-1;
		if (nLevel<0)
			nLevel=0;
		Data.setData(i, nLevelCol, nLevel);

		bool bCodeChanged=false;
		if(nRecordID < 1)	// newly inserted field (no ID)
		{
			bInsertMode = true;
			//TEMP: fill style to 0
			Data.setData(i, nStyleCol, 0);
		}
		else	//editing already existing record
		{
			//check if the code was changed
			QString strOldCode;
			if(GetCodeFromID(Ret_pStatus, nRecordID, strOldCode,strSQLWhereFilter))
			{
				if(strCode != strOldCode)
				{
					bCodeChanged=true;
					// record code was changed!!!!, recalc all needed things
					//STEP 1: check if this new code was already taken!
					int nExistingID = 0;
					if(GetIDFromCode(Ret_pStatus, strCode, nExistingID, strSQLWhereFilter))
					{
						Ret_pStatus.setError(StatusCodeSet::MSG_HIER_ENTITY_RECORD_DUPLICATE_CODE);
						_EXIT_ON_ERROR_ROLLBACK_AND_UNLOCK
					}

					//change code on all children...if code is changed then all subprojects must have code = old_code-old_parent_code+new_parent_code
					OnCodeChanged(Ret_pStatus,strSQLWhereFilter,strOldCode,strCode);
					if(!Ret_pStatus.IsOK())
					{ 
						_EXIT_ON_ERROR_ROLLBACK_AND_UNLOCK
					}
				}
			}
			else
				Q_ASSERT(false);	//error fetching record code
		}


		//calculate "haschildren" field
		int nChildrenCnt = CalculateChildCount(strCode, strSQLWhereFilter);
		Data.setData(i, nHasChildrenCol, (nChildrenCnt > 0)? 1 : 0);


		//NOTE: we must write records one by one because previous one might
		//      turn out to be parent of the new one (when calculating parent of the new one, previous must already be in a DB)
		DbRecordSet lstRow = Data.getRow(i);
		Data.Dump();
		TableOrm.Write(Ret_pStatus,lstRow,m_nWriteView);
		if(!Ret_pStatus.IsOK())
		{ 
			if (Ret_pStatus.getErrorCode()==StatusCodeSet::ERR_SQL_UNIQUE_CONSTRAINT_VIOLATION)
				Ret_pStatus.setError(StatusCodeSet::ERR_BUS_DUPLICATE_PROJECT_CODE);
			_EXIT_ON_ERROR_ROLLBACK_AND_UNLOCK
		}
		//copy the ID data back to orig list
		Data.setData(i,nIdCol, lstRow.getDataRef(0, nIdCol));


		if(bInsertMode || bCodeChanged)
		{
			//now update all children data -> parent id of new childrens of this node (if any)
			if (nChildrenCnt>0)
			{
				if(!UpdateChildren(Ret_pStatus,lstRow,strSQLWhereFilter))
				{ 
					Ret_pStatus.setError(1,"Error while updating children nodes of projects.");
					_EXIT_ON_ERROR_ROLLBACK_AND_UNLOCK
				}
			}

			//update haschildren of parent of this node (only if insert or code changed): always set 1 as this node is child...
			if(nParentID >= 0)
				UpdateHasChildren(Ret_pStatus,nParentID,strSQLWhereFilter,true);

		}
	}


	//EMPTY LISTS to avoid unnecessary client/server traffic
	//Return only WRITE list in short format, to notify client with new ID, LM, FK,etc...
	if (m_bReturnShortViewAfterWrite)
	{
		DbRecordSet returnedList;
		DefineHctReturnList(returnedList); //<ID,LM,FK,PARENT,LEVEL,SIBLING>
		returnedList.merge(Data);
		Data=returnedList;	
	}

	//commit transaction
	//------------------------------------------
	TableOrm.GetDbSqlQuery()->Commit(Ret_pStatus);

	if (!pLockResourceID.isEmpty())//unlock
		Locker.UnLock(Ret_pStatus, pLockResourceID);
}

/*!
Deletes whole entity data

\param Ret_pStatus	- returns error if only one fails
\param Data			- list of entity/record ID's for delete
\param pDbConn db	- connection

*/
void HierarchicalData::Delete(Status &Ret_pStatus, DbRecordSet &Data, QString pLockResourceID,QString strSQLWhereFilter )  const
{
	QString strSQL;
	DbSqlQuery query(Ret_pStatus,m_pDbManager);
	if(!Ret_pStatus.IsOK())return; 
	
	//if not already locked lock records:
	bool bLockedFromHere = false;
	BusinessLocker Locker(Ret_pStatus, m_nTableID, m_pDbManager);
	if(!Ret_pStatus.IsOK()) 
		return;
	if (pLockResourceID.isEmpty())
	{
		Locker.Lock(Ret_pStatus, Data, pLockResourceID);
		if(!Ret_pStatus.IsOK()) 
			return;
		bLockedFromHere = true;
	}

	//start transaction
	query.BeginTransaction(Ret_pStatus);
	if(!Ret_pStatus.IsOK())
	{
		Status err;
		if (bLockedFromHere && !pLockResourceID.isEmpty())
		Locker.UnLock(err, pLockResourceID);
		return;
	}

	int nSize=Data.getRowCount();
	for(int i=0;i<nSize;i++)
	{
		//entity id
		int nEntityID;
		Data.getData(i,0,nEntityID);
		Q_ASSERT(nEntityID!=0);

		//TOFIX CalcParentID(query, status, nEntityID)
		//calculate parent node (update parent's HASCHILDREN flag if necessary)
		int nParentID = -1;
		strSQL  = "SELECT " + m_pTableKeyData.m_strFieldPrefix + "_PARENT FROM " + m_pTableKeyData.m_strTableName;
		strSQL += " WHERE " + m_pTableKeyData.m_strFieldPrefix + "_ID = " + QVariant(nEntityID).toString();
		strSQL +=strSQLWhereFilter;

		query.Execute(Ret_pStatus, strSQL);
		if(!Ret_pStatus.IsOK())
		{
			_EXIT_ON_ERROR_ROLLBACK_AND_UNLOCK_QUERY
		}

		DbRecordSet rsResult;
		query.FetchData(rsResult);
		if(rsResult.getRowCount()>0)
			nParentID = rsResult.getDataRef(0,0).toInt();

		//
		// delete the node
		//
		strSQL="DELETE FROM " + m_pTableKeyData.m_strTableName + " WHERE " + m_pTableKeyData.m_strPrimaryKey + " = "+QVariant(nEntityID).toString();
		strSQL +=strSQLWhereFilter;

		query.Execute(Ret_pStatus,strSQL);
		if(!Ret_pStatus.IsOK())
		{ 
			_EXIT_ON_ERROR_ROLLBACK_AND_UNLOCK_QUERY
		}

		//update parent's HASCHILDREN flag if necessary
		if(nParentID >= 0)
		{
			//TOFIX CalcChildCount(query, status, nEntityID)
			//check if the parent has any child left
			QString strSQL = "SELECT count(*) FROM " + m_pTableKeyData.m_strTableName;
			strSQL += " WHERE " + m_pTableKeyData.m_strFieldPrefix + "_PARENT";
			strSQL += " = " + QString().sprintf("%d",nParentID);
			strSQL +=strSQLWhereFilter;

			query.Execute(Ret_pStatus, strSQL);
			if(!Ret_pStatus.IsOK())
			{
				_EXIT_ON_ERROR_ROLLBACK_AND_UNLOCK_QUERY
			}

			DbRecordSet rsResult;
			query.FetchData(rsResult);

			int nChildCount = 0;
			if(rsResult.getRowCount()>0)
				nChildCount = rsResult.getDataRef(0,0).toInt();
			if(nChildCount >0)
				nChildCount = 1;

			strSQL = "UPDATE " + m_pTableKeyData.m_strTableName;
			strSQL += " SET " + m_pTableKeyData.m_strFieldPrefix + "_HASCHILDREN = " + QString().sprintf("%d",nChildCount);
			strSQL += " WHERE " + m_pTableKeyData.m_strFieldPrefix + "_ID";
			strSQL += " = " + QString().sprintf("%d",nParentID);

			query.Execute(Ret_pStatus, strSQL);
			if(!Ret_pStatus.IsOK())
			{
				_EXIT_ON_ERROR_ROLLBACK_AND_UNLOCK_QUERY
			}
		}
	}

	query.Commit(Ret_pStatus);
	
	if (!pLockResourceID.isEmpty())//unlock
		Locker.UnLock(Ret_pStatus, pLockResourceID);
}

bool HierarchicalData::GetAncestorNodes(const QString &strCode, DbRecordSet &Data, QString strSQLWhereFilter)  const
{
	Q_ASSERT(strCode.length() > 0);

	//create query (just for connection reservation/transaction):
	Status status;
	DbSqlQuery query(status, m_pDbManager);
	if(!status.IsOK())
		return false; //reserve connection failed

	//calculate fields to fetch
	QString strFields = GetSelectNames(Data);
	Q_ASSERT(!strFields.isEmpty());

	QString strSQL;
	strSQL.sprintf("SELECT %s FROM %s WHERE ", 
		strFields.toLatin1().constData(),
		m_pTableKeyData.m_strTableName.toLatin1().constData());

	//strSQL +=strSQLWhereFilter;

	if(m_pTableKeyData.m_bHasActiveFlag)
	{
		QString strSQL2;
		strSQL2.sprintf("%s_ACTIVE_FLAG=1 AND ", m_pTableKeyData.m_strFieldPrefix.toLatin1().constData());
		strSQL += strSQL2;
	}

	int nLength = strCode.length();
	QString strSQL1;

	//B.T: universal DB indepent:
	m_pDbManager->GetFunction()->PrepareHierarchySelect(strSQL1,strCode,m_pTableKeyData.m_strFieldPrefix,false);

	strSQL += strSQL1;
	strSQL +=strSQLWhereFilter;

	query.Prepare(status, strSQL);
	if(!status.IsOK()){
		Q_ASSERT_X(false, "HierarchicalData::GetAncestorNodes", status.getErrorText().toLatin1());	
		return false;
	}

	query.bindValue(0, strCode);

	query.ExecutePrepared(status);

	//query.Execute(status, strSQL);
	if(!status.IsOK())
	{
		Q_ASSERT_X(false, "HierarchicalData::GetAncestorNodes", status.getErrorText().toLatin1());	
		return false;
	}


	DbRecordSet rsResult;
	query.FetchData(rsResult);

	Data = rsResult;
	//Data.Dump();
	return true;
}

void HierarchicalData::DefineHctReturnList(DbRecordSet& pList) const
{
	pList.destroy();
	pList.addColumn(QVariant::Int,m_pTableKeyData.m_strFieldPrefix + "_ID");
	pList.addColumn(QVariant::DateTime,m_pTableKeyData.m_strLastModified);

	pList.addColumn(QVariant::Int,m_pTableKeyData.m_strFieldPrefix + "_PARENT");
	pList.addColumn(QVariant::Int,m_pTableKeyData.m_strFieldPrefix + "_LEVEL");
	pList.addColumn(QVariant::Int,m_pTableKeyData.m_strFieldPrefix + "_HASCHILDREN");
	pList.addColumn(QVariant::Int,m_pTableKeyData.m_strFieldPrefix + "_STYLE");

	pList.addColumn(QVariant::String,m_pTableKeyData.m_strFieldPrefix + "_CODE");
	pList.addColumn(QVariant::String,m_pTableKeyData.m_strFieldPrefix + "_NAME");

}

bool HierarchicalData::GetDescendantsNodes(const QString &strCode, DbRecordSet &Data, QString strSQLWhereFilter, bool bOnlyActiveProjects, int nLevelsDeep)  const
{
	Q_ASSERT(strCode.length() > 0);

	//create query (just for connection reservation/transaction):
	Status status;
	DbSqlQuery query(status, m_pDbManager);
	if(!status.IsOK())
		return false; //reserve connection failed

	//calculate fields to fetch
	QString strFields = GetSelectNames(Data);
	if(strFields.isEmpty())
		strFields = "*";

	QString strSQL;
	strSQL.sprintf("SELECT %s FROM %s WHERE ", 
		strFields.toLatin1().constData(),
		m_pTableKeyData.m_strTableName.toLatin1().constData());
	//strSQL +=strSQLWhereFilter;

	if(m_pTableKeyData.m_bHasActiveFlag && bOnlyActiveProjects)
	{
		QString strSQL2;
		strSQL2.sprintf("%s_ACTIVE_FLAG=1 AND ", m_pTableKeyData.m_strFieldPrefix.toLatin1().constData());
		strSQL += strSQL2;
	}

	int nLength = strCode.length();
	QString strSQL1;
	//B.T: universal DB indepent:
	m_pDbManager->GetFunction()->PrepareHierarchySelect(strSQL1,strCode,m_pTableKeyData.m_strFieldPrefix,true);
	strSQL += strSQL1;
	strSQL +=strSQLWhereFilter;
	query.Prepare(status, strSQL);
	if(!status.IsOK()){
		Q_ASSERT_X(false, "HierarchicalData::GetDescendantsNodes", status.getErrorText().toLatin1());	
		return false;
	}

	query.bindValue(0, strCode);
	query.ExecutePrepared(status);
	if(!status.IsOK())
		return false;

	DbRecordSet rsResult;
	query.FetchData(rsResult);
	Data=rsResult;

	int nLevelCol = Data.getColumnIdx(m_pTableKeyData.m_strFieldPrefix + "_LEVEL");
	Q_ASSERT(nLevelCol >= 0);

	//B.T. filter records manually to get levels deep:
	int nSize=Data.getRowCount();
	if (nLevelsDeep>0 && nSize>0)
	{
		Data.sort(nLevelCol);
		
		int nLevelCnt=0;
		int nCurrLevel=Data.getDataRef(0,nLevelCol).toInt();
		Data.clearSelection();
		for (int i=0;i<nSize;i++)
		{
			Data.selectRow(i);
			if (nCurrLevel!=Data.getDataRef(i,nLevelCol).toInt())
			{
				nCurrLevel=Data.getDataRef(i,nLevelCol).toInt();
				nLevelCnt++;
				if (nLevelCnt>=nLevelsDeep)
				{
					Data.deleteUnSelectedRows();
					return true;
				}
			}
		}
	}
	return true;
}

bool HierarchicalData::GetChildNodes(const QString &strCode, DbRecordSet &Data, QString strSQLWhereFilter) const
{
	//TOFIX this could return too much data, is there any direct query to clear this list!
	//get all the descendants
	if(!GetDescendantsNodes(strCode, Data, strSQLWhereFilter))
		return false;

	//now filter the list to remove entries that have parents in this list
	int nCodeCol = Data.getColumnIdx(m_pTableKeyData.m_strFieldPrefix + "_CODE");
	Q_ASSERT(nCodeCol >= 0);

	//sort the data
	Data.sort(nCodeCol);

	int nCount = Data.getRowCount();
	for(int i=0; i<nCount-1; i++)
	{
		if(HierarchicalHelper::IsCodeParent( Data.getDataRef(i, nCodeCol).toString(),
			Data.getDataRef(i+1, nCodeCol).toString()))
		{
			Data.deleteRow(i+1);
			i --;
			nCount --;
		}
	}

	return true;
}



int HierarchicalData::CalculateSibling(const QString &strCode, int nParentID, QString strSQLWhereFilter) const
{
	// calculate sibling (inserting the node to the place defined by sort position)
	int nSibling = -1;

	DbRecordSet levelData;
	levelData.defineFromView(DbSqlTableView::getView(m_pTableKeyData.m_nViewID));

	//select count of the existing siblings positioned before this node
	QString strSQL = "SELECT COUNT(*) FROM " + m_pTableKeyData.m_strTableName;
	strSQL += " WHERE " + m_pTableKeyData.m_strFieldPrefix + "_PARENT";
	if(nParentID >= 0)
		strSQL += " = " + QString().sprintf("%d",nParentID);
	else
		strSQL += " IS NULL";

	strSQL += " AND " + m_pTableKeyData.m_strFieldPrefix + "_CODE < '" + strCode + "'";
	strSQL +=strSQLWhereFilter;

	Status status;
	DbSqlQuery query(status, m_pDbManager);
	if(!status.IsOK())
		return nSibling;

	query.Execute(status, strSQL);
	if(!status.IsOK())
		return nSibling;

	DbRecordSet rsResult;
	query.FetchData(rsResult);

	if(rsResult.getRowCount()>0)
		nSibling = rsResult.getDataRef(0,0).toInt();

	return nSibling;
}

int HierarchicalData::CalcParent(const QString &strCode, int &nParentLevel, QString strSQLWhereFilter) const
{
	int nParentID = -1;
	nParentLevel = -1;

	DbRecordSet Data;
	Data.defineFromView(DbSqlTableView::getView(m_pTableKeyData.m_nViewID));

	int nIdCol = Data.getColumnIdx(m_pTableKeyData.m_strFieldPrefix + "_ID");
	Q_ASSERT(nIdCol >= 0);
	int nCodeCol = Data.getColumnIdx(m_pTableKeyData.m_strFieldPrefix + "_CODE");
	Q_ASSERT(nCodeCol >= 0);
	int nLevelCol = Data.getColumnIdx(m_pTableKeyData.m_strFieldPrefix + "_LEVEL");
	Q_ASSERT(nLevelCol >= 0);

	if(!GetAncestorNodes(strCode, Data, strSQLWhereFilter))
		return -1;	//this is a root node, no parent

	//find longest ancestor code (eg. parent)
	int nMax = 0;
	int nIdx = -1;
	int nAncestors = Data.getRowCount();
	for(int j=0; j<nAncestors; j++)	{
		QString strCode1 = Data.getDataRef(j, nCodeCol).toString();
		if(strCode1.length() > nMax){
			nMax = strCode1.length();
			nIdx = j;
		}
	}

	// parent node found
	if(nIdx >= 0){
		// read parent info
		nParentID = Data.getDataRef(nIdx, nIdCol).toInt();
		nParentLevel = Data.getDataRef(nIdx, nLevelCol).toInt();
	}

	return nParentID;
}

QString HierarchicalData::GetSelectNames(const DbRecordSet &lstNodes) const
{
	QString strFields;

	int nCount = lstNodes.getColumnCount();
	for(int i=0; i<nCount; i++)
	{
		int nDataType;
		QString strField;//, strAlias, strTable;
		lstNodes.getColumn(i, nDataType, strField);//, strAlias, strTable);

		if(strField.right(9).toLower()=="_expanded") //BT: skip expaneded field->calculated
			continue;

		strFields += strField+",";
	}

	strFields.chop(1);
	return strFields;
}

int HierarchicalData::CalculateChildCount(const QString &strCode, QString strSQLWhereFilter) const
{
	DbRecordSet Data;
	if(!GetChildNodes(strCode, Data, strSQLWhereFilter))
		return 0;

	return Data.getRowCount();
}

int HierarchicalData::CalculateChildCount(int nRecordID, QString strSQLWhereFilter) const
{
	//select count of the existing siblings positioned before this node
	QString strSQL = "SELECT COUNT(*) "; 
	strSQL += " FROM " + m_pTableKeyData.m_strTableName;
	strSQL += " WHERE " + m_pTableKeyData.m_strFieldPrefix + "_PARENT";
	if(nRecordID >= 0)
		strSQL += " = " + QString().sprintf("%d",nRecordID);
	else
		strSQL += " IS NULL";
	strSQL +=strSQLWhereFilter;

	Status status;
	DbSqlQuery query(status, m_pDbManager);
	if(!status.IsOK()){
		Q_ASSERT_X(false, "HierarchicalData::GetCodeFromID", status.getErrorText().toLatin1());	
		return 0;
	}

	query.Execute(status, strSQL);
	if(!status.IsOK()){
		Q_ASSERT_X(false, "HierarchicalData::GetCodeFromID", status.getErrorText().toLatin1());	
		return 0;
	}

	DbRecordSet rsResult;
	query.FetchData(rsResult);

	if(rsResult.getRowCount()>0){
		return rsResult.getDataRef(0,0).toInt();
	}

	return 0;
}

bool HierarchicalData::GetCodeFromID(Status &status, int nRecordID, QString &strCode, QString strSQLWhereFilter)  const
{
	//select count of the existing siblings positioned before this node
	QString strSQL = "SELECT "; 
	strSQL += m_pTableKeyData.m_strFieldPrefix + "_CODE";
	strSQL += " FROM " + m_pTableKeyData.m_strTableName;
	strSQL += " WHERE " + m_pTableKeyData.m_strFieldPrefix + "_ID=";
	strSQL += QString().sprintf("%d",nRecordID);
	strSQL +=strSQLWhereFilter;

	DbSqlQuery query(status, m_pDbManager);
	if(!status.IsOK()){
		Q_ASSERT_X(false, "HierarchicalData::GetCodeFromID", status.getErrorText().toLatin1());	
		return false;
	}

	query.Execute(status, strSQL);
	if(!status.IsOK()){
		Q_ASSERT_X(false, "HierarchicalData::GetCodeFromID", status.getErrorText().toLatin1());	
		return false;
	}

	DbRecordSet rsResult;
	query.FetchData(rsResult);

	if(rsResult.getRowCount()>0){
		strCode = rsResult.getDataRef(0,0).toString();
		return true;
	}

	return false;
}

bool HierarchicalData::GetIDFromCode(Status &status, const QString &strCode, int &nRecordID,QString strSQLWhereFilter) const
{
	nRecordID = -1;

	//select count of the existing siblings positioned before this node
	QString strSQL = "SELECT "; 
	strSQL += m_pTableKeyData.m_strFieldPrefix + "_ID";
	strSQL += " FROM " + m_pTableKeyData.m_strTableName;
	strSQL += " WHERE " + m_pTableKeyData.m_strFieldPrefix + "_CODE=?";
	strSQL +=strSQLWhereFilter;

	DbSqlQuery query(status, m_pDbManager);
	if(!status.IsOK()){
		Q_ASSERT_X(false, "HierarchicalData::GetIDFromCode", status.getErrorText().toLatin1());	
		return false;
	}

	//Prepare statement for execution.
	query.Prepare(status, strSQL);
	if(!status.IsOK()){
		Q_ASSERT_X(false, "HierarchicalData::GetIDFromCode", status.getErrorText().toLatin1());	
		return false;
	}

	query.bindValue(0, strCode);
	query.ExecutePrepared(status);
	if(!status.IsOK()){
		Q_ASSERT_X(false, "HierarchicalData::GetIDFromCode", status.getErrorText().toLatin1());	
		return false;
	}

	DbRecordSet rsResult;
	query.FetchData(rsResult);

	if(rsResult.getRowCount()>0){
		nRecordID = rsResult.getDataRef(0,0).toInt();
		return true;
	}

	return false;
}



//B.T. 22.03.2013: when code is changed to new one: detach all childrens and check has_children flag for parents
//nNodeID = node ID (after code is changed)
//nNodeParentID = parent node of nNodeID (before code changed)

//B.T. 18.09.2013: change code on all children...so if parent is change get all childs with him...
//TOFIX: code update swing into..managers..
bool HierarchicalData::OnCodeChanged(Status &status, QString strSQLWhereFilter,QString strOldCode, QString strNewCode) const
{
	DbSqlQuery query(status, m_pDbManager);
	if(!status.IsOK())
		return false; //reserve connection failed

	//for given node, find all children below
	QString strSQL1;
	m_pDbManager->GetFunction()->PrepareHierarchySelect(strSQL1,strOldCode,m_pTableKeyData.m_strFieldPrefix,true);
	QString strSQL;

	int nLengthOldCode=strOldCode.size();

	strSQL = "UPDATE " + m_pTableKeyData.m_strTableName+" SET "+m_pTableKeyData.m_strFieldPrefix+ "_CODE =? || SUBSTRING("+m_pTableKeyData.m_strFieldPrefix+ "_CODE FROM "+QString::number(nLengthOldCode+1)+" FOR CHAR_LENGTH("+m_pTableKeyData.m_strFieldPrefix+ "_CODE))";
	strSQL += " WHERE "+strSQL1+strSQLWhereFilter; 

	query.Prepare(status,strSQL);
	if(!status.IsOK())
		return false;

	query.bindValue(0,strNewCode);
	query.bindValue(1,strOldCode);

	query.ExecutePrepared(status);
	if(!status.IsOK())
		return false;
	
	return true;
}


//B.T. 22.03.2013: based on parent nodes, update their children with PARENT_ID
bool HierarchicalData::UpdateChildren(Status &status, DbRecordSet &lstParentNodes,QString strSQLWhereFilter) const
{
	DbSqlQuery query(status, m_pDbManager);
	if(!status.IsOK())
		return false; //reserve connection failed

	int nIdCol = lstParentNodes.getColumnIdx(m_pTableKeyData.m_strFieldPrefix + "_ID");
	Q_ASSERT(nIdCol >= 0);
	int nCodeCol = lstParentNodes.getColumnIdx(m_pTableKeyData.m_strFieldPrefix + "_CODE");
	Q_ASSERT(nCodeCol >= 0);
	int nLevelCol = lstParentNodes.getColumnIdx(m_pTableKeyData.m_strFieldPrefix + "_LEVEL");
	Q_ASSERT(nLevelCol >= 0);

	//for each inserted node update the children
	int nCount = lstParentNodes.getRowCount();
	for(int i=0; i<nCount; i++)
	{
		int nID = 0;
		lstParentNodes.getData(i,nIdCol, nID);
		QString strCode;
		lstParentNodes.getData(i,nCodeCol, strCode);
		int nLevel = 0;
		lstParentNodes.getData(i,nLevelCol, nLevel);

		//for given node, find first children on level below, but note that code can skip: e.g.: B. -> B.XY.AB.
		QString strSQL1;
		m_pDbManager->GetFunction()->PrepareHierarchySelect(strSQL1,strCode,m_pTableKeyData.m_strFieldPrefix,true);
		QString strSQL;

		strSQL = "SELECT FIRST 1 "+m_pTableKeyData.m_strFieldPrefix+ "_CODE FROM " + m_pTableKeyData.m_strTableName;
		strSQL += " WHERE "+strSQL1+strSQLWhereFilter; 
		strSQL += " ORDER BY "+m_pTableKeyData.m_strFieldPrefix + "_CODE ASC";
		 
		query.Prepare(status, strSQL);
		if(!status.IsOK()){
			Q_ASSERT_X(false, "HierarchicalData::UpdateChildren", status.getErrorText().toLatin1());	
			return false;
		}
		query.bindValue(0, strCode);
		query.ExecutePrepared(status);
		if(!status.IsOK())
		{
			return false;
		}

		QString strFirstChildCode="";
		if (query.next())
		{
			strFirstChildCode=query.value(0).toString();
		}

		//means there are childrens:
		if (!strFirstChildCode.isEmpty())
		{
			//determine children level:
			int nLevel=strFirstChildCode.split(".",QString::SkipEmptyParts).count()-1;
			if (nLevel<0)
				nLevel=0;

			//now execute update of all childrens on same level as first children found
			strSQL = "UPDATE " + m_pTableKeyData.m_strTableName;
			strSQL += " SET " + m_pTableKeyData.m_strFieldPrefix + "_PARENT = " + QString::number(nID);
			strSQL += " WHERE "+ m_pTableKeyData.m_strFieldPrefix + "_LEVEL = " + QString::number(nLevel)+" AND "+strSQL1+strSQLWhereFilter; 

			query.Prepare(status, strSQL);
			if(!status.IsOK()){
				Q_ASSERT_X(false, "HierarchicalData::UpdateChildren", status.getErrorText().toLatin1());	
				return false;
			}
			query.bindValue(0, strCode);
			query.ExecutePrepared(status);
			if(!status.IsOK())
			{
				return false;
			}
		}
	}

		return true;
}


//B.T. 22.03.2013: based on child nodes, find their parents and update their PARENT_ID, if not found->set null
//Note: only first child is enough on same level...
bool HierarchicalData::UpdateParent(Status &status, DbRecordSet &lstChildNodes,QString strSQLWhereFilter) const
{

	DbSqlQuery query(status, m_pDbManager);
	if(!status.IsOK())
		return false; //reserve connection failed

	int nCodeCol = lstChildNodes.getColumnIdx(m_pTableKeyData.m_strFieldPrefix + "_CODE");
	Q_ASSERT(nCodeCol >= 0);
	int nParentCol = lstChildNodes.getColumnIdx(m_pTableKeyData.m_strFieldPrefix + "_PARENT");
	Q_ASSERT(nParentCol >= 0);


	//for each inserted node update the children
	int nCount = lstChildNodes.getRowCount();
	for(int i=0; i<nCount; i++)
	{
		QString strCode;
		lstChildNodes.getData(i,nCodeCol, strCode);
		int nOldParent = 0;
		lstChildNodes.getData(i,nParentCol, nOldParent);

		//for given node, find parent, this parent will be same for all other on same level from old parent
		QString strSQL1;
		QString strSQL;
		m_pDbManager->GetFunction()->PrepareGetParentCode(strSQL1,strCode,m_pTableKeyData.m_strFieldPrefix,m_pTableKeyData.m_strTableName);
		strSQL =strSQL1+strSQLWhereFilter;
		strSQL += " ORDER BY "+m_pTableKeyData.m_strFieldPrefix+"_CODE DESC";

		query.Prepare(status, strSQL);
		if(!status.IsOK()){
			Q_ASSERT_X(false, "HierarchicalData::UpdateChildren", status.getErrorText().toLatin1());	
			return false;
		}
		query.bindValue(0, strCode);
		query.ExecutePrepared(status);
		if(!status.IsOK())
		{
			return false;
		}

		int nParentID=0;
		if (query.next())
		{
			nParentID=query.value(0).toInt();
		}


		//now execute update of all childrens on same level as first children found
		strSQL = "UPDATE " + m_pTableKeyData.m_strTableName;
		if (nParentID>0)
			strSQL += " SET " + m_pTableKeyData.m_strFieldPrefix + "_PARENT = " + QString::number(nParentID);
		else
			strSQL += " SET " + m_pTableKeyData.m_strFieldPrefix + "_PARENT = NULL ";
		strSQL += " WHERE "+ m_pTableKeyData.m_strFieldPrefix + "_PARENT = " + QString::number(nOldParent); //update all siblings for old parent

		query.Execute(status, strSQL);
		if(!status.IsOK()){
			Q_ASSERT_X(false, "HierarchicalData::UpdateParent", status.getErrorText().toLatin1());	
			return false;
		}

		//set has children of parent
		if (nParentID>0)
		{
			UpdateHasChildren(status,nParentID,strSQLWhereFilter,true);
			if(!status.IsOK())
				return false;
		}

	}
	
	return true;
}
//bSetHasChildrenNoCheck if false then it will calculate ih there are any children, else it will just set to 1
bool HierarchicalData::UpdateHasChildren(Status &status, int nParentID,QString strSQLWhereFilter,int bSetHasChildrenNoCheck) const
{
	if (nParentID<=0)
		return false;

	DbSqlQuery query(status, m_pDbManager);
	if(!status.IsOK())
		return false; //reserve connection failed

	if (bSetHasChildrenNoCheck)
	{
		//update has children flag:
		QString strSQL = "UPDATE " + m_pTableKeyData.m_strTableName;
		strSQL += " SET " + m_pTableKeyData.m_strFieldPrefix + "_HASCHILDREN = 1";
		strSQL += " WHERE " + m_pTableKeyData.m_strFieldPrefix + "_ID";
		strSQL += " = " + QString::number(nParentID);
		query.Execute(status,strSQL);
		if(!status.IsOK())
			return false;
	}
	else
	{

	
		//select count of the existing siblings positioned before this node
		QString strSQL = "SELECT "; 
		strSQL += m_pTableKeyData.m_strFieldPrefix + "_CODE";
		strSQL += " FROM " + m_pTableKeyData.m_strTableName;
		strSQL += " WHERE " + m_pTableKeyData.m_strFieldPrefix + "_ID="+QString::number(nParentID);
		strSQL +=strSQLWhereFilter;
		query.Execute(status,strSQL);
		if(!status.IsOK())
			return false;

		QString strCode="";
		if (query.next())
		{
			strCode=query.value(0).toString();
		}

		//get child count:
		QString strSQL1;
		m_pDbManager->GetFunction()->PrepareHierarchySelect(strSQL1,strCode,m_pTableKeyData.m_strFieldPrefix,true);
		strSQL = "SELECT COUNT(*) FROM "+ m_pTableKeyData.m_strTableName+" WHERE ";
		strSQL += strSQL1;
		strSQL +=strSQLWhereFilter;
		query.Prepare(status,strSQL);
		if(!status.IsOK())
			return false;

		query.bindValue(0, strCode);
		query.ExecutePrepared(status);
		if(!status.IsOK())
			return false;

		int nHasChildren=0;
		if (query.next())
		{
			nHasChildren=query.value(0).toInt();
		}

		//update has children flag:
		strSQL = "UPDATE " + m_pTableKeyData.m_strTableName;
		strSQL += " SET " + m_pTableKeyData.m_strFieldPrefix + "_HASCHILDREN = "+QString::number(nHasChildren);
		strSQL += " WHERE " + m_pTableKeyData.m_strFieldPrefix + "_ID";
		strSQL += " = " + QString::number(nParentID);
		query.Execute(status,strSQL);
		if(!status.IsOK())
			return false;

	}
	return true;
}