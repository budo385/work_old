#ifndef COMMGRIDVIEWTABLEHELPER_SERVER_H
#define COMMGRIDVIEWTABLEHELPER_SERVER_H

#include <QObject>
#include "bus_core/bus_core/commgridviewtablehelper.h"

class CommGridViewTableHelper_Server : public CommGridViewTableHelper
{
	Q_OBJECT

public:
	CommGridViewTableHelper_Server(QObject *parent=0);
	~CommGridViewTableHelper_Server();

	int GetLoggedPersonID();
	QString GetLoggedPersonInitials();
	void InitializeHelper();

private:
	
};

#endif // COMMGRIDVIEWTABLEHELPER_SERVER_H
