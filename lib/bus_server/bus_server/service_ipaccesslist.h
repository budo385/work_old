#ifndef SERIVCE_IPACCESSLIST_H
#define SERIVCE_IPACCESSLIST_H


#include "systemservice.h"


/*!
	\class Serivce_IPAccessList
	\brief Manages IP access list
	\ingroup Bus_System

	Anyone can store & read IP acess list (chain) by unique name.
	Unique name is chosen because this feature will be used by App. servers, they need hardcore name

*/
class Serivce_IPAccessList : public SystemService
{
	
public:

	void Read(Status &pStatus,QString strListName,DbRecordSet& lstRecords);
	void Write(Status &pStatus,QString strListName,DbRecordSet& lstRecords);
	void Delete(Status &pStatus,QString strListName);

    
};

#endif // SERIVCE_IPACCESSLIST_H
