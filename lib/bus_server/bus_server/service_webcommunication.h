#ifndef SERVICE_WEBCOMMUNICATION_H
#define SERVICE_WEBCOMMUNICATION_H

#include "bus_interface/bus_interface/interface_webcommunication.h"
#include "businessservice.h"

class Service_WebCommunication : public Interface_WebCommunication, public BusinessService
{
public:
	void ReadContactViews(Status &Ret_pStatus, DbRecordSet &Ret_Views);
	void ReadContactViewData(Status &Ret_pStatus, int nContactID, DbRecordSet &Ret_Data, int &Ret_nTotalCount,int nViewID, int nUseOneColumn, int nUseFromTo, QDateTime datFrom, QDateTime datTo, int nPrevNext, int nRangeSelectorSelector, int nMaxRecords,QString &Ret_strFrom, QString &Ret_strTo);
	void ReadUserViews(Status &Ret_pStatus, DbRecordSet &Ret_Views);
	void ReadUserViewData(Status &Ret_pStatus, int nUserID, DbRecordSet &Ret_Data, int &Ret_nTotalCount,int nViewID, int nUseOneColumn, int nUseFromTo, QDateTime datFrom, QDateTime datTo, int nPrevNext, int nRangeSelectorSelector, int nMaxRecords,QString &Ret_strFrom, QString &Ret_strTo);
	void ReadProjectViews(Status &Ret_pStatus, DbRecordSet &Ret_Views);
	void ReadProjectViewData(Status &Ret_pStatus, int nProjectID, DbRecordSet &Ret_Data, int &Ret_nTotalCount,int nViewID, int nUseOneColumn, int nUseFromTo, QDateTime datFrom, QDateTime datTo, int nPrevNext, int nRangeSelectorSelector, int nMaxRecords,QString &Ret_strFrom, QString &Ret_strTo);
	
	//user related stuff:
	void ReadUserDocuments(Status &Ret_pStatus, int &Ret_nTotalCount,DbRecordSet &Ret_Documents, int nUserID,int nDocType=-1, int nFromN=-1, int nToN=-1);
	void ReadUserEmails(Status &Ret_pStatus, int &Ret_nTotalCount, DbRecordSet &Ret_Emails, int nUserID, int nFromN=-1, int nToN=-1, int nSortOrder=-1, QString strEmailAcc="",QString strEmailSender="", int nMailBox=0);
	void ReadCommEntityCount(Status &Ret_pStatus, int nUserID, int &Ret_nFileDocCount, int &Ret_nWebSiteCount, int &Ret_nNotesCount,int &Ret_nEmailCount);


private:


};

#endif // SERVICE_WEBCOMMUNICATION_H
