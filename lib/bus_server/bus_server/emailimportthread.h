#ifndef EMAILIMPORTTHREAD_H
#define EMAILIMPORTTHREAD_H

#include "common/common/backgroundtaskinterface.h"
#include <QTimer>
#include "common/common/dbrecordset.h"
#include "common/common/status.h"
#include "common/common/threadsync.h"


//Separate Thread object
class EmailImportThread : public QObject
{
	Q_OBJECT
	//int m_nIniLstRow;
public:
	EmailImportThread(){};//for dummies
	EmailImportThread(ThreadSynchronizer *pThreadSync,DbRecordSet &lstEmailAccounts);
	~EmailImportThread();
	
	static QString	GenerateAccUID();
	void			ImportMessageFromMime(Status &status, int nPersonID, int nIsOutgoing, QString strFrom, QString strTo, QString strSubject, QString strMime, QString strFullFrom, QString strCC, QString strBCC);
	void			ImportEmails(Status status, DbRecordSet &lstData, int nPersonID, int &nSkippedOrReplacedCnt, bool bAddDupsAsNewRow, bool bSkipExisting = false);
	QDateTime		GetLastActivity(){QReadLocker lck(&m_Lck_ActivityDate); return m_datlastActivity;}
	static QString	GetFileNameFromContentType(const QString &strContentType);

signals:
	void Stopped();
	

public slots:
	void SetThreadActive(){QWriteLocker lck(&m_Lck_ActivityDate); m_datlastActivity=QDateTime::currentDateTime();};
	void ExecuteTask();
	void Destroy();
	void GetEmailAccounts(DbRecordSet &lstEmailAccounts){lstEmailAccounts=m_lstEmailAccounts;};

private:
	void DoActionPop3();
	bool HandleMailAccount(int nIdx);
	void WriteEmailAttachments(Status &Ret_pStatus,int nEmailID, DbRecordSet &Attachments);
	bool HandlePop3Account(const QString &strHost, int nPort, bool bUseSSL, bool bUseSTLS, const QString &strUSER, const QString &strPass, int nPersonID, QDateTime &dtLastCheck, bool bKnownSenderOnly, bool bFullScan, QString strLogInfo, QString &strLastErrorEmailUID, QString &strAccErrorLog, QString strEmail);
	bool HandleImapAccount(const QString &strHost, int nPort, bool bUseSSL, bool bUseSTLS, const QString &strUSER, const QString &strPass, int nPersonID, QDateTime &dtLastCheck, bool bKnownSenderOnly, QString strLogInfo, QString &strLastErrorEmailUID, QString &strAccErrorLog, QString strEmail);
	
	
	QReadWriteLock m_Lck_ActivityDate;

	ThreadSynchronizer	*m_ThreadSync;
	DbRecordSet			m_lstEmailAccounts;
	QDateTime			m_datlastActivity;
};


#endif // EMAILIMPORTTHREAD_H
