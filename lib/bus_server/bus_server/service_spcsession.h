#ifndef SERVICE_SPCSESSION_H
#define SERVICE_SPCSESSION_H

#include "bus_interface/bus_interface/interface_spcsession.h"
#include "businessservice.h"

class Service_SpcSession : public Interface_SpcSession, public BusinessService
{
public:
	void Login(Status& Ret_Status, QString& RetOut_strSessionID,QString strUserName, QString strAuthToken,QString strClientNonce,int nProgCode=0, QString strProgVer="", QString strClientID="", QString strPlatform="",int nClientTimeZoneOffsetMinutes=0);
	void Logout(Status &Ret_Status,QString strSessionID);
};

#endif // SERVICE_SPCSESSION_H
