#include "service_webserver.h"
#include "common/common/authenticator.h"
#include "db/db/dbconnectionreserver.h"

#include "bus_core/bus_core/usersessionmanager.h"
extern UserSessionManager	*g_UserSessionManager;
#include "privateserviceset.h"
extern PrivateServiceSet* g_PrivateServiceSet; 
#include "businessserviceset.h"
extern BusinessServiceSet* g_BusinessServiceSet;
#include "bus_core/bus_core/servercontrolabstract.h"
extern ServerControlAbstract *g_AppServer;

void Service_WebServer::Logout(Status &pStatus,QString strSessionID)
{
	g_UserSessionManager->DeleteSession(pStatus,strSessionID);
}

void Service_WebServer::Login(Status& Ret_Status, QString& RetOut_strSessionID,int& Ret_nContactID,int& Ret_nPersonID,QString strUserName, QString strAuthToken,QString strClientNonce,int nProgCode, QString strProgVer, QString strClientID, QString strPlatform, int nClientTimeZoneOffsetMinutes)
{
	//--------------------------------------------------------
	// AUTHENTICATE USER
	//--------------------------------------------------------

	//load from DB username, password, group, 
	DbRecordSet rowUserData;
	g_PrivateServiceSet->CoreUser->GetUserByUserName(Ret_Status,strUserName,rowUserData);
	if(!Ret_Status.IsOK())return;

	//authenticate user:

	//is user exists:
	if(rowUserData.getRowCount()!=1)
	{Ret_Status.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_WRONG_PASS_USER);return;}

	//is user active (first test core then bus):
	if(rowUserData.getDataRef(0,"CUSR_ACTIVE_FLAG").toInt()!=1)
	{Ret_Status.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_NO_SUBSCRIPTION);return;}
	else if(rowUserData.getDataRef(0,"BPER_ID").toInt()!=0 && rowUserData.getDataRef(0,"BPER_ACTIVE_FLAG").toInt()==0)
	{Ret_Status.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_NO_SUBSCRIPTION);return;}

	QByteArray bytePassword;
	//B.T. 2013.02.05: if non SPC user then as usual, if SPC then convert existing password to unbase64:username hashs
	if (rowUserData.getDataRef(0,"BPER_FPERSON_SEQ").toInt()==0)
	{
		//test password:
		bytePassword=rowUserData.getDataRef(0,"CUSR_PASSWORD").toByteArray();
	}
	else
	{
		bytePassword=QByteArray::fromBase64(rowUserData.getDataRef(0,"CUSR_PASSWORD").toByteArray());
		bytePassword=Authenticator::GeneratePassword(strUserName,bytePassword);
	}


	if(!Authenticator::AuthenticateFromWebService(strClientNonce,strAuthToken,strUserName,bytePassword))
	{Ret_Status.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_WRONG_PASS_USER);return;}

	g_UserSessionManager->CreateSessionForWebService(Ret_Status,RetOut_strSessionID,rowUserData,nClientTimeZoneOffsetMinutes);

	Ret_nPersonID=rowUserData.getDataRef(0,"BPER_ID").toInt();
	Ret_nContactID=rowUserData.getDataRef(0,"BPER_CONTACT_ID").toInt();
}


//--------------------------------------------------------
// CHANGE PASSWORD: newPass is base64, auth token & client nonce are generated from old password.
//--------------------------------------------------------
void Service_WebServer::ChangePassword(Status &Ret_Status,QString& RetOut_strSessionID, QString strUsername, QString strAuthToken,QString strClientNonce,QByteArray newPass, int nClientTimeZoneOffsetMinutes)
{

	DbRecordSet rowUserData;
	DbConnectionReserver conn(Ret_Status,GetDbManager()); //reserve global connection
	if(!Ret_Status.IsOK())return;

	//--------------------------------------------------------
	// AUTHENTICATE USER
	//--------------------------------------------------------

	//load from DB username, password, group, 
	g_PrivateServiceSet->CoreUser->GetUserByUserName(Ret_Status,strUsername,rowUserData);
	if(!Ret_Status.IsOK())return;

	//is user exists:
	if(rowUserData.getRowCount()!=1)
	{Ret_Status.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_WRONG_PASS_USER);return;}

	//is user active:
	if(rowUserData.getDataRef(0,"CUSR_ACTIVE_FLAG").toInt()!=1)
	{Ret_Status.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_NO_ACTIVE);return;}


	QByteArray bytePassword;
	//B.T. 2013.02.05: if non SPC user then as usual, if SPC then convert existing password to unbase64:username hashs
	if (rowUserData.getDataRef(0,"BPER_FPERSON_SEQ").toInt()==0)
	{
		//test password:
		bytePassword=rowUserData.getDataRef(0,"CUSR_PASSWORD").toByteArray();
		QByteArray newPassClean=QByteArray::fromBase64(newPass);
		newPass=Authenticator::GeneratePassword(strUsername,newPassClean);
	}
	else
	{
		bytePassword=QByteArray::fromBase64(rowUserData.getDataRef(0,"CUSR_PASSWORD").toByteArray());
		bytePassword=Authenticator::GeneratePassword(strUsername,bytePassword);
	}
	if(!Authenticator::AuthenticateFromWebService(strClientNonce,strAuthToken,strUsername,bytePassword))
	{Ret_Status.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_WRONG_PASS_USER);return;}


	//--------------------------------------------------------
	// SET NEW PASS
	//--------------------------------------------------------
	DbSqlQuery query(Ret_Status,GetDbManager());
	if(!Ret_Status.IsOK())return;


	QString strSQL="UPDATE CORE_USER SET CUSR_PASSWORD=? WHERE CUSR_USERNAME=?";
	query.Prepare(Ret_Status,strSQL);
	if(!Ret_Status.IsOK())
	{
		return;
	}
	query.bindValue(0,newPass);
	query.bindValue(1,strUsername);
	query.ExecutePrepared(Ret_Status);
	if(!Ret_Status.IsOK())
	{
		return;
	}

	if (Ret_Status.IsOK())
	{
		g_UserSessionManager->DeleteSession(Ret_Status,RetOut_strSessionID);
		g_UserSessionManager->CreateSessionForWebService(Ret_Status,RetOut_strSessionID,rowUserData,nClientTimeZoneOffsetMinutes);
	}

}

//reload web pages: give instruction to appserver..
void Service_WebServer::ReloadWebPages(Status &Ret_Status)
{
	g_AppServer->ReloadWebPages(Ret_Status);
}

void Service_WebServer::GetServerTime( Status &Ret_Status, QDateTime& Ret_datCurrentServerTime, QDateTime& Ret_datTimeNextTest, int& Ret_nActiveClientSocketCnt)
{
	Ret_datCurrentServerTime=QDateTime::currentDateTime();
	Ret_datTimeNextTest =QDateTime::fromString(g_AppServer->GetIniFile()->m_strNextTestTime,"yyyyMMddHHmm");
	HTTPServer *pServer = g_AppServer->GetHttpServer();
	Ret_nActiveClientSocketCnt = pServer->GetActiveSocketsCount();
}

void Service_WebServer::SetNextTestTime( Status &Ret_Status, QDateTime datSetTimeNextTest )
{
	QString strDateTimeString=datSetTimeNextTest.toString("yyyyMMddHHmm");
	g_AppServer->GetIniFile()->m_strNextTestTime=strDateTimeString;
	g_AppServer->SaveSettings();
}