#ifndef SERVICE_WEBSERVER_H
#define SERVICE_WEBSERVER_H



#include "bus_interface/bus_interface/interface_webserver.h"
#include "businessservice.h"

class Service_WebServer : public Interface_WebServer, public BusinessService
{
public:
	void Login(Status& Ret_Status, QString& RetOut_strSessionID,int& Ret_nContactID,int& Ret_nPersonID,QString strUserName, QString strAuthToken,QString strClientNonce,int nProgCode=0, QString strProgVer="", QString strClientID="", QString strPlatform="",int nClientTimeZoneOffsetMinutes=0);
	void Logout(Status &Ret_Status,QString strSessionID);
	void ChangePassword(Status &Ret_Status,QString& RetOut_strSessionID, QString strUsername, QString strAuthToken,QString strClientNonce,QByteArray newPass, int nClientTimeZoneOffsetMinutes);
	void ReloadWebPages(Status &Ret_Status);
	void GetServerTime(Status &Ret_Status, QDateTime& Ret_datCurrentServerTime, QDateTime& Ret_datTimeNextTest, int& Ret_nActiveClientSocketCnt);
	void SetNextTestTime(Status &Ret_Status, QDateTime datSetTimeNextTest);

};

#endif // SERVICE_WEBSERVER_H
