#ifndef BUSINESSSERVICENESTED_H
#define BUSINESSSERVICENESTED_H


#include "privateserviceset.h"
#include "systemserviceset.h"
#include "businessserviceset.h"


extern SystemServiceSet* g_SystemServiceSet;	
extern PrivateServiceSet* g_PrivateServiceSet; 
extern BusinessServiceSet* g_BusinessServiceSet;


#endif //BUSINESSSERVICENESTED_H
