#ifndef SERVICE_BUSEVENTLOG_H
#define SERVICE_BUSEVENTLOG_H

#include "bus_interface/bus_interface/interface_buseventlog.h"
#include "businessservice.h"

/*!
\class Service_BusEventLog
\brief Access to CORE_EVENT_LOG table in db.
\ingroup Bus_System

Provides methods for reading, inserting and deleting events to event log table.
For managing log table, this is private service, it is isolated as System service 
so it can be accessible by global app. server manager objects such as UserSession etc...

*/

class Service_BusEventLog : public Interface_BusEventLog, public BusinessService
{
	virtual ~Service_BusEventLog(){};

	//Add an Event.
	void AddEvent(Status &pStatus, int &EventID, int EventCode, QString EventText, int EvType, 
		int EvCategoryID, QDateTime EventDateTime = QDateTime());
	//Delete event.
	void DeleteEventByEventID(Status &pStatus, int EventID); 
	void DeleteEventByDateTime(Status &pStatus, QDateTime DateOlderThenGivenDatetime);
	//Get event index list.
	void GetEventRecordsetByType(Status &pStatus, int EvType, DbRecordSet &EventRecordSet);
	void GetEventRecordsetByCategory(Status &pStatus, int EvCategoryID, DbRecordSet &EventRecordSet);
	void GetEventRecordsetByDateTime(Status &pStatus, QDateTime BeginTime, QDateTime EndTime, DbRecordSet &EventRecordSet);

};

#endif // SERVICE_BUSEVENTLOG_H
