#include "loadbalancer.h"



/*!
	Init LBO, 
	
	\param bActive					only stores flag indicating that LBO is running on this App. server (used by session manager)
	\param strCurrentIPAddress		IP:port of machine on which LBO process resides
	\param nLBOListExpire			(sec) in which LBO list will be refreshed from DB table

*/
LoadBalancer::LoadBalancer(bool bActive,SystemServiceSet* pServices,QString strCurrentIPAddress, int nLBOListExpire)
{
	m_Services=pServices;
	m_strCurrentIPAddress=strCurrentIPAddress;
	m_nLBOListExpireTime=nLBOListExpire;
	m_datLastLBOUpdate=QDateTime(QDate(1900,1,1),QTime(0,0)); //init to boogie time (01.01.1900) for first, thus forcing refresh;
	m_bActive=bActive;	//set if active
}




/*!
	Returns IP:port of the server with min load, if not found, error or, return empty string

	\param bSkipCurrent		- if true, then from list will be removed current app. server/lbo

*/
QString LoadBalancer::GetServerWithMinLoad(bool bSkipCurrent)
{
	//if(!m_bActive) return ""; //if inactive return;

	DbRecordSet lstLBO;
	GetLBOList(lstLBO,bSkipCurrent);

	if(lstLBO.getRowCount()>0)
		return lstLBO.getDataRef(0,1).toString();
	else
		return "";

}

/*!
	Returns LBO list, if expired, refreshes from DB

	\param lstLBO			- returns list of all servers in system order by load factor
	\param bSkipCurrent		- if true, then from list will be removed current app. server/lbo

*/
void LoadBalancer::GetLBOList(DbRecordSet &lstLBO,bool bSkipCurrent)
{

	QWriteLocker lock(&m_LBORWLock);

	//if(!m_bActive) return ; //if inactive return;

	lstLBO.destroy();
	Status err;

	//IF EXPIRED, RELOAD LIST (lock access):
	if(m_datLastLBOUpdate.addSecs(m_nLBOListExpireTime)<QDateTime::currentDateTime())
	{
		//m_LBORWLock.lockForWrite();

		//load server data from DB:
		m_Services->AppSrvSession->ReadLBOList(err,m_lstLBO);
		if(!err.IsOK()){return;}

		//calculate load factor (CURR/MAX):
		int nSize=m_lstLBO.getRowCount();
		for(int i=0;i<nSize;++i)
		{
			Q_ASSERT(m_lstLBO.getDataRef(i,3).toInt()); //if 0 assert!!
			double dblLoadFactor;
			if(m_lstLBO.getDataRef(i,3).toInt()!=0)
			{
				double x=m_lstLBO.getDataRef(i,2).toDouble()+1;
				double y=m_lstLBO.getDataRef(i,3).toDouble();
				dblLoadFactor=x/y;
			}
			else
				dblLoadFactor=1;

			m_lstLBO.setData(i,4,dblLoadFactor);
		}
		m_lstLBO.sort(4);
		m_datLastLBOUpdate=QDateTime::currentDateTime();//set new time
		//m_LBORWLock.unlock();
	}


	//mutex read lock:
	//m_LBORWLock.lockForRead();
	lstLBO=m_lstLBO;
	//m_LBORWLock.unlock();

	//remove current 
	if(bSkipCurrent)
	{
		lstLBO.find(lstLBO.getColumnIdx("COAS_IP_ADDRESS"),m_strCurrentIPAddress);
		lstLBO.deleteSelectedRows();
	}
}


/*!
	Gets server with lowest load if that server is our server err=0, else err=REDIRECT

	\param err			- 0 if no redirection needed, else REDIRECT (ErrorText contains IP:port info)
*/
void LoadBalancer::IsRedirectionNeeded(Status &err)
{

	//if(!m_bActive) return; //if inactive return;

	QString ip=GetServerWithMinLoad(false);
	if(ip==m_strCurrentIPAddress)
	{ 
		err.setError(0);
		return;
	}
	else
	{
		err.setError(StatusCodeSet::ERR_SYSTEM_REDIRECT,ip);
		return;

	}

}