#ifndef SERVICE_VOICECALLCENTER_H
#define SERVICE_VOICECALLCENTER_H

#include <QtCore>
#include "common/common/status.h"

#include "bus_interface/bus_interface/interface_voicecallcenter.h"
#include "businessservice.h"

/*!
    \class Service_VoiceCallCenter
    \brief Service class to store data on voice calls
    \ingroup Bus_Interface_Collection
		
*/

class Service_VoiceCallCenter : public Interface_VoiceCallCenter, public BusinessService
{
public:
	virtual ~Service_VoiceCallCenter(){};

	void ReadCall(Status &Ret_pStatus, int nVoiceCallID,DbRecordSet &Ret_CallData, DbRecordSet &Ret_UAR,DbRecordSet &Ret_GAR);
	void WriteCall(Status &Ret_pStatus, DbRecordSet &RetOut_CallData, QString strLockRes,DbRecordSet &RetOut_ScheduleTask,DbRecordSet &RetOut_UAR,DbRecordSet &RetOut_GAR);
	void ListCalls(Status &Ret_pStatus, DbRecordSet &RetOut_lstCallData, QDate &datDate, int nOwnerID);
	void DeleteCalls(Status &Ret_pStatus, DbRecordSet &RetOut_lstIDs);
};


#endif //SERVICE_VoiceCallCenter_H
