#include "service_spcsession.h"
#include "common/common/authenticator.h"
#include "db/db/dbconnectionreserver.h"

#include "bus_core/bus_core/usersessionmanager.h"
extern UserSessionManager	*g_UserSessionManager;
#include "privateserviceset.h"
extern PrivateServiceSet* g_PrivateServiceSet; 
#include "businessserviceset.h"
extern BusinessServiceSet* g_BusinessServiceSet;
#include "bus_core/bus_core/servercontrolabstract.h"
extern ServerControlAbstract *g_AppServer;

void Service_SpcSession::Logout(Status &pStatus,QString strSessionID)
{
	g_UserSessionManager->DeleteSession(pStatus,strSessionID);
}

void Service_SpcSession::Login(Status& Ret_Status, QString& RetOut_strSessionID,QString strUserName, QString strAuthToken,QString strClientNonce,int nProgCode, QString strProgVer, QString strClientID, QString strPlatform, int nClientTimeZoneOffsetMinutes)
{
	//--------------------------------------------------------
	// AUTHENTICATE USER
	//--------------------------------------------------------

	//load from DB username, password, group, 
	DbRecordSet rowUserData;
	g_PrivateServiceSet->CoreUser->GetUserByUserName(Ret_Status,strUserName,rowUserData);
	if(!Ret_Status.IsOK())return;

	//authenticate user:

	//is user exists:
	if(rowUserData.getRowCount()!=1)
	{Ret_Status.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_WRONG_PASS_USER);return;}

	//is user active (first test core then bus):
	if(rowUserData.getDataRef(0,"CUSR_ACTIVE_FLAG").toInt()!=1)
	{Ret_Status.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_NO_ACTIVE);return;}
	else if(rowUserData.getDataRef(0,"BPER_ID").toInt()!=0 && rowUserData.getDataRef(0,"BPER_ACTIVE_FLAG").toInt()==0)
	{Ret_Status.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_NO_ACTIVE);return;}

	QByteArray bytePassword;
	//B.T. 2013.02.05: if non SPC user then as usual, if SPC then convert existing password to unbase64:username hashs
	if (rowUserData.getDataRef(0,"BPER_FPERSON_SEQ").toInt()==0)
	{
		//test password:
		bytePassword=rowUserData.getDataRef(0,"CUSR_PASSWORD").toByteArray();
	}
	else
	{
		bytePassword=QByteArray::fromBase64(rowUserData.getDataRef(0,"CUSR_PASSWORD").toByteArray());
		bytePassword=Authenticator::GeneratePassword(strUserName,bytePassword);
	}


	if(!Authenticator::AuthenticateFromWebService(strClientNonce,strAuthToken,strUserName,bytePassword))
	{Ret_Status.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_WRONG_PASS_USER);return;}

	g_UserSessionManager->CreateSessionForWebService(Ret_Status,RetOut_strSessionID,rowUserData,nClientTimeZoneOffsetMinutes);

}

