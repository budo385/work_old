#ifndef SERVICE_MODULELICENSE_H
#define SERVICE_MODULELICENSE_H

#include "bus_interface/bus_interface/interface_modulelicense.h"
#include "businessservice.h"

/*!
    \class Service_ModuleLicense
    \brief Service class for a BO to query client's access rights (based on keyfile)
    \ingroup Bus_Interface_Collection
		
*/

class Service_ModuleLicense : public Interface_ModuleLicense, public BusinessService
{
public:
	virtual ~Service_ModuleLicense(){};

	void GetAllModuleLicenseData(Status& Ret_pStatus, QString& strModuleCode, QString& strMLIID,DbRecordSet& RetOut_pList,QString& RetOut_strUserName, int& RetOut_nLicenseID, QString &RetOut_strReportLine1, QString &RetOut_strReportLine2, QString &Ret_strCustomerID,int &Ret_nCustomSolutionID);
	void GetFPList(Status& Ret_pStatus, QString& strModuleCode, QString& strMLIID, DbRecordSet& RetOut_pList);
	void GetLicenseInfo(Status& Ret_pStatus, QString& RetOut_strUserName, int& RetOut_nLicenseID, QString &RetOut_strReportLine1, QString &RetOut_strReportLine2, QString &Ret_strCustomerID,int &Ret_nCustomSolutionID);
};


#endif //SERVICE_MODULELICENSE_H
