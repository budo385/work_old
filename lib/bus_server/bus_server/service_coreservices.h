#ifndef SERVICE_CORESERVICES_H
#define SERVICE_CORESERVICES_H

#include "businessservice.h"
#include "bus_interface/bus_interface/interface_coreservices.h"


/*!
	\class Service_CoreServices
	\brief Access to LBO and other core services
	\ingroup Bus_System

*/
class Service_CoreServices : public Interface_CoreServices, public BusinessService
{

public:
	
	void ReadLBOList(Status &Ret_pStatus, DbRecordSet &Ret_Data);
	void CheckVersion(Status& Ret_Status,QString strClientVersion,QString &Ret_strServerVersion,QString &Ret_strServerDbVersion,QString& Ret_strTemplateInUse,int& Ret_nAskOnStartup,int& Ret_nStartStartUpWizard,QByteArray& Ret_byteVersion, int &Ret_nClientTimeZoneOffsetMinutes);
	void UpdateVersion(Status& Ret_Status,QByteArray byteVersion);
	void ReadUserSessionData(Status& Ret_Status,DbRecordSet &Ret_UserData, DbRecordSet &Ret_ARSet,DbRecordSet &Ret_ContactData, DbRecordSet &Ret_recSettings, DbRecordSet &Ret_recOptions, DbRecordSet &Ret_lstMessages);
	void GetUserSessions(Status& Ret_Status,int &Ret_nUserSessions);
	void SavePersonalSettings(Status &Ret_pStatus, DbRecordSet &RetOut_pLstForWrite);
	void SavePersonalSettingsByPersonID(Status &Ret_pStatus, int nPersonID, DbRecordSet &RetOut_pLstForWrite);
	void GetPersonalSettingsByPersonID(Status &Ret_pStatus, int nPersonID, DbRecordSet &RetOut_pLstForWrite);

private:
	void SavePersonalSettings(Status &pStatus, int nPersonID, DbRecordSet &pLstForWrite);
	void GetOptionsAndSettings(Status &pStatus, int nPersonID, DbRecordSet &recSettings, DbRecordSet &recOptions);
};

#endif // SERVICE_CORESERVICES_H
