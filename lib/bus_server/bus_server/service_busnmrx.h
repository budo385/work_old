#ifndef SERVICE_BUSNMRX_H
#define SERVICE_BUSNMRX_H


#include "businessservice.h"
#include "bus_interface/bus_interface/interface_busnmrx.h"


/*!
	\class Service_BusNMRX
	\brief NMRX pattern
	\ingroup Bus_Services
*/

class Service_BusNMRX : public Interface_BusNMRX, public BusinessService
{
public:

	//NM RX operations:
	void ReadOnePair(Status &Ret_pStatus, int nN_TableID,int nM_TableID, int nKey1,int nKey2,DbRecordSet &Ret_Data,int nX_TableID);
	void ReadMultiPairs(Status &Ret_pStatus, DbRecordSet &DataPairs, DbRecordSet &Ret_Data,int nX_TableID);
	void Write(Status &Ret_pStatus, DbRecordSet &RetOut_Data,QString strLockRes,int nX_TableID);
	void WriteSimple(Status &Ret_pStatus, DbRecordSet &RetOut_Data,QString strLockRes,int nX_TableID=-1);
	void Delete(Status &Ret_pStatus, DbRecordSet &DataForDelete);
	void Lock(Status &Ret_pStatus, int nNMRX_ID,QString &Ret_strLockRes);
	void LockMulti(Status &Ret_pStatus, DbRecordSet lstLockData,QString &Ret_strLockRes);
	void Unlock(Status &Ret_pStatus, QString strLockRes);
	void WriteRelations(Status &Ret_pStatus, DbRecordSet lstRelations, int nTable1_ID, int nTable2_ID);
	void ReadRelations(Status &Ret_pStatus, int nTable1_ID, int nKey1_ID,DbRecordSet &Ret_lstRelations);
	void WriteCEContactLink(Status &Ret_pStatus, DbRecordSet lstCeIDs, DbRecordSet &RetOut_Data);
	void WriteCEProjectLink(Status &Ret_pStatus, DbRecordSet lstCeIDs, DbRecordSet &RetOut_Data);
	void CreateAssignmentData(Status &Ret_pStatus,DbRecordSet &Ret_lstNMRXData,int nMasterTableID,int nAssigmentTableID,DbRecordSet &lstIDForAssign, QString strDefaultRoleName,int nMasterTableRecordID,int nX_TableID, DbRecordSet &lstPersons,bool &Ret_bInserted);

	//ROLE
	void ReadRole(Status &Ret_pStatus, int nN_TableID,int nM_TableID, DbRecordSet &Ret_Data);
	void WriteRole(Status &Ret_pStatus, DbRecordSet &RetOut_Data,DbRecordSet &DataForDelete,QString strLockRes);
	void DeleteRole(Status &Ret_pStatus, DbRecordSet &DataForDelete);
	void LockRole(Status &Ret_pStatus, int nN_TableID,int nM_TableID, QString &Ret_strLockRes);
	void UnlockRole(Status &Ret_pStatus, QString strLockRes);
	void CheckRole(Status &Ret_pStatus, int nN_TableID,int nM_TableID,QString strRoleName, DbRecordSet &Ret_Data, bool &Ret_bRoleInserted);

};
#endif // SERVICE_BUSNMRX_H
