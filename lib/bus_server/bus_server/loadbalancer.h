#ifndef LOADBALANCER_H
#define LOADBALANCER_H


#include "systemserviceset.h"
#include <QMutex>

/*!
	\class LoadBalancer
	\brief Holds list of all app. servers in system
	\ingroup Bus_Server

	
	Tests load of application servers in system, returns list of all of them or app. server with min load
	Multithread Safe!!
*/
class LoadBalancer 
{


public:
    LoadBalancer(bool bActive,SystemServiceSet* pServices,QString strCurrentIPAddress, int nLBOListExpire=60); //LBO list expires for 60sec

	QString GetServerWithMinLoad(bool bSkipCurrent=true);
	void IsRedirectionNeeded(Status &err);
	void GetLBOList(DbRecordSet &lstLBO,bool bSkipCurrent=true);
	bool IsActive(){return m_bActive;}


private:

	//DB acesss:
	SystemServiceSet* m_Services;

	//LBO LIST:
	QReadWriteLock m_LBORWLock;		///< RWlock for accessing m_lstLBO
	DbRecordSet m_lstLBO;
	QDateTime m_datLastLBOUpdate;
	int m_nLBOListExpireTime;		//< in secs, expires from last update( must reload from server)
	QString m_strCurrentIPAddress;	//< IP:port of app. server or LB where this LBO is active!
	bool m_bActive;					//< if false then inactive
};

#endif // LOADBALANCER_H
