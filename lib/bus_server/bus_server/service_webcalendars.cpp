#include "service_webcalendars.h"
#include "service_buscalendar.h"

#include "bus_core/bus_core/contacttypemanager.h"
#include "common/common/datahelper.h"
#include "bus_core/bus_core/customavailability.h"
#include "bus_core/bus_core/globalconstants.h"

#include "bus_core/bus_core/usersessionmanager.h"
extern UserSessionManager	*g_UserSessionManager;
#include "businessserviceset.h"
extern BusinessServiceSet* g_BusinessServiceSet;

void Service_WebCalendars::ReadCalendars(Status &Ret_pStatus, int &Ret_nTotalCount, DbRecordSet &Ret_Calendars, QDate datFrom, QDate datTo, QString &Ret_strFrom, QString &Ret_strTo)
{
	//qDebug()<<datFrom;
	//qDebug()<<datTo;

	//check dates:
	if (datFrom.isNull())
	{
		datFrom	=	QDate(QDate::currentDate().year(),QDate::currentDate().month(),1);
		datTo	=	datFrom.addMonths(1).addDays(-1);
	}

	Ret_strFrom	=	datFrom.toString(Qt::SystemLocaleLongDate);
	Ret_strTo	=	datTo.toString(Qt::SystemLocaleLongDate);


	//read calendars entries for logged contact/user
	int nContactID=g_UserSessionManager->GetContactID();

	//load types:
	ServerLocalCache Types;
	Types.LoadTypes(ContactTypeManager::TYPE_CALENDAR_PRESENCE);

	QString strSQL = DbSqlTableView::getSQLView(DbSqlTableView::TVIEW_WEBSERVICE_BUS_CALENDAR_SELECT);
	strSQL += " WHERE BCEV_TEMPLATE_FLAG = 0 AND ((BCOL_FROM >= ? AND BCOL_TO <= ?) \
			  OR (BCOL_FROM <= ? AND BCOL_TO >= ?) OR (BCOL_FROM <= ? AND BCOL_TO >= ?) ) ";

	strSQL += " AND BNMR_TABLE_2 = 22 AND BNMR_TABLE_KEY_ID_2 = "+QString::number(nContactID); 
	strSQL += " ORDER BY BCOL_FROM "; 

	DbSqlQuery query(Ret_pStatus, GetDbManager());
	if(!Ret_pStatus.IsOK()) return;

	Ret_Calendars.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_WEBSERVICE_BUS_CALENDAR_SELECT));

	query.Prepare(Ret_pStatus, strSQL);
	if (!Ret_pStatus.IsOK()) return;
	query.bindValue(0, datFrom);
	query.bindValue(1, datTo);
	query.bindValue(2, datFrom);
	query.bindValue(3, datFrom);
	query.bindValue(4, datTo);
	query.bindValue(5, datTo);
	query.ExecutePrepared(Ret_pStatus);
	if (!Ret_pStatus.IsOK()) return;

	query.FetchData(Ret_Calendars);

	Ret_nTotalCount=Ret_Calendars.getRowCount();
	for (int i=0;i<Ret_nTotalCount;i++)
	{
		//----------------------------------------------------------------------
		//get nmrx persons & contacts then convert 'em as comma separated list:
		//----------------------------------------------------------------------
		DbRecordSet rowCal=Ret_Calendars.getRow(i);

		DbRecordSet lstContacts;
		g_BusinessServiceSet->BusCalendar->GetEventPartContacts(Ret_pStatus, rowCal, lstContacts);

		DbRecordSet lstProjects;
		g_BusinessServiceSet->BusCalendar->GetEventPartProjects(Ret_pStatus, rowCal, lstProjects);


		QString strAssignedList;
		if (Ret_pStatus.IsOK())
		{
			int nSize2=lstContacts.getRowCount();
			for (int k=0;k<nSize2;k++)
				strAssignedList+=lstContacts.getDataRef(k,"BCNT_NAME").toString()+", ";
			strAssignedList.chop(2);
		}
		Ret_Calendars.setData(i,"ASSIGNED_LIST",strAssignedList);

		//----------------------------------------------------------------------
		//get title:
		//----------------------------------------------------------------------
		if (!Ret_Calendars.getDataRef(i,"BCEV_TITLE").toString().isEmpty())
		{
			Ret_Calendars.setData(i,"EVENT_TITLE",Ret_Calendars.getDataRef(i,"BCEV_TITLE").toString());
		}
		else if (lstProjects.getRowCount()>0)
		{
			Ret_Calendars.setData(i,"EVENT_TITLE",lstProjects.getDataRef(0,"BUSP_NAME").toString());
		}
		else
		{
			QString strBody=Ret_Calendars.getDataRef(i,"BCOL_DESCRIPTION").toString();
			if (!strBody.isEmpty())
			{
				//convert to !????
				strBody = DataHelper::ExtractTextFromHTML(strBody);
				strBody = strBody.left(200); //first 200 as title? hehe
				strBody = strBody.remove("&nbsp;", Qt::CaseInsensitive);
				strBody = strBody.remove("&#8217;", Qt::CaseInsensitive);
				strBody = strBody.remove("&#8230;", Qt::CaseInsensitive);
				Ret_Calendars.setData(i,"EVENT_TITLE",strBody);
			}
			else
			{
				Ret_Calendars.setData(i,"EVENT_TITLE","");

			}
		}


		//----------------------------------------------------------------------
		//get STATUS:
		//----------------------------------------------------------------------
		//if from<current date -> 0, else if presence type...
		int nStatus=GetStatus(Ret_Calendars.getDataRef(i,"BCOL_FROM").toDateTime(),Ret_Calendars.getDataRef(i,"BCEP_PRESENCE_TYPE_ID").toInt(),Types,Ret_Calendars.getDataRef(i,"BCEP_STATUS").toInt());
		Ret_Calendars.setData(i,"STATUS",nStatus);


		//----------------------------------------------------------------------
		//convert date time: from, to, date
		//----------------------------------------------------------------------
		QDateTime from1 = Ret_Calendars.getDataRef(i,"BCOL_FROM").toDateTime();
		QDateTime to1 = Ret_Calendars.getDataRef(i,"BCOL_TO").toDateTime();

		QString strFrom=from1.toString("hh:mm");
		QString strTo=to1.toString("hh:mm");
		QDate date=from1.date();
		Ret_Calendars.setData(i,"TIME_FROM",strFrom);
		Ret_Calendars.setData(i,"TIME_TO",strTo);

		QString str = date.toString(Qt::SystemLocaleLongDate);
		Ret_Calendars.setData(i,"DATE_LOCAL_STR",str);


	}
	
	
	//Ret_Calendars.Dump();

}


/*
	0	- red flag (past)
	1	- preliminary (yellow)
	2	- final (green)
	3	- light blue background, but no dot
	4	- do not show any dot at all
*/
int Service_WebCalendars::GetStatus(QDateTime datFrom, int nPresenceID,ServerLocalCache &Types, int nStateID)
{
	//if lesst then status=0
	if (datFrom<QDateTime::currentDateTime())
	{
		return 0;
	}

	//-> issue 2544

/*
For CSID=1200:

	1. If the End Timestamp of a CalEv is in the past: Show red dot.
	Else:
	2. If the presence type of a CalEv is "preliminary", whow yellow dot.
	Else:
	3. If the presence typeof a CalEv is "valid", show green dot.
	Else:
	4. If the presence type of a CalEv is "available", show a light blue background, but no dot.
	Else:
	5. If there is no presence type or another presence type: Do not show any dot at all.


For CSID<>1200:

	1. If the End Timestamp of a CalEv is in the past: Show red dot.
	Else:
	2. If the state of a CalEv is "preliminary", whow yellow dot.
	Else:
	3. If the state of a CalEv is "final", show green dot.
	Else:
	4. If there is no state: Do not show any dot at all.
	Else:
	5. Show red dot ("cancelled", "over").


	lstDataCalEventPart.getDataRef(0, "BCEP_STATUS").toInt()

*/

	int nRow=Types.m_lstTypes.find("BCMT_ID",nPresenceID,true);

	bool bAnestezist = CustomAvailability::IsAvailable_AS_CalendarViewWizard();
	if (bAnestezist)
	{
		if (nRow<0)
			return 4;
		QString strTypeName = Types.m_lstTypes.getDataRef(nRow,"BCMT_TYPE_NAME").toString().toLower();

		if (nPresenceID==65)
			return 1;
		if (nPresenceID==66)
			return 2;
		if (nPresenceID==67)
			return 3;

/*
		{
		}
		if (strTypeName == QString("preliminary") || strTypeName == QString("vorl�ufig"))
			return 1;
		if (strTypeName == QString("valid") || strTypeName == QString("g�ltig"))
			return 2;
		if (strTypeName == QString("available") || strTypeName == QString("verf�gbar"))
			return 3;
*/
		return 4;
	}
	else
	{
		switch(nStateID)
		{
		case GlobalConstants::TYPE_CAL_PRELIMINARY:
			return 1;
			break;
		case GlobalConstants::TYPE_CAL_FIXED:
			return 2;
			break;
		case GlobalConstants::TYPE_CAL_OVER:
		case GlobalConstants::TYPE_CAL_CANCELLED:
			return 0;
		    break;
		default:
			return 4;
		    break;
		}
	}


}



