#ifndef SYSTEMSERVICESET_H
#define SYSTEMSERVICESET_H

#include "systemservice_collection.h"
#include "systemservice.h"



/*!
    \class SystemServiceSet
    \brief Repository of all private/server side business objects which are not accessible by client and used by system
    \ingroup Bus_System

	When business service has no user context (system) then use this set. Example is garbage collector, session handler,
	LBO process or other, processes.
	NO user context means: 
	- operations can only be performed on core tables (such as module_lic, user_session, app_srv_session, etc..)
	- there will be no history log for those tables (there is no user context)
	- there will be no record locking through core_locking table (use short live DB locking mechanism)
	- operations are not tested for AR rights


*/

class SystemServiceSet
{
public:
	SystemServiceSet(DbSqlManager *pDbManager);//,QString strSysSession);
	~SystemServiceSet();
	
	
	Service_UserSession* UserSession;
	Service_AppSrvSession* AppSrvSession;
	Serivce_IPAccessList* IPAccessList;


	//call to init set (in inher. constructor)
	void InitServiceSet();

protected:
	QList<SystemService *> m_ServiceList;	//< Used to store pointers to bus services
	DbSqlManager *m_DbManager;
	//QString m_strSysSession;				//< System session as SYS_AppSerialID

};




#endif //SYSTEMSERVICESET_H
