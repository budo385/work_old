/*
#include "service_modulelicensemanager.h"
#include "bus_core/bus_core/fp_default.h"

//GLOBAL OBJECT:
#include "bus_core_/bus_core/licenseaccessrights.h"

extern LicenseAccessRights g_ModuleLicense;


void Service_ModuleLicenseManager::LoadKeyFile(Status& Ret_pStatus, QString &strKeyFile)
{
	Ret_pStatus.setError(0);

	if(!g_ModuleLicense.LoadLicense(strKeyFile)){
		Ret_pStatus.setError(1);
		Ret_pStatus.setErrorDetails("Failed to load keyfile.");
	}
}
	
int Service_ModuleLicenseManager::GetUniqueLicenseID(Status& Ret_pStatus)
{
	Ret_pStatus.setError(0);

	if(!g_ModuleLicense.IsLoaded()){
		Ret_pStatus.setError(1);
		Ret_pStatus.setErrorDetails("Keyfile not loaded.");
		return 0;
	}
	
	return g_ModuleLicense.GetLicenseInfo().m_nLicenseID;
}

int Service_ModuleLicenseManager::GetMaxLicenseCount(Status& Ret_pStatus, QString& strModuleCode, QString& strMLIID)
{
	Ret_pStatus.setError(0);

	if(!g_ModuleLicense.IsLoaded()){
		Ret_pStatus.setError(1);
		Ret_pStatus.setErrorDetails("Keyfile not loaded.");
		return 0;
	}
	
	LicenseModInfo *list = NULL;
	if(!g_ModuleLicense.GetARList(strModuleCode, strMLIID, &list)){
		Ret_pStatus.setError(1);
		Ret_pStatus.setErrorDetails("Failed to find Module/MLIID data.");
		return 0;
	}

	return list->m_nNumLicenses;
}

bool Service_ModuleLicenseManager::IsFPAvailable(Status& Ret_pStatus, int FPcode, QString& strModuleCode,QString& strMLIID, int &Ret_nValue)
{
	Ret_pStatus.setError(0);

	if(!g_ModuleLicense.IsLoaded()){
		Ret_pStatus.setError(1);
		Ret_pStatus.setErrorDetails("Keyfile not loaded.");
		return false;
	}

	return g_ModuleLicense.CanUseFP(strModuleCode, strMLIID, FPcode, Ret_nValue);
}

void Service_ModuleLicenseManager::GetFPList(Status& Ret_pStatus, QString& strModuleCode, QString& strMLIID, DbRecordSet& RetOut_pList)
{
	Ret_pStatus.setError(0);

	//redefine result recordset
	RetOut_pList.destroy();
	RetOut_pList.addColumn(QVariant::Int, "FP_Code");
	RetOut_pList.addColumn(QVariant::Int, "FP_CanUse");
	RetOut_pList.addColumn(QVariant::Int, "FP_Value");

	//build access right list for a given Module/MLIID combination
	for(int i=1; i<=COUNT_FPS; i++)
	{
		RetOut_pList.addRow();

		RetOut_pList.setData(i-1, 0, i);	//set code

		int nValue = 0;
		if(g_ModuleLicense.CanUseFP(strModuleCode, strMLIID, i, nValue))	
			RetOut_pList.setData(i-1, 1, 1);	//set access right - enabled
		else
			RetOut_pList.setData(i-1, 1, 0);	//set access right - disabled

		RetOut_pList.setData(i-1, 2, nValue);	//set value
	}
}

*/