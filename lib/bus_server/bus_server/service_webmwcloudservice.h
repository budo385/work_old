#ifndef SERVICE_WEBMWCLOUDSERVICE_H
#define SERVICE_WEBMWCLOUDSERVICE_H

#include "bus_interface/bus_interface/interface_webmwcloudservice.h"
#include "businessservice.h"

/*
	Session less web rest object: mwreply server can access it
*/

class Service_WebMWCloudService : public Interface_WebMWCloudService, public BusinessService
{
public:
	void CreateUser(Status &Ret_pStatus, QString strUserEmail, QString strPass, QString strFirstName, QString strLastName);
	void ChangeUserPassword(Status &Ret_pStatus, QString strUserEmail, QString strPass);
	void EnableUserAccount(Status &Ret_pStatus, QString strUserEmail, bool bEnable);
	void DeleteUser(Status &Ret_pStatus, QString strUserEmail);

	void GetUsersEmailCnt(Status &Ret_pStatus, DbRecordSet &Ret_lstData);
	void SendUserErrorPushMessage(Status &Ret_pStatus, QString strUsername, QString strEmailFrom, QString strErrorMsg);
	
	void AddUserEmailAccountIn(Status &Ret_pStatus, QString strUsername, QString strConce, QString strAuthToken, QString strEmail, QString strLogin, QString strPassword, QString strServer, int nPort, bool nIsImap, int nConnectionType, bool bAccountActive, int nDeleteEmailsAfterNoDays);
	void AddUserEmailAccountOut(Status &Ret_pStatus, QString strUsername, QString strConce, QString strAuthToken, QString strEmail, QString strLogin, QString strPassword, QString strServer, int nPort, int nConnectionType, bool  bAuthenticationRequired);
	void RemoveUserEmailAccount(Status &Ret_pStatus, QString strUsername, QString strConce, QString strAuthToken, QString strEmail);
	void DeleteEmail(Status &Ret_pStatus,  QString strUsername, QString strConce, QString strAuthToken, int nEmailID);
	void DeleteMultipleEmails(Status &Ret_pStatus,  QString strUsername, QString strConce, QString strAuthToken, QString strEmailIDs);
	void BlackListEmailAddress(Status &Ret_pStatus, QString strUsername, QString strConce, QString strAuthToken, QString strEmail, bool bActivateBlocking);
	void GetEmailAddressBlackList(Status &Ret_pStatus, QString strUsername, QString strConce, QString strAuthToken, QString &RetOut_strList);
	void StoreEmail(Status &Ret_pStatus, QString strUsername, QString strConce, QString strAuthToken, int nIsOutgoing,QString strFrom,QString strTo,QString strSubject, QString strMime,QString strFullFrom,QString strCC,QString strBCC);
	void MoveEmail(Status &Ret_pStatus, QString strUsername, QString strConce, QString strAuthToken, int nEmailID, int nMailbox, QString strDueDate, QString strReminderType); 
	
	//new API:
	void GetUnreadEmailsCount(Status &Ret_pStatus, QString strUsername, QString strConce, QString strAuthToken, int &Ret_nCount);
	void RegisterPushNotificationID(Status &Ret_pStatus, QString strUsername, QString strConce, QString strAuthToken, QString strPushToken, int nOSType);
	void ActivatePushNotification(Status &Ret_pStatus, QString strUsername, QString strConce, QString strAuthToken, int nEnablePush);
	void SetNewEmailsPushNotifications(Status &Ret_pStatus, QString strUsername, QString strConce, QString strAuthToken, int nState, int nSilentMode, QString strContacts);
	void SetEmailAccountPushNotifications(Status &Ret_pStatus, QString strUsername, QString strConce, QString strAuthToken, QString strEmailAccount,int nEnablePush);
	//test:
	void SendPushNotification(Status &Ret_pStatus, QString strUsername, QString strConce, QString strAuthToken, QString strMessage, int nOSType, QString strPushToken,  QString strPushSoundName, int nBadgeNumber);


private:
	int	 GetPersonID(Status &Ret_pStatus,QString strUsername);
	void GeneratePersonCode(Status &Ret_pStatus,QString &strCode);
	void GetDefaultPersonRole(Status &Ret_pStatus,int &nRoleID);
	void GetUserByEmail(Status &Ret_pStatus,QString strEmail, int &nPersonID);
	QString LogOn(QString strEmail, QString strConce, QString strAuthToken);
	void	LogOut(QString strSession);

	//POP3/IMAP related helpers
	void LoadPersonEmailInAccounts(Status &Ret_pStatus, int nPersonID, DbRecordSet &lstPersonAccData);
	void SavePersonEmailInAccounts(Status &Ret_pStatus, int nPersonID, DbRecordSet &lstPersonAccData);
	int  FindEmailInAccountIdx(const QString &strEmail, DbRecordSet &lstPersonAccData);

	//SMTP related helpers
	void LoadPersonSmtpSetting(Status &Ret_pStatus, int nPersonID, DbRecordSet &lstPersonAccData);
	void SetPersonSmtpSetting(Status &Ret_pStatus, int nPersonID, QString strEmail, QString strLogin, QString strPassword, QString strServer, int nPort, int nConnectionType, bool bAuthenticationRequired);
	void LoadPersonSettingValue(Status &Ret_pStatus, int nPersonID, DbRecordSet &lstData, int nValueSet, int nValueCode, const QString &strResFieldName, const QString &strSqlField);
	void SavePersonSettingValue(Status &Ret_pStatus, int nPersonID, DbRecordSet &lstData, int nValueSet, int nValueCode, const QString &strResFieldName, const QString &strSqlField);
	
	void SaveLastErrorMsgDateTime(Status &pStatus,QString strDateErr, int nPersonID);
};


#endif // SERVICE_WEBMWCLOUDSERVICE_H
