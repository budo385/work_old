#ifndef SYSTEMSERVICE_H
#define SYSTEMSERVICE_H

#include "db/db/dbsqlmanager.h"

/*!
    \class SystemService
    \brief System service as context free version of ordinary BusinessService 
	\ingroup Bus_System

	Use:
	- use it as base class for all System Business services
*/

class SystemService 
{

public:

	virtual ~SystemService(){};//this is essential

	//get set methods for server side objects that every BO need:
	DbSqlManager* GetDbManager(){return m_DbManager;};

	static void SetDbManager(DbSqlManager* pDbManager){m_DbManager=pDbManager;};
	//static void SetSystemSessionID(QString strSysSession){m_strSysSession=strSysSession;};

	//isolated to enable context free ops:
	QString GetSessionID(){return "";}//{return m_strSysSession;}
	int GetUserID(){return 0;}

protected:
	static DbSqlManager *m_DbManager;			//< Db access

};



#endif //SYSTEMSERVICE_H
