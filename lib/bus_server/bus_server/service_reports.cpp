#include "service_reports.h"
#include "db_core/db_core/dbsqltableview.h"
#include "db/db/dbsimpleorm.h"

void Service_Reports::GetReportRecordSet(Status &pStatus, DbRecordSet &pReportRecordSet, int nViewID, QString strWhere /*= NULL*/)
{
	//Table ORM - name of table is dummy - view ID must be supplied.
	DbSimpleOrm TableOrm(pStatus, CORE_LOCKING, GetDbManager()); 
	if(!pStatus.IsOK()) return;

	//Read.
	TableOrm.Read(pStatus, pReportRecordSet, strWhere, nViewID);
}
