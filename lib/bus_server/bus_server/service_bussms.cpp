#include "service_bussms.h"
#include "db/db/dbsimpleorm.h"
#include "db_core/db_core/dbsqltableview.h"
#include "bus_core/bus_core/globalconstants.h"
#include "businesslocker.h"
#include "db/db/dbconnectionreserver.h"

#include "privateserviceset.h"
extern PrivateServiceSet* g_PrivateServiceSet; 
#include "businessserviceset.h"
extern BusinessServiceSet* g_BusinessServiceSet;

void Service_BusSms::ReadSms(Status &Ret_pStatus, int nVoiceCallID, DbRecordSet &Ret_CallData, DbRecordSet &Ret_UAR,DbRecordSet &Ret_GAR)
{
	DbConnectionReserver conn(Ret_pStatus,GetDbManager()); //reserve global connection
	if(!Ret_pStatus.IsOK())return;

	DbRecordSet set;
	set.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CE_COMM_ENTITY));
	Ret_CallData.copyDefinition(set, false);
	set.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_SMS));
	Ret_CallData.copyDefinition(set, false);
	set.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_TASKS));
	Ret_CallData.copyDefinition(set, false);

	Status status;
	//B.T. adapted to work when task is empty
	QString strWhere= QString("\
							  FROM CE_COMM_ENTITY \
							  INNER JOIN BUS_SMS ON BSMS_COMM_ENTITY_ID=CENT_ID\
							  LEFT OUTER JOIN BUS_TASKS ON CENT_TASK_ID=BTKS_ID\
							  WHERE BSMS_ID=%1").arg(nVoiceCallID);

	g_BusinessServiceSet->ClientSimpleORM->ReadAdv(status, Ret_CallData, strWhere);
	if(!Ret_pStatus.IsOK()) return;

	//read uar/gar:
	if (Ret_CallData.getRowCount()>0)
	{
		//int nCommEntityID=Ret_CallData.getDataRef(0,"BSMS_COMM_ENTITY_ID").toInt();
		g_BusinessServiceSet->AccessRights->ReadGUAR(Ret_pStatus,nVoiceCallID,BUS_SMS,Ret_UAR,Ret_GAR);
	}
}

void Service_BusSms::WriteSms(Status &Ret_pStatus, DbRecordSet &RetOut_CallData, QString strLockRes,DbRecordSet &RetOut_ScheduleTask,DbRecordSet &RetOut_UAR,DbRecordSet &RetOut_GAR)
{
	//RetOut_CallData.Dump();
	if(RetOut_CallData.getRowCount()<1){
		Ret_pStatus.setError(1);	//TOFIX custom error code
		return;
	}
	Ret_pStatus.setError(0);

	//init ORM's:
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_SMS, GetDbManager());
	if(!Ret_pStatus.IsOK()) return;
	DbSimpleOrm TableOrm_BUS_TASKS(Ret_pStatus, BUS_TASKS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	DbSimpleOrm NewOrm(Ret_pStatus, CE_COMM_ENTITY, GetDbManager());
	if(!Ret_pStatus.IsOK()) return;

	int nCommEvID = RetOut_CallData.getDataRef(0, "CENT_ID").toInt();

	// voice call type TOFIX hardcoded for now
	int nSystemTypeID	= GlobalConstants::CE_TYPE_SMS;	
	int nColIdx = RetOut_CallData.getColumnIdx("CENT_SYSTEM_TYPE_ID");
	RetOut_CallData.setColValue(nColIdx, nSystemTypeID);



	//start transaction
	//------------------------------------------
	TableOrm.GetDbSqlQuery()->BeginTransaction(Ret_pStatus);
	if(!Ret_pStatus.IsOK())
	{ 
		return;
	}

	//---------------------------------------
	//write TASK
	//---------------------------------------
	bool bNewTaskCreated=false;
	if (RetOut_ScheduleTask.getRowCount() > 0)
	{
		if (RetOut_ScheduleTask.getDataRef(0,"BTKS_ID").toInt()<=0)
			bNewTaskCreated=true;
		TableOrm_BUS_TASKS.Write(Ret_pStatus,RetOut_ScheduleTask);
		if(!Ret_pStatus.IsOK())
		{ 
			TableOrm.GetDbSqlQuery()->Rollback();	
			return;
		}
		int nTaskID = RetOut_ScheduleTask.getDataRef(0, "BTKS_ID").toInt();
		RetOut_CallData.setData(0, "CENT_TASK_ID", nTaskID);
	}


	QString strSQL;
	DbRecordSet set;
	set.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CE_COMM_ENTITY));
	int nColCnt1 = set.getColumnCount();
	int nColCnt2 = RetOut_CallData.getColumnCount();

	NewOrm.Write(Ret_pStatus, RetOut_CallData, -1, nColCnt2-nColCnt1); // write only com_entity part
	if(!Ret_pStatus.IsOK())
	{ 
		TableOrm.GetDbSqlQuery()->Rollback();	
		return;
	}

	//refresh ID if inserting
	if(nCommEvID <= 0)
	{
		//nCommEvID = NewOrm.GetDbSqlQuery()->GetLastInsertedID(Ret_pStatus,"CENT_ID");	
		//RetOut_CallData.setData(0, "CENT_ID", nCommEvID);	//return ID back to the user
		nCommEvID = RetOut_CallData.getDataRef(0, "CENT_ID").toInt(); //BT: when inserted, new id is stored inside list
	}

	DbRecordSet Data;
	Data.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_SMS));
	Data.merge(RetOut_CallData);
	Data.setData(0, "BSMS_COMM_ENTITY_ID", nCommEvID);
	//Data.Dump();

	//
	// insert new concrete CommEvent - voice call (TOFIX with all call details)
	//
	TableOrm.Write(Ret_pStatus, Data, DbSqlTableView::TVIEW_BUS_SMS);
	if(!Ret_pStatus.IsOK())
	{ 
		TableOrm.GetDbSqlQuery()->Rollback();	
		return;
	}

	//unlock if needed:
	if (!strLockRes.isEmpty())
	{
		BusinessLocker Locker(Ret_pStatus, CE_COMM_ENTITY, GetDbManager());
		if(!Ret_pStatus.IsOK())return;
		Locker.UnLock(Ret_pStatus,strLockRes);

	}

	//UAR/GAR (only for 1st row:)
	int nID=Data.getDataRef(0,"BSMS_ID").toInt();
	g_BusinessServiceSet->AccessRights->WriteGUAR(Ret_pStatus,nID,BUS_SMS,RetOut_UAR,RetOut_GAR);
	if (!Ret_pStatus.IsOK())
	{
		TableOrm.GetDbSqlQuery()->Rollback();
		return;
	}

	//g_BusinessServiceSet->AccessRights->UpdateProjectCommEntityGUAR(Ret_pStatus,BUS_SMS,Data);
	//if(!Ret_pStatus.IsOK()) 
	//{
	//	TableOrm.GetDbSqlQuery()->Rollback();
	//	return;
	//}

	//re-read uar/gar (only for inherit-enabled entity: project, voice_call, email, doc)
	g_BusinessServiceSet->AccessRights->ReadGUAR(Ret_pStatus,nID,BUS_SMS,RetOut_UAR,RetOut_GAR);
	if(!Ret_pStatus.IsOK()) 
	{
		TableOrm.GetDbSqlQuery()->Rollback();
		return;
	}

	TableOrm.GetDbSqlQuery()->Commit(Ret_pStatus);

	RetOut_CallData.setData(0, "BSMS_ID", Data.getDataRef(0, "BSMS_ID").toInt());	//return ID back to the user
	//RetOut_CallData.Dump();

	if (Ret_pStatus.IsOK())
	{
		g_PrivateServiceSet->BusinessHelper->ClientTaskNotification(Ret_pStatus,RetOut_ScheduleTask,bNewTaskCreated);
	}
}

void Service_BusSms::ListSmsMessages(Status &Ret_pStatus, DbRecordSet &RetOut_lstCallData, QDate &datDate, int nOwnerID)
{
	Ret_pStatus.setError(0);

	//Make local query, can fail if connection reservation fails.
	DbSqlQuery query(Ret_pStatus,GetDbManager());
	if(!Ret_pStatus.IsOK()) return;

	QDateTime BeginTime(datDate);
	QDateTime EndTime(datDate);	
	EndTime = EndTime.addDays(1);
	EndTime	= EndTime.addMSecs(-1);
	//qDebug() << BeginTime << EndTime;

	//Prepare query.
	QString strSQL="SELECT * FROM BUS_SMS, CE_COMM_ENTITY WHERE CE_COMM_ENTITY.CENT_SYSTEM_TYPE_ID="+QVariant(GlobalConstants::CE_TYPE_SMS).toString()+" AND CE_COMM_ENTITY.CENT_ID=BUS_SMS.BSMS_COMM_ENTITY_ID ";
	if(nOwnerID > 0)
		strSQL += QString().sprintf("AND CE_COMM_ENTITY.CENT_OWNER_ID=%d ", nOwnerID);
	strSQL += "AND BSMS_DATE_SENT BETWEEN ? AND ?";

	//Prepare statement for execution.
	query.Prepare(Ret_pStatus, strSQL);
	//Bind date.
	query.bindValue(0, BeginTime);
	query.bindValue(1, EndTime);
	//Execute prepared
	query.ExecutePrepared(Ret_pStatus);
	if(!Ret_pStatus.IsOK()) return;

	//If everything went fine fetch records.
	DbRecordSet rsResult;
	query.FetchData(rsResult);

	//rsResult.Dump();
	//qDebug() << "Results: " << rsResult.getRowCount();

	RetOut_lstCallData.clear();
	RetOut_lstCallData.merge(rsResult);
}

void Service_BusSms::DeleteSmsMessages(Status &Ret_pStatus, DbRecordSet &RetOut_lstIDs)
{
	//RetOut_lstIDs.Dump();
	Ret_pStatus.setError(0);

	//Make local query, can fail if connection reservation fails.
	DbSqlQuery query(Ret_pStatus,GetDbManager());
	if(!Ret_pStatus.IsOK()) return;

	//delete rows in chunks
	QString strSQL = "DELETE FROM CE_COMM_ENTITY WHERE CENT_SYSTEM_TYPE_ID="+QVariant(GlobalConstants::CE_TYPE_SMS).toString()+" AND CENT_ID IN";

	int nNumRows = RetOut_lstIDs.getRowCount();
	int nStartFrom = 0;
	while(nStartFrom < nNumRows)
	{
		QString strWhere;
		nStartFrom = DbSqlTableDefinition::GenerateChunkedWhereStatement(RetOut_lstIDs, "CENT_ID", strWhere, nStartFrom);

		QString strQuery = strSQL + strWhere;
		query.Execute(Ret_pStatus, strQuery);
		if(!Ret_pStatus.IsOK()) return;
	}

	RetOut_lstIDs.clear();
}

