#include "service_mainentityselector.h"
#include "bus_core/bus_core/mainentitycalculatedname.h"
#include "common/common/entity_id_collection.h"
#include "bus_core/bus_core/mainentityfilter.h"
#include "db_core/db_core/dbsqltableview.h"
#include "db/db/dbsimpleorm.h"
#include "db/db/dbconnectionreserver.h"
#include "bus_core/bus_core/emailhelpercore.h"
#include "common/common/logger.h"
#include "trans/trans/xmlutil.h"
#include "bus_core/bus_core/useraccessright.h"

#include "businessserviceset.h"
extern BusinessServiceSet* g_BusinessServiceSet;
#include "bus_core/bus_core/usersessionmanager.h"
extern UserSessionManager	*g_UserSessionManager;
#include "bus_core/bus_core/useraccessright.h"
extern UserAccessRight *g_AccessRight;				//global access right tester


/*!
	Read main entity data. Implement own main entity read out data here:
	- add to switch
	- define own private custom method


	\param Ret_pStatus		- returns error (RETURNED)
	\param nEntityID		- entity id defined in the the bus_core/entity_id_collection.h
	\param Filter			- filter parameters stored in the DbRecordSet
	\param Ret_Data			- returned data
	\param pDbConn			- db connection

*/
void Service_MainEntitySelector::ReadData(Status &Ret_pStatus, int nEntityID, DbRecordSet Filter, DbRecordSet &Ret_Data)
{
	switch(nEntityID)
	{
	case ENTITY_BUS_PERSON:
		ReadBusPerson(Ret_pStatus,nEntityID,Filter,Ret_Data);
		break;
	case ENTITY_CORE_USER:
		ReadCoreUser(Ret_pStatus,nEntityID,Filter,Ret_Data);
		break;
	case ENTITY_BUS_ORGANIZATION:
		ReadBusOrganization(Ret_pStatus,nEntityID,Filter,Ret_Data);
		break;
	case ENTITY_BUS_CM_TYPES:
		ReadContactTypes(Ret_pStatus,nEntityID,Filter,Ret_Data);
		break;
	case ENTITY_BUS_DEPARTMENT:
		ReadBusDepartment(Ret_pStatus,nEntityID,Filter,Ret_Data);
		break;
	case ENTITY_CONTACT_ORGANIZATION:
		ReadContactOrganization(Ret_pStatus,nEntityID,Filter,Ret_Data);
		break;
	case ENTITY_CONTACT_DEPARTMENT:
		ReadContactDepartment(Ret_pStatus,nEntityID,Filter,Ret_Data);
		break;
	case ENTITY_CONTACT_FUNCTION:
		ReadContactFunction(Ret_pStatus,nEntityID,Filter,Ret_Data);
		break;
	case ENTITY_CONTACT_PROFESSION:
		ReadContactProfession(Ret_pStatus,nEntityID,Filter,Ret_Data);
		break;
	case ENTITY_DOC_TEMPLATE_CATEGORY:
		ReadCategoryDoc(Ret_pStatus,nEntityID,Filter,Ret_Data);
		break;
	case ENTITY_EMAIL_TEMPLATE_CATEGORY:
		ReadCategoryEmail(Ret_pStatus,nEntityID,Filter,Ret_Data);
		break;
	case ENTITY_BUS_COST_CENTER:
		ReadBusCostCenter(Ret_pStatus,nEntityID,Filter,Ret_Data);
		break;
	case ENTITY_CE_TYPES:
		ReadCeTypes(Ret_pStatus,nEntityID,Filter,Ret_Data);
		break;
	case ENTITY_BUS_PROJECT:
		ReadProjects(Ret_pStatus,nEntityID,Filter,Ret_Data);
		break;
	case ENTITY_CE_EVENT_TYPES:
		ReadCeEventTypes(Ret_pStatus,nEntityID,Filter,Ret_Data);
		break;
	case ENTITY_BUS_CONTACT:
		g_BusinessServiceSet->BusContact->ReadShortContactList(Ret_pStatus,Filter,Ret_Data);
		break;
	case ENTITY_BUS_CM_ADDRESS_SCHEMAS:
		g_BusinessServiceSet->BusAddressSchemas->Read(Ret_pStatus,Ret_Data);
		break;
	case ENTITY_BUS_OPT_GRID_VIEWS:
		ReadGridViews(Ret_pStatus,nEntityID,Filter,Ret_Data);
		break;
	case ENTITY_NMRX_ROLES:
		g_BusinessServiceSet->BusNMRX->ReadRole(Ret_pStatus,-1,-1,Ret_Data);
		break;
	case ENTITY_BUS_DM_APPLICATIONS:
		ReadDMApplications(Ret_pStatus,nEntityID,Filter,Ret_Data);
		break;
	case ENTITY_BUS_DM_TEMPLATES: //always for logged user:
		g_BusinessServiceSet->BusDocuments->ReadTemplates(Ret_pStatus,GetUserSessionManager()->GetPersonID(),Ret_Data);
		break;
	case ENTITY_BUS_CM_GROUP: //always for logged user:
		ReadContactGroups(Ret_pStatus,nEntityID,Filter,Ret_Data);
		break;
	case ENTITY_BUS_GROUP_ITEMS: //always for logged user:
		ReadGroupItems(Ret_pStatus,nEntityID,Filter,Ret_Data);
		break;
	case ENITITY_ALL_EMAIL_ADDRESSES:
		ReadAllEmails(Ret_pStatus,nEntityID,Filter,Ret_Data);
		break;
	case ENTITY_CALENDAR_RESOURCES:
		ReadResources(Ret_pStatus,nEntityID,Filter,Ret_Data);
		break;
	case ENTITY_CALENDAR_EVENT_TEMPLATES: //always for logged user:
		g_BusinessServiceSet->BusCalendar->ReadTemplates(Ret_pStatus,Ret_Data);
		break;
	case ENTITY_CALENDAR_EVENT_TEMPLATE_CATEGORY:
		ReadCategoryCalendar(Ret_pStatus,nEntityID,Filter,Ret_Data);
		break;
	case ENTITY_BUS_EMAIL_TEMPLATES:
		ReadEmailTemplates(Ret_pStatus,nEntityID,Filter,Ret_Data);
		break;
	case ENTITY_PERSON_EMAIL_ADDRESS_SELECTOR:
		ReadPersonEmailAddresses(Ret_pStatus,nEntityID,Filter,Ret_Data);
		break;
	case ENTITY_CALENDAR_VIEW:
		ReadCalendarViews(Ret_pStatus,nEntityID,Filter,Ret_Data);
		break;
	case ENTITY_PAYMENT_CONDITIONS:
		ReadPaymentConditions(Ret_pStatus,nEntityID,Filter,Ret_Data);
		break;
	case ENTITY_CUSTOM_FIELDS:
		ReadCustomFields(Ret_pStatus,nEntityID,Filter,Ret_Data);
		break;

	default:
		Q_ASSERT_X(false, "Service_MainEntitySelector::ReadData", "Entity not supportted!");
	}
	//_DUMP(Ret_Data);
}

/*!
	Read Bus Person in TVIEW_BUS_PERSON_SELECTION format, fill NAME column
*/
void Service_MainEntitySelector::ReadBusPerson(Status &Ret_pStatus, int nEntityID, DbRecordSet Filter, DbRecordSet &Ret_Data)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_PERSON, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;


	//if filter exists try to set filter:
	QString strWhere;
	if(Filter.getRowCount()!=0)
	{
		//Filter.Dump();
		strWhere=MainEntityFilter::GetWhereStatement(Filter,true);
	}

	DbRecordSet rowData;
	bool bSkipFilterSystemUser=false;
	g_UserSessionManager->GetUserData(rowData);
	if (rowData.getRowCount()>0)
		if (rowData.getDataRef(0,"CUSR_IS_SYSTEM").toInt()==1)
			bSkipFilterSystemUser=true;

	if (!bSkipFilterSystemUser)
	{
		if (strWhere.indexOf("WHERE")==-1)
			strWhere+=" WHERE CUSR_IS_SYSTEM=0 OR CUSR_IS_SYSTEM IS NULL";
		else
			strWhere+=" AND CUSR_IS_SYSTEM=0 OR CUSR_IS_SYSTEM IS NULL"; 
	}
	
	//strWhere+=" ORDER BY BPER_CODE,BPER_LAST_NAME,BPER_FIRST_NAME"; //order by code, last name/first name
	strWhere+=" ORDER BY BPER_LAST_NAME,BPER_FIRST_NAME"; //order by last name/first name->issue 2531

	//load all
	TableOrm.Read(Ret_pStatus,Ret_Data,strWhere,DbSqlTableView::TVIEW_BUS_PERSON_SELECTION);


	//MB: reread those with login acc: set flag=0/1 if CUSR_USERNAME is empty or NULL

	int nSize=Ret_Data.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		if (Ret_Data.getDataRef(i,"CUSR_USERNAME").toString().isEmpty())
			Ret_Data.setData(i,"IS_LOGIN_ACC",0);
		else
			Ret_Data.setData(i,"IS_LOGIN_ACC",1);
	}


	//_DUMP(Ret_Data);

	if(!Ret_pStatus.IsOK()) return;


	//calculated names: 
	MainEntityCalculatedName filler;
	filler.Initialize(ENTITY_BUS_PERSON,Ret_Data);
	filler.FillCalculatedNames(Ret_Data,Ret_Data.getColumnIdx("BPER_NAME"),Ret_Data.getColumnIdx("BPER_DEPT_NAME"));

	//Ret_Data.sort("BPER_NAME");
}

/*!
	Read BusOrganization in TVIEW_BUS_ORGANIZATION_SELECTION format
*/
void Service_MainEntitySelector::ReadBusOrganization(Status &Ret_pStatus, int nEntityID, DbRecordSet Filter, DbRecordSet &Ret_Data)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_ORGANIZATION, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	//load all
	TableOrm.Read(Ret_pStatus,Ret_Data,"",DbSqlTableView::TVIEW_BUS_ORGANIZATION_SELECTION);

	if(!Ret_pStatus.IsOK()) return;
}




/*!
Read Bus Person in TVIEW_CORE_USER_SELECTION_SHORT format, fill NAME column
*/
void Service_MainEntitySelector::ReadCoreUser(Status &Ret_pStatus, int nEntityID, DbRecordSet Filter, DbRecordSet &Ret_Data)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, CORE_USER, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	DbRecordSet rowData;
	bool bSkipFilterSystemUser=false;
	g_UserSessionManager->GetUserData(rowData);
	if (rowData.getRowCount()>0)
		if (rowData.getDataRef(0,"CUSR_IS_SYSTEM").toInt()==1)
			bSkipFilterSystemUser=true;

	QString strWhere=" WHERE CUSR_IS_SYSTEM=0 OR CUSR_IS_SYSTEM IS NULL ORDER BY CUSR_USERNAME";
	if (bSkipFilterSystemUser)
		strWhere="";

	//load all
	TableOrm.Read(Ret_pStatus,Ret_Data,strWhere,DbSqlTableView::TVIEW_CORE_USER_SELECTION_SHORT);

	if(!Ret_pStatus.IsOK()) return;

	//fill name (calculated field):)
	int nLastNameIdx=Ret_Data.getColumnIdx("CUSR_LAST_NAME");
	int nFirstNameIdx=Ret_Data.getColumnIdx("CUSR_FIRST_NAME");
	int nNameIdx=Ret_Data.getColumnIdx("CUSR_NAME");
	int nSize=Ret_Data.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		QString strName=Ret_Data.getDataRef(i,nLastNameIdx).toString()+", "+Ret_Data.getDataRef(i,nFirstNameIdx).toString();
		Ret_Data.setData(i,nNameIdx,strName);
	}
}



/*!
	Read user defined types: by entity type -> inside filter
*/
void Service_MainEntitySelector::ReadContactTypes(Status &Ret_pStatus, int nEntityID, DbRecordSet Filter, DbRecordSet &Ret_Data)
{

	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_CM_TYPES, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	//if filter exists try to set filter:
	QString strWhere;
	if(Filter.getRowCount()!=0)
	{
		strWhere=MainEntityFilter::GetWhereStatement(Filter,true);
	}
	
	//load all types:
	TableOrm.Read(Ret_pStatus,Ret_Data,strWhere);

}

/*!
	Read Bus Department in TVIEW_BUS_DEPARTMENT_SELECTION format
*/
void Service_MainEntitySelector::ReadBusDepartment(Status &Ret_pStatus, int nEntityID, DbRecordSet Filter, DbRecordSet &Ret_Data)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_DEPARTMENT, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	//if filter exists try to set filter:
	QString strWhere;
	if(Filter.getRowCount()!=0)
	{
		strWhere=MainEntityFilter::GetWhereStatement(Filter,true);
	}

	strWhere+=" ORDER BY BDEPT_CODE"; 

	//load all
	TableOrm.Read(Ret_pStatus,Ret_Data,strWhere,DbSqlTableView::TVIEW_BUS_DEPARTMENT_SELECTION);

	if(!Ret_pStatus.IsOK()) return;
}



void Service_MainEntitySelector::ReadCategoryDoc(Status &Ret_pStatus, int nEntityID, DbRecordSet Filter, DbRecordSet &Ret_Data)
{
	//make local query, can fail if connection reservation fails
	DbSqlQuery query(Ret_pStatus,GetDbManager());
	if(!Ret_pStatus.IsOK()) return;

	//prepare insert query:
	QString strSQL="SELECT DISTINCT BDMD_CATEGORY FROM BUS_DM_DOCUMENTS WHERE BDMD_CATEGORY<>'' AND NOT(BDMD_CATEGORY IS NULL) ORDER BY BDMD_CATEGORY";

	//exec & auto destroy any connection held:
	query.Execute(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK()) return;


	//fetch result:
	Ret_Data.destroy();
	Ret_Data.addColumn(QVariant::String,"NAME");

	query.FetchData(Ret_Data);

}
void Service_MainEntitySelector::ReadCategoryEmail(Status &Ret_pStatus, int nEntityID, DbRecordSet Filter, DbRecordSet &Ret_Data)
{
	//make local query, can fail if connection reservation fails
	DbSqlQuery query(Ret_pStatus,GetDbManager());
	if(!Ret_pStatus.IsOK()) return;

	//prepare insert query:
	QString strSQL="SELECT DISTINCT BEM_CATEGORY FROM BUS_EMAIL WHERE BEM_CATEGORY<>'' AND NOT(BEM_CATEGORY IS NULL) AND BEM_TEMPLATE_FLAG = 1 ORDER BY BEM_CATEGORY";

	//exec & auto destroy any connection held:
	query.Execute(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK()) return;


	//fetch result:
	Ret_Data.destroy();
	Ret_Data.addColumn(QVariant::String,"NAME");

	query.FetchData(Ret_Data);
}



/*!
	Read contact organizations
*/
void Service_MainEntitySelector::ReadContactOrganization(Status &Ret_pStatus, int nEntityID, DbRecordSet Filter, DbRecordSet &Ret_Data)
{

	//make local query, can fail if connection reservation fails
	DbSqlQuery query(Ret_pStatus,GetDbManager());
	if(!Ret_pStatus.IsOK()) return;

	//prepare insert query:
	QString strSQL="SELECT BCNT_ID,BCNT_ORGANIZATIONNAME FROM BUS_CM_CONTACT WHERE BCNT_ORGANIZATIONNAME<>'' AND NOT(BCNT_ORGANIZATIONNAME IS NULL) ORDER BY BCNT_ORGANIZATIONNAME";

	//exec & auto destroy any connection held:
	query.Execute(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK()) return;

	//fetch result:
	Ret_Data.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_SUBSET_SELECTION));
	query.FetchData(Ret_Data);

	//read Org:
	DbRecordSet lstOrg;
	ReadBusOrganization(Ret_pStatus,ENTITY_BUS_ORGANIZATION,Filter,lstOrg);

	int nSize=lstOrg.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		//add only inf not already exist
		Ret_Data.addRow();
		Ret_Data.setData(Ret_Data.getRowCount()-1,0,-1); //no contact id
		Ret_Data.setData(Ret_Data.getRowCount()-1,1,lstOrg.getDataRef(i,"BORG_NAME").toString());
	}

	//remove duplicates
	Ret_Data.removeDuplicates(1);
	Ret_Data.sort(1);
}


/*!
	Read contact departments
*/
void Service_MainEntitySelector::ReadContactDepartment(Status &Ret_pStatus, int nEntityID, DbRecordSet Filter, DbRecordSet &Ret_Data)
{

	//make local query, can fail if connection reservation fails
	DbSqlQuery query(Ret_pStatus,GetDbManager());
	if(!Ret_pStatus.IsOK()) return;

	//prepare insert query:
	QString strSQL="SELECT BCNT_ID,BCNT_DEPARTMENTNAME FROM BUS_CM_CONTACT WHERE BCNT_DEPARTMENTNAME<>'' AND NOT(BCNT_DEPARTMENTNAME IS NULL) ORDER BY BCNT_DEPARTMENTNAME";

	//exec & auto destroy any connection held:
	query.Execute(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK()) return;


	//fetch result:
	Ret_Data.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_SUBSET_SELECTION));
	query.FetchData(Ret_Data);

	//read Org:
	DbRecordSet lstDept;
	ReadBusDepartment(Ret_pStatus,ENTITY_BUS_DEPARTMENT,Filter,lstDept);


	int nSize=lstDept.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		//add only inf not already exist
		Ret_Data.addRow();
		Ret_Data.setData(Ret_Data.getRowCount()-1,0,-1); //no contact id
		Ret_Data.setData(Ret_Data.getRowCount()-1,1,lstDept.getDataRef(i,"BDEPT_NAME").toString());
	}

	//remove duplicates
	Ret_Data.removeDuplicates(1);
	Ret_Data.sort(1);
}



/*!
Read contact functions
*/
void Service_MainEntitySelector::ReadContactFunction(Status &Ret_pStatus, int nEntityID, DbRecordSet Filter, DbRecordSet &Ret_Data)
{

	//make local query, can fail if connection reservation fails
	DbSqlQuery query(Ret_pStatus,GetDbManager());
	if(!Ret_pStatus.IsOK()) return;

	//prepare insert query:
	//QString strSQL="SELECT DISTINCT BCNT_FUNCTION FROM BUS_CM_CONTACT WHERE BCNT_FUNCTION<>'' AND NOT(BCNT_FUNCTION IS NULL) ORDER BY BCNT_FUNCTION";
	//issue: 886
	QString strSQL="SELECT BCNT_FUNCTION AS NAME, COUNT(*) AS CNT FROM BUS_CM_CONTACT WHERE BCNT_FUNCTION<>'' AND NOT(BCNT_FUNCTION IS NULL) GROUP BY BCNT_FUNCTION ORDER BY CNT DESC";

	//exec & auto destroy any connection held:
	query.Execute(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK()) return;
	//Ret_Data.removeDuplicates(1);


	//fetch result:
	Ret_Data.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_SUBSET_SELECTION));
	DbRecordSet data;
	query.FetchData(data);
	Ret_Data.merge(data);
}


/*!
	Read contact profession
*/
void Service_MainEntitySelector::ReadContactProfession(Status &Ret_pStatus, int nEntityID, DbRecordSet Filter, DbRecordSet &Ret_Data)
{
	//make local query, can fail if connection reservation fails
	DbSqlQuery query(Ret_pStatus,GetDbManager());
	if(!Ret_pStatus.IsOK()) return;

	//prepare insert query:
	QString strSQL="SELECT BCNT_ID,BCNT_PROFESSION FROM BUS_CM_CONTACT WHERE BCNT_FUNCTION<>'' AND NOT(BCNT_PROFESSION IS NULL) ORDER BY BCNT_PROFESSION";

	//exec & auto destroy any connection held:
	query.Execute(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK()) return;
	Ret_Data.removeDuplicates(1);


	//fetch result:
	Ret_Data.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_SUBSET_SELECTION));
	query.FetchData(Ret_Data);
}

/*!
	Read BusCostCenter in TVIEW_BUS_COST_CENTER_SELECTION format
*/
void Service_MainEntitySelector::ReadBusCostCenter(Status &Ret_pStatus, int nEntityID, DbRecordSet Filter, DbRecordSet &Ret_Data)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_COST_CENTER, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	//if filter exists try to set filter:
	QString strWhere;
	if(Filter.getRowCount()!=0)
	{
		strWhere=MainEntityFilter::GetWhereStatement(Filter,true);
	}

	strWhere+=" ORDER BY BCTC_CODE"; 

	//load all
	TableOrm.Read(Ret_pStatus,Ret_Data,strWhere,DbSqlTableView::TVIEW_BUS_COST_CENTER_SELECTION);

	if(!Ret_pStatus.IsOK()) return;
}

/*!
	Read CeTypes in TVIEW_CE_TYPE format
*/
void Service_MainEntitySelector::ReadCeTypes(Status &Ret_pStatus, int nEntityID, DbRecordSet Filter, DbRecordSet &Ret_Data)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, CE_TYPE, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	//if filter exists try to set filter:
	QString strWhere;
	if(Filter.getRowCount()!=0)
	{
		strWhere=MainEntityFilter::GetWhereStatement(Filter,true);
	}

	strWhere+=" ORDER BY CET_CODE"; 

	//load all
	TableOrm.Read(Ret_pStatus,Ret_Data,strWhere,DbSqlTableView::TVIEW_CE_TYPE_SELECT);
	if(!Ret_pStatus.IsOK()) return;
}

/*!
	Read CeTypes in TVIEW_BUS_PROJECT format
*/
void Service_MainEntitySelector::ReadProjects(Status &Ret_pStatus, int nEntityID, DbRecordSet Filter, DbRecordSet &Ret_Data)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_PROJECT, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	//if filter exists try to set filter:
	QString strWhere;
	if(Filter.getRowCount()!=0)
	{
		strWhere=MainEntityFilter::GetWhereStatement(Filter,true);
	}
	strWhere+=" ORDER BY BUSP_CODE"; //LEVEL,BUSP_PARENT";

	//load all
	g_AccessRight->SQLFilterRecords(BUS_PROJECT,strWhere);
	TableOrm.Read(Ret_pStatus,Ret_Data,strWhere,DbSqlTableView::TVIEW_BUS_PROJECT_SELECT);
	if(!Ret_pStatus.IsOK()) return;
}

/*!
	Read CeTypes in TVIEW_BUS_PROJECT format
*/
void Service_MainEntitySelector::ReadCalendarViews(Status &Ret_pStatus, int nEntityID, DbRecordSet Filter, DbRecordSet &Ret_Data)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_CAL_VIEW, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	//if filter exists try to set filter:
	QString strWhere;
	if(Filter.getRowCount()!=0)
	{
		strWhere=MainEntityFilter::GetWhereStatement(Filter,true);
	}
	strWhere+=" ORDER BY BCALV_SORT_CODE"; //LEVEL,BUSP_PARENT";

	//load all
	g_AccessRight->SQLFilterRecords(BUS_CAL_VIEW,strWhere);
	TableOrm.Read(Ret_pStatus,Ret_Data,strWhere,DbSqlTableView::TVIEW_BUS_CAL_VIEW);
	if(!Ret_pStatus.IsOK()) return;
}

/*!
	Read CeTypes in TVIEW_CE_EVENT_TYPE format
*/
void Service_MainEntitySelector::ReadCeEventTypes(Status &Ret_pStatus, int nEntityID, DbRecordSet Filter, DbRecordSet &Ret_Data)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, CE_EVENT_TYPE, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	//if filter exists try to set filter:
	QString strWhere;
	if(Filter.getRowCount()!=0)
	{
		strWhere=MainEntityFilter::GetWhereStatement(Filter,true);
	}
	strWhere+=" ORDER BY CEVT_CODE,CEVT_NAME";	//default result sorting

	//load all
	TableOrm.Read(Ret_pStatus,Ret_Data,strWhere,DbSqlTableView::TVIEW_CE_EVENT_TYPE);
	if(!Ret_pStatus.IsOK()) return;

	
}


void Service_MainEntitySelector::ReadGridViews(Status &Ret_pStatus, int nEntityID, DbRecordSet Filter, DbRecordSet &Ret_Data)
{

	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_OPT_GRID_VIEWS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	//if filter exists try to set filter:
	QString strWhere;
	if(Filter.getRowCount()!=0)
	{
		strWhere=MainEntityFilter::GetWhereStatement(Filter,true);
	}
	strWhere+=" ORDER BY BOGW_VIEW_NAME";	//default result sorting

	//load all
	TableOrm.Read(Ret_pStatus,Ret_Data,strWhere,DbSqlTableView::TVIEW_BUS_OPT_GRID_VIEWS);
	if(!Ret_pStatus.IsOK()) return;
}



/*!
	If one FUI has many SAPNE's, it will load every entity separately.

	\param Ret_pStatus		- if one fails, error is preserved
	\param lstEntityIDs		- list of entity id (one col, every row id)
	\param lstFilters		- list of filters (one col, every row filter record)
	\param Ret_Data			- returned data   (cols, every row returned data, STATUS_CODE,STATUS_TEXT) 
	\param pDbConn			- db connection

*/
void Service_MainEntitySelector::ReadDataBatch(Status &Ret_pStatus, DbRecordSet lstEntityIDs, DbRecordSet lstFilters, DbRecordSet &Ret_Data)
{
	//Reserve connection.
	DbConnectionReserver conn(Ret_pStatus,GetDbManager()); //reserve global connection
	if(!Ret_pStatus.IsOK())return;

	Ret_Data.destroy();
	Ret_Data.addColumn(DbRecordSet::GetVariantType(),"RESULT");
	Ret_Data.addColumn(QVariant::Int,"STATUS_CODE");
	Ret_Data.addColumn(QVariant::String,"STATUS_TEXT");

	DbRecordSet lstFilter,lstResult;
	int nSize=lstEntityIDs.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		//DbRecordSet lstResult;
		lstResult.destroy();
		int nID=lstEntityIDs.getDataRef(i,0).toInt();
		//DbRecordSet lstFilter=lstFilters.getDataRef(i,0).value<DbRecordSet>();
		ReadData(Ret_pStatus,nID,lstFilter,lstResult);
		Ret_Data.addRow();
		int nRow=Ret_Data.getRowCount()-1;
		Ret_Data.setData(i,0,lstResult);
		Ret_Data.setData(i,1,Ret_pStatus.getErrorCode());
		Ret_Data.setData(i,2,Ret_pStatus.getErrorText());
	}
}


void Service_MainEntitySelector::ReadDMApplications(Status &Ret_pStatus, int nEntityID, DbRecordSet Filter, DbRecordSet &Ret_Data)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_DM_APPLICATIONS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	//if filter exists try to set filter:
	QString strWhere;
	if(Filter.getRowCount()!=0)
	{
		strWhere=MainEntityFilter::GetWhereStatement(Filter,true);
	}
	strWhere+=" ORDER BY BDMA_NAME";	//default result sorting

	//load all: make select view: id,code, name
	TableOrm.Read(Ret_pStatus,Ret_Data,strWhere,DbSqlTableView::TVIEW_BUS_DM_APPLICATIONS);
	if(!Ret_pStatus.IsOK()) return;


}

//SAPNE opens pop selector, all data must be displayed on selector popup, but question is: what is it in cache?
//method: send count from cache, if same on server, nothing returned, if not equeal, all is reloaded.
//method: compare Last modify dates: get maximum from cache and from server if server >, reload
//NOTE: datClientCacheTime is not tested, as client cache doesnt have LM field->to much code rewrite for this
void Service_MainEntitySelector::ReadDataAll(Status &Ret_pStatus, int nEntityID,int nCountInClientCache, QDateTime datClientCacheTime, DbRecordSet &Ret_Data)
{
	DbConnectionReserver conn(Ret_pStatus,GetDbManager());
	if(!Ret_pStatus.IsOK()) return;

	int nServerCount=GetCountForTable(Ret_pStatus,nEntityID);
	if(!Ret_pStatus.IsOK()) return;

	QDateTime datServer=GetLastModifyDateForTable(Ret_pStatus,nEntityID);
	if(!Ret_pStatus.IsOK()) return;

	//if server count different, go go
	if (nServerCount>nCountInClientCache || (nServerCount==0 && nCountInClientCache==0)) //for assembled data!
	{
		DbRecordSet empty;
		ReadData(Ret_pStatus,nEntityID,empty,Ret_Data);
		return;
	}

	//if count is same, compare LMs only if server count>0
	/*
	if (nServerCount>0 && nServerCount == nCountInClientCache && datServer>datClientCacheTime)
	{
		DbRecordSet empty;
		ReadData(Ret_pStatus,nEntityID,empty,Ret_Data);
		return;
	}
	*/

	//if passed 1 & 2, client cache is actual, do not return nothing
}


//When read, SAPNE must get details for one ID: e.g. person name, code for person ID
//If not found in cache, SAPNE will try to load from server
//To optimize: if number od records are relativly small (e.g. <50), reload whole group, this can boot performance for many repetitive readings of entities with many SAPNE's
//Filter contains filter for that 1 record
void Service_MainEntitySelector::ReadDataOne(Status &Ret_pStatus, int nEntityID,DbRecordSet Filter, DbRecordSet &Ret_Data)
{
	DbConnectionReserver conn(Ret_pStatus,GetDbManager());
	if(!Ret_pStatus.IsOK()) return;

	int nServerCount=GetCountForTable(Ret_pStatus,nEntityID);
	if(!Ret_pStatus.IsOK()) return;

	if (nServerCount<500) //hardcoded 500 as limit, reload all:
	{
		DbRecordSet empty;
		ReadData(Ret_pStatus,nEntityID,empty,Ret_Data);
		return;
	}
	else		//reload one:
	{
		ReadData(Ret_pStatus,nEntityID,Filter,Ret_Data);
		return;
	}

}


QDateTime Service_MainEntitySelector::GetLastModifyDateForTable(Status &err,int nEntityID)
{

	QString strSQL;
	QDateTime datOld(QDate(1900,1,1));


	switch(nEntityID)
	{
	case ENTITY_BUS_PROJECT:
		strSQL="SELECT MAX(BUSP_DAT_LAST_MODIFIED) FROM BUS_PROJECTS";
		break;
	case ENTITY_BUS_PERSON:
		strSQL="SELECT MAX(BPER_DAT_LAST_MODIFIED) FROM BUS_PERSON";
		break;
	case ENTITY_CORE_USER:
		strSQL="SELECT MAX(CUSR_DAT_LAST_MODIFIED) FROM CORE_USER";
		break;
	case ENTITY_BUS_ORGANIZATION:
		strSQL="SELECT MAX(BORG_DAT_LAST_MODIFIED) FROM BUS_ORGANIZATIONS";
		break;
	case ENTITY_BUS_CM_TYPES:
		strSQL="SELECT MAX(BCMT_DAT_LAST_MODIFIED) FROM BUS_CM_TYPES";
		break;
	case ENTITY_BUS_DEPARTMENT:
		strSQL="SELECT MAX(CUSR_DAT_LAST_MODIFIED) FROM CORE_USER";
		break;
	case ENTITY_CONTACT_ORGANIZATION:
	case ENTITY_CONTACT_DEPARTMENT:
	case ENTITY_CONTACT_FUNCTION:
	case ENTITY_CONTACT_PROFESSION:
	case ENTITY_BUS_CONTACT:
		strSQL="SELECT MAX(BCNT_DAT_LAST_MODIFIED) FROM BUS_CM_CONTACT";
		break;
	case ENTITY_DOC_TEMPLATE_CATEGORY:
		strSQL="SELECT MAX(BDMD_DAT_LAST_MODIFIED) FROM BUS_DM_DOCUMENTS";
		break;
	case ENTITY_EMAIL_TEMPLATE_CATEGORY:
		strSQL="SELECT MAX(BEM_DAT_LAST_MODIFIED) FROM BUS_EMAIL";
		break;
	case ENTITY_BUS_COST_CENTER:
		strSQL="SELECT MAX(BCTC_DAT_LAST_MODIFIED) FROM BUS_COST_CENTERS";
		break;
	case ENTITY_CE_TYPES:
		strSQL="SELECT MAX(CET_DAT_LAST_MODIFIED) FROM CE_TYPE";
		break;
	case ENTITY_CE_EVENT_TYPES:
		strSQL="SELECT MAX(CEVT_DAT_LAST_MODIFIED) FROM CE_EVENT_TYPE";
		break;
	case ENTITY_BUS_CM_ADDRESS_SCHEMAS:
		strSQL="SELECT MAX(BCMAS_DAT_LAST_MODIFIED) FROM BUS_CM_ADDRESS_SCHEMAS";
		break;
	case ENTITY_BUS_OPT_GRID_VIEWS:
		strSQL="SELECT MAX(BOGW_DAT_LAST_MODIFIED) FROM BUS_OPT_GRID_VIEWS";
		break;
	case ENTITY_NMRX_ROLES:
		strSQL="SELECT MAX(BNRO_DAT_LAST_MODIFIED) FROM BUS_NMRX_ROLE";
		break;
	case ENTITY_CALENDAR_EVENT_TEMPLATE_CATEGORY:
		strSQL="SELECT MAX(BCEV_DAT_LAST_MODIFIED) FROM BUS_CAL_EVENT";
		break;

/*
	case ENTITY_BUS_DM_APPLICATIONS:
		strSQL="SELECT MAX(BDMA_DAT_LAST_MODIFIED) FROM BUS_DM_APPLICATIONS";
		break;
	case ENTITY_BUS_DM_TEMPLATES: //always for logged user:
		strSQL="SELECT MAX(BDMD_DAT_LAST_MODIFIED) FROM BUS_DM_DOCUMENTS WHERE ";
		break;
*/
	default:
		return datOld;
	}


	if (strSQL.isEmpty())
		return datOld;


	//execute
	DbSqlQuery query(err, GetDbManager()); 
	if(!err.IsOK()) return datOld;
	
	query.Execute(err,strSQL);
	if(!err.IsOK()) return datOld;

	if(query.next())
		return query.value(0).toDateTime();
	else
		return datOld;

}



int Service_MainEntitySelector::GetCountForTable(Status &err,int nEntityID)
{

	QString strSQL;
	int nCount=0;


	switch(nEntityID)
	{
	case ENTITY_BUS_PROJECT:
		strSQL="SELECT COUNT(*) FROM BUS_PROJECTS";
		break;
	case ENTITY_BUS_PERSON:
		strSQL="SELECT COUNT(*) FROM BUS_PERSON";
		break;
	case ENTITY_CORE_USER:
		strSQL="SELECT COUNT(*) FROM CORE_USER";
		break;
	case ENTITY_BUS_ORGANIZATION:
		strSQL="SELECT COUNT(*) FROM BUS_ORGANIZATIONS";
		break;
	case ENTITY_BUS_CM_TYPES:
		strSQL="SELECT COUNT(*) FROM BUS_CM_TYPES";
		break;
	case ENTITY_BUS_DEPARTMENT:
		strSQL="SELECT COUNT(*) FROM CORE_USER";
		break;
	case ENTITY_CONTACT_ORGANIZATION:
	case ENTITY_CONTACT_DEPARTMENT:
	case ENTITY_CONTACT_FUNCTION:
	case ENTITY_CONTACT_PROFESSION:
	case ENTITY_BUS_CONTACT:
		strSQL="SELECT COUNT(*) FROM BUS_CM_CONTACT";
		break;
	case ENTITY_DOC_TEMPLATE_CATEGORY:
		strSQL="SELECT COUNT(*) FROM BUS_DM_DOCUMENTS";
		break;
	case ENTITY_EMAIL_TEMPLATE_CATEGORY:
		strSQL="SELECT COUNT(*) FROM BUS_EMAIL";
		break;
	case ENTITY_BUS_COST_CENTER:
		strSQL="SELECT COUNT(*) FROM BUS_COST_CENTERS";
		break;
	case ENTITY_CE_TYPES:
		strSQL="SELECT COUNT(*) FROM CE_TYPE";
		break;
	case ENTITY_CE_EVENT_TYPES:
		strSQL="SELECT COUNT(*) FROM CE_EVENT_TYPE";
		break;
	case ENTITY_BUS_CM_ADDRESS_SCHEMAS:
		strSQL="SELECT COUNT(*) FROM BUS_CM_ADDRESS_SCHEMAS";
		break;
	case ENTITY_BUS_OPT_GRID_VIEWS:
		strSQL="SELECT COUNT(*) FROM BUS_OPT_GRID_VIEWS";
		break;
	case ENTITY_NMRX_ROLES:
		strSQL="SELECT COUNT(*) FROM BUS_NMRX_ROLE";
		break;
	case ENTITY_CALENDAR_EVENT_TEMPLATE_CATEGORY:
		strSQL="SELECT COUNT(*) FROM BUS_CAL_EVENT";
		break;
/*
	case ENTITY_BUS_DM_APPLICATIONS:
		strSQL="SELECT MAX(BDMA_DAT_LAST_MODIFIED) FROM BUS_DM_APPLICATIONS";
		break;
	case ENTITY_BUS_DM_TEMPLATES: //always for logged user:
		strSQL="SELECT MAX(BDMD_DAT_LAST_MODIFIED) FROM BUS_DM_DOCUMENTS WHERE ";
		break;
*/
	default:
		return nCount;
	}


	if (strSQL.isEmpty())
		return nCount;


	//execute
	DbSqlQuery query(err, GetDbManager()); 
	if(!err.IsOK()) return nCount;
	
	query.Execute(err,strSQL);
	if(!err.IsOK()) return nCount;

	if(query.next())
		return query.value(0).toInt();
	else
		return nCount;

}



void Service_MainEntitySelector::ReadContactGroups(Status &Ret_pStatus, int nEntityID, DbRecordSet Filter, DbRecordSet &Ret_Data)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_CM_GROUP, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;


	//if filter exists try to set filter:
	QString strWhere;
	if(Filter.getRowCount()!=0)
	{
		//Filter.Dump();
		strWhere=MainEntityFilter::GetWhereStatement(Filter,true);
	}
	strWhere+=" ORDER BY BGTR_NAME,BGIT_CODE,BGIT_NAME";

	//load all
	TableOrm.Read(Ret_pStatus,Ret_Data,strWhere,DbSqlTableView::TVIEW_BUS_CM_GROUP_SELECT);

	if(!Ret_pStatus.IsOK()) return;


	//calculated names: 
	//MainEntityCalculatedName filler;
	//filler.Initialize(ENTITY_BUS_CM_GROUP,Ret_Data,Ret_Data.getColumnIdx("BGIT_CODE"),Ret_Data.getColumnIdx("BGIT_NAME"));
	//filler.FillCalculatedNames(Ret_Data,Ret_Data.getColumnIdx("BGIT_ITEM_NAME"));
}


void Service_MainEntitySelector::ReadGroupItems(Status &Ret_pStatus, int nEntityID, DbRecordSet Filter, DbRecordSet &Ret_Data)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_GROUP_ITEMS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;


	//if filter exists try to set filter:
	QString strWhere;
	if(Filter.getRowCount()!=0)
	{
		//Filter.Dump();
		strWhere=MainEntityFilter::GetWhereStatement(Filter,true);
	}

	//load all
	TableOrm.Read(Ret_pStatus,Ret_Data,strWhere); //,DbSqlTableView::TVIEW_BUS_CM_GROUP_SELECT);

	if(!Ret_pStatus.IsOK()) return;


}

void Service_MainEntitySelector::ReadAllEmails(Status &Ret_pStatus, int nEntityID, DbRecordSet Filter, DbRecordSet &Ret_Data)
{
	//Make local query, can fail if connection reservation fails.
	DbSqlQuery query(Ret_pStatus,GetDbManager());
	if(!Ret_pStatus.IsOK()) return;

	Ret_Data.destroy();
	Ret_Data.addColumn(QVariant::String,"EMAIL");

	//from contact emails:
	DbRecordSet lstData;

	QString strSQL="SELECT DISTINCT BCME_ADDRESS FROM BUS_CM_EMAIL";
	query.Execute(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK()) return;
	query.FetchData(lstData);

	if(!Ret_pStatus.IsOK()) return;
	int nIdx=lstData.getColumnIdx("BCME_ADDRESS");
	if (nIdx>=0)
	{
		lstData.setColumn(nIdx,QVariant::String,"EMAIL");
		Ret_Data.merge(lstData);
	}
	
	/*
	//1.5mb in 6sec 800rec
	QString strSQL="SELECT * FROM BUS_DM_DOCUMENTS";
	query.Execute(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK()) return;
	lstData.destroy();
	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Start fetch all doc");
	query.FetchData(lstData);
	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"End fetch all doc");

	QByteArray byteVal;
	byteVal=XmlUtil::ConvertRecordSet2ByteArray_Fast(lstData);
	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Size data:"+QVariant(byteVal.size()).toString()+"Size rec:"+QVariant(lstData.getRowCount()).toString());
	

	//68mb in 2min 3500rec
	strSQL="SELECT * FROM BUS_EMAIL";
	query.Execute(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK()) return;
	lstData.destroy();
	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Start fetch all mail");
	query.FetchData(lstData);
	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"End fetch all mail");

	byteVal=XmlUtil::ConvertRecordSet2ByteArray_Fast(lstData);
	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Size data:"+QVariant(byteVal.size()).toString()+"Size rec:"+QVariant(lstData.getRowCount()).toString());


	//1mb =40 sec
	QString strSQL="SELECT BEM_FROM,BEM_TO,BEM_CC,BEM_BCC FROM BUS_EMAIL";
	query.Execute(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK()) return;
	lstData.destroy();
	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Start fetch TO");
	query.FetchData(lstData);
	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"End fetch TO");

	QByteArray byteVal=XmlUtil::ConvertRecordSet2ByteArray_Fast(lstData);
	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Size data:"+QVariant(byteVal.size()).toString()+"Size rec:"+QVariant(lstData.getRowCount()).toString());


	//from from/to bcc
	strSQL="SELECT BEM_FROM,BEM_TO,BEM_CC,BEM_BCC FROM BUS_EMAIL";
	query.Execute(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK()) return;
	lstData.destroy();
	query.FetchData(lstData);
	int nSize=lstData.getRowCount();
	for(int i=0;i<nSize;i++)
	{
		DbRecordSet rowData;
		EmailHelperCore::ExtractMailsFromEmailRow(lstData.getRow(i),rowData);
		Ret_Data.merge(rowData);
	}

	*/
	Ret_Data.removeDuplicates(0);

}

void Service_MainEntitySelector::ReadResources(Status &Ret_pStatus, int nEntityID, DbRecordSet Filter, DbRecordSet &Ret_Data)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_CAL_RESOURCE, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	//if filter exists try to set filter:
	QString strWhere;
	if(Filter.getRowCount()!=0)
	{
		strWhere=MainEntityFilter::GetWhereStatement(Filter,true);
	}

	//load all
	TableOrm.Read(Ret_pStatus,Ret_Data,strWhere,DbSqlTableView::TVIEW_BUS_CAL_RESOURCE_SELECT);
	if(!Ret_pStatus.IsOK()) return;
}


void Service_MainEntitySelector::ReadCategoryCalendar(Status &Ret_pStatus, int nEntityID, DbRecordSet Filter, DbRecordSet &Ret_Data)
{
	//make local query, can fail if connection reservation fails
	DbSqlQuery query(Ret_pStatus,GetDbManager());
	if(!Ret_pStatus.IsOK()) return;

	//prepare insert query:
	QString strSQL="SELECT DISTINCT BCEV_CATEGORY FROM BUS_CAL_EVENT WHERE BCEV_CATEGORY<>'' AND NOT(BCEV_CATEGORY IS NULL) ORDER BY BCEV_CATEGORY";

	//exec & auto destroy any connection held:
	query.Execute(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK()) return;

	//fetch result:
	Ret_Data.destroy();
	Ret_Data.addColumn(QVariant::String,"NAME");

	query.FetchData(Ret_Data);

}

void Service_MainEntitySelector::ReadEmailTemplates(Status &Ret_pStatus, int nEntityID, DbRecordSet Filter, DbRecordSet &Ret_Data)
{
	Status err;
	QString strWhere =" WHERE BEM_TEMPLATE_FLAG=1 ";
	g_AccessRight->SQLFilterRecords(BUS_EMAIL,strWhere);
	g_BusinessServiceSet->ClientSimpleORM->Read(err,BUS_EMAIL,Ret_Data,strWhere,DbSqlTableView::TVIEW_BUS_EMAIL_TEMPLATES);

	SortDataList lstSort;
	lstSort<<SortData(Ret_Data.getColumnIdx("BEM_CATEGORY"),0);
	lstSort<<SortData(Ret_Data.getColumnIdx("BEM_TEMPLATE_NAME"),0);
	Ret_Data.sortMulti(lstSort);

}

void Service_MainEntitySelector::ReadPersonEmailAddresses(Status &Ret_pStatus, int nEntityID, DbRecordSet Filter, DbRecordSet &Ret_Data)
{
	//if filter exists try to set filter:
	QString strWhere;
	if(Filter.getRowCount()!=0)
	{
		//Filter.Dump();
		strWhere=MainEntityFilter::GetWhereStatement(Filter,true);
	}

	g_BusinessServiceSet->ClientSimpleORM->Read(Ret_pStatus,BUS_CM_EMAIL,Ret_Data,strWhere,DbSqlTableView::TVIEW_BUS_CM_EMAIL_SELECT);
}


void Service_MainEntitySelector::ReadPaymentConditions(Status &Ret_pStatus, int nEntityID, DbRecordSet Filter, DbRecordSet &Ret_Data)
{
	//if filter exists try to set filter:
	QString strWhere;
	if(Filter.getRowCount()!=0)
	{
		//Filter.Dump();
		strWhere=MainEntityFilter::GetWhereStatement(Filter,true);
	}

	g_BusinessServiceSet->ClientSimpleORM->Read(Ret_pStatus,BUS_CM_PAYMENT_CONDITIONS,Ret_Data,strWhere,DbSqlTableView::TVIEW_BUS_CM_PAYMENT_CONDITIONS_SELECT);

}

void Service_MainEntitySelector::ReadCustomFields(Status &Ret_pStatus, int nEntityID, DbRecordSet Filter, DbRecordSet &Ret_Data)
{
	//if filter exists try to set filter:
	QString strWhere;
	if(Filter.getRowCount()!=0)
	{
		//Filter.Dump();
		strWhere=MainEntityFilter::GetWhereStatement(Filter,true);
	}

	g_BusinessServiceSet->ClientSimpleORM->Read(Ret_pStatus,BUS_CUSTOM_FIELDS,Ret_Data,strWhere,DbSqlTableView::TVIEW_BUS_CUSTOM_FIELDS_SELECT);

	SortDataList lstSort;
	lstSort<<SortData(Ret_Data.getColumnIdx("BCF_TABLE_ID"),0);
	lstSort<<SortData(Ret_Data.getColumnIdx("BCF_NAME"),0);
	Ret_Data.sortMulti(lstSort);

}

