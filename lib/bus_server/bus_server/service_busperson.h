#ifndef SERVICE_BUSPERSON_H
#define SERVICE_BUSPERSON_H

#include "businessservice.h"
#include "bus_interface/bus_interface/interface_busperson.h"


class Service_BusPerson : public Interface_BusPerson, public BusinessService
{
public:
	void ReadData(Status &Ret_pStatus, int nPersonID, DbRecordSet &Ret_Data,DbRecordSet &Ret_CoreUserData, DbRecordSet &Ret_RoleList);
	void WriteData(Status &Ret_pStatus, DbRecordSet &RetOut_Data, DbRecordSet &RetOut_CoreUserData, QString pLockResourceID="", int nPersonRoleId=-1);
	void GetPersonProjectListID(Status &Ret_pStatus, int nPersonID,int &Ret_nListID);

private:
	
};

#endif // SERVICE_BUSPERSON_H
