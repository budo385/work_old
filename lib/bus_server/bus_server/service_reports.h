#ifndef SERVICE_REPORTS_H
#define SERVICE_REPORTS_H

#include "bus_interface/bus_interface/interface_reports.h"
#include "businessservice.h"

class Service_Reports : public Interface_Reports, public BusinessService
{
public:
	~Service_Reports(){};

	void GetReportRecordSet(Status &pStatus, DbRecordSet &pReportRecordSet, int nViewID, QString strWhere);
    
};

#endif // SERVICE_REPORTS_H
