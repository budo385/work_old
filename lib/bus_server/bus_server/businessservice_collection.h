#ifndef SERVICE_COLLECTION_H
#define SERVICE_COLLECTION_H


//-----------------------------------------------------------
//Holds header for all server side business objects
//-----------------------------------------------------------

#include "service_userlogon.h"
#include "service_modulelicense.h"
#include "service_buseventlog.h"
#include "service_accessrights.h"
#include "service_mainentityselector.h"
#include "service_coreservices.h"
#include "service_buscontact.h"
#include "service_reports.h"
#include "service_voicecallcenter.h"
#include "service_busemail.h"
#include "service_busaddressschemas.h"
#include "service_clientsimpleorm.h"
#include "service_busimport.h"
#include "service_storedprojectlist.h"
#include "service_busgridviews.h"
#include "service_busnmrx.h"
#include "service_busgrouptree.h"
#include "service_busdocuments.h"
#include "service_buscommunication.h"
#include "service_servercontrol.h"
#include "service_busproject.h"
#include "service_busperson.h"
#include "service_bussms.h"
#include "service_buscalendar.h"
#include "service_webcontacts.h"
#include "service_webdocuments.h"
#include "service_webemails.h"
#include "service_webserver.h"
#include "service_webprojects.h"
#include "service_webcalendars.h"
#include "service_websessiondata.h"
#include "service_webcommunication.h"
#include "service_buscustomfields.h"
#include "service_spcbusinessfigures.h"
#include "service_spcsession.h"
#include "service_webmwcloudservice.h"

#endif //SERVICE_COLLECTION_H
