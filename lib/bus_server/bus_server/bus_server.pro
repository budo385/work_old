# ----------------------------------------------------
# This file is generated by the Qt Visual Studio Add-in.
# ------------------------------------------------------

TEMPLATE = lib
TARGET = bus_server
DESTDIR = ../../../exe/appserver/Debug
QT += core sql network xml
CONFIG += staticlib release
DEFINES += QT_SQL_LIB QT_NETWORK_LIB BUS_SERVER_LIB _TEST_DATABASE
INCLUDEPATH += ./GeneratedFiles \
    ./GeneratedFiles/Debug \
    ./../../../lib \
    ./GeneratedFiles/$(Configuration) \
    . \
    ./../../../lib/openssl/win32_build/include
LIBS += -L"."
DEPENDPATH += .
MOC_DIR += ./GeneratedFiles/debug
OBJECTS_DIR += debug
UI_DIR += ./GeneratedFiles
RCC_DIR += ./GeneratedFiles
include(bus_server.pri)
