#include "service_clientsimpleorm.h"
#include "common/common/datahelper.h"
#include "db_core/db_core/dbsqltabledefinition.h"
#include "businesslocker.h"
#include "db/db/dbsimpleorm.h"
#include "hierarchicaldata.h"

#include "privateserviceset.h"
extern PrivateServiceSet* g_PrivateServiceSet; 

/*!
	Write to database from &pLstForWrite into nTableID, return details in &Ret_pLstStatusRows and error in pStatus.
	Write records by FULL table view.
	Note: pLstForWrite must contain mandatory field LastModified if table has one, otherwise not.

	\param Ret_pStatus		- error
	\param nTableID			- Table ID.
	\param Ret_pLstForWrite	- list of records for write (it will return from server in same format with new ID's etc..)
	\param pLockResourceID	- Locked resource ID - if you locked something before. If empty Write() will try to lock and abort if fails. It will unlock if locked from outside.
	\param pLstStatusRows	- list of statuses for each row 
	\param nQueryView		- instead of using full table view (table), record set can be written by nQueryView (e.g. to update only some columns ), -1 : use full view
	\param nSkipLastColumns	- number of columns to skip from end of recordset (query must be valid, but recordset can be expanded with dynamic cols)
	\param RetOut_pLstForDelete	- list of records for delete (same format or  only ID)
	
	\param pDbConn			- pointer to database connection.
*/
void Service_ClientSimpleORM::Write(Status &Ret_pStatus, int nTableID, DbRecordSet &Ret_pLstForWrite, QString pLockResourceID /*= QString()*/, int nQueryView /*= -1*/, int nSkipLastColumns /*= 0*/, DbRecordSet &RetOut_pLstForDelete /*= NULL*/)
{
	Status err;

	//Locked locally.
	bool bLockedFromHere = false;
	//Make local simple ORM and business locker object (locker must be made in case records are not already locked).
	DbSimpleOrm TableOrm(Ret_pStatus, nTableID, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) 
		return;

	BusinessLocker Locker(Ret_pStatus, nTableID, GetDbManager());
	if(!Ret_pStatus.IsOK()) 
		return;
	
	//--------------------------------------
	//	Check is it previously locked. 
	//	If not lock it.
	//	Always fire lock: if insert it will return with empty pLockResourceID
	//--------------------------------------
	if (pLockResourceID.isEmpty())
	{
		Locker.Lock(Ret_pStatus, Ret_pLstForWrite, pLockResourceID);
		if(!Ret_pStatus.IsOK()) 
			return;
		bLockedFromHere = true;
	}

	//--------------------------------------
	//	WRITE.
	//--------------------------------------
	//begin transaction:
	TableOrm.GetDbSqlQuery()->BeginTransaction(Ret_pStatus);
	if(!Ret_pStatus.IsOK()) 
	{
		if (bLockedFromHere && !pLockResourceID.isEmpty())
			Locker.UnLock(err, pLockResourceID);
		return;
	}

	TableOrm.Write(Ret_pStatus, Ret_pLstForWrite, nQueryView, nSkipLastColumns);
	if(!Ret_pStatus.IsOK()) 
	{
		TableOrm.GetDbSqlQuery()->Rollback();
		if (bLockedFromHere && !pLockResourceID.isEmpty())
			Locker.UnLock(err, pLockResourceID);
		return;
	}

	//--------------------------------------
	//	DELETE IF NEEDED
	//--------------------------------------
	if (RetOut_pLstForDelete.getRowCount()>0)
	{
		TableOrm.DeleteFast(Ret_pStatus, RetOut_pLstForDelete);
		if(!Ret_pStatus.IsOK()) 
		{
			TableOrm.GetDbSqlQuery()->Rollback();
			if (bLockedFromHere && !pLockResourceID.isEmpty())
				Locker.UnLock(err, pLockResourceID);
			return;
		}

	}

	//commit tran
	TableOrm.GetDbSqlQuery()->Commit(Ret_pStatus);
	if(!Ret_pStatus.IsOK()) 
	{
		if (bLockedFromHere && !pLockResourceID.isEmpty())
			Locker.UnLock(err, pLockResourceID);
		return;
	}

	//--------------------------------------
	//	IF ALL WENT FINE UNLOCK.
	//--------------------------------------
	if (!pLockResourceID.isEmpty())
		Locker.UnLock(Ret_pStatus, pLockResourceID);


}

/*!
	Read records by FULL view
	\param Ret_pStatus		- Error.
	\param nTableID			- Table ID.
	\param Ret_pLstRead		- list of records for read, list is recreated 1:1 to full table view query result 
	\param strWhereClause	- additional WHERE clause
	\param nQueryView		- instead of using full table view (table), record set can be fetched by nQueryView
	\param bDistinct		- Distinct clause.
	\param pDbConn			- pointer to database connection.
*/
void Service_ClientSimpleORM::Read(Status &Ret_pStatus, int nTableID, DbRecordSet &Ret_pLstRead, QString strWhereClause /*= QString()*/, int nQueryView /*= -1*/, bool bDistinct /*= false*/ /*= NULL*/)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, nTableID, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) 
		return;
	//Read.
	TableOrm.Read(Ret_pStatus, Ret_pLstRead, strWhereClause, nQueryView, bDistinct);
}

/*!
	Delete records from &pLstForDelete, return details in &pLstStatusRows and error in pStatus.
	Write records by FULL table view.

	\param pStatus			- error
	\param nTableID			- Table ID.
	\param pLstForDelete	- Recordset of rows to delete.
	\param pLockResourceID	- Locked resource ID - if you locked something before. If empty Write() will try to lock and abort if fails. It will not unlock if locked from outside.
	\param pLstStatusRows	- list of statuses for each row 
	\param pBoolTransaction - 1 all or nothing
	\param pBoolLock		- lock before write
	\param pDbConn			- pointer to database connection.
*/
void Service_ClientSimpleORM::Delete(Status &Ret_pStatus, int nTableID, DbRecordSet &pLstForDelete, QString pLockResourceID /*= QString()*/, DbRecordSet &Ret_pLstStatusRows /*= DbRecordSet()*/, bool pBoolTransaction /*= true*/ /*= NULL*/)
{
	Status err;

	//Locked locally.
	bool bLockedFromHere = false;
	//Make local simple ORM and business locker object (locker must be made in case records are not already locked).
	DbSimpleOrm TableOrm(Ret_pStatus, nTableID, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) 
		return;

	BusinessLocker Locker(Ret_pStatus, nTableID, GetDbManager());
	if(!Ret_pStatus.IsOK()) 
		return;

	//--------------------------------------
	//	Check is it previously locked. 
	//	If not lock it.
	//--------------------------------------
	if (pLockResourceID.isEmpty())
	{
		Locker.Lock(Ret_pStatus, pLstForDelete, pLockResourceID);
		if(!Ret_pStatus.IsOK()) 
			return;
		bLockedFromHere = true;
	}

	//--------------------------------------
	//	DELETE: always all
	//--------------------------------------
	TableOrm.DeleteFast(Ret_pStatus, pLstForDelete /*&Ret_pLstStatusRows, BT. changed again...pBoolTransaction*/);
	if(!Ret_pStatus.IsOK()) 
	{
		//If locked from here then unlock records.
		if (bLockedFromHere)
			Locker.UnLock(err, pLockResourceID);
		return;
	}

	//--------------------------------------
	//	IF ALL WENT FINE UNLOCK.
	//--------------------------------------
	if (bLockedFromHere)
		Locker.UnLock(Ret_pStatus, pLockResourceID);
}

/*!
	Lock records, can lock all or nothing or just partial, in later case it will return list with records that are already locked

	\param pStatus				- return error
	\param TableID				- table id
	\param LockList				- Recordset for locking.
	\param Ret_pLockResourceID	- Locked resource ID - returning parameter.
	\param strLockedResourceID	- returned resource id
	\param pLstStatusRows		- filled only with bad ones (doesn't succeed in locking) if bAllOrNothing= false
	\param bAllOrNothing		- true means all or nothing, else pLstStatusRows is filled with bad ones
	\param pDbConn				- pointer to database connection.
*/
void Service_ClientSimpleORM::Lock(Status &Ret_pStatus, int nTableID, DbRecordSet &pLstForLocking, QString &Ret_pResourceID, DbRecordSet &Ret_pLstStatusRows /*= DbRecordSet()*/, bool bAllOrNothing /*= true*/ /*= NULL*/)
{
	BusinessLocker Locker(Ret_pStatus, nTableID, GetDbManager());
	if(!Ret_pStatus.IsOK()) 
		return;

	Locker.Lock(Ret_pStatus, pLstForLocking, Ret_pResourceID, &Ret_pLstStatusRows, bAllOrNothing);
	if(!Ret_pStatus.IsOK()) 
		return;
}

/*!
	Unlock records.

	\param Ret_pStatus			- return error
	\param nTableID				- table id
	\param Ret_bUnLocked		- return value (true/false).
	\param pResourceID			- Locked resource ID (to unlock).
	\param pDbConn				- pointer to database connection.
*/
void Service_ClientSimpleORM::UnLock(Status &Ret_pStatus, int nTableID, bool &Ret_bUnLocked, QString &pResourceID /*= NULL*/)
{
	BusinessLocker Locker(Ret_pStatus, nTableID, GetDbManager());
	if(!Ret_pStatus.IsOK()) 
		return;

	Ret_bUnLocked = Locker.UnLock(Ret_pStatus, pResourceID);
}

void Service_ClientSimpleORM::UnLockByRecordID(Status &Ret_pStatus, int nTableID, DbRecordSet &pLstForUnLocking, bool &Ret_bUnLocked)
{
	BusinessLocker Locker(Ret_pStatus, nTableID, GetDbManager());
	if(!Ret_pStatus.IsOK()) 
		return;

	//unlock them all, one by one in multiple sql-s..not so demanding funct, so we can, as Obama, the black magician says: yes we can!!
	Ret_bUnLocked=false; //only one is enough
	int nSize = pLstForUnLocking.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		if(Locker.UnLock(Ret_pStatus, nTableID, pLstForUnLocking.getDataRef(i,0).toInt()))
			Ret_bUnLocked=true;
	}
		
}

/*!
	Is locked.

	\param Ret_pStatus			- return error
	\param nTableID				- table id
	\param Ret_bLocked			- return value (true/false).
	\param LockList				- Locked resource ID (to check).
	\param pDbConn				- pointer to database connection.
*/
void Service_ClientSimpleORM::IsLocked(Status &Ret_pStatus, int nTableID, bool &Ret_bLocked, DbRecordSet &LockList /*= NULL*/)
{
	BusinessLocker Locker(Ret_pStatus, nTableID, GetDbManager());
	if(!Ret_pStatus.IsOK()) 
		return;

	int PrimaryColumnNumber = GetPrimaryColumnNo(nTableID, LockList);
	QList<int> LockListLocal;
	int rowCount = LockList.getRowCount();
	for (int i = 0; i < rowCount; ++i)
		LockListLocal << LockList.getDataRef(i, PrimaryColumnNumber).toInt();

	Ret_bLocked = Locker.IsLocked(Ret_pStatus, nTableID, LockListLocal);
}

int Service_ClientSimpleORM::GetPrimaryColumnNo(int nTableID, DbRecordSet &pLst)
{
	//table data
	DbTableKeyData	TableKeyData;
	QString			strPrimaryKey;
	DbSqlTableDefinition::GetKeyData(nTableID, TableKeyData);
	strPrimaryKey = TableKeyData.m_strPrimaryKey;

	//find pk column inside list
	int nPrimaryKeyColumn= pLst.getColumnIdx(strPrimaryKey);
	Q_ASSERT_X(nPrimaryKeyColumn!=-1,"SIMPLE_ORM", "Primary key column not found");
	return nPrimaryKeyColumn;
}

void Service_ClientSimpleORM::ReadFromParentIDs(Status &Ret_pStatus, int nTableID,DbRecordSet &Ret_pLstRead, QString strColFK2ParentIDName, DbRecordSet &pLstOfID,QString strColIDName,QString strWhereClauseBefore,QString strWhereClauseAfter,int nQueryView, bool bDistinct)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, nTableID, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) 
		return;

	//Read.
	TableOrm.ReadFromParentIDs(Ret_pStatus, Ret_pLstRead, strColFK2ParentIDName,pLstOfID,strColIDName,strWhereClauseBefore,strWhereClauseAfter,nQueryView,bDistinct);
}



//reads data based on record set defintion & join clause 
//- define RetOut_pLstRead with columns you want, if RetOut_pLstRead has no columns it will look like: SELECT * FROM ....
//- strJoinClause set to join all tables + where statement: e.g. " FROM TABLE1, TABLE2 WHERE X=Y, A=Z" ,etc...

void Service_ClientSimpleORM::ReadAdv (Status &Ret_pStatus, DbRecordSet &RetOut_pLstRead, QString strJoinClause )
{

	//Make local query, can fail if connection reservation fails.
	DbSqlQuery query(Ret_pStatus,GetDbManager());
	if(!Ret_pStatus.IsOK()) return;

	QString strSQLFields=DataHelper::getSQLColumnsFromRecordSet(RetOut_pLstRead);
	if (strSQLFields.isEmpty())
		strSQLFields="*";


	QString strSQL="SELECT "+strSQLFields+" "+strJoinClause;

	query.Execute(Ret_pStatus,strSQL);

	if (Ret_pStatus.IsOK())
	{
		query.FetchData(RetOut_pLstRead);
	}
}

void Service_ClientSimpleORM::GetRowCount(Status &Ret_pStatus, int nTableID, QString strWhereClause, int &Ret_nCount)
{
	//Make local query, can fail if connection reservation fails.
	DbSqlQuery query(Ret_pStatus,GetDbManager());
	if(!Ret_pStatus.IsOK()) return;

	QString strSQL="SELECT COUNT(*) FROM " + DbSqlTableDefinition::GetTableName(nTableID) + " " + strWhereClause;
	query.Execute(Ret_pStatus, strSQL);

	Ret_nCount = -1;

	if (Ret_pStatus.IsOK())
	{
		DbRecordSet lstRead;
		query.FetchData(lstRead);
		if(lstRead.getRowCount()>0)
			Ret_nCount = lstRead.getDataRef(0,0).toInt();
	}
}


//simple: execute SQL as is...if there are any results returns them
void Service_ClientSimpleORM::ExecuteSQL(Status &Ret_pStatus, QString strSQL, DbRecordSet &Ret_LstReturn)
{
	//make local query, can fail if connection reservation fails
	DbSqlQuery query(Ret_pStatus,GetDbManager());
	if(!Ret_pStatus.IsOK()) return;

	query.Execute(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK()) return;

	query.FetchData(Ret_LstReturn);

}


//Writes all data from list which is filtered by parent id inside parent fk column
//op: insert new, edit old, if not exists on DB, deletes from Db based on parent id
//RetOut_pLstForWrite for write
//pLockResourceID unlock after write (if locked already, if not lock internally)
//nQueryView view in which RetOut_pLstForWrite is defined (can contain skip write cols)
void Service_ClientSimpleORM::WriteParentSubData(Status &Ret_pStatus, int nTableID, DbRecordSet &RetOut_pLstForWrite,int nParentId, QString strColParentName, QString pLockResourceID,int nQueryView)
{
	Status err;
	bool bLockedFromHere = false;
	DbSimpleOrm TableOrm(Ret_pStatus, nTableID, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	BusinessLocker Locker(Ret_pStatus, nTableID, GetDbManager());
	if(!Ret_pStatus.IsOK()) return;

	//--------------------------------------
	//	Check is it previously locked. 
	//	If not lock it.
	//	Always fire lock: if insert it will return with empty pLockResourceID
	//--------------------------------------
	if (pLockResourceID.isEmpty())
	{
		Locker.Lock(Ret_pStatus, RetOut_pLstForWrite, pLockResourceID);
		if(!Ret_pStatus.IsOK()) 
			return;
		bLockedFromHere = true;
	}
	//--------------------------------------
	//	WRITE.
	//--------------------------------------
	//begin transaction:
	TableOrm.GetDbSqlQuery()->BeginTransaction(Ret_pStatus);
	if(!Ret_pStatus.IsOK()) 
	{
		if (bLockedFromHere && !pLockResourceID.isEmpty())
			Locker.UnLock(err, pLockResourceID);
		return;
	}

	int nSkipLastCols=0;
	if (nQueryView!=-1)
		nSkipLastCols=DbSqlTableView::getView(nQueryView).m_nSkipLastColsWrite;
	
	TableOrm.WriteSubData(Ret_pStatus,nTableID,RetOut_pLstForWrite,strColParentName,nParentId,true,nSkipLastCols,"");
	if(!Ret_pStatus.IsOK()) 
	{
		if (bLockedFromHere && !pLockResourceID.isEmpty())
			Locker.UnLock(err, pLockResourceID);
		TableOrm.GetDbSqlQuery()->Rollback();
		return;
	}

	//commit tran
	TableOrm.GetDbSqlQuery()->Commit(Ret_pStatus);
	if(!Ret_pStatus.IsOK()) 
	{
		if (bLockedFromHere && !pLockResourceID.isEmpty())
			Locker.UnLock(err, pLockResourceID);
		return;
	}
	//--------------------------------------
	//	IF ALL WENT FINE UNLOCK.
	//--------------------------------------
	if (!pLockResourceID.isEmpty())
		Locker.UnLock(Ret_pStatus, pLockResourceID);

}

//strSQLWhereFilter: additional filter if more then 1 tree in same table (gruoup items)
//nQueryView - defintion of RetOut_pLstForWrite (if -1 then 1:1 as table definition)
void Service_ClientSimpleORM::WriteHierarchicalData(Status &Ret_pStatus, int nTableID,DbRecordSet &RetOut_pLstForWrite, QString pLockResourceID, QString strSQLWhereFilter, int nQueryView)
{
	HierarchicalData HierarchicalWriter;
	HierarchicalWriter.Initialize(nTableID,GetDbManager(),nQueryView);
	HierarchicalWriter.Write(Ret_pStatus,RetOut_pLstForWrite,pLockResourceID,strSQLWhereFilter);
}
void Service_ClientSimpleORM::DeleteHierarchicalData(Status &Ret_pStatus, int nTableID,DbRecordSet &pLstForDelete, QString pLockResourceID, QString strSQLWhereFilter)
{
	HierarchicalData HierarchicalWriter;
	HierarchicalWriter.Initialize(nTableID,GetDbManager());
	HierarchicalWriter.Delete(Ret_pStatus,pLstForDelete,pLockResourceID,strSQLWhereFilter);
}
