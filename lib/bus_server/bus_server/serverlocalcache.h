#ifndef SERVERLOCALCACHE_H
#define SERVERLOCALCACHE_H

#include "common/common/dbrecordset.h"


//Note: use as local object, init with valid DbConnection

class ServerLocalCache
{
public:
	ServerLocalCache();
	bool LoadTypes(int nEntityTypeID=-1);
	QVariant GetUserTypeBasedOnSystemType(int nEntityTypeID, int nSystemTypeID);

	DbRecordSet m_lstTypes;

//private:


};

#endif // SERVERLOCALCACHE_H
