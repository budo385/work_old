#include "service_busperson.h"
#include "db/db/dbsimpleorm.h"
#include "businesslocker.h"
#include "bus_core/bus_core/optionsandsettingsorganizer.h"
#include "bus_core/bus_core/optionsandsettingsmanager.h"


#include "businessserviceset.h"
extern BusinessServiceSet* g_BusinessServiceSet;
#include "privateserviceset.h"
extern PrivateServiceSet* g_PrivateServiceSet; 


#define _EXIT_ON_ERROR_ROLLBACK_AND_UNLOCK_QUERY \
	query->Rollback(); \
	Status err; \
	if (bLockedFromHere && !pLockResourceID.isEmpty())\
	Locker.UnLock(err, pLockResourceID);\
	return;

void Service_BusPerson::ReadData(Status &Ret_pStatus, int nPersonID, DbRecordSet &Ret_Data,DbRecordSet &Ret_CoreUserData, DbRecordSet &Ret_RoleList)
{
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_PERSON, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	DbSimpleOrm TableOrmCoreUser(Ret_pStatus, CORE_USER, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	QString strWhere;
	strWhere=" WHERE BPER_ID = "+QVariant(nPersonID).toString(); 
	TableOrm.Read(Ret_pStatus,Ret_Data,strWhere);
	if(!Ret_pStatus.IsOK()) return;

	Ret_Data.addColumn(QVariant::ByteArray,"BPIC_PICTURE");
	Ret_Data.addColumn(QVariant::Int,"USER_ID");

	strWhere=" WHERE CUSR_PERSON_ID = "+QVariant(nPersonID).toString(); 
	TableOrmCoreUser.Read(Ret_pStatus,Ret_CoreUserData,strWhere);
	if(!Ret_pStatus.IsOK()) return;

	if (Ret_Data.getRowCount()>0 && Ret_CoreUserData.getRowCount()>0)
		Ret_Data.setData(0,"USER_ID",Ret_CoreUserData.getDataRef(0,"CUSR_ID"));

	g_BusinessServiceSet->AccessRights->GetPersonRoleList(Ret_pStatus,Ret_RoleList,nPersonID);
	if(!Ret_pStatus.IsOK()) return;
}

void Service_BusPerson::WriteData(Status &Ret_pStatus, DbRecordSet &RetOut_Data,DbRecordSet &RetOut_CoreUserData,QString pLockResourceID,int nPersonRoleId )
{
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_PERSON, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	DbSimpleOrm TableOrmCoreUser(Ret_pStatus, CORE_USER, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	DbSimpleOrm TableOrmSettins(Ret_pStatus, BUS_OPT_SETTINGS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;


	//lock if not already locked
	//-----------------------------------------------
	BusinessLocker Locker(Ret_pStatus, BUS_PERSON, GetDbManager());
	if(!Ret_pStatus.IsOK()) return;
	bool bLockedFromHere = false;
	if (pLockResourceID.isEmpty())
	{
		Locker.Lock(Ret_pStatus, RetOut_Data, pLockResourceID);
		if(!Ret_pStatus.IsOK()) 
			return;
		bLockedFromHere = true;
	}
	//-----------------------------------------------


	DbSqlQuery *query=TableOrm.GetDbSqlQuery();
	query->BeginTransaction(Ret_pStatus);
	if(!Ret_pStatus.IsOK()) 
	{
		Status err;
		if (bLockedFromHere && !pLockResourceID.isEmpty())
			Locker.UnLock(err, pLockResourceID);
		return;
	}

	bool bInsert=false;
	if (RetOut_Data.getDataRef(0,"BPER_ID").toInt()==0)
		bInsert=true;

	//---------------------------------------------------------------------------------------------------------------
	//when editing person: if organization is changed, change contact org if person is contact..
	//---------------------------------------------------------------------------------------------------------------

	//check if edit:
	if (RetOut_Data.getRowCount()>0)
	{
		if (!bInsert && RetOut_Data.getDataRef(0,"BPER_CONTACT_ID").toInt()!=0 && RetOut_Data.getDataRef(0,"BPER_ORGANIZATION_ID").toInt()!=0)	//if edit && contact && org id is set
		{
			//get org name from  contact
			QString strSQL="SELECT BCNT_ORGANIZATIONNAME FROM bus_cm_contact WHERE BCNT_ID="+RetOut_Data.getDataRef(0,"BPER_CONTACT_ID").toString();
			query->Execute(Ret_pStatus,strSQL);
			if(!Ret_pStatus.IsOK())
			{
				_EXIT_ON_ERROR_ROLLBACK_AND_UNLOCK_QUERY
			}

			DbRecordSet tmp;
			query->FetchData(tmp);
			QString strOrg_Contact;
			if(tmp.getRowCount()>0 && tmp.getColumnCount()>0)
			{
				strOrg_Contact=tmp.getDataRef(0,0).toString();
			}

			//get org name from  person
			strSQL="SELECT BORG_NAME FROM bus_organizations WHERE BORG_ID="+RetOut_Data.getDataRef(0,"BPER_ORGANIZATION_ID").toString();
			query->Execute(Ret_pStatus,strSQL);
			if(!Ret_pStatus.IsOK()) 
			{
				_EXIT_ON_ERROR_ROLLBACK_AND_UNLOCK_QUERY
			}
			query->FetchData(tmp);
			QString strOrg_Person;
			if(tmp.getRowCount()>0 && tmp.getColumnCount()>0)
			{
				strOrg_Person=tmp.getDataRef(0,0).toString();
			}

			if (strOrg_Contact != strOrg_Person)
			{
				strSQL="UPDATE bus_cm_contact SET BCNT_ORGANIZATIONNAME = ? WHERE BCNT_ID="+RetOut_Data.getDataRef(0,"BPER_CONTACT_ID").toString();
				query->Prepare(Ret_pStatus,strSQL);
				if(!Ret_pStatus.IsOK())
				{
					_EXIT_ON_ERROR_ROLLBACK_AND_UNLOCK_QUERY
				}

				query->bindValue(0,strOrg_Person);
				query->ExecutePrepared(Ret_pStatus);
				if(!Ret_pStatus.IsOK())
				{
					_EXIT_ON_ERROR_ROLLBACK_AND_UNLOCK_QUERY
				}

			}
		}
	}


	//---------------------------------------------------------------------------------------------------------------
	//SAVE PERSON BIG PICTURE IF ANY
	//---------------------------------------------------------------------------------------------------------------
	g_PrivateServiceSet->BusinessHelper->WriteEntityPictures(Ret_pStatus,RetOut_Data,"BPER_BIG_PICTURE_ID","BPER_PICTURE");
	if (!Ret_pStatus.IsOK())
	{
		_EXIT_ON_ERROR_ROLLBACK_AND_UNLOCK_QUERY
	}
	//---------------------------------------------------------------------------------------------------------------
	//SAVE PERSON
	//---------------------------------------------------------------------------------------------------------------
	TableOrm.Write(Ret_pStatus,RetOut_Data,-1,2); //skip last TWO columns: BPIC_PICTURE && USER_ID!!!
	if (!Ret_pStatus.IsOK())
	{
		_EXIT_ON_ERROR_ROLLBACK_AND_UNLOCK_QUERY
	}
	int nPersonID=RetOut_Data.getDataRef(0,"BPER_ID").toInt();

	//---------------------------------------------------------------------------------------------------------------
	//SAVE OR DELETE CORE USER
	//---------------------------------------------------------------------------------------------------------------
	if (RetOut_CoreUserData.getRowCount()>0)
	{
		RetOut_CoreUserData.setData(0,"CUSR_PERSON_ID",nPersonID);
		TableOrmCoreUser.Write(Ret_pStatus,RetOut_CoreUserData);
		if (!Ret_pStatus.IsOK())
		{
			_EXIT_ON_ERROR_ROLLBACK_AND_UNLOCK_QUERY
		}
		RetOut_Data.setData(0,"USER_ID",RetOut_CoreUserData.getDataRef(0,"CUSR_ID"));
	}
	else if (RetOut_Data.getDataRef(0,"USER_ID").toInt()>0) //delete user!?
	{

		QList<int> lstForDelete;
		lstForDelete<<RetOut_Data.getDataRef(0,"USER_ID").toInt();
		TableOrmCoreUser.DeleteFast(Ret_pStatus,lstForDelete);
		if (!Ret_pStatus.IsOK())
		{
			_EXIT_ON_ERROR_ROLLBACK_AND_UNLOCK_QUERY
		}
		RetOut_Data.setData(0,"USER_ID",0);
	}

	//---------------------------------------------------------------------------------------------------------------
	//DELETE EXISTING ROLES IF EDIT
	//---------------------------------------------------------------------------------------------------------------
	if (!bInsert)
	{
		DbRecordSet recRoleList;
		Status status;
		//For now only one role can be added to one user - so delete previous roles if exists - if not in insert mode.
		g_BusinessServiceSet->AccessRights->GetPersonRoleList(Ret_pStatus,recRoleList,nPersonID);
		if (!Ret_pStatus.IsOK())
		{
			_EXIT_ON_ERROR_ROLLBACK_AND_UNLOCK_QUERY
		}

		int nRoleCount = recRoleList.getRowCount();
		for (int i = 0; i < nRoleCount; ++i)
		{
			int nRoleID = recRoleList.getDataRef(i, "CROL_ID").toInt();
			g_BusinessServiceSet->AccessRights->DeleteRoleFromPerson(status, nPersonID, nRoleID);
			if (!Ret_pStatus.IsOK())
			{
				_EXIT_ON_ERROR_ROLLBACK_AND_UNLOCK_QUERY
			}
		}
	}
	//---------------------------------------------------------------------------------------------------------------
	//INSERT NEW ROLES
	//---------------------------------------------------------------------------------------------------------------
	g_BusinessServiceSet->AccessRights->InsertNewRoleToPerson(Ret_pStatus, nPersonID, nPersonRoleId);
	if (!Ret_pStatus.IsOK())
	{
		_EXIT_ON_ERROR_ROLLBACK_AND_UNLOCK_QUERY
	}

	//---------------------------------------------------------------------------------------------------------------
	//SAVE DEFAULT PERSON SETTINGS WHEN INSERT
	//---------------------------------------------------------------------------------------------------------------
	if (bInsert)
	{
		//Get personal settings recordset, insert person id in it and write it.
		DbRecordSet recSettingsRecordSet;
		OptionsAndSettingsOrganizer OptionsOrganizer;
		recSettingsRecordSet = OptionsOrganizer.GetPersonalSettings();
		int rowCount = recSettingsRecordSet.getRowCount();
		for (int i = 0; i < rowCount; ++i)
			recSettingsRecordSet.setData(i, 10, nPersonID);
		TableOrmSettins.Write(Ret_pStatus, recSettingsRecordSet);
		if (!Ret_pStatus.IsOK())
		{
			_EXIT_ON_ERROR_ROLLBACK_AND_UNLOCK_QUERY
		}
	}


	query->Commit(Ret_pStatus);

	if (!pLockResourceID.isEmpty())
		Locker.UnLock(Ret_pStatus, pLockResourceID);

}


void Service_BusPerson::GetPersonProjectListID(Status &Ret_pStatus, int nPersonID,int &Ret_nListID)
{

	//get options and settings:
	//----------------------------------

	DbRecordSet recSettings;
	DbRecordSet recOptions;
	g_PrivateServiceSet->BusinessHelper->GetOptionsAndSettings(Ret_pStatus,nPersonID,recSettings,recOptions);
	if (!Ret_pStatus.IsOK()) return;

	OptionsAndSettingsManager settings;
	settings.SetSettingsDataSource(&recSettings,nPersonID);
	settings.SetOptionsDataSource(&recOptions);

	Ret_nListID = settings.GetPersonSetting(PROJECT_SETTINGS_DEF_STORED_LIST).toInt();
	if(Ret_nListID > 0)
		return;

	//get bper record:
	//----------------------------------
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_PERSON, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	QString strWhere;
	DbRecordSet recUserData;
	strWhere=" WHERE BPER_ID = "+QVariant(nPersonID).toString(); 
	TableOrm.Read(Ret_pStatus,recUserData,strWhere);
	if(!Ret_pStatus.IsOK()) return;

	if (recUserData.getRowCount()>0)
		Ret_nListID = recUserData.getDataRef(0,"BPER_DEF_PROJECT_LIST_ID").toInt();
	if(Ret_nListID > 0)
		return;


	//get options 
	//----------------------------------
	Ret_nListID = settings.GetApplicationOption(PROJECT_OPTIONS_DEF_STORED_LIST).toInt();
	if(Ret_nListID > 0)
		return;


	Ret_nListID=0;

}