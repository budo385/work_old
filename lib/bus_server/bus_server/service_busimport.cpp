#include "bus_core/bus_core/businessdatatransformer.h"
#include "common/common/csvimportfile.h"
#include "common/common/datahelper.h"
#include "bus_core/bus_core/hierarchicalhelper.h"
#include "serverlocalcache.h"
#include "bus_core/bus_core/contacttypemanager.h"
#include "businesslocker.h"
#include "service_mainentityselector.h"
#include "common/common/entity_id_collection.h"
#include "bus_core/bus_core/countries.h"
#include "common/common/authenticator.h"
#include "bus_core/bus_core/optionsandsettingsid.h"
#include "bus_core/bus_core/globalconstants.h"

#include "businessserviceset.h"
extern BusinessServiceSet* g_BusinessServiceSet;
#include "bus_core/bus_core/usersessionmanager.h"
extern UserSessionManager	*g_UserSessionManager;
#include "privateserviceset.h"
extern PrivateServiceSet* g_PrivateServiceSet; 
#include "bus_core/bus_core/importexportmanagerabstract.h"
extern ImportExportManagerAbstract*	g_pImportExportManager;
#include "common/common/logger.h"
extern Logger g_Logger;					//global logger

#ifndef min
#define min(a,b) (((a)<(b))? (a):(b))
#endif

//MR: simple replacement for MainEntitySelectionController for two field lookup (loading only those fields)
class SimpleTableCache {
public:
	SimpleTableCache(){};
	bool Load(const QString &strTable, const QString &strKeyField, const QString &strValueField, Status &pStatus, DbSqlManager *pManager);
	QVariant GetValue(const QVariant &key);
	DbRecordSet& GetSet(){ return m_data; }

protected:
	DbRecordSet m_data;
};

bool SimpleTableCache::Load(const QString &strTable, const QString &strKeyField, const QString &strValueField, Status &pStatus, DbSqlManager *pManager)
{
	DbSqlQuery *pQuery = new DbSqlQuery(pStatus, pManager);
	if(!pStatus.IsOK()){ 
		if(pQuery) delete pQuery; 
		return false; 
	}

	QString strQuery = QString("SELECT %1, %2 FROM %3 ORDER BY %1").arg(strKeyField).arg(strValueField).arg(strTable);
	if(pQuery->Execute(pStatus, strQuery))
	{
		pQuery->FetchData(m_data);					//read all by names (if not said other).
		if(pQuery) delete pQuery;
		return true;
	}		

	if(pQuery)	delete pQuery;
	return false;
}

QVariant SimpleTableCache::GetValue(const QVariant &key)
{
	int nRow = m_data.find(0, key, true);
	if(nRow >= 0){
		return m_data.getDataRef(nRow, 1);
	}
	return QVariant();
}

//init hier
Service_BusImport::Service_BusImport()
{
	m_TreeItemManager.Initialize(BUS_PROJECT,GetDbManager());
}

void UpdateStatus(DbRecordSet &RetOut_ErrRow, Status &status)
{
	RetOut_ErrRow.setData(0, "StatusCode", status.getErrorCode());
	RetOut_ErrRow.setData(0, "StatusText", status.getErrorText());
}

/*!
	Imports contacts from list, returns status.

	\param Ret_pStatus		- return error if one fails
	\param RetOut_Dat		- data in TVIEW_BUS_CONTACT_FULL, status errors are returned
	\param nOperation		- 0 - replace existing, 1-update, 2-ignore, 3-skip if exists
	\param bCreateUsers		- true: automatically creates entries in CORE_USER,BUS_PERSON

*/
void Service_BusImport::ImportContacts(Status &Ret_pStatus, DbRecordSet &RetOut_Data, DbRecordSet &RetOut_ErrRow, int nOperation, bool bCreateUsers)
{

#ifdef _DEBUG //BT incoming list must be TVIEW_BUS_CONTACT_FULL!!!!
	DbRecordSet Test;
	Test.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CONTACT_FULL));
	Q_ASSERT(Test.getColumnCount()==RetOut_Data.getColumnCount());
#endif

	//RetOut_Data.Dump();

	Status status;
	int nTotalDuplicateCnt = 0;

	//lift me up:
	switch(nOperation)
	{
	case 0:	// REPLACE_EXISTING
		{


			//Make local query, can fail if connection reservation fails.
			DbSimpleOrm orm(status, BUS_CM_CONTACT, GetDbManager()); 
			if(!status.IsOK()){ UpdateStatus(RetOut_ErrRow, status); return; }

			BusinessLocker locker(status,BUS_CM_CONTACT,GetDbManager(),orm.GetDbConnection());
			if(!status.IsOK()){ UpdateStatus(RetOut_ErrRow, status); return; }
			QString strLockedRes;

			//for each record to write
			int nCnt = RetOut_Data.getRowCount();
			for(int i=0; i<nCnt; i++)
			{
				RetOut_ErrRow.setData(0, "ErrRow", i);

				//find all duplicate records
				DbRecordSet resultIDs;
				FindContactDuplicates(status, RetOut_Data.getRow(i), resultIDs, *orm.GetDbSqlQuery());
				if(!status.IsOK()){ 
					UpdateStatus(RetOut_ErrRow, status);
					return;
				}

		#ifdef _DEBUG
				/*
				//Test err reporting
				if(i==5){
					status.setError(1, "aaa");
					UpdateStatus(RetOut_ErrRow, status);
					return;
				}
				*/
				/*
				int nDbgCol = RetOut_Data.getColumnIdx("BCNT_LASTNAME");
				if(QString("Burlon") == RetOut_Data.getDataRef(i, "BCNT_LASTNAME").toString()){
					RetOut_Data.getRow(i).Dump();
					RetOut_Data.getDataRef(i, "LST_ADDRESS").value<DbRecordSet>().Dump();
				}
				*/
		#endif

				//prepare (single row) list to be written
				DbRecordSet lstWrite;
				lstWrite.copyDefinition(RetOut_Data);
				lstWrite.addRow();
				DbRecordSet row = RetOut_Data.getRow(i);
				lstWrite.assignRow(0, row); 
				lstWrite.setData(0, "BCNT_OWNER_ID", g_UserSessionManager->GetPersonID());

				//int nSkipLastCols=DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CONTACT_FULL).m_nSkipLastCols;
				//nSkipLastCols += 4; // account for additional columns used for grid display

				int nMatchedRows = resultIDs.getRowCount();
				if(nMatchedRows > 0)
				{
#ifdef _DEBUG
					nTotalDuplicateCnt += nMatchedRows;
					qDebug() << "ImportContacts: contact" 
						<< lstWrite.getDataRef(0, "BCNT_FIRSTNAME").toString()
						<< lstWrite.getDataRef(0, "BCNT_LASTNAME").toString()
						<< lstWrite.getDataRef(0, "BCNT_ORGANIZATIONNAME").toString()
						<< "already exsits; total dups=" 
						<< nTotalDuplicateCnt;

					QFile file("d:\\out.txt");
					file.open(QIODevice::WriteOnly|QIODevice::Append);
					file.write(lstWrite.getDataRef(0, "BCNT_FIRSTNAME").toString().toLocal8Bit());
					file.write("\t");
					file.write(lstWrite.getDataRef(0, "BCNT_LASTNAME").toString().toLocal8Bit());
					file.write("\t");
					file.write(lstWrite.getDataRef(0, "BCNT_ORGANIZATIONNAME").toString().toLocal8Bit());
					file.write("\n");
					file.close();
#endif
					//record(s) already exist, delete attached phones/addresses/email/internet
					for(int j=0; j<nMatchedRows; j++)
					{
						int nRowID = resultIDs.getDataRef(j, 0).toInt(); 
					
						//store back row ID (will overwrite if more than one match)
						RetOut_Data.setData(i, "BCNT_ID", nRowID);

						lstWrite.getDataRef(0, "BCNT_ID") = QVariant(nRowID);

						//DbLockList LockList;
						//LockList << nRowID;

						//lock record before deleting it
						locker.Lock(status, lstWrite,strLockedRes);
						if(!status.IsOK()){ UpdateStatus(RetOut_ErrRow, status); return; }

						QString strQuery;
						strQuery.sprintf("DELETE FROM BUS_CM_PHONE WHERE BCMP_CONTACT_ID=%d", nRowID);
						orm.GetDbSqlQuery()->Execute(status, strQuery);
						if(!status.IsOK()){ UpdateStatus(RetOut_ErrRow, status); return; }

						strQuery.sprintf("DELETE FROM BUS_CM_EMAIL WHERE BCME_CONTACT_ID=%d", nRowID);
						orm.GetDbSqlQuery()->Execute(status, strQuery);
						if(!status.IsOK()){ UpdateStatus(RetOut_ErrRow, status); return; }

						strQuery.sprintf("DELETE FROM BUS_CM_INTERNET WHERE BCMI_CONTACT_ID=%d", nRowID);
						orm.GetDbSqlQuery()->Execute(status, strQuery);
						if(!status.IsOK()){ UpdateStatus(RetOut_ErrRow, status); return; }

						strQuery.sprintf("DELETE FROM BUS_CM_ADDRESS WHERE BCMA_CONTACT_ID=%d", nRowID);
						orm.GetDbSqlQuery()->Execute(status, strQuery);
						if(!status.IsOK()){ UpdateStatus(RetOut_ErrRow, status); return; }

						strQuery.sprintf("DELETE FROM BUS_CM_JOURNAL WHERE BCMJ_CONTACT_ID=%d", nRowID);
						orm.GetDbSqlQuery()->Execute(status, strQuery);
						if(!status.IsOK()){ UpdateStatus(RetOut_ErrRow, status); return; }

						strQuery.sprintf("DELETE FROM BUS_CM_DEBTOR WHERE BCMD_CONTACT_ID=%d", nRowID);
						orm.GetDbSqlQuery()->Execute(status, strQuery);
						if(!status.IsOK()){ UpdateStatus(RetOut_ErrRow, status); return; }

						//add new data
						UpdateContactPhones(status, nRowID, RetOut_Data.getRow(i), orm);
						if(!status.IsOK()){ 
							UpdateStatus(RetOut_ErrRow, status); locker.UnLock(status, strLockedRes);return; 
						}
						UpdateContactEmails(status, nRowID, RetOut_Data.getRow(i), orm);
						if(!status.IsOK()){ 
							UpdateStatus(RetOut_ErrRow, status); locker.UnLock(status, strLockedRes);return; 
						}
						UpdateContactInternet(status, nRowID, RetOut_Data.getRow(i), orm);
						if(!status.IsOK()){ 
							UpdateStatus(RetOut_ErrRow, status); locker.UnLock(status, strLockedRes);return; 
						}
						UpdateContactAddresses(status, nRowID, RetOut_Data.getRow(i), orm);
						if(!status.IsOK()){ 
							UpdateStatus(RetOut_ErrRow, status); locker.UnLock(status, strLockedRes);return; 
						}
						UpdateContactJournal(status, nRowID, RetOut_Data.getRow(i), orm);
						if(!status.IsOK()){ 
							UpdateStatus(RetOut_ErrRow, status); locker.UnLock(status, strLockedRes);return; 
						}
						UpdateContactDebtor(status, nRowID, RetOut_Data.getRow(i), orm);
						if(!status.IsOK()){ 
							qDebug() <<  QString("Failed to update debtor, code: %1").arg(RetOut_Data.getDataRef(i, "BCMD_DEBTORCODE").toString());
							//g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("Failed to update debtor, code: %1").arg(RetOut_Data.getDataRef(i, "BCMD_DEBTORCODE").toString()));
							UpdateStatus(RetOut_ErrRow, status); locker.UnLock(status, strLockedRes);return; 
						}

						//unlock
						locker.UnLock(status, strLockedRes);
						if(!status.IsOK()){	UpdateStatus(RetOut_ErrRow, status);  return; }

						//lstWrite.Dump();
						//qDebug() << "ImportContacts: overwrite old data";

						//overwrite data from this record
						orm.Write(status, lstWrite, DbSqlTableView::TVIEW_BUS_CONTACT_FULL);//,nSkipLastCols);
						if(!status.IsOK()){ 
							UpdateStatus(RetOut_ErrRow, status); return; 
						}
					}
				}
				else	// row did not exist before
				{
					//lstWrite.Dump();
					//qDebug() << "ImportContacts: write new contact";

					orm.Write(status, lstWrite, DbSqlTableView::TVIEW_BUS_CONTACT_FULL);//,nSkipLastCols);
					if(!status.IsOK()){ UpdateStatus(RetOut_ErrRow, status);  return; }

					int nRowID =0;
					if (lstWrite.getRowCount()==1)
						nRowID = lstWrite.getDataRef(0,"BCNT_ID").toInt();

					//store back row ID
					RetOut_Data.setData(i, "BCNT_ID", nRowID);

					//add new data
					UpdateContactPhones(status, nRowID, lstWrite, orm);
					if(!status.IsOK()){ UpdateStatus(RetOut_ErrRow, status); return; }
					UpdateContactEmails(status, nRowID, lstWrite, orm);
					if(!status.IsOK()){ UpdateStatus(RetOut_ErrRow, status); return; }
					UpdateContactInternet(status, nRowID, lstWrite, orm);
					if(!status.IsOK()){ UpdateStatus(RetOut_ErrRow, status); return; }
					UpdateContactAddresses(status, nRowID, lstWrite, orm);
					if(!status.IsOK()){ UpdateStatus(RetOut_ErrRow, status); return; }
					UpdateContactJournal(status, nRowID, lstWrite, orm);
					if(!status.IsOK()){ UpdateStatus(RetOut_ErrRow, status); return; }
					UpdateContactDebtor(status, nRowID, lstWrite, orm);
					if(!status.IsOK()){ 
						UpdateStatus(RetOut_ErrRow, status); return; 
					}
				}
			}
		}
		break;
	case 1:	// UPDATE_EXISTING
		{
			//-------------------------------------
			//		WRITE PICTURES: ->BT added for QCW
			//-------------------------------------
			g_PrivateServiceSet->BusinessHelper->WriteEntityPictures(status,RetOut_Data,"BCNT_BIG_PICTURE_ID","BCNT_PICTURE");
			if(!status.IsOK()){ UpdateStatus(RetOut_ErrRow, status);  return; }


			//Make local query, can fail if connection reservation fails.
			DbSimpleOrm orm(status, BUS_CM_CONTACT, GetDbManager()); 
			if(!status.IsOK()){ UpdateStatus(RetOut_ErrRow, status); return; }

			BusinessLocker locker(status,BUS_CM_CONTACT,GetDbManager(),orm.GetDbConnection());
			if(!status.IsOK()){ UpdateStatus(RetOut_ErrRow, status); return; }
			QString strLockedRes;

			//for each record to write
			int nCnt = RetOut_Data.getRowCount();
			for(int i=0; i<nCnt; i++)
			{
				RetOut_ErrRow.setData(0, "ErrRow", i);

				//find all duplicate records
				DbRecordSet resultRows;
				FindContactDuplicatesFull(status, RetOut_Data.getRow(i), resultRows, *orm.GetDbSqlQuery());
				if(!Ret_pStatus.IsOK()){ UpdateStatus(RetOut_ErrRow, status); return; }
				
				//prepare (single row) list to be written
				DbRecordSet lstWrite;
				lstWrite.copyDefinition(RetOut_Data);
				lstWrite.addRow();
				//TOFIX assign only those rows that are emtpy in the DB
				DbRecordSet row = RetOut_Data.getRow(i);
				lstWrite.assignRow(0, row); 
				lstWrite.setData(0, "BCNT_OWNER_ID", g_UserSessionManager->GetPersonID());

				//int nSkipLastCols=DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CONTACT_FULL).m_nSkipLastCols;
				//nSkipLastCols += 4; // account for additional columns used for grid display

				int nMatchedRows = resultRows.getRowCount();
				if(nMatchedRows > 0)
				{
					//record(s) already exist 
					for(int j=0; j<nMatchedRows; j++)
					{
						int nRowID = resultRows.getDataRef(j, 0).toInt(); 
						lstWrite.getDataRef(0, "BCNT_ID") = QVariant(nRowID);

						//store back row ID (will overwrite if more than one)
						RetOut_Data.setData(i, "BCNT_ID", nRowID);

						//lock record before updating it
						locker.Lock(status, lstWrite,strLockedRes);
						if(!status.IsOK()){ UpdateStatus(RetOut_ErrRow, status); return; }

						//add new data if not already in the database
						UpdateContactPhones(status, nRowID, RetOut_Data.getRow(i), orm);
						if(!status.IsOK()){ UpdateStatus(RetOut_ErrRow, status); locker.UnLock(status, strLockedRes);return; }
						UpdateContactEmails(status, nRowID, RetOut_Data.getRow(i), orm);
						if(!status.IsOK()){ UpdateStatus(RetOut_ErrRow, status); locker.UnLock(status, strLockedRes);return; }
						UpdateContactInternet(status, nRowID, RetOut_Data.getRow(i), orm);
						if(!status.IsOK()){ UpdateStatus(RetOut_ErrRow, status); locker.UnLock(status, strLockedRes);return; }
						UpdateContactAddresses(status, nRowID, RetOut_Data.getRow(i), orm);
						if(!status.IsOK()){ UpdateStatus(RetOut_ErrRow, status); locker.UnLock(status, strLockedRes);return; }
						UpdateContactJournal(status, nRowID, RetOut_Data.getRow(i), orm);
						if(!status.IsOK()){ UpdateStatus(RetOut_ErrRow, status); locker.UnLock(status, strLockedRes);return; }
						UpdateContactDebtor(status, nRowID, RetOut_Data.getRow(i), orm);
						if(!status.IsOK()){ UpdateStatus(RetOut_ErrRow, status); locker.UnLock(status, strLockedRes);return; }

						//unlock
						locker.UnLock(status, strLockedRes);
						if(!status.IsOK()){	UpdateStatus(RetOut_ErrRow, status); return; }

						//overwrite data from this record
						orm.Write(status, lstWrite, DbSqlTableView::TVIEW_BUS_CONTACT_FULL);//,nSkipLastCols);
						if(!status.IsOK()){ UpdateStatus(RetOut_ErrRow, status); return; }
					}
				}
				else	// row did not exist before
				{
					//write new record
					orm.Write(status, lstWrite, DbSqlTableView::TVIEW_BUS_CONTACT_FULL);//,nSkipLastCols);
					if(!status.IsOK()){ UpdateStatus(RetOut_ErrRow, status);  return; }

					int nRowID =0;
					if (lstWrite.getRowCount()==1)
						nRowID = lstWrite.getDataRef(0,"BCNT_ID").toInt();
					//int nRowID = orm.GetDbSqlQuery()->GetLastInsertedID(status, "BCNT_ID");
						
					//store back row ID
					RetOut_Data.setData(i, "BCNT_ID", nRowID);

					//add new data if not already in the database
					UpdateContactPhones(status, nRowID, lstWrite, orm);
					if(!status.IsOK()){ UpdateStatus(RetOut_ErrRow, status); return; }
					UpdateContactEmails(status, nRowID, lstWrite, orm);
					if(!status.IsOK()){ UpdateStatus(RetOut_ErrRow, status);  return; }
					UpdateContactInternet(status, nRowID, lstWrite, orm);
					if(!status.IsOK()){ UpdateStatus(RetOut_ErrRow, status); return; }
					UpdateContactAddresses(status, nRowID, lstWrite, orm);
					if(!status.IsOK()){ UpdateStatus(RetOut_ErrRow, status); return; }
					UpdateContactJournal(status, nRowID, lstWrite, orm);
					if(!status.IsOK()){ UpdateStatus(RetOut_ErrRow, status); return; }
					UpdateContactDebtor(status, nRowID, lstWrite, orm);
					if(!status.IsOK()){ UpdateStatus(RetOut_ErrRow, status); return; }
				}
			}
		}
		break;
	case 3:	// SKIP_EXISTING
		{
			//Make local query, can fail if connection reservation fails.
			DbSimpleOrm orm(status, BUS_CM_CONTACT, GetDbManager()); 
			if(!status.IsOK()){ UpdateStatus(RetOut_ErrRow, status); return; }

			//for each record to write
			int nCnt = RetOut_Data.getRowCount();
			for(int i=0; i<nCnt; i++)
			{
				RetOut_ErrRow.setData(0, "ErrRow", i);

				//find all duplicate records
				DbRecordSet resultIDs;
				FindContactDuplicates(status, RetOut_Data.getRow(i), resultIDs, *orm.GetDbSqlQuery());
				if(!status.IsOK()){ UpdateStatus(RetOut_ErrRow, status); return; }

				//prepare (single row) list to be written
				DbRecordSet lstWrite;
				lstWrite.copyDefinition(RetOut_Data);
				lstWrite.addRow();
				DbRecordSet row = RetOut_Data.getRow(i);
				lstWrite.assignRow(0, row);
				lstWrite.setData(0, "BCNT_OWNER_ID", g_UserSessionManager->GetPersonID());

				//int nSkipLastCols=DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CONTACT_FULL).m_nSkipLastCols;
				//nSkipLastCols += 4; // account for additional columns used for grid display

				int nMatchedRows = resultIDs.getRowCount();
				if(nMatchedRows == 0)
				{
					//write new record
					orm.Write(status, lstWrite, DbSqlTableView::TVIEW_BUS_CONTACT_FULL);//,nSkipLastCols);
					if(!status.IsOK()){ UpdateStatus(RetOut_ErrRow, status); return; }

					int nRowID =0;
					if (lstWrite.getRowCount()==1)
						nRowID = lstWrite.getDataRef(0,"BCNT_ID").toInt();
					//int nRowID = orm.GetDbSqlQuery()->GetLastInsertedID(status, "BCNT_ID");

					//store back row ID
					RetOut_Data.setData(i, "BCNT_ID", nRowID);

					//add new data
					UpdateContactPhones(status, nRowID, lstWrite, orm);
					if(!status.IsOK()){ UpdateStatus(RetOut_ErrRow, status); return; }
					UpdateContactEmails(status, nRowID, lstWrite, orm);
					if(!status.IsOK()){ UpdateStatus(RetOut_ErrRow, status); return; }
					UpdateContactInternet(status, nRowID, lstWrite, orm);
					if(!status.IsOK()){ UpdateStatus(RetOut_ErrRow, status); return; }
					UpdateContactAddresses(status, nRowID, lstWrite, orm);
					if(!status.IsOK()){ UpdateStatus(RetOut_ErrRow, status); return; }
					UpdateContactJournal(status, nRowID, lstWrite, orm);
					if(!status.IsOK()){ UpdateStatus(RetOut_ErrRow, status); return; }
					UpdateContactDebtor(status, nRowID, lstWrite, orm);
					if(!status.IsOK()){ UpdateStatus(RetOut_ErrRow, status); return; }
				}
				//else skip writing contact that already exists in a database
			}
		}
		break;
	case 2:		//IGNORE_EXISTING
	default:	//Operation 2 is default = INSERTS ALL, no transaction, if one fails return status,:
		RetOut_Data.setColValue("BCNT_OWNER_ID", g_UserSessionManager->GetPersonID());
		g_BusinessServiceSet->BusContact->WriteDataFromImport(Ret_pStatus,RetOut_Data);
		break;
	}


	//--------------------------------------------------------
	//on success, delete some data columns to keep the network load light
	//--------------------------------------------------------
	DbRecordSet lstTmpOut;
	lstTmpOut.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CONTACT_SELECTION));
	lstTmpOut.addColumn(QVariant::Int, "STATUS_CODE");
	lstTmpOut.addColumn(QVariant::String, "STATUS_TEXT");
	lstTmpOut.addColumn(DbRecordSet::GetVariantType(),"LST_GROUPS");	

	lstTmpOut.merge(RetOut_Data);

	//write groups:
	Status err;
	WriteGroupContactData(err,nOperation,lstTmpOut);
	if (!err.IsOK() && Ret_pStatus.IsOK())
		Ret_pStatus=err;

	RetOut_Data = lstTmpOut;
}

//process single line
void Service_BusImport::WriteProjectDebtors(Status &Ret_pStatus, DbRecordSet &RetOut_Data)
{
	if(RetOut_Data.getRowCount() < 1)
		return;

	QString strDebtorCodes = RetOut_Data.getDataRef(0, "CALC_DEBTOR_CODES").toString();
	if(!strDebtorCodes.isEmpty())
	{
		DbRecordSet rowNMRX;
		rowNMRX.addColumn(QVariant::String, "DEBTOR_CODE");
		rowNMRX.addColumn(QVariant::String, "DEBTOR_PROJ_ROLE");

		//
		QStringList lstRoles = RetOut_Data.getDataRef(0, "CALC_ROLES").toString().split(";");
		QStringList lstCodes = strDebtorCodes.split(";");
		int nRolesCnt = lstRoles.size();
		int nCodesCnt = lstCodes.size();
		for(int i=0; i<nCodesCnt; i++)
		{
			rowNMRX.addRow();
			rowNMRX.setData(i, "DEBTOR_CODE",		lstCodes[i]);
			if(nRolesCnt > i)
				rowNMRX.setData(i, "DEBTOR_PROJ_ROLE",	lstRoles[i]);	//.trimmed()
			else if(nRolesCnt > 0) 
				rowNMRX.setData(i, "DEBTOR_PROJ_ROLE",	lstRoles[nRolesCnt-1]);
			else
				Q_ASSERT(false);	//no role for you
		}

		//now call to store this somewhere
		int nProjectID = RetOut_Data.getDataRef(0, "BUSP_ID").toInt();
		g_BusinessServiceSet->BusContact->AssignDebtors2Project(Ret_pStatus, nProjectID, rowNMRX);
	}
}

/*!
Imports projects

\param Ret_pStatus		- return error if one fails
\param RetOut_Dat		- data in TVIEW_BUS_PROJECT + status fields + "CALC_DEBTOR_CODES" + "CALC_ROLES"
\param nOperation		- 0 - replace existing, 1-update, 2-ignore, 3-skip if exists

*/
void Service_BusImport::ImportProjects(Status &Ret_pStatus, DbRecordSet &RetOut_Data,int nOperation)
{
	//RetOut_Data.Dump();

	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm orm(Ret_pStatus, BUS_PROJECT, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	//lift me up:
	switch(nOperation)
	{
	case 0:	// REPLACE_EXISTING
		{
			//for each record to write
			int nCnt = RetOut_Data.getRowCount();
			for(int i=0; i<nCnt; i++)
			{
				//find all duplicate records
				DbRecordSet resultIDs;
				FindProjectDuplicates(Ret_pStatus, RetOut_Data.getRow(i), resultIDs, *orm.GetDbSqlQuery());
				if(!Ret_pStatus.IsOK())
					return;

				//prepare (single row) list to be written
				DbRecordSet lstWrite;
				lstWrite.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_PROJECT));	//DO NOT copy definition, because there are some additional fields here!!!
				DbRecordSet row = RetOut_Data.getRow(i);
				lstWrite.merge(row);
		
				//B.T. update new fields
				//MR 2013.04.04: avoid disaster, there were no SPC tables in databases yet, so entire import fails (Eventum #2693)
				UpdateProjectSPCFields(Ret_pStatus,*orm.GetDbSqlQuery(), lstWrite, RetOut_Data.getRow(i));
				if(!Ret_pStatus.IsOK())
					return;			

				int nMatchedRows = resultIDs.getRowCount();
				if(nMatchedRows > 0)
				{
					//record(s) already exist
					for(int j=0; j<nMatchedRows; j++)
					{
						int nRowID = resultIDs.getDataRef(j, 0).toInt(); 
					
						//store back row ID (will overwrite if more than one match)
						RetOut_Data.setData(i, "BUSP_ID", nRowID);

						//fix the record that we are trying to save to have valid HCT data
						lstWrite.getDataRef(0, "BUSP_ID") = QVariant(nRowID);
						lstWrite.getDataRef(0, "BUSP_LEVEL") = resultIDs.getDataRef(j, "BUSP_LEVEL");
						lstWrite.getDataRef(0, "BUSP_PARENT") = resultIDs.getDataRef(j, "BUSP_PARENT");
						lstWrite.getDataRef(0, "BUSP_HASCHILDREN") = resultIDs.getDataRef(j, "BUSP_HASCHILDREN");
						lstWrite.getDataRef(0, "BUSP_ICON") = resultIDs.getDataRef(j, "BUSP_ICON");
						lstWrite.getDataRef(0, "BUSP_STYLE") = resultIDs.getDataRef(j, "BUSP_STYLE");

						//overwrite data from this record
						WriteHierarchicalData(Ret_pStatus, lstWrite);
						if(!Ret_pStatus.IsOK())
							return;

						RetOut_Data.setData(i, "BUSP_LEVEL", lstWrite.getDataRef(0, "BUSP_LEVEL").toInt());
						RetOut_Data.setData(i, "BUSP_PARENT", lstWrite.getDataRef(0, "BUSP_PARENT").toInt());
						RetOut_Data.setData(i, "BUSP_HASCHILDREN", lstWrite.getDataRef(0, "BUSP_HASCHILDREN").toInt());

						//write optional debtors
						WriteProjectDebtors(Ret_pStatus, row);
						if(!Ret_pStatus.IsOK())
							return;
					}
				}
				else	// row did not exist before
				{
					//write new record
					WriteHierarchicalData(Ret_pStatus, lstWrite);
					if(!Ret_pStatus.IsOK())
						return;

					//store back row ID
					RetOut_Data.setData(i, "BUSP_ID", lstWrite.getDataRef(0, "BUSP_ID").toInt());
					RetOut_Data.setData(i, "BUSP_LEVEL", lstWrite.getDataRef(0, "BUSP_LEVEL").toInt());
					RetOut_Data.setData(i, "BUSP_PARENT", lstWrite.getDataRef(0, "BUSP_PARENT").toInt());
					RetOut_Data.setData(i, "BUSP_HASCHILDREN", lstWrite.getDataRef(0, "BUSP_HASCHILDREN").toInt());

					//write optional debtors
					WriteProjectDebtors(Ret_pStatus, row);
					if(!Ret_pStatus.IsOK())
						return;
				}
			}
		}
		break;
	case 1:	// UPDATE_EXISTING
		{
			//for each record to write
			int nCnt = RetOut_Data.getRowCount();
			for(int i=0; i<nCnt; i++)
			{
				//find all duplicate records
				DbRecordSet resultIDs;
				FindProjectDuplicates(Ret_pStatus, RetOut_Data.getRow(i), resultIDs, *orm.GetDbSqlQuery());
				if(!Ret_pStatus.IsOK())
					return;

				//prepare (single row) list to be written
				DbRecordSet lstWrite;
				lstWrite.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_PROJECT));	//DO NOT copy definition, because there are some additional fields here!!!
				//TOFIX assign only those rows that are empty in  the DB
				DbRecordSet row = RetOut_Data.getRow(i);
				lstWrite.merge(row); 

				//B.T. update new fields
				//MR 2013.04.04: avoid disaster, there were no SPC tables in databases yet, so entire import fails (Eventum #2693)
				UpdateProjectSPCFields(Ret_pStatus,*orm.GetDbSqlQuery(), lstWrite, RetOut_Data.getRow(i));
				if(!Ret_pStatus.IsOK())
				{
					//lstWrite.Dump();
					//RetOut_Data.getRow(i).Dump();
					return;
				}

				int nMatchedRows = resultIDs.getRowCount();
				if(nMatchedRows > 0)
				{
					//record(s) already exist
					for(int j=0; j<nMatchedRows; j++)
					{
						int nRowID = resultIDs.getDataRef(j, 0).toInt(); 
					
						//store back row ID (will overwrite if more than one match)
						RetOut_Data.setData(i, "BUSP_ID", nRowID);

						//fix the record that we are trying to save to have valid HCT data
						lstWrite.getDataRef(0, "BUSP_ID") = QVariant(nRowID);
						lstWrite.getDataRef(0, "BUSP_LEVEL") = resultIDs.getDataRef(j, "BUSP_LEVEL");
						lstWrite.getDataRef(0, "BUSP_PARENT") = resultIDs.getDataRef(j, "BUSP_PARENT");
						lstWrite.getDataRef(0, "BUSP_HASCHILDREN") = resultIDs.getDataRef(j, "BUSP_HASCHILDREN");
						lstWrite.getDataRef(0, "BUSP_ICON") = resultIDs.getDataRef(j, "BUSP_ICON");
						lstWrite.getDataRef(0, "BUSP_STYLE") = resultIDs.getDataRef(j, "BUSP_STYLE");

						//overwrite data from this record
						WriteHierarchicalData(Ret_pStatus, lstWrite);
						if(!Ret_pStatus.IsOK())
							return;

						RetOut_Data.setData(i, "BUSP_LEVEL", lstWrite.getDataRef(0, "BUSP_LEVEL").toInt());
						RetOut_Data.setData(i, "BUSP_PARENT", lstWrite.getDataRef(0, "BUSP_PARENT").toInt());
						RetOut_Data.setData(i, "BUSP_HASCHILDREN", lstWrite.getDataRef(0, "BUSP_HASCHILDREN").toInt());

						//write optional debtors
						WriteProjectDebtors(Ret_pStatus, row);
						if(!Ret_pStatus.IsOK())
							return;
					}
				}
				else	// row did not exist before
				{
					//write new record
					WriteHierarchicalData(Ret_pStatus, lstWrite);
					if(!Ret_pStatus.IsOK())
						return;

					//store back row ID and all relevant data
					RetOut_Data.setData(i, "BUSP_ID", lstWrite.getDataRef(0, "BUSP_ID").toInt());
					RetOut_Data.setData(i, "BUSP_LEVEL", lstWrite.getDataRef(0, "BUSP_LEVEL").toInt());
					RetOut_Data.setData(i, "BUSP_PARENT", lstWrite.getDataRef(0, "BUSP_PARENT").toInt());
					RetOut_Data.setData(i, "BUSP_HASCHILDREN", lstWrite.getDataRef(0, "BUSP_HASCHILDREN").toInt());

					//write optional debtors
					WriteProjectDebtors(Ret_pStatus, row);
					if(!Ret_pStatus.IsOK())
						return;
				}
			}
		}
		break;
	case 3:	// SKIP_EXISTING
		{
			//for each record to write
			int nCnt = RetOut_Data.getRowCount();
			for(int i=0; i<nCnt; i++)
			{
				//find all duplicate records
				DbRecordSet resultIDs;
				FindProjectDuplicates(Ret_pStatus, RetOut_Data.getRow(i), resultIDs, *orm.GetDbSqlQuery());
				if(!Ret_pStatus.IsOK())
				{
	//				RetOut_Data.getRow(i).Dump();
//					resultIDs.Dump();
					return;
				}

				//prepare (single row) list to be written
				DbRecordSet lstWrite;
				lstWrite.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_PROJECT));	//DO NOT copy definition, because there are some additional fields here!!!
				DbRecordSet row = RetOut_Data.getRow(i);
				lstWrite.merge(row); 

				//B.T. update new fields
				//MR 2013.04.04: avoid disaster, there were no SPC tables in databases yet, so entire import fails (Eventum #2693)
				UpdateProjectSPCFields(Ret_pStatus,*orm.GetDbSqlQuery(), lstWrite, RetOut_Data.getRow(i));
				if(!Ret_pStatus.IsOK())
					return;

				int nMatchedRows = resultIDs.getRowCount();
				if(nMatchedRows > 0)
				{
					//record(s) already exist, skip writing
				}
				else	// row did not exist before
				{
					//write new record
					WriteHierarchicalData(Ret_pStatus, lstWrite);
					if(!Ret_pStatus.IsOK())
					{
//						lstWrite.Dump();
						return;
					}

					//store back row ID
					RetOut_Data.setData(i, "BUSP_ID", lstWrite.getDataRef(0, "BUSP_ID").toInt());
					RetOut_Data.setData(i, "BUSP_LEVEL", lstWrite.getDataRef(0, "BUSP_LEVEL").toInt());
					RetOut_Data.setData(i, "BUSP_PARENT", lstWrite.getDataRef(0, "BUSP_PARENT").toInt());
					RetOut_Data.setData(i, "BUSP_HASCHILDREN", lstWrite.getDataRef(0, "BUSP_HASCHILDREN").toInt());

					//write optional debtors
					WriteProjectDebtors(Ret_pStatus, row);
					if(!Ret_pStatus.IsOK())
					{
//						row.Dump();
//						lstWrite.Dump();
						return;
					}
				}
			}
		}
		break;
	case 2:	//IGNORE_EXISTING
	default://Operation 2 is default = INSERTS ALL, no transaction, if one fails return status,:
		{
			//for each record to write
			int nCnt = RetOut_Data.getRowCount();
			for(int i=0; i<nCnt; i++)
			{

				//prepare (single row) list to be written
				DbRecordSet lstWrite;
				lstWrite.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_PROJECT));	//DO NOT copy definition, because there are some additional fields here!!!
				DbRecordSet row = RetOut_Data.getRow(i);
				lstWrite.merge(row); 

				//B.T. update new fields
				//MR 2013.04.04: avoid disaster, there were no SPC tables in databases yet, so entire import fails (Eventum #2693)
				UpdateProjectSPCFields(Ret_pStatus,*orm.GetDbSqlQuery(), lstWrite, RetOut_Data.getRow(i));
				if(!Ret_pStatus.IsOK())
					return;

				//write new record
				WriteHierarchicalData(Ret_pStatus, lstWrite);
				if(!Ret_pStatus.IsOK())
					return;

				//store back row ID
				RetOut_Data.setData(i, "BUSP_ID", lstWrite.getDataRef(0, "BUSP_ID").toInt());
				RetOut_Data.setData(i, "BUSP_LEVEL", lstWrite.getDataRef(0, "BUSP_LEVEL").toInt());
				RetOut_Data.setData(i, "BUSP_PARENT", lstWrite.getDataRef(0, "BUSP_PARENT").toInt());
				RetOut_Data.setData(i, "BUSP_HASCHILDREN", lstWrite.getDataRef(0, "BUSP_HASCHILDREN").toInt());

				//write optional debtors
				WriteProjectDebtors(Ret_pStatus, row);
				if(!Ret_pStatus.IsOK())
					return;
			}
		}
		break;
	}
}


/*!
	Imports persons

	\param Ret_pStatus		- return error if one fails
	\param RetOut_Dat		- data in view ?
	\param nOperation		- 0 - replace existing, 1-update, 2-ignore, 3-skip if exists
	\param bCreateUsers		- true: automatically creates entries in CORE_USER
	\param bCreateContacts	- true: automatically creates entries in contact

*/
void Service_BusImport::ImportPersons(Status &Ret_pStatus, DbRecordSet &RetOut_Data,int nOperation, bool bCreateUsers, bool bCreateContacts, int nRoleID)
{
	//RetOut_Data.Dump();

	if (RetOut_Data.getColumnIdx("BPER_ID")==-1)
		RetOut_Data.addColumn(QVariant::Int,"BPER_ID"); //BT: return ID for count
	//lstData.Dump();

	DbRecordSet lstData;
	lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_PERSON_IMPORT));
	lstData.merge(RetOut_Data);

	int nDefaultOrgID=GetDefaultOrganization(Ret_pStatus);

	//lstData.Dump();
	//TOFIX use Service_BusPerson::WriteData to create user entry too? 

	//lift me up:
	switch(nOperation)
	{
	case 0:		// REPLACE_EXISTING
		{
			//Make local query, can fail if connection reservation fails.
			DbSimpleOrm orm(Ret_pStatus, BUS_PERSON, GetDbManager()); 
			if(!Ret_pStatus.IsOK()) return;
			DbSimpleOrm orm1(Ret_pStatus, CORE_USER, GetDbManager()); 
			if(!Ret_pStatus.IsOK()) return;

			//for each record to write
			int nCnt = RetOut_Data.getRowCount();
			for(int i=0; i<nCnt; i++)
			{
				//delete all duplicate records
				DbRecordSet resultIDs;
				FindPersonDuplicates(Ret_pStatus, lstData.getRow(i), resultIDs, *orm.GetDbSqlQuery());
				if(!Ret_pStatus.IsOK())
					return;

				//prepare (single row) list to be written
				DbRecordSet lstWrite;
				lstWrite.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_PERSON));
				lstWrite.merge(lstData.getRow(i)); 

				QVariant nOrgID, nDeptID;
				QString strResOrgName, strResDeptName;
				DbRecordSet row = RetOut_Data.getRow(i);
				GetOrganizationInfo(Ret_pStatus, nOrgID, nDeptID, row, *orm.GetDbSqlQuery(),strResOrgName, strResDeptName, nDefaultOrgID);
				if(!Ret_pStatus.IsOK()) return;
				lstWrite.getDataRef(0, "BPER_ORGANIZATION_ID") = nOrgID;
				lstWrite.getDataRef(0, "BPER_DEPARTMENT_ID") = nDeptID;

				int nMatchedRows = resultIDs.getRowCount();
				if(nMatchedRows > 0)
				{
					//record(s) already exist
					for(int j=0; j<nMatchedRows; j++)
					{
						int nRowID = resultIDs.getDataRef(j, 0).toInt(); 
					
						//store back row ID (will overwrite if more than one match)
						RetOut_Data.setData(i, "BPER_ID", nRowID);

						//fix the record that we are trying to save to have valid HCT data
						lstWrite.getDataRef(0, "BPER_ID") = QVariant(nRowID);

						if(RetOut_Data.getDataRef(i, "GENERATE_CONTACT").toInt()>0)
						{
							DbSimpleOrm TableContact(Ret_pStatus, BUS_CM_CONTACT, GetDbManager()); 
							if(!Ret_pStatus.IsOK()) return;

							DbRecordSet lstContact;
							BusinessDataTransformer::Person2Contact(lstWrite, lstContact,true,false,true);
							//patch contact with selected org info
							if(!nOrgID.isNull())
								lstContact.setData(0, "BCNT_ORGANIZATIONNAME", strResOrgName);
							if(!nDeptID.isNull())
								lstContact.setData(0, "BCNT_DEPARTMENTNAME", strResDeptName);

							//issue #1794 - skip generating contact if it already exists
							DbRecordSet resultIDs;
							FindContactDuplicates(Ret_pStatus, lstContact, resultIDs, *orm.GetDbSqlQuery());
							if(!Ret_pStatus.IsOK())
								return;

							if(resultIDs.getRowCount() > 0)
							{
								//update contact link
								lstWrite.getDataRef(0, "BPER_CONTACT_ID") = resultIDs.getDataRef(0, 0);
							}
							else
							{
								DbRecordSet lstStatuses;
								TableContact.Write(Ret_pStatus,lstContact,-1,0,&lstStatuses);
								//update contact link
								lstWrite.getDataRef(0, "BPER_CONTACT_ID") = lstContact.getDataRef(0, "BCNT_ID");
							}
						}

						//overwrite data from this record
						orm.Write(Ret_pStatus, lstWrite);
						if(!Ret_pStatus.IsOK())
							return;

						GenerateRoleAndUserFromPerson(Ret_pStatus, nRoleID, lstWrite, orm1);
						if(!Ret_pStatus.IsOK())
							return;
					}
				}
				else	// row did not exist before
				{
					if(RetOut_Data.getDataRef(i, "GENERATE_CONTACT").toInt()>0)
					{
						DbSimpleOrm TableContact(Ret_pStatus, BUS_CM_CONTACT, GetDbManager()); 
						if(!Ret_pStatus.IsOK()) return;

						DbRecordSet lstContact;
						BusinessDataTransformer::Person2Contact(lstWrite, lstContact,true,false,true);

						//patch contact with selected org info
						if(!nOrgID.isNull())
							lstContact.setData(0, "BCNT_ORGANIZATIONNAME", strResOrgName);
						if(!nDeptID.isNull())
							lstContact.setData(0, "BCNT_DEPARTMENTNAME", strResDeptName);

						//issue #1794 - skip generating contact if it already exists
						DbRecordSet resultIDs;
						FindContactDuplicates(Ret_pStatus, lstContact, resultIDs, *orm.GetDbSqlQuery());
						if(!Ret_pStatus.IsOK())
							return;

						if(resultIDs.getRowCount() > 0)
						{
							//update contact link
							lstWrite.getDataRef(0, "BPER_CONTACT_ID") = resultIDs.getDataRef(0, 0);
						}
						else
						{
							DbRecordSet lstStatuses;
							TableContact.Write(Ret_pStatus,lstContact,-1,0,&lstStatuses);
							//update contact link
							lstWrite.getDataRef(0, "BPER_CONTACT_ID") = lstContact.getDataRef(0, "BCNT_ID");
						}
					}

					//write new record
					orm.Write(Ret_pStatus, lstWrite);
					if(!Ret_pStatus.IsOK())
						return;

					GenerateRoleAndUserFromPerson(Ret_pStatus, nRoleID, lstWrite, orm1);
					if(!Ret_pStatus.IsOK())
						return;
				}
			}
		}
		break;
	case 1:		// UPDATE_EXISTING
		{
			//Make local query, can fail if connection reservation fails.
			DbSimpleOrm orm(Ret_pStatus, BUS_PERSON, GetDbManager()); 
			if(!Ret_pStatus.IsOK()) return;
			DbSimpleOrm orm1(Ret_pStatus, CORE_USER, GetDbManager()); 
			if(!Ret_pStatus.IsOK()) return;

			//for each record to write
			int nCnt = lstData.getRowCount();
			for(int i=0; i<nCnt; i++)
			{
				//find all duplicate records
				DbRecordSet resultIDs;
				FindPersonDuplicates(Ret_pStatus, lstData.getRow(i), resultIDs, *orm.GetDbSqlQuery());
				if(!Ret_pStatus.IsOK())
					return;

				//prepare (single row) list to be written
				DbRecordSet lstWrite;
				lstWrite.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_PERSON));
				lstWrite.merge(lstData.getRow(i)); 

				//lstWrite.Dump();

				//RetOut_Data.Dump();

				QVariant nOrgID, nDeptID;
				QString strResOrgName, strResDeptName;
				DbRecordSet row = RetOut_Data.getRow(i);
				GetOrganizationInfo(Ret_pStatus, nOrgID, nDeptID, row, *orm.GetDbSqlQuery(), strResOrgName, strResDeptName, nDefaultOrgID);
				if(!Ret_pStatus.IsOK()) return;
				//qDebug()<<nOrgID.toInt()<<" "<<nDeptID.toInt();
				lstWrite.getDataRef(0, "BPER_ORGANIZATION_ID") = nOrgID;
				lstWrite.getDataRef(0, "BPER_DEPARTMENT_ID") = nDeptID;

				//lstWrite.Dump();

				int nMatchedRows = resultIDs.getRowCount();
				if(nMatchedRows > 0)
				{
					//record(s) already exist
					for(int j=0; j<nMatchedRows; j++)
					{
						int nRowID = resultIDs.getDataRef(j, 0).toInt(); 
					
						//store back row ID (will overwrite if more than one match)
						RetOut_Data.setData(i, "BPER_ID", nRowID);

						//fix the record that we are trying to save to have valid HCT data
						lstWrite.getDataRef(0, "BPER_ID") = QVariant(nRowID);

						//lstWrite.Dump();

						if(RetOut_Data.getDataRef(i, "GENERATE_CONTACT").toInt()>0)
						{
							DbSimpleOrm TableContact(Ret_pStatus, BUS_CM_CONTACT, GetDbManager()); 
							if(!Ret_pStatus.IsOK()) return;

							DbRecordSet lstContact;
							BusinessDataTransformer::Person2Contact(lstWrite, lstContact,true,false,true);
							//patch contact with selected org info
							if(!nOrgID.isNull())
								lstContact.setData(0, "BCNT_ORGANIZATIONNAME", strResOrgName);
							if(!nDeptID.isNull())
								lstContact.setData(0, "BCNT_DEPARTMENTNAME", strResDeptName);

							//issue #1794 - skip generating contact if it already exists
							DbRecordSet resultIDs;
							FindContactDuplicates(Ret_pStatus, lstContact, resultIDs, *orm.GetDbSqlQuery());
							if(!Ret_pStatus.IsOK())
								return;

							if(resultIDs.getRowCount() > 0)
							{
								//update contact link
								lstWrite.getDataRef(0, "BPER_CONTACT_ID") = resultIDs.getDataRef(0, 0);
							}
							else
							{
								DbRecordSet lstStatuses;
								TableContact.Write(Ret_pStatus,lstContact,-1,0,&lstStatuses);
								//update contact link
								lstWrite.getDataRef(0, "BPER_CONTACT_ID") = lstContact.getDataRef(0, "BCNT_ID");
							}
						}

						//overwrite data from this record
						orm.Write(Ret_pStatus, lstWrite);
						if(!Ret_pStatus.IsOK())
							return;

						GenerateRoleAndUserFromPerson(Ret_pStatus, nRoleID, lstWrite, orm1, true);
						if(!Ret_pStatus.IsOK())
							return;
					}
				}
				else	// row did not exist before
				{
					if(RetOut_Data.getDataRef(i, "GENERATE_CONTACT").toInt()>0)
					{
						DbSimpleOrm TableContact(Ret_pStatus, BUS_CM_CONTACT, GetDbManager()); 
						if(!Ret_pStatus.IsOK()) return;

						DbRecordSet lstContact;
						BusinessDataTransformer::Person2Contact(lstWrite, lstContact,true,false,true);

						//patch contact with selected org info
						if(!nOrgID.isNull())
							lstContact.setData(0, "BCNT_ORGANIZATIONNAME", strResOrgName);
						if(!nDeptID.isNull())
							lstContact.setData(0, "BCNT_DEPARTMENTNAME", strResDeptName);

						//issue #1794 - skip generating contact if it already exists
						DbRecordSet resultIDs;
						FindContactDuplicates(Ret_pStatus, lstContact, resultIDs, *orm.GetDbSqlQuery());
						if(!Ret_pStatus.IsOK())
							return;

						if(resultIDs.getRowCount() > 0)
						{
							//update contact link
							lstWrite.getDataRef(0, "BPER_CONTACT_ID") = resultIDs.getDataRef(0, 0);
						}
						else
						{
							DbRecordSet lstStatuses;
							TableContact.Write(Ret_pStatus,lstContact,-1,0,&lstStatuses);
							//update contact link
							lstWrite.getDataRef(0, "BPER_CONTACT_ID") = lstContact.getDataRef(0, "BCNT_ID");
						}
					}

					//write new record
					orm.Write(Ret_pStatus, lstWrite);
					if(!Ret_pStatus.IsOK())
						return;

					GenerateRoleAndUserFromPerson(Ret_pStatus, nRoleID, lstWrite, orm1, true);
					if(!Ret_pStatus.IsOK())
						return;

					//store back row ID and all relevant data
				}
			}
		}
		break;
	case 3:		// SKIP_EXISTING
		{
			//Make local query, can fail if connection reservation fails.
			DbSimpleOrm orm(Ret_pStatus, BUS_PERSON, GetDbManager()); 
			if(!Ret_pStatus.IsOK()) return;
			DbSimpleOrm orm1(Ret_pStatus, CORE_USER, GetDbManager()); 
			if(!Ret_pStatus.IsOK()) return;

			//for each record to write
			int nCnt = lstData.getRowCount();
			for(int i=0; i<nCnt; i++)
			{
				//find all duplicate records
				DbRecordSet resultIDs;
				FindPersonDuplicates(Ret_pStatus, lstData.getRow(i), resultIDs, *orm.GetDbSqlQuery());
				if(!Ret_pStatus.IsOK())
					return;

				//prepare (single row) list to be written
				DbRecordSet lstWrite;
				lstWrite.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_PERSON));
				lstWrite.merge(lstData.getRow(i)); 

				QVariant nOrgID, nDeptID;
				QString strResOrgName, strResDeptName;
				DbRecordSet row = RetOut_Data.getRow(i);
				GetOrganizationInfo(Ret_pStatus, nOrgID, nDeptID, row, *orm.GetDbSqlQuery(), strResOrgName, strResDeptName, nDefaultOrgID);
				if(!Ret_pStatus.IsOK()) return;
				lstWrite.getDataRef(0, "BPER_ORGANIZATION_ID") = nOrgID;
				lstWrite.getDataRef(0, "BPER_DEPARTMENT_ID") = nDeptID;

				int nMatchedRows = resultIDs.getRowCount();
				if(nMatchedRows > 0)
				{
					//record(s) already exist, skip writing
				}
				else	// row did not exist before
				{
					if(RetOut_Data.getDataRef(i, "GENERATE_CONTACT").toInt()>0)
					{
						DbSimpleOrm TableContact(Ret_pStatus, BUS_CM_CONTACT, GetDbManager()); 
						if(!Ret_pStatus.IsOK()) return;

						DbRecordSet lstContact;
						BusinessDataTransformer::Person2Contact(lstWrite, lstContact,true,false,true);
						//patch contact with selected org info
						if(!nOrgID.isNull())
							lstContact.setData(0, "BCNT_ORGANIZATIONNAME", strResOrgName);
						if(!nDeptID.isNull())
							lstContact.setData(0, "BCNT_DEPARTMENTNAME", strResDeptName);

						//issue #1794 - skip generating contact if it already exists
						DbRecordSet resultIDs;
						FindContactDuplicates(Ret_pStatus, lstContact, resultIDs, *orm.GetDbSqlQuery());
						if(!Ret_pStatus.IsOK())
							return;

						if(resultIDs.getRowCount() > 0)
						{
							//update contact link
							lstWrite.getDataRef(0, "BPER_CONTACT_ID") = resultIDs.getDataRef(0, 0);
						}
						else
						{
							DbRecordSet lstStatuses;
							TableContact.Write(Ret_pStatus,lstContact,-1,0,&lstStatuses);
							//update contact link
							lstWrite.getDataRef(0, "BPER_CONTACT_ID") = lstContact.getDataRef(0, "BCNT_ID");
						}
					}

					//write new record
					orm.Write(Ret_pStatus, lstWrite);
					if(!Ret_pStatus.IsOK())
						return;

					GenerateRoleAndUserFromPerson(Ret_pStatus, nRoleID, lstWrite, orm1);
					if(!Ret_pStatus.IsOK())
						return;

					//store back row ID
					RetOut_Data.setData(i, "BPER_ID", lstWrite.getDataRef(0,"BPER_ID"));

				}
			}
		}
		break;
	case 2:		//IGNORE_EXISTING
	default:	//Operation 2 is default = INSERTS ALL, no transaction, if one fails return status,:
		{
			//Make local query, can fail if connection reservation fails.
			DbSimpleOrm orm(Ret_pStatus, BUS_PERSON, GetDbManager()); 
			if(!Ret_pStatus.IsOK()) return;
			DbSimpleOrm orm1(Ret_pStatus, CORE_USER, GetDbManager()); 
			if(!Ret_pStatus.IsOK()) return;

			//for each record to write
			int nCnt = lstData.getRowCount();
			for(int i=0; i<nCnt; i++)
			{
				//prepare (single row) list to be written
				DbRecordSet lstWrite;
				lstWrite.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_PERSON));
				lstWrite.merge(lstData.getRow(i));

				QVariant nOrgID, nDeptID;
				QString strResOrgName, strResDeptName;
				DbRecordSet row = RetOut_Data.getRow(i);
				GetOrganizationInfo(Ret_pStatus, nOrgID, nDeptID, row, *orm.GetDbSqlQuery(), strResOrgName, strResDeptName, nDefaultOrgID);
				if(!Ret_pStatus.IsOK()) return;
				lstWrite.getDataRef(0, "BPER_ORGANIZATION_ID") = nOrgID;
				lstWrite.getDataRef(0, "BPER_DEPARTMENT_ID") = nDeptID;

				if(RetOut_Data.getDataRef(i, "GENERATE_CONTACT").toInt()>0)
				{
					DbSimpleOrm TableContact(Ret_pStatus, BUS_CM_CONTACT, GetDbManager()); 
					if(!Ret_pStatus.IsOK()) return;

					DbRecordSet lstContact;
					BusinessDataTransformer::Person2Contact(lstWrite, lstContact,true,false,true);
					//patch contact with selected org info
					if(!nOrgID.isNull())
						lstContact.setData(0, "BCNT_ORGANIZATIONNAME", strResOrgName);
					if(!nDeptID.isNull())
						lstContact.setData(0, "BCNT_DEPARTMENTNAME", strResDeptName);

					//issue #1794 - skip generating contact if it already exists
					DbRecordSet resultIDs;
					FindContactDuplicates(Ret_pStatus, lstContact, resultIDs, *orm.GetDbSqlQuery());
					if(!Ret_pStatus.IsOK())
						return;

					if(resultIDs.getRowCount() > 0)
					{
						//update contact link
						lstWrite.getDataRef(0, "BPER_CONTACT_ID") = resultIDs.getDataRef(0, 0);
					}
					else
					{
						DbRecordSet lstStatuses;
						TableContact.Write(Ret_pStatus,lstContact,-1,0,&lstStatuses);
						//update contact link
						lstWrite.getDataRef(0, "BPER_CONTACT_ID") = lstContact.getDataRef(0, "BCNT_ID");
					}
				}

				//write new record
				orm.Write(Ret_pStatus, lstWrite);
				if(!Ret_pStatus.IsOK())
					return;

				GenerateRoleAndUserFromPerson(Ret_pStatus, nRoleID, lstWrite, orm1);
				if(!Ret_pStatus.IsOK())
					return;

				//store back row ID
				RetOut_Data.setData(i, "BPER_ID", lstWrite.getDataRef(0,"BPER_ID"));
			}
		}
		break;
	}
}

void Service_BusImport::ImportUserContactRelationships(Status &Ret_pStatus, DbRecordSet &RetOut_Data)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm orm(Ret_pStatus, BUS_PERSON, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	int nFailedMatchesCount = 0;

	//for each record to write
	int nCnt = RetOut_Data.getRowCount();
	for(int i=0; i<nCnt; i++)
	{
		//STEP 1: find person ID
		//1. If a Pers. No. is available, search a user record by a Pers. No..
		//2. If 1. is not possible (empty or no user found), use the initials to search a user.
		//3. If 3. is not possible (empty or no user found), use first & last name to search a user
		int nPersonID = -1;
		int nPersonContactID = -1;
		QString strPersonNumber = RetOut_Data.getDataRef(i, "BPER_CODE").toString();
		if(!strPersonNumber.isEmpty())
		{
			//search person by person number
			DbRecordSet resultIDs;
			DbSqlQuery &query = *orm.GetDbSqlQuery();
			QString strQuery = "SELECT BPER_ID, BPER_CONTACT_ID FROM BUS_PERSON WHERE BPER_CODE=?";
			Status status;
			query.Prepare(status, strQuery);
			if(status.IsOK()){
				query.bindValue(0, strPersonNumber);
				query.ExecutePrepared(status);
				if(status.IsOK()){
					query.FetchData(resultIDs);
					if(resultIDs.getRowCount() > 0){
						nPersonID = resultIDs.getDataRef(0, 0).toInt();
						nPersonContactID = resultIDs.getDataRef(0, 1).toInt();
					}
				}
			}
		}
		if(nPersonID <= 0)
		{
			QString strPersonInitials = RetOut_Data.getDataRef(i, "BPER_INITIALS").toString();
			if(!strPersonInitials.isEmpty())
			{
				//search person by person initials
				DbRecordSet resultIDs;
				DbSqlQuery &query = *orm.GetDbSqlQuery();
				QString strQuery = "SELECT BPER_ID, BPER_CONTACT_ID FROM BUS_PERSON WHERE BPER_INITIALS=?";
				Status status;
				query.Prepare(status, strQuery);
				if(status.IsOK()){
					query.bindValue(0, strPersonInitials);
					query.ExecutePrepared(status);
					if(status.IsOK()){
						query.FetchData(resultIDs);
						if(resultIDs.getRowCount() > 0){
							nPersonID = resultIDs.getDataRef(0, 0).toInt();
							nPersonContactID = resultIDs.getDataRef(0, 1).toInt();
						}
					}
				}
			}
		}
		if(nPersonID <= 0)
		{
			QString strPersonFirstName = RetOut_Data.getDataRef(i, "BPER_FIRST_NAME").toString();
			QString strPersonLastName = RetOut_Data.getDataRef(i, "BPER_LAST_NAME").toString();
			if(!strPersonFirstName.isEmpty() && !strPersonLastName.isEmpty())
			{
				//search person by person first and last name
				DbRecordSet resultIDs;
				DbSqlQuery &query = *orm.GetDbSqlQuery();
				QString strQuery = "SELECT BPER_ID, BPER_CONTACT_ID FROM BUS_PERSON WHERE BPER_FIRST_NAME=? AND BPER_LAST_NAME=?";
				Status status;
				query.Prepare(status, strQuery);
				if(status.IsOK()){
					query.bindValue(0, strPersonFirstName);
					query.bindValue(1, strPersonLastName);
					query.ExecutePrepared(status);
					if(status.IsOK()){
						query.FetchData(resultIDs);
						if(resultIDs.getRowCount() > 0){
							nPersonID = resultIDs.getDataRef(0, 0).toInt();
							nPersonContactID = resultIDs.getDataRef(0, 1).toInt();
						}
					}
				}
			}
		}
		if(nPersonID <= 0 || nPersonContactID<=0 ){
			nFailedMatchesCount ++;
			continue;
		}

		//STEP 2: User exists, find contact to be matched against nPersonContactID
		//contact must match name, last name and organization
		//Quote:
		//To find a suitable contact, search for identical First Name, Last Name and Organization. The import
		//format one First Name column only and no Middle Name (this is typical). We do have a middle
		//name field which may be used or not. To be sure to find idenditcal contacts, the First Name column
		//from the import file must be the same as
		//- the First Name field in the contact record, if Middle Name is empty, or
		//- First Name + space + Middle Name in the contact record, if the Middle Name is not empty.
		int nContactID = -1;
		QString strContactFirstName = RetOut_Data.getDataRef(i, "BCNT_FIRSTNAME").toString();
		QString strContactLastName = RetOut_Data.getDataRef(i, "BCNT_LASTNAME").toString();
		QString strContactOrganization = RetOut_Data.getDataRef(i, "BCNT_ORGANIZATIONNAME").toString();
		DbRecordSet resultIDs;
		DbSqlQuery &query = *orm.GetDbSqlQuery();
		Status status;
		QString strQuery = "SELECT BCNT_ID FROM BUS_CM_CONTACT WHERE ";
		if(strContactOrganization.isEmpty())
			strQuery += "(BCNT_ORGANIZATIONNAME IS NULL OR BCNT_ORGANIZATIONNAME='')";
		else
			strQuery += "BCNT_ORGANIZATIONNAME=?";
		if(strContactLastName.isEmpty())
			strQuery += " AND (BCNT_LASTNAME IS NULL OR BCNT_LASTNAME='')";
		else
			strQuery += " AND BCNT_LASTNAME=?";
		if(strContactFirstName.isEmpty())
			strQuery += " AND (BCNT_FIRSTNAME IS NULL OR BCNT_FIRSTNAME='')";
		else{
			strQuery += " AND ((BCNT_FIRSTNAME=? AND (BCNT_MIDDLENAME IS NULL OR BCNT_MIDDLENAME='')) OR ((BCNT_FIRSTNAME || ' ' || BCNT_MIDDLENAME)=?))";
		}

		query.Prepare(status, strQuery);
		if(status.IsOK()){
			int nFieldIdx = 0;
			if(!strContactOrganization.isEmpty()){
				query.bindValue(nFieldIdx, strContactOrganization);
				nFieldIdx ++;
			}
			if(!strContactLastName.isEmpty()){
				query.bindValue(nFieldIdx, strContactLastName);
				nFieldIdx ++;
			}
			if(!strContactFirstName.isEmpty()){
				query.bindValue(nFieldIdx, strContactFirstName);
				nFieldIdx ++;
				query.bindValue(nFieldIdx, strContactFirstName);
				nFieldIdx ++;
			}
			
			query.ExecutePrepared(status);
			if(status.IsOK()){
				query.FetchData(resultIDs);
				if(resultIDs.getRowCount() > 0){
					nContactID = resultIDs.getDataRef(0, 0).toInt();
				}
			}
		}
		if(nContactID <= 0){
			nFailedMatchesCount ++;
			continue;
		}

		//now we must connect two contacts through the role ID
		if(nPersonContactID != nContactID){
			DbRecordSet lstRelations;
			lstRelations.addColumn(QVariant::Int, "ID1");
			lstRelations.addColumn(QVariant::Int, "ID2");
			lstRelations.addColumn(QVariant::String, "ROLE");
			lstRelations.addRow();
			lstRelations.setData(0, 0, nPersonContactID);
			lstRelations.setData(0, 1, nContactID);
			lstRelations.setData(0, 2, RetOut_Data.getDataRef(i, "BUCR_ROLE").toString());

			g_BusinessServiceSet->BusNMRX->WriteRelations(status, lstRelations, BUS_CM_CONTACT, BUS_CM_CONTACT);
		}
	}
}

void Service_BusImport::ImportContactContactRelationships(Status &Ret_pStatus, DbRecordSet &RetOut_Data)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm orm(Ret_pStatus, BUS_CM_CONTACT, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	int nFailedMatchesCount = 0;

	//for each record to write
	int nCnt = RetOut_Data.getRowCount();
	for(int i=0; i<nCnt; i++)
	{
		//STEP 1: Find contact #1
		//contact must match name, last name and organization
		//Quote:
		//To find a suitable contact, search for identical First Name, Last Name and Organization. The import
		//format one First Name column only and no Middle Name (this is typical). We do have a middle
		//name field which may be used or not. To be sure to find idenditcal contacts, the First Name column
		//from the import file must be the same as
		//- the First Name field in the contact record, if Middle Name is empty, or
		//- First Name + space + Middle Name in the contact record, if the Middle Name is not empty.
		int nContact1ID = -1;
		QString strContact1FirstName = RetOut_Data.getDataRef(i, "BCNT1_FIRSTNAME").toString();
		QString strContact1LastName = RetOut_Data.getDataRef(i, "BCNT1_LASTNAME").toString();
		QString strContact1Organization = RetOut_Data.getDataRef(i, "BCNT1_ORGANIZATIONNAME").toString();
		DbRecordSet resultIDs;
		DbSqlQuery &query = *orm.GetDbSqlQuery();
		Status status;
		QString strQuery = "SELECT BCNT_ID FROM BUS_CM_CONTACT WHERE ";
		if(strContact1Organization.isEmpty())
			strQuery += "(BCNT_ORGANIZATIONNAME IS NULL OR BCNT_ORGANIZATIONNAME='')";
		else
			strQuery += "BCNT_ORGANIZATIONNAME=?";
		if(strContact1LastName.isEmpty())
			strQuery += " AND (BCNT_LASTNAME IS NULL OR BCNT_LASTNAME='')";
		else
			strQuery += " AND BCNT_LASTNAME=?";
		if(strContact1FirstName.isEmpty())
			strQuery += " AND (BCNT_FIRSTNAME IS NULL OR BCNT_FIRSTNAME='')";
		else{
			strQuery += " AND ((BCNT_FIRSTNAME=? AND (BCNT_MIDDLENAME IS NULL OR BCNT_MIDDLENAME='')) OR ((BCNT_FIRSTNAME || ' ' || BCNT_MIDDLENAME)=?))";
		}

		query.Prepare(status, strQuery);
		if(status.IsOK()){
			int nFieldIdx = 0;
			if(!strContact1Organization.isEmpty()){
				query.bindValue(nFieldIdx, strContact1Organization);
				nFieldIdx ++;
			}
			if(!strContact1LastName.isEmpty()){
				query.bindValue(nFieldIdx, strContact1LastName);
				nFieldIdx ++;
			}
			if(!strContact1FirstName.isEmpty()){
				query.bindValue(nFieldIdx, strContact1FirstName);
				nFieldIdx ++;
				query.bindValue(nFieldIdx, strContact1FirstName);
				nFieldIdx ++;
			}
			
			query.ExecutePrepared(status);
			if(status.IsOK()){
				query.FetchData(resultIDs);
				if(resultIDs.getRowCount() > 0){
					nContact1ID = resultIDs.getDataRef(0, 0).toInt();
				}
			}
		}
		if(nContact1ID <= 0){
			nFailedMatchesCount ++;
			continue;
		}

		//STEP 2: Find contact #2
		int nContact2ID = -1;
		QString strContact2FirstName = RetOut_Data.getDataRef(i, "BCNT2_FIRSTNAME").toString();
		QString strContact2LastName = RetOut_Data.getDataRef(i, "BCNT2_LASTNAME").toString();
		QString strContact2Organization = RetOut_Data.getDataRef(i, "BCNT2_ORGANIZATIONNAME").toString();
		strQuery = "SELECT BCNT_ID FROM BUS_CM_CONTACT WHERE ";
		if(strContact2Organization.isEmpty())
			strQuery += "(BCNT_ORGANIZATIONNAME IS NULL OR BCNT_ORGANIZATIONNAME='')";
		else
			strQuery += "BCNT_ORGANIZATIONNAME=?";
		if(strContact2LastName.isEmpty())
			strQuery += " AND (BCNT_LASTNAME IS NULL OR BCNT_LASTNAME='')";
		else
			strQuery += " AND BCNT_LASTNAME=?";
		if(strContact2FirstName.isEmpty())
			strQuery += " AND (BCNT_FIRSTNAME IS NULL OR BCNT_FIRSTNAME='')";
		else{
			strQuery += " AND ((BCNT_FIRSTNAME=? AND (BCNT_MIDDLENAME IS NULL OR BCNT_MIDDLENAME='')) OR ((BCNT_FIRSTNAME || ' ' || BCNT_MIDDLENAME)=?))";
		}

		query.Prepare(status, strQuery);
		if(status.IsOK()){
			int nFieldIdx = 0;
			if(!strContact2Organization.isEmpty()){
				query.bindValue(nFieldIdx, strContact2Organization);
				nFieldIdx ++;
			}
			if(!strContact2LastName.isEmpty()){
				query.bindValue(nFieldIdx, strContact2LastName);
				nFieldIdx ++;
			}
			if(!strContact2FirstName.isEmpty()){
				query.bindValue(nFieldIdx, strContact2FirstName);
				nFieldIdx ++;
				query.bindValue(nFieldIdx, strContact2FirstName);
				nFieldIdx ++;
			}
			
			query.ExecutePrepared(status);
			if(status.IsOK()){
				query.FetchData(resultIDs);
				if(resultIDs.getRowCount() > 0){
					nContact2ID = resultIDs.getDataRef(0, 0).toInt();
				}
			}
		}
		if(nContact2ID <= 0){
			nFailedMatchesCount ++;
			continue;
		}

		//now we must connect two contacts through the role ID
		if(nContact1ID != nContact2ID){
			DbRecordSet lstRelations;
			lstRelations.addColumn(QVariant::Int, "ID1");
			lstRelations.addColumn(QVariant::Int, "ID2");
			lstRelations.addColumn(QVariant::String, "ROLE");
			lstRelations.addRow();
			lstRelations.setData(0, 0, nContact1ID);
			lstRelations.setData(0, 1, nContact2ID);
			lstRelations.setData(0, 2, RetOut_Data.getDataRef(i, "BUCR_ROLE").toString());

			g_BusinessServiceSet->BusNMRX->WriteRelations(status, lstRelations, BUS_CM_CONTACT, BUS_CM_CONTACT);
		}
	}
}

void Service_BusImport::GetContactContactRelationships(Status &Ret_pStatus, DbRecordSet &RetOut_Data)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm orm(Ret_pStatus, BUS_CM_CONTACT, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	//append and fill additional fields ROLE, contact info
	RetOut_Data.addColumn(DbRecordSet::GetVariantType(), "LST_ROLES");

	DbRecordSet lstRoles;
	lstRoles.addColumn(QVariant::Int, "BUCR_ROLE_DIRECTION");
	lstRoles.addColumn(QVariant::String, "BUCR_ROLE");
	lstRoles.addColumn(QVariant::String, "BUCR_DESCRIPTION");
	lstRoles.addColumn(QVariant::String, "BCNT_FIRSTNAME");
	lstRoles.addColumn(QVariant::String, "BCNT_LASTNAME");
	lstRoles.addColumn(QVariant::String, "BCNT_ORGANIZATIONNAME");
	lstRoles.addColumn(QVariant::String, "BCNT_MIDDLENAME");
	
	//update additional data
	int nRows = RetOut_Data.getRowCount();
	for(int i=0; i<nRows; i++)
	{
		int nCntID = RetOut_Data.getDataRef(i, "BCNT_ID").toInt();

		lstRoles.clear();

		//calc roles
		DbRecordSet lstDataNMRX;
		DbRecordSet lstDataContacts;
		g_BusinessServiceSet->BusContact->ReadNMRXContactsFromContact_Ext(Ret_pStatus, nCntID, lstDataNMRX, lstDataContacts);
		if(!Ret_pStatus.IsOK())
			return;	//TOFIX

		int nRolesCnt = lstDataNMRX.getRowCount();
		int nContactCnt = lstDataContacts.getRowCount();
#ifdef _DEBUG
		//lstDataNMRX.Dump();
		//lstDataContacts.Dump();
#endif
		Q_ASSERT(nRolesCnt == nContactCnt);

		//fill in the data
		int j;
		for(j=0; j<nRolesCnt; j++){
			lstRoles.addRow();
			int nRowIdx = lstRoles.getRowCount()-1;
			lstRoles.setData(nRowIdx, "BUCR_ROLE_DIRECTION", lstDataNMRX.getDataRef(j, "IS_AB").toInt());
			lstRoles.setData(nRowIdx, "BUCR_ROLE", lstDataNMRX.getDataRef(j, "BNRO_NAME").toString());
			lstRoles.setData(nRowIdx, "BUCR_DESCRIPTION", lstDataNMRX.getDataRef(j, "BNRO_DESCRIPTION").toString());
			lstRoles.setData(nRowIdx, "BCNT_FIRSTNAME", lstDataContacts.getDataRef(j, "BCNT_FIRSTNAME").toString());
			lstRoles.setData(nRowIdx, "BCNT_LASTNAME", lstDataContacts.getDataRef(j, "BCNT_LASTNAME").toString());
			lstRoles.setData(nRowIdx, "BCNT_ORGANIZATIONNAME", lstDataContacts.getDataRef(j, "BCNT_ORGANIZATIONNAME").toString());
			lstRoles.setData(nRowIdx, "BCNT_MIDDLENAME", lstDataContacts.getDataRef(j, "BCNT_MIDDLENAME").toString());
		}

		//MB: do the same for contact-user relationships
		DbRecordSet lstDataNMRX1;
		DbRecordSet lstDataContacts1;
		g_BusinessServiceSet->BusContact->ReadNMRXUsersFromContact_Ext(Ret_pStatus, nCntID, lstDataNMRX1, lstDataContacts1);
		if(!Ret_pStatus.IsOK())
			return;	//TOFIX

		int nRolesCnt1 = lstDataNMRX1.getRowCount();
		int nContactCnt1 = lstDataContacts1.getRowCount();
#ifdef _DEBUG
		//lstDataNMRX1.Dump();
		//lstDataContacts1.Dump();
#endif
		Q_ASSERT(nRolesCnt1 == nContactCnt1);

		//fill in the data
		for(j=0; j<nRolesCnt1; j++){
			lstRoles.addRow();
			int nRowIdx = lstRoles.getRowCount()-1;
			lstRoles.setData(nRowIdx, "BUCR_ROLE_DIRECTION", lstDataNMRX1.getDataRef(j, "IS_AB").toInt());
			lstRoles.setData(nRowIdx, "BUCR_ROLE", lstDataNMRX1.getDataRef(j, "BNRO_NAME").toString());
			lstRoles.setData(nRowIdx, "BUCR_DESCRIPTION", lstDataNMRX1.getDataRef(j, "BNRO_DESCRIPTION").toString());
			lstRoles.setData(nRowIdx, "BCNT_FIRSTNAME", lstDataContacts1.getDataRef(j, "BPER_FIRSTNAME").toString());
			lstRoles.setData(nRowIdx, "BCNT_LASTNAME", lstDataContacts1.getDataRef(j, "BPER_LASTNAME").toString());
			lstRoles.setData(nRowIdx, "BCNT_ORGANIZATIONNAME", lstDataContacts1.getDataRef(j, "BCNT_ORGANIZATIONNAME").toString());
			lstRoles.setData(nRowIdx, "BCNT_MIDDLENAME", lstDataContacts1.getDataRef(j, "BCNT_MIDDLENAME").toString());
		}

		RetOut_Data.setData(i, "LST_ROLES", lstRoles);
	}
}

//copies data from contacts, creates persons & users, status is NOT copied back...TOFIX
//contid->
void Service_BusImport::WritePersonUserFromContact(Status &Ret_pStatus, DbRecordSet &RetOut_Data)
{

	//filter only contacts with status_code==0:
	DbRecordSet lstCleanContacts;
	lstCleanContacts.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_CONTACT));
	RetOut_Data.find(RetOut_Data.getColumnIdx("STATUS_CODE"),0);
	lstCleanContacts.merge(RetOut_Data,true);

	//if no valid contact get out
	if(lstCleanContacts.getRowCount()==0)
		return;

	//WRITE PERSONS:
	DbSimpleOrm TablePerson(Ret_pStatus, BUS_PERSON, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	DbRecordSet lstPerson,lstStatuses;
	BusinessDataTransformer::Contact2Person(lstCleanContacts,lstPerson);
//	lstPerson.Dump();
	TablePerson.Write(Ret_pStatus,lstPerson,-1,1,&lstStatuses); //skip BPIC_PICTURE
	//AssignErrorsToParentStatuses()

	//WRITE USERS:
	DbSimpleOrm TableUser(Ret_pStatus, CORE_USER, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	DbRecordSet lstUser;
	
	BusinessDataTransformer::Person2User(lstPerson,lstUser);
	//lstUser.Dump();
	TableUser.Write(Ret_pStatus,lstUser,-1,0,&lstStatuses);
	//AssignErrorsToParentStatuses(

}

//Based on algorithm by MB:
//Algorithm for initials: generate initials string starting at i=1 and j=1 and check, whether it is already used as a username. If it is not, increment j and i in turn by generating new initials strings until you find a unique name. If i and j reach the length of the first and last name, use the personal code for the username.
//Formula: Initials i,j:= LeftString(FirstName,i) + LeftString(LastName,j)
//before this, set username = initials from person
//lstUser , must have columns at end: STATUS_CODE, STATUS_TEXT
void Service_BusImport::GenerateUsername(Status &Ret_pStatus,DbRecordSet &lstUser)
{
	//make local query, can fail if connection reservation fails
	DbSqlQuery query(Ret_pStatus,GetDbManager());
	if(!Ret_pStatus.IsOK()) return;

	bool bSucess=false;

	//prepare statement:
	QString strSQL="SELECT COUNT(*) FROM CORE_USER WHERE CUSR_USERNAME=?";
	query.Prepare(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK()) return;

	QString strFirstName,strLastName,strUserName;
	int nUserNameIdx=lstUser.getColumnIdx("CUSR_USERNAME");
	int nFirstNameIdx=lstUser.getColumnIdx("CUSR_FIRST_NAME");
	int nLastNameIdx=lstUser.getColumnIdx("CUSR_LAST_NAME");
	int nStatusCodeIdx=lstUser.getColumnIdx("STATUS_CODE");
	int nStatusTextIdx=lstUser.getColumnIdx("STATUS_TEXT");

	//bind value:
	query.bindValue(0,strUserName);

	//for each user, test username on DB: too long!!!
	//loop if username is set, skip, if not generate, store index 
	//check in DB for occurences
	//if 
	int nSize=lstUser.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		bSucess=false;
		strUserName=lstUser.getDataRef(i,nUserNameIdx).toString();
		strFirstName=lstUser.getDataRef(i,nFirstNameIdx).toString();
		strLastName=lstUser.getDataRef(i,nLastNameIdx).toString();
		int nMaxLength=strFirstName.size(); //get lowest length
		if (strLastName.size()<nMaxLength)
			nMaxLength=strLastName.size();

		//if username is already set, test it:
		if (!strUserName.isEmpty())
		{
			query.bindValue(0,strUserName);
			query.ExecutePrepared(Ret_pStatus);
			if(!Ret_pStatus.IsOK()) return;

			int nCount=0;
			if(query.GetQSqlQuery()->next())
				nCount=query.GetQSqlQuery()->value(0).toInt();
			if (nCount==0) //username sucessfully teste!
				continue;
		}
		
		//increase username and test
		for(int k=2;k<nMaxLength;k++)
		{
			strUserName=strFirstName.left(k)+strLastName.left(k);
			query.bindValue(0,strUserName);
			query.ExecutePrepared(Ret_pStatus);
			if(!Ret_pStatus.IsOK()) return;
			int nCount=0;
			if(query.GetQSqlQuery()->next())
				nCount=query.GetQSqlQuery()->value(0).toInt();
			if (nCount==0) //username sucessfully teste!
			{
				bSucess=true;
				lstUser.setData(i,nUserNameIdx,strUserName);
				break;
			}
		}

		//if failed, set error
		if (!bSucess)
		{
			//if we reach this point, it means we can not generate unique username: set error
			lstUser.setData(i,nStatusCodeIdx,StatusCodeSet::ERR_BUS_FAILED_TO_GENERATE_USERNAME);
			//lstUser.setData(i,nStatusTextIdx,"strUserName");
			//STATUS_CODE
		}
	}
}

void Service_BusImport::FindContactDuplicates(Status &status, const DbRecordSet& contact, DbRecordSet& resultIDs, DbSqlQuery &query)
{
	resultIDs.clear();

	bool bFirstNameNULL = contact.getDataRef(0, "BCNT_FIRSTNAME").isNull();
	bool bLastNameNULL  = contact.getDataRef(0, "BCNT_LASTNAME").isNull();
	bool bOrgNameNULL   = contact.getDataRef(0, "BCNT_ORGANIZATIONNAME").isNull();
	bool bLocationNULL  = contact.getDataRef(0, "BCNT_LOCATION").isNull();

	//check if such contact already exists in a database  (fetch only ID for found contacts)
	QString strQuery = "SELECT BCNT_ID FROM BUS_CM_CONTACT WHERE BCNT_FIRSTNAME";
	if(bFirstNameNULL)	strQuery += " IS NULL "; else strQuery += "=? ";
	strQuery += "AND BCNT_LASTNAME";
	if(bLastNameNULL)	strQuery += " IS NULL "; else strQuery += "=? ";
	strQuery += "AND BCNT_ORGANIZATIONNAME";
	if(bOrgNameNULL)	strQuery += " IS NULL "; else strQuery += "=?";
	strQuery += "AND BCNT_LOCATION";
	if(bLocationNULL)	strQuery += " IS NULL "; else strQuery += "=?";

	query.Prepare(status, strQuery);
	if(!status.IsOK())
		return;
	int nField = 0;
	if(!bFirstNameNULL)
		query.bindValue(nField++, contact.getDataRef(0, "BCNT_FIRSTNAME").toString());
	if(!bLastNameNULL)
		query.bindValue(nField++, contact.getDataRef(0, "BCNT_LASTNAME").toString());
	if(!bOrgNameNULL)
		query.bindValue(nField++, contact.getDataRef(0, "BCNT_ORGANIZATIONNAME").toString());
	if(!bLocationNULL)
		query.bindValue(nField++, contact.getDataRef(0, "BCNT_LOCATION").toString());

	query.ExecutePrepared(status);
	if(!status.IsOK())
		return;
	query.FetchData(resultIDs);
}

void Service_BusImport::FindContactDuplicatesFull(Status &status, const DbRecordSet& contact, DbRecordSet& resultRows, DbSqlQuery &query)
{
	resultRows.clear();

	bool bFirstNameNULL = contact.getDataRef(0, "BCNT_FIRSTNAME").isNull();
	bool bLastNameNULL  = contact.getDataRef(0, "BCNT_LASTNAME").isNull();
	bool bOrgNameNULL   = contact.getDataRef(0, "BCNT_ORGANIZATIONNAME").isNull();
	bool bLocationNULL  = contact.getDataRef(0, "BCNT_LOCATION").isNull();

	//check if such contact already exists in a database (fetch full data for found contacts)
	QString strQuery  = DbSqlTableView::getSQLView(DbSqlTableView::TVIEW_BUS_CONTACT_FULL);
			strQuery += " WHERE BCNT_FIRSTNAME";
	if(bFirstNameNULL)	strQuery += " IS NULL "; else strQuery += "=? ";
	strQuery += "AND BCNT_LASTNAME";
	if(bLastNameNULL)	strQuery += " IS NULL "; else strQuery += "=? ";
	strQuery += "AND BCNT_ORGANIZATIONNAME";
	if(bOrgNameNULL)	strQuery += " IS NULL "; else strQuery += "=?";
	strQuery += "AND BCNT_LOCATION";
	if(bLocationNULL)	strQuery += " IS NULL "; else strQuery += "=?";

	query.Prepare(status, strQuery);
	if(!status.IsOK())
		return;
	int nField = 0;
	if(!bFirstNameNULL)
		query.bindValue(nField++, contact.getDataRef(0, "BCNT_FIRSTNAME").toString());
	if(!bLastNameNULL)
		query.bindValue(nField++, contact.getDataRef(0, "BCNT_LASTNAME").toString());
	if(!bOrgNameNULL)
		query.bindValue(nField++, contact.getDataRef(0, "BCNT_ORGANIZATIONNAME").toString());
	if(!bLocationNULL)
		query.bindValue(nField++, contact.getDataRef(0, "BCNT_LOCATION").toString());

	query.ExecutePrepared(status);
	if(!status.IsOK())
		return;
	query.FetchData(resultRows);
}

void Service_BusImport::UpdateContactPhones(Status &status, int nContactID, const DbRecordSet& data, DbSimpleOrm &orm)
{
	//fetch all phones for this contact
	QString strQuery;
	strQuery.sprintf("SELECT BCMP_FULLNUMBER FROM BUS_CM_PHONE WHERE BCMP_CONTACT_ID=%d", nContactID);
	orm.GetDbSqlQuery()->Execute(status, strQuery);
	if(!status.IsOK())
		return;
	
	DbRecordSet resultRows;
	orm.GetDbSqlQuery()->FetchData(resultRows);
	int nResCnt = resultRows.getRowCount();

	//clean incoming phone list from all phones already in the database
	DbRecordSet lstPhones = data.getDataRef(0, "LST_PHONE").value<DbRecordSet>();
	int nNumCol = lstPhones.getColumnIdx("BCMP_FULLNUMBER");
	for(int i=0; i<nResCnt; i++){
		lstPhones.find(nNumCol, resultRows.getDataRef(i, 0));
		lstPhones.deleteSelectedRows();
	}

	//fill foreign key field
	int nCnt = lstPhones.getRowCount();
	for(int i=0; i<nCnt; i++){
		lstPhones.setData(i, "BCMP_CONTACT_ID", nContactID);
	}

	//now write the remaining phones into the database
	DbSimpleOrm orm1(status, BUS_CM_PHONE, GetDbManager()); 
	if(!status.IsOK()) return;
	//lstPhones.Dump();
	orm1.Write(status, lstPhones, DbSqlTableView::TVIEW_BUS_CM_PHONE,1);
}

void Service_BusImport::UpdateContactEmails(Status &status, int nContactID, const DbRecordSet& data, DbSimpleOrm &orm)
{
	//fetch all emails for this contact
	QString strQuery;
	strQuery.sprintf("SELECT BCME_ADDRESS FROM BUS_CM_EMAIL WHERE BCME_CONTACT_ID=%d", nContactID);
	orm.GetDbSqlQuery()->Execute(status, strQuery);
	if(!status.IsOK())
		return;
	
	DbRecordSet resultRows;
	orm.GetDbSqlQuery()->FetchData(resultRows);
	int nResCnt = resultRows.getRowCount();

	//clean incoming phone list from all phones already in the database
	DbRecordSet lstMails = data.getDataRef(0, "LST_EMAIL").value<DbRecordSet>();
	int nNumCol = lstMails.getColumnIdx("BCME_ADDRESS");
	for(int i=0; i<nResCnt; i++){
		lstMails.find(nNumCol, resultRows.getDataRef(i, 0));
		lstMails.deleteSelectedRows();
	}

	//fill foreign key field
	int nCnt = lstMails.getRowCount();
	for(int i=0; i<nCnt; i++){
		lstMails.setData(i, "BCME_CONTACT_ID", nContactID);
	}

	//now write the remaining emails into the database
	DbSimpleOrm orm1(status, BUS_CM_EMAIL, GetDbManager()); 
	if(!status.IsOK()) return;
	//lstMails.Dump();
	orm1.Write(status, lstMails, DbSqlTableView::TVIEW_BUS_CM_EMAIL,1);
}

void Service_BusImport::UpdateContactInternet(Status &status, int nContactID, const DbRecordSet& data, DbSimpleOrm &orm)
{
	//fetch all URLS for this contact
	QString strQuery;
	strQuery.sprintf("SELECT BCMI_ADDRESS FROM BUS_CM_INTERNET WHERE BCMI_CONTACT_ID=%d", nContactID);
	orm.GetDbSqlQuery()->Execute(status, strQuery);
	if(!status.IsOK())
		return;
	
	DbRecordSet resultRows;
	orm.GetDbSqlQuery()->FetchData(resultRows);
	int nResCnt = resultRows.getRowCount();

	//clean incoming list from all URLs already in the database
	DbRecordSet lstURLS = data.getDataRef(0, "LST_INTERNET").value<DbRecordSet>();
	int nNumCol = lstURLS.getColumnIdx("BCMI_ADDRESS");
	for(int i=0; i<nResCnt; i++){
		lstURLS.find(nNumCol, resultRows.getDataRef(i, 0));
		lstURLS.deleteSelectedRows();
	}

	//fill foreign key field
	int nCnt = lstURLS.getRowCount();
	for(int i=0; i<nCnt; i++){
		lstURLS.setData(i, "BCMI_CONTACT_ID", nContactID);
	}

	//now write the remaining URLs into the database
	DbSimpleOrm orm1(status, BUS_CM_INTERNET, GetDbManager()); 
	if(!status.IsOK()) return;
	//lstURLS.Dump();
	orm1.Write(status, lstURLS,DbSqlTableView::TVIEW_BUS_CM_INTERNET,1);
}

void Service_BusImport::UpdateContactAddresses(Status &status, int nContactID, const DbRecordSet& data, DbSimpleOrm &orm)
{
	//fetch all Addresses for this contact
	QString strQuery;
	strQuery.sprintf("SELECT BCMA_FORMATEDADDRESS FROM BUS_CM_ADDRESS WHERE BCMA_CONTACT_ID=%d", nContactID);
	orm.GetDbSqlQuery()->Execute(status, strQuery);
	if(!status.IsOK())
		return;

	DbRecordSet resultRows;
	orm.GetDbSqlQuery()->FetchData(resultRows);
	int nResCnt = resultRows.getRowCount();

	//clean incoming list from all Addresses already in the database
	DbRecordSet lstAddrs = data.getDataRef(0, "LST_ADDRESS").value<DbRecordSet>();
	int nNumCol = lstAddrs.getColumnIdx("BCMA_FORMATEDADDRESS");
	for(int i=0; i<nResCnt; i++){
		lstAddrs.find(nNumCol, resultRows.getDataRef(i, 0));
		lstAddrs.deleteSelectedRows();
	}

	//fill foreign key field
	int nCnt = lstAddrs.getRowCount();
	for(int i=0; i<nCnt; i++){
		lstAddrs.setData(i, "BCMA_CONTACT_ID", nContactID);
	}

	//now write the remaining Addresses into the database
	DbSimpleOrm orm1(status, BUS_CM_ADDRESS, GetDbManager()); 
	if(!status.IsOK()) return;
	//lstAddrs.Dump();
	orm1.Write(status, lstAddrs, DbSqlTableView::TVIEW_BUS_CM_ADDRESS,2);


	//B.T. 12.03.2013: check if there is any address type, if not, then insert new one: default one (issue 2582)
	int nDefaultAddressTypeID=GetDefaultAdddressType();
	if (nDefaultAddressTypeID>0)
	{
		for(int i=0; i<nCnt; i++)
		{
			int nAddrID=lstAddrs.getDataRef(i,"BCMA_ID").toInt();
			strQuery="SELECT COUNT(*) FROM BUS_CM_ADDRESS_TYPES WHERE BCMAT_ADDRESS_ID = "+QString::number(nAddrID);
			orm.GetDbSqlQuery()->Execute(status, strQuery);
			if(!status.IsOK())
				return;

			int nCnt=0;
			if (orm.GetDbSqlQuery()->next())
				nCnt =orm.GetDbSqlQuery()->value(0).toInt();

			if (nCnt==0)
			{
				DbRecordSet rowAddrType;
				rowAddrType.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_ADDRESS_TYPES));
				rowAddrType.addRow();
				rowAddrType.setData(0,"BCMAT_ADDRESS_ID",nAddrID);
				rowAddrType.setData(0,"BCMAT_TYPE_ID",nDefaultAddressTypeID);

				//Make local query, can fail if connection reservation fails.
				DbSimpleOrm TableOrm(status, BUS_CM_ADDRESS_TYPES, GetDbManager()); 
				if(!status.IsOK()) return;

				TableOrm.Write(status,rowAddrType);
				if(!status.IsOK()) return;
			}
		}
	}




}

void Service_BusImport::UpdateContactJournal(Status &status, int nContactID, const DbRecordSet& data, DbSimpleOrm &orm)
{
	//NOTE: here we do not clean incoming list from all Addresses already in the database 
	//		there is no way to uniquely identify the journal data
	DbRecordSet lstJournal = data.getDataRef(0, "LST_JOURNAL").value<DbRecordSet>();

	//fill foreign key field
	int nCnt = lstJournal.getRowCount();
	//B.T.: if zero then skip, nothing to write, delete?
	if( nCnt==0)
		return;

	for(int i=0; i<nCnt; i++){
		lstJournal.setData(i, "BCMJ_CONTACT_ID", nContactID);
	}

	//now write the remaining Addresses into the database
	DbSimpleOrm orm1(status, BUS_CM_JOURNAL, GetDbManager()); 
	if(!status.IsOK()) return;
	orm1.Write(status, lstJournal, DbSqlTableView::TVIEW_BUS_CM_JOURNAL);
}

void Service_BusImport::UpdateContactDebtor(Status &status, int nContactID, const DbRecordSet& data, DbSimpleOrm &orm)
{
	//delete all Debtor records for this contact
	QString strQuery;
	strQuery.sprintf("DELETE FROM BUS_CM_DEBTOR WHERE BCMD_CONTACT_ID=%d", nContactID);
	orm.GetDbSqlQuery()->Execute(status, strQuery);
	if(!status.IsOK())
		return;

	DbRecordSet dataRows;
	dataRows.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_DEBTOR));
	dataRows.merge(data);
	
	//fill foreign key field
	int nCnt = dataRows.getRowCount();
	//B.T.: if zero then skip, nothing to write, delete?
	if( nCnt==0)
		return;

	for(int i=0; i<nCnt; i++){
		dataRows.setData(i, "BCMD_CONTACT_ID", nContactID);
	}

	//now write the remaining Debtors into the database
	DbSimpleOrm orm1(status, BUS_CM_DEBTOR, GetDbManager()); 
	if(!status.IsOK()) return;
	//dataRows.Dump();
	orm1.Write(status, dataRows, DbSqlTableView::TVIEW_BUS_CM_DEBTOR);
}

//write group data
void Service_BusImport::WriteGroupContactData(Status &Ret_pStatus, int nOperation,DbRecordSet &RetOut_Data)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_CM_GROUP, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	//write group data one by one
	int nCntIdx=RetOut_Data.getColumnIdx("BCNT_ID");
	int nLstGroupsIdx=RetOut_Data.getColumnIdx("LST_GROUPS");
	int nStatusCodeIdx=RetOut_Data.getColumnIdx("STATUS_CODE");
	int nStatusTextIdx=RetOut_Data.getColumnIdx("STATUS_TEXT");

	Status err;
	int nSize=RetOut_Data.getRowCount();

	for(int i=0;i<nSize;++i)
	{
		int nContactID=RetOut_Data.getDataRef(i,nCntIdx).toInt();
		if (nContactID==0) continue;									//if cnt id ==0
		if (RetOut_Data.getDataRef(i,nStatusCodeIdx).toInt()!=0) continue;	//if previous error
		DbRecordSet lstGroups=RetOut_Data.getDataRef(i,nLstGroupsIdx).value<DbRecordSet>();
		if (lstGroups.getRowCount()==0)continue;						//if nothing to import skip
		int nContIdxInSub=lstGroups.getColumnIdx("BGCN_CONTACT_ID");
		if (nContIdxInSub==-1)continue;

		lstGroups.setColValue(nContIdxInSub,nContactID);		//set contact id, 

		WriteGroupContactRecord(err,nOperation,lstGroups,TableOrm,nContactID);

		RetOut_Data.setData(i,nStatusCodeIdx,err.getErrorCode());
		RetOut_Data.setData(i,nStatusTextIdx,err.getErrorText());
		if (!err.IsOK())
			Ret_pStatus=err;
	}
}

//writes all group assignments for one Contact
void Service_BusImport::WriteGroupContactRecord(Status &Ret_pStatus, int nOperation,DbRecordSet &lstContacGroup, DbSimpleOrm &TableOrm,int nContactID)
{
	DbSqlQuery *query=TableOrm.GetDbSqlQuery();

	int nCategoryIdx=lstContacGroup.getColumnIdx("BGIT_EXTERNAL_CATEGORY");
	int nGroupIDIdx=lstContacGroup.getColumnIdx("BGCN_ITEM_ID");

	//LOAD previous assiggment (nodes)
	DbRecordSet lstItemsAssigned;
	query->Execute(Ret_pStatus, "SELECT DISTINCT BGCN_ITEM_ID FROM bus_cm_group WHERE BGCN_CONTACT_ID="+QVariant(nContactID).toString());
	if(!Ret_pStatus.IsOK())	return;
	query->FetchData(lstItemsAssigned);

	lstItemsAssigned.setColumn(0,QVariant::Int,"BGIT_ID");

	//LOAD current assignment (nodes)
	DbRecordSet lstItemsToBeAssigned;
	QString strWhereIn;
	int nCount=DbSqlTableDefinition::GenerateChunkedWhereStatement(lstContacGroup,"BGIT_EXTERNAL_CATEGORY",strWhereIn,0,-1,true);
	if (nCount>=0)
	{
		query->Execute(Ret_pStatus, "SELECT DISTINCT BGIT_ID,BGIT_EXTERNAL_CATEGORY FROM bus_group_items WHERE BGIT_EXTERNAL_CATEGORY IN "+strWhereIn);
		if(!Ret_pStatus.IsOK())	return;
		query->FetchData(lstItemsToBeAssigned);
	}
	lstItemsAssigned.merge(lstItemsToBeAssigned);
	lstItemsAssigned.removeDuplicates(0);
	

	//LOCK ALL NODES (PROTOCOL For Changing node assignment/content tell us to lock node):
	QString strLocked;
	BusinessLocker locker(Ret_pStatus,BUS_GROUP_ITEMS,GetDbManager(), TableOrm.GetDbConnection());
	locker.Lock(Ret_pStatus,lstItemsAssigned,strLocked);
	if(!Ret_pStatus.IsOK())	return;

	//tran:
	query->BeginTransaction(Ret_pStatus);
	if(!Ret_pStatus.IsOK())
	{
		Status err;
		locker.UnLock(err,strLocked);
		return;
	}

	//delete previous assigment:
	query->Execute(Ret_pStatus, "DELETE FROM bus_cm_group WHERE BGCN_CONTACT_ID="+QVariant(nContactID).toString());
	if(!Ret_pStatus.IsOK())
	{
		Status err;
		locker.UnLock(err,strLocked);
		query->Rollback();
		return;
	}


	//update for new assignment:

	DbRecordSet lstContacGroupAssigment;
	lstContacGroupAssigment.copyDefinition(lstContacGroup);

	//select all item Id's with given category
	int nSize=lstItemsToBeAssigned.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		int nRow=lstContacGroup.find(nCategoryIdx,lstItemsToBeAssigned.getDataRef(i,1),true);
		if (nRow!=-1)
		{
			lstContacGroupAssigment.addRow();
			int nLastAdded=lstContacGroupAssigment.getRowCount()-1;
			DbRecordSet row = lstContacGroup.getRow(nRow);
			lstContacGroupAssigment.assignRow(nLastAdded,row);
			lstContacGroupAssigment.setData(nLastAdded,nGroupIDIdx,lstItemsToBeAssigned.getDataRef(i,0));
		}
	}


	if (lstContacGroupAssigment.getRowCount()>0)
	{
		TableOrm.Write(Ret_pStatus,lstContacGroupAssigment,-1,1); //write assignment, skip last col
		if(!Ret_pStatus.IsOK())
		{
			Status err;
			locker.UnLock(err,strLocked);
			query->Rollback();
			return;
		}
	}

	//commit //unlock:
	query->Commit(Ret_pStatus);
	
	Status err;
	locker.UnLock(err,strLocked);
}

void Service_BusImport::FindPersonDuplicates(Status &status, const DbRecordSet& contact, DbRecordSet& resultIDs, DbSqlQuery &query)
{
	resultIDs.clear();

	bool bCodeNULL = contact.getDataRef(0, "BPER_CODE").isNull();

	//check if such contact already exists in a database  (fetch only ID for found contacts)
	QString strQuery = "SELECT * FROM BUS_PERSON WHERE BPER_CODE";
	if(bCodeNULL) strQuery += " IS NULL "; else strQuery += "=?";

	query.Prepare(status, strQuery);
	if(!status.IsOK())
		return;
	int nField = 0;
	if(!bCodeNULL)
		query.bindValue(nField++, contact.getDataRef(0, "BPER_CODE").toString());

	query.ExecutePrepared(status);
	if(!status.IsOK())
		return;
	query.FetchData(resultIDs);
}

void Service_BusImport::FindProjectDuplicates(Status &status, const DbRecordSet& contact, DbRecordSet& resultIDs, DbSqlQuery &query)
{
	resultIDs.clear();

	bool bCodeNULL = contact.getDataRef(0, "BUSP_CODE").isNull();

	//check if such contact already exists in a database  (fetch only ID for found contacts)
	QString strQuery = "SELECT * FROM BUS_PROJECTS WHERE BUSP_CODE";
	if(bCodeNULL) strQuery += " IS NULL "; else strQuery += "=?";

	query.Prepare(status, strQuery);
	if(!status.IsOK())
		return;
	int nField = 0;
	if(!bCodeNULL)
		query.bindValue(nField++, contact.getDataRef(0, "BUSP_CODE").toString());

	query.ExecutePrepared(status);
	if(!status.IsOK())
		return;
	query.FetchData(resultIDs);
}

void Service_BusImport::WriteHierarchicalData(Status &Ret_pStatus, DbRecordSet &RetOut_Data)
{
	m_TreeItemManager.Write(Ret_pStatus,RetOut_Data,"","");
}

//automated data exchange
void Service_BusImport::ProcessProjects_Import(Status &Ret_pStatus, QString strFileExt, QString strFilePrefix, QString strFileExtOfProcessed, QString strDirPath, DbRecordSet &Ret_ProcessedFiles, DbRecordSet &Ret_pErrors)
{
	strDirPath=QDir::toNativeSeparators(strDirPath);
	if(strDirPath.isEmpty() || strDirPath.at(strDirPath.size()-1) != QDir::separator())
		strDirPath += QDir::separator();

	//load column mapping / code mapping files
	QMap<int, int> mapCols;
	QMap<QString, QString> mapCodes;
	QString strNamesHeader;
	bool bUseColMap     = LoadColMap(strDirPath + "SOK_Map_Cols_Proj_Imp.map", "~~~SOKRATES Data Exchange - Version: 1.00 - Format: Projects Column Mapping - Subformat: Project Import", mapCols, strNamesHeader, true);
	bool bUseHCodeMap = LoadHCodeMap(strDirPath + "SOK_Hier_Map_Proj_Imp.map", "~~~SOKRATES Data Exchange - Version: 1.00 - Format: Projects Code Mapping - Subformat: Projects Import", mapCodes);

	//redefine list of processed files
	Ret_ProcessedFiles.destroy();
	Ret_ProcessedFiles.addColumn(QVariant::String, "FileName");
	Ret_ProcessedFiles.addColumn(QVariant::Int,	   "FileOK");
	Ret_ProcessedFiles.addColumn(QVariant::String, "FileErrorTxt");
	Ret_ProcessedFiles.addColumn(QVariant::Int,	   "NumRecsSuccess");
	Ret_ProcessedFiles.addColumn(QVariant::Int,	   "NumRecsFailed");

	//redefine list of errors
	Ret_pErrors.destroy();
	Ret_pErrors.addColumn(QVariant::String, "FileName");
	Ret_pErrors.addColumn(QVariant::Int,	"RecIdx");	//starts from 1
	Ret_pErrors.addColumn(QVariant::Int,	"ErrNo");
	Ret_pErrors.addColumn(QVariant::String, "ErrText");

	//list files matching the pattern
	QString strTemplate;
	if(!strFilePrefix.isEmpty())
		strTemplate += strFilePrefix + "*";
	if(!strFileExt.isEmpty()){
		if(strTemplate.isEmpty())
			strTemplate += "*";
		strTemplate += ".";
		strTemplate += strFileExt;
	}
	QStringList filters;
    filters << strTemplate;
	QDir dir(strDirPath);
	QStringList lstFiles = dir.entryList(filters);

	int nCount = lstFiles.count();
	for(int j=0; j<nCount; j++)
	{
		QString strCurFile = lstFiles[j];

		Ret_pStatus.setError(0);
		
		//skip "." and ".."
		if(strCurFile == "." || strCurFile == "..")
			continue;

		strCurFile = strDirPath + strCurFile;

		//log file as processed
		Ret_ProcessedFiles.addRow();
		int nLogRows = Ret_ProcessedFiles.getRowCount();
		Ret_ProcessedFiles.setData(nLogRows-1, "FileName", strCurFile);

		DbRecordSet lstInput;
		CsvImportFile file;
		//QString strFormat1 = "~~~SOKRATES Data Exchange - Version: 1.00 - Format: Projects - Subformat: Project Main Short";
		//QString strFormat2 = "~~~SOKRATES Data Exchange - Version: 1.00 - Charset: UTF8 - Format: Projects - Subformat: Project Main Short";

		QStringList lstFormatPlain;
		lstFormatPlain << "~~~SOKRATES Data Exchange - Version: 1.00 - Format: Projects - Subformat: Project Main Short";
		lstFormatPlain << "~~~SOKRATES Data Exchange - Version: 3.00 - Format: Projects - Subformat: Project Main Short"; //B.T. added on 26.03.2013
		QStringList lstFormatUtf8;
		lstFormatUtf8 << "~~~SOKRATES Data Exchange - Version: 1.00 - Charset: UTF8 - Format: Projects - Subformat: Project Main Short";

		file.Load(Ret_pStatus, strCurFile, lstInput, lstFormatPlain, lstFormatUtf8);
		if(Ret_pStatus.IsOK())
		{
			//B.T. added:
			bool bIsVersion2 = ("~~~SOKRATES Data Exchange - Version: 2.00 - Format: Projects - Subformat: Project Main Short" == file.GetHeaderLine());
			bool bIsVersion3 = ("~~~SOKRATES Data Exchange - Version: 3.00 - Format: Projects - Subformat: Project Main Short" == file.GetHeaderLine());
			int nFieldsCnt = lstInput.getColumnCount();
			int nMandatoryCnt = (bIsVersion2)? 23 : 21;
			if (bIsVersion3)
			{
				nMandatoryCnt=40;
			}

			Ret_ProcessedFiles.setData(nLogRows-1, "FileOK", 0);
			if(!bUseColMap)
			{
				if(nMandatoryCnt != nFieldsCnt)
				{
					//MB: field count is checked only when no col mapping used
					Ret_ProcessedFiles.setData(nLogRows-1, "FileOK", 1);
					Ret_ProcessedFiles.setData(nLogRows-1, "FileErrorTxt", QObject::tr("Document does not contain %1 fields for each record!").arg(21));
					continue;
				}
			}

			DbRecordSet lstImport;
			lstImport.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_PROJECT_IMPORT));
			//B.T. added 08.04.2013.
			lstImport.addColumn(QVariant::String, "CALC_DEBTOR_CODES");
			lstImport.addColumn(QVariant::String, "CALC_ROLES");

			//merge results with destination list
			int nCnt = lstInput.getRowCount();
			int i;
			for(i=0; i<nCnt; i++)
			{
				lstImport.addRow();
				int nRow = lstImport.getRowCount() - 1;
				int nCol = 0;
				int nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BUSP_CODE",				lstInput.getDataRef(i, nRealCol).toString());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BUSP_NAME",				lstInput.getDataRef(i, nRealCol).toString());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BUSP_START_DATE",          DataHelper::ParseDateString(lstInput.getDataRef(i, nRealCol).toString()));
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BUSP_DEADLINE_DATE",       DataHelper::ParseDateString(lstInput.getDataRef(i, nRealCol).toString()));
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BUSP_COMPLETION_DATE",     DataHelper::ParseDateString(lstInput.getDataRef(i, nRealCol).toString()));
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BUSP_LATEST_COMPLETION_DATE",  DataHelper::ParseDateString(lstInput.getDataRef(i, nRealCol).toString()));
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BUSP_OFFER_DATE",			DataHelper::ParseDateString(lstInput.getDataRef(i, nRealCol).toString()));
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BUSP_OFFER_VALID_DATE",    DataHelper::ParseDateString(lstInput.getDataRef(i, nRealCol).toString()));
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BUSP_ORDER_DATE",          DataHelper::ParseDateString(lstInput.getDataRef(i, nRealCol).toString()));
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BUSP_CONTRACT_DATE",       DataHelper::ParseDateString(lstInput.getDataRef(i, nRealCol).toString()));
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BUSP_LEADER_CODE",			lstInput.getDataRef(i, nRealCol).toString());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BUSP_RESPONSIBLE_CODE",    lstInput.getDataRef(i, nRealCol).toString());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BUSP_DESCRIPTION",         lstInput.getDataRef(i, nRealCol).toString());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BUSP_ACTIVE_FLAG",			lstInput.getDataRef(i, nRealCol).toInt());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BUSP_TEMPLATE_FLAG",       lstInput.getDataRef(i, nRealCol).toInt());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BUSP_LOCATION",			lstInput.getDataRef(i, nRealCol).toString());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BUSP_TIME_BUDGET",         lstInput.getDataRef(i, nRealCol).toDouble());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BUSP_FIXED_FEE",           lstInput.getDataRef(i, nRealCol).toDouble());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BUSP_COST_BUDGET",         lstInput.getDataRef(i, nRealCol).toDouble());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BUSP_ORGANIZATION_CODE",   lstInput.getDataRef(i, nRealCol).toString());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BUSP_DEPARTMENT_CODE",     lstInput.getDataRef(i, nRealCol).toString());

				if (bIsVersion2 || bIsVersion3)
				{
					//B.T. for V2 format:
					nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
					lstImport.setData(nRow, "CALC_DEBTOR_CODES",     lstInput.getDataRef(i, nRealCol).toString());
					nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
					lstImport.setData(nRow, "CALC_ROLES",     lstInput.getDataRef(i, nRealCol).toString());
				}
				
				if (bIsVersion3)
				{
					//B.T. for V3 format:
					nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
					lstImport.setData(nRow, "BUSP_STATUS_CODE",     lstInput.getDataRef(i, nRealCol).toString());
					nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
					lstImport.setData(nRow, "BUSP_AREA_CODE",     lstInput.getDataRef(i, nRealCol).toString());
					nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
					lstImport.setData(nRow, "BUSP_SECTOR_CODE",     lstInput.getDataRef(i, nRealCol).toString());
					nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
					lstImport.setData(nRow, "BUSP_INDUSTRY_CODE",     lstInput.getDataRef(i, nRealCol).toString());
					nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
					lstImport.setData(nRow, "BUSP_SERVICE_TYPE_CODE",     lstInput.getDataRef(i, nRealCol).toString());
					nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
					lstImport.setData(nRow, "BUSP_PROCESS_CODE",     lstInput.getDataRef(i, nRealCol).toString());
					nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
					lstImport.setData(nRow, "BUSP_RATE_CAT_CODE",     lstInput.getDataRef(i, nRealCol).toString());
					nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
					lstImport.setData(nRow, "BUSP_EXP_RATE_CAT_CODE",     lstInput.getDataRef(i, nRealCol).toString());
					nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
					lstImport.setData(nRow, "BUSP_PLAN_COST_CAT_CODE",     lstInput.getDataRef(i, nRealCol).toString());
					nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
					lstImport.setData(nRow, "BUSP_TYPE_CODE",     lstInput.getDataRef(i, nRealCol).toString());
					nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
					lstImport.setData(nRow, "BUSP_DEF_ACTIVITY_CODE",     lstInput.getDataRef(i, nRealCol).toString());

					nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
					lstImport.setData(nRow, "BUSP_INVOICING_TYPE",     lstInput.getDataRef(i, nRealCol).toInt());
					nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
					lstImport.setData(nRow, "BUSP_PRODUCTIVE",     lstInput.getDataRef(i, nRealCol).toInt());
					nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
					lstImport.setData(nRow, "BUSP_NOTCHARGEABLE",     lstInput.getDataRef(i, nRealCol).toInt());
					nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
					lstImport.setData(nRow, "BUSP_INVOICEABLE",     lstInput.getDataRef(i, nRealCol).toInt());
					nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
					lstImport.setData(nRow, "BUSP_ABSENTTIME",     lstInput.getDataRef(i, nRealCol).toInt());
					nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
					lstImport.setData(nRow, "BUSP_HOLIDAYS",     lstInput.getDataRef(i, nRealCol).toInt());
				}
				

				//fix project code on Hierarchical Code mapping
				if(bUseHCodeMap){
					QString strOrig = lstImport.getDataRef(nRow, "BUSP_CODE").toString();
					while(1){
						if(strOrig.isEmpty())
							break;
						if(mapCodes.contains(strOrig)){
							//replace matching prefix with the mapped one
							QString strNewCode = lstImport.getDataRef(nRow, "BUSP_CODE").toString(); 
							strNewCode = strNewCode.right(strNewCode.size()-strOrig.size());
							strNewCode = mapCodes[strOrig] + strNewCode;

							lstImport.setData(nRow, "BUSP_CODE", strNewCode);
							break;
						}
						else
							strOrig = HierarchicalHelper::CalcParentCode(strOrig);//check if ancestor code is mapped
					}
				}
			}

			DbRecordSet lstImportFull;
			lstImportFull.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_PROJECT));
			//B.T.: add columns manually:
			lstImportFull.addColumn(QVariant::String, "CALC_DEBTOR_CODES");
			lstImportFull.addColumn(QVariant::String, "CALC_ROLES");
			lstImportFull.addColumn(QVariant::String, "BUSP_STATUS_CODE");
			lstImportFull.addColumn(QVariant::String, "BUSP_AREA_CODE");
			lstImportFull.addColumn(QVariant::String, "BUSP_SECTOR_CODE");
			lstImportFull.addColumn(QVariant::String, "BUSP_INDUSTRY_CODE");
			lstImportFull.addColumn(QVariant::String, "BUSP_SERVICE_TYPE_CODE");
			lstImportFull.addColumn(QVariant::String, "BUSP_PROCESS_CODE");
			lstImportFull.addColumn(QVariant::String, "BUSP_RATE_CAT_CODE");
			lstImportFull.addColumn(QVariant::String, "BUSP_EXP_RATE_CAT_CODE");
			lstImportFull.addColumn(QVariant::String, "BUSP_PLAN_COST_CAT_CODE");
			lstImportFull.addColumn(QVariant::String, "BUSP_TYPE_CODE");
			lstImportFull.addColumn(QVariant::String, "BUSP_DEF_ACTIVITY_CODE");
			

			lstImportFull.merge(lstImport);
			
			ImportProjects(Ret_pStatus, lstImportFull, 1 /*UPDATE_EXISTING*/);

			//count number of successfully and unsucessfully imported rows
			int nSuccess = 0;
			int nFailure = 0;
			nCnt = lstImportFull.getRowCount();
			for(i=0; i<nCnt; i++)
			{
				int nID = lstImportFull.getDataRef(i, "BUSP_ID").toInt();
				if(nID > 0)
					nSuccess ++;
				else{
					nFailure ++;

					//mark error for this row
					Ret_pErrors.addRow();
					int nErrRows = Ret_pErrors.getRowCount();
					Ret_pErrors.setData(nErrRows-1, "FileName", strCurFile);
					Ret_pErrors.setData(nErrRows-1, "RecIdx", i+1);	//starts from 1
					Ret_pErrors.setData(nErrRows-1, "ErrNo", 1);	//TOFIX
					Ret_pErrors.setData(nErrRows-1, "ErrText", "");	//TOFIX
				}
			}
			//write totals statistics
			Ret_ProcessedFiles.setData(nLogRows-1, "NumRecsSuccess", nSuccess);
			Ret_ProcessedFiles.setData(nLogRows-1, "NumRecsFailed", nFailure);

			//rename file - change extension to indicate file was processed
			g_pImportExportManager->DisableFileWatcher();
			QFileInfo info(strCurFile);
			QString strNewFile = strDirPath + info.baseName() + "." + strFileExtOfProcessed;
			if(strCurFile != strNewFile){
				QFile::remove(strNewFile);
				QFile::rename(strCurFile, strNewFile);
			}
			g_pImportExportManager->EnableFileWatcher();
		}
		else{
			Ret_ProcessedFiles.setData(nLogRows-1, "FileOK", 0);
			Ret_ProcessedFiles.setData(nLogRows-1, "FileErrorTxt", Ret_pStatus.getErrorText());
		}
	}
}

void Service_BusImport::ProcessProjects_Export(Status &Ret_pStatus, QString strFileExt, QString strFilePrefix, QString strFileExtOfProcessed, QString strDirPath, int nExportLasN, bool bExportAddDateTimeOnExportFile, DbRecordSet &Ret_ProcessedFiles, DbRecordSet &Ret_pErrors, DbRecordSet &ProjectsForExport, bool bClientSideExport, QByteArray &Ret_File, bool bUtf8)
{
	strDirPath=QDir::toNativeSeparators(strDirPath);
	if(strDirPath.isEmpty() || strDirPath.at(strDirPath.size()-1) != QDir::separator())
		strDirPath += QDir::separator();

	//load column mapping / code mapping files
	QMap<int, int> mapCols;
	QMap<QString, QString> mapCodes;
	QString strNamesHeader;
	bool bUseColMap     = LoadColMap(strDirPath + "SOK_Map_Cols_Proj_Exp.map", "~~~SOKRATES Data Exchange - Version: 1.00 - Format: Projects Column Mapping - Subformat: Project Export", mapCols, strNamesHeader,false);
	bool bUseHCodeMap = LoadHCodeMap(strDirPath + "SOK_Hier_Map_Proj_Exp.map", "~~~SOKRATES Data Exchange - Version: 1.00 - Format: Projects Code Mapping - Subformat: Projects Export", mapCodes);

	QString strOutFile;
	if(!bClientSideExport)
	{
		//generate output file path
		strOutFile = strDirPath;
		strOutFile += strFilePrefix;
		if(bExportAddDateTimeOnExportFile)
			strOutFile += QDateTime::currentDateTime().toString("_yyyyMMdd_hh_mm");
		strOutFile += ".";
		strOutFile += strFileExt;
	}

	//fetch projects to export
	QString strQuery = "SELECT * FROM BUS_PROJECTS";
	QString strWhere;
	bool bBind = false;

	DbRecordSet lstExport;
	lstExport.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_PROJECT_IMPORT));

	int nCnt = ProjectsForExport.getRowCount();
	int nStartRow = 0; //when exporting in chunks

	while(1)	// fetch data in batches
	{
		if(nCnt > 0)
		{
			const int nMaxBatch = 500;
			int nMax = nStartRow + min(nMaxBatch, (nCnt-nStartRow));

			//export only selected projects
			strWhere = " WHERE BUSP_ID IN (";
			for(int i=nStartRow; i<nMax; i++){
				strWhere += ProjectsForExport.getDataRef(i,0).toString();
				if(i < (nMax-1))
					strWhere += ",";
			}
			strWhere += ")";

			nStartRow = nMax;
		}
		else{
			if(nExportLasN > 0)
			{
				//export only projects updated whitin the last X days
				strWhere = " WHERE BUSP_DAT_LAST_MODIFIED > ?";
				bBind = true;
			}
		}

		DbSqlQuery query(Ret_pStatus,GetDbManager());
		if(!Ret_pStatus.IsOK()) return;

		QString strQuery1 = strQuery + strWhere;
		//qDebug() << strQuery1;

		query.Prepare(Ret_pStatus, strQuery1);
		if(!Ret_pStatus.IsOK())
			return;

		if(bBind){
			QDateTime dtLimit = QDateTime::currentDateTime().addDays(-nExportLasN);
			query.bindValue(0, dtLimit);
		}

		query.ExecutePrepared(Ret_pStatus);
		if(!Ret_pStatus.IsOK())
			return;

		DbRecordSet lstData;
		query.FetchData(lstData);
		lstExport.merge(lstData);

		if(nCnt <= 0 || nStartRow >= nCnt)
			break;
	}

	QFile file(strOutFile);
	if(!bClientSideExport)
	{
		//TOFIX open and lock file
		if (!file.open(QIODevice::WriteOnly)){
			Ret_pStatus.setError(1,QObject::tr("Can not open file"));
			return;
		}
	}
	
	QString strFormat;
	if(bUtf8)
		strFormat = "~~~SOKRATES Data Exchange - Version: 1.00 - Charset: UTF8 - Format: Projects - Subformat: Project Main Short\r\n";
	else
		strFormat = "~~~SOKRATES Data Exchange - Version: 1.00 - Format: Projects - Subformat: Project Main Short\r\n";
	if(!bClientSideExport)
		file.write((bUtf8) ? strFormat.toUtf8() : strFormat.toLocal8Bit());
	else
		Ret_File += (bUtf8) ? strFormat.toUtf8() : strFormat.toLocal8Bit();

	//write records to file (use both mappings if needed)
	int nRows = lstExport.getRowCount();
	for(int i=0; i<nRows; i++)
	{
		//fix project code on Hierarchical Code mapping
		if(bUseHCodeMap)
		{
			QString strOrig = lstExport.getDataRef(i, "BUSP_CODE").toString();
			while(1){
				if(strOrig.isEmpty())
					break;
				if(mapCodes.contains(strOrig)){
					//replace matching prefix with the mapped one
					QString strNewCode = lstExport.getDataRef(i, "BUSP_CODE").toString(); 
					strNewCode = strNewCode.right(strNewCode.size()-strOrig.size());
					strNewCode = mapCodes[strOrig] + strNewCode;

					lstExport.setData(i, "BUSP_CODE", strNewCode);
					break;
				}
				else
					strOrig = HierarchicalHelper::CalcParentCode(strOrig); //check if ancestor code is mapped
			}
		}

		//write fields
		QString strField;
		int nMaxCols = 21;
		for(int nCol=0; nCol<nMaxCols; nCol++)
		{
			int nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
			if(lstExport.getDataRef(i, nRealCol).isNull())
				strField = "";
			else
				strField = lstExport.getDataRef(i, nRealCol).toString();

			//
			// make the fields CSV valid
			//
			
			//replace escap quotes " to ""
			strField.replace("\"", "\"\"");

			//now convert all possible line endings into the single one
			strField.replace("\r\n",   "\n");
			strField.replace("\r",	  "\n");

			//escape new line
			strField.replace("\n", "//");

			//quote field if needed
			if(strField.indexOf(" ") >= 0 ||
				strField.indexOf("\t") >= 0)
			{
				strField.prepend("\"");
				strField.append("\"");
			}

			if(!bClientSideExport)
				file.write((bUtf8) ? strField.toUtf8() : strField.toLocal8Bit());
			else
				Ret_File += (bUtf8) ? strField.toUtf8() : strField.toLocal8Bit();

			if(nCol<(nMaxCols-1)){
				if(!bClientSideExport)
					file.write("\t");
				else
					Ret_File += "\t";
			}
		}

		if(!bClientSideExport)
			file.write("\r\n");
		else
			Ret_File += "\r\n";
	}

	if(!bClientSideExport)
		file.close();

	//log file as processed
	Ret_ProcessedFiles.destroy();
	Ret_ProcessedFiles.addColumn(QVariant::String, "FileName");
	Ret_ProcessedFiles.addColumn(QVariant::Int,	   "FileOK");
	Ret_ProcessedFiles.addColumn(QVariant::String, "FileErrorTxt");
	Ret_ProcessedFiles.addColumn(QVariant::Int,	   "NumRecsSuccess");
	Ret_ProcessedFiles.addColumn(QVariant::Int,	   "NumRecsFailed");

	Ret_ProcessedFiles.addRow();
	Ret_ProcessedFiles.setData(0, "FileName", strOutFile);
	Ret_ProcessedFiles.setData(0, "FileOK", 0);
	Ret_ProcessedFiles.setData(0, "NumRecsSuccess", nRows);
}

void Service_BusImport::ProcessUsers_Import(Status &Ret_pStatus, QString strFileExt, QString strFilePrefix, QString strFileExtOfProcessed, QString strDirPath, DbRecordSet &Ret_ProcessedFiles, DbRecordSet &Ret_pErrors, int nRoleID)
{
	strDirPath=QDir::toNativeSeparators(strDirPath);
	if(strDirPath.isEmpty() || strDirPath.at(strDirPath.size()-1) != QDir::separator())
		strDirPath += QDir::separator();

	//load column mapping / code mapping files
	QMap<int, int> mapCols;
	QString strNamesHeader;
	bool bUseColMap = LoadColMap(strDirPath + "SOK_Map_Cols_User_Imp.map", "~~~SOKRATES Data Exchange - Version: 1.00 - Format: Users Column Mapping - Subformat: User Import", mapCols, strNamesHeader,true);

	//redefine list of processed files
	Ret_ProcessedFiles.destroy();
	Ret_ProcessedFiles.addColumn(QVariant::String, "FileName");
	Ret_ProcessedFiles.addColumn(QVariant::Int,	   "FileOK");
	Ret_ProcessedFiles.addColumn(QVariant::String, "FileErrorTxt");
	Ret_ProcessedFiles.addColumn(QVariant::Int,	   "NumRecsSuccess");
	Ret_ProcessedFiles.addColumn(QVariant::Int,	   "NumRecsFailed");

	//redefine list of errors
	Ret_pErrors.destroy();
	Ret_pErrors.addColumn(QVariant::String, "FileName");
	Ret_pErrors.addColumn(QVariant::Int,	"RecIdx");	//starts from 1
	Ret_pErrors.addColumn(QVariant::Int,	"ErrNo");
	Ret_pErrors.addColumn(QVariant::String, "ErrText");

	//list files matching the pattern
	QString strTemplate;
	if(!strFilePrefix.isEmpty())
		strTemplate += strFilePrefix + "*";
	if(!strFileExt.isEmpty()){
		if(strTemplate.isEmpty())
			strTemplate += "*";
		strTemplate += ".";
		strTemplate += strFileExt;
	}
	QStringList filters;
    filters << strTemplate;
	QDir dir(strDirPath);
	QStringList lstFiles = dir.entryList(filters);

	int nCount = lstFiles.count();
	for(int j=0; j<nCount; j++)
	{
		Ret_pStatus.setError(0);

		QString strCurFile = lstFiles[j];
		
		//skip "." and ".."
		if(strCurFile == "." || strCurFile == "..")
			continue;

		strCurFile = strDirPath + strCurFile;

		//log file as processed
		Ret_ProcessedFiles.addRow();
		int nLogRows = Ret_ProcessedFiles.getRowCount();
		Ret_ProcessedFiles.setData(nLogRows-1, "FileName", strCurFile);

		DbRecordSet lstInput;
		CsvImportFile file;
		//QString strFormat1 = "~~~SOKRATES Data Exchange - Version: 1.00 - Format: Users - Subformat: User Details";
		//QString strFormat2 = "~~~SOKRATES Data Exchange - Version: 1.00 - Charset: UTF8 - Format: Users - Subformat: User Details";

		QStringList lstFormatPlain;
		lstFormatPlain << "~~~SOKRATES Data Exchange - Version: 1.00 - Format: Users - Subformat: User Details";
		QStringList lstFormatUtf8;
		lstFormatUtf8 << "~~~SOKRATES Data Exchange - Version: 1.00 - Charset: UTF8 - Format: Users - Subformat: User Details";

		file.Load(Ret_pStatus, strCurFile, lstInput, lstFormatPlain, lstFormatUtf8);
		if(Ret_pStatus.IsOK())
		{
			int nFieldsCnt = lstInput.getColumnCount();
			if(!bUseColMap && 18 != nFieldsCnt){
				//MB: field count is checked only when no col mapping used
				Ret_ProcessedFiles.setData(nLogRows-1, "FileOK", 1);
				Ret_ProcessedFiles.setData(nLogRows-1, "FileErrorTxt", QObject::tr("Document does not contain %1 fields for each record!").arg(56));
				continue;
			}
			else
				Ret_ProcessedFiles.setData(nLogRows-1, "FileOK", 0);

			DbRecordSet lstImport;
			lstImport.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_PERSON_IMPORT));

			//merge results with destination list
			int nCnt = lstInput.getRowCount();
			int i;
			for(i=0; i<nCnt; i++)
			{
				lstImport.addRow();
				int nRow = lstImport.getRowCount() - 1;
				int nCol = 0;
				int nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BPER_CODE",				lstInput.getDataRef(i, nRealCol).toString());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "GENERATE_CONTACT",			lstInput.getDataRef(i, nRealCol).toInt());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BPER_CODE_EXT",	        lstInput.getDataRef(i, nRealCol).toString());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BPER_FIRST_NAME",			lstInput.getDataRef(i, nRealCol).toString());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BPER_LAST_NAME",			lstInput.getDataRef(i, nRealCol).toString());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BPER_INITIALS",			lstInput.getDataRef(i, nRealCol).toString());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BORG_CODE",				lstInput.getDataRef(i, nRealCol).toString());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BDEPT_CODE",				lstInput.getDataRef(i, nRealCol).toString());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BPER_DATE_ENTERED",        DataHelper::ParseDateString(lstInput.getDataRef(i, nRealCol).toString()));
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BPER_DATE_LEFT",			DataHelper::ParseDateString(lstInput.getDataRef(i, nRealCol).toString()));
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BPER_ACTIVE_FLAG",			lstInput.getDataRef(i, nRealCol).toInt());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BPER_SEX",					lstInput.getDataRef(i, nRealCol).toInt());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BPER_DIRECT_PHONE",         lstInput.getDataRef(i, nRealCol).toString());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BPER_INSURANCE_ID",		lstInput.getDataRef(i, nRealCol).toString());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BPER_BIRTH_DATE",			DataHelper::ParseDateString(lstInput.getDataRef(i, nRealCol).toString()));
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BPER_OCCUPATION",			lstInput.getDataRef(i, nRealCol).toDouble());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "CLC_FUNCTION_CODE",		lstInput.getDataRef(i, nRealCol).toString());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BPER_DESCRIPTION",           lstInput.getDataRef(i, nRealCol).toString());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
			}

			DbRecordSet lstImportFull;
			//lstImportFull.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CONTACT_FULL));
			lstImportFull.copyDefinition(lstImport);
			lstImportFull.merge(lstImport);

			
			//lstImportFull.Dump();
			//lstImport.Dump();
			
			ImportPersons(Ret_pStatus, lstImportFull, 1 /*UPDATE_EXISTING*/, false, false,nRoleID);

			//count number of successfully and unsucessfully imported rows
			int nSuccess = 0;
			int nFailure = 0;
			nCnt = lstImportFull.getRowCount();
			for(i=0; i<nCnt; i++)
			{
				int nID = lstImportFull.getDataRef(i, "BPER_ID").toInt();
				if(nID > 0)
					nSuccess ++;
				else{
					nFailure ++;

					//mark error for this row
					Ret_pErrors.addRow();
					int nErrRows = Ret_pErrors.getRowCount();
					Ret_pErrors.setData(nErrRows-1, "FileName", strCurFile);
					Ret_pErrors.setData(nErrRows-1, "RecIdx", i+1);	//starts from 1
					Ret_pErrors.setData(nErrRows-1, "ErrNo", 1);	//TOFIX
					Ret_pErrors.setData(nErrRows-1, "ErrText", "");	//TOFIX
				}
			}
			//write totals statistics
			Ret_ProcessedFiles.setData(nLogRows-1, "NumRecsSuccess", nSuccess);
			Ret_ProcessedFiles.setData(nLogRows-1, "NumRecsFailed", nFailure);

			//rename file - change extension to indicate file was processed
			g_pImportExportManager->DisableFileWatcher();
			QFileInfo info(strCurFile);
			QString strNewFile = strDirPath + info.baseName() + "." + strFileExtOfProcessed;
			if(strCurFile != strNewFile){
				QFile::remove(strNewFile);
				QFile::rename(strCurFile, strNewFile);
			}
			g_pImportExportManager->EnableFileWatcher();
		}
		else{
			Ret_ProcessedFiles.setData(nLogRows-1, "FileOK", 0);
			Ret_ProcessedFiles.setData(nLogRows-1, "FileErrorTxt", Ret_pStatus.getErrorText());
		}
	}
}

void Service_BusImport::ProcessUsers_Export(Status &Ret_pStatus, QString strFileExt, QString strFilePrefix, QString strFileExtOfProcessed, QString strDirPath, int nExportLasN, bool bExportAddDateTimeOnExportFile, DbRecordSet &Ret_ProcessedFiles, DbRecordSet &Ret_pErrors, DbRecordSet &UsersForExport, bool bClientSideExport, QByteArray &Ret_File, bool bUtf8)
{
	//generate output file path
	strDirPath=QDir::toNativeSeparators(strDirPath);

	QString strOutFile;
	if(!bClientSideExport)
	{
		strOutFile = strDirPath;
		if(strOutFile.isEmpty() || strOutFile.at(strOutFile.size()-1) != QDir::separator())
			strOutFile += QDir::separator();
		strOutFile += strFilePrefix;
		if(bExportAddDateTimeOnExportFile)
			strOutFile += QDateTime::currentDateTime().toString("_yyyyMMdd_hh_mm");
		strOutFile += ".";
		strOutFile += strFileExt;
	}

	//load column mapping / code mapping files
	QMap<int, int> mapCols;
	QString strNamesHeader;
	bool bUseColMap     = LoadColMap(strDirPath + "SOK_Map_Cols_User_Exp.map", "~~~SOKRATES Data Exchange - Version: 1.00 - Format: Users Column Mapping - Subformat: User Export", mapCols, strNamesHeader,false);

	//fetch users to export
	QString strQuery = "SELECT * FROM BUS_PERSON";
	strQuery += " LEFT OUTER JOIN BUS_ORGANIZATIONS ON BPER_ORGANIZATION_ID=BORG_ID";
	strQuery += " LEFT OUTER JOIN BUS_DEPARTMENTS ON BPER_DEPARTMENT_ID=BDEPT_ID";
	QString strWhere;
	bool bBind = false;

	DbRecordSet lstExport;
	lstExport.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_PERSON_IMPORT));

	int nCnt = UsersForExport.getRowCount();
	int nStartRow = 0; //when exporting in chunks

	while(1)	// fetch data in batches
	{
		if(nCnt > 0)
		{
			const int nMaxBatch = 500;
			int nMax = nStartRow + min(nMaxBatch, (nCnt-nStartRow));

			//export only selected projects
			strWhere = " WHERE BPER_ID IN (";
			for(int i=nStartRow; i<nMax; i++){
				strWhere += UsersForExport.getDataRef(i,0).toString();
				if(i < (nMax-1))
					strWhere += ",";
			}
			strWhere += ")";

			nStartRow = nMax;
		}
		else{
			if(nExportLasN > 0)
			{
				//export only projects updated whitin the last X days
				strWhere = " WHERE BPER_DAT_LAST_MODIFIED > ?";
				bBind = true;
			}
		}

		DbSqlQuery query(Ret_pStatus,GetDbManager());
		if(!Ret_pStatus.IsOK()) return;

		QString strQuery1 = strQuery + strWhere;

		query.Prepare(Ret_pStatus, strQuery1);
		if(!Ret_pStatus.IsOK()){
			return;
		}

		if(bBind){
			QDateTime dtLimit = QDateTime::currentDateTime().addDays(-nExportLasN);
			query.bindValue(0, dtLimit);
		}

		query.ExecutePrepared(Ret_pStatus);
		if(!Ret_pStatus.IsOK())
			return;

		DbRecordSet lstData;
		query.FetchData(lstData);
		lstExport.merge(lstData);

		if(nCnt <= 0 || nStartRow >= nCnt)
			break;
	}

	//TOFIX open and lock file
	QFile file(strOutFile);
	if(!bClientSideExport){
		if (!file.open(QIODevice::WriteOnly)){
			Ret_pStatus.setError(1,QObject::tr("Can not open file"));
			return;
		}
	}

	QString strFormat;
	if(bUtf8)
		strFormat = "~~~SOKRATES Data Exchange - Version: 1.00 - Charset: UTF8 - Format: Users - Subformat: User Details\r\n";
	else
		strFormat = "~~~SOKRATES Data Exchange - Version: 1.00 - Format: Users - Subformat: User Details\r\n";
	if(!bClientSideExport)
		file.write((bUtf8) ? strFormat.toUtf8() : strFormat.toLocal8Bit());
	else
		Ret_File += (bUtf8) ? strFormat.toUtf8() : strFormat.toLocal8Bit();

	//write records to file (use both mappings if needed)
	int nRows = lstExport.getRowCount();
	for(int i=0; i<nRows; i++)
	{
		//write fields
		QString strField;
		int nMaxCols = 18;
		for(int nCol=0; nCol<nMaxCols; nCol++)
		{
			int nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
			if(lstExport.getDataRef(i, nRealCol).isNull())
				strField = "";
			else
				strField = lstExport.getDataRef(i, nRealCol).toString();

			//
			// make the fields CSV valid
			//
			
			//replace escap quotes " to ""
			strField.replace("\"", "\"\"");

			//now convert all possible line endings into the single one
			strField.replace("\r\n",   "\n");
			strField.replace("\r",	  "\n");

			//escape new line
			strField.replace("\n", "//");

			//quote field if needed
			if(strField.indexOf(" ") >= 0 ||
				strField.indexOf("\t") >= 0)
			{
				strField.prepend("\"");
				strField.append("\"");
			}

			if(!bClientSideExport)
				file.write((bUtf8) ? strField.toUtf8() : strField.toLocal8Bit());
			else
				Ret_File += (bUtf8) ? strField.toUtf8() : strField.toLocal8Bit();

			if(nCol<(nMaxCols-1))
			{
				if(!bClientSideExport)
					file.write("\t");
				else
					Ret_File += "\t";
			}
		}

		if(!bClientSideExport)
			file.write("\r\n");
		else
			Ret_File += "\r\n";
	}

	if(!bClientSideExport)
		file.close();

	//log file as processed
	Ret_ProcessedFiles.destroy();
	Ret_ProcessedFiles.addColumn(QVariant::String, "FileName");
	Ret_ProcessedFiles.addColumn(QVariant::Int,	   "FileOK");
	Ret_ProcessedFiles.addColumn(QVariant::String, "FileErrorTxt");
	Ret_ProcessedFiles.addColumn(QVariant::Int,	   "NumRecsSuccess");
	Ret_ProcessedFiles.addColumn(QVariant::Int,	   "NumRecsFailed");

	Ret_ProcessedFiles.addRow();
	Ret_ProcessedFiles.setData(0, "FileName", strOutFile);
	Ret_ProcessedFiles.setData(0, "FileOK", 0);
	Ret_ProcessedFiles.setData(0, "NumRecsSuccess", nRows);
}

void Service_BusImport::ProcessDebtors_Export(Status &Ret_pStatus, QString strFileExt, QString strFilePrefix, QString strFileExtOfProcessed, QString strDirPath, int nExportLasN, bool bExportAddDateTimeOnExportFile, DbRecordSet &Ret_ProcessedFiles, DbRecordSet &Ret_pErrors, int nAddressTypeFilter, bool bUtf8)
{
	strDirPath=QDir::toNativeSeparators(strDirPath);
	if(strDirPath.isEmpty() || strDirPath.at(strDirPath.size()-1) != QDir::separator())
		strDirPath += QDir::separator();

	//load column mapping / code mapping files
	QMap<int, int> mapCols;
	QString strNamesHeader;
	bool bUseColMap     = LoadColMap(strDirPath + "SOK_Map_Cols_Cont_Exp.map", "~~~SOKRATES Data Exchange - Version: 1.00 - Format: Debtors Column Mapping - Subformat: Debtor Export", mapCols, strNamesHeader,false);

	//generate output file path
	QString strOutFile = strDirPath;
	strOutFile += strFilePrefix;
	if(bExportAddDateTimeOnExportFile)
		strOutFile += QDateTime::currentDateTime().toString("_yyyyMMdd_hh_mm");
	strOutFile += ".";
	strOutFile += strFileExt;

	DbSqlQuery query(Ret_pStatus,GetDbManager());
	if(!Ret_pStatus.IsOK()) return;

	//fetch contacts/debtors to export
	QString strQuery  = "SELECT * FROM BUS_CM_CONTACT";
			strQuery += "  INNER JOIN BUS_CM_DEBTOR ON BCMD_CONTACT_ID=BCNT_ID"; //BT: when using joins, use in all cases
			strQuery += "  LEFT OUTER JOIN BUS_CM_ADDRESS ON BCMA_CONTACT_ID=BCNT_ID";
			if(nAddressTypeFilter == 0)	strQuery += " AND BCMA_IS_DEFAULT=1";
			strQuery += "  LEFT OUTER JOIN BUS_CM_EMAIL ON BCME_CONTACT_ID=BCNT_ID AND BCME_IS_DEFAULT=1";
			strQuery += "  LEFT OUTER JOIN BUS_CM_INTERNET ON BCMI_CONTACT_ID=BCNT_ID AND BCMI_IS_DEFAULT=1";
//			strQuery += "  LEFT OUTER JOIN BUS_CM_JOURNAL ON BCMJ_CONTACT_ID=BCNT_ID";
			strQuery += "  LEFT OUTER JOIN BUS_PERSON ON BPER_CONTACT_ID=BCNT_ID";
	QString strWhere =" WHERE "; 
	if(query.GetDbConnection()->driverName().indexOf("MYSQL")>=0)
		strWhere += "  (LENGTH(BCMD_DEBTORCODE)>0 OR LENGTH(BCMD_DEBTORACCOUNT)>0) ";
	else
		strWhere += "  (CHAR_LENGTH(BCMD_DEBTORCODE)>0 OR CHAR_LENGTH(BCMD_DEBTORACCOUNT)>0) ";

	bool bBind = false;

	if(nExportLasN > 0)
	{
		//export only contacts updated whitin the last X days
		strWhere += " AND BCNT_DAT_LAST_MODIFIED > ?";
		bBind = true;
	}

	if(nAddressTypeFilter > 0)
	{
		strQuery += QString(" INNER JOIN BUS_CM_ADDRESS_TYPES ON BCMAT_ADDRESS_ID=BCMA_ID AND BCMAT_TYPE_ID=%1").arg(nAddressTypeFilter);
	}

	strQuery += strWhere;

	query.Prepare(Ret_pStatus, strQuery);
	if(!Ret_pStatus.IsOK())
		return;

	if(bBind){
		QDateTime dtLimit = QDateTime::currentDateTime().addDays(-nExportLasN);
		//qDebug()<<dtLimit.toString();
		query.bindValue(0, dtLimit);
	}

	query.ExecutePrepared(Ret_pStatus);
	if(!Ret_pStatus.IsOK())
		return;

	DbRecordSet lstData;
	query.FetchData(lstData);

	DbRecordSet lstExport;
	lstExport.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CONTACT_FULL_TEXT_IMPORT));
	lstExport.renameColumn("CLC_EMAIL_ADDRESS", "BCME_ADDRESS"); // so we can merge
	lstExport.renameColumn("CLC_EMAIL_ADDRESS_DESC", "BCME_DESCRIPTION"); // so we can merge
	lstExport.renameColumn("CLC_WEB_ADDRESS", "BCMI_ADDRESS"); // so we can merge
	lstExport.renameColumn("CLC_WEB_ADDRESS_DESC", "BCMI_DESCRIPTION"); // so we can merge
	lstExport.merge(lstData);

	//update additional data
	ServerLocalCache cache;
	cache.LoadTypes();
	int nPhoneBussID = cache.GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_BUSINESS).toInt();
	int nPhoneMobiID = cache.GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_MOBILE).toInt();
	int nPhonePrivID = cache.GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_PRIVATE).toInt();
	int nPhonePrivMobID = cache.GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_PRIVATE_MOBILE).toInt();
	int nPhoneFaxID   = cache.GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_FAX).toInt();
	int nPhoneSkypeID = cache.GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_SKYPE).toInt();

	int nRows = lstExport.getRowCount();
	for(int i=0; i<nRows; i++)
	{
		int nCntID = lstData.getDataRef(i, "BCNT_ID").toInt();

		//calc groups
		QString strQuery = QString("SELECT BGIT_EXTERNAL_CATEGORY FROM bus_cm_group INNER JOIN bus_group_items ON BGIT_ID=BGCN_ITEM_ID WHERE BGCN_CONTACT_ID=%1").arg(nCntID);
		query.Execute(Ret_pStatus, strQuery);
		if(!Ret_pStatus.IsOK())
			return;	//TOFIX

		DbRecordSet lstGroups;
		query.FetchData(lstGroups);

		QString strGroups;
		int nCntRes = lstGroups.getRowCount();
		for(int j=0; j<nCntRes; j++){
			strGroups += lstGroups.getDataRef(j, 0).toString();
			if(j < nCntRes-1)
				strGroups += ",";
		}

		//set calculated data
		lstExport.setData(i, "CLC_GROUPS", strGroups);

		// convert from HTML to plain text
		QString strData;
		lstExport.getData(i, "BCMD_INVOICE_TEXT", strData);
		StripHTML(strData);
		lstExport.setData(i, "BCMD_INVOICE_TEXT", strData);

		lstExport.getData(i, "BCNT_DESCRIPTION", strData);
		StripHTML(strData);
		lstExport.setData(i, "BCNT_DESCRIPTION", strData);

		lstExport.getData(i, "BCMD_DESCRIPTION", strData);
		StripHTML(strData);
		lstExport.setData(i, "BCMD_DESCRIPTION", strData);

		//
		// fetch telephones by types
		//
		QString strQuery2 = QString("SELECT BCMP_FULLNUMBER FROM BUS_CM_PHONE WHERE BCMP_CONTACT_ID=%1").arg(nCntID);
	
		if(nPhoneBussID > 0){
			query.Execute(Ret_pStatus, strQuery2 + QString(" AND BCMP_TYPE_ID=%1").arg(nPhoneBussID));
			if(!Ret_pStatus.IsOK())
				return;
			DbRecordSet lstPhone;
			query.FetchData(lstPhone);
			if(lstPhone.getRowCount() > 0)
				lstExport.setData(i, "CLC_PHONE_BUSINESS_CENTRAL", lstPhone.getDataRef(0, 0).toString());
		}
		if(nPhonePrivID > 0){
			query.Execute(Ret_pStatus, strQuery2 + QString(" AND BCMP_TYPE_ID=%1").arg(nPhonePrivID));
			if(!Ret_pStatus.IsOK())
				return;
			DbRecordSet lstPhone;
			query.FetchData(lstPhone);
			if(lstPhone.getRowCount() > 0)
				lstExport.setData(i, "CLC_PHONE_PRIVATE", lstPhone.getDataRef(0, 0).toString());
		}
		if(nPhonePrivMobID > 0){
			query.Execute(Ret_pStatus, strQuery2 + QString(" AND BCMP_TYPE_ID=%1").arg(nPhonePrivMobID));
			if(!Ret_pStatus.IsOK())
				return;
			DbRecordSet lstPhone;
			query.FetchData(lstPhone);
			if(lstPhone.getRowCount() > 0)
				lstExport.setData(i, "CLC_PHONE_PRIVATE_MOBILE", lstPhone.getDataRef(0, 0).toString());
		}
		if(nPhoneMobiID > 0){
			query.Execute(Ret_pStatus, strQuery2 + QString(" AND BCMP_TYPE_ID=%1").arg(nPhoneMobiID));
			if(!Ret_pStatus.IsOK())
				return;
			DbRecordSet lstPhone;
			query.FetchData(lstPhone);
			if(lstPhone.getRowCount() > 0)
				lstExport.setData(i, "CLC_PHONE_MOBILE", lstPhone.getDataRef(0, 0).toString());
		}
		if(nPhoneFaxID > 0){
			query.Execute(Ret_pStatus, strQuery2 + QString(" AND BCMP_TYPE_ID=%1").arg(nPhoneFaxID));
			if(!Ret_pStatus.IsOK())
				return;
			DbRecordSet lstPhone;
			query.FetchData(lstPhone);
			if(lstPhone.getRowCount() > 0)
				lstExport.setData(i, "CLC_PHONE_FAX", lstPhone.getDataRef(0, 0).toString());
		}
		if(nPhoneSkypeID > 0){
			query.Execute(Ret_pStatus, strQuery2 + QString(" AND BCMP_TYPE_ID=%1").arg(nPhoneSkypeID));
			if(!Ret_pStatus.IsOK())
				return;
			DbRecordSet lstPhone;
			query.FetchData(lstPhone);
			if(lstPhone.getRowCount() > 0)
				lstExport.setData(i, "CLC_PHONE_SKYPE", lstPhone.getDataRef(0, 0).toString());
		}
	}

	//TOFIX open and lock file
	QFile file(strOutFile);
	if (!file.open(QIODevice::WriteOnly)){
		Ret_pStatus.setError(1,QObject::tr("Can not open file"));
		return;
	}

	QString strFormat;
	if(bUtf8)
		strFormat = "~~~SOKRATES Data Exchange - Version: 2.00 - Charset: UTF8 - Format: Contacts - Subformat: Contacts Details\r\n";
	else
		strFormat = "~~~SOKRATES Data Exchange - Version: 2.00 - Format: Contacts - Subformat: Contacts Details\r\n";
	file.write((bUtf8) ? strFormat.toUtf8() : strFormat.toLocal8Bit());

	//write records to file (use both mappings if needed)
	for(int i=0; i<nRows; i++)
	{
		//write fields
		QString strField;
		int nMaxCols = 66;
		for(int nCol=0; nCol<nMaxCols; nCol++)
		{
			int nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
			if(lstExport.getDataRef(i, nRealCol).isNull())
				strField = "";
			else
				strField = lstExport.getDataRef(i, nRealCol).toString();

			//
			// make the fields CSV valid
			//
			
			//replace escape quotes " to ""
			strField.replace("\"", "\"\"");

			//now convert all possible line endings into the single one
			strField.replace("\r\n",   "\n");
			strField.replace("\r",	  "\n");

			//escape new line
			strField.replace("\n", "//");

			//quote field if needed
			if(strField.indexOf(" ") >= 0 ||
				strField.indexOf("\t") >= 0)
			{
				strField.prepend("\"");
				strField.append("\"");
			}

			file.write((bUtf8) ? strField.toUtf8() : strField.toLocal8Bit());

			if(nCol<(nMaxCols-1))
				file.write("\t");
		}

		file.write("\r\n");
	}

	file.close(),

	//log file as processed
	Ret_ProcessedFiles.destroy();
	Ret_ProcessedFiles.addColumn(QVariant::String, "FileName");
	Ret_ProcessedFiles.addColumn(QVariant::Int,	   "FileOK");
	Ret_ProcessedFiles.addColumn(QVariant::String, "FileErrorTxt");
	Ret_ProcessedFiles.addColumn(QVariant::Int,	   "NumRecsSuccess");
	Ret_ProcessedFiles.addColumn(QVariant::Int,	   "NumRecsFailed");

	Ret_ProcessedFiles.addRow();
	Ret_ProcessedFiles.setData(0, "FileName", strOutFile);
	Ret_ProcessedFiles.setData(0, "FileOK", 0);
	Ret_ProcessedFiles.setData(0, "NumRecsSuccess", nRows);
}

void Service_BusImport::ProcessContacts_Import(Status &Ret_pStatus, QString strFileExt, QString strFilePrefix, QString strFileExtOfProcessed, QString strDirPath, DbRecordSet &Ret_ProcessedFiles, DbRecordSet &Ret_pErrors)
{
	strDirPath=QDir::toNativeSeparators(strDirPath);
	if(strDirPath.isEmpty() || strDirPath.at(strDirPath.size()-1) != QDir::separator())
		strDirPath += QDir::separator();

	//load column mapping / code mapping files
	QMap<int, int> mapCols;
	QString strNamesHeader;
	bool bUseColMap     = LoadColMap(strDirPath + "SOK_Map_Cols_Cont_Imp.map", "~~~SOKRATES Data Exchange - Version: 1.00 - Format: Contacts Column Mapping - Subformat: Contact Import", mapCols, strNamesHeader,true);

	//redefine list of processed files
	Ret_ProcessedFiles.destroy();
	Ret_ProcessedFiles.addColumn(QVariant::String, "FileName");
	Ret_ProcessedFiles.addColumn(QVariant::Int,	   "FileOK");
	Ret_ProcessedFiles.addColumn(QVariant::String, "FileErrorTxt");
	Ret_ProcessedFiles.addColumn(QVariant::Int,	   "NumRecsSuccess");
	Ret_ProcessedFiles.addColumn(QVariant::Int,	   "NumRecsFailed");

	//redefine list of errors
	Ret_pErrors.destroy();
	Ret_pErrors.addColumn(QVariant::String, "FileName");
	Ret_pErrors.addColumn(QVariant::Int,	"RecIdx");	//starts from 1
	Ret_pErrors.addColumn(QVariant::Int,	"ErrNo");
	Ret_pErrors.addColumn(QVariant::String, "ErrText");

	//list files matching the pattern
	QString strTemplate;
	if(!strFilePrefix.isEmpty())
		strTemplate += strFilePrefix + "*";
	if(!strFileExt.isEmpty()){
		if(strTemplate.isEmpty())
			strTemplate += "*";
		strTemplate += ".";
		strTemplate += strFileExt;
	}
	QStringList filters;
    filters << strTemplate;
	QDir dir(strDirPath);
	QStringList lstFiles = dir.entryList(filters);

	int nCount = lstFiles.count();
	for(int j=0; j<nCount; j++)
	{
		QString strCurFile = lstFiles[j];
		Ret_pStatus.setError(0);
		
		//skip "." and ".."
		if(strCurFile == "." || strCurFile == "..")
			continue;

		strCurFile = strDirPath + strCurFile;

		//log file as processed
		Ret_ProcessedFiles.addRow();
		int nLogRows = Ret_ProcessedFiles.getRowCount();
		Ret_ProcessedFiles.setData(nLogRows-1, "FileName", strCurFile);

		DbRecordSet lstInput;
		CsvImportFile file;
		//QString strFormat1 = "~~~SOKRATES Data Exchange - Version: 1.00 - Format: Contacts - Subformat: Contacts Details";
		//QString strFormat2 = "~~~SOKRATES Data Exchange - Version: 1.00 - Charset: UTF8 - Format: Contacts - Subformat: Contacts Details";

		QStringList lstFormatPlain;
		lstFormatPlain << "~~~SOKRATES Data Exchange - Version: 1.00 - Format: Contacts - Subformat: Contacts Details";
		QStringList lstFormatUtf8;
		lstFormatUtf8 << "~~~SOKRATES Data Exchange - Version: 1.00 - Charset: UTF8 - Format: Contacts - Subformat: Contacts Details";

		file.Load(Ret_pStatus, strCurFile, lstInput, lstFormatPlain, lstFormatUtf8);
		if(Ret_pStatus.IsOK())
		{
			int nFieldsCnt = lstInput.getColumnCount();
			if(!bUseColMap && 56 != nFieldsCnt){
				//MB: field count is checked only when no col mapping used
				Ret_ProcessedFiles.setData(nLogRows-1, "FileOK", 1);
				Ret_ProcessedFiles.setData(nLogRows-1, "FileErrorTxt", QObject::tr("Document does not contain %1 fields for each record!").arg(56));
				continue;
			}
			else
				Ret_ProcessedFiles.setData(nLogRows-1, "FileOK", 0);

			DbRecordSet lstImport;
			lstImport.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CONTACT_FULL_TEXT_IMPORT));

			//merge results with destination list
			int nCnt = lstInput.getRowCount();
			int i;
			for(i=0; i<nCnt; i++)
			{
				lstImport.addRow();
				int nRow = lstImport.getRowCount() - 1;
				int nCol = 0;
				int nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "CLC_IS_ADD_ON",			lstInput.getDataRef(i, nRealCol).toInt());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BCNT_FIRSTNAME",			lstInput.getDataRef(i, nRealCol).toString());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BCNT_LASTNAME",	        lstInput.getDataRef(i, nRealCol).toString());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BCNT_MIDDLENAME",			lstInput.getDataRef(i, nRealCol).toString());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BCNT_NICKNAME",			lstInput.getDataRef(i, nRealCol).toString());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BCNT_BIRTHDAY",			DataHelper::ParseDateString(lstInput.getDataRef(i, nRealCol).toString()));
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BCNT_SEX",					lstInput.getDataRef(i, nRealCol).toInt());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BCNT_DEPARTMENTNAME",		lstInput.getDataRef(i, nRealCol).toString());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BCNT_PROFESSION",          lstInput.getDataRef(i, nRealCol).toString());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BCNT_FUNCTION",			lstInput.getDataRef(i, nRealCol).toString());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BCNT_SHORTNAME",			lstInput.getDataRef(i, nRealCol).toString());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BCNT_FOUNDATIONDATE",		DataHelper::ParseDateString(lstInput.getDataRef(i, nRealCol).toString()));
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BCNT_DESCRIPTION",         lstInput.getDataRef(i, nRealCol).toString());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BCNT_LANGUAGECODE",		lstInput.getDataRef(i, nRealCol).toString());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BCNT_SUPPRESS_MAILING",    lstInput.getDataRef(i, nRealCol).toInt());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BCNT_OLD_CODE",			lstInput.getDataRef(i, nRealCol).toString());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BCMA_FORMATEDADDRESS",     lstInput.getDataRef(i, nRealCol).toString());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BCMA_FIRSTNAME",           lstInput.getDataRef(i, nRealCol).toString());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BCMA_LASTNAME",			lstInput.getDataRef(i, nRealCol).toString());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BCMA_MIDDLENAME",			lstInput.getDataRef(i, nRealCol).toString());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BCMA_TITLE",				lstInput.getDataRef(i, nRealCol).toString());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BCMA_SALUTATION",				lstInput.getDataRef(i, nRealCol).toString());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BCMA_ORGANIZATIONNAME",				lstInput.getDataRef(i, nRealCol).toString());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BCMA_ORGANIZATIONNAME_2",				lstInput.getDataRef(i, nRealCol).toString());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BCMA_STREET",				lstInput.getDataRef(i, nRealCol).toString());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BCMA_ZIP",				lstInput.getDataRef(i, nRealCol).toString());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BCMA_CITY",				lstInput.getDataRef(i, nRealCol).toString());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BCMA_COUNTRY_CODE",				Countries::GetISOCodeFromDs(lstInput.getDataRef(i, nRealCol).toString()));
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BCMA_COUNTRY_NAME",				lstInput.getDataRef(i, nRealCol).toString());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BCMA_REGION",						lstInput.getDataRef(i, nRealCol).toString());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BCMA_POBOX",				lstInput.getDataRef(i, nRealCol).toString());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BCMA_POBOXZIP",				lstInput.getDataRef(i, nRealCol).toString());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BCMA_OFFICECODE",				lstInput.getDataRef(i, nRealCol).toString());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "CLC_EMAIL_ADDRESS",				lstInput.getDataRef(i, nRealCol).toString());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "CLC_EMAIL_ADDRESS_DESC",				lstInput.getDataRef(i, nRealCol).toString());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "CLC_WEB_ADDRESS",				lstInput.getDataRef(i, nRealCol).toString());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "CLC_WEB_ADDRESS_DESC",				lstInput.getDataRef(i, nRealCol).toString());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "CLC_PHONE_BUSINESS_CENTRAL",				lstInput.getDataRef(i, nRealCol).toString());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "CLC_PHONE_BUSINESS_DIRECT",				lstInput.getDataRef(i, nRealCol).toString());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "CLC_PHONE_MOBILE",				lstInput.getDataRef(i, nRealCol).toString());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "CLC_PHONE_FAX",				lstInput.getDataRef(i, nRealCol).toString());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "CLC_PHONE_PRIVATE",				lstInput.getDataRef(i, nRealCol).toString());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "CLC_PHONE_PRIVATE_MOBILE",				lstInput.getDataRef(i, nRealCol).toString());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "CLC_PHONE_SKYPE",				lstInput.getDataRef(i, nRealCol).toString());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "CLC_PHONE_GENERIC",				lstInput.getDataRef(i, nRealCol).toString());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "CLC_PHONE_GENERIC_NAME",				lstInput.getDataRef(i, nRealCol).toString());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BCMJ_PERSON_CODE",				lstInput.getDataRef(i, nRealCol).toString());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BCMJ_DATE",					DataHelper::ParseDateString(lstInput.getDataRef(i, nRealCol).toString()));
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BCMJ_TEXT",					lstInput.getDataRef(i, nRealCol).toString());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "BCMJ_IS_PRIVATE",				lstInput.getDataRef(i, nRealCol).toInt());
				nCol ++; nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
				lstImport.setData(nRow, "CLC_GROUPS",					lstInput.getDataRef(i, nRealCol).toString());
			}

			DbRecordSet lstImportFull;
			lstImportFull.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CONTACT_FULL));
			//lstImportFull.merge(lstImport);
			
			//BR: made tottaly new function:
			ConvertFullContactTextList_2_ContactFull(Ret_pStatus,lstImport,lstImportFull);
			if(!Ret_pStatus.IsOK()) return;

			DbRecordSet rowErr;
			rowErr.addColumn(QVariant::Int, "StatusCode");
			rowErr.addColumn(QVariant::String, "StatusText");
			rowErr.addColumn(QVariant::Int, "ErrRow");
			rowErr.addRow();

			ImportContacts(Ret_pStatus, lstImportFull, rowErr, 1 /*UPDATE_EXISTING*/, false);

			//count number of successfully and unsucessfully imported rows
			int nSuccess = 0;
			int nFailure = 0;
			nCnt = lstImportFull.getRowCount();
			for(i=0; i<nCnt; i++)
			{
				int nID = lstImportFull.getDataRef(i, "BCNT_ID").toInt();
				if(nID > 0)
					nSuccess ++;
				else{
					nFailure ++;

					//mark error for this row
					Ret_pErrors.addRow();
					int nErrRows = Ret_pErrors.getRowCount();
					Ret_pErrors.setData(nErrRows-1, "FileName", strCurFile);
					Ret_pErrors.setData(nErrRows-1, "RecIdx", i+1);	//starts from 1
					Ret_pErrors.setData(nErrRows-1, "ErrNo", 1);	//TOFIX
					Ret_pErrors.setData(nErrRows-1, "ErrText", "");	//TOFIX
				}
			}
			//write totals statistics
			Ret_ProcessedFiles.setData(nLogRows-1, "NumRecsSuccess", nSuccess);
			Ret_ProcessedFiles.setData(nLogRows-1, "NumRecsFailed", nFailure);

			//rename file - change extension to indicate file was processed
			g_pImportExportManager->DisableFileWatcher();
			QFileInfo info(strCurFile);
			QString strNewFile = strDirPath + info.baseName() + "." + strFileExtOfProcessed;
			if(strCurFile != strNewFile){
				QFile::remove(strNewFile);
				QFile::rename(strCurFile, strNewFile);
			}
			g_pImportExportManager->EnableFileWatcher();
		}
		else{
			Ret_ProcessedFiles.setData(nLogRows-1, "FileOK", 0);
			Ret_ProcessedFiles.setData(nLogRows-1, "FileErrorTxt", Ret_pStatus.getErrorText());
		}
	}
}

//normal:  map [data file col] -> [import view col]
//reverse: map [import view col] -> [data file col]
bool Service_BusImport::LoadColMap(QString strFile, QString lstFormatHdrLine, QMap<int, int> &mapCols, QString &strNamesHeader, bool bReverseMap)
{
	mapCols.clear();
	
	QMap<int, QString> mapColNames;

	//open the file
	QFile file(strFile);
	if (!file.open(QIODevice::ReadOnly))
		return false;

	//read first line - format specification
	QString strFirstLine = file.readLine(1000);

	//strip line endings (up to two chars)
	if( strFirstLine.length()>0 && 
		(strFirstLine.at(strFirstLine.length()-1) == '\r' ||
		 strFirstLine.at(strFirstLine.length()-1) == '\n')){
			strFirstLine = strFirstLine.mid(0, strFirstLine.length()-1);
	}
	if( strFirstLine.length()>0 && 
		(strFirstLine.at(strFirstLine.length()-1) == '\r' ||
		 strFirstLine.at(strFirstLine.length()-1) == '\n')){
			strFirstLine = strFirstLine.mid(0, strFirstLine.length()-1);
	}
	if(strFirstLine != lstFormatHdrLine)
		return false;

	//now parse the rest of the data dynamically
	QString strChunk;
	while (!file.atEnd())
	{
		strChunk = strChunk.fromLocal8Bit(file.readLine());
		
		//strip comment
		int nPos = strChunk.indexOf(";");
		if(nPos > 0)
			strChunk = strChunk.left(nPos);

		int nLeftCol  = -1;
		int nRightCol = -1;

		nPos = strChunk.indexOf(":");
		if(nPos > 0)
		{
			nLeftCol  = strChunk.left(nPos).trimmed().toInt();
			nRightCol = strChunk.right(strChunk.count()-nPos-1).trimmed().toInt();
			Q_ASSERT(nLeftCol > 0);
			Q_ASSERT(nRightCol > 0);

			//convert to zero-based index
			nRightCol --;
			nLeftCol --;

			//mapColNames[nLeftCol] = ""; //TOFIX

			if(bReverseMap)
				mapCols[nRightCol] = nLeftCol;	// mapped in reversed order!!!
			else
				mapCols[nLeftCol] = nRightCol;	// mapped in normal order!!!
		}
	}

	//create a header line out of mapping
	//WARNING: assumes that all columns were specified (no gaps in mapping file)
	strNamesHeader = "";
	int nSize = mapColNames.size();
	for(int i=0; i<nSize; i++){
		if(!strNamesHeader.isEmpty())
			strNamesHeader += "\t";
		strNamesHeader += mapColNames[i+1];
	}

	return true;
}

bool Service_BusImport::LoadHCodeMap(QString strFile, QString lstFormatHdrLine, QMap<QString, QString> &mapCodes)
{
	mapCodes.clear();

	//open the file
	QFile file(strFile);
	if (!file.open(QIODevice::ReadOnly))
		return false;

	//read first line - format specification
	QString strFirstLine = file.readLine(1000);

	//strip line endings (up to two chars)
	if( strFirstLine.length()>0 && 
		(strFirstLine.at(strFirstLine.length()-1) == '\r' ||
		 strFirstLine.at(strFirstLine.length()-1) == '\n')){
			strFirstLine = strFirstLine.mid(0, strFirstLine.length()-1);
	}
	if( strFirstLine.length()>0 && 
		(strFirstLine.at(strFirstLine.length()-1) == '\r' ||
		 strFirstLine.at(strFirstLine.length()-1) == '\n')){
			strFirstLine = strFirstLine.mid(0, strFirstLine.length()-1);
	}
	if(strFirstLine != lstFormatHdrLine)
		return false;

	//now parse the rest of the data dynamically
	QString strChunk;
	while (!file.atEnd()){
		strChunk = strChunk.fromLocal8Bit(file.readLine());
		
		//strip comment
		int nPos = strChunk.indexOf(";");
		if(nPos > 0)
			strChunk = strChunk.left(nPos);

		QString strLeftSide;
		QString strRightSide;

		nPos = strChunk.indexOf(":");
		if(nPos > 0){
			strLeftSide  = strChunk.left(nPos).trimmed();
			strRightSide = strChunk.right(strChunk.count()-nPos-1).trimmed();

			//strip comment marks
			strLeftSide = strLeftSide.left(strLeftSide.size()-1);
			strLeftSide = strLeftSide.right(strLeftSide.size()-1);

			strRightSide = strRightSide.left(strRightSide.size()-1);
			strRightSide = strRightSide.right(strRightSide.size()-1);

			Q_ASSERT(strLeftSide.size() > 0);
			Q_ASSERT(strRightSide.size() > 0);

			mapCodes[strLeftSide] = strRightSide;
		}
	}

	return true;
}

void Service_BusImport::GetOrganizationInfo(Status &status, QVariant &nOrgID, QVariant &nDeptID, DbRecordSet& lstData, DbSqlQuery &query, QString &strResOrgName, QString &strResDeptName, int nDefaultOrgID)
{
	QString strOrg  = lstData.getDataRef(0, "BORG_CODE").toString();
	QString strDept = lstData.getDataRef(0, "BDEPT_CODE").toString();


	strResOrgName = "";
	strResDeptName = "";

	if(!strOrg.isEmpty())
	{
		QString strQuery = "SELECT BORG_ID, BORG_NAME FROM BUS_ORGANIZATIONS WHERE BORG_CODE=?";
		query.Prepare(status, strQuery);
		if(!status.IsOK())
			return;
		query.bindValue(0, strOrg);
		query.ExecutePrepared(status);
		if(!status.IsOK())
			return;
	
		DbRecordSet lstTmp;
		query.FetchData(lstTmp);
		if(lstTmp.getRowCount()>0){
			nOrgID = lstTmp.getDataRef(0,0);
			strResOrgName = lstTmp.getDataRef(0,1).toString();
		}
	}

	if(!strDept.isEmpty())
	{
		QString strQuery = "SELECT BDEPT_ID, BDEPT_NAME FROM BUS_DEPARTMENTS WHERE BDEPT_CODE=?";
		if(nOrgID.toInt() > 0)
			strQuery += QString(" AND BDEPT_ORGANIZATION_ID=%1").arg(nOrgID.toInt());

		query.Prepare(status, strQuery);
		if(!status.IsOK())
			return;
		query.bindValue(0, strDept);
		query.ExecutePrepared(status);
		if(!status.IsOK())
			return;
	
		DbRecordSet lstTmp;
		query.FetchData(lstTmp);
		if(lstTmp.getRowCount()>0){
			nDeptID = lstTmp.getDataRef(0,0);
			strResDeptName = lstTmp.getDataRef(0,1).toString();
		}

		//B.T. MB on skype 18.03.2013: if dept is empty and organization is set then create new department based on dept code if not found in DB.
		if ((nOrgID.toInt()>0 || nDefaultOrgID>0) && nDeptID.toInt()==0)
		{
			//write department
			DbRecordSet lstDeptWrite;
			lstDeptWrite.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_DEPARTMENT));
			lstDeptWrite.addRow();
			lstDeptWrite.setData(0,"BDEPT_ACTIVE_FLAG",1);
			lstDeptWrite.setData(0,"BDEPT_ICON","");	
			lstDeptWrite.setData(0,"BDEPT_CODE",strDept);	
			lstDeptWrite.setData(0,"BDEPT_NAME","");	
			if (nOrgID.toInt()==0)//MB: 07.06.2013: on Skype if imported org is empty then use default organization from options
				lstDeptWrite.setData(0,"BDEPT_ORGANIZATION_ID",nDefaultOrgID);	
			else
				lstDeptWrite.setData(0,"BDEPT_ORGANIZATION_ID",nOrgID);	

			HierarchicalData HierarchicalWriter;
			HierarchicalWriter.Initialize(BUS_DEPARTMENT,GetDbManager());
			HierarchicalWriter.Write(status,lstDeptWrite);

			if (!status.IsOK())
				return;

			//qDebug()<<nDeptID.toInt();
			nDeptID=lstDeptWrite.getDataRef(0,"BDEPT_ID");
			strResDeptName="";
		}

	}
}

// code borrowed from export_debtors and export_users
void Service_BusImport::ProcessContacts_Export(Status &Ret_pStatus, QString strFileExt, QString strFilePrefix, QString strFileExtOfProcessed, QString strDirPath, int nExportLasN, bool bExportAddDateTimeOnExportFile, DbRecordSet &Ret_ProcessedFiles, DbRecordSet &Ret_pErrors, DbRecordSet &UsersForExport, bool bClientSideExport, QByteArray &Ret_File, bool bUtf8, bool bSokratesHdr, bool bColumnTitles, bool bOutlookExport, QString strLang, QByteArray datHedFile)
{
	strDirPath=QDir::toNativeSeparators(strDirPath);
	if(strDirPath.isEmpty() || strDirPath.at(strDirPath.size()-1) != QDir::separator())
		strDirPath += QDir::separator();

	//load column mapping / code mapping files
	QMap<int, int> mapCols;
	QString strNamesHeader;
	bool bUseColMap     = LoadColMap(strDirPath + "SOK_Map_Cols_Cont_Exp.map", "~~~SOKRATES Data Exchange - Version: 1.00 - Format: Debtors Column Mapping - Subformat: Debtor Export", mapCols, strNamesHeader,false);

	//generate output file path
	QString strOutFile;
	if(!bClientSideExport)
	{
		strOutFile = strDirPath;
		strOutFile += strFilePrefix;
		if(bExportAddDateTimeOnExportFile)
			strOutFile += QDateTime::currentDateTime().toString("_yyyyMMdd_hh_mm");
		strOutFile += ".";
		strOutFile += strFileExt;
	}

	DbSqlQuery query(Ret_pStatus,GetDbManager());
	if(!Ret_pStatus.IsOK()) return;

	//fetch contacts/debtors to export
	QString strQuery  = "SELECT * FROM BUS_CM_CONTACT";
			strQuery += "  LEFT OUTER JOIN BUS_CM_DEBTOR ON BCMD_CONTACT_ID=BCNT_ID"; //BT: when using joins, use in all cases
			strQuery += "  LEFT OUTER JOIN BUS_CM_ADDRESS ON BCMA_CONTACT_ID=BCNT_ID";
//			if(nAddressTypeFilter == 0)	strQuery += " AND BCMA_IS_DEFAULT=1";
			strQuery += "  LEFT OUTER JOIN BUS_CM_EMAIL ON BCME_CONTACT_ID=BCNT_ID AND BCME_IS_DEFAULT=1";
			strQuery += "  LEFT OUTER JOIN BUS_CM_INTERNET ON BCMI_CONTACT_ID=BCNT_ID AND BCMI_IS_DEFAULT=1";
//			strQuery += "  LEFT OUTER JOIN BUS_CM_JOURNAL ON BCMJ_CONTACT_ID=BCNT_ID";
			strQuery += "  LEFT OUTER JOIN BUS_PERSON ON BPER_CONTACT_ID=BCNT_ID";

	DbRecordSet lstExport;
	lstExport.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CONTACT_FULL_TEXT_IMPORT));
	lstExport.renameColumn("CLC_EMAIL_ADDRESS", "BCME_ADDRESS"); // so we can merge
	lstExport.renameColumn("CLC_EMAIL_ADDRESS_DESC", "BCME_DESCRIPTION"); // so we can merge
	lstExport.renameColumn("CLC_WEB_ADDRESS", "BCMI_ADDRESS"); // so we can merge
	lstExport.renameColumn("CLC_WEB_ADDRESS_DESC", "BCMI_DESCRIPTION"); // so we can merge

	bool bBind = false;
	int nCnt = UsersForExport.getRowCount();
	int nStartRow = 0; //when exporting in chunks

	ServerLocalCache cache;
	cache.LoadTypes();
	int nPhoneBussID = cache.GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_BUSINESS).toInt();
	int nPhoneBussDirectID = cache.GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_DIRECT).toInt();
	int nPhoneMobiID = cache.GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_MOBILE).toInt();
	int nPhonePrivID = cache.GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_PRIVATE).toInt();
	int nPhonePrivMobID = cache.GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_PRIVATE_MOBILE).toInt();
	int nPhoneFaxID   = cache.GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_FAX).toInt();
	int nPhoneSkypeID = cache.GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_SKYPE).toInt();

	while(1)	// fetch data in batches
	{
		QString strWhere;

		//prepare query for the new batch of records
		if(nCnt > 0)
		{
			const int nMaxBatch = 500;
			int nMax = nStartRow + min(nMaxBatch, (nCnt-nStartRow));

			//export only selected contacts
			strWhere = " WHERE BCNT_ID IN (";
			for(int i=nStartRow; i<nMax; i++){
				strWhere += UsersForExport.getDataRef(i,0).toString();
				if(i < (nMax-1))
					strWhere += ",";
			}
			strWhere += ")";
			strWhere += " ORDER BY BCNT_ID";

			nStartRow = nMax;
		}
		else{
			if(nExportLasN > 0)
			{
				//export only projects updated whitin the last X days
				strWhere = " WHERE BCNT_DAT_LAST_MODIFIED > ?";
				strWhere += " ORDER BY BCNT_ID";
				bBind = true;
			}
		}

		QString strQuery1 = strQuery + strWhere;

		query.Prepare(Ret_pStatus, strQuery1);
		if(!Ret_pStatus.IsOK())
			return;

		if(bBind){
			QDateTime dtLimit = QDateTime::currentDateTime().addDays(-nExportLasN);
			query.bindValue(0, dtLimit);
		}

		query.ExecutePrepared(Ret_pStatus);
		if(!Ret_pStatus.IsOK())
			return;

		int nOffset = lstExport.getRowCount();

		//fetch new batch of records
		DbRecordSet lstData;
		query.FetchData(lstData);
		lstData.addColumn(QVariant::Int, "CLC_IS_ADD_ON");	//"Follow-up" record
		//lstData.Dump();

		//calculate CLC_IS_ADD_ON
		int nColID = lstData.getColumnIdx("BCNT_ID");
		lstData.sort("BCNT_ID");
		int nCount = lstData.getRowCount();
		for(int j=0; j<nCount; j++)
		{
			lstData.setData(j, "CLC_IS_ADD_ON", 0);
			for(int k=j+1; k<nCount; k++)
			{
				if(lstData.getDataRef(k, "BCNT_ID").toInt() == lstData.getDataRef(j, "BCNT_ID").toInt())
				{
					if(lstData.getDataRef(k, "BCMA_IS_DEFAULT").toInt() > 0)
					{
						//this is the main record for this contact, we must swap rows so this one comes first in the list
						//(the first row must have CLC_IS_ADD_ON=0)
						DbRecordSet rowTmp = lstData.getRow(j);
						DbRecordSet rowTmp2 = lstData.getRow(k);
						lstData.assignRow(j, rowTmp2);
						lstData.assignRow(k, rowTmp);
						
						lstData.setData(j, "CLC_IS_ADD_ON", 0);
						lstData.setData(k, "CLC_IS_ADD_ON", 1); 
					}
					else{
						lstData.setData(k, "CLC_IS_ADD_ON", 1);
					}

					//we must reset duplicate value fields in each add-on record
					for(int z=j; z<k; z++)
					{
						//check and remove duplicate addresses
						if(lstData.getDataRef(z, "BCMA_FORMATEDADDRESS").toString() == lstData.getDataRef(k, "BCMA_FORMATEDADDRESS").toString()){
							lstData.setData(k, "BCMA_FORMATEDADDRESS", QVariant(QVariant::String));	//reset to NULL
							lstData.setData(k, "BCMA_FIRSTNAME", QVariant(QVariant::String));	//reset to NULL
							lstData.setData(k, "BCMA_LASTNAME", QVariant(QVariant::String));	//reset to NULL
							lstData.setData(k, "BCMA_MIDDLENAME", QVariant(QVariant::String));	//reset to NULL
							lstData.setData(k, "BCMA_TITLE", QVariant(QVariant::String));	//reset to NULL
							lstData.setData(k, "BCMA_SALUTATION", QVariant(QVariant::String));	//reset to NULL
							lstData.setData(k, "BCMA_ORGANIZATIONNAME", QVariant(QVariant::String));	//reset to NULL
							lstData.setData(k, "BCMA_ORGANIZATIONNAME_2", QVariant(QVariant::String));	//reset to NULL
							lstData.setData(k, "BCMA_STREET", QVariant(QVariant::String));	//reset to NULL
							lstData.setData(k, "BCMA_ZIP", QVariant(QVariant::String));	//reset to NULL
							lstData.setData(k, "BCMA_CITY", QVariant(QVariant::String));	//reset to NULL
							lstData.setData(k, "BCMA_COUNTRY_CODE", QVariant(QVariant::String));	//reset to NULL
							lstData.setData(k, "BCMA_COUNTRY_NAME", QVariant(QVariant::String));	//reset to NULL
							lstData.setData(k, "BCMA_REGION", QVariant(QVariant::String));	//reset to NULL
							lstData.setData(k, "BCMA_POBOX", QVariant(QVariant::String));	//reset to NULL
							lstData.setData(k, "BCMA_POBOXZIP", QVariant(QVariant::String));	//reset to NULL
							lstData.setData(k, "BCMA_OFFICECODE", QVariant(QVariant::String));	//reset to NULL
						}

						//check and remove duplicate email
						if(lstData.getDataRef(j, "BCME_ADDRESS").toString() == lstData.getDataRef(k, "BCME_ADDRESS").toString()){
							lstData.setData(k, "BCME_ADDRESS", QVariant(QVariant::String));	//reset to NULL
							lstData.setData(k, "BCME_DESCRIPTION", QVariant(QVariant::String));	//reset to NULL
						}

						//check and remove duplicate web addr
						if(lstData.getDataRef(j, "BCMI_ADDRESS").toString() == lstData.getDataRef(k, "BCMI_ADDRESS").toString()){
							lstData.setData(k, "BCMI_ADDRESS", QVariant(QVariant::String));	//reset to NULL
							lstData.setData(k, "BCMI_DESCRIPTION", QVariant(QVariant::String));	//reset to NULL
						}
					}

					j++;	//skip finding "addons of the addon" row (we start from next row with ADDON=0)
				}
			}
		}
		lstExport.merge(lstData);
		//lstExport.Dump();

		int nLastNonAddonIdx = -1;
		int nLastContactID = -1;
		int nLastMaxPhoneCnt = 0;

		//update additional data
		int nRows = lstData.getRowCount();
		for(int i=0; i<nRows; i++)
		{
			int nCntID = lstData.getDataRef(i, "BCNT_ID").toInt();
			int nIsAddon = lstData.getDataRef(i, "CLC_IS_ADD_ON").toInt();
			if(0 == nIsAddon){
				//check if last processed contact ID needs more rows to put all the phone numbers it has
				bool bListExpanded = false;
				if(nLastNonAddonIdx >= 0){
					int nLastContactRows = i-nLastNonAddonIdx-1;
					if(nLastMaxPhoneCnt > nLastContactRows)
					{
						//insert new rows in this position to make space for missing phones
						int nRowsToAdd = nLastMaxPhoneCnt - nLastContactRows;
						DbRecordSet row;
						row.copyDefinition(lstData);
						row.addRow();
						row.setData(0, "BCNT_ID", nLastContactID);
						row.setData(0, "CLC_IS_ADD_ON", 1);

						DbRecordSet row2;
						row2.copyDefinition(lstExport);	//two lists have different defs?!?!?
						row2.addRow();
						row2.setData(0, "CLC_IS_ADD_ON", 1);

						for(int b=0; b<nRowsToAdd; b++){
							lstData.insertRow(i);
							lstData.assignRow(i, row);

							lstExport.insertRow(i+nOffset);
							lstExport.assignRow(i+nOffset, row2);
						}

						//now fake some data to proceed looping
						nRows += nRowsToAdd;
						nCntID = nLastContactID;
						nIsAddon = 1;
						bListExpanded = true;
					}
				}

				if(!bListExpanded){
					nLastNonAddonIdx = i;
					nLastContactID = nCntID;
					nLastMaxPhoneCnt = 0;
				}
			}

			//calc groups
			QString strQuery = QString("SELECT BGIT_EXTERNAL_CATEGORY FROM bus_cm_group INNER JOIN bus_group_items ON BGIT_ID=BGCN_ITEM_ID WHERE BGCN_CONTACT_ID=%1").arg(nCntID);
			query.Execute(Ret_pStatus, strQuery);
			if(!Ret_pStatus.IsOK())
				return;	//TOFIX

			DbRecordSet lstGroups;
			query.FetchData(lstGroups);

			QString strGroups;
			int nCntRes = lstGroups.getRowCount();
			for(int j=0; j<nCntRes; j++){
				strGroups += lstGroups.getDataRef(j, 0).toString();
				if(j < nCntRes-1)
					strGroups += ",";
			}

			//set calculated data
			lstExport.setData(i+nOffset, "CLC_GROUPS", strGroups);

			// convert from HTML to plain text
			QString strData;
			lstExport.getData(i+nOffset, "BCMD_INVOICE_TEXT", strData);
			StripHTML(strData);
			lstExport.setData(i+nOffset, "BCMD_INVOICE_TEXT", strData);

			lstExport.getData(i+nOffset, "BCNT_DESCRIPTION", strData);
			StripHTML(strData);
			lstExport.setData(i+nOffset, "BCNT_DESCRIPTION", strData);

			lstExport.getData(i+nOffset, "BCMD_DESCRIPTION", strData);
			StripHTML(strData);
			lstExport.setData(i+nOffset, "BCMD_DESCRIPTION", strData);

			//
			// fetch telephones by types (if you have N phones, they are written in N rows for the same record)
			//
			QString strQuery2 = QString("SELECT BCMP_FULLNUMBER FROM BUS_CM_PHONE WHERE BCMP_CONTACT_ID=%1").arg(nCntID);
			int nRelIdx = i - nLastNonAddonIdx;	//index relative to the first result row for this contact
		
			if(nPhoneBussID > 0){
				query.Execute(Ret_pStatus, strQuery2 + QString(" AND BCMP_TYPE_ID=%1").arg(nPhoneBussID));
				if(!Ret_pStatus.IsOK())
					return;
				DbRecordSet lstPhone;
				query.FetchData(lstPhone);
				int nCount = lstPhone.getRowCount();
				if(nCount > nRelIdx)
					lstExport.setData(i+nOffset, "CLC_PHONE_BUSINESS_CENTRAL", lstPhone.getDataRef(nRelIdx, 0).toString());
				if(nCount > nLastMaxPhoneCnt)
					nLastMaxPhoneCnt = nCount;
			}
			if(nPhoneBussDirectID > 0){
				query.Execute(Ret_pStatus, strQuery2 + QString(" AND BCMP_TYPE_ID=%1").arg(nPhoneBussDirectID));
				if(!Ret_pStatus.IsOK())
					return;
				DbRecordSet lstPhone;
				query.FetchData(lstPhone);
				int nCount = lstPhone.getRowCount();
				if(nCount > nRelIdx)
					lstExport.setData(i+nOffset, "CLC_PHONE_BUSINESS_DIRECT", lstPhone.getDataRef(nRelIdx, 0).toString());
				if(nCount > nLastMaxPhoneCnt)
					nLastMaxPhoneCnt = nCount;
			}
			if(nPhonePrivID > 0){
				query.Execute(Ret_pStatus, strQuery2 + QString(" AND BCMP_TYPE_ID=%1").arg(nPhonePrivID));
				if(!Ret_pStatus.IsOK())
					return;
				DbRecordSet lstPhone;
				query.FetchData(lstPhone);
				int nCount = lstPhone.getRowCount();
				if(nCount > nRelIdx)
					lstExport.setData(i+nOffset, "CLC_PHONE_PRIVATE", lstPhone.getDataRef(nRelIdx, 0).toString());
				if(nCount > nLastMaxPhoneCnt)
					nLastMaxPhoneCnt = nCount;
			}
			if(nPhonePrivMobID > 0){
				query.Execute(Ret_pStatus, strQuery2 + QString(" AND BCMP_TYPE_ID=%1").arg(nPhonePrivMobID));
				if(!Ret_pStatus.IsOK())
					return;
				DbRecordSet lstPhone;
				query.FetchData(lstPhone);
				int nCount = lstPhone.getRowCount();
				if(nCount > nRelIdx)
					lstExport.setData(i+nOffset, "CLC_PHONE_PRIVATE_MOBILE", lstPhone.getDataRef(nRelIdx, 0).toString());
				if(nCount > nLastMaxPhoneCnt)
					nLastMaxPhoneCnt = nCount;
			}
			if(nPhoneMobiID > 0){
				query.Execute(Ret_pStatus, strQuery2 + QString(" AND BCMP_TYPE_ID=%1").arg(nPhoneMobiID));
				if(!Ret_pStatus.IsOK())
					return;
				DbRecordSet lstPhone;
				query.FetchData(lstPhone);
				int nCount = lstPhone.getRowCount();
				if(nCount > nRelIdx)
					lstExport.setData(i+nOffset, "CLC_PHONE_MOBILE", lstPhone.getDataRef(nRelIdx, 0).toString());
				if(nCount > nLastMaxPhoneCnt)
					nLastMaxPhoneCnt = nCount;
			}
			if(nPhoneFaxID > 0){
				query.Execute(Ret_pStatus, strQuery2 + QString(" AND BCMP_TYPE_ID=%1").arg(nPhoneFaxID));
				if(!Ret_pStatus.IsOK())
					return;
				DbRecordSet lstPhone;
				query.FetchData(lstPhone);
				int nCount = lstPhone.getRowCount();
				if(nCount > nRelIdx)
					lstExport.setData(i+nOffset, "CLC_PHONE_FAX", lstPhone.getDataRef(nRelIdx, 0).toString());
				if(nCount > nLastMaxPhoneCnt)
					nLastMaxPhoneCnt = nCount;
			}
			if(nPhoneSkypeID > 0){
				query.Execute(Ret_pStatus, strQuery2 + QString(" AND BCMP_TYPE_ID=%1").arg(nPhoneSkypeID));
				if(!Ret_pStatus.IsOK())
					return;
				DbRecordSet lstPhone;
				query.FetchData(lstPhone);
				int nCount = lstPhone.getRowCount();
				if(nCount > nRelIdx)
					lstExport.setData(i+nOffset, "CLC_PHONE_SKYPE", lstPhone.getDataRef(nRelIdx, 0).toString());
				if(nCount > nLastMaxPhoneCnt)
					nLastMaxPhoneCnt = nCount;
			}
		}

		if(nCnt <= 0 || nStartRow >= nCnt)
			break;
	}

	//lstExport.Dump();

	//TOFIX open and lock file
	QFile file(strOutFile);
	if(!bClientSideExport)
	{
		if (!file.open(QIODevice::WriteOnly)){
			Ret_pStatus.setError(1,QObject::tr("Can not open file"));
			return;
		}
	}

	QString strFormat;
	if(bSokratesHdr){
		if(bUtf8)
			strFormat = "~~~SOKRATES Data Exchange - Version: 2.00 - Charset: UTF8 - Format: Contacts - Subformat: Contacts Details\r\n";
		else
			strFormat = "~~~SOKRATES Data Exchange - Version: 2.00 - Format: Contacts - Subformat: Contacts Details\r\n";
		if(!bClientSideExport)
			file.write((bUtf8) ? strFormat.toUtf8() : strFormat.toLocal8Bit());
		else
			Ret_File += (bUtf8) ? strFormat.toUtf8() : strFormat.toLocal8Bit();
	}
	
	int nMaxCols = 66;

	QMap<int, QString> mapTitles;
	if(bColumnTitles || bOutlookExport)
	{
		if(datHedFile.size()>0)
		{
			//hed file was sent directly from the client
			//calculate the mapping
			QString strChunk;
			while (datHedFile.size() > 0)
			{
				//"read" line
				int nEOL = datHedFile.indexOf('\n');
				if(nEOL >= 0){
					strChunk = strChunk.fromLocal8Bit(datHedFile.left(nEOL+1));
					datHedFile = datHedFile.mid(nEOL+1);
				}
				else{
					strChunk = strChunk.fromLocal8Bit(datHedFile);
					datHedFile = "";
				}

				int nPos = strChunk.indexOf("\t");
				if(nPos > 0)
				{
					int nLeftCol  = strChunk.left(nPos).trimmed().toInt();
					Q_ASSERT(nLeftCol > 0);
					nLeftCol --;	//convert to zero-based index
					Q_ASSERT(nLeftCol < nMaxCols); // invalid column index used in the .hed file?

					QString strRightCol = strChunk.right(strChunk.count()-nPos).trimmed();
					mapTitles[nLeftCol] = strRightCol;	// mapped in normal order!!!
				}
			}
		}
		else
		{
			QString strHdrFile;
			if(strLang == "de"){
				if(bOutlookExport)
					strHdrFile = "Head_Cntct_Outlook_Ger.hed";
				else
					strHdrFile = "Head_Cntct_Comm_Ger.hed";
			}
			else{
				if(bOutlookExport)
					strHdrFile = "Head_Cntct_Outlook_Eng.hed";
				else
					strHdrFile = "Head_Cntct_Comm_Eng.hed";
			}

			QFile fileMap(QCoreApplication::applicationDirPath() + "/" + strHdrFile);
			if (!fileMap.open(QIODevice::ReadOnly)){
				Ret_pStatus.setError(1,QObject::tr("Can not open .hed file"));
				return;
			}
			//now parse the rest of the data dynamically
			QString strChunk;
			while (!fileMap.atEnd())
			{
				strChunk = strChunk.fromLocal8Bit(fileMap.readLine());
				int nPos = strChunk.indexOf("\t");
				if(nPos > 0)
				{
					int nLeftCol  = strChunk.left(nPos).trimmed().toInt();
					Q_ASSERT(nLeftCol > 0);
					nLeftCol --;	//convert to zero-based index
					Q_ASSERT(nLeftCol < nMaxCols); // invalid column index used in the .hed file?

					QString strRightCol = strChunk.right(strChunk.count()-nPos).trimmed();
					mapTitles[nLeftCol] = strRightCol;	// mapped in normal order!!!
				}
			}
		}
	}

	if(bColumnTitles || bOutlookExport)
	{
		QString strTitles;
		//write title line
		for(int i=0; i<nMaxCols; i++)
		{
			if(!mapTitles[i].isEmpty()){
				if(!strTitles.isEmpty())
					strTitles += "\t";
				strTitles += mapTitles[i];
			}
		}
		strTitles += "\r\n";
		if(!bClientSideExport)
			file.write((bUtf8) ? strTitles.toUtf8() : strTitles.toLocal8Bit());
		else
			Ret_File += (bUtf8) ? strTitles.toUtf8() : strTitles.toLocal8Bit();
	}

	//write records to file (use both mappings if needed)
	int nRows = lstExport.getRowCount();
	for(int i=0; i<nRows; i++)
	{
		if(bColumnTitles || bOutlookExport)
		{
			//filter out follow up lines
			if(lstExport.getDataRef(i, "CLC_IS_ADD_ON").toInt() > 0)
				continue;
		}

		//write fields
		QString strField;
		for(int nCol=0; nCol<nMaxCols; nCol++)
		{
			int nRealCol = (bUseColMap && mapCols.contains(nCol)) ? mapCols[nCol] : nCol;
			
			//skip exporting column that was not in the header title file
			if((bOutlookExport || bColumnTitles) && mapTitles[nRealCol].isEmpty())
				continue;

			if(lstExport.getDataRef(i, nRealCol).isNull())
				strField = "";
			else
				strField = lstExport.getDataRef(i, nRealCol).toString();

			//
			// make the fields CSV valid
			//

			//replace escape quotes " to ""
			strField.replace("\"", "\"\"");

			//now convert all possible line endings into the single one
			strField.replace("\r\n",   "\n");
			strField.replace("\r",	  "\n");

			//escape new line
			strField.replace("\n", "//");

			//quote field if needed
			if( strField.indexOf(" ") >= 0 ||
				strField.indexOf("\t") >= 0)
			{
				strField.prepend("\"");
				strField.append("\"");
			}

			if(!bClientSideExport)
				file.write((bUtf8) ? strField.toUtf8() : strField.toLocal8Bit());
			else
				Ret_File += (bUtf8) ? strField.toUtf8() : strField.toLocal8Bit();

			if(nCol<(nMaxCols-1)){
				if(!bClientSideExport)
					file.write("\t");
				else
					Ret_File += "\t";
			}
		}

		if(!bClientSideExport)
			file.write("\r\n");
		else
			Ret_File += "\r\n";
	}

	if(!bClientSideExport)
		file.close();

	//log file as processed
	Ret_ProcessedFiles.destroy();
	Ret_ProcessedFiles.addColumn(QVariant::String, "FileName");
	Ret_ProcessedFiles.addColumn(QVariant::Int,	   "FileOK");
	Ret_ProcessedFiles.addColumn(QVariant::String, "FileErrorTxt");
	Ret_ProcessedFiles.addColumn(QVariant::Int,	   "NumRecsSuccess");
	Ret_ProcessedFiles.addColumn(QVariant::Int,	   "NumRecsFailed");

	Ret_ProcessedFiles.addRow();
	Ret_ProcessedFiles.setData(0, "FileName", strOutFile);
	Ret_ProcessedFiles.setData(0, "FileOK", 0);
	Ret_ProcessedFiles.setData(0, "NumRecsSuccess", nRows);
}

void Service_BusImport::ProcessContactContactRelations_Export(Status &Ret_pStatus, QString strFileExt, QString strFilePrefix, QString strFileExtOfProcessed, QString strDirPath, int nExportLasN, bool bExportAddDateTimeOnExportFile, DbRecordSet &Ret_ProcessedFiles, DbRecordSet &Ret_pErrors, DbRecordSet &UsersForExport, bool bClientSideExport, QByteArray &Ret_File, bool bUtf8, bool bSokratesHdr, bool bColumnTitles, bool bOutlookExport, QString strLang, QByteArray datHedFile)
{
	strDirPath=QDir::toNativeSeparators(strDirPath);
	if(strDirPath.isEmpty() || strDirPath.at(strDirPath.size()-1) != QDir::separator())
		strDirPath += QDir::separator();

	//load column mapping / code mapping files
	QString strNamesHeader;

	//generate output file path
	QString strOutFile;
	if(!bClientSideExport)
	{
		strOutFile = strDirPath;
		strOutFile += strFilePrefix;
		if(bExportAddDateTimeOnExportFile)
			strOutFile += QDateTime::currentDateTime().toString("_yyyyMMdd_hh_mm");
		strOutFile += ".";
		strOutFile += strFileExt;
	}

	DbSqlQuery query(Ret_pStatus,GetDbManager());
	if(!Ret_pStatus.IsOK()) return;

	//fetch contacts/debtors to export
	QString strQuery  = "SELECT * FROM BUS_CM_CONTACT";
			strQuery += "  LEFT OUTER JOIN BUS_CM_DEBTOR ON BCMD_CONTACT_ID=BCNT_ID"; //BT: when using joins, use in all cases
			strQuery += "  LEFT OUTER JOIN BUS_CM_ADDRESS ON BCMA_CONTACT_ID=BCNT_ID";
//			if(nAddressTypeFilter == 0)	strQuery += " AND BCMA_IS_DEFAULT=1";
			strQuery += "  LEFT OUTER JOIN BUS_CM_EMAIL ON BCME_CONTACT_ID=BCNT_ID AND BCME_IS_DEFAULT=1";
			strQuery += "  LEFT OUTER JOIN BUS_CM_INTERNET ON BCMI_CONTACT_ID=BCNT_ID AND BCMI_IS_DEFAULT=1";
//			strQuery += "  LEFT OUTER JOIN BUS_CM_JOURNAL ON BCMJ_CONTACT_ID=BCNT_ID";
			strQuery += "  LEFT OUTER JOIN BUS_PERSON ON BPER_CONTACT_ID=BCNT_ID";

	DbRecordSet lstExport;
	lstExport.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CONTACT_CONTACT_RELATIONSHIP_EXPORT));
	lstExport.renameColumn("CLC_EMAIL_ADDRESS", "BCME_ADDRESS"); // so we can merge
	lstExport.renameColumn("CLC_EMAIL_ADDRESS_DESC", "BCME_DESCRIPTION"); // so we can merge
	lstExport.renameColumn("CLC_WEB_ADDRESS", "BCMI_ADDRESS"); // so we can merge
	lstExport.renameColumn("CLC_WEB_ADDRESS_DESC", "BCMI_DESCRIPTION"); // so we can merge
	lstExport.addColumn(QVariant::Int, "BCNT_ID"); // so we can merge
	
	bool bBind = false;
	int nCnt = UsersForExport.getRowCount();
	int nStartRow = 0; //when exporting in chunks

	ServerLocalCache cache;
	cache.LoadTypes();
	int nPhoneBussID = cache.GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_BUSINESS).toInt();
	int nPhoneBussDirectID = cache.GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_DIRECT).toInt();
	int nPhoneMobiID = cache.GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_MOBILE).toInt();
	int nPhonePrivID = cache.GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_PRIVATE).toInt();
	int nPhonePrivMobID = cache.GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_PRIVATE_MOBILE).toInt();
	int nPhoneFaxID   = cache.GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_FAX).toInt();
	int nPhoneSkypeID = cache.GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_SKYPE).toInt();

	while(1)	// fetch data in batches
	{
		QString strWhere;

		if(nCnt > 0)
		{
			const int nMaxBatch = 500;
			int nMax = nStartRow + min(nMaxBatch, (nCnt-nStartRow));

			//export only selected contacts
			strWhere = " WHERE BCNT_ID IN (";
			for(int i=nStartRow; i<nMax; i++){
				strWhere += UsersForExport.getDataRef(i,0).toString();
				if(i < (nMax-1))
					strWhere += ",";
			}
			strWhere += ")";

			nStartRow = nMax;
		}
		else{
			if(nExportLasN > 0)
			{
				//export only projects updated whitin the last X days
				strWhere = " WHERE BCNT_DAT_LAST_MODIFIED > ?";
				bBind = true;
			}
		}

		QString strQuery1 = strQuery + strWhere;

		query.Prepare(Ret_pStatus, strQuery1);
		if(!Ret_pStatus.IsOK())
			return;

		if(bBind){
			QDateTime dtLimit = QDateTime::currentDateTime().addDays(-nExportLasN);
			query.bindValue(0, dtLimit);
		}

		query.ExecutePrepared(Ret_pStatus);
		if(!Ret_pStatus.IsOK())
			return;

		int nOffset = lstExport.getRowCount();

		DbRecordSet lstData;
		query.FetchData(lstData);
		lstData.sort("BCNT_ID");
		lstExport.merge(lstData);
		//lstExport.Dump();

		//update additional data in a batch
		int nRows = lstData.getRowCount();
		for(int i=0; i<nRows; i++)
		{
			int nCntID = lstData.getDataRef(i, "BCNT_ID").toInt();
			
			// convert from HTML to plain text
			QString strData;
			lstExport.getData(i, "BCMD_INVOICE_TEXT", strData);
			StripHTML(strData);
			lstExport.setData(i, "BCMD_INVOICE_TEXT", strData);

			lstExport.getData(i, "BCNT_DESCRIPTION", strData);
			StripHTML(strData);
			lstExport.setData(i, "BCNT_DESCRIPTION", strData);

			lstExport.getData(i, "BCMD_DESCRIPTION", strData);
			StripHTML(strData);
			lstExport.setData(i, "BCMD_DESCRIPTION", strData);

			//
			// fetch telephones by types
			//
			QString strQuery2 = QString("SELECT BCMP_FULLNUMBER FROM BUS_CM_PHONE WHERE BCMP_CONTACT_ID=%1").arg(nCntID);
		
			if(nPhoneBussID > 0){
				query.Execute(Ret_pStatus, strQuery2 + QString(" AND BCMP_TYPE_ID=%1").arg(nPhoneBussID));
				if(!Ret_pStatus.IsOK())
					return;
				DbRecordSet lstPhone;
				query.FetchData(lstPhone);
				if(lstPhone.getRowCount() > 0)
					lstExport.setData(i+nOffset, "CLC_PHONE_BUSINESS_CENTRAL", lstPhone.getDataRef(0, 0).toString());
			}
			if(nPhoneBussDirectID > 0){
				query.Execute(Ret_pStatus, strQuery2 + QString(" AND BCMP_TYPE_ID=%1").arg(nPhoneBussDirectID));
				if(!Ret_pStatus.IsOK())
					return;
				DbRecordSet lstPhone;
				query.FetchData(lstPhone);
				if(lstPhone.getRowCount() > 0)
					lstExport.setData(i+nOffset, "CLC_PHONE_BUSINESS_DIRECT", lstPhone.getDataRef(0, 0).toString());
			}
			if(nPhonePrivID > 0){
				query.Execute(Ret_pStatus, strQuery2 + QString(" AND BCMP_TYPE_ID=%1").arg(nPhonePrivID));
				if(!Ret_pStatus.IsOK())
					return;
				DbRecordSet lstPhone;
				query.FetchData(lstPhone);
				if(lstPhone.getRowCount() > 0)
					lstExport.setData(i+nOffset, "CLC_PHONE_PRIVATE", lstPhone.getDataRef(0, 0).toString());
			}
			if(nPhonePrivMobID > 0){
				query.Execute(Ret_pStatus, strQuery2 + QString(" AND BCMP_TYPE_ID=%1").arg(nPhonePrivMobID));
				if(!Ret_pStatus.IsOK())
					return;
				DbRecordSet lstPhone;
				query.FetchData(lstPhone);
				if(lstPhone.getRowCount() > 0)
					lstExport.setData(i+nOffset, "CLC_PHONE_PRIVATE_MOBILE", lstPhone.getDataRef(0, 0).toString());
			}
			if(nPhoneMobiID > 0){
				query.Execute(Ret_pStatus, strQuery2 + QString(" AND BCMP_TYPE_ID=%1").arg(nPhoneMobiID));
				if(!Ret_pStatus.IsOK())
					return;
				DbRecordSet lstPhone;
				query.FetchData(lstPhone);
				if(lstPhone.getRowCount() > 0)
					lstExport.setData(i+nOffset, "CLC_PHONE_MOBILE", lstPhone.getDataRef(0, 0).toString());
			}
			if(nPhoneFaxID > 0){
				query.Execute(Ret_pStatus, strQuery2 + QString(" AND BCMP_TYPE_ID=%1").arg(nPhoneFaxID));
				if(!Ret_pStatus.IsOK())
					return;
				DbRecordSet lstPhone;
				query.FetchData(lstPhone);
				if(lstPhone.getRowCount() > 0)
					lstExport.setData(i+nOffset, "CLC_PHONE_FAX", lstPhone.getDataRef(0, 0).toString());
			}
			if(nPhoneSkypeID > 0){
				query.Execute(Ret_pStatus, strQuery2 + QString(" AND BCMP_TYPE_ID=%1").arg(nPhoneSkypeID));
				if(!Ret_pStatus.IsOK())
					return;
				DbRecordSet lstPhone;
				query.FetchData(lstPhone);
				if(lstPhone.getRowCount() > 0)
					lstExport.setData(i+nOffset, "CLC_PHONE_SKYPE", lstPhone.getDataRef(0, 0).toString());
			}
		}

		if(nCnt <= 0 || nStartRow >= nCnt)
			break;
	}

	lstExport.setColValue("CLC_IS_ADD_ON", 0);

	//update additional data
	int nRows = lstExport.getRowCount();
	int i;
	for(i=0; i<nRows; i++)
	{
		int nCntID = lstExport.getDataRef(i, "BCNT_ID").toInt();

		//calc roles
		Status Ret_pStatus;
		DbRecordSet lstDataNMRX;
		DbRecordSet lstDataContacts;
		g_BusinessServiceSet->BusContact->ReadNMRXContactsFromContact_Ext(Ret_pStatus, nCntID, lstDataNMRX, lstDataContacts);
		if(!Ret_pStatus.IsOK())
			return;	//TOFIX

		int nRolesCnt = lstDataNMRX.getRowCount();
		int nContactCnt = lstDataContacts.getRowCount();
		
#ifdef _DEBUG
		//lstDataNMRX.Dump();
		//lstDataContacts.Dump();
#endif
		Q_ASSERT(nRolesCnt == nContactCnt);

		//if multiple roles, we need to create multiple duplicate contact rows for export
		if(nRolesCnt > 1){
			for(int j=1; j<nRolesCnt; j++){
				lstExport.insertRow(i+j);
				DbRecordSet row = lstExport.getRow(i);
				lstExport.assignRow(i+j, row);
			}
		}

		//fill in the data
		for(int j=0; j<nRolesCnt; j++){
			//update relationship direction
			//Direction: > if the base contact is first in the relation definition window, < if the base contact is the second in the relation definition window 
			lstExport.setData(i+j, "BUCR_ROLE_DIRECTION", (lstDataNMRX.getDataRef(j, "IS_AB").toInt() > 0) ? ">" : "<");
 			lstExport.setData(i+j, "BUCR_ROLE", lstDataNMRX.getDataRef(j, "BNRO_NAME").toString());
			lstExport.setData(i+j, "BUCR_DESCRIPTION", lstDataNMRX.getDataRef(j, "BNRO_DESCRIPTION").toString());
			lstExport.setData(i+j, "BCNT2_FIRSTNAME", lstDataContacts.getDataRef(j, "BCNT_FIRSTNAME").toString());
			lstExport.setData(i+j, "BCNT2_LASTNAME", lstDataContacts.getDataRef(j, "BCNT_LASTNAME").toString());
			lstExport.setData(i+j, "BCNT2_ORGANIZATIONNAME", lstDataContacts.getDataRef(j, "BCNT_ORGANIZATIONNAME").toString());
		}

		if(nRolesCnt > 1){
			i += nRolesCnt-1;
			nRows += nRolesCnt-1;
		}
	}

	 //update additional data
    nRows = lstExport.getRowCount();
    for(i=0; i<nRows; i++)
    {
        int nCntID = lstExport.getDataRef(i, "BCNT_ID").toInt();

        //calc groups
        QString strQuery = QString("SELECT BGIT_CODE, BGIT_NAME FROM bus_cm_group INNER JOIN bus_group_items ON BGIT_ID=BGCN_ITEM_ID WHERE BGCN_CONTACT_ID=%1").arg(nCntID);
        query.Execute(Ret_pStatus, strQuery);
        if(!Ret_pStatus.IsOK())
            return;    //TOFIX

        DbRecordSet lstGroups;
        query.FetchData(lstGroups);

        QString strGroups;
        int nCntRes = lstGroups.getRowCount();
        for(int j=0; j<nCntRes; j++){
            strGroups += lstGroups.getDataRef(j, 0).toString();
			strGroups += " ";
			strGroups += lstGroups.getDataRef(j, 1).toString();
            if(j < nCntRes-1)
                strGroups += ",";
        }

        //set calculated data
        lstExport.setData(i, "CLC_GROUPS", strGroups);
	}

	//lstExport.Dump();

	//TOFIX open and lock file
	QFile file(strOutFile);
	if(!bClientSideExport)
	{
		if (!file.open(QIODevice::WriteOnly)){
			Ret_pStatus.setError(1,QObject::tr("Can not open file"));
			return;
		}
	}

	QString strFormat;
	if(bSokratesHdr){
		if(bUtf8)
			strFormat = "~~~SOKRATES Data Exchange - Version: 2.00 - Charset: UTF8 - Format: Contacts - Subformat: Contact Relations\r\n";
		else
			strFormat = "~~~SOKRATES Data Exchange - Version: 2.00 - Format: Contacts - Subformat: Contact Relations\r\n";
		if(!bClientSideExport)
			file.write((bUtf8) ? strFormat.toUtf8() : strFormat.toLocal8Bit());
		else
			Ret_File += (bUtf8) ? strFormat.toUtf8() : strFormat.toLocal8Bit();
	}
	
	int nMaxCols = 73;	//issue #2419

	QMap<int, QString> mapTitles;
	if(bColumnTitles || bOutlookExport)
	{
		if(datHedFile.size()>0)
		{
			//hed file was sent directly from the client
			//calculate the mapping
			QString strChunk;
			while (datHedFile.size() > 0)
			{
				//"read" line
				int nEOL = datHedFile.indexOf('\n');
				if(nEOL >= 0){
					strChunk = strChunk.fromLocal8Bit(datHedFile.left(nEOL+1));
					datHedFile = datHedFile.mid(nEOL+1);
				}
				else{
					strChunk = strChunk.fromLocal8Bit(datHedFile);
					datHedFile = "";
				}

				int nPos = strChunk.indexOf("\t");
				if(nPos > 0)
				{
					int nLeftCol  = strChunk.left(nPos).trimmed().toInt();
					Q_ASSERT(nLeftCol > 0);
					nLeftCol --;	//convert to zero-based index
					Q_ASSERT(nLeftCol < nMaxCols); // invalid column index used in the .hed file?

					QString strRightCol = strChunk.right(strChunk.count()-nPos).trimmed();
					mapTitles[nLeftCol] = strRightCol;	// mapped in normal order!!!
				}
			}
		}
		else
		{
			QString strHdrFile;
			if(strLang == "de"){
				//if(bOutlookExport)
				//	strHdrFile = "Head_Cntct_Outlook_Ger.hed";
				//else
					strHdrFile = "Head_Cntct_Rel_Ger.hed";
			}
			else{
				//if(bOutlookExport)
				//	strHdrFile = "Head_Cntct_Outlook_Eng.hed";
				//else
					strHdrFile = "Head_Cntct_Rel_Eng.hed";
			}

			QFile fileMap(QCoreApplication::applicationDirPath() + "/" + strHdrFile);
			if (!fileMap.open(QIODevice::ReadOnly)){
				Ret_pStatus.setError(1,QObject::tr("Can not open .hed file"));
				return;
			}
			//now parse the rest of the data dynamically
			QString strChunk;
			while (!fileMap.atEnd())
			{
				strChunk = strChunk.fromLocal8Bit(fileMap.readLine());
				int nPos = strChunk.indexOf("\t");
				if(nPos > 0)
				{
					int nLeftCol  = strChunk.left(nPos).trimmed().toInt();
					Q_ASSERT(nLeftCol > 0);
					nLeftCol --;	//convert to zero-based index
					Q_ASSERT(nLeftCol < nMaxCols); // invalid column index used in the .hed file?

					QString strRightCol = strChunk.right(strChunk.count()-nPos).trimmed();
					mapTitles[nLeftCol] = strRightCol;	// mapped in normal order!!!
				}
			}
		}
	}

	if(bColumnTitles || bOutlookExport)
	{
		QString strTitles;
		//write title line
		for(int i=0; i<nMaxCols; i++)
		{
			if(!mapTitles[i].isEmpty()){
				if(!strTitles.isEmpty())
					strTitles += "\t";
				strTitles += mapTitles[i];
			}
		}
		strTitles += "\r\n";
		if(!bClientSideExport)
			file.write((bUtf8) ? strTitles.toUtf8() : strTitles.toLocal8Bit());
		else
			Ret_File += (bUtf8) ? strTitles.toUtf8() : strTitles.toLocal8Bit();
	}

	//write records to file (use both mappings if needed)
	nRows = lstExport.getRowCount();
	for(int i=0; i<nRows; i++)
	{
		if(bColumnTitles || bOutlookExport)
		{
			//filter out follow up lines
			if(lstExport.getDataRef(i, "CLC_IS_ADD_ON").toInt() > 0)
				continue;
		}

		//write fields
		QString strField;
		for(int nCol=0; nCol<nMaxCols; nCol++)
		{
			int nRealCol = nCol;
			
			//skip exporting column that was not in the header title file
			if((bOutlookExport || bColumnTitles) && mapTitles[nRealCol].isEmpty())
				continue;

			if(lstExport.getDataRef(i, nRealCol).isNull())
				strField = "";
			else
				strField = lstExport.getDataRef(i, nRealCol).toString();

			//
			// make the fields CSV valid
			//

			//replace escape quotes " to ""
			strField.replace("\"", "\"\"");

			//now convert all possible line endings into the single one
			strField.replace("\r\n",   "\n");
			strField.replace("\r",	  "\n");

			//escape new line
			strField.replace("\n", "//");

			//quote field if needed
			if( strField.indexOf(" ") >= 0 ||
				strField.indexOf("\t") >= 0)
			{
				strField.prepend("\"");
				strField.append("\"");
			}

			if(!bClientSideExport)
				file.write((bUtf8) ? strField.toUtf8() : strField.toLocal8Bit());
			else
				Ret_File += (bUtf8) ? strField.toUtf8() : strField.toLocal8Bit();

			if(nCol<(nMaxCols-1)){
				if(!bClientSideExport)
					file.write("\t");
				else
					Ret_File += "\t";
			}
		}

		if(!bClientSideExport)
			file.write("\r\n");
		else
			Ret_File += "\r\n";
	}

	if(!bClientSideExport)
		file.close();

	//log file as processed
	Ret_ProcessedFiles.destroy();
	Ret_ProcessedFiles.addColumn(QVariant::String, "FileName");
	Ret_ProcessedFiles.addColumn(QVariant::Int,	   "FileOK");
	Ret_ProcessedFiles.addColumn(QVariant::String, "FileErrorTxt");
	Ret_ProcessedFiles.addColumn(QVariant::Int,	   "NumRecsSuccess");
	Ret_ProcessedFiles.addColumn(QVariant::Int,	   "NumRecsFailed");

	Ret_ProcessedFiles.addRow();
	Ret_ProcessedFiles.setData(0, "FileName", strOutFile);
	Ret_ProcessedFiles.setData(0, "FileOK", 0);
	Ret_ProcessedFiles.setData(0, "NumRecsSuccess", nRows);
}

void Service_BusImport::ConvertFullContactTextList_2_ContactFull(Status &Ret_Status,DbRecordSet &lstInput, DbRecordSet &lstOut)
{

	//--------------------------
	//OPTIONS AND SETTINGS.
	//--------------------------
	DbRecordSet Ret_recSettings,Ret_recOptions,lstUsers,lstSchemas,lstTypes;
	QString strLoggedPersonID = QVariant(g_UserSessionManager->GetPersonID()).toString();

	DbSimpleOrm OPTIONS_SETTINGS_ORM(Ret_Status, BUS_OPT_SETTINGS, GetDbManager()); 
	if(!Ret_Status.IsOK()) return;

	//First person settings.
	QString strWhere = "WHERE BOUS_PERSON_ID = " + strLoggedPersonID;
	OPTIONS_SETTINGS_ORM.Read(Ret_Status, Ret_recSettings, strWhere);
	if(!Ret_Status.IsOK()) return;

	strWhere = "WHERE BOUS_PERSON_ID IS NULL ";
	OPTIONS_SETTINGS_ORM.Read(Ret_Status, Ret_recOptions, strWhere);
	if(!Ret_Status.IsOK()) return;

	//-------------------------
	//Users:
	//--------------------------
	DbRecordSet filter;
	g_BusinessServiceSet->MainEntitySelector->ReadData(Ret_Status,ENTITY_BUS_PERSON,filter,lstUsers);
	if(!Ret_Status.IsOK()) return;

	//--------------------------
	//Address shemas
	//--------------------------
	g_BusinessServiceSet->BusAddressSchemas->Read(Ret_Status,lstSchemas);
	if(!Ret_Status.IsOK()) return;

	//--------------------------
	//Contact types
	//--------------------------
	filter.clear();
	g_BusinessServiceSet->MainEntitySelector->ReadData(Ret_Status,ENTITY_BUS_CM_TYPES,filter,lstTypes);
	if(!Ret_Status.IsOK()) return;

	ContactTypeManager::ConvertContactListFromImport(lstInput,lstOut,"",&lstUsers);
	ContactTypeManager::PrepareContactListFromImport(lstOut,&lstTypes,&Ret_recSettings,&Ret_recOptions,g_UserSessionManager->GetPersonID(),&lstSchemas);
}

// Html escape sequences table
typedef struct {
	int  cLetter;
	const char *szEscape;
} HtmlEscape;

static std::vector<HtmlEscape> g_lstTableSort2;

//table is sorted by first field to enable binary search
static const HtmlEscape _table_char[] =
{
	{'\"',	"&quot;"},  //
	{'&',	"&amp;"},   //
	{'<',	"&lt;"},    //
	{'>',	"&gt;"},    //
	{' ',	"&nbsp;"},  //breaking space
	{161,	"&iexcl;"}, // - inverted exclamation mark
	{162,	"&cent;"},  //
	{163,	"&pound;"}, //
 	{164,	"&curren;"},//
	{165,	"&yen;"},   //
	{166,	"&brvbar;"},// - broken (vertical) bar
	{167,	"&sect;"},  // - section sign
	{168,	"&uml;"},   // - umlaut
	{169,	"&copy;"},  // - copyright sign
	{170,	"&ordf;"},  // - feminine ordinal
	{171,	"&laquo;"}, // - left guillemet
	{174,	"&reg;"},   // - registered sign

	{176,	"&deg;"},   // - degree sign
	{177,	"&plusmn;"},// - plus or minus
	{178,	"&sup2;"},  // - superscript two
	{179,	"&sup3;"},  // - superscript three

	{187,	"&raquo;"}, // - right guillemet

	{192,	"&Agrave;"},//
	{193,	"&Aacute;"},//'
	{194,	"&Acirc;"}, //
	{195,	"&Atilde;"},//'
	{196,	"&Auml;"},  //
	{197,	"&Aring;"}, //
	{198,	"&AElig;"}, //
	{199,	"&Ccedil;"},//
	{200,	"&Egrave;"},//'
	{201,	"&Eacute;"},//
	{202,	"&Ecirc;"}, //
	{203,	"&Euml;"},  //
	{204,	"&Igrave;"},//
	{205,	"&Iacute;"},//
	{206,	"&Icirc;"}, //
	{207,	"&Iuml;"},  //
	{208,	"&ETH;"},   //' - capital Eth, Icelandic
	{209,	"&Ntilde;"},//
	{210,	"&Ograve;"},//
	{211,	"&Oacute;"},//
	{212,	"&Ocirc;"}, //
	{213,	"&Otilde;"},//
	{214,	"&Ouml;"},  //
	{215,	"&times;"}, // - multiply sign
	{216,	"&Oslash;"},//'
	{217,	"&Ugrave;"},//
	{218,	"&Uacute;"},//
	{219,	"&Ucirc;"}, //
	{220,	"&Uuml;"},  //
	{221,	"&Yacute;"},//
	{222,	"&THORN;"}, // - capital THORN, Icelandic
	{223,	"&szlig;"}, //
	{224,	"&agrave;"},//
	{225,	"&aacute;"},//
	{226,	"&acirc;"}, //
	{227,	"&atilde;"},//
	{228,	"&auml;"},  //
	{229,	"&aring;"}, //

	{230,	"&aelig;"}, //
	{231,	"&ccedil;"},//
	{232,	"&egrave;"},//
	{233,	"&eacute;"},//
	{234,	"&ecirc;"}, //
	{235,	"&euml;"},  //
	{236,	"&igrave;"},//
	{237,	"&iacute;"},//
	{238,	"&icirc;"}, //
	{239,	"&iuml;"},  //
	{240,	"&eth;"},   // - small eth, Icelandic
	{241,	"&ntilde;"},//
	{242,	"&ograve;"},//
	{243,	"&oacute;"},//
	{244,	"&ocirc;"},	//
	{245,	"&otilde;"},//
	{246,	"&ouml;"},  //

	{248,	"&oslash;"},//
	{249,	"&ugrave;"},//
	{250,	"&uacute;"},//
	{251,	"&ucirc;"}, //
	{252,	"&uuml;"},  //
	{253,	"&yacute;"},//
	{254,	"&thorn;"}, // - small thorn, Icelandic
	{255,	"&yuml;"},  //

	{338,	"&OElig;"}, //
	{339,	"&oelig;"}, //
 	{352,	"&Scaron;"},//
 	{353,	"&scaron;"},//
	{376,	"&Yuml;"},  //
 	{402,	"&fnof;"},  //

 	{8211,	"&#8211;"}, //' - en dash (demi-cadratin)
 	{8212,	"&#8212;"}, //' - em dash (cadratin)

 	{8249,	"&#8249;"}, //' - left single guillemet
 	{8250,	"&#8250;"}, //' - right single guillemet
 	{8364,	"&euro;"},  //'
 	{8482,	"&trade;"}, //' - trademark

	//TOFIX add more chars if needed
};

class TblComparator{
public:
	bool operator()(const HtmlEscape &a, const HtmlEscape &b)
	{
		//operator < (is a<b ?)
		return (strcmp(a.szEscape, b.szEscape) < 0);
	};
};

#define SIZE_OF(x) (sizeof(x)/sizeof(x[0]))
static int table_bin_search_escape(const char *szFind, int nLeft = 0, int nRight = SIZE_OF(_table_char)-1);

#include <algorithm>

void UnescapeChars(QString &data)
{
	g_lstTableSort2.clear();
	for(unsigned int i=0; i<SIZE_OF(_table_char); i++)
		g_lstTableSort2.push_back(_table_char[i]);
	TblComparator cmp;
	std::sort(g_lstTableSort2.begin(), g_lstTableSort2.end(), cmp);

	unsigned int nPos = 0;
	while(1)
	{
		int nPosStart = data.indexOf('&', nPos);
		if(nPosStart < 0)
			break;

		int nPosEnd = data.indexOf(';', nPosStart+1);
		if(nPosEnd >= 0)
		{
			//extract escape sequence
			std::string strChar = data.mid(nPosStart, nPosEnd-nPosStart+1).toLatin1().constData();
			//TRACE("Escape sequence %s found!\n", strChar.c_str());

			int nRes = table_bin_search_escape(strChar.c_str());
			if(nRes >= 0)
			{
				//replace escape sequence with original UTF-8 character
				QChar chLetter(g_lstTableSort2[nRes].cLetter);

				data.remove(nPosStart, nPosEnd+1-nPosStart);
				data.insert(nPosStart, chLetter);
			}
			//else
			//	TRACE("ERROR: HTML escape sequence %s is not supported yet!\n", strChar.c_str());
		}
		else
			break;	//no sequence found

		nPos = nPosStart+1;
	}
}

int table_bin_search_escape(const char *szFind, int nLeft, int nRight)
{
	//TRACE("bin search [Escape:%s], l=%d, r=%d\n", szFind, nLeft, nRight);

	if(nLeft > nRight) {
		//TRACE("bin search: no match found\n");
		return -1;	//no match found
	}

	//check middle of the range
	int nMid = (nLeft + nRight)/2;
	if(0 == strcmp(szFind, g_lstTableSort2[nMid].szEscape)){
		//TRACE("bin search found [Escape:%s], m=%d\n", szFind, nMid);
		return nMid;	//match found
	}

	if(nLeft == nRight){
		//TRACE("bin search: no match found\n");
		return -1;	//no match found
	}

	if(strcmp(szFind, g_lstTableSort2[nMid].szEscape) < 0)
	{
		//TRACE("Search lower half, mid[%d]=%s\n", nMid, g_lstTableSort2[nMid].szEscape);
		return table_bin_search_escape(szFind, nLeft, nMid-1);	//search lower half
	}
	else{
		//TRACE("Search upper half, mid[%d]=%s\n", nMid, g_lstTableSort2[nMid].szEscape);
		return table_bin_search_escape(szFind, nMid+1, nRight);	//search upper half
	}
}

void Service_BusImport::StripHTML(QString &strData)
{
	//strip everything between <style and </style>
	int nStart = 0;
	if((nStart = strData.indexOf("<style", nStart)) >= 0)
	{
		int nEnd = strData.indexOf("</style>", nStart+1);
		if(nEnd >= 0)
		{
			nEnd += strlen("</style>");
			//cut the tag 
			strData.remove(nStart, nEnd-nStart);
		}
	}

	//strip tags
	nStart = 0;
	while((nStart = strData.indexOf('<', nStart)) >= 0)
	{
		int nEnd = strData.indexOf('>', nStart+1);
		if(nEnd < 0)
			break;
		//cut the tag 
		strData.remove(nStart, nEnd-nStart+1);
	}

	//now unescape HTML chars
	UnescapeChars(strData);
}

//B.T. 12.03.2011: get default address type from DB then assign to all new imported contacts: issue 2582
int Service_BusImport::GetDefaultAdddressType()
{
	Status err;

	DbSqlQuery query(err, GetDbManager()); 
	if(!err.IsOK()) return -1;

	QString strSQL="SELECT BCMT_ID FROM BUS_CM_TYPES WHERE BCMT_IS_DEFAULT=1 AND BCMT_ENTITY_TYPE="+QString::number(ContactTypeManager::TYPE_ADDRESS);
	query.Execute(err,strSQL);
	if(!err.IsOK()) return -1;

	if (query.next())
		return query.value(0).toInt();

	return -1;
}

//automated data exchange for SPL
void Service_BusImport::ProcessStoredProjectList_Import(Status &Ret_pStatus, QString strFileExt, QString strFilePrefix, QString strFileExtOfProcessed, QString strDirPath, DbRecordSet &Ret_ProcessedFiles, DbRecordSet &Ret_pErrors)
{
	strDirPath=QDir::toNativeSeparators(strDirPath);
	if(strDirPath.isEmpty() || strDirPath.at(strDirPath.size()-1) != QDir::separator())
		strDirPath += QDir::separator();

	//redefine list of processed files
	Ret_ProcessedFiles.destroy();
	Ret_ProcessedFiles.addColumn(QVariant::String, "FileName");
	Ret_ProcessedFiles.addColumn(QVariant::Int,	   "FileOK");
	Ret_ProcessedFiles.addColumn(QVariant::String, "FileErrorTxt");
	Ret_ProcessedFiles.addColumn(QVariant::Int,	   "NumRecsSuccess");
	Ret_ProcessedFiles.addColumn(QVariant::Int,	   "NumRecsFailed");

	//redefine list of errors
	Ret_pErrors.destroy();
	Ret_pErrors.addColumn(QVariant::String, "FileName");
	Ret_pErrors.addColumn(QVariant::Int,	"RecIdx");	//starts from 1
	Ret_pErrors.addColumn(QVariant::Int,	"ErrNo");
	Ret_pErrors.addColumn(QVariant::String, "ErrText");

	//B.T.: must be only two files: prefix+_headers.+prefix
	QString strHeaderFile=strDirPath+strFilePrefix+"_Headers."+strFileExt;
	QString strDetailsFile=strDirPath+strFilePrefix+"_Details."+strFileExt;


	Ret_ProcessedFiles.addRow();
	//Ret_pErrors.addRow();

	CsvImportFile file;

	//STEP 1: load files

	DbRecordSet lstHeaderData;
	file.Load(Ret_pStatus, strHeaderFile, lstHeaderData, QStringList(), QStringList(), true);
	if(!Ret_pStatus.IsOK())
	{
		Ret_pStatus.setError(StatusCodeSet::ERR_BUS_IMPORT_SKIPPED); //B.T. 2013-06-26: when files are imported on file change..ignore this message
		return;

		QString strErr=Ret_pStatus.getErrorText();
		Ret_pStatus.setError(1, strErr);
		Ret_ProcessedFiles.setData(0,"FileName",strHeaderFile);
		Ret_ProcessedFiles.setData(0,"FileOK",1); //error
		Ret_ProcessedFiles.setData(0,"FileErrorTxt",strErr);
		return;
	}

	int nColCnt = lstHeaderData.getColumnCount();
	if(nColCnt != 8){
		QString strErr=QString("Invalid column count in header file (%1 instead of 8): %2").arg(nColCnt).arg(strHeaderFile);
		Ret_pStatus.setError(1, strErr);
		Ret_ProcessedFiles.setData(0,"FileName",strHeaderFile);
		Ret_ProcessedFiles.setData(0,"FileOK",1); //error
		Ret_ProcessedFiles.setData(0,"FileErrorTxt",strErr);
		return;
	}

	DbRecordSet lstDetailsData;
	file.Load(Ret_pStatus, strDetailsFile, lstDetailsData, QStringList(), QStringList(), true);
	if(!Ret_pStatus.IsOK())
	{
		Ret_pStatus.setError(StatusCodeSet::ERR_BUS_IMPORT_SKIPPED); //B.T. 2013-06-26: when files are imported on file change..ignore this message
		return;


		QString strErr=Ret_pStatus.getErrorText();
		Ret_pStatus.setError(1, strErr);
		Ret_ProcessedFiles.setData(0,"FileName",strDetailsFile);
		Ret_ProcessedFiles.setData(0,"FileOK",1); //error
		Ret_ProcessedFiles.setData(0,"FileErrorTxt",strErr);
		return;
	}

	nColCnt = lstDetailsData.getColumnCount();
	if(nColCnt != 9){
		QString strErr=QString("Invalid column count in details file (%1 instead of 9): %2").arg(nColCnt).arg(strDetailsFile);
		Ret_pStatus.setError(1, strErr);
		Ret_ProcessedFiles.setData(0,"FileName",strDetailsFile);
		Ret_ProcessedFiles.setData(0,"FileOK",1); //error
		Ret_ProcessedFiles.setData(0,"FileErrorTxt",strErr);
		return;
	}

	//STEP 2: write header data

	//SPL Main Data: AKA "header" file import is defined by MB like this:
	//PL_CODE			Code		Mostly, this is a name. Spaces etc. are allowed
	//PL_NAME						Mostly empty, unless the list was defined in Timesheet
	//PL_PERSON			Pers.No.	If a pers.no. is defined, only this user can see this SPL. It is hidden for all others.
	//PL_ABTEILUNG		Department	If a pers.no. is defined, only users from this department or subdepartments of it can see this SPL. It is hidden for all others.
	//PL_INKL_SEL		1: Is Selection
	//PL_SEL_KRIT		Selection criteria	XML format, to be converted to SQL when needed.
	//PL_SAMMELLISTE	1: Is Filter
	//PL_LST_SUBLISTS	List of sub-SPLs	XML format
	//-------------------------------------------------------------------------------
	//This input is to be stored into BUS_STORED_PROJECT_LIST as follows:
	//
	//BSPL_NAME -> trimmed(PL_CODE +" "+ PL_NAME)  /* PL_NAME je najcesce prazan */
	//BSPL_USEBY_PERSON_ID -> PL_PERSON (search BUS_PERSON by BPER_CODE = PL_PERSON) if not found then set NULL, but report error on console (qdebug)
	//BSPL_USEBY_DEPT_ID -> PL_ABTEILUNG (search BUS_DEPARTMENTS by BDEPT_CODE = PL_ABTEILUNG) if not found then set NULL
	//BSPL_IS_SELECTION -> PL_INKL_SEL
	//BSPL_SELECTION_DATA -> PL_SEL_KRIT
	//BSPL_IS_FILTER -> PL_SAMMELLISTE
	//BSPL_FILTER_DATA -> PL_LST_SUBLISTS

	DbRecordSet lstData;
	lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_STORED_PROJECT_LIST));

	SimpleTableCache PersonCache;
	PersonCache.Load("BUS_PERSON", "BPER_CODE", "BPER_ID", Ret_pStatus, GetDbManager());

	SimpleTableCache DepartmentCache;
	DepartmentCache.Load("BUS_DEPARTMENTS", "BDEPT_CODE", "BDEPT_ID", Ret_pStatus, GetDbManager());

	//get currently available lists to prevent duplicate names	
	DbRecordSet recLists;
	g_BusinessServiceSet->StoredProjectLists->GetListNames(Ret_pStatus, recLists);

	int nRowCnt = lstHeaderData.getRowCount();
	int i;
	int nSkipped = 0;
	for(i=0; i<nRowCnt; i++)
	{
		//TOFIX set BSPL_OWNER to some value?
		QString strName = lstHeaderData.getDataRef(i, 0).toString();
		if(!lstHeaderData.getDataRef(i, 1).toString().isEmpty()){
			strName += " ";
			strName += lstHeaderData.getDataRef(i, 1).toString();
		}
		strName = strName.trimmed();	//trimmed(PL_CODE +" "+ PL_NAME)
		if(strName.isEmpty()){
			Ret_pErrors.addRow();
			Ret_pErrors.setData(Ret_pErrors.getRowCount()-1,"FileName",strHeaderFile);
			Ret_pErrors.setData(Ret_pErrors.getRowCount()-1,"RecIdx",i);
			Ret_pErrors.setData(Ret_pErrors.getRowCount()-1,"ErrText","Empty code + name");

			nSkipped ++;
			continue;
		}

		int nPersonID = -1;	//search BUS_PERSON by BPER_CODE = PL_PERSON
		QString strPersonCode = lstHeaderData.getDataRef(i, 2).toString();
		if(!strPersonCode.isEmpty()){
			nPersonID = PersonCache.GetValue(strPersonCode).toInt();
			if (nPersonID < 1)
			{
				Ret_pErrors.addRow();
				Ret_pErrors.setData(Ret_pErrors.getRowCount()-1,"FileName",strHeaderFile);
				Ret_pErrors.setData(Ret_pErrors.getRowCount()-1,"RecIdx",i);
				Ret_pErrors.setData(Ret_pErrors.getRowCount()-1,"ErrText",QString("Could not find person id with code: " +  strPersonCode));
				
				nSkipped ++;
				continue;
			}
		}
		int nDepartmentID = -1;
		QString strDepartmentCode = lstHeaderData.getDataRef(i, 3).toString();
		if(!strDepartmentCode.isEmpty()){
			nDepartmentID = DepartmentCache.GetValue(strDepartmentCode).toInt();
			if (nDepartmentID < 1)
			{
				Ret_pErrors.addRow();
				Ret_pErrors.setData(Ret_pErrors.getRowCount()-1,"FileName",strHeaderFile);
				Ret_pErrors.setData(Ret_pErrors.getRowCount()-1,"RecIdx",i);
				Ret_pErrors.setData(Ret_pErrors.getRowCount()-1,"ErrText",QString("Could not find department id with code: " +  strDepartmentCode));

				nSkipped ++;
				continue;
			}
		}

		lstData.addRow();
		int nIdx = i-nSkipped;

		//#2693: if project list with this name already exists, overwrite it (reusing the ID of existing list)
		int nRow = recLists.find("BSPL_NAME", strName, true);
		if(nRow >= 0){
			int nTreeID = recLists.getDataRef(nRow, "BSPL_ID").toInt();
			lstData.setData(nIdx, "BSPL_ID", nTreeID);

			//delete previous list content
			DbSqlQuery query(Ret_pStatus, GetDbManager());
			QString strSql = QString("DELETE FROM BUS_STORED_PROJECT_LIST_DATA WHERE BSPLD_TREE_ID=%1").arg(nTreeID);
			query.Execute(Ret_pStatus, strSql);
		}

		lstData.setData(nIdx, "BSPL_NAME",				strName);
		if (nPersonID > 0)			lstData.setData(nIdx, "BSPL_USEBY_PERSON_ID",	nPersonID);
		if (nDepartmentID > 0)		lstData.setData(nIdx, "BSPL_USEBY_DEPT_ID",	nDepartmentID);
		lstData.setData(nIdx, "BSPL_IS_SELECTION",		lstHeaderData.getDataRef(i, 4).toInt());
		lstData.setData(nIdx, "BSPL_SELECTION_DATA",	lstHeaderData.getDataRef(i, 5).toString());
		lstData.setData(nIdx, "BSPL_IS_FILTER",			lstHeaderData.getDataRef(i, 6).toInt());
		lstData.setData(nIdx, "BSPL_FILTER_DATA",		lstHeaderData.getDataRef(i, 7).toString());
	}

	//write data into the database
	DbSimpleOrm TableOrm(Ret_pStatus,BUS_STORED_PROJECT_LIST,GetDbManager());
	if(!Ret_pStatus.IsOK())
	{
		QString strErr=Ret_pStatus.getErrorText();
		Ret_pStatus.setError(1, strErr);
		Ret_ProcessedFiles.setData(0,"FileName",strHeaderFile);
		Ret_ProcessedFiles.setData(0,"FileOK",1); //error
		Ret_ProcessedFiles.setData(0,"FileErrorTxt",strErr);
		return;
	}

	TableOrm.Write(Ret_pStatus,lstData);
	if(!Ret_pStatus.IsOK())
	{
		QString strErr=Ret_pStatus.getErrorText();
		Ret_pStatus.setError(1, strErr);
		Ret_ProcessedFiles.setData(0,"FileName",strHeaderFile);
		Ret_ProcessedFiles.setData(0,"FileOK",1); //error
		Ret_ProcessedFiles.setData(0,"FileErrorTxt",strErr);
		return;
	}

	int nRowSuccessHeader=nRowCnt-nSkipped;
	int nRowFailedHeader=nSkipped;

	//STEP 3: write details data

	//SPL Details: AKA "details" file import is defined by MB like this:
	//PL_CODE			Code		Mostly, this is a name. Spaces etc. are allowed
	//PL_NAME						Mostly empty, unless the list was defined in Timesheet
	//PR_CODE			Project Code
	//PR_AKTIV			0=not active 1=active	Currently used to mark inactive projects in the tree
	//T_LIST_ELEM_STATE	0=no subelements 1=Collapsed (+) 2=Expanded (-)
	//PR_STUFE			Level					Starts with 0
	//PR_EX_UNTERPROJ	0=no subelements in DB	1=subelements exist in DB
	//T_MAIN_NODE		Project code of parent node
	//PR_STYLE			Styling					Used to display icons etc. depending on project type (not yet used in SPC-Q7)
	//----------------------------------------------------------------------------
	//These records are copied directly to BUS_STORED_PROJECT_LIST_DATA with following algorithm
	//PR_CODE -> BSPLD_PROJECT_ID (find BUSP_ID by PR_CODE in BUS_PROJECT table and set it here)
	//T_LIST_ELEM_STATE-> BSPLD_EXPANDED

	lstData.destroy();
	lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_STORED_PROJECT_LIST_DATA));

	SimpleTableCache ProjectListCache;
	ProjectListCache.Load("BUS_STORED_PROJECT_LIST", "BSPL_NAME", "BSPL_ID", Ret_pStatus, GetDbManager());

	SimpleTableCache ProjectCache;
	ProjectCache.Load("BUS_PROJECTS", "BUSP_CODE", "BUSP_ID", Ret_pStatus, GetDbManager());

	nSkipped = 0;
	nRowCnt = lstDetailsData.getRowCount();

	for(int z=0; z<nRowCnt; z++)
	{
		QString strName = lstDetailsData.getDataRef(z, 0).toString();	
		if(!lstDetailsData.getDataRef(z, 1).toString().isEmpty()){
			strName += " ";
			strName += lstDetailsData.getDataRef(z, 1).toString();
		}
		strName = strName.trimmed();	//trimmed(PL_CODE +" "+ PL_NAME)
		if(strName.isEmpty()){
			Ret_pErrors.addRow();
			Ret_pErrors.setData(Ret_pErrors.getRowCount()-1,"FileName",strDetailsFile);
			Ret_pErrors.setData(Ret_pErrors.getRowCount()-1,"RecIdx",z);
			Ret_pErrors.setData(Ret_pErrors.getRowCount()-1,"ErrText",QString("Empty code + name"));
			nSkipped ++;
			continue;
		}

		//search project list ID by its name
		QString strProjListName = strName;
		int nTreeID = ProjectListCache.GetValue(strProjListName).toInt();
		if (nTreeID < 1){
			Ret_pErrors.addRow();
			Ret_pErrors.setData(Ret_pErrors.getRowCount()-1,"FileName",strDetailsFile);
			Ret_pErrors.setData(Ret_pErrors.getRowCount()-1,"RecIdx",z);
			Ret_pErrors.setData(Ret_pErrors.getRowCount()-1,"ErrText",QString("(project list not found using name: [%1])").arg(strProjListName));
			nSkipped ++;
			continue;
		}

		int nProjectID = -1;
		QString strProjectCode = lstDetailsData.getDataRef(z, 2).toString();
		if(!strProjectCode.isEmpty()){
			nProjectID = ProjectCache.GetValue(strProjectCode).toInt();
		}
		if (nProjectID < 1){
			Ret_pErrors.addRow();
			Ret_pErrors.setData(Ret_pErrors.getRowCount()-1,"FileName",strDetailsFile);
			Ret_pErrors.setData(Ret_pErrors.getRowCount()-1,"RecIdx",z);
			Ret_pErrors.setData(Ret_pErrors.getRowCount()-1,"ErrText",QString("(project not found using code: [%1])").arg(strProjectCode));
			/*
			//debug code to dump project cache to file
			if(!QFile::exists("D:\\aaa.uuu")){
				FILE *pOut = fopen("D:\\aaa.uuu", "w");
				if(pOut){
					int nCnt = ProjectCache.GetSet().getRowCount();
					for(int i=0; i<nCnt; i++){
						fprintf(pOut, "%s:%s\n", 
							ProjectCache.GetSet().getDataRef(i, 0).toString().toLocal8Bit().constData(),
							ProjectCache.GetSet().getDataRef(i, 1).toString().toLocal8Bit().constData());
					}
					fclose(pOut);
				}
			}
			*/
			nSkipped ++;
			continue;
		}

		DbSimpleOrm TableOrm1(Ret_pStatus,BUS_STORED_PROJECT_LIST_DATA,GetDbManager());
		if(!Ret_pStatus.IsOK())
		{
			QString strErr=Ret_pStatus.getErrorText();
			Ret_pStatus.setError(1, strErr);
			Ret_ProcessedFiles.setData(0,"FileName",strDetailsFile);
			Ret_ProcessedFiles.setData(0,"FileOK",1); //error
			Ret_ProcessedFiles.setData(0,"FileErrorTxt",strErr);
			return;
		}

		//if this exact project already exists in that list, overwrite it
		DbRecordSet lstExisting;
		TableOrm1.Read(Ret_pStatus, lstExisting, QString("WHERE BSPLD_TREE_ID=%1 AND BSPLD_PROJECT_ID=%2").arg(nTreeID).arg(nProjectID), DbSqlTableView::TVIEW_BUS_STORED_PROJECT_LIST_DATA);
		if(!Ret_pStatus.IsOK())
		{
			QString strErr=Ret_pStatus.getErrorText();
			Ret_pStatus.setError(1, strErr);
			Ret_ProcessedFiles.setData(0,"FileName",strDetailsFile);
			Ret_ProcessedFiles.setData(0,"FileOK",1); //error
			Ret_ProcessedFiles.setData(0,"FileErrorTxt",strErr);
			return;
		}
		int nID = -1;
		int nCnt = lstExisting.getRowCount();
		if(nCnt>0){
			nID = lstExisting.getDataRef(0, "BSPLD_ID").toInt();

			//if duplicates exist (there were no checks before in the code), delete all except the first item
			if(nCnt > 1){
				DbSqlQuery query(Ret_pStatus, GetDbManager());
				if(Ret_pStatus.IsOK()){ 
					for(int i=1; i<nCnt; i++){
						int nRowID = lstExisting.getDataRef(i, "BSPLD_ID").toInt();
						QString strSql = QString("DELETE FROM BUS_STORED_PROJECT_LIST_DATA WHERE BSPLD_ID=%1").arg(nRowID);
						query.Execute(Ret_pStatus, strSql);
					}
				}
			}
		}

		lstData.addRow();

		int nIdx = lstData.getRowCount()-1;
		if(nID > 0) lstData.setData(nIdx, "BSPLD_ID", nID);		//overwrites existing item
		lstData.setData(nIdx, "BSPLD_TREE_ID",		nTreeID);
		lstData.setData(nIdx, "BSPLD_PROJECT_ID",	nProjectID);
		lstData.setData(nIdx, "BSPLD_EXPANDED",		lstDetailsData.getDataRef(z, 4).toInt());

		//B.T: breaks on large import:
		//go by chunks of 10000
		if (nIdx==10000 || z==nRowCnt-1)
		{
			//write data into the database
			TableOrm1.Write(Ret_pStatus,lstData);
			if(!Ret_pStatus.IsOK())
			{
				QString strErr=Ret_pStatus.getErrorText();
				Ret_pStatus.setError(1, strErr);
				Ret_ProcessedFiles.setData(0,"FileName",strDetailsFile);
				Ret_ProcessedFiles.setData(0,"FileOK",1); //error
				Ret_ProcessedFiles.setData(0,"FileErrorTxt",strErr);
				return;
			}

			lstData.clear();
		}
	}

	//any remaining data
	if (lstData.getRowCount() > 0)
	{
		//write data into the database
		DbSimpleOrm TableOrm1(Ret_pStatus,BUS_STORED_PROJECT_LIST_DATA,GetDbManager());
		if(!Ret_pStatus.IsOK())
		{
			QString strErr=Ret_pStatus.getErrorText();
			Ret_pStatus.setError(1, strErr);
			Ret_ProcessedFiles.setData(0,"FileName",strDetailsFile);
			Ret_ProcessedFiles.setData(0,"FileOK",1); //error
			Ret_ProcessedFiles.setData(0,"FileErrorTxt",strErr);
			return;
		}
		TableOrm1.Write(Ret_pStatus,lstData);
		if(!Ret_pStatus.IsOK())
		{
			QString strErr=Ret_pStatus.getErrorText();
			Ret_pStatus.setError(1, strErr);
			Ret_ProcessedFiles.setData(0,"FileName",strDetailsFile);
			Ret_ProcessedFiles.setData(0,"FileOK",1); //error
			Ret_ProcessedFiles.setData(0,"FileErrorTxt",strErr);
			return;
		}
		lstData.clear();
	}

	//set all status
	//Ret_ProcessedFiles.addRow();
	Ret_ProcessedFiles.setData(Ret_ProcessedFiles.getRowCount()-1, "FileName", strHeaderFile);
	Ret_ProcessedFiles.setData(Ret_ProcessedFiles.getRowCount()-1, "FileOK", 0);
	Ret_ProcessedFiles.setData(Ret_ProcessedFiles.getRowCount()-1, "NumRecsSuccess", nRowSuccessHeader);
	Ret_ProcessedFiles.setData(Ret_ProcessedFiles.getRowCount()-1, "NumRecsFailed", nRowFailedHeader);

	//rename file - change extension to indicate file was processed
	g_pImportExportManager->DisableFileWatcher();
	QFileInfo info(strHeaderFile);
	QString strNewFile = strDirPath + info.baseName() + "." + strFileExtOfProcessed;
	QFile::remove(strNewFile);
	QFile::rename(strHeaderFile, strNewFile);
	g_pImportExportManager->EnableFileWatcher();

	Ret_ProcessedFiles.addRow();
	Ret_ProcessedFiles.setData(Ret_ProcessedFiles.getRowCount()-1, "FileName", strDetailsFile);
	Ret_ProcessedFiles.setData(Ret_ProcessedFiles.getRowCount()-1, "FileOK", 0);
	Ret_ProcessedFiles.setData(Ret_ProcessedFiles.getRowCount()-1, "NumRecsSuccess", nRowCnt-nSkipped);
	Ret_ProcessedFiles.setData(Ret_ProcessedFiles.getRowCount()-1, "NumRecsFailed", nSkipped);

	//rename file - change extension to indicate file was processed
	g_pImportExportManager->DisableFileWatcher();
	QFileInfo info2(strDetailsFile);
	strNewFile = strDirPath + info2.baseName() + "." + strFileExtOfProcessed;
	QFile::remove(strNewFile);
	QFile::rename(strDetailsFile, strNewFile);
	g_pImportExportManager->EnableFileWatcher();

	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("Importing stored project list finished successfully"));
}

void Service_BusImport::GenerateRoleAndUserFromPerson(Status &Ret_pStatus, int nRoleID, DbRecordSet &lstWrite, DbSimpleOrm &orm1, bool bForceLoginUpdate)
{
	//only when role specified
	if(nRoleID > 0)
	{
		//add role if defined, and not already set for this person, add it
		int nPersonID = lstWrite.getDataRef(0, "BPER_ID").toInt();
		Q_ASSERT(nPersonID > 0);
		DbRecordSet lstAssignedRoles;
		g_BusinessServiceSet->AccessRights->GetPersonRoleList(Ret_pStatus, lstAssignedRoles, nPersonID);
		if (!Ret_pStatus.IsOK())
			return;
		if(0 == lstAssignedRoles.getRowCount()){
			g_BusinessServiceSet->AccessRights->InsertNewRoleToPerson(Ret_pStatus, nPersonID, nRoleID);
			if (!Ret_pStatus.IsOK())
				return;
		}

		//generate new user for this record if needed
		//STEP1: check if this person already has an user record
		int nUserID = -1;
		DbSqlQuery query(Ret_pStatus,GetDbManager());
		QString strSql = QString("SELECT CUSR_ID FROM CORE_USER WHERE CUSR_PERSON_ID=%1").arg(nPersonID);
		query.Prepare(Ret_pStatus, strSql, true);
		if(!Ret_pStatus.IsOK())
			return;
		if(query.ExecutePrepared(Ret_pStatus)){
			if (query.next()){
				nUserID = query.value(0).toInt();
				if(!bForceLoginUpdate)
					return;	// this person already has a user record
			}
		}

		//STEP2: generate unique login from the visum
		int nSuffix = -1;
		QString strLogin = lstWrite.getDataRef(0, "BPER_INITIALS").toString();
		if(strLogin.isEmpty())
			return;	//can not create user for person with no visum
		strSql = QString("SELECT CUSR_USERNAME FROM CORE_USER WHERE CUSR_USERNAME LIKE '%1%' AND NOT CUSR_PERSON_ID=%2 ORDER BY CUSR_USERNAME").arg(strLogin).arg(nPersonID);
		query.Prepare(Ret_pStatus, strSql, true);
		if(!Ret_pStatus.IsOK())
			return;
		if(query.ExecutePrepared(Ret_pStatus)){
			while (query.next()){
				QString strLastLogin = query.value(0).toString();
				QString strSection = strLastLogin.mid(strLogin.size());
				if(strSection.isEmpty()){
					//login exists as exact match
					if(nSuffix < 0) nSuffix = 0;
				}
				else{
					bool bOk = false;
					int nSection = strSection.toInt(&bOk);
					if(bOk){
						if(nSection > nSuffix) nSuffix = nSection;
					}
				}
			}
		}
		//increment section if needed
		if(nSuffix >= 0){
			nSuffix ++;
			strLogin += QString("%1").arg(nSuffix);
		}

		//STEP3: generate CORE_USER login record
		DbRecordSet rowUser;
		rowUser.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CORE_USER));
		rowUser.addRow();
		if(nUserID > 0){
			rowUser.setData(0, "CUSR_ID",	nUserID);	//update mode, see bForceLoginUpdate
		}
		rowUser.setData(0, "CUSR_FIRST_NAME",	lstWrite.getDataRef(0, "BPER_FIRST_NAME").toString());
		rowUser.setData(0, "CUSR_LAST_NAME",	lstWrite.getDataRef(0, "BPER_LAST_NAME").toString());
		rowUser.setData(0, "CUSR_USERNAME",		strLogin);
		rowUser.setData(0, "CUSR_ACTIVE_FLAG",	1);
		rowUser.setData(0, "CUSR_PERSON_ID",	nPersonID);

		//B.T. 23.04.2013. Miro u naughty, naughty boy, leaving password field = NULL is not right way. Special procedure must be executed to set empty password, watch and learn:
		QByteArray pass=Authenticator::GeneratePassword(strLogin,"");
		rowUser.setData(0,"CUSR_PASSWORD",pass);

		orm1.Write(Ret_pStatus, rowUser);
	}
}

//26.03.2013: B.T. for new V3 project import format: based on code fields in the rowImport, check if record already exists in table, if not create new one (just code) and set id back
void Service_BusImport::UpdateProjectSPCFields(Status &Ret_pStatus,DbSqlQuery &query,DbRecordSet &rowWrite, DbRecordSet rowImport)
{
	UpdateProjectSPCField_One(Ret_pStatus,query,rowWrite,rowImport,"BUSP_STATUS_CODE","BUSP_STATUS_ID","SFPS_ID","SFPS_CODE",SPC_PR_STATUS,false);
	if (!Ret_pStatus.IsOK())
		return;
	UpdateProjectSPCField_One(Ret_pStatus,query,rowWrite,rowImport,"BUSP_AREA_CODE","BUSP_AREA_ID","SEPB_ID","SEPB_CODE",SPC_PR_AREA,true);
	if (!Ret_pStatus.IsOK())
		return;
	UpdateProjectSPCField_One(Ret_pStatus,query,rowWrite,rowImport,"BUSP_SECTOR_CODE","BUSP_SECTOR_ID","SEFG_ID","SEFG_CODE",SPC_SECTOR,true);
	if (!Ret_pStatus.IsOK())
		return;
	UpdateProjectSPCField_One(Ret_pStatus,query,rowWrite,rowImport,"BUSP_INDUSTRY_CODE","BUSP_INDUSTRY_ID","SEWG_ID","SEWG_CODE",SPC_INDUSTRY_GROUP,true);
	if (!Ret_pStatus.IsOK())
		return;
	UpdateProjectSPCField_One(Ret_pStatus,query,rowWrite,rowImport,"BUSP_SERVICE_TYPE_CODE","BUSP_SERVICE_TYPE_ID","SELA_ID","SELA_CODE",SPC_SERVICE_TYPE,true);
	if (!Ret_pStatus.IsOK())
		return;
	UpdateProjectSPCField_One(Ret_pStatus,query,rowWrite,rowImport,"BUSP_PROCESS_CODE","BUSP_PROCESS_ID","SEPZ_ID","SEPZ_CODE",SPC_PROCESS,true);
	if (!Ret_pStatus.IsOK())
		return;
	UpdateProjectSPCField_One(Ret_pStatus,query,rowWrite,rowImport,"BUSP_RATE_CAT_CODE","BUSP_RATE_CAT_ID","SETK_ID","SETK_CODE",SPC_RATE_CAT,false);
	if (!Ret_pStatus.IsOK())
		return;
	UpdateProjectSPCField_One(Ret_pStatus,query,rowWrite,rowImport,"BUSP_EXP_RATE_CAT_CODE","BUSP_EXP_RATE_CAT_ID","SETK_ID","SETK_CODE",SPC_RATE_CAT,false);
	if (!Ret_pStatus.IsOK())
		return;
	UpdateProjectSPCField_One(Ret_pStatus,query,rowWrite,rowImport,"BUSP_PLAN_COST_CAT_CODE","BUSP_PLAN_COST_CAT_ID","SETK_ID","SETK_CODE",SPC_RATE_CAT,false);
	if (!Ret_pStatus.IsOK())
		return;
	UpdateProjectSPCField_One(Ret_pStatus,query,rowWrite,rowImport,"BUSP_TYPE_CODE","BUSP_TYPE_ID","SEAP_ID","SEAP_CODE",SPC_PR_TYPE,true);
	if (!Ret_pStatus.IsOK())
		return;
	UpdateProjectSPCField_One(Ret_pStatus,query,rowWrite,rowImport,"BUSP_DEF_ACTIVITY_CODE","BUSP_DEF_ACTIVITY_ID","SET_ID","SET_CODE",SPC_ACTIVITY,true);
	if (!Ret_pStatus.IsOK())
		return;
}



void Service_BusImport::UpdateProjectSPCField_One(Status &Ret_pStatus,DbSqlQuery &query, DbRecordSet &rowWrite, DbRecordSet &rowImport, 
							   QString strProjCodeColName,
							   QString strProjIDColName,
							   QString strIDColName,
							   QString strCodeColName,
							   int nTableID,
							   bool bIsTreeStructure
							   )
{

	DbTableKeyData table_data;
	DbSqlTableDefinition::GetKeyData(nTableID,table_data);
	int nQueryView=table_data.m_nViewID;
	QString strTableName =table_data.m_strTableName;

	QString strCode = rowImport.getDataRef(0,strProjCodeColName).toString().toLower();
	if (strCode.isEmpty())
		return;

	QString strSQL="SELECT "+strIDColName+" FROM "+strTableName+" WHERE LOWER("+strCodeColName+") = ?";
	query.Prepare(Ret_pStatus,strSQL);
	if (!Ret_pStatus.IsOK())
		return;
	query.bindValue(0,strCode);
	query.ExecutePrepared(Ret_pStatus);
	if (!Ret_pStatus.IsOK())
		return;

	int nID=0;
	if (query.next())
	{
		nID=query.value(0).toInt();
	}
	if (nID==0)
	{
		if (bIsTreeStructure)
		{
			DbRecordSet lstWrite;
			lstWrite.defineFromView(DbSqlTableView::getView(nQueryView));
			lstWrite.addRow();
			lstWrite.setData(0,strCodeColName,strCode);	

			HierarchicalData HierarchicalWriter;
			HierarchicalWriter.Initialize(nTableID,GetDbManager());
			HierarchicalWriter.Write(Ret_pStatus,lstWrite);
			if (!Ret_pStatus.IsOK())
				return;

			//lstWrite.Dump();
			nID=lstWrite.getDataRef(0,strIDColName).toInt();
		}
		else
		{
			QString strColName=strCodeColName.left(strCodeColName.indexOf("_"))+"_NAME";

			strSQL="INSERT INTO "+strTableName+" ("+strCodeColName+","+strColName+") VALUES(?,'')";
			query.Prepare(Ret_pStatus,strSQL);
			if (!Ret_pStatus.IsOK())
				return;
			query.bindValue(0,strCode);
			query.ExecutePrepared(Ret_pStatus);
			if (!Ret_pStatus.IsOK())
				return;

			nID=query.GetLastInsertedID(Ret_pStatus,strIDColName);
		}
	}

	rowWrite.setData(0,strProjIDColName,nID);
}




int Service_BusImport::GetDefaultOrganization(Status &Ret_pStatus)
{
	DbSimpleOrm OPTIONS_SETTINGS_ORM(Ret_pStatus, BUS_OPT_SETTINGS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return 0;

	DbRecordSet recOpt;
	QString strWhere = "WHERE BOUS_PERSON_ID IS NULL AND BOUS_SETTING_ID = " + QString::number(APP_DEFAULT_ORGANIZATION_ID);
	OPTIONS_SETTINGS_ORM.Read(Ret_pStatus, recOpt, strWhere);
	if(!Ret_pStatus.IsOK()) return 0;

	if (recOpt.getRowCount()>0)
	{
		return recOpt.getDataRef(0,"BOUS_VALUE").toInt();
	}
	return 0;
}

void Service_BusImport::ImportContactCustomData(Status &Ret_pStatus, QString strCustomFieldName, int nTableID, DbRecordSet &RetOut_Data)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm orm(Ret_pStatus, BUS_CM_CONTACT, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	DbSimpleOrm ormBOOL(Ret_pStatus, BUS_CUSTOM_BOOL, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	DbSimpleOrm ormDATE(Ret_pStatus, BUS_CUSTOM_DATE, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	DbSimpleOrm ormDATETIME(Ret_pStatus, BUS_CUSTOM_DATETIME, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	DbSimpleOrm ormFLOAT(Ret_pStatus, BUS_CUSTOM_FLOAT, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	DbSimpleOrm ormINT(Ret_pStatus, BUS_CUSTOM_INT, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	DbSimpleOrm ormLONGTEXT(Ret_pStatus, BUS_CUSTOM_LONGTEXT, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	DbSimpleOrm ormTEXT(Ret_pStatus, BUS_CUSTOM_TEXT, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	
	DbSqlQuery &query = *orm.GetDbSqlQuery();
	if (RetOut_Data.getRowCount()==0)
		return;


	//---------------------------------------------------
	//			FIND CUSTOM FLD
	//---------------------------------------------------

	//get custom field by name and table id:
	query.Prepare(Ret_pStatus,"SELECT BCF_ID,BCF_DATA_TYPE FROM BUS_CUSTOM_FIELDS WHERE LOWER(BCF_NAME)=? AND BCF_TABLE_ID=?");
	if(!Ret_pStatus.IsOK()) return;

	query.bindValue(0,strCustomFieldName.toLower());
	query.bindValue(1,nTableID);

	query.ExecutePrepared(Ret_pStatus);
	if(!Ret_pStatus.IsOK()) return;

	int nCustomFld_ID=0;
	int nCustomFld_Type=0;
	if (query.next())
	{
		nCustomFld_ID=query.value(0).toInt();
		nCustomFld_Type=query.value(1).toInt();
	}
	//if custom fld is not found
	if (nCustomFld_Type==0 || nCustomFld_ID==0)
	{
		RetOut_Data.setColValue("STATUS_CODE",2); //custom fld not found
		return;
	}
	
	//---------------------------------------------------
	//			DEFINE LISTS
	//---------------------------------------------------

	DbRecordSet lstDataForWrite;
	switch (nCustomFld_Type)
	{
	case GlobalConstants::TYPE_CUSTOM_DATA_BOOL:
		lstDataForWrite.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CUSTOM_BOOL));
		break;
	case GlobalConstants::TYPE_CUSTOM_DATA_INT:
		lstDataForWrite.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CUSTOM_INT));
		break;
	case GlobalConstants::TYPE_CUSTOM_DATA_FLOAT:
		lstDataForWrite.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CUSTOM_FLOAT));
		break;
	case GlobalConstants::TYPE_CUSTOM_DATA_TEXT:
		lstDataForWrite.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CUSTOM_TEXT));
		break;
	case GlobalConstants::TYPE_CUSTOM_DATA_LONGTEXT:
		lstDataForWrite.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CUSTOM_LONGTEXT));
		break;
	case GlobalConstants::TYPE_CUSTOM_DATA_DATE:
		lstDataForWrite.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CUSTOM_DATE));
		break;
	case GlobalConstants::TYPE_CUSTOM_DATA_DATETIME:
		lstDataForWrite.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CUSTOM_DATETIME));
		break;
	default:
		{
			RetOut_Data.setColValue("STATUS_CODE",3); //wrong data type
			return;
		}
	}
	lstDataForWrite.addColumn(QVariant::Int,"IDX"); //for sync
	RetOut_Data.setColValue("STATUS_CODE",0); //reset status code to 0

	//---------------------------------------------------
	//			FIND CONTACT RECORD AND FILL DATA
	//---------------------------------------------------
		
	int nCnt = RetOut_Data.getRowCount();
	for(int i=0; i<nCnt; i++)
	{
		//STEP 1: find contact ID
		//contact must match name, last name and organization
		//Quote:
		//To find a suitable contact, search for identical First Name, Last Name and Organization. The import
		//format one First Name column only and no Middle Name (this is typical). We do have a middle
		//name field which may be used or not. To be sure to find idenditcal contacts, the First Name column
		//from the import file must be the same as
		//- the First Name field in the contact record, if Middle Name is empty, or
		//- First Name + space + Middle Name in the contact record, if the Middle Name is not empty.
		int nContactID = -1;
		QString strContactFirstName = RetOut_Data.getDataRef(i, "BCNT_FIRSTNAME").toString();
		QString strContactLastName = RetOut_Data.getDataRef(i, "BCNT_LASTNAME").toString();
		QString strContactOrganization = RetOut_Data.getDataRef(i, "BCNT_ORGANIZATIONNAME").toString();
		DbRecordSet resultIDs;
		
		Status status;
		QString strQuery = "SELECT BCNT_ID FROM BUS_CM_CONTACT WHERE ";
		if(strContactOrganization.isEmpty())
			strQuery += "(BCNT_ORGANIZATIONNAME IS NULL OR BCNT_ORGANIZATIONNAME='')";
		else
			strQuery += "BCNT_ORGANIZATIONNAME=?";
		if(strContactLastName.isEmpty())
			strQuery += " AND (BCNT_LASTNAME IS NULL OR BCNT_LASTNAME='')";
		else
			strQuery += " AND BCNT_LASTNAME=?";
		if(strContactFirstName.isEmpty())
			strQuery += " AND (BCNT_FIRSTNAME IS NULL OR BCNT_FIRSTNAME='')";
		else{
			strQuery += " AND ((BCNT_FIRSTNAME=? AND (BCNT_MIDDLENAME IS NULL OR BCNT_MIDDLENAME='')) OR ((BCNT_FIRSTNAME || ' ' || BCNT_MIDDLENAME)=?))";
		}

		query.Prepare(status, strQuery);
		if(status.IsOK())
		{
			int nFieldIdx = 0;
			if(!strContactOrganization.isEmpty()){
				query.bindValue(nFieldIdx, strContactOrganization);
				nFieldIdx ++;
			}
			if(!strContactLastName.isEmpty()){
				query.bindValue(nFieldIdx, strContactLastName);
				nFieldIdx ++;
			}
			if(!strContactFirstName.isEmpty()){
				query.bindValue(nFieldIdx, strContactFirstName);
				nFieldIdx ++;
				query.bindValue(nFieldIdx, strContactFirstName);
				nFieldIdx ++;
			}

			query.ExecutePrepared(status);
			if(status.IsOK()){
				query.FetchData(resultIDs);
				if(resultIDs.getRowCount() > 0){
					nContactID = resultIDs.getDataRef(0, 0).toInt();
				}
			}
		}
		//check SQL error
		if (!status.IsOK())
		{
			RetOut_Data.setData(i,"STATUS_CODE",4);
			RetOut_Data.setData(i,"STATUS_TEXT",status.getErrorText());
			continue;
		}
		//check contact id:
		if(nContactID <= 0)
		{
			RetOut_Data.setData(i,"STATUS_CODE",1);
			RetOut_Data.setData(i,"STATUS_TEXT",QString(""));
			continue;
		}

		//now find existing record based on contactid, fld_id, table_id:
		
		

		//now we must pick table based on data type and insert value:
		QString strValue = RetOut_Data.getDataRef(i,"BCF_VALUE").toString();
		lstDataForWrite.addRow();
		int nRow= lstDataForWrite.getRowCount()-1;
		lstDataForWrite.setData(nRow,"IDX",i);

		DbSqlQuery *query = orm.GetDbSqlQuery();
		
		switch (nCustomFld_Type)
		{
		case GlobalConstants::TYPE_CUSTOM_DATA_BOOL:
			{
				//get existing value and overwrite:
				strQuery = "SELECT BCB_ID FROM BUS_CUSTOM_BOOL WHERE BCB_CUSTOM_FIELD_ID= "+QString::number(nCustomFld_ID)+" AND BCB_TABLE_ID="+QString::number(nTableID)+" AND BCB_RECORD_ID="+QString::number(nContactID);
				query->Execute(status,strQuery);
				if (!status.IsOK())
				{
					RetOut_Data.setData(i,"STATUS_CODE",4);
					RetOut_Data.setData(i,"STATUS_TEXT",status.getErrorText());
					continue;
				}
				int nID=0;
				if (query->next())
				{
					nID=query->value(0).toInt();
					lstDataForWrite.setData(nRow,"BCB_ID",nID);
				}

				// value of True is accepted if one of the following values is imported, and « False » otherwise : 1, True, TRUE, true, Wahr, wahr, WAHR, Yes, YES, Y, y, yes, j, J, Ja, ja, JA, X
				strValue=strValue.toLower();
				if (strValue=="true" || strValue=="1" || strValue=="wahr" || strValue=="yes"  || strValue=="y"  || strValue=="j"   || strValue=="ja" || strValue=="x")
					lstDataForWrite.setData(nRow,"BCB_VALUE",1);
				else
					lstDataForWrite.setData(nRow,"BCB_VALUE",0);
				
				lstDataForWrite.setData(nRow,"BCB_CUSTOM_FIELD_ID",nCustomFld_ID);
				lstDataForWrite.setData(nRow,"BCB_TABLE_ID",nTableID);
				lstDataForWrite.setData(nRow,"BCB_RECORD_ID",nContactID);

			}
			break;
		case GlobalConstants::TYPE_CUSTOM_DATA_INT:
			{
				//get existing value and overwrite:
				strQuery = "SELECT BCI_ID FROM BUS_CUSTOM_INT WHERE BCI_CUSTOM_FIELD_ID= "+QString::number(nCustomFld_ID)+" AND BCI_TABLE_ID="+QString::number(nTableID)+" AND BCI_RECORD_ID="+QString::number(nContactID);
				query->Execute(status,strQuery);
				if (!status.IsOK())
				{
					RetOut_Data.setData(i,"STATUS_CODE",4);
					RetOut_Data.setData(i,"STATUS_TEXT",status.getErrorText());
					continue;
				}
				int nID=0;
				if (query->next())
				{
					nID=query->value(0).toInt();
					lstDataForWrite.setData(nRow,"BCI_ID",nID);
				}

				int nValue = strValue.trimmed().toInt();
				lstDataForWrite.setData(nRow,"BCI_VALUE",nValue);

				lstDataForWrite.setData(nRow,"BCI_CUSTOM_FIELD_ID",nCustomFld_ID);
				lstDataForWrite.setData(nRow,"BCI_TABLE_ID",nTableID);
				lstDataForWrite.setData(nRow,"BCI_RECORD_ID",nContactID);
			}
			break;
		case GlobalConstants::TYPE_CUSTOM_DATA_FLOAT:
			{
				//get existing value and overwrite:
				strQuery = "SELECT BCFL_ID FROM BUS_CUSTOM_FLOAT WHERE BCFL_CUSTOM_FIELD_ID= "+QString::number(nCustomFld_ID)+" AND BCFL_TABLE_ID="+QString::number(nTableID)+" AND BCFL_RECORD_ID="+QString::number(nContactID);
				query->Execute(status,strQuery);
				if (!status.IsOK())
				{
					RetOut_Data.setData(i,"STATUS_CODE",4);
					RetOut_Data.setData(i,"STATUS_TEXT",status.getErrorText());
					continue;
				}
				int nID=0;
				if (query->next())
				{
					nID=query->value(0).toInt();
					lstDataForWrite.setData(nRow,"BCFL_ID",nID);
				}

				int nValue = strValue.trimmed().toInt();
				lstDataForWrite.setData(nRow,"BCFL_VALUE",nValue);
	
				lstDataForWrite.setData(nRow,"BCFL_CUSTOM_FIELD_ID",nCustomFld_ID);
				lstDataForWrite.setData(nRow,"BCFL_TABLE_ID",nTableID);
				lstDataForWrite.setData(nRow,"BCFL_RECORD_ID",nContactID);
			}
			break;
		case GlobalConstants::TYPE_CUSTOM_DATA_TEXT:
			{
				//get existing value and overwrite:
				strQuery = "SELECT BCT_ID FROM BUS_CUSTOM_TEXT WHERE BCT_CUSTOM_FIELD_ID= "+QString::number(nCustomFld_ID)+" AND BCT_TABLE_ID="+QString::number(nTableID)+" AND BCT_RECORD_ID="+QString::number(nContactID);
				query->Execute(status,strQuery);
				if (!status.IsOK())
				{
					RetOut_Data.setData(i,"STATUS_CODE",4);
					RetOut_Data.setData(i,"STATUS_TEXT",status.getErrorText());
					continue;
				}
				int nID=0;
				if (query->next())
				{
					nID=query->value(0).toInt();
					lstDataForWrite.setData(nRow,"BCT_ID",nID);
				}

				lstDataForWrite.setData(nRow,"BCT_VALUE",strValue);
				lstDataForWrite.setData(nRow,"BCT_CUSTOM_FIELD_ID",nCustomFld_ID);
				lstDataForWrite.setData(nRow,"BCT_TABLE_ID",nTableID);
				lstDataForWrite.setData(nRow,"BCT_RECORD_ID",nContactID);
			}
			break;
		case GlobalConstants::TYPE_CUSTOM_DATA_LONGTEXT:
			{
				//get existing value and overwrite:
				strQuery = "SELECT BCLT_ID FROM BUS_CUSTOM_LONGTEXT WHERE BCLT_CUSTOM_FIELD_ID= "+QString::number(nCustomFld_ID)+" AND BCLT_TABLE_ID="+QString::number(nTableID)+" AND BCLT_RECORD_ID="+QString::number(nContactID);
				query->Execute(status,strQuery);
				if (!status.IsOK())
				{
					RetOut_Data.setData(i,"STATUS_CODE",4);
					RetOut_Data.setData(i,"STATUS_TEXT",status.getErrorText());
					continue;
				}
				int nID=0;
				if (query->next())
				{
					nID=query->value(0).toInt();
					lstDataForWrite.setData(nRow,"BCLT_ID",nID);
				}

				lstDataForWrite.setData(nRow,"BCLT_VALUE",strValue);
				lstDataForWrite.setData(nRow,"BCLT_CUSTOM_FIELD_ID",nCustomFld_ID);
				lstDataForWrite.setData(nRow,"BCLT_TABLE_ID",nTableID);
				lstDataForWrite.setData(nRow,"BCLT_RECORD_ID",nContactID);
			}
			break;
		case GlobalConstants::TYPE_CUSTOM_DATA_DATE:
			{
				//get existing value and overwrite:
				strQuery = "SELECT BCD_ID FROM BUS_CUSTOM_DATE WHERE BCD_CUSTOM_FIELD_ID= "+QString::number(nCustomFld_ID)+" AND BCD_TABLE_ID="+QString::number(nTableID)+" AND BCD_RECORD_ID="+QString::number(nContactID);
				query->Execute(status,strQuery);
				if (!status.IsOK())
				{
					RetOut_Data.setData(i,"STATUS_CODE",4);
					RetOut_Data.setData(i,"STATUS_TEXT",status.getErrorText());
					continue;
				}
				int nID=0;
				if (query->next())
				{
					nID=query->value(0).toInt();
					lstDataForWrite.setData(nRow,"BCD_ID",nID);
				}

				QDate datVal=DataHelper::ParseDateString(strValue);
				lstDataForWrite.setData(0,"BCD_VALUE",datVal);
				
				lstDataForWrite.setData(0,"BCD_CUSTOM_FIELD_ID",nCustomFld_ID);
				lstDataForWrite.setData(0,"BCD_TABLE_ID",nTableID);
				lstDataForWrite.setData(0,"BCD_RECORD_ID",nContactID);
			}
			break;
		case GlobalConstants::TYPE_CUSTOM_DATA_DATETIME:
			{
				//get existing value and overwrite:
				strQuery = "SELECT BCDT_ID FROM BUS_CUSTOM_DATETIME WHERE BCDT_CUSTOM_FIELD_ID= "+QString::number(nCustomFld_ID)+" AND BCDT_TABLE_ID="+QString::number(nTableID)+" AND BCDT_RECORD_ID="+QString::number(nContactID);
				query->Execute(status,strQuery);
				if (!status.IsOK())
				{
					RetOut_Data.setData(i,"STATUS_CODE",4);
					RetOut_Data.setData(i,"STATUS_TEXT",status.getErrorText());
					continue;
				}
				int nID=0;
				if (query->next())
				{
					nID=query->value(0).toInt();
					lstDataForWrite.setData(nRow,"BCDT_ID",nID);
				}

				QDateTime datVal=QDateTime(DataHelper::ParseDateString(strValue));
				lstDataForWrite.setData(0,"BCDT_VALUE",datVal);

				lstDataForWrite.setData(0,"BCDT_CUSTOM_FIELD_ID",nCustomFld_ID);
				lstDataForWrite.setData(0,"BCDT_TABLE_ID",nTableID);
				lstDataForWrite.setData(0,"BCDT_RECORD_ID",nContactID);
			}
			break;
		default:
			{
				Q_ASSERT(false); //should not be here
				RetOut_Data.setColValue("STATUS_CODE",3); //wrong data type
				return;
			}
		}

	}

	if (lstDataForWrite.getRowCount()==0)
		return;

	//---------------------------------------------------
	//			WRITE DATA 2 DB
	//---------------------------------------------------
	DbRecordSet lstStatus;
	switch (nCustomFld_Type)
	{
	case GlobalConstants::TYPE_CUSTOM_DATA_BOOL:
		{
			ormBOOL.Write(Ret_pStatus,lstDataForWrite,-1,1,&lstStatus);
		}
		break;
	case GlobalConstants::TYPE_CUSTOM_DATA_INT:
		{
			ormINT.Write(Ret_pStatus,lstDataForWrite,-1,1,&lstStatus);
		}
		break;
	case GlobalConstants::TYPE_CUSTOM_DATA_FLOAT:
		{
			ormFLOAT.Write(Ret_pStatus,lstDataForWrite,-1,1,&lstStatus);
		}
		break;
	case GlobalConstants::TYPE_CUSTOM_DATA_TEXT:
		{
			ormTEXT.Write(Ret_pStatus,lstDataForWrite,-1,1,&lstStatus);
		}
		break;
	case GlobalConstants::TYPE_CUSTOM_DATA_LONGTEXT:
		{
			ormLONGTEXT.Write(Ret_pStatus,lstDataForWrite,-1,1,&lstStatus);
		}
		break;
	case GlobalConstants::TYPE_CUSTOM_DATA_DATE:
		{
			ormDATE.Write(Ret_pStatus,lstDataForWrite,-1,1,&lstStatus);
		}
		break;
	case GlobalConstants::TYPE_CUSTOM_DATA_DATETIME:
		{
			ormDATETIME.Write(Ret_pStatus,lstDataForWrite,-1,1,&lstStatus);
		}
		break;
	default:
		{
			Q_ASSERT(false); //should not be here
			RetOut_Data.setColValue("STATUS_CODE",3); //wrong data type
			return;
		}
	}

	//some global error:
	if (!Ret_pStatus.IsOK())
		return;

	//lstStatus.Dump();

	//---------------------------------------------------
	//			SAVE STATUS for each record
	//---------------------------------------------------
	int nSizeWritten=lstDataForWrite.getRowCount();
	for (int i=0;i<nSizeWritten;i++)
	{
		if (lstStatus.getDataRef(i,"STATUS_CODE").toInt()!=0)
		{
			int nIdx=lstDataForWrite.getDataRef(i,"IDX").toInt();
			RetOut_Data.setData(nIdx,"STATUS_CODE",4);
			RetOut_Data.setData(nIdx,"STATUS_TEXT",lstStatus.getDataRef(0,"STATUS_TEXT"));
		}
	}

	//RetOut_Data.Dump();


}