#include "emailimportthread.h"
#include <QDateTime>

#include "common/common/threadid.h"
#include "trans/trans/xmlutil.h"
#include "bus_core/bus_core/globalconstants.h"
#include "common/common/datahelper.h"
#include "common/common/authenticator.h"
#include "common/common/sha256hash.h"
#include "bus_trans_client/bus_trans_client/userstoragehttpclient.h"
#include "../exe/appserver/appserver/mailpp/src/mailpp/mailpp.h"
#include "../exe/appserver/appserver/mimepp/src/mimepp/mimepp.h"
#include "bus_core/bus_core/optionsandsettingsid.h"
#include "bus_core/bus_core/mainentityfilter.h"
#include "bus_core/bus_core/accuarcore.h"

#include <openssl/ssl.h>
#include <openssl/err.h>
#include <openssl/rand.h>

#include "common/common/rsakey.h"

#include "common/common/logger.h"
extern Logger g_Logger;					//global logger
#include "bus_server/bus_server/businessserviceset.h"
extern BusinessServiceSet* g_BusinessServiceSet;
#include "bus_core/bus_core/servercontrolabstract.h"
extern ServerControlAbstract *g_AppServer;



#if defined(WIN32)
#  define WIN32_LEAN_AND_MEAN
#  include <windows.h>
#  define snprintf _snprintf
#  define strcasecmp _stricmp
#endif

#define min(a,b)    (((a) < (b)) ? (a) : (b))

#define TYPE_POP3 1
#define TYPE_IMAP 2

#define MAX_ATTACHMENT_LIST_SIZE 16777216

typedef void (EmailImportThread::* FN_SET_ACTIVE)();

class myProtocolListener;
void FolderList2String(const QList<QStringList> &lstData, QString &strResult);
void String2FolderList(QString strData, QList<QStringList> &lstResult);
bool Thunderbird_GetMailAttBodyRecursive(mimepp::Entity &parent, DbRecordSet &lstRowResult, DbRecordSet &lstAttachments, QString strLogInfo);
void FilterEmailsByContact(Status status, DbRecordSet &lstData, int nPersonID, bool bSelectOnly = false);
void FilterAttachmentRecords(DbRecordSet &lstAttachments);
void ProcessAddressList(mimepp::AddressList &list, DbRecordSet &lstRowResult, int nCurRow, int nType, bool bAreTemplates, QSet<QString> &setTmp);
bool Pop3Login(mailpp::Pop3Client &client, const QString &strHost, int nPort, bool bUseSSL, bool bUseSTLS, const QString &strUSER, const QString &strPass, QString strLogInfo);
bool Pop3GetMessageIdxByDate(mailpp::Pop3Client &client, myProtocolListener &listener, const QDateTime &dtLastCheck, bool bUseUIDL, bool bUseTOP, QList<uint32_t> &lstMessageIdx, std::map<int, std::string> &mapMsgNum2Uidl, std::map<int, QDateTime> &mapMsgNum2Date, bool bFullScan, QString strLogInfo, int nPersonID, FN_SET_ACTIVE pSetThreadActive=NULL, EmailImportThread *pThread=NULL);
bool Pop3FetchMessages(mailpp::Pop3Client &client, myProtocolListener &listener, QList<uint32_t> &lstMessageIdx, std::map<int, std::string> &mapMsgNum2Uidl, std::map<int, QDateTime> &mapMsgNum2Date, bool bKnownSenderOnly, QDateTime &dtLastMessage, DbRecordSet &lstDataEmails, QString strLogInfo, QString &strLastErrorEmailUID, QString &strAccErrorLog, int nPersonID, QString strAccountEmail, FN_SET_ACTIVE pSetThreadActive=NULL, EmailImportThread *pThread=NULL);
bool Pop3FetchSingleMessage(mailpp::Pop3Client &client, myProtocolListener &listener, int nMessageIdx, const char *szUIDL, bool bKnownSenderOnly, QDateTime &dtLastMessage, DbRecordSet &lstDataEmails, QString strLogInfo, QString &strLastErrorEmailUID, QString &strAccErrorLog, QDateTime dtRecv, QString strAccountEmail);
bool ImapLogin(mailpp::Imap4Client &client, const QString &strHost, int nPort, bool bUseSSL, bool bUseSTLS, const QString &strUSER, const QString &strPass, QString strLogInfo);
bool ImapGetMessageIdxByDate(mailpp::Imap4Client &client, const QDateTime &dtLastCheck, QList<uint32_t> &lstMessageIdx, std::map<int, std::string> &mapMsgNum2Uidl, std::map<int, QDateTime> &mapMsgNum2Date, QString strLogInfo, int nPersonID, FN_SET_ACTIVE pSetThreadActive=NULL, EmailImportThread *pThread=NULL);
bool ImapFetchMessages(mailpp::Imap4Client &client, const QList<uint32_t> &lstMessageIdx, bool bKnownSenderOnly, QDateTime &dtLastMessage, DbRecordSet &lstDataEmails, std::map<int, std::string> &mapMsgNum2Uidl, std::map<int, QDateTime> &mapMsgNum2Date,  QString strLogInfo, QString &strLastErrorEmailUID, QString &strAccErrorLog, int nPersonID, FN_SET_ACTIVE pSetThreadActive=NULL, EmailImportThread *pThread=NULL);
bool ImapFetchSingleMessage(mailpp::Imap4Client &client, uint32_t nMessageIdx, const char *szUID, bool bKnownSenderOnly, QDateTime &dtLastMessage, DbRecordSet &lstDataEmails, QString strLogInfo, QString &strLastErrorEmailUID, QString &strAccErrorLog, QDateTime dtRecv);
//int FindEmailByUID(QByteArray &arData, int nPersonID);
//QString CalcCheckSum(DbRecordSet &rowEmail, int nEmailOwnerID);
QString DecodeField(const char *szField);
void DeleteEmailsOlderThanDays(int nPersonID, const QString &strEmail, int nDelEmailsAfterNumDays, QString strLogInfo);
QString PersonNameFromID(int nPersonID);
QString ParseFullEmailBodyForAttachmentName(mailpp::Imap4Client& client, uint32_t nMessageIdx, int nMimeBodyPart);


//// START IMAP helper class

// Attachment class saves information about body parts that are
// interpreted as attachments

class Attachment {
public:
    Attachment() { size = 0; }
    unsigned size;
	std::string filename;
    std::string contentType;
    std::string contentDescription;
	std::string contentEncoding;
	std::string CID;
    std::string bodyPartRef;    // reference we would use to retrieve the body part
};

// Message class saves essential information about the message, including
// a list of attachments

class Message {
public:
    Message() {size=0;numLines=0;partsCount=0;numAttachments=0;attachments=0;bIsHtml=false;}
    ~Message();
    unsigned size;
    unsigned numLines;
    std::string to;
    std::string cc;
    std::string bcc;
    std::string from;
    std::string subject;
    std::string date;
    int partsCount;  // counts how many parts we have examined
    std::string memoRef;  // reference needed to retrieve memo text part
    std::string memo;
	std::string memoEncoding;
	std::string memoCharset;
	bool bIsHtml;
    int numAttachments;
    Attachment** attachments;
    void appendAttachment(Attachment* attachment);
};

Message::~Message()
{
    for (int i=0; i < numAttachments; ++i) {
        delete attachments[i];
    }
    if (attachments != 0) {
        free(attachments);
    }
}

void Message::appendAttachment(Attachment* attachment)
{
    Attachment** attachs = (Attachment**) realloc(attachments,
        (numAttachments+1)*sizeof(Attachment*));
    if (attachs != 0) {
        attachments = attachs;
        attachments[numAttachments] = attachment;
        ++numAttachments;
    }
    else {
        delete attachment;
    }
}

void processFetchFull(mailpp::Imap4Client& client, int nMessageIdx, Message& message);
void processFetchBodySection(const mailpp::Imap4Client& client, Message& message, QString strLogInfo="");
QDateTime ParseRfc2822DateTime(QString strDate);

//// END IMAP

//date only: 15 May 2009
QString DateToRfc2822(const QDate &date)
{
	if(!date.isValid()) return "";
	char *szEnMonths[] = {"JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"};
	return QString("%1-%2-%3").arg(date.day()).arg(szEnMonths[date.month()-1]).arg(date.year());
}

static int verify_callback2(X509_STORE_CTX *,void *)
{
	return 1;	//do not terminate
}

class myTraceOutput : public mailpp::TraceOutput
{
public:
	/// Default constructor
	myTraceOutput(){};
	virtual ~myTraceOutput(){};

	virtual void Send (const char *buffer, unsigned length)
	{
#ifdef _DEBUG
		qDebug() << "IMAP >" << buffer;
#endif
		//ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, "SMTP > " + QString(buffer));
	};

	virtual void Receive (const char *buffer, unsigned length)
	{
#ifdef _DEBUG
		qDebug() << "IMAP <" << buffer;
#endif
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG, "IMAP incoming data < " + QString(buffer));
	};
};

class myProtocolListener : public mailpp::ProtocolListener
{
public:
	virtual void ClearLines() {
		m_lstLines.clear();
	};
	virtual void LineReceived(const char *line){
		m_lstLines.push_back(line);	
	};

public:
	std::vector<std::string> m_lstLines;
};

//-----------thread object-----------------
EmailImportThread::EmailImportThread(ThreadSynchronizer *pThreadSync,DbRecordSet &lstEmailAccounts)
{
	m_ThreadSync=pThreadSync;
	m_lstEmailAccounts=lstEmailAccounts;
	SetThreadActive();
}

EmailImportThread::~EmailImportThread()
{
}

void EmailImportThread::Destroy()
{
	deleteLater();
	m_ThreadSync->ThreadRelease();
}

//run in separate thread:
void EmailImportThread::ExecuteTask()
{
	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0,"Email synchronization started, thread: "+QVariant(ThreadIdentificator::GetCurrentThreadID()).toString());
	DoActionPop3();
	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0,"Email synchronization done, thread: "+QVariant(ThreadIdentificator::GetCurrentThreadID()).toString());
	emit Stopped();
}

#define EML_SECURITY_NONE	  1
#define EML_SECURITY_STARTTLS 2
#define EML_SECURITY_SSLTLS	  3

void EmailImportThread::DoActionPop3()
{
#ifdef _WIN32
	//------------------------------------------------------------------------------------------------------------------------
	//B.T. comment: all accounts for this thread are in m_lstEmailAccounts (store last check time)
	//------------------------------------------------------------------------------------------------------------------------
	int nSize=m_lstEmailAccounts.getRowCount();
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Start email import, updating accounts: %1, thread: %2").arg(nSize).arg(ThreadIdentificator::GetCurrentThreadID()));
	qDebug()<<"POP3/IMAP Sync thread:"<<ThreadIdentificator::GetCurrentThreadID() << ", updating accounts:" << nSize;

	//code tests:
	//QString strRTF = DecodeField("=?UTF-8?Q?Ihr_pers=c3=b6nlicher_XING-Newsletter_11_|_2014?=");
	//QString strFrom = DecodeField("=?UTF-8?B?QnJhbmltaXIgVHJ1bWJpxIc=?=\r\n <trumbic@helix-business-soft.hr>");
	
	/*
	Status status;
	QFile file("D:/mime_umlaut.txt");
	file.open(QIODevice::ReadOnly);
	QByteArray data=file.readAll();
	QString strMime = QString::fromUtf8(data);
	ImportMessageFromMime(status, 1, 0, "a@a.com", "b@b.com", "=?utf-8?B?VW1sYXV0ZTogw7bDpMO8?=", strMime, "a@a.com", "", "");
	*/

	for (int y=0;y<nSize;y++)
	{
		HandleMailAccount(y);
	}
#endif //#ifdef _WIN32
}

//Writes attachments to DB
void EmailImportThread::WriteEmailAttachments(Status &Ret_pStatus,int nEmailID, DbRecordSet &Attachments)
{
	//Write content of attachments to local temp location, best: temp+/email_attach_+email_id
	QString strAttachmentSubDir="_email_attach_"+QString::number(nEmailID);
	QString strAttachmentSubDirFullPath= QDir::tempPath()+"/"+strAttachmentSubDir;

	QDir attach(strAttachmentSubDirFullPath);
	if (attach.exists())
	{
		DataHelper::RemoveAllFromDirectory(QDir::tempPath()+"/"+strAttachmentSubDir,true,false);
	}
	else
	{
		QDir attach_create(QDir::tempPath());
		if(!attach_create.mkdir(strAttachmentSubDir))
		{
			Ret_pStatus.setError(StatusCodeSet::ERR_GENERAL,"Failed to create directory "+strAttachmentSubDirFullPath);
			return;
		}
	}

	//rename all files with prefix (to avoid problems)
	QString strAttachmentPrefix="email_attach_"+QString::number(nEmailID)+"_";

	//write files one by one to local disk
	int nSize=Attachments.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		QString strName=strAttachmentPrefix+Attachments.getDataRef(i,"BEA_NAME").toString();
		QString strFilePath=strAttachmentSubDirFullPath+"/"+strName;
		int nAttachID=Attachments.getDataRef(i,"BEA_ID").toInt();

		QFile file(strFilePath);
		if(!file.open(QIODevice::WriteOnly))
		{
			Ret_pStatus.setError(StatusCodeSet::ERR_GENERAL,"Failed to create file "+strFilePath);
			DataHelper::RemoveAllFromDirectory(strAttachmentSubDirFullPath,true,true);
			return;
		}
		QByteArray	byteFileContent=Attachments.getDataRef(0,"BEA_CONTENT").toByteArray(); 
		int nErr=file.write(byteFileContent);
		if (nErr==-1)
		{
			Ret_pStatus.setError(StatusCodeSet::ERR_GENERAL,"Failed to write to file "+strFilePath);
			DataHelper::RemoveAllFromDirectory(strAttachmentSubDirFullPath,true,true);
			return;
		}

		file.close();
	}

	//clear temp object:
	DataHelper::RemoveAllFromDirectory(strAttachmentSubDirFullPath,true,true);
	QByteArray empty;
	Attachments.setColValue("BEA_CONTENT",empty);

	//actually write attachment records on server (no blob)
	g_BusinessServiceSet->BusEmail->WriteAttachments(Ret_pStatus,nEmailID,Attachments);
	if (!Ret_pStatus.IsOK())
	{
		DataHelper::RemoveAllFromDirectory(strAttachmentSubDirFullPath,true,true);
		return;
	}

	//send signal to server to fetch files and write to db
	g_BusinessServiceSet->BusEmail->WriteEmailAttachmentsFromUserStorage(Ret_pStatus,nEmailID,Attachments);
}

#ifdef _WIN32
//TOFIX similar fn with the same name is in the Sokrates_SPC
bool Thunderbird_GetMailAttBodyRecursive(mimepp::Entity &parent, DbRecordSet &lstRowResult, DbRecordSet &lstAttachments, QString strLogInfo)
{
	int nCurRow = lstRowResult.getRowCount()-1;

	//recursively drill down to find the correct body and attachments (may have multipart inside the multipart, ...)
	bool bBodyPartSet = false;
	bool bBodyPartHTML = false;

	int nParts = parent.body().numBodyParts();
	for(int i=0; i<nParts; i++)
	{
		mimepp::BodyPart &part = parent.body().bodyPartAt(i);
		mimepp::MediaType::MType type = part.headers().contentType().typeAsEnum();

		//extract attachment name
		QString strContType = QString::fromUtf8(part.headers().fieldBody("Content-Type").text().c_str());
		QString strName = EmailImportThread::GetFileNameFromContentType(strContType);

		/*
		int nPos = strContType.indexOf("name=");
		if(nPos >= 0){
			strName = strContType.mid(nPos+5);
			nPos = strName.indexOf("\"");
			if(nPos >= 0){
				strName = strName.left(nPos);
				strName = DecodeField(strName.toUtf8().constData());
			}
		}
		*/
		if(strName.isEmpty())
		{
			// not an attachment, must be the body
			if(!bBodyPartSet || !bBodyPartHTML)
			{
				if (type == mimepp::MediaType::MULTIPART)
				{
					if(Thunderbird_GetMailAttBodyRecursive(part, lstRowResult, lstAttachments, strLogInfo)){
						 bBodyPartSet = true;
						 bBodyPartHTML = lstRowResult.getDataRef(nCurRow, "BEM_EMAIL_TYPE").toInt();
					}
				}
				else if (type == mimepp::MediaType::TEXT)
				{
					QString strCharset = "US-ASCII";
					if (part.headers().hasField("Content-Type")) {
						const mimepp::MediaType& mtype = part.headers().contentType();
						mimepp::String subtype = mtype.subtype();
						int numParams = mtype.numParameters();
						for (int i=0; i < numParams; ++i) {
							const mimepp::Parameter& param = part.headers().contentType().parameterAt(i);
							const mimepp::String& name = param.name();
							if (0 == strcasecmp("charset", name.c_str())) {
								strCharset = param.value().c_str();
								break;
							}
						}
					}

					//transfer format decoding
					int cte = mimepp::TransferEncodingType::_7BIT;
					if (part.headers().hasField("Content-Transfer-Encoding")) {
						cte = part.headers().contentTransferEncoding().asEnum();
					}
					std::string strData;
					if (cte == mimepp::TransferEncodingType::QUOTED_PRINTABLE) {
						mimepp::QuotedPrintableDecoder dec;
						strData = dec.decode(part.body().getString().c_str()).c_str();
					}
					else if (cte == mimepp::TransferEncodingType::BASE64) {
						mimepp::Base64Decoder dec;
						strData = dec.decode(part.body().getString().c_str()).c_str();
					}
					else
						strData = part.body().getString().c_str();

					//text code page decoding
					QString strRTF = QString::fromUtf8(strData.c_str());
					if(!strCharset.isEmpty()){
						QTextCodec *codec = QTextCodec::codecForName(strCharset.toLocal8Bit().constData());
						if(codec)
							strRTF = codec->toUnicode(strData.c_str());
					}

					//refresh body
					if(!strRTF.isEmpty()){
						//can still be text-only, check mail type
						bool bHtml = (strRTF.indexOf("<html", 0, Qt::CaseInsensitive) >= 0) || (strRTF.indexOf("<!DOCTYPE", 0, Qt::CaseInsensitive) >= 0) || (strContType.indexOf("html") >= 0);
						lstRowResult.setData(nCurRow, "BEM_BODY", strRTF);
						lstRowResult.setData(nCurRow, "BEM_EMAIL_TYPE", (bHtml)? 1 : 0);
						bBodyPartHTML = bHtml;
					}
					bBodyPartSet = true;
				}
				else{
					//int i=0; //TOFIX ?
				}
			}
		}
		else
		{
			//an attachment

			//Attachment contents
			// Check content-transfer-encoding, and decode if necessary
			mimepp::String text = part.body().getString();
			int cte = mimepp::TransferEncodingType::_7BIT;
			if (part.headers().hasField("Content-Transfer-Encoding")) {
				cte = part.headers().contentTransferEncoding().asEnum();
			}
			if (cte == mimepp::TransferEncodingType::QUOTED_PRINTABLE) {
				mimepp::QuotedPrintableDecoder dec;
				text = dec.decode(text);
			}
			else if (cte == mimepp::TransferEncodingType::BASE64) {
				mimepp::Base64Decoder dec;
				text = dec.decode(text);
			}

			//if the attachment is "winmail.dat" we must extract one or more attachments from within its contents
			bool bParsed = false;
			if(0 == strcasecmp("winmail.dat", strName.toLocal8Bit().constData()))
			{
				mimepp::TnefDecoder dec(text);
				if(0 == dec.parse())
				{
					bParsed = true;
					//one winmail.dat (TNEF) attachment can have more than one attachment inside
					unsigned nCnt = dec.numAttachments();
					g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%2: parsing winmail.dat attachments (packing %1 items inside; buffer size=%3). thread: %4").arg(nCnt).arg(strLogInfo).arg(text.size()).arg(ThreadIdentificator::GetCurrentThreadID()));

					for(int j=0; j<nCnt; j++){
						mimepp::TnefAttachment &att = dec.attachmentAt(j);

						lstAttachments.addRow();
						int nRow = lstAttachments.getRowCount()-1;

						QString strAttName = QString::fromUtf8(att.fileName().c_str());
						lstAttachments.setData(nRow, "BEA_NAME", strAttName);
					
						int nLen = att.content().length();
						QByteArray att1(att.content().c_str(), nLen);
						//B.T.: compress attachment, damn it:
						att1=qCompress(att1,1); //min compression
						lstAttachments.setData(nRow, "BEA_CONTENT", att1);
					}
				}
				else{
					g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0,QString("%1: Failed to parse Winmail.dat attachment (buffer size=%2), thread: %3").arg(strLogInfo).arg(text.size()).arg(ThreadIdentificator::GetCurrentThreadID()));
				}
			}

			if(!bParsed)
			{
				lstAttachments.addRow();
				int nRow = lstAttachments.getRowCount()-1;
				lstAttachments.setData(nRow, "BEA_NAME", strName);

				int nLen = text.length();
				QByteArray att(text.c_str(), nLen);
				//B.T.: compress attachment, damn it:
				att=qCompress(att,1); //min compression
				lstAttachments.setData(nRow, "BEA_CONTENT", att);

				//extract CID
				if (part.headers().hasField("Content-ID")){
					QString strCID = part.headers().fieldBody("Content-ID").text().c_str();
					//strip <> quotes
					strCID.chop(1); 
					strCID = strCID.right(strCID.length()-1);
					lstAttachments.setData(nRow, "BEA_CID_LINK", strCID);
					qDebug() << "Attachment CID:" << strCID;
				}
			}
		}
	}
	return bBodyPartSet;
}
#endif

void FilterEmailsByContact(Status status, DbRecordSet &lstData, int nPersonID, bool bSelectOnly)
{
	qDebug() << "FUI_ImportEmail::FilterEmailsByContact";
	int nRows = lstData.getRowCount();
	int nTotalRows = nRows;	//remember input size
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("ImportEmails - filter mails by contact (row count=%1), thread: %2").arg(nTotalRows).arg(ThreadIdentificator::GetCurrentThreadID()));

	int nCountSkipped = 0;
	int nCountPrivate = 0;
	for(int i=0; i<nRows; i++)
	{
		//lstData.Dump();

		//extract contacts for this email
		DbRecordSet lstContactLinks;
		lstData.getData(i, "CONTACTS", lstContactLinks);

		int nContactCnt = lstContactLinks.getRowCount();
		for(int j=0; j<nContactCnt; j++)
		{
			int nContactID = lstContactLinks.getDataRef(j, "BNMR_TABLE_KEY_ID_2").toInt();
			//lstContactLinks.setData(nRow, "CELC_LINK_ROLE_ID", nLinkRole);

			//get contact flags: BCNT_DO_NOT_SYNC_MAIL, BCNT_SYNC_MAIL_AS_PRIV
			DbRecordSet lstContactInfo;
			QString strWhere= QString(" WHERE BCNT_ID=%1").arg(nContactID);
			MainEntityFilter filter;
			filter.SetFilter(MainEntityFilter::FILTER_SQL_WHERE,strWhere);
			g_BusinessServiceSet->BusContact->ReadData(status, filter.Serialize(), lstContactInfo);
			if(!status.IsOK()){ 
				g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0,QString("FilterEmailsByContact - error: %1, thread: %2").arg(status.getErrorText()).arg(ThreadIdentificator::GetCurrentThreadID()));
				return;	// no msg due to multithreading
			}

			//now use flags to filter the list
			if( lstContactInfo.getRowCount() > 0)
			{
				if(lstContactInfo.getDataRef(0, "BCNT_DO_NOT_SYNC_MAIL").toInt() > 0)
				{
					nCountSkipped ++;
					if(bSelectOnly)
					{
						lstData.selectRow(i);
					}
					else
					{
						//remove the row from the recordset
						lstData.deleteRow(i);
						i --; nRows --;
					}
				}
				else if(lstContactInfo.getDataRef(0, "BCNT_SYNC_MAIL_AS_PRIV").toInt() > 0)
				{
					//mark as private before importing
					nCountPrivate ++;
					//lstData.setData(i, "CENT_IS_PRIVATE", 1);
					DbRecordSet lstUAR,lstGAR;
					AccUARCore::CreatePrivateUAR(nPersonID,lstData.getDataRef(i,"BEM_ID").toInt(),BUS_EMAIL,lstUAR,lstGAR);
					lstData.setData(i, "UAR_TABLE", lstUAR);
					lstData.setData(i, "GAR_TABLE", lstGAR);
					//lstUAR.Dump();
				}
			}
		}
	}

	qDebug() << QString("FilterEmailsByContact: out of %1 messages, %2 messages were removed, %3 messages marked as private").arg(nTotalRows).arg(nCountSkipped).arg(nCountPrivate);
}

//similar to FUI_ImportEmail::ImportEmails
//B.T. 2014-04-03 transfered back to the class....why are we using C++??? objects, classes
void EmailImportThread::ImportEmails(Status status, DbRecordSet &lstData, int nPersonID, int &nSkippedOrReplacedCnt, bool bAddDupsAsNewRow, bool bSkipExisting)
{
	qDebug() << "FUI_ImportEmail::ImportEmails";
	int nSize = lstData.getRowCount();
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("ImportEmails - import operation (row count = %1), thread: %2").arg(nSize).arg(ThreadIdentificator::GetCurrentThreadID()));

	//B.T. reset unread flag to 1
	lstData.setColValue("BEM_UNREAD_FLAG",1);
	//
	// import process
	//
	
	if(nSize < 1) return;	//nothing to import

	const int nChunkSize = 30;
	int nCount = 0;
	int nFailuresCnt = 0;
	int nStart = -1;
	int nRowProgress = 0;

	qDebug() << "Import Email Start";
		
	while(nCount<nSize)
	{
		SetThreadActive();

		int nAttachmentChunkSize=0;

		DbRecordSet lstChunk; //BT: always redefine chunk list!!!
		lstChunk.copyDefinition(lstData);
		if (lstChunk.getColumnIdx("ATTACHMENTS")>=0)
			lstChunk.removeColumn(lstChunk.getColumnIdx("ATTACHMENTS")); //BT = remove this, coz it will go on server

		DbRecordSet lstAttachments;
		lstAttachments.addColumn(DbRecordSet::GetVariantType(), "ATT_LIST");
		DbRecordSet lstContactLinks;
		lstContactLinks.addColumn(DbRecordSet::GetVariantType(), "CONTACT_LIST");

		//prepare chunk
		lstChunk.clear();

		qDebug() << "Import Email New Chunk";

		int nMax = min(nCount+nChunkSize, nSize);
		for(int i=nCount; i<nMax; i++)
		{
			nAttachmentChunkSize+=lstData.getDataRef(i, "ATTACHMENTS_SIZE").toInt();
			if (nAttachmentChunkSize>=MAX_ATTACHMENT_LIST_SIZE)
			{
				if (i==nCount) //if only 1 mail is bigger then otribi attachments until <MAX_ATTACHMENT_LIST_SIZE
				{
					g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("ImportEmails - start trying to truncate attachment of %1 bytes for mail %2, thread: %3").arg(nAttachmentChunkSize).arg(lstData.getDataRef(i,"BEM_SUBJECT").toString()).arg(ThreadIdentificator::GetCurrentThreadID()));
					DbRecordSet lstAttachmentOneEmail=lstData.getDataRef(i, "ATTACHMENTS").value<DbRecordSet>();
					FilterAttachmentRecords(lstAttachmentOneEmail);
					Q_ASSERT(lstAttachmentOneEmail.getStoredByteSize()<MAX_ATTACHMENT_LIST_SIZE); 
					nAttachmentChunkSize=lstAttachmentOneEmail.getStoredByteSize(); //add new value
					g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("ImportEmails - attachment truncated to the %1 bytes, thread: %2").arg(nAttachmentChunkSize).arg(ThreadIdentificator::GetCurrentThreadID()));
				}
				else
					break; //if limit is reached, exit loop and send chunk as is, lstattachments should be smaller then MAX_ATTACHMENT_LIST_SIZE
			}

			lstChunk.merge(lstData.getRow(i));
			//append attachment
			lstAttachments.addRow();
			DbRecordSet row1 = lstData.getDataRef(i, "ATTACHMENTS").value<DbRecordSet>();
			lstAttachments.setData(lstAttachments.getRowCount()-1, 0, row1);

#ifdef _DEBUG
			DbRecordSet tmp = lstData.getDataRef(i, "ATTACHMENTS").value<DbRecordSet>();
			int nAttCnt = tmp.getRowCount();
			for(int j=0; j<nAttCnt; j++)
				qDebug() << "Attachment" << j << "Size" << tmp.getDataRef(j, "BEA_CONTENT").toByteArray().size() << "Name" << tmp.getDataRef(j, "BEA_NAME").toString();
#endif

			lstContactLinks.addRow();
			DbRecordSet row = lstData.getDataRef(i, "CONTACTS").value<DbRecordSet>();
			lstContactLinks.setData(lstContactLinks.getRowCount()-1, 0, row);
		}

		Q_ASSERT(lstChunk.getRowCount()!=0); //must be >0

		//if(lstChunk.getRowCount() < 1)
		//	break;

		Q_ASSERT(lstAttachments.getRowCount() == lstChunk.getRowCount());

		//BT added: set owner to logged user:
		if (nPersonID!=0)
			lstChunk.setColValue(lstChunk.getColumnIdx("CENT_OWNER_ID"),nPersonID);
		//BT added: set mail type:
		lstChunk.setColValue(lstChunk.getColumnIdx("CENT_SYSTEM_TYPE_ID"),GlobalConstants::CE_TYPE_EMAIL);

		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("ImportEmails - prepare to send data chunk %1 rows, %3 bytes (total %2), attachment list size: %4, thread: %5").arg(lstChunk.getRowCount()).arg(nSize).arg(lstChunk.getStoredByteSize()).arg(lstAttachments.getStoredByteSize()).arg(ThreadIdentificator::GetCurrentThreadID()));

		int nChunkSizePre = lstChunk.getRowCount();

		QString strLockRes;
		DbRecordSet lstSchedule;
		g_BusinessServiceSet->BusEmail->WriteMultiple(status, lstChunk, lstContactLinks, lstSchedule, lstAttachments, strLockRes, bSkipExisting, nSkippedOrReplacedCnt, bAddDupsAsNewRow);
		if(!status.IsOK()){
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0,QString("ImportEmails - error: %1, thread: %2").arg(status.getErrorText()).arg(ThreadIdentificator::GetCurrentThreadID()));
			return;
		}

		int nChunkSize = lstChunk.getRowCount();
		Q_ASSERT(nChunkSizePre == nChunkSize);

		//now write email IDs back into the  main list
		for(int z=0; z<nChunkSize; z++){
			int nEmailID = lstChunk.getDataRef(z, "BEM_ID").toInt();
			//some emails may be skipped (already existing in the database, ID=0 (NULL))
			//Q_ASSERT(nEmailID > 0);
			//BT:nEmailID=0 for skipped emails...
			lstData.setData(nCount + z, "BEM_ID", nEmailID);
		}
		
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("ImportEmails - chunk save success, thread: %1").arg(ThreadIdentificator::GetCurrentThreadID()));

		//proceed to next chunk
		nCount += nChunkSizePre;
	}

	qDebug() << "Import Email Exit";
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("ImportEmails - Import Email Exit, thread: %1").arg(ThreadIdentificator::GetCurrentThreadID()));
}

//for one mail filters biggest attachment until sum <16mb
//FUI_ImportEmail::FilterAttachmentRecords
void FilterAttachmentRecords(DbRecordSet &lstAttachments)
{
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("ImportEmails - filter attachment records, thread: %1").arg(ThreadIdentificator::GetCurrentThreadID()));

	while(lstAttachments.getStoredByteSize()>MAX_ATTACHMENT_LIST_SIZE)
	{
		int nMaxAttach=0;
		int nRowWithBiggestAttachment=-1;
		int nSize=lstAttachments.getRowCount();
		for(int i=0;i<nSize;i++)
		{
			int nAttchSize=lstAttachments.getDataRef(i,"BEA_CONTENT").toByteArray().size();
			if (nAttchSize>nMaxAttach)
			{
				nMaxAttach=nAttchSize;
				nRowWithBiggestAttachment=i;
			}
		}
		if (nRowWithBiggestAttachment==-1)
		{
			Q_ASSERT(false); //this means that list >40Mb and no attachments!?
			return;
		}
		QString strName = lstAttachments.getDataRef(nRowWithBiggestAttachment, "BEA_NAME").toString();
		//QByteArray datNewContents = qCompress(tr("Attachment '%1� is too big (%d MB), Skipped!")).arg(strName).arg(nAttSizeMB).toLatin1());
		QByteArray datNewContents = QString("Attachment size limits reached. '%1� Skipped!").arg(strName).toLatin1();
		datNewContents = qCompress(datNewContents,1);
		lstAttachments.setData(nRowWithBiggestAttachment, "BEA_CONTENT", datNewContents);
		//overwrite name
		strName = "Attachment Skipped � Read Info.txt";
		lstAttachments.setData(nRowWithBiggestAttachment, "BEA_NAME", strName);

		g_Logger.logMessage(StatusCodeSet::TYPE_WARNING,0,QString("Attachment %1 was not imported, maximum size is exceeded, thread: %2").arg(lstAttachments.getDataRef(nRowWithBiggestAttachment,"BEA_NAME").toString()).arg(ThreadIdentificator::GetCurrentThreadID()));
	}
}

#ifdef _WIN32
//MailManager::ProcessAddressList
void ProcessAddressList(mimepp::AddressList &list, DbRecordSet &lstRowResult, int nCurRow, int nType, bool bAreTemplates, QSet<QString> &setTmp)
{
	int nAddrCnt = list.numAddresses();
	for(int i=0; i<nAddrCnt; i++)
	{
		if(!bAreTemplates)
		{
			QString strName, strEmail;
			mimepp::Address &addr = list.addressAt(i);
			mimepp::Mailbox addr1(addr.getString());
			addr1.parse();

			strName  = QString::fromUtf8(addr1.displayNameUtf8().c_str());
			strEmail  = addr1.localPart().c_str();
			strEmail += "@";
			strEmail += addr1.domain().c_str();

			//append new recipient
			QString strValue;
			if(1 == nType)
				strValue = lstRowResult.getDataRef(nCurRow, "BEM_TO").toString();
			else if(2 == nType)
				strValue = lstRowResult.getDataRef(nCurRow, "BEM_CC").toString();
			else if(3 == nType)
				strValue = lstRowResult.getDataRef(nCurRow, "BEM_BCC").toString();

			if(!strValue.isEmpty())
				strValue += ";";

			QString strMail(strName);
			if(!strEmail.isEmpty()){
				strMail += " <";
				strMail += strEmail;
				strMail += ">";
			}

			strValue += strMail;

			//qDebug() << "Email" << strEmail;
			setTmp.insert(strEmail);

			//store back the value
			if(1 == nType)
				lstRowResult.setData(nCurRow, "BEM_TO", strValue);
			else if(2 == nType)
				lstRowResult.setData(nCurRow, "BEM_CC", strValue);
			else if(3 == nType)
				lstRowResult.setData(nCurRow, "BEM_BCC", strValue);
		}
	}
}
#endif

//Service_BusEmail::FindEmail
/*
int FindEmailByUID(DbRecordSet &rowEmail, int nEmailOwnerID)
{
	QString strChkSum = CalcCheckSum();
	Status status;
	DbSqlQuery query(status, g_BusinessServiceSet->GetDbManager());	
	if(!status.IsOK()){
		Q_ASSERT_X(false, "Service_BusEmail::FindEmail", status.getErrorText().toLatin1());	
		return -1;
	}

	//find email by entryID
	QString strSQL, strData;
	strSQL = "SELECT BEM_ID FROM BUS_EMAIL WHERE BEM_EXT_ID_CHKSUM =?";

	//Prepare statement for execution.
	query.Prepare(status, strSQL);
	if(!status.IsOK()){
		Q_ASSERT_X(false, "Service_BusEmail::FindEmail", status.getErrorText().toLatin1());	
		return -1;
	}
	query.bindValue(0, strChkSum);

	query.ExecutePrepared(status);
	if(!status.IsOK()){
		Q_ASSERT_X(false, "Service_BusEmail::FindEmail", status.getErrorText().toLatin1());	
		return -1;
	}

	if (query.next())
	{
		return query.value(0).toInt();
	}
	return -1;
}

//B.T. 2014-07-30: new technique for chksum UID: user
QString CalcCheckSum(DbRecordSet &rowEmail, int nEmailOwnerID)
{
	if (rowEmail.getRowCount()==0)
		return "";

	QString strToAcc=rowEmail.getDataRef(0,"BEM_TO_EMAIL_ACCOUNT").toString().left(120);
	if (strToAcc.isEmpty())
		strToAcc=rowEmail.getDataRef(0,"BEM_TO").toString().left(120);

	QString strChkSum=QString::number(nEmailOwnerID)+"-";
	strChkSum+=strToAcc+"-";
	strChkSum+=QString(rowEmail.getDataRef(0,"BEM_BODY").toByteArray().size())+"-";
	strChkSum+=rowEmail.getDataRef(0,"BEM_EXT_ID").toString();
	return strChkSum;
}

*/
/*
int FindEmailByUID(QByteArray &arData, int nPersonID)
{
	if(arData.size() < 1)
		return -1;

	Status status;
	DbSqlQuery query(status, g_BusinessServiceSet->GetDbManager());	
	if(!status.IsOK()){
		Q_ASSERT_X(false, "Service_BusEmail::FindEmail", status.getErrorText().toLatin1());	
		return -1;
	}

	//find email by entryID
	QString strSQL, strData;
	if(query.GetDbConnection()->driverName().indexOf("QIBASE") >= 0){
	 	strSQL = "SELECT BEM_ID FROM BUS_EMAIL INNER JOIN CE_COMM_ENTITY ON BEM_COMM_ENTITY_ID=CENT_ID WHERE BEM_EXT_ID_CHKSUM =? AND CENT_OWNER_ID=?";
		strData = arData.toBase64().constData();
	}
	else
		strSQL = "SELECT BEM_ID FROM BUS_EMAIL INNER JOIN CE_COMM_ENTITY ON BEM_COMM_ENTITY_ID=CENT_ID WHERE BEM_EXT_ID=? AND CENT_OWNER_ID=?";

	//Prepare statement for execution.
	query.Prepare(status, strSQL);
	if(!status.IsOK()){
		Q_ASSERT_X(false, "Service_BusEmail::FindEmail", status.getErrorText().toLatin1());	
		return -1;
	}

	if(query.GetDbConnection()->driverName().indexOf("QIBASE") >= 0)
		query.bindValue(0, strData);
	else
		query.bindValue(0, arData);
	query.bindValue(1, nPersonID);

	query.ExecutePrepared(status);
	if(!status.IsOK()){
		Q_ASSERT_X(false, "Service_BusEmail::FindEmail", status.getErrorText().toLatin1());	
		return -1;
	}

	DbRecordSet rsResult;
	query.FetchData(rsResult);

	if(rsResult.getRowCount()>0){
		return rsResult.getDataRef(0,0).toInt();
	}

	return -1;
}
*/

bool EmailImportThread::HandleMailAccount(int nIdx)
{
	//
	// STEP 1: get account info
	//
	bool bIsActive = true;
	if(m_lstEmailAccounts.getColumnIdx("POP3_ACCOUNT_ACTIVE")>=0)
		bIsActive = (m_lstEmailAccounts.getDataRef(nIdx, "POP3_ACCOUNT_ACTIVE").toInt() > 0);
	bool bIsPOP3 = true;
	if(m_lstEmailAccounts.getColumnIdx("POP3_SERVER_TYPE")>=0)
		bIsPOP3 = (m_lstEmailAccounts.getDataRef(nIdx, "POP3_SERVER_TYPE").toInt() == TYPE_POP3);
	bool bUseSSL	= (EML_SECURITY_NONE != m_lstEmailAccounts.getDataRef(nIdx, "POP3_USE_SSL").toInt());
	bool bUseSTLS	= (EML_SECURITY_STARTTLS == m_lstEmailAccounts.getDataRef(nIdx, "POP3_USE_SSL").toInt());
	QString strHost = m_lstEmailAccounts.getDataRef(nIdx, "POP3_HOST").toString();
	int nPort	= m_lstEmailAccounts.getDataRef(nIdx, "POP3_PORT").toInt();
	QString strUSER = m_lstEmailAccounts.getDataRef(nIdx, "POP3_USER").toString();
	QString strPass = m_lstEmailAccounts.getDataRef(nIdx, "POP3_PASS").toString();
	QString strEmail = m_lstEmailAccounts.getDataRef(nIdx, "POP3_EMAIL").toString();
	int nPersonID=m_lstEmailAccounts.getDataRef(nIdx, "BOUS_PERSON_ID").toInt();

	//info written in ever log item
	QString strLogInfo = QString("[%1:%2:%3: thread = %4]").arg(bIsPOP3? "POP3" : "IMAP").arg(PersonNameFromID(nPersonID)).arg(strEmail).arg(ThreadIdentificator::GetCurrentThreadID());

	if(!bIsActive){
		g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%6: Skip checking inactive email acc #%5: %1 (host: %2:%3), IsPOP3=%4").arg(strEmail).arg(strHost).arg(nPort).arg(bIsPOP3).arg(nIdx).arg(strLogInfo));
		return false;
	}
	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%6: Check email acc #%5: %1 (host: %2:%3), IsPOP3=%4").arg(strEmail).arg(strHost).arg(nPort).arg(bIsPOP3).arg(nIdx).arg(strLogInfo));

	//special fields
	bool bKnownSenderOnly =	m_lstEmailAccounts.getDataRef(nIdx, "POP3_KNOWN_CONTACT_ONLY").toInt()>0;
	int nCheckEveryMin    = m_lstEmailAccounts.getDataRef(nIdx, "POP3_CHECK_EVERY_MIN").toInt();
	QDateTime dtLastCheck = m_lstEmailAccounts.getDataRef(nIdx, "POP3_LAST_CHECK").toDateTime();
	QDateTime dtLastMsg;
	if(m_lstEmailAccounts.getColumnIdx("POP3_LAST_MESSAGE")>=0)	//newer field
		dtLastMsg = m_lstEmailAccounts.getDataRef(nIdx, "POP3_LAST_MESSAGE").toDateTime();
	else
		m_lstEmailAccounts.addColumn(QVariant::DateTime, "POP3_LAST_MESSAGE");
	
	/*
	if (PersonNameFromID(nPersonID)=="sokrates2@sokrates.ch" && strEmail=="bogdan@sokrates.ch")
	{
		//return true;
		g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%1: Reset account to -3 days").arg(strLogInfo));
		dtLastMsg=QDateTime::currentDateTime().addDays(-12);
		dtLastCheck=QDateTime::currentDateTime().addDays(-12);
	}
	*/

	//QString strTest = DecodeField("=?iso-8859-1?Q?Wir_kommen_zu_Ihnen:_Canon_Roadshow_in_Ihrer?= =?iso-8859-1?Q?_N=E4he?=");
	/*
	if (strEmail=="hahn565@gmail.com")
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%1: Reset account to -3 days").arg(strLogInfo));
		dtLastMsg=QDateTime::currentDateTime().addDays(-11);
		dtLastCheck=QDateTime::currentDateTime().addDays(-11);
	}
	else
		return true;
	*/

	//qDebug()<<dtLastMsg;
	//qDebug()<<dtLastCheck;

	bool bFullScan = false;
	if(m_lstEmailAccounts.getColumnIdx("POP3_FULL_SCAN_MODE")>=0)
		bFullScan = m_lstEmailAccounts.getDataRef(nIdx, "POP3_FULL_SCAN_MODE").toInt()>0;
	else
		m_lstEmailAccounts.addColumn(QVariant::Int, "POP3_FULL_SCAN_MODE");
	int nDelEmailsAfterNumDays = 0;
	if(m_lstEmailAccounts.getColumnIdx("POP3_DEL_EMAIL_AFTER_DAYS")>=0)
		nDelEmailsAfterNumDays = m_lstEmailAccounts.getDataRef(nIdx, "POP3_DEL_EMAIL_AFTER_DAYS").toInt();
	
	QString strAccUID;
	if(m_lstEmailAccounts.getColumnIdx("POP3_ACC_UID")>=0)
		strAccUID = m_lstEmailAccounts.getDataRef(nIdx, "POP3_ACC_UID").toString();	
	else
		m_lstEmailAccounts.addColumn(QVariant::String, "POP3_ACC_UID");
	if(strAccUID.isEmpty())
		m_lstEmailAccounts.setData(nIdx, "POP3_ACC_UID", GenerateAccUID()); //unique ID for this account (needs to be unique only locally within a set of accounts for a single person)
	
	if(m_lstEmailAccounts.getColumnIdx("ERROR_EMAIL_UID")<0)
		m_lstEmailAccounts.addColumn(QVariant::String, "ERROR_EMAIL_UID");	//UID that failed last time we did import
	QString strLastErrorEmailUID = m_lstEmailAccounts.getDataRef(nIdx, "ERROR_EMAIL_UID").toString();
	
	if(m_lstEmailAccounts.getColumnIdx("ACC_ERROR_MESSAGES")<0)
		m_lstEmailAccounts.addColumn(QVariant::String, "ACC_ERROR_MESSAGES");	//log with recent issues with this account
	QString strAccErrorLog = m_lstEmailAccounts.getDataRef(nIdx, "ACC_ERROR_MESSAGES").toString();

	//
	// STEP 2: check if we need to process account
	//
	if(dtLastCheck.isValid() && dtLastCheck.addSecs(60*nCheckEveryMin) > QDateTime::currentDateTime()){
		g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%3: Skip checking account, check time has not come yet (%1)[checked each %2 minutes]. thread: %4").arg(dtLastCheck.addSecs(60*nCheckEveryMin).toString("yyyy-MM-dd HH:mm:ss")).arg(nCheckEveryMin).arg(strLogInfo).arg(ThreadIdentificator::GetCurrentThreadID()));
		return false;
	}
	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%3: Start checking account, last email timestamp: (%1)[checked each %2 minutes], last check time was: %4").arg(dtLastMsg.toString("yyyy-MM-dd HH:mm:ss")).arg(nCheckEveryMin).arg(strLogInfo).arg(dtLastCheck.toString("yyyy-MM-dd HH:mm:ss")));


	//do not even attempt to download emails that you are supposed to delete immediately
	if(nDelEmailsAfterNumDays > 0){
		QDateTime datFrom = QDateTime::currentDateTime();
		datFrom.setTime(QTime(0,0,0));
		datFrom = datFrom.addDays(-nDelEmailsAfterNumDays);

		//qDebug()<<datFrom;

		if(dtLastMsg < datFrom){
			g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%4: Last msg date %1 is older than required deletion after days date %2 (%3 days), update last message date, thread: %5").arg(dtLastMsg.toString("yyyy-MM-dd HH:mm:ss")).arg(datFrom.toString("yyyy-MM-dd HH:mm:ss")).arg(nDelEmailsAfterNumDays).arg(strLogInfo).arg(ThreadIdentificator::GetCurrentThreadID()));
			dtLastMsg = datFrom;
		}
	}
	
	//qDebug()<<dtLastMsg;
	//
	// STEP 3: process an account
	//

	SetThreadActive();
	bool bOK;
	if(bIsPOP3) {
		bOK = HandlePop3Account(strHost, nPort, bUseSSL, bUseSTLS, strUSER, strPass, nPersonID, dtLastMsg, bKnownSenderOnly, bFullScan, strLogInfo, strLastErrorEmailUID, strAccErrorLog, strEmail);
	}
	else {
		bOK = HandleImapAccount(strHost, nPort, bUseSSL, bUseSTLS, strUSER, strPass, nPersonID, dtLastMsg, bKnownSenderOnly, strLogInfo, strLastErrorEmailUID, strAccErrorLog, strEmail);
	}
	SetThreadActive();

	//
	// STEP 4: delete old emails, if needed
	//
	if(nDelEmailsAfterNumDays > 0)
	{
		DeleteEmailsOlderThanDays(nPersonID, strEmail, nDelEmailsAfterNumDays,  strLogInfo);
	}

	//ensure some junk email with wrong date from the future does not break our system
	if(dtLastMsg > QDateTime::currentDateTime())
		dtLastMsg = QDateTime::currentDateTime();

	//qDebug()<<dtLastMsg;

	//
	// STEP 5: update account dates
	//
	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%4: Email account #%3 checked success=%1, last email timestamp is: (%2). thread: %5").arg(bOK).arg(dtLastMsg.toString("yyyy-MM-dd HH:mm:ss")).arg(nIdx).arg(strLogInfo).arg(ThreadIdentificator::GetCurrentThreadID()));
	if(bOK){ 
		m_lstEmailAccounts.setData(nIdx, "POP3_LAST_MESSAGE", dtLastMsg);
		m_lstEmailAccounts.setData(nIdx, "POP3_FULL_SCAN_MODE", 0);	 //set to "smart" mode
	}
	m_lstEmailAccounts.setData(nIdx, "POP3_LAST_CHECK", QDateTime::currentDateTime());
	//m_lstEmailAccounts.setData(nIdx, "POP3_LAST_CHECK", QDateTime::currentDateTime().addDays(-1));
	//store back error field changes
	m_lstEmailAccounts.setData(nIdx, "ERROR_EMAIL_UID", strLastErrorEmailUID);
	m_lstEmailAccounts.setData(nIdx, "ACC_ERROR_MESSAGES", strAccErrorLog);

	return bOK;
}

//
// POP 3 protocol
//
bool EmailImportThread::HandlePop3Account(const QString &strHost, int nPort, bool bUseSSL, bool bUseSTLS, const QString &strUSER, const QString &strPass, int nPersonID, QDateTime &dtLastCheck, bool bKnownSenderOnly, bool bFullScan, QString strLogInfo, QString &strLastErrorEmailUID, QString &strAccErrorLog, QString strEmail)
{
	//init POP3 client
	mailpp::Pop3Client client;
#ifdef _DEBUG
	//myTraceOutput tracer;
	//client.SetTraceOutput(&tracer, false);
#endif
	myProtocolListener listener;
	client.SetListener(&listener, false);

	//login
	if(!Pop3Login(client, strHost, nPort, bUseSSL, bUseSTLS, strUSER, strPass, strLogInfo))
	{
		//B.T. notify user:
		g_AppServer->SendErrorMessage(nPersonID,"Failed to connect to the POP3 server. Check account settings.",1,strEmail);
		return false;
	}

	//test capabilities
	bool bUseUIDL = false;
	bool bUseTop = false;
	bool bUsePipelining = false;
	if(client.Capa() < 0){
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("%1: Failed to send POP3 command (CAPA). thread: %2").arg(strLogInfo).arg(ThreadIdentificator::GetCurrentThreadID()));
		client.Disconnect();
		return false;
	}
	if(mailpp::Pop3Client::OK != client.ReplyCode()){
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("%2: POP3 error: %1, thread: %3").arg(client.ErrorMessage()).arg(strLogInfo).arg(ThreadIdentificator::GetCurrentThreadID()));
		//NOTE: this is a harmless error so we don't return here (CAPA not supported)
	}
	else{
		//TOFIX possible cases for improving mail download speed (filtered by some date):
		//1. try to send multiple "TOP" commands at the same time with PIPELINING (http://www.faqs.org/rfcs/rfc2449.html)? (less round trips) -> examples? http://lists.gnu.org/archive/html/tpop3d-devel/2005-02/msg00000.html
		//2. read message from last index to first, abort reading when msg_date-last_read_date > 24hours!

		//query result to see if POP3 server supports UIDD command
		//Example reply lines:
		//SASL LOGIN PLAIN CRAM-MD5 DIGEST-MD5
		//STLS
		//LAST
		//TOP
		//USER
		//PIPELINING
		//UIDL
		//IMPLEMENTATION CommuniGatePro
				
		int nCapLines = listener.m_lstLines.size();
		for(int k=0; k<nCapLines; k++)
		{
			//dump all supported CAPS to log
			g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%3: POP3 CAPS (%1): %2, thread: %4").arg(k).arg(listener.m_lstLines[k].c_str()).replace("\r\n", "").replace("\n", "").arg(strLogInfo).arg(ThreadIdentificator::GetCurrentThreadID()));
			if(listener.m_lstLines[k].find("UIDL") != std::string::npos){
				bUseUIDL = true;
			}
			else if(listener.m_lstLines[k].find("TOP") != std::string::npos){
				bUseTop = true; 
			}
			else if(listener.m_lstLines[k].find("PIPELINING") != std::string::npos){
				bUsePipelining = true; 
			}
		}
	}

	//TOFIX use PIPELINING for TOP if supported (one day, mailpp does not allow sending custom commands!!!!)
	//get indexes for messages to be downloaded (filtered by date)

	//qDebug()<<dtLastCheck;
	QList<uint32_t> lstMessageIdx;
	std::map<int, std::string> mapMsgNum2Uidl;
	std::map<int, QDateTime> mapMsgNum2Date;
	if(!Pop3GetMessageIdxByDate(client, listener, dtLastCheck, bUseUIDL, bUseTop, lstMessageIdx, mapMsgNum2Uidl, mapMsgNum2Date, bFullScan, strLogInfo, nPersonID,&EmailImportThread::SetThreadActive,this))
		return false;

	//define IMPORT list:
	DbRecordSet lstDataEmails;	//email storage
	DbRecordSet set;
	set.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CE_COMM_ENTITY));
	lstDataEmails.copyDefinition(set, false);
	set.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_EMAIL));
	lstDataEmails.copyDefinition(set, false);
	lstDataEmails.addColumn(QVariant::String, "PROJECT_NAME");
	lstDataEmails.addColumn(QVariant::String, "PROJECT_CODE");
	lstDataEmails.addColumn(QVariant::String, "EMAIL_UNASSIGNED");
	lstDataEmails.addColumn(DbRecordSet::GetVariantType(), "ATTACHMENTS");
	lstDataEmails.addColumn(DbRecordSet::GetVariantType(), "CONTACTS");
	lstDataEmails.addColumn(DbRecordSet::GetVariantType(), "UAR_TABLE");
	lstDataEmails.addColumn(DbRecordSet::GetVariantType(), "GAR_TABLE");
	lstDataEmails.addColumn(QVariant::Int, "ATTACHMENTS_SIZE");

	//PLAN: if single message read was stuck, try at least to save the ones we managed to get successfully
	//		NOTE: this assumes that the list of messages was sorted by date so that the "last message date" is correct (my checks confirm this assumption)
	if(Pop3FetchMessages(client, listener, lstMessageIdx, mapMsgNum2Uidl, mapMsgNum2Date, bKnownSenderOnly, dtLastCheck, lstDataEmails, strLogInfo, strLastErrorEmailUID, strAccErrorLog, nPersonID, strEmail,&EmailImportThread::SetThreadActive,this)){
		//shut down client gracefully (connection still active)
		client.Quit();
		client.Disconnect();
	}

	//mark all emails as being read from this exact account
	lstDataEmails.setColValue("BEM_TO_EMAIL_ACCOUNT", strEmail);

	//lstDataEmails.Dump();

	//import all messages for a single account
	Status status; 
	FilterEmailsByContact(status, lstDataEmails, nPersonID);
	if(!status.IsOK()) return false;
	int nSkippedOrReplacedCnt = 0; 

	//lstDataEmails.Dump();

	ImportEmails(status, lstDataEmails, nPersonID, nSkippedOrReplacedCnt, false, true);
	if(!status.IsOK()) return false;

	//B.T. avoid to send push to empty ID's of emails (skipped)
	lstDataEmails.find("BEM_ID",0,false,false,true,true);
	lstDataEmails.deleteSelectedRows();

	//B.T. send push if ok:
	g_AppServer->SendNewEmailNotification(nPersonID,strEmail,lstDataEmails);

#ifdef _DEBUG
	qDebug() << QString("%3: POP3: Importing %1 emails, skipped or replaced %2").arg(lstDataEmails.getRowCount()).arg(nSkippedOrReplacedCnt).arg(strLogInfo);
	//lstDataEmails.Dump();
#endif
	return true;
}

//
// IMAP server type
//
bool EmailImportThread::HandleImapAccount(const QString &strHost, int nPort, bool bUseSSL, bool bUseSTLS, const QString &strUSER, const QString &strPass, int nPersonID, QDateTime &dtLastCheck, bool bKnownSenderOnly, QString strLogInfo, QString &strLastErrorEmailUID, QString &strAccErrorLog, QString strEmail)
{
    // Create an IMAP4 client
	mailpp::Imap4Client client;
#ifdef _DEBUG
	//mailpp::StdTraceOutput traceOut;
	//client.SetTraceOutput(&traceOut, false);
#endif

	/*
	if ((nPersonID==39 && strEmail=="bogdan@sokrates.ch") || strEmail=="user0000@everxconnect.com")
	{
		myTraceOutput tracer;
		client.SetTraceOutput(&tracer, false);
	}
	*/

	//login
	if(!ImapLogin(client, strHost, nPort, bUseSSL, bUseSTLS, strUSER, strPass, strLogInfo))
	{
		//B.T. notify user:
		g_AppServer->SendErrorMessage(nPersonID,"Failed to connect to the IMAP server. Check account settings.",1,strEmail);
		return false;
	}

	//get indexes for messages to be downloaded (filtered by date)
	QList<uint32_t> lstMessageIdx;
	std::map<int, std::string> mapMsgNum2Uidl;
	std::map<int, QDateTime> mapMsgNum2Date;
	if(!ImapGetMessageIdxByDate(client, dtLastCheck, lstMessageIdx, mapMsgNum2Uidl, mapMsgNum2Date, strLogInfo, nPersonID,&EmailImportThread::SetThreadActive,this))
		return false;

	//define IMPORT list:
	DbRecordSet lstDataEmails;	//email storage
	DbRecordSet set;
	set.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CE_COMM_ENTITY));
	lstDataEmails.copyDefinition(set, false);
	set.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_EMAIL));
	lstDataEmails.copyDefinition(set, false);
	lstDataEmails.addColumn(QVariant::String, "PROJECT_NAME");
	lstDataEmails.addColumn(QVariant::String, "PROJECT_CODE");
	lstDataEmails.addColumn(QVariant::String, "EMAIL_UNASSIGNED");
	lstDataEmails.addColumn(DbRecordSet::GetVariantType(), "ATTACHMENTS");
	lstDataEmails.addColumn(DbRecordSet::GetVariantType(), "CONTACTS");
	lstDataEmails.addColumn(DbRecordSet::GetVariantType(), "UAR_TABLE");
	lstDataEmails.addColumn(DbRecordSet::GetVariantType(), "GAR_TABLE");
	lstDataEmails.addColumn(QVariant::Int, "ATTACHMENTS_SIZE");

	//fetch emails from server
	//PLAN: if single message read was stuck, try at least to save the ones we managed to get successfully
	//		NOTE: this assumes that the list of messages was sorted by date so that the "last message date" is correct (my checks confirm this assumption)
	if(ImapFetchMessages(client, lstMessageIdx, bKnownSenderOnly, dtLastCheck, lstDataEmails, mapMsgNum2Uidl, mapMsgNum2Date, strLogInfo, strLastErrorEmailUID, strAccErrorLog, nPersonID,&EmailImportThread::SetThreadActive,this)){
		//shut down client gracefully (connection still active)
		client.Logout();
		client.Disconnect();
	}

	//mark all emails as being read from this exact account
	lstDataEmails.setColValue("BEM_TO_EMAIL_ACCOUNT", strEmail);

	//import all messages for a single account
	Status status; 
	FilterEmailsByContact(status, lstDataEmails, nPersonID);
	if(!status.IsOK()) return false;
	int nSkippedOrReplacedCnt = 0; 
	ImportEmails(status, lstDataEmails, nPersonID, nSkippedOrReplacedCnt, false, true);
	if(!status.IsOK()) return false;

	//B.T. avoid to send push to empty ID's of emails (skipped)
	lstDataEmails.find("BEM_ID",0,false,false,true,true);
	lstDataEmails.deleteSelectedRows();

	//B.T. send push if ok:
	g_AppServer->SendNewEmailNotification(nPersonID,strEmail,lstDataEmails);

#ifdef _DEBUG
	qDebug() << QString("%3: IMAP: Importing %1 emails, skipped or replaced %2").arg(lstDataEmails.getRowCount()).arg(nSkippedOrReplacedCnt).arg(strLogInfo);
	//lstDataEmails.Dump();
#endif
	return true;
}

bool Pop3Login(mailpp::Pop3Client &client, const QString &strHost, int nPort, bool bUseSSL, bool bUseSTLS, const QString &strUSER, const QString &strPass, QString strLogInfo)
{
	bool bWait = !bUseSSL || bUseSTLS;

	if(client.Connect(strHost.toLocal8Bit().constData(), nPort, bWait) < 0){
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("%1: Failed to connect to POP3 host. thread: %2").arg(strLogInfo).arg(ThreadIdentificator::GetCurrentThreadID()));
		return false;
	}
	//connected, check the reply code as well
	if(bWait && mailpp::Pop3Client::OK != client.ReplyCode()){
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("%2: POP3 error: %1, thread: %3").arg(client.ErrorMessage()).arg(strLogInfo).arg(ThreadIdentificator::GetCurrentThreadID()));
		return false;
	}

	//initiate SSL if needed
	if(bUseSSL)
	{
		if(bUseSTLS)
		{
			if(client.Stls() < 0){
				g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("%1: Failed to arrange TLS connection to POP3 host., thread: %2").arg(strLogInfo).arg(ThreadIdentificator::GetCurrentThreadID()));
				return false;
			}
			if(mailpp::Pop3Client::OK != client.ReplyCode()){
				g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("%2: POP3 error: %1, thread: %3").arg(client.ErrorMessage()).arg(strLogInfo).arg(ThreadIdentificator::GetCurrentThreadID()));
				client.Quit();
				client.Disconnect();
				return false;
			}
		}
		
	#if 1
		//ignore error about hostname mismatch within the certificate
		SSL_CTX	*ctx = SSL_CTX_new(SSLv23_client_method());	//TLSv1_client_method
		SSL *ssl = NULL;
		if(ctx){
			SSL_CTX_set_options(ctx, SSL_OP_ALL);
			SSL_CTX_set_cert_verify_callback(ctx, verify_callback2, NULL);
			ssl = SSL_new(ctx);
		}
		else{
			char szBuf[1000];
			ERR_error_string(ERR_get_error(), szBuf);
			qDebug() << "Error:" << ERR_get_error() << "(" << szBuf << ")";
			client.Disconnect();
			return false;
		}
		if( client.ConnectTlsOpenSSL(strHost.toLocal8Bit().constData(), ssl, true) < 0 &&
			mailpp::BAD_CERTIFICATE_NAME_ERROR != client.ErrorCode())

	#else
		if( client.ConnectTls(strHost.toLocal8Bit().constData(), true) < 0 &&
			mailpp::BAD_CERTIFICATE_NAME_ERROR != client.ErrorCode())
	#endif
		{
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("%3: Failed to establish TLS connection to POP3 host (code=%1, msg=%2). thread: %4").arg(client.ErrorCode()).arg(client.ErrorMessage()).arg(strLogInfo).arg(ThreadIdentificator::GetCurrentThreadID()));

			char szBuf[1000];
			ERR_error_string(ERR_get_error(), szBuf);
			qDebug() << "Error:" << ERR_get_error() << "(" << szBuf << ")";

			client.Disconnect();
			return false;
		}
		if(mailpp::Pop3Client::OK != client.ReplyCode()){
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("%2: POP3 error: %1, thread: %3").arg(client.ErrorMessage()).arg(strLogInfo).arg(ThreadIdentificator::GetCurrentThreadID()));
			client.Quit();
			client.Disconnect();
			return false;
		}
	}

	//login
	if(client.User(strUSER.toLocal8Bit().constData()) < 0){
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("%1: Failed to send POP3 command (USER). thread: %2").arg(strLogInfo).arg(ThreadIdentificator::GetCurrentThreadID()));
		client.Disconnect();
		return false;
	}
	if(mailpp::Pop3Client::OK != client.ReplyCode()){
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("%2: POP3 error: %1, thread: %3").arg(client.ErrorMessage()).arg(strLogInfo).arg(ThreadIdentificator::GetCurrentThreadID()));
		client.Quit();
		client.Disconnect();
		return false;
	}

	if(client.Pass(strPass.toLocal8Bit().constData()) < 0){
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("%1: Failed to send POP3 command (PASS). thread: %2").arg(strLogInfo).arg(ThreadIdentificator::GetCurrentThreadID()));
		client.Disconnect();
		return false;
	}
	if(mailpp::Pop3Client::OK != client.ReplyCode()){
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("%2: POP3 error: %1, thread: %3").arg(client.ErrorMessage()).arg(strLogInfo).arg(ThreadIdentificator::GetCurrentThreadID()));
		client.Quit();
		client.Disconnect();
		return false;
	}

	return true;
}

bool Pop3GetMessageIdxByDate(mailpp::Pop3Client &client, myProtocolListener &listener, const QDateTime &dtLastCheck, bool bUseUIDL, bool bUseTOP, QList<uint32_t> &lstMessageIdx, std::map<int, std::string> &mapMsgNum2Uidl, std::map<int, QDateTime> &mapMsgNum2Date, bool bFullScan, QString strLogInfo, int nPersonID, FN_SET_ACTIVE pSetThreadActive, EmailImportThread *pThread)
{
	lstMessageIdx.clear();
	mapMsgNum2Uidl.clear();

	//list messages available in the mailbox
	if(client.List() < 0){
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("%1: Failed to send POP3 command (LIST). thread: %2").arg(strLogInfo).arg(ThreadIdentificator::GetCurrentThreadID()));
		client.Disconnect();
		return false;
	}
	if(mailpp::Pop3Client::OK != client.ReplyCode()){
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("%2: POP3 error: %1, thread: 3%").arg(client.ErrorMessage()).arg(strLogInfo).arg(ThreadIdentificator::GetCurrentThreadID()));
		client.Quit();
		client.Disconnect();
		return false;
	}

	//for each message (line)
	std::vector<std::string> lstMsgs = listener.m_lstLines;
	int nMessages = lstMsgs.size();
	for(int i=0; i<nMessages; i++)
	{
		//parse message index
		int nMsgNum = -1;
		int nSize  = -1;
		int nRes = sscanf(lstMsgs[i].c_str(), "%d %d", &nMsgNum, &nSize);
		if(2 == nRes)
			lstMessageIdx << nMsgNum;
	}

	if(pSetThreadActive)(pThread->*pSetThreadActive)();
	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%2: Found total of %1 messages in POP3 mailbox, thread: %3").arg(lstMessageIdx.size()).arg(strLogInfo).arg(ThreadIdentificator::GetCurrentThreadID()));

	//check to see if we can get UUID of all the messages, without fetching it first
	if(bUseUIDL)
	{
		if(client.Uidl() < 0){
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0,QString("%1: Failed to send POP3 command (UIDL). thread: %2").arg(strLogInfo).arg(ThreadIdentificator::GetCurrentThreadID()));
			client.Disconnect();
			return false;
		}
		if(mailpp::Pop3Client::OK != client.ReplyCode()){
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("%2: POP3 error: %1, thread: %3").arg(client.ErrorMessage()).arg(strLogInfo).arg(ThreadIdentificator::GetCurrentThreadID()));
			//ignore error
		}

		//there should be only one line in the reply, but just in case
		std::vector<std::string> lstLines = listener.m_lstLines;
		int nUildLines = lstLines.size();
		for(int k=0; k<nUildLines; k++)
		{
			//store num -> UIDL mapping
			int nMsgNum = -1;
			char szUidl[100];
			int nRes = sscanf(lstLines[k].c_str(), "%d %s", &nMsgNum, szUidl);
			if(2 == nRes)
				mapMsgNum2Uidl[nMsgNum] = szUidl;
		}

		//TOFIX check if we want to load this message by UIDL (if supported)
		//B.T. this is irelevant here, as WriteEmail() already is doing this: just be sure to match last message date and start from that point...
		/*
		int nCnt = lstMessageIdx.size();
		for(int i=nCnt-1; i>=0; i--)
		{
			int nMsgNum = lstMessageIdx[i];
			QString strUIDL = mapMsgNum2Uidl[nMsgNum].c_str();
			if(!strUIDL.isEmpty()){
				//TOFIX check if this UIDL exists on server?, if so, continue to the next message
				QByteArray binUUID(strUIDL.toLocal8Bit().constData());
				if(FindEmailByUID(binUUID, nPersonID) >= 0){
					g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%2: Skip importing message idx=%1 (UID already exists in DB), thread: %3").arg(nMsgNum).arg(strLogInfo).arg(ThreadIdentificator::GetCurrentThreadID()));

					if(bFullScan){
						//full scan mode will continue checking messages before this one
						lstMessageIdx.removeAt(i);	
					}
					else{
						//if not full scan, then ignore all messages before the one that is already in the database
						g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%1: POP3 mode to skip all messages before the one that already exists in DB, thread: %2").arg(strLogInfo).arg(ThreadIdentificator::GetCurrentThreadID()));
						for(int j=0; j<=i; j++)
							lstMessageIdx.removeAt(0);
						break;
					}
				}
			}
		}*/
	}

	//quicker way to filter remaining emails by message date (fetch only header instead of entire email)
	if(bUseTOP)
	{
		//no pipelining		
		int nCnt = lstMessageIdx.size();
		for(int i=0; i<nCnt; i++)
		{
			if(pSetThreadActive)(pThread->*pSetThreadActive)();

			//just download headers for faster processing
			int nMsgNum = lstMessageIdx[i];
			/*
			if (nMsgNum!=8079)
			{
				continue;
			}
			*/
			if(client.Top(nMsgNum, 0) < 0){
				g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("%1: Failed to send POP3 command (TOP).thread: %2").arg(strLogInfo).arg(ThreadIdentificator::GetCurrentThreadID()));
				client.Disconnect();
				return false;
			}
			if(mailpp::Pop3Client::OK != client.ReplyCode()){
				g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("%2: POP3 error: %1 thread: %3").arg(client.ErrorMessage()).arg(strLogInfo).arg(ThreadIdentificator::GetCurrentThreadID()));
				client.Quit();
				client.Disconnect();
				return false;
			}

			//fetch the message contents
			std::vector<std::string> lstLines = listener.m_lstLines;
			std::string strHeaders;
			int nLines = lstLines.size();
			for(int j=0; j<nLines; j++){
				strHeaders += lstLines[j];
			}
			mimepp::Message msg(strHeaders.c_str());
			msg.parse();

			QString strSubject = DecodeField(msg.headers().subject().text().c_str());

			QDateTime dtMessage = QDateTime::fromTime_t(msg.headers().date().asUnixTime());
			if(dtLastCheck.isValid() && dtLastCheck >= dtMessage)
			{
				//skip importing message
				g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%4: Skip importing message idx=%1 (old message from: %2)[subject=%3], thread: %5").arg(nMsgNum).arg(dtMessage.toString("yyyy-MM-dd HH:mm:ss")).arg(strSubject).arg(strLogInfo).arg(ThreadIdentificator::GetCurrentThreadID()));
				lstMessageIdx.removeAt(i);
				i --; nCnt --;
			}
		}
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%3: POP3: %1 messages found being newer than date: %2.").arg(lstMessageIdx.size()).arg(dtLastCheck.toString("yyyy-MM-dd HH:mm:ss")).arg(strLogInfo));
	return true;
}

bool Pop3FetchSingleMessage(mailpp::Pop3Client &client, myProtocolListener &listener, int nMessageIdx, const char *szUIDL, bool bKnownSenderOnly, QDateTime &dtLastMessage, DbRecordSet &lstDataEmails, QString strLogInfo, QString &strLastErrorEmailUID, QString &strAccErrorLog, QDateTime dtRecv, int nPersonID, QString strAccountEmail)
{
	
	//fetch message body
	if(client.Retr(nMessageIdx) < 0){
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0,QString("%1: Failed to send POP3 command (RETR).").arg(strLogInfo));
		if(strLastErrorEmailUID == QString(strLastErrorEmailUID)){
			//this is a second time we detected issue with this email, fake as if we successfully imported it (increase the time)
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0,QString("%3: Failed to fetch message idx=%1, UID=%2 for the 2nd time, skipping.").arg(nMessageIdx).arg(szUIDL).arg(strLogInfo));
			dtLastMessage = dtRecv;
			strLastErrorEmailUID = "";	//clear marking
		}
		else{
			//this is a first time we detected issue with this email, mark it, but will retry in the next cycle
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0,QString("%3: Failed to fetch message idx=%1, UID=%2 for the 1st time, will retry later.").arg(nMessageIdx).arg(szUIDL).arg(strLogInfo));
			strLastErrorEmailUID = szUIDL;
		}
		client.Disconnect();
		return false;
	}
	if(mailpp::Pop3Client::OK != client.ReplyCode()){
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("%2: POP3 error: %1").arg(client.ErrorMessage()).arg(strLogInfo));
		if(strLastErrorEmailUID == QString(strLastErrorEmailUID)){
			//this is a second time we detected issue with this email, fake as if we successfully imported it (increase the time)
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0,QString("%3: Failed to fetch message idx=%1, UID=%2 for the 2nd time, skipping.").arg(nMessageIdx).arg(szUIDL).arg(strLogInfo));
			dtLastMessage = dtRecv;
			strLastErrorEmailUID = "";	//clear marking
		}
		else{
			//this is a first time we detected issue with this email, mark it, but will retry in the next cycle
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0,QString("%3: Failed to fetch message idx=%1, UID=%2 for the 1st time, will retry later.").arg(nMessageIdx).arg(szUIDL).arg(strLogInfo));
			strLastErrorEmailUID = szUIDL;
		}
		client.Quit();
		client.Disconnect();
		return false;
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0,QString("%1: Pop3FetchSingleMessage start parsing email").arg(strLogInfo));

	//fetch the message contents
	std::vector<std::string> lstLines = listener.m_lstLines;
	std::string strEmail;
	int nLines = lstLines.size();
	for(int j=0; j<nLines; j++){
		strEmail += lstLines[j];
	}
	int k=0;

	mimepp::Message msg(strEmail.c_str());
	msg.parse();

	g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0,QString("%1: Pop3FetchSingleMessage email parsed").arg(strLogInfo));

	//debug code, log

	if(g_AppServer->GetIniFile()->m_strLoggedEmails.indexOf(strAccountEmail) >= 0)
	{
		//ensure log dir exists
		bool bDirOK = true;
		QDir dir(QCoreApplication::applicationDirPath());
		if (!dir.exists("logged_emails"))
			bDirOK = dir.mkdir("logged_emails");

		if(!bDirOK){
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("%1: Failed to create logged_emails dir.").arg(strLogInfo));
		}
		else{
			//user + "_" + account + "_" + timestamp + txt
			QString strFile = QCoreApplication::applicationDirPath() + "/logged_emails/" + PersonNameFromID(nPersonID) + "_" + strAccountEmail + "_" + QDateTime::currentDateTime().toString("yyyyMMdd_HH-mm-ss.zzz") + ".txt";
			QFile file(strFile);
			if(!file.open(QIODevice::WriteOnly)) {
				g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("%1: Failed to create file [%2].").arg(strLogInfo).arg(strFile));
			}
			else{
				QByteArray byteFileContent(strEmail.c_str()); 
				int nErr=file.write(byteFileContent);
				if (nErr==-1){
					g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("%1: Failed to write to file [%2].").arg(strLogInfo).arg(strFile));
				}
				file.close();
			}
		}
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0,QString("%1: Pop3FetchSingleMessage start decoding fields").arg(strLogInfo));

	QString strTrueFrom;
	QString strFrom = DecodeField(msg.headers().from().getString().c_str());
	strTrueFrom = strFrom;
	QString strReplyTo = DecodeField(msg.headers().replyTo().getString().c_str());
	if(!strReplyTo.isEmpty()){
		strFrom = strReplyTo;
		g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%2: POP3: From field was filled from ReplyTo value (%1).").arg(strFrom).arg(strLogInfo));
	}

	QString strSubjectOrig(msg.headers().subject().text().c_str());
	g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0,QString("%1: Pop3FetchSingleMessage fields decoded, we are processing email with original subject: %2").arg(strLogInfo).arg(strSubjectOrig));
	QString strSubject = DecodeField(msg.headers().subject().text().c_str());
	g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0,QString("%1: Pop3FetchSingleMessage fields decoded, we are processing email with subject: %2").arg(strLogInfo).arg(strSubject));

	//test if email valid
	if(strFrom.indexOf('<') < 0){
		//no email inside, dump everything to file
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("%2: POP3: invalid From field detected [%1], subject [%3].").arg(strFrom).arg(strLogInfo).arg(strSubject));
	}

	//test if we should skip importing this mail
	if(bKnownSenderOnly)
	{
		//extract email part only
		int nStart = strFrom.indexOf('<');
		if(nStart >= 0){
			int nEnd = strFrom.indexOf('>', nStart);
			if(nEnd >= 0){
				QString strFromEmail = strFrom.mid(nStart+1, nEnd-nStart-1);
					
				//get contact flags: BCNT_DO_NOT_SYNC_MAIL, BCNT_SYNC_MAIL_AS_PRIV
				DbRecordSet lstContactInfo;
				lstContactInfo.addColumn(QVariant::Int, "BCME_CONTACT_ID");
				lstContactInfo.addColumn(QVariant::String, "BUS_CM_EMAIL");
				lstContactInfo.addRow();
				lstContactInfo.setData(0, "BUS_CM_EMAIL", strFromEmail);
				Status status;
				g_BusinessServiceSet->BusCommunication->GetContactFromEmail(status, lstContactInfo);
				if(status.IsOK()){
					_DUMP(lstContactInfo);
					if( 0 == lstContactInfo.getRowCount() || 
						lstContactInfo.getDataRef(0, "BCME_CONTACT_ID").toInt()<=0)
					{
						g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%3: Skip this message, email from unknown contact: %1 (subject=%2)").arg(strFromEmail).arg(strSubject).arg(strLogInfo));
						return true;	//no contact for this mail
					}
				}
			}
		}
	}

	//store the message into the database
	lstDataEmails.addRow();
	int nCurRow = lstDataEmails.getRowCount()-1;

	//write From info
	lstDataEmails.setData(nCurRow, "BEM_FROM", strFrom);
	lstDataEmails.setData(nCurRow, "BEM_REPLY_TO", strFrom);
	lstDataEmails.setData(nCurRow, "BEM_TRUE_FROM", strTrueFrom);
	

	//store subject
	lstDataEmails.setData(nCurRow, "BEM_SUBJECT", strSubject);
	
	//fix date to local date
	QDateTime dtRev;
	dtRev.setTime_t(msg.headers().received().date().asUnixTime());
	//dtRev.setTimeSpec(Qt::UTC);	//TOFIX?
	//QDateTime tmpDat=dtRev.toLocalTime();
	lstDataEmails.setData(nCurRow, "BEM_RECV_TIME", dtRev);

	lstDataEmails.setData(nCurRow, "BEM_OUTGOING", 0);

	g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0,QString("%1: Pop3FetchSingleMessage processing address list").arg(strLogInfo));

	//add to/cc/bcc addresses
	QSet<QString> setTmp;
	ProcessAddressList(msg.headers().to(),  lstDataEmails,	nCurRow, 1,	false, setTmp);
	ProcessAddressList(msg.headers().cc(),  lstDataEmails,	nCurRow, 2,	false, setTmp);
	ProcessAddressList(msg.headers().bcc(), lstDataEmails,	nCurRow, 3,	false, setTmp);

	bool bBodyPartSet = false;
	bool bBodyPartHTML = false;

	//store attachments
	DbRecordSet lstAttachments;
	lstAttachments.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_EMAIL_ATTACHMENT));
	if(Thunderbird_GetMailAttBodyRecursive(msg, lstDataEmails, lstAttachments, strLogInfo)){
		bBodyPartSet = true;
	}
	
	//lstAttachments.Dump();

	lstDataEmails.setData(nCurRow, "ATTACHMENTS", lstAttachments);
	lstDataEmails.setData(nCurRow, "ATTACHMENTS_SIZE", lstAttachments.getStoredByteSize());

	g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0,QString("%1: Pop3FetchSingleMessage processing attachments").arg(strLogInfo));

	//only if no body part found (not multipart)
	if(!bBodyPartSet){
		QString strCharset = "US-ASCII";
		if (msg.headers().hasField("Content-Type")) {
			const mimepp::MediaType& mtype = msg.headers().contentType();
			mimepp::String subtype = mtype.subtype();
			int numParams = mtype.numParameters();
			for (int i=0; i < numParams; ++i) {
				const mimepp::Parameter& param = msg.headers().contentType().parameterAt(i);
				const mimepp::String& name = param.name();
				if (0 == strcasecmp("charset", name.c_str())) {
					strCharset = param.value().c_str();
					break;
				}
			}
		}

		//transfer format decoding
		std::string strData;
		int cte = mimepp::TransferEncodingType::_7BIT;
		if (msg.headers().hasField("Content-Transfer-Encoding")) {
			cte = msg.headers().contentTransferEncoding().asEnum();
		}
		if (cte == mimepp::TransferEncodingType::QUOTED_PRINTABLE) {
			mimepp::QuotedPrintableDecoder dec;
			strData = dec.decode(msg.body().getString().c_str()).c_str();
		}
		else if (cte == mimepp::TransferEncodingType::BASE64) {
			mimepp::Base64Decoder dec;
			strData = dec.decode(msg.body().getString().c_str()).c_str();
		}
		else
			strData = msg.body().getString().c_str();

		//make sure to convert to the proper code page
		QString strRTF = QString::fromUtf8(strData.c_str());
		if(!strCharset.isEmpty()){
			QTextCodec *codec = QTextCodec::codecForName(strCharset.toLocal8Bit().constData());
			if(codec)
				strRTF = codec->toUnicode(strData.c_str());
		}

		if(!strRTF.isEmpty()){
			//can still be text-only, check mail type
			QString strContType = msg.headers().fieldBody("Content-Type").text().c_str();
			bool bHtml = (strRTF.indexOf("<html", 0, Qt::CaseInsensitive) >= 0) || (strRTF.indexOf("<!DOCTYPE", 0, Qt::CaseInsensitive) >= 0) || (strContType.indexOf("html") >= 0);
			lstDataEmails.setData(nCurRow, "BEM_BODY", strRTF);
			lstDataEmails.setData(nCurRow, "BEM_EMAIL_TYPE", (bHtml)? 1 : 0);
		}
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0,QString("%1: Pop3FetchSingleMessage UUID").arg(strLogInfo));

	//if UIDL not supported, calculate our own UUID and check if we want to load this message
	QString strUIDL = szUIDL;
	if(strUIDL.isEmpty())
	{
		Sha256Hash Hasher;
		QByteArray binInput(lstDataEmails.getDataRef(nCurRow, "BEM_BODY").toString().toLocal8Bit().constData());
		strUIDL = Hasher.GetHash(binInput).toHex();

		//B.T. 2014-07-30, skip this as this is happening already on Write() method...
		/*
		//check for skip once more
		if(!strUIDL.isEmpty())
		{
			QByteArray binUUID(strUIDL.toLocal8Bit().constData());
			if(FindEmailByUID(binUUID, nPersonID) >= 0){
				//email already exists in the database, skip importing once more
				g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0,"Mail UUID already exists in DB, do not import.");
				lstDataEmails.deleteRow(nCurRow);
				return true;
			}
		}
		*/
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0,QString("%1: Pop3FetchSingleMessage at end").arg(strLogInfo));

	//update date of the latest processed message 
	QDateTime dtMessage = QDateTime::fromTime_t(msg.headers().date().asUnixTime());
	if(!dtLastMessage.isValid() || dtLastMessage < dtMessage)
		dtLastMessage = dtMessage;	//store more recent date

	//store UUID entry
	QByteArray binData(strUIDL.toLocal8Bit());
	lstDataEmails.setData(nCurRow, "BEM_EXT_ID", binData);

	//store app name
	lstDataEmails.setData(nCurRow, "BEM_EXT_APP", "Sokrates");

	lstDataEmails.setData(nCurRow, "BEM_UNREAD_FLAG", 1);

	return true;
}

bool Pop3FetchMessages(mailpp::Pop3Client &client, myProtocolListener &listener, QList<uint32_t> &lstMessageIdx, std::map<int, std::string> &mapMsgNum2Uidl, std::map<int, QDateTime> &mapMsgNum2Date, bool bKnownSenderOnly, QDateTime &dtLastMessage, DbRecordSet &lstDataEmails, QString strLogInfo, QString &strLastErrorEmailUID, QString &strAccErrorLog, int nPersonID, QString strAccountEmail, FN_SET_ACTIVE pSetThreadActive, EmailImportThread *pThread)
{
	//for each message (line)
	int nCnt = lstMessageIdx.size();
	for(int i=0; i<nCnt; i++)
	{
		if(pSetThreadActive)(pThread->*pSetThreadActive)();
		//just download headers for faster processing
		int nMessageIdx = lstMessageIdx[i];
		QString strUIDL = mapMsgNum2Uidl[nMessageIdx].c_str();
		QDateTime dtRecv = mapMsgNum2Date[nMessageIdx];
		g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%2: POP3 Fetch message index:%1.").arg(nMessageIdx).arg(strLogInfo));

		if(!Pop3FetchSingleMessage(client, listener, nMessageIdx, strUIDL.toLocal8Bit().constData(), bKnownSenderOnly, dtLastMessage, lstDataEmails, strLogInfo, strLastErrorEmailUID, strAccErrorLog, dtRecv, nPersonID, strAccountEmail))
			return false;
	}	//for each message (line)

	return true;
}

bool ImapLogin(mailpp::Imap4Client &client, const QString &strHost, int nPort, bool bUseSSL, bool bUseSTLS, const QString &strUSER, const QString &strPass, QString strLogInfo)
{
	bool bWait = !bUseSSL || bUseSTLS;

	// Open connection to the server
	if(client.Connect(strHost.toLocal8Bit().constData(), nPort, bWait) < 0){
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("%1: Failed to connect to IMAP host.").arg(strLogInfo));
		return false;
	}

	//initiate SSL if needed
	if(bUseSTLS)
	{
		if(client.Starttls() < 0){
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("%1: Failed to arrange TLS connection to host.").arg(strLogInfo));
			return false;
		}
		if(mailpp::Imap4Client::OK != client.CommandStatus()){
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("%2: IMAP error: %1").arg(client.ErrorMessage()).arg(strLogInfo));
			client.Logout();
			client.Disconnect();
			return false;
		}
	}
	if(bUseSSL)
	{
		//
		//ignore error about hostname mismatch within the certificate
		//
#if 1
		SSL_CTX	*ctx = SSL_CTX_new(SSLv23_client_method());	//TLSv1_client_method
		SSL *ssl = NULL;
		if(ctx){
			SSL_CTX_set_options(ctx, SSL_OP_ALL);
			SSL_CTX_set_cert_verify_callback(ctx, verify_callback2, NULL);
			ssl = SSL_new(ctx);
		}
		else{
			char szBuf[1000];
			ERR_error_string(ERR_get_error(), szBuf);
			qDebug() << "Error:" << ERR_get_error() << "(" << szBuf << ")";
			client.Disconnect();
			return false;
		}
		if( client.ConnectTlsOpenSSL(strHost.toLocal8Bit().constData(), ssl, true) < 0 &&
			mailpp::BAD_CERTIFICATE_NAME_ERROR != client.ErrorCode())
#else
		if( client.ConnectTls(strPOP3.toLocal8Bit().constData(), true) < 0 &&
			mailpp::BAD_CERTIFICATE_NAME_ERROR != client.ErrorCode())
#endif
		{
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("%3: IMAP: Failed to establish TLS connection to host (code=%1, msg=%2).").arg(client.ErrorCode()).arg(client.ErrorMessage()).arg(strLogInfo));

			char szBuf[1000];
			ERR_error_string(ERR_get_error(), szBuf);
			qDebug() << "Error:" << ERR_get_error() << "(" << szBuf << ")";
			client.Disconnect();
			return false;
		}
	}

	// Send LOGIN command
	if( client.Login(strUSER.toLocal8Bit().constData(), strPass.toLocal8Bit().constData()) < 0)
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("%3: IMAP: Login error (code=%1, msg=%2).").arg(client.ErrorCode()).arg(client.ErrorMessage()).arg(strLogInfo));
		client.Disconnect();
		return false;
	}
	if(mailpp::Imap4Client::OK != client.CommandStatus()){
		QString strError;
		QString strSubError;
		if(mailpp::Imap4Client::BAD == client.CommandStatus()){
			int numResponses = client.NumResponses();
			for (int j=0; j<numResponses; ++j) {
				std::string strName = client.ResponseAt(j).Name();
				if (strcasecmp(client.ResponseAt(j).Name(), "BAD") == 0){
					const mailpp::BadResponse& resp = (const mailpp::BadResponse&) client.ResponseAt(j);
					strSubError = resp.ResponseText();
					break;
				}
			}
			strError = QString("%2: IMAP: Login error #2 'BAD' response from server (%1)").arg(strSubError).arg(strLogInfo);
		}
		else if(mailpp::Imap4Client::NO == client.CommandStatus()){
			int numResponses = client.NumResponses();
			for (int j=0; j<numResponses; ++j) {
				std::string strName = client.ResponseAt(j).Name();
				if (strcasecmp(client.ResponseAt(j).Name(), "NO") == 0){
					const mailpp::NoResponse& resp = (const mailpp::NoResponse&) client.ResponseAt(j);
					strSubError = resp.ResponseText();
					break;
				}
			}
			strError = QString("%2: IMAP: Login error #2 'NO' response from server (%1)").arg(strSubError).arg(strLogInfo);
		}
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, strError);
	#ifdef _DEBUG
		qDebug() << strError;
	#endif
		client.Logout();
		client.Disconnect();
		return false;
	}

	//TOFIX DEBUG: send List command to list available folders
    if(client.Select("INBOX") < 0)		//TOFIX hardcoded "INBOX"?
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("%3: IMAP: Select error (code=%1, msg=%2).").arg(client.ErrorCode()).arg(client.ErrorMessage()).arg(strLogInfo));
		client.Disconnect();
		return false;
	}
	if(mailpp::Imap4Client::OK != client.CommandStatus()){
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("%3: IMAP: Select error (code=%1, msg=%2).").arg(client.ErrorCode()).arg(client.ErrorMessage()).arg(strLogInfo));
		client.Logout();
		client.Disconnect();
		return false;
	}
	return true;
}

bool ImapGetMessageIdxByDate(mailpp::Imap4Client &client, const QDateTime &dtLastCheck, QList<uint32_t> &lstMessageIdx, std::map<int, std::string> &mapMsgNum2Uidl, std::map<int, QDateTime> &mapMsgNum2Date, QString strLogInfo, int nPersonID, FN_SET_ACTIVE pSetThreadActive, EmailImportThread *pThread)
{
	lstMessageIdx.clear();
	mapMsgNum2Uidl.clear();

	if(dtLastCheck.isValid())
	{
		//initial date search is imprecise
		QDateTime dtCheckSince = dtLastCheck;
		QString strSearch = QString("SENTSINCE %1").arg(DateToRfc2822(dtCheckSince.date()));
		g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%2: IMAP: Search IMAP messages since date: %1.").arg(DateToRfc2822(dtLastCheck.date())).arg(strLogInfo));
		
		if(client.Search(NULL, strSearch.toLocal8Bit().constData()) < 0)		//TOFIX hardcoded "INBOX"?
		{
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("%3: IMAP: Search error (code=%1, msg=%2).").arg(client.ErrorCode()).arg(client.ErrorMessage()).arg(strLogInfo));
			client.Disconnect();
			return false;
		}

		g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%1: IMAP: Search IMAP messages step1").arg(strLogInfo));

		if(mailpp::Imap4Client::OK != client.CommandStatus()){
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("%3: IMAP: Search error (code=%1, msg=%2).").arg(client.ErrorCode()).arg(client.ErrorMessage()).arg(strLogInfo));
			client.Logout();
			client.Disconnect();
			return false;
		}
		
		if(pSetThreadActive)(pThread->*pSetThreadActive)();
		g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%1: IMAP: Search IMAP messages step2").arg(strLogInfo));

		// Find the SEARCH response, which will tell us what messages match the search
		int numMessages = 0;
		int i;
		int numResponses = client.NumResponses();
		for (i=0; i < numResponses; ++i) {
			if(pSetThreadActive)(pThread->*pSetThreadActive)();
			if (strcasecmp(client.ResponseAt(i).Name(), "search") == 0) {
				const mailpp::SearchResponse& resp = (const mailpp::SearchResponse&)client.ResponseAt(i);
				numMessages = resp.NumMatches();
				for(int i=0; i<numMessages; i++){
					lstMessageIdx << resp.Match(i);
				}
				break;	//found it
			}
		}

		g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%2: IMAP: Search IMAP messages step3, found %1 messages").arg(lstMessageIdx.size()).arg(strLogInfo));

		//now fetch UIDs for those same messages
		if(client.UidSearch(NULL, strSearch.toLocal8Bit().constData()) < 0)		//TOFIX hardcoded "INBOX"?
		{
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("%3: IMAP: UidSearch error (code=%1, msg=%2).").arg(client.ErrorCode()).arg(client.ErrorMessage()).arg(strLogInfo));
			client.Disconnect();
			return false;
		}

		g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%1: IMAP: Search IMAP messages step4").arg(strLogInfo));

		if(mailpp::Imap4Client::OK != client.CommandStatus()){
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("%3: IMAP: UidSearch error (code=%1, msg=%2).").arg(client.ErrorCode()).arg(client.ErrorMessage()).arg(strLogInfo));
			//TOFIX Log NO or BAD response
			client.Logout();
			client.Disconnect();
			return false;
		}

		g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%1: IMAP: Search IMAP messages step5").arg(strLogInfo));

		// Find the SEARCH response, which will tell us what messages match the search
		int nMsgCnt = lstMessageIdx.size();
		numResponses = client.NumResponses();
		for (i=0; i < numResponses; ++i) {
			if(pSetThreadActive)(pThread->*pSetThreadActive)();
			if (strcasecmp(client.ResponseAt(i).Name(), "search") == 0) {
				const mailpp::SearchResponse& resp = (const mailpp::SearchResponse&)client.ResponseAt(i);
				numMessages = resp.NumMatches();
				//WARNING: assuming that the same search will return UIDs in the exact order as it returned message IDs
				Q_ASSERT(numMessages == nMsgCnt);
				if(numMessages == nMsgCnt){
					for(int i=0; i<numMessages; i++){
						char szUID[200];
						sprintf(szUID, "%d", resp.Match(i));
						int nMsgIdx = lstMessageIdx[i];
						mapMsgNum2Uidl[nMsgIdx] = szUID;
					}
				}
				break;	//found it
			}
		}

		g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%1: IMAP: Search IMAP messages ended").arg(strLogInfo));

	}
	else
	{
		if(pSetThreadActive)(pThread->*pSetThreadActive)();
		g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%1: List entire IMAP folder.").arg(strLogInfo));

		if(client.Examine("INBOX") < 0)		//TOFIX hardcoded "INBOX"?
		{
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("%3: IMAP: Examine error (code=%1, msg=%2).").arg(client.ErrorCode()).arg(client.ErrorMessage()).arg(strLogInfo));
			client.Disconnect();
			return false;
		}
		if(mailpp::Imap4Client::OK != client.CommandStatus()){
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("%3: IMAP: Examine error (code=%1, msg=%2).").arg(client.ErrorCode()).arg(client.ErrorMessage()).arg(strLogInfo));
			//TOFIX handle NO or BAD reply
			client.Logout();
			client.Disconnect();
			return false;
		}

		// Find the EXISTS response, which will tell us how many messages are in the folder.
		int numMessages = 0;
		int i;
		int numResponses = client.NumResponses();
		for (i=0; i < numResponses; ++i) {
			if(pSetThreadActive)(pThread->*pSetThreadActive)();
			if (strcasecmp(client.ResponseAt(i).Name(), "exists") == 0) {
				const mailpp::ExistsResponse& resp = (const mailpp::ExistsResponse&)client.ResponseAt(i);
				numMessages = resp.Number();
				for(int i=0; i<numMessages; i++){
					lstMessageIdx << i+1;	//message index starts with 1
				}
				break;	//found it
			}
		}

		//now fetch UIDs for those same messages
		if(client.UidSearch(NULL, "") < 0)		//TOFIX hardcoded "INBOX"?
		{
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("%3: IMAP: UidSearch error1 (code=%1, msg=%2).").arg(client.ErrorCode()).arg(client.ErrorMessage()).arg(strLogInfo));
			client.Disconnect();
			return false;
		}
		if(mailpp::Imap4Client::OK != client.CommandStatus()){
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("%3: IMAP: UidSearch error2 (code=%1, msg=%2).").arg(client.ErrorCode()).arg(client.ErrorMessage()).arg(strLogInfo));
			//TOFIX handle NO or BAD reply
			client.Logout();
			client.Disconnect();
			return false;
		}

		// Find the SEARCH response, which will tell us what messages match the search
		int nMsgCnt = lstMessageIdx.size();
		numResponses = client.NumResponses();
		for (i=0; i < numResponses; ++i) {
			if(pSetThreadActive)(pThread->*pSetThreadActive)();
			if (strcasecmp(client.ResponseAt(i).Name(), "search") == 0) {
				const mailpp::SearchResponse& resp = (const mailpp::SearchResponse&)client.ResponseAt(i);
				numMessages = resp.NumMatches();
				//WARNING: assuming that the same search will return UIDs in the exact order as it returned message IDs
				Q_ASSERT(numMessages == nMsgCnt);
				if(numMessages == nMsgCnt){
					for(int i=0; i<numMessages; i++){
						char szUID[200];
						sprintf(szUID, "%d", resp.Match(i));
						int nMsgIdx = lstMessageIdx[i];
						mapMsgNum2Uidl[nMsgIdx] = szUID;
					}
				}
				break;	//found it
			}
		}
	}

	//for each message
	int numMessages = lstMessageIdx.size();
	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%2: Found %1 matching messages in IMAP mailbox folder.").arg(numMessages).arg(strLogInfo));
	if(pSetThreadActive)(pThread->*pSetThreadActive)();

	//B.T. this is irelevant here, as WriteEmail() already is doing this: just be sure to match last message date and start from that point...
	/*
	//1st clear all messages that are already in the database (check UIDs)
	for(int i=numMessages-1; i>=0; i--)
	{
		int nMsgNum = lstMessageIdx[i];
		QString strUIDL = mapMsgNum2Uidl[nMsgNum].c_str();
		if(!strUIDL.isEmpty()){
			//TOFIX check if this UIDL exists on server?, if so, continue to the next message
			QByteArray binUUID(strUIDL.toLocal8Bit().constData());
			if(FindEmailByUID(binUUID, nPersonID) >= 0){
				g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%2: Skip importing message idx=%1 (UID already exists in DB)").arg(nMsgNum).arg(strLogInfo));

				//if(bFullScan){
					//full scan mode will continue checking messages before this one
					lstMessageIdx.removeAt(i);	
				//}
				//else{
					//if not full scan, then ignore all messages before the one that is already in the database
					//g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%1: POP3 mode to skip all messages before the one that already exists in DB").arg(strLogInfo));
					//for(int j=0; j<=i; j++)
					//	lstMessageIdx.removeAt(0);
					//break;
				//}
			}
		}
	}
	*/

	numMessages = lstMessageIdx.size();	//refresh count
	
	if(numMessages > 0)
	{
		if(pSetThreadActive)(pThread->*pSetThreadActive)();

		//now prepare to fetch dates for the survivind messages 
		QString strMsgSet;
		for(int i=0; i<numMessages; i++){
			strMsgSet += QString("%1").arg(lstMessageIdx[i]);
			if(i < numMessages-1)
				strMsgSet += ",";
		}
		//fetch FULL OR ENVELOPE can be stuck sometimes, so first try to fetch only message date for quick filtering
		//FIX: do not use INTERNALDATE, that is not related to message received time
		if(client.Fetch(strMsgSet.toLatin1().constData(), "BODY.PEEK[HEADER.FIELDS (Date Subject)]") < 0)
		{
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("%3: IMAP: Fetch error #1 (code=%1, msg=%2).").arg(client.ErrorCode()).arg(client.ErrorMessage()).arg(strLogInfo));
			client.Disconnect();
			return false;
		}
		if(mailpp::Imap4Client::OK != client.CommandStatus()){
			QString strError;
			QString strSubError;
			if(mailpp::Imap4Client::BAD == client.CommandStatus()){
				int numResponses = client.NumResponses();
				for (int j=0; j<numResponses; ++j) {
					std::string strName = client.ResponseAt(j).Name();
					if (strcasecmp(client.ResponseAt(j).Name(), "BAD") == 0){
						const mailpp::BadResponse& resp = (const mailpp::BadResponse&) client.ResponseAt(j);
						strSubError = resp.ResponseText();
						break;
					}
				}
				strError = QString("%2: IMAP: Fetch error #2 'BAD' response from server (%1)").arg(strSubError).arg(strLogInfo);
			}
			else if(mailpp::Imap4Client::NO == client.CommandStatus()){
				int numResponses = client.NumResponses();
				for (int j=0; j<numResponses; ++j) {
					std::string strName = client.ResponseAt(j).Name();
					if (strcasecmp(client.ResponseAt(j).Name(), "NO") == 0){
						const mailpp::NoResponse& resp = (const mailpp::NoResponse&) client.ResponseAt(j);
						strSubError = resp.ResponseText();
						break;
					}
				}
				strError = QString("%2: IMAP: Fetch error #2 'NO' response from server (%1)").arg(strSubError).arg(strLogInfo);
			}
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, strError);
			client.Logout();
			client.Disconnect();
			return false;
		}
		int numResponses = client.NumResponses();
		for (int j=0; j<numResponses; ++j) 
		{
			if(pSetThreadActive)(pThread->*pSetThreadActive)();

			// Find the FETCH responses
			if (strcasecmp(client.ResponseAt(j).Name(), "fetch") == 0) 
			{
				const mailpp::FetchResponse& resp = (const mailpp::FetchResponse&) client.ResponseAt(j);
				uint32_t nSequence = resp.SequenceNumber();
				QDateTime dtReceived;
				QString strSubject; 
				if(resp.BodySection()){
					QString strData = resp.BodySection()->Content();
					if(!strData.isEmpty())
					{
						//extract subject
						//warning, some subjects seem to have more than one line!!!!
						int nSubjStart = strData.indexOf("Subject: ", 0, Qt::CaseInsensitive);
						if(nSubjStart >= 0){
							nSubjStart += 9;
							//int nEnd = strData.indexOf("\r\n", nSubjStart);
							int nEnd = strData.indexOf("\r\nDate:", nSubjStart);
							if(nEnd >= 0)
								strSubject = DecodeField(strData.mid(nSubjStart, nEnd-nSubjStart).toLocal8Bit().constData());
						}

						//extract date
						int nDateStart = strData.indexOf("Date: ", 0, Qt::CaseInsensitive);
						if(nDateStart >= 0){
							nDateStart += 6;
							int nEnd = strData.indexOf("\r\n", nDateStart);
							if(nEnd >= 0){
								QString strDate = strData.mid(nDateStart, nEnd-nDateStart);
								dtReceived = ParseRfc2822DateTime(strDate);
							}
						}
					}
				}
				else{
					g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("%2: Failed to fetch Date and subject, message idx=%1").arg(nSequence).arg(strLogInfo));
					continue;
				}
				if(dtReceived.isValid() && dtLastCheck >= dtReceived)
				{
					g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%4: Skip importing old message idx=%1 (date: %2) [Subject:%3]").arg(nSequence).arg(dtReceived.toString("yyyy-MM-dd HH:mm:ss")).arg(strSubject).arg(strLogInfo));
					int nIdx = lstMessageIdx.indexOf(nSequence);
					if(nIdx >= 0)
						lstMessageIdx.removeAt(nIdx);
					else
						g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("%2: Failed finding imap message index idx=%1").arg(nSequence).arg(strLogInfo));
				}
				else {
					g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%4: Mark new message for import idx=%1 (date: %2) [Subject:%3]").arg(nSequence).arg(dtReceived.toString("yyyy-MM-dd HH:mm:ss")).arg(strSubject).arg(strLogInfo));
					mapMsgNum2Date[nSequence] = dtReceived;
				}
			}
		}
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%3: IMAP: %1 IMAP messages found being newer than date: %2.").arg(lstMessageIdx.size()).arg(dtLastCheck.toString("yyyy-MM-dd HH:mm:ss")).arg(strLogInfo));
	return true;
}

//Example (subject) field to decode: 
//"Fwd: WG: =?UTF-8?B?R3LDvG5lcyBMaWNodCBmw7xyIEUtVm90aW5nLCBSU0Egaw==?= =?UTF-8?B?b29wZXJpZXJ0ZSBtaXQgTlNBLCBMb2V3ZS1SZXR0dW5nIHNjaGVpdGVydA==?="
//"=?UTF-8?Q?Ihr_pers=c3=b6nlicher_XING-Newsletter_11_|_2014?=" (ne radi s mimepp)
QString DecodeField(const char *szField)
{
	//there can be more than one encoded section in the code
	QString strBeforeChunk;
	QString strChunk;
	QString strAfterChunk;
	QString strRawData(szField);

	//B.T. 2014-08-21: this is all wrong, find "?=" in string and that's first delimiter:
	int nSubEnc = strRawData.indexOf("=?");
	if (nSubEnc>0)
	{
		strBeforeChunk=strRawData.left(nSubEnc); 
		strRawData = strRawData.mid(strBeforeChunk.length());
		strBeforeChunk=strBeforeChunk.trimmed(); //if space between chunks, skip it
	}

	nSubEnc = strRawData.indexOf("?=");
	if (nSubEnc>0)
	{
		strChunk = strRawData.mid(0, nSubEnc+2);
		strAfterChunk = strRawData.mid(nSubEnc+2);
	}
	else
		strChunk = strRawData;
	
	/*
	int nSubEnc = strRawData.indexOf("=?", 1);

	//skip ending mark "=?="
	if(nSubEnc > 0){
		//if(nSubEnc+2 < strRawData.length() && strRawData.at(nSubEnc+2) == '=')
		//	nSubEnc = strRawData.indexOf("=?", nSubEnc+1);
		if(nSubEnc+2 < strRawData.length() && strRawData.at(nSubEnc+2) == '=')
			nSubEnc += 3;
		else 
			nSubEnc += 2;
	}
	//break string into pieces
	if(nSubEnc > 0){
		strChunk = strRawData.mid(0, nSubEnc);
		strAfterChunk = strRawData.mid(nSubEnc);
	}
	else
		strChunk = strRawData;
	*/

	//decode current part
	mimepp::EncodedWord encoded(strChunk.toLocal8Bit().constData());
	encoded.parse();
	QString strData = encoded.decodedText().c_str();
	QString strCharset = encoded.charset().c_str();
	if(!strCharset.isEmpty()){
		QTextCodec *codec = QTextCodec::codecForName(encoded.charset().c_str());
		if(codec)
			strData = codec->toUnicode(encoded.decodedText().c_str());
	}
	if(strData == strChunk){
		//mimepp failed to decode the chunk, lets do it ourselves
		int nCharsetEnd = strChunk.indexOf("?", 3);
		if(nCharsetEnd >= 0){
			//charset string exists
			strCharset = strChunk.mid(2, nCharsetEnd-2);

			//strip charset part
			strChunk = strChunk.right(strChunk.size()-nCharsetEnd);
			if(strChunk.size() > 5)	//"?Q?" prefix and "=?" suffix
			{
				bool bDecodingAlgorithmOK = (strChunk.at(1) == "B" || strChunk.at(1) == "b" || strChunk.at(1) == "Q" || strChunk.at(1) == "q");
				if(bDecodingAlgorithmOK)
				{
					bool bDecodeBase64 = (strChunk.at(1) == "B" || strChunk.at(1) == "b");
					//strip prefix and suffix
					strChunk = strChunk.right(strChunk.size()-3);
					strChunk = strChunk.left(strChunk.size()-2);

					if (bDecodeBase64) {
						mimepp::Base64Decoder dec;
						strData = dec.decode(strChunk.toLocal8Bit().constData()).c_str();
						if(!strCharset.isEmpty()){
							QTextCodec *codec = QTextCodec::codecForName(strCharset.toLocal8Bit().constData());
							if(codec)
								strData = codec->toUnicode(dec.decode(strChunk.toLocal8Bit().constData()).c_str());
						}
					}
					else {
						mimepp::QuotedPrintableDecoder dec;
						strData = dec.decode(strChunk.toLocal8Bit().constData()).c_str();
						if(!strCharset.isEmpty()){
							QTextCodec *codec = QTextCodec::codecForName(strCharset.toLocal8Bit().constData());
							if(codec)
								strData = codec->toUnicode(dec.decode(strChunk.toLocal8Bit().constData()).c_str());
						}
					}
				}
			}
		}
	}


	//recursively decode the rest of the data
	if(!strAfterChunk.isEmpty()){
		strData += DecodeField(strAfterChunk.toLocal8Bit().constData());
	}

	return strBeforeChunk+strData;
}

QString DecodeEmail(const mailpp::AddressData &adr)
{
	QString strEmail = DecodeField(adr.PersonalName());
	if(!strEmail.isEmpty())
		strEmail += " ";
	strEmail += "<"; 
	strEmail += DecodeField(adr.MailboxName());
	strEmail += "@"; 
	strEmail += DecodeField(adr.HostName());
	strEmail += ">";
	return strEmail;
}

bool ImapFetchSingleMessage(mailpp::Imap4Client &client, uint32_t nMessageIdx, const char *szUID, bool bKnownSenderOnly, QDateTime &dtLastMessage, DbRecordSet &lstDataEmails, QString strLogInfo, QString &strLastErrorEmailUID, QString &strAccErrorLog, QDateTime dtRecv, int nPersonID)
{
	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%2: IMAP: Start fetching message idx=%1").arg(nMessageIdx).arg(strLogInfo));

	// Send a FETCH with the FULL macro (equivalent to (FLAGS INTERNALDATE RFC822.SIZE ENVELOPE BODY))
	if(client.Fetch(nMessageIdx, "FULL") < 0)
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("%3: IMAP: Fetch error #3 (code=%1, msg=%2).").arg(client.ErrorCode()).arg(client.ErrorMessage()).arg(strLogInfo));
		if(strLastErrorEmailUID == QString(strLastErrorEmailUID)){
			//this is a second time we detected issue with this email, fake as if we successfully imported it (increase the time)
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0,QString("%3: Failed to fetch message idx=%1, UID=%2 for the 2nd time, skipping.").arg(nMessageIdx).arg(szUID).arg(strLogInfo));
			dtLastMessage = dtRecv;
			strLastErrorEmailUID = "";	//clear marking
		}
		else{
			//this is a first time we detected issue with this email, mark it, but will retry in the next cycle
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0,QString("%3: Failed to fetch message idx=%1, UID=%2 for the 1st time, will retry later.").arg(nMessageIdx).arg(szUID).arg(strLogInfo));
			strLastErrorEmailUID = szUID;
		}
		//TOFIX prepend error into strAccErrorLog
		client.Disconnect();
		return false;
	}
	if(mailpp::Imap4Client::OK != client.CommandStatus()){
		if(strLastErrorEmailUID == QString(strLastErrorEmailUID)){
			//this is a second time we detected issue with this email, fake as if we successfully imported it (increase the time)
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0,QString("%3: Failed to fetch message idx=%1, UID=%2 for the 2nd time, skipping.").arg(nMessageIdx).arg(szUID).arg(strLogInfo));
			dtLastMessage = dtRecv;
			strLastErrorEmailUID = "";	//clear marking
		}
		else{
			//this is a first time we detected issue with this email, mark it, but will retry in the next cycle
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0,QString("%3: Failed to fetch message idx=%1, UID=%2 for the 1st time, will retry later.").arg(nMessageIdx).arg(szUID).arg(strLogInfo));
			strLastErrorEmailUID = szUID;
		}
		//TOFIX prepend error into strAccErrorLog
		QString strError;
		QString strSubError;
		if(mailpp::Imap4Client::BAD == client.CommandStatus()){
			int numResponses = client.NumResponses();
			for (int j=0; j<numResponses; ++j) {
				std::string strName = client.ResponseAt(j).Name();
				if (strcasecmp(client.ResponseAt(j).Name(), "BAD") == 0){
					const mailpp::BadResponse& resp = (const mailpp::BadResponse&) client.ResponseAt(j);
					strSubError = resp.ResponseText();
					break;
				}
			}
			strError = QString("%2: IMAP: Fetch error #4 'BAD' response from server (%1)").arg(strSubError).arg(strLogInfo);
		}
		else if(mailpp::Imap4Client::NO == client.CommandStatus()){
			int numResponses = client.NumResponses();
			for (int j=0; j<numResponses; ++j) {
				std::string strName = client.ResponseAt(j).Name();
				if (strcasecmp(client.ResponseAt(j).Name(), "NO") == 0){
					const mailpp::NoResponse& resp = (const mailpp::NoResponse&) client.ResponseAt(j);
					strSubError = resp.ResponseText();
					break;
				}
			}
			strError = QString("%2: IMAP: Fetch error #4 'NO' response from server (%1)").arg(strSubError).arg(strLogInfo);
		}
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, strError);
	#ifdef _DEBUG
		qDebug() << strError;
	#endif
		client.Logout();
		client.Disconnect();
		return false;
	}

	// Process each FETCH response -- one for each message in the folder.
	// Extract the information we want to print for our listing.
	int numResponses = client.NumResponses();
	for (int j=0; j<numResponses; ++j) 
	{
		// Find the FETCH responses
		if (strcasecmp(client.ResponseAt(j).Name(), "fetch") == 0) 
		{
			const mailpp::FetchResponse& resp = (const mailpp::FetchResponse&) client.ResponseAt(j);

			// Get the ENVELOPE structure
			const mailpp::EnvelopeData* env = resp.Envelope();
			// Get the FLAGS list
			const mailpp::FlagsData* flags = resp.Flags();

			// Then extract the desired information and print a summary line
			if (env != 0 && flags != 0) 
			{
				//make sure that the email is correctly decoded

				QString strSubjectOrig(env->Subject());
				g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0,QString("%1: IMAP: we are processing email with original subject: %2").arg(strLogInfo).arg(strSubjectOrig));

				QString strSubject = DecodeField(env->Subject());
				qDebug() << "Subject: " << strSubject;

				QString strTrueFrom;	//MB 2013.03.05.

				QString strFrom;
				Q_ASSERT(env->NumFrom() > 0);
				if(env->NumFrom() > 0){
					strFrom = DecodeEmail(env->From(0));
					qDebug() << "From: " << strFrom;
					strTrueFrom = strFrom;
				}
				if(env->NumReplyTo() > 0){
					strFrom = DecodeEmail(env->ReplyTo(0));
					g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%2: IMAP: From field was filled from ReplyTo value (%1).").arg(strFrom).arg(strLogInfo));
				}

				//test if we should skip importing this mail
				if(bKnownSenderOnly)
				{
					//get contact flags: BCNT_DO_NOT_SYNC_MAIL, BCNT_SYNC_MAIL_AS_PRIV
					DbRecordSet lstContactInfo;
					lstContactInfo.addColumn(QVariant::Int, "BCME_CONTACT_ID");
					lstContactInfo.addColumn(QVariant::String, "BUS_CM_EMAIL");
					lstContactInfo.addRow();
					lstContactInfo.setData(0, "BUS_CM_EMAIL", strFrom);
					Status status;
					g_BusinessServiceSet->BusCommunication->GetContactFromEmail(status, lstContactInfo);
					if(status.IsOK()){
						_DUMP(lstContactInfo);
						if( 0 == lstContactInfo.getRowCount() || 
							lstContactInfo.getDataRef(0, "BCME_CONTACT_ID").toInt()<=0)
						{
							g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%2: Skip this message, email from unknown contact: %1").arg(strFrom).arg(strLogInfo));
							break;	//no contact for this mail
						}
					}
				}

				g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%2: IMAP: fetching message step 1, idx=%1").arg(nMessageIdx).arg(strLogInfo));

				//RFC 2822 format: "Tue, 13 Nov 2012 12:09:28 +0100"
				QString strDate = env->Date();
				QDateTime dtReceived = ParseRfc2822DateTime(strDate);

				//store the message into the database
				lstDataEmails.addRow();
				int nCurRow = lstDataEmails.getRowCount()-1;

				//store message time
				lstDataEmails.setData(nCurRow, "BEM_RECV_TIME", dtReceived);

				//store UUID entry
				unsigned nUID = resp.Uid();
				if(0 != nUID){
					QByteArray binData((const char *)&nUID, sizeof(nUID));
					lstDataEmails.setData(nCurRow, "BEM_EXT_ID", binData);
				}

				//write From info
				lstDataEmails.setData(nCurRow, "BEM_FROM", strFrom);
				lstDataEmails.setData(nCurRow, "BEM_REPLY_TO", strFrom);
				lstDataEmails.setData(nCurRow, "BEM_TRUE_FROM", strTrueFrom);
				
				//write subject
				lstDataEmails.setData(nCurRow, "BEM_SUBJECT", strSubject);
		
				lstDataEmails.setData(nCurRow, "BEM_OUTGOING", 0);

				g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%2: IMAP: fetching message step 2, idx=%1").arg(nMessageIdx).arg(strLogInfo));

				//add to/cc/bcc addresses
				int nNumTo = env->NumTo();
				if(nNumTo > 0){
					for(int k=0; k<nNumTo; k++){
						QString strEmail = DecodeEmail(env->To(k));
						QString strValue = lstDataEmails.getDataRef(nCurRow, "BEM_TO").toString();
						if(!strValue.isEmpty())
							strValue += ";";
						strValue += strEmail;
						lstDataEmails.setData(nCurRow, "BEM_TO", strValue);
					}
					qDebug() << "To: " << lstDataEmails.getDataRef(nCurRow, "BEM_TO").toString();
				}
				int nNumCc = env->NumCc();
				if(nNumCc > 0){
					for(int k=0; k<nNumCc; k++){
						QString strEmail = DecodeEmail(env->Cc(k));
						QString strValue = lstDataEmails.getDataRef(nCurRow, "BEM_CC").toString();
						if(!strValue.isEmpty())
							strValue += ";";
						strValue += strEmail;
						lstDataEmails.setData(nCurRow, "BEM_CC", strValue);
					}
					qDebug() << "Cc: " << lstDataEmails.getDataRef(nCurRow, "BEM_CC").toString();
				}
				int nNumBcc = env->NumBcc();
				if(nNumBcc > 0){
					for(int k=0; k<nNumBcc; k++){
						QString strEmail = DecodeEmail(env->Bcc(k));
						QString strValue = lstDataEmails.getDataRef(nCurRow, "BEM_BCC").toString();
						if(!strValue.isEmpty())
							strValue += ";";
						strValue += strEmail;
						lstDataEmails.setData(nCurRow, "BEM_BCC", strValue);
					}
					qDebug() << "Bcc: " << lstDataEmails.getDataRef(nCurRow, "BEM_BCC").toString();
				}

				g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%2: IMAP: fetching message step 3, idx=%1").arg(nMessageIdx).arg(strLogInfo));

				//get multipart structure and message body
				Message message;
				processFetchFull(client, nMessageIdx, message);

				//store body
				QString strRTF(message.memo.c_str());
				if(!message.memoCharset.empty()){
					//decode from different charsets if needed
					QTextCodec *codec = QTextCodec::codecForName(message.memoCharset.c_str());
					if(codec)
						strRTF = codec->toUnicode(message.memo.c_str());
				}
				bool bHtml = message.bIsHtml; //(strRTF.indexOf("<html", 0, Qt::CaseInsensitive) >= 0) || (strRTF.indexOf("<!DOCTYPE", 0, Qt::CaseInsensitive) >= 0);
				lstDataEmails.setData(nCurRow, "BEM_BODY", strRTF);
				lstDataEmails.setData(nCurRow, "BEM_EMAIL_TYPE", (bHtml)? 1 : 0);

				//fetch and store attachments
				DbRecordSet lstAttachments;
				lstAttachments.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_EMAIL_ATTACHMENT));
				int nNumAttachments = message.numAttachments;
				g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%2: IMAP: fetching message step 4, attachments: %3, idx=%1").arg(nMessageIdx).arg(strLogInfo).arg(nNumAttachments));
				for(int z=0; z<nNumAttachments; z++)
				{
					QString strAttName(message.attachments[z]->filename.c_str());
					g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%2: IMAP: fetching message step 4.1, attachment name: %3, idx=%1").arg(nMessageIdx).arg(strLogInfo).arg(strAttName));
					strAttName = DecodeField(strAttName.toUtf8().constData());
					if(strAttName.isEmpty())
					{
						//strAttName="BT_unknown_attachment_name";
						int nBodyPart = QString(message.attachments[z]->bodyPartRef.c_str()).toInt();
						g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%2: IMAP: fetching message step 4.5, attachment name is empty, trying to parse whole body, target part: %3, idx=%1").arg(nMessageIdx).arg(strLogInfo).arg(nBodyPart));
						strAttName=ParseFullEmailBodyForAttachmentName(client,nMessageIdx,nBodyPart);
						if (strAttName.isEmpty())
							g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%2: IMAP: fetching message step 4.6, attachment name is still empty, skipping import of attachment, idx=%1").arg(nMessageIdx).arg(strLogInfo));
					}
					
					if(!strAttName.isEmpty())	//can be body part instead of real attachment
					{
						//an attachment
						Message attMessage;	//just to store the contents here
						attMessage.memoEncoding = message.attachments[z]->contentEncoding;
						std::string bodySection = std::string("(BODY[") + message.attachments[z]->bodyPartRef + std::string("])");
						//QString strX(bodySection.c_str());
						g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%2: IMAP: fetching attachment step 4.2, attachment name: %3, idx=%1").arg(nMessageIdx).arg(strLogInfo).arg(strAttName));
						client.Fetch(nMessageIdx, bodySection.c_str());
						g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%2: IMAP: fetching attachment, start processing step 4.2.1, attachment name: %3, idx=%1").arg(nMessageIdx).arg(strLogInfo).arg(strAttName));
						processFetchBodySection(client, attMessage,strLogInfo);
						g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%2: IMAP: fetching attachment, end processing step 4.2.1, attachment name: %3, idx=%1").arg(nMessageIdx).arg(strLogInfo).arg(strAttName));


						//if the attachment is "winmail.dat" we must extract one or more attachments from within its contents
						bool bParsed = false;
						if(0 == strcasecmp("winmail.dat", strAttName.toLocal8Bit().constData()))
						{
							g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%2: IMAP: detected winmail.dat step 4.3, attachment name: %3, idx=%1").arg(nMessageIdx).arg(strLogInfo).arg(strAttName));

							mimepp::TnefDecoder dec(mimepp::String(attMessage.memo.c_str(), attMessage.memo.size()));	//binary string
							if(0 == dec.parse())
							{
								bParsed = true;
								//one winmail.dat (TNEF) attachment can have more than one attachment inside
								unsigned nCnt = dec.numAttachments();
								g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%2: IMAP: parsing winmail.dat attachments (packing %1 items inside; buffer size=%3).").arg(nCnt).arg(strLogInfo).arg(attMessage.memo.size()));

								if(0 == nCnt){
									//dump invalid winmail.dat to file

									//ensure log dir exists
									bool bDirOK = true;
									QDir dir(QCoreApplication::applicationDirPath());
									if (!dir.exists("logged_emails"))
										bDirOK = dir.mkdir("logged_emails");

									if(!bDirOK){
										g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("%1: Failed to create logged_emails dir.").arg(strLogInfo));
									}
									else{
										//user + "_" + account + "_" + timestamp + txt
										QString strFile = QCoreApplication::applicationDirPath() + "/logged_emails/" + PersonNameFromID(nPersonID) + "_winmaildat_" + QDateTime::currentDateTime().toString("yyyyMMdd_HH-mm-ss.zzz") + ".txt";
										g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%2: IMAP: dump invalid winmail.dat attachment to file (%1).").arg(strFile).arg(strLogInfo));
										QFile file(strFile);
										if(!file.open(QIODevice::WriteOnly)) {
											g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("%1: Failed to create file [%2].").arg(strLogInfo).arg(strFile));
										}
										else{
											QByteArray byteFileContent(attMessage.memo.c_str(), attMessage.memo.size()); //binary content	
											int nErr=file.write(byteFileContent);
											if (nErr==-1){
												g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("%1: Failed to write to file [%2].").arg(strLogInfo).arg(strFile));
											}
											file.close();
										}
									}
								}
								else{
									for(int j=0; j<nCnt; j++){
										mimepp::TnefAttachment &att = dec.attachmentAt(j);

										lstAttachments.addRow();
										int nRow = lstAttachments.getRowCount()-1;
										lstAttachments.setData(nRow, "BEA_NAME", att.fileName().c_str());
					
										int nLen = att.content().length();
										QByteArray att1(att.content().c_str(), nLen);
										//B.T.: compress attachment, damn it:
										att1=qCompress(att1,1); //min compression
										lstAttachments.setData(nRow, "BEA_CONTENT", att1);
									}
								}
							}
							else{
								g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0,QString("%1: IMAP: Failed to parse Winmail.dat attachment (buffer size=%2)").arg(strLogInfo).arg(attMessage.memo.size()));
							}
						}

						g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%2: IMAP: fetching message step 5 idx=%1").arg(nMessageIdx).arg(strLogInfo).arg(nNumAttachments));

						if(!bParsed)
						{
							lstAttachments.addRow();
							int nRow = lstAttachments.getRowCount()-1;
							lstAttachments.setData(nRow, "BEA_NAME", strAttName);

							//Attachment contents
							int nLen = attMessage.memo.length();
							QByteArray att(attMessage.memo.c_str(), nLen);
							//B.T.: compress attachment, damn it:
							att=qCompress(att,1); //min compression
							lstAttachments.setData(nRow, "BEA_CONTENT", att);

							if(!message.attachments[z]->CID.empty())
								lstAttachments.setData(nRow, "BEA_CID_LINK", message.attachments[z]->CID.c_str());
						}
					}
				}
				//IMAP_GetMailAttBodyRecursive(message, lstDataEmails, lstAttachments);

				//finally store the data
				lstDataEmails.setData(nCurRow, "ATTACHMENTS", lstAttachments);
				lstDataEmails.setData(nCurRow, "ATTACHMENTS_SIZE", lstAttachments.getStoredByteSize());

				g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%2: IMAP: fetching message step 6 idx=%1").arg(nMessageIdx).arg(strLogInfo).arg(nNumAttachments));

				//if UIDL not supported, calculate our own UUID and check if we want to load this message
				QString strUID = szUID;
				if(strUID.isEmpty()){
					Sha256Hash Hasher;
					QByteArray binInput(lstDataEmails.getDataRef(nCurRow, "BEM_BODY").toString().toLocal8Bit().constData());
					strUID = Hasher.GetHash(binInput).toHex();
					//check for skip once more
					if(!strUID.isEmpty())
					{
						//B.T. this is irelevant here, as WriteEmail() already is doing this: just be sure to match last message date and start from that point...
						/*
						QByteArray binUUID(strUID.toLocal8Bit().constData());
						if(FindEmailByUID(binUUID, nPersonID) >= 0){
							//email already exists in the database, skip importing once more
							g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%1: Mail UUID already exists in DB, do not import.").arg(strLogInfo));
							lstDataEmails.deleteRow(nCurRow);
							return true;
						}
						*/
					}
				}

				g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%2: IMAP: fetching message step 7 idx=%1").arg(nMessageIdx).arg(strLogInfo).arg(nNumAttachments));

				//update date of the latest processed message 
				if(!dtLastMessage.isValid() || dtLastMessage < dtReceived){
					dtLastMessage = dtReceived;	//store more recent date
					//g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%2: New last email timestamp: %1").arg(dtLastMessage.toString("yyyy-MM-dd HH:mm:ss")).arg(strLogInfo));
				}
				//else
				//	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%2: Email time: %1").arg(dtReceived.toString("yyyy-MM-dd HH:mm:ss")).arg(strLogInfo));

				//store UUID entry
				QByteArray binData(strUID.toLocal8Bit());
				lstDataEmails.setData(nCurRow, "BEM_EXT_ID", binData);

				//store app name
				lstDataEmails.setData(nCurRow, "BEM_EXT_APP", "Sokrates");

				lstDataEmails.setData(nCurRow, "BEM_UNREAD_FLAG", 1);

				break;	//break on first "fetch" response
			}
		}
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%2: IMAP: End fetching message idx=%1").arg(nMessageIdx).arg(strLogInfo));
	return true;
}

bool ImapFetchMessages(mailpp::Imap4Client &client, const QList<uint32_t> &lstMessageIdx, bool bKnownSenderOnly, QDateTime &dtLastMessage, DbRecordSet &lstDataEmails, std::map<int, std::string> &mapMsgNum2Uidl, std::map<int, QDateTime> &mapMsgNum2Date, QString strLogInfo, QString &strLastErrorEmailUID, QString &strAccErrorLog, int nPersonID, FN_SET_ACTIVE pSetThreadActive, EmailImportThread *pThread)
 {
 	//fetch messages one by one
	int numMessages = lstMessageIdx.size();
	for(int i=0; i<numMessages; i++)
	{
		if(pSetThreadActive)(pThread->*pSetThreadActive)();
		int nMessageIdx = lstMessageIdx[i];
		QString strUID = mapMsgNum2Uidl[nMessageIdx].c_str();
		QDateTime dtRecv = mapMsgNum2Date[nMessageIdx];
		g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%2: Imap Fetch message index:%1.").arg(nMessageIdx).arg(strLogInfo));

		if(!ImapFetchSingleMessage(client, nMessageIdx, strUID.toLocal8Bit().constData(), bKnownSenderOnly, dtLastMessage, lstDataEmails, strLogInfo, strLastErrorEmailUID, strAccErrorLog, dtRecv, nPersonID))
			return false;
	}

	return true;	//data fetched
 }

// Processes the responses from the FETCH (BODY[...]) command and saves the memo text
//IMPORTANT: used to fetch any part of the email, including binary attachment !!!!
void processFetchBodySection(const mailpp::Imap4Client& client, Message& message, QString strLogInfo)
{
	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%1: IMAP: processFetchBodySection, step 1").arg(strLogInfo));

    // Iterate over the responses to find the FETCH response
    int numResponses = client.NumResponses();
    for (int i=0; i < numResponses; ++i) {
        const mailpp::Response& r = client.ResponseAt(i);
		g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%1: IMAP: processFetchBodySection, step 2, response no: %2").arg(strLogInfo).arg(i));
        if (strcasecmp(r.Name(), "fetch") == 0) {
            // Process this FETCH response
            const mailpp::FetchResponse& response = (const mailpp::FetchResponse&)r;
            // If there is a body section item, then process it to get the content
            const mailpp::BodySectionData* bodySection = response.BodySection();

			g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%1: IMAP: processFetchBodySection, step 3").arg(strLogInfo));
			
            if (bodySection != 0) {
				//decode body if needed
				if (0 == strcasecmp(message.memoEncoding.c_str(), "quoted-printable")) {
					g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%1: IMAP: processFetchBodySection, step 4").arg(strLogInfo));
					mimepp::QuotedPrintableDecoder dec;
					//binary safe decoding (do not assume it is \0 terminated)
					mimepp::String strDecoded = dec.decode(bodySection->Content());
					g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%1: IMAP: processFetchBodySection, step 4.1").arg(strLogInfo));
					message.memo.assign(strDecoded.c_str(), strDecoded.size());
					g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%1: IMAP: processFetchBodySection, step 4.2").arg(strLogInfo));
				}
				else if (0 == strcasecmp(message.memoEncoding.c_str(), "base64")) {
					g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%1: IMAP: processFetchBodySection, step 5").arg(strLogInfo));
					mimepp::Base64Decoder dec;
					//binary safe decoding (do not assume it is \0 terminated)
					mimepp::String strDecoded = dec.decode(bodySection->Content());
					g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%1: IMAP: processFetchBodySection, step 5.1").arg(strLogInfo));
					message.memo.assign(strDecoded.c_str(), strDecoded.size());
					g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%1: IMAP: processFetchBodySection, step 5.2").arg(strLogInfo));
				}
				else{
					g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("%1: IMAP: processFetchBodySection, step 6").arg(strLogInfo));
					message.memo = std::string(bodySection->Content(), bodySection->Length());
				}
            }
        }
    }
}

// Processes an ENVELOPE structure and saves essential information from the header fields
void processEnvelope(const mailpp::EnvelopeData& envelope, Message& message)
{
	// To
	int n = envelope.NumTo();
    int i;
    for (i = 0; i < n; ++i) {
        if (i > 0) {
            message.to.append(", ");
        }
        const mailpp::AddressData& to = envelope.To(i);
        if (to.PersonalName() != 0 && to.PersonalName()[0] != 0) {
            message.to.append(to.PersonalName());
        }
        else {
            message.to.append(to.MailboxName());
            message.to.append("@");
            message.to.append(to.HostName());
        }
    }

    // Cc
    n = envelope.NumCc();
    for (i = 0; i < n; ++i) {
        if (i > 0) {
            message.cc.append(", ");
        }
        const mailpp::AddressData& cc = envelope.Cc(i);
        if (cc.PersonalName() != 0 && cc.PersonalName()[0] != 0) {
            message.cc.append(cc.PersonalName());
        }
        else {
            message.cc.append(cc.MailboxName());
            message.cc.append("@");
            message.cc.append(cc.HostName());
        }
    }

    // Bcc
    n = envelope.NumBcc();
    for (i = 0; i < n; ++i) {
        if (i > 0) {
            message.bcc.append(", ");
        }
        const mailpp::AddressData& bcc = envelope.Bcc(i);
        if (bcc.PersonalName() != 0 && bcc.PersonalName()[0] != 0) {
            message.bcc.append(bcc.PersonalName());
        }
        else {
            message.bcc.append(bcc.MailboxName());
            message.bcc.append("@");
            message.bcc.append(bcc.HostName());
        }
    }

    // From
    n = envelope.NumFrom();
    for (i = 0; i < n; ++i) {
        if (i > 0) {
            message.from.append(", ");
        }
        const mailpp::AddressData& from = envelope.From(i);
        if (from.PersonalName() != 0 && from.PersonalName()[0] != 0) {
            message.from.append(from.PersonalName());
        }
        else {
            message.from.append(from.MailboxName());
            message.from.append("@");
            message.from.append(from.HostName());
        }
    }

    // Subject
    message.subject = envelope.Subject();

    // Date
    message.date = envelope.Date();
}

// Processes a BODYSTRUCTURE structure and tries to determine which body
// part contains the memo text.  Saves a reference to this body part.
// Since we call this function recursively, we also have a 'level'
// parameter that indicates the recursion depth.
void processBodyStruct(const mailpp::BodyStructureData& bodyStruct, const std::string& partRef, Message& message)
{
    int numBodyParts = bodyStruct.NumBodyParts();
    if (numBodyParts == 0) 
	{
		//std::string strMediaType = bodyStruct.MediaType();
		//std::string strMediaSub  = bodyStruct.MediaSubtype();

        // If this is the first "leaf" body part we have seen, and if
        // the media type is "text", then this is the memo text part we want.
        if (message.partsCount == 0 && 
			strcasecmp(bodyStruct.MediaType(), "text") == 0) 
		{
            if (partRef.empty()) {
                message.memoRef = "TEXT";
            }
            else {
                message.memoRef = partRef;
            }
			message.memoEncoding = bodyStruct.ContentTransferEncoding();
			message.bIsHtml = (strcasecmp(bodyStruct.MediaSubtype(), "html") == 0);

			//test for "charset"
			int nParmCnt = bodyStruct.NumContentTypeParams();
			for(int i=0; i<nParmCnt; i++){
				if(0 == strcasecmp("charset", bodyStruct.ContentTypeParamName(i))){
					message.memoCharset = bodyStruct.ContentTypeParamValue(i);
					break;
				}
			}
        }
		else if (message.partsCount == 1 && 
				 strcasecmp(bodyStruct.MediaType(), "text") == 0 &&
				 strcasecmp(bodyStruct.MediaSubtype(), "html") == 0) 
		{
			//allow overwriting TEXT version with HTML body (prefered version of email body)
            if (partRef.empty()) {
                message.memoRef = "TEXT";
            }
            else {
                message.memoRef = partRef;
            }
			message.memoEncoding = bodyStruct.ContentTransferEncoding();
			message.bIsHtml = true;

			//test for "charset"
			int nParmCnt = bodyStruct.NumContentTypeParams();
			for(int i=0; i<nParmCnt; i++){
				if(0 == strcasecmp("charset", bodyStruct.ContentTypeParamName(i))){
					message.memoCharset = bodyStruct.ContentTypeParamValue(i);
					break;
				}
			}
        }
        // Otherwise, this body part is considered an attachment
        else 
		{
            // Save the information related to this attachment
            Attachment* attach = new Attachment;
            attach->size = bodyStruct.Size();
            attach->contentType = std::string(bodyStruct.MediaType()) + "/" + std::string(bodyStruct.MediaSubtype());
            attach->contentDescription = bodyStruct.ContentDescription();
            attach->bodyPartRef = partRef;
            for (int i=0; i < bodyStruct.NumContentTypeParams(); ++i) {
                if (strcasecmp(bodyStruct.ContentTypeParamName(i), "name")==0){
					attach->filename = bodyStruct.ContentTypeParamValue(i);
					break;
                }
            }
			attach->contentEncoding = bodyStruct.ContentTransferEncoding();
			attach->CID = bodyStruct.ContentId();
			//strip "<>" enclosing characters
			if(!attach->CID.empty() && attach->CID.at(0) == '<')
				attach->CID.erase(0, 1);
			if(!attach->CID.empty() && attach->CID.at(attach->CID.size()-1) == '>')
				attach->CID.erase(attach->CID.size()-1, 1);

			message.appendAttachment(attach);
        }
        ++message.partsCount;
    }
    else { // numBodyParts > 0

        // If there are "leaf" body parts, then call this function recursively for each one
        for (int i=0; i < numBodyParts; ++i) 
		{
            const mailpp::BodyStructureData* nestedBodyStruct = bodyStruct.BodyPart(i);
            char s[64];
            if (partRef.size() > 0) {
                snprintf(s, sizeof(s), "%s.%d", partRef.c_str(), i+1);
            }
            else {
                snprintf(s, sizeof(s), "%d", i+1);
            }
            std::string childPartRef = s;
            processBodyStruct(*nestedBodyStruct, childPartRef, message);
        }
    }
}

// Processes the responses from the FETCH (...) command and saves essential
// information about the message
void processFetchFull(mailpp::Imap4Client& client, int nMessageIdx, Message& message)
{
    // Iterate over the responses to find the FETCH response
    int numResponses = client.NumResponses();
    for (int i=0; i < numResponses; ++i) 
	{
		const mailpp::Response& r = client.ResponseAt(i);
        if (strcasecmp(r.Name(), "fetch") == 0) 
		{
            // Process this FETCH response
            const mailpp::FetchResponse& response = (const mailpp::FetchResponse&) r;

            // Save the message size
            message.size = response.Rfc822Size();

			//FIX: we are not interested in this, we already fetch this by ourselves
            // If there is an ENVELOPE structure, then get the header information from it
            //const mailpp::EnvelopeData* envelope = response.Envelope();
            //if (envelope != 0) {
            //    processEnvelope(*envelope, message);
            //}

            // If there is a BODYSTRUCTURE structure, then examine it to
            // find the body part that contains the memo text
            const mailpp::BodyStructureData* bodyStruct = response.BodyStructure();
            if (bodyStruct != 0) {
				processBodyStruct(*bodyStruct, std::string(), message);
            }
        }
    }

	//now fetch actual data for main body part
	if (message.memoRef.size() > 0) {
		std::string bodySection = std::string("(BODY[") + message.memoRef + std::string("])");
		client.Fetch(nMessageIdx, bodySection.c_str());
		processFetchBodySection(client, message);
	}
}

static const char * const qt_shortMonthNames[] = {
	"Jan", "Feb", "Mar", "Apr", "May", "Jun",
	"Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
	
int qt_monthNumberFromShortName(const QString &shortName)
{
	for (unsigned int i = 0; i < sizeof(qt_shortMonthNames) / sizeof(qt_shortMonthNames[0]); ++i) {
		if (shortName == QLatin1String(qt_shortMonthNames[i]))
			return i + 1;
	}
	return -1;
}

static void rfcDateImpl(const QString &s, QDate *dd = 0, QTime *dt = 0, int *utcOffset = 0)
{
	int day = -1;
	int month = -1;
	int year = -1;
	int hour = -1;
	int min = -1;
	int sec = -1;
	int hourOffset = 0;
	int minOffset = 0;
	bool positiveOffset = false;
	// Matches "Wdy, DD Mon YYYY HH:MM:SS �hhmm" (Wdy, being optional)
	QRegExp rex(QStringLiteral("^(?:[A-Z][a-z]+,)?[ \\t]*(\\d{1,2})[ \\t]+([A-Z][a-z]+)[ \\t]+(\\d\\d\\d\\d)(?:[ \\t]+(\\d\\d):(\\d\\d)(?::(\\d\\d))?)?[ \\t]*(?:([+-])(\\d\\d)(\\d\\d))?"));
	if (s.indexOf(rex) == 0) {
		if (dd) {
			day = rex.cap(1).toInt();
			month = qt_monthNumberFromShortName(rex.cap(2));
			year = rex.cap(3).toInt();
		}
		if (dt) {
			if (!rex.cap(4).isEmpty()) {
				hour = rex.cap(4).toInt();
				min = rex.cap(5).toInt();
				sec = rex.cap(6).toInt();
			}
			positiveOffset = (rex.cap(7) == QStringLiteral("+"));
			hourOffset = rex.cap(8).toInt();
			minOffset = rex.cap(9).toInt();
		}
		if (utcOffset)
			*utcOffset = ((hourOffset * 60 + minOffset) * (positiveOffset ? 60 : -60));
	} else {
		// Matches "Wdy Mon DD HH:MM:SS YYYY"
		QRegExp rex(QStringLiteral("^[A-Z][a-z]+[ \\t]+([A-Z][a-z]+)[ \\t]+(\\d\\d)(?:[ \\t]+(\\d\\d):(\\d\\d):(\\d\\d))?[ \\t]+(\\d\\d\\d\\d)[ \\t]*(?:([+-])(\\d\\d)(\\d\\d))?"));
		if (s.indexOf(rex) == 0) {
			if (dd) {
				month = qt_monthNumberFromShortName(rex.cap(1));
				day = rex.cap(2).toInt();
				year = rex.cap(6).toInt();
			}
			if (dt) {
				if (!rex.cap(3).isEmpty()) {
					hour = rex.cap(3).toInt();
					min = rex.cap(4).toInt();
					sec = rex.cap(5).toInt();
				}
				positiveOffset = (rex.cap(7) == QStringLiteral("+"));
				hourOffset = rex.cap(8).toInt();
				minOffset = rex.cap(9).toInt();
			}
			if (utcOffset)
				*utcOffset = ((hourOffset * 60 + minOffset) * (positiveOffset ? 60 : -60));
		}
	}
	if (dd)
		*dd = QDate(year, month, day);
	if (dt)
		*dt = QTime(hour, min, sec);
}

//RFC 2822 format: "Tue, 13 Nov 2012 12:09:28 +0100"
QDateTime ParseRfc2822DateTime(QString strDate)
{
	//return QDateTime::fromString(strDate, Qt::RFC2822Date);	//this date type support seems to be missing in our older Qt

	//https://qt.gitorious.org/qt/qtbase/source/93f5e0598ad2c7738409eb027fc868ab35db5920:src/corelib/tools/qdatetime.cpp
	QDate date;
	QTime time;
	int utcOffset = 0;
	rfcDateImpl(strDate, &date, &time, &utcOffset);
	if (!date.isValid() || !time.isValid())
		return QDateTime();
	QDateTime dateTime(date, time, Qt::UTC);
	//QDateTime dateTime(date, time);
	//dateTime.setOffsetFromUtc(utcOffset);
	//https://gist.github.com/wimleers/702387
	// Mark this QDateTime as one with a certain offset from UTC, and set that offset.
	dateTime.setTimeSpec(Qt::OffsetFromUTC);
	dateTime.setUtcOffset(utcOffset);
	// Convert this QDateTime to UTC.
	dateTime = dateTime.toUTC();
	//back to local
	dateTime = dateTime.toLocalTime();
	//g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("Parsed IMAP date string [%1] to datetime [%2]").arg(strDate).arg(dateTime.toString("yyyy-MM-dd HH:mm:ss")));
	return dateTime;

	/*
	//FIX: my old code that ignored timezone or UTC or something
	int nPos = strDate.indexOf(",");
	if(nPos >= 0)
		strDate = strDate.mid(nPos+1);

	char szFullDate[300];
	sprintf(szFullDate, "%s", strDate.toLatin1().constData());

	int nDay, nYear, nHour, nMinute, nSec, nDummy;
	char szMonth[20];
	sscanf(szFullDate, "%d %s %d %02d:%02d:%02d +%d", &nDay, szMonth, &nYear, &nHour, &nMinute, &nSec, &nDummy);

	//month name to number
	int nMonth = 0;
	if(0 == strcmp("Jan", szMonth))
		nMonth = 1;
	else if(0 == strcmp("Feb", szMonth))
		nMonth = 2;
	else if(0 == strcmp("Mar", szMonth))
		nMonth = 3;
	else if(0 == strcmp("Apr", szMonth))
		nMonth = 4;
	else if(0 == strcmp("May", szMonth))
		nMonth = 5;
	else if(0 == strcmp("Jul", szMonth))
		nMonth = 6;
	else if(0 == strcmp("Jun", szMonth))
		nMonth = 7;
	else if(0 == strcmp("Aug", szMonth))
		nMonth = 8;
	else if(0 == strcmp("Sep", szMonth))
		nMonth = 9;
	else if(0 == strcmp("Oct", szMonth))
		nMonth = 10;
	else if(0 == strcmp("Nov", szMonth))
		nMonth = 11;
	else if(0 == strcmp("Dec", szMonth))
		nMonth = 12;

	QDateTime res;
	res.setDate(QDate(nYear, nMonth, nDay));
	res.setTime(QTime(nHour, nMinute, nSec));
	return res;
	*/
}

void DeleteEmailsOlderThanDays(int nPersonID, const QString &strEmail, int nDelEmailsAfterNumDays, QString strLogInfo)
{
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("%2: Delete emails older than %1 days.").arg(nDelEmailsAfterNumDays).arg(strLogInfo));
	if(nDelEmailsAfterNumDays < 1) return;

	//prepare date parameter
	QDateTime datFrom = QDateTime::currentDateTime();
	datFrom.setTime(QTime(0,0,0));
	datFrom = datFrom.addDays(-nDelEmailsAfterNumDays);

	QString strSql = QString("SELECT BEM_ID, BEM_RECV_TIME FROM BUS_EMAIL INNER JOIN CE_COMM_ENTITY ON CENT_ID=BEM_COMM_ENTITY_ID WHERE CENT_OWNER_ID=%1 AND BEM_RECV_TIME<?").arg(nPersonID);
	Status status;
	DbSqlQuery query(status, g_BusinessServiceSet->GetDbManager());
	if(!status.IsOK()){
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0,QString("%2: Failed creating query: %1").arg(status.getErrorText()).arg(strLogInfo));
		return;
	}
	query.Prepare(status, strSql);
	if (!status.IsOK()){ 
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0,QString("%2: Failed preparing query: %1").arg(status.getErrorText()).arg(strLogInfo));
		return;
	}
	query.bindValue(0, datFrom);
	query.ExecutePrepared(status);
	if (!status.IsOK()){ 
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0,QString("%2: Failed executing query: %1").arg(status.getErrorText()).arg(strLogInfo));
		return;
	}
	DbRecordSet lstData;
	query.FetchData(lstData);

	int nRows = lstData.getRowCount();
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("%2: Dump info on %1 emails to be deleted:").arg(nRows).arg(strLogInfo));
	for(int i=0; i<nRows; i++){
		int nEmailID = lstData.getDataRef(i,0).toInt();
		QDateTime dtRecv = lstData.getDataRef(i,1).toDateTime();
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("%5: %1/%2: Email ID=%3, RECVTime=%4").arg(i+1).arg(nRows).arg(nEmailID).arg(dtRecv.toString("yyyy-MM-dd HH:mm:ss")).arg(strLogInfo));
	}

	//main query
	strSql = QString("DELETE FROM BUS_EMAIL WHERE BEM_ID IN (SELECT BEM_ID FROM BUS_EMAIL INNER JOIN CE_COMM_ENTITY ON CENT_ID=BEM_COMM_ENTITY_ID WHERE CENT_OWNER_ID=%1 AND BEM_RECV_TIME<?)").arg(nPersonID);
	
	//Get query object and its select statement.
	query.Prepare(status, strSql);
	if (!status.IsOK()){ 
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0,QString("%2: Failed preparing query: %1").arg(status.getErrorText()).arg(strLogInfo));
		return;
	}
	query.bindValue(0, datFrom);
	query.ExecutePrepared(status);
	if (!status.IsOK()){ 
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0,QString("%2: Failed executing query: %1").arg(status.getErrorText()).arg(strLogInfo));
		return;
	}
	
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("%2: Deleting emails done (%1 rows affected).").arg(query.rowsAffected()).arg(strLogInfo));
}

QString EmailImportThread::GenerateAccUID()
{
	char szBuffer[1024];
	//3rd field to write - random number block (16 bytes)
	OpenSSLHelper::Extern_RAND_bytes((unsigned char *)szBuffer, 16);		//generate fresh 8 random bytes
	return QString(QByteArray::fromRawData(szBuffer, 16).toBase64());
}

QString PersonNameFromID(int nPersonID)
{
	Status status;
	DbSqlQuery query(status, g_BusinessServiceSet->GetDbManager());
	if(status.IsOK()){
		QString strSQL = QString("SELECT CUSR_USERNAME FROM CORE_USER WHERE CUSR_PERSON_ID=%1").arg(nPersonID);
		query.Execute(status, strSQL);
		if(status.IsOK()){
			if (query.next())
				return query.value(0).toString();
		}
	}
	return QString("(person %1)").arg(nPersonID);	//error
}

void EmailImportThread::ImportMessageFromMime(Status &status, int nPersonID, int nIsOutgoing, QString strFrom, QString strTo, QString strSubject, QString strMime, QString strFullFrom, QString strCC, QString strBCC)
{
	//define IMPORT list:
	DbRecordSet lstDataEmails;	//email storage
	DbRecordSet set;
	set.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CE_COMM_ENTITY));
	lstDataEmails.copyDefinition(set, false);
	set.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_EMAIL));
	lstDataEmails.copyDefinition(set, false);
	lstDataEmails.addColumn(QVariant::String, "PROJECT_NAME");
	lstDataEmails.addColumn(QVariant::String, "PROJECT_CODE");
	lstDataEmails.addColumn(QVariant::String, "EMAIL_UNASSIGNED");
	lstDataEmails.addColumn(DbRecordSet::GetVariantType(), "ATTACHMENTS");
	lstDataEmails.addColumn(DbRecordSet::GetVariantType(), "CONTACTS");
	lstDataEmails.addColumn(DbRecordSet::GetVariantType(), "UAR_TABLE");
	lstDataEmails.addColumn(DbRecordSet::GetVariantType(), "GAR_TABLE");
	lstDataEmails.addColumn(QVariant::Int, "ATTACHMENTS_SIZE");

	//NOTE: similar code found in Pop3FetchSingleMessage
	mimepp::Message msg(strMime.toUtf8().constData());
	msg.parse();

	/*
	//debug code, log
	if(g_AppServer->GetIniFile()->m_strLoggedEmails.indexOf(strAccountEmail) >= 0)
	{
			
	}
	*/
	
	//store the message into the database
	lstDataEmails.addRow();
	int nCurRow = lstDataEmails.getRowCount()-1;

	//write From info
	strFrom = DecodeField(strFrom.toLocal8Bit().constData());
	lstDataEmails.setData(nCurRow, "BEM_FROM", strFrom);
	lstDataEmails.setData(nCurRow, "BEM_REPLY_TO", strFrom);	//equals to From

	strFullFrom = DecodeField(strFullFrom.toLocal8Bit().constData());
	lstDataEmails.setData(nCurRow, "BEM_TRUE_FROM", strFullFrom);
	
	//store subject
	strSubject = DecodeField(strSubject.toLocal8Bit().constData());
	lstDataEmails.setData(nCurRow, "BEM_SUBJECT", strSubject);
	
	lstDataEmails.setData(nCurRow, "BEM_RECV_TIME", QDateTime::currentDateTime());	//TOFIX MBs MIME does not contain header fields!

	lstDataEmails.setData(nCurRow, "BEM_OUTGOING", nIsOutgoing);

	//add to/cc/bcc addresses
	strTo = DecodeField(strTo.toLocal8Bit().constData());
	lstDataEmails.setData(nCurRow, "BEM_TO", strTo);
	strCC = DecodeField(strCC.toLocal8Bit().constData());
	lstDataEmails.setData(nCurRow, "BEM_CC", strCC);
	strBCC = DecodeField(strBCC.toLocal8Bit().constData());
	lstDataEmails.setData(nCurRow, "BEM_BCC", strBCC);

	bool bBodyPartSet = false;
	bool bBodyPartHTML = false;

	//store attachments
	DbRecordSet lstAttachments;
	lstAttachments.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_EMAIL_ATTACHMENT));
	if(Thunderbird_GetMailAttBodyRecursive(msg, lstDataEmails, lstAttachments, "")){
		bBodyPartSet = true;
	}
	lstDataEmails.setData(nCurRow, "ATTACHMENTS", lstAttachments);
	lstDataEmails.setData(nCurRow, "ATTACHMENTS_SIZE", lstAttachments.getStoredByteSize());

	//only if no body part found (not multipart)
	if(!bBodyPartSet){
		QString strCharset = "US-ASCII";
		if (msg.headers().hasField("Content-Type")) {
			const mimepp::MediaType& mtype = msg.headers().contentType();
			mimepp::String subtype = mtype.subtype();
			int numParams = mtype.numParameters();
			for (int i=0; i < numParams; ++i) {
				const mimepp::Parameter& param = msg.headers().contentType().parameterAt(i);
				const mimepp::String& name = param.name();
				if (0 == strcasecmp("charset", name.c_str())) {
					strCharset = param.value().c_str();
					break;
				}
			}
		}

		//transfer format decoding
		std::string strData;
		int cte = mimepp::TransferEncodingType::_7BIT;
		if (msg.headers().hasField("Content-Transfer-Encoding")) {
			cte = msg.headers().contentTransferEncoding().asEnum();
		}
		if (cte == mimepp::TransferEncodingType::QUOTED_PRINTABLE) {
			mimepp::QuotedPrintableDecoder dec;
			strData = dec.decode(msg.body().getString().c_str()).c_str();
		}
		else if (cte == mimepp::TransferEncodingType::BASE64) {
			mimepp::Base64Decoder dec;
			strData = dec.decode(msg.body().getString().c_str()).c_str();
		}
		else
			strData = msg.body().getString().c_str();

		//make sure to convert to the proper code page
		QString strRTF = QString::fromUtf8(strData.c_str());
		if(!strCharset.isEmpty()){
			QTextCodec *codec = QTextCodec::codecForName(strCharset.toLocal8Bit().constData());
			if(codec)
				strRTF = codec->toUnicode(strData.c_str());
		}

		if(!strRTF.isEmpty()){
			//can still be text-only, check mail type
			QString strContType = msg.headers().fieldBody("Content-Type").text().c_str();
			bool bHtml = (strRTF.indexOf("<html", 0, Qt::CaseInsensitive) >= 0) || (strRTF.indexOf("<!DOCTYPE", 0, Qt::CaseInsensitive) >= 0) || (strContType.indexOf("html") >= 0);
			lstDataEmails.setData(nCurRow, "BEM_BODY", strRTF);
			lstDataEmails.setData(nCurRow, "BEM_EMAIL_TYPE", (bHtml)? 1 : 0);
		}
	}
/*
#ifdef _DEBUG
	QString strRtf;
	lstDataEmails.getData(nCurRow, "BEM_BODY", strRtf);
#endif
*/
	//store app name
	lstDataEmails.setData(nCurRow, "BEM_EXT_APP", "Sokrates");
	lstDataEmails.setData(nCurRow, "BEM_UNREAD_FLAG", 1);

	//now do the import
	int nSkippedOrReplacedCnt = 0;
	bool bAddDupsAsNewRow = false;
	bool bSkipExisting = false;
	ImportEmails(status, lstDataEmails, nPersonID, nSkippedOrReplacedCnt, bAddDupsAsNewRow, bSkipExisting);
}



QString ParseFullEmailBodyForAttachmentName(mailpp::Imap4Client& client, uint32_t nMessageIdx,int nMimeBodyPart)
{
	//fetch whole email, detect boundary, get to the email part, extract name from 'name' or 'filename':
	client.Fetch(nMessageIdx, "BODY[]");

	int numResponses = client.NumResponses();
	for (int i=0; i < numResponses; ++i) 
	{
		const mailpp::Response& r = client.ResponseAt(i);
		if (strcasecmp(r.Name(), "fetch") == 0) 
		{
			// Process this FETCH response
			const mailpp::FetchResponse& response = (const mailpp::FetchResponse&)r;
			// If there is a body section item, then process it to get the content
			const mailpp::BodySectionData* bodySection = response.BodySection();
			const mailpp::BodyStructureData* bodyStruct = response.BodyStructure();

			if (bodySection)
			{
				int nLen=bodySection->Length();
				QByteArray rawMimeEmail;
				rawMimeEmail=QByteArray(bodySection->Content(),nLen);

				int nPos=rawMimeEmail.indexOf("boundary=");
				nPos=nPos+QString("boundary=").length();
				QByteArray bBoundary=rawMimeEmail.mid(nPos,rawMimeEmail.indexOf("\n",nPos)-nPos-1).trimmed();
				bBoundary.replace("\"","");
				if(bBoundary.right(1)==";")
					bBoundary.chop(1);


				nPos=rawMimeEmail.indexOf(bBoundary,nPos+20);
				int nPartDetected=0;
				while (nPos>0)
				{
					nPartDetected++;
					if (nPartDetected==nMimeBodyPart)
					{
						QString strPartHeader=rawMimeEmail.mid(nPos+bBoundary.size(),300);
						QString strName = EmailImportThread::GetFileNameFromContentType(strPartHeader);
						return strName;
					}

					nPos=rawMimeEmail.indexOf(bBoundary,nPos+20);
				}

			}
		}
	}

	return "";

}

QString EmailImportThread::GetFileNameFromContentType( const QString &strContentType )
{
	int nPos = strContentType.indexOf("name=");
	if (nPos>0)
	{
		nPos+=QString("name=").length();
		int nEndPos=strContentType.indexOf(";",nPos);
		if (nEndPos<0 || strContentType.mid(nPos,nEndPos-nPos).indexOf("\n",nPos)>0)
		{
			nEndPos=strContentType.indexOf("\n",nPos);
		}
		QString strName=strContentType.mid(nPos,nEndPos-nPos-1).trimmed();
		strName.replace("\"","");
		return strName;
	}
	nPos = strContentType.indexOf("filename=");
	if (nPos>0)
	{
		nPos+=QString("filename=").length();
		int nEndPos=strContentType.indexOf(";",nPos);
		if (nEndPos<0 || strContentType.mid(nPos,nEndPos-nPos).indexOf("\n",nPos)>0)
		{
			nEndPos=strContentType.indexOf("\n",nPos);
		}
		QString strName=strContentType.mid(nPos,nEndPos-nPos-1).trimmed();
		strName.replace("\"","");
		return strName;
	}
	return "";
}
