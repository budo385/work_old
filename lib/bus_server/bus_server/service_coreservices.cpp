#include "service_coreservices.h"
#include "common/common/config_version_sc.h"
#include "db/db/dbsimpleorm.h"
#include "db_core/db_core/dbsqltableview.h"
#include "bus_core/bus_core/optionsandsettingsorganizer.h"
#include "common/common/entity_id_collection.h"

//GLOBALS:
#include "loadbalancer.h"
extern LoadBalancer *g_LoadBalancer;
#include "bus_core/bus_core/messagedispatcher.h"
extern MessageDispatcher *g_MessageDispatcher;
#include "common/common/logger.h"
extern Logger				g_Logger;
#include "businessserviceset.h"
extern BusinessServiceSet* g_BusinessServiceSet;
#include "bus_core/bus_core/usersessionmanager.h"
extern UserSessionManager	*g_UserSessionManager;
#include "privateserviceset.h"
extern PrivateServiceSet* g_PrivateServiceSet; 



/*!
Returns current list of all app. servers in system, for LBO function

	\param Ret_Status			- if err occur, returned here
	\param Ret_Data				- returned list in TVIEW_CORE_APP_SRV_SESSION_STATUS
	\param pDbConn				- db conn
*/
void Service_CoreServices::ReadLBOList(Status &Ret_pStatus, DbRecordSet &Ret_Data)
{
	
	Ret_pStatus.setError(0);
	Q_ASSERT(g_LoadBalancer!=NULL);			   //can be null in thick mode, so test
	g_LoadBalancer->GetLBOList(Ret_Data,true); //skip current IP (does not mean nothing to client)

}





/*!
	Called by client after login, before any operation, to test versions

	\param Ret_Status			- if err occur, returned here
	\param nClientVersion		- client APP version, store in common/config.h
*/
void Service_CoreServices::CheckVersion(Status& Ret_Status,QString strClientVersion,QString &Ret_strServerVersion,QString &Ret_strServerDbVersion,QString& Ret_strTemplateInUse,int& Ret_nAskOnStartup,int& Ret_nStartStartUpWizard,QByteArray& Ret_byteVersion, int &Ret_nClientTimeZoneOffsetMinutes)
{
	Ret_Status.setError(0);		
	Ret_strServerVersion=QString(APPLICATION_VERSION);
	Ret_strServerDbVersion=QVariant(DATABASE_VERSION).toString();


	//LOAD from DB:
	DbSqlQuery query(Ret_Status, GetDbManager());
	if(!Ret_Status.IsOK()) return;

	//reset template flag=0, do not ask for template
	QString strSQL = "SELECT CDI_TEMPLATE_DB_USED,CDI_ASK_FOR_TEMPLATE_DB, CDI_START_SETUP_WIZARD,CDI_SETUP_WIZARD_WIZARD_INFO FROM CORE_DATABASE_INFO";
	query.Execute(Ret_Status, strSQL);
	if(!Ret_Status.IsOK()) return;

	if (query.next())
	{
		Ret_strTemplateInUse=query.value(0).toString();
		Ret_nAskOnStartup=query.value(1).toInt();
		Ret_nStartStartUpWizard=query.value(2).toInt();
		Ret_byteVersion=query.value(3).toByteArray();
	}
	else
		Ret_Status.setError(StatusCodeSet::ERR_BUS_TEMPLATE_DATA_NOT_EXISTS);

	//get time zone offset for client connection from server in minutes: substract value from server time to get right datetime. This is done automatically through XML layer except for server messages..
	Ret_nClientTimeZoneOffsetMinutes = g_UserSessionManager->GetClientTimeZoneOffsetMinutes();

}


/*!
	Called by client after login, loads defaults 

	\param Ret_Status			- if err occur, returned here
	\param Ret_DefaultOrg		- default organization
	\param Ret_IsAliveInterval	- is alive period in seconds
	\param Ret_ARSet			- AR set (access right)
*/
void Service_CoreServices::ReadUserSessionData(Status& Ret_Status,DbRecordSet &Ret_UserData, DbRecordSet &Ret_ARSet,DbRecordSet &Ret_ContactData, DbRecordSet &Ret_recSettings, DbRecordSet &Ret_recOptions, DbRecordSet &Ret_lstMessages)
{
	//instant tableORM
	DbSimpleOrm TableOrmOrg(Ret_Status, BUS_ORGANIZATION, GetDbManager()); 
	if(!Ret_Status.IsOK()) return;

	//user data
	int nUserID=m_SessionManager->GetUserID();
	g_PrivateServiceSet->CoreUser->GetUserByID(Ret_Status,nUserID,Ret_UserData); //TVIEW_CORE_USER_SELECTION format
	if(!Ret_Status.IsOK())return;


	//user contact data:
	int nContactID=Ret_UserData.getDataRef(0,"BPER_CONTACT_ID").toInt();
	if(nContactID!=0)
	{
		g_BusinessServiceSet->BusContact->ReadContactDefaults(Ret_Status,nContactID,Ret_ContactData); //TVIEW_BUS_CM_DEAFULTS
		if(!Ret_Status.IsOK()) return;
	}

	//AR set:
	g_BusinessServiceSet->AccessRights->GetUserAccRightsRecordSet(Ret_Status,Ret_ARSet,m_SessionManager->GetUserID());
	if(!Ret_Status.IsOK()) return;


	//--------------------------
	//OPTIONS AND SETTINGS.
	//--------------------------
	GetOptionsAndSettings(Ret_Status, m_SessionManager->GetPersonID(), Ret_recSettings, Ret_recOptions);

	//--------------------------
	//MESSAGES:
	//--------------------------
	g_MessageDispatcher->GetMessages(m_SessionManager->GetPersonID(),Ret_lstMessages);

/*
	DbSqlQuery query1(Ret_Status,GetDbManager(),TableOrmOrg.GetDbConnection());
	if(!Ret_Status.IsOK()) return;

	QString str1="SELECT * FROM BUS_CM_ADDRESS WHERE BCMA_ID<13000";
	query1.Execute(Ret_Status,str1);
	DbRecordSet lstData;
	query1.FetchData(lstData);
	lstData.Dump();

	DbRecordSet lstdataX;
	lstdataX.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_ADDRESS));
	QString strX=DbSqlTableView::getSQLInsert(DbSqlTableView::TVIEW_BUS_CM_ADDRESS);
	query1.Prepare(Ret_Status,strX);
	if(!Ret_Status.IsOK()) return;
	lstdataX.addRow();
	lstdataX.setData(0,"BCMA_CONTACT_ID",1);
	lstdataX.setData(0,"BCMA_FORMATEDADDRESS","Antei�aaaaa�1");

	DbView viewData=DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_ADDRESS);
	int nInsertSkipFirstCol=viewData.m_nSkipFirstColsInsert;
	int nSkipLastColumns=viewData.m_nSkipLastColsWrite;
	query1.BindRowValues(lstdataX,0,nInsertSkipFirstCol,nSkipLastColumns);
	query1.ExecutePrepared(Ret_Status);

*/

	//QSqlQuery *query = TableOrmOrg.GetDbSqlQuery()->GetQSqlQuery();
	/*
	bool bok=query->exec("SELECT BPIC_PICTURE FROM BUS_BIG_PICTURE WHERE BPIC_ID=1");
	if (!bok)
	{
		qDebug()<<query->lastError();
	}

	
	if(query->next())
	{
		QByteArray test1=query->value(0).toByteArray();
		QFile file("d:/test.bin");
		if(file.open(QIODevice::WriteOnly))
		{
			file.write(test1);
			file.close();
		}
	}
		*/
/*
	//QSqlQuery *query = TableOrmOrg.GetDbSqlQuery()->GetQSqlQuery();
	bool bok=query->exec("SELECT BPIC_PICTURE FROM BUS_BIG_PICTURE WHERE BPIC_ID=111");
	if (!bok)
	{
		qDebug()<<query->lastError();
	}

	QByteArray ba;
	QVariant val;
	bool beof;
	bok=query->fetchBlobChunk(val,beof);

	if (!bok)
	{
		qDebug()<<query->lastError();
	}

	ba.append(val.toByteArray());

	while (bok && beof==false)
	{
		bok=query->fetchBlobChunk(val,beof);
		ba.append(val.toByteArray());
	}
	
	QFile file("d:/xx1.bin");
	if(file.open(QIODevice::WriteOnly))
	{
		file.write(ba);
		file.close();
	}

	//write:

	bok=query->prepare("UPDATE BUS_BIG_PICTURE SET BPIC_DAT_LAST_MODIFIED=CURRENT_TIMESTAMP,BPIC_PICTURE=? WHERE BPIC_ID=111");
	if (!bok)
	{
		qDebug()<<query->lastError();
	}

	ba.clear();
	if(file.open(QIODevice::ReadOnly))
	{
		//chunk: is 8kb (just to test):
		beof=false;
		while(file.bytesAvailable()>0)
		{
			QByteArray buffer=file.read(60000);
			ba.append(buffer);
			beof=(file.bytesAvailable()==0);
			bok=query->writeBlobChunk(buffer,beof);
			if (!bok)
			{
				qDebug()<<query->lastError();
				break;
			}
		}

		//file.write(ba);
		file.close();
	}


	bok=query->exec("SELECT BPIC_PICTURE FROM BUS_BIG_PICTURE WHERE BPIC_ID=111");
	if (!bok)
	{
		qDebug()<<query->lastError();
	}


	if(query->next())
	{
		QByteArray test1=query->value(0).toByteArray();
		QFile file("d:/test_111.bin");
		if(file.open(QIODevice::WriteOnly))
		{
			file.write(test1);
			file.close();
		}
	}
*/
}



void Service_CoreServices::GetUserSessions(Status& Ret_Status,int &Ret_nUserSessions)
{
	Ret_nUserSessions=m_SessionManager->GetSessionCount();
}

void Service_CoreServices::SavePersonalSettings(Status &Ret_pStatus, DbRecordSet &RetOut_pLstForWrite /*= NULL*/)
{
	SavePersonalSettings(Ret_pStatus, m_SessionManager->GetPersonID(), RetOut_pLstForWrite);
}


void Service_CoreServices::UpdateVersion(Status& Ret_Status,QByteArray byteVersion)
{
	//
	DbSqlQuery query(Ret_Status, GetDbManager());
	if(!Ret_Status.IsOK()) return;

	//reset template flag=0, do not ask for template
	QString strSQL = "UPDATE CORE_DATABASE_INFO SET CDI_DATABASE_VERSION=?,CDI_SETUP_WIZARD_WIZARD_INFO=?";
	query.Prepare(Ret_Status, strSQL);
	if(!Ret_Status.IsOK()) return;

	query.bindValue(0,DATABASE_VERSION);
	query.bindValue(1,byteVersion);

	query.ExecutePrepared(Ret_Status);
	if(!Ret_Status.IsOK()) return;

}

void Service_CoreServices::SavePersonalSettingsByPersonID(Status &Ret_pStatus, int nPersonID, DbRecordSet &RetOut_pLstForWrite)
{
	SavePersonalSettings(Ret_pStatus, nPersonID, RetOut_pLstForWrite);
}

void Service_CoreServices::GetPersonalSettingsByPersonID(Status &Ret_pStatus, int nPersonID, DbRecordSet &RetOut_pLstForWrite)
{
	DbRecordSet recOptions;
	GetOptionsAndSettings(Ret_pStatus, nPersonID, RetOut_pLstForWrite, recOptions);
}

void Service_CoreServices::SavePersonalSettings(Status &pStatus, int nPersonID, DbRecordSet &pLstForWrite)
{
	//Get person ID.
	QString strPersonID = QVariant(nPersonID).toString();

	//Get person settings.
	DbRecordSet recSettings;
	DbSimpleOrm SETTINGS_ORM(pStatus, BUS_OPT_SETTINGS, GetDbManager());
	if(!pStatus.IsOK()) return;
	QString strWhere = "WHERE BOUS_PERSON_ID = " + strPersonID;
	SETTINGS_ORM.Read(pStatus, recSettings, strWhere);
	if(!pStatus.IsOK()) return;

	int nRowCount = pLstForWrite.getRowCount();
	for (int i = 0; i < nRowCount; ++i)
	{
		//Check does this combination of settings and person already exits, but here doesn't have an ID.
		if (pLstForWrite.getDataRef(i, "BOUS_ID").isNull())
		{
			int nBOUS_SETTING_ID = pLstForWrite.getDataRef(i, "BOUS_SETTING_ID").toInt();
			int nFoundRow = recSettings.find("BOUS_SETTING_ID", nBOUS_SETTING_ID, true);
			//If found that setting then replace the incoming BOUS_ID = NULL with found one - 
			//so it will be a nice update, not a breaking insert on unique index. Amen.
			if(nFoundRow >= 0)
				pLstForWrite.setData(i, "BOUS_ID", recSettings.getDataRef(nFoundRow, "BOUS_ID").toInt());
		}
	}

	//Now write the settings.
	SETTINGS_ORM.Write(pStatus,	pLstForWrite);
}

void Service_CoreServices::GetOptionsAndSettings(Status &pStatus, int nPersonID, DbRecordSet &recSettings, DbRecordSet &recOptions)
{
	//Get person ID.
	QString strPersonID = QVariant(nPersonID).toString();

	DbSimpleOrm OPTIONS_SETTINGS_ORM(pStatus, BUS_OPT_SETTINGS, GetDbManager()); 
	if(!pStatus.IsOK()) return;

	QString strWhere = "WHERE BOUS_PERSON_ID = " + strPersonID;
	if (nPersonID>0)
	{
		//First person settings.
		OPTIONS_SETTINGS_ORM.Read(pStatus, recSettings, strWhere);
		if(!pStatus.IsOK()) return;
	}
	else
	{
		recSettings.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_OPT_SETTINGS));
	}

	//_DUMP(recSettings);

	strWhere = "WHERE BOUS_PERSON_ID IS NULL ";
	OPTIONS_SETTINGS_ORM.Read(pStatus, recOptions, strWhere);
	if(!pStatus.IsOK()) return;
}
