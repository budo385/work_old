#include "service_userlogon.h"
#include "db/db/dbconnectionreserver.h"
#include "common/common/authenticator.h"
#include "db/db/dbsqlquery.h"
#include "common/common/threadid.h"

#include "common/common/logger.h"
extern Logger				g_Logger;
#include "bus_core/bus_core/usersessionmanager.h"
extern UserSessionManager	*g_UserSessionManager;
#include "privateserviceset.h"
extern PrivateServiceSet* g_PrivateServiceSet; 

/*!
	Logins, generates new sessionId if all OK

	\param Ret_Status			- if err occur, returned here
	\param RetOut_strSessionID	- returned session id (32) byte string
	\param strUsername			- app. server id 
	\param strAuthToken			- 32 byte string, auth token, sha256 digested from (username, pass + client nonce string)
	\param strClientNonce		- client nonce string use to decode  strAuthToken
	\param strModuleCode		- client module code
	\param strMLIID				- MLIID
	\param pDbConn				- db conn
*/
void Service_UserLogon::Login(Status& Ret_Status, QString& RetOut_strSessionID,QString strUsername, QByteArray strAuthToken,QByteArray strClientNonce,QString strModuleCode, QString strMLIID, QString strParentSession, bool bIsWebLogin, int nClientTimeZoneOffsetMinutes)	
{

	DbRecordSet rowUserData;
	DbConnectionReserver conn(Ret_Status,GetDbManager()); //reserve global connection
	if(!Ret_Status.IsOK())return;

	//--------------------------------------------------------
	// AUTHENTICATE USER
	//--------------------------------------------------------
	
	//load from DB username, password, group, 
	g_PrivateServiceSet->CoreUser->GetUserByUserName(Ret_Status,strUsername,rowUserData);
	if(!Ret_Status.IsOK())return;


	//is user exists:
	if(rowUserData.getRowCount()!=1)
		{Ret_Status.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_WRONG_PASS_USER);return;}

	//is user active (first test core then bus):
	if(rowUserData.getDataRef(0,"CUSR_ACTIVE_FLAG").toInt()!=1)
		{Ret_Status.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_NO_ACTIVE);return;}
	else if(rowUserData.getDataRef(0,"BPER_ID").toInt()!=0 && rowUserData.getDataRef(0,"BPER_ACTIVE_FLAG").toInt()==0)
		{Ret_Status.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_NO_ACTIVE);return;}

	QByteArray bytePassword;
	//B.T. 2013.02.05: if non SPC user then as usual, if SPC then convert existing password to unbase64:username hashs
	if (rowUserData.getDataRef(0,"BPER_FPERSON_SEQ").toInt()==0)
	{
		//test password:
		bytePassword=rowUserData.getDataRef(0,"CUSR_PASSWORD").toByteArray();
	}
	else
	{
		bytePassword=QByteArray::fromBase64(rowUserData.getDataRef(0,"CUSR_PASSWORD").toByteArray());
		bytePassword=Authenticator::GeneratePassword(strUsername,bytePassword);
	}

	if(!Authenticator::Authenticate(strClientNonce,strAuthToken,strUsername,bytePassword))
	{Ret_Status.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_WRONG_PASS_USER);return;}


	//--------------------------------------------------------
	// CREATE USER DATA ON SERVER
	//--------------------------------------------------------
	g_PrivateServiceSet->CoreUser->BuildUserContextData(Ret_Status,RetOut_strSessionID,rowUserData,strModuleCode,strMLIID,strParentSession, nClientTimeZoneOffsetMinutes);

	if (Ret_Status.IsOK())
	{
		QString strMsg=QString("%1;%2;%3;%4;%5;%6").arg(strUsername).arg(ThreadIdentificator::GetCurrentThreadID()).arg(g_UserSessionManager->GetClientIP()).arg(g_UserSessionManager->GetSocketID()).arg(rowUserData.getDataRef(0,"CUSR_ID").toInt()).arg(RetOut_strSessionID);
		g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_CLIENT_LOGGED,strMsg);
	}

}




/*!
	Logout:
	- delete all used data

	\param Ret_Status			- if err occur, returned here
	\param pDbConn				- db conn
*/
void Service_UserLogon::Logout(Status& Ret_Status)
{
	DbRecordSet rowUserData;
	g_UserSessionManager->GetUserData(rowUserData);
	QString strSessionID=m_SessionManager->GetSessionID();
	if (rowUserData.getRowCount()>0)
	{
		QString strMsg=QString("%1;%2;%3;%4;%5;%6").arg(rowUserData.getDataRef(0,"CUSR_USERNAME").toString()).arg(ThreadIdentificator::GetCurrentThreadID()).arg(g_UserSessionManager->GetClientIP()).arg(g_UserSessionManager->GetSocketID()).arg(rowUserData.getDataRef(0,"CUSR_ID").toInt()).arg(strSessionID);
		g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_CLIENT_LOGOUT,strMsg);
	}
	
	g_PrivateServiceSet->CoreUser->DestroyUserContextData(Ret_Status,strSessionID);
}




//chg pass:
void Service_UserLogon::ChangePassword(Status& Ret_Status,QString strUsername, QByteArray strAuthToken,QByteArray strClientNonce,QByteArray newPass,QString strNewUserName)
{
	DbRecordSet rowUserData;
	DbConnectionReserver conn(Ret_Status,GetDbManager()); //reserve global connection
	if(!Ret_Status.IsOK())return;
	
	//--------------------------------------------------------
	// AUTHENTICATE USER
	//--------------------------------------------------------

	//load from DB username, password, group, 
	g_PrivateServiceSet->CoreUser->GetUserByUserName(Ret_Status,strUsername,rowUserData);
	if(!Ret_Status.IsOK())return;

	//is user exists:
	if(rowUserData.getRowCount()!=1)
	{Ret_Status.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_WRONG_PASS_USER);return;}

	//is user active:
	if(rowUserData.getDataRef(0,"CUSR_ACTIVE_FLAG").toInt()!=1)
	{Ret_Status.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_NO_ACTIVE);return;}

	//B.T. 2013.02.05: if not SPC user then store as hash and check
	if (rowUserData.getDataRef(0,"BPER_FPERSON_SEQ").toInt()==0)
	{
		QByteArray newPassClean=QByteArray::fromBase64(newPass);
		newPass=Authenticator::GeneratePassword(strNewUserName,newPassClean);

		//test password:
		QByteArray bytePassword=rowUserData.getDataRef(0,"CUSR_PASSWORD").toByteArray();
		if(!Authenticator::Authenticate(strClientNonce,strAuthToken,strUsername,bytePassword))
		{Ret_Status.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_WRONG_PASS_USER);return;}
	}


	//--------------------------------------------------------
	// SET NEW PASS
	//--------------------------------------------------------

	DbSqlQuery query(Ret_Status,GetDbManager());
	if(!Ret_Status.IsOK())return;


	QString strSQL="UPDATE CORE_USER SET CUSR_PASSWORD=?, CUSR_USERNAME=? WHERE CUSR_USERNAME=?";
	query.Prepare(Ret_Status,strSQL);
	if(!Ret_Status.IsOK())
	{
		return;
	}
	query.bindValue(0,newPass);
	query.bindValue(1,strNewUserName);
	query.bindValue(2,strUsername);
	query.ExecutePrepared(Ret_Status);
	if(!Ret_Status.IsOK())
	{
		return;
	}


}



/*!
Called by client to notify that is alive

\param Ret_Status			- if err occur, returned here
\param pDbConn				- db conn
*/
void Service_UserLogon::IsAlive(Status& Ret_Status)
{
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::MSG_SYSTEM_CLIENT_IS_ALIVE,QVariant(ThreadIdentificator::GetCurrentThreadID()).toString()+";"+g_UserSessionManager->GetClientIP()+";"+QVariant(g_UserSessionManager->GetUserID()).toString());
	Ret_Status.setError(0);
}

