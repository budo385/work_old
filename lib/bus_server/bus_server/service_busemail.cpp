#include "service_busemail.h"
#include "db/db/dbsimpleorm.h"
#include "db_core/db_core/dbsqltableview.h"
#include "bus_core/bus_core/globalconstants.h"
#include "businesslocker.h"
#include "common/common/datahelper.h"
#include "common/common/threadid.h"

#include "businessserviceset.h"
extern BusinessServiceSet* g_BusinessServiceSet;
#include "privateserviceset.h"
extern PrivateServiceSet* g_PrivateServiceSet; 
#include "bus_core/bus_core/usersessionmanager.h"
extern UserSessionManager	*g_UserSessionManager;
#include "common/common/logger.h"
extern Logger g_Logger;		


//Reads all data for 1 mail
void Service_BusEmail::Read(Status &Ret_pStatus, int nEmailID, DbRecordSet &RetOut_Data, DbRecordSet &Ret_ContactLink,DbRecordSet &Ret_ScheduleTask,DbRecordSet &Ret_ContactEmails,  DbRecordSet &Ret_Attachments,DbRecordSet &Ret_UAR,DbRecordSet &Ret_GAR )
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_EMAIL, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	//DbSimpleOrm TableOrm_CE(Ret_pStatus, CE_COMM_ENTITY, GetDbManager()); 
	//if(!Ret_pStatus.IsOK()) return;
	DbSimpleOrm TableOrm_Emails(Ret_pStatus, BUS_CM_EMAIL, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	DbSimpleOrm TableOrm_Attachments(Ret_pStatus, BUS_EMAIL_ATTACHMENT, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	DbSimpleOrm BUS_EMAIL_BINARY_TableOrm(Ret_pStatus, BUS_EMAIL_BINARY, GetDbManager());
	if(!Ret_pStatus.IsOK()) return;


	QString strWhere ="WHERE BEM_ID="+QVariant(nEmailID).toString();
	
	//--------------------------------
	//load  EMAIL + COMM ENTITY in one sweep
	//--------------------------------
	TableOrm.Read(Ret_pStatus, RetOut_Data, strWhere,DbSqlTableView::TVIEW_BUS_EMAIL_COMM_ENTITY);
	if(!Ret_pStatus.IsOK()) return;

	//empty row?
	if (RetOut_Data.getRowCount()==0)
		return;

	int nCommEntityID=RetOut_Data.getDataRef(0,"BEM_COMM_ENTITY_ID").toInt();
	int nTaskEntityID=RetOut_Data.getDataRef(0,"CENT_TASK_ID").toInt();


	//--------------------------------
	//load  ATTACHMENT
	//--------------------------------
	strWhere ="WHERE BEA_EMAIL_ID="+QVariant(nEmailID).toString();
	TableOrm_Attachments.Read(Ret_pStatus, Ret_Attachments, strWhere);
	if(!Ret_pStatus.IsOK()) return;


	//--------------------------------
	//load  blob binary:
	//--------------------------------
	DbRecordSet rowBinaryData;
	strWhere ="WHERE BEB_EMAIL_ID="+QVariant(nEmailID).toString();
	BUS_EMAIL_BINARY_TableOrm.Read(Ret_pStatus, rowBinaryData, strWhere);
	if(!Ret_pStatus.IsOK()) return;
	if (rowBinaryData.getRowCount()>0 && RetOut_Data.getRowCount()>0)
	{
		RetOut_Data.setData(0,"BEM_BODY", rowBinaryData.getDataRef(0,"BEB_BODY"));
	}

	//--------------------------------
	//load  emails for all contacts in link:
	//--------------------------------
	//read linkage tables:
	if (nCommEntityID!=0)
	{
		//read contact link:
		strWhere ="WHERE BNMR_TABLE_KEY_ID_1="+QVariant(nCommEntityID).toString()+" AND "+NMRX_CE_CONTACT_WHERE;
		g_BusinessServiceSet->ClientSimpleORM->Read(Ret_pStatus,BUS_NMRX_RELATION,Ret_ContactLink,strWhere,-1,false);
		if(!Ret_pStatus.IsOK()) return;

		//read all emails from server:
		TableOrm_Emails.ReadFromParentIDs(Ret_pStatus, Ret_ContactEmails, "BCME_CONTACT_ID",Ret_ContactLink,"BNMR_TABLE_KEY_ID_2");
		if(!Ret_pStatus.IsOK()) return;
	}

	//--------------------------------
	//load  TASK record
	//--------------------------------
	if (nTaskEntityID!=0)
	{
		strWhere ="WHERE BTKS_ID="+QVariant(nTaskEntityID).toString();
		g_BusinessServiceSet->ClientSimpleORM->Read(Ret_pStatus,BUS_TASKS,Ret_ScheduleTask,strWhere,-1,false);
		if(!Ret_pStatus.IsOK()) return;
	}

	//read uar/gar:
	g_BusinessServiceSet->AccessRights->ReadGUAR(Ret_pStatus,nEmailID,BUS_EMAIL,Ret_UAR,Ret_GAR);
}

void Service_BusEmail::WriteMultiple(Status &Ret_pStatus, DbRecordSet &RetOut_Data, DbRecordSet &RetOut_ContactLink, DbRecordSet &RetOut_ScheduleTask,  DbRecordSet &RetOut_Attachments,QString strLockRes, bool bSkipExisting, int &RetOut_nSkippedOrReplacedCnt, bool bAddDupsAsNewRow)
{
	int nCount = RetOut_Data.getRowCount();
	DbRecordSet TempReturnListOfNewEmailIDs;
	TempReturnListOfNewEmailIDs.addColumn(QVariant::Int,"BEM_ID");

	//B.T. 2014-07-30: use cache instead of SQL for each row if there are more then 2 emails for user:
	DbRecordSet lstChkSumCache;
	bool bUseChkSumCache=false;
	if (nCount>1)
	{
		bool bDifferentUserDetected=false;
		int nOwnerID=RetOut_Data.getDataRef(0, "CENT_OWNER_ID").toInt();
		for(int i=1; i<nCount; i++)
		{
			if (nOwnerID!=RetOut_Data.getDataRef(i, "CENT_OWNER_ID").toInt())
			{
				bDifferentUserDetected=true;
				break;
			}
		}

		if (!bDifferentUserDetected)
		{
			QTime timer;
			timer.start();
			//load all chksum id's in one list per user:
			LoadAllUserEmailCheckSumUID(Ret_pStatus,lstChkSumCache,nOwnerID);
			int nElapsed=timer.elapsed();
			g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0, QString("Loading all checksums size %1, time elapsed: %2ms,  thread: %3").arg(lstChkSumCache.getRowCount()).arg(nElapsed).arg(ThreadIdentificator::GetCurrentThreadID()));
			if (!Ret_pStatus.IsOK())
				return;
			bUseChkSumCache=true;

			
		}
	}

	for(int i=0; i<nCount; i++)
	{
		if(bSkipExisting)//B.T. changed 2014-04-16, was commented!?, now it's ok...trust me I know what I'm doing :)
		{
			//skip if email exists
			//QByteArray ar = RetOut_Data.getDataRef(i, "BEM_EXT_ID").toByteArray();
			int nOwnerID=RetOut_Data.getDataRef(i, "CENT_OWNER_ID").toInt(); //B.T. changed 2014-04-16, find by ext id and owner..as same email can be imported twice for different users...
			int nEmailID = -1;
			if (bUseChkSumCache)
			{
				QString strChkSum = CalcCheckSum(RetOut_Data.getRow(i),nOwnerID);
				int nRow=lstChkSumCache.find("BEM_EXT_ID_CHKSUM",strChkSum,true);
				if (nRow>=0)
					nEmailID=lstChkSumCache.getDataRef(nRow,"BEM_ID").toInt();
			}
			else
				nEmailID = FindEmailByUID(RetOut_Data.getRow(i),nOwnerID);

			if(nEmailID >= 0){
				if(bAddDupsAsNewRow){
					RetOut_Data.setData(i, "BEM_EXT_ID", QVariant(QVariant::ByteArray)); //reset email ID
				}
				else{
					g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("Skip importing message subject=%1 (UID already exists in DB), thread: %2").arg(RetOut_Data.getDataRef(i,"BEM_SUBJECT").toString()).arg(ThreadIdentificator::GetCurrentThreadID()));
					RetOut_nSkippedOrReplacedCnt ++;  //count skipped or replaced items
					if(bSkipExisting)
					{
						//BT: 09.03.2011: return list must have same No of Rows as incoming, skipped mark as id=0 (to avoid attachment import in email import thread)
						TempReturnListOfNewEmailIDs.addRow(); //return in list all ID's: new and skipped...
						//TempReturnListOfNewEmailIDs.setData(TempReturnListOfNewEmailIDs.getRowCount()-1,"BEM_ID",nEmailID);
						TempReturnListOfNewEmailIDs.setData(TempReturnListOfNewEmailIDs.getRowCount()-1,"BEM_ID",0);
						continue;
					}
					else{
						//overwrite over the same mail
						RetOut_Data.setData(i, "BEM_ID", nEmailID);
					}
				}
			}
		}

		DbRecordSet lstAtts;
		lstAtts.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_EMAIL_ATTACHMENT));
		if(RetOut_Attachments.getRowCount()>0){
			lstAtts.merge(RetOut_Attachments.getDataRef(i,0).value<DbRecordSet>());
		}
		DbRecordSet lstContactLinks;
		lstContactLinks.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION));
		if(RetOut_ContactLink.getRowCount()>0){
			lstContactLinks.merge(RetOut_ContactLink.getDataRef(i,0).value<DbRecordSet>());
		}

		DbRecordSet rowEmail=RetOut_Data.getRow(i);
		//BT: support for UAR/GAR:
		DbRecordSet lstUAR,lstGAR;
		if (rowEmail.getColumnIdx("UAR_TABLE")>=0)
			lstUAR=rowEmail.getDataRef(0,"UAR_TABLE").value<DbRecordSet>();
		if (rowEmail.getColumnIdx("GAR_TABLE")>=0)
			lstGAR=rowEmail.getDataRef(0,"GAR_TABLE").value<DbRecordSet>();

		DbRecordSet empty;
		Write(	Ret_pStatus, 
				rowEmail, 
				lstContactLinks,
				empty, //RetOut_ScheduleTask.getRow(i),
				lstAtts,
				strLockRes,
				lstUAR,
				lstGAR);
		
		//rowEmail.Dump();
		TempReturnListOfNewEmailIDs.merge(rowEmail);
		//TOFIX break on first error?
	}

	RetOut_Data.clear();
	RetOut_ContactLink.clear();
	RetOut_Attachments.clear();
	RetOut_Data=TempReturnListOfNewEmailIDs;
	//RetOut_Data.Dump();


}

//Writes: 1 email, CE entity, task and contact link + attachemnt -> all in one transaction
void Service_BusEmail::Write(Status &Ret_pStatus, DbRecordSet &RetOut_Data, DbRecordSet &RetOut_ContactLink, DbRecordSet &RetOut_ScheduleTask,  DbRecordSet &RetOut_Attachments,QString strLockRes,DbRecordSet &RetOut_UAR,DbRecordSet &RetOut_GAR)
{
	DbSimpleOrm BUS_EMAIL_TableOrm(Ret_pStatus, BUS_EMAIL, GetDbManager());
	if(!Ret_pStatus.IsOK()) return;
	DbSimpleOrm CE_COMM_ENTITY_TableORM(Ret_pStatus, CE_COMM_ENTITY, GetDbManager());
	if(!Ret_pStatus.IsOK()) return;
	DbSimpleOrm BUS_TASKS_TableORM(Ret_pStatus, BUS_TASKS, GetDbManager());
	if(!Ret_pStatus.IsOK()) return;
	DbSimpleOrm BUS_EMAIL_BINARY_TableOrm(Ret_pStatus, BUS_EMAIL_BINARY, GetDbManager());
	if(!Ret_pStatus.IsOK()) return;

	//DbSimpleOrm BUS_EMAIL_ATTACH_TableORM(Ret_pStatus, BUS_EMAIL_ATTACHMENT, GetDbManager());
	//if(!Ret_pStatus.IsOK()) return;


	//--------------------------------------
	//START TRANSACTION
	//--------------------------------------
	BUS_EMAIL_TableOrm.GetDbSqlQuery()->BeginTransaction(Ret_pStatus);
	if(!Ret_pStatus.IsOK()) return;

	//--------------------------------------
	//INSERT BUS_TASKS
	//--------------------------------------
	bool bNewTaskCreated=false;
	if (RetOut_ScheduleTask.getRowCount() > 0)
	{
		if (RetOut_ScheduleTask.getDataRef(0,"BTKS_ID").toInt()<=0)
			bNewTaskCreated=true;

		//Write.
		BUS_TASKS_TableORM.Write(Ret_pStatus, RetOut_ScheduleTask);
		if (!Ret_pStatus.IsOK())
		{
			BUS_EMAIL_TableOrm.GetDbSqlQuery()->Rollback();
			return;
		}

		//Get BTKS_ID and add it to CE_COMM_ENTITY.
		int nBTKS_ID = RetOut_ScheduleTask.getDataRef(0, "BTKS_ID").toInt();
		RetOut_Data.setData(0, "CENT_TASK_ID", nBTKS_ID);
	}


	//---------------------------------------
	//write CE_COMM_ENTITY
	//---------------------------------------

	//BT:01.03.2011: generate TOOLTIP from body:

	DbRecordSet rowCommEntity;
	rowCommEntity.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CE_COMM_ENTITY));
	rowCommEntity.merge(RetOut_Data);

	int nCharsNumber=250;
	if (RetOut_Data.getRowCount()>0)
	{
		QString strBody = RetOut_Data.getDataRef(0, "BEM_BODY").toString();
		//If html - BEM_EMAIL_TYPE = 1, if text BEM_EMAIL_TYPE = 0.
		if (RetOut_Data.getDataRef(0, "BEM_EMAIL_TYPE").toInt())
		{
			strBody = DataHelper::ExtractTextFromHTML(strBody);
			strBody = strBody.left(250);
			strBody = strBody.remove("&nbsp;", Qt::CaseInsensitive);
			strBody = strBody.remove("&#8217;", Qt::CaseInsensitive);
			strBody = strBody.remove("&#8230;", Qt::CaseInsensitive);
		}
		else
		{
			strBody = strBody.left(nCharsNumber);
		}
		rowCommEntity.setData(0,"CENT_TOOLTIP",strBody);
	}


	CE_COMM_ENTITY_TableORM.Write(Ret_pStatus, rowCommEntity);
	if (!Ret_pStatus.IsOK())
	{
		BUS_EMAIL_TableOrm.GetDbSqlQuery()->Rollback();
		return;
	}
	//Get CENT_ID and add it to output lists:
	int nCENT_ID = rowCommEntity.getDataRef(0, "CENT_ID").toInt();
	RetOut_Data.setColValue(RetOut_Data.getColumnIdx("CENT_ID"),rowCommEntity.getDataRef(0,"CENT_ID"));
	RetOut_Data.setColValue(RetOut_Data.getColumnIdx("BEM_COMM_ENTITY_ID"),rowCommEntity.getDataRef(0,"CENT_ID"));
	RetOut_ContactLink.setColValue(RetOut_ContactLink.getColumnIdx("BNMR_TABLE_KEY_ID_1"), nCENT_ID);


	//---------------------------------------
	//write EMAIL
	//---------------------------------------
	DbRecordSet rowEmail;
	rowEmail.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_EMAIL));
	rowEmail.merge(RetOut_Data);

	//rowEmail.Dump();
	int nChksumCol = rowEmail.getColumnIdx("BEM_EXT_ID_CHKSUM");
	int nExtIDCol = rowEmail.getColumnIdx("BEM_EXT_ID");

	//B.T. 2014-07-30: insert body into separate table: (we always operate on 1 row):
	DbRecordSet rowBinaryBody;
	rowBinaryBody.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_EMAIL_BINARY));
	rowBinaryBody.addRow();

	//calc size and checksum:
	rowEmail.setData(0,"BEM_SIZE",rowEmail.getDataRef(0,"BEM_BODY").toByteArray().size());
	QString strChkSum = CalcCheckSum(rowEmail,rowCommEntity.getDataRef(0,"CENT_OWNER_ID").toInt());
	rowEmail.setData(0, nChksumCol, strChkSum);
	rowBinaryBody.setData(0,"BEB_BODY", rowEmail.getDataRef(0,"BEM_BODY"));
	rowEmail.setData(0,"BEM_BODY",QVariant(QVariant::String)); //set null
	//if (rowEmail.getDataRef(0,"").toInt()==1)
	rowEmail.setData(0,"BEM_MAILBOX_TYPE",GlobalConstants::TYPE_MAILBOX_INBOX); //set mailbox type to inbox for all new (incoming, outgoing)
		
		/*
		// update base64 "checksum" field
		QByteArray arExtID = rowEmail.getDataRef(i, nExtIDCol).toByteArray();
		if(arExtID.length() > 0)
		{

			rowEmail.setData(i, nChksumCol, QString(arExtID.toBase64().constData()));
		}
		*/
	

	//rowEmail.Dump();
	bool bInsert=false;
	int nEmailID=rowEmail.getDataRef(0,"BEM_ID").toInt();
	if (nEmailID<=0)
		bInsert=true;
	
	BUS_EMAIL_TableOrm.Write(Ret_pStatus,rowEmail);
	if(!Ret_pStatus.IsOK())
	{
		BUS_EMAIL_TableOrm.GetDbSqlQuery()->Rollback();
		return;
	}
	
	//assign doc id back: 1-1
	nEmailID=rowEmail.getDataRef(0,"BEM_ID").toInt();
	RetOut_Data.setColValue(RetOut_Data.getColumnIdx("BEM_ID"),nEmailID);

	if (!bInsert)
	{
		QString strWhereBinary ="WHERE BEB_EMAIL_ID="+QVariant(nEmailID).toString();
		QString newBody = rowBinaryBody.getDataRef(0,"BEB_BODY").toString();
		BUS_EMAIL_BINARY_TableOrm.Read(Ret_pStatus,rowBinaryBody,strWhereBinary);
		if(!Ret_pStatus.IsOK())
		{
			BUS_EMAIL_TableOrm.GetDbSqlQuery()->Rollback();
			return;
		}
		if (rowBinaryBody.getRowCount()==0)
		{
			rowBinaryBody.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_EMAIL_BINARY));
			rowBinaryBody.addRow();
		}
		rowBinaryBody.setData(0,"BEB_BODY",newBody);
	}



	//write body, but test if already exists:
	rowBinaryBody.setData(0,"BEB_EMAIL_ID",nEmailID);
	BUS_EMAIL_BINARY_TableOrm.Write(Ret_pStatus,rowBinaryBody);
	if(!Ret_pStatus.IsOK())
	{
		BUS_EMAIL_TableOrm.GetDbSqlQuery()->Rollback();
		return;
	}



	//RetOut_Attachments.setColValue(RetOut_Attachments.getColumnIdx("BEA_EMAIL_ID"),nEmailID);
	//---------------------------------------
	//write ATTACHMENT
	//---------------------------------------
	//issue 2664: somehow IS_ZIP flew to the server...
	if (RetOut_Attachments.getColumnIdx("IS_ZIP")>0)
		RetOut_Attachments.removeColumn(RetOut_Attachments.getColumnIdx("IS_ZIP"));
	BUS_EMAIL_TableOrm.WriteSubData(Ret_pStatus,BUS_EMAIL_ATTACHMENT,RetOut_Attachments,"BEA_EMAIL_ID",nEmailID,true,0,"");
	if (!Ret_pStatus.IsOK())
	{
		BUS_EMAIL_TableOrm.GetDbSqlQuery()->Rollback();
		return;
	}

	//--------------------------------------
	//INSERT CE_CONTACT_LINK
	//--------------------------------------
	g_BusinessServiceSet->BusNMRX->WriteCEContactLink(Ret_pStatus,rowCommEntity,RetOut_ContactLink);
	if (!Ret_pStatus.IsOK())
	{
		BUS_EMAIL_TableOrm.GetDbSqlQuery()->Rollback();
		return;
	}

	
	//UAR/GAR:
	g_BusinessServiceSet->AccessRights->WriteGUAR(Ret_pStatus,nEmailID,BUS_EMAIL,RetOut_UAR,RetOut_GAR);
	if (!Ret_pStatus.IsOK())
	{
		BUS_EMAIL_TableOrm.GetDbSqlQuery()->Rollback();
		return;
	}
	g_BusinessServiceSet->AccessRights->UpdateProjectCommEntityGUAR(Ret_pStatus,BUS_EMAIL,RetOut_Data);
	if(!Ret_pStatus.IsOK()) 
	{
		BUS_EMAIL_TableOrm.GetDbSqlQuery()->Rollback();
		return;
	}
	//re-read uar/gar (only for inherit-enabled entity: project, voice_call, email, doc)
	g_BusinessServiceSet->AccessRights->ReadGUAR(Ret_pStatus,nEmailID,BUS_EMAIL,RetOut_UAR,RetOut_GAR);
	if(!Ret_pStatus.IsOK()) 
	{
		BUS_EMAIL_TableOrm.GetDbSqlQuery()->Rollback();
		return;
	}
	

	//--------------------------------------
	//If all good commit.
	//--------------------------------------
	BUS_EMAIL_TableOrm.GetDbSqlQuery()->Commit(Ret_pStatus);

	//---------------------------------------
	//			UNLOCK:
	//---------------------------------------

	if (!strLockRes.isEmpty())
	{
		BusinessLocker Locker(Ret_pStatus, CE_COMM_ENTITY, GetDbManager());
		if(!Ret_pStatus.IsOK())return;
		Locker.UnLock(Ret_pStatus,strLockRes);
	}

	if (Ret_pStatus.IsOK())
	{
		g_PrivateServiceSet->BusinessHelper->ClientTaskNotification(Ret_pStatus,RetOut_ScheduleTask,bNewTaskCreated);
	}
}


//Attachments : list in list: every row coresponds to list of attach of one email
void Service_BusEmail::UpdateEmailFromClient(Status &Ret_pStatus, DbRecordSet &Emails, DbRecordSet &Attachments)
{
	QString strLockRes;

	DbSimpleOrm BUS_EMAIL_TableOrm(Ret_pStatus, BUS_EMAIL, GetDbManager());
	if(!Ret_pStatus.IsOK()) return;
	DbSimpleOrm BUS_EMAIL_ATTACH_TableORM(Ret_pStatus, BUS_EMAIL_ATTACHMENT, GetDbManager());
	if(!Ret_pStatus.IsOK()) return;
	BusinessLocker Locker(Ret_pStatus,CE_COMM_ENTITY,GetDbManager());
	if(!Ret_pStatus.IsOK()) return;

	//Emails.Dump();
	//Attachments.Dump();

	if (Emails.getRowCount()==0) return;

	DbRecordSet tmpLock;
	tmpLock.addColumn(QVariant::Int,"CENT_ID");
	tmpLock.merge(Emails);

	Locker.Lock(Ret_pStatus,tmpLock,strLockRes);
	if(!Ret_pStatus.IsOK()) return;

	//--------------------------------------
	//START TRANSACTION
	//--------------------------------------
	BUS_EMAIL_TableOrm.GetDbSqlQuery()->BeginTransaction(Ret_pStatus);
	if(!Ret_pStatus.IsOK()) return;

	//---------------------------------------
	//write EMAIL
	//---------------------------------------
	//reset flag->sync
	Emails.setColValue(Emails.getColumnIdx("BEM_IS_SYNC_IN_PROGRESS"),0);
	BUS_EMAIL_TableOrm.Write(Ret_pStatus,Emails,DbSqlTableView::TVIEW_BUS_EMAIL_UPDATE);
	if(!Ret_pStatus.IsOK())
	{
		BUS_EMAIL_TableOrm.GetDbSqlQuery()->Rollback();
		return;
	}
	
	//---------------------------------------
	//write ATTACHMENT list in list 
	//---------------------------------------
	//Delete previous:
	BUS_EMAIL_ATTACH_TableORM.DeleteFromParentIDs(Ret_pStatus,"BEA_EMAIL_ID",Emails,"BEM_ID");
	if(!Ret_pStatus.IsOK())
	{
		BUS_EMAIL_TableOrm.GetDbSqlQuery()->Rollback();
		Status err;
		Locker.UnLock(err,strLockRes);
		return;
	}


	//write new if we have:
	if (Attachments.getRowCount()>0)
	{

		DbRecordSet lstAttachmentAll;
		lstAttachmentAll.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_EMAIL_ATTACHMENT));
		int nEmailIdx=lstAttachmentAll.getColumnIdx("BEA_EMAIL_ID");
		
		//extract list in list from attachment
		int nSize=Emails.getRowCount();
		for(int i=0;i<nSize;++i)
		{	
			int nEmailID=Emails.getDataRef(0,"BEM_ID").toInt();
			DbRecordSet lstAttachment=Attachments.getDataRef(i,0).value<DbRecordSet>();
			//lstAttachment.Dump();
			lstAttachment.setColValue(nEmailIdx,nEmailID);
			lstAttachmentAll.merge(lstAttachment);
		}
		if (lstAttachmentAll.getRowCount()>0)
		{
			BUS_EMAIL_ATTACH_TableORM.Write(Ret_pStatus,lstAttachmentAll);
			if(!Ret_pStatus.IsOK())
			{
				BUS_EMAIL_TableOrm.GetDbSqlQuery()->Rollback();
				Status err;
				Locker.UnLock(err,strLockRes);
				return;
			}
		}
	}

	
	//--------------------------------------
	//If all good commit.
	//--------------------------------------
	BUS_EMAIL_TableOrm.GetDbSqlQuery()->Commit(Ret_pStatus);


	//---------------------------------------
	//			UNLOCK:
	//---------------------------------------

	Locker.UnLock(Ret_pStatus,strLockRes);
}


void Service_BusEmail::List(Status &Ret_pStatus, DbRecordSet &RetOut_lstData, QDate &datDateFrom, QDate &datDateTo, int nOwnerID, int nFilterID)
{
	Ret_pStatus.setError(0);

	//Make local query, can fail if connection reservation fails.
	DbSqlQuery query(Ret_pStatus,GetDbManager());
	if(!Ret_pStatus.IsOK()) return;

	QDateTime BeginTime(datDateFrom);
	

	QDateTime EndTime;
	//If FROM-TO filter.
	if (nFilterID == 3)
		EndTime.setDate(datDateTo);
	else
	{
		EndTime.setDate(datDateFrom);
		EndTime	= EndTime.addDays(1);
		EndTime	= EndTime.addMSecs(-1);
	}

	//Prepare query.
	QString strSQL="SELECT * FROM BUS_EMAIL, CE_COMM_ENTITY WHERE CE_COMM_ENTITY.CENT_SYSTEM_TYPE_ID="+QVariant(GlobalConstants::CE_TYPE_EMAIL).toString()+" AND CE_COMM_ENTITY.CENT_ID=BUS_EMAIL.BEM_COMM_ENTITY_ID ";
	if(nOwnerID > 0)
		strSQL += QString().sprintf("AND CE_COMM_ENTITY.CENT_OWNER_ID=%d ", nOwnerID);

	if (nFilterID == 0)
	{
		query.Execute(Ret_pStatus, strSQL);
		if(!Ret_pStatus.IsOK()) return;
	}
	else if (nFilterID == 1)
	{
		strSQL += "AND BEM_RECV_TIME >= ?";
		//Prepare statement for execution.
		query.Prepare(Ret_pStatus, strSQL);
		//Bind date.
		query.bindValue(0, BeginTime);
		//Execute prepared
		query.ExecutePrepared(Ret_pStatus);
		if(!Ret_pStatus.IsOK()) return;
	}
	else if (nFilterID == 2 || nFilterID == 3)
	{
		strSQL += "AND BEM_RECV_TIME BETWEEN ? AND ?";
		//Prepare statement for execution.
		query.Prepare(Ret_pStatus, strSQL);
		//Bind date.
		query.bindValue(0, BeginTime);
		query.bindValue(1, EndTime);
		//Execute prepared.
		query.ExecutePrepared(Ret_pStatus);
		if(!Ret_pStatus.IsOK()) return;
	}
	else
		Q_ASSERT_X(0, "Filter!", "what filter is this!");

	//If everything went fine fetch records.
	DbRecordSet rsResult;
	query.FetchData(rsResult);


	RetOut_lstData.clear();
	RetOut_lstData.merge(rsResult);
}

void Service_BusEmail::Delete(Status &Ret_pStatus, DbRecordSet &RetOut_lstIDs)
{
	//RetOut_lstIDs.Dump();
	Ret_pStatus.setError(0);

	//Make local query, can fail if connection reservation fails.
	DbSqlQuery query(Ret_pStatus,GetDbManager());
	if(!Ret_pStatus.IsOK()) return;

	//delete rows in chunks
	QString strSQL = "DELETE FROM CE_COMM_ENTITY WHERE CENT_SYSTEM_TYPE_ID="+QVariant(GlobalConstants::CE_TYPE_EMAIL).toString()+" AND CENT_ID IN";

	int nNumRows = RetOut_lstIDs.getRowCount();
	int nStartFrom = 0;
	while(nStartFrom < nNumRows)
	{
		QString strWhere;
		nStartFrom = DbSqlTableDefinition::GenerateChunkedWhereStatement(RetOut_lstIDs, "CENT_ID", strWhere, nStartFrom);

		QString strQuery = strSQL + strWhere;
		query.Execute(Ret_pStatus, strQuery);
		if(!Ret_pStatus.IsOK()) return;
	}

	RetOut_lstIDs.clear();
}



void Service_BusEmail::WriteEmailLog(Status &Ret_pStatus, DbRecordSet &RetOut_Data, QString strLockRes)
{

	DbSimpleOrm BUS_EMAIL_TableOrm(Ret_pStatus, BUS_EMAIL, GetDbManager());
	if(!Ret_pStatus.IsOK()) return;
	DbSimpleOrm CE_COMM_ENTITY_TableORM(Ret_pStatus, CE_COMM_ENTITY, GetDbManager());
	if(!Ret_pStatus.IsOK()) return;
	DbSimpleOrm BUS_TASKS_TableORM(Ret_pStatus, BUS_TASKS, GetDbManager());
	if(!Ret_pStatus.IsOK()) return;


	//--------------------------------------
	//START TRANSACTION
	//--------------------------------------
	BUS_EMAIL_TableOrm.GetDbSqlQuery()->BeginTransaction(Ret_pStatus);
	if(!Ret_pStatus.IsOK()) return;


	//---------------------------------------
	//write CE_COMM_ENTITY
	//---------------------------------------
	DbRecordSet rowCommEntity;
	rowCommEntity.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CE_COMM_ENTITY));
	rowCommEntity.merge(RetOut_Data);

	CE_COMM_ENTITY_TableORM.Write(Ret_pStatus, rowCommEntity);
	if (!Ret_pStatus.IsOK())
	{
		BUS_EMAIL_TableOrm.GetDbSqlQuery()->Rollback();
		return;
	}
	//Get CENT_ID and add it to output lists:
	int nCENT_ID = rowCommEntity.getDataRef(0, "CENT_ID").toInt();
	RetOut_Data.setColValue(RetOut_Data.getColumnIdx("CENT_ID"),rowCommEntity.getDataRef(0,"CENT_ID"));
	RetOut_Data.setColValue(RetOut_Data.getColumnIdx("BEM_COMM_ENTITY_ID"),rowCommEntity.getDataRef(0,"CENT_ID"));


	//---------------------------------------
	//write EMAIL
	//---------------------------------------
	DbRecordSet rowEmail;
	rowEmail.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_EMAIL));
	rowEmail.merge(RetOut_Data);

	BUS_EMAIL_TableOrm.Write(Ret_pStatus,rowEmail);
	if(!Ret_pStatus.IsOK())
	{
		BUS_EMAIL_TableOrm.GetDbSqlQuery()->Rollback();
		return;
	}
	//assign doc id back: 1-1
	RetOut_Data.setColValue(RetOut_Data.getColumnIdx("BEM_ID"),rowEmail.getDataRef(0,"BEM_ID"));


	//--------------------------------------
	//If all good commit.
	//--------------------------------------
	BUS_EMAIL_TableOrm.GetDbSqlQuery()->Commit(Ret_pStatus);


	//---------------------------------------
	//			UNLOCK:
	//---------------------------------------

	if (!strLockRes.isEmpty())
	{
		BusinessLocker Locker(Ret_pStatus, CE_COMM_ENTITY, GetDbManager());
		if(!Ret_pStatus.IsOK())return;
		Locker.UnLock(Ret_pStatus,strLockRes);
	}
}

//B.T. :changed 2014-04-16, MB reported bug that emails for same email for two different users are not imported: find by EXT ID AND USER_ID!!!!
int Service_BusEmail::FindEmailByUID(DbRecordSet &rowEmail, int nEmailOwnerID)
{
	QString strChkSum = CalcCheckSum(rowEmail,nEmailOwnerID);
	Status status;
	DbSqlQuery query(status, g_BusinessServiceSet->GetDbManager());	
	if(!status.IsOK()){
		Q_ASSERT_X(false, "Service_BusEmail::FindEmail", status.getErrorText().toLatin1());	
		return -1;
	}

	//find email by entryID
	QString strSQL, strData;
	strSQL = "SELECT BEM_ID FROM BUS_EMAIL WHERE BEM_EXT_ID_CHKSUM =?";

	//Prepare statement for execution.
	query.Prepare(status, strSQL);
	if(!status.IsOK()){
		Q_ASSERT_X(false, "Service_BusEmail::FindEmail", status.getErrorText().toLatin1());	
		return -1;
	}
	query.bindValue(0, strChkSum);

	query.ExecutePrepared(status);
	if(!status.IsOK()){
		Q_ASSERT_X(false, "Service_BusEmail::FindEmail", status.getErrorText().toLatin1());	
		return -1;
	}

	if (query.next())
	{
		return query.value(0).toInt();
	}
	return -1;
}


//B.T. :changed 2014-04-16, MB reported bug that emails for same email for two different users are not imported: find by EXT ID AND USER_ID!!!!
void Service_BusEmail::LoadAllUserEmailCheckSumUID(Status &pStatus, DbRecordSet &lstChekSumIDs, int nEmailOwnerID)
{
	DbSqlQuery query(pStatus, g_BusinessServiceSet->GetDbManager());	
	if(!pStatus.IsOK())return;

	query.Execute(pStatus,"SELECT BEM_ID,BEM_EXT_ID_CHKSUM FROM BUS_EMAIL INNER JOIN CE_COMM_ENTITY ON BEM_COMM_ENTITY_ID=CENT_ID WHERE CENT_OWNER_ID ="+QString::number(nEmailOwnerID));
	if(!pStatus.IsOK())return;

	query.FetchData(lstChekSumIDs);
}



/*
int Service_BusEmail::FindEmail(QByteArray &arData)
{
	if(arData.size() < 1)
		return -1;

	Status status;
	DbSqlQuery query(status, GetDbManager());	
	if(!status.IsOK()){
		Q_ASSERT_X(false, "Service_BusEmail::FindEmail", status.getErrorText().toLatin1());	
		return -1;
	}

	//find email by entryID
	QString strSQL, strData;
	if(query.GetDbConnection()->driverName().indexOf("QIBASE") >= 0){
	 	strSQL = "SELECT BEM_ID FROM BUS_EMAIL WHERE BEM_EXT_ID_CHKSUM =?";
		strData = arData.toBase64().constData();
	}
	else
		strSQL = "SELECT BEM_ID FROM BUS_EMAIL WHERE BEM_EXT_ID=?";

	//Prepare statement for execution.
	query.Prepare(status, strSQL);
	if(!status.IsOK()){
		Q_ASSERT_X(false, "Service_BusEmail::FindEmail", status.getErrorText().toLatin1());	
		return -1;
	}

	if(query.GetDbConnection()->driverName().indexOf("QIBASE") >= 0)
		query.bindValue(0, strData);
	else
		query.bindValue(0, arData);

	query.ExecutePrepared(status);
	if(!status.IsOK()){
		Q_ASSERT_X(false, "Service_BusEmail::FindEmail", status.getErrorText().toLatin1());	
		return -1;
	}

	DbRecordSet rsResult;
	query.FetchData(rsResult);

	if(rsResult.getRowCount()>0){
		return rsResult.getDataRef(0,0).toInt();
	}

	return -1;
}
*/

void Service_BusEmail::SetEmailRead(Status &Ret_pStatus, int nCE_ID)
{

	//Make local query, can fail if connection reservation fails.
	DbSqlQuery query(Ret_pStatus,GetDbManager());
	if(!Ret_pStatus.IsOK()) return;
	BusinessLocker Locker(Ret_pStatus,CE_COMM_ENTITY,GetDbManager());
	if(!Ret_pStatus.IsOK()) return;


	DbLockList list;
	list<<nCE_ID;
	QString strLockRes;
	Locker.Lock(Ret_pStatus,list,strLockRes);
	if(!Ret_pStatus.IsOK()) return;

	//reset unread flag
	query.Execute(Ret_pStatus,"UPDATE BUS_EMAIL SET BEM_UNREAD_FLAG=0 WHERE BEM_COMM_ENTITY_ID="+QVariant(nCE_ID).toString());
	Status err;
	Locker.UnLock(err,strLockRes);

}

void Service_BusEmail::WriteAttachments(Status &Ret_pStatus,int nEmailID, DbRecordSet &RetOut_Attachments)
{
	DbSimpleOrm BUS_EMAIL_TableOrm(Ret_pStatus, BUS_EMAIL, GetDbManager());
	if(!Ret_pStatus.IsOK()) return;

	//---------------------------------------
	//write ATTACHMENT
	//---------------------------------------
	BUS_EMAIL_TableOrm.GetDbSqlQuery()->BeginTransaction(Ret_pStatus);
	if(!Ret_pStatus.IsOK()) return;

	BUS_EMAIL_TableOrm.WriteSubData(Ret_pStatus,BUS_EMAIL_ATTACHMENT,RetOut_Attachments,"BEA_EMAIL_ID",nEmailID,true,0,"");
	if (!Ret_pStatus.IsOK())
	{
		BUS_EMAIL_TableOrm.GetDbSqlQuery()->Rollback();
		return;
	}

	BUS_EMAIL_TableOrm.GetDbSqlQuery()->Commit(Ret_pStatus);
}

void Service_BusEmail::WriteEmailAttachmentsFromUserStorage(Status &Ret_pStatus,int nEmailID, DbRecordSet &Attachments)
{
	//DbSqlQuery query(Ret_pStatus, GetDbManager()); 
	//if(!Ret_pStatus.IsOK()) return;

	QString strDirectory=DataHelper::EncodeSession2Directory(g_UserSessionManager->GetSessionID());
	QString strServerFilePath=g_UserSessionManager->InitializeUserStorageDirectory(Ret_pStatus,strDirectory);
	if(!Ret_pStatus.IsOK()) 	
		return;


	//starting subdir is: 
	QString strAttachmentPrefix="email_attach_"+QString::number(nEmailID)+"_";

	//load each file from subdir according to data in list: do not lock, skip if file does not exist

	//query.BeginTransaction(Ret_pStatus);
	//if(!Ret_pStatus.IsOK()) return;

	int nSize=Attachments.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		QString strName=strAttachmentPrefix+Attachments.getDataRef(i,"BEA_NAME").toString();
		QString strFilePath=strServerFilePath+"/"+strName;
		int nAttachID=Attachments.getDataRef(i,"BEA_ID").toInt();

		//--------------------------------------------
		// insert from file by chunks:
		//--------------------------------------------
		QString strSQL ="UPDATE BUS_EMAIL_ATTACHMENT SET BEA_CONTENT=? WHERE BEA_ID="+QString::number(nAttachID);
		g_PrivateServiceSet->BusinessHelper->File2BlobChunked(Ret_pStatus,strSQL,strFilePath);
		if(!Ret_pStatus.IsOK()) 
		{
			//query.Rollback();
			return;
		}

		QFile::remove(strFilePath);
	}

	//query.Commit(Ret_pStatus);

}

	//B.T. 2014-07-30: new technique for chksum UID: user
QString Service_BusEmail::CalcCheckSum(DbRecordSet &rowEmail, int nEmailOwnerID)
{
	if (rowEmail.getRowCount()==0)
		return "";

	QString strToAcc=rowEmail.getDataRef(0,"BEM_TO_EMAIL_ACCOUNT").toString().left(120);
	if (strToAcc.isEmpty())
		strToAcc=rowEmail.getDataRef(0,"BEM_TO").toString().left(120);

	QString strChkSum=QString::number(nEmailOwnerID)+"-";
	strChkSum+=strToAcc+"-";
	strChkSum+=QString::number(rowEmail.getDataRef(0,"BEM_BODY").toByteArray().size())+"-";
	strChkSum+=rowEmail.getDataRef(0,"BEM_EXT_ID").toByteArray().toBase64();
	return strChkSum.left(200);
}