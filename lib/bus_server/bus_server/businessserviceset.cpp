#include "businessserviceset.h"


//init static pointers:
DbSqlManager* BusinessService::m_DbManager=NULL;
UserSessionManager* BusinessService::m_SessionManager=NULL;

/*!
	When creating service, place service instance here

	\param pDbManager			- global DbSqlManager
	\param pSessionManager		- global UserSessionManager for getting user context

*/
BusinessServiceSet::BusinessServiceSet(DbSqlManager *pDbManager,UserSessionManager* pSessionManager)
{

	//set global managers:
	m_SessionManager=pSessionManager;
	m_DbManager=pDbManager;
	Q_ASSERT_X(m_SessionManager!=NULL,"Service set","Session manager empty");
	Q_ASSERT_X(m_DbManager!=NULL,"Service set","DB manager empty");

	//init all services:
	InitServiceSet();

	//obj2
	UserLogon = new Service_UserLogon; m_ServiceList.append(dynamic_cast<Service_UserLogon*>(UserLogon));
	ModuleLicense = new Service_ModuleLicense; m_ServiceList.append(dynamic_cast<Service_ModuleLicense*>(ModuleLicense));
	BusEventLog = new Service_BusEventLog; m_ServiceList.append(dynamic_cast<Service_BusEventLog*>(BusEventLog));

	AccessRights = new Service_AccessRights; m_ServiceList.append(dynamic_cast<Service_AccessRights*>(AccessRights));
	MainEntitySelector = new Service_MainEntitySelector; m_ServiceList.append(dynamic_cast<Service_MainEntitySelector*>(MainEntitySelector));
	CoreServices = new Service_CoreServices; m_ServiceList.append(dynamic_cast<Service_CoreServices*>(CoreServices));
	BusContact = new Service_BusContact; m_ServiceList.append(dynamic_cast<Service_BusContact*>(BusContact));	
	VoiceCallCenter = new Service_VoiceCallCenter; m_ServiceList.append(dynamic_cast<Service_VoiceCallCenter*>(VoiceCallCenter));
	BusEmail = new Service_BusEmail; m_ServiceList.append(dynamic_cast<Service_BusEmail*>(BusEmail));
	Reports = new Service_Reports; m_ServiceList.append(dynamic_cast<Service_Reports*>(Reports));
	BusAddressSchemas = new Service_BusAddressSchemas; m_ServiceList.append(dynamic_cast<Service_BusAddressSchemas*>(BusAddressSchemas));
	ClientSimpleORM = new Service_ClientSimpleORM; m_ServiceList.append(dynamic_cast<Service_ClientSimpleORM*>(ClientSimpleORM));
	BusImport = new Service_BusImport; m_ServiceList.append(dynamic_cast<Service_BusImport*>(BusImport));
	StoredProjectLists = new Service_StoredProjectLists; m_ServiceList.append(dynamic_cast<Service_StoredProjectLists*>(StoredProjectLists));
	BusGridViews = new Service_BusGridViews; m_ServiceList.append(dynamic_cast<Service_BusGridViews*>(BusGridViews));
	BusNMRX = new Service_BusNMRX; m_ServiceList.append(dynamic_cast<Service_BusNMRX*>(BusNMRX));
	BusGroupTree = new Service_BusGroupTree; m_ServiceList.append(dynamic_cast<Service_BusGroupTree*>(BusGroupTree));
	BusDocuments = new Service_BusDocuments; m_ServiceList.append(dynamic_cast<Service_BusDocuments*>(BusDocuments));
	BusCommunication = new Service_BusCommunication; m_ServiceList.append(dynamic_cast<Service_BusCommunication*>(BusCommunication));
	ServerControl = new Service_ServerControl; m_ServiceList.append(dynamic_cast<Service_ServerControl*>(ServerControl));
	BusProject = new Service_BusProject; m_ServiceList.append(dynamic_cast<Service_BusProject*>(BusProject));
	BusPerson  = new Service_BusPerson; m_ServiceList.append(dynamic_cast<Service_BusPerson*>(BusPerson));
	BusSms  = new Service_BusSms; m_ServiceList.append(dynamic_cast<Service_BusSms*>(BusSms));
	BusCalendar  = new Service_BusCalendar; m_ServiceList.append(dynamic_cast<Service_BusCalendar*>(BusCalendar));

	WebContacts  = new Service_WebContacts; m_ServiceList.append(dynamic_cast<Service_WebContacts*>(WebContacts));
	WebDocuments  = new Service_WebDocuments; m_ServiceList.append(dynamic_cast<Service_WebDocuments*>(WebDocuments));
	WebEmails  = new Service_WebEmails; m_ServiceList.append(dynamic_cast<Service_WebEmails*>(WebEmails));
	WebServer  = new Service_WebServer; m_ServiceList.append(dynamic_cast<Service_WebServer*>(WebServer));
	WebProjects  = new Service_WebProjects; m_ServiceList.append(dynamic_cast<Service_WebProjects*>(WebProjects));
	WebCalendars  = new Service_WebCalendars; m_ServiceList.append(dynamic_cast<Service_WebCalendars*>(WebCalendars));
	WebSessionData  = new Service_WebSessionData; m_ServiceList.append(dynamic_cast<Service_WebSessionData*>(WebSessionData));
	WebCommunication  = new Service_WebCommunication; m_ServiceList.append(dynamic_cast<Service_WebCommunication*>(WebCommunication));

	BusCustomFields  = new Service_BusCustomFields; m_ServiceList.append(dynamic_cast<Service_BusCustomFields*>(BusCustomFields));
	//etc...

	SpcSession  = new Service_SpcSession; m_ServiceList.append(dynamic_cast<Service_SpcSession*>(SpcSession));
	SpcBusinessFigures  = new Service_SpcBusinessFigures; m_ServiceList.append(dynamic_cast<Service_SpcBusinessFigures*>(SpcBusinessFigures));
	WebMWCloudService  = new Service_WebMWCloudService; m_ServiceList.append(dynamic_cast<Service_WebMWCloudService*>(WebMWCloudService));



	//check pointers (only in debug):
#ifndef QT_NO_DEBUG
	CheckServiceSet();
#endif
}



//delete all services from list
BusinessServiceSet::~BusinessServiceSet()
{
	qDeleteAll(m_ServiceList);  //new faster way to delete all objects
	/*
		int nSize=m_ServiceList.size();
		for(int i=0;i<nSize;++i)
		{
			delete m_ServiceList.at(i);
		}
		*/
}


//init static members (just shortcut to globals)
void BusinessServiceSet::InitServiceSet()
{
	BusinessService::SetUserSessionManager(m_SessionManager);
	BusinessService::SetDbManager(m_DbManager);
}


//check for initialization
void BusinessServiceSet::CheckServiceSet()
{

	//qDeleteAll(m_ServiceList);  //new faster way to delete all objects
		
	int nSize=m_ServiceList.size();
		for(int i=0;i<nSize;++i)
		{
			if(m_ServiceList.at(i)==NULL)
				Q_ASSERT_X(false,"INIT_SRV","NULL Pointers detected!");
		
		}
	
}
