#include "db_core/db_core/dbsqltableview.h"
#include "service_buscommunication.h"
#include "bus_core/bus_core/commfiltersettingsids.h"
#include "bus_core/bus_core/globalconstants.h"
#include <QDomDocument>
#include "common/common/entity_id_collection.h"
#include "db/db/dbconnectionreserver.h"
#include "businesslocker.h"
#include "common/common/datahelper.h"
#include "trans/trans/xmlutil.h"
#include "db/db/dbsimpleorm.h"


#include "bus_core/bus_core/usersessionmanager.h"
extern UserSessionManager	*g_UserSessionManager;
#include "businessserviceset.h"
extern BusinessServiceSet* g_BusinessServiceSet;
#include "bus_core/bus_core/useraccessright.h"
extern UserAccessRight *g_AccessRight;
#include "privateserviceset.h"
extern PrivateServiceSet* g_PrivateServiceSet; 



Service_BusCommunication::~Service_BusCommunication()
{
}

void Service_BusCommunication::SaveCommFilterViews(Status &Ret_pStatus, int &Ret_nViewID, DbRecordSet &recView, DbRecordSet &recViewsData /*= NULL*/)
{
	DbSimpleOrm BUS_COMM_VIEW_ORM(Ret_pStatus, BUS_COMM_VIEW, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	//recView.Dump();
	//recViewsData.Dump();

	//--------------------------------------
	//START TRANSACTION
	//--------------------------------------
	BUS_COMM_VIEW_ORM.GetDbSqlQuery()->BeginTransaction(Ret_pStatus);
	if(!Ret_pStatus.IsOK()) return;
	
	//Write view.
	BUS_COMM_VIEW_ORM.Write(Ret_pStatus, recView);
	if (!Ret_pStatus.IsOK())
	{
		BUS_COMM_VIEW_ORM.GetDbSqlQuery()->Rollback();
		return;
	}

	Ret_nViewID = recView.getDataRef(0, 0).toInt();
	recViewsData.setColValue(3, Ret_nViewID);

	//Write settings.
	DbSimpleOrm BUS_COMM_VIEW_SETTINGS_ORM(Ret_pStatus, BUS_COMM_VIEW_SETTINGS, GetDbManager()); 
	if(!Ret_pStatus.IsOK())
	{
		BUS_COMM_VIEW_ORM.GetDbSqlQuery()->Rollback();
		return;
	}
	
	//Write.
	BUS_COMM_VIEW_SETTINGS_ORM.Write(Ret_pStatus, recViewsData);
	if(!Ret_pStatus.IsOK())
	{
		BUS_COMM_VIEW_ORM.GetDbSqlQuery()->Rollback();
		return;
	}

	//--------------------------------------
	//COMMIT TRANSACTION
	//--------------------------------------
	BUS_COMM_VIEW_ORM.GetDbSqlQuery()->Commit(Ret_pStatus);
}

//Used to get default application views.
void Service_BusCommunication::GetDefaultDesktopFilterViews(Status &Ret_pStatus, int nLoggedPersonID, DbRecordSet &RetOut_recViews /*= NULL*/)
{
	//Contact data Table ORM.
	DbSimpleOrm BUS_COMM_VIEW_ORM(Ret_pStatus, BUS_COMM_VIEW, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	QString strLoggedPersonID = QVariant(nLoggedPersonID).toString();
	QString strWhere = " WHERE BUSCV_TYPE = 0 AND BUSCV_OPEN_ON_STARTUP =1 AND (BUSCV_OWNER_ID = " + strLoggedPersonID + " OR (BUSCV_IS_PUBLIC =1 AND BUSCV_OWNER_ID <> " + strLoggedPersonID + ")) ORDER BY BUSCV_SORT_CODE";

	//Read.
	BUS_COMM_VIEW_ORM.Read(Ret_pStatus, RetOut_recViews, strWhere);
	//if(!Ret_pStatus.IsOK()) return;
}

//Used just to get views.
void Service_BusCommunication::GetCommFilterViews(Status &Ret_pStatus, DbRecordSet &RetOut_recViews, int nPersonID, int nGridType /*= NULL*/)
{
	//Contact data Table ORM.
	DbSimpleOrm BUS_COMM_VIEW_ORM(Ret_pStatus, BUS_COMM_VIEW, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	QString strPersonID = QVariant(nPersonID).toString();
	QString strGridType = QVariant(nGridType).toString();
	QString strWhere = " WHERE BUSCV_TYPE = " + strGridType + " AND (BUSCV_OWNER_ID = " + strPersonID + " OR (BUSCV_IS_PUBLIC =1 AND BUSCV_OWNER_ID <> " + strPersonID + ")) ORDER BY BUSCV_SORT_CODE";

	//Read.
	BUS_COMM_VIEW_ORM.Read(Ret_pStatus, RetOut_recViews, strWhere);
	//if(!Ret_pStatus.IsOK()) return;
}

//Used only in filter to get data to display.
void Service_BusCommunication::GetCommFilterData(Status &Ret_pStatus, DbRecordSet &RetOut_recSortRecordSet, DbRecordSet &RetOut_recViewsData, DbRecordSet &RetOut_recViewsChBoxesData, int nViewID, int nLoggedPersonID /*= NULL*/)
{
	//Reserve connection.
	DbConnectionReserver conn(Ret_pStatus,GetDbManager()); //reserve global connection
	if(!Ret_pStatus.IsOK())return;
	
	//Get record numbers to display.
	GetComplexFilterData(Ret_pStatus, RetOut_recViewsData, nLoggedPersonID);
	if(!Ret_pStatus.IsOK())return;

	//RetOut_recViewsData.Dump();

	//Settings.
	DbSimpleOrm BUS_COMM_VIEW_SETTINGS_ORM(Ret_pStatus, BUS_COMM_VIEW_SETTINGS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	QString strViewID = QVariant(nViewID).toString();
	QString strWhere = " WHERE BUSCS_VIEW_ID = " + strViewID;

	//Read.
	BUS_COMM_VIEW_SETTINGS_ORM.Read(Ret_pStatus, RetOut_recViewsChBoxesData, strWhere);
	if(!Ret_pStatus.IsOK()) return;

	//Read Sorting data from view.
	DbSimpleOrm BUS_COMM_VIEW_ORM(Ret_pStatus, BUS_COMM_VIEW, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	strWhere = " WHERE BUSCV_ID = " + strViewID;

	//Read.
	DbRecordSet recTmpView;
	BUS_COMM_VIEW_ORM.Read(Ret_pStatus, recTmpView, strWhere);
	if(!Ret_pStatus.IsOK()) return;

	if (recTmpView.getRowCount()>0)
	{
		QByteArray byteView = recTmpView.getDataRef(0, "BUSCV_COMMGRIDVIEW_SORT").toByteArray();
		RetOut_recSortRecordSet = XmlUtil::ConvertByteArray2RecordSet_Fast(byteView);
	}
	//_DUMP(m_lstSortRecordSet);

	//RetOut_recViewsChBoxesData.Dump();
}

//Used from view selector to get some  view data.
void Service_BusCommunication::GetCommFilterViewDataAndViews(Status &Ret_pStatus, DbRecordSet &RetOut_recViewData, DbRecordSet &RetOut_recViews, int nViewID, int nGridType /*= NULL*/)
{
	//Reserve connection.
	DbConnectionReserver conn(Ret_pStatus,GetDbManager()); //reserve global connection
	if(!Ret_pStatus.IsOK())return;

	int nLoggedPersonID = g_UserSessionManager->GetPersonID();

	//Get views to display in view combo box - for currently logged user. Person is for later.
	GetCommFilterViews(Ret_pStatus, RetOut_recViews, nLoggedPersonID, nGridType);
	if(!Ret_pStatus.IsOK())return;

	//settings.
	DbSimpleOrm BUS_COMM_VIEW_SETTINGS_ORM(Ret_pStatus, BUS_COMM_VIEW_SETTINGS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	QString strViewID = QVariant(nViewID).toString();
	QString strWhere = " WHERE BUSCS_VIEW_ID = " + strViewID;

	//Read.
	BUS_COMM_VIEW_SETTINGS_ORM.Read(Ret_pStatus, RetOut_recViewData, strWhere);
	
	//_DUMP(RetOut_recViewData);
}

void Service_BusCommunication::GetGridViewForContact(Status &Ret_pStatus, int &RetOut_nViewID, int nContactID /*= NULL*/)
{
	//Contact data Table ORM.
	DbSimpleOrm BUS_COMM_VIEW_ORM(Ret_pStatus, BUS_CM_CONTACT, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	QString strContactID = QVariant(nContactID).toString();
	QString strWhere = " WHERE BCNT_ID = " + strContactID;

	//Read.
	DbRecordSet recContact;
	BUS_COMM_VIEW_ORM.Read(Ret_pStatus, recContact, strWhere);

	if (recContact.getRowCount())
		RetOut_nViewID = recContact.getDataRef(0, "BCNT_DEFAULT_GRID_VIEW_ID").toInt();
}

void Service_BusCommunication::GetGridViewForProject(Status &Ret_pStatus, int &RetOut_nViewID, int nProjectID /*= NULL*/)
{
	//Contact data Table ORM.
	DbSimpleOrm BUS_COMM_VIEW_ORM(Ret_pStatus, BUS_PROJECT, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	QString strProjectID = QVariant(nProjectID).toString();
	QString strWhere = " WHERE BUSP_ID = " + strProjectID;

	//Read.
	DbRecordSet recProject;
	BUS_COMM_VIEW_ORM.Read(Ret_pStatus, recProject, strWhere);

	//recProject.Dump();

	if (recProject.getRowCount())
		RetOut_nViewID = recProject.getDataRef(0, "BUSP_DEFAULT_GRID_VIEW_ID").toInt();
}

void Service_BusCommunication::GetPersonCommData(Status &Ret_pStatus, bool &Ret_bRowsStripped, DbRecordSet &RetOut_recEmails, DbRecordSet &RetOut_recDocuments, DbRecordSet &RetOut_recVoiceCalls, int PersonID, DbRecordSet recFilter, bool bForceFilterByTaskType /*= false*/)
 {
	int nMaxRowCount=4000;
	Ret_bRowsStripped=false;

	DbConnectionReserver conn(Ret_pStatus,GetDbManager()); //reserve global connection
	if(!Ret_pStatus.IsOK())return;

	//recFilter.Dump();

	bool bFilterByEntityType = GetPersonFilterSettingBool(FILTER_BY_ENTITY_TYPE_ACTIVE, recFilter);
	
	if (!bFilterByEntityType || (bFilterByEntityType && GetPersonFilterSettingBool(SHOW_EMAILS, recFilter)) || (!bFilterByEntityType && bForceFilterByTaskType))
	{
		GetPersonEmails(Ret_pStatus, RetOut_recEmails, PersonID, recFilter, bForceFilterByTaskType);
		if(!Ret_pStatus.IsOK())return;
		int cc = RetOut_recEmails.getRowCount();

		if (RetOut_recEmails.getRowCount() > nMaxRowCount)
		{
			int m = RetOut_recEmails.getRowCount();
			RetOut_recEmails.clearSelection();
			RetOut_recEmails.selectRowFromTo(0, nMaxRowCount-1);
			RetOut_recEmails.deleteUnSelectedRows();
			Ret_bRowsStripped=true;
			int m1 = RetOut_recEmails.getRowCount();
			int m2 = RetOut_recEmails.getRowCount();
		}
	}

	if (!bFilterByEntityType || (bFilterByEntityType && GetPersonFilterSettingBool(SHOW_DOCUMENTS, recFilter)) || (!bFilterByEntityType && bForceFilterByTaskType) || GetPersonFilterSettingBool(FILTER_DOCUMENTS_BY_APPLICATION_ACTIVE, recFilter))
	{
		GetPersonDocuments(Ret_pStatus, RetOut_recDocuments, PersonID, recFilter, bForceFilterByTaskType);
		if(!Ret_pStatus.IsOK())return;

		if ((RetOut_recEmails.getRowCount() + RetOut_recDocuments.getRowCount()) > nMaxRowCount)
		{
			int rowsToDelete = (RetOut_recEmails.getRowCount() + RetOut_recDocuments.getRowCount()) - nMaxRowCount;
			RetOut_recDocuments.clearSelection();
			RetOut_recDocuments.selectRowFromTo(0, RetOut_recDocuments.getRowCount()-rowsToDelete-1);
			RetOut_recDocuments.deleteUnSelectedRows();
			Ret_bRowsStripped=true;
		}
	}

	if (!bFilterByEntityType || (bFilterByEntityType && GetPersonFilterSettingBool(SHOW_VOICE_CALLS, recFilter)) || (!bFilterByEntityType && bForceFilterByTaskType))
	{
		GetPersonVoiceCall(Ret_pStatus, RetOut_recVoiceCalls, PersonID, recFilter, bForceFilterByTaskType);
		if(!Ret_pStatus.IsOK())return;

		if ((RetOut_recEmails.getRowCount() + RetOut_recDocuments.getRowCount() + RetOut_recVoiceCalls.getRowCount()) > nMaxRowCount)
		{
			int rowsToDelete = (RetOut_recEmails.getRowCount() + RetOut_recDocuments.getRowCount() + RetOut_recVoiceCalls.getRowCount()) - nMaxRowCount;
			RetOut_recVoiceCalls.clearSelection();
			RetOut_recVoiceCalls.selectRowFromTo(0, RetOut_recVoiceCalls.getRowCount()-rowsToDelete-1);
			RetOut_recVoiceCalls.deleteUnSelectedRows();
			Ret_bRowsStripped=true;
		}
	}

}

void Service_BusCommunication::GetContactCommData(Status &Ret_pStatus, DbRecordSet &RetOut_recEmails, DbRecordSet &RetOut_recDocuments, DbRecordSet &RetOut_recVoiceCalls, int ContactID, DbRecordSet recFilter /*= NULL*/)
{
	//Reserve connection.
	DbConnectionReserver conn(Ret_pStatus,GetDbManager()); //reserve global connection
	if(!Ret_pStatus.IsOK())return;

	//recFilter.Dump();

	if (!GetPersonFilterSettingBool(FILTER_BY_ENTITY_TYPE_ACTIVE, recFilter))
		return;

	if (GetPersonFilterSettingBool(SHOW_EMAILS, recFilter))
	{
		GetContactEmails(Ret_pStatus, RetOut_recEmails, ContactID, recFilter);

		//RetOut_recEmails.Dump();
		if(!Ret_pStatus.IsOK())return;
	}
	if (GetPersonFilterSettingBool(SHOW_DOCUMENTS, recFilter) || GetPersonFilterSettingBool(FILTER_DOCUMENTS_BY_APPLICATION_ACTIVE, recFilter))
	{
		GetContactDocuments(Ret_pStatus, RetOut_recDocuments, ContactID, recFilter);
		//RetOut_recDocuments.Dump();
		if(!Ret_pStatus.IsOK())return;
	}
	if (GetPersonFilterSettingBool(SHOW_VOICE_CALLS, recFilter))
	{
		GetContactVoiceCall(Ret_pStatus, RetOut_recVoiceCalls, ContactID, recFilter);
		//RetOut_recVoiceCalls.Dump();
		if(!Ret_pStatus.IsOK())return;
	}
}

void Service_BusCommunication::GetProjectCommData(Status &Ret_pStatus, DbRecordSet &RetOut_recEmails, DbRecordSet &RetOut_recDocuments, DbRecordSet &RetOut_recVoiceCalls, int nProjectID, DbRecordSet recFilter /*= NULL*/)
{
	//Reserve connection.
	DbConnectionReserver conn(Ret_pStatus,GetDbManager()); //reserve global connection
	if(!Ret_pStatus.IsOK())return;

//	DbRecordSet recCommEntity;
//	GetProjectCommEntityIDs(Ret_pStatus, nProjectID, recCommEntity);
//	if(!Ret_pStatus.IsOK()) return;

	//recFilter.Dump();

	if (!GetPersonFilterSettingBool(FILTER_BY_ENTITY_TYPE_ACTIVE, recFilter))
		return;

	if (GetPersonFilterSettingBool(SHOW_EMAILS, recFilter))
	{
		GetProjectEmails(Ret_pStatus, RetOut_recEmails, nProjectID, recFilter);
		//RetOut_recEmails.Dump();
		if(!Ret_pStatus.IsOK())return;
	}
	if (GetPersonFilterSettingBool(SHOW_DOCUMENTS, recFilter) || GetPersonFilterSettingBool(FILTER_DOCUMENTS_BY_APPLICATION_ACTIVE, recFilter))
	{
		GetProjectDocuments(Ret_pStatus, RetOut_recDocuments, nProjectID, recFilter);
		//RetOut_recDocuments.Dump();
		if(!Ret_pStatus.IsOK())return;
	}
	if (GetPersonFilterSettingBool(SHOW_VOICE_CALLS, recFilter))
	{
		GetProjectVoiceCall(Ret_pStatus, RetOut_recVoiceCalls, nProjectID, recFilter);
		//RetOut_recVoiceCalls.Dump();
		if(!Ret_pStatus.IsOK())return;
	}
}

void Service_BusCommunication::GetCalendarCommData(Status &Ret_pStatus, DbRecordSet &RetOut_recEmails, DbRecordSet &RetOut_recDocuments, DbRecordSet &RetOut_recVoiceCalls, DbRecordSet recEntities, DbRecordSet recFilter)
{
	RetOut_recEmails.destroy();
	RetOut_recEmails.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CONTACT_COMM_GRID_EMAIL_VIEW));
	RetOut_recDocuments.destroy();
	RetOut_recDocuments.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CONTACT_COMM_GRID_DOCUMENT_VIEW));
	RetOut_recVoiceCalls.destroy();
	RetOut_recVoiceCalls.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_COMM_GRID_VOICECALL_VIEW));

	int nRowCount = recEntities.getRowCount();
	DbRecordSet recEmails;
	DbRecordSet recDocuments;
	DbRecordSet recVoiceCalls;
	for(int i = 0; i < nRowCount; i++)
	{
		recEmails.clear();
		recDocuments.clear();
		recVoiceCalls.clear();

		int nEntityID	= recEntities.getDataRef(i, "ENTITY_ID").toInt();
		int nEntityType = recEntities.getDataRef(i, "ENTITY_TYPE").toInt();
		int nPersonID	= recEntities.getDataRef(i, "ENTITY_PERSON_ID").toInt();

		if (nEntityType == GlobalConstants::TYPE_CAL_USER_SELECT)
		{
			bool bRowsStripped = false;
			GetPersonCommData(Ret_pStatus, bRowsStripped, recEmails, recDocuments, recVoiceCalls, nPersonID, recFilter);
		}
		/*
		else if (nEntityType == GlobalConstants::TYPE_CAL_CONTACT_SELECT)
		{
			GetContactCommData(Ret_pStatus, recEmails, recDocuments, recVoiceCalls, nEntityID, recFilter);
		}
		else if (nEntityType == GlobalConstants::TYPE_CAL_PROJECT_SELECT)
		{
			GetProjectCommData(Ret_pStatus, recEmails, recDocuments, recVoiceCalls, nEntityID, recFilter);
		}
		*/
		
		RetOut_recEmails.merge(recEmails);
		RetOut_recDocuments.merge(recDocuments);
		RetOut_recVoiceCalls.merge(recVoiceCalls);
	}
	//_DUMP(RetOut_recEmails);
	//_DUMP(RetOut_recDocuments);
	//_DUMP(RetOut_recVoiceCalls);
}

void Service_BusCommunication::GetCalendarGridCommData(Status &Ret_pStatus, DbRecordSet &RetOut_recEmails, DbRecordSet &RetOut_recDocuments, DbRecordSet &RetOut_recVoiceCalls, DbRecordSet recEntities, DbRecordSet recFilter)
{
	RetOut_recEmails.destroy();
	RetOut_recDocuments.destroy();
	RetOut_recVoiceCalls.destroy();

	int nRowCount = recEntities.getRowCount();
	DbRecordSet recEmails;
	DbRecordSet recDocuments;
	DbRecordSet recVoiceCalls;
	for(int i = 0; i < nRowCount; i++)
	{
		recEmails.destroy();
		recDocuments.destroy();
		recVoiceCalls.destroy();

		int nEntityID	= recEntities.getDataRef(i, "ENTITY_ID").toInt();
		int nEntityType = recEntities.getDataRef(i, "ENTITY_TYPE").toInt();
		int nPersonID	= recEntities.getDataRef(i, "ENTITY_PERSON_ID").toInt();

		if (nEntityType == GlobalConstants::TYPE_CAL_USER_SELECT)
		{
			bool bRowsStripped = false;
			GetPersonCommData(Ret_pStatus, bRowsStripped, recEmails, recDocuments, recVoiceCalls, nPersonID, recFilter, true);
		}
		
		recEmails.addColumn(QVariant::Int, "ENTITY_ID");
		recEmails.addColumn(QVariant::Int, "ENTITY_TYPE");
		recEmails.addColumn(QVariant::Int, "ENTITY_PERSON_ID");
		recDocuments.addColumn(QVariant::Int, "ENTITY_ID");
		recDocuments.addColumn(QVariant::Int, "ENTITY_TYPE");
		recDocuments.addColumn(QVariant::Int, "ENTITY_PERSON_ID");
		recVoiceCalls.addColumn(QVariant::Int, "ENTITY_ID");
		recVoiceCalls.addColumn(QVariant::Int, "ENTITY_TYPE");
		recVoiceCalls.addColumn(QVariant::Int, "ENTITY_PERSON_ID");
		
		if (!RetOut_recEmails.getColumnCount())
		{
			RetOut_recEmails.copyDefinition(recEmails);
		}
		if (!RetOut_recDocuments.getColumnCount())
		{
			RetOut_recDocuments.copyDefinition(recDocuments);
		}
		if (!RetOut_recVoiceCalls.getColumnCount())
		{
			RetOut_recVoiceCalls.copyDefinition(recVoiceCalls);
		}
 
		//_DUMP(recEmails);
		//_DUMP(recDocuments);
		//_DUMP(recVoiceCalls);

		recEmails.setColValue("ENTITY_ID", nEntityID);
		recEmails.setColValue("ENTITY_TYPE", nEntityType);
		recEmails.setColValue("ENTITY_PERSON_ID", nPersonID);
		recDocuments.setColValue("ENTITY_ID", nEntityID);
		recDocuments.setColValue("ENTITY_TYPE", nEntityType);
		recDocuments.setColValue("ENTITY_PERSON_ID", nPersonID);
		recVoiceCalls.setColValue("ENTITY_ID", nEntityID);
		recVoiceCalls.setColValue("ENTITY_TYPE", nEntityType);
		recVoiceCalls.setColValue("ENTITY_PERSON_ID", nPersonID);

		CheckForDoubleEntities(RetOut_recEmails, recEmails);
		CheckForDoubleEntities(RetOut_recDocuments, recDocuments);
		CheckForDoubleEntities(RetOut_recVoiceCalls, recVoiceCalls);
	}

	//_DUMP(RetOut_recEmails);
	//_DUMP(RetOut_recDocuments);
	//_DUMP(RetOut_recVoiceCalls);
}

void Service_BusCommunication::GetCommGridDataByIDs(Status &Ret_pStatus, DbRecordSet &RetOut_recData, DbRecordSet recIDsRecordset, int nGridType, int nEntityType /*= NULL*/)
{
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_EMAIL, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	QString strSQL;
	DbRecordSet tmpRecordSet;
	RetOut_recData.destroy();

	//DESKTOP_GRID
	if (nGridType == 0 || nGridType == 3)
	{
		if (nEntityType == ENTITY_BUS_DM_DOCUMENTS)
		{
			RetOut_recData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_COMM_GRID_DOCUMENT_VIEW));
			strSQL = " ";
			TableOrm.ReadFromParentIDs(Ret_pStatus, tmpRecordSet, "BDMD_ID", recIDsRecordset, "BDMD_ID", QString(), strSQL, DbSqlTableView::TVIEW_COMM_GRID_DOCUMENT_VIEW, true);
			//Let's get only one first "TO" contact for every email.
			GetDocumentsContactData(Ret_pStatus, tmpRecordSet);
		}
		else if (nEntityType == ENTITY_BUS_EMAILS)
		{
			RetOut_recData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_COMM_GRID_EMAIL_VIEW));
			strSQL = " ";
			TableOrm.ReadFromParentIDs(Ret_pStatus, tmpRecordSet, "BEM_ID", recIDsRecordset, "BEM_ID", QString(), strSQL, DbSqlTableView::TVIEW_COMM_GRID_EMAIL_VIEW, true);
			//Get contact data.
			GetEmailContactData(Ret_pStatus, tmpRecordSet);
		}
		else if (nEntityType == ENTITY_BUS_VOICECALLS)
		{
			RetOut_recData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_COMM_GRID_VOICECALL_VIEW));
			strSQL = " ";
			TableOrm.ReadFromParentIDs(Ret_pStatus, tmpRecordSet, "BVC_ID", recIDsRecordset, "BVC_ID", QString(), strSQL, DbSqlTableView::TVIEW_COMM_GRID_VOICECALL_VIEW, true);
		}
	}
	//CONTACT_GRID
	else if (nGridType == 1)
	{
		if (nEntityType == ENTITY_BUS_DM_DOCUMENTS)
		{
			RetOut_recData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CONTACT_COMM_GRID_DOCUMENT_VIEW));
			strSQL = "  ";
			TableOrm.ReadFromParentIDs(Ret_pStatus, tmpRecordSet, "BDMD_ID", recIDsRecordset, "BDMD_ID", QString(), strSQL, DbSqlTableView::TVIEW_CONTACT_COMM_GRID_DOCUMENT_VIEW, true);
			//Let's get only one first "TO" contact for every email.
			GetDocumentsContactData(Ret_pStatus, tmpRecordSet);
		}
		else if (nEntityType == ENTITY_BUS_EMAILS)
		{
			RetOut_recData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CONTACT_COMM_GRID_EMAIL_VIEW));
			strSQL = "  ";
			TableOrm.ReadFromParentIDs(Ret_pStatus, tmpRecordSet, "BEM_ID", recIDsRecordset, "BEM_ID", QString(), strSQL, DbSqlTableView::TVIEW_CONTACT_COMM_GRID_EMAIL_VIEW, true);
			//tmpRecordSet.Dump();
			//Get contact data.
			GetEmailContactData(Ret_pStatus, tmpRecordSet);
		}
		else if (nEntityType == ENTITY_BUS_VOICECALLS)
		{
			RetOut_recData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_COMM_GRID_VOICECALL_VIEW));
			strSQL = "  ";
			TableOrm.ReadFromParentIDs(Ret_pStatus, tmpRecordSet, "BVC_ID", recIDsRecordset, "BVC_ID", QString(), strSQL, DbSqlTableView::TVIEW_COMM_GRID_VOICECALL_VIEW, true);
		}
	}
	//PROJECT_GRID
	else if (nGridType == 2)
	{
		if (nEntityType == ENTITY_BUS_DM_DOCUMENTS)
		{
			RetOut_recData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_COMM_GRID_DOCUMENT_VIEW));
			strSQL = " ";
			TableOrm.ReadFromParentIDs(Ret_pStatus, tmpRecordSet, "BDMD_ID", recIDsRecordset, "BDMD_ID", QString(), strSQL, DbSqlTableView::TVIEW_COMM_GRID_DOCUMENT_VIEW, true);
			//Let's get only one first "TO" contact for every email.
			GetDocumentsContactData(Ret_pStatus, tmpRecordSet);
		}
		else if (nEntityType == ENTITY_BUS_EMAILS)
		{
			RetOut_recData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_COMM_GRID_EMAIL_VIEW));
			strSQL = " ";
			TableOrm.ReadFromParentIDs(Ret_pStatus, tmpRecordSet, "BEM_ID", recIDsRecordset, "BEM_ID", QString(), strSQL, DbSqlTableView::TVIEW_COMM_GRID_EMAIL_VIEW, true);
			//Get contact data.
			GetEmailContactData(Ret_pStatus, tmpRecordSet);
		}
		else if (nEntityType == ENTITY_BUS_VOICECALLS)
		{
			RetOut_recData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_COMM_GRID_VOICECALL_VIEW));
			strSQL = " ";
			TableOrm.ReadFromParentIDs(Ret_pStatus, tmpRecordSet, "BVC_ID", recIDsRecordset, "BVC_ID", QString(), strSQL, DbSqlTableView::TVIEW_COMM_GRID_VOICECALL_VIEW, true);
		}
	}

	CheckForDoubleEntities(RetOut_recData, tmpRecordSet);
}

//Private method for fetching mails for person.
void Service_BusCommunication::GetPersonEmails(Status &Ret_pStatus, DbRecordSet &RetOut_recEmails, int nID, DbRecordSet recFilter, bool bForceFilterByTaskType)
{
	//QTime timer;
	//timer.start();

	//Locals.
	bool bFilterByGrid		 = GetPersonFilterSettingBool(VIEW_SELECTION_GRID_ACTIVE, recFilter);
	bool bFilterByEntityType = GetPersonFilterSettingBool(FILTER_BY_ENTITY_TYPE_ACTIVE, recFilter);

	QDateTime datFrom = GetPersonFilterSettingDateTime(FROM_DATE, recFilter);
	QDateTime datTo	  = GetPersonFilterSettingDateTime(TO_DATE, recFilter);
	QString strPersonID = QVariant(nID).toString();
	//For DbSqlQuery objects full sql string.
	QString strSQL	 = DbSqlTableView::getSQLView(DbSqlTableView::TVIEW_COMM_GRID_EMAIL_VIEW);
	strSQL			+= " WHERE 1=1 ";
	
	//Access rights.
	g_AccessRight->SQLFilterRecords(BUS_EMAIL, strSQL);

	//If logged user is the same as selected one then ok, else look just the data that is not private.
	QString strLoggedPersonID = QVariant(g_UserSessionManager->GetPersonID()).toString();
	QString strNoTaskSql;
	//B.T. to fix: no more private
	//if (strLoggedPersonID==strPersonID)
		strNoTaskSql = " AND CENT_OWNER_ID = " + strPersonID + " AND ((BTKS_ID IS NULL) OR (BTKS_ID IS NOT NULL AND (BTKS_IS_TASK_ACTIVE = 0 OR BTKS_SYSTEM_STATUS >= 3)) ) ";
	//else
		//strNoTaskSql = " AND (CENT_OWNER_ID = " + strPersonID + " AND CENT_IS_PRIVATE = 0) AND ((BTKS_ID IS NULL) OR (BTKS_ID IS NOT NULL AND (BTKS_IS_TASK_ACTIVE = 0 OR BTKS_SYSTEM_STATUS >= 3)) ) ";

	//Filter by date.
	QString strNoTaskTimeSql = " ";
	if (GetPersonFilterSettingBool(FILTER_BY_DATE_ACTIVE, recFilter))
		strNoTaskTimeSql = " AND BEM_RECV_TIME > ? AND BEM_RECV_TIME < ? ";

	//Filter by CE type.
	if (GetPersonFilterSettingBool(FILTER_BY_CE_ENTITY_TYPE_ACTIVE, recFilter))
	{
		QString strCENT_CE_TYPE_IDs;
		DbRecordSet recCEFilter = GetPersonFilterSettingRecordSet(CE_ENTITY_TYPE_FILTER_EMAILS, recFilter);
		int nRowCount = recCEFilter.getRowCount();
		//_DUMP(recCEFilter);
		if (nRowCount > 0)
		{
			for (int i = 0; i < nRowCount; ++i)
			{
				int nChecked = recCEFilter.getDataRef(i, "CHECKED_ITEM_STATE").toInt();
				if (QVariant(nChecked).toBool())
					strCENT_CE_TYPE_IDs += recCEFilter.getDataRef(i, "CHECKED_ITEM_ID").toString() + ",";
			}

			//If all items are unchecked then look only for those whose CENT_CE_TYPE_ID is NULL.
			if (strCENT_CE_TYPE_IDs.isEmpty())
				strCENT_CE_TYPE_IDs = " AND CENT_CE_TYPE_ID IS NULL ";
			else
			{
				//Chop last ,.
				strCENT_CE_TYPE_IDs.chop(1);
				QString strTmp = strCENT_CE_TYPE_IDs;
				strCENT_CE_TYPE_IDs = " AND CENT_CE_TYPE_ID IN (";
				strCENT_CE_TYPE_IDs += strTmp + ") ";
			}
		}

		strSQL += strCENT_CE_TYPE_IDs;
	}

	//recFilter.Dump();

	//Get query object and its select statement.
	DbSqlQuery query(Ret_pStatus, GetDbManager());
	if(!Ret_pStatus.IsOK()) return;

	//Destroy returning recordset and define from view.
	RetOut_recEmails.destroy();
	RetOut_recEmails.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_COMM_GRID_EMAIL_VIEW));

	//Create temporary recordset for data fetching.
	DbRecordSet tmpRecordSet;
	tmpRecordSet.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_COMM_GRID_EMAIL_VIEW));

	//_DUMP(recFilter);

	//qDebug()<<"1 "<<timer.elapsed();
	//timer.start();

	//2014.03.19: remove BEM_BODY from select if not full text search:
	bool bRemoveBodyFromSelect = !GetPersonFilterSettingBool(FILTER_SEARCH_TEXT_ACTIVE, recFilter);
	//'' as BEM_BODY

	//Get unread emails.
	if (GetPersonFilterSettingBool(SHOW_EMAILS_UNREAD, recFilter) && bFilterByGrid)
	{
		QString strUnassignedWhere = strSQL + strNoTaskTimeSql + strNoTaskSql + " AND BEM_UNREAD_FLAG =1 AND BEM_OUTGOING = 0";
		tmpRecordSet.clear();
		if (bRemoveBodyFromSelect)
		{
			strUnassignedWhere.replace("BEB_BODY as BEM_BODY","'' as BEM_BODY");
		}

		//Check for date/time filter.
		if (GetPersonFilterSettingBool(FILTER_BY_DATE_ACTIVE, recFilter))
		{

			query.Prepare(Ret_pStatus, strUnassignedWhere);
			if (!Ret_pStatus.IsOK()) return;
			query.bindValue(0, datFrom);
			query.bindValue(1, datTo);
			query.ExecutePrepared(Ret_pStatus);
		}
		else
		{
			query.Execute(Ret_pStatus, strUnassignedWhere);
		}
		
		if (!Ret_pStatus.IsOK()) return;
		query.FetchData(tmpRecordSet);
		//tmpRecordSet.Dump();
		RetOut_recEmails.merge(tmpRecordSet);
	}

	//qDebug()<<"2 "<<timer.elapsed();
	//timer.start();


	//Get unassigned emails.
	if (GetPersonFilterSettingBool(SHOW_EMAILS_UNASSIGNED, recFilter) && bFilterByGrid)
	{
		QString strUnassignedWhere = strSQL + strNoTaskTimeSql + strNoTaskSql + " AND ( \
											  (SELECT COUNT(*) FROM BUS_NMRX_RELATION WHERE BNMR_TABLE_KEY_ID_1 = CENT_ID AND "+NMRX_CE_CONTACT_WHERE+") = 0 \
											  AND CENT_TASK_ID IS NULL AND (BEM_TEMPLATE_FLAG = 0 OR BEM_TEMPLATE_FLAG IS NULL) \
											  AND (SELECT COUNT(*) FROM BUS_NMRX_RELATION WHERE BNMR_TABLE_KEY_ID_1 = CENT_ID AND "+NMRX_CE_PROJECT_WHERE+") = 0 \
											  AND BEM_PROJECT_ID IS NULL) ";

		tmpRecordSet.clear();

		if (bRemoveBodyFromSelect)
		{
			strUnassignedWhere.replace("BEB_BODY as BEM_BODY","'' as BEM_BODY");
		}
		
		//Check for date/time filter.
		if (GetPersonFilterSettingBool(FILTER_BY_DATE_ACTIVE, recFilter))
		{
			query.Prepare(Ret_pStatus, strUnassignedWhere);
			if (!Ret_pStatus.IsOK()) return;
			query.bindValue(0, datFrom);
			query.bindValue(1, datTo);
			query.ExecutePrepared(Ret_pStatus);
		}
		else
		{
			query.Execute(Ret_pStatus, strUnassignedWhere);
		}
		if (!Ret_pStatus.IsOK()) return;
		query.FetchData(tmpRecordSet);

		//tmpRecordSet.Dump();

		//Check for double rows - if not double merge it.
		CheckForDoubleEntities(RetOut_recEmails, tmpRecordSet);
		//tmpRecordSet.Dump();
	}

	//qDebug()<<"3 "<<timer.elapsed();
	//timer.start();

	GetPersonEmailTasks(strSQL, query, Ret_pStatus, RetOut_recEmails, strPersonID, recFilter, bForceFilterByTaskType, bFilterByGrid, datFrom, datTo);
/*
	QString strTaskTypeFilter = GetTaskTypeFilterString(recFilter);
	
	//Get scheduled emails.
	if (GetPersonFilterSettingBool(SHOW_EMAILS_SCHEDULED, recFilter) && bFilterByGrid)
	{
		QString strScheduled = strSQL + strTaskTypeFilter + " AND (BTKS_OWNER = " + strPersonID + " OR BTKS_ORIGINATOR = " + strPersonID + ")" +
 								" AND BTKS_SYSTEM_TYPE = 3 AND BTKS_SYSTEM_STATUS < 3 \
								  AND BTKS_IS_TASK_ACTIVE = 1 \
								  AND BTKS_START IS NOT NULL AND BTKS_START > ?";

		tmpRecordSet.clear();
		
		query.Prepare(Ret_pStatus, strScheduled);
		if (!Ret_pStatus.IsOK()) return;
		query.bindValue(0, QDateTime::currentDateTime());
		query.ExecutePrepared(Ret_pStatus);
		
		if (!Ret_pStatus.IsOK()) return;
		query.FetchData(tmpRecordSet);
		
		//tmpRecordSet.Dump();

		//Filter by date if date filter active.
		if (GetPersonFilterSettingBool(FILTER_BY_DATE_ACTIVE, recFilter))
			TaskDateFilter(datFrom, datTo, tmpRecordSet);

		//Check for double rows - if not double merge it.
		CheckForDoubleEntities(RetOut_recEmails, tmpRecordSet);
		//tmpRecordSet.Dump();
	}

	//recFilter.Dump();

	//Get due emails.
	if (GetPersonFilterSettingBool(SHOW_EMAILS_DUE, recFilter) && bFilterByGrid)
	{
		QString strDue = strSQL + strTaskTypeFilter + " AND BTKS_SYSTEM_TYPE  = 3 AND (BTKS_OWNER = " + strPersonID + " OR BTKS_ORIGINATOR = " + strPersonID + ")" +
								" AND BTKS_IS_TASK_ACTIVE =1 AND BTKS_SYSTEM_STATUS < 3 AND \
								( \
								(BTKS_DUE >= ? AND BTKS_START < ?) OR \
								(BTKS_DUE IS NULL AND (BTKS_START IS NULL OR BTKS_START <= ?)) OR \
								(BTKS_START IS NULL AND BTKS_DUE > ?) \
								)";

		tmpRecordSet.clear();
		
		query.Prepare(Ret_pStatus, strDue);
		if (!Ret_pStatus.IsOK()) return;
		query.bindValue(0, QDateTime::currentDateTime());
		query.bindValue(1, QDateTime::currentDateTime());
		query.bindValue(2, QDateTime::currentDateTime());
		query.bindValue(3, QDateTime::currentDateTime());
		query.ExecutePrepared(Ret_pStatus);
		
		if (!Ret_pStatus.IsOK()) return;
		query.FetchData(tmpRecordSet);
		//tmpRecordSet.Dump();

		//Filter by date if date filter active.
		if (GetPersonFilterSettingBool(FILTER_BY_DATE_ACTIVE, recFilter))
			TaskDateFilter(datFrom, datTo, tmpRecordSet);
		
		//Check for double rows - if not double merge it.
		CheckForDoubleEntities(RetOut_recEmails, tmpRecordSet);
		//tmpRecordSet.Dump();
	}

	//Get overdue emails.
	if (GetPersonFilterSettingBool(SHOW_EMAILS_OVERDUE, recFilter) && bFilterByGrid)
	{
		QString strOverDue = strSQL + strTaskTypeFilter + " AND BTKS_SYSTEM_TYPE  = 3 AND (BTKS_OWNER = " + strPersonID + " OR BTKS_ORIGINATOR = " + strPersonID + ")" +
										" AND BTKS_SYSTEM_STATUS < 3 AND BTKS_IS_TASK_ACTIVE =1 \
										AND BTKS_DUE < ?";

		tmpRecordSet.clear();
			
		query.Prepare(Ret_pStatus, strOverDue);
		if (!Ret_pStatus.IsOK()) return;
		query.bindValue(0, QDateTime::currentDateTime());
		query.ExecutePrepared(Ret_pStatus);
		
		if (!Ret_pStatus.IsOK()) return;
		query.FetchData(tmpRecordSet);

		//tmpRecordSet.Dump();

		//Filter by date if date filter active.
		if (GetPersonFilterSettingBool(FILTER_BY_DATE_ACTIVE, recFilter))
			TaskDateFilter(datFrom, datTo, tmpRecordSet);

		//Check for double rows - if not double merge it.
		CheckForDoubleEntities(RetOut_recEmails, tmpRecordSet);
		//tmpRecordSet.Dump();
	}
*/

	//qDebug()<<"4 "<<timer.elapsed();
	//timer.start();


	//Get template emails.
	if (GetPersonFilterSettingBool(SHOW_EMAILS_TEMPLATES, recFilter) && bFilterByGrid)
	{
		QString strTemplates = strSQL + strNoTaskTimeSql + strNoTaskSql + " AND BEM_TEMPLATE_FLAG =1 ";

		tmpRecordSet.clear();
		//Check for date/time filter.
		if (GetPersonFilterSettingBool(FILTER_BY_DATE_ACTIVE, recFilter))
		{
			query.Prepare(Ret_pStatus, strTemplates);
			if (!Ret_pStatus.IsOK()) return;
			query.bindValue(0, datFrom);
			query.bindValue(1, datTo);
			query.ExecutePrepared(Ret_pStatus);
		}
		else
		{
			query.Execute(Ret_pStatus, strTemplates);
		}
		if (!Ret_pStatus.IsOK()) return;
		query.FetchData(tmpRecordSet);
		//tmpRecordSet.Dump();

		//Check for double rows - if not double merge it.
		CheckForDoubleEntities(RetOut_recEmails, tmpRecordSet);
		//tmpRecordSet.Dump();
	}
	

	//qDebug()<<"5 "<<timer.elapsed();
	//timer.start();

	//Get last N emails.
	if (GetPersonFilterSettingBool(SHOW_EMAILS_LAST, recFilter) && bFilterByGrid)
	{
		int nLastRecords = GetPersonFilterSettingInt(LAST_EMAILS_NUMBER, recFilter);

		QString strLastRecords = strSQL + strNoTaskTimeSql + strNoTaskSql + " ORDER BY BEM_DAT_LAST_MODIFIED DESC";
		GetDbManager()->GetFunction()->PrepareLimit(strLastRecords,0,nLastRecords);

		tmpRecordSet.clear();
		//Check for date/time filter.
		if (GetPersonFilterSettingBool(FILTER_BY_DATE_ACTIVE, recFilter))
		{
			query.Prepare(Ret_pStatus, strLastRecords);
			if (!Ret_pStatus.IsOK()) return;
			query.bindValue(0, datFrom);
			query.bindValue(1, datTo);
			query.ExecutePrepared(Ret_pStatus);
		}
		else
		{
			query.Execute(Ret_pStatus, strLastRecords);
		}
		if (!Ret_pStatus.IsOK()) return;
		query.FetchData(tmpRecordSet);
		//tmpRecordSet.Dump();

		//Check for double rows - if not double merge it.
		//tmpRecordSet.Dump();
		CheckForDoubleEntities(RetOut_recEmails, tmpRecordSet);
		//tmpRecordSet.Dump();
	}

	//qDebug()<<"6 "<<timer.elapsed();
	//timer.start();


	//If complex filter active and email filter active get all emails.
	if (!bFilterByGrid && bFilterByEntityType)
	{
		QString strLastRecords = strSQL + strNoTaskTimeSql + strNoTaskSql;

		tmpRecordSet.clear();
		//Check for date/time filter.
		if (GetPersonFilterSettingBool(FILTER_BY_DATE_ACTIVE, recFilter))
		{
			query.Prepare(Ret_pStatus, strLastRecords);
			if (!Ret_pStatus.IsOK()) return;
			query.bindValue(0, datFrom);
			query.bindValue(1, datTo);
			query.ExecutePrepared(Ret_pStatus);
		}
		else
		{
			query.Execute(Ret_pStatus, strLastRecords);
		}
		if (!Ret_pStatus.IsOK()) return;
		query.FetchData(RetOut_recEmails);
		//tmpRecordSet.Dump();

	}

	//qDebug()<<"7 "<<timer.elapsed();
	//timer.start();


	AddEmailAttachmentCountField(RetOut_recEmails);
	
	//If no email return.
	if (!RetOut_recEmails.getRowCount())
		return;

	//If grid not active filter by task.
	if ((!bFilterByGrid && bFilterByEntityType) || bForceFilterByTaskType)
		FilterByTaskType(recFilter, RetOut_recEmails, true);

	//_DUMP(RetOut_recEmails);

	if (GetPersonFilterSettingBool(FILTER_SEARCH_TEXT_ACTIVE, recFilter))
		FilterEmailsBySearchText(RetOut_recEmails, recFilter);
	
	//made by MP -> commented by MP on 21.5.2008 because Herzog had 20 mails with 1 MB body in a view.
	//int nColumnId = RetOut_recEmails.getColumnIdx("BEM_BODY");
	//RetOut_recEmails.setColValue(nColumnId, QString());

	//Return only 250 characters from email body for tooltip in grid.
	//BT:01.03.2011: use CE_TOOLTIP field
	//StripEmailBodyToNCharacters(RetOut_recEmails);

	//qDebug() << "before contact data" << timer.elapsed();


	//qDebug()<<"8 "<<timer.elapsed();
	//timer.start();


	//Get contact data.
	GetEmailContactData(Ret_pStatus, RetOut_recEmails);

	//qDebug()<<"9 "<<timer.elapsed();
	//timer.start();


	//Get email attachment count.
	CalculateEmailAttachmentCountField(Ret_pStatus, RetOut_recEmails);
	
	//int c=RetOut_recEmails.getRowCount();
	//int ii=0;
	//int nCx=RetOut_recEmails.getColumnIdx("BEM_BODY");

	//_DUMP(RetOut_recEmails);
	//qDebug() << "after contact data" << timer.elapsed();

	//qDebug()<<"10 "<<timer.elapsed();

}

//Private method for fetching mails for contact.
void Service_BusCommunication::GetContactEmails(Status &Ret_pStatus, DbRecordSet &RetOut_recEmails, int nID, DbRecordSet recFilter)
{
	//Locals.
	QString strWhere;
	QDateTime datFrom = GetPersonFilterSettingDateTime(FROM_DATE, recFilter);
	QDateTime datTo	  = GetPersonFilterSettingDateTime(TO_DATE, recFilter);
	RetOut_recEmails.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CONTACT_COMM_GRID_EMAIL_VIEW));
	strWhere = DbSqlTableView::getSQLView(DbSqlTableView::TVIEW_CONTACT_COMM_GRID_EMAIL_VIEW);
	strWhere += " WHERE BNMR_TABLE_KEY_ID_2 = " + QVariant(nID).toString();
	//strWhere += " WHERE CELC_CONTACT_ID = " + QVariant(nID).toString() + " AND ((NOT A.BPER_CONTACT_ID = " + QVariant(nID).toString() + ") OR A.BPER_CONTACT_ID IS NULL)"; commented on issue #1975.
	
	//Access rights.
	g_AccessRight->SQLFilterRecords(BUS_EMAIL, strWhere);

	//Filter by CE type.
	if (GetPersonFilterSettingBool(FILTER_BY_CE_ENTITY_TYPE_ACTIVE, recFilter))
	{
		QString strCENT_CE_TYPE_IDs;
		DbRecordSet recCEFilter = GetPersonFilterSettingRecordSet(CE_ENTITY_TYPE_FILTER_EMAILS, recFilter);
		int nRowCount = recCEFilter.getRowCount();
		if (nRowCount > 0)
		{
			for (int i = 0; i < nRowCount; ++i)
			{
				int nChecked = recCEFilter.getDataRef(i, "CHECKED_ITEM_STATE").toInt();
				if (QVariant(nChecked).toBool())
					strCENT_CE_TYPE_IDs += recCEFilter.getDataRef(i, "CHECKED_ITEM_ID").toString() + ",";
			}

			//If all items are unchecked then look only for those whose CENT_CE_TYPE_ID is NULL.
			if (strCENT_CE_TYPE_IDs.isEmpty())
				strCENT_CE_TYPE_IDs = " AND CENT_CE_TYPE_ID IS NULL ";
			else
			{
				//Chop last ,.
				strCENT_CE_TYPE_IDs.chop(1);
				QString strTmp = strCENT_CE_TYPE_IDs;
				strCENT_CE_TYPE_IDs = " AND CENT_CE_TYPE_ID IN (";
				strCENT_CE_TYPE_IDs += strTmp + ") ";
			}
		}

		strWhere += strCENT_CE_TYPE_IDs;
	}

	//Get query object and its select statement.
	DbSqlQuery query(Ret_pStatus, GetDbManager());
	if(!Ret_pStatus.IsOK()) return;
	
	if (GetPersonFilterSettingBool(FILTER_BY_DATE_ACTIVE, recFilter))
	{
		strWhere += " AND BEM_RECV_TIME > ? AND BEM_RECV_TIME < ? ";
		query.Prepare(Ret_pStatus, strWhere);
		if (!Ret_pStatus.IsOK()) return;
		query.bindValue(0, datFrom);
		query.bindValue(1, datTo);
		query.ExecutePrepared(Ret_pStatus);
	}
	else
		query.Execute(Ret_pStatus, strWhere);

	//Fetch data.
	if (!Ret_pStatus.IsOK()) return;
	query.FetchData(RetOut_recEmails);


	AddEmailAttachmentCountField(RetOut_recEmails);

	//If no email return.
	if (!RetOut_recEmails.getRowCount())
		return;
		
	if (GetPersonFilterSettingBool(FILTER_SEARCH_TEXT_ACTIVE, recFilter))
		FilterEmailsBySearchText(RetOut_recEmails, recFilter);

	//made by MP -> commented by MP on 21.5.2008 because Herzog had 20 mails with 1 MB body in a view.
	//int nColumnId = RetOut_recEmails.getColumnIdx("BEM_BODY");
	//RetOut_recEmails.setColValue(nColumnId, QString());

	//Delete duplicate rows - when you assign multiple projects to an entity entry.
	CheckForDoubleEntitiesInContacts(RetOut_recEmails);
	
	//Check task type filter.
	FilterByTaskType(recFilter, RetOut_recEmails, true);

	//Return only 250 characters from email body for tooltip in grid.
	//StripEmailBodyToNCharacters(RetOut_recEmails);
	
	//_DUMP(RetOut_recEmails);

	//Get contact data.
	GetEmailContactData(Ret_pStatus, RetOut_recEmails);
	
	//Get email attachment count.
	CalculateEmailAttachmentCountField(Ret_pStatus, RetOut_recEmails);

	//RetOut_recEmails.Dump();
}

void Service_BusCommunication::GetProjectEmails(Status &Ret_pStatus, DbRecordSet &RetOut_recEmails, int nProjectID, DbRecordSet recFilter)
{
	//Get logged person ID.
	QString strLoggedPersonID = QVariant(g_UserSessionManager->GetPersonID()).toString();
	//Locals.
	QString strWhere;
	QDateTime datFrom = GetPersonFilterSettingDateTime(FROM_DATE, recFilter);
	QDateTime datTo	  = GetPersonFilterSettingDateTime(TO_DATE, recFilter);
	RetOut_recEmails.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_COMM_GRID_EMAIL_VIEW));
	strWhere = DbSqlTableView::getSQLView(DbSqlTableView::TVIEW_COMM_GRID_EMAIL_VIEW);
	strWhere += " WHERE BEM_PROJECT_ID = " + QVariant(nProjectID).toString();

	//Access rights.
	g_AccessRight->SQLFilterRecords(BUS_EMAIL, strWhere);

	//B.T. to fix: no more private
	//strWhere += " AND ((CENT_OWNER_ID = " + strLoggedPersonID + ") OR (CENT_OWNER_ID <> " + strLoggedPersonID + " AND CENT_IS_PRIVATE = 0)) ";

	//Filter by CE type.
	if (GetPersonFilterSettingBool(FILTER_BY_CE_ENTITY_TYPE_ACTIVE, recFilter))
	{
		QString strCENT_CE_TYPE_IDs;
		DbRecordSet recCEFilter = GetPersonFilterSettingRecordSet(CE_ENTITY_TYPE_FILTER_EMAILS, recFilter);
		int nRowCount = recCEFilter.getRowCount();
		if (nRowCount > 0)
		{
			for (int i = 0; i < nRowCount; ++i)
			{
				int nChecked = recCEFilter.getDataRef(i, "CHECKED_ITEM_STATE").toInt();
				if (QVariant(nChecked).toBool())
					strCENT_CE_TYPE_IDs += recCEFilter.getDataRef(i, "CHECKED_ITEM_ID").toString() + ",";
			}

			//If all items are unchecked then look only for those whose CENT_CE_TYPE_ID is NULL.
			if (strCENT_CE_TYPE_IDs.isEmpty())
				strCENT_CE_TYPE_IDs = " AND CENT_CE_TYPE_ID IS NULL ";
			else
			{
				//Chop last ,.
				strCENT_CE_TYPE_IDs.chop(1);
				QString strTmp = strCENT_CE_TYPE_IDs;
				strCENT_CE_TYPE_IDs = " AND CENT_CE_TYPE_ID IN (";
				strCENT_CE_TYPE_IDs += strTmp + ") ";
			}
		}

		strWhere += strCENT_CE_TYPE_IDs;
	}

	//Get query object and its select statement.
	DbSqlQuery query(Ret_pStatus, GetDbManager());
	if(!Ret_pStatus.IsOK()) return;

	if (GetPersonFilterSettingBool(FILTER_BY_DATE_ACTIVE, recFilter))
	{
		strWhere += " AND BEM_RECV_TIME > ? AND BEM_RECV_TIME < ? ";
		query.Prepare(Ret_pStatus, strWhere);
		if (!Ret_pStatus.IsOK()) return;
		query.bindValue(0, datFrom);
		query.bindValue(1, datTo);
		query.ExecutePrepared(Ret_pStatus);
	}
	else
		query.Execute(Ret_pStatus, strWhere);

	//Fetch data.
	if (!Ret_pStatus.IsOK()) return;
	query.FetchData(RetOut_recEmails);

	AddEmailAttachmentCountField(RetOut_recEmails);
	
	//If no email return.
	if (!RetOut_recEmails.getRowCount())
		return;

	if (GetPersonFilterSettingBool(FILTER_SEARCH_TEXT_ACTIVE, recFilter))
		FilterEmailsBySearchText(RetOut_recEmails, recFilter);

	//made by MP -> commented by MP on 21.5.2008 because Herzog had 20 mails with 1 MB body in a view.
	//int nColumnId = RetOut_recEmails.getColumnIdx("BEM_BODY");
	//RetOut_recEmails.setColValue(nColumnId, QString());

	//Return only 250 characters from email body for tooltip in grid.
	//StripEmailBodyToNCharacters(RetOut_recEmails);

	//Check task type filter.
	FilterByTaskType(recFilter, RetOut_recEmails, true);

	//Get contact data.
	GetEmailContactData(Ret_pStatus, RetOut_recEmails);

	//Get email attachment count.
	CalculateEmailAttachmentCountField(Ret_pStatus, RetOut_recEmails);
}

//Private for getting contact data for email.
void Service_BusCommunication::GetEmailContactData(Status &Ret_pStatus, DbRecordSet &RetOut_recEmails)
{
	//QTime timer;
	//timer.start();

	int nRowCount = RetOut_recEmails.getRowCount();
	if (!nRowCount)
		return;
	
	//Contact data Table ORM.
	DbSimpleOrm ContactOrm(Ret_pStatus, BUS_CM_CONTACT, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	QString strContactWhere;
	strContactWhere = " AND BNMR_TABLE_KEY_ID_2 = BCNT_ID AND BNMR_SYSTEM_ROLE = (BEM_OUTGOING+1) AND BEM_COMM_ENTITY_ID = BNMR_TABLE_KEY_ID_1 ";
	strContactWhere+=" AND "+QString(NMRX_CE_CONTACT_WHERE);
	DbRecordSet tmpContacts;

	ContactOrm.ReadFromParentIDs(Ret_pStatus, tmpContacts, "BNMR_TABLE_KEY_ID_1", RetOut_recEmails, "CENT_ID", "", strContactWhere, DbSqlTableView::TVIEW_COMM_CONTACT_EMAIL_VIEW);
	if(!Ret_pStatus.IsOK()) return;

	//qDebug()<<"GET CONTACT DATA: "<<timer.elapsed();


	DataHelper::DataOperation(DataHelper::OP_EDIT_ONLY,&tmpContacts,&RetOut_recEmails,NULL,NULL,RetOut_recEmails.getColumnIdx("CENT_ID"),tmpContacts.getColumnIdx("BNMR_TABLE_KEY_ID_1"));
	
	//Create temporary recordset.
	DbRecordSet tmp;
	tmp.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_COMM_CONTACT_EMAIL_VIEW));
	

	/*
	//Loop through emails and add contact data.
	for (int i = 0; i < nRowCount; ++i)
	{
		tmp.clear();
		int nCENT_ID = RetOut_recEmails.getDataRef(i, "CENT_ID").toInt();
		//If outgoing.
		tmpContacts.clearSelection();
		tmpContacts.find("CELC_COMM_ENTITY_ID", nCENT_ID);
		tmp.merge(tmpContacts, true);
		tmpContacts.deleteSelectedRows();
		
		if (!tmp.getRowCount())
			continue;

		tmp.clearSelection();
		if (RetOut_recEmails.getDataRef(i, "BEM_OUTGOING").toBool())
			tmp.find("BEM_OUTGOING", 1);
		else
			tmp.find("BEM_OUTGOING", 0);

		tmp.deleteUnSelectedRows();
		
*/
		//tmp.sort("CELC_DAT_LAST_MODIFIED", 1 /*DESCENDIG*/);
/*		
		RetOut_recEmails.setData(i, "BCNT_ID",				tmp.getDataRef(0, "BCNT_ID"));
		RetOut_recEmails.setData(i, "BCNT_LASTNAME",		tmp.getDataRef(0, "BCNT_LASTNAME"));
		RetOut_recEmails.setData(i, "BCNT_FIRSTNAME",		tmp.getDataRef(0, "BCNT_FIRSTNAME"));
		RetOut_recEmails.setData(i, "BCNT_ORGANIZATIONNAME",tmp.getDataRef(0, "BCNT_ORGANIZATIONNAME"));
	}
*/	

	
}

//Private person for documents.
void Service_BusCommunication::GetPersonDocuments(Status &Ret_pStatus, DbRecordSet &RetOut_recDocuments, int nID, DbRecordSet recFilter, bool bForceFilterByTaskType)
{
	//Locals.
	bool bFilterByGrid		 = GetPersonFilterSettingBool(VIEW_SELECTION_GRID_ACTIVE, recFilter);
	bool bFilterByEntityType = GetPersonFilterSettingBool(FILTER_BY_ENTITY_TYPE_ACTIVE, recFilter);

	QDateTime datFrom = GetPersonFilterSettingDateTime(FROM_DATE, recFilter);
	QDateTime datTo	  = GetPersonFilterSettingDateTime(TO_DATE, recFilter);
	QString strPersonID = QVariant(nID).toString();
	//For DbSqlQuery objects full sql string.
	QString strSQL	 = DbSqlTableView::getSQLView(DbSqlTableView::TVIEW_COMM_GRID_DOCUMENT_VIEW);
	strSQL			+= " WHERE 1=1 ";
//	strSQL			+= " WHERE BDMD_COMM_ENTITY_ID = CENT_ID ";
	
	//Access rights.
	g_AccessRight->SQLFilterRecords(BUS_DM_DOCUMENTS, strSQL);
	
	//If logged user is the same as selected one then ok, else look just the data that is not private.
	QString strLoggedPersonID = QVariant(g_UserSessionManager->GetPersonID()).toString();
	QString strNoTaskSql;
	//B.T. to fix: no more private
	//if (strLoggedPersonID==strPersonID)
		strNoTaskSql = " AND CENT_OWNER_ID = " + strPersonID + " AND ((BTKS_ID IS NULL) OR (BTKS_ID IS NOT NULL AND (BTKS_IS_TASK_ACTIVE = 0 OR BTKS_SYSTEM_STATUS >= 3)) ) ";
	//else
	//	strNoTaskSql = " AND (CENT_OWNER_ID = " + strPersonID + " AND CENT_IS_PRIVATE = 0) AND ((BTKS_ID IS NULL) OR (BTKS_ID IS NOT NULL AND (BTKS_IS_TASK_ACTIVE = 0 OR BTKS_SYSTEM_STATUS >= 3)) ) ";

	//Filter by application type.
	if (GetPersonFilterSettingBool(FILTER_DOCUMENTS_BY_APPLICATION_ACTIVE, recFilter))
	{
		DbRecordSet tmpAppTypes = GetPersonFilterSettingRecordSet(APPLICATION_TYPES, recFilter);
		int ntmpAppTypesRowCount = tmpAppTypes.getRowCount();
		if (ntmpAppTypesRowCount)
		{
			strSQL += " AND BDMD_APPLICATION_ID IN (";
			for (int i = 0; i < ntmpAppTypesRowCount; ++i)
			{
				strSQL += tmpAppTypes.getDataRef(i, "BDMA_ID").toString();
				strSQL += ",";
			}
			strSQL.chop(1);
			strSQL += ") ";
		}
	}

	//Filter by date.
	QString strNoTaskTimeSql = " ";
	if (GetPersonFilterSettingBool(FILTER_BY_DATE_ACTIVE, recFilter))
		strNoTaskTimeSql = " AND ((BDMD_DAT_LAST_MODIFIED > ? AND BDMD_DAT_LAST_MODIFIED < ?) OR (BDMD_DAT_CREATED > ? AND BDMD_DAT_CREATED < ?)) ";

	//Filter by CE type.
	if (GetPersonFilterSettingBool(FILTER_BY_CE_ENTITY_TYPE_ACTIVE, recFilter))
	{
		QString strCENT_CE_TYPE_IDs;
		DbRecordSet recCEFilter = GetPersonFilterSettingRecordSet(CE_ENTITY_TYPE_FILTER_DOCUMENTS, recFilter);
		//_DUMP(recCEFilter);
		int nRowCount = recCEFilter.getRowCount();
		if (nRowCount > 0)
		{
			for (int i = 0; i < nRowCount; ++i)
			{
				int nChecked = recCEFilter.getDataRef(i, "CHECKED_ITEM_STATE").toInt();
				if (QVariant(nChecked).toBool())
					strCENT_CE_TYPE_IDs += recCEFilter.getDataRef(i, "CHECKED_ITEM_ID").toString() + ",";
			}

			//If all items are unchecked then look only for those whose CENT_CE_TYPE_ID is NULL.
			if (strCENT_CE_TYPE_IDs.isEmpty())
				strCENT_CE_TYPE_IDs = " AND CENT_CE_TYPE_ID IS NULL ";
			else
			{
				//Chop last ,.
				strCENT_CE_TYPE_IDs.chop(1);
				QString strTmp = strCENT_CE_TYPE_IDs;
				strCENT_CE_TYPE_IDs = " AND CENT_CE_TYPE_ID IN (";
				strCENT_CE_TYPE_IDs += strTmp + ") ";
			}
		}

		strSQL += strCENT_CE_TYPE_IDs;
	}
	
	//Get query object and its select statement.
	DbSqlQuery query(Ret_pStatus, GetDbManager());
	if(!Ret_pStatus.IsOK()) return;

	//Destroy returning recordset and define from view.
	RetOut_recDocuments.destroy();
	RetOut_recDocuments.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_COMM_GRID_DOCUMENT_VIEW));

	//Create temporary recordset for data fetching.
	DbRecordSet tmpRecordSet;
	tmpRecordSet.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_COMM_GRID_DOCUMENT_VIEW));

	/*
	//Get checked out documents.
	if (GetPersonFilterSettingBool(SHOW_CHECKED_OUT_DOCUMENTS_COMPLEX, recFilter) && GetPersonFilterSettingBool(SHOW_COMPLEX_FILTER_FIRST, recFilter)))
	{
		QString strUnassignedWhere = strSQL + "  AND BDMD_IS_CHECK_OUT =1 ";

		tmpRecordSet.clear();
		query.Execute(Ret_pStatus, strUnassignedWhere);
		if (!Ret_pStatus.IsOK()) return;
		query.FetchData(tmpRecordSet);

		RetOut_recDocuments.merge(tmpRecordSet);
	}
	*/

	//Get unassigned documents.
	if (GetPersonFilterSettingBool(DOCUMENTS_UNASSIGNED, recFilter) && bFilterByGrid)
	{
		QString strUnassignedWhere = strSQL + strNoTaskTimeSql + strNoTaskSql + "  AND ( \
											  (SELECT COUNT(*) FROM BUS_NMRX_RELATION WHERE BNMR_TABLE_KEY_ID_1 = CENT_ID AND "+NMRX_CE_CONTACT_WHERE+") = 0 \
											  AND CENT_TASK_ID IS NULL AND BDMD_TEMPLATE_FLAG = 0 \
											  AND (SELECT COUNT(*) FROM BUS_NMRX_RELATION WHERE BNMR_TABLE_KEY_ID_1 = CENT_ID AND "+NMRX_CE_PROJECT_WHERE+") = 0 ) ";

		tmpRecordSet.clear();
		//Check for date/time filter.
		if (GetPersonFilterSettingBool(FILTER_BY_DATE_ACTIVE, recFilter))
		{
			query.Prepare(Ret_pStatus, strUnassignedWhere);
			if (!Ret_pStatus.IsOK()) return;
			query.bindValue(0, datFrom);
			query.bindValue(1, datTo);
			query.bindValue(2, datFrom);
			query.bindValue(3, datTo);
			query.ExecutePrepared(Ret_pStatus);
		}
		else
		{
			query.Execute(Ret_pStatus, strUnassignedWhere);
		}
		if (!Ret_pStatus.IsOK()) return;
		query.FetchData(tmpRecordSet);

		//Check for double rows - if not double merge it.
		CheckForDoubleEntities(RetOut_recDocuments, tmpRecordSet);
	}

	QString strTaskTypeFilter = GetTaskTypeFilterString(recFilter);

	//Get scheduled documents.
	if (GetPersonFilterSettingBool(DOCUMENTS_SCHEDULED, recFilter) && bFilterByGrid)
	{
		QString strScheduled = strSQL + strTaskTypeFilter + " AND (BTKS_OWNER = " + strPersonID + " OR BTKS_ORIGINATOR = " + strPersonID + ")" + 
										" AND BTKS_SYSTEM_TYPE = 1 AND BTKS_IS_TASK_ACTIVE =1 AND BTKS_SYSTEM_STATUS < 3 \
										  AND BTKS_START IS NOT NULL AND BTKS_START > ?";

		tmpRecordSet.clear();

		query.Prepare(Ret_pStatus, strScheduled);
		if (!Ret_pStatus.IsOK()) return;
		query.bindValue(0, QDateTime::currentDateTime());
		query.ExecutePrepared(Ret_pStatus);
		
		if (!Ret_pStatus.IsOK()) return;
		query.FetchData(tmpRecordSet);

		//Filter by date if date filter active.
		if (GetPersonFilterSettingBool(FILTER_BY_DATE_ACTIVE, recFilter))
			TaskDateFilter(datFrom, datTo, tmpRecordSet);

		//Check for double rows - if not double merge it.
		CheckForDoubleEntities(RetOut_recDocuments, tmpRecordSet);
	}

	//Get due documents.
	if (GetPersonFilterSettingBool(DOCUMENTS_DUE, recFilter) && bFilterByGrid)
	{
		QString strDue = strSQL + strTaskTypeFilter + " AND BTKS_SYSTEM_TYPE  = 1 AND (BTKS_OWNER = " + strPersonID + " OR BTKS_ORIGINATOR = " + strPersonID + ")" +
								  " AND BTKS_IS_TASK_ACTIVE =1 AND BTKS_SYSTEM_STATUS < 3 AND \
								    ( \
									(BTKS_DUE >= ? AND BTKS_START < ?) OR \
									(BTKS_DUE IS NULL AND (BTKS_START IS NULL OR BTKS_START <= ?)) OR \
									(BTKS_START IS NULL AND BTKS_DUE > ?) \
									)";

		tmpRecordSet.clear();
			
		query.Prepare(Ret_pStatus, strDue);
		if (!Ret_pStatus.IsOK()) return;
		query.bindValue(0, QDateTime::currentDateTime());
		query.bindValue(1, QDateTime::currentDateTime());
		query.bindValue(2, QDateTime::currentDateTime());
		query.bindValue(3, QDateTime::currentDateTime());
		query.ExecutePrepared(Ret_pStatus);
		
		if (!Ret_pStatus.IsOK()) return;
		query.FetchData(tmpRecordSet);

		//Filter by date if date filter active.
		if (GetPersonFilterSettingBool(FILTER_BY_DATE_ACTIVE, recFilter))
			TaskDateFilter(datFrom, datTo, tmpRecordSet);

		//Check for double rows - if not double merge it.
		CheckForDoubleEntities(RetOut_recDocuments, tmpRecordSet);
	}

	//Get overdue documents.
	if (GetPersonFilterSettingBool(DOCUMENTS_OVERDUE, recFilter) && bFilterByGrid)
	{
		QString strOverDue = strSQL + strTaskTypeFilter + " AND BTKS_SYSTEM_TYPE  = 1 AND (BTKS_OWNER = " + strPersonID + " OR BTKS_ORIGINATOR = " + strPersonID + ")" +
									  " AND BTKS_IS_TASK_ACTIVE =1 AND BTKS_SYSTEM_STATUS < 3 \
										AND BTKS_DUE < ?";

		tmpRecordSet.clear();
			
		query.Prepare(Ret_pStatus, strOverDue);
		if (!Ret_pStatus.IsOK()) return;
		query.bindValue(0, QDateTime::currentDateTime());
		query.ExecutePrepared(Ret_pStatus);
		
		if (!Ret_pStatus.IsOK()) return;
		query.FetchData(tmpRecordSet);

		//Filter by date if date filter active.
		if (GetPersonFilterSettingBool(FILTER_BY_DATE_ACTIVE, recFilter))
			TaskDateFilter(datFrom, datTo, tmpRecordSet);

		//Check for double rows - if not double merge it.
		CheckForDoubleEntities(RetOut_recDocuments, tmpRecordSet);
	}

	//Get template documents.
	if (GetPersonFilterSettingBool(DOCUMENTS_TEMPLATES, recFilter) && bFilterByGrid)
	{
		QString strTemplates = strSQL + strNoTaskTimeSql + strNoTaskSql + " AND BDMD_TEMPLATE_FLAG =1 ";

		tmpRecordSet.clear();

		//Check for date/time filter.
		if (GetPersonFilterSettingBool(FILTER_BY_DATE_ACTIVE, recFilter))
		{
			query.Prepare(Ret_pStatus, strTemplates);
			if (!Ret_pStatus.IsOK()) return;
			query.bindValue(0, datFrom);
			query.bindValue(1, datTo);
			query.bindValue(2, datFrom);
			query.bindValue(3, datTo);
			query.ExecutePrepared(Ret_pStatus);
		}
		else
		{
			query.Execute(Ret_pStatus, strTemplates);
		}
		if (!Ret_pStatus.IsOK()) return;
		query.FetchData(tmpRecordSet);

		//Check for double rows - if not double merge it.
		CheckForDoubleEntities(RetOut_recDocuments, tmpRecordSet);
	}

	//Get last N documents.
	if (GetPersonFilterSettingBool(DOCUMENTS_LAST, recFilter) && bFilterByGrid)
	{
		int nLastRecords = GetPersonFilterSettingInt(LAST_DOCUMENTS_NUMBER, recFilter);
		
		//First get last 10 CENT_ID's.
		//This was before access rights implementation.
		/*QString strLastNCENT_ID = "SELECT CENT_ID							\
								   FROM CE_COMM_ENTITY \
								   INNER JOIN BUS_DM_DOCUMENTS ON BDMD_COMM_ENTITY_ID = CENT_ID \
								   ORDER BY BDMD_DAT_LAST_MODIFIED DESC ";*/

		//This is after access rights implementation.
		QString strLastNCENT_ID	 = DbSqlTableView::getSQLView(DbSqlTableView::TVIEW_COMM_GRID_DOCUMENT_VIEW);
		strLastNCENT_ID += " WHERE 1=1 " + strNoTaskTimeSql + strNoTaskSql + "ORDER BY BDMD_DAT_LAST_MODIFIED DESC ";
	   
		//Get CENT_ID's.
		tmpRecordSet.clear();
		GetDbManager()->GetFunction()->PrepareLimit(strLastNCENT_ID,0,nLastRecords);

		//Access rights.
		g_AccessRight->SQLFilterRecords(BUS_DM_DOCUMENTS, strLastNCENT_ID);

		query.Execute(Ret_pStatus, strLastNCENT_ID);
		if (!Ret_pStatus.IsOK()) return;
		query.FetchData(tmpRecordSet);

		//Now include those CENT_ID's in sql and get data.
		QString strLastRecords = strSQL + strNoTaskTimeSql + strNoTaskSql + " AND CENT_ID IN (";
		int nRowCount = tmpRecordSet.getRowCount();

		//Check are there any documents at all.
		if (nRowCount)
		{
			for (int i = 0; i < nRowCount; i++)
			{
				strLastRecords += tmpRecordSet.getDataRef(i, "CENT_ID").toString();
				strLastRecords += ",";
			}
			strLastRecords.chop(1);
			strLastRecords += ") ";

			strLastRecords += " ORDER BY BDMD_DAT_LAST_MODIFIED DESC";

			tmpRecordSet.clear();
			//Check for date/time filter.
			if (GetPersonFilterSettingBool(FILTER_BY_DATE_ACTIVE, recFilter))
			{
				query.Prepare(Ret_pStatus, strLastRecords);
				if (!Ret_pStatus.IsOK()) return;
				query.bindValue(0, datFrom);
				query.bindValue(1, datTo);
				query.bindValue(2, datFrom);
				query.bindValue(3, datTo);
				query.ExecutePrepared(Ret_pStatus);
			}
			else
			{
				query.Execute(Ret_pStatus, strLastRecords);
			}
			if (!Ret_pStatus.IsOK()) return;
			query.FetchData(tmpRecordSet);
			//tmpRecordSet.Dump();

			//Check for double rows - if not double merge it.
			CheckForDoubleEntities(RetOut_recDocuments, tmpRecordSet);
		}
	}

	//If complex filter not active and document filter active get all documents.
	if ((!bFilterByGrid && bFilterByEntityType) || (!bFilterByGrid && GetPersonFilterSettingBool(FILTER_DOCUMENTS_BY_APPLICATION_ACTIVE, recFilter)))
	{
		int nLastRecords = GetPersonFilterSettingInt(LAST_DOCUMENTS_NUMBER, recFilter);

		QString strLastRecords = strSQL + strNoTaskTimeSql + strNoTaskSql ;

		tmpRecordSet.clear();
		if (GetPersonFilterSettingBool(FILTER_BY_DATE_ACTIVE, recFilter))
		{
			query.Prepare(Ret_pStatus, strLastRecords);
			if (!Ret_pStatus.IsOK()) return;
			query.bindValue(0, datFrom);
			query.bindValue(1, datTo);
			query.bindValue(2, datFrom);
			query.bindValue(3, datTo);
			query.ExecutePrepared(Ret_pStatus);
		}
		else
		{
			query.Execute(Ret_pStatus, strLastRecords);
		}
		if (!Ret_pStatus.IsOK()) return;
		query.FetchData(tmpRecordSet);

		//Check for double rows - if not double merge it.
		CheckForDoubleEntities(RetOut_recDocuments, tmpRecordSet);
	}

	//If no email return.
	if (!RetOut_recDocuments.getRowCount())
		return;

	//If grid not active filter by task.
	if ((!bFilterByGrid && bFilterByEntityType) || bForceFilterByTaskType)
		FilterByTaskType(recFilter, RetOut_recDocuments, true);

	//Filter by search text.
	if (GetPersonFilterSettingBool(FILTER_SEARCH_TEXT_ACTIVE, recFilter))
		FilterDocumentsBySearchText(RetOut_recDocuments, recFilter);

	//Strip note description to 3072 chars.
	StripDocumentBodyToNCharacters(RetOut_recDocuments);

	//Let's get only one first "TO" contact for every email.
	GetDocumentsContactData(Ret_pStatus, RetOut_recDocuments);

	//_DUMP(RetOut_recDocuments);
}

//Private for contact documents.
void Service_BusCommunication::GetContactDocuments(Status &Ret_pStatus, DbRecordSet &RetOut_recDocuments, int nID, DbRecordSet recFilter)
{
	//Locals.
	QString strWhere;
	QDateTime datFrom = GetPersonFilterSettingDateTime(FROM_DATE, recFilter);
	QDateTime datTo	  = GetPersonFilterSettingDateTime(TO_DATE, recFilter);
	RetOut_recDocuments.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CONTACT_COMM_GRID_DOCUMENT_VIEW));
	strWhere = DbSqlTableView::getSQLView(DbSqlTableView::TVIEW_CONTACT_COMM_GRID_DOCUMENT_VIEW);
	strWhere += " WHERE K2.BNMR_TABLE_KEY_ID_2 = " + QVariant(nID).toString();
	//strWhere += " AND ((NOT A.BPER_CONTACT_ID = " + QVariant(nID).toString() + ") OR A.BPER_CONTACT_ID IS NULL)"; commented on issue #1975.

	//Access rights.
	g_AccessRight->SQLFilterRecords(BUS_DM_DOCUMENTS, strWhere);

	//Filter by CE type.
	if (GetPersonFilterSettingBool(FILTER_BY_CE_ENTITY_TYPE_ACTIVE, recFilter))
	{
		QString strCENT_CE_TYPE_IDs;
		DbRecordSet recCEFilter = GetPersonFilterSettingRecordSet(CE_ENTITY_TYPE_FILTER_DOCUMENTS, recFilter);
		int nRowCount = recCEFilter.getRowCount();
		if (nRowCount > 0)
		{
			for (int i = 0; i < nRowCount; ++i)
			{
				int nChecked = recCEFilter.getDataRef(i, "CHECKED_ITEM_STATE").toInt();
				if (QVariant(nChecked).toBool())
					strCENT_CE_TYPE_IDs += recCEFilter.getDataRef(i, "CHECKED_ITEM_ID").toString() + ",";
			}

			//If all items are unchecked then look only for those whose CENT_CE_TYPE_ID is NULL.
			if (strCENT_CE_TYPE_IDs.isEmpty())
				strCENT_CE_TYPE_IDs = " AND CENT_CE_TYPE_ID IS NULL ";
			else
			{
				//Chop last ,.
				strCENT_CE_TYPE_IDs.chop(1);
				QString strTmp = strCENT_CE_TYPE_IDs;
				strCENT_CE_TYPE_IDs = " AND CENT_CE_TYPE_ID IN (";
				strCENT_CE_TYPE_IDs += strTmp + ") ";
			}
		}

		strWhere += strCENT_CE_TYPE_IDs;
	}

	//Filter by application type.
	if (GetPersonFilterSettingBool(FILTER_DOCUMENTS_BY_APPLICATION_ACTIVE, recFilter))
	{
		DbRecordSet tmpAppTypes = GetPersonFilterSettingRecordSet(APPLICATION_TYPES, recFilter);

		int ntmpAppTypesRowCount = tmpAppTypes.getRowCount();
		if (ntmpAppTypesRowCount)
		{
			strWhere += " AND BDMD_APPLICATION_ID IN (";
			for (int i = 0; i < ntmpAppTypesRowCount; ++i)
			{
				strWhere += tmpAppTypes.getDataRef(i, "BDMA_ID").toString();
				strWhere += ",";
			}
			strWhere.chop(1);
			strWhere += ") ";
		}
	}

	//Get query object and its select statement.
	DbSqlQuery query(Ret_pStatus, GetDbManager());
	if(!Ret_pStatus.IsOK()) return;

	if (GetPersonFilterSettingBool(FILTER_BY_DATE_ACTIVE, recFilter))
	{
		strWhere += " AND ((BDMD_DAT_LAST_MODIFIED > ? AND BDMD_DAT_LAST_MODIFIED < ?) OR (BDMD_DAT_CREATED > ? AND BDMD_DAT_CREATED < ?))";
		query.Prepare(Ret_pStatus, strWhere);
		if (!Ret_pStatus.IsOK()) return;
		query.bindValue(0, datFrom);
		query.bindValue(1, datTo);
		query.bindValue(2, datFrom);
		query.bindValue(3, datTo);
		query.ExecutePrepared(Ret_pStatus);
	}
	else
		query.Execute(Ret_pStatus, strWhere);

	//Fetch data.
	if (!Ret_pStatus.IsOK()) return;
	query.FetchData(RetOut_recDocuments);

	//If no email return.
	if (!RetOut_recDocuments.getRowCount())
		return;

	//Filter by search text.
	if (GetPersonFilterSettingBool(FILTER_SEARCH_TEXT_ACTIVE, recFilter))
		FilterDocumentsBySearchText(RetOut_recDocuments, recFilter);
	
	//Delete duplicate rows - when you assign multiple projects to an entity entry.
	CheckForDoubleEntitiesInContacts(RetOut_recDocuments);

	//Check task type filter.
	FilterByTaskType(recFilter, RetOut_recDocuments, true);
	
	//Strip note description to 3072 chars.
	StripDocumentBodyToNCharacters(RetOut_recDocuments);

	//Get contact data.
	GetDocumentsContactData(Ret_pStatus, RetOut_recDocuments);
}

//Private for project documents.
void Service_BusCommunication::GetProjectDocuments(Status &Ret_pStatus, DbRecordSet &RetOut_recDocuments, int nID, DbRecordSet recFilter)
{
	//Locals.
	QString strLoggedPersonID = QVariant(g_UserSessionManager->GetPersonID()).toString();
	QString strWhere;
	QDateTime datFrom = GetPersonFilterSettingDateTime(FROM_DATE, recFilter);
	QDateTime datTo	  = GetPersonFilterSettingDateTime(TO_DATE, recFilter);
	RetOut_recDocuments.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_COMM_GRID_DOCUMENT_VIEW));
	strWhere = DbSqlTableView::getSQLView(DbSqlTableView::TVIEW_COMM_GRID_DOCUMENT_VIEW);
	//B.T. to fix: no more private
	strWhere += " WHERE BNMR_TABLE_KEY_ID_2 = " + QVariant(nID).toString();
				//" AND ((CENT_OWNER_ID = " + strLoggedPersonID + ") OR (CENT_OWNER_ID <> " + strLoggedPersonID + " AND CENT_IS_PRIVATE = 0))";
	
	//Access rights.
	g_AccessRight->SQLFilterRecords(BUS_DM_DOCUMENTS, strWhere);

	//Filter by CE type.
	if (GetPersonFilterSettingBool(FILTER_BY_CE_ENTITY_TYPE_ACTIVE, recFilter))
	{
		QString strCENT_CE_TYPE_IDs;
		DbRecordSet recCEFilter = GetPersonFilterSettingRecordSet(CE_ENTITY_TYPE_FILTER_DOCUMENTS, recFilter);
		int nRowCount = recCEFilter.getRowCount();
		if (nRowCount > 0)
		{
			for (int i = 0; i < nRowCount; ++i)
			{
				int nChecked = recCEFilter.getDataRef(i, "CHECKED_ITEM_STATE").toInt();
				if (QVariant(nChecked).toBool())
					strCENT_CE_TYPE_IDs += recCEFilter.getDataRef(i, "CHECKED_ITEM_ID").toString() + ",";
			}

			//If all items are unchecked then look only for those whose CENT_CE_TYPE_ID is NULL.
			if (strCENT_CE_TYPE_IDs.isEmpty())
				strCENT_CE_TYPE_IDs = " AND CENT_CE_TYPE_ID IS NULL ";
			else
			{
				//Chop last ,.
				strCENT_CE_TYPE_IDs.chop(1);
				QString strTmp = strCENT_CE_TYPE_IDs;
				strCENT_CE_TYPE_IDs = " AND CENT_CE_TYPE_ID IN (";
				strCENT_CE_TYPE_IDs += strTmp + ") ";
			}
		}

		strWhere += strCENT_CE_TYPE_IDs;
	}

	//Filter by application type.
	if (GetPersonFilterSettingBool(FILTER_DOCUMENTS_BY_APPLICATION_ACTIVE, recFilter))
	{
		DbRecordSet tmpAppTypes = GetPersonFilterSettingRecordSet(APPLICATION_TYPES, recFilter);

		int ntmpAppTypesRowCount = tmpAppTypes.getRowCount();
		if (ntmpAppTypesRowCount)
		{
			strWhere += " AND BDMD_APPLICATION_ID IN (";
			for (int i = 0; i < ntmpAppTypesRowCount; ++i)
			{
				strWhere += tmpAppTypes.getDataRef(i, "BDMA_ID").toString();
				strWhere += ",";
			}
			strWhere.chop(1);
			strWhere += ")";
		}
	}
	
	//Get query object and its select statement.
	DbSqlQuery query(Ret_pStatus, GetDbManager());
	if(!Ret_pStatus.IsOK()) return;

	if (GetPersonFilterSettingBool(FILTER_BY_DATE_ACTIVE, recFilter))
	{
		strWhere += " AND ((BDMD_DAT_LAST_MODIFIED > ? AND BDMD_DAT_LAST_MODIFIED < ?) OR (BDMD_DAT_CREATED > ? AND BDMD_DAT_CREATED < ?))";
		query.Prepare(Ret_pStatus, strWhere);
		if (!Ret_pStatus.IsOK()) return;
		query.bindValue(0, datFrom);
		query.bindValue(1, datTo);
		query.bindValue(2, datFrom);
		query.bindValue(3, datTo);
		query.ExecutePrepared(Ret_pStatus);
	}
	else
		query.Execute(Ret_pStatus, strWhere);

	//Fetch data.
	if (!Ret_pStatus.IsOK()) return;
	query.FetchData(RetOut_recDocuments);

	//If no email return.
	if (!RetOut_recDocuments.getRowCount())
		return;

	//Filter by search text.
	if (GetPersonFilterSettingBool(FILTER_SEARCH_TEXT_ACTIVE, recFilter))
		FilterDocumentsBySearchText(RetOut_recDocuments, recFilter);
	
	//Check task type filter.
	FilterByTaskType(recFilter, RetOut_recDocuments, true);

	//Strip note description to 3072 chars.
	StripDocumentBodyToNCharacters(RetOut_recDocuments);

	//Get contact data.
	GetDocumentsContactData(Ret_pStatus, RetOut_recDocuments);
	//RetOut_recEmails.Dump();
}

void Service_BusCommunication::GetDocumentsContactData(Status &Ret_pStatus, DbRecordSet &RetOut_recDocuments)
{
	int nRowCount = RetOut_recDocuments.getRowCount();
	if (!nRowCount)
		return;

	//Contact data Table ORM.
	DbSimpleOrm ContactOrm(Ret_pStatus, BUS_CM_CONTACT, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	QString strContactWhere;
	strContactWhere = " AND BNMR_TABLE_KEY_ID_2 = BCNT_ID AND BDMD_COMM_ENTITY_ID = BNMR_TABLE_KEY_ID_1 ";
	strContactWhere+=" AND "+QString(NMRX_CE_CONTACT_WHERE);
	DbRecordSet tmpContacts;

	ContactOrm.ReadFromParentIDs(Ret_pStatus, tmpContacts, "BNMR_TABLE_KEY_ID_1", RetOut_recDocuments, "CENT_ID", "", strContactWhere, DbSqlTableView::TVIEW_COMM_CONTACT_DOCUMENT_VIEW);
	if(!Ret_pStatus.IsOK()) return;

	//Create temporary recordset.
	DbRecordSet tmp;
	tmp.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_COMM_CONTACT_DOCUMENT_VIEW));

	//Loop through emails and add contact data.
	for (int i = 0; i < nRowCount; ++i)
	{
		tmp.clear();
		int nCENT_ID = RetOut_recDocuments.getDataRef(i, "CENT_ID").toInt();
		//If outgoing.
		tmpContacts.clearSelection();
		tmpContacts.find("BNMR_TABLE_KEY_ID_1", nCENT_ID);
		tmp.merge(tmpContacts, true);
		tmpContacts.deleteSelectedRows();

		if (!tmp.getRowCount())
			continue;

		tmp.sort("BNMR_DAT_LAST_MODIFIED", 1 /*DESCENDIG*/);

		RetOut_recDocuments.setData(i, "BCNT_ID",				tmp.getDataRef(0, "BCNT_ID"));
		RetOut_recDocuments.setData(i, "BCNT_LASTNAME",			tmp.getDataRef(0, "BCNT_LASTNAME"));
		RetOut_recDocuments.setData(i, "BCNT_FIRSTNAME",		tmp.getDataRef(0, "BCNT_FIRSTNAME"));
		RetOut_recDocuments.setData(i, "BCNT_ORGANIZATIONNAME",	tmp.getDataRef(0, "BCNT_ORGANIZATIONNAME"));
	}

/*
	//Contact data Table ORM.
	DbSimpleOrm ContactOrm(Ret_pStatus, BUS_CM_CONTACT, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	//Let's get only one first "TO" contact for every email.
	int nRowCount = RetOut_recDocuments.getRowCount();
	for (int i = 0; i < nRowCount; ++i)
	{
		//Contact filling.
		if (!RetOut_recDocuments.getDataRef(i, "CENT_ID").isNull())
		{
			QString strCENT_ID = RetOut_recDocuments.getDataRef(i, "CENT_ID").toString();
			QString strContactWhere = " WHERE CELC_CONTACT_ID = BCNT_ID AND CELC_COMM_ENTITY_ID = " + strCENT_ID;
			strContactWhere += " ORDER BY CELC_DAT_LAST_MODIFIED DESC";
			DbRecordSet tmp;
			ContactOrm.Read(Ret_pStatus, tmp, strContactWhere, DbSqlTableView::TVIEW_COMM_CONTACT_EMAIL_VIEW);
			if (tmp.getRowCount()>0)
			{
				RetOut_recDocuments.setData(i, "BCNT_ID",				tmp.getDataRef(0, "BCNT_ID"));
				RetOut_recDocuments.setData(i, "BCNT_LASTNAME",			tmp.getDataRef(0, "BCNT_LASTNAME"));
				RetOut_recDocuments.setData(i, "BCNT_FIRSTNAME",		tmp.getDataRef(0, "BCNT_FIRSTNAME"));
				RetOut_recDocuments.setData(i, "BCNT_ORGANIZATIONNAME", tmp.getDataRef(0, "BCNT_ORGANIZATIONNAME"));
			}
		}
	}
*/
}

//Private for person voice calls.
void Service_BusCommunication::GetPersonVoiceCall(Status &Ret_pStatus, DbRecordSet &RetOut_recVoiceCalls, int nID, DbRecordSet recFilter, bool bForceFilterByTaskType)
{
	//Locals.
	bool bFilterByGrid		 = GetPersonFilterSettingBool(VIEW_SELECTION_GRID_ACTIVE, recFilter);
	bool bFilterByEntityType = GetPersonFilterSettingBool(FILTER_BY_ENTITY_TYPE_ACTIVE, recFilter);

	QDateTime datFrom = GetPersonFilterSettingDateTime(FROM_DATE, recFilter);
	QDateTime datTo	  = GetPersonFilterSettingDateTime(TO_DATE, recFilter);
	QString strPersonID = QVariant(nID).toString();
	//For DbSqlQuery objects full sql string.
	QString strSQL	 = DbSqlTableView::getSQLView(DbSqlTableView::TVIEW_COMM_GRID_VOICECALL_VIEW);
	strSQL			+= " WHERE 1=1 "; 
	
	//Access rights.
	g_AccessRight->SQLFilterRecords(BUS_VOICECALLS, strSQL);

	//If logged user is the same as selected one then ok, else look just the data that is not private.
	QString strLoggedPersonID = QVariant(g_UserSessionManager->GetPersonID()).toString();
	QString strNoTaskSql;
	//B.T. to fix: no more private
	//if (strLoggedPersonID==strPersonID)
		strNoTaskSql = " AND CENT_OWNER_ID = " + strPersonID + " AND ((BTKS_ID IS NULL) OR (BTKS_ID IS NOT NULL AND (BTKS_IS_TASK_ACTIVE = 0 OR BTKS_SYSTEM_STATUS >= 3)) ) ";
	//else
	//	strNoTaskSql = " AND (CENT_OWNER_ID = " + strPersonID + " AND CENT_IS_PRIVATE = 0) AND ((BTKS_ID IS NULL) OR (BTKS_ID IS NOT NULL AND (BTKS_IS_TASK_ACTIVE = 0 OR BTKS_SYSTEM_STATUS >= 3)) ) ";

	//Filter by date.
	QString strNoTaskTimeSql = " ";
	if (GetPersonFilterSettingBool(FILTER_BY_DATE_ACTIVE, recFilter))
		strNoTaskTimeSql = " AND (BVC_START >= ? AND BVC_START < ?) ";
	
	//Filter by CE type.
	if (GetPersonFilterSettingBool(FILTER_BY_CE_ENTITY_TYPE_ACTIVE, recFilter))
	{
		QString strCENT_CE_TYPE_IDs;
		DbRecordSet recCEFilter = GetPersonFilterSettingRecordSet(CE_ENTITY_TYPE_FILTER_VOICE_CALLS, recFilter);
		int nRowCount = recCEFilter.getRowCount();
		//_DUMP(recCEFilter);
		if (nRowCount > 0)
		{
			for (int i = 0; i < nRowCount; ++i)
			{
				int nChecked = recCEFilter.getDataRef(i, "CHECKED_ITEM_STATE").toInt();
				if (QVariant(nChecked).toBool())
					strCENT_CE_TYPE_IDs += recCEFilter.getDataRef(i, "CHECKED_ITEM_ID").toString() + ",";
			}

			//If all items are unchecked then look only for those whose CENT_CE_TYPE_ID is NULL.
			if (strCENT_CE_TYPE_IDs.isEmpty())
				strCENT_CE_TYPE_IDs = " AND CENT_CE_TYPE_ID IS NULL ";
			else
			{
				//Chop last ,.
				strCENT_CE_TYPE_IDs.chop(1);
				QString strTmp = strCENT_CE_TYPE_IDs;
				strCENT_CE_TYPE_IDs = " AND CENT_CE_TYPE_ID IN (";
				strCENT_CE_TYPE_IDs += strTmp + ") ";
			}
		}

		strSQL += strCENT_CE_TYPE_IDs;
	}

	//Get query object and its select statement.
	DbSqlQuery query(Ret_pStatus, GetDbManager());
	if(!Ret_pStatus.IsOK()) return;

	//Destroy returning recordset and define from view.
	RetOut_recVoiceCalls.destroy();
	RetOut_recVoiceCalls.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_COMM_GRID_VOICECALL_VIEW));
	
	//Create temporary recordset for data fetching.
	DbRecordSet tmpRecordSet;
	tmpRecordSet.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_COMM_GRID_VOICECALL_VIEW));

	//Get unassigned voice calls.
	if (GetPersonFilterSettingBool(SHOW_VOICE_CALLS_UNASSIGNED, recFilter) && bFilterByGrid)
	{
		QString strUnassignedWhere = strSQL + strNoTaskTimeSql + strNoTaskSql + 
									" AND (BVC_CONTACT_ID IS NULL AND CENT_TASK_ID IS NULL AND BVC_PROJECT_ID IS NULL) ";

		tmpRecordSet.clear();
		//Check for date/time filter.
		if (GetPersonFilterSettingBool(FILTER_BY_DATE_ACTIVE, recFilter))
		{
			query.Prepare(Ret_pStatus, strUnassignedWhere);
			if (!Ret_pStatus.IsOK()) return;
			query.bindValue(0, datFrom);
			query.bindValue(1, datTo);
			query.ExecutePrepared(Ret_pStatus);
		}
		else
		{
			query.Execute(Ret_pStatus, strUnassignedWhere);
		}
		if (!Ret_pStatus.IsOK()) return;
		query.FetchData(tmpRecordSet);

		RetOut_recVoiceCalls.merge(tmpRecordSet);
	}

	//Get missed voice calls.
	if (GetPersonFilterSettingBool(SHOW_VOICE_CALLS_MISSED, recFilter) && bFilterByGrid)
	{
		QDateTime datTodayStart(QDate::currentDate(), QTime(0,0,0));
		QDateTime datTodayEnd(QDate::currentDate(), QTime(23,59,59));
		//You get only todays missed calls - if no date time filter used.
		QString strTimeForNoDateFilter = " AND BVC_START > ? AND BVC_START < ? ";
		QString strMissed = strSQL + strNoTaskTimeSql + strNoTaskSql + " AND BVC_CALL_STATUS = 9 ";
		
		tmpRecordSet.clear();
		//Check for date/time filter.
		if (GetPersonFilterSettingBool(FILTER_BY_DATE_ACTIVE, recFilter))
		{
			query.Prepare(Ret_pStatus, strMissed);
			if (!Ret_pStatus.IsOK()) return;
			query.bindValue(0, datFrom);
			query.bindValue(1, datTo);
			query.ExecutePrepared(Ret_pStatus);
		}
		else
		{
			strMissed += strTimeForNoDateFilter;
			query.Prepare(Ret_pStatus, strMissed);
			if (!Ret_pStatus.IsOK()) return;
			//Show only today's missed voice calls.
			query.bindValue(0, datTodayStart);
			query.bindValue(1, datTodayEnd);
			query.ExecutePrepared(Ret_pStatus);
		}
		if (!Ret_pStatus.IsOK()) return;
		query.FetchData(tmpRecordSet);

		//Check for double rows - if not double merge it.
		CheckForDoubleEntities(RetOut_recVoiceCalls, tmpRecordSet);
	}

	QString strTaskTypeFilter = GetTaskTypeFilterString(recFilter);

	//Get scheduled voice calls.
	if (GetPersonFilterSettingBool(SHOW_VOICE_CALLS_SCHEDULED, recFilter) && bFilterByGrid)
	{
		QString strScheduled = strSQL + strTaskTypeFilter + " AND (BTKS_OWNER = " + strPersonID + " OR BTKS_ORIGINATOR = " + strPersonID + ")" +
									    " AND BTKS_SYSTEM_TYPE = 2 AND BTKS_SYSTEM_STATUS < 3 AND BTKS_IS_TASK_ACTIVE =1 \
										  AND BTKS_START IS NOT NULL AND BTKS_START > ?";
		
		tmpRecordSet.clear();
			
		query.Prepare(Ret_pStatus, strScheduled);
		if (!Ret_pStatus.IsOK()) return;
		query.bindValue(0, QDateTime::currentDateTime());
		query.ExecutePrepared(Ret_pStatus);
		
		if (!Ret_pStatus.IsOK()) return;
		query.FetchData(tmpRecordSet);

		//Filter by date if date filter active.
		if (GetPersonFilterSettingBool(FILTER_BY_DATE_ACTIVE, recFilter))
			TaskDateFilter(datFrom, datTo, tmpRecordSet);

		//Check for double rows - if not double merge it.
		CheckForDoubleEntities(RetOut_recVoiceCalls, tmpRecordSet);
	}

	//Get due voice calls.
	if (GetPersonFilterSettingBool(SHOW_VOICE_CALLS_DUE, recFilter) && bFilterByGrid)
	{
		QString strDue = strSQL + strTaskTypeFilter + " AND BTKS_SYSTEM_TYPE  = 2 AND (BTKS_OWNER = " + strPersonID + " OR BTKS_ORIGINATOR = " + strPersonID + ")" +
						" AND BTKS_IS_TASK_ACTIVE =1 AND BTKS_SYSTEM_STATUS < 3 AND \
						( \
						(BTKS_DUE >= ? AND BTKS_START < ?) OR \
						(BTKS_DUE IS NULL AND (BTKS_START IS NULL OR BTKS_START <= ?)) OR \
						(BTKS_START IS NULL AND BTKS_DUE > ?) \
						)";

		tmpRecordSet.clear();
			
		query.Prepare(Ret_pStatus, strDue);
		if (!Ret_pStatus.IsOK()) return;
		query.bindValue(0, QDateTime::currentDateTime());
		query.bindValue(1, QDateTime::currentDateTime());
		query.bindValue(2, QDateTime::currentDateTime());
		query.bindValue(3, QDateTime::currentDateTime());
		query.ExecutePrepared(Ret_pStatus);
		
		if (!Ret_pStatus.IsOK()) return;
		query.FetchData(tmpRecordSet);

		//Filter by date if date filter active.
		if (GetPersonFilterSettingBool(FILTER_BY_DATE_ACTIVE, recFilter))
			TaskDateFilter(datFrom, datTo, tmpRecordSet);

		//Check for double rows - if not double merge it.
		CheckForDoubleEntities(RetOut_recVoiceCalls, tmpRecordSet);
	}

	//Get overdue voice calls.
	if (GetPersonFilterSettingBool(SHOW_VOICE_CALLS_OVERDUE, recFilter) && bFilterByGrid)
	{
		QString strOverDue = strSQL + strTaskTypeFilter + " AND BTKS_SYSTEM_TYPE  = 2 AND (BTKS_OWNER = " + strPersonID + " OR BTKS_ORIGINATOR = " + strPersonID + ")" +
			" AND BTKS_IS_TASK_ACTIVE =1 AND BTKS_SYSTEM_STATUS < 3 \
			  AND BTKS_DUE < ?";
			
		tmpRecordSet.clear();

		query.Prepare(Ret_pStatus, strOverDue);
		if (!Ret_pStatus.IsOK()) return;
		query.bindValue(0, QDateTime::currentDateTime());
		query.ExecutePrepared(Ret_pStatus);
		
		if (!Ret_pStatus.IsOK()) return;
		query.FetchData(tmpRecordSet);

		//Filter by date if date filter active.
		if (GetPersonFilterSettingBool(FILTER_BY_DATE_ACTIVE, recFilter))
			TaskDateFilter(datFrom, datTo, tmpRecordSet);

		//Check for double rows - if not double merge it.
		CheckForDoubleEntities(RetOut_recVoiceCalls, tmpRecordSet);
	}

	//Get last N voice calls.
	if (GetPersonFilterSettingBool(SHOW_VOICE_CALLS_LAST, recFilter) && bFilterByGrid)
	{
		int nLastRecords = GetPersonFilterSettingInt(LAST_VOICE_CALLS_NUMBER, recFilter);
		
		QString strLastRecords = strSQL + strNoTaskTimeSql + strNoTaskSql + " ORDER BY BVC_DAT_LAST_MODIFIED DESC";
		GetDbManager()->GetFunction()->PrepareLimit(strLastRecords,0,nLastRecords);

		tmpRecordSet.clear();
		//Check for date/time filter.
		if (GetPersonFilterSettingBool(FILTER_BY_DATE_ACTIVE, recFilter))
		{
			query.Prepare(Ret_pStatus, strLastRecords);
			if (!Ret_pStatus.IsOK()) return;
			query.bindValue(0, datFrom);
			query.bindValue(1, datTo);
			query.ExecutePrepared(Ret_pStatus);
		}
		else
		{
			query.Execute(Ret_pStatus, strLastRecords);
		}
		if (!Ret_pStatus.IsOK()) return;
		query.FetchData(tmpRecordSet);
		//tmpRecordSet.Dump();

		//Check for double rows - if not double merge it.
		CheckForDoubleEntities(RetOut_recVoiceCalls, tmpRecordSet);
	}

	//If complex filter active and voice call filter active get all voice calls.
	if (!bFilterByGrid && bFilterByEntityType)
	{
		QString strLastRecords = strSQL + strNoTaskTimeSql + strNoTaskSql;

		//Check for date/time filter.
		if (GetPersonFilterSettingBool(FILTER_BY_DATE_ACTIVE, recFilter))
		{
			query.Prepare(Ret_pStatus, strLastRecords);
			if (!Ret_pStatus.IsOK()) return;
			query.bindValue(0, datFrom);
			query.bindValue(1, datTo);
			query.ExecutePrepared(Ret_pStatus);
		}
		else
		{
			query.Execute(Ret_pStatus, strLastRecords);
		}
		if (!Ret_pStatus.IsOK()) return;
		query.FetchData(RetOut_recVoiceCalls);
	}

	//If no voice call return.
	if (!RetOut_recVoiceCalls.getRowCount())
		return;
	
	//If grid not active filter by task.
	if ((!bFilterByGrid && bFilterByEntityType) || bForceFilterByTaskType)
		FilterByTaskType(recFilter, RetOut_recVoiceCalls, true);

	//Delete duplicate rows - when you assign multiple projects to an entity entry.
	CheckForDoubleEntitiesInContacts(RetOut_recVoiceCalls);

	//Filter by search text.
	if (GetPersonFilterSettingBool(FILTER_SEARCH_TEXT_ACTIVE, recFilter))
		FilterVoiceCallBySearchText(RetOut_recVoiceCalls, recFilter);
}

//Private for contact voice calls.
void Service_BusCommunication::GetContactVoiceCall(Status &Ret_pStatus, DbRecordSet &RetOut_recVoiceCalls, int nID, DbRecordSet recFilter)
{
	//Get logged person ID.
	QString strLoggedPersonID = QVariant(g_UserSessionManager->GetPersonID()).toString();
	//Locals.
	QString strWhere;
	QDateTime datFrom = GetPersonFilterSettingDateTime(FROM_DATE, recFilter);
	QDateTime datTo	  = GetPersonFilterSettingDateTime(TO_DATE, recFilter);
	RetOut_recVoiceCalls.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_COMM_GRID_VOICECALL_VIEW));
	strWhere = DbSqlTableView::getSQLView(DbSqlTableView::TVIEW_COMM_GRID_VOICECALL_VIEW);
	//strWhere += " WHERE ((NOT A.BPER_CONTACT_ID = " + QVariant(nID).toString() + ") OR A.BPER_CONTACT_ID IS NULL)"; commented on issue #1975.
	//strWhere += " AND BVC_CONTACT_ID = " + QVariant(nID).toString(); commented on issue #1975.
	strWhere += " WHERE BVC_CONTACT_ID = " + QVariant(nID).toString(); //added on issue #1975.
	
	//B.T. to fix: no more private
	//strWhere += " AND ((CENT_OWNER_ID = " + strLoggedPersonID + ") OR (CENT_OWNER_ID <> " + strLoggedPersonID + " AND CENT_IS_PRIVATE = 0)) ";
//	strWhere += " AND (CENT_OWNER_ID <> " + strLoggedPersonID + " AND CENT_IS_PRIVATE = 0) ";

	//Access rights.
	g_AccessRight->SQLFilterRecords(BUS_VOICECALLS, strWhere);

	//Filter by CE type.
	if (GetPersonFilterSettingBool(FILTER_BY_CE_ENTITY_TYPE_ACTIVE, recFilter))
	{
		QString strCENT_CE_TYPE_IDs;
		DbRecordSet recCEFilter = GetPersonFilterSettingRecordSet(CE_ENTITY_TYPE_FILTER_VOICE_CALLS, recFilter);
		int nRowCount = recCEFilter.getRowCount();
		if (nRowCount > 0)
		{
			for (int i = 0; i < nRowCount; ++i)
			{
				int nChecked = recCEFilter.getDataRef(i, "CHECKED_ITEM_STATE").toInt();
				if (QVariant(nChecked).toBool())
					strCENT_CE_TYPE_IDs += recCEFilter.getDataRef(i, "CHECKED_ITEM_ID").toString() + ",";
			}

			//If all items are unchecked then look only for those whose CENT_CE_TYPE_ID is NULL.
			if (strCENT_CE_TYPE_IDs.isEmpty())
				strCENT_CE_TYPE_IDs = " AND CENT_CE_TYPE_ID IS NULL ";
			else
			{
				//Chop last ,.
				strCENT_CE_TYPE_IDs.chop(1);
				QString strTmp = strCENT_CE_TYPE_IDs;
				strCENT_CE_TYPE_IDs = " AND CENT_CE_TYPE_ID IN (";
				strCENT_CE_TYPE_IDs += strTmp + ") ";
			}
		}

		strWhere += strCENT_CE_TYPE_IDs;
	}

	//Get query object and its select statement.
	DbSqlQuery query(Ret_pStatus, GetDbManager());
	if(!Ret_pStatus.IsOK()) return;

	if (GetPersonFilterSettingBool(FILTER_BY_DATE_ACTIVE, recFilter))
	{
		strWhere += " AND (BVC_START > ? AND BVC_START < ?) ";
		query.Prepare(Ret_pStatus, strWhere);
		if (!Ret_pStatus.IsOK()) return;
		query.bindValue(0, datFrom);
		query.bindValue(1, datTo);
		query.ExecutePrepared(Ret_pStatus);
	}
	else
		query.Execute(Ret_pStatus, strWhere);

	//Fetch data.
	if (!Ret_pStatus.IsOK()) return;
	query.FetchData(RetOut_recVoiceCalls);

	//If no voice call return.
	if (!RetOut_recVoiceCalls.getRowCount())
		return;

	//Check task type filter.
	FilterByTaskType(recFilter, RetOut_recVoiceCalls, true);
	
	//Filter by search text.
	if (GetPersonFilterSettingBool(FILTER_SEARCH_TEXT_ACTIVE, recFilter))
		FilterVoiceCallBySearchText(RetOut_recVoiceCalls, recFilter);
}

void Service_BusCommunication::GetProjectVoiceCall(Status &Ret_pStatus, DbRecordSet &RetOut_recVoiceCalls, int nProjectID, DbRecordSet recFilter)
{
	//Get logged person ID.
	QString strLoggedPersonID = QVariant(g_UserSessionManager->GetPersonID()).toString();
	//Locals.
	QString strWhere;
	QDateTime datFrom = GetPersonFilterSettingDateTime(FROM_DATE, recFilter);
	QDateTime datTo	  = GetPersonFilterSettingDateTime(TO_DATE, recFilter);
	RetOut_recVoiceCalls.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_COMM_GRID_VOICECALL_VIEW));
	strWhere = DbSqlTableView::getSQLView(DbSqlTableView::TVIEW_COMM_GRID_VOICECALL_VIEW);
	strWhere += " WHERE BVC_PROJECT_ID = " + QVariant(nProjectID).toString();
	//B.T. to fix: no more private
	//strWhere += " AND ((CENT_OWNER_ID = " + strLoggedPersonID + ") OR (CENT_OWNER_ID <> " + strLoggedPersonID + " AND CENT_IS_PRIVATE = 0)) ";

	//Access rights.
	g_AccessRight->SQLFilterRecords(BUS_VOICECALLS, strWhere);

	//Filter by CE type.
	if (GetPersonFilterSettingBool(FILTER_BY_CE_ENTITY_TYPE_ACTIVE, recFilter))
	{
		QString strCENT_CE_TYPE_IDs;
		DbRecordSet recCEFilter = GetPersonFilterSettingRecordSet(CE_ENTITY_TYPE_FILTER_VOICE_CALLS, recFilter);
		int nRowCount = recCEFilter.getRowCount();
		if (nRowCount > 0)
		{
			for (int i = 0; i < nRowCount; ++i)
			{
				int nChecked = recCEFilter.getDataRef(i, "CHECKED_ITEM_STATE").toInt();
				if (QVariant(nChecked).toBool())
					strCENT_CE_TYPE_IDs += recCEFilter.getDataRef(i, "CHECKED_ITEM_ID").toString() + ",";
			}

			//If all items are unchecked then look only for those whose CENT_CE_TYPE_ID is NULL.
			if (strCENT_CE_TYPE_IDs.isEmpty())
				strCENT_CE_TYPE_IDs = " AND CENT_CE_TYPE_ID IS NULL ";
			else
			{
				//Chop last ,.
				strCENT_CE_TYPE_IDs.chop(1);
				QString strTmp = strCENT_CE_TYPE_IDs;
				strCENT_CE_TYPE_IDs = " AND CENT_CE_TYPE_ID IN (";
				strCENT_CE_TYPE_IDs += strTmp + ") ";
			}
		}

		strWhere += strCENT_CE_TYPE_IDs;
	}

	//Get query object and its select statement.
	DbSqlQuery query(Ret_pStatus, GetDbManager());
	if(!Ret_pStatus.IsOK()) return;

	if (GetPersonFilterSettingBool(FILTER_BY_DATE_ACTIVE, recFilter))
	{
		strWhere += " AND (BVC_START > ? AND BVC_START < ?) ";
		query.Prepare(Ret_pStatus, strWhere);
		if (!Ret_pStatus.IsOK()) return;
		query.bindValue(0, datFrom);
		query.bindValue(1, datTo);
		query.ExecutePrepared(Ret_pStatus);
	}
	else
		query.Execute(Ret_pStatus, strWhere);

	//Fetch data.
	if (!Ret_pStatus.IsOK()) return;
	query.FetchData(RetOut_recVoiceCalls);

	//If no voice call return.
	if (!RetOut_recVoiceCalls.getRowCount())
		return;

	//Check task type filter.
	FilterByTaskType(recFilter, RetOut_recVoiceCalls, true);
	
	//Filter by search text.
	if (GetPersonFilterSettingBool(FILTER_SEARCH_TEXT_ACTIVE, recFilter))
		FilterVoiceCallBySearchText(RetOut_recVoiceCalls, recFilter);
}

void Service_BusCommunication::GetPersonEmailTasks(QString &strSQL, DbSqlQuery &query, Status &Ret_pStatus, DbRecordSet &RetOut_recEmails, QString &strPersonID, DbRecordSet &recFilter, bool &bForceFilterByTaskType, bool &bFilterByGrid, QDateTime &datFrom, QDateTime &datTo)
{
	//Create temporary recordset for data fetching.
	DbRecordSet tmpRecordSet;
	tmpRecordSet.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_COMM_GRID_EMAIL_VIEW));
	 
	QString strTaskTypeFilter = GetTaskTypeFilterString(recFilter);

	//Get scheduled emails.
	if (GetPersonFilterSettingBool(SHOW_EMAILS_SCHEDULED, recFilter) && bFilterByGrid)
	{
		QString strScheduled = strSQL + strTaskTypeFilter + " AND (BTKS_OWNER = " + strPersonID + " OR BTKS_ORIGINATOR = " + strPersonID + ")" +
			" AND BTKS_SYSTEM_TYPE = 3 AND BTKS_SYSTEM_STATUS < 3 \
			AND BTKS_IS_TASK_ACTIVE = 1 \
			AND BTKS_START IS NOT NULL AND BTKS_START > ?";

		tmpRecordSet.clear();

		query.Prepare(Ret_pStatus, strScheduled);
		if (!Ret_pStatus.IsOK()) return;
		query.bindValue(0, QDateTime::currentDateTime());
		query.ExecutePrepared(Ret_pStatus);

		if (!Ret_pStatus.IsOK()) return;
		query.FetchData(tmpRecordSet);

		//tmpRecordSet.Dump();

		//Filter by date if date filter active.
		if (GetPersonFilterSettingBool(FILTER_BY_DATE_ACTIVE, recFilter))
			TaskDateFilter(datFrom, datTo, tmpRecordSet);

		//Check for double rows - if not double merge it.
		CheckForDoubleEntities(RetOut_recEmails, tmpRecordSet);
		//tmpRecordSet.Dump();
	}

	//recFilter.Dump();

	//Get due emails.
	if (GetPersonFilterSettingBool(SHOW_EMAILS_DUE, recFilter) && bFilterByGrid)
	{
		QString strDue = strSQL + strTaskTypeFilter + " AND BTKS_SYSTEM_TYPE  = 3 AND (BTKS_OWNER = " + strPersonID + " OR BTKS_ORIGINATOR = " + strPersonID + ")" +
			" AND BTKS_IS_TASK_ACTIVE =1 AND BTKS_SYSTEM_STATUS < 3 AND \
			( \
			(BTKS_DUE >= ? AND BTKS_START < ?) OR \
			(BTKS_DUE IS NULL AND (BTKS_START IS NULL OR BTKS_START <= ?)) OR \
			(BTKS_START IS NULL AND BTKS_DUE > ?) \
			)";

		tmpRecordSet.clear();

		query.Prepare(Ret_pStatus, strDue);
		if (!Ret_pStatus.IsOK()) return;
		query.bindValue(0, QDateTime::currentDateTime());
		query.bindValue(1, QDateTime::currentDateTime());
		query.bindValue(2, QDateTime::currentDateTime());
		query.bindValue(3, QDateTime::currentDateTime());
		query.ExecutePrepared(Ret_pStatus);

		if (!Ret_pStatus.IsOK()) return;
		query.FetchData(tmpRecordSet);
		//tmpRecordSet.Dump();

		//Filter by date if date filter active.
		if (GetPersonFilterSettingBool(FILTER_BY_DATE_ACTIVE, recFilter))
			TaskDateFilter(datFrom, datTo, tmpRecordSet);

		//Check for double rows - if not double merge it.
		CheckForDoubleEntities(RetOut_recEmails, tmpRecordSet);
		//tmpRecordSet.Dump();
	}

	//Get overdue emails.
	if (GetPersonFilterSettingBool(SHOW_EMAILS_OVERDUE, recFilter) && bFilterByGrid)
	{
		QString strOverDue = strSQL + strTaskTypeFilter + " AND BTKS_SYSTEM_TYPE  = 3 AND (BTKS_OWNER = " + strPersonID + " OR BTKS_ORIGINATOR = " + strPersonID + ")" +
			" AND BTKS_SYSTEM_STATUS < 3 AND BTKS_IS_TASK_ACTIVE =1 \
			AND BTKS_DUE < ?";

		tmpRecordSet.clear();

		query.Prepare(Ret_pStatus, strOverDue);
		if (!Ret_pStatus.IsOK()) return;
		query.bindValue(0, QDateTime::currentDateTime());
		query.ExecutePrepared(Ret_pStatus);

		if (!Ret_pStatus.IsOK()) return;
		query.FetchData(tmpRecordSet);

		//tmpRecordSet.Dump();

		//Filter by date if date filter active.
		if (GetPersonFilterSettingBool(FILTER_BY_DATE_ACTIVE, recFilter))
			TaskDateFilter(datFrom, datTo, tmpRecordSet);

		//Check for double rows - if not double merge it.
		CheckForDoubleEntities(RetOut_recEmails, tmpRecordSet);
		//tmpRecordSet.Dump();
	}
}

void Service_BusCommunication::GetComplexFilterData(Status &Ret_pStatus, DbRecordSet &RetOut_recFilterData, int nPersonID /*= NULL*/)
{
	int nLoggedPersonID = g_UserSessionManager->GetPersonID();

	//Locals.
	int nUnassignedVoiceCalls	= 0;
	int nMissedVoiceCalls		= 0;
	int nScheduledVoiceCalls	= 0;
	int nDueVoiceCalls			= 0;
	int nOverdueVoiceCalls		= 0;
	int nUnreadEmails			= 0;
	int nUnassignedEmails		= 0;
	int nScheduledEmails		= 0;
	int nDueEmails				= 0;
	int nOverdueEmails			= 0;
	int nTemplatesEmails		= 0;
	int nCheckedOutDocuments	= 0;
	int nUnassignedDocuments	= 0;
	int nScheduledDocuments		= 0;
	int nDueDocuments			= 0;
	int nOverdueDocuments		= 0;
	int nTemplatesDocuments		= 0;

	QString strPersonID = QVariant(nPersonID).toString();
	QDate	  dateNow = QDate::currentDate();

	//Get query.
	DbSqlQuery query(Ret_pStatus, GetDbManager());
	if (!Ret_pStatus.IsOK()) return;

	QDateTime dateTimeNow = GetDbManager()->GetCurrentDateTime(Ret_pStatus,query.GetDbConnection());
	
	QString strSql;
	//VOICE CALLS
	//Unassigned - no contact.
	strSql = "SELECT COUNT(DISTINCT CENT_ID) \
			  FROM CE_COMM_ENTITY \
			  INNER JOIN BUS_VOICECALLS ON BVC_COMM_ENTITY_ID = CENT_ID \
			  WHERE CENT_OWNER_ID = " + strPersonID + " \
			  AND ((BVC_CONTACT_ID IS NULL) AND (CENT_TASK_ID IS NULL) AND (BVC_PROJECT_ID IS NULL))";

	//Access rights.
	g_AccessRight->SQLFilterRecords(BUS_VOICECALLS, strSql);

	query.Execute(Ret_pStatus, strSql);
	if (!Ret_pStatus.IsOK()) return;
	if(query.next())
		nUnassignedVoiceCalls = query.value(0).toInt();

	//Missed.
	strSql = "SELECT COUNT(*) \
			  FROM CE_COMM_ENTITY \
			  INNER JOIN BUS_VOICECALLS ON BVC_COMM_ENTITY_ID = CENT_ID \
			  WHERE CENT_OWNER_ID = " + strPersonID + " \
			  AND BVC_CALL_STATUS = 9 \
			  AND BVC_START > ? AND BVC_START < ? ";

	QDateTime datTodayStart(QDate::currentDate(), QTime(0,0,0));
	QDateTime datTodayEnd(QDate::currentDate(), QTime(23,59,59));
	
	//Access rights.
	g_AccessRight->SQLFilterRecords(BUS_VOICECALLS, strSql);

	query.Prepare(Ret_pStatus, strSql);
	if (!Ret_pStatus.IsOK()) return;
	query.bindValue(0, datTodayStart);
	query.bindValue(1, datTodayEnd);

	query.ExecutePrepared(Ret_pStatus);
	if (!Ret_pStatus.IsOK()) return;
	if(query.next())
		nMissedVoiceCalls = query.value(0).toInt();

	//Scheduled.
	//AND CENT_OWNER_ID = " + strPersonID + " - This was before. Task owner must see it's task even if not it's originator.
	strSql = "SELECT COUNT(*) \
			  FROM CE_COMM_ENTITY \
			  INNER JOIN BUS_VOICECALLS ON BVC_COMM_ENTITY_ID = CENT_ID \
			  INNER JOIN BUS_TASKS ON CENT_TASK_ID = BTKS_ID \
			  WHERE BTKS_SYSTEM_TYPE = 2 \
			  AND (BTKS_OWNER = " + strPersonID + " OR BTKS_ORIGINATOR = " + strPersonID + ") \
			  AND BTKS_IS_TASK_ACTIVE =1 AND BTKS_SYSTEM_STATUS < 3 \
			  AND BTKS_START IS NOT NULL AND BTKS_START > ? ";
	
	//Access rights.
	g_AccessRight->SQLFilterRecords(BUS_VOICECALLS, strSql);

	query.Prepare(Ret_pStatus, strSql);
	if (!Ret_pStatus.IsOK()) return;
	query.bindValue(0, dateTimeNow);

	query.ExecutePrepared(Ret_pStatus);
	if (!Ret_pStatus.IsOK()) return;
	if(query.next())
		nScheduledVoiceCalls = query.value(0).toInt();
	
	//Due.
	//AND CENT_OWNER_ID = " + strPersonID + " - This was before. Task owner must see it's task even if not it's originator.
	strSql = "SELECT COUNT(*) \
			  FROM CE_COMM_ENTITY \
			  INNER JOIN BUS_VOICECALLS ON BVC_COMM_ENTITY_ID = CENT_ID \
			  INNER JOIN BUS_TASKS ON CENT_TASK_ID = BTKS_ID \
			  WHERE BTKS_SYSTEM_TYPE = 2 \
			  AND (BTKS_OWNER = " + strPersonID + " OR BTKS_ORIGINATOR = " + strPersonID + ") \
			  AND BTKS_IS_TASK_ACTIVE =1 AND BTKS_SYSTEM_STATUS < 3 \
			  AND ((BTKS_DUE >= ? AND BTKS_START < ?) OR (BTKS_DUE IS NULL AND (BTKS_START IS NULL OR BTKS_START <= ?)) \
			  OR (BTKS_START IS NULL AND BTKS_DUE > ?) \
			  )";

	//Access rights.
	g_AccessRight->SQLFilterRecords(BUS_VOICECALLS, strSql);

	query.Prepare(Ret_pStatus, strSql);
	if (!Ret_pStatus.IsOK()) return;
	query.bindValue(0, dateTimeNow);
	query.bindValue(1, dateTimeNow);
	query.bindValue(2, dateTimeNow);
	query.bindValue(3, dateTimeNow);

	query.ExecutePrepared(Ret_pStatus);
	if (!Ret_pStatus.IsOK()) return;
	if(query.next())
		nDueVoiceCalls = query.value(0).toInt();

	//OverDue.
	//AND CENT_OWNER_ID = " + strPersonID + " - This was before. Task owner must see it's task even if not it's originator.
	strSql = "SELECT COUNT(*) \
			 FROM CE_COMM_ENTITY \
			 INNER JOIN BUS_VOICECALLS ON BVC_COMM_ENTITY_ID = CENT_ID \
			 INNER JOIN BUS_TASKS ON CENT_TASK_ID = BTKS_ID \
			 WHERE BTKS_SYSTEM_TYPE = 2 \
			 AND (BTKS_OWNER = " + strPersonID + " OR BTKS_ORIGINATOR = " + strPersonID + ") \
			 AND BTKS_IS_TASK_ACTIVE =1 AND BTKS_SYSTEM_STATUS < 3 \
			 AND BTKS_DUE < ?";

	//Access rights.
	g_AccessRight->SQLFilterRecords(BUS_VOICECALLS, strSql);

	query.Prepare(Ret_pStatus, strSql);
	if (!Ret_pStatus.IsOK()) return;
	query.bindValue(0, dateTimeNow);

	query.ExecutePrepared(Ret_pStatus);
	if (!Ret_pStatus.IsOK()) return;
	if(query.next())
		nOverdueVoiceCalls = query.value(0).toInt();

	//EMAILS.
	//Unread.
	strSql = "SELECT COUNT(*) \
			  FROM CE_COMM_ENTITY \
			  INNER JOIN BUS_EMAIL ON BEM_COMM_ENTITY_ID = CENT_ID \
			  WHERE CENT_OWNER_ID = " + strPersonID + " \
			  AND BEM_UNREAD_FLAG =1 \
			  AND BEM_OUTGOING = 0";

	//Access rights.
	g_AccessRight->SQLFilterRecords(BUS_EMAIL, strSql);

	query.Execute(Ret_pStatus, strSql);
	if (!Ret_pStatus.IsOK()) return;
	if(query.next())
		nUnreadEmails = query.value(0).toInt();

	//Unassigned.
	//LEFT OUTER JOIN CE_CONTACT_LINK ON CENT_ID = CELC_COMM_ENTITY_ID
	strSql = "SELECT COUNT(DISTINCT CENT_ID) \
			  FROM CE_COMM_ENTITY \
			  INNER JOIN BUS_EMAIL ON BEM_COMM_ENTITY_ID = CENT_ID \
			  LEFT OUTER JOIN BUS_NMRX_RELATION ON CENT_ID = BNMR_TABLE_KEY_ID_1 AND "+QString(NMRX_CE_PROJECT_WHERE)+\
			  " WHERE CENT_OWNER_ID = " + strPersonID + " \
			  AND (((SELECT COUNT(*) FROM BUS_NMRX_RELATION WHERE BNMR_TABLE_KEY_ID_1 = CENT_ID AND "+NMRX_CE_CONTACT_WHERE+") <= 1) AND CENT_TASK_ID IS NULL AND (BEM_TEMPLATE_FLAG =0 OR BEM_TEMPLATE_FLAG IS NULL) AND BNMR_ID IS NULL AND BEM_PROJECT_ID IS NULL)";
//			  AND (CELC_ID IS NULL AND CENT_TASK_ID IS NULL AND BEM_TEMPLATE_FLAG =0 AND CELP_ID IS NULL)";

	//Access rights.
	g_AccessRight->SQLFilterRecords(BUS_EMAIL, strSql);

	query.Execute(Ret_pStatus, strSql);
	if (!Ret_pStatus.IsOK()) return;
	if(query.next())
		nUnassignedEmails = query.value(0).toInt();

	//Scheduled.
	//AND CENT_OWNER_ID = " + strPersonID + " - This was before. Task owner must see it's task even if not it's originator.
	strSql = "SELECT COUNT(*) \
			  FROM CE_COMM_ENTITY \
			  INNER JOIN BUS_EMAIL ON BEM_COMM_ENTITY_ID = CENT_ID \
			  INNER JOIN BUS_TASKS ON CENT_TASK_ID = BTKS_ID \
			  WHERE BTKS_SYSTEM_TYPE = 3 \
			  AND (BTKS_OWNER = " + strPersonID + " OR BTKS_ORIGINATOR = " + strPersonID + ") \
			  AND BTKS_IS_TASK_ACTIVE =1 AND BTKS_SYSTEM_STATUS < 3 \
			  AND BTKS_START IS NOT NULL AND BTKS_START > ? ";

	//Access rights.
	g_AccessRight->SQLFilterRecords(BUS_EMAIL, strSql);

	query.Prepare(Ret_pStatus, strSql);
	if (!Ret_pStatus.IsOK()) return;
	query.bindValue(0, dateTimeNow);
	query.ExecutePrepared(Ret_pStatus);
	if (!Ret_pStatus.IsOK()) return;
	if(query.next())
		nScheduledEmails = query.value(0).toInt();

	//Due.
	//AND CENT_OWNER_ID = " + strPersonID + " - This was before. Task owner must see it's task even if not it's originator.
	strSql = "SELECT COUNT(*) \
			  FROM CE_COMM_ENTITY \
			  INNER JOIN BUS_EMAIL ON BEM_COMM_ENTITY_ID = CENT_ID \
			  INNER JOIN BUS_TASKS ON CENT_TASK_ID = BTKS_ID \
			  WHERE BTKS_SYSTEM_TYPE = 3 \
			  AND (BTKS_OWNER = " + strPersonID + " OR BTKS_ORIGINATOR = " + strPersonID + ") \
			  AND BTKS_IS_TASK_ACTIVE =1 \
			  AND BTKS_SYSTEM_STATUS < 3 \
			  AND ((BTKS_DUE >= ? AND BTKS_START < ?) OR (BTKS_DUE IS NULL AND (BTKS_START IS NULL OR BTKS_START <= ?)) \
			  OR (BTKS_START IS NULL AND BTKS_DUE > ?) \
			  )";

	//Access rights.
	g_AccessRight->SQLFilterRecords(BUS_EMAIL, strSql);

	query.Prepare(Ret_pStatus, strSql);
	if (!Ret_pStatus.IsOK()) return;
	query.bindValue(0, dateTimeNow);
	query.bindValue(1, dateTimeNow);
	query.bindValue(2, dateTimeNow);
	query.bindValue(3, dateTimeNow);
	
	query.ExecutePrepared(Ret_pStatus);
	if (!Ret_pStatus.IsOK()) return;
	if(query.next())
		nDueEmails = query.value(0).toInt();

	//OverDue.
	//AND CENT_OWNER_ID = " + strPersonID + " - This was before. Task owner must see it's task even if not it's originator.
	strSql = "SELECT COUNT(*) \
			  FROM CE_COMM_ENTITY \
			  INNER JOIN BUS_EMAIL ON BEM_COMM_ENTITY_ID = CENT_ID \
			  INNER JOIN BUS_TASKS ON CENT_TASK_ID = BTKS_ID \
			  WHERE BTKS_SYSTEM_TYPE = 3 \
			  AND (BTKS_OWNER = " + strPersonID + " OR BTKS_ORIGINATOR = " + strPersonID + ") \
			  AND BTKS_IS_TASK_ACTIVE =1 AND BTKS_SYSTEM_STATUS < 3 \
			  AND BTKS_DUE < ?";

	//Access rights.
	g_AccessRight->SQLFilterRecords(BUS_EMAIL, strSql);

	query.Prepare(Ret_pStatus, strSql);
	if (!Ret_pStatus.IsOK()) return;
	query.bindValue(0, dateTimeNow);
	query.ExecutePrepared(Ret_pStatus);
	if (!Ret_pStatus.IsOK()) return;
	if(query.next())
		nOverdueEmails = query.value(0).toInt();

	//Templates.
	strSql = "SELECT COUNT(*) \
			  FROM CE_COMM_ENTITY \
			  INNER JOIN BUS_EMAIL ON BEM_COMM_ENTITY_ID = CENT_ID \
			  WHERE CENT_OWNER_ID = " + strPersonID + " \
			  AND BEM_TEMPLATE_FLAG =1";

	//Access rights.
	g_AccessRight->SQLFilterRecords(BUS_EMAIL, strSql);

	query.Execute(Ret_pStatus, strSql);
	if (!Ret_pStatus.IsOK()) return;
	if(query.next())
		nTemplatesEmails = query.value(0).toInt();

	//DOCUMENTS.
	//Unassigned.
	strSql = "SELECT COUNT(DISTINCT CENT_ID) \
			  FROM BUS_DM_DOCUMENTS \
			  INNER JOIN CE_COMM_ENTITY ON BDMD_COMM_ENTITY_ID = CENT_ID\
			  LEFT OUTER JOIN BUS_NMRX_RELATION K2 ON CENT_ID = K2.BNMR_TABLE_KEY_ID_1 AND "+QString(NMRX_CE_CONTACT_WHERE_AS_K2)+\
			  " LEFT OUTER JOIN BUS_NMRX_RELATION K1 ON CENT_ID = K1.BNMR_TABLE_KEY_ID_1 AND "+QString(NMRX_CE_PROJECT_WHERE_AS_K1)+ \
			  " WHERE CENT_OWNER_ID = " + strPersonID + " \
			  AND (K2.BNMR_ID IS NULL AND CENT_TASK_ID IS NULL AND BDMD_TEMPLATE_FLAG =0 AND K1.BNMR_ID IS NULL)";

	//Access rights.
	g_AccessRight->SQLFilterRecords(BUS_DM_DOCUMENTS, strSql);
	
	query.Execute(Ret_pStatus, strSql);
	if (!Ret_pStatus.IsOK()) return;
	if(query.next())
		nUnassignedDocuments = query.value(0).toInt();

	//Scheduled.
	//AND CENT_OWNER_ID = " + strPersonID + " - This was before. Task owner must see it's task even if not it's originator.
	strSql = "SELECT COUNT(*) \
			  FROM CE_COMM_ENTITY \
			  INNER JOIN BUS_DM_DOCUMENTS ON BDMD_COMM_ENTITY_ID = CENT_ID \
			  INNER JOIN BUS_TASKS ON CENT_TASK_ID = BTKS_ID \
			  WHERE BTKS_SYSTEM_TYPE = 1 \
			  AND (BTKS_OWNER = " + strPersonID + " OR BTKS_ORIGINATOR = " + strPersonID + ") \
			  AND BTKS_IS_TASK_ACTIVE =1 AND BTKS_SYSTEM_STATUS < 3 \
			  AND BTKS_START IS NOT NULL AND BTKS_START > ? ";

	//Access rights.
	g_AccessRight->SQLFilterRecords(BUS_DM_DOCUMENTS, strSql);

	query.Prepare(Ret_pStatus, strSql);
	if (!Ret_pStatus.IsOK()) return;
	query.bindValue(0, dateTimeNow);
	query.ExecutePrepared(Ret_pStatus);
	if(query.next())
		nScheduledDocuments = query.value(0).toInt();

	//Due.
	//AND CENT_OWNER_ID = " + strPersonID + " - This was before. Task owner must see it's task even if not it's originator.
	strSql = "SELECT COUNT(BDMD_ID) \
			 FROM CE_COMM_ENTITY \
			 INNER JOIN BUS_DM_DOCUMENTS ON BDMD_COMM_ENTITY_ID = CENT_ID \
			 INNER JOIN BUS_TASKS ON CENT_TASK_ID = BTKS_ID \
			 AND (BTKS_OWNER = " + strPersonID + " OR BTKS_ORIGINATOR = " + strPersonID + ") \
			 AND BTKS_IS_TASK_ACTIVE =1 AND BTKS_SYSTEM_STATUS < 3 \
			 AND ((BTKS_DUE >= ? AND BTKS_START < ?) OR (BTKS_DUE IS NULL AND (BTKS_START IS NULL OR BTKS_START <= ?)) OR (BTKS_START IS NULL AND BTKS_DUE > ?)) \
			 WHERE BTKS_SYSTEM_TYPE = 1";

	//Access rights.
	g_AccessRight->SQLFilterRecords(BUS_DM_DOCUMENTS, strSql);

	query.Prepare(Ret_pStatus, strSql);
	if (!Ret_pStatus.IsOK()) return;
	query.bindValue(0, dateTimeNow);
	query.bindValue(1, dateTimeNow);
	query.bindValue(2, dateTimeNow);
	query.bindValue(3, dateTimeNow);

	query.ExecutePrepared(Ret_pStatus);
	if (!Ret_pStatus.IsOK()) return;
	if(query.next())
		nDueDocuments = query.value(0).toInt();

	//OverDue.
	//AND CENT_OWNER_ID = " + strPersonID + " - This was before. Task owner must see it's task even if not it's originator.
	strSql = "SELECT COUNT(*) \
			 FROM CE_COMM_ENTITY \
			 INNER JOIN BUS_DM_DOCUMENTS ON BDMD_COMM_ENTITY_ID = CENT_ID \
			 INNER JOIN BUS_TASKS ON CENT_TASK_ID = BTKS_ID \
			 WHERE BTKS_SYSTEM_TYPE = 1 \
			 AND (BTKS_OWNER = " + strPersonID + " OR BTKS_ORIGINATOR = " + strPersonID + ") \
			 AND BTKS_IS_TASK_ACTIVE =1 AND BTKS_SYSTEM_STATUS < 3 \
			 AND BTKS_DUE < ?";

	//Access rights.
	g_AccessRight->SQLFilterRecords(BUS_DM_DOCUMENTS, strSql);

	query.Prepare(Ret_pStatus, strSql);
	if (!Ret_pStatus.IsOK()) return;
	query.bindValue(0, dateTimeNow);
	query.ExecutePrepared(Ret_pStatus);
	if (!Ret_pStatus.IsOK()) return;
	if(query.next())
		nOverdueDocuments = query.value(0).toInt();

	//Templates.
	strSql = "SELECT COUNT(*) \
			  FROM CE_COMM_ENTITY \
			  INNER JOIN BUS_DM_DOCUMENTS ON BDMD_COMM_ENTITY_ID = CENT_ID \
			  WHERE CENT_OWNER_ID = " + strPersonID + " \
			  AND BDMD_TEMPLATE_FLAG =1";

	//Access rights.
	g_AccessRight->SQLFilterRecords(BUS_DM_DOCUMENTS, strSql);

	query.Execute(Ret_pStatus, strSql);
	if (!Ret_pStatus.IsOK()) return;
	query.next();
	nTemplatesDocuments = query.value(0).toInt();

	//Checked out documents.
	strSql = "SELECT COUNT(*) \
			 FROM CE_COMM_ENTITY \
			 INNER JOIN BUS_DM_DOCUMENTS ON BDMD_COMM_ENTITY_ID = CENT_ID \
			 WHERE CENT_OWNER_ID = " + strPersonID + " \
			 AND BDMD_IS_CHECK_OUT =1";

	//Access rights.
	g_AccessRight->SQLFilterRecords(BUS_DM_DOCUMENTS, strSql);

	query.Execute(Ret_pStatus, strSql);
	if (!Ret_pStatus.IsOK()) return;
	query.next();
	nCheckedOutDocuments = query.value(0).toInt();

	RetOut_recFilterData.destroy();
	RetOut_recFilterData.defineFromView(DbSqlTableView::getView(DbSqlTableView::MVIEW_COMPLEX_COMM_FILTER));
	RetOut_recFilterData.addRow();
	//VOICE CALLS
	RetOut_recFilterData.setData(0, "UNASSIGNEDVOICECALLS", nUnassignedVoiceCalls);
	RetOut_recFilterData.setData(0, "MISSEDVOICECALLS",		nMissedVoiceCalls);
	RetOut_recFilterData.setData(0, "SCHEDULEDVOICECALLS",	nScheduledVoiceCalls);
	RetOut_recFilterData.setData(0, "DUEVOICECALLS",		nDueVoiceCalls);
	RetOut_recFilterData.setData(0, "OVERDUEVOICECALLS",	nOverdueVoiceCalls);
	//EMAILS
	RetOut_recFilterData.setData(0, "UNREADEMAILS",			nUnreadEmails);
	RetOut_recFilterData.setData(0, "UNASSIGNEDEMAILS",		nUnassignedEmails);
	RetOut_recFilterData.setData(0, "SCHEDULEDEMAILS",		nScheduledEmails);
	RetOut_recFilterData.setData(0, "DUEEMAILS",			nDueEmails);
	RetOut_recFilterData.setData(0, "OVERDUEEMAILS",		nOverdueEmails);
	RetOut_recFilterData.setData(0, "TEMPLATESEMAILS",		nTemplatesEmails);
	//DOCUMENTS
	RetOut_recFilterData.setData(0, "CHECKEDOUTDOCUMENTS",	nCheckedOutDocuments);
	RetOut_recFilterData.setData(0, "UNASSIGNEDDOCUMENTS",	nUnassignedDocuments);
	RetOut_recFilterData.setData(0, "SCHEDULEDDOCUMENTS",	nScheduledDocuments);
	RetOut_recFilterData.setData(0, "DUEDOCUMENTS",			nDueDocuments);
	RetOut_recFilterData.setData(0, "OVERDUEDOCUMENTS",		nOverdueDocuments);
	RetOut_recFilterData.setData(0, "TEMPLATESDOCUMENTS",	nTemplatesDocuments);
}

void Service_BusCommunication::GetContactFromEmail(Status &Ret_pStatus, DbRecordSet &RetOut_lstData)
{
	DbSqlQuery query(Ret_pStatus, GetDbManager());
	if(!Ret_pStatus.IsOK()) return;

	int nContactCol = RetOut_lstData.getColumnIdx("BCME_CONTACT_ID");
	int nEmailCol   = RetOut_lstData.getColumnIdx("BUS_CM_EMAIL");

	QString strSql = "SELECT BCME_CONTACT_ID FROM BUS_CM_EMAIL WHERE LOWER(BCME_ADDRESS)=?";
	query.Prepare(Ret_pStatus, strSql);
	if (!Ret_pStatus.IsOK()) return;

	int nCount = RetOut_lstData.getRowCount();
	for(int i=0; i<nCount; i++)
	{
		//case-insensitive search
		QString strEmail = RetOut_lstData.getDataRef(i, nEmailCol).toString().toLower();

		query.bindValue(0, strEmail);
		query.ExecutePrepared(Ret_pStatus);
		if (!Ret_pStatus.IsOK()) return;
		
		DbRecordSet rcData;
		query.FetchData(rcData);

		//write ID back to original list
		if(rcData.getRowCount() > 0)
			RetOut_lstData.setData(i, nContactCol, rcData.getDataRef(0, 0).toInt());
	}
}

//Set task is done - means:
//	BTKS_SYSTEM_STATUS = GlobalConstants::TASK_STATUS_COMPLETED (3 - completed)
//	BTKS_COMPLETED = QDateTime::currentDateTime()
//	if BTKS_OWNER isNull() -> BTKS_OWNER = g_UserSessionManager->GetPersonID()
//  ALSO ->
// [#927] Note: Re: Time in "Task Done" Phone Call Tasks set to 00:00
// When a phone call task is marked as "Done" AND the start time is 00:00, 
// then set the date and start time to the actual date time. The end time remains empty.
//	--> it is done on server.

void Service_BusCommunication::SetTaskIsDone(Status &Ret_pStatus, DbRecordSet &lstData)
{
	DbRecordSet tmp;
	QDateTime datCurrentDateTime = QDateTime::currentDateTime();

	//Main Table ORM for getting data.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_TASKS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	
	//recCommEntity->Dump();

	//--------------------------------------
	//START TRANSACTION
	//--------------------------------------
	TableOrm.GetDbSqlQuery()->BeginTransaction(Ret_pStatus);
	if (!Ret_pStatus.IsOK())
	{
		return;
	}

	//Read.
	TableOrm.ReadFromParentIDs(Ret_pStatus, tmp, "BTKS_ID", lstData, "BTKS_ID");
	if (!Ret_pStatus.IsOK())
	{
		TableOrm.GetDbSqlQuery()->Rollback();
		return;
	}

	//Set values to mark task as done.
	int nRowCount = tmp.getRowCount();
	for (int i = 0; i < nRowCount; ++i)
	{
		tmp.setData(i, "BTKS_SYSTEM_STATUS", GlobalConstants::TASK_STATUS_COMPLETED);
		tmp.setData(i, "BTKS_COMPLETED", datCurrentDateTime);
		if (tmp.getDataRef(i, "BTKS_OWNER").isNull())
			tmp.setData(i, "BTKS_OWNER", g_UserSessionManager->GetPersonID());
	}


	//Write recordset back.
	TableOrm.Write(Ret_pStatus, tmp);
	if (!Ret_pStatus.IsOK())
	{
		TableOrm.GetDbSqlQuery()->Rollback();
		return;
	}

	//Select rows that have the same originator and owner of task.
	for (int i = 0; i < nRowCount; i++)
	{
		if(tmp.getDataRef(i, "BTKS_OWNER").toInt() == tmp.getDataRef(i, "BTKS_ORIGINATOR"))
			tmp.selectRow(i);
	}

	//Delete rows that have the same owner and originator.
	tmp.deleteSelectedRows();

	//If nothing to update on CE_COMM_ENTITY return.
	if (!tmp.getRowCount())
	{
		TableOrm.GetDbSqlQuery()->Commit(Ret_pStatus);
		return;
	}

	DbSimpleOrm CENT_TableOrm(Ret_pStatus, CE_COMM_ENTITY, GetDbManager()); 
	if (!Ret_pStatus.IsOK())
	{
		TableOrm.GetDbSqlQuery()->Rollback();
		return;
	}

	QString strWhere = " WHERE CENT_TASK_ID IN (";
	nRowCount = tmp.getRowCount();
	for (int i = 0; i < nRowCount; i++)
		strWhere += tmp.getDataRef(i, "BTKS_ID").toString() + ",";

	strWhere.chop(1);
	strWhere += ") ";

	DbRecordSet recCENT;
	CENT_TableOrm.Read(Ret_pStatus, recCENT, strWhere);
	if (!Ret_pStatus.IsOK())
	{
		TableOrm.GetDbSqlQuery()->Rollback();
		return;
	}

	DbRecordSet recPhoneCENT;
	recPhoneCENT.copyDefinition(recCENT);

	//Loop through CENT and update CENT_OWNER_ID.
	nRowCount = recCENT.getRowCount();
	for (int i = 0; i < nRowCount; i++)
	{
		//Find row number of record in BTKS table.
		int nRow = tmp.find("BTKS_ID", recCENT.getDataRef(i, "CENT_TASK_ID").toInt(), true);
		int nOwnerID = tmp.getDataRef(nRow,	"BTKS_OWNER").toInt();
		recCENT.setData(i, "CENT_OWNER_ID", nOwnerID);

		//See is this CE entity a phone call. If true store it in a list for later updating - issue #924.
		if (recCENT.getDataRef(i, "CENT_SYSTEM_TYPE_ID").toInt() == 0)
		{
			recPhoneCENT.addRow();
			recPhoneCENT.setData(recPhoneCENT.getRowCount()-1, "CENT_ID", recCENT.getDataRef(i, "CENT_ID").toInt());
		}
	}

	//Write the new owner of CENT.
	CENT_TableOrm.Write(Ret_pStatus, recCENT);
	if (!Ret_pStatus.IsOK())
	{
		TableOrm.GetDbSqlQuery()->Rollback();
		return;
	}

	//Issue #924 - go through voice calls and update.
	if (recPhoneCENT.getRowCount() > 0)
	{
		DbSimpleOrm BUS_VOICECALLS_TableORM(Ret_pStatus, BUS_VOICECALLS, GetDbManager()); 
		if (!Ret_pStatus.IsOK())
		{
			TableOrm.GetDbSqlQuery()->Rollback();
			return;
		}

		//Read voice calls from CENT_ID.
		DbRecordSet recVOICE_CALLS;
		BUS_VOICECALLS_TableORM.ReadFromParentIDs(Ret_pStatus, recVOICE_CALLS, "BVC_COMM_ENTITY_ID", recPhoneCENT, "CENT_ID");
		if (!Ret_pStatus.IsOK())
		{
			TableOrm.GetDbSqlQuery()->Rollback();
			return;
		}

		//If no voice calls do nothing.
		if (recVOICE_CALLS.getRowCount() > 0)
		{
			int nRowCount = recVOICE_CALLS.getRowCount();
			for (int i = 0; i < nRowCount; ++i)
			{
				QTime startTime = recVOICE_CALLS.getDataRef(i, "BVC_START").toDateTime().time();
				if (startTime == QTime(0, 0, 0, 0))
				{
					recVOICE_CALLS.setData(i, "BVC_START", datCurrentDateTime);
					recVOICE_CALLS.selectRow(i);
				}
			}

			//Delete rows that don't need to be updated.
			recVOICE_CALLS.deleteUnSelectedRows();

			//Write updated start time.
			if (recVOICE_CALLS.getRowCount() > 0)
			{
				BUS_VOICECALLS_TableORM.Write(Ret_pStatus, recVOICE_CALLS);
				if (!Ret_pStatus.IsOK())
				{
					TableOrm.GetDbSqlQuery()->Rollback();
					return;
				}
			}
		}
	}

	//--------------------------------------
	//COMMIT TRANSACTION
	//--------------------------------------
	TableOrm.GetDbSqlQuery()->Commit(Ret_pStatus);

	if (Ret_pStatus.IsOK())
	{
		g_PrivateServiceSet->BusinessHelper->ClientTaskNotification(Ret_pStatus,tmp,false);
	}


}

void Service_BusCommunication::PostPoneTasks(Status &Ret_pStatus, DbRecordSet &lstData, QDateTime datDueDateTime, QDateTime datStartDateTime)
{
	DbRecordSet tmp;
	QDateTime datCurrentDateTime = QDateTime::currentDateTime();

	//Main Table ORM for getting data.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_TASKS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	//recCommEntity->Dump();

	//--------------------------------------
	//START TRANSACTION
	//--------------------------------------
	TableOrm.GetDbSqlQuery()->BeginTransaction(Ret_pStatus);
	if (!Ret_pStatus.IsOK())
	{
		return;
	}

	//Read.
	TableOrm.ReadFromParentIDs(Ret_pStatus, tmp, "BTKS_ID", lstData, "BTKS_ID");
	if (!Ret_pStatus.IsOK())
	{
		TableOrm.GetDbSqlQuery()->Rollback();
		return;
	}

	//Set values to mark task as done.
	int nRowCount = tmp.getRowCount();
	for (int i = 0; i < nRowCount; ++i)
	{
		tmp.setData(i, "BTKS_DUE", datDueDateTime);
		tmp.setData(i, "BTKS_START", datStartDateTime);
	}

	//Write recordset back.
	TableOrm.Write(Ret_pStatus, tmp);
	if (!Ret_pStatus.IsOK())
	{
		TableOrm.GetDbSqlQuery()->Rollback();
		return;
	}

	//--------------------------------------
	//COMMIT TRANSACTION
	//--------------------------------------
	TableOrm.GetDbSqlQuery()->Commit(Ret_pStatus);

	if (Ret_pStatus.IsOK())
	{
		g_PrivateServiceSet->BusinessHelper->ClientTaskNotification(Ret_pStatus,tmp,false);
	}
}

void Service_BusCommunication::SetEmailsRead(Status &Ret_pStatus, DbRecordSet &lstData/*=NULL*/)
{
	DbRecordSet tmp;

	//Main Table ORM for getting data.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_EMAIL, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	//Lock first.
	BusinessLocker Locker(Ret_pStatus,BUS_EMAIL,GetDbManager());
	if(!Ret_pStatus.IsOK()) return;

	QString strLockRes;
	Locker.Lock(Ret_pStatus,lstData,strLockRes);
	if(!Ret_pStatus.IsOK()) return;

	//Status for locker.
	Status err;

	//Read.
	TableOrm.ReadFromParentIDs(Ret_pStatus, tmp, "BEM_ID", lstData, "BEM_ID");
	if(!Ret_pStatus.IsOK()) 
	{
		Locker.UnLock(err, strLockRes);
		if(!err.IsOK()) 
			Ret_pStatus = err;

		return;
	}

	//Set values to mark task as done.
	int nRowCount = tmp.getRowCount();
	for (int i = 0; i < nRowCount; ++i)
		tmp.setData(i, "BEM_UNREAD_FLAG", 0);

	//Write recordset back.
	TableOrm.Write(Ret_pStatus, tmp);
	if(!Ret_pStatus.IsOK())
	{
		Locker.UnLock(err, strLockRes);
		if(!err.IsOK()) 
			Ret_pStatus = err;
		
		return;
	}

	Locker.UnLock(Ret_pStatus, strLockRes);
}

//By courtesy of B.T.
//Change assignment for more different entities in CE grid from SAPNE, locks entity before engaging
//nAssignedID = project or contact id, oe owner id
//nAssignedType = 0-contact, 1-project, 2- owner
//nOperation = 1-add, 0-remove
//nAssignedRole only for contacts: to, from etc...from CELC_LINK_ROLE_ID
//Data<CENT_ID,CE_TYPE,<BEM_ID, BDMD_ID, BVC_ID>,ROLE_ID> (4 cols)
void Service_BusCommunication::ChangeEntityAssigment(Status &Ret_pStatus, int nAssignedType,int nAssignedID,  int nOperation, DbRecordSet &Data)
{

	//Load whole bunch of orms, let's rock

	//Contact data Table ORM.
	DbSimpleOrm NMRXOrm(Ret_pStatus, BUS_NMRX_RELATION, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	DbSimpleOrm EmailOrm(Ret_pStatus, BUS_EMAIL, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	DbSimpleOrm VoiceOrm(Ret_pStatus, BUS_VOICECALLS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	BusinessLocker Locker(Ret_pStatus, CE_COMM_ENTITY, GetDbManager());
	if(!Ret_pStatus.IsOK()) 
		return;

	Status err;

	//_DUMP(Data);

	//no transaction: when 1st fails, return, blah...
	int nSize=Data.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		int nCE_ID=Data.getDataRef(i,0).toInt();
		int nCe_Type=Data.getDataRef(i,1).toInt();
		int nEntityID=Data.getDataRef(i,2).toInt();
		int nAssignedRole=Data.getDataRef(i,3).toInt();

		//BT: issue 2535, assigned id is incorrect (-1) so use busp_id or contact id inside lists..
		if (nOperation==0)
		{
			if (nAssignedType==0) //contact
			{
				nAssignedID=Data.getDataRef(i,5).toInt();
			}
			else if (nAssignedType==1) //project
			{
				nAssignedID=Data.getDataRef(i,4).toInt();
			}
		}

		if (nCE_ID == 113)
		{
			//_DUMP(Data);
		}

		//BT: on 2535: on error abort all further actions!!!!

		//store global error (reload CE grid on client):
		//if (!Ret_pStatus.IsOK()) err=Ret_pStatus;
		//Ret_pStatus.setError(0);


		//if type=2, set owner: only assignedID is valid
		if (nAssignedType==2)
		{
			//lock
			DbLockList list;
			list<<nCE_ID;
			QString strLocked;
			Locker.Lock(Ret_pStatus,list,strLocked);
			if(!Ret_pStatus.IsOK()) return;

			if (nOperation) //Adding new assignment, make sure is not duplicate
			{
				QString strSQL=" UPDATE ce_comm_entity SET CENT_OWNER_ID= "+QVariant(nAssignedID).toString()+" WHERE CENT_ID ="+QVariant(nCE_ID).toString(); 
				EmailOrm.GetDbSqlQuery()->Execute(Ret_pStatus,strSQL);
				if(!Ret_pStatus.IsOK()) 
				{
					Status err;
					Locker.UnLock(err,strLocked);
					return;
				}
			}
			else //remove:
			{
				QString strSQL=" UPDATE ce_comm_entity SET CENT_OWNER_ID= NULL WHERE CENT_ID ="+QVariant(nCE_ID).toString(); 
				EmailOrm.GetDbSqlQuery()->Execute(Ret_pStatus,strSQL);
				if(!Ret_pStatus.IsOK()) 
				{
					Status err;
					Locker.UnLock(err,strLocked);
					return;
				}

			}
			
			//unlock
			Locker.UnLock(Ret_pStatus,strLocked);
			continue;
		}




		//switch:
		switch(nCe_Type)
		{
		case GlobalConstants::CE_TYPE_DOCUMENT: //ignore project & contact role
			{
				//read, if found return, if not found insert
				if (nAssignedType) //PROJECT
				{
					//lock
					DbLockList list;
					list<<nCE_ID;
					QString strLocked;
					Locker.Lock(Ret_pStatus,list,strLocked);
					if(!Ret_pStatus.IsOK())
						return;

					if (nOperation) //Adding new assignment, make sure is not duplicate
					{
						DbRecordSet lstData;
						QString strWhere=" WHERE BNMR_TABLE_KEY_ID_1 ="+QVariant(nCE_ID).toString(); 
						strWhere +=" AND "+QString(NMRX_CE_PROJECT_WHERE);
						NMRXOrm.Read(Ret_pStatus,lstData,strWhere);
						if(!Ret_pStatus.IsOK()) 
						{
							Status err;
							Locker.UnLock(err,strLocked);
							return;
						}

						int nRow=lstData.find(lstData.getColumnIdx("BNMR_TABLE_KEY_ID_2"),nAssignedID,true);
						if (nRow!=-1)
						{
							Status err;
							Locker.UnLock(err,strLocked);
							continue;
						}

						lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION));
						lstData.addRow();
						lstData.setData(0, "BNMR_TABLE_KEY_ID_1", nCE_ID);
						lstData.setData(0, "BNMR_TABLE_KEY_ID_2", nAssignedID);
						lstData.setData(0, "BNMR_SYSTEM_ROLE", nAssignedRole);
						lstData.setColValue("BNMR_TABLE_1",CE_COMM_ENTITY);
						lstData.setColValue("BNMR_TABLE_2",BUS_PROJECT);
						lstData.setColValue("BNMR_TYPE",GlobalConstants::NMRX_TYPE_ASSIGN);

						//write
						NMRXOrm.Write(Ret_pStatus,lstData);
						if(!Ret_pStatus.IsOK()) 
						{
							Status err;
							Locker.UnLock(err,strLocked);
							return;
						}
					}
					else //Removing assignment
					{
						//issue 2535:
						QString strSQL=" DELETE FROM BUS_NMRX_RELATION WHERE BNMR_TABLE_KEY_ID_1 ="+QVariant(nCE_ID).toString()+ " AND BNMR_TABLE_KEY_ID_2="+QVariant(nAssignedID).toString(); 
						strSQL+=" AND "+QString(NMRX_CE_PROJECT_WHERE);
						NMRXOrm.GetDbSqlQuery()->Execute(Ret_pStatus,strSQL);
						if(!Ret_pStatus.IsOK()) 
						{
							Status err;
							Locker.UnLock(err,strLocked);
							return;
						}
					}

					//unlock
					Locker.UnLock(Ret_pStatus,strLocked);
					continue;

				}
				else //CONTACT
				{
					//lock
					DbLockList list;
					list<<nCE_ID;
					QString strLocked;
					Locker.Lock(Ret_pStatus,list,strLocked);
					if(!Ret_pStatus.IsOK()) return;

					if (nOperation) //Adding new assignment, make sure is not duplicate
					{
						DbRecordSet lstData;
						QString strWhere=" WHERE BNMR_TABLE_KEY_ID_1 ="+QVariant(nCE_ID).toString(); 
						strWhere +=" AND "+QString(NMRX_CE_CONTACT_WHERE);
						NMRXOrm.Read(Ret_pStatus,lstData,strWhere);
						if(!Ret_pStatus.IsOK()) 
						{
							Status err;
							Locker.UnLock(err,strLocked);
							return;
						}

						int nRow=lstData.find(lstData.getColumnIdx("BNMR_TABLE_KEY_ID_2"),nAssignedID,true);
						if (nRow!=-1)
						{
							Status err;
							Locker.UnLock(err,strLocked);
							continue;
						}

						lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION));
						lstData.addRow();
						lstData.setData(0, "BNMR_TABLE_KEY_ID_1", nCE_ID);
						lstData.setData(0, "BNMR_TABLE_KEY_ID_2", nAssignedID);
						lstData.setData(0, "BNMR_SYSTEM_ROLE", nAssignedRole);
						lstData.setColValue("BNMR_TABLE_1",CE_COMM_ENTITY);
						lstData.setColValue("BNMR_TABLE_2",BUS_CM_CONTACT);
						lstData.setColValue("BNMR_TYPE",GlobalConstants::NMRX_TYPE_ASSIGN);

						//write
						NMRXOrm.Write(Ret_pStatus,lstData);
						if(!Ret_pStatus.IsOK()) 
						{
							Status err;
							Locker.UnLock(err,strLocked);
							return;
						}
					}
					else //Removing assignment
					{

						//issue 2535:
						QString strSQL=" DELETE FROM BUS_NMRX_RELATION WHERE BNMR_TABLE_KEY_ID_1 ="+QVariant(nCE_ID).toString()+ " AND BNMR_TABLE_KEY_ID_2="+QVariant(nAssignedID).toString(); 
						strSQL+=" AND "+QString(NMRX_CE_CONTACT_WHERE);
						//QString strSQL=" DELETE FROM ce_contact_link WHERE CELC_COMM_ENTITY_ID ="+QVariant(nCE_ID).toString(); 
						NMRXOrm.GetDbSqlQuery()->Execute(Ret_pStatus,strSQL);
						if(!Ret_pStatus.IsOK()) 
						{
							Status err;
							Locker.UnLock(err,strLocked);
							return;
						}
					}

					//unlock
					Locker.UnLock(Ret_pStatus,strLocked);
					continue;

				}
			}
			break;


		case GlobalConstants::CE_TYPE_EMAIL:
			{
				//read, if found return, if not found insert
				if (nAssignedType) //PROJECT
				{
					//lock
					DbLockList list;
					list<<nCE_ID;
					QString strLocked;
					Locker.Lock(Ret_pStatus,list,strLocked);
					if(!Ret_pStatus.IsOK()) return;

					if (nOperation) //Adding new assignment, make sure is not duplicate
					{
						QString strSQL=" UPDATE bus_email SET BEM_PROJECT_ID="+QVariant(nAssignedID).toString()+" WHERE BEM_ID ="+QVariant(nEntityID).toString(); 
						EmailOrm.GetDbSqlQuery()->Execute(Ret_pStatus,strSQL);
						if(!Ret_pStatus.IsOK()) 
						{
							Status err;
							Locker.UnLock(err,strLocked);
							return;
						}
					}
					else //Removing assignment
					{
						QString strSQL=" UPDATE bus_email SET BEM_PROJECT_ID= NULL WHERE BEM_ID ="+QVariant(nEntityID).toString(); 
						EmailOrm.GetDbSqlQuery()->Execute(Ret_pStatus,strSQL);
						if(!Ret_pStatus.IsOK()) 
						{
							Status err;
							Locker.UnLock(err,strLocked);
							return;
						}
					}

					//unlock
					Locker.UnLock(Ret_pStatus,strLocked);
					continue;

				}
				else //CONTACT
				{
					//lock
					DbLockList list;
					list<<nCE_ID;
					QString strLocked;
					Locker.Lock(Ret_pStatus,list,strLocked);
					if(!Ret_pStatus.IsOK()) return;

					if (nOperation) //Adding new assignment, not if already exists
					{
						DbRecordSet lstData;
						QString strWhere=" WHERE BNMR_TABLE_KEY_ID_1 ="+QVariant(nCE_ID).toString(); 
						strWhere +=" AND "+QString(NMRX_CE_CONTACT_WHERE);
						NMRXOrm.Read(Ret_pStatus,lstData,strWhere);
						if(!Ret_pStatus.IsOK()) 
						{
							Status err;
							Locker.UnLock(err,strLocked);
							return;
						}

						//lstData.Dump();

						if (lstData.getRowCount()>0)
						{

							//if found FROM exit
							int nRow=lstData.find(lstData.getColumnIdx("BNMR_SYSTEM_ROLE"),QVariant(GlobalConstants::CONTACT_LINK_ROLE_FROM).toInt(),true);
							if (nRow!=-1)
							{
								//if already exists with that role, exit
								if (nAssignedRole==GlobalConstants::CONTACT_LINK_ROLE_FROM && nAssignedID==lstData.getDataRef(nRow, "BNMR_TABLE_KEY_ID_2").toInt())
								{
									Status err;
									Locker.UnLock(err,strLocked);
									continue;
								}
							}

							//if found TO exit
							nRow=lstData.find(lstData.getColumnIdx("BNMR_SYSTEM_ROLE"),QVariant(GlobalConstants::CONTACT_LINK_ROLE_TO).toInt(),true);
							if (nRow!=-1)
							{
								//if already exists with that role, exit
								if (nAssignedRole==GlobalConstants::CONTACT_LINK_ROLE_TO && nAssignedID==lstData.getDataRef(nRow, "BNMR_TABLE_KEY_ID_2").toInt())
								{
									Status err;
									Locker.UnLock(err,strLocked);
									continue;
								}
							}

							//if we assign to FROM and other already exists, delete it:
							if (nAssignedRole==GlobalConstants::CONTACT_LINK_ROLE_FROM)
							{
								lstData.find(lstData.getColumnIdx("BNMR_SYSTEM_ROLE"),QVariant(GlobalConstants::CONTACT_LINK_ROLE_FROM).toInt());
								lstData.deleteUnSelectedRows();

								//remove first:
								//nRow=lstData.find(lstData.getColumnIdx("CELC_CONTACT_ID"),nAssignedID,true);
								if (lstData.getRowCount()>0)
								{
									QString strSQL=" DELETE FROM BUS_NMRX_RELATION WHERE BNMR_ID ="+lstData.getDataRef(0, "BNMR_ID").toString(); 
									NMRXOrm.GetDbSqlQuery()->Execute(Ret_pStatus,strSQL);
									if(!Ret_pStatus.IsOK()) 
									{
										Status err;
										Locker.UnLock(err,strLocked);
										return;
									}
								}
							}

						}



						lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION));
						lstData.addRow();
						lstData.setData(0, "BNMR_TABLE_KEY_ID_1", nCE_ID);
						lstData.setData(0, "BNMR_TABLE_KEY_ID_2", nAssignedID);
						lstData.setData(0, "BNMR_SYSTEM_ROLE", nAssignedRole);
						lstData.setColValue("BNMR_TABLE_1",CE_COMM_ENTITY);
						lstData.setColValue("BNMR_TABLE_2",BUS_CM_CONTACT);
						lstData.setColValue("BNMR_TYPE",GlobalConstants::NMRX_TYPE_ASSIGN);

						//write
						NMRXOrm.Write(Ret_pStatus,lstData);
						if(!Ret_pStatus.IsOK()) 
						{
							Status err;
							Locker.UnLock(err,strLocked);
							return;
						}
					}
					else //Removing assignment //it will be forbiden
					{
						QString strSQL=" DELETE FROM BUS_NMRX_RELATION WHERE BNMR_TABLE_KEY_ID_1 ="+QVariant(nCE_ID).toString()+ " AND BNMR_TABLE_KEY_ID_2="+QVariant(nAssignedID).toString(); 
						strSQL+=" AND "+QString(NMRX_CE_CONTACT_WHERE);
						//QString strSQL=" DELETE FROM ce_contact_link WHERE CELC_COMM_ENTITY_ID ="+QVariant(nCE_ID).toString(); 
						NMRXOrm.GetDbSqlQuery()->Execute(Ret_pStatus,strSQL);
						if(!Ret_pStatus.IsOK()) 
						{
							Status err;
							Locker.UnLock(err,strLocked);
							return;
						}
					}

					//unlock
					Locker.UnLock(Ret_pStatus,strLocked);
					continue;

				}
		
			}
			break;

		case GlobalConstants::CE_TYPE_VOICE_CALL:
			{
				//read, if found return, if not found insert
				if (nAssignedType) //PROJECT
				{
					//lock
					DbLockList list;
					list<<nCE_ID;
					QString strLocked;
					Locker.Lock(Ret_pStatus,list,strLocked);
					if(!Ret_pStatus.IsOK()) return;

					if (nOperation) //Adding new assignment, 
					{
						QString strSQL=" UPDATE bus_voicecalls SET BVC_PROJECT_ID="+QVariant(nAssignedID).toString()+" WHERE BVC_ID ="+QVariant(nEntityID).toString(); 
						
						VoiceOrm.GetDbSqlQuery()->Execute(Ret_pStatus,strSQL);
						if(!Ret_pStatus.IsOK()) 
						{
							Status err;
							Locker.UnLock(err,strLocked);
							return;
						}
					}
					else //Removing assignment
					{
						QString strSQL=" UPDATE bus_voicecalls SET BVC_PROJECT_ID= NULL WHERE BVC_ID ="+QVariant(nEntityID).toString(); 
						VoiceOrm.GetDbSqlQuery()->Execute(Ret_pStatus,strSQL);
						if(!Ret_pStatus.IsOK()) 
						{
							Status err;
							Locker.UnLock(err,strLocked);
							return;
						}
					}

					//unlock
					Locker.UnLock(Ret_pStatus,strLocked);
					continue;
				}
				else //CONTACT
				{
					//lock
					DbLockList list;
					list<<nCE_ID;
					QString strLocked;
					Locker.Lock(Ret_pStatus,list,strLocked);
					if(!Ret_pStatus.IsOK()) return;

					if (nOperation) //Adding new assignment,
					{
						QString strSQL=" UPDATE bus_voicecalls SET BVC_CONTACT_ID="+QVariant(nAssignedID).toString()+" WHERE BVC_ID ="+QVariant(nEntityID).toString(); 

						VoiceOrm.GetDbSqlQuery()->Execute(Ret_pStatus,strSQL);
						if(!Ret_pStatus.IsOK()) 
						{
							Status err;
							Locker.UnLock(err,strLocked);
							return;
						}
					}
					else //Removing assignment
					{
						QString strSQL=" UPDATE bus_voicecalls SET BVC_CONTACT_ID= NULL WHERE BVC_ID ="+QVariant(nEntityID).toString(); 
						VoiceOrm.GetDbSqlQuery()->Execute(Ret_pStatus,strSQL);
						if(!Ret_pStatus.IsOK()) 
						{
							Status err;
							Locker.UnLock(err,strLocked);
							return;
						}
					}

					//unlock
					Locker.UnLock(Ret_pStatus,strLocked);
					continue;

				}
			}

		}

	}


	//Ret_pStatus=err;

}

bool Service_BusCommunication::GetPersonFilterSettingBool(int nSettingID, DbRecordSet &recFilter)
{
	//recFilter.Dump();
	bool bSettingValue;
	if(recFilter.find("BUSCS_SETTING_ID", nSettingID, false, false, true) < 1)
		return false;
	else
		bSettingValue = recFilter.getSelectedRecordSet().getDataRef(0, "BUSCS_VALUE").toBool();

	//recFilter.getSelectedRecordSet().Dump();

	return bSettingValue;
}

int Service_BusCommunication::GetPersonFilterSettingInt(int nSettingID, DbRecordSet &recFilter)
{
	int nSettingValue;
	if(recFilter.find("BUSCS_SETTING_ID", nSettingID, false, false, true) < 1)
		return -1;
	else
		nSettingValue = recFilter.getSelectedRecordSet().getDataRef(0, "BUSCS_VALUE").toInt();

	return nSettingValue;
}

QString Service_BusCommunication::GetPersonFilterSettingString(int nSettingID, DbRecordSet &recFilter)
{
	QString strSettingValue;
	if(recFilter.find("BUSCS_SETTING_ID", nSettingID, false, false, true) < 1)
		return strSettingValue;
	else
		strSettingValue = recFilter.getSelectedRecordSet().getDataRef(0, "BUSCS_VALUE_STRING").toString();

	return strSettingValue;
}

QDateTime Service_BusCommunication::GetPersonFilterSettingDateTime(int nSettingID, DbRecordSet &recFilter)
{
	QDateTime datSettingValue;
	if(recFilter.find("BUSCS_SETTING_ID", nSettingID, false, false, true) < 1)
		return datSettingValue;
	else
		datSettingValue = recFilter.getSelectedRecordSet().getDataRef(0, "BUSCS_VALUE_DATETIME").toDateTime();

	return datSettingValue;
}

DbRecordSet Service_BusCommunication::GetPersonFilterSettingRecordSet(int nSettingID, DbRecordSet &recFilter)
{
	DbRecordSet recSettingValue;
	if(recFilter.find("BUSCS_SETTING_ID", nSettingID, false, false, true) < 1)
		return recSettingValue;
	else{
		QByteArray ar = recFilter.getSelectedRecordSet().getDataRef(0, "BUSCS_VALUE_BYTE").toByteArray();
		recSettingValue = XmlUtil::ConvertByteArray2RecordSet_Fast(ar);
	}

	return recSettingValue;
}

//Check for double rows and add only that are not doubled.
void Service_BusCommunication::CheckForDoubleEntities(DbRecordSet &RetOut_RecordSet, DbRecordSet &tmpRecordSet)
{
	//Check for double rows - if not double merge it.
	int nRowCount = tmpRecordSet.getRowCount();
	for (int i = 0; i < nRowCount; ++i)
	{
		int nCENT_ID = tmpRecordSet.getDataRef(i, "CENT_ID").toInt();
		if (!RetOut_RecordSet.find("CENT_ID", nCENT_ID))
			RetOut_RecordSet.merge(tmpRecordSet.getRow(i));
	}
}

void Service_BusCommunication::CheckForDoubleEntitiesInContacts(DbRecordSet &RetOut_RecordSet)
{
	RetOut_RecordSet.clearSelection();
	QList<int> lstIDs;
	int nRowCount = RetOut_RecordSet.getRowCount();
	for (int i = 0; i < nRowCount; i++)
	{
		int nCENT_ID = RetOut_RecordSet.getDataRef(i, 0).toInt();
		if(lstIDs.contains(nCENT_ID))
			RetOut_RecordSet.selectRow(i);
		else
			lstIDs << nCENT_ID;
	}
	RetOut_RecordSet.deleteSelectedRows();
}

void Service_BusCommunication::ReadCEMenuData(Status &Ret_pStatus, int nPersonID, DbRecordSet &Ret_DocTemplate, DbRecordSet &Ret_DocApps, DbRecordSet &Ret_DocCheckOut,DbRecordSet &Ret_EmailTemplate)
{
	//Reserve connection.
	DbConnectionReserver conn(Ret_pStatus,GetDbManager()); //reserve global connection
	if(!Ret_pStatus.IsOK())return;

	//B.T. to fix: no more private
	QString strWhere =" WHERE BDMD_TEMPLATE_FLAG=1"; // AND CENT_OWNER_ID="+QVariant(nPersonID).toString()+" OR (CENT_OWNER_ID <> "+QVariant(nPersonID).toString()+" AND CENT_IS_PRIVATE =0 AND BDMD_TEMPLATE_FLAG=1)";
	g_AccessRight->SQLFilterRecords(BUS_DM_DOCUMENTS,strWhere);
	g_BusinessServiceSet->ClientSimpleORM->Read(Ret_pStatus,BUS_DM_DOCUMENTS,Ret_DocTemplate,strWhere,DbSqlTableView::TVIEW_BUS_DM_DOCUMENTS_COMM_ENTITY,false);
	if(!Ret_pStatus.IsOK()) return;

	g_BusinessServiceSet->ClientSimpleORM->Read(Ret_pStatus,BUS_DM_APPLICATIONS,Ret_DocApps,"",-1,false);
	if(!Ret_pStatus.IsOK()) return;

	strWhere =" WHERE BDMD_IS_CHECK_OUT=1 AND BDMD_CHECK_OUT_USER_ID="+QVariant(nPersonID).toString();
	g_AccessRight->SQLFilterRecords(BUS_DM_DOCUMENTS,strWhere);
	g_BusinessServiceSet->ClientSimpleORM->Read(Ret_pStatus,BUS_DM_DOCUMENTS,Ret_DocCheckOut,strWhere,-1,false);
	if(!Ret_pStatus.IsOK()) return;

	//B.T. to fix: no more private
	strWhere =" WHERE BEM_TEMPLATE_FLAG=1"; //AND CENT_OWNER_ID="+QVariant(nPersonID).toString()+" OR (CENT_OWNER_ID <> "+QVariant(nPersonID).toString()+" AND CENT_IS_PRIVATE =0 AND BEM_TEMPLATE_FLAG=1)";
	g_AccessRight->SQLFilterRecords(BUS_EMAIL,strWhere);
	g_BusinessServiceSet->ClientSimpleORM->Read(Ret_pStatus,BUS_EMAIL,Ret_EmailTemplate,strWhere,DbSqlTableView::TVIEW_BUS_EMAIL_TEMPLATES,false);
}



//Assigns comm data: <entity, data> to contact
void Service_BusCommunication::AssignCommDataToContact(Status &Ret_pStatus, DbRecordSet &Data, int nContactID)
{

	DbSimpleOrm TableOrm_NMRX_CONTACT_LINK(Ret_pStatus, BUS_NMRX_RELATION, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	//lock all entities:
	QString strLockRes;
	BusinessLocker Locker(Ret_pStatus, CE_COMM_ENTITY, GetDbManager());
	if(!Ret_pStatus.IsOK()) return;


	//get lists:
	DbRecordSet lstEmails,lstVC,lstDocs;
	int nRow=Data.find(0,QVariant(ENTITY_BUS_EMAILS).toInt(),true);
	if (nRow!=-1)
	{
		lstEmails=Data.getDataRef(nRow,1).value<DbRecordSet>();
	}
	nRow=Data.find(0,QVariant(ENTITY_BUS_VOICECALLS).toInt(),true);
	if (nRow!=-1)
	{
		lstVC=Data.getDataRef(nRow,1).value<DbRecordSet>();
	}
	nRow=Data.find(0,QVariant(ENTITY_BUS_DM_DOCUMENTS).toInt(),true);
	if (nRow!=-1)
	{
		lstDocs=Data.getDataRef(nRow,1).value<DbRecordSet>();
	}

	//----------------------
	//			LOCK
	//----------------------

	DbLockList lstLock;
	int nSize=lstEmails.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		lstLock<<lstEmails.getDataRef(i,"CENT_ID").toInt();
	}
	nSize=lstVC.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		lstLock<<lstVC.getDataRef(i,"CENT_ID").toInt();
	}
	nSize=lstDocs.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		lstLock<<lstDocs.getDataRef(i,"CENT_ID").toInt();
	}

	Locker.Lock(Ret_pStatus,lstLock,strLockRes);
	if(!Ret_pStatus.IsOK()) return;



	//Read previous assignment for given contact:
	DbRecordSet lstPrevLink;

	QString strWhere="WHERE BNMR_TABLE_KEY_ID_2= "+QVariant(nContactID).toString()+" AND "+NMRX_CE_CONTACT_WHERE;
	TableOrm_NMRX_CONTACT_LINK.Read(Ret_pStatus,lstPrevLink,strWhere);
	if(!Ret_pStatus.IsOK())
	{
		Status err;
		Locker.UnLock(err,strLockRes);
		return;
	}
	

	//lstPrevLink.Dump();

	//----------------------
	//			DOCS
	//----------------------

	DbRecordSet lstContactLink;
	//lstDocs.Dump();

	//for docs, use CELC link
	if (lstDocs.getRowCount()>0)
	{
		lstContactLink.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION));
		int nSize=lstDocs.getRowCount();
		for(int i=0;i<nSize;++i)
		{
			int nCentID=lstDocs.getDataRef(i,"CENT_ID").toInt();
			if (lstPrevLink.find("BNMR_TABLE_KEY_ID_1",nCentID,true)!=-1) //if already assigned, skip it
				continue;

			lstContactLink.addRow();
			int nLastRow=lstContactLink.getRowCount()-1;

			lstContactLink.setData(nLastRow,"BNMR_TABLE_KEY_ID_1",lstDocs.getDataRef(i,"CENT_ID").toInt());
			lstContactLink.setData(nLastRow,"BNMR_TABLE_KEY_ID_2",nContactID);
			lstContactLink.setData(nLastRow,"BNMR_SYSTEM_ROLE",GlobalConstants::CONTACT_LINK_ROLE_DOC_OWNER);	//role=undefined
		}

		lstContactLink.setColValue("BNMR_TABLE_1",CE_COMM_ENTITY);
		lstContactLink.setColValue("BNMR_TABLE_2",BUS_CM_CONTACT);
		lstContactLink.setColValue("BNMR_TYPE",GlobalConstants::NMRX_TYPE_ASSIGN);
		//lstContactLink.Dump();

		TableOrm_NMRX_CONTACT_LINK.Write(Ret_pStatus,lstContactLink);
		if(!Ret_pStatus.IsOK())
		{
			Status err;
			Locker.UnLock(err,strLockRes);
			return;
		}

	}


	//UNLOCK
	Locker.UnLock(Ret_pStatus,strLockRes);



	//MB: do not support for now other entities:

	//for VC, use BVC_CONTACT_ID field

	
	//for Email, use CELC link

}



//Assigns comm data: <entity, data> to contact
void Service_BusCommunication::AssignCommDataToProject(Status &Ret_pStatus, DbRecordSet &Data, int nProjectID)
{

	DbSimpleOrm TableOrm_NMRX_PROJECT_LINK(Ret_pStatus, BUS_NMRX_RELATION, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	//lock all entities:
	QString strLockRes;
	BusinessLocker Locker(Ret_pStatus, CE_COMM_ENTITY, GetDbManager());
	if(!Ret_pStatus.IsOK()) return;


	//get lists:
	DbRecordSet lstEmails,lstVC,lstDocs;
	int nRow=Data.find(0,QVariant(ENTITY_BUS_EMAILS).toInt(),true);
	if (nRow!=-1)
	{
		lstEmails=Data.getDataRef(nRow,1).value<DbRecordSet>();
	}
	nRow=Data.find(0,QVariant(ENTITY_BUS_VOICECALLS).toInt(),true);
	if (nRow!=-1)
	{
		lstVC=Data.getDataRef(nRow,1).value<DbRecordSet>();
	}
	nRow=Data.find(0,QVariant(ENTITY_BUS_DM_DOCUMENTS).toInt(),true);
	if (nRow!=-1)
	{
		lstDocs=Data.getDataRef(nRow,1).value<DbRecordSet>();
	}

	//----------------------
	//			LOCK
	//----------------------

	DbLockList lstLock;
	int nSize=lstEmails.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		lstLock<<lstEmails.getDataRef(i,"CENT_ID").toInt();
	}
	nSize=lstVC.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		lstLock<<lstVC.getDataRef(i,"CENT_ID").toInt();
	}
	nSize=lstDocs.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		lstLock<<lstDocs.getDataRef(i,"CENT_ID").toInt();
	}

	Locker.Lock(Ret_pStatus,lstLock,strLockRes);
	if(!Ret_pStatus.IsOK()) return;



	//Read previous assignment for given project:
	DbRecordSet lstPrevLink;
	QString strWhere="WHERE BNMR_TABLE_KEY_ID_2= "+QVariant(nProjectID).toString()+" AND "+NMRX_CE_PROJECT_WHERE;
	TableOrm_NMRX_PROJECT_LINK.Read(Ret_pStatus,lstPrevLink,strWhere);
	if(!Ret_pStatus.IsOK())
	{
		Status err;
		Locker.UnLock(err,strLockRes);
		return;
	}


	//----------------------
	//			DOCS
	//----------------------

	DbRecordSet lstProjectLink;

	//for docs, use CELP link
	if (lstDocs.getRowCount()>0)
	{
		lstProjectLink.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION));
		int nSize=lstDocs.getRowCount();
		for(int i=0;i<nSize;++i)
		{
			int nCentID=lstDocs.getDataRef(i,"CENT_ID").toInt();
			if (lstPrevLink.find("BNMR_TABLE_KEY_ID_1",nCentID,true)!=-1) //if already assigned, skip it
				continue;

			lstProjectLink.addRow();
			int nLastRow=lstProjectLink.getRowCount()-1;
			lstProjectLink.setData(nLastRow,"BNMR_TABLE_KEY_ID_1",lstDocs.getDataRef(i,"CENT_ID").toInt());
			lstProjectLink.setData(nLastRow,"BNMR_TABLE_KEY_ID_2",nProjectID);
		}
	}

	lstProjectLink.setColValue("BNMR_TABLE_1",CE_COMM_ENTITY);
	lstProjectLink.setColValue("BNMR_TABLE_2",BUS_PROJECT);
	lstProjectLink.setColValue("BNMR_TYPE",GlobalConstants::NMRX_TYPE_ASSIGN);

	//BEGIN TRANSACTION
	TableOrm_NMRX_PROJECT_LINK.GetDbSqlQuery()->BeginTransaction(Ret_pStatus);
	if(!Ret_pStatus.IsOK())
	{
		Status err;
		Locker.UnLock(err,strLockRes);
		return;
	}


	//WRITE PROJECT LINK for Emails,Docs
	if (lstProjectLink.getRowCount()>0)
	{
		TableOrm_NMRX_PROJECT_LINK.Write(Ret_pStatus,lstProjectLink);
		if(!Ret_pStatus.IsOK())
		{
			TableOrm_NMRX_PROJECT_LINK.GetDbSqlQuery()->Rollback();
			Status err;
			Locker.UnLock(err,strLockRes);
			return;
		}
	}


	//----------------------
	//			EMAIL
	//----------------------
	//for Email, use CELP link
	nSize=lstEmails.getRowCount();
	for(int i=0;i<nSize;++i)
	{

		//update each record (already locked):
		QString strSQL="UPDATE bus_email SET BEM_PROJECT_ID="+QVariant(nProjectID).toString() +" WHERE BEM_ID="+lstEmails.getDataRef(i,"BEM_ID").toString();
		TableOrm_NMRX_PROJECT_LINK.GetDbSqlQuery()->Execute(Ret_pStatus,strSQL);
		if(!Ret_pStatus.IsOK())
		{
			TableOrm_NMRX_PROJECT_LINK.GetDbSqlQuery()->Rollback();
			Status err;
			Locker.UnLock(err,strLockRes);
			return;
		}

	}


	//----------------------
	//			Phones
	//----------------------
	nSize=lstVC.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		//update each record (already locked):
		QString strSQL="UPDATE bus_voicecalls SET BVC_PROJECT_ID="+QVariant(nProjectID).toString() +" WHERE BVC_ID="+lstVC.getDataRef(i,"BVC_ID").toString();
		TableOrm_NMRX_PROJECT_LINK.GetDbSqlQuery()->Execute(Ret_pStatus,strSQL);
		if(!Ret_pStatus.IsOK())
		{
			TableOrm_NMRX_PROJECT_LINK.GetDbSqlQuery()->Rollback();
			Status err;
			Locker.UnLock(err,strLockRes);
			return;
		}
	}

	//COMMIT
	TableOrm_NMRX_PROJECT_LINK.GetDbSqlQuery()->Commit(Ret_pStatus);
	Status err;
	Locker.UnLock(err,strLockRes);
}



	
bool Service_BusCommunication::StringContainsText(QString strStringToSearch, QString strSearchText)
{
	if (strStringToSearch.contains(strSearchText, Qt::CaseInsensitive))
		return true;
	else	
		return false;
}

bool Service_BusCommunication::HtmlContainsText(QString strHtmlToSearch, QString strSearchText)
{
	QString strTmp;
	
	strTmp = DataHelper::ExtractTextFromHTML(strHtmlToSearch);

	if (strTmp.contains(strSearchText, Qt::CaseInsensitive))
		return true;
	else	
		return false;
}

void Service_BusCommunication::FilterEmailsBySearchText(DbRecordSet &RetOut_recEmails, DbRecordSet recFilter)
{
	RetOut_recEmails.clearSelection();
	QString strText = GetPersonFilterSettingString(SEARCH_TEXT, recFilter);
	if (!strText.isEmpty())
	{
		int nEmailsRowCount = RetOut_recEmails.getRowCount();
		for (int i = 0; i < nEmailsRowCount; ++i)
		{
			bool bCheckName = GetPersonFilterSettingBool(SEARCH_IN_NAME, recFilter);
			bool bCheckDesc = GetPersonFilterSettingBool(SEARCH_IN_DESCRIPTION, recFilter);
			//Check to see is that word found in email subject.
			QString str = RetOut_recEmails.getDataRef(i, "BEM_SUBJECT").toString();
			if(bCheckName && StringContainsText(str, strText))
			{
				RetOut_recEmails.selectRow(i);
				continue;
			}
			//Check to see is that word found in task subject.
			str = RetOut_recEmails.getDataRef(i, "BTKS_SUBJECT").toString();
			if(bCheckName && StringContainsText(str, strText))
			{
				RetOut_recEmails.selectRow(i);
				continue;
			}
			//Check to see is that word found in email body.
			str = RetOut_recEmails.getDataRef(i, "BEM_BODY").toString();
			if(bCheckDesc && HtmlContainsText(str, strText))
			{
				RetOut_recEmails.selectRow(i);
				continue;
			}
			//Check to see is that word found in task descr.
			str = RetOut_recEmails.getDataRef(i, "BTKS_DESCRIPTION").toString();
			if(bCheckDesc && HtmlContainsText(str, strText))
			{
				RetOut_recEmails.selectRow(i);
				continue;
			}
		}
	}

	RetOut_recEmails.deleteUnSelectedRows();
	RetOut_recEmails.clearSelection();
}

void Service_BusCommunication::FilterDocumentsBySearchText(DbRecordSet &RetOut_recDocuments, DbRecordSet recFilter)
{
	RetOut_recDocuments.clearSelection();
	QString strText = GetPersonFilterSettingString(SEARCH_TEXT, recFilter);
	if (!strText.isEmpty())
	{
		int nDocsRowCount = RetOut_recDocuments.getRowCount();
		for (int i = 0; i < nDocsRowCount; ++i)
		{
			bool bCheckName = GetPersonFilterSettingBool(SEARCH_IN_NAME, recFilter);
			bool bCheckDesc = GetPersonFilterSettingBool(SEARCH_IN_DESCRIPTION, recFilter);
			//Check to see is that word found in email subject.
			QString str = RetOut_recDocuments.getDataRef(i, "BDMD_NAME").toString();
			if(bCheckName && StringContainsText(str, strText))
			{
				RetOut_recDocuments.selectRow(i);
				continue;
			}
			//Check to see is that word found in task subject.
			str = RetOut_recDocuments.getDataRef(i, "BTKS_SUBJECT").toString();
			if(bCheckName && StringContainsText(str, strText))
			{
				RetOut_recDocuments.selectRow(i);
				continue;
			}
			//Check to see is that word found in email body.
			str = RetOut_recDocuments.getDataRef(i, "BDMD_DESCRIPTION").toString();
			if(bCheckDesc && HtmlContainsText(str, strText))
			{
				RetOut_recDocuments.selectRow(i);
				continue;
			}
			//Check to see is that word found in task descr.
			str = RetOut_recDocuments.getDataRef(i, "BTKS_DESCRIPTION").toString();
			if(bCheckDesc && HtmlContainsText(str, strText))
			{
				RetOut_recDocuments.selectRow(i);
				continue;
			}
		}
	}

	RetOut_recDocuments.deleteUnSelectedRows();
	RetOut_recDocuments.clearSelection();
}

void Service_BusCommunication::FilterVoiceCallBySearchText(DbRecordSet &RetOut_recVoiceCalls, DbRecordSet recFilter)
{
	RetOut_recVoiceCalls.clearSelection();
	QString strText = GetPersonFilterSettingString(SEARCH_TEXT, recFilter);
	if (!strText.isEmpty())
	{
		int nVoiceCallsRowCount = RetOut_recVoiceCalls.getRowCount();
		for (int i = 0; i < nVoiceCallsRowCount; ++i)
		{
			bool bCheckName = GetPersonFilterSettingBool(SEARCH_IN_NAME, recFilter);
			bool bCheckDesc = GetPersonFilterSettingBool(SEARCH_IN_DESCRIPTION, recFilter);
			//Check to see is that word found in email subject.
			QString str = RetOut_recVoiceCalls.getDataRef(i, "BVC_CALLER_ID").toString();
			if(bCheckName && StringContainsText(str, strText))
			{
				RetOut_recVoiceCalls.selectRow(i);
				continue;
			}
			//Check to see is that word found in task subject.
			str = RetOut_recVoiceCalls.getDataRef(i, "BTKS_SUBJECT").toString();
			if(bCheckName && StringContainsText(str, strText))
			{
				RetOut_recVoiceCalls.selectRow(i);
				continue;
			}
			//Check to see is that word found in email body.
			str = RetOut_recVoiceCalls.getDataRef(i, "BVC_DESCRIPTION").toString();
			if(bCheckDesc && HtmlContainsText(str, strText))	
			{
				RetOut_recVoiceCalls.selectRow(i);
				continue;
			}
			//Check to see is that word found in task descr.
			str = RetOut_recVoiceCalls.getDataRef(i, "BTKS_DESCRIPTION").toString();
			if(bCheckDesc && HtmlContainsText(str, strText))
			{
				RetOut_recVoiceCalls.selectRow(i);
				continue;
			}
		}
	}

	RetOut_recVoiceCalls.deleteUnSelectedRows();
	RetOut_recVoiceCalls.clearSelection();
}

void Service_BusCommunication::TaskDateFilter(QDateTime &datFrom, QDateTime &datTo, DbRecordSet &recTasks)
{
	//qDebug()<<datFrom;
	//qDebug()<<datTo;


	recTasks.clearSelection();
	int nRowCount = recTasks.getRowCount();
	for (int i = 0; i < nRowCount; ++i)
	{
		QDateTime datStart, datDue, datCompleted;
		datStart	= recTasks.getDataRef(i, "BTKS_START").toDateTime();
		datDue		= recTasks.getDataRef(i, "BTKS_DUE").toDateTime();
		datCompleted= recTasks.getDataRef(i, "BTKS_COMPLETED").toDateTime();

		//qDebug()<<datStart;
		//qDebug()<<datDue;
		//qDebug()<<datCompleted;

		if(!datCompleted.isNull())
		{
			if (!(datFrom <= datCompleted &&  datCompleted <= datTo)) 
				recTasks.selectRow(i);
		}
		else if(datStart.isNull())
		{
			if (!datDue.isNull())
				if (datDue < datFrom)
					recTasks.selectRow(i);
		}
		else if(datDue.isNull())
		{
			if(datStart > datTo)
				recTasks.selectRow(i);
		}
		else
		{
			//if (datDue < datFrom && datStart > datTo)
			if (datStart > datTo) //issue 1070
				recTasks.selectRow(i);
		}
	}
	recTasks.deleteSelectedRows();
}

//Returns only 250 characters of email body to grid for tooltip.
void Service_BusCommunication::StripEmailBodyToNCharacters(DbRecordSet &RetOut_recEmails, int nCharsNumber /*= 250*/)
{
	//BT: CE_TOOLTIP is now filled right at insert email
	return;

	/*
	int nRowCount = RetOut_recEmails.getRowCount();
	for (int i = 0; i < nRowCount; i++)
	{
		QString strBody = RetOut_recEmails.getDataRef(i, "BEM_BODY").toString();
		//If html - BEM_EMAIL_TYPE = 1, if text BEM_EMAIL_TYPE = 0.
		if (RetOut_recEmails.getDataRef(i, "BEM_EMAIL_TYPE").toInt())
		{
			strBody = DataHelper::ExtractTextFromHTML(strBody);
			strBody = strBody.left(nCharsNumber);
			strBody = strBody.remove("&nbsp;", Qt::CaseInsensitive);
			strBody = strBody.remove("&#8217;", Qt::CaseInsensitive);
			strBody = strBody.remove("&#8230;", Qt::CaseInsensitive);
		}
		else
		{
			strBody = strBody.left(nCharsNumber);
		}

		//Put body back to recordset.
		RetOut_recEmails.setData(i, "BEM_BODY", strBody);
	}
	*/
}

void Service_BusCommunication::StripDocumentBodyToNCharacters(DbRecordSet &RetOut_recDocuments, int nCharsNumber /*= 3072*/)
{
	int nRowCount = RetOut_recDocuments.getRowCount();
	for (int i = 0; i < nRowCount; i++)
	{
		QString strBody = RetOut_recDocuments.getDataRef(i, "BDMD_DESCRIPTION").toString();
		//If note document strip 3072 characters.
		if (RetOut_recDocuments.getDataRef(i, "BDMD_DOC_TYPE").toInt()==3)
		{
			strBody = DataHelper::ExtractTextFromHTML(strBody);
			strBody = strBody.left(nCharsNumber);
			strBody = strBody.remove("&nbsp;", Qt::CaseInsensitive);
			strBody = strBody.remove("&#8217;", Qt::CaseInsensitive);
			strBody = strBody.remove("&#8230;", Qt::CaseInsensitive);
			//Put description back to recordset.
			RetOut_recDocuments.setData(i, "BDMD_DESCRIPTION", strBody);
		}
	}
}

QString Service_BusCommunication::GetTaskTypeFilterString(DbRecordSet &recFilter)
{
	if (!GetPersonFilterSettingBool(FILTER_BY_TASK_TYPE_ACTIVE, recFilter))
		return QString();

	QString strTaskSQL;
	int nPriorityFrom = GetPersonFilterSettingInt(TASK_PRIORITY_FROM, recFilter);
	int nPriorityTo = GetPersonFilterSettingInt(TASK_PRIORITY_TO, recFilter);
	DbRecordSet recTaskTypes = GetPersonFilterSettingRecordSet(TASK_TYPE_FILTER, recFilter);

	strTaskSQL = " AND BTKS_PRIORITY >= " + QVariant(nPriorityFrom).toString() + " AND BTKS_PRIORITY <= " + QVariant(nPriorityTo).toString();

	int nRowCount = recTaskTypes.getRowCount();
	if (nRowCount)
	{
		strTaskSQL += " AND BTKS_TASK_TYPE_ID IN (";
		QStringList lst;

		for (int i = 0; i < nRowCount; ++i)
			lst << recTaskTypes.getDataRef(i, "BCMT_ID").toString();

		strTaskSQL += lst.join(",") + ")";
	}

	return strTaskSQL;
}

void Service_BusCommunication::FilterByTaskType(DbRecordSet &recFilter, DbRecordSet &RetOut_RecordSet, bool bDeleteNonTasks /*= false*/)
{
	bool b = GetPersonFilterSettingBool(FILTER_BY_TASK_TYPE_ACTIVE, recFilter);
	if (!GetPersonFilterSettingBool(FILTER_BY_TASK_TYPE_ACTIVE, recFilter) || RetOut_RecordSet.getRowCount()==0)
		return ;

	int nPriorityFrom = GetPersonFilterSettingInt(TASK_PRIORITY_FROM, recFilter);
	int nPriorityTo = GetPersonFilterSettingInt(TASK_PRIORITY_TO, recFilter);
	DbRecordSet recTaskTypes = GetPersonFilterSettingRecordSet(TASK_TYPE_FILTER, recFilter);
	//_DUMP(RetOut_RecordSet);
	//_DUMP(recTaskTypes);

	//Get task type list.
	QList<int> lstTaskTypes;
	int nRowCount = recTaskTypes.getRowCount();
	for (int i = 0; i < nRowCount; ++i)
		lstTaskTypes << recTaskTypes.getDataRef(i, "BCMT_ID").toInt();

	RetOut_RecordSet.clearSelection();
	nRowCount = RetOut_RecordSet.getRowCount();
	for (int i = 0; i < nRowCount; ++i)
	{
		if (!RetOut_RecordSet.getDataRef(i, "BTKS_ID").isNull())
		{
			//Check priority.
			int nBTKS_PRIORITY = RetOut_RecordSet.getDataRef(i, "BTKS_PRIORITY").toInt();
			if (nBTKS_PRIORITY < nPriorityFrom || nBTKS_PRIORITY > nPriorityTo)
				RetOut_RecordSet.selectRow(i);

			//Check task types.
			if (!lstTaskTypes.isEmpty())
			{
				if (RetOut_RecordSet.getDataRef(i, "BTKS_TASK_TYPE_ID").isNull())
					RetOut_RecordSet.selectRow(i);
				
				int nBTKS_TASK_TYPE_ID = RetOut_RecordSet.getDataRef(i, "BTKS_TASK_TYPE_ID").toInt();
				if (!lstTaskTypes.contains(nBTKS_TASK_TYPE_ID))
					RetOut_RecordSet.selectRow(i);
			}
		}
		else if (bDeleteNonTasks)
		{
			RetOut_RecordSet.selectRow(i);
		}
	}

	RetOut_RecordSet.deleteSelectedRows();
}

int Service_BusCommunication::GetEmailAttachmentCount(Status &Ret_pStatus, int nBEM_ID)
{
	DbSqlQuery query(Ret_pStatus, GetDbManager());
	if(!Ret_pStatus.IsOK()) return -1;

	QString strSql = "SELECT COUNT(*) FROM BUS_EMAIL_ATTACHMENT WHERE BEA_EMAIL_ID=? AND ((BEA_CID_LINK IS NULL) OR (BEA_CID_LINK = '')) ";
	query.Prepare(Ret_pStatus, strSql);
	if (!Ret_pStatus.IsOK()) return -1;
	query.bindValue(0, nBEM_ID);
	query.ExecutePrepared(Ret_pStatus);
	if (!Ret_pStatus.IsOK()) return -1;
	query.next();

	return query.GetQSqlQuery()->value(0).toInt();
}

void Service_BusCommunication::AddEmailAttachmentCountField(DbRecordSet &RetOut_recEmails)
{
	//_DUMP(RetOut_recEmails);
	RetOut_recEmails.addColumn(QVariant::Int, "EMAIL_ATTACHMENT_COUNT");
	//_DUMP(RetOut_recEmails);
}

void Service_BusCommunication::CalculateEmailAttachmentCountField(Status &Ret_pStatus, DbRecordSet &RetOut_recEmails)
{
	int nRowCount = RetOut_recEmails.getRowCount();
	for (int i = 0; i < nRowCount; i++)
	{
		int nBEM_ID = RetOut_recEmails.getDataRef(i, "BEM_ID").toInt();
		int nAttachCount = GetEmailAttachmentCount(Ret_pStatus, nBEM_ID);
		RetOut_recEmails.setData(i, "EMAIL_ATTACHMENT_COUNT", nAttachCount);
	}
}


void Service_BusCommunication::CheckIfRecordExists(Status &Ret_pStatus, int nEntityType, int nRecordID, bool &Ret_bExists)
{

	DbSqlQuery query(Ret_pStatus, GetDbManager());
	if(!Ret_pStatus.IsOK()) return;

	Ret_bExists=false;

	QString strSQL;

	switch (nEntityType)
	{
	case ENTITY_BUS_PROJECT:
		strSQL="SELECT COUNT(*) FROM BUS_PROJECTS WHERE BUSP_ID="+QString::number(nRecordID);
		break;
	case ENTITY_BUS_CONTACT:
		strSQL="SELECT COUNT(*) FROM BUS_CM_CONTACT WHERE BCNT_ID="+QString::number(nRecordID);
		break;
	case ENTITY_BUS_DM_DOCUMENTS:
		strSQL="SELECT COUNT(*) FROM BUS_DM_DOCUMENTS WHERE BDMD_ID="+QString::number(nRecordID);
		break;
	case ENTITY_BUS_EMAILS:
		strSQL="SELECT COUNT(*) FROM BUS_EMAIL WHERE BEM_ID="+QString::number(nRecordID);
		break;
	case ENTITY_BUS_VOICECALLS:
		strSQL="SELECT COUNT(*) FROM BUS_VOICECALLS WHERE BVC_ID="+QString::number(nRecordID);
		break;
	}

	if (!strSQL.isEmpty())
	{
		query.Execute(Ret_pStatus,strSQL);
		if (!Ret_pStatus.IsOK())
			return;

		if(query.next())
			Ret_bExists = query.value(0).toInt()? true:false;
	}

}