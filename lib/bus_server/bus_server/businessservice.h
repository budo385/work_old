#ifndef BUSINESSSERVICE_H
#define BUSINESSSERVICE_H

#include "bus_core/bus_core/usersessionmanager.h"
#include "db/db/dbsqlmanager.h"



/*!
    \class BusinessService
    \brief Abstract/empty base class for all business service objects
	\ingroup Bus_Services

	Use:
	- use it as base class for all Business services
*/

class BusinessService
{

public:

	virtual ~BusinessService(){};

	//get set methods for server side objects that every BO need:
	UserSessionManager* GetUserSessionManager(){return m_SessionManager;};
	DbSqlManager* GetDbManager(){return m_DbManager;};

	static void SetUserSessionManager(UserSessionManager* pSessionManager){m_SessionManager=pSessionManager;};
	static void SetDbManager(DbSqlManager* pDbManager){m_DbManager=pDbManager;};
	

	//isolated to enable context free ops:
	QString GetSessionID(){return m_SessionManager->GetSessionID();}
	int GetUserID(){return m_SessionManager->GetUserID();}

protected:
	static UserSessionManager* m_SessionManager;	//< Session manager
	static DbSqlManager *m_DbManager;				//< Db access


};



#endif //BUSINESSSERVICE_H
