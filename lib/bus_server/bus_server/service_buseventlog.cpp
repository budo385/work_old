#include "service_buseventlog.h"
#include "db_core/db_core/dbsqltableview.h"
#include "db/db/dbsimpleorm.h"

/*!
Log event to event logger.

\param pStatus		 - If err occur, returned here
\param EventCode	 - Event code.
\param EventText	 - Event text.
\param EventType	 - Event type.
\param EventSource	 - Returned Recordset with roles.
\param EventDateTime - Returned Recordset with roles.
\param pDbConn		 - Db conn.
*/
void Service_BusEventLog::AddEvent(Status &pStatus, int &EventID, int EventCode, QString EventText, int EvType, int EvCategoryID, 
								   QDateTime EventDateTime /*= NULL*/)
{
	//Define recordset from table view.
	DbRecordSet Recordset;
	Recordset.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CORE_EVENT_LOG));

	//Declare status recordset (has no useful meaning here - just for writing with ORM).
	//DbRecordSet StatusRecordset;

	//Get current date time.
	QDateTime CurrentDateTime = QDateTime::currentDateTime();

	//If EventDateTime is empty then make it current.
	if (!EventDateTime.isValid())
		EventDateTime = CurrentDateTime;

	//Add data to recordset (first two fields are left empty - id and global id).
	Recordset.addRow();
	Recordset.setData(0,3, EventCode);
	Recordset.setData(0,4, EventText);
	Recordset.setData(0,5, EvType);
	Recordset.setData(0,6, EvCategoryID);
	Recordset.setData(0,7, CurrentDateTime);

	//Define ORM for CORE_EVENT_LOG.
	DbSimpleOrm TableOrm(pStatus, CORE_EVENT_LOG, GetDbManager()); 
	if(!pStatus.IsOK()) return;

	//Write to CORE_EVENT_LOG.
	TableOrm.Write(pStatus, Recordset);//, &StatusRecordset);

	EventID = Recordset.getDataRef(0, 0).toInt();
}
/*!
Delete event by event ID.

\param pStatus		- If err occur, returned here
\param EventID		- Event ID.
\param pDbConn		- Db conn.
*/
void Service_BusEventLog::DeleteEventByEventID(Status &pStatus, int EventID /*= NULL*/)
{
	//Declare status recordset (has no useful meaning here - just for writing with ORM).
	//DbRecordSet StatusRecordset;

	//Define ORM for CORE_EVENT_LOG.
	DbSimpleOrm TableOrm(pStatus, CORE_EVENT_LOG, GetDbManager()); 
	if(!pStatus.IsOK()) return;

	//Create delete list.
	QList<int> DeleteList;
	DeleteList << EventID;

	//Delete event.
	TableOrm.DeleteFast(pStatus, DeleteList);//, StatusRecordset);
}

/*!
Delete event older than.

\param pStatus						- If err occur, returned here
\param DateOlderThenGivenDatetime	- Date older than.
\param pDbConn						- Db conn.
*/
void Service_BusEventLog::DeleteEventByDateTime(Status &pStatus, QDateTime DateOlderThenGivenDatetime)
{
	//Make local query, can fail if connection reservation fails.
	DbSqlQuery query(pStatus,GetDbManager());
	if(!pStatus.IsOK()) return;

	//Prepare delete query.
	QString strSQL="DELETE FROM CORE_EVENT_LOG WHERE CREL_EVENT_DATETIME < ?";

	//Prepare statement for execution.
	query.Prepare(pStatus, strSQL);
	//Bind date.
	query.bindValue(0, DateOlderThenGivenDatetime);
	//Execute prepared
	query.ExecutePrepared(pStatus);
}

/*!
Get events by type.

\param pStatus				- If err occur, returned here
\param Identifier			- Event identifier.
\param EventRecordSet		- Event recordset.
\param pDbConn				- Db conn.
*/
void Service_BusEventLog::GetEventRecordsetByType(Status &pStatus, int EvType, DbRecordSet &EventRecordSet)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(pStatus, CORE_EVENT_LOG, GetDbManager()); 
	if(!pStatus.IsOK()) return;

	//Construct WHERE clause.
	QString strWhere = "WHERE ";

	//Append to where clause what it needs.
	strWhere += " CREL_EVENT_TYPE = " + QVariant(EvType).toString();

	//Read.
	TableOrm.Read(pStatus, EventRecordSet, strWhere);
}

/*!
Get events by source.

\param pStatus				- If err occur, returned here
\param Identifier			- Event source.
\param EventRecordSet		- Event recordset.
\param pDbConn				- Db conn.
*/
void Service_BusEventLog::GetEventRecordsetByCategory(Status &pStatus, int EvCategoryID, DbRecordSet &EventRecordSet)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(pStatus, CORE_EVENT_LOG, GetDbManager()); 
	if(!pStatus.IsOK()) return;

	//Construct WHERE clause.
	QString strWhere = " WHERE CREL_EVENT_CATEGORY_ID = " + QVariant(EvCategoryID).toString();

	//Read.
	TableOrm.Read(pStatus, EventRecordSet, strWhere);
}

/*!
Get events by date time interval.

\param pStatus				- If err occur, returned here
\param BeginTime			- Begin time.
\param EndTime				- End time.
\param EventRecordSet		- Event recordset.
\param pDbConn				- Db conn.
*/
void Service_BusEventLog::GetEventRecordsetByDateTime(Status &pStatus, QDateTime BeginTime, QDateTime EndTime, DbRecordSet &EventRecordSet)
{
	//Make local query, can fail if connection reservation fails.
	DbSqlQuery query(pStatus,GetDbManager());
	if(!pStatus.IsOK()) return;

	//Prepare delete query.
	QString strSQL="SELECT * FROM CORE_EVENT_LOG WHERE CREL_EVENT_DATETIME BETWEEN ? AND ?";
	//Prepare statement for execution.
	query.Prepare(pStatus, strSQL);
	//Bind date.
	query.bindValue(0, BeginTime);
	query.bindValue(1, EndTime);
	//Execute prepared
	query.ExecutePrepared(pStatus);

	//If everything went fine fetch records.
	if (pStatus.IsOK())
		query.FetchData(EventRecordSet);
}
