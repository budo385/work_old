#ifndef SERVICE_USERSESSION_H
#define SERVICE_USERSESSION_H


#include "systemservice.h"



/*!
    \class Service_UserSession
    \brief Access to DB User session table
    \ingroup Bus_System

	Provides methods for chaning user session data in DB table
		
*/

class Service_UserSession: public SystemService
{

public:
	

	//read
	void ReadSessionBySessionID(Status &pStatus,QString strSessionID,DbRecordSet& lstSessions);
	void ReadSessionByAppSrvID(Status &pStatus,int nAppServerSessionID,DbRecordSet& lstSessions);
	void ReadActiveSessionByAppSrvID(Status &pStatus,int nAppServerSessionID,DbRecordSet& lstSessions);
	//create
	void InsertSession(Status &pStatus,QString strSessionID,int nAppServerSessionID,int nMaxSessionCount,QString strModuleCode, QString strMLIID,int nUserID,int &nSessionRecordID, int nParentSessionID=0);
	//update to new app.srv.id list of ses id's
	void ActivateSession(Status &pStatus,int nAppServerSessionID,QString strSessionID,int &nSessionRecordID,int nMaxSessionCount);
	void TransferSessions(Status &pStatus,int nAppServerSessionID,int nOldAppServerSessionID);
	void InactivateSessions(Status &pStatus,int nAppServerSessionID,int nSessionRecordID=-1);
	void DeleteInactiveSessions(Status &pStatus,int nAppServerSessionID);

	//delete
	void DeleteSession(Status &pStatus,int nAppServerSessionID,QList<int> lstSessions);
	void DeleteSessionByAppSrvID(Status &pStatus,int nAppServerSessionID);

	//check session
	void CheckSession(Status &pStatus,int nSessionRecordID);

private:
	void CheckLicenseCount(Status &pStatus,int nMaxSessionCount,QString strModuleCode, QString strMLIID);

};


#endif //SERVICE_USERSESSION_H
