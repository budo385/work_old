#include "accessrightsmanipulator.h"

#include "db/db/dbsimpleorm.h"
#include "bus_core/bus_core/accessrightsorganizer.h"
#include "bus_core/bus_core/accessrightset.h"
#include "bus_core/bus_core/accessright.h"

AccessRightsManipulator::AccessRightsManipulator(DbSqlManager *pManager)
{
	m_pManager  = pManager;
}

AccessRightsManipulator::~AccessRightsManipulator()
{
	m_pManager  = NULL;
}

/*!
Insert new role to person.

\param pStatus					- If err occur, returned here
\param PersonID					- Person ID.
\param RoleID					- Role ID.
*/
void AccessRightsManipulator::InsertNewRoleToPerson(Status &pStatus, int PersonID, int RoleID)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(pStatus, BUS_PERSON, m_pManager); 
	if(!pStatus.IsOK()) return;

	//Read BUS_PERSON recordset to update DAT_LAST_MODIFIED.
	DbRecordSet PersonRecordSet;
	QString strWhere = "WHERE BPER_ID = %1";
	TableOrm.Read(pStatus, PersonRecordSet, strWhere.arg(QVariant(PersonID).toString()), DbSqlTableView::TVIEW_BUS_PERSON);
	if(!pStatus.IsOK()) 
		return;

	//--------------------------------------
	//START TRANSACTION.
	//--------------------------------------
	m_pManager->BeginTransaction(pStatus,TableOrm.GetDbConnection());
	if(!pStatus.IsOK()) 
	{
		return;
	}

	//--------------------------------------
	//INSERT ROLE TO PERSON.
	//--------------------------------------
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm CORE_USERROLE_TableOrm(pStatus, BUS_PERSONROLE, m_pManager); 
	if(!pStatus.IsOK()) 
	{
		m_pManager->RollbackTransaction(pStatus,TableOrm.GetDbConnection());
		return;
	}

	//Create RecordSet.
	DbRecordSet RecordSet;
	//Define from view.
	RecordSet.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_PERSONROLE));
	//Insert data.
	RecordSet.insertRow(0);
	RecordSet.setData(0, 3, PersonID);
	RecordSet.setData(0, 4, RoleID);

	//Write to CORE_USERROLE.
	CORE_USERROLE_TableOrm.Write(pStatus, RecordSet);
	if(!pStatus.IsOK()) 
	{
		m_pManager->RollbackTransaction(pStatus,TableOrm.GetDbConnection());
		return;
	}

	//--------------------------------------
	//UPDATE DAT_LAST_MODIFIED TO BUS_PERSON.
	//--------------------------------------
	TableOrm.Write(pStatus, PersonRecordSet);
	if(!pStatus.IsOK()) 
	{
		m_pManager->RollbackTransaction(pStatus,TableOrm.GetDbConnection());
		return;
	}

	//--------------------------------------
	//If all good commit.
	//--------------------------------------
	m_pManager->CommitTransaction(pStatus,TableOrm.GetDbConnection());
}

/*!
Delete role from person.

\param pStatus					- If err occur, returned here
\param PersonID					- Person ID.
\param RoleID					- Role ID.
*/
void AccessRightsManipulator::DeleteRoleFromPerson(Status &pStatus, int PersonID, int RoleID)
{
	//--------------------------------------
	//BUS_PERSONROLE ORM.
	//--------------------------------------
	DbSimpleOrm TableOrm(pStatus, BUS_PERSONROLE, m_pManager); 
	if(!pStatus.IsOK()) 
		return;

	//Read recordset to delete.
	DbRecordSet PersonRoleRecordSet;
	QString strWhere = "WHERE BPRL_PERSON_ID = %1 AND BPRL_ROLE_ID = %2";
	TableOrm.Read(pStatus, PersonRoleRecordSet, strWhere.arg(QVariant(PersonID).toString(), QVariant(RoleID).toString()), DbSqlTableView::TVIEW_BUS_PERSONROLE);
	if(!pStatus.IsOK()) 
		return;

	//--------------------------------------
	//START TRANSACTION.
	//--------------------------------------
	m_pManager->BeginTransaction(pStatus,TableOrm.GetDbConnection());
	if(!pStatus.IsOK()) 
	{
		m_pManager->RollbackTransaction(pStatus,TableOrm.GetDbConnection());
		return;
	}

	//Delete from BUS_PERSONROLE.
	TableOrm.DeleteFast(pStatus, PersonRoleRecordSet);
	if(!pStatus.IsOK())
	{
		m_pManager->RollbackTransaction(pStatus,TableOrm.GetDbConnection());
		return;
	}

	//--------------------------------------
	//Update DAT_LAST_MODIFIED to BUS_PERSON.
	//--------------------------------------
	DbSimpleOrm BUS_PERSON_TableOrm(pStatus, BUS_PERSON, m_pManager); 
	if(!pStatus.IsOK()) 
	{
		m_pManager->RollbackTransaction(pStatus,BUS_PERSON_TableOrm.GetDbConnection());
		return;
	}

	//Read recordset to rewrite.
	DbRecordSet PersonRecordSet;
	//Where clause.
	QString strWhere1 = "WHERE BPER_ID = %1";

	//Read from BUS_PERSON
	BUS_PERSON_TableOrm.Read(pStatus, PersonRecordSet, strWhere1.arg(QVariant(PersonID).toString()));

	//Write back.
	BUS_PERSON_TableOrm.Write(pStatus, PersonRecordSet);
	if(!pStatus.IsOK()) 
	{
		m_pManager->RollbackTransaction(pStatus,BUS_PERSON_TableOrm.GetDbConnection());
		return;
	}

	//--------------------------------------
	//If all good commit.
	//--------------------------------------
	m_pManager->CommitTransaction(pStatus,BUS_PERSON_TableOrm.GetDbConnection());
}

/*!
Insert new role to user.

\param pStatus					- If err occur, returned here
\param UserID					- User ID.
\param RoleID					- Role ID.
*/
void AccessRightsManipulator::InsertNewRoleToUser(Status &pStatus, int UserID, int RoleID)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(pStatus, CORE_USER, m_pManager); 
	if(!pStatus.IsOK()) return;

	//Read CORE_USER recordset to update DAT_LAST_MODIFIED.
	DbRecordSet UserRecordSet;
	QString strWhere = "WHERE CUSR_ID = %1";
	TableOrm.Read(pStatus, UserRecordSet, strWhere.arg(QVariant(UserID).toString()), DbSqlTableView::TVIEW_CORE_USER);
	if(!pStatus.IsOK()) 
		return;

	//--------------------------------------
	//START TRANSACTION.
	//--------------------------------------
	m_pManager->BeginTransaction(pStatus,TableOrm.GetDbConnection());
	if(!pStatus.IsOK()) 
		return;

	//--------------------------------------
	//INSERT ROLE TO PERSON.
	//--------------------------------------
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm CORE_USERROLE_TableOrm(pStatus, CORE_USERROLE, m_pManager); 
	if(!pStatus.IsOK()) 
	{
		m_pManager->RollbackTransaction(pStatus,CORE_USERROLE_TableOrm.GetDbConnection());
		return;
	}

	//Create RecordSet.
	DbRecordSet RecordSet;
	//Define from view.
	RecordSet.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CORE_USERROLE));
	//Insert data.
	RecordSet.insertRow(0);
	RecordSet.setData(0, 3, UserID);
	RecordSet.setData(0, 4, RoleID);

	//Write to CORE_USERROLE.
	CORE_USERROLE_TableOrm.Write(pStatus, RecordSet);
	if(!pStatus.IsOK()) 
	{
		m_pManager->RollbackTransaction(pStatus,CORE_USERROLE_TableOrm.GetDbConnection());
		return;
	}

	//--------------------------------------
	//UPDATE DAT_LAST_MODIFIED TO CORE_USER.
	//--------------------------------------
	TableOrm.Write(pStatus, UserRecordSet);
	if(!pStatus.IsOK()) 
	{
		m_pManager->RollbackTransaction(pStatus,TableOrm.GetDbConnection());
		return;
	}

	//--------------------------------------
	//If all good commit.
	//--------------------------------------
	m_pManager->CommitTransaction(pStatus,TableOrm.GetDbConnection());
}

/*!
Delete role from user.

\param pStatus					- If err occur, returned here
\param UserID					- User ID.
\param RoleID					- Role ID.
*/
void AccessRightsManipulator::DeleteRoleFromUser(Status &pStatus, int UserID, int RoleID)
{
	//--------------------------------------
	//CORE_USERROLE ORM.
	//--------------------------------------
	DbSimpleOrm TableOrm(pStatus, CORE_USERROLE, m_pManager); 
	if(!pStatus.IsOK()) 
		return;

	//Read recordset to delete.
	DbRecordSet PersonRoleRecordSet;
	QString strWhere = "WHERE CURL_USER_ID = %1 AND CURL_ROLE_ID = %2";
	TableOrm.Read(pStatus, PersonRoleRecordSet, strWhere.arg(QVariant(UserID).toString(), QVariant(RoleID).toString()), DbSqlTableView::TVIEW_CORE_USERROLE);
	if(!pStatus.IsOK()) 
		return;

	//--------------------------------------
	//START TRANSACTION.
	//--------------------------------------
	m_pManager->BeginTransaction(pStatus,TableOrm.GetDbConnection());
	if(!pStatus.IsOK()) 
		return;

	//Delete from CORE_USERROLE.
	TableOrm.DeleteFast(pStatus, PersonRoleRecordSet);
	if(!pStatus.IsOK())
	{
		m_pManager->RollbackTransaction(pStatus,TableOrm.GetDbConnection());
		return;
	}

	//--------------------------------------
	//Update DAT_LAST_MODIFIED to CORE_USER.
	//--------------------------------------
	DbSimpleOrm CORE_USER_TableOrm(pStatus, CORE_USER, m_pManager); 
	if(!pStatus.IsOK()) 
	{
		m_pManager->RollbackTransaction(pStatus,CORE_USER_TableOrm.GetDbConnection());
		return;
	}

	//Read recordset to rewrite.
	DbRecordSet PersonRecordSet;
	//Where clause.
	QString strWhere1 = "WHERE CUSR_ID = %1";

	//Read from CORE_USER
	CORE_USER_TableOrm.Read(pStatus, PersonRecordSet, strWhere1.arg(QVariant(UserID).toString()));

	//Write back.
	CORE_USER_TableOrm.Write(pStatus, PersonRecordSet);
	if(!pStatus.IsOK()) 
	{
		m_pManager->RollbackTransaction(pStatus,CORE_USER_TableOrm.GetDbConnection());
		return;
	}

	//--------------------------------------
	//If all good commit.
	//--------------------------------------
	m_pManager->CommitTransaction(pStatus,CORE_USER_TableOrm.GetDbConnection());
}

/*!
Delete role.

\param pStatus	- If err occur, returned here
\param RoleID	- Role ID.
*/
void AccessRightsManipulator::DeleteRole(Status &pStatus, int RoleID)
{
	//This ORM is for later.
	DbSimpleOrm CORE_ROLE_Orm(pStatus, CORE_ROLE, m_pManager); 
	if(!pStatus.IsOK()) 
		return;

	//--------------------------------------
	//START TRANSACTION.
	//--------------------------------------
	m_pManager->BeginTransaction(pStatus, CORE_ROLE_Orm.GetDbConnection());
	if(!pStatus.IsOK()) 
		return;

	//--------------------------------------
	//	DELETE CORE_ROLE.
	//--------------------------------------
	//Delete list
	QList<int> CORE_ROLE_DeleteList;
	CORE_ROLE_DeleteList << RoleID;
	CORE_ROLE_Orm.DeleteFast(pStatus, CORE_ROLE_DeleteList);
	if (!pStatus.IsOK()) 
	{
		//ROLLBACK
		m_pManager->RollbackTransaction(pStatus, CORE_ROLE_Orm.GetDbConnection());
		return;
	}

	//COMMIT
	m_pManager->CommitTransaction(pStatus, CORE_ROLE_Orm.GetDbConnection());
}

/*!
Delete access right set from role.

\param pStatus		- If err occur, returned here
\param RoleID		- Role ID.
\param AccRSetID	- Access right ID.
*/
void AccessRightsManipulator::DeleteAccRSetFromRole(Status &pStatus, int RoleID, int AccRSetID)
{
	//--------------------------------------
	//	DELETE CORE_ACCESSRIGHTS of role.
	//--------------------------------------
	DbSimpleOrm CORE_ACCESSRIGHTS_TableOrm(pStatus, CORE_ACCESSRIGHTS, m_pManager); 
	if (!pStatus.IsOK()) 
		return;

	//--------------------------------------
	//START TRANSACTION.
	//--------------------------------------
	m_pManager->BeginTransaction(pStatus, CORE_ACCESSRIGHTS_TableOrm.GetDbConnection());
	if(!pStatus.IsOK()) 
		return;

	//Where.
	QString CORE_ACCESSRIGHTS_strWhere = "WHERE CAR_ROLE_ID = %1 AND CAR_ACCRSET_ID = %2";

	//CORE_ACCESSRIGHTS recordset.
	DbRecordSet CORE_ACCESSRIGHTS_RecordSet;

	//Read what to delete.
	CORE_ACCESSRIGHTS_TableOrm.Read(pStatus, CORE_ACCESSRIGHTS_RecordSet, CORE_ACCESSRIGHTS_strWhere.arg(QVariant(RoleID).toString(), QVariant(AccRSetID).toString()), DbSqlTableView::TVIEW_CORE_ACCESSRIGHTS);
	if (!pStatus.IsOK()) 
	{
		//ROLLBACK
		m_pManager->RollbackTransaction(pStatus, CORE_ACCESSRIGHTS_TableOrm.GetDbConnection());
		return;
	}

	//Delete from CORE_ACCESSRIGHTS.
	CORE_ACCESSRIGHTS_TableOrm.DeleteFast(pStatus, CORE_ACCESSRIGHTS_RecordSet);
	if (!pStatus.IsOK()) 
	{
		//ROLLBACK
		m_pManager->RollbackTransaction(pStatus, CORE_ACCESSRIGHTS_TableOrm.GetDbConnection());
		return;
	}

	//--------------------------------------
	//	DELETE CORE_ACCRSETROLE of role.
	//--------------------------------------
	DbSimpleOrm CORE_ACCRSETROLE_TableOrm(pStatus, CORE_ACCRSETROLE, m_pManager); 
	if (!pStatus.IsOK()) 
	{
		//ROLLBACK
		m_pManager->RollbackTransaction(pStatus, CORE_ACCESSRIGHTS_TableOrm.GetDbConnection());
		return;
	}

	//Where.
	QString CORE_ACCRSETROLE_strWhere = "WHERE CASTR_ROLE_ID = %1 AND CASTR_ACCRSET_ID = %2";

	//CORE_ACCESSRIGHTS recordset.
	DbRecordSet CORE_ACCRSETROLE_RecordSet;

	//Read what to delete.
	CORE_ACCRSETROLE_TableOrm.Read(pStatus, CORE_ACCRSETROLE_RecordSet, CORE_ACCRSETROLE_strWhere.arg(QVariant(RoleID).toString(), QVariant(AccRSetID).toString()), DbSqlTableView::TVIEW_CORE_ACCRSETROLE);
	if (!pStatus.IsOK()) 
	{
		//ROLLBACK
		m_pManager->RollbackTransaction(pStatus, CORE_ACCESSRIGHTS_TableOrm.GetDbConnection());
		return;
	}

	//Delete from CORE_ACCESSRIGHTS.
	CORE_ACCRSETROLE_TableOrm.DeleteFast(pStatus, CORE_ACCRSETROLE_RecordSet);
	if (!pStatus.IsOK()) 
	{
		//ROLLBACK
		m_pManager->RollbackTransaction(pStatus, CORE_ACCESSRIGHTS_TableOrm.GetDbConnection());
		return;
	}

	//COMMIT
	m_pManager->CommitTransaction(pStatus, CORE_ACCESSRIGHTS_TableOrm.GetDbConnection());
}

/*!
Insert new access right set to role.

\param pStatus		- If err occur, returned here
\param RoleID		- Role ID.
\param AccRSetID	- Access right ID.
*/
void AccessRightsManipulator::InsertNewAccRSetToRole(Status &pStatus, int RoleID, int AccRSetID)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm CORE_ROLE_TableOrm(pStatus, CORE_ROLE, m_pManager); 
	if(!pStatus.IsOK()) return;

	//Read CORE_USER recordset to update DAT_LAST_MODIFIED.
	DbRecordSet CORE_ROLE_RecordSet;
	QString strWhere = "WHERE CROL_ID = %1";
	CORE_ROLE_TableOrm.Read(pStatus, CORE_ROLE_RecordSet, strWhere.arg(QVariant(RoleID).toString()), DbSqlTableView::TVIEW_CORE_ROLE);
	if(!pStatus.IsOK()) 
		return;

	//--------------------------------------
	//START TRANSACTION.
	//--------------------------------------
	m_pManager->BeginTransaction(pStatus, CORE_ROLE_TableOrm.GetDbConnection());
	if(!pStatus.IsOK()) 
	{
		m_pManager->RollbackTransaction(pStatus, CORE_ROLE_TableOrm.GetDbConnection());
		return;
	}

	//--------------------------------------
	//INSERT ACCRSET TO ROLE.
	//--------------------------------------
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm CORE_ACCRSETROLE_TableOrm(pStatus, CORE_ACCRSETROLE, m_pManager); 
	if(!pStatus.IsOK()) 
	{
		m_pManager->RollbackTransaction(pStatus, CORE_ROLE_TableOrm.GetDbConnection());
		return;
	}

	//Create RecordSet.
	DbRecordSet RecordSet;
	//Define from view.
	RecordSet.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CORE_ACCRSETROLE));
	//Insert data.
	RecordSet.insertRow(0);
	RecordSet.setData(0, 3, RoleID);
	RecordSet.setData(0, 4, AccRSetID);

	//Write to CORE_ACCRSETROLE.
	CORE_ACCRSETROLE_TableOrm.Write(pStatus, RecordSet);
	if(!pStatus.IsOK()) 
	{
		m_pManager->RollbackTransaction(pStatus, CORE_ROLE_TableOrm.GetDbConnection());
		return;
	}

	//--------------------------------------
	//UPDATE DAT_LAST_MODIFIED TO CORE_ROLE.
	//--------------------------------------
	CORE_ROLE_TableOrm.Write(pStatus, CORE_ROLE_RecordSet);
	if(!pStatus.IsOK()) 
	{
		m_pManager->RollbackTransaction(pStatus, CORE_ROLE_TableOrm.GetDbConnection());
		return;
	}

	//--------------------------------------
	//INSERT ACCESS RIGHTS.
	//--------------------------------------
	InsertAccessRights(pStatus, RoleID, AccRSetID);
	if(!pStatus.IsOK()) 
	{
		m_pManager->RollbackTransaction(pStatus, CORE_ROLE_TableOrm.GetDbConnection());
		return;
	}

	//--------------------------------------
	//If all good commit.
	//--------------------------------------
	m_pManager->CommitTransaction(pStatus, CORE_ROLE_TableOrm.GetDbConnection());
}

/*!
Insert new role.

\param pStatus			- If err occur, returned here
\param nNewRoleRowID	- Role ID.
\param RoleName			- Role name.
\param RoleDescription	- Role description.
\param RoleType			- Role type.
*/
void AccessRightsManipulator::InsertNewRole(Status &pStatus, int &nNewRoleRowID, QString RoleName, QString RoleDescription, int RoleType)
{
	DbSimpleOrm TableOrm(pStatus, CORE_ROLE, m_pManager); 
	if(!pStatus.IsOK()) 
		return;

	//Read recordset to rewrite.
	DbRecordSet RoleRecordSet;
	RoleRecordSet.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CORE_ROLE));

	//Insert row.
	RoleRecordSet.insertRow(0);
	//Set User Name.
	RoleRecordSet.setData(0, 3, RoleName);
	//Set User Last Name.
	RoleRecordSet.setData(0, 4, RoleDescription);
	//Set User password.
	RoleRecordSet.setData(0, 5, RoleType);

	//Write.
	TableOrm.Write(pStatus, RoleRecordSet);
	if(!pStatus.IsOK())
		return;

	//Return last inserted ID.
	nNewRoleRowID = RoleRecordSet.getDataRef(0, 0).toInt();
}

/*!
Rename role.

\param pStatus				- If err occur, returned here
\param RoleID				- Role ID.
\param NewRoleName			- Role name.
\param NewRoleDescription	- Role description.
*/
void AccessRightsManipulator::RenameRole(Status &pStatus, int RoleID, QString NewRoleName, QString NewRoleDescription)
{
	DbSimpleOrm TableOrm(pStatus, CORE_ROLE, m_pManager); 
	if(!pStatus.IsOK()) 
		return;

	//Read recordset to rewrite.
	DbRecordSet RoleRecordSet;
	QString strWhere = "WHERE CROL_ID = %1";
	TableOrm.Read(pStatus, RoleRecordSet, strWhere.arg(QVariant(RoleID).toString()), DbSqlTableView::TVIEW_CORE_ROLE);
	if(!pStatus.IsOK()) 
		return;

	//Rename Person First name.
	RoleRecordSet.setData(0, 3, NewRoleName);
	//Rename Person Last name.
	RoleRecordSet.setData(0, 4, NewRoleDescription);

	//Update.
	TableOrm.Write(pStatus, RoleRecordSet);
}

/*!
Insert access right to role.

\param pStatus				- If err occur, returned here
\param RoleID				- Role ID.
\param ARSID				- Access right set ID.
*/
void AccessRightsManipulator::InsertAccessRights(Status &pStatus, int RoleID, int ARSID)
{
	DbSimpleOrm TableOrm(pStatus, CORE_ACCESSRIGHTS, m_pManager); 
	if(!pStatus.IsOK()) 
		return;

	//Instantiate Access right organizer.
	AccessRightsOrganizer ACCOrganizer;

	//Get access right set but first get access right set code.
	DbSimpleOrm CORE_ACCRSETTableOrm(pStatus, CORE_ACCRSET, m_pManager); 
	if(!pStatus.IsOK()) 
		return;

	DbRecordSet tmpRecordSet;
	QString strWh = " WHERE CAST_ID = %1";
	CORE_ACCRSETTableOrm.Read(pStatus, tmpRecordSet, strWh.arg(QVariant(ARSID).toString()));
	Q_ASSERT(tmpRecordSet.getRowCount() > 0);
	int ARSCode = tmpRecordSet.getDataRef(0, 6).toInt();

	AccessRightSet *ARSet = ACCOrganizer.GetAccessRightSet(ARSCode);

	//Get access rigths list.
	QVector<int> ARAddList;
	ARSet->GetARSAddList(ARAddList);

	//Loop through list.
	QVectorIterator<int> i(ARAddList);

	while (i.hasNext())
	{
		int ARID = i.next();
		DbRecordSet ARRecordSet;
		AccessRight *ARight = ACCOrganizer.GetAccessRight(ARID);

		ARight->GetARRecordSet(ARRecordSet);
		ARRecordSet.setData(0, 3, ARSID);	//Access right set ID.
		ARRecordSet.setData(0, 4, RoleID);	//Role ID.

		//Write.
		TableOrm.Write(pStatus, ARRecordSet);
		if(!pStatus.IsOK())
			return;
	}
}
//Only for reorganizing - Access right set MUST!!! be defined in accessrightsorganizer.h
void AccessRightsManipulator::InsertNewAccRSetToDatabase(Status &pStatus, int nARSID)
{
	//Instantiate Access right organizer.
	AccessRightsOrganizer ACCOrganizer;
	//Get access rights set.
	AccessRightSet *ARSet = ACCOrganizer.GetAccessRightSet(nARSID);

	DbRecordSet recARSet; 
	ARSet->GetARSRecordSet(recARSet);

	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(pStatus, CORE_ACCRSET, m_pManager); 
	if(!pStatus.IsOK()) return;

	//First check is there an existing access right set with the same ID???
	DbRecordSet recTmp;
	TableOrm.Read(pStatus, recTmp);
	if(!pStatus.IsOK()) return;

	//Go through rows and check for existing access right set.
	int nRowCount = recTmp.getRowCount();
	for (int i = 0; i < nRowCount; ++i)
	{
		if (recTmp.getDataRef(i, "CAST_CODE").toInt() == nARSID)
			return;
	}

	//Write.
	TableOrm.Write(pStatus, recARSet);
	if(!pStatus.IsOK()) return;
}

//Only for reorganizing - Access right MUST!!! be defined in accessrightsorganizer.h
//In fact we add rights to existing roles which have this access rights set.
//All new or existing roles that don't have this access rights set on adding will get this access right automaticaly.
void AccessRightsManipulator::InsertNewAccessRightToAccRSet(Status &pStatus, int nARSID, int nAccessRightID)
{
	//Get all access rights set ID for given ARSet.
	DbSimpleOrm CORE_ACCRSETTableOrm(pStatus, CORE_ACCRSET, m_pManager); 
	if(!pStatus.IsOK()) 
		return;
	//Read.
	DbRecordSet CORE_ACCRSET_Recordset;
	QString strWhere = "WHERE CAST_CODE = %1";
	CORE_ACCRSETTableOrm.Read(pStatus, CORE_ACCRSET_Recordset, strWhere.arg(QVariant(nARSID).toString()), DbSqlTableView::TVIEW_CORE_ACCRSET);
	if(!pStatus.IsOK()) 
		return;

	int nCAST_ID;
	int nRowCount = CORE_ACCRSET_Recordset.getRowCount();
	if (!nRowCount)
		return;
	else
		nCAST_ID = CORE_ACCRSET_Recordset.getDataRef(0, "CAST_ID").toInt();

	//Get all access rights which have this access rights set.
	DbSimpleOrm CORE_ACCESSRIGHTS_TableORM(pStatus, CORE_ACCESSRIGHTS, m_pManager); 
	if(!pStatus.IsOK()) return;
	
	//Read.
	DbRecordSet CORE_ACCESSRIGHTS_RecordSet;
	strWhere = "WHERE CAR_ACCRSET_ID = %1 AND CAR_ROLE_ID NOT IN (SELECT CAR_ROLE_ID FROM CORE_ACCESSRIGHTS WHERE CAR_CODE = %2)";
	CORE_ACCESSRIGHTS_TableORM.Read(pStatus, CORE_ACCESSRIGHTS_RecordSet, strWhere.arg(QVariant(nCAST_ID).toString()).arg(QVariant(nAccessRightID).toString()), DbSqlTableView::TVIEW_CORE_ACCESSRIGHTS);
	if(!pStatus.IsOK()) 
		return;

	//Get row count. If no row count return - nobody has this access rights set.
	nRowCount = CORE_ACCESSRIGHTS_RecordSet.getRowCount();
	if (!nRowCount)
		return;

	//Instantiate Access right organizer.
	AccessRightsOrganizer ACCOrganizer;
	//Get access rights set.
	AccessRight *AccRight = ACCOrganizer.GetAccessRight(nAccessRightID);

	//Create RecordSet.
	DbRecordSet CORE_ACCESSRIGHTS_INSERT_RecordSet;
	DbRecordSet CORE_ACCESSRIGHTS_INSERT_Template;
	AccRight->GetARRecordSet(CORE_ACCESSRIGHTS_INSERT_Template);
	CORE_ACCESSRIGHTS_INSERT_RecordSet.copyDefinition(CORE_ACCESSRIGHTS_INSERT_Template);


	//Loop through records and find which roles have this access rights set and create insert recordset.
	QList<int> lstRoles;
	for (int i = 0; i < nRowCount; ++i)
	{
		int nRoleID = CORE_ACCESSRIGHTS_RecordSet.getDataRef(i, "CAR_ROLE_ID").toInt();
		if (!lstRoles.contains(nRoleID))
		{
			lstRoles << nRoleID;
			CORE_ACCESSRIGHTS_INSERT_RecordSet.merge(CORE_ACCESSRIGHTS_INSERT_Template);

			//What row is current row.			
			int nRow = CORE_ACCESSRIGHTS_INSERT_RecordSet.getRowCount()-1;

			CORE_ACCESSRIGHTS_INSERT_RecordSet.setData(nRow, "CAR_ROLE_ID", nRoleID);
			CORE_ACCESSRIGHTS_INSERT_RecordSet.setData(nRow, "CAR_ACCRSET_ID", nCAST_ID);
		}
	}

	CORE_ACCESSRIGHTS_TableORM.Write(pStatus, CORE_ACCESSRIGHTS_INSERT_RecordSet);
	if(!pStatus.IsOK()) 
		return;
}

//Insert an existing access rights set to predefined role - a role that has role code. (all user defined roles have CROL_CODE = NULL).
//It checks is this accrset already inserted in given role, if not it adds it.
//Only for reorganizing.
void AccessRightsManipulator::InsertNewAccRSetToDefaultRole(Status &Ret_pStatus, int nRoleCode, int nAccRSetCode)
{
	//Get given role.
	DbSimpleOrm CORE_ROLE_ORM(Ret_pStatus, CORE_ROLE, m_pManager); 
	if(!Ret_pStatus.IsOK()) 
		return;
	
	//Read.
	DbRecordSet CORE_ROLE_Recordset;
	QString strWhere = "WHERE CROL_CODE = %1";
	CORE_ROLE_ORM.Read(Ret_pStatus, CORE_ROLE_Recordset, strWhere.arg(QVariant(nRoleCode).toString()), DbSqlTableView::TVIEW_CORE_ROLE);
	if(!Ret_pStatus.IsOK()) 
		return;

	//Get role id for looking in CORE_ACCRSETROLE table for existing 
	int nCROL_ID;
	if (!CORE_ROLE_Recordset.getRowCount())
		return;
	else
		nCROL_ID = CORE_ROLE_Recordset.getDataRef(0, "CROL_ID").toInt();

	//Get given access rights set ID.
	DbSimpleOrm CORE_ACCRSETTableOrm(Ret_pStatus, CORE_ACCRSET, m_pManager); 
	if(!Ret_pStatus.IsOK()) 
		return;
	//Read.
	DbRecordSet CORE_ACCRSET_Recordset;
	strWhere = "WHERE CAST_CODE = %1";
	CORE_ACCRSETTableOrm.Read(Ret_pStatus, CORE_ACCRSET_Recordset, strWhere.arg(QVariant(nAccRSetCode).toString()), DbSqlTableView::TVIEW_CORE_ACCRSET);
	if(!Ret_pStatus.IsOK()) 
		return;

	int nCAST_ID;
	if (!CORE_ACCRSET_Recordset.getRowCount())
		return;
	else
		nCAST_ID = CORE_ACCRSET_Recordset.getDataRef(0, "CAST_ID").toInt();
	
	//Get role to accrset table to see it this access rights set already defined in this role.
	DbSimpleOrm CORE_ACCRSETROLE_ORM(Ret_pStatus, CORE_ACCRSETROLE, m_pManager); 
	if(!Ret_pStatus.IsOK()) 
		return;
	
	//Read.
	DbRecordSet CORE_ACCRSETROLE_Recordset;
	strWhere = "WHERE CASTR_ROLE_ID = %1 AND CASTR_ACCRSET_ID = %2 ";
	CORE_ACCRSETROLE_ORM.Read(Ret_pStatus, CORE_ACCRSETROLE_Recordset, strWhere.arg(QVariant(nCROL_ID).toString()).arg(QVariant(nCAST_ID).toString()), DbSqlTableView::TVIEW_CORE_ACCRSETROLE);
	if(!Ret_pStatus.IsOK()) 
		return;

	//If there is already this access rights set inserted in this role return.
	if (CORE_ACCRSETROLE_Recordset.getRowCount()>0)
		return;

	//If not insert it.
	InsertNewAccRSetToRole(Ret_pStatus, nCROL_ID, nCAST_ID);
}
