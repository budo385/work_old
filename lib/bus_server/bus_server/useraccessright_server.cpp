#include "useraccessright_server.h"

#include "bus_core/bus_core/usersessionmanager.h"
extern UserSessionManager	*g_UserSessionManager;
#include "businessserviceset.h"
extern BusinessServiceSet* g_BusinessServiceSet;

UserAccessRight_Server::UserAccessRight_Server()
{
}


UserAccessRight_Server::~UserAccessRight_Server()
{
}

int UserAccessRight_Server::GetUserID()
{
	return g_UserSessionManager->GetUserID();
}
int	UserAccessRight_Server::GetPersonID()
{
	return g_UserSessionManager->GetPersonID();
}
bool UserAccessRight_Server::IsSystemUser()
{
	return g_UserSessionManager->IsSystemUser(); //is sys user under current thread
}
void UserAccessRight_Server::TestUAR(Status &Ret_pStatus, int nTableID, int nPersonID, int nAccessLevel, DbRecordSet &lstTest_IDs, DbRecordSet &Ret_lstResult_IDs, bool bReturnAllowed)
{
	g_BusinessServiceSet->AccessRights->TestUAR(Ret_pStatus,nTableID,nPersonID,nAccessLevel,lstTest_IDs,Ret_lstResult_IDs,bReturnAllowed);
}
