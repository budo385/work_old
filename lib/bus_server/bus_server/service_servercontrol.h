#ifndef SERVICE_SERVERCONTROL_H
#define SERVICE_SERVERCONTROL_H

#include "businessservice.h"
#include "bus_interface/bus_interface/interface_servercontrol.h"

// used to control APP server-> not available in THICK

class Service_ServerControl : public Interface_ServerControl, public BusinessService
{
public:

	//bcp & restore
	void GetBackupData(Status &Ret_pStatus,int& Ret_nBackupFreq,QString& Ret_strBackupPath,QString& Ret_datBackupLastDate,QString& Ret_strRestoreOnNextStart,int& Ret_nBackupDay,QString& Ret_strBackupTime,int& Ret_nDeleteOldBackupsDay);
	void SetBackupData(Status &Ret_pStatus,int nBackupFreq, QString strBackupPath, int nBackupDay,QString strBackupTime,int nDeleteOldBackupsDay);
	void LoadBackupList(Status &Ret_pStatus,DbRecordSet &RetOut_Data);
	void Backup(Status &Ret_pStatus);
	void Restore(Status &Ret_pStatus,QString strRestoreFile);
	void LoadTemplateList(Status &Ret_pStatus,DbRecordSet &RetOut_Data);
	void RestoreFromTemplate(Status &Ret_pStatus,QString strRestoreFile);
	void SetTemplateData(Status &Ret_pStatus,QString strTemplateInUse,int nAskOnStartup, int nStartStartUpWizard);
	void GetTemplateData(Status &Ret_pStatus,QString &Ret_strTemplateInUse,int &Ret_nAskOnStartup,int &Ret_nStartStartUpWizard);
	void LoadBackupFile(Status &Ret_pStatus, QString strBackupName,QByteArray &Ret_bcpContent);
	void SaveBackupFile(Status &Ret_pStatus, QString strBackupName,QByteArray &Destroy_bcpContent);
	void DeleteBackupFile(Status &Ret_pStatus, QString strBackupName);
	//import & export
	void SaveImportExportSettings(Status &Ret_pStatus, DbRecordSet lstSettings);
	void LoadImportExportSettings(Status &Ret_pStatus, DbRecordSet &Ret_lstSettings);
	void StartProcessProject(Status &Ret_pStatus); 
	void StartProcessUser(Status &Ret_pStatus); 
	void StartProcessDebtor(Status &Ret_pStatus); 
	void StartProcessContact(Status &Ret_pStatus); 
	void StartProcessStoredProjectList(Status &Ret_pStatus); 
	//log:
	void GetLog(Status &Ret_pStatus,QByteArray &Ret_logData,int &RetOut_nCurrentPosition,int &RetOut_nLogFileSizeOnStartOfChunkRead,int nChunkSize);
	void GetLogData(Status &Ret_pStatus, int &Ret_nLogLevel, int &Ret_nLogMaxSize, int &Ret_nBufferSize, int &Ret_nCurrentSize);
	void SetLogData(Status &Ret_pStatus, int nLogLevel, int nLogMaxSize, int nBufferSize);
	//reminders:
	void GetAllActiveRemindersForUser(Status &Ret_pStatus,int nPersonID,DbRecordSet &Ret_Reminders);
	void DeleteReminders(Status &Ret_pStatus,DbRecordSet &lstReminders);
	void PostPoneReminders(Status &Ret_pStatus,DbRecordSet &lstReminders, int nMinutes);
};

#endif // SERVICE_SERVERCONTROL_H
