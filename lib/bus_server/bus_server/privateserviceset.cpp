#include "privateserviceset.h"


/*!
	When creating service, place service instance here

	\param pDbManager		- global DbSqlManager
	\param pSessionManager	- global UserSessionManager for getting user context

*/
PrivateServiceSet::PrivateServiceSet(DbSqlManager *pDbManager,UserSessionManager* pSessionManager)
{
	//set global managers:
	m_SessionManager=pSessionManager;
	m_DbManager=pDbManager;
	Q_ASSERT_X(m_SessionManager!=NULL,"Service set","Session manager empty");
	Q_ASSERT_X(m_DbManager!=NULL,"Service set","DB manager empty");

	//init all services:
	InitServiceSet();

	CoreUser = new Service_CoreUser; m_ServiceList.append(dynamic_cast<Service_CoreUser*>(CoreUser));
	BusinessHelper= new Service_BusinessHelper;m_ServiceList.append(dynamic_cast<Service_BusinessHelper*>(BusinessHelper));



#ifndef QT_NO_DEBUG
	//check pointers:
	CheckServiceSet();
#endif

}




//delete all services from list
PrivateServiceSet::~PrivateServiceSet()
{
	qDeleteAll(m_ServiceList);  //new faster way to delete all objects

	/*
		int nSize=m_ServiceList.size();
		for(int i=0;i<nSize;++i)
		{
			delete m_ServiceList.at(i);
		}
	*/
}


//init all services
void PrivateServiceSet::InitServiceSet()
{
	BusinessService::SetUserSessionManager(m_SessionManager);
	BusinessService::SetDbManager(m_DbManager);
}


//check for initialization
void PrivateServiceSet::CheckServiceSet()
{
		int nSize=m_ServiceList.size();
		for(int i=0;i<nSize;++i)
		{
			if(m_ServiceList.at(i)==NULL)
				Q_ASSERT_X(false,"INIT_SRV","NULL Pointers detected!");
		
		}
}
