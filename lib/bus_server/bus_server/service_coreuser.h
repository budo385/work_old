#ifndef SERVICE_COREUSER_H
#define SERVICE_COREUSER_H

#include "businessservice.h"


/*!
	\class Service_CoreUser
	\brief Core User API's
	\ingroup Bus_Services

	Provides methods for client Login/Logout by core user

*/

class Service_CoreUser : public BusinessService
{
public:

	//User cached context data:session, user info, Ar, Fp sets (not available on App. server interface):
	void BuildUserContextData(Status &pStatus,QString &strSession,DbRecordSet &rowUserData, QString strModuleCode, QString strMLIID, QString strParentSession="", int nClientTimeZoneOffsetMinutes=0); //called when user is authenticated, creates session & etc..
	void DestroyUserContextData(Status &pStatus,QString strSession,int nUserThreadID=-1);	//called before user session deletion: release memory

	void GetUserByUserName(Status &Ret_pStatus, QString strUsername, DbRecordSet &Ret_Data);
	void GetUserBySession(Status &Ret_pStatus, QString strSession, DbRecordSet &Ret_Data);
	void GetUserByID(Status &Ret_pStatus, int nUserID, DbRecordSet &Ret_Data);

};

#endif // SERVICE_COREUSER_H
