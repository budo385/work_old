#include "service_busgrouptree.h"
#include "db_core/db_core/dbsqltableview.h"
#include "db/db/dbsimpleorm.h"
#include "bus_server/bus_server/businesslocker.h"
#include "common/common/datahelper.h"
#include "common/common/entity_id_collection.h"
#include "bus_core/bus_core/useraccessright.h"
extern UserAccessRight *g_AccessRight;				//global access right tester
#include "businessserviceset.h"
extern BusinessServiceSet* g_BusinessServiceSet;

//init hier
Service_BusGroupTree::Service_BusGroupTree()
{
	m_TreeItemManager.Initialize(BUS_GROUP_ITEMS,GetDbManager());
}

void Service_BusGroupTree::ReadTree(Status &Ret_pStatus, int nGroupEntityTypeID,DbRecordSet &Ret_Data)
{

	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_GROUP_TREE, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	QString strWhere=" WHERE BGTR_ENTITY_TYPE_ID= "+QVariant(nGroupEntityTypeID).toString();

	//load all
	TableOrm.Read(Ret_pStatus,Ret_Data,strWhere);
}

void Service_BusGroupTree::WriteTree(Status &Ret_pStatus, DbRecordSet &RetOut_Data,QString strLockRes)
{

	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_GROUP_TREE, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	//write
	TableOrm.Write(Ret_pStatus,RetOut_Data);

	//if write ok & locked, unlock:
	if(Ret_pStatus.IsOK() && !strLockRes.isEmpty())
	{
		BusinessLocker locker(Ret_pStatus,BUS_GROUP_TREE,GetDbManager(), TableOrm.GetDbConnection());
		locker.UnLock(Ret_pStatus,strLockRes);
	}

}

//lock tree before:
void Service_BusGroupTree::DeleteTree(Status &Ret_pStatus, int nTreeID)
{

	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_GROUP_TREE, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;


	QString strLocked;
	BusinessLocker locker(Ret_pStatus,BUS_GROUP_TREE,GetDbManager(), TableOrm.GetDbConnection());
	if(!Ret_pStatus.IsOK()) return;

	//try to lock contacts:
	DbLockList list;
	list<<nTreeID;
	locker.Lock(Ret_pStatus,list,strLocked);
	if(!Ret_pStatus.IsOK()) return;
	
	//delete:
	QList<int> lstDel;
	lstDel<<nTreeID;
	TableOrm.DeleteFast(Ret_pStatus,lstDel);

	Status err;
	locker.UnLock(err,strLocked);
	
}

void Service_BusGroupTree::LockTree(Status &Ret_pStatus, int nTreeID,QString &Ret_strLockRes )
{

	//Make Locker Obj & lock:
	BusinessLocker locker(Ret_pStatus,BUS_GROUP_TREE,GetDbManager());
	QList<int> lstLock;
	lstLock<<nTreeID;
	locker.Lock(Ret_pStatus,lstLock,Ret_strLockRes);
}

void Service_BusGroupTree::UnLockTree(Status &Ret_pStatus, QString strLockRes )
{
	BusinessLocker locker(Ret_pStatus,BUS_GROUP_TREE,GetDbManager());
	locker.UnLock(Ret_pStatus,strLockRes);
}



//-------------------------------------------------
//ITEMS:
//-------------------------------------------------

void Service_BusGroupTree::ReadGroupTree(Status &Ret_pStatus, int nTreeID,DbRecordSet &Ret_Data)
{


	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_GROUP_ITEMS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	QString strWhere=" WHERE BGIT_TREE_ID= "+QVariant(nTreeID).toString()+" ORDER BY BGIT_CODE";

	//load all
	TableOrm.Read(Ret_pStatus,Ret_Data,strWhere);

}

void Service_BusGroupTree::WriteGroupItem(Status &Ret_pStatus, int nTreeID,DbRecordSet &RetOut_Data)
{
	DbSqlQuery query(Ret_pStatus,GetDbManager());
	if(!Ret_pStatus.IsOK()) return;
	query.BeginTransaction(Ret_pStatus);
	if(!Ret_pStatus.IsOK()) return;

	QString strWhere=" AND BGIT_TREE_ID="+QVariant(nTreeID).toString();
	m_TreeItemManager.Write(Ret_pStatus,RetOut_Data,"",strWhere);
	if(!Ret_pStatus.IsOK()) 
	{
		query.Rollback();
		return;
	}

	g_BusinessServiceSet->AccessRights->UpdateGroupContactsGUAR(Ret_pStatus,RetOut_Data);
	if(!Ret_pStatus.IsOK()) 
	{
		query.Rollback();
		return;
	}

	query.Commit(Ret_pStatus);
}

//only can be executed if tree id locked.
void Service_BusGroupTree::DeleteGroupItem(Status &Ret_pStatus, int nTreeID,DbRecordSet &DataForDelete)
{
	DbSqlQuery query(Ret_pStatus,GetDbManager());
	if(!Ret_pStatus.IsOK()) return;
	query.BeginTransaction(Ret_pStatus);
	if(!Ret_pStatus.IsOK()) return;

	QString strWhere=" AND BGIT_TREE_ID="+QVariant(nTreeID).toString();
	m_TreeItemManager.Delete(Ret_pStatus,DataForDelete,"",strWhere);
	if(!Ret_pStatus.IsOK()) 
	{
		query.Rollback();
		return;
	}

	g_BusinessServiceSet->AccessRights->UpdateGroupContactsGUAR(Ret_pStatus,DataForDelete);
	if(!Ret_pStatus.IsOK()) 
	{
		query.Rollback();
		return;
	}
	query.Commit(Ret_pStatus);
}



/*!
	Changes content of group: content of group is NM table between item <-> entity data (e.g.: item<->contact in table bus_cm_group)
	When adding new group NM table, just add here inside switch.
	Operation is consist of these steps:
	- lock all records inside NM relation
	- reload real content data (to be sure)
	- change them
	- unlock
	- return real content data (to be sure)

	\param Ret_pStatus			- err
	\param nGroupEntityTypeID	- entity ID (contacts, etc..)
	\param nGroupItemID			- item ID
	\param nOperation			- from DataHelper: add, remove, replace, intersect
	\param RetOut_GroupContent	- Data in format of NM table for write, after operation it will be filled with all content data

*/
void Service_BusGroupTree::ChangeGroupContent(Status &Ret_pStatus, int nGroupEntityTypeID,int nGroupItemID,int nOperation, DbRecordSet &RetOut_GroupContent)
{

	DbSqlQuery query(Ret_pStatus,GetDbManager());
	if(!Ret_pStatus.IsOK()) return;
	query.BeginTransaction(Ret_pStatus);
	if(!Ret_pStatus.IsOK()) return;


	switch(nGroupEntityTypeID)
	{
	case ENTITY_BUS_CONTACT:
		ChangeContent(Ret_pStatus,nGroupEntityTypeID,nGroupItemID,nOperation,RetOut_GroupContent,BUS_CM_GROUP,"BGCN_ITEM_ID","BGCN_CONTACT_ID");
		break;

	default:
		Q_ASSERT(false);
		break;
	}

	if(!Ret_pStatus.IsOK()) 
	{
		query.Rollback();
		return;
	}
	DbRecordSet lstGroup;
	lstGroup.addColumn(QVariant::Int,"BGIT_ID");
	lstGroup.addRow();
	lstGroup.setData(0,0,nGroupItemID);
	//lstGroup.Dump();
	g_BusinessServiceSet->AccessRights->UpdateGroupContactsGUAR(Ret_pStatus,lstGroup);
	if(!Ret_pStatus.IsOK()) 
	{
		query.Rollback();
		return;
	}
	query.Commit(Ret_pStatus);


}



/*!
	Reads content of one item from specific NM entity table (e.g.: item<->contact in table bus_cm_group)
	Add own here in switch case

	\param Ret_pStatus			- err
	\param nGroupEntityTypeID	- entity ID (contacts, etc..)
	\param GroupItems			- list of group/item ID (at col 0)
	\param Ret_GroupContent		- Data in specific format (e.g. TVIEW_BUS_CM_GROUP for contacts)
*/
void Service_BusGroupTree::ReadGroupContent(Status &Ret_pStatus, int nGroupEntityTypeID,DbRecordSet &GroupItems,DbRecordSet &Ret_GroupContent)
{


	switch(nGroupEntityTypeID)
	{
		case ENTITY_BUS_CONTACT:
			{

				//To avoid lags: reads groups by chunks: first read all group content, then at client read one by one

				DbSimpleOrm TableOrm(Ret_pStatus, BUS_CM_GROUP,GetDbManager()); 
				if(!Ret_pStatus.IsOK()) return;

				//make d' filter: (contact NM relation to the group has same)
				QString strWhere;
				QString strName=GroupItems.getColumnName(0);
				DbSqlTableDefinition::GenerateChunkedWhereStatement(GroupItems,strName,strWhere,0,-1);
				strWhere=" WHERE BGCN_ITEM_ID IN "+strWhere;
				TableOrm.Read(Ret_pStatus,Ret_GroupContent,strWhere);

			}
			break;
		default:
			Q_ASSERT(false);
	}


}



//for any table, just init NM table ID, M table ID and name of ID field inside NM table of table M: see example for contact
//strM_KeyID = GROUP
//strN_KeyID = CONTACT or other Related
void Service_BusGroupTree::ChangeContent(Status &Ret_pStatus, int nGroupEntityTypeID,int nGroupItemID,int nOperation, DbRecordSet &RetOut_GroupContent,int nTableNMRelation_ID,QString strGroupKeyID,QString strRelationKeyID)
{

	DbRecordSet lstNMData,lstWrite,lstDelete;

	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, nTableNMRelation_ID, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;



	//lock only ITEM:
	DbLockList list;
	list<<nGroupItemID;
	QString strLockedResourceID;
	BusinessLocker locker(Ret_pStatus,BUS_GROUP_ITEMS,GetDbManager(), TableOrm.GetDbConnection());
	locker.Lock(Ret_pStatus,list,strLockedResourceID);
	if(!Ret_pStatus.IsOK()) return;


	//load prev content-> 
	QString strWhere=" WHERE "+strGroupKeyID+" = "+QVariant(nGroupItemID).toString();
	TableOrm.Read(Ret_pStatus,lstNMData,strWhere);
	if(!Ret_pStatus.IsOK()) return;

	//change data:
	//int nMIDIdx=lstNMData.getColumnIdx(strM_KeyID);
	int nNIDIdx=lstNMData.getColumnIdx(strRelationKeyID);
	
	//only filter if add:
	if (nOperation==DataHelper::OP_ADD)
	{
		DataHelper::RemoveDuplicates(&lstNMData,&RetOut_GroupContent,nNIDIdx,nNIDIdx);
	}
	//RetOut_GroupContent.Dump();

	DataHelper::DataOperation(nOperation,&RetOut_GroupContent,&lstNMData,&lstWrite,&lstDelete,nNIDIdx);
	//lstNMData.Dump();
	//lstWrite.Dump();
	//lstDelete.Dump();


	if (lstDelete.getRowCount()>0 || lstWrite.getRowCount()>0)
	{
		//begin transaction
		TableOrm.GetDbSqlQuery()->BeginTransaction(Ret_pStatus);
		if(!Ret_pStatus.IsOK())
		{	
			Status err;
			locker.UnLock(err,strLockedResourceID);
			return;
		}

		//delete: 
		if (lstDelete.getRowCount()>0)
		{
			TableOrm.DeleteFast(Ret_pStatus,lstDelete);
			if(!Ret_pStatus.IsOK())
			{	
				TableOrm.GetDbSqlQuery()->Rollback();
				Status err;
				locker.UnLock(err,strLockedResourceID);
				return;
			}

		}

		//write: 
		if (lstWrite.getRowCount()>0)
		{
			TableOrm.Write(Ret_pStatus,lstWrite);
			if(!Ret_pStatus.IsOK())
			{	
				TableOrm.GetDbSqlQuery()->Rollback();
				Status err;
				locker.UnLock(err,strLockedResourceID);
				return;
			}

		}

		//commit tran:
		TableOrm.GetDbSqlQuery()->Commit(Ret_pStatus);
		if(!Ret_pStatus.IsOK())
		{	
			Status err;
			locker.UnLock(err,strLockedResourceID);
			return;
		}



	}

	//read whole content to be safe:
	DbRecordSet lstGroupItems;
	lstGroupItems.addColumn(QVariant::Int,"GROUP_ID");
	lstGroupItems.addRow();
	lstGroupItems.setData(0,0,nGroupItemID);
	ReadGroupContent(Ret_pStatus,nGroupEntityTypeID,lstGroupItems,RetOut_GroupContent);
	if(!Ret_pStatus.IsOK())
	{	
		Status err;
		locker.UnLock(err,strLockedResourceID);
		return;
	}

	//unlock:
	locker.UnLock(Ret_pStatus,strLockedResourceID);


}


//updates item content: desc+ext category, locks before
void Service_BusGroupTree::WriteGroupItemData(Status &Ret_pStatus, int nGroupItemID, QString strExtCategory, QString strDescription)
{
	DbSqlQuery query(Ret_pStatus, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	QString strLockedResourceID;
	DbLockList list;
	list<<nGroupItemID;

	//lock all
	BusinessLocker locker(Ret_pStatus,BUS_GROUP_ITEMS,GetDbManager(), query.GetDbConnection());
	locker.Lock(Ret_pStatus,list,strLockedResourceID);
	if(!Ret_pStatus.IsOK()) return;

	QString strSQL="UPDATE bus_group_items SET BGIT_DESCRIPTION=?, BGIT_EXTERNAL_CATEGORY=? WHERE BGIT_ID=?";

	query.Prepare(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK())
	{ 
		Status err;
		locker.UnLock(err,strLockedResourceID);
		return;
	}
	query.bindValue(0,strDescription);
	query.bindValue(1,strExtCategory);
	query.bindValue(2,nGroupItemID);

	query.ExecutePrepared(Ret_pStatus);
	Status err;
	locker.UnLock(err,strLockedResourceID);
			

}



//deletes one item from group, one or more group in Groups (first col is group id)
void Service_BusGroupTree::RemoveGroupContentItem(Status &Ret_pStatus, int nGroupEntityTypeID,DbRecordSet &GroupItemsForRemove,DbRecordSet &Groups )
{
	DbSqlQuery query(Ret_pStatus,GetDbManager());
	if(!Ret_pStatus.IsOK()) return;
	query.BeginTransaction(Ret_pStatus);
	if(!Ret_pStatus.IsOK()) return;

	switch(nGroupEntityTypeID)
	{
	case ENTITY_BUS_CONTACT:
		{
			DeleteContent(Ret_pStatus,nGroupEntityTypeID,Groups,GroupItemsForRemove,BUS_CM_GROUP,"BGCN_ITEM_ID","BGCN_CONTACT_ID");
		}
		break;
	default:
		Q_ASSERT(false);
	}

	if(!Ret_pStatus.IsOK()) 
	{
		query.Rollback();
		return;
	}
	g_BusinessServiceSet->AccessRights->UpdateGroupContactsGUAR(Ret_pStatus,Groups);
	if(!Ret_pStatus.IsOK()) 
	{
		query.Rollback();
		return;
	}
	query.Commit(Ret_pStatus);
}



//write content data:
void Service_BusGroupTree::WriteGroupContentData(Status &Ret_pStatus, int nGroupEntityTypeID, DbRecordSet &GroupItems)
{

	switch(nGroupEntityTypeID)
	{
	case ENTITY_BUS_CONTACT:
		{
			DbSimpleOrm TableOrm(Ret_pStatus, BUS_CM_GROUP,GetDbManager()); 
			if(!Ret_pStatus.IsOK()) return;

			//lock all
			QString strLockedResourceID;
			BusinessLocker locker(Ret_pStatus,BUS_CM_GROUP,GetDbManager(), TableOrm.GetDbConnection());
			locker.Lock(Ret_pStatus,GroupItems,strLockedResourceID);
			if(!Ret_pStatus.IsOK()) return;
			TableOrm.Write(Ret_pStatus,GroupItems);

			//unlock:
			Status err;
			locker.UnLock(err,strLockedResourceID);
		}
		break;
	default:
		Q_ASSERT(false);
	}
}



//Deletes content item from one group of all groups in Groups, if GroupItemsForRemove=empty, deletes all
//for any table, just init NM table ID, M table ID and name of ID field inside NM table of table M: see example for contact
//strM_KeyID = GROUP
//strN_KeyID = CONTACT or other Related
void Service_BusGroupTree::DeleteContent(Status &Ret_pStatus, int nGroupEntityTypeID,DbRecordSet &Groups,DbRecordSet &GroupItemsForRemove,int nTableNMRelation_ID,QString strGroupKeyID,QString strRelationKeyID)
{
	//_DUMP(Groups);

	//if group undefined:
	if (Groups.getRowCount()==0)
		return;

	//make group filter:
	QString strWhereGroups;
	QString strName=Groups.getColumnName(0);
	DbSqlTableDefinition::GenerateChunkedWhereStatement(Groups,strName,strWhereGroups,0,-1);
	if (Groups.getRowCount()==1)
		strWhereGroups=strGroupKeyID+"="+Groups.getDataRef(0,0).toString();
	else
		strWhereGroups=strGroupKeyID+" IN "+strWhereGroups;


	//query:
	DbSqlQuery query(Ret_pStatus, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	// - lock group in bus_group_items
	
	//lock all
	QString strLockedResourceID;
	BusinessLocker locker(Ret_pStatus,BUS_GROUP_ITEMS,GetDbManager(), query.GetDbConnection());
	locker.Lock(Ret_pStatus,Groups,strLockedResourceID);
	if(!Ret_pStatus.IsOK()) return;


	//----------------------------------------------
	//			TRANSACTION
	//----------------------------------------------
	query.BeginTransaction(Ret_pStatus);
	if(!Ret_pStatus.IsOK())	return;



	//----------------------------------------------
	//			DELETE RECORDS
	//----------------------------------------------

	//GroupItemsForRemove.Dump();

	int nCount=0;
	QString strWhereIn,strWhere,strRelationTableName;
	strRelationTableName=DbSqlTableDefinition::GetTableName(nTableNMRelation_ID); 

	if (GroupItemsForRemove.getRowCount()>0)
	{
		while (nCount>=0)
		{
			nCount=DbSqlTableDefinition::GenerateChunkedWhereStatement(GroupItemsForRemove,strRelationKeyID,strWhereIn,nCount,-1);
			if (nCount<0) break;
			strWhere=" WHERE "+strRelationKeyID+" IN "+strWhereIn+" AND "+strWhereGroups;
			QString strSql = "DELETE FROM "+ strRelationTableName+strWhere;
			query.Execute(Ret_pStatus,strSql);
			if(!Ret_pStatus.IsOK())
			{
				query.Rollback();
				//unlock:
				Status err;
				locker.UnLock(err,strLockedResourceID);
				return;
			}
		}

	}
	else
	{
		strWhere=" WHERE "+strWhereGroups; //whole groups
		QString strSql = "DELETE FROM "+ strRelationTableName+strWhere;
		query.Execute(Ret_pStatus,strSql);
		if(!Ret_pStatus.IsOK())
		{
			query.Rollback();
			//unlock:
			Status err;
			locker.UnLock(err,strLockedResourceID);
			return;
		}

	}


	//commit & unlock:
	query.Commit(Ret_pStatus);

	Status err;
	locker.UnLock(err,strLockedResourceID);

}

void Service_BusGroupTree::GetChildrenGroups(Status &Ret_pStatus, int nGroupParentID,DbRecordSet &Ret_ChildrenGroups)
{
	QString strCode;

	//create query (just for connection reservation/transaction):
	DbSqlQuery query(Ret_pStatus, GetDbManager());
	if(!Ret_pStatus.IsOK())
		return;

	//get code:
	QString strSQL="SELECT BGIT_CODE FROM BUS_GROUP_ITEMS WHERE BGIT_ID="+QVariant(nGroupParentID).toString();
	query.Execute(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK())
	{
		return;
	}
	DbRecordSet lstCode;
	query.FetchData(lstCode);

	if (lstCode.getRowCount()==0)
		return;
	else
		strCode=lstCode.getDataRef(0,0).toString();


	//calculate fields to fetch
	QString strFields = "BGIT_ID";
	strSQL.clear();
	strSQL.sprintf("SELECT %s FROM %s WHERE ", 
				   strFields.toLatin1().constData(),
				   "BUS_GROUP_ITEMS");

	int nLength = strCode.length();
	QString strSQL1;
	//B.T: universal DB indepent:
	GetDbManager()->GetFunction()->PrepareHierarchySelect(strSQL1,strCode,"BGIT",true);

	strSQL += strSQL1;

	query.Prepare(Ret_pStatus, strSQL);
	if(!Ret_pStatus.IsOK()){
		Q_ASSERT_X(false, "Service_BusGroupTree::GetChildrenGroups", Ret_pStatus.getErrorText().toLatin1());	
		return;
	}

	query.bindValue(0, strCode);

	query.ExecutePrepared(Ret_pStatus);

	//query.Execute(Ret_pStatus, strSQL);
	if(!Ret_pStatus.IsOK())
		return;

	query.FetchData(Ret_ChildrenGroups);

}