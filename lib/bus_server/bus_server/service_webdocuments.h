#ifndef SERVICE_WEBDOCUMENTS_H
#define SERVICE_WEBDOCUMENTS_H

#include "bus_interface/bus_interface/interface_webdocuments.h"
#include "businessservice.h"

class Service_WebDocuments : public Interface_WebDocuments, public BusinessService
{
public:
	//for logged user:
	void ReadDocuments(Status &Ret_pStatus, DbRecordSet &Ret_Documents);
	void ReadDocumentsWithFilter(Status &Ret_pStatus, int &Ret_nTotalCount,DbRecordSet &Ret_Documents,  int nDocType=-1, int nFromN=-1, int nToN=-1);

	//general api:
	void SearchDocuments(Status &Ret_pStatus, int &Ret_nTotalCount,DbRecordSet &Ret_Documents,  int nDocType=-1, int nFromN=-1, int nToN=-1, int nContactID=-1, int nProjectID=-1,  int nOwnerUserID=-1, QString strSearchByName="", QDateTime datFromLastModified=QDateTime(), QDateTime datToLastModified=QDateTime(), QDateTime datFromCreated=QDateTime(), QDateTime datToCreated=QDateTime());
	void ReadDocumentDetails(Status &Ret_pStatus, int nDocumentID, DbRecordSet &Ret_Document);
	void ReadNoteDocument(Status &Ret_pStatus, int nDocumentID,QString &Ret_strDocumentURL);
	void CheckIn(Status &Ret_pStatus, int nDocumentID);
	void CheckOut(Status &Ret_pStatus, int nDocumentID,QString &Ret_strDocumentURL,bool bReadOnly=true);
	void CheckOutCopy(Status &Ret_pStatus, int nDocumentID,QString &Ret_strDocumentURL);

private:
	void UnembedHtmlPixJob(QString &strHtmlData, QStringList &lstResTempFiles, QString strTempDir, QString strTagStart, QString strSettingStart);
	void UnembedHtmlPix(QString &strHtmlData, QStringList &lstResTempFiles, QString strTempDir);
};



#endif // SERVICE_WEBDOCUMENTS_H
