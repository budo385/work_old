#ifndef SERVICE_BUSCALENDAR_H
#define SERVICE_BUSCALENDAR_H

#include "bus_interface/bus_interface/interface_buscalendar.h"
#include "businessservice.h"
#include "db/db/dbsimpleorm.h"

class Service_BusCalendar : public Interface_BusCalendar, public BusinessService
{
public:
	virtual ~Service_BusCalendar(){};

	void SaveCalFilterView(Status &Ret_pStatus, int &Ret_nViewID, DbRecordSet recView, DbRecordSet recCalEntities, DbRecordSet recCalTypeIDs);
	void GetCalFilterViews(Status &Ret_pStatus, DbRecordSet &RetOut_recViews, int nPersonID, int nGridType = 0);
	void GetCalFilterViewsWithEntitesAndTypes(Status &Ret_pStatus, DbRecordSet &RetOut_recViews, DbRecordSet &RetOut_recCalEntities, DbRecordSet &RetOut_recCalTypeIDs, int nPersonID, int nGridType = 0);
	void GetCalFilterView(Status &Ret_pStatus, DbRecordSet &RetOut_recViewsData, DbRecordSet &RetOut_recCalEntities, DbRecordSet &RetOut_recCalTypeIDs, int nViewID);
	void DeleteCalFilterView(Status &Ret_pStatus, int nViewID);

	void WriteEvent(Status &Ret_pStatus, DbRecordSet &RetOut_Data, DbRecordSet &RetOut_Parts,DbRecordSet &RetOut_ScheduleTask,QString strLockRes,DbRecordSet &RetOut_UAR,DbRecordSet &RetOut_GAR);
	void ReadEvent(Status &Ret_pStatus, int nCalEventID, DbRecordSet &Ret_Data, DbRecordSet &Ret_Parts, DbRecordSet &Ret_ScheduleTask,DbRecordSet &Ret_UAR,DbRecordSet &Ret_GAR);
	void ReadTemplates(Status &Ret_pStatus, DbRecordSet &Ret_Data);
	void ReadOnePart(Status &Ret_pStatus, int nPartID,DbRecordSet &RetOut_RowPart,DbRecordSet &RetOut_RowActiveOption);
	void ReadOneEvent(Status &Ret_pStatus, int nBCEV_ID, DbRecordSet &Ret_EventParts, DbRecordSet &Ret_Contacts, DbRecordSet &Ret_Resources, DbRecordSet &Ret_Projects, DbRecordSet &Ret_Breaks);
	void ReadEventPartsCountForEntities(Status &Ret_pStatus, DbRecordSet &RetOut_recEntities, DbRecordSet recFilter, QDateTime datStartDate, QDateTime datEndDate);
	void ReadEventParts(Status &Ret_pStatus, DbRecordSet recFilter, int nEntityType, int nEntityID, QDateTime datStartDate, QDateTime datEndDate, DbRecordSet &Ret_EventParts, DbRecordSet &Ret_Contacts, DbRecordSet &Ret_Resources, DbRecordSet &Ret_Projects, DbRecordSet &Ret_Breaks);
	void ReadEventPartsForInsert(Status &Ret_pStatus, DbRecordSet recEntities, QDateTime datStartDate, QDateTime datEndDate, DbRecordSet &Ret_EventParts, DbRecordSet &Ret_Contacts, DbRecordSet &Ret_Resources, DbRecordSet &Ret_Projects, DbRecordSet &Ret_Breaks);
	void ModifyEventDateRange(Status &Ret_pStatus, int nCalEventPartID, int nBCOL_ID, QDateTime datFrom, QDateTime datTo);
	void DeleteEventPart(Status &Ret_pStatus, int nCalEventPartID, int nBCOL_ID, int nBCEV_ID);
	void DeleteEventParts(Status &Ret_pStatus, DbRecordSet recDeleteItemsData);
	void ReadReservation(Status &Ret_pStatus, int nEntityType, int nEntityID, QDateTime datStartDate, QDateTime datEndDate, DbRecordSet &Ret_Reservations);
	void ReadEventPartEntitesForOneEntity(Status &Ret_pStatus, DbRecordSet recFilter, int nEntityType, int nEntityID, QDateTime datStartDate, QDateTime datEndDate, DbRecordSet &Ret_Contacts, DbRecordSet &Ret_Resources, DbRecordSet &Ret_Projects);
	void ModifyEventPresence(Status &Ret_pStatus, QDateTime datFrom, QDateTime datTo, DbRecordSet recFilter, DbRecordSet lstAssignedEntities, int nPresenceTypeID);

	//Invite:
	void SetInviteStatus(Status &Ret_pStatus, int nCalEventID,int nPartID,int nInviteID,int nContactId_ConfirmedBy, int nPersonID_ConfirmedBy, int nStatus,int nOwnerID=-1,bool bNotifyOwner=false);
	void SendInviteBySokrates(Status &Ret_pStatus, int nCalEventID,DbRecordSet &DataInvite);
	void PrepareInviteRecordsForSendByEmail(Status &Ret_pStatus, DbRecordSet &RetOut_DataInvite, int nDefaultEmailTemplateID);
	void WriteInviteRecordsAfterSend(Status &Ret_pStatus, DbRecordSet &DataInvite,bool bOnlyUpdateSentStatus=false);
	void WriteInviteReplyStatus(Status &Ret_pStatus, DbRecordSet &RetOut_DataInvite, int nTypeOfMessage=0);

	//Synchronize
	void WriteFromOutlook(Status &Ret_pStatus, DbRecordSet &RetOut_Data, bool bSkipExisting,DbRecordSet &Ret_lstStatuses);
	void ReadForOutlook(Status &Ret_pStatus, DbRecordSet &Ret_Data, QDate datFrom, QDate datTo=QDate());

	//BT: make public I can use it from another object (great functions by the way)
	void GetEventPartContacts(Status &Ret_pStatus, DbRecordSet &recParts, DbRecordSet &Ret_Contacts);
	void GetEventPartResources(Status &Ret_pStatus, DbRecordSet &recParts, DbRecordSet &Ret_Resources);
	void GetEventPartProjects(Status &Ret_pStatus, DbRecordSet &recParts, DbRecordSet &Ret_Projects);
	void GetEventPartBreaks(Status &Ret_pStatus, DbRecordSet &recParts, DbRecordSet &Ret_Breaks);

private:
	void GetEventParts(Status &Ret_pStatus, DbRecordSet &Ret_EventParts, QString &strSQL, DbRecordSet &recFilter, QDateTime &datStartDate, QDateTime &datEndDate, int nEntityID=-1);
	void CreateNewCalendarEvent(Status &Ret_pStatus, DbRecordSet &DataInvite,DbRecordSet &Ret_Event,DbRecordSet &Ret_Parts,DbRecordSet &lstPersons);
	void AssignCalendarContacts(Status &Ret_pStatus, DbRecordSet &rowPart,DbRecordSet &lstContactIDs,DbRecordSet &lstPersons);
	void WriteEventOutlookID(Status &Ret_pStatus, int nPartID,QString strOutlookID);
	void DeleteEventPartPrivate(Status &Ret_pStatus, DbSimpleOrm &TableOrm_CalEvent, DbSimpleOrm &TableOrm_CalPart, DbSimpleOrm &TableOrm_CalOptions, int nCalEventPartID, int nBCOL_ID, int nBCEV_ID);
	void FindCalendarPartByOutlookID(Status &Ret_pStatus, QString strOutlookID, DbRecordSet &Ret_Data);
	void GetCalFilterViewEntitesAndTypeIDsData(Status &pStatus, DbRecordSet &recViews, DbRecordSet &recCalEntities, DbRecordSet &recCalTypeIDs);
};

#endif // SERVICE_BUSCALENDAR_H
