#ifndef SERVICE_BUSCOMMUNICATION_H
#define SERVICE_BUSCOMMUNICATION_H

#include "bus_interface/bus_interface/interface_buscommunication.h"
#include "businessservice.h"
#include "db/db/dbsqlquery.h"

class Service_BusCommunication : public Interface_BusCommunication, public BusinessService
{
public:
	~Service_BusCommunication();

	void SaveCommFilterViews(Status &Ret_pStatus, int &Ret_nViewID, DbRecordSet &recView, DbRecordSet &recViewsData);
	void GetDefaultDesktopFilterViews(Status &Ret_pStatus, int nLoggedPersonID, DbRecordSet &RetOut_recViews);
	void GetCommFilterViews(Status &Ret_pStatus, DbRecordSet &RetOut_recViews, int nPersonID, int nGridType);
	void GetCommFilterData(Status &Ret_pStatus, DbRecordSet &RetOut_recSortRecordSet, DbRecordSet &RetOut_recViewsData, DbRecordSet &RetOut_recViewsChBoxesData, int nViewID, int nLoggedPersonID);
	void GetCommFilterViewDataAndViews(Status &Ret_pStatus, DbRecordSet &RetOut_recViewData, DbRecordSet &RetOut_recViews, int nViewID, int nGridType);
	
	void GetGridViewForContact(Status &Ret_pStatus, int &RetOut_nViewID, int nContactID);
	void GetGridViewForProject(Status &Ret_pStatus, int &RetOut_nViewID, int nProjectID);

	void GetPersonCommData(Status &Ret_pStatus, bool &Ret_bRowsStripped, DbRecordSet &RetOut_recEmails, DbRecordSet &RetOut_recDocuments, DbRecordSet &RetOut_recVoiceCalls, int PersonID, DbRecordSet recFilter, bool bForceFilterByTaskType = false);
	void GetContactCommData(Status &Ret_pStatus, DbRecordSet &RetOut_recEmails, DbRecordSet &RetOut_recDocuments, DbRecordSet &RetOut_recVoiceCalls, int ContactID, DbRecordSet recFilter);
	void GetProjectCommData(Status &Ret_pStatus, DbRecordSet &RetOut_recEmails, DbRecordSet &RetOut_recDocuments, DbRecordSet &RetOut_recVoiceCalls, int nProjectID, DbRecordSet recFilter);
	void GetCalendarCommData(Status &Ret_pStatus, DbRecordSet &RetOut_recEmails, DbRecordSet &RetOut_recDocuments, DbRecordSet &RetOut_recVoiceCalls, DbRecordSet recEntities, DbRecordSet recFilter);
	void GetCalendarGridCommData(Status &Ret_pStatus, DbRecordSet &RetOut_recEmails, DbRecordSet &RetOut_recDocuments, DbRecordSet &RetOut_recVoiceCalls, DbRecordSet recEntities, DbRecordSet recFilter);
	
	void GetCommGridDataByIDs(Status &Ret_pStatus, DbRecordSet &RetOut_recData, DbRecordSet recIDsRecordset, int nGridType, int nEntityType);
	
	void GetComplexFilterData(Status &Ret_pStatus, DbRecordSet &RetOut_recFilterData, int nPersonID);
	
	// map from email -> contact ID (email import)
	void GetContactFromEmail(Status &Ret_pStatus, DbRecordSet &RetOut_lstData);
	
	void SetTaskIsDone(Status &Ret_pStatus, DbRecordSet &lstData);
	void PostPoneTasks(Status &Ret_pStatus, DbRecordSet &lstData, QDateTime datDueDateTime, QDateTime datStartDateTime);
	void SetEmailsRead(Status &Ret_pStatus, DbRecordSet &lstData);
	void ChangeEntityAssigment(Status &Ret_pStatus, int nAssignedType,int nAssignedID, int nOperation, DbRecordSet &Data);
	void ReadCEMenuData(Status &Ret_pStatus, int nPersonID, DbRecordSet &Ret_DocTemplate, DbRecordSet &Ret_DocApps, DbRecordSet &Ret_DocCheckOut,DbRecordSet &Ret_EmailTemplate);

	//Assign on drop:
	void AssignCommDataToContact(Status &Ret_pStatus, DbRecordSet &Data, int nContactID);
	void AssignCommDataToProject(Status &Ret_pStatus, DbRecordSet &Data, int nProjectID);

	//BT: 04.03.2013: Check if record exists:
	void CheckIfRecordExists(Status &Ret_pStatus, int nEntityType, int nRecordID, bool &Ret_bExists);

private:
	void GetPersonEmails(Status &Ret_pStatus, DbRecordSet &RetOut_recEmails, int nID, DbRecordSet recFilter, bool bForceFilterByTaskType);
	void GetContactEmails(Status &Ret_pStatus, DbRecordSet &RetOut_recEmails, int nID, DbRecordSet recFilter);
	void GetProjectEmails(Status &Ret_pStatus, DbRecordSet &RetOut_recEmails, int nID, DbRecordSet recFilter);
	void GetEmailContactData(Status &Ret_pStatus, DbRecordSet &RetOut_recEmails);
	
	void GetPersonDocuments(Status &Ret_pStatus, DbRecordSet &RetOut_recDocuments, int nID, DbRecordSet recFilter, bool bForceFilterByTaskType);
	void GetContactDocuments(Status &Ret_pStatus, DbRecordSet &RetOut_recDocuments, int nID, DbRecordSet recFilter);
	void GetProjectDocuments(Status &Ret_pStatus, DbRecordSet &RetOut_recDocuments, int nID, DbRecordSet recFilter);
	void GetDocumentsContactData(Status &Ret_pStatus, DbRecordSet &RetOut_recDocuments);
	
	void GetPersonVoiceCall(Status &Ret_pStatus, DbRecordSet &RetOut_recVoiceCalls, int nID,  DbRecordSet recFilter, bool bForceFilterByTaskType);
	void GetContactVoiceCall(Status &Ret_pStatus, DbRecordSet &RetOut_recVoiceCalls, int nID, DbRecordSet recFilter);
	void GetProjectVoiceCall(Status &Ret_pStatus, DbRecordSet &RetOut_recVoiceCalls, int nID, DbRecordSet recFilter);
	 
	void GetPersonEmailTasks(QString &strSQL, DbSqlQuery &query, Status &Ret_pStatus, DbRecordSet &RetOut_recEmails, QString &strPersonID, DbRecordSet &recFilter, bool &bForceFilterByTaskType, bool &bFilterByGrid, QDateTime &datFrom, QDateTime &datTo);

	bool GetPersonFilterSettingBool(int nSettingID, DbRecordSet &recFilter);
	int  GetPersonFilterSettingInt(int nSettingID, DbRecordSet &recFilter);
	QString GetPersonFilterSettingString(int nSettingID, DbRecordSet &recFilter);
	QDateTime GetPersonFilterSettingDateTime(int nSettingID, DbRecordSet &recFilter);
	DbRecordSet GetPersonFilterSettingRecordSet(int nSettingID, DbRecordSet &recFilter);
	void CheckForDoubleEntities(DbRecordSet &RetOut_RecordSet, DbRecordSet &tmpRecordSet);
	void CheckForDoubleEntitiesInContacts(DbRecordSet &RetOut_RecordSet);
	bool StringContainsText(QString strStringToSearch, QString strSearchText);
	bool HtmlContainsText(QString strHtmlToSearch, QString strSearchText);
	void FilterEmailsBySearchText(DbRecordSet &RetOut_recEmails, DbRecordSet recFilter);
	void FilterDocumentsBySearchText(DbRecordSet &RetOut_recDocuments, DbRecordSet recFilter);
	void FilterVoiceCallBySearchText(DbRecordSet &RetOut_recVoiceCalls, DbRecordSet recFilter);
	void TaskDateFilter(QDateTime &datFrom, QDateTime &datTo, DbRecordSet &recTasks);
	void StripEmailBodyToNCharacters(DbRecordSet &RetOut_recEmails, int nCharsNumber = 250);
	void StripDocumentBodyToNCharacters(DbRecordSet &RetOut_recDocuments, int nCharsNumber = 3072);
	QString GetTaskTypeFilterString(DbRecordSet &recFilter);
	void FilterByTaskType(DbRecordSet &recFilter, DbRecordSet &RetOut_RecordSet, bool bDeleteNonTasks = false);
	int GetEmailAttachmentCount(Status &Ret_pStatus, int nBEM_ID);
	void AddEmailAttachmentCountField(DbRecordSet &RetOut_recEmails);
	void CalculateEmailAttachmentCountField(Status &Ret_pStatus, DbRecordSet &RetOut_recEmails);
};

#endif // SERVICE_BUSCOMMUNICATION_H
