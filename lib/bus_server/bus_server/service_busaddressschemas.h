#ifndef SERVICE_BUSADDRESSSCHEMAS_H
#define SERVICE_BUSADDRESSSCHEMAS_H


#include "businessservice.h"
#include "bus_interface/bus_interface/interface_busaddressschemas.h"



/*!
	\class Service_BusAddressSchemas
	\brief Addr schemas
	\ingroup Bus_Services

*/

class Service_BusAddressSchemas : public Interface_BusAddressSchemas, public BusinessService
{
public:

	//basic ops:
	void Write(Status &Ret_pStatus, DbRecordSet &RetOut_Data,QString strLockRes);
	void Read(Status &Ret_pStatus, DbRecordSet &Ret_Data);
	void Delete(Status &Ret_pStatus, int nRecordID);
	void Lock(Status &Ret_pStatus, int nRecordID,QString &Ret_strLockRes);
	void Unlock(Status &Ret_pStatus, QString strLockRes);

};



#endif // SERVICE_BUSADDRESSSCHEMAS_H
