#include "service_webcommunication.h"

#include "bus_core/bus_core/commfiltersettingsids.h"
#include "commgridviewhelper_server.h"
#include "commgridviewtablehelper_server.h"
#include "common/common/datahelper.h"
#include "db/db/dbconnectionreserver.h"
#include "db/db/dbsimpleorm.h"
#include "bus_core/bus_core/usersessionmanager.h"
extern UserSessionManager	*g_UserSessionManager;
#include "businessserviceset.h"
extern BusinessServiceSet* g_BusinessServiceSet;
#include "bus_core/bus_core/useraccessright.h"
extern UserAccessRight *g_AccessRight;	


//Ret_Views: <ID, NAME, RANGE, USE_FROM_TO, DATE_FROM, DATE_TO> 
void Service_WebCommunication::ReadContactViews(Status &Ret_pStatus, DbRecordSet &Ret_Views)
{
	Ret_Views.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_WEBSERVICE_BUS_COMM_VIEWS));

	//Reserve connection.
	DbConnectionReserver conn(Ret_pStatus,GetDbManager()); //reserve global connection
	if(!Ret_pStatus.IsOK())return;

	//settings.
	DbSimpleOrm BUS_COMM_VIEW_SETTINGS_ORM(Ret_pStatus, BUS_COMM_VIEW_SETTINGS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;


	int nLoggedPersonID = g_UserSessionManager->GetPersonID();

	//Get views to display in view combo box - for currently logged user. Person is for later.
	DbRecordSet RetOut_recViews;
	g_BusinessServiceSet->BusCommunication->GetCommFilterViews(Ret_pStatus, RetOut_recViews, nLoggedPersonID, 1 /*contact grid type */);
	if(!Ret_pStatus.IsOK())return;



	int nSize=RetOut_recViews.getRowCount();
	Ret_Views.addRow(nSize);
	for (int i=0;i<nSize;i++)
	{
		DbRecordSet RetOut_recViewData;
		QString strWhere = QString(" WHERE BUSCS_SETTING_ID IN (%1,%2,%3,%4) AND BUSCS_VIEW_ID = %5").arg(FILTER_BY_DATE_ACTIVE).arg(FROM_DATE).arg(TO_DATE).arg(DATE_RANGE_SELECTOR).arg(RetOut_recViews.getDataRef(i,"BUSCV_ID").toInt());
		BUS_COMM_VIEW_SETTINGS_ORM.Read(Ret_pStatus, RetOut_recViewData, strWhere);
		if(!Ret_pStatus.IsOK()) return;

		//compile all data into format for web connect:
		Ret_Views.setData(i,"BUSCV_ID",RetOut_recViews.getDataRef(i,"BUSCV_ID").toInt());
		Ret_Views.setData(i,"BUSCV_NAME",RetOut_recViews.getDataRef(i,"BUSCV_NAME").toString());

		int nRangeSelector;
		int nRow=RetOut_recViewData.find("BUSCS_SETTING_ID",DATE_RANGE_SELECTOR,true);
		if (nRow>=0)
		{
			nRangeSelector=RetOut_recViewData.getDataRef(nRow,"BUSCS_VALUE").toInt();
			Ret_Views.setData(i,"DATE_RANGE",RetOut_recViewData.getDataRef(nRow,"BUSCS_VALUE").toInt());
		}
		else
		{
			nRangeSelector=DataHelper::UNDEFINED;
			Ret_Views.setData(i,"DATE_RANGE",DataHelper::UNDEFINED);
		}

		//Logic: if Dat ChkBox is set and from & date are valid and rangeselector<=0 then use only from & to
		//else if rangeselector is active, always use current date to span date range
		//if none is active then send all empty: use_date=empty, range=empty, get all records....
		//if range selector is use then override all other settings and set range on currentdate
		if (nRangeSelector>0)
		{
			//use current date range
			QDate datFrom; 
			QDate datTo;
			DataHelper::DateRangeSelectorCurrentButtonClicked(datFrom,datTo,nRangeSelector);
			QDateTime dtFrom = QDateTime(datFrom, QTime(0,0,0));
			QDateTime dtTo = QDateTime(datTo, QTime(23,59,59));

			Ret_Views.setData(i,"USE_FROM_TO",1);
			Ret_Views.setData(i,"DATE_FROM",dtFrom);
			Ret_Views.setData(i,"DATE_TO",dtTo);
		}
		else
		{
			nRow=RetOut_recViewData.find("BUSCS_SETTING_ID",FILTER_BY_DATE_ACTIVE,true);
			if (nRow>=0)
				Ret_Views.setData(i,"USE_FROM_TO",RetOut_recViewData.getDataRef(nRow,"BUSCS_VALUE").toInt());
			else
				Ret_Views.setData(i,"USE_FROM_TO",0);

			nRow=RetOut_recViewData.find("BUSCS_SETTING_ID",FROM_DATE,true);
			if (nRow>=0)
				Ret_Views.setData(i,"DATE_FROM",RetOut_recViewData.getDataRef(nRow,"BUSCS_VALUE_DATETIME").toDateTime());
			else
				Ret_Views.setData(i,"DATE_FROM",QDate::currentDate());

			nRow=RetOut_recViewData.find("BUSCS_SETTING_ID",TO_DATE,true);
			if (nRow>=0)
				Ret_Views.setData(i,"DATE_TO",RetOut_recViewData.getDataRef(nRow,"BUSCS_VALUE_DATETIME").toDateTime());
			else
				Ret_Views.setData(i,"DATE_TO",QDate::currentDate());

		}

	}

	//Ret_Views.Dump();
}


//nPrevNext=0 (current), -1 prev=1,-2 prev=2,1=next 1
void Service_WebCommunication::ReadContactViewData(Status &Ret_pStatus, int nContactID, DbRecordSet &Ret_Data, int &Ret_nTotalCount,int nViewID, int nUseOneColumn, int nUseFromTo, QDateTime datFrom, QDateTime datTo, int nPrevNext, int nRangeSelectorSelector, int nMaxRecords,QString &Ret_strFrom, QString &Ret_strTo)
{

	Ret_Data.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_WEBSERVICE_BUS_COMM_DATA));

	if (!datTo.isValid())
		datTo=QDateTime::currentDateTime();
	if (!datTo.isValid())
		datTo=QDateTime::currentDateTime();


	if (nMaxRecords<=0)nMaxRecords=500; //set default if not set

	//compile recFilter with data
	DbRecordSet RetOut_recViewData;
	int nLoggedPersonID = g_UserSessionManager->GetPersonID();

	//settings.
	DbSimpleOrm BUS_COMM_VIEW_SETTINGS_ORM(Ret_pStatus, BUS_COMM_VIEW_SETTINGS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	QString strWhere = QString(" WHERE BUSCS_VIEW_ID = %5").arg(nViewID);
	BUS_COMM_VIEW_SETTINGS_ORM.Read(Ret_pStatus, RetOut_recViewData, strWhere);
	if(!Ret_pStatus.IsOK()) return;

	QDateTime Ret_datFrom=datFrom;
	QDateTime Ret_datTo=datTo;

	//calculate date range if needed:
	if (nRangeSelectorSelector>0)
	{
		QDate datumFrom = datFrom.date();
		QDate datumTo = datTo.date();

		for (int i=0;i<qAbs(nPrevNext);i++)
		{
			if (nPrevNext<0)
				DataHelper::DateRangeSelectorPreviousNextButtonClicked(datumFrom, datumTo, nRangeSelectorSelector, -1);
			else
				DataHelper::DateRangeSelectorPreviousNextButtonClicked(datumFrom, datumTo, nRangeSelectorSelector, 1);
		}

		Ret_datFrom = QDateTime(datumFrom, QTime(0,0,0));
		Ret_datTo = QDateTime(datumTo, QTime(23,59,59));
	}

	if (nUseFromTo)
	{
		int nRow=RetOut_recViewData.find("BUSCS_SETTING_ID",FROM_DATE,true);
		if (nRow>0)
			RetOut_recViewData.setData(nRow,"BUSCS_VALUE_DATETIME",Ret_datFrom);

		nRow=RetOut_recViewData.find("BUSCS_SETTING_ID",TO_DATE,true);
		if (nRow>0)
			RetOut_recViewData.setData(nRow,"BUSCS_VALUE_DATETIME",Ret_datTo);

		if (Ret_datFrom.isValid())
			Ret_strFrom=Ret_datFrom.date().toString(Qt::SystemLocaleLongDate);
		else
			Ret_strFrom="";//QDate::currentDate().toString(Qt::SystemLocaleLongDate);

		if (Ret_datTo.isValid())
			Ret_strTo=Ret_datTo.date().toString(Qt::SystemLocaleLongDate);
		else
			Ret_strTo="";//QDate::currentDate().toString(Qt::SystemLocaleLongDate);
	}
	else
	{
			Ret_strFrom="";
			Ret_strTo="";
	}



	//void GetPersonCommData(Status &Ret_pStatus, DbRecordSet &RetOut_recEmails, DbRecordSet &RetOut_recDocuments, DbRecordSet &RetOut_recVoiceCalls, int PersonID, DbRecordSet recFilter, bool bForceFilterByTaskType = false);

	DbRecordSet RetOut_recEmails,RetOut_recDocuments,RetOut_recVoiceCalls;
	g_BusinessServiceSet->BusCommunication->GetContactCommData(Ret_pStatus, RetOut_recEmails, RetOut_recDocuments,RetOut_recVoiceCalls,nContactID, RetOut_recViewData);
	if(!Ret_pStatus.IsOK()) return;


	//compile list based on parameter nUseTwoColumns
	bool bSidebarMode=false;
	if (nUseOneColumn)
		bSidebarMode=true;
	
	CommGridViewTableHelper_Server tableViewHelper;
	int nRowCountEmails = RetOut_recEmails.getRowCount();
	int nRowCountVoiceCalls = RetOut_recVoiceCalls.getRowCount();
	int nRowCountDocuments = RetOut_recDocuments.getRowCount();
	int nRowCount = nRowCountEmails + nRowCountVoiceCalls + nRowCountDocuments;

	int nRowsInGrid = 0;
	for (int i = 0; i < nRowCountEmails; ++i)
	{
		//Fill model recordset.
		tableViewHelper.FirstColumnData((int)CommGridViewHelper::EMAIL, nRowsInGrid, RetOut_recEmails, i, bSidebarMode);
		tableViewHelper.SecondColumnData((int)CommGridViewHelper::EMAIL, RetOut_recEmails,i, nRowsInGrid, bSidebarMode);
		tableViewHelper.ThirdColumnData((int)CommGridViewHelper::EMAIL, nRowsInGrid, RetOut_recEmails, i, bSidebarMode);

		//Fill data and sorting hashes.
		tableViewHelper.FillRowHashes(nRowsInGrid, (int)CommGridViewHelper::EMAIL, RetOut_recEmails, i);

		//Increase row number.
		nRowsInGrid++;
	}

	for (int i = 0; i < nRowCountVoiceCalls; ++i)
	{
		//Fill model recordset.
		tableViewHelper.FirstColumnData((int)CommGridViewHelper::VOICECALL, nRowsInGrid, RetOut_recVoiceCalls, i, bSidebarMode);
		tableViewHelper.SecondColumnData((int)CommGridViewHelper::VOICECALL, RetOut_recVoiceCalls,i, nRowsInGrid, bSidebarMode);
		tableViewHelper.ThirdColumnData((int)CommGridViewHelper::VOICECALL, nRowsInGrid, RetOut_recVoiceCalls, i, bSidebarMode);

		//Fill data and sorting hashes.
		tableViewHelper.FillRowHashes(nRowsInGrid, (int)CommGridViewHelper::VOICECALL, RetOut_recVoiceCalls, i);

		//Increase row number.
		nRowsInGrid++;
	}

	for (int i = 0; i < nRowCountDocuments; ++i)
	{
		//Fill model recordset.
		tableViewHelper.FirstColumnData((int)CommGridViewHelper::DOCUMENT, nRowsInGrid, RetOut_recDocuments, i, bSidebarMode);
		tableViewHelper.SecondColumnData((int)CommGridViewHelper::DOCUMENT, RetOut_recDocuments,i, nRowsInGrid, bSidebarMode);
		tableViewHelper.ThirdColumnData((int)CommGridViewHelper::DOCUMENT, nRowsInGrid, RetOut_recDocuments, i, bSidebarMode);

		//Fill data and sorting hashes.
		tableViewHelper.FillRowHashes(nRowsInGrid, (int)CommGridViewHelper::DOCUMENT, RetOut_recDocuments, i);

		//m_recDocument->Dump();

		//Increase row number.
		nRowsInGrid++;
	}

	DbRecordSet recApplications;
	Status err;
	CommGridViewHelper_Server m_CmGridHelper;
	m_CmGridHelper.GetDocApplicationsFromServer(recApplications, err);
	QHash<int, QString> hshDocIconNames = CommGridViewHelper::GetDocumentTypeIconNames();

	//Sort table, initialize and set model.
	tableViewHelper.SortTableData(nRowCount, CommGridViewHelper::DESKTOP_GRID);

	tableViewHelper.CheckAndFixIcon3AndIcon4(recApplications, hshDocIconNames);

	/*
	// Data to send
	tableViewHelper.m_hshRow2Color;
	tableViewHelper.m_hshRow2Pix1;				//sidebar
	tableViewHelper.m_hshRow2Pix2;				//sidebar
	tableViewHelper.m_hshRow2Pix3;				//sidebar
	tableViewHelper.m_hshRow2Pix4;				//sidebar
	tableViewHelper.m_hshRow2Date1;				//sidebar
	tableViewHelper.m_hshRow2Date2;
	tableViewHelper.m_hshRow2Autor;
	tableViewHelper.m_hshRow2Subject;			//sidebar
	tableViewHelper.m_hshRow2Descr;				//sidebar
	tableViewHelper.m_hshRow2SecondColDisplay;
	tableViewHelper.m_hshRow2Owner;
	tableViewHelper.m_hshRow2Contact;			//sidebar
	tableViewHelper.m_hshRow2Project;
	tableViewHelper.m_hshRow2TaskColor;			//sidebar
	tableViewHelper.m_hshRow2TaskIcon;
	tableViewHelper.m_hshRow2Pix4ByteArray;		//sidebar
	tableViewHelper.m_hshRow2TaskPriority;		//sidebar
	*/

	//ide
	//  m_hshDate1 m_hshSubject m_hshDescription m_hshContact m_hshRow2TaskPriority m_hshRow2TaskColor;  	m_hshRow2Pix1; m_hshRow2Pix2; m_hshRow2Pix3; m_hshRow2Pix4;


	int nSize=tableViewHelper.m_hshRow2CENT_ID.size();
	Ret_nTotalCount=nSize;

	if (nSize>nMaxRecords) //limit the size
		nSize=nMaxRecords;

	Ret_Data.addRow(nSize);
	for (int i=0;i<nSize;i++)
	{
		int nCentID=tableViewHelper.m_hshRow2CENT_ID.value(i);
		Ret_Data.setData(i,"CENT_ID",nCentID);					
		Ret_Data.setData(i,"BTKS_ID",tableViewHelper.m_hshRow2BTKS_ID.value(i));					

		Ret_Data.setData(i,"Pix1",QString(tableViewHelper.m_hshRow2Pix1.value(i)).replace(":","/images/"));	
		Ret_Data.setData(i,"Pix2",QString(tableViewHelper.m_hshRow2Pix2.value(i)).replace(":","/images/"));	
		Ret_Data.setData(i,"Pix3",QString(tableViewHelper.m_hshRow2Pix3.value(i)).replace(":","/images/"));	
		Ret_Data.setData(i,"Pix4",QString(tableViewHelper.m_hshRow2Pix4.value(i)).replace(":","/images/"));	

		QString strSubject=DataHelper::ReturnCommGridTextFromHTML(tableViewHelper.m_hshRow2Subject.value(i));
		Ret_Data.setData(i,"Subject",strSubject);
		Ret_Data.setData(i,"Contact",tableViewHelper.m_hshRow2Contact.value(i));

		QByteArray byte=tableViewHelper.m_hshRow2TaskIcon.value(i);
		Ret_Data.setData(i,"TaskIcon",byte);
		QByteArray byte1=tableViewHelper.m_hshRow2Pix4ByteArray.value(i);
		Ret_Data.setData(i,"Pix4ByteArray",byte1);

		Ret_Data.setData(i,"TaskColor",tableViewHelper.m_hshRow2TaskColor.value(i));
		Ret_Data.setData(i,"TaskPriority",tableViewHelper.m_hshRow2TaskPriority.value(i));

		//if sidebar then..
		if (bSidebarMode)
		{
			Ret_Data.setData(i,"Description",strSubject);					//sidebar
		}
		else
		{
			Ret_Data.setData(i,"Color",tableViewHelper.m_hshRow2Color.value(i));
			Ret_Data.setData(i,"Date1",tableViewHelper.m_hshRow2Date1.value(i));
			Ret_Data.setData(i,"Date2",tableViewHelper.m_hshRow2Date2.value(i));
			Ret_Data.setData(i,"Autor",tableViewHelper.m_hshRow2Autor.value(i));
			
			//PM: 02.04.2012 
			//qDebug()<<DataHelper::ReturnCommGridTextFromHTML(tableViewHelper.m_hshRow2SecondColDisplay.value(i));
			QString strTempSubjectLine2 = DataHelper::ReturnCommGridTextFromHTML(tableViewHelper.m_hshRow2SecondColDisplay.value(i));
			if (strTempSubjectLine2.indexOf(strSubject)==0)
			{
				strTempSubjectLine2 = strTempSubjectLine2.mid(strSubject.length());
			}
			//Ret_Data.setData(i,"SecondColDisplay",DataHelper::ReturnCommGridTextFromHTML(tableViewHelper.m_hshRow2SecondColDisplay.value(i)));
			Ret_Data.setData(i,"SecondColDisplay",strTempSubjectLine2);

			Ret_Data.setData(i,"Owner",tableViewHelper.m_hshRow2Owner.value(i));
			Ret_Data.setData(i,"Project",tableViewHelper.m_hshRow2Project.value(i));
		}


		//test by CENT ID and set document type and id:
		if (RetOut_recDocuments.getColumnCount()>0)
		{
			int nRow=RetOut_recDocuments.find("CENT_ID",nCentID,true);
			if (nRow>=0)
			{
				Ret_Data.setData(i,"Document_Type",CommGridViewHelper::DOCUMENT);
				Ret_Data.setData(i,"Document_ID",RetOut_recDocuments.getDataRef(nRow,"BDMD_ID").toInt());

				if (RetOut_recDocuments.getDataRef(nRow,"BDMD_DOC_TYPE").toInt()==GlobalConstants::DOC_TYPE_URL)
				{
					Ret_Data.setData(i,"EXTENSION","_URL");
					Ret_Data.setData(i,"URL_PATH",RetOut_recDocuments.getDataRef(nRow,"BDMD_DOC_PATH"));
					
				}
				else if (RetOut_recDocuments.getDataRef(nRow,"BDMD_DOC_TYPE").toInt()==GlobalConstants::DOC_TYPE_NOTE)
				{
					Ret_Data.setData(i,"EXTENSION","_NOTE");
				}
				else if (!RetOut_recDocuments.getDataRef(nRow,"BDMD_DOC_PATH").toString().isEmpty())
				{
					QFileInfo info(RetOut_recDocuments.getDataRef(nRow,"BDMD_DOC_PATH").toString());
					Ret_Data.setData(i,"EXTENSION",info.suffix());
				}
				else
				{
					//it's ok:-> file is probably some other file
					//Q_ASSERT(false); //zovi BT
				}


			}
		}

		if (RetOut_recEmails.getColumnCount()>0)
		{
			int nRow=RetOut_recEmails.find("CENT_ID",nCentID,true);
			if (nRow>=0)
			{
				Ret_Data.setData(i,"Document_Type",CommGridViewHelper::EMAIL);
				Ret_Data.setData(i,"Document_ID",RetOut_recEmails.getDataRef(nRow,"BEM_ID").toInt());
			}
		}

		if (RetOut_recVoiceCalls.getColumnCount()>0)
		{
			int nRow=RetOut_recVoiceCalls.find("CENT_ID",nCentID,true);
			if (nRow>=0)
			{
				Ret_Data.setData(i,"Document_Type",CommGridViewHelper::VOICECALL);
				Ret_Data.setData(i,"Document_ID",RetOut_recVoiceCalls.getDataRef(nRow,"BVC_ID").toInt());
			}
		}
	}



	//Ret_Data.Dump();

}




//Ret_Views: <ID, NAME, RANGE, USE_FROM_TO, DATE_FROM, DATE_TO> 
void Service_WebCommunication::ReadUserViews(Status &Ret_pStatus, DbRecordSet &Ret_Views)
{
	Ret_Views.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_WEBSERVICE_BUS_COMM_VIEWS));

	//Reserve connection.
	DbConnectionReserver conn(Ret_pStatus,GetDbManager()); //reserve global connection
	if(!Ret_pStatus.IsOK())return;

	//settings.
	DbSimpleOrm BUS_COMM_VIEW_SETTINGS_ORM(Ret_pStatus, BUS_COMM_VIEW_SETTINGS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	
	int nLoggedPersonID = g_UserSessionManager->GetPersonID();

	//Get views to display in view combo box - for currently logged user. Person is for later.
	DbRecordSet RetOut_recViews;
	g_BusinessServiceSet->BusCommunication->GetCommFilterViews(Ret_pStatus, RetOut_recViews, nLoggedPersonID, 0 /*desktop grid type */);
	if(!Ret_pStatus.IsOK())return;



	int nSize=RetOut_recViews.getRowCount();
	Ret_Views.addRow(nSize);
	for (int i=0;i<nSize;i++)
	{
		DbRecordSet RetOut_recViewData;
		QString strWhere = QString(" WHERE BUSCS_SETTING_ID IN (%1,%2,%3,%4) AND BUSCS_VIEW_ID = %5").arg(FILTER_BY_DATE_ACTIVE).arg(FROM_DATE).arg(TO_DATE).arg(DATE_RANGE_SELECTOR).arg(RetOut_recViews.getDataRef(i,"BUSCV_ID").toInt());
		BUS_COMM_VIEW_SETTINGS_ORM.Read(Ret_pStatus, RetOut_recViewData, strWhere);
		if(!Ret_pStatus.IsOK()) return;

		//compile all data into format for web connect:
		Ret_Views.setData(i,"BUSCV_ID",RetOut_recViews.getDataRef(i,"BUSCV_ID").toInt());
		Ret_Views.setData(i,"BUSCV_NAME",RetOut_recViews.getDataRef(i,"BUSCV_NAME").toString());

		int nRangeSelector;
		int nRow=RetOut_recViewData.find("BUSCS_SETTING_ID",DATE_RANGE_SELECTOR,true);
		if (nRow>=0)
		{
			nRangeSelector=RetOut_recViewData.getDataRef(nRow,"BUSCS_VALUE").toInt();
			Ret_Views.setData(i,"DATE_RANGE",RetOut_recViewData.getDataRef(nRow,"BUSCS_VALUE").toInt());
		}
		else
		{
			nRangeSelector=DataHelper::UNDEFINED;
			Ret_Views.setData(i,"DATE_RANGE",DataHelper::UNDEFINED);
		}

		//Logic: if Dat ChkBox is set and from & date are valid and rangeselector<=0 then use only from & to
		//else if rangeselector is active, always use current date to span date range
		//if none is active then send all empty: use_date=empty, range=empty, get all records....
		//if range selector is use then override all other settings and set range on currentdate
		if (nRangeSelector>0)
		{
			//use current date range
			QDate datFrom; 
			QDate datTo;
			DataHelper::DateRangeSelectorCurrentButtonClicked(datFrom,datTo,nRangeSelector);
			QDateTime dtFrom = QDateTime(datFrom, QTime(0,0,0));
			QDateTime dtTo = QDateTime(datTo, QTime(23,59,59));

			Ret_Views.setData(i,"USE_FROM_TO",1);
			Ret_Views.setData(i,"DATE_FROM",dtFrom);
			Ret_Views.setData(i,"DATE_TO",dtTo);
		}
		else
		{
			nRow=RetOut_recViewData.find("BUSCS_SETTING_ID",FILTER_BY_DATE_ACTIVE,true);
			if (nRow>=0)
				Ret_Views.setData(i,"USE_FROM_TO",RetOut_recViewData.getDataRef(nRow,"BUSCS_VALUE").toInt());
			else
				Ret_Views.setData(i,"USE_FROM_TO",0);

			nRow=RetOut_recViewData.find("BUSCS_SETTING_ID",FROM_DATE,true);
			if (nRow>=0)
				Ret_Views.setData(i,"DATE_FROM",RetOut_recViewData.getDataRef(nRow,"BUSCS_VALUE_DATETIME").toDateTime());
			else
				Ret_Views.setData(i,"DATE_FROM",QDate::currentDate());

			nRow=RetOut_recViewData.find("BUSCS_SETTING_ID",TO_DATE,true);
			if (nRow>=0)
				Ret_Views.setData(i,"DATE_TO",RetOut_recViewData.getDataRef(nRow,"BUSCS_VALUE_DATETIME").toDateTime());
			else
				Ret_Views.setData(i,"DATE_TO",QDate::currentDate());

		}

	}

	//Ret_Views.Dump();
}


//nPrevNext=0 (current), -1 prev=1,-2 prev=2,1=next 1
void Service_WebCommunication::ReadUserViewData(Status &Ret_pStatus, int nUserID, DbRecordSet &Ret_Data, int &Ret_nTotalCount,int nViewID, int nUseOneColumn, int nUseFromTo, QDateTime datFrom, QDateTime datTo, int nPrevNext, int nRangeSelectorSelector, int nMaxRecords,QString &Ret_strFrom, QString &Ret_strTo)
{

	Ret_Data.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_WEBSERVICE_BUS_COMM_DATA));

	if (!datTo.isValid())
		datTo=QDateTime::currentDateTime();
	if (!datTo.isValid())
		datTo=QDateTime::currentDateTime();

	//if -1 then loggedn persona
	if (nUserID==-1)
		nUserID = g_UserSessionManager->GetPersonID();

	if (nMaxRecords<=0)nMaxRecords=500; //set default if not set

	//compile recFilter with data
	DbRecordSet RetOut_recViewData;

	//settings.
	DbSimpleOrm BUS_COMM_VIEW_SETTINGS_ORM(Ret_pStatus, BUS_COMM_VIEW_SETTINGS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	QString strWhere = QString(" WHERE BUSCS_VIEW_ID = %5").arg(nViewID);
	BUS_COMM_VIEW_SETTINGS_ORM.Read(Ret_pStatus, RetOut_recViewData, strWhere);
	if(!Ret_pStatus.IsOK()) return;

	QDateTime Ret_datFrom=datFrom;
	QDateTime Ret_datTo=datTo;

	//calculate date range if needed:
	if (nRangeSelectorSelector>0)
	{
		QDate datumFrom = datFrom.date();
		QDate datumTo = datTo.date();

		for (int i=0;i<qAbs(nPrevNext);i++)
		{
			if (nPrevNext<0)
				DataHelper::DateRangeSelectorPreviousNextButtonClicked(datumFrom, datumTo, nRangeSelectorSelector, -1);
			else
				DataHelper::DateRangeSelectorPreviousNextButtonClicked(datumFrom, datumTo, nRangeSelectorSelector, 1);
		}

		Ret_datFrom = QDateTime(datumFrom, QTime(0,0,0));
		Ret_datTo = QDateTime(datumTo, QTime(23,59,59));
	}

	if (nUseFromTo)
	{
		int nRow=RetOut_recViewData.find("BUSCS_SETTING_ID",FROM_DATE,true);
		if (nRow>0)
			RetOut_recViewData.setData(nRow,"BUSCS_VALUE_DATETIME",Ret_datFrom);

		nRow=RetOut_recViewData.find("BUSCS_SETTING_ID",TO_DATE,true);
		if (nRow>0)
			RetOut_recViewData.setData(nRow,"BUSCS_VALUE_DATETIME",Ret_datTo);

		if (Ret_datFrom.isValid())
			Ret_strFrom=Ret_datFrom.date().toString(Qt::SystemLocaleLongDate);
		else
			Ret_strFrom="";//QDate::currentDate().toString(Qt::SystemLocaleLongDate);

		if (Ret_datTo.isValid())
			Ret_strTo=Ret_datTo.date().toString(Qt::SystemLocaleLongDate);
		else
			Ret_strTo="";//QDate::currentDate().toString(Qt::SystemLocaleLongDate);
	}
	else
	{
			Ret_strFrom="";
			Ret_strTo="";
	}



	//void GetPersonCommData(Status &Ret_pStatus, DbRecordSet &RetOut_recEmails, DbRecordSet &RetOut_recDocuments, DbRecordSet &RetOut_recVoiceCalls, int PersonID, DbRecordSet recFilter, bool bForceFilterByTaskType = false);

	DbRecordSet RetOut_recEmails,RetOut_recDocuments,RetOut_recVoiceCalls;
	bool bRowsStripped = false;
	g_BusinessServiceSet->BusCommunication->GetPersonCommData(Ret_pStatus, bRowsStripped, RetOut_recEmails, RetOut_recDocuments,RetOut_recVoiceCalls,nUserID, RetOut_recViewData);
	if(!Ret_pStatus.IsOK()) return;


	//compile list based on parameter nUseTwoColumns
	bool bSidebarMode=false;
	if (nUseOneColumn)
		bSidebarMode=true;
	
	CommGridViewTableHelper_Server tableViewHelper;
	int nRowCountEmails = RetOut_recEmails.getRowCount();
	int nRowCountVoiceCalls = RetOut_recVoiceCalls.getRowCount();
	int nRowCountDocuments = RetOut_recDocuments.getRowCount();
	int nRowCount = nRowCountEmails + nRowCountVoiceCalls + nRowCountDocuments;

	int nRowsInGrid = 0;
	for (int i = 0; i < nRowCountEmails; ++i)
	{
		//Fill model recordset.
		tableViewHelper.FirstColumnData((int)CommGridViewHelper::EMAIL, nRowsInGrid, RetOut_recEmails, i, bSidebarMode);
		tableViewHelper.SecondColumnData((int)CommGridViewHelper::EMAIL, RetOut_recEmails,i, nRowsInGrid, bSidebarMode);
		tableViewHelper.ThirdColumnData((int)CommGridViewHelper::EMAIL, nRowsInGrid, RetOut_recEmails, i, bSidebarMode);

		//Fill data and sorting hashes.
		tableViewHelper.FillRowHashes(nRowsInGrid, (int)CommGridViewHelper::EMAIL, RetOut_recEmails, i);

		//Increase row number.
		nRowsInGrid++;
	}

	for (int i = 0; i < nRowCountVoiceCalls; ++i)
	{
		//Fill model recordset.
		tableViewHelper.FirstColumnData((int)CommGridViewHelper::VOICECALL, nRowsInGrid, RetOut_recVoiceCalls, i, bSidebarMode);
		tableViewHelper.SecondColumnData((int)CommGridViewHelper::VOICECALL, RetOut_recVoiceCalls,i, nRowsInGrid, bSidebarMode);
		tableViewHelper.ThirdColumnData((int)CommGridViewHelper::VOICECALL, nRowsInGrid, RetOut_recVoiceCalls, i, bSidebarMode);

		//Fill data and sorting hashes.
		tableViewHelper.FillRowHashes(nRowsInGrid, (int)CommGridViewHelper::VOICECALL, RetOut_recVoiceCalls, i);

		//Increase row number.
		nRowsInGrid++;
	}

	for (int i = 0; i < nRowCountDocuments; ++i)
	{
		//Fill model recordset.
		tableViewHelper.FirstColumnData((int)CommGridViewHelper::DOCUMENT, nRowsInGrid, RetOut_recDocuments, i, bSidebarMode);
		tableViewHelper.SecondColumnData((int)CommGridViewHelper::DOCUMENT, RetOut_recDocuments,i, nRowsInGrid, bSidebarMode);
		tableViewHelper.ThirdColumnData((int)CommGridViewHelper::DOCUMENT, nRowsInGrid, RetOut_recDocuments, i, bSidebarMode);

		//Fill data and sorting hashes.
		tableViewHelper.FillRowHashes(nRowsInGrid, (int)CommGridViewHelper::DOCUMENT, RetOut_recDocuments, i);

		//m_recDocument->Dump();

		//Increase row number.
		nRowsInGrid++;
	}

	DbRecordSet recApplications;
	Status err;
	CommGridViewHelper_Server m_CmGridHelper;
	m_CmGridHelper.GetDocApplicationsFromServer(recApplications, err);
	QHash<int, QString> hshDocIconNames = CommGridViewHelper::GetDocumentTypeIconNames();

	//Sort table, initialize and set model.
	tableViewHelper.SortTableData(nRowCount, CommGridViewHelper::DESKTOP_GRID);

	tableViewHelper.CheckAndFixIcon3AndIcon4(recApplications, hshDocIconNames);

	/*
	// Data to send
	tableViewHelper.m_hshRow2Color;
	tableViewHelper.m_hshRow2Pix1;				//sidebar
	tableViewHelper.m_hshRow2Pix2;				//sidebar
	tableViewHelper.m_hshRow2Pix3;				//sidebar
	tableViewHelper.m_hshRow2Pix4;				//sidebar
	tableViewHelper.m_hshRow2Date1;				//sidebar
	tableViewHelper.m_hshRow2Date2;
	tableViewHelper.m_hshRow2Autor;
	tableViewHelper.m_hshRow2Subject;			//sidebar
	tableViewHelper.m_hshRow2Descr;				//sidebar
	tableViewHelper.m_hshRow2SecondColDisplay;
	tableViewHelper.m_hshRow2Owner;
	tableViewHelper.m_hshRow2Contact;			//sidebar
	tableViewHelper.m_hshRow2Project;
	tableViewHelper.m_hshRow2TaskColor;			//sidebar
	tableViewHelper.m_hshRow2TaskIcon;
	tableViewHelper.m_hshRow2Pix4ByteArray;		//sidebar
	tableViewHelper.m_hshRow2TaskPriority;		//sidebar
	*/
	//ide
	//  m_hshDate1 m_hshSubject m_hshDescription m_hshContact m_hshRow2TaskPriority m_hshRow2TaskColor;  	m_hshRow2Pix1; m_hshRow2Pix2; m_hshRow2Pix3; m_hshRow2Pix4;


	int nSize=tableViewHelper.m_hshRow2CENT_ID.size();
	Ret_nTotalCount=nSize;

	if (nSize>nMaxRecords) //limit the size
		nSize=nMaxRecords;

	Ret_Data.addRow(nSize);
	for (int i=0;i<nSize;i++)
	{
		int nCentID=tableViewHelper.m_hshRow2CENT_ID.value(i);
		Ret_Data.setData(i,"CENT_ID",nCentID);					
		Ret_Data.setData(i,"BTKS_ID",tableViewHelper.m_hshRow2BTKS_ID.value(i));					

		Ret_Data.setData(i,"Pix1",QString(tableViewHelper.m_hshRow2Pix1.value(i)).replace(":","/images/"));	
		Ret_Data.setData(i,"Pix2",QString(tableViewHelper.m_hshRow2Pix2.value(i)).replace(":","/images/"));	
		Ret_Data.setData(i,"Pix3",QString(tableViewHelper.m_hshRow2Pix3.value(i)).replace(":","/images/"));	
		Ret_Data.setData(i,"Pix4",QString(tableViewHelper.m_hshRow2Pix4.value(i)).replace(":","/images/"));	

		QString strSubject=DataHelper::ReturnCommGridTextFromHTML(tableViewHelper.m_hshRow2Subject.value(i));
		Ret_Data.setData(i,"Subject",strSubject);
		Ret_Data.setData(i,"Contact",tableViewHelper.m_hshRow2Contact.value(i));

		QByteArray byte=tableViewHelper.m_hshRow2TaskIcon.value(i);
		Ret_Data.setData(i,"TaskIcon",byte);
		QByteArray byte1=tableViewHelper.m_hshRow2Pix4ByteArray.value(i);
		Ret_Data.setData(i,"Pix4ByteArray",byte1);

		Ret_Data.setData(i,"TaskColor",tableViewHelper.m_hshRow2TaskColor.value(i));
		Ret_Data.setData(i,"TaskPriority",tableViewHelper.m_hshRow2TaskPriority.value(i));

		//if sidebar then..
		if (bSidebarMode)
		{
			Ret_Data.setData(i,"Description",strSubject);					//sidebar
		}
		else
		{
			Ret_Data.setData(i,"Color",tableViewHelper.m_hshRow2Color.value(i));
			Ret_Data.setData(i,"Date1",tableViewHelper.m_hshRow2Date1.value(i));
			Ret_Data.setData(i,"Date2",tableViewHelper.m_hshRow2Date2.value(i));
			Ret_Data.setData(i,"Autor",tableViewHelper.m_hshRow2Autor.value(i));
			
			//PM: 02.04.2012 
			//qDebug()<<DataHelper::ReturnCommGridTextFromHTML(tableViewHelper.m_hshRow2SecondColDisplay.value(i));
			QString strTempSubjectLine2 = DataHelper::ReturnCommGridTextFromHTML(tableViewHelper.m_hshRow2SecondColDisplay.value(i));
			if (strTempSubjectLine2.indexOf(strSubject)==0)
			{
				strTempSubjectLine2 = strTempSubjectLine2.mid(strSubject.length());
			}
			//Ret_Data.setData(i,"SecondColDisplay",DataHelper::ReturnCommGridTextFromHTML(tableViewHelper.m_hshRow2SecondColDisplay.value(i)));
			Ret_Data.setData(i,"SecondColDisplay",strTempSubjectLine2);

			Ret_Data.setData(i,"Owner",tableViewHelper.m_hshRow2Owner.value(i));
			Ret_Data.setData(i,"Project",tableViewHelper.m_hshRow2Project.value(i));
		}


	

		if (RetOut_recDocuments.getColumnCount()>0)
		{
			int nRow=RetOut_recDocuments.find("CENT_ID",nCentID,true);
			if (nRow>=0)
			{
				//test by CENT ID and set document type and id:
				//DbRecordSet rowX=RetOut_recDocuments.getRow(nRow);
				//rowX.Dump();

				Ret_Data.setData(i,"Document_Type",CommGridViewHelper::DOCUMENT);
				Ret_Data.setData(i,"Document_ID",RetOut_recDocuments.getDataRef(nRow,"BDMD_ID").toInt());

				if (RetOut_recDocuments.getDataRef(nRow,"BDMD_DOC_TYPE").toInt()==GlobalConstants::DOC_TYPE_URL)
				{
					Ret_Data.setData(i,"EXTENSION","_URL");
					Ret_Data.setData(i,"URL_PATH",RetOut_recDocuments.getDataRef(nRow,"BDMD_DOC_PATH"));
				}
				else if (RetOut_recDocuments.getDataRef(nRow,"BDMD_DOC_TYPE").toInt()==GlobalConstants::DOC_TYPE_NOTE)
				{
						Ret_Data.setData(i,"EXTENSION","_NOTE");
				}
				else if (!RetOut_recDocuments.getDataRef(nRow,"BDMD_DOC_PATH").toString().isEmpty())
				{
					QFileInfo info(RetOut_recDocuments.getDataRef(nRow,"BDMD_DOC_PATH").toString());
					Ret_Data.setData(i,"EXTENSION",info.suffix());
				}
				else
				{
					//it's ok:-> file is probably some other file
					//Q_ASSERT(false); //zovi BT
				}
			}
		}

		if (RetOut_recEmails.getColumnCount()>0)
		{
			int nRow=RetOut_recEmails.find("CENT_ID",nCentID,true);
			if (nRow>=0)
			{
				Ret_Data.setData(i,"Document_Type",CommGridViewHelper::EMAIL);
				Ret_Data.setData(i,"Document_ID",RetOut_recEmails.getDataRef(nRow,"BEM_ID").toInt());
			}
		}

		if (RetOut_recVoiceCalls.getColumnCount()>0)
		{
			int nRow=RetOut_recVoiceCalls.find("CENT_ID",nCentID,true);
			if (nRow>=0)
			{
				Ret_Data.setData(i,"Document_Type",CommGridViewHelper::VOICECALL);
				Ret_Data.setData(i,"Document_ID",RetOut_recVoiceCalls.getDataRef(nRow,"BVC_ID").toInt());
			}
		}
	}



	//Ret_Data.Dump();

}




//Ret_Views: <ID, NAME, RANGE, USE_FROM_TO, DATE_FROM, DATE_TO> 
void Service_WebCommunication::ReadProjectViews(Status &Ret_pStatus, DbRecordSet &Ret_Views)
{
	Ret_Views.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_WEBSERVICE_BUS_COMM_VIEWS));

	//Reserve connection.
	DbConnectionReserver conn(Ret_pStatus,GetDbManager()); //reserve global connection
	if(!Ret_pStatus.IsOK())return;

	//settings.
	DbSimpleOrm BUS_COMM_VIEW_SETTINGS_ORM(Ret_pStatus, BUS_COMM_VIEW_SETTINGS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;


	int nLoggedPersonID = g_UserSessionManager->GetPersonID();

	//Get views to display in view combo box - for currently logged user. Person is for later.
	DbRecordSet RetOut_recViews;
	g_BusinessServiceSet->BusCommunication->GetCommFilterViews(Ret_pStatus, RetOut_recViews, nLoggedPersonID, 2 /*project grid type */);
	if(!Ret_pStatus.IsOK())return;



	int nSize=RetOut_recViews.getRowCount();
	Ret_Views.addRow(nSize);
	for (int i=0;i<nSize;i++)
	{
		DbRecordSet RetOut_recViewData;
		QString strWhere = QString(" WHERE BUSCS_SETTING_ID IN (%1,%2,%3,%4) AND BUSCS_VIEW_ID = %5").arg(FILTER_BY_DATE_ACTIVE).arg(FROM_DATE).arg(TO_DATE).arg(DATE_RANGE_SELECTOR).arg(RetOut_recViews.getDataRef(i,"BUSCV_ID").toInt());
		BUS_COMM_VIEW_SETTINGS_ORM.Read(Ret_pStatus, RetOut_recViewData, strWhere);
		if(!Ret_pStatus.IsOK()) return;

		//compile all data into format for web connect:
		Ret_Views.setData(i,"BUSCV_ID",RetOut_recViews.getDataRef(i,"BUSCV_ID").toInt());
		Ret_Views.setData(i,"BUSCV_NAME",RetOut_recViews.getDataRef(i,"BUSCV_NAME").toString());

		int nRangeSelector;
		int nRow=RetOut_recViewData.find("BUSCS_SETTING_ID",DATE_RANGE_SELECTOR,true);
		if (nRow>=0)
		{
			nRangeSelector=RetOut_recViewData.getDataRef(nRow,"BUSCS_VALUE").toInt();
			Ret_Views.setData(i,"DATE_RANGE",RetOut_recViewData.getDataRef(nRow,"BUSCS_VALUE").toInt());
		}
		else
		{
			nRangeSelector=DataHelper::UNDEFINED;
			Ret_Views.setData(i,"DATE_RANGE",DataHelper::UNDEFINED);
		}

		//Logic: if Dat ChkBox is set and from & date are valid and rangeselector<=0 then use only from & to
		//else if rangeselector is active, always use current date to span date range
		//if none is active then send all empty: use_date=empty, range=empty, get all records....
		//if range selector is use then override all other settings and set range on currentdate
		if (nRangeSelector>0)
		{
			//use current date range
			QDate datFrom; 
			QDate datTo;
			DataHelper::DateRangeSelectorCurrentButtonClicked(datFrom,datTo,nRangeSelector);
			QDateTime dtFrom = QDateTime(datFrom, QTime(0,0,0));
			QDateTime dtTo = QDateTime(datTo, QTime(23,59,59));

			Ret_Views.setData(i,"USE_FROM_TO",1);
			Ret_Views.setData(i,"DATE_FROM",dtFrom);
			Ret_Views.setData(i,"DATE_TO",dtTo);
		}
		else
		{
			nRow=RetOut_recViewData.find("BUSCS_SETTING_ID",FILTER_BY_DATE_ACTIVE,true);
			if (nRow>=0)
				Ret_Views.setData(i,"USE_FROM_TO",RetOut_recViewData.getDataRef(nRow,"BUSCS_VALUE").toInt());
			else
				Ret_Views.setData(i,"USE_FROM_TO",0);

			nRow=RetOut_recViewData.find("BUSCS_SETTING_ID",FROM_DATE,true);
			if (nRow>=0)
				Ret_Views.setData(i,"DATE_FROM",RetOut_recViewData.getDataRef(nRow,"BUSCS_VALUE_DATETIME").toDateTime());
			else
				Ret_Views.setData(i,"DATE_FROM",QDate::currentDate());

			nRow=RetOut_recViewData.find("BUSCS_SETTING_ID",TO_DATE,true);
			if (nRow>=0)
				Ret_Views.setData(i,"DATE_TO",RetOut_recViewData.getDataRef(nRow,"BUSCS_VALUE_DATETIME").toDateTime());
			else
				Ret_Views.setData(i,"DATE_TO",QDate::currentDate());

		}

	}

	//Ret_Views.Dump();
}


//nPrevNext=0 (current), -1 prev=1,-2 prev=2,1=next 1
void Service_WebCommunication::ReadProjectViewData(Status &Ret_pStatus, int nProjectID, DbRecordSet &Ret_Data, int &Ret_nTotalCount,int nViewID, int nUseOneColumn, int nUseFromTo, QDateTime datFrom, QDateTime datTo, int nPrevNext, int nRangeSelectorSelector, int nMaxRecords,QString &Ret_strFrom, QString &Ret_strTo)
{

	Ret_Data.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_WEBSERVICE_BUS_COMM_DATA));

	if (!datTo.isValid())
		datTo=QDateTime::currentDateTime();
	if (!datTo.isValid())
		datTo=QDateTime::currentDateTime();


	if (nMaxRecords<=0)nMaxRecords=500; //set default if not set

	//compile recFilter with data
	DbRecordSet RetOut_recViewData;

	//settings.
	DbSimpleOrm BUS_COMM_VIEW_SETTINGS_ORM(Ret_pStatus, BUS_COMM_VIEW_SETTINGS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	QString strWhere = QString(" WHERE BUSCS_VIEW_ID = %5").arg(nViewID);
	BUS_COMM_VIEW_SETTINGS_ORM.Read(Ret_pStatus, RetOut_recViewData, strWhere);
	if(!Ret_pStatus.IsOK()) return;

	QDateTime Ret_datFrom=datFrom;
	QDateTime Ret_datTo=datTo;

	//calculate date range if needed:
	if (nRangeSelectorSelector>0)
	{
		QDate datumFrom = datFrom.date();
		QDate datumTo = datTo.date();

		for (int i=0;i<qAbs(nPrevNext);i++)
		{
			if (nPrevNext<0)
				DataHelper::DateRangeSelectorPreviousNextButtonClicked(datumFrom, datumTo, nRangeSelectorSelector, -1);
			else
				DataHelper::DateRangeSelectorPreviousNextButtonClicked(datumFrom, datumTo, nRangeSelectorSelector, 1);
		}

		Ret_datFrom = QDateTime(datumFrom, QTime(0,0,0));
		Ret_datTo = QDateTime(datumTo, QTime(23,59,59));
	}

	if (nUseFromTo)
	{
		int nRow=RetOut_recViewData.find("BUSCS_SETTING_ID",FROM_DATE,true);
		if (nRow>0)
			RetOut_recViewData.setData(nRow,"BUSCS_VALUE_DATETIME",Ret_datFrom);

		nRow=RetOut_recViewData.find("BUSCS_SETTING_ID",TO_DATE,true);
		if (nRow>0)
			RetOut_recViewData.setData(nRow,"BUSCS_VALUE_DATETIME",Ret_datTo);

		if (Ret_datFrom.isValid())
			Ret_strFrom=Ret_datFrom.date().toString(Qt::SystemLocaleLongDate);
		else
			Ret_strFrom="";//QDate::currentDate().toString(Qt::SystemLocaleLongDate);

		if (Ret_datTo.isValid())
			Ret_strTo=Ret_datTo.date().toString(Qt::SystemLocaleLongDate);
		else
			Ret_strTo="";//QDate::currentDate().toString(Qt::SystemLocaleLongDate);
	}
	else
	{
		Ret_strFrom="";
		Ret_strTo="";
	}



	//void GetPersonCommData(Status &Ret_pStatus, DbRecordSet &RetOut_recEmails, DbRecordSet &RetOut_recDocuments, DbRecordSet &RetOut_recVoiceCalls, int PersonID, DbRecordSet recFilter, bool bForceFilterByTaskType = false);

	DbRecordSet RetOut_recEmails,RetOut_recDocuments,RetOut_recVoiceCalls;
	g_BusinessServiceSet->BusCommunication->GetProjectCommData(Ret_pStatus, RetOut_recEmails, RetOut_recDocuments,RetOut_recVoiceCalls,nProjectID, RetOut_recViewData);
	if(!Ret_pStatus.IsOK()) return;


	//compile list based on parameter nUseTwoColumns
	bool bSidebarMode=false;
	if (nUseOneColumn)
		bSidebarMode=true;

	CommGridViewTableHelper_Server tableViewHelper;
	int nRowCountEmails = RetOut_recEmails.getRowCount();
	int nRowCountVoiceCalls = RetOut_recVoiceCalls.getRowCount();
	int nRowCountDocuments = RetOut_recDocuments.getRowCount();
	int nRowCount = nRowCountEmails + nRowCountVoiceCalls + nRowCountDocuments;

	int nRowsInGrid = 0;
	for (int i = 0; i < nRowCountEmails; ++i)
	{
		//Fill model recordset.
		tableViewHelper.FirstColumnData((int)CommGridViewHelper::EMAIL, nRowsInGrid, RetOut_recEmails, i, bSidebarMode);
		tableViewHelper.SecondColumnData((int)CommGridViewHelper::EMAIL, RetOut_recEmails,i, nRowsInGrid, bSidebarMode);
		tableViewHelper.ThirdColumnData((int)CommGridViewHelper::EMAIL, nRowsInGrid, RetOut_recEmails, i, bSidebarMode);

		//Fill data and sorting hashes.
		tableViewHelper.FillRowHashes(nRowsInGrid, (int)CommGridViewHelper::EMAIL, RetOut_recEmails, i);

		//Increase row number.
		nRowsInGrid++;
	}

	for (int i = 0; i < nRowCountVoiceCalls; ++i)
	{
		//Fill model recordset.
		tableViewHelper.FirstColumnData((int)CommGridViewHelper::VOICECALL, nRowsInGrid, RetOut_recVoiceCalls, i, bSidebarMode);
		tableViewHelper.SecondColumnData((int)CommGridViewHelper::VOICECALL, RetOut_recVoiceCalls,i, nRowsInGrid, bSidebarMode);
		tableViewHelper.ThirdColumnData((int)CommGridViewHelper::VOICECALL, nRowsInGrid, RetOut_recVoiceCalls, i, bSidebarMode);

		//Fill data and sorting hashes.
		tableViewHelper.FillRowHashes(nRowsInGrid, (int)CommGridViewHelper::VOICECALL, RetOut_recVoiceCalls, i);

		//Increase row number.
		nRowsInGrid++;
	}

	for (int i = 0; i < nRowCountDocuments; ++i)
	{
		//Fill model recordset.
		tableViewHelper.FirstColumnData((int)CommGridViewHelper::DOCUMENT, nRowsInGrid, RetOut_recDocuments, i, bSidebarMode);
		tableViewHelper.SecondColumnData((int)CommGridViewHelper::DOCUMENT, RetOut_recDocuments,i, nRowsInGrid, bSidebarMode);
		tableViewHelper.ThirdColumnData((int)CommGridViewHelper::DOCUMENT, nRowsInGrid, RetOut_recDocuments, i, bSidebarMode);

		//Fill data and sorting hashes.
		tableViewHelper.FillRowHashes(nRowsInGrid, (int)CommGridViewHelper::DOCUMENT, RetOut_recDocuments, i);

		//m_recDocument->Dump();

		//Increase row number.
		nRowsInGrid++;
	}

	DbRecordSet recApplications;
	Status err;
	CommGridViewHelper_Server m_CmGridHelper;
	m_CmGridHelper.GetDocApplicationsFromServer(recApplications, err);
	QHash<int, QString> hshDocIconNames = CommGridViewHelper::GetDocumentTypeIconNames();

	//Sort table, initialize and set model.
	tableViewHelper.SortTableData(nRowCount, CommGridViewHelper::DESKTOP_GRID);

	tableViewHelper.CheckAndFixIcon3AndIcon4(recApplications, hshDocIconNames);

	/*
	// Data to send
	tableViewHelper.m_hshRow2Color;
	tableViewHelper.m_hshRow2Pix1;				//sidebar
	tableViewHelper.m_hshRow2Pix2;				//sidebar
	tableViewHelper.m_hshRow2Pix3;				//sidebar
	tableViewHelper.m_hshRow2Pix4;				//sidebar
	tableViewHelper.m_hshRow2Date1;				//sidebar
	tableViewHelper.m_hshRow2Date2;
	tableViewHelper.m_hshRow2Autor;
	tableViewHelper.m_hshRow2Subject;			//sidebar
	tableViewHelper.m_hshRow2Descr;				//sidebar
	tableViewHelper.m_hshRow2SecondColDisplay;
	tableViewHelper.m_hshRow2Owner;
	tableViewHelper.m_hshRow2Contact;			//sidebar
	tableViewHelper.m_hshRow2Project;
	tableViewHelper.m_hshRow2TaskColor;			//sidebar
	tableViewHelper.m_hshRow2TaskIcon;
	tableViewHelper.m_hshRow2Pix4ByteArray;		//sidebar
	tableViewHelper.m_hshRow2TaskPriority;		//sidebar
	*/

	//ide
	//  m_hshDate1 m_hshSubject m_hshDescription m_hshContact m_hshRow2TaskPriority m_hshRow2TaskColor;  	m_hshRow2Pix1; m_hshRow2Pix2; m_hshRow2Pix3; m_hshRow2Pix4;


	int nSize=tableViewHelper.m_hshRow2CENT_ID.size();
	Ret_nTotalCount=nSize;

	if (nSize>nMaxRecords) //limit the size
		nSize=nMaxRecords;

	Ret_Data.addRow(nSize);
	for (int i=0;i<nSize;i++)
	{
		int nCentID=tableViewHelper.m_hshRow2CENT_ID.value(i);
		Ret_Data.setData(i,"CENT_ID",nCentID);					
		Ret_Data.setData(i,"BTKS_ID",tableViewHelper.m_hshRow2BTKS_ID.value(i));					

		Ret_Data.setData(i,"Pix1",QString(tableViewHelper.m_hshRow2Pix1.value(i)).replace(":","/images/"));	
		Ret_Data.setData(i,"Pix2",QString(tableViewHelper.m_hshRow2Pix2.value(i)).replace(":","/images/"));	
		Ret_Data.setData(i,"Pix3",QString(tableViewHelper.m_hshRow2Pix3.value(i)).replace(":","/images/"));	
		Ret_Data.setData(i,"Pix4",QString(tableViewHelper.m_hshRow2Pix4.value(i)).replace(":","/images/"));	

		QString strSubject=DataHelper::ReturnCommGridTextFromHTML(tableViewHelper.m_hshRow2Subject.value(i));
		Ret_Data.setData(i,"Subject",strSubject);
		Ret_Data.setData(i,"Contact",tableViewHelper.m_hshRow2Contact.value(i));

		QByteArray byte=tableViewHelper.m_hshRow2TaskIcon.value(i);
		Ret_Data.setData(i,"TaskIcon",byte);
		QByteArray byte1=tableViewHelper.m_hshRow2Pix4ByteArray.value(i);
		Ret_Data.setData(i,"Pix4ByteArray",byte1);

		Ret_Data.setData(i,"TaskColor",tableViewHelper.m_hshRow2TaskColor.value(i));
		Ret_Data.setData(i,"TaskPriority",tableViewHelper.m_hshRow2TaskPriority.value(i));

		//if sidebar then..
		if (bSidebarMode)
		{
			Ret_Data.setData(i,"Description",strSubject);					//sidebar
		}
		else
		{
			Ret_Data.setData(i,"Color",tableViewHelper.m_hshRow2Color.value(i));
			Ret_Data.setData(i,"Date1",tableViewHelper.m_hshRow2Date1.value(i));
			Ret_Data.setData(i,"Date2",tableViewHelper.m_hshRow2Date2.value(i));
			Ret_Data.setData(i,"Autor",tableViewHelper.m_hshRow2Autor.value(i));
			//PM: 02.04.2012 
			//qDebug()<<DataHelper::ReturnCommGridTextFromHTML(tableViewHelper.m_hshRow2SecondColDisplay.value(i));
			QString strTempSubjectLine2 = DataHelper::ReturnCommGridTextFromHTML(tableViewHelper.m_hshRow2SecondColDisplay.value(i));
			if (strTempSubjectLine2.indexOf(strSubject)==0)
			{
				strTempSubjectLine2 = strTempSubjectLine2.mid(strSubject.length());
			}
			//Ret_Data.setData(i,"SecondColDisplay",DataHelper::ReturnCommGridTextFromHTML(tableViewHelper.m_hshRow2SecondColDisplay.value(i)));
			Ret_Data.setData(i,"SecondColDisplay",strTempSubjectLine2);

			Ret_Data.setData(i,"Owner",tableViewHelper.m_hshRow2Owner.value(i));
			Ret_Data.setData(i,"Project",tableViewHelper.m_hshRow2Project.value(i));
		}


		//test by CENT ID and set document type and id:
		if (RetOut_recDocuments.getColumnCount()>0)
		{
			int nRow=RetOut_recDocuments.find("CENT_ID",nCentID,true);
			if (nRow>=0)
			{
				Ret_Data.setData(i,"Document_Type",CommGridViewHelper::DOCUMENT);
				Ret_Data.setData(i,"Document_ID",RetOut_recDocuments.getDataRef(nRow,"BDMD_ID").toInt());

				if (RetOut_recDocuments.getDataRef(nRow,"BDMD_DOC_TYPE").toInt()==GlobalConstants::DOC_TYPE_URL)
				{
					Ret_Data.setData(i,"EXTENSION","_URL");
					Ret_Data.setData(i,"URL_PATH",RetOut_recDocuments.getDataRef(nRow,"BDMD_DOC_PATH"));
				}
				else if (RetOut_recDocuments.getDataRef(nRow,"BDMD_DOC_TYPE").toInt()==GlobalConstants::DOC_TYPE_NOTE)
				{
					Ret_Data.setData(i,"EXTENSION","_NOTE");
				}
				else if (!RetOut_recDocuments.getDataRef(nRow,"BDMD_DOC_PATH").toString().isEmpty())
				{
					QFileInfo info(RetOut_recDocuments.getDataRef(nRow,"BDMD_DOC_PATH").toString());
					Ret_Data.setData(i,"EXTENSION",info.suffix());
				}
				else
				{
					//it's ok:-> file is probably some other file
					//Q_ASSERT(false); //zovi BT
				}
			}
		}

		if (RetOut_recEmails.getColumnCount()>0)
		{
			int nRow=RetOut_recEmails.find("CENT_ID",nCentID,true);
			if (nRow>=0)
			{
				Ret_Data.setData(i,"Document_Type",CommGridViewHelper::EMAIL);
				Ret_Data.setData(i,"Document_ID",RetOut_recEmails.getDataRef(nRow,"BEM_ID").toInt());
			}
		}

		if (RetOut_recVoiceCalls.getColumnCount()>0)
		{
			int nRow=RetOut_recVoiceCalls.find("CENT_ID",nCentID,true);
			if (nRow>=0)
			{
				Ret_Data.setData(i,"Document_Type",CommGridViewHelper::VOICECALL);
				Ret_Data.setData(i,"Document_ID",RetOut_recVoiceCalls.getDataRef(nRow,"BVC_ID").toInt());
			}
		}
	}



	//Ret_Data.Dump();

}


void Service_WebCommunication::ReadCommEntityCount(Status &Ret_pStatus, int nUserID, int &Ret_nFileDocCount, int &Ret_nWebSiteCount, int &Ret_nNotesCount,int &Ret_nEmailCount)
{

	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_DM_DOCUMENTS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	DbSqlQuery *query=TableOrm.GetDbSqlQuery();

	QString strSQL=" INNER JOIN CE_COMM_ENTITY ON BDMD_COMM_ENTITY_ID = CENT_ID";
	strSQL+=QString(" AND CENT_OWNER_ID=")+QString::number(nUserID);


	//doc file:
	QString strExec="SELECT COUNT(*) FROM BUS_DM_DOCUMENTS "+strSQL+" WHERE BDMD_DOC_TYPE =" +QString::number(GlobalConstants::DOC_TYPE_INTERNET_FILE);
	g_AccessRight->SQLFilterRecords(BUS_DM_DOCUMENTS,strExec);
	query->Execute(Ret_pStatus,strExec);
	if(!Ret_pStatus.IsOK()) return;
	if(query->next())
		Ret_nFileDocCount=query->value(0).toInt();
	else
		Ret_nFileDocCount=0;

	//web file:
	strExec="SELECT COUNT(*) FROM BUS_DM_DOCUMENTS "+strSQL+" WHERE BDMD_DOC_TYPE =" +QString::number(GlobalConstants::DOC_TYPE_URL);
	g_AccessRight->SQLFilterRecords(BUS_DM_DOCUMENTS,strExec);
	query->Execute(Ret_pStatus,strExec);
	if(!Ret_pStatus.IsOK()) return;
	if(query->next())
		Ret_nWebSiteCount=query->value(0).toInt();
	else
		Ret_nWebSiteCount=0;

	//note file:
	strExec="SELECT COUNT(*) FROM BUS_DM_DOCUMENTS "+strSQL+" WHERE BDMD_DOC_TYPE =" +QString::number(GlobalConstants::DOC_TYPE_NOTE);
	g_AccessRight->SQLFilterRecords(BUS_DM_DOCUMENTS,strExec);
	query->Execute(Ret_pStatus,strExec);
	if(!Ret_pStatus.IsOK()) return;
	if(query->next())
		Ret_nNotesCount=query->value(0).toInt();
	else
		Ret_nNotesCount=0;


	//emails:
	strSQL=" INNER JOIN CE_COMM_ENTITY ON BEM_COMM_ENTITY_ID = CENT_ID";
	strSQL+=QString(" AND CENT_OWNER_ID=")+QString::number(nUserID);

	strExec="SELECT COUNT(*) FROM BUS_EMAIL "+strSQL+" WHERE (BEM_TEMPLATE_FLAG=0 OR BEM_TEMPLATE_FLAG IS NULL)";
	g_AccessRight->SQLFilterRecords(BUS_EMAIL,strExec);
	query->Execute(Ret_pStatus,strExec);
	if(!Ret_pStatus.IsOK()) return;
	if(query->next())
		Ret_nEmailCount=query->value(0).toInt();
	else
		Ret_nEmailCount=0;


}


void Service_WebCommunication::ReadUserDocuments(Status &Ret_pStatus, int &Ret_nTotalCount,DbRecordSet &Ret_Documents, int nUserID,int nDocType, int nFromN, int nToN)
{
	g_BusinessServiceSet->WebDocuments->SearchDocuments(Ret_pStatus,Ret_nTotalCount,Ret_Documents,nDocType,nFromN,nToN,-1,-1,nUserID);
}
void Service_WebCommunication::ReadUserEmails(Status &Ret_pStatus, int &Ret_nTotalCount, DbRecordSet &Ret_Emails, int nUserID, int nFromN, int nToN, int nSortOrder, QString strEmailAcc, QString strEmailSender, int nMailBox)
{
	QDate empty;
	g_BusinessServiceSet->WebEmails->SearchEmails(Ret_pStatus,Ret_nTotalCount,Ret_Emails,nSortOrder,nFromN,nToN,-1,-1,nUserID,empty, empty, strEmailAcc, strEmailSender,nMailBox);
}