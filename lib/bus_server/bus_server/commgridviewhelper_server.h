#ifndef COMMGRIDVIEWHELPER_SERVER_H
#define COMMGRIDVIEWHELPER_SERVER_H

#include <QObject>
#include "bus_interface/bus_interface/interface_buscommunication.h"
#include "businessservice.h"
#include "bus_core/bus_core/commgridviewhelper.h"

class CommGridViewHelper_Server : public CommGridViewHelper, public BusinessService
{
	Q_OBJECT

public:
	void GetDocApplicationsFromServer(DbRecordSet &lstData, Status &err);

private:
	DbRecordSet m_lstApplicationsCache; //please use this cache inside one thread/bocall
};

#endif // COMMGRIDVIEWHELPER_SERVER_H
