#include "service_buscalendar.h"
//#include "db/db/dbsimpleorm.h"
#include "bus_core/bus_core/globalconstants.h"
#include "bus_server/bus_server/businesslocker.h"
#include "common/common/entity_id_collection.h"
#include "bus_core/bus_core/calendarhelpercore.h"
#include "bus_core/bus_core/contacttypemanager.h"
#include "bus_core/bus_core/mainentityfilter.h"

#include "bus_core/bus_core/useraccessright.h"
extern UserAccessRight *g_AccessRight;				//global access right tester
#include "businessserviceset.h"
extern BusinessServiceSet* g_BusinessServiceSet;
#include "privateserviceset.h"
extern PrivateServiceSet* g_PrivateServiceSet; 
#include "bus_core/bus_core/usersessionmanager.h"
extern UserSessionManager	*g_UserSessionManager;
#include "bus_core/bus_core/messagedispatcher.h"
extern MessageDispatcher* g_MessageDispatcher;
#include "trans/trans/xmlutil.h"
#include "bus_core/bus_core/mainentitycalculatedname.h"

#define EXIT_IF_ERROR_AND_ROLLBACK  \
									if(!Ret_pStatus.IsOK())\
									{\
										TableOrm_CalEvent.GetDbSqlQuery()->Rollback();\
										return;\
									}

void Service_BusCalendar::SaveCalFilterView(Status &Ret_pStatus, int &Ret_nViewID, DbRecordSet recView, DbRecordSet recCalEntities, DbRecordSet recCalTypeIDs)
{
	DbSimpleOrm BUS_CAL_VIEW_ORM(Ret_pStatus, BUS_CAL_VIEW, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	DbSimpleOrm BUS_CAL_VIEW_ENTITIES_ORM(Ret_pStatus, BUS_CAL_VIEW_ENTITIES, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	DbSimpleOrm BUS_CAL_VIEW_CE_TYPES_ORM(Ret_pStatus, BUS_CAL_VIEW_CE_TYPES, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

//_DUMP(recView);
//_DUMP(recCalEntities);
//_DUMP(recCalTypeIDs);

	//--------------------------------------
	//CAL_VIEW_ORM START TRANSACTION
	//--------------------------------------
	BUS_CAL_VIEW_ORM.GetDbSqlQuery()->BeginTransaction(Ret_pStatus);
	if(!Ret_pStatus.IsOK()) return;

	//Write view.
	BUS_CAL_VIEW_ORM.Write(Ret_pStatus, recView);
	if (!Ret_pStatus.IsOK())
	{
		BUS_CAL_VIEW_ORM.GetDbSqlQuery()->Rollback();
		return;
	}

	Ret_nViewID = recView.getDataRef(0, 0).toInt();

	//Set calendar view ID to entities.
	int nEntitiesRowCount=recCalEntities.getRowCount();
	for (int i=0; i<nEntitiesRowCount; i++)
	{
		QVariant intNull(QVariant::Int);
		recCalEntities.setData(i, "BCVE_ID", intNull);
		recCalEntities.setData(i, "BCVE_CAL_VIEW_ID", Ret_nViewID);
	}

	//Set calendar view ID to calendar type IDs.
	int nTypeIDsCount=recCalTypeIDs.getRowCount();
	for (int i=0; i<nTypeIDsCount; i++)
	{
		QVariant intNull(QVariant::Int);
		recCalTypeIDs.setData(i, "BCCT_ID", intNull);
		recCalTypeIDs.setData(i, "BCCT_CAL_VIEW_ID", Ret_nViewID);
	}

	//_DUMP(recCalEntities);
	//_DUMP(recCalTypeIDs);

	if (nEntitiesRowCount)
	{
		BUS_CAL_VIEW_ENTITIES_ORM.DeleteFromParentIDs(Ret_pStatus,"BCVE_CAL_VIEW_ID",recView,"BCALV_ID");
		if(!Ret_pStatus.IsOK())
		{
			BUS_CAL_VIEW_ORM.GetDbSqlQuery()->Rollback();
			return;
		}

		//Write entities.
		BUS_CAL_VIEW_ENTITIES_ORM.Write(Ret_pStatus, recCalEntities);
		if (!Ret_pStatus.IsOK())
		{
			BUS_CAL_VIEW_ORM.GetDbSqlQuery()->Rollback();
			return;
		}
	}

	if (nTypeIDsCount)
	{
		BUS_CAL_VIEW_CE_TYPES_ORM.DeleteFromParentIDs(Ret_pStatus,"BCCT_CAL_VIEW_ID",recView,"BCALV_ID");
		if(!Ret_pStatus.IsOK())
		{
			BUS_CAL_VIEW_ORM.GetDbSqlQuery()->Rollback();
			return;
		}

		//Write type id's.
		BUS_CAL_VIEW_CE_TYPES_ORM.Write(Ret_pStatus, recCalTypeIDs);
		if (!Ret_pStatus.IsOK())
		{
			BUS_CAL_VIEW_ORM.GetDbSqlQuery()->Rollback();
			return;
		}

	}

	//--------------------------------------
	//CAL_VIEW_ORM COMMIT TRANSACTION
	//--------------------------------------
	BUS_CAL_VIEW_ORM.GetDbSqlQuery()->Commit(Ret_pStatus);
}

void Service_BusCalendar::GetCalFilterViews(Status &Ret_pStatus, DbRecordSet &RetOut_recViews, int nPersonID, int nGridType /*= 0*/)
{
	//Contact data Table ORM.
	DbSimpleOrm BUS_CAL_VIEW_ORM(Ret_pStatus, BUS_CAL_VIEW, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	QString strPersonID = QVariant(nPersonID).toString();
	QString strGridType = QVariant(nGridType).toString();
	QString strWhere;
	if(-1 == nGridType)	//list all views regardless of the grid type
		strWhere = " WHERE (BCALV_OWNER_ID = " + strPersonID + " OR (BCALV_IS_PUBLIC =1 AND BCALV_OWNER_ID <> " + strPersonID + ")) ORDER BY BCALV_NAME";
	else
		strWhere = " WHERE BCALV_TYPE = " + strGridType + " AND (BCALV_OWNER_ID = " + strPersonID + " OR (BCALV_IS_PUBLIC =1 AND BCALV_OWNER_ID <> " + strPersonID + ")) ORDER BY BCALV_NAME";

	//Read.
	BUS_CAL_VIEW_ORM.Read(Ret_pStatus, RetOut_recViews, strWhere);
	if(!Ret_pStatus.IsOK()) return;

	//_DUMP(RetOut_recViews);
}

void Service_BusCalendar::GetCalFilterViewsWithEntitesAndTypes(Status &Ret_pStatus, DbRecordSet &RetOut_recViews, DbRecordSet &RetOut_recCalEntities, DbRecordSet &RetOut_recCalTypeIDs, int nPersonID, int nGridType /*= 0*/)
{
	GetCalFilterViews(Ret_pStatus, RetOut_recViews, nPersonID, nGridType);
	GetCalFilterViewEntitesAndTypeIDsData(Ret_pStatus, RetOut_recViews, RetOut_recCalEntities, RetOut_recCalTypeIDs);
}

void Service_BusCalendar::GetCalFilterView(Status &Ret_pStatus, DbRecordSet &RetOut_recViewsData, DbRecordSet &RetOut_recCalEntities, DbRecordSet &RetOut_recCalTypeIDs, int nViewID)
{
	//Contact data Table ORM.
	DbSimpleOrm BUS_CAL_VIEW_ORM(Ret_pStatus, BUS_CAL_VIEW, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	QString strViewID = QVariant(nViewID).toString();
	QString strWhere = " WHERE BCALV_ID = " + strViewID;

	//Read.
	BUS_CAL_VIEW_ORM.Read(Ret_pStatus, RetOut_recViewsData, strWhere);
	if(!Ret_pStatus.IsOK()) return;

	//_DUMP(RetOut_recViewsData);

	GetCalFilterViewEntitesAndTypeIDsData(Ret_pStatus, RetOut_recViewsData, RetOut_recCalEntities, RetOut_recCalTypeIDs);
}

void Service_BusCalendar::DeleteCalFilterView(Status &Ret_pStatus, int nViewID)
{
	//Contact data Table ORM.
	DbSimpleOrm BUS_CAL_VIEW_ORM(Ret_pStatus, BUS_CAL_VIEW, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	QList<int> lstDelete;
	lstDelete << nViewID;

	//Read.
	BUS_CAL_VIEW_ORM.DeleteFast(Ret_pStatus, lstDelete);
	if(!Ret_pStatus.IsOK()) return;
}
/*
void Service_BusCalendar::CopyCalendarFilterData(int nRowToCopyFrom, DbRecordSet &recEntities, DbRecordSet &recViewToCopyTo)
{
	recViewToCopyTo.addRow();
	int nRow = recViewToCopyTo.getRowCount()-1;
	recViewToCopyTo.setData(nRow, "ENTITY_ID", recEntities.getDataRef(nRowToCopyFrom, "ENTITY_ID").toInt());
	recViewToCopyTo.setData(nRow, "ENTITY_TYPE", recEntities.getDataRef(nRowToCopyFrom, "ENTITY_TYPE").toInt());
	recViewToCopyTo.setData(nRow, "ENTITY_NAME", recEntities.getDataRef(nRowToCopyFrom, "ENTITY_NAME").toString());
	recViewToCopyTo.setData(nRow, "ENTITY_PERSON_ID", recEntities.getDataRef(nRowToCopyFrom, "ENTITY_PERSON_ID").toInt());
}

void Service_BusCalendar::CleanupCalFilterData(Status &Ret_pStatus, DbRecordSet &RetOut_recViews)	
{
	//MR: fix issue #2533 
	//dynamically cleanup all views to remove non-existing zombie entities
	int nViewCnt = RetOut_recViews.getRowCount();
	for(int i=0; i<nViewCnt; i++)
	{
		// cleanup single view
		bool bModified = false;
		DbRecordSet recView = RetOut_recViews.getRow(i);
		
		//unpack settings field
		QByteArray bytSettings = recView.getDataRef(0, "BCALV_VALUE_BYTE").toByteArray();
		DbRecordSet recSettings = XmlUtil::ConvertByteArray2RecordSet_Fast(bytSettings);

//		_DUMP(recSettings);

		//now extract the list of entities in the calendar
		QByteArray bytEntities = recSettings.getDataRef(0, "CALENDAR_ENTITIES").toByteArray();
		DbRecordSet recEntities = XmlUtil::ConvertByteArray2RecordSet_Fast(bytEntities);

		DbRecordSet recUserEntities, recProjectEntities, recContactEntities, recResourceEntities;
		recUserEntities.copyDefinition(recEntities);
		recProjectEntities.copyDefinition(recEntities);
		recContactEntities.copyDefinition(recEntities);
		recResourceEntities.copyDefinition(recEntities);

		int nRows = recEntities.getRowCount();
		for(int j=0; j<nRows; j++)
		{
			recEntities.clearSelection();
			recEntities.selectRow(j);
			if(recEntities.getDataRef(j, "ENTITY_TYPE").toInt()==GlobalConstants::TYPE_CAL_USER_SELECT)
			{
				recUserEntities.merge(recEntities, true);
				//CopyCalendarFilterData(j, recEntities, recUserEntities);
			}
			else if (recEntities.getDataRef(j, "ENTITY_TYPE").toInt()==GlobalConstants::TYPE_CAL_PROJECT_SELECT)
			{
				recProjectEntities.merge(recEntities, true);
				//CopyCalendarFilterData(j, recEntities, recProjectEntities);
			}
			else if (recEntities.getDataRef(j, "ENTITY_TYPE").toInt()==GlobalConstants::TYPE_CAL_CONTACT_SELECT)
			{
				recContactEntities.merge(recEntities, true);
				//CopyCalendarFilterData(j, recEntities, recContactEntities);
			}
			else if (recEntities.getDataRef(j, "ENTITY_TYPE").toInt()==GlobalConstants::TYPE_CAL_RESOURCE_SELECT)
			{
				recResourceEntities.merge(recEntities, true);
				//CopyCalendarFilterData(j, recEntities, recResourceEntities);
			}

			CleanupCalendarViewFilterData(Ret_pStatus, bModified, recUserEntities, ENTITY_BUS_PERSON);
			CleanupCalendarViewFilterData(Ret_pStatus, bModified, recProjectEntities, ENTITY_BUS_PROJECT);
			CleanupCalendarViewFilterData(Ret_pStatus, bModified, recContactEntities, ENTITY_BUS_CONTACT);
			CleanupCalendarViewFilterData(Ret_pStatus, bModified, recResourceEntities, ENTITY_CALENDAR_RESOURCES);
		}

		if(bModified)
		{
			//pack entitites
			recEntities.clear();
			recEntities.merge(recUserEntities);
			recEntities.merge(recProjectEntities);
			recEntities.merge(recContactEntities);
			recEntities.merge(recResourceEntities);
			bytEntities = XmlUtil::ConvertRecordSet2ByteArray_Fast(recEntities);
			recSettings.setData(0, "CALENDAR_ENTITIES", bytEntities);

			//pack settings
			//Check is currently selected row in calendar higher than entities number.
			if (recSettings.getDataRef(0, "CALENDAR_CURRENT_ENTITY").toInt() >= recEntities.getRowCount())
			{
				recSettings.setData(0, "CALENDAR_CURRENT_ENTITY",0);
			}

			bytSettings = XmlUtil::ConvertRecordSet2ByteArray_Fast(recSettings);
			recView.setData(0, "BCALV_VALUE_BYTE", bytSettings);

			int nViewID = recView.getDataRef(0, "BCALV_ID").toInt();
			SaveCalFilterView(Ret_pStatus, nViewID, recView);
			
			RetOut_recViews.assignRow(i, recView);
		}
	}
}

void Service_BusCalendar::CleanupCalendarViewFilterData(Status &Ret_pStatus, bool &bModified, DbRecordSet &ViewsData, int nEntityType)
{
	int nRowCount=ViewsData.getRowCount();
	if (nRowCount==0)
		return;

	//_DUMP(ViewsData);

	QString strWhere;
	if (nEntityType==ENTITY_BUS_PERSON)
	{
		strWhere=" BPER_ID IN (";
	}
	else if (nEntityType==ENTITY_BUS_PROJECT)
	{
		strWhere=" BUSP_ID IN (";
	}
	else if (nEntityType==ENTITY_BUS_CONTACT)
	{
		strWhere=" BCNT_ID IN (";
	}
	else if (nEntityType==ENTITY_CALENDAR_RESOURCES)
	{
		strWhere=" BRES_ID IN (";
	}

	for (int i=0; i<nRowCount; i++)
	{
		if (nEntityType==ENTITY_BUS_PERSON)
		{
			strWhere += ViewsData.getDataRef(i, "ENTITY_PERSON_ID").toString();
		}
		else
		{
			strWhere += ViewsData.getDataRef(i, "ENTITY_ID").toString();
		}
		strWhere += ",";
	}
	strWhere.chop(1);
	strWhere += ") ";

	DbRecordSet recEntitiesData;
	MainEntityFilter filter;
	filter.SetFilter(MainEntityFilter::FILTER_SQL_WHERE,strWhere);
	g_BusinessServiceSet->MainEntitySelector->ReadData(Ret_pStatus,nEntityType,filter.Serialize(),recEntitiesData);
	if(!Ret_pStatus.IsOK()) return;

	ViewsData.clearSelection();
	for (int i=0; i<nRowCount; i++)
	{
		//Get entity id from database (it's always in first place).
		int nViewEntityID;
		if (nEntityType==ENTITY_BUS_PERSON)
		{
			nViewEntityID=ViewsData.getDataRef(i, "ENTITY_PERSON_ID").toInt();
		}
		else
		{
			nViewEntityID=ViewsData.getDataRef(i, "ENTITY_ID").toInt();
		}

		//_DUMP(recEntitiesData);
		//_DUMP(ViewsData);

		//Find the row in view recordset.
		int nRow;
		nRow=recEntitiesData.find(0, nViewEntityID, true, false, true);

		//Check name of the entity, If there is no entity delete row.
		if (nRow>=0)
		{
			if (nEntityType==ENTITY_BUS_PERSON)
			{
				if (recEntitiesData.getDataRef(nRow, "BPER_NAME").toString()!=ViewsData.getDataRef(i, "ENTITY_NAME").toString())
				{
					bModified=true;
					ViewsData.setData(i, "ENTITY_NAME", recEntitiesData.getDataRef(nRow, "BPER_NAME").toString());
				}
			}
			else if (nEntityType==ENTITY_BUS_PROJECT)
			{
				if (recEntitiesData.getDataRef(nRow, "BUSP_NAME").toString()!=ViewsData.getDataRef(i, "ENTITY_NAME").toString())
				{
					bModified=true;
					ViewsData.setData(i, "ENTITY_NAME", recEntitiesData.getDataRef(nRow, "BUSP_NAME").toString());
				}
			}
			else if (nEntityType==ENTITY_BUS_CONTACT)
			{
				if (recEntitiesData.getDataRef(nRow, "BCNT_NAME").toString()!=ViewsData.getDataRef(i, "ENTITY_NAME").toString())
				{
					bModified=true;
					ViewsData.setData(i, "ENTITY_NAME", recEntitiesData.getDataRef(nRow, "BCNT_NAME").toString());
				}
			}
			else if (nEntityType==ENTITY_CALENDAR_RESOURCES)
			{
				if (recEntitiesData.getDataRef(nRow, "BRES_NAME").toString()!=ViewsData.getDataRef(i, "ENTITY_NAME").toString())
				{
					bModified=true;
					ViewsData.setData(i, "ENTITY_NAME", recEntitiesData.getDataRef(nRow, "BRES_NAME").toString());
				}
			}
		}
		else
		{
			ViewsData.selectRow(i);
			bModified=true;
		}
	}
	
	ViewsData.deleteSelectedRows();

//	_DUMP(recEntitiesData);
//	_DUMP(ViewsData);
}
*/
void Service_BusCalendar::WriteEvent(Status &Ret_pStatus, DbRecordSet &RetOut_Data, DbRecordSet &RetOut_Parts,DbRecordSet &RetOut_ScheduleTask,QString strLockRes,DbRecordSet &RetOut_UAR,DbRecordSet &RetOut_GAR)
{

	if (RetOut_Data.getRowCount()==0 || RetOut_Parts.getRowCount()==0)
		return;

	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm_CalEvent(Ret_pStatus, BUS_CAL_EVENT, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	DbSimpleOrm TableOrm_CalPart(Ret_pStatus, BUS_CAL_EVENT_PART, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	DbSimpleOrm TableOrm_CalOption(Ret_pStatus, BUS_CAL_OPTIONS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	DbSimpleOrm TableOrm_CalBreak(Ret_pStatus, BUS_CAL_BREAKS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	DbSimpleOrm TableOrm_CE_COMM_ENTITY(Ret_pStatus, CE_COMM_ENTITY, GetDbManager());
	if(!Ret_pStatus.IsOK()) return;
	DbSimpleOrm TableOrm_BUS_TASKS(Ret_pStatus, BUS_TASKS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	//---------------------------------------
	//			START TRANSACTION
	//---------------------------------------
	TableOrm_CalEvent.GetDbSqlQuery()->BeginTransaction(Ret_pStatus);
	if(!Ret_pStatus.IsOK()) return;


	//---------------------------------------
	//write TASK
	//---------------------------------------
	bool bNewTaskCreated=false;
	if (RetOut_ScheduleTask.getRowCount() > 0)
	{
		if (RetOut_ScheduleTask.getDataRef(0,"BTKS_ID").toInt()<=0)
			bNewTaskCreated=true;

		TableOrm_BUS_TASKS.Write(Ret_pStatus,RetOut_ScheduleTask);
		EXIT_IF_ERROR_AND_ROLLBACK

		//assign tasks to first part, else reset to 0:
		RetOut_Parts.setColValue("CENT_TASK_ID",QVariant(QVariant::Int));
		if (RetOut_Parts.getRowCount()>0)
		{
			RetOut_Parts.setData(0,"CENT_TASK_ID",RetOut_ScheduleTask.getDataRef(0,"BTKS_ID"));
		}
	}

	//main:
	TableOrm_CalEvent.Write(Ret_pStatus, RetOut_Data);
	EXIT_IF_ERROR_AND_ROLLBACK
	int nCalEventID=RetOut_Data.getDataRef(0,"BCEV_ID").toInt();
	RetOut_Parts.setColValue("BCEP_EVENT_ID",nCalEventID);

	//parts ce_entity:
	DbRecordSet lstCommEntity;
	lstCommEntity.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CE_COMM_ENTITY));
	lstCommEntity.merge(RetOut_Parts);
	TableOrm_CE_COMM_ENTITY.Write(Ret_pStatus,lstCommEntity);
	EXIT_IF_ERROR_AND_ROLLBACK
	//assign doc id back: 1-1
	int nSize=lstCommEntity.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		RetOut_Parts.setData(i,"BCEP_COMM_ENTITY_ID",lstCommEntity.getDataRef(i,"CENT_ID"));
		RetOut_Parts.setData(i,"CENT_ID",lstCommEntity.getDataRef(i,"CENT_ID"));
	}

	//parts:
	int nSkipLastCols=DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_EVENT_PART_COMM_ENTITY).m_nSkipLastColsWrite;
	TableOrm_CalPart.WriteSubData(Ret_pStatus,BUS_CAL_EVENT_PART,RetOut_Parts,"BCEP_EVENT_ID",nCalEventID,true,nSkipLastCols);
	//TableOrm_CalPart.Write(Ret_pStatus, RetOut_Parts,DbSqlTableView::TVIEW_BUS_CAL_EVENT_PART_COMM_ENTITY);
	EXIT_IF_ERROR_AND_ROLLBACK

	nSize=RetOut_Parts.getRowCount();
	for(int i=0;i<nSize;i++)
	{
		int nPartID=RetOut_Parts.getDataRef(i,"BCEP_ID").toInt();
		int nCommEntityID=RetOut_Parts.getDataRef(i,"CENT_ID").toInt();
		DbRecordSet lstOptions=RetOut_Parts.getDataRef(i,"OPTION_LIST").value<DbRecordSet>();
		lstOptions.setColValue("BCOL_EVENT_PART_ID",nPartID);

		//lstOptions.Dump();

		//options
		TableOrm_CalOption.WriteSubData(Ret_pStatus, BUS_CAL_OPTIONS,lstOptions, "BCOL_EVENT_PART_ID",nPartID,true,1);
		EXIT_IF_ERROR_AND_ROLLBACK

		//breaks
		int nSize2=lstOptions.getRowCount();
		for(int k=0;k<nSize2;k++)
		{
			int nOptionID=lstOptions.getDataRef(k,"BCOL_ID").toInt();
			DbRecordSet lstBreak=lstOptions.getDataRef(k,"BREAK_LIST").value<DbRecordSet>();
			lstBreak.setColValue("BCBL_OPTIONS_ID",nOptionID);
			//if (lstBreak.getRowCount()==0)
			//	continue;
			//lstBreak.Dump();
			TableOrm_CalBreak.WriteSubData(Ret_pStatus,BUS_CAL_BREAKS,lstBreak,"BCBL_OPTIONS_ID",nOptionID); //to delete unexisting breaks
			//TableOrm_CalBreak.Write(Ret_pStatus, lstBreak);
			EXIT_IF_ERROR_AND_ROLLBACK
			lstOptions.setData(k,"BREAK_LIST",lstBreak); //store back lists for id
		}
		RetOut_Parts.setData(i,"OPTION_LIST",lstOptions); //store back lists for id

		//NMRX
		DbRecordSet lstNMRXPerson=RetOut_Parts.getDataRef(i,"NMRX_PERSON_LIST").value<DbRecordSet>();

		lstNMRXPerson.setColValue("BNMR_TABLE_KEY_ID_1",nCommEntityID);
		g_BusinessServiceSet->BusNMRX->Write(Ret_pStatus,lstNMRXPerson,"",BUS_CAL_INVITE);
		EXIT_IF_ERROR_AND_ROLLBACK

		DbRecordSet lstNMRXProject=RetOut_Parts.getDataRef(i,"NMRX_PROJECT_LIST").value<DbRecordSet>();
		lstNMRXProject.setColValue("BNMR_TABLE_KEY_ID_1",nCommEntityID);
		g_BusinessServiceSet->BusNMRX->Write(Ret_pStatus,lstNMRXProject,"",-1);
		EXIT_IF_ERROR_AND_ROLLBACK

		DbRecordSet lstNMRXResources=RetOut_Parts.getDataRef(i,"NMRX_RESOURCE_LIST").value<DbRecordSet>();
		lstNMRXResources.setColValue("BNMR_TABLE_KEY_ID_1",nCommEntityID);
		g_BusinessServiceSet->BusNMRX->Write(Ret_pStatus,lstNMRXResources,"",-1);
		EXIT_IF_ERROR_AND_ROLLBACK

		RetOut_Parts.setData(i,"NMRX_PERSON_LIST",lstNMRXPerson);
		RetOut_Parts.setData(i,"NMRX_PROJECT_LIST",lstNMRXProject);
		RetOut_Parts.setData(i,"NMRX_RESOURCE_LIST",lstNMRXResources);
	}
	
	//---------------------------------------
	//			COMIT TRANSACTION
	//---------------------------------------
	TableOrm_CalEvent.GetDbSqlQuery()->Commit(Ret_pStatus);
	if(!Ret_pStatus.IsOK()) return;

	//---------------------------------------
	//			UNLOCK:
	//---------------------------------------
	if (!strLockRes.isEmpty())
	{
		BusinessLocker Locker(Ret_pStatus, BUS_CAL_EVENT, GetDbManager());
		if(!Ret_pStatus.IsOK())return;
		Locker.UnLock(Ret_pStatus,strLockRes);
	}

	if (Ret_pStatus.IsOK())
	{
		g_PrivateServiceSet->BusinessHelper->ClientTaskNotification(Ret_pStatus,RetOut_ScheduleTask,bNewTaskCreated);
	}

}


void Service_BusCalendar::ReadEvent(Status &Ret_pStatus, int nCalEventID, DbRecordSet &Ret_Data, DbRecordSet &Ret_Parts, DbRecordSet &Ret_ScheduleTask,DbRecordSet &Ret_UAR,DbRecordSet &Ret_GAR)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm_CalEvent(Ret_pStatus, BUS_CAL_EVENT, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	DbSimpleOrm TableOrm_CalPart(Ret_pStatus, BUS_CAL_EVENT_PART, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	DbSimpleOrm TableOrm_CalOption(Ret_pStatus, BUS_CAL_OPTIONS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	DbSimpleOrm TableOrm_CalBreak(Ret_pStatus, BUS_CAL_BREAKS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	//main
	QString strWhere ="WHERE BCEV_ID="+QString::number(nCalEventID);
	TableOrm_CalEvent.Read(Ret_pStatus, Ret_Data, strWhere);
	if(!Ret_pStatus.IsOK()) return;
	if (Ret_Data.getRowCount()==0)
	{
		Ret_pStatus.setError(StatusCodeSet::ERR_BUS_FAILED_TO_LOAD_RECORD);
		return;
	}
	//parts:
	strWhere ="WHERE BCEP_EVENT_ID="+QString::number(nCalEventID);
	TableOrm_CalPart.Read(Ret_pStatus, Ret_Parts, strWhere,DbSqlTableView::TVIEW_BUS_CAL_EVENT_PART_COMM_ENTITY);
	if(!Ret_pStatus.IsOK()) return;


	int nTaskEntityID=-1;
	int nSize=Ret_Parts.getRowCount();
	for(int i=0;i<nSize;i++)
	{
		int nCommEntityID=Ret_Parts.getDataRef(i,"CENT_ID").toInt();
		int nPartID=Ret_Parts.getDataRef(i,"BCEP_ID").toInt();
		if (Ret_Parts.getDataRef(i,"CENT_TASK_ID").toInt()>0)
			nTaskEntityID=Ret_Parts.getDataRef(i,"CENT_TASK_ID").toInt(); //task can be assigned to any part, find it

		//Options && Breaks:
		DbRecordSet lstOptions;
		QString strWhere ="WHERE BCOL_EVENT_PART_ID="+QString::number(nPartID);
		TableOrm_CalOption.Read(Ret_pStatus, lstOptions, strWhere,DbSqlTableView::TVIEW_BUS_CAL_OPTIONS_SELECT);
		if(!Ret_pStatus.IsOK()) return;

		int nSize2=lstOptions.getRowCount();
		for(int k=0;k<nSize2;k++)
		{
			DbRecordSet lstBreak;
			int nOptionID=lstOptions.getDataRef(k,"BCOL_ID").toInt();
			QString strWhere ="WHERE BCBL_OPTIONS_ID="+QString::number(nOptionID);
			TableOrm_CalBreak.Read(Ret_pStatus, lstBreak, strWhere);
			if(!Ret_pStatus.IsOK()) return;
			lstOptions.setData(k,"BREAK_LIST",lstBreak);
		}
		lstOptions.sort("BCOL_FROM");
		Ret_Parts.setData(i,"PART_FROM",lstOptions.getDataRef(0,"BCOL_FROM").toDateTime());
		Ret_Parts.setData(i,"PART_TO",lstOptions.getDataRef(0,"BCOL_TO").toDateTime());
		Ret_Parts.setData(i,"OPTION_LIST",lstOptions);

		//NMRX:
		if (nCommEntityID!=0)
		{
			//read contact/person link:
			DbRecordSet lstPersonContactLink;
			DbRecordSet lstContactLink;
			g_BusinessServiceSet->BusNMRX->ReadOnePair(Ret_pStatus,CE_COMM_ENTITY,BUS_CM_CONTACT,nCommEntityID,-1,lstContactLink,BUS_CAL_INVITE);
			if(!Ret_pStatus.IsOK()) return;

			//g_BusinessServiceSet->BusNMRX->ReadOnePair(Ret_pStatus,CE_COMM_ENTITY,BUS_PERSON,nCommEntityID,-1,lstPersonContactLink,BUS_CAL_INVITE);
			//if(!Ret_pStatus.IsOK()) return;

			//lstPersonContactLink.merge(lstContactLink);

			//read project link
			DbRecordSet lstProjectLink;
			g_BusinessServiceSet->BusNMRX->ReadOnePair(Ret_pStatus,CE_COMM_ENTITY,BUS_PROJECT,nCommEntityID,-1,lstProjectLink);
			if(!Ret_pStatus.IsOK()) return;

			//read resource link
			DbRecordSet lstResourceLink;
			g_BusinessServiceSet->BusNMRX->ReadOnePair(Ret_pStatus,CE_COMM_ENTITY,BUS_CAL_RESOURCE,nCommEntityID,-1,lstResourceLink);

			Ret_Parts.setData(i,"NMRX_PERSON_LIST",lstContactLink);
			Ret_Parts.setData(i,"NMRX_PROJECT_LIST",lstProjectLink);
			Ret_Parts.setData(i,"NMRX_RESOURCE_LIST",lstResourceLink);
		}
	}

	//read task records:
	if (nTaskEntityID!=0)
	{
		strWhere ="WHERE BTKS_ID="+QVariant(nTaskEntityID).toString();
		g_BusinessServiceSet->ClientSimpleORM->Read(Ret_pStatus,BUS_TASKS,Ret_ScheduleTask,strWhere,-1,false);
		if(!Ret_pStatus.IsOK()) return;
	}

	//do not sort parts if template
	if (!Ret_Data.getDataRef(0,"BCEV_TEMPLATE_FLAG").toInt())
		Ret_Parts.sort("PART_FROM");

	//read uar/gar:
	//g_BusinessServiceSet->AccessRights->ReadGUAR(Ret_pStatus,nDocumentID,BUS_DM_DOCUMENTS,Ret_UAR,Ret_GAR);
}

//read all templates:
void Service_BusCalendar::ReadTemplates(Status &Ret_pStatus, DbRecordSet &Ret_Data)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_CAL_EVENT, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	QString strWhere =" WHERE BCEV_TEMPLATE_FLAG=1";

	//load
	g_AccessRight->SQLFilterRecords(BUS_CAL_EVENT,strWhere);
	TableOrm.Read(Ret_pStatus, Ret_Data, strWhere,DbSqlTableView::TVIEW_BUS_CAL_EVENT);
	if(!Ret_pStatus.IsOK()) return;

}

//DataInvite = BUS_CAL_INVITE + TABLE_ID<cont or person> + ID; 
void Service_BusCalendar::SendInviteBySokrates(Status &Ret_pStatus, int nCalEventID,DbRecordSet &DataInvite)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_CAL_INVITE, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	DataInvite.find("BPER_ID",QVariant(QVariant::Int),false,false,true,true);
	DataInvite.deleteSelectedRows();

	/*
	//get all person Id's for contact id's, if some is not person, remove from list:
	DataInvite.clearSelection();
	int nSize=DataInvite.getRowCount();
	for(int i=0;i<nSize;i++)
	{
		if (DataInvite.getDataRef(i,"TABLE_ID")==BUS_CM_CONTACT)
		{
			QString strSQL="SELECT BPER_ID FROM BUS_PERSON WHERE BPER_CONTACT_ID="+QString::number(DataInvite.getDataRef(i,"ID").toInt());
			TableOrm.GetDbSqlQuery()->Execute(Ret_pStatus,strSQL);
			if(!Ret_pStatus.IsOK()) return;
			DbRecordSet rowPersonID;
			TableOrm.GetDbSqlQuery()->FetchData(rowPersonID);
			if (rowPersonID.getRowCount()>0)
			{
				DataInvite.setData(i,"BPER_ID",rowPersonID.getDataRef(0,0).toInt());
			}
			else
				DataInvite.selectRow(i);
		}
		else
		{
			DataInvite.setData(i,"BPER_ID",DataInvite.getDataRef(i,"ID"));
		}
	}
	DataInvite.deleteSelectedRows();
	*/

	//edit each rowInvite row, set status to sent and send signal to clients:
	DataInvite.setColValue("BCIV_IS_SENT",1);

	DbRecordSet lstInvite;
	lstInvite.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_INVITE));
	lstInvite.merge(DataInvite);

	//lock event id
	QList<int> lockinglist;
	lockinglist.append(nCalEventID);
	QString strLockRes;
	BusinessLocker Locker(Ret_pStatus, BUS_CAL_EVENT, GetDbManager());
	if(!Ret_pStatus.IsOK())return;
	Locker.Lock(Ret_pStatus,lockinglist,strLockRes);
	if(!Ret_pStatus.IsOK())return;

	TableOrm.Write(Ret_pStatus,lstInvite);
	if(!Ret_pStatus.IsOK())
	{
		Status err;
		Locker.UnLock(err,strLockRes);
		return;
	}
	Locker.UnLock(Ret_pStatus,strLockRes);
	if(!Ret_pStatus.IsOK())return;


	//as task card: compile message and sent:
	
	int nSize=DataInvite.getRowCount();
	for(int i=0;i<nSize;i++)
	{
		//is owner logged? find 0, 1 or more socket id's:
		int nTargetPersonID=DataInvite.getDataRef(i,"BPER_ID").toInt();
		if (nTargetPersonID<=0) continue;
		//if (nTargetPersonID==g_UserSessionManager->GetPersonID()) -> issue 2231
		//	continue;
		QStringList lstPersons;
		lstPersons.append(QString::number(nTargetPersonID)); //invite will be stored in app. server memory until user is logged!

		//<event_id>;<part_id>;<owner_id>;<invite_id>;<target_person_id> (";" delimited)!
		QStringList lstMessage;
		lstMessage.append(QString::number(nCalEventID));
		lstMessage.append(DataInvite.getDataRef(i,"BCEP_ID").toString());
		//lstMessage.append(DataInvite.getDataRef(i,"BCEP_COMM_ENTITY_ID").toString());
		lstMessage.append(DataInvite.getDataRef(0,"CENT_OWNER_ID").toString());
		lstMessage.append(DataInvite.getDataRef(i,"BCIV_ID").toString());
		lstMessage.append(DataInvite.getDataRef(i,"BPER_ID").toString());
		QString strMsg=lstMessage.join(";");
		g_MessageDispatcher->SendMessageX(StatusCodeSet::TYPE_SILENT_SERVER_NOTIFICATION,StatusCodeSet::ERR_BUS_NEW_INVITE_NOTIFICATION,strMsg,&lstPersons,g_UserSessionManager->GetSocketID());
	}

}

//RetOut_RowPart in extended selection format: part+ce_comm+option(active)
void Service_BusCalendar::ReadOnePart(Status &Ret_pStatus, int nPartID,DbRecordSet &RetOut_RowPart,DbRecordSet &RetOut_RowActiveOption)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm_CalPart(Ret_pStatus, BUS_CAL_EVENT_PART, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	DbSimpleOrm TableOrm_CalOption(Ret_pStatus, BUS_CAL_OPTIONS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	//part:
	QString strWhere ="WHERE BCEP_ID="+QString::number(nPartID);
	TableOrm_CalPart.Read(Ret_pStatus, RetOut_RowPart, strWhere,DbSqlTableView::TVIEW_BUS_CAL_EVENT_PART_COMM_ENTITY);
	if(!Ret_pStatus.IsOK()) return;

	//active option:
	DbRecordSet lstOptions;
	//issue: 
	//strWhere ="WHERE BCOL_IS_ACTIVE=1 AND BCOL_EVENT_PART_ID="+QString::number(nPartID); 
	strWhere ="WHERE BCOL_EVENT_PART_ID="+QString::number(nPartID);
	TableOrm_CalOption.Read(Ret_pStatus, RetOut_RowActiveOption, strWhere,DbSqlTableView::TVIEW_BUS_CAL_OPTIONS_SELECT);
	if(!Ret_pStatus.IsOK()) return;

	//if(RetOut_RowPart.getRowCount()>0)
	//	RetOut_RowPart.setData(0,"LST_OPTIONS",RetOut_RowActiveOption);

}

//nStatus=GlobalConstants::CalendarInviteStatus
void Service_BusCalendar::SetInviteStatus(Status &Ret_pStatus, int nCalEventID,int nPartID,int nInviteID,int nContactId_ConfirmedBy, int nPersonID_ConfirmedBy,int nStatus, int nOwnerID,bool bNotifyOwner)
{
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_CAL_INVITE, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	//lock event id
	QList<int> lockinglist;
	lockinglist.append(nCalEventID);
	QString strLockRes;
	BusinessLocker Locker(Ret_pStatus, BUS_CAL_EVENT, GetDbManager());
	if(!Ret_pStatus.IsOK())return;
	Locker.Lock(Ret_pStatus,lockinglist,strLockRes);
	if(!Ret_pStatus.IsOK())return;

	//change invite status:
	QString strSQL="UPDATE BUS_CAL_INVITE SET BCIV_STATUS="+QString::number(nStatus)+" WHERE BCIV_ID =" +QString::number(nInviteID);
	TableOrm.GetDbSqlQuery()->Execute(Ret_pStatus,strSQL);
	if (!Ret_pStatus.IsOK())
	{
		Status err;
		Locker.UnLock(err,strLockRes);
		return;
	}

	Locker.UnLock(Ret_pStatus,strLockRes);
	if(!Ret_pStatus.IsOK())return;

	//read out what is target of invite: contact or person


	if (bNotifyOwner)
	{
		//is owner logged? find 0, 1 or more socket id's:
		if (nOwnerID<=0)
			return;
		//QStringList lstSockets=g_UserSessionManager->GetSocketListForPersonID(nOwnerID);
		//if (nTargetPersonID==g_UserSessionManager->GetPersonID()) 
		//	continue;
		QStringList lstPersons;
		lstPersons.append(QString::number(nOwnerID)); //invite will be stored in app. server memory until user is logged!

		//event_id,part_id, owner_id,invite_id,nContactId_ConfirmedBy,nPersonID_ConfirmedBy,status(";" delimited)!
		QStringList lstMessage;
		lstMessage.append(QString::number(nCalEventID));
		lstMessage.append(QString::number(nPartID));
		lstMessage.append(QString::number(nOwnerID));
		lstMessage.append(QString::number(nInviteID));
		lstMessage.append(QString::number(nContactId_ConfirmedBy));  //can be 0
		lstMessage.append(QString::number(nPersonID_ConfirmedBy));  //can be 0
		lstMessage.append(QString::number(nStatus));  //can be 0

		QString strMsg=lstMessage.join(";");
		g_MessageDispatcher->SendMessageX(StatusCodeSet::TYPE_SILENT_SERVER_NOTIFICATION,StatusCodeSet::ERR_BUS_INVITE_NOTIFY_ORIGINATOR,strMsg,&lstPersons,g_UserSessionManager->GetSocketID());
	}

}

void Service_BusCalendar::ReadOneEvent(Status &Ret_pStatus, int nBCEV_ID, DbRecordSet &Ret_EventParts, DbRecordSet &Ret_Contacts, DbRecordSet &Ret_Resources, DbRecordSet &Ret_Projects, DbRecordSet &Ret_Breaks)
{
	//Contact data Table ORM.
	QString strSQL = DbSqlTableView::getSQLView(DbSqlTableView::MVIEW_CALENDAR_ONE_EVENT_PARTS);
	strSQL += " WHERE BCEV_ID = ? AND BCEV_TEMPLATE_FLAG = 0 "; 

	DbSqlQuery query(Ret_pStatus, GetDbManager());
	if(!Ret_pStatus.IsOK()) return;

	Ret_EventParts.defineFromView(DbSqlTableView::getView(DbSqlTableView::MVIEW_CALENDAR_ONE_EVENT_PARTS));

	query.Prepare(Ret_pStatus, strSQL);
	if (!Ret_pStatus.IsOK()) return;
	query.bindValue(0, nBCEV_ID);
	query.ExecutePrepared(Ret_pStatus);
	if (!Ret_pStatus.IsOK()) return;

	query.FetchData(Ret_EventParts);

	GetEventPartContacts(Ret_pStatus, Ret_EventParts, Ret_Contacts);
	GetEventPartProjects(Ret_pStatus, Ret_EventParts, Ret_Projects);
	GetEventPartResources(Ret_pStatus, Ret_EventParts, Ret_Resources);
	GetEventPartBreaks(Ret_pStatus, Ret_EventParts, Ret_Breaks);
}

void Service_BusCalendar::ReadEventPartsCountForEntities(Status &Ret_pStatus, DbRecordSet &RetOut_recEntities, DbRecordSet recFilter, QDateTime datStartDate, QDateTime datEndDate)
{
	//_DUMP(RetOut_recEntities);
	if (RetOut_recEntities.getRowCount()==0)
		return;

	DbRecordSet recUserSelectPartsCount;
	DbRecordSet recProjectSelectPartsCount;
	DbRecordSet recContactSelectPartsCount;
	DbRecordSet recResourceSelectPartsCount;

	QString strUserEnitiesIDs = "(";
	QString strProjectEnitiesIDs = "(";
	QString strContactEnitiesIDs = "(";
	QString strResourceEnitiesIDs = "(";

	bool bSelectUsers=false;
	bool bSelectProject=false;
	bool bSelectContact=false;
	bool bSelectResources=false;

	//Skip first row for sort.
	int nRowCount = RetOut_recEntities.getRowCount();
	for (int i = 1; i < nRowCount; i++)
	{
		int nEntityId = RetOut_recEntities.getDataRef(i, "ENTITY_ID").toInt();
		int nEntityType = RetOut_recEntities.getDataRef(i, "ENTITY_TYPE").toInt();
		int nTrueEntityType = nEntityType;
		if (nEntityType == GlobalConstants::TYPE_CAL_USER_SELECT)
			nEntityType = GlobalConstants::TYPE_CAL_CONTACT_SELECT;

		if (nEntityType == GlobalConstants::TYPE_CAL_USER_SELECT)
		{
			bSelectUsers = true;
			strUserEnitiesIDs += QVariant(nEntityId).toString();
			strUserEnitiesIDs += ",";
		}
		else if (nEntityType == GlobalConstants::TYPE_CAL_PROJECT_SELECT)
		{
			bSelectProject = true;
			strProjectEnitiesIDs += QVariant(nEntityId).toString();
			strProjectEnitiesIDs += ",";
		}
		else if (nEntityType == GlobalConstants::TYPE_CAL_CONTACT_SELECT)
		{
			bSelectContact = true;
			strContactEnitiesIDs += QVariant(nEntityId).toString();
			strContactEnitiesIDs += ",";
		}
		else if (nEntityType == GlobalConstants::TYPE_CAL_RESOURCE_SELECT)
		{
			bSelectResources = true;
			strResourceEnitiesIDs += QVariant(nEntityId).toString();
			strResourceEnitiesIDs += ",";
		}
	}

	//Get cent id's for entities.
	QString strSQL = DbSqlTableView::getSQLView(DbSqlTableView::MVIEW_CALENDAR_EVENT_PARTS_SIMPLE);
	strSQL += " WHERE BCEV_TEMPLATE_FLAG = 0 AND ((BCOL_FROM >= ? AND BCOL_TO <= ?) \
			  OR (BCOL_FROM <= ? AND BCOL_TO >= ?) OR (BCOL_FROM <= ? AND BCOL_TO >= ?) ) ";

	if (bSelectUsers)
	{
		strUserEnitiesIDs.chop(1);
		strUserEnitiesIDs += ") ";
		QString strUserSelect=strSQL;
		strUserSelect += " AND BNMR_TABLE_2 = 10 AND BNMR_TABLE_KEY_ID_2 IN "; 
		strUserSelect += strUserEnitiesIDs; 

		GetEventParts(Ret_pStatus, recUserSelectPartsCount, strUserSelect, recFilter, datStartDate, datEndDate);
		//_DUMP(recUserSelectPartsCount);
	}
	if (bSelectProject)
	{
		strProjectEnitiesIDs.chop(1);
		strProjectEnitiesIDs += ") ";
		QString strProjectSelect=strSQL;
		strProjectSelect += " AND BNMR_TABLE_2 = 42 AND BNMR_TABLE_KEY_ID_2 IN "; 
		strProjectSelect += strProjectEnitiesIDs; 

		GetEventParts(Ret_pStatus, recProjectSelectPartsCount, strProjectSelect, recFilter, datStartDate, datEndDate);
		//_DUMP(recProjectSelectPartsCount);
	}
	if (bSelectContact)
	{
		strContactEnitiesIDs.chop(1);
		strContactEnitiesIDs += ") ";
		QString strContactSelect=strSQL;
		strContactSelect += " AND BNMR_TABLE_2 = 22 AND BNMR_TABLE_KEY_ID_2 IN "; 
		strContactSelect += strContactEnitiesIDs; 

		GetEventParts(Ret_pStatus, recContactSelectPartsCount, strContactSelect, recFilter, datStartDate, datEndDate);
		//_DUMP(recContactSelectPartsCount);
	}
	if (bSelectResources)
	{
		strResourceEnitiesIDs.chop(1);
		strResourceEnitiesIDs += ") ";
		QString strResourceSelect=strSQL;
		strResourceSelect += " AND BNMR_TABLE_2 = 70 AND BNMR_TABLE_KEY_ID_2 IN "; 
		strResourceSelect += strResourceEnitiesIDs; 

		GetEventParts(Ret_pStatus, recResourceSelectPartsCount, strResourceSelect, recFilter, datStartDate, datEndDate);
		//_DUMP(recResourceSelectPartsCount);
	}

	//Reorder entities recordset and return - first record stays on it's place.
	//First create tmp recordset and copy entities with events to it, then add other entites to it.
	DbRecordSet tmpRec;
	tmpRec.copyDefinition(RetOut_recEntities);
	tmpRec.addRow();
	tmpRec.assignRow(0, RetOut_recEntities.getRow(0));
	RetOut_recEntities.clearSelection();
	for (int i = 1; i < nRowCount; i++)
	{
		int nEntityId = RetOut_recEntities.getDataRef(i, "ENTITY_ID").toInt();
		int nEntityType = RetOut_recEntities.getDataRef(i, "ENTITY_TYPE").toInt();
		int nTrueEntityType = nEntityType;
		if (nEntityType == GlobalConstants::TYPE_CAL_USER_SELECT)
			nEntityType = GlobalConstants::TYPE_CAL_CONTACT_SELECT;

		if (nEntityType == GlobalConstants::TYPE_CAL_USER_SELECT && bSelectUsers)
		{
			int nRow=recUserSelectPartsCount.find("BNMR_TABLE_KEY_ID_2",nEntityId,true);
			if (nRow>=0)
			{
				tmpRec.addRow();
				tmpRec.assignRow(tmpRec.getRowCount()-1, RetOut_recEntities.getRow(i));
				RetOut_recEntities.selectRow(i);
			}
		}
		else if (nEntityType == GlobalConstants::TYPE_CAL_PROJECT_SELECT && bSelectProject)
		{
			int nRow=recProjectSelectPartsCount.find("BNMR_TABLE_KEY_ID_2",nEntityId,true);
			if (nRow>=0)
			{
				tmpRec.addRow();
				tmpRec.assignRow(tmpRec.getRowCount()-1, RetOut_recEntities.getRow(i));
				RetOut_recEntities.selectRow(i);
			}
		}
		else if (nEntityType == GlobalConstants::TYPE_CAL_CONTACT_SELECT && bSelectContact)
		{
			int nRow=recContactSelectPartsCount.find("BNMR_TABLE_KEY_ID_2",nEntityId,true);
			if (nRow>=0)
			{
				tmpRec.addRow();
				tmpRec.assignRow(tmpRec.getRowCount()-1, RetOut_recEntities.getRow(i));
				RetOut_recEntities.selectRow(i);
			}
		}
		else if (nEntityType == GlobalConstants::TYPE_CAL_RESOURCE_SELECT && bSelectResources)
		{
			int nRow=recResourceSelectPartsCount.find("BNMR_TABLE_KEY_ID_2",nEntityId,true);
			if (nRow>=0)
			{
				tmpRec.addRow();
				tmpRec.assignRow(tmpRec.getRowCount()-1, RetOut_recEntities.getRow(i));
				RetOut_recEntities.selectRow(i);
			}
		}
	}
	
	if (tmpRec.getRowCount()>1)
	{
		for (int i = 1; i < nRowCount; i++)
		{
			if (!RetOut_recEntities.isRowSelected(i))
			{
				tmpRec.addRow();
				tmpRec.assignRow(tmpRec.getRowCount()-1, RetOut_recEntities.getRow(i));
			}
		}
	}

//	_DUMP(tmpRec);
//	_DUMP(RetOut_recEntities);
	if (tmpRec.getRowCount()==RetOut_recEntities.getRowCount())
		RetOut_recEntities=tmpRec;
}

void Service_BusCalendar::ReadEventParts(Status &Ret_pStatus, DbRecordSet recFilter, int nEntityType, int nEntityID, QDateTime datStartDate, QDateTime datEndDate, DbRecordSet &Ret_EventParts, DbRecordSet &Ret_Contacts, DbRecordSet &Ret_Resources, DbRecordSet &Ret_Projects, DbRecordSet &Ret_Breaks)
{
	//_DUMP(recFilter);
	//Contact data Table ORM.
	QString strSQL = DbSqlTableView::getSQLView(DbSqlTableView::MVIEW_CALENDAR_EVENT_PARTS);
	strSQL += " WHERE BCEV_TEMPLATE_FLAG = 0 AND ((BCOL_FROM >= ? AND BCOL_TO <= ?) \
				OR (BCOL_FROM <= ? AND BCOL_TO >= ?) OR (BCOL_FROM <= ? AND BCOL_TO >= ?) ) ";

	if (nEntityType == GlobalConstants::TYPE_CAL_USER_SELECT)
		strSQL += " AND BNMR_TABLE_2 = 10 AND BNMR_TABLE_KEY_ID_2 = ? "; 
	else if (nEntityType == GlobalConstants::TYPE_CAL_PROJECT_SELECT)
		strSQL += " AND BNMR_TABLE_2 = 42 AND BNMR_TABLE_KEY_ID_2 = ? "; 
	else if (nEntityType == GlobalConstants::TYPE_CAL_CONTACT_SELECT)
		strSQL += " AND BNMR_TABLE_2 = 22 AND BNMR_TABLE_KEY_ID_2 = ? "; 
	else if (nEntityType == GlobalConstants::TYPE_CAL_RESOURCE_SELECT)
		strSQL += " AND BNMR_TABLE_2 = 70 AND BNMR_TABLE_KEY_ID_2 = ? "; 
	
	GetEventParts(Ret_pStatus, Ret_EventParts, strSQL, recFilter, datStartDate, datEndDate, nEntityID);

	GetEventPartContacts(Ret_pStatus, Ret_EventParts, Ret_Contacts);
	GetEventPartProjects(Ret_pStatus, Ret_EventParts, Ret_Projects);
	GetEventPartResources(Ret_pStatus, Ret_EventParts, Ret_Resources);
	GetEventPartBreaks(Ret_pStatus, Ret_EventParts, Ret_Breaks);
}

void Service_BusCalendar::ReadEventPartsForInsert(Status &Ret_pStatus, DbRecordSet recEntities, QDateTime datStartDate, QDateTime datEndDate, DbRecordSet &Ret_EventParts, DbRecordSet &Ret_Contacts, DbRecordSet &Ret_Resources, DbRecordSet &Ret_Projects, DbRecordSet &Ret_Breaks)
{
	int nCount = recEntities.getRowCount();
	for (int i = 0; i < nCount; i++)
	{
		int nEntityId = recEntities.getDataRef(i, "ENTITY_ID").toInt();
		int nEntityType = recEntities.getDataRef(i, "ENTITY_TYPE").toInt();
		int nTrueEntityType = nEntityType;
		if (nEntityType == GlobalConstants::TYPE_CAL_USER_SELECT)
			nEntityType = GlobalConstants::TYPE_CAL_CONTACT_SELECT;

		DbRecordSet recEventParts;
		DbRecordSet recContacts;
		DbRecordSet recResources;
		DbRecordSet recProjects;
		DbRecordSet recBreaks;

		DbRecordSet recFilter;
		ReadEventParts(Ret_pStatus, recFilter, nEntityType, nEntityId, datStartDate, datEndDate, recEventParts, recContacts, recResources, recProjects, recBreaks);

		//_DUMP(recEventParts);
		//_DUMP(recContacts);
		//_DUMP(recResources);
		//_DUMP(recProjects);
		//_DUMP(recBreaks);
		
		if (!Ret_EventParts.getColumnCount())
			Ret_EventParts.copyDefinition(recEventParts);
		Ret_EventParts.merge(recEventParts);

		if (!Ret_Contacts.getColumnCount())
			Ret_Contacts.copyDefinition(recContacts);
		Ret_Contacts.merge(recContacts);
		
		if (!Ret_Resources.getColumnCount())
			Ret_Resources.copyDefinition(recContacts);
		Ret_Resources.merge(recResources);
		
		if (!Ret_Projects.getColumnCount())
			Ret_Projects.copyDefinition(recContacts);
		Ret_Projects.merge(recProjects);
		
		if (!Ret_Breaks.getColumnCount())
			Ret_Breaks.copyDefinition(recContacts);
		Ret_Breaks.merge(recBreaks);

		//_DUMP(Ret_EventParts);
		//_DUMP(Ret_Contacts);
		//_DUMP(Ret_Resources);
		//_DUMP(Ret_Projects);
		//_DUMP(Ret_Breaks);

	}
}

void Service_BusCalendar::GetEventPartContacts(Status &Ret_pStatus, DbRecordSet &recParts, DbRecordSet &recContacts)
{
	//Contact data Table ORM.
	DbSimpleOrm ContactOrm(Ret_pStatus, BUS_CM_CONTACT, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	QString strWhere;
	strWhere = " AND BNMR_TABLE_KEY_ID_2 = BCNT_ID AND CENT_ID = BNMR_TABLE_KEY_ID_1 AND ";
	strWhere+= QString(NMRX_CE_CONTACT_WHERE);

	ContactOrm.ReadFromParentIDs(Ret_pStatus, recContacts, "BNMR_TABLE_KEY_ID_1", recParts, "CENT_ID", "", strWhere, DbSqlTableView::MVIEW_CALENDAR_EVENT_CONTACT_VIEW);
	if(!Ret_pStatus.IsOK()) return;

	recContacts.addColumn(QVariant::String, "BCNT_NAME");
	//calculated names: 
	//_DUMP(recContacts);
	recContacts.setColValue("BCNT_ORGANIZATIONNAME", "");
	MainEntityCalculatedName filler;
	filler.Initialize(ENTITY_BUS_CONTACT,recContacts);
	filler.FillCalculatedNames(recContacts,recContacts.getColumnIdx("BCNT_NAME"));
}

void Service_BusCalendar::GetEventPartResources(Status &Ret_pStatus, DbRecordSet &recParts, DbRecordSet &recResources)
{
	//Contact data Table ORM.
	DbSimpleOrm ProjectOrm(Ret_pStatus, BUS_CAL_RESOURCE, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	QString strWhere;
	strWhere = " AND BNMR_TABLE_KEY_ID_2 = BRES_ID AND CENT_ID = BNMR_TABLE_KEY_ID_1 AND ";
	strWhere+= QString(NMRX_CE_RESOURCE_WHERE);

	ProjectOrm.ReadFromParentIDs(Ret_pStatus, recResources, "BNMR_TABLE_KEY_ID_1", recParts, "CENT_ID", "", strWhere, DbSqlTableView::MVIEW_CALENDAR_EVENT_RESOURCE_VIEW);
	if(!Ret_pStatus.IsOK()) return;

	//_DUMP(recResources);
}

void Service_BusCalendar::GetEventPartProjects(Status &Ret_pStatus, DbRecordSet &recParts, DbRecordSet &recProjects)
{
	//Contact data Table ORM.
	DbSimpleOrm ProjectOrm(Ret_pStatus, BUS_PROJECT, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	QString strWhere;
	strWhere = " AND BNMR_TABLE_KEY_ID_2 = BUSP_ID AND CENT_ID = BNMR_TABLE_KEY_ID_1 AND ";
	strWhere+= QString(NMRX_CE_PROJECT_WHERE);

	ProjectOrm.ReadFromParentIDs(Ret_pStatus, recProjects, "BNMR_TABLE_KEY_ID_1", recParts, "CENT_ID", "", strWhere, DbSqlTableView::MVIEW_CALENDAR_EVENT_PROJECT_VIEW);
	if(!Ret_pStatus.IsOK()) return;
}

void Service_BusCalendar::GetEventPartBreaks(Status &Ret_pStatus, DbRecordSet &recParts, DbRecordSet &recBreaks)
{
	//Contact data Table ORM.
	DbSimpleOrm BreaksOrm(Ret_pStatus, BUS_CAL_BREAKS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	QString strWhere;
	strWhere = " ";

	BreaksOrm.ReadFromParentIDs(Ret_pStatus, recBreaks, "BCBL_OPTIONS_ID", recParts, "BCOL_ID");
	//BreaksOrm.ReadFromParentIDs(Ret_pStatus, recBreaks, "BCBL_OPTIONS_ID", recParts, "BCOL_ID", "", strWhere, DbSqlTableView::MVIEW_CALENDAR_EVENT_BREAK_VIEW, true);
	if(!Ret_pStatus.IsOK()) return;
}


void Service_BusCalendar::ModifyEventDateRange(Status &Ret_pStatus, int nCalEventPartID, int nBCOL_ID, QDateTime datFrom, QDateTime datTo)
{
	DbSimpleOrm TableOrm_CalEvent(Ret_pStatus, BUS_CAL_EVENT, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	QString strSQL="SELECT BCEV_ID,BCEV_FROM, BCEV_TO FROM BUS_CAL_EVENT";
	strSQL+=" INNER JOIN BUS_CAL_EVENT_PART ON BCEP_EVENT_ID = BCEV_ID ";
	strSQL+=" WHERE BCEP_ID =" + QString::number(nCalEventPartID);

	DbSqlQuery *query=TableOrm_CalEvent.GetDbSqlQuery();
	query->Execute(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK()) return;

	//Test if cal event out of boundaries, if so make adjustments
	QDateTime datEvFrom,datEvTo;
	int nCalEvID=-1;
	if(query->next())
	{
		nCalEvID=query->value(0).toInt();
		datEvFrom=query->value(1).toDateTime();
		datEvTo=query->value(2).toDateTime();
	}
	else
	{
		Ret_pStatus.setError(1,"Invalid Calendar Part ID");
		return;
	}

	datEvFrom=datFrom;
	datEvTo=datTo;

	/*
	if (datEvFrom>datFrom)
		datEvFrom=datFrom;
	if (datEvTo<datTo)
		datEvTo=datTo;
	if (datEvTo>datTo)
		datEvTo=datTo;
	*/

	//BEGIN TRANSACTION
	query->BeginTransaction(Ret_pStatus);
	if(!Ret_pStatus.IsOK()) return;

	//CAL_EVENT:
	strSQL="UPDATE BUS_CAL_EVENT SET BCEV_FROM = ?, BCEV_TO = ? WHERE BCEV_ID =?";
	query->Prepare(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK()) 
	{
		query->Rollback();
		return;
	}

	query->bindValue(0,datEvFrom);
	query->bindValue(1,datEvTo);
	query->bindValue(2,nCalEvID);
	query->ExecutePrepared(Ret_pStatus);
	if(!Ret_pStatus.IsOK()) 
	{
		query->Rollback();
		return;
	}


	//OPTION/PART modfiy:
	strSQL="UPDATE BUS_CAL_OPTIONS SET BCOL_FROM = ?, BCOL_TO = ? WHERE BCOL_ID =?";
	query->Prepare(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK()) 
	{
		query->Rollback();
		return;
	}

	query->bindValue(0,datFrom);
	query->bindValue(1,datTo);
	query->bindValue(2,nBCOL_ID);
	query->ExecutePrepared(Ret_pStatus);
	if(!Ret_pStatus.IsOK()) 
	{
		query->Rollback();
		return;
	}

	query->Commit(Ret_pStatus);
	
}

//if single part calendar entry: delete whole event
void Service_BusCalendar::DeleteEventPart(Status &Ret_pStatus, int nCalEventPartID, int nBCOL_ID, int nBCEV_ID)
{
	DbSimpleOrm TableOrm_CalEvent(Ret_pStatus, BUS_CAL_EVENT, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	DbSimpleOrm TableOrm_CalPart(Ret_pStatus, BUS_CAL_EVENT_PART, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	DbSimpleOrm TableOrm_CalOptions(Ret_pStatus, BUS_CAL_OPTIONS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	QList<int> lockList;
	lockList << nBCEV_ID;
	QString strLockRes;
	BusinessLocker Locker(Ret_pStatus, BUS_CAL_EVENT, GetDbManager());
	if(!Ret_pStatus.IsOK())return;
	Locker.Lock(Ret_pStatus, lockList, strLockRes);
	if(!Ret_pStatus.IsOK())return;

	DeleteEventPartPrivate(Ret_pStatus, TableOrm_CalEvent, TableOrm_CalPart, TableOrm_CalOptions, nCalEventPartID, nBCOL_ID, nBCEV_ID);

	Locker.UnLock(Ret_pStatus,strLockRes);
}

void Service_BusCalendar::DeleteEventParts(Status &Ret_pStatus, DbRecordSet recDeleteItemsData)
{
	QList<int> lockList;
	int nRowCount = recDeleteItemsData.getRowCount();
	for (int i = 0; i < nRowCount; i++)
		lockList << recDeleteItemsData.getDataRef(i, "nBcevID").toInt();;
	QString strLockRes;
	BusinessLocker Locker(Ret_pStatus, BUS_CAL_EVENT, GetDbManager());
	if(!Ret_pStatus.IsOK())return;
	Locker.Lock(Ret_pStatus, lockList, strLockRes);
	if(!Ret_pStatus.IsOK())return;
	
	DbSimpleOrm TableOrm_CalEvent(Ret_pStatus, BUS_CAL_EVENT, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	DbSimpleOrm TableOrm_CalPart(Ret_pStatus, BUS_CAL_EVENT_PART, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	DbSimpleOrm TableOrm_CalOptions(Ret_pStatus, BUS_CAL_OPTIONS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	DbSqlQuery *query1=TableOrm_CalEvent.GetDbSqlQuery();
	query1->BeginTransaction(Ret_pStatus);
	
	for (int i = 0; i < nRowCount; i++)
	{
		int nCalEventPartID = recDeleteItemsData.getDataRef(i, "nBcepID").toInt();
		int nBCOL_ID = recDeleteItemsData.getDataRef(i, "nBCOL_ID").toInt();
		int nBCEV_ID = recDeleteItemsData.getDataRef(i, "nBCEV_ID").toInt();

		DeleteEventPartPrivate(Ret_pStatus, TableOrm_CalEvent, TableOrm_CalPart, TableOrm_CalOptions, nCalEventPartID, nBCOL_ID, nBCEV_ID);
		if (!Ret_pStatus.IsOK())
			break;
	}

	if (Ret_pStatus.IsOK())
		query1->Commit(Ret_pStatus);
	else
		query1->Rollback();

	Locker.UnLock(Ret_pStatus,strLockRes);
}

//read reservations:
void Service_BusCalendar::ReadReservation(Status &Ret_pStatus, int nEntityType, int nEntityID, QDateTime datStartDate, QDateTime datEndDate, DbRecordSet &Ret_Reservations)
{
	Ret_Reservations.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_RESERVATION));
	//Ret_ReservationsRecalculated.defineFromView(DbSqlTableView::getView(DbSqlTableView::MVIEW_BUS_CAL_RESERVATION));

	DbSimpleOrm TableOrm_Reservation(Ret_pStatus, BUS_CAL_RESERVATION, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	QString strWhere;
	if(nEntityType==GlobalConstants::TYPE_CAL_CONTACT_SELECT)
	{
		strWhere="BCRS_CONTACT_ID = "+QString::number(nEntityID);
	}
	else if (nEntityType==GlobalConstants::TYPE_CAL_PROJECT_SELECT)
	{
		strWhere="BCRS_PROJECT_ID = "+QString::number(nEntityID);
	}
	else if (nEntityType==GlobalConstants::TYPE_CAL_RESOURCE_SELECT)
	{
		strWhere="BCRS_RESOURCE_ID = "+QString::number(nEntityID);
	}
	else
		return; //its ok!

	QString strColumnForSelect=DbSqlTableView::getSQLColumnsFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_RESERVATION));
	QString strWhere_ByPeriod=strWhere+" AND BCRS_FREE_FROM >=? AND BCRS_FREE_TO <=? AND BCRS_IS_FREE=1";

	DbSqlQuery *query = TableOrm_Reservation.GetDbSqlQuery();
	query->Prepare(Ret_pStatus,"SELECT "+strColumnForSelect+" FROM BUS_CAL_RESERVATION WHERE "+strWhere_ByPeriod);
	if(!Ret_pStatus.IsOK()) return;

	query->bindValue(0,datStartDate);
	query->bindValue(1,datEndDate);

	query->ExecutePrepared(Ret_pStatus);
	if(!Ret_pStatus.IsOK()) return;

	DbRecordSet lstTempData;
	query->FetchData(lstTempData);

	QString strWhere_ByWeekDay=strWhere+" AND BCRS_IS_FREE=0";
	query->Execute(Ret_pStatus,"SELECT "+strColumnForSelect+" FROM BUS_CAL_RESERVATION WHERE "+strWhere_ByWeekDay);
	if(!Ret_pStatus.IsOK()) return;

	DbRecordSet lstTempData_2;
	query->FetchData(lstTempData_2);

	Ret_Reservations.merge(lstTempData);
	Ret_Reservations.merge(lstTempData_2);

}

void Service_BusCalendar::ReadEventPartEntitesForOneEntity(Status &Ret_pStatus, DbRecordSet recFilter, int nEntityType, int nEntityID, QDateTime datStartDate, QDateTime datEndDate, DbRecordSet &Ret_Contacts, DbRecordSet &Ret_Resources, DbRecordSet &Ret_Projects)
{
	DbRecordSet recEventParts, recBreaks;
	ReadEventParts(Ret_pStatus, recFilter, nEntityType, nEntityID, datStartDate, datEndDate, recEventParts, Ret_Contacts, Ret_Resources, Ret_Projects, recBreaks);
}


//: find owner name, type name and email templates
void Service_BusCalendar::PrepareInviteRecordsForSendByEmail(Status &Ret_pStatus, DbRecordSet &RetOut_DataInvite, int nDefaultEmailTemplateID)
{
	DbSqlQuery query(Ret_pStatus, GetDbManager());
	if(!Ret_pStatus.IsOK()) return;

	DbRecordSet lstPersons;
	DbRecordSet empty;
	g_BusinessServiceSet->MainEntitySelector->ReadData(Ret_pStatus,ENTITY_BUS_PERSON,empty,lstPersons);
	if(!Ret_pStatus.IsOK()) return;

	int nSize=RetOut_DataInvite.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		if (RetOut_DataInvite.getDataRef(i,"CENT_CE_TYPE_ID").toInt()>0)
		{
			QString strSQL="SELECT CEVT_NAME FROM CE_EVENT_TYPE WHERE CEVT_ID = "+QString::number(RetOut_DataInvite.getDataRef(i,"CENT_CE_TYPE_ID").toInt());
			query.Execute(Ret_pStatus,strSQL);
			if(!Ret_pStatus.IsOK()) return;
			DbRecordSet row;
			query.FetchData(row);
			if (row.getRowCount()>0)
				RetOut_DataInvite.setData(i,"TYPE_NAME",row.getDataRef(0,0).toString());
		}

		if (RetOut_DataInvite.getDataRef(i,"CENT_OWNER_ID").toInt()>0)
		{
			QString strSQL="SELECT BPER_FIRST_NAME,BPER_LAST_NAME FROM BUS_PERSON WHERE BPER_ID = "+QString::number(RetOut_DataInvite.getDataRef(i,"CENT_OWNER_ID").toInt());
			query.Execute(Ret_pStatus,strSQL);
			if(!Ret_pStatus.IsOK()) return;
			DbRecordSet row;
			query.FetchData(row);
			if (row.getRowCount()>0)
				RetOut_DataInvite.setData(i,"OWNER_NAME",row.getDataRef(0,"BPER_FIRST_NAME").toString()+" "+row.getDataRef(0,"BPER_LAST_NAME").toString());
		}

		int nTemplateID=RetOut_DataInvite.getDataRef(i,"BCIV_EMAIL_TEMPLATE_ID").toInt();
		if (nTemplateID<=0)
			nTemplateID=nDefaultEmailTemplateID;

		if (nTemplateID>0)
		{
			//QString strSQL="SELECT BEM_BODY FROM BUS_EMAIL WHERE BEM_ID = "+QString::number(nTemplateID);
			QString strSQL="SELECT BEB_BODY AS BEM_BODY FROM BUS_EMAIL LEFT OUTER JOIN BUS_EMAIL_BINARY ON BEB_EMAIL_ID = BEM_ID WHERE BEM_ID="+QString::number(nTemplateID);
			query.Execute(Ret_pStatus,strSQL);
			if(!Ret_pStatus.IsOK()) return;
			DbRecordSet row;
			query.FetchData(row);
			if (row.getRowCount()>0)
				RetOut_DataInvite.setData(i,"EMAIL_TEMPLATE_BODY",row.getDataRef(0,"BEM_BODY").toString());
		}

		//host/recipient:
		//from owner_id: (person) get contact id+default mail address:
		int nContactID=-1;
		int nRow=lstPersons.find("BPER_ID",RetOut_DataInvite.getDataRef(i,"CENT_OWNER_ID").toInt(),true);
		if (nRow>=0)
		{
			nContactID=lstPersons.getDataRef(nRow,"BPER_CONTACT_ID").toInt();
		}

		int nRecipientContactID=RetOut_DataInvite.getDataRef(i,"RECIPIENT_CONTACT_ID").toInt();
		QString strOwnerEmail;
		QString strRecepientEmail=RetOut_DataInvite.getDataRef(i,"BCIV_EMAIL_ADDRESS").toString();;
		Status err;

		DbRecordSet lstDefaults;
		if (nContactID>0)
		{
			g_BusinessServiceSet->BusContact->ReadContactDefaults(err, nContactID, lstDefaults);
		}
		if (lstDefaults.getRowCount()>0)
			strOwnerEmail=lstDefaults.getDataRef(0,"BCME_ADDRESS").toString();

		if (nRecipientContactID>0 && strRecepientEmail.isEmpty())
		{
			g_BusinessServiceSet->BusContact->ReadContactDefaults(err, nRecipientContactID, lstDefaults);
			if (lstDefaults.getRowCount()>0)
				strRecepientEmail=lstDefaults.getDataRef(0,"BCME_ADDRESS").toString();
		}

		RetOut_DataInvite.setData(i,"ORGANIZER_MAIL",strOwnerEmail);
		RetOut_DataInvite.setData(i,"ORGANIZER_CONTACT_ID",nContactID);
		RetOut_DataInvite.setData(i,"RECIPIENT_MAIL",strRecepientEmail);

		//BY SMS, if phone is empty then get default one:
		if (RetOut_DataInvite.getDataRef(i,"BCIV_BY_SMS").toInt()>0 && nRecipientContactID>0 && RetOut_DataInvite.getDataRef(i,"BCIV_SMS_NUMBERS").toString().isEmpty())
		{
			DbRecordSet lstTypesForSMS;
			lstTypesForSMS.addColumn(QVariant::Int,"ID");
			lstTypesForSMS.addRow(2);
			lstTypesForSMS.setData(0,0,ContactTypeManager::SYSTEM_PHONE_MOBILE);
			lstTypesForSMS.setData(1,0,ContactTypeManager::SYSTEM_PHONE_PRIVATE_MOBILE);
			DbRecordSet lstSMSPhones;
			g_BusinessServiceSet->BusContact->ReadContactPhones(err,nRecipientContactID,lstSMSPhones,-1,lstTypesForSMS);

			if (lstSMSPhones.getRowCount()>0)
			{
				RetOut_DataInvite.setData(i,"BCIV_SMS_NUMBERS",lstSMSPhones.getDataRef(0,"BCMP_SEARCH").toString());
			}
		}


		//RetOut_DataInvite.setData(i,"RECIPIENT_CONTACT_ID",nRecipientContactID);
	}
}

//just update subject and OUID
void Service_BusCalendar::WriteInviteRecordsAfterSend(Status &Ret_pStatus, DbRecordSet &DataInvite,bool bOnlyUpdateSentStatus)
{

	//DataInvite.Dump();

	DbSqlQuery query(Ret_pStatus, GetDbManager());
	if(!Ret_pStatus.IsOK()) return;

	if (bOnlyUpdateSentStatus)
	{
		int nSize=DataInvite.getRowCount();
		for (int i=0;i<nSize;i++)
		{
			int nInviteID=DataInvite.getDataRef(i,"BCIV_ID").toInt();
			QString strSQL="UPDATE BUS_CAL_INVITE SET BCIV_IS_SENT=1 WHERE BCIV_ID="+QString::number(nInviteID);
			query.Execute(Ret_pStatus,strSQL);
			if(!Ret_pStatus.IsOK()) return;
		}
		return;
	}
	else
	{
		QString strSQL="UPDATE BUS_CAL_INVITE SET BCIV_OUID=?, BCIV_SUBJECT_ID=?, BCIV_EMAIL_TEMPLATE_ID=?, BCIV_IS_SENT=? WHERE BCIV_ID=?";
		query.Prepare(Ret_pStatus,strSQL);
		if(!Ret_pStatus.IsOK()) return;

		int nSize=DataInvite.getRowCount();
		for (int i=0;i<nSize;i++)
		{
			int nSetSent=1;
			query.bindValue(0,DataInvite.getDataRef(i,"BCIV_OUID").toString());
			query.bindValue(1,DataInvite.getDataRef(i,"BCIV_SUBJECT_ID").toString());
			query.bindValue(2,DataInvite.getDataRef(i,"BCIV_EMAIL_TEMPLATE_ID"));
			query.bindValue(3,nSetSent);
			query.bindValue(4,DataInvite.getDataRef(i,"BCIV_ID").toInt());
			query.ExecutePrepared(Ret_pStatus);
			if(!Ret_pStatus.IsOK()) return;
		}
	}

}


//By Subject ID and OUID find INVITE record: if found, update status, if not found create new event: assign to owner/attendee
//nTypeOfMessage: 0- response, 1-invite from host, 2-cancel event from host
void Service_BusCalendar::WriteInviteReplyStatus(Status &Ret_pStatus, DbRecordSet &DataInvite, int nTypeOfMessage)
{
	if (DataInvite.getRowCount()==0)
		return;

	DbSimpleOrm TableOrm(Ret_pStatus, BUS_CAL_INVITE, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	//TRY TO FIND INVITE BY SUBJECT_ID or OUTLOOK ID
	QString strSubjectID=DataInvite.getDataRef(0,"BCIV_SUBJECT_ID").toString();
	QString strOutlookID=DataInvite.getDataRef(0,"BCIV_OUID").toString();
	QString strWhere=" WHERE BCIV_SUBJECT_ID='"+strSubjectID+"'";

	DbRecordSet rowInvite;
	TableOrm.Read(Ret_pStatus,rowInvite,strWhere);
	if(!Ret_pStatus.IsOK()) return;
	if (rowInvite.getRowCount()==0)
	{
		//TRY TO FIND INVITE BY OUL_ID
		strWhere=" WHERE BCIV_OUID='"+strOutlookID+"'";
		TableOrm.Read(Ret_pStatus,rowInvite,strWhere);
		if(!Ret_pStatus.IsOK()) return;
	}


	if (nTypeOfMessage==0)
	{
		//event was not found and reply received: ignore
		if (rowInvite.getRowCount()==0)
			return;

		rowInvite.setData(0,"BCIV_STATUS",DataInvite.getDataRef(0,"BCIV_STATUS"));
		rowInvite.setData(0,"BCIV_ANSWER_TEXT",DataInvite.getDataRef(0,"BCIV_ANSWER_TEXT"));
		TableOrm.Write(Ret_pStatus,rowInvite);
		return;
	}

	if (nTypeOfMessage==1)
	{
		//PROTOCOL: we have received invite from outside system (OL user or some other system in ivCal format: create new cal_Event
		//if exists it will be skipped: by OL id
		DbRecordSet empty;
		WriteFromOutlook(Ret_pStatus, DataInvite,true,empty);
		return;
	}

	if (nTypeOfMessage==2)
	{
		//PROTOCOL: ?? host canceled event: find it in database and set status as canceled
		DbRecordSet dataOld;
		FindCalendarPartByOutlookID(Ret_pStatus, DataInvite.getDataRef(0,"BCIV_OUID").toString(), dataOld);
		if(!Ret_pStatus.IsOK()) return;

		if(dataOld.getRowCount()>0)
		{
			QString strSQL="UPDATE BUS_CAL_EVENT_PART SET BCEP_STATUS = "+QString::number(GlobalConstants::TYPE_CAL_CANCELLED)+" WHERE BCEP_ID="+dataOld.getDataRef(0,"BCEP_ID").toString();
			TableOrm.GetDbSqlQuery()->Execute(Ret_pStatus,strSQL);
		}
	}


}

void Service_BusCalendar::GetEventParts(Status &Ret_pStatus, DbRecordSet &Ret_EventParts, QString &strSQL, DbRecordSet &recFilter, QDateTime &datStartDate, QDateTime &datEndDate, int nEntityID/*=-1*/)
{
	bool bShowPreliminary = recFilter.getDataRef(0, "SHOW_PRELIMINARY_EVENTS").toBool();
	bool bShowInformationOnly = recFilter.getDataRef(0, "SHOW_INFORMATION_EVENTS").toBool();
	bool bFilterByType = recFilter.getDataRef(0, "FILTER_BY_CALENDAR_TYPE").toBool();
	QByteArray ar = recFilter.getDataRef(0, "CALENDAR_TYPE_RECORDSET").toByteArray();
	DbRecordSet recCalTypeIDs = XmlUtil::ConvertByteArray2RecordSet_Fast(ar);
	QStringList lstTypeIDsList;

	if (!bShowPreliminary)
		strSQL += " AND BCEP_STATUS<>1 "; 
	if (!bShowInformationOnly)
		strSQL += " AND (BCEV_IS_INFORMATION<>1 OR BCEV_IS_INFORMATION IS NULL) "; 

	if (bFilterByType)
	{
		int nRowCount = recCalTypeIDs.getRowCount();
		for(int i = 0; i < nRowCount; i++)
			lstTypeIDsList << recCalTypeIDs.getDataRef(i, "CHECKED_ITEM_ID").toString();

		if (!lstTypeIDsList.isEmpty())
		{
			strSQL += " AND CENT_CE_TYPE_ID IN (" + lstTypeIDsList.join(",") + ") ";
		}
	}

	//if (recFilter.getDataRef(0, "SHOW_PRELIMINARY_EVENTS").toInt() == 1)
	//	strSQL += " AND BCEV_IS_PRELIMINARY=1 "; 
	//if (recFilter.getDataRef(0, "SHOW_INFORMATION_EVENTS").toInt() == 1)
	//	strSQL += " AND BCEV_IS_INFORMATION=1 "; 

	if (nEntityID>=0)
	{
		strSQL += " ORDER BY BCOL_FROM "; 
		Ret_EventParts.defineFromView(DbSqlTableView::getView(DbSqlTableView::MVIEW_CALENDAR_EVENT_PARTS));
	}
	else
	{
		Ret_EventParts.defineFromView(DbSqlTableView::getView(DbSqlTableView::MVIEW_CALENDAR_EVENT_PARTS_SIMPLE));
	}

	DbSqlQuery query(Ret_pStatus, GetDbManager());
	if(!Ret_pStatus.IsOK()) return;


	query.Prepare(Ret_pStatus, strSQL);
	if (!Ret_pStatus.IsOK()) return;
	query.bindValue(0, datStartDate);
	query.bindValue(1, datEndDate);
	query.bindValue(2, datStartDate);
	query.bindValue(3, datStartDate);
	query.bindValue(4, datEndDate);
	query.bindValue(5, datEndDate);
	if (nEntityID>=0)
	{
		query.bindValue(6, nEntityID);
	}
	query.ExecutePrepared(Ret_pStatus);
	if (!Ret_pStatus.IsOK()) return;

	query.FetchData(Ret_EventParts);
}

//from invite record create new event: organizer, recipient
void Service_BusCalendar::CreateNewCalendarEvent(Status &Ret_pStatus, DbRecordSet &rowInvite,DbRecordSet &Ret_Event,DbRecordSet &Ret_Parts,DbRecordSet &lstPersons)
{
	if (rowInvite.getRowCount()==0)
	{
		return;
	}

	if (lstPersons.getRowCount()==0)
	{
		DbRecordSet empty;
		g_BusinessServiceSet->MainEntitySelector->ReadData(Ret_pStatus,ENTITY_BUS_PERSON,empty,lstPersons);
		if(!Ret_pStatus.IsOK()) return;
	}

	int nLoggedID=g_UserSessionManager->GetPersonID();
	//base fields+owner
	CalendarHelperCore::CreateCalendarDataFromInviteData(rowInvite,Ret_Event,Ret_Parts,lstPersons,nLoggedID);
	if (Ret_Event.getRowCount()==0 || Ret_Parts.getRowCount()==0 )
	{
		return;
	}

	//BT: use only contact list from above: ignore these fields:
	/*
	//assignments: organizer + recipient
	//invite records: set all status as invited..
	DbRecordSet lstNMRXPerson=Ret_Parts.getDataRef(0,"NMRX_PERSON_LIST").value<DbRecordSet>();
	if (lstNMRXPerson.getColumnCount()==0)
		lstNMRXPerson.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION_SELECT));

	int nOwnerContactID=Ret_Parts.getDataRef(0,"CENT_OWNER_ID").toInt();
	DbRecordSet dataAssign;
	dataAssign.addColumn(QVariant::Int,"ID");
	dataAssign.addRow();
	dataAssign.setData(0,0,nOwnerContactID);

	if (rowInvite.getDataRef(0,"RECIPIENT_CONTACT_ID").toInt()>0)
	{
		dataAssign.addRow();
		dataAssign.setData(1,0,rowInvite.getDataRef(0,"RECIPIENT_CONTACT_ID").toInt());
	}

	AssignCalendarContacts(Ret_pStatus,Ret_Parts,dataAssign,lstPersons);
	*/
}


//RetOut_Data is in TVIEW_BUS_CAL_INVITE_ICALENDAR_FIELDS
//old calendar entries are recognized by BCIV_OUID
//NOTE: every cal event owner is logged user: assigned contacts are taken from CONTACT_ASSIGN_LIST list (TVIEW_BUS_NMRX_RELATION+BNRO_ROLE)
//Warning: RetOut_Data will return only new BCEV_ID's
//Ret_lstStatuses for each row in RetOut_Data it will return STATUS_CODE/TEXT
void Service_BusCalendar::WriteFromOutlook(Status &Ret_pStatus, DbRecordSet &RetOut_Data, bool bSkipExisting,DbRecordSet &Ret_lstStatuses)
{
	Ret_lstStatuses.destroy();
	Ret_lstStatuses.addColumn(QVariant::Int,"Status Code");
	Ret_lstStatuses.addColumn(QVariant::String,"Status Text");

	DbSqlQuery query(Ret_pStatus, GetDbManager());
	if(!Ret_pStatus.IsOK()) return;

	DbRecordSet lstPersons,empty;
	g_BusinessServiceSet->MainEntitySelector->ReadData(Ret_pStatus,ENTITY_BUS_PERSON,empty,lstPersons);
	if(!Ret_pStatus.IsOK()) return;

	int nSize=RetOut_Data.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		Ret_lstStatuses.addRow();
		int nStatRow=Ret_lstStatuses.getRowCount()-1;
		Ret_lstStatuses.setData(nStatRow,0,0);

		DbRecordSet rowInvite=RetOut_Data.getRow(i);
		DbRecordSet rowEvent,rowPart;

		//test TITLE, FROM AND TO: if invalid skip and mark as error:
		if (rowInvite.getDataRef(0,"BCOL_FROM").isNull() || rowInvite.getDataRef(0,"BCOL_TO").isNull())
		{
			Ret_lstStatuses.setData(nStatRow,0,1);
			Ret_lstStatuses.setData(nStatRow,1,"Calendar entry from and to date times are invalid");
			continue;
		}
		if (rowInvite.getDataRef(0,"BCEV_TITLE").isNull())
		{
			rowInvite.setData(0,"BCEV_TITLE","Unknown Event");
		}

		//search existing calendar entries by BCIV_OUID: if found then update if bSkipExisting = false, else insert, amen!
		//-------------------------------------------------------------------------------------
		DbRecordSet dataOld;
		FindCalendarPartByOutlookID(Ret_pStatus, rowInvite.getDataRef(0,"BCIV_OUID").toString(), dataOld);
		if (!Ret_pStatus.IsOK())
		{
			Ret_lstStatuses.setData(nStatRow,0,Ret_pStatus.getErrorCode());
			Ret_lstStatuses.setData(nStatRow,1,Ret_pStatus.getErrorText());
			continue;
		}
		if(dataOld.getRowCount()>0 && bSkipExisting)
			continue;
		

		//check dates: if there is calendar entry in database that already exists, then skip it
		//-------------------------------------------------------------------------------------
		/*
		QString strSQL="SELECT BCOL_ID FROM BUS_CAL_OPTIONS WHERE BCOL_FROM>=? AND BCOL_TO<=?";
		query.Prepare(Ret_pStatus,strSQL);
		if (!Ret_pStatus.IsOK())
		{
			Ret_lstStatuses.setData(nStatRow,0,Ret_pStatus.getErrorCode());
			Ret_lstStatuses.setData(nStatRow,1,Ret_pStatus.getErrorText());
			continue;
		}
		query.bindValue(0,rowInvite.getDataRef(0,"BCOL_FROM").toDateTime());
		query.bindValue(1,rowInvite.getDataRef(0,"BCOL_TO").toDateTime());
		query.ExecutePrepared(Ret_pStatus);
		if (!Ret_pStatus.IsOK())
		{
			Ret_lstStatuses.setData(nStatRow,0,Ret_pStatus.getErrorCode());
			Ret_lstStatuses.setData(nStatRow,1,Ret_pStatus.getErrorText());
			continue;
		}

		DbRecordSet dataOverlapping;
		query.FetchData(dataOverlapping);
		if (dataOverlapping.getRowCount()>0)
		{
			Ret_lstStatuses.setData(nStatRow,0,1);
			Ret_lstStatuses.setData(nStatRow,1,"Calendar entry with same range from/to already exists");
			continue;
		}
		*/
		//-------------------------------------------------------------------------------------

		//create new data from metadata record
		//warning: if exists, only part data should be updated!!!
		//-------------------------------------------------------------------------------------
		CreateNewCalendarEvent(Ret_pStatus, rowInvite,rowEvent,rowPart,lstPersons);
		if (!Ret_pStatus.IsOK())
		{
			Ret_lstStatuses.setData(nStatRow,0,Ret_pStatus.getErrorCode());
			Ret_lstStatuses.setData(nStatRow,1,Ret_pStatus.getErrorText());
			continue;
		}

		//from this list in nmrx format..create additional assignments:
		DbRecordSet lstContactToAssign=RetOut_Data.getDataRef(i,"CONTACT_ASSIGN_LIST").value<DbRecordSet>();
		if (lstContactToAssign.getColumnCount()>0 && lstContactToAssign.getRowCount()>0)
		{
			AssignCalendarContacts(Ret_pStatus,rowPart,lstContactToAssign,lstPersons);
			if(!Ret_pStatus.IsOK())
			{
				Ret_lstStatuses.setData(nStatRow,0,Ret_pStatus.getErrorCode());
				Ret_lstStatuses.setData(nStatRow,1,Ret_pStatus.getErrorText());
				continue;
			}
		}

		//assign old ID's if update is in progress: effectively: erase all prev data, just leave new...
		//BCEV_ID,BCEP_ID,BCOL_ID,CENT_ID,BCOL_FROM,BCOL_TO FROM BUS_CAL_EVENT ";
		//-------------------------------------------------------------------------------------
		if (dataOld.getRowCount()>0)
		{
			DbSimpleOrm TableOrm_CalEvent(Ret_pStatus, BUS_CAL_EVENT, GetDbManager()); 
			if(!Ret_pStatus.IsOK()) return;

			//pass old rowEvent: eventually move from-to
			TableOrm_CalEvent.Read(Ret_pStatus,rowEvent," WHERE BCEV_ID="+QString::number(dataOld.getDataRef(0,"BCEV_ID").toInt()));
			/*rowEvent.setData(0,"BCEV_ID",dataOld.getDataRef(0,"BCEV_ID"));*/

			rowPart.setData(0,"BCEP_ID",dataOld.getDataRef(0,"BCEP_ID"));
			rowPart.setData(0,"BCEP_EVENT_ID",dataOld.getDataRef(0,"BCEV_ID").toInt());
			rowPart.setData(0,"BCEP_COMM_ENTITY_ID",dataOld.getDataRef(0,"CENT_ID").toInt());
			rowPart.setData(0,"CENT_ID",dataOld.getDataRef(0,"CENT_ID"));

			DbRecordSet lstOptions=rowPart.getDataRef(0,"NMRX_PERSON_LIST").value<DbRecordSet>();
			lstOptions.setData(0,"BCOL_ID",dataOld.getDataRef(0,"BCOL_ID"));
			lstOptions.setData(0,"BCOL_EVENT_PART_ID",dataOld.getDataRef(0,"BCEP_ID").toInt());
			rowPart.setData(0,"NMRX_PERSON_LIST",lstOptions);
		}


		//write to DB:
		//-------------------------------------------------------------------------------------
		if (Ret_pStatus.IsOK())
		{
			//rowEvent.Dump();
			//rowPart.Dump();

			DbRecordSet empty;
			WriteEvent(Ret_pStatus, rowEvent, rowPart,empty,"", empty,empty);
			if(!Ret_pStatus.IsOK())
			{
				Ret_lstStatuses.setData(nStatRow,0,Ret_pStatus.getErrorCode());
				Ret_lstStatuses.setData(nStatRow,1,Ret_pStatus.getErrorText());
				continue;
			}

			if (!rowInvite.getDataRef(0,"BCIV_OUID").isNull())
			{
				WriteEventOutlookID(Ret_pStatus,rowPart.getDataRef(0,"BCEP_ID").toInt(),rowInvite.getDataRef(0,"BCIV_OUID").toString());
				if(!Ret_pStatus.IsOK())
				{
					Ret_lstStatuses.setData(nStatRow,0,Ret_pStatus.getErrorCode());
					Ret_lstStatuses.setData(nStatRow,1,Ret_pStatus.getErrorText());
					continue;
				}
			}

			RetOut_Data.setData(i,"BCEV_ID",rowEvent.getDataRef(0,"BCEV_ID"));
		}
	}

	//to allow Ret_lstStatuses list to return, if fatal error, exit at once..
	Ret_pStatus.setError(0);

}

//lstContactAssign = TVIEW_BUS_NMRX_RELATION+BNRO_ROLE
void Service_BusCalendar::AssignCalendarContacts(Status &Ret_pStatus, DbRecordSet &rowPart,DbRecordSet &lstContactAssign,DbRecordSet &lstPersons)
{
	if (lstContactAssign.getRowCount()==0)
		return;

	DbRecordSet lstNMRXPerson=rowPart.getDataRef(0,"NMRX_PERSON_LIST").value<DbRecordSet>();
	if (lstNMRXPerson.getColumnCount()==0)
		lstNMRXPerson.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION_SELECT));
	Status err;


	//---------------ONLY FOR FUKING ROLES:

	DbRecordSet lstRoles;
	lstRoles.addColumn(QVariant::String,"BNRO_NAME");
	lstRoles.merge(lstContactAssign);
	lstRoles.removeDuplicates(0);

	int nSize=lstRoles.getRowCount();
	DbRecordSet lstNMRXPerson_Chunk;
	lstNMRXPerson_Chunk.copyDefinition(lstNMRXPerson);
		
	for(int i=0;i<nSize;i++)
	{
		DbRecordSet lstContactAssign_Chunk;
		lstContactAssign_Chunk.destroy();
		lstContactAssign_Chunk.addColumn(QVariant::Int,"BNMR_TABLE_KEY_ID_2");
		lstContactAssign_Chunk.addColumn(QVariant::String,"BNRO_NAME");
		lstNMRXPerson_Chunk.clear();

		lstContactAssign.find("BNRO_NAME",lstRoles.getDataRef(i,0).toString());
		lstContactAssign_Chunk.merge(lstContactAssign,true);

		if (lstContactAssign_Chunk.getRowCount()>0)
		{
			bool bInserted;
			g_BusinessServiceSet->BusNMRX->CreateAssignmentData(Ret_pStatus,lstNMRXPerson_Chunk,CE_COMM_ENTITY,BUS_CM_CONTACT,lstContactAssign_Chunk,lstContactAssign_Chunk.getDataRef(0,"BNRO_NAME").toString(),0,BUS_CAL_INVITE,lstPersons,bInserted);
			if(!Ret_pStatus.IsOK()) return;

			lstNMRXPerson.merge(lstNMRXPerson_Chunk);
		}
	}

	//sort by role: 
	lstNMRXPerson.sort("BNRO_NAME");

	//lstNMRXPerson.Dump();


	int nSize2=lstNMRXPerson.getRowCount();
	for (int k=0;k<nSize2;k++)
	{
		DbRecordSet lstInvite=lstNMRXPerson.getDataRef(0,"X_RECORD").value<DbRecordSet>();
		lstInvite.setData(0,"BCIV_IS_SENT",1);
		lstNMRXPerson.setData(0,"X_RECORD",lstInvite);
	}

	rowPart.setData(0,"NMRX_PERSON_LIST",lstNMRXPerson);
}


//RetOut_Data is in TVIEW_BUS_CAL_INVITE_ICALENDAR_FIELDS
//CONTACT_ASSIGN_LIST contains assignments +EMAIL field (email address of each contact)
void Service_BusCalendar::ReadForOutlook(Status &Ret_pStatus, DbRecordSet &Ret_Data, QDate datFrom, QDate datTo)
{
	DbSqlQuery query(Ret_pStatus, GetDbManager());
	if(!Ret_pStatus.IsOK()) return;
	Ret_Data.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_INVITE_ICALENDAR_FIELDS));

	DbRecordSet lstPersons;
	DbRecordSet empty;
	g_BusinessServiceSet->MainEntitySelector->ReadData(Ret_pStatus,ENTITY_BUS_PERSON,empty,lstPersons);
	if(!Ret_pStatus.IsOK()) return;

	//get all entries, skip templates and preliminary and information
	QString strSQL="SELECT BCEV_ID,BCEP_ID,BCOL_ID,CENT_ID,BCPOD_OUTLOOK_ID,BCOL_FROM,BCOL_TO FROM BUS_CAL_EVENT ";
	strSQL+="INNER JOIN BUS_CAL_EVENT_PART ON BCEV_ID = BCEP_EVENT_ID ";
	strSQL+="INNER JOIN CE_COMM_ENTITY ON CENT_ID = BCEP_COMM_ENTITY_ID ";
	strSQL+="INNER JOIN BUS_CAL_OPTIONS ON BCEP_ID = BCOL_EVENT_PART_ID ";
	strSQL+="LEFT JOIN BUS_CAL_EV_PART_OUID ON BCEP_ID = BCPOD_PART_ID ";
	strSQL+="WHERE BCEV_TEMPLATE_FLAG = 0 AND (BCEV_IS_PRELIMINARY = 0 OR BCEV_IS_PRELIMINARY IS NULL) AND (BCEV_IS_INFORMATION=0 OR BCEV_IS_INFORMATION IS NULL) AND BCOL_FROM>=?";

	if (datTo.isValid())
		strSQL+=" AND BCOL_TO<=? ORDER BY BCEP_ID,BCOL_ID";
	else
		strSQL+=" ORDER BY BCEP_ID,BCOL_ID";
	
	query.Prepare(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK()) return;
	query.bindValue(0,datFrom);
	if (datTo.isValid())
		query.bindValue(1,datTo);

	query.ExecutePrepared(Ret_pStatus);
	if(!Ret_pStatus.IsOK()) return;

	DbRecordSet lstData;
	query.FetchData(lstData);

	//now: find proposals: same BCEP_ID, different BCOL_ID-> skip em
	int nSize=lstData.getRowCount();
	if (nSize==0)
		return;

	DbRecordSet tmpList=lstData;
	for (int i=0;i<nSize;i++)
	{
		int nBcepID=tmpList.getDataRef(i,"BCEP_ID").toInt();
		tmpList.find("BCEP_ID",nBcepID);
		if (tmpList.getSelectedCount()>1)
		{
			lstData.find("BCEP_ID",nBcepID);
			lstData.deleteSelectedRows();
		}
	}

	//lstData contains all id's, now fetch full data & convert to invite record:

	nSize=lstData.getRowCount();
	for (int z=0;z<nSize;z++)
	{
		//read event data one by one:
		//---------------------------------
		int nCalEventID= lstData.getDataRef(z,"BCEV_ID").toInt();
		DbRecordSet rowEvent,rowPart,rowTasks,rowUAR,rowGAR;
		ReadEvent(Ret_pStatus, nCalEventID, rowEvent, rowPart, rowTasks,rowUAR,rowGAR);
		if(!Ret_pStatus.IsOK()) return;

		//convert to invite record: special care to assignemtns records: nmrxperson->copy to contact assign list
		//---------------------------------
		DbRecordSet lstDummy,lstEmpty;
		DbRecordSet lstInviteRecords;
		CalendarHelperCore::CopyDataFromCalendarDataToInviteRecord(rowEvent,rowPart,lstInviteRecords,lstDummy);
		if (lstInviteRecords.getRowCount()>0)
			lstInviteRecords.setData(0,"BCIV_OUID",lstData.getDataRef(z,"BCPOD_OUTLOOK_ID"));


		//add mail address to the contact assign list:
		DbRecordSet lstAssignments=lstInviteRecords.getDataRef(0,"CONTACT_ASSIGN_LIST").value<DbRecordSet>();
		int nSize_3=lstAssignments.getRowCount();
		lstAssignments.addColumn(QVariant::String,"EMAIL");
		for (int y=0;y<nSize_3;y++)
		{
			int nContactID=lstAssignments.getDataRef(y,"BNMR_TABLE_KEY_ID_2").toInt();
			DbRecordSet lstContactData;
			g_BusinessServiceSet->BusContact->ReadContactDefaults(Ret_pStatus, nContactID, lstContactData);
			if(!Ret_pStatus.IsOK()) return;
			if (lstContactData.getRowCount()>0)
				lstAssignments.setData(y,"EMAIL",lstContactData.getDataRef(0,"BCME_ADDRESS"));
		}
		lstInviteRecords.setData(0,"CONTACT_ASSIGN_LIST",lstAssignments);
		Ret_Data.merge(lstInviteRecords);
	}
}

//after importing from outlook save outlook-id if not already saved
void Service_BusCalendar::WriteEventOutlookID(Status &Ret_pStatus, int nPartID,QString strOutlookID)
{
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_CAL_EV_PART_OUID, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	DbSqlQuery *query=TableOrm.GetDbSqlQuery();

	QString strSQL="SELECT BCPOD_OUTLOOK_ID FROM BUS_CAL_EV_PART_OUID WHERE BCPOD_OUTLOOK_ID ='"+strOutlookID+"' AND BCPOD_PART_ID="+QString::number(nPartID);
	query->Execute(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK()) return;
	DbRecordSet data;
	query->FetchData(data);
	if (data.getRowCount()>0)
		return;

	DbRecordSet rowData;
	rowData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CAL_EV_PART_OUID));
	rowData.addRow();
	rowData.setData(0,"BCPOD_OUTLOOK_ID",strOutlookID);
	rowData.setData(0,"BCPOD_PART_ID",nPartID);

	TableOrm.Write(Ret_pStatus,rowData);

}

void Service_BusCalendar::DeleteEventPartPrivate(Status &Ret_pStatus, DbSimpleOrm &TableOrm_CalEvent, DbSimpleOrm &TableOrm_CalPart, DbSimpleOrm &TableOrm_CalOptions, int nCalEventPartID, int nBCOL_ID, int nBCEV_ID)
{
	//find cal event id
	QString strSQL="SELECT BCEV_ID FROM BUS_CAL_EVENT";
	strSQL+=" INNER JOIN BUS_CAL_EVENT_PART ON BCEP_EVENT_ID = BCEV_ID ";
	strSQL+=" WHERE BCEP_ID =" + QString::number(nCalEventPartID);

	//Part count.
	DbSqlQuery *query=TableOrm_CalPart.GetDbSqlQuery();
	query->Execute(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK()) return;
	int nCalEvID=-1;
	if(query->next())
	{
		nCalEvID=query->value(0).toInt();
	}
	else
	{
		Ret_pStatus.setError(1,"Invalid Calendar Part ID");
		return;
	}

	//find count event part id 
	int nPartCount=0;
	strSQL="SELECT COUNT(*) FROM BUS_CAL_EVENT";
	strSQL+=" INNER JOIN BUS_CAL_EVENT_PART ON BCEP_EVENT_ID = BCEV_ID ";
	strSQL+=" INNER JOIN BUS_CAL_OPTIONS ON BCOL_EVENT_PART_ID = BCEP_ID ";
	strSQL+=" WHERE BCEP_EVENT_ID =" + QString::number(nCalEvID);

	query->Execute(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK()) return;
	if(query->next())
		nPartCount=query->value(0).toInt();

	//Options count.
	int nOptionsCount=0;
	strSQL="SELECT COUNT(*) FROM BUS_CAL_OPTIONS";
	strSQL+=" WHERE BCOL_EVENT_PART_ID =" + QString::number(nCalEventPartID);

	query->Execute(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK()) return;
	if(query->next())
		nOptionsCount=query->value(0).toInt();

	if ((nPartCount==1) && (nOptionsCount==1)) //if nPartCount==1 and no more than one option, delete whole event
	{
		QList<int> lstDel;
		lstDel<<nCalEvID;
		TableOrm_CalEvent.DeleteFast(Ret_pStatus,lstDel);
	}
	else if ((nPartCount>1) && (nOptionsCount==1))//if nPartCount !=1 and no more than one option, delete only this part!: deletion from part table: cecomm is deleted by trigger
	{
		QList<int> lstDel;
		lstDel<<nCalEventPartID;
		TableOrm_CalPart.DeleteFast(Ret_pStatus,lstDel);
	}
	else //Delete only this option.
	{
		QList<int> lstDel;
		lstDel<<nBCOL_ID;
		TableOrm_CalOptions.DeleteFast(Ret_pStatus,lstDel);
	}
}
//Ret_Data will contain all id's of part, event etc..or empty if not found..
void Service_BusCalendar::FindCalendarPartByOutlookID(Status &Ret_pStatus, QString strOutlookID, DbRecordSet &Ret_Data)
{
	DbSqlQuery query(Ret_pStatus, GetDbManager());
	if(!Ret_pStatus.IsOK()) return;

	if (!strOutlookID.isEmpty())
	{
		QString strSQL="SELECT BCEV_ID,BCEP_ID,BCOL_ID,CENT_ID,BCOL_FROM,BCOL_TO FROM BUS_CAL_EVENT ";
		strSQL+="INNER JOIN BUS_CAL_EVENT_PART ON BCEV_ID = BCEP_EVENT_ID ";
		strSQL+="INNER JOIN BUS_CAL_OPTIONS ON BCEP_ID = BCOL_EVENT_PART_ID ";
		strSQL+="INNER JOIN CE_COMM_ENTITY ON CENT_ID = BCEP_COMM_ENTITY_ID ";
		strSQL+="INNER JOIN BUS_CAL_EV_PART_OUID ON BCEP_ID = BCPOD_PART_ID ";
		strSQL+="WHERE BCPOD_OUTLOOK_ID='"+strOutlookID+"'";

		query.Execute(Ret_pStatus,strSQL);
		if (!Ret_pStatus.IsOK())	return;

		query.FetchData(Ret_Data);
	}

}

void Service_BusCalendar::ModifyEventPresence(Status &Ret_pStatus, QDateTime datFrom, QDateTime datTo, DbRecordSet recFilter, DbRecordSet lstAssignedEntities, int nPresenceTypeID)
{

	//recFilter.setData(0,"SHOW_PRELIMINARY_EVENTS",1);

	//READ ALL EVENTS FOR GIVEN RANGE BASED ON FILTER AND ON entity id, type
	DbRecordSet lstEventPartsAll;
	int nSize=lstAssignedEntities.getRowCount();
	for (int i=0;i<nSize;++i)
	{
		int nEntityID=lstAssignedEntities.getDataRef(i,"ENTITY_ID").toInt();
		int nEntityType=lstAssignedEntities.getDataRef(i,"ENTITY_TYPE").toInt();
		if (nEntityType==GlobalConstants::TYPE_CAL_USER_SELECT)
			nEntityType=GlobalConstants::TYPE_CAL_CONTACT_SELECT;

		DbRecordSet lstEventParts,lstContacts,lstRes,lstProjects,lstBreaks;
		//recFilter.Dump();
		//qDebug()<<datFrom;
		//qDebug()<<datTo;

		ReadEventParts(Ret_pStatus,recFilter,GlobalConstants::TYPE_CAL_CONTACT_SELECT,nEntityID,datFrom,datTo,lstEventParts,lstContacts,lstRes,lstProjects,lstBreaks);

		//lstEventParts.Dump();

		if(lstEventPartsAll.getColumnCount()==0)
			lstEventPartsAll.copyDefinition(lstEventParts);

		lstEventPartsAll.merge(lstEventParts);
	}

	//remove duplicate events:
	lstEventPartsAll.removeDuplicates(lstEventPartsAll.getColumnIdx("BCEP_ID"));


	//set sql: update all 

	DbSqlQuery query(Ret_pStatus, GetDbManager());
	if(!Ret_pStatus.IsOK()) return;


	QString strSQL="UPDATE BUS_CAL_EVENT_PART SET BCEP_PRESENCE_TYPE_ID = "+QString::number(nPresenceTypeID)+" WHERE BCEP_ID IN (";
	nSize=lstEventPartsAll.getRowCount();
	if (nSize==0)
		return;
	for (int i=0;i<nSize;++i)
	{
		strSQL +=lstEventPartsAll.getDataRef(i,"BCEP_ID").toString()+",";
	}
	strSQL.chop(1);
	strSQL+=")";

	query.Execute(Ret_pStatus,strSQL);


}

void Service_BusCalendar::GetCalFilterViewEntitesAndTypeIDsData(Status &pStatus, DbRecordSet &recViews, DbRecordSet &recCalEntities, DbRecordSet &recCalTypeIDs)
{
	//Read entities.
	DbSimpleOrm BUS_CAL_VIEW_ENTITIES_ORM(pStatus, BUS_CAL_VIEW_ENTITIES, GetDbManager()); 
	if(!pStatus.IsOK()) return;

	QString strWhereAfter=" ORDER BY BCVE_CAL_VIEW_ID, BCVE_SORT_CODE ASC";
	BUS_CAL_VIEW_ENTITIES_ORM.ReadFromParentIDs(pStatus, recCalEntities, "BCVE_CAL_VIEW_ID", recViews, "BCALV_ID", QString(), strWhereAfter, DbSqlTableView::TVIEW_BUS_CAL_VIEW_ENTITIES_SELECT);
	if(!pStatus.IsOK()) return;

	//_DUMP(recViews);

	//Select all rows where there exists person id.
	DbRecordSet recUserEntities;
	recUserEntities.copyDefinition(recCalEntities);
	int nEntitiesCount=recCalEntities.getRowCount();
	recCalEntities.clearSelection();
	for(int i=0; i<nEntitiesCount; i++)
	{
		if(recCalEntities.getDataRef(i, "BCVE_USER_ID").toInt()>0)
			recCalEntities.selectRow(i);
	}
	recUserEntities.merge(recCalEntities, true);
	recCalEntities.deleteSelectedRows();

	//calculated person names: 
	MainEntityCalculatedName personFiller;
	personFiller.Initialize(ENTITY_BUS_PERSON,recUserEntities);
	personFiller.FillCalculatedNames(recUserEntities,recUserEntities.getColumnIdx("BPER_NAME"),recUserEntities.getColumnIdx("BPER_DEPT_NAME"));

	//Select all rows where there exists contact id.
	DbRecordSet recContactEntities;
	recContactEntities.copyDefinition(recCalEntities);
	nEntitiesCount=recCalEntities.getRowCount();
	recCalEntities.clearSelection();
	for(int i=0; i<nEntitiesCount; i++)
	{
		if(recCalEntities.getDataRef(i, "BCVE_CONTACT_ID").toInt()>0)
			recCalEntities.selectRow(i);
	}
	recContactEntities.merge(recCalEntities, true);
	recCalEntities.deleteSelectedRows();

	//calculated contact names: 
	MainEntityCalculatedName contactFiller;
	contactFiller.Initialize(ENTITY_BUS_CONTACT,recContactEntities);
	contactFiller.FillCalculatedNames(recContactEntities,recContactEntities.getColumnIdx("BCNT_NAME"));

	//Merge the data and sort it nicely.
	recCalEntities.merge(recUserEntities);
	recCalEntities.merge(recContactEntities);

	SortDataList defaultSortList;
	SortData sortID(3); //sort by BCVE_CAL_VIEW_ID
	SortData sortSortCode(9); //sort by BCVE_SORT_CODE
	defaultSortList << sortID << sortSortCode;
	recCalEntities.sortMulti(defaultSortList);

	//_DUMP(recCalEntities);

	//Read ce types.
	DbSimpleOrm BUS_CAL_VIEW_CE_TYPES_ORM(pStatus, BUS_CAL_VIEW_CE_TYPES, GetDbManager()); 
	if(!pStatus.IsOK()) return;

	BUS_CAL_VIEW_CE_TYPES_ORM.ReadFromParentIDs(pStatus, recCalTypeIDs, "BCCT_CAL_VIEW_ID", recViews, "BCALV_ID");
	if(!pStatus.IsOK()) return;

	//_DUMP(recCalTypeIDs);
}
