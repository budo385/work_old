#include "service_ipaccesslist.h"
#include "db/db/dbsimpleorm.h"



/*!
	Reads one list by unique name

	\param pStatus		- if err occur, returned here
	\param strListName	- unique list name
	\param lstRecords	- full view in TVIEW_CORE_IP_ACCESS_LIST
	\param pDbConn		- db conn
*/
void Serivce_IPAccessList::Read(Status &pStatus,QString strListName,DbRecordSet& lstRecords)
{
	//make local query, can fail if connection reservation fails
	DbSimpleOrm TableOrm(pStatus,CORE_IPACCESSLIST,GetDbManager()); if(!pStatus.IsOK()) return;

	//read:
	TableOrm.Read(pStatus,lstRecords,"WHERE CIPR_LIST_NAME= '"+strListName+"'");


}

/*!
	Writes one list by unique name (insert/update)

	\param pStatus		- if err occur, returned here
	\param strListName	- unique list name
	\param lstRecords	- full view in TVIEW_CORE_IP_ACCESS_LIST
	\param pDbConn		- db conn
*/
void Serivce_IPAccessList::Write(Status &pStatus,QString strListName,DbRecordSet& lstRecords)
{

	DbRecordSet lstReturnStatuses;
	Status errLock;

	//make local query, can fail if connection reservation fails
	DbSimpleOrm TableOrm(pStatus,CORE_IPACCESSLIST,GetDbManager()); if(!pStatus.IsOK()) return;

	//LOCK:
	DbLockList lstLock;
	DbSqlTableDefinition::CopyFromRecordSet2List(CORE_IPACCESSLIST,lstLock,lstRecords);
	//TableOrm.GetDbSqlQuery()->LockRecordsDirect(pStatus,"CORE_IPACCESSLIST","CIPR_ID",lstLock);
	if(!pStatus.IsOK())return;

	//qDebug()<<lstRecords.getRowCount();

	//delete all previous content/no transaction needed
	Delete(pStatus,strListName);
	if(!pStatus.IsOK())return;

	//write:
	TableOrm.Write(pStatus,lstRecords,-1,0,&lstReturnStatuses);

	//test for errors:
	/*
	if(pStatus.IsOK()) 
		TableOrm.GetDbSqlQuery()->UnLockRecordsDirect(errLock,"CORE_IPACCESSLIST","CIPR_ID",lstLock);
	else
	{
		//find bad one:
		int nBadOne=lstReturnStatuses.find(1,0,true,false,true,false);
		if(nBadOne!=-1)
		{
			pStatus.setError(lstReturnStatuses.getDataRef(nBadOne,"STATUS_CODE").toInt(),
				lstReturnStatuses.getDataRef(nBadOne,"STATUS_TEXT").toString());
			TableOrm.UnLockRecordsDirect(errLock,lstRecords);
		}
	}
	*/

//	TableOrm.GetDbSqlQuery()->UnLockRecordsDirect(errLock,"CORE_IPACCESSLIST","CIPR_ID",lstLock);

}


/*!
	Deletes one list by unique name

	\param pStatus		- if err occur, returned here
	\param strListName	- unique list name
	\param pDbConn		- db conn
*/
void Serivce_IPAccessList::Delete(Status &pStatus,QString strListName)
{

	//make local query, can fail if connection reservation fails
	DbSqlQuery query(pStatus,GetDbManager());if(!pStatus.IsOK()) return;

	//prepare insert query:
	QString strSQL="DELETE FROM CORE_IPACCESSLIST WHERE CIPR_LIST_NAME= '"+strListName+"'";

	//exec & auto destroy any connection held:
	query.Execute(pStatus,strSQL);
}

