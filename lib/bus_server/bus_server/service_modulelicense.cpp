#include "service_modulelicense.h"


///GLOBAL OBJECT:
#include "bus_core/bus_core/licenseaccessrights.h"

extern LicenseAccessRights g_ModuleLicense;

void Service_ModuleLicense::GetFPList(Status& Ret_pStatus, QString& strModuleCode, QString& strMLIID, DbRecordSet& RetOut_pList)
{
	Ret_pStatus.setError(0);

	//pass to global object:
	g_ModuleLicense.GetFPList(strModuleCode,strMLIID,RetOut_pList);
}

void Service_ModuleLicense::GetLicenseInfo(Status& Ret_pStatus, /* QString& strModuleCode, QString& strMLIID,*/ QString& RetOut_strUserName, int& RetOut_nLicenseID, QString &RetOut_strReportLine1, QString &RetOut_strReportLine2, QString &Ret_strCustomerID,int &Ret_nCustomSolutionID)
{
	Ret_pStatus.setError(0);

	RetOut_strUserName		= g_ModuleLicense.GetLicenseInfo().m_strClientName;
	RetOut_nLicenseID		= g_ModuleLicense.GetLicenseInfo().m_nLicenseID;
	RetOut_strReportLine1	= g_ModuleLicense.GetLicenseInfo().m_strReportLine1;
	RetOut_strReportLine2	= g_ModuleLicense.GetLicenseInfo().m_strReportLine2;
	Ret_strCustomerID		= g_ModuleLicense.GetLicenseInfo().m_strCustomerID;
	Ret_nCustomSolutionID	= g_ModuleLicense.GetLicenseInfo().m_nCustomSolutionID;
}


void Service_ModuleLicense::GetAllModuleLicenseData(Status& Ret_pStatus, QString& strModuleCode, QString& strMLIID,DbRecordSet& RetOut_pList,QString& RetOut_strUserName, int& RetOut_nLicenseID, QString &RetOut_strReportLine1, QString &RetOut_strReportLine2, QString &Ret_strCustomerID,int &Ret_nCustomSolutionID)
{
	Ret_pStatus.setError(0);
	g_ModuleLicense.GetFPList(strModuleCode,strMLIID,RetOut_pList);
	RetOut_strUserName	= g_ModuleLicense.GetLicenseInfo().m_strClientName;
	RetOut_nLicenseID	= g_ModuleLicense.GetLicenseInfo().m_nLicenseID;
	RetOut_strReportLine1 = g_ModuleLicense.GetLicenseInfo().m_strReportLine1;
	RetOut_strReportLine2 = g_ModuleLicense.GetLicenseInfo().m_strReportLine2;
	Ret_strCustomerID		= g_ModuleLicense.GetLicenseInfo().m_strCustomerID;
	Ret_nCustomSolutionID	= g_ModuleLicense.GetLicenseInfo().m_nCustomSolutionID;
}
