#include "service_webprojects.h"
#include "bus_core/bus_core/globalconstants.h"
#include "common/common/entity_id_collection.h"

#include "businessserviceset.h"
extern BusinessServiceSet* g_BusinessServiceSet;
#include "bus_core/bus_core/usersessionmanager.h"
extern UserSessionManager	*g_UserSessionManager;
#include "bus_core/bus_core/useraccessright.h"
extern UserAccessRight *g_AccessRight;	


void Service_WebProjects::ReadProjectLists(Status &Ret_pStatus, DbRecordSet &Ret_Lists, int &Ret_nStartListID)
{
	DbRecordSet data; //coz of MR stupidity->declared column as BLOB-<must be converted to string..
	g_BusinessServiceSet->StoredProjectLists->GetListNames(Ret_pStatus,data);

	Ret_Lists.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_WEBSERVICE_BUS_PROJECT_LIST));
	Ret_Lists.addRow(data.getRowCount());

	int nSize=data.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		int nID=data.getDataRef(i,"BSPL_ID").toInt();
		QByteArray name=data.getDataRef(i,"BSPL_NAME").toByteArray();
		Ret_Lists.setData(i,"BSPL_ID",nID);
		Ret_Lists.setData(i,"BSPL_NAME",QString(name));

	}

	//get default id for user:
	int nPersonID=g_UserSessionManager->GetPersonID();

	//get settings fav:
	DbRecordSet recSettings;
	g_BusinessServiceSet->BusPerson->GetPersonProjectListID(Ret_pStatus,nPersonID,Ret_nStartListID);
	if(!Ret_pStatus.IsOK()) return;

	Ret_Lists.renameColumnsFromAlias(DbSqlTableView::getView(DbSqlTableView::TVIEW_WEBSERVICE_BUS_PROJECT_LIST));
}


void Service_WebProjects::ReadProjectListData(Status &Ret_pStatus, int nListID,DbRecordSet &Ret_Data,QString strParentProjectCode, int nLevelsDeep)
{
	Ret_Data.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_WEBSERVICE_BUS_PROJECT_SELECT));
	DbRecordSet data;

	g_BusinessServiceSet->StoredProjectLists->GetListDataLevelByLevel(Ret_pStatus,nListID,data,strParentProjectCode,nLevelsDeep);


	Ret_Data.merge(data);
	Ret_Data.renameColumnsFromAlias(DbSqlTableView::getView(DbSqlTableView::TVIEW_WEBSERVICE_BUS_PROJECT_SELECT));

	//Ret_Data.Dump();

}


void Service_WebProjects::ReadCommEntityCount(Status &Ret_pStatus, int nProjectID, int &Ret_nFileDocCount, int &Ret_nWebSiteCount, int &Ret_nNotesCount,int &Ret_nMailCount)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_DM_DOCUMENTS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	DbSqlQuery *query=TableOrm.GetDbSqlQuery();

	QString strSQL=" INNER JOIN CE_COMM_ENTITY ON BDMD_COMM_ENTITY_ID = CENT_ID";
	strSQL+=QString(" INNER JOIN BUS_NMRX_RELATION ON CENT_ID = BNMR_TABLE_KEY_ID_1 AND ")+QString(NMRX_CE_PROJECT_WHERE);
	strSQL+=QString(" AND BNMR_TABLE_KEY_ID_2=")+QString::number(nProjectID);


	//doc file:
	QString strExec="SELECT COUNT(*) FROM BUS_DM_DOCUMENTS "+strSQL+" WHERE BDMD_DOC_TYPE =" +QString::number(GlobalConstants::DOC_TYPE_INTERNET_FILE);
	g_AccessRight->SQLFilterRecords(BUS_DM_DOCUMENTS,strExec);
	query->Execute(Ret_pStatus,strExec);
	if(!Ret_pStatus.IsOK()) return;
	if(query->next())
		Ret_nFileDocCount=query->value(0).toInt();
	else
		Ret_nFileDocCount=0;

	//web file:
	strExec="SELECT COUNT(*) FROM BUS_DM_DOCUMENTS "+strSQL+" WHERE BDMD_DOC_TYPE =" +QString::number(GlobalConstants::DOC_TYPE_URL);
	g_AccessRight->SQLFilterRecords(BUS_DM_DOCUMENTS,strExec);
	query->Execute(Ret_pStatus,strExec);
	if(!Ret_pStatus.IsOK()) return;
	if(query->next())
		Ret_nWebSiteCount=query->value(0).toInt();
	else
		Ret_nWebSiteCount=0;

	//note file:
	strExec="SELECT COUNT(*) FROM BUS_DM_DOCUMENTS "+strSQL+" WHERE BDMD_DOC_TYPE =" +QString::number(GlobalConstants::DOC_TYPE_NOTE);
	g_AccessRight->SQLFilterRecords(BUS_DM_DOCUMENTS,strExec);
	query->Execute(Ret_pStatus,strExec);
	if(!Ret_pStatus.IsOK()) return;
	if(query->next())
		Ret_nNotesCount=query->value(0).toInt();
	else
		Ret_nNotesCount=0;


	//emails:
	strExec="SELECT COUNT(*) FROM BUS_EMAIL WHERE BEM_PROJECT_ID = "+QString::number(nProjectID)+" AND BEM_TEMPLATE_FLAG=0";
	g_AccessRight->SQLFilterRecords(BUS_EMAIL,strExec);
	query->Execute(Ret_pStatus,strExec);
	if(!Ret_pStatus.IsOK()) return;
	if(query->next())
		Ret_nMailCount=query->value(0).toInt();
	else
		Ret_nMailCount=0;

}

void Service_WebProjects::ReadProjectDocuments(Status &Ret_pStatus, int &Ret_nTotalCount,DbRecordSet &Ret_Documents, int nProjectID,int nDocType, int nFromN, int nToN)
{
	g_BusinessServiceSet->WebDocuments->SearchDocuments(Ret_pStatus,Ret_nTotalCount,Ret_Documents,nDocType,nFromN,nToN,-1,nProjectID);
}
void Service_WebProjects::ReadProjectEmails(Status &Ret_pStatus, int &Ret_nTotalCount, DbRecordSet &Ret_Emails, int nProjectID, int nFromN, int nToN)
{
	g_BusinessServiceSet->WebEmails->SearchEmails(Ret_pStatus,Ret_nTotalCount,Ret_Emails,-1,nFromN,nToN,-1,nProjectID);
}




