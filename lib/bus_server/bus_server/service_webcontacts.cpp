#include "service_webcontacts.h"
#include "db_core/db_core/dbsqltableview.h"
#include "db/db/dbsimpleorm.h"
#include "bus_core/bus_core/mainentitycalculatedname.h"
#include "bus_core/bus_core/optionsandsettingsmanager.h"
#include "common/common/entity_id_collection.h"
#include "bus_server/bus_server/serverlocalcache.h"
#include "bus_core/bus_core/mainentityfilter.h"
#include "bus_core/bus_core/contacttypemanager.h"
#include "bus_core/bus_core/globalconstants.h"

#include "bus_core/bus_core/useraccessright.h"
extern UserAccessRight *g_AccessRight;	
#include "bus_core/bus_core/usersessionmanager.h"
extern UserSessionManager	*g_UserSessionManager;
#include "businessserviceset.h"
#include "common/common/datahelper.h"
extern BusinessServiceSet* g_BusinessServiceSet;


/*!
	Read short contacts
*/
void Service_WebContacts::ReadContacts(Status &Ret_pStatus,  DbRecordSet &Ret_Contacts)
{
	Ret_Contacts.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CONTACT_SELECTION));

	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_CM_CONTACT, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	QString strWhere;
	g_AccessRight->SQLFilterRecords(BUS_CM_CONTACT,strWhere);
	TableOrm.Read(Ret_pStatus, Ret_Contacts, strWhere, DbSqlTableView::TVIEW_BUS_CONTACT_SELECTION);
	if(!Ret_pStatus.IsOK()) return;

	//calculated names: 
	MainEntityCalculatedName filler;
	filler.Initialize(ENTITY_BUS_CONTACT,Ret_Contacts);
	filler.FillCalculatedNames(Ret_Contacts,Ret_Contacts.getColumnIdx("BCNT_NAME"));

	ContactTypeManager::SortContactList(Ret_Contacts);
	Ret_Contacts.renameColumnsFromAlias(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CONTACT_SELECTION));
};

/*!
	Read short contacts

	\param Ret_pStatus		- returns error (RETURNED)
	\param Ret_Contacts		- returned data in TVIEW_BUS_CONTACT_SELECTION
	\param Ret_nTotalCount		- count of Ret_Contacts
	\param nFilter			- -1 all default, 0 (only cont with org sorted by org, last, first), 1-by  name: all contacts 
	\param nFromN			- to retrieve only from pos N
	\param nToN				- if nFromN is defined, define nToN
	\param strFromLetter	- from letter to retrieve only part of the list (applied before from/to)
	\param strToLetter		- to letter

*/
void Service_WebContacts::ReadContactsWithFilter(Status &Ret_pStatus,  int &Ret_nTotalCount,DbRecordSet &Ret_Contacts,int nFilter,int nFromN, int nToN, QString strFromLetter, QString strToLetter)
{
	Ret_Contacts.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CONTACT_SELECTION));

	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_CM_CONTACT, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	QString strWhere;

	g_AccessRight->SQLFilterRecords(BUS_CM_CONTACT,strWhere);
	TableOrm.Read(Ret_pStatus, Ret_Contacts, strWhere, DbSqlTableView::TVIEW_BUS_CONTACT_SELECTION);
	if(!Ret_pStatus.IsOK()) return;

	//calculated names: 
	MainEntityCalculatedName filler;
	filler.Initialize(ENTITY_BUS_CONTACT,Ret_Contacts);
	filler.FillCalculatedNames(Ret_Contacts,Ret_Contacts.getColumnIdx("BCNT_NAME"));

	Ret_nTotalCount= Ret_Contacts.getRowCount();
	if (Ret_nTotalCount==0)
		return;

	if (nFilter==0)
	{
		//filter = 0: remove all without org_Name ->yellow pages
		Ret_Contacts.find("BCNT_ORGANIZATIONNAME",QString(""));
		Ret_Contacts.deleteSelectedRows();
		ContactTypeManager::SortContactList(Ret_Contacts);
		DataHelper::GetPartOfRecordSet(Ret_Contacts,"BCNT_ORGANIZATIONNAME",strFromLetter,strToLetter);
	}
	else if (nFilter==1) 
	{
		//filter = 1: sort persons by last and org by org in same list
		Ret_Contacts.addColumn(QVariant::String,"SORT");
		int nSize=Ret_Contacts.getRowCount();
		for (int i=0;i<nSize;i++)
		{
			if (Ret_Contacts.getDataRef(i,"BCNT_TYPE").toInt()==ContactTypeManager::CM_TYPE_PERSON)
				Ret_Contacts.setData(i,"SORT",Ret_Contacts.getDataRef(i,"BCNT_LASTNAME").toString()+Ret_Contacts.getDataRef(i,"BCNT_FIRSTNAME").toString());
			else
				Ret_Contacts.setData(i,"SORT",Ret_Contacts.getDataRef(i,"BCNT_ORGANIZATIONNAME").toString());
		}
		Ret_Contacts.sort("SORT");
		DataHelper::GetPartOfRecordSet(Ret_Contacts,"SORT",strFromLetter,strToLetter);
		Ret_Contacts.removeColumn(Ret_Contacts.getColumnIdx("SORT"));
	}
	else //normal
	{
		ContactTypeManager::SortContactList(Ret_Contacts);
		DataHelper::GetPartOfRecordSet(Ret_Contacts,"BCNT_ORGANIZATIONNAME",strFromLetter,strToLetter);
	}

	DataHelper::GetPartOfRecordSet(Ret_Contacts,nFromN,nToN);
	Ret_Contacts.renameColumnsFromAlias(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CONTACT_SELECTION));
};


//nSearch=0 by fields, 1- by group
void Service_WebContacts::SearchContacts(Status &Ret_pStatus, int &Ret_nTotalCount, DbRecordSet &Ret_Contacts,int nSearchType, QString strField1_Name, QString strField1_Value, QString strField2_Name, QString strField2_Value, int nGroupID,int nFromN, int nToN, QString strFromLetter, QString strToLetter)
{
	QString strSQLWhere;
	if (nSearchType==0)
	{
		QString strSQLWhereJoin;
		ContactTypeManager::AssembleSQLQueryForSearch(strSQLWhereJoin,strSQLWhere,strField1_Name,-1,strField1_Value, QString(), strField2_Name,-1,strField2_Value, QString(), true);
		if (!strSQLWhere.isEmpty())
			strSQLWhere=strSQLWhereJoin+" WHERE "+strSQLWhere;
	}
	else
	{
		strSQLWhere=" INNER JOIN bus_cm_group ON BGCN_CONTACT_ID = BCNT_ID WHERE BGCN_ITEM_ID="+QString::number(nGroupID);
	}

	MainEntityFilter filter;
	filter.SetFilter(MainEntityFilter::FILTER_SQL_WHERE,strSQLWhere);
	g_BusinessServiceSet->BusContact->ReadShortContactList(Ret_pStatus,filter.Serialize(),Ret_Contacts);

	Ret_nTotalCount=Ret_Contacts.getRowCount();

	ContactTypeManager::SortContactList(Ret_Contacts);
	DataHelper::GetPartOfRecordSet(Ret_Contacts,"BCNT_ORGANIZATIONNAME",strFromLetter,strToLetter);
	DataHelper::GetPartOfRecordSet(Ret_Contacts,nFromN,nToN);
	Ret_Contacts.renameColumnsFromAlias(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CONTACT_SELECTION));
};


/*!
	Read favs based on logged user settings

	\param Ret_pStatus		- returns error (RETURNED)
	\param Ret_Data			- returned data in TVIEW_BUS_CONTACT_SELECTION_WITH_PICS
*/

void Service_WebContacts::ReadFavorites(Status &Ret_pStatus, DbRecordSet &Ret_Favorites)
{
	Ret_Favorites.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CONTACT_SELECTION_WITH_PICS));

	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_CM_CONTACT, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	//get settings fav:
	int nPersonID=g_UserSessionManager->GetPersonID();
	DbRecordSet recSettings;
	g_BusinessServiceSet->CoreServices->GetPersonalSettingsByPersonID(Ret_pStatus, nPersonID, recSettings);
	if(!Ret_pStatus.IsOK()) return;

	OptionsAndSettingsManager settings;
	settings.SetSettingsDataSource(&recSettings,nPersonID);
	QString strFavs = settings.GetPersonSetting(CONTACT_FAVORITES).toString(); //, comma delimited
	QStringList lstFavsToRead = strFavs.split(",",QString::SkipEmptyParts);
	if (lstFavsToRead.size()==0)
		return;

	QString strWhere =" WHERE BCNT_ID IN ("+lstFavsToRead.join(",")+")";
	g_AccessRight->SQLFilterRecords(BUS_CM_CONTACT,strWhere);
	TableOrm.Read(Ret_pStatus, Ret_Favorites, strWhere, DbSqlTableView::TVIEW_BUS_CONTACT_SELECTION_WITH_PICS);
	if(!Ret_pStatus.IsOK()) return;

	ContactTypeManager::SortContactList(Ret_Favorites);

	//calculated names: 
	MainEntityCalculatedName filler;
	filler.Initialize(ENTITY_BUS_CONTACT,Ret_Favorites);
	filler.FillCalculatedNames(Ret_Favorites,Ret_Favorites.getColumnIdx("BCNT_NAME"));
	Ret_Favorites.renameColumnsFromAlias(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CONTACT_SELECTION_WITH_PICS));

};

void Service_WebContacts::ReadContactDetails(Status &Ret_pStatus, int nContactID, DbRecordSet &Ret_ContactData,DbRecordSet &Ret_Emails,DbRecordSet &Ret_WebSites,DbRecordSet &Ret_Phones,DbRecordSet &Ret_Sms,DbRecordSet &Ret_Sykpe,DbRecordSet &Ret_Addresses)
{
	//define main record:
	Ret_ContactData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CONTACT_SELECTION_WITH_PICS));

	//read all sub data:
	ReadContactAddresses(Ret_pStatus,nContactID,Ret_Emails,Ret_WebSites,Ret_Phones,Ret_Sms,Ret_Sykpe,Ret_Addresses);
	if(!Ret_pStatus.IsOK()) return;

	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_CM_CONTACT, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	QString strWhere =" WHERE BCNT_ID = "+QString::number(nContactID);
	g_AccessRight->SQLFilterRecords(BUS_CM_CONTACT,strWhere);
	TableOrm.Read(Ret_pStatus, Ret_ContactData, strWhere, DbSqlTableView::TVIEW_BUS_CONTACT_SELECTION_WITH_PICS);
	if(!Ret_pStatus.IsOK()) return;

	//calculated names: 
	MainEntityCalculatedName filler;
	filler.Initialize(ENTITY_BUS_CONTACT,Ret_ContactData);
	filler.FillCalculatedNames(Ret_ContactData,Ret_ContactData.getColumnIdx("BCNT_NAME"));
	Ret_ContactData.renameColumnsFromAlias(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CONTACT_SELECTION_WITH_PICS));

}

//read all subdata for one contact: //contact/xxx/
void Service_WebContacts::ReadContactAddresses(Status &Ret_pStatus, int nContactID, DbRecordSet &Ret_Emails,DbRecordSet &Ret_WebSites,DbRecordSet &Ret_Phones,DbRecordSet &Ret_Sms,DbRecordSet &Ret_Sykpe,DbRecordSet &Ret_Addresses)
{
	Ret_Emails.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_WEBSERVICE_BUS_CM_EMAIL_SELECT));
	Ret_WebSites.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_WEBSERVICE_BUS_CM_INTERNET_SELECT));
	Ret_Phones.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_WEBSERVICE_BUS_CM_PHONE_SELECT));
	Ret_Sykpe.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_WEBSERVICE_BUS_CM_SKYPE_SELECT));
	Ret_Sms.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_WEBSERVICE_BUS_CM_PHONE_SELECT));
	Ret_Addresses.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_WEBSERVICE_BUS_CM_ADDRESS_SELECT));

	//read all:
	QString strSQLWhere =" WHERE BCNT_ID="+QString::number(nContactID);
	MainEntityFilter filter;
	filter.SetFilter(MainEntityFilter::FILTER_SQL_WHERE,strSQLWhere);
	DbRecordSet dataContact;
	g_BusinessServiceSet->BusContact->ReadData(Ret_pStatus,filter.Serialize(),dataContact);

	if (!Ret_pStatus.IsOK() || dataContact.getRowCount()!=1)
		return;

	//extract and prepare list if any:
	Ret_Emails.merge(dataContact.getDataRef(0,"LST_EMAIL").value<DbRecordSet>());
	Ret_WebSites.merge(dataContact.getDataRef(0,"LST_INTERNET").value<DbRecordSet>());

	Ret_Emails.sort("BCME_IS_DEFAULT",1);
	Ret_WebSites.sort("BCMI_IS_DEFAULT",1);

	//phone:
	DbRecordSet lstPhones=dataContact.getDataRef(0,"LST_PHONE").value<DbRecordSet>();

		
	//sms (all mobile phones):
	ServerLocalCache cache;
	cache.LoadTypes();
	int nPhoneMobiID = cache.GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_MOBILE).toInt();
	int nPhonePrivMobID = cache.GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_PHONE_PRIVATE_MOBILE).toInt();
	int nSkypeID = cache.GetUserTypeBasedOnSystemType(ContactTypeManager::TYPE_PHONE,ContactTypeManager::SYSTEM_SKYPE).toInt();
	
	//first get skypes:
	//lstPhones.Dump();
	lstPhones.find("BCMP_TYPE_ID",nSkypeID);
	Ret_Sykpe.merge(lstPhones,true);
	//Ret_Sykpe.Dump();

	lstPhones.deleteSelectedRows();
	Ret_Phones.merge(lstPhones);
	//Ret_Phones.Dump();

	lstPhones.clearSelection();
	lstPhones.find("BCMP_TYPE_ID",nPhoneMobiID);
	lstPhones.find("BCMP_TYPE_ID",nPhonePrivMobID,false,false,false,true);
	Ret_Sms.merge(lstPhones,true);

	Ret_Phones.sort("BCMP_IS_DEFAULT",1);
	Ret_Sykpe.sort("BCMP_IS_DEFAULT",1);
	Ret_Sms.sort("BCMP_IS_DEFAULT",1);


	//addresses
	DbRecordSet lstAddress=dataContact.getDataRef(0,"LST_ADDRESS").value<DbRecordSet>();
	Ret_Addresses.merge(lstAddress);

	//compile field of all address types: 
	int nSize=lstAddress.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		DbRecordSet lstTypes=lstAddress.getDataRef(i,"LST_TYPES").value<DbRecordSet>();
		int nSize2=lstTypes.getRowCount();
		QString strTypes;
		for (int k=0;k<nSize2;k++)
		{
			strTypes+=lstTypes.getDataRef(k,"BCMT_TYPE_NAME").toString()+",";
		}
		if (!strTypes.isEmpty())
			strTypes.chop(1);
		Ret_Addresses.setData(i,"ADDITIONAL_ADDRESS_TYPE_NAMES",strTypes);
	}

	Ret_Addresses.sort("BCMA_IS_DEFAULT",1);


	Ret_Emails.renameColumnsFromAlias(DbSqlTableView::getView(DbSqlTableView::TVIEW_WEBSERVICE_BUS_CM_EMAIL_SELECT));
	Ret_WebSites.renameColumnsFromAlias(DbSqlTableView::getView(DbSqlTableView::TVIEW_WEBSERVICE_BUS_CM_INTERNET_SELECT));
	Ret_Phones.renameColumnsFromAlias(DbSqlTableView::getView(DbSqlTableView::TVIEW_WEBSERVICE_BUS_CM_PHONE_SELECT));
	Ret_Sykpe.renameColumnsFromAlias(DbSqlTableView::getView(DbSqlTableView::TVIEW_WEBSERVICE_BUS_CM_SKYPE_SELECT));
	Ret_Sms.renameColumnsFromAlias(DbSqlTableView::getView(DbSqlTableView::TVIEW_WEBSERVICE_BUS_CM_PHONE_SELECT));
	Ret_Addresses.renameColumnsFromAlias(DbSqlTableView::getView(DbSqlTableView::TVIEW_WEBSERVICE_BUS_CM_ADDRESS_SELECT));

	//Ret_Phones.Dump();

};



void Service_WebContacts::ReadContactDocuments(Status &Ret_pStatus, int &Ret_nTotalCount,DbRecordSet &Ret_Documents, int nContactID,int nDocType, int nFromN, int nToN)
{
	g_BusinessServiceSet->WebDocuments->SearchDocuments(Ret_pStatus,Ret_nTotalCount,Ret_Documents,nDocType,nFromN,nToN,nContactID);
}
void Service_WebContacts::ReadContactEmails(Status &Ret_pStatus, int &Ret_nTotalCount, DbRecordSet &Ret_Emails, int nContactID, int nFromN, int nToN)
{
	g_BusinessServiceSet->WebEmails->SearchEmails(Ret_pStatus,Ret_nTotalCount,Ret_Emails,-1,nFromN,nToN,nContactID);
}


void Service_WebContacts::ReadCommEntityCount(Status &Ret_pStatus, int nContactID, int &Ret_nFileDocCount, int &Ret_nWebSiteCount, int &Ret_nNotesCount,int &Ret_nEmailCount)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_DM_DOCUMENTS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	DbSqlQuery *query=TableOrm.GetDbSqlQuery();

	QString strSQL=" INNER JOIN CE_COMM_ENTITY ON BDMD_COMM_ENTITY_ID = CENT_ID";
	strSQL+=QString(" INNER JOIN BUS_NMRX_RELATION K2 ON CENT_ID = K2.BNMR_TABLE_KEY_ID_1 AND ")+QString(NMRX_CE_CONTACT_WHERE_AS_K2);
	strSQL+=QString(" AND K2.BNMR_TABLE_KEY_ID_2=")+QString::number(nContactID);


	//doc file:
	QString strExec="SELECT COUNT(*) FROM BUS_DM_DOCUMENTS "+strSQL+" WHERE BDMD_DOC_TYPE =" +QString::number(GlobalConstants::DOC_TYPE_INTERNET_FILE);
	g_AccessRight->SQLFilterRecords(BUS_DM_DOCUMENTS,strExec);
	query->Execute(Ret_pStatus,strExec);
	if(!Ret_pStatus.IsOK()) return;
	if(query->next())
		Ret_nFileDocCount=query->value(0).toInt();
	else
		Ret_nFileDocCount=0;

	//web file:
	strExec="SELECT COUNT(*) FROM BUS_DM_DOCUMENTS "+strSQL+" WHERE BDMD_DOC_TYPE =" +QString::number(GlobalConstants::DOC_TYPE_URL);
	g_AccessRight->SQLFilterRecords(BUS_DM_DOCUMENTS,strExec);
	query->Execute(Ret_pStatus,strExec);
	if(!Ret_pStatus.IsOK()) return;
	if(query->next())
		Ret_nWebSiteCount=query->value(0).toInt();
	else
		Ret_nWebSiteCount=0;

	//note file:
	strExec="SELECT COUNT(*) FROM BUS_DM_DOCUMENTS "+strSQL+" WHERE BDMD_DOC_TYPE =" +QString::number(GlobalConstants::DOC_TYPE_NOTE);
	g_AccessRight->SQLFilterRecords(BUS_DM_DOCUMENTS,strExec);
	query->Execute(Ret_pStatus,strExec);
	if(!Ret_pStatus.IsOK()) return;
	if(query->next())
		Ret_nNotesCount=query->value(0).toInt();
	else
		Ret_nNotesCount=0;


	//emails:
	strSQL=" INNER JOIN CE_COMM_ENTITY ON BEM_COMM_ENTITY_ID = CENT_ID";
	strSQL+=QString(" INNER JOIN BUS_NMRX_RELATION K2 ON CENT_ID = K2.BNMR_TABLE_KEY_ID_1 AND ")+QString(NMRX_CE_CONTACT_WHERE_AS_K2);
	strSQL+=QString(" AND K2.BNMR_TABLE_KEY_ID_2=")+QString::number(nContactID);

	strExec="SELECT COUNT(*) FROM BUS_EMAIL "+strSQL+" WHERE (BEM_TEMPLATE_FLAG=0 OR BEM_TEMPLATE_FLAG IS NULL)";
	g_AccessRight->SQLFilterRecords(BUS_EMAIL,strExec);
	query->Execute(Ret_pStatus,strExec);
	if(!Ret_pStatus.IsOK()) return;
	if(query->next())
		Ret_nEmailCount=query->value(0).toInt();
	else
		Ret_nEmailCount=0;




}

