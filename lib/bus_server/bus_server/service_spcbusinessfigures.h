#ifndef SERVICE_SPCBUSINESSFIGURES_H
#define SERVICE_SPCBUSINESSFIGURES_H

#include "bus_interface/bus_interface/interface_spcbusinessfigures.h"
#include "businessservice.h"

class Service_SpcBusinessFigures : public Interface_SpcBusinessFigures, public BusinessService
{
public:
	void Routine_275(Status& Ret_Status, DbRecordSet &Ret_lstData, QString strPersonNumber, QDate datFrom, QDate datTo, QString strProjCode="", int nIncl_Prov_Pr=0, int nIncl_Prov_Tsk=0);
	void Routine_156(Status& Ret_Status, DbRecordSet &Ret_lstData, QString strProjCode, double dT_AUFW_NK_NF, QDate datTo=QDate(), int nABT_EQUAL=0,  int nNO_SUBPROJ=0, int nWERT_INKL_NFAKT=0, QString sABT="", int nNO_PROJSUM=-1);

private:
	void LoadFL_Default(Status& Ret_Status, DbRecordSet &recFLDefault);
	void InitVars_141(DbRecordSet &Ret_lstData, int nPer1, int nPer2, double dT_AUFW_NK_NF);
		
	void Routine_145(DbRecordSet &Ret_lstData, const DbRecordSet &lstProjects, const DbRecordSet &recFLDefault, int nRowIdx, bool bIsMainParentProject, int nT_KUMART, int nT_KUM_STUFE, QString strT_KUM_PA);
	void Routine_146(DbRecordSet &Ret_lstData, const DbRecordSet &lstProjects, const DbRecordSet &recFLDefault, int nRowIdx);
	void Routine_152(Status& Ret_Status,DbRecordSet &Ret_lstData, const DbRecordSet &lstProjects, const DbRecordSet &recFLDefault, int nRowIdx, int nNO_PROJSUM, int nT_DIAG_KUMULIERT);
	
	void	Routine_147(Status& Ret_Status, DbRecordSet &Ret_lstData, const DbRecordSet &lstProjects, const DbRecordSet &recFLDefault, int nRowIdx, int nT_DIAG_KUMULIERT, QString strProjCode, QDate datTo, int nABT_EQUAL=0, QString strABT="" );
	double	Routine_148(Status& Ret_Status, const DbRecordSet &lstProjects, const DbRecordSet &recFLDefault, int nRowIdx, QString strProjCode, QDate datDate, int nWERT_INKL_NFAKT=0);
	void	Routine_153(Status& Ret_Status, const DbRecordSet &recFLDefault, QString strProjCode, QDate datDate1, QDate datDate2, int nBUDGET_GB_HS, int nINKL_NOT_FAKT, double &nHONORAR, double &nNEBENKOSTEN);
	QDate	Routine_154(Status& Ret_Status, QString strProjCode, QDate datDate);

	void Routine_103_SP(Status& Ret_Status, DbRecordSet &Ret_lstData, const DbRecordSet &lstProjects, const DbRecordSet &recFLDefault, int nRowIdx, QString strProjCode, QDate datFrom, QDate datTo, int nINCLUDE_SP=0, int nABT_EQUAL=0, QString strABT="");
	void Routine_104_SP(Status& Ret_Status, DbRecordSet &Ret_lstData, const DbRecordSet &lstProjects, const DbRecordSet &recFLDefault, int nRowIdx, QString strProjCode, QDate datFrom, QDate datTo, int nINCLUDE_SP=0, int nABT_EQUAL=0, QString strABT="");
	void Routine_105_SP(Status& Ret_Status, DbRecordSet &Ret_lstData, const DbRecordSet &lstProjects, const DbRecordSet &recFLDefault, int nRowIdx, QString strProjCode, QDate datFrom, QDate datTo, int nP_UZ_ONLY=0, int nP_UZ_STUFE=0, int nP_ABT_FREMD_ONLY=0, int nPINIT=0, int nINCLUDE_SP=0, int nABT_EQUAL=0, QString strABT="");
	void Routine_113_SP(Status& Ret_Status, DbRecordSet &Ret_lstData, const DbRecordSet &lstProjects, const DbRecordSet &recFLDefault, int nRowIdx, QString strProjCode, QDate datFrom, QDate datTo, int nINCLUDE_SP=0, int nABT_EQUAL=0, QString strABT="" );

};

#endif // SERVICE_SPCSESSION_H

