#include "service_webemails.h"
#include "bus_core/bus_core/globalconstants.h"
#include "common/common/datahelper.h"
#include "bus_core/bus_core/mainentitycalculatedname.h"
#include "common/common/entity_id_collection.h"
#include "common/common/zipcompression.h"
#include "common/common/userstoragehelper.h"
#include "common/common/logger.h"
#include "common/common/threadid.h"
#include "trans/trans/xmlutil.h"
#include "bus_core/bus_core/optionsandsettingsid.h"

#include "bus_core/bus_core/usersessionmanager.h"
extern UserSessionManager	*g_UserSessionManager;
#include "bus_core/bus_core/useraccessright.h"
extern UserAccessRight *g_AccessRight;	
#include "businessserviceset.h"
extern BusinessServiceSet* g_BusinessServiceSet;


//read all emails for logged contact
void Service_WebEmails::ReadEmails(Status &Ret_pStatus, int &Ret_nTotalCount, DbRecordSet &Ret_Emails, int nFromN, int nToN)
{
	int nContactID=g_UserSessionManager->GetContactID();
	SearchEmails(Ret_pStatus,Ret_nTotalCount,Ret_Emails,-1,nFromN,nToN,nContactID);
}


//read email detail for logged contact
void Service_WebEmails::ReadEmailDetails(Status &Ret_pStatus, int nEmailID, DbRecordSet &Ret_Email, DbRecordSet &Ret_Attachments)
{
	Ret_Email.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_WEBSERVICE_BUS_EMAIL_SELECT));

	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_EMAIL, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	QString strSQL=" INNER JOIN CE_COMM_ENTITY ON BEM_COMM_ENTITY_ID = CENT_ID";
	QString strWhere =" WHERE BEM_ID="+QString::number(nEmailID);

	strSQL=DbSqlTableView::getSQLView(DbSqlTableView::TVIEW_WEBSERVICE_BUS_EMAIL_SELECT)+strSQL+strWhere;

	DbSqlQuery *query=TableOrm.GetDbSqlQuery();
	query->Prepare(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK()) return;

	query->ExecutePrepared(Ret_pStatus);
	if(!Ret_pStatus.IsOK()) return;

	DbRecordSet lstData;
	query->FetchData(lstData);
	Ret_Email.merge(lstData);

	//B.T. :reset read flag upon reading email details: MB on skype 09.05.2014.
	query->Execute(Ret_pStatus,"UPDATE BUS_EMAIL SET BEM_UNREAD_FLAG=0 WHERE BEM_ID="+QString::number(nEmailID));
	if(!Ret_pStatus.IsOK()) return;

	//get attachments
	GetEmailAttachments(Ret_pStatus,nEmailID,Ret_Attachments);
	if(!Ret_pStatus.IsOK()) return;

	//get contact from/to
	GetFromToContacts(Ret_pStatus,Ret_Email);
	if(!Ret_pStatus.IsOK()) return;

	Ret_Email.renameColumnsFromAlias(DbSqlTableView::getView(DbSqlTableView::TVIEW_WEBSERVICE_BUS_EMAIL_SELECT));
	Ret_Attachments.renameColumnsFromAlias(DbSqlTableView::getView(DbSqlTableView::TVIEW_WEBSERVICE_BUS_EMAIL_ATTACHMENT_LIST));

}


//nSortOrder: default by last_modif desc, if 1 then by subject
//nFilter: default all non templates, if 1 then only templates
void Service_WebEmails::SearchEmails(Status &Ret_pStatus, int &Ret_nTotalCount,DbRecordSet &Ret_Emails,  int nSortOrder, int nFromN, int nToN, int nContactID, int nProjectID, int nOwnerUserID,  QDate datFrom, QDate datTo, QString strEmailAcc, QString strEmailSender, int nMailBox)
{

	//MB: asked, when refresh inbox then reset last sync flag:
	if (nOwnerUserID>0)
		ResetLastRefreshDate(nOwnerUserID);

	Ret_Emails.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_WEBSERVICE_BUS_EMAIL_SELECT));

	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_EMAIL, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	QString strSQL=" INNER JOIN CE_COMM_ENTITY ON BEM_COMM_ENTITY_ID = CENT_ID";
	//strSQL+=" INNER JOIN BUS_PERSON ON BPER_ID = CENT_OWNER_ID";
	//strSQL+=" LEFT OUTER JOIN CE_EVENT_TYPE ON CEVT_ID = CENT_CE_TYPE_ID";
	//-----------------------------WHERE----------------------------

	QString strWhere;
	int nBindCounter=-1;
	int nBindBy_From_Idx=-1;
	int nBindBy_To_Idx=-1;


	if (nContactID>0)
	{
		strSQL+=QString(" INNER JOIN BUS_NMRX_RELATION K2 ON CENT_ID = K2.BNMR_TABLE_KEY_ID_1 AND ")+QString(NMRX_CE_CONTACT_WHERE_AS_K2);
		strWhere+=QString(" AND K2.BNMR_TABLE_KEY_ID_2=")+QString::number(nContactID);
	}
	else if (nProjectID>0) //nContact and project are mutually exclusive..
	{
		strWhere+=QString(" AND BEM_PROJECT_ID=")+QString::number(nProjectID);
	}
	else if (nOwnerUserID>0)
	{
		strSQL+=QString(" AND CENT_OWNER_ID=")+QString::number(nOwnerUserID);
	}

	if (nMailBox>0)
	{
		strSQL+=QString(" AND BEM_MAILBOX_TYPE=")+QString::number(nMailBox);
	}
	
	//
	if (!strEmailAcc.isEmpty())
	{
		strWhere+=QString(" AND BEM_TO_EMAIL_ACCOUNT='")+strEmailAcc+"'";
	}

	if (!strEmailSender.isEmpty())
	{
		//strWhere+=" AND BEM_FROM LIKE '%"+strEmailSender+"%'";
		strWhere+=" AND (BEM_FROM LIKE '%"+strEmailSender+"%' OR (BEM_OUTGOING=1 AND BEM_TO LIKE '%"+strEmailSender+"%'))";
	}

	
	if (datFrom.isValid())
	{
		nBindCounter++;
		nBindBy_From_Idx=nBindCounter;
		strWhere+=" AND BEM_RECV_TIME >=?";
	}
	if (datTo.isValid())
	{
		nBindCounter++;
		nBindBy_To_Idx=nBindCounter;
		strWhere+=" AND BEM_RECV_TIME =<?";
	}

	//B.T. : skip crawling using template_flag:
	/*
	if (strWhere.isEmpty())
		strWhere=" WHERE (BEM_TEMPLATE_FLAG=0 OR BEM_TEMPLATE_FLAG IS NULL)"; //remove AND at start
	else
		strWhere=" WHERE "+strWhere.mid(4)+" AND (BEM_TEMPLATE_FLAG=0 OR BEM_TEMPLATE_FLAG IS NULL)"; //remove AND at start
	*/

	if (!strWhere.isEmpty())
		strWhere=" WHERE "+strWhere.mid(4); //remove AND at start

	QString strSQL_Cnt="SELECT COUNT(*) FROM BUS_EMAIL "+strSQL+strWhere; //for total cnt prepare another statement with same where statement
	g_AccessRight->SQLFilterRecords(BUS_EMAIL,strSQL_Cnt);

	strSQL=DbSqlTableView::getSQLView(DbSqlTableView::TVIEW_WEBSERVICE_BUS_EMAIL_SELECT,true)+strSQL+strWhere;
	GetDbManager()->GetFunction()->PrepareLimit(strSQL,nFromN,nToN);

	//g_AccessRight->SQLFilterRecords(BUS_EMAIL,strSQL);

	//-----------------------------bind if needed: byname, bydate----------------------------
	//g_AccessRight->SQLFilterRecords(BUS_EMAIL,strSQL);
	if (nSortOrder==1)
		strSQL+=" ORDER BY BEM_SUBJECT ASC";
	else
		strSQL+=" ORDER BY BEM_RECV_TIME DESC";

	DbSqlQuery *query=TableOrm.GetDbSqlQuery();

	query->Prepare(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK()) return;

	if (datFrom.isValid())
	{
		query->bindValue(nBindBy_From_Idx,QDateTime(datFrom));
	}
	if (datTo.isValid())
	{
		query->bindValue(nBindBy_To_Idx,QDateTime(datTo));
	}

	QTime timer_api;
	timer_api.start();
	
	query->ExecutePrepared(Ret_pStatus);
	if(!Ret_pStatus.IsOK()) return;

	DbRecordSet lstData;
	query->FetchData(lstData);
	Ret_Emails.merge(lstData);
	/*
	int nElapsed=timer_api.elapsed();
	if (nElapsed>1000)
	{
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_SPEED_TEST_LEVEL,1,QString("ReadEmails, size: %1, time: %2, thread: %3").arg(Ret_Emails.getRowCount()).arg(nElapsed).arg(ThreadIdentificator::GetCurrentThreadID()));
	}
	*/
	//Ret_Emails.removeDuplicates(Ret_Emails.getColumnIdx("BEM_ID")); //remove duplicate entries

	query->Execute(Ret_pStatus,strSQL_Cnt);
	if(!Ret_pStatus.IsOK()) return;
	if(query->next())
		Ret_nTotalCount=query->value(0).toInt();
	else
		Ret_nTotalCount=Ret_Emails.getRowCount();



	
	//Ret_Emails.Dump();


	//----------------FROM -- TO test ---------------------
	//DataHelper::GetPartOfRecordSet(Ret_Emails,nFromN,nToN);
	
	//get attachment count
	GetEmailAttachmentCount(Ret_pStatus,Ret_Emails);

	//get contact from/to
	GetFromToContacts(Ret_pStatus,Ret_Emails);

	Ret_Emails.renameColumnsFromAlias(DbSqlTableView::getView(DbSqlTableView::TVIEW_WEBSERVICE_BUS_EMAIL_SELECT));
}


void Service_WebEmails::GetEmailAttachmentCount(Status &Ret_pStatus, DbRecordSet &lstEmails)
{
	QString strColIDName="BEM_ID";
	int nCount=0;
	QString strWhere,strWhereIn;
	DbRecordSet lstChunkRead;
	DbRecordSet lstRead;

	//execute
	DbSqlQuery query(Ret_pStatus, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	while (nCount>=0)
	{
		//compile FK in (n1,n2,...) statement
		nCount=DbSqlTableDefinition::GenerateChunkedWhereStatement(lstEmails,strColIDName,strWhereIn,nCount);
		if (nCount<0) break;
		strWhere="SELECT BEA_EMAIL_ID, COUNT(*) CNT FROM BUS_EMAIL_ATTACHMENT WHERE BEA_EMAIL_ID IN "+strWhereIn+" AND ((BEA_CID_LINK IS NULL) OR (BEA_CID_LINK = '')) GROUP BY BEA_EMAIL_ID";

		query.Execute(Ret_pStatus,strWhere);
		if(!Ret_pStatus.IsOK()) return;
		query.FetchData(lstChunkRead);

		//issue 1342, read list always defined
		if(lstRead.getColumnCount()==0) //if not defined, define
			lstRead.copyDefinition(lstChunkRead);

		lstRead.merge(lstChunkRead);    //merge
	}


	int nRowCount = lstEmails.getRowCount();
	for (int i = 0; i < nRowCount; i++)
	{
		int nRow=lstRead.find("BEA_EMAIL_ID",lstEmails.getDataRef(i,"BEM_ID").toInt(),true);
		if (nRow>=0)
			lstEmails.setData(i,"NUMBER_OF_ATTACHMENTS",lstRead.getDataRef(nRow,"CNT").toInt());
		else
			lstEmails.setData(i,"NUMBER_OF_ATTACHMENTS",0);
	}
}


void Service_WebEmails::GetEmailAttachments(Status &Ret_pStatus, int nEmailID,DbRecordSet &Ret_Attachments)
{
	Ret_Attachments.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_WEBSERVICE_BUS_EMAIL_ATTACHMENT_LIST));

	//execute
	DbSqlQuery query(Ret_pStatus, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	
	QString strSQL="SELECT BEA_ID, BEA_NAME FROM BUS_EMAIL_ATTACHMENT WHERE BEA_EMAIL_ID = "+QString::number(nEmailID)+" AND ((BEA_CID_LINK IS NULL) OR (BEA_CID_LINK = ''))";
	query.Execute(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK()) return;
	DbRecordSet lstData;
	query.FetchData(Ret_Attachments);
	Ret_Attachments.merge(lstData);

	int nRowCount = Ret_Attachments.getRowCount();
	for (int i = 0; i < nRowCount; i++)
	{
		QFileInfo info(Ret_Attachments.getDataRef(i,"BEA_NAME").toString());
		if (info.completeSuffix().toLower()=="zip")
			Ret_Attachments.setData(i,"IS_ZIP",1);
		else
			Ret_Attachments.setData(i,"IS_ZIP",0);

		Ret_Attachments.setData(i,"EXT",info.completeSuffix().toLower().trimmed());
	}

}

//save attachment onto disk in separe
void Service_WebEmails::GetAttachmentURL(Status &Ret_pStatus, int nAttachmentID, QString &Ret_strDocumentURL)
{
	DbRecordSet Ret_AttachmentFiles;
	SaveAttachment(Ret_pStatus,nAttachmentID,Ret_AttachmentFiles,false);

	if (Ret_AttachmentFiles.getRowCount()>0)
	{
		Ret_strDocumentURL=Ret_AttachmentFiles.getDataRef(0,"URL").toString();
	}
}


//unzip attachment, return urls as they already exists
void Service_WebEmails::GetAttachmentUnzippedFiles(Status &Ret_pStatus, int nAttachmentID, DbRecordSet &Ret_AttachmentFiles)
{
	SaveAttachment(Ret_pStatus,nAttachmentID,Ret_AttachmentFiles,true);
	Ret_AttachmentFiles.removeColumn(Ret_AttachmentFiles.getColumnIdx("LOCAL_SERVER_PATH"));
	Ret_AttachmentFiles.renameColumnsFromAlias(DbSqlTableView::getView(DbSqlTableView::TVIEW_WEBSERVICE_BUS_EMAIL_ATTACHMENT_LIST));
}


void Service_WebEmails::GetFromToContacts(Status &Ret_pStatus, DbRecordSet &lstEmails)
{
	//for each email id select contact
	QString strSQL="SELECT K2.BNMR_SYSTEM_ROLE,K2.BNMR_TABLE_KEY_ID_2,BCNT_LASTNAME,BCNT_FIRSTNAME,BCNT_ORGANIZATIONNAME FROM BUS_CM_CONTACT ";
	strSQL+=QString(" INNER JOIN BUS_NMRX_RELATION K2 ON BCNT_ID = K2.BNMR_TABLE_KEY_ID_2 AND ")+QString(NMRX_CE_CONTACT_WHERE_AS_K2);
	strSQL+=QString(" WHERE K2.BNMR_TABLE_KEY_ID_1 =? AND (K2.BNMR_SYSTEM_ROLE=")+QString::number(GlobalConstants::CONTACT_LINK_ROLE_FROM) +" OR K2.BNMR_SYSTEM_ROLE="+QString::number(GlobalConstants::CONTACT_LINK_ROLE_TO)+")";

	//calculated names: 
	MainEntityCalculatedName filler;

	DbSqlQuery query(Ret_pStatus, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	query.Prepare(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK()) return;

	int nRowCount = lstEmails.getRowCount();
	for (int i = 0; i < nRowCount; i++)
	{
		int nCentID=lstEmails.getDataRef(i,"CENT_ID").toInt();
		query.bindValue(0,nCentID);
		query.ExecutePrepared(Ret_pStatus);
		if(!Ret_pStatus.IsOK()) return;

		DbRecordSet lstData;
		query.FetchData(lstData);
		lstData.addColumn(QVariant::String,"BCNT_NAME");

		//lstData.Dump();

		filler.Initialize(ENTITY_BUS_CONTACT,lstData);
		filler.FillCalculatedNames(lstData,lstData.getColumnIdx("BCNT_NAME"));

		//lstData.Dump();

		int nRow = lstData.find("BNMR_SYSTEM_ROLE",QString::number(GlobalConstants::CONTACT_LINK_ROLE_FROM),true);
		if (nRow>=0)
		{
			lstEmails.setData(i,"CONTACT_FROM_NAME",lstData.getDataRef(nRow,"BCNT_NAME"));
			lstEmails.setData(i,"CONTACT_FROM_ID",lstData.getDataRef(nRow,"BNMR_TABLE_KEY_ID_2"));
		}
		nRow = lstData.find("BNMR_SYSTEM_ROLE",QString::number(GlobalConstants::CONTACT_LINK_ROLE_TO),true);
		if (nRow>=0)
		{
			lstEmails.setData(i,"CONTACT_TO_NAME",lstData.getDataRef(nRow,"BCNT_NAME"));
			lstEmails.setData(i,"CONTACT_TO_ID",lstData.getDataRef(nRow,"BNMR_TABLE_KEY_ID_2"));
		}
	}



}



void Service_WebEmails::SaveAttachment(Status &Ret_pStatus, int nAttachmentID, DbRecordSet &RetOut_UnZippedPaths, bool bUnzip)
{
	RetOut_UnZippedPaths.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_WEBSERVICE_BUS_EMAIL_ATTACHMENT_LIST));
	
	DbSimpleOrm TableOrm_Attachments(Ret_pStatus, BUS_EMAIL_ATTACHMENT, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	QString strDirectory=DataHelper::EncodeSession2Directory(g_UserSessionManager->GetSessionID());
	QString strServerFilePath=g_UserSessionManager->InitializeUserStorageDirectory(Ret_pStatus,strDirectory);
	if(!Ret_pStatus.IsOK()) 	
		return;

	//get attachment into memory:
	//--------------------------------
	DbRecordSet lstDataAttach;
	QString strWhere ="WHERE BEA_ID="+QVariant(nAttachmentID).toString();
	TableOrm_Attachments.Read(Ret_pStatus, lstDataAttach, strWhere);
	if(!Ret_pStatus.IsOK()) return;

	if (lstDataAttach.getRowCount()!=1)
		return;

	QString strAttachmentSubDirAlone="_email_attach_"+QString::number(nAttachmentID);
	QDir attach(strServerFilePath+"/"+strAttachmentSubDirAlone);
	if (!attach.exists())
	{
		QDir attach_create(strServerFilePath);
		if(!attach_create.mkdir(strAttachmentSubDirAlone))
		{
			Ret_pStatus.setError(StatusCodeSet::ERR_BUS_DOCUMENT_FROM_ATTACH,"Failed to create directory "+strAttachmentSubDirAlone);
			return;
		}
	}

	QString strAttachmentSubDir=strServerFilePath+"/"+strAttachmentSubDirAlone;

	//save onto disk
	//----------------------------------------------------------------
	QString strAttachmentFileName=lstDataAttach.getDataRef(0,"BEA_NAME").toString();
	QString strFilePath=strAttachmentSubDir+"/"+strAttachmentFileName;
	QFile file(strFilePath);
	if(!file.open(QIODevice::WriteOnly))
	{
		Ret_pStatus.setError(StatusCodeSet::ERR_BUS_DOCUMENT_FROM_ATTACH,"Failed to create file "+strAttachmentFileName);
		DataHelper::RemoveAllFromDirectory(strAttachmentSubDir,true,true);
		return;
	}
	QByteArray	byteFileContent=qUncompress(lstDataAttach.getDataRef(0,"BEA_CONTENT").toByteArray()); 
	int nErr=file.write(byteFileContent);
	if (nErr==-1)
	{
		Ret_pStatus.setError(StatusCodeSet::ERR_BUS_DOCUMENT_FROM_ATTACH,"Failed to write to file "+strAttachmentFileName);
		DataHelper::RemoveAllFromDirectory(strAttachmentSubDir,true,true);
		return;
	}
	file.close();

	//unzip if requested: all files are inside /dir/ in lstFiles
	//----------------------------------------------------------------
	QStringList lstFiles;
	lstFiles<<strAttachmentFileName;

	if (bUnzip)
	{
		//unzip:
		lstFiles.clear();
		if(!ZipCompression::Unzip(strFilePath,strAttachmentSubDir,lstFiles,true))
		{
			Ret_pStatus.setError(StatusCodeSet::ERR_BUS_DOCUMENT_FROM_ATTACH,"Failed to unzip file "+strAttachmentFileName);
			DataHelper::RemoveAllFromDirectory(strAttachmentSubDir,true,true);
			return;
		}
		/*
		QStringList lstFilesNew;
		int nSize=lstFiles.size();
		for (int i=0;i<nSize;i++)
		{
			QString strPath=lstFiles.at(i).toLower().mid(strAttachmentSubDir.length());
			if (strPath.left(1)!="/")
				strPath.prepend("/");
			lstFilesNew.append(strPath);
		}

		//B.T.: as zip can contains subfolders set them as relative path: c:/gffgfg -> /xx/yy/file.pdf

		if (lstFiles.size()>0)
		{
			ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("SaveAttachment 1st unzipped path: %1").arg(lstFiles.at(0)));
			ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("SaveAttachment 1st unzipped path: %1").arg(lstFilesNew.at(0)));
		}

		if (lstFiles.size()>1)
		{
			ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("SaveAttachment 1st unzipped path: %1").arg(lstFiles.at(1)));
			ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("SaveAttachment 1st unzipped path: %1").arg(lstFilesNew.at(1)));
		}


		lstFiles=lstFilesNew;
		*/
	}


	int nSize=lstFiles.size();
	RetOut_UnZippedPaths.addRow(nSize);
	RetOut_UnZippedPaths.addColumn(QVariant::String,"LOCAL_SERVER_PATH"); //B.T. 2014.01.29: Added temp to read binary data..
	for (int i=0;i<nSize;i++)
	{
		QFileInfo info(strAttachmentSubDir+"/"+lstFiles.at(i));

		if (info.fileName().trimmed().isEmpty()) //unzipped file with subdirs:
		{
			continue;
		}
		QString strSubDir=lstFiles.at(i).mid(strAttachmentSubDir.length());
		strSubDir=QDir::cleanPath(strAttachmentSubDirAlone+"/"+strSubDir.left(strSubDir.indexOf(info.fileName(),0,Qt::CaseInsensitive)));

		QString strURL=UserStorageHelper::GenerateRequestQueryString(lstFiles.at(i),"download",strSubDir);
		strURL+="?web_session="+g_UserSessionManager->GetSessionID();
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("SaveAttachment file: %1 path: %2").arg(info.fileName()).arg(strURL));
		RetOut_UnZippedPaths.setData(i,"BEA_ID",nAttachmentID);
		RetOut_UnZippedPaths.setData(i,"BEA_NAME",info.fileName());
		RetOut_UnZippedPaths.setData(i,"URL",strURL);
		RetOut_UnZippedPaths.setData(i,"LOCAL_SERVER_PATH",info.absoluteFilePath());
		
		RetOut_UnZippedPaths.setData(i,"EXT",info.completeSuffix().toLower().trimmed());
	
	}

}


void Service_WebEmails::ReadEmailAsPdf(Status &Ret_pStatus, int nEmailID,QString &Ret_strDocumentURL)
{
	DbRecordSet rowEmail;
	DbRecordSet lstAttachment;

	//Make local query, can fail if connection reservation fails.
	//DbSimpleOrm TableOrm(Ret_pStatus, BUS_EMAIL, GetDbManager()); 
	//if(!Ret_pStatus.IsOK()) return;

	DbSimpleOrm TableOrm_ATT(Ret_pStatus, BUS_EMAIL_ATTACHMENT, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;


	QString strSQL="SELECT BEM_SUBJECT, BEB_BODY AS BEM_BODY FROM BUS_EMAIL INNER JOIN BUS_EMAIL_BINARY ON BEB_EMAIL_ID = BEM_ID WHERE BEM_ID="+QString::number(nEmailID);
	TableOrm_ATT.GetDbSqlQuery()->Execute(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK()) return;

	TableOrm_ATT.GetDbSqlQuery()->FetchData(rowEmail);

	/*
	QString strWhere=" WHERE BEM_ID="+QString::number(nEmailID);
	TableOrm.Read(Ret_pStatus,rowEmail,strWhere);
	if(!Ret_pStatus.IsOK()) return;
	*/

	if (rowEmail.getRowCount()!=1)
	{
		Ret_pStatus.setError(1,"No email found");
		return;
	}

	TableOrm_ATT.Read(Ret_pStatus,lstAttachment," WHERE BEA_EMAIL_ID="+QString::number(nEmailID));
	if(!Ret_pStatus.IsOK()) return;


	QString strDirectory=DataHelper::EncodeSession2Directory(g_UserSessionManager->GetSessionID());
	QString strServerFilePath=g_UserSessionManager->InitializeUserStorageDirectory(Ret_pStatus,strDirectory);
	if(!Ret_pStatus.IsOK()) 	
		return;

	QString strAttachmentSubDirAlone="_email_body_"+QString::number(nEmailID);
	QDir attach(strServerFilePath+"/"+strAttachmentSubDirAlone);
	if (!attach.exists())
	{
		QDir attach_create(strServerFilePath);
		if(!attach_create.mkdir(strAttachmentSubDirAlone))
		{
			Ret_pStatus.setError(StatusCodeSet::ERR_BUS_DOCUMENT_FROM_ATTACH,"Failed to create directory "+strAttachmentSubDirAlone);
			return;
		}
	}
	QString strAttachmentSubDir=strServerFilePath+"/"+strAttachmentSubDirAlone;
	UnpackCIDToTemp(rowEmail,lstAttachment,strAttachmentSubDir);

	QByteArray byteContent=rowEmail.getDataRef(0,"BEM_BODY").toByteArray();
	QString strName=rowEmail.getDataRef(0,"BEM_SUBJECT").toByteArray();
	strName=QUrl::toPercentEncoding(strName);

	QString strInFile=strAttachmentSubDir+"/"+strName+".html";
	QString strPDFFile=strAttachmentSubDir+"/"+strName+".pdf";

	//write html:
	QFile html_file(strInFile);
	if(!html_file.open(QIODevice::WriteOnly))
	{
		Ret_pStatus.setError(1,"Error while trying to convert to pdf");
		return;
	}
	html_file.write(byteContent);
	html_file.close();

	QProcess gentool;

	//call gen tool:
	QStringList lstArgs;
	lstArgs<<"-f";
	lstArgs<<"pdf";
	lstArgs<<"-i";
	lstArgs<<strInFile;
	lstArgs<<"-o";
	lstArgs<<strPDFFile;
	lstArgs<<"-t";
	lstArgs<<strAttachmentSubDir;

	gentool.start("gentool",lstArgs);
	if (!gentool.waitForFinished(-1)) //wait forever
	{	
		Ret_pStatus.setError(1,"Error while trying to convert to pdf");
		return;
	}

	int nExitCode=gentool.exitCode();
	QByteArray errContent=gentool.readAllStandardError();
	if (nExitCode != 0 && errContent.size()!=0)
	{
		Ret_pStatus.setError(1,"Error while trying to convert to pdf");
		return;
	}

	//return link:
	Ret_strDocumentURL=UserStorageHelper::GenerateRequestQueryString(strPDFFile,"download",strAttachmentSubDirAlone);
}

void Service_WebEmails::EmbedCidAsURI(DbRecordSet &rowEmail,DbRecordSet &lstAttachments,QString strTempPath, QString &strBody, DbRecordSet &lstCIDInfo, QString strTagStart, QString strSettingStart, bool &bChanged)
{
	int nStartPos = 0;

	while(1)
	{
		//search sample: <img src="cid:part1.01040700.04020300@st.t-com.hr" alt="">
		//search sample: <img id="aaaa" src="cid:part1.01040700.04020300@st.t-com.hr" alt="">
		int nPos = strBody.indexOf(strTagStart, nStartPos, Qt::CaseInsensitive);
		if(nPos >= 0)
		{
			int nPosLinkEnd = strBody.indexOf(">", nPos+1);
			bool bStrange = false;
			int nPos2 = strBody.indexOf(strSettingStart + "\"cid:", nPos+1);
			if(nPos2 < 0){
				nPos2 = strBody.indexOf(strSettingStart + "3D\"cid:", nPos+1);
				bStrange = true;
			}

			if(nPos2 < 0 || nPos2 > nPosLinkEnd)
			{
				nStartPos = nPos+1;
				continue;
			}

			bChanged = true;

			//extract CID
			QString strCIDLink;
			QString strCID;
			if(nPosLinkEnd >= 0){
				strCIDLink = strBody.mid(nPos, nPosLinkEnd-nPos+1);

				nPos2 += bStrange? strSettingStart.length() + strlen("3D\"cid:") : strSettingStart.length() +  strlen("\"cid:");
				strCID = strBody.mid(nPos2, nPosLinkEnd-nPos2+1);
				int nEnd = strCID.indexOf("\"");
				if(nEnd > 0)
					strCID = strCID.mid(0, nEnd);
			}

			if(strCID.isEmpty()){
				Q_ASSERT(false);		//invalid CID in HTML
				nStartPos = nPos+1;
				continue;
			}

			//find attachment having matching CID
			int nAttCnt = lstAttachments.getRowCount();
			int nAttIdx = -1;
			for(int i=0; i<nAttCnt; i++){
				if (lstAttachments.getDataRef(i,"BEA_CID_LINK").toString() == strCID){
					nAttIdx = i;
					break;
				}
			}
			if(nAttIdx < 0){
				//Q_ASSERT(false);		//could not find matching attachment
				nStartPos = nPos + qMin(1, strCIDLink.length());
				continue;
			}

			// unpack the attachment to temp folder
			QString strName = lstAttachments.getDataRef(nAttIdx,"BEA_NAME").toString();
			QFileInfo attInfo(strName);
			QString strTmpPath;
			//generate unique temp name
			int nTry = 0; bool bOK = false;
			while(nTry < 30 && !bOK){
				strTmpPath = strTempPath;
				strTmpPath += "/";
				strTmpPath += attInfo.completeBaseName();
				strTmpPath += QVariant(nTry).toString();
				strTmpPath += ".";
				strTmpPath += attInfo.completeSuffix();
				nTry ++;
				bOK = !QFile::exists(strTmpPath);
			}
			if(!bOK){
				Q_ASSERT(false);	//could not create unused temp path
				nStartPos = nPos + qMin(1, strCIDLink.length());
				continue;
			}

			//save attachment
			QFileInfo infoTarget(strTmpPath);
			QString strBaseName=infoTarget.fileName();
			QFile file(strTmpPath);
			if(file.open(QIODevice::WriteOnly))
			{
				QByteArray byteFileContent=qUncompress(lstAttachments.getDataRef(nAttIdx,"BEA_CONTENT").toByteArray()); 
				file.write(byteFileContent);
				file.close();
			}

			//remember att info for revert operation
			lstCIDInfo.addRow();
			int nRow = lstCIDInfo.getRowCount()-1;
			lstCIDInfo.setData(nRow, "CID", strCID);
			lstCIDInfo.setData(nRow, "Path", strTmpPath);
			lstCIDInfo.setData(nRow, "IsTemporary", 1);

			//rewrite tag in the HTML body
			strBody = strBody.mid(0, nPos) + strBody.mid(nPos + strCIDLink.length());

			QString strNewTag = strTagStart + " " + strSettingStart.length() + "\"";
			strNewTag += strTmpPath;
			strNewTag += "\">";
			strBody.insert(nPos, strNewTag);

			//keep searching forward
			nStartPos = nPos + strNewTag.length();
		}
		else
			break;
	}
}

void Service_WebEmails::UnpackCIDToTemp(DbRecordSet &rowEmail,DbRecordSet &lstAttachments,QString strTempPath)
{
	DbRecordSet lstCIDInfo;

	//PROTECTION: must not call this more than once
	//Q_ASSERT(!m_bCIDPrepared);
	//if (m_bCIDPrepared)
	//	return;
	//m_bCIDPrepared = true;

	//THIS is called at the dialog startup:
	//inspect html body for "<img scr="cid:... " and 
	//rewrite these links as an ordinary link to temporary unpacked image attachments
	lstCIDInfo.destroy();
	lstCIDInfo.addColumn(QVariant::String,  "CID");
	lstCIDInfo.addColumn(QVariant::String,  "Path");
	lstCIDInfo.addColumn(QVariant::Int,	  "IsTemporary");

	bool bChanged = false;
	QString strBody = rowEmail.getDataRef(0,"BEM_BODY").toString();
	strBody = strBody.replace("=3D", "=");

	/*
	//TOFIX this did not work too well, so restored original below
	EmbedCidAsURI(rowEmail, lstAttachments, strTempPath, strBody, lstCIDInfo, "<img", "src=", bChanged);
	//MB: 2014.03.07 new CSS properties that can contain CID embedded image
	EmbedCidAsURI(rowEmail, lstAttachments, strTempPath, strBody, lstCIDInfo, "<table", "background=", bChanged);
	EmbedCidAsURI(rowEmail, lstAttachments, strTempPath, strBody, lstCIDInfo, "<div", "background=", bChanged);
	EmbedCidAsURI(rowEmail, lstAttachments, strTempPath, strBody, lstCIDInfo, "<td", "background=", bChanged);
	*/

	int nStartPos = 0;
	while(1)
	{
		//search sample: <img src="cid:part1.01040700.04020300@st.t-com.hr" alt="">
		//search sample: <img id="aaaa" src="cid:part1.01040700.04020300@st.t-com.hr" alt="">
		int nPos = strBody.indexOf("<img", nStartPos, Qt::CaseInsensitive);
		if(nPos >= 0)
		{
			int nPosLinkEnd = strBody.indexOf(">", nPos+1);
			bool bStrange = false;
			int nPos2 = strBody.indexOf("src=\"cid:", nPos+1);
			if(nPos2 < 0){
				nPos2 = strBody.indexOf("src=3D\"cid:", nPos+1);
				bStrange = true;
			}

			if(nPos2 < 0 || nPos2 > nPosLinkEnd)
			{
				nStartPos = nPos+1;
				continue;
			}

			bChanged = true;

			//extract CID
			QString strCIDLink;
			QString strCID;
			if(nPosLinkEnd >= 0){
				strCIDLink = strBody.mid(nPos, nPosLinkEnd-nPos+1);

				nPos2 += bStrange? strlen("src=3D\"cid:") : strlen("src=\"cid:");
				strCID = strBody.mid(nPos2, nPosLinkEnd-nPos2+1);
				int nEnd = strCID.indexOf("\"");
				if(nEnd > 0)
					strCID = strCID.mid(0, nEnd);
			}

			if(strCID.isEmpty()){
				Q_ASSERT(false);		//invalid CID in HTML
				nStartPos = nPos+1;
				continue;
			}

			//find attachment having matching CID
			int nAttCnt = lstAttachments.getRowCount();
			int nAttIdx = -1;
			for(int i=0; i<nAttCnt; i++){
				if (lstAttachments.getDataRef(i,"BEA_CID_LINK").toString() == strCID){
					nAttIdx = i;
					break;
				}
			}
			if(nAttIdx < 0){
				//Q_ASSERT(false);		//could not find matching attachment
				nStartPos = nPos + qMin(1, strCIDLink.length());
				continue;
			}

			// unpack the attachment to temp folder
			QString strName = lstAttachments.getDataRef(nAttIdx,"BEA_NAME").toString();
			QFileInfo attInfo(strName);
			QString strTmpPath;
			//generate unique temp name
			int nTry = 0; bool bOK = false;
			while(nTry < 30 && !bOK){
				strTmpPath = strTempPath;
				strTmpPath += "/";
				strTmpPath += attInfo.completeBaseName();
				strTmpPath += QVariant(nTry).toString();
				strTmpPath += ".";
				strTmpPath += attInfo.completeSuffix();
				nTry ++;
				bOK = !QFile::exists(strTmpPath);
			}
			if(!bOK){
				Q_ASSERT(false);	//could not create unused temp path
				nStartPos = nPos + qMin(1, strCIDLink.length());
				continue;
			}

			//save attachment
			QFileInfo infoTarget(strTmpPath);
			QString strBaseName=infoTarget.fileName();
			QFile file(strTmpPath);
			if(file.open(QIODevice::WriteOnly))
			{
				QByteArray byteFileContent=qUncompress(lstAttachments.getDataRef(nAttIdx,"BEA_CONTENT").toByteArray()); 
				file.write(byteFileContent);
				file.close();
			}

			//remember att info for revert operation
			lstCIDInfo.addRow();
			int nRow = lstCIDInfo.getRowCount()-1;
			lstCIDInfo.setData(nRow, "CID", strCID);
			lstCIDInfo.setData(nRow, "Path", strTmpPath);
			lstCIDInfo.setData(nRow, "IsTemporary", 1);

			//rewrite tag in the HTML body
			strBody = strBody.mid(0, nPos) + strBody.mid(nPos + strCIDLink.length());

			QString strNewTag = "<img src=\"";
			strNewTag += strTmpPath;
			strNewTag += "\">";
			strBody.insert(nPos, strNewTag);

			//keep searching forward
			nStartPos = nPos + strNewTag.length();
		}
		else
			break;
	}
	
	//replace body string with "patched" one
	if(bChanged)
		rowEmail.setData(0,"BEM_BODY", strBody);
}

void Service_WebEmails::ReadEmailAsHtml(Status &Ret_pStatus, int nEmailID,QString &Ret_strDocumentURL)
{
	DbRecordSet rowEmail;
	DbRecordSet lstAttachment;

	//Make local query, can fail if connection reservation fails.
	//DbSimpleOrm TableOrm(Ret_pStatus, BUS_EMAIL, GetDbManager()); 
	//if(!Ret_pStatus.IsOK()) return;

	DbSimpleOrm TableOrm_ATT(Ret_pStatus, BUS_EMAIL_ATTACHMENT, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	

	QString strSQL="SELECT BEM_EMAIL_TYPE, BEB_BODY AS BEM_BODY FROM BUS_EMAIL INNER JOIN BUS_EMAIL_BINARY ON BEB_EMAIL_ID = BEM_ID WHERE BEM_ID="+QString::number(nEmailID);
	TableOrm_ATT.GetDbSqlQuery()->Execute(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK()) return;

	TableOrm_ATT.GetDbSqlQuery()->FetchData(rowEmail);

	/*
	QString strWhere=" WHERE BEM_ID="+QString::number(nEmailID);
	TableOrm.Read(Ret_pStatus,rowEmail,strWhere);
	if(!Ret_pStatus.IsOK()) return;
	*/
	if (rowEmail.getRowCount()!=1)
	{
		Ret_pStatus.setError(1,"No email found");
		return;
	}

	TableOrm_ATT.Read(Ret_pStatus,lstAttachment," WHERE BEA_EMAIL_ID="+QString::number(nEmailID));
	if(!Ret_pStatus.IsOK()) return;


	QString strDirectory=DataHelper::EncodeSession2Directory(g_UserSessionManager->GetSessionID());
	QString strServerFilePath=g_UserSessionManager->InitializeUserStorageDirectory(Ret_pStatus,strDirectory);
	if(!Ret_pStatus.IsOK()) 	
		return;

	QString strAttachmentSubDirAlone="_email_body_"+QString::number(nEmailID);
	QDir attach(strServerFilePath+"/"+strAttachmentSubDirAlone);
	if (!attach.exists())
	{
		QDir attach_create(strServerFilePath);
		if(!attach_create.mkdir(strAttachmentSubDirAlone))
		{
			Ret_pStatus.setError(StatusCodeSet::ERR_BUS_DOCUMENT_FROM_ATTACH,"Failed to create directory "+strAttachmentSubDirAlone);
			return;
		}
	}
	QString strAttachmentSubDir=strServerFilePath+"/"+strAttachmentSubDirAlone;
	UnpackCIDToTemp(rowEmail,lstAttachment,strAttachmentSubDir);

	//QByteArray byteContent=rowEmail.getDataRef(0,"BEM_BODY").toByteArray();
	QString byteContent=rowEmail.getDataRef(0,"BEM_BODY").toString();
	//QString strName=rowEmail.getDataRef(0,"BEM_SUBJECT").toByteArray();
	//strName=QUrl::toPercentEncoding(strName);
	QString strName="email_"+QString::number(nEmailID); //B.T. changed 9-26-2013 to avoid umlaut and other problems...

	//replace all link of CIDs with url link: /
	byteContent = byteContent.replace(strAttachmentSubDir+"/","");


	//Issue: if plain txt then save as .txt...
	QString strInFile=strAttachmentSubDir+"/"+strName+".html";
	if(rowEmail.getDataRef(0,"BEM_EMAIL_TYPE").toInt()==GlobalConstants::EMAIL_TYPE_PLAIN_TEXT)
	{
		//strInFile=strAttachmentSubDir+"/"+strName+".txt";
		byteContent = byteContent.replace("\n","<br>");
		byteContent = byteContent.prepend("<html><body>");
		byteContent = byteContent.append("</body></html>");
	}

	//write html:
	QFile html_file(strInFile);
	if(!html_file.open(QIODevice::WriteOnly))
	{
		Ret_pStatus.setError(1,"Error while trying to convert to html/txt");
		return;
	}
	
	QByteArray x = byteContent.toUtf8();
	//QByteArray x = byteContent.toLatin1(); //B.T. 09.10.2013: try with toLatin1()...Marin se zali da nema umlauta..
	html_file.write(x);
	html_file.close();

	//return link:
	Ret_strDocumentURL=UserStorageHelper::GenerateRequestQueryString(strInFile,"download",strAttachmentSubDirAlone);

}

void Service_WebEmails::ReadEmailAsPdfBinary( Status &Ret_pStatus, int nEmailID,QByteArray &Ret_byteData)
{
	DbRecordSet rowEmail;
	DbRecordSet lstAttachment;

	//Make local query, can fail if connection reservation fails.
	//DbSimpleOrm TableOrm(Ret_pStatus, BUS_EMAIL, GetDbManager()); 
	//if(!Ret_pStatus.IsOK()) return;

	DbSimpleOrm TableOrm_ATT(Ret_pStatus, BUS_EMAIL_ATTACHMENT, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	QString strSQL="SELECT BEM_SUBJECT, BEB_BODY AS BEM_BODY FROM BUS_EMAIL INNER JOIN BUS_EMAIL_BINARY ON BEB_EMAIL_ID = BEM_ID WHERE BEM_ID="+QString::number(nEmailID);
	TableOrm_ATT.GetDbSqlQuery()->Execute(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK()) return;
	TableOrm_ATT.GetDbSqlQuery()->FetchData(rowEmail);


	if (rowEmail.getRowCount()!=1)
	{
		Ret_pStatus.setError(1,"No email found");
		return;
	}

	TableOrm_ATT.Read(Ret_pStatus,lstAttachment," WHERE BEA_EMAIL_ID="+QString::number(nEmailID));
	if(!Ret_pStatus.IsOK()) return;


	QString strDirectory=DataHelper::EncodeSession2Directory(g_UserSessionManager->GetSessionID());
	QString strServerFilePath=g_UserSessionManager->InitializeUserStorageDirectory(Ret_pStatus,strDirectory);
	if(!Ret_pStatus.IsOK()) 	
		return;

	QString strAttachmentSubDirAlone="_email_body_"+QString::number(nEmailID);
	QDir attach(strServerFilePath+"/"+strAttachmentSubDirAlone);
	if (!attach.exists())
	{
		QDir attach_create(strServerFilePath);
		if(!attach_create.mkdir(strAttachmentSubDirAlone))
		{
			Ret_pStatus.setError(StatusCodeSet::ERR_BUS_DOCUMENT_FROM_ATTACH,"Failed to create directory "+strAttachmentSubDirAlone);
			return;
		}
	}
	QString strAttachmentSubDir=strServerFilePath+"/"+strAttachmentSubDirAlone;
	UnpackCIDToTemp(rowEmail,lstAttachment,strAttachmentSubDir);

	QByteArray byteContent=rowEmail.getDataRef(0,"BEM_BODY").toByteArray();
	QString strName=rowEmail.getDataRef(0,"BEM_SUBJECT").toByteArray();
	strName=QUrl::toPercentEncoding(strName);

	QString strInFile=strAttachmentSubDir+"/"+strName+".html";
	QString strPDFFile=strAttachmentSubDir+"/"+strName+".pdf";

	//write html:
	QFile html_file(strInFile);
	if(!html_file.open(QIODevice::WriteOnly))
	{
		Ret_pStatus.setError(1,"Error while trying to convert to pdf");
		return;
	}
	html_file.write(byteContent);
	html_file.close();

	QProcess gentool;

	//call gen tool:
	QStringList lstArgs;
	lstArgs<<"-f";
	lstArgs<<"pdf";
	lstArgs<<"-i";
	lstArgs<<strInFile;
	lstArgs<<"-o";
	lstArgs<<strPDFFile;
	lstArgs<<"-t";
	lstArgs<<strAttachmentSubDir;

	gentool.start("gentool",lstArgs);
	if (!gentool.waitForFinished(-1)) //wait forever
	{	
		Ret_pStatus.setError(1,"Error while trying to convert to pdf");
		return;
	}

	int nExitCode=gentool.exitCode();
	QByteArray errContent=gentool.readAllStandardError();
	if (nExitCode != 0 && errContent.size()!=0)
	{
		Ret_pStatus.setError(1,"Error while trying to convert to pdf");
		return;
	}


	//read pdf:
	QFile pdf_file(strPDFFile);
	if(!pdf_file.open(QIODevice::ReadOnly))
	{
		Ret_pStatus.setError(1,"Error while trying to read pdf");
		return;
	}
	Ret_byteData = pdf_file.readAll();
	pdf_file.close();


}

void Service_WebEmails::ReadEmailAsHtmlBinary( Status &Ret_pStatus, int nEmailID,QByteArray &Ret_byteData )
{
	QTime timer_api;
	timer_api.start();

	DbRecordSet rowEmail;
	DbRecordSet lstAttachment;

	//Make local query, can fail if connection reservation fails.
	//DbSimpleOrm TableOrm(Ret_pStatus, BUS_EMAIL, GetDbManager()); 
	//if(!Ret_pStatus.IsOK()) return;

	DbSimpleOrm TableOrm_ATT(Ret_pStatus, BUS_EMAIL_ATTACHMENT, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	QString strSQL="SELECT BEM_EMAIL_TYPE, BEB_BODY AS BEM_BODY FROM BUS_EMAIL INNER JOIN BUS_EMAIL_BINARY ON BEB_EMAIL_ID = BEM_ID WHERE BEM_ID="+QString::number(nEmailID);
	TableOrm_ATT.GetDbSqlQuery()->Execute(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK()) return;
	TableOrm_ATT.GetDbSqlQuery()->FetchData(rowEmail);

	//	QString strWhere=" WHERE BEM_ID="+QString::number(nEmailID);
	//TableOrm.Read(Ret_pStatus,rowEmail,strWhere);
	//if(!Ret_pStatus.IsOK()) return;
	/*
	int nElapsed=timer_api.elapsed();
	if (nElapsed>1000)
	{
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_SPEED_TEST_LEVEL,1,QString("ReadEmailAsHtmlBinary step 1, time: %1, thread: %2").arg(nElapsed).arg(ThreadIdentificator::GetCurrentThreadID()));
	}
	timer_api.restart();
	*/

	if (rowEmail.getRowCount()!=1)
	{
		Ret_pStatus.setError(1,"No email found");
		return;
	}

	TableOrm_ATT.Read(Ret_pStatus,lstAttachment," WHERE BEA_EMAIL_ID="+QString::number(nEmailID));
	if(!Ret_pStatus.IsOK()) return;
	/*
	nElapsed=timer_api.elapsed();
	if (nElapsed>1000)
	{
		ApplicationLogger::logMessage(StatusCodeSet::TYPE_SPEED_TEST_LEVEL,1,QString("ReadEmailAsHtmlBinary step 2, time: %1, thread: %2").arg(nElapsed).arg(ThreadIdentificator::GetCurrentThreadID()));
	}
	*/

	QString strDirectory=DataHelper::EncodeSession2Directory(g_UserSessionManager->GetSessionID());
	QString strServerFilePath=g_UserSessionManager->InitializeUserStorageDirectory(Ret_pStatus,strDirectory);
	if(!Ret_pStatus.IsOK()) 	
		return;

	QString strAttachmentSubDirAlone="_email_body_"+QString::number(nEmailID);
	QDir attach(strServerFilePath+"/"+strAttachmentSubDirAlone);
	if (!attach.exists())
	{
		QDir attach_create(strServerFilePath);
		if(!attach_create.mkdir(strAttachmentSubDirAlone))
		{
			Ret_pStatus.setError(StatusCodeSet::ERR_BUS_DOCUMENT_FROM_ATTACH,"Failed to create directory "+strAttachmentSubDirAlone);
			return;
		}
	}
	QString strAttachmentSubDir=strServerFilePath+"/"+strAttachmentSubDirAlone;
	UnpackCIDToTemp(rowEmail,lstAttachment,strAttachmentSubDir);

	//QByteArray byteContent=rowEmail.getDataRef(0,"BEM_BODY").toByteArray();
	QString byteContent=rowEmail.getDataRef(0,"BEM_BODY").toString();
	//QString strName=rowEmail.getDataRef(0,"BEM_SUBJECT").toByteArray();
	//strName=QUrl::toPercentEncoding(strName);
	QString strName="email_"+QString::number(nEmailID); //B.T. changed 9-26-2013 to avoid umlaut and other problems...

	//replace all link of CIDs with url link: /
	byteContent = byteContent.replace(strAttachmentSubDir+"/","");


	//Issue: if plain txt then save as .txt...
	QString strInFile=strAttachmentSubDir+"/"+strName+".html";
	if(rowEmail.getDataRef(0,"BEM_EMAIL_TYPE").toInt()==GlobalConstants::EMAIL_TYPE_PLAIN_TEXT)
	{
		//strInFile=strAttachmentSubDir+"/"+strName+".txt";
		byteContent = byteContent.replace("\n","<br>");
		byteContent = byteContent.prepend("<html><body>");
		byteContent = byteContent.append("</body></html>");
	}

	//write html:
	QFile html_file(strInFile);
	if(!html_file.open(QIODevice::WriteOnly))
	{
		Ret_pStatus.setError(1,"Error while trying to convert to html/txt");
		return;
	}

	EmbedHtmlPix(byteContent,strAttachmentSubDir);

	QByteArray x = byteContent.toUtf8();
	//QByteArray x = byteContent.toLatin1(); //B.T. 09.10.2013: try with toLatin1()...Marin se zali da nema umlauta..
	html_file.write(x);
	html_file.close();

	Ret_byteData=x;


}

void Service_WebEmails::GetAttachmentBinary( Status &Ret_pStatus, int nAttachmentID, QByteArray &Ret_byteData )
{
	DbRecordSet Ret_AttachmentFiles;
	SaveAttachment(Ret_pStatus,nAttachmentID,Ret_AttachmentFiles,false);

	ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("GetAttachmentBinary attach list cnt: %1").arg(Ret_AttachmentFiles.getRowCount()));

	if (Ret_AttachmentFiles.getRowCount()>0)
	{
		QString strFile = Ret_AttachmentFiles.getDataRef(0,"LOCAL_SERVER_PATH").toString();

		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("GetAttachmentBinary attach list read local file: %1").arg(strFile));

		//read pdf:
		QFile pdf_file(Ret_AttachmentFiles.getDataRef(0,"LOCAL_SERVER_PATH").toString());
		if(!pdf_file.open(QIODevice::ReadOnly))
		{
			Ret_pStatus.setError(1,"Error while trying to read pdf");
			return;
		}

		Ret_byteData = pdf_file.readAll();

		ApplicationLogger::logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("GetAttachmentBinary attach list read bytes : %1").arg(Ret_byteData.size()));

		pdf_file.close();
	}

}




void Service_WebEmails::EmbedHtmlPix(QString &strHtmlData, QString strAttachmentDir)
{
	QStringList lstResOrigFiles;

	//find and embed each picture in the HTML
	int nPos = strHtmlData.indexOf("<img", 0, Qt::CaseInsensitive);
	while(nPos >= 0)
	{
		//extract file path
		int nSrcStart = strHtmlData.indexOf("src=\"", nPos, Qt::CaseInsensitive);
		if(nSrcStart > 0)
		{
			nSrcStart += 5; //strlen("src=\"")
			int nSrcEnd = strHtmlData.indexOf("\"", nSrcStart);
			if(nSrcEnd > 0)
			{
				QString strFile = strHtmlData.mid(nSrcStart, nSrcEnd-nSrcStart);

				//FIX: skip loading remote files, issue #2050
				if(strFile.startsWith("http://")){
					nPos = nSrcEnd + 1;
					//search next
					nPos = strHtmlData.indexOf("<img", nPos+1, Qt::CaseInsensitive);
					continue;
				}

				//load file into the byte array
				QFile file(strAttachmentDir+"/"+strFile);
				if (file.open(QIODevice::ReadOnly))
				{
					//convert to base64
					QByteArray arDataBase64 = file.readAll().toBase64();

					//write back into the string (replace old "src" value with embedded data)
					strHtmlData.remove(nSrcStart, nSrcEnd-nSrcStart);
					strHtmlData.insert(nSrcStart, QString(arDataBase64));

					//use extension for mime type
					QString strExt = strFile;
					int nPosExt = strExt.lastIndexOf('.');
					if(nPosExt > 0)
						strExt = strExt.mid(nPosExt+1);
					strHtmlData.insert(nSrcStart, QString("data:image/%1;base64,").arg(strExt));

					lstResOrigFiles.append(strFile);
				}
			}
		}

		//search next
		nPos = strHtmlData.indexOf("<img", nPos+1, Qt::CaseInsensitive);
	}
}





//first load from DB then save only necessary fields and save it back...
void Service_WebEmails::ResetLastRefreshDate(int nPersonID)
{
	DbRecordSet lstPersonAccData;
	LoadPersonEmailAccData(nPersonID,lstPersonAccData);

	if (lstPersonAccData.getRowCount()>0)
	{
		QDateTime datLastCheck=QDateTime::currentDateTime().addDays(-1);
		if (lstPersonAccData.getColumnIdx("POP3_LAST_CHECK")>=0)
			lstPersonAccData.setColValue("POP3_LAST_CHECK",datLastCheck);

		ApplicationLogger::logMessage(StatusCodeSet::TYPE_INFORMATION,0, QString("Reset check time to %1 for person %2").arg(datLastCheck.toString("yyyy-MM-dd HH:mm:ss")).arg(nPersonID));
		
		Status status;
		DbSqlQuery query(status, g_BusinessServiceSet->GetDbManager());
		if(!status.IsOK()) return;

		QByteArray binPersonSettings = XmlUtil::ConvertRecordSet2ByteArray_Fast(lstPersonAccData);
		QString strSQL = QString("UPDATE BUS_OPT_SETTINGS SET BOUS_VALUE_BYTE=? WHERE BOUS_SETTING_SET_ID=%1 AND BOUS_SETTING_ID=%2 AND BOUS_PERSON_ID=%3").arg(EMAIL_IN_SETTINGS).arg(EMAIL_IN_SETTINGS_ACCOUNTS).arg(nPersonID);
		query.Prepare(status, strSQL);
		if (!status.IsOK()) return;
		query.bindValue(0, binPersonSettings);
		query.ExecutePrepared(status);
		if (!status.IsOK()) return;
	}
}

void Service_WebEmails::LoadPersonEmailAccData(int nPersonID, DbRecordSet &lstPersonAccData)
{
	Status status;
	DbSqlQuery query(status, g_BusinessServiceSet->GetDbManager());
	if(!status.IsOK()) return;

	QString strSQL = QString("SELECT BOUS_VALUE_BYTE FROM BUS_OPT_SETTINGS WHERE BOUS_SETTING_SET_ID=%1 AND BOUS_SETTING_ID=%2 AND BOUS_PERSON_ID =%3").arg(EMAIL_IN_SETTINGS).arg(EMAIL_IN_SETTINGS_ACCOUNTS).arg(nPersonID);
	query.Execute(status, strSQL);
	if (!status.IsOK()) return;

	if (query.next())
	{
		QByteArray binPersonSettings;
		binPersonSettings=query.value(0).toByteArray();
		lstPersonAccData = XmlUtil::ConvertByteArray2RecordSet_Fast(binPersonSettings);
	}

}
