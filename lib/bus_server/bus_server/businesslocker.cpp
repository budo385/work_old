#include "businesslocker.h"
#include "db/db/dbsqlquery.h"

#include "bus_core/bus_core/usersessionmanager.h"
extern UserSessionManager	*g_UserSessionManager;
#include "db_core/db_core/dbsqltabledefinition.h"
#include "privateserviceset.h"
extern PrivateServiceSet* g_PrivateServiceSet; 
#include "common/common/sleeper.h"
#include "common/common/threadid.h"
//#include "common/common/loggerabstract.h"

quint64 BusinessLocker::m_nUniqueLockKey = 0;



/*!
	Business locker

	\param pStatus			- return error if connection create failed
	\param nTableId			- can be set to one table, or you can state own table
	\param pDatabaseManager - external manager
	\param strUserSession	- user session (must be set)
	\param pDb				- connection, if be NULL, then own connection will be created
	\param bCheckParents	- if true (then it will lock parent table instead of given record id's to lock)

*/
BusinessLocker::BusinessLocker(Status& pStatus,  int nTableId, DbSqlManager* pDatabaseManager, bool bCheckParents)
{
	pStatus.setError(0);

	DbTableKeyData pTableKeyData;
	m_nTableID=nTableId;


	DbSqlTableDefinition::GetKeyData(m_nTableID,pTableKeyData);
	m_strPrimaryKey=pTableKeyData.m_strPrimaryKey;
	m_strTableName=pTableKeyData.m_strTableName;

	m_pDbManager = pDatabaseManager;
	m_pDb		 = m_pDbManager->GetThreadDbConnection();

	m_bCheckParents = bCheckParents;
	if (m_pDb!=NULL)
	{
		m_bKeepConnectionFlag = true;
	}
	else
	{
		m_pDb = m_pDbManager->ReserveConnection(pStatus); //reserve connection
		if(!pStatus.IsOK()) return;
		m_bKeepConnectionFlag = false;
	}


	//for current thread:
	m_strUserSession=g_UserSessionManager->GetSessionID();
	m_intSessionRecordID=g_UserSessionManager->GetSessionRecordID();
	Q_ASSERT(!m_strUserSession.isEmpty()); //assert if session is invalid!!!
	Q_ASSERT(m_intSessionRecordID!=0); //assert if session is invalid!!!

}

BusinessLocker::~BusinessLocker()
{
	//if we reserved then release:
	if (!m_bKeepConnectionFlag)
		m_pDbManager->ReleaseConnection(m_pDb);
}


/*!
	Lock records, can lock all or nothing or just partial, in later case it will return list with records that are already locked

	\param pStatus				- return error
	\param TableID				- table id
	\param LockList				- list of ID records for locking
	\param strLockedResourceID	- returned resource id
	\param pLstStatusRows		- filled only with bad ones (doesnt succeed in locking) if bAllOrNothing= false
	\param bAllOrNothing		- true means all or nothing, else pLstStatusRows is filled with bad ones
	\param nTryLockTimeOut		- in ms, if locked then sleeps and tries 3x again with set timeout

	\return bool				- FALSE if status=error
*/
bool BusinessLocker::LockRecords(Status& pStatus, int TableID, DbLockList &LockList, QString& strLockedResourceID, DbRecordSet *pLstStatusRows,bool bAllOrNothing, int nTryLockTimeOut)
{
	pStatus.setError(0);
	bool bOK;
	int nTries=3; //3 times, sleep between tries..

	do
	{
		//check only if both agree
		if (!m_bCheckParents || DbSqlTableDefinition::GetParentID(TableID) == -1)
			bOK= LockRecordsInternal(pStatus, TableID, LockList, strLockedResourceID,pLstStatusRows,bAllOrNothing);
		else
		{
			DbLockList tmp=GetParentLockList(pStatus, TableID, LockList);
			bOK=  LockRecordsInternal(pStatus, DbSqlTableDefinition::GetParentID(TableID),tmp, 
				strLockedResourceID,pLstStatusRows,bAllOrNothing);
		}

		if (pStatus.IsOK())
			break;

		//try 2 times before quit
		nTries--;
		if (nTries==0)
			break;

		if (nTryLockTimeOut>0)
			ThreadSleeper::Sleep(nTryLockTimeOut);
	}
	while (pStatus.getErrorCode());

	ConvertIsLockedError(pStatus);
	return bOK;
}



//convenient, uses nTableID:
bool BusinessLocker::UnLock(Status &pStatus, QString &pResourceID)
{
	//if res empty, return
	pStatus.setError(0);
	if(pResourceID.isEmpty())
		return true;

	return UnlockByResourceID(pStatus,pResourceID);
}

bool BusinessLocker::UnLock(Status &pStatus, int nTableID, int nRecordID)
{
	pStatus.setError(0);
	return UnlockByRecordID(pStatus,nTableID,nRecordID);
}


//convenient, uses nTableID:
bool BusinessLocker::Lock(Status &pStatus, QList<int> &pLstForLocking, QString &pResourceID, DbRecordSet *pLstStatusRows, bool bAllOrNothing, int nTryLockTimeOut)
{
	return LockRecords(pStatus,m_nTableID, pLstForLocking,pResourceID, pLstStatusRows, bAllOrNothing,nTryLockTimeOut);
}


//convenient, uses nTableID, convert from full DbRecordSet to listlocking:
bool BusinessLocker::Lock(Status &pStatus , const DbRecordSet &pLstForLocking, QString &pResourceID, DbRecordSet *pLstStatusRows, bool bAllOrNothing, int nTryLockTimeOut)
{

	//convert locking list:
	DbLockList LockList;
	//find PK:
	int nPrimaryKeyColumn=GetPrimaryColumnNo(pLstForLocking);

	//Convert DbRecordSet to DbLockList.
	for (int i=0; i < pLstForLocking.getRowCount(); ++i)
	{
		if(!pLstForLocking.getDataRef(i, nPrimaryKeyColumn).isNull()&&pLstForLocking.getDataRef(i, nPrimaryKeyColumn)!=0)
			LockList.append(pLstForLocking.getDataRef(i, nPrimaryKeyColumn).toInt());
	}

	return LockRecords(pStatus, m_nTableID,LockList,pResourceID, pLstStatusRows, bAllOrNothing,nTryLockTimeOut);

}
//convert locking error to locked by ...
void BusinessLocker::ConvertIsLockedError(Status &pStatus)
{
	if(pStatus.getErrorCode()==StatusCodeSet::ERR_SQL_ALREADY_LOCKED)
	{

		//try to get user data from cache by session id:
		DbRecordSet rowUserData;
		g_UserSessionManager->GetUserData(rowUserData,pStatus.getErrorTextRaw());

		//if not found, load from DB; must come from another server
		if(rowUserData.getRowCount()==0)
		{
			g_PrivateServiceSet->CoreUser->GetUserBySession(pStatus,pStatus.getErrorTextRaw(),rowUserData);
			if(!pStatus.IsOK())return;
		}

		//if still not found, just exit:
		if(rowUserData.getRowCount()!=1)
			return;

		//compile error message:
		//Locked by FIRST_NAME & LAST NAME
		QString strErrorMessage;
		if(rowUserData.getDataRef(0,"BPER_CODE").toString().isEmpty()) //if user is not business person then just provide name
			strErrorMessage=rowUserData.getDataRef(0,"CUSR_FIRST_NAME").toString()+" "+rowUserData.getDataRef(0,"CUSR_LAST_NAME").toString();
		else
			strErrorMessage=rowUserData.getDataRef(0,"BPER_CODE").toString()+" - "+rowUserData.getDataRef(0,"CUSR_FIRST_NAME").toString()+" "+rowUserData.getDataRef(0,"CUSR_LAST_NAME").toString();

		pStatus.setError(StatusCodeSet::ERR_SQL_LOCKED_BY_ANOTHER,strErrorMessage);
	}
	else if (pStatus.getErrorCode()==StatusCodeSet::ERR_SQL_DUPLICATE_ENTRY) //already locked by another user/thread
	{
		pStatus.setError(StatusCodeSet::ERR_SQL_LOCKED_BY_ANOTHER_UNKNOWN);
	}


}





/* just assert to check wheteevr PK column exists in list */
int BusinessLocker::GetPrimaryColumnNo(const DbRecordSet &pLst)
{
	//find pk column inside list
	int nPrimaryKeyColumn= pLst.getColumnIdx(m_strPrimaryKey);
	Q_ASSERT_X(nPrimaryKeyColumn!=-1,"SIMPLE_ORM", "Primary key column not found");
	return nPrimaryKeyColumn;
}




/*!
Unlock records by resource ID
\param pStatus,				- returns erros
\param pLockedResourceID	- locked res. id
\return bool				- FALSE if pStatus has errors
*/
bool BusinessLocker::UnlockByResourceID(Status& pStatus, QString& pLockedResourceID)
{
	DbSqlQuery query(pStatus,m_pDbManager);
	query.BeginTransaction(pStatus);
	if(!pStatus.IsOK())return false;

	query.Prepare(pStatus, "DELETE FROM CORE_LOCKING WHERE CORL_RESOURCE_ID = ?");
	query.bindValue(0, pLockedResourceID);

	// If some error came out
	if (!query.ExecutePrepared(pStatus))
	{
		pStatus.setError(StatusCodeSet::ERR_SQL_LOCKING_FAIL, 
			"Error trying to delete from CORL_TABLE values for RESOURCE_ID " + pLockedResourceID);
		query.Rollback();
		return false;
	}

	// If there was nothing to unlock or everything passed just fine, smile.
	pStatus.setError(StatusCodeSet::ERR_NONE);
	query.Commit(pStatus);
	return true;
}

/*!
	Unlock records by resource ID
	\param pStatus,				- returns error
	\param nTableID				- table id
	\param nRecordID			- record id
	\return bool				- FALSE if pStatus has errors
*/
bool BusinessLocker::UnlockByRecordID(Status& pStatus, int nTableID, int nRecordID)
{
	DbSqlQuery query(pStatus,m_pDbManager);
	query.BeginTransaction(pStatus);
	if(!pStatus.IsOK())return false;

	query.Prepare(pStatus, "DELETE FROM CORE_LOCKING WHERE CORL_RECORD_ID = ? AND CORL_TABLE_ID =?");
	query.bindValue(0, nRecordID);
	query.bindValue(1, nTableID);

	// If some error came out
	if (!query.ExecutePrepared(pStatus))
	{
		pStatus.setError(StatusCodeSet::ERR_SQL_LOCKING_FAIL, QString("Error trying to delete from CORL_TABLE values for record id %1 and table id %2").arg(nRecordID).arg(nTableID));
		query.Rollback();
		return false;
	}

	// If there was nothing to unlock or everything passed just fine, smile.
	pStatus.setError(StatusCodeSet::ERR_NONE);
	query.Commit(pStatus);
	return true;
}



/*!
Unlock records by session ID
\param pStatus,				- returns erros
\param pUserSessionID		- user session
\return bool				- FALSE if pStatus has errors
*/
bool BusinessLocker::UnlockByUserSessionID(Status& pStatus, QString& pUserSessionID)
{
	DbSqlQuery query(pStatus,m_pDbManager);
	query.BeginTransaction(pStatus);
	if(!pStatus.IsOK())return false;
	query.Prepare(pStatus, "DELETE FROM CORE_LOCKING WHERE CORL_SESSION_ID = ?");
	query.bindValue(0	, pUserSessionID);

	// If some error came out
	if (!query.ExecutePrepared(pStatus))
	{
		pStatus.setError(StatusCodeSet::ERR_SQL_LOCKING_FAIL, 
			"Error trying to delete from CORL_TABLE values for SESSION_ID " + pUserSessionID);
		query.Rollback();
		return false;
	}

	// If there was nothing to unlock or everything passed just fine, smile.
	pStatus.setError(StatusCodeSet::ERR_NONE);
	query.Commit(pStatus);
	return true;
}


/*!
Checks if records are locked

\param pStatus		- return error if one is locked
\param TableID		- table id
\param LockList		- list of ID records for locking
\return bool		- FALSE if status=error
*/

bool BusinessLocker::IsLocked(Status& pStatus, int TableID, DbLockList &LockList)
{

	if (!m_bCheckParents || DbSqlTableDefinition::GetParentID(TableID) == -1)
		return IsLockedInternal(pStatus, TableID, LockList);
	else
	{
		DbLockList tmp=GetParentLockList(pStatus, TableID, LockList);
		return IsLockedInternal(pStatus, DbSqlTableDefinition::GetParentID(TableID),tmp);
	}
}
/*!
	Checks if records are locked

	\param pStatus		- return error if one is locked
	\param TableID		- table id
	\param LockList		- list of ID records for locking
	\return int			- 0=not locked, 1- locked by me, 2- locked by other
*/

int BusinessLocker::IsLockedByMe(Status& pStatus, int TableID, DbLockList &LockList)
{
	bool bOK=IsLocked(pStatus,TableID, LockList);
	if (!pStatus.IsOK() || !bOK) //if error or not locked
		return 0;


	QString strLockSession=pStatus.getErrorTextRaw();
	if(strLockSession==g_UserSessionManager->GetSessionID())
		return 1;
	else
		return 2;
	
}

bool BusinessLocker::LockRecordsInternal(Status& pStatus, int TableID, DbLockList &LockList,QString& strLockedResourceID, DbRecordSet *pLstStatusRows,bool bAllOrNothing)
{
	if (!LockList.count())
	{
		pStatus.setError(StatusCodeSet::ERR_NONE);
		return true;
	}

	//reset error:
	pStatus.setError(StatusCodeSet::ERR_NONE);
	FormatStatusRowsLst(pLstStatusRows);//redefine status list

	//only if allornothing
	if(bAllOrNothing)
	{ //test if already locked:
		if(IsLocked(pStatus,TableID,LockList)) return false;
		if(!pStatus.IsOK())return false;
	}

	//locked resource refresh
	strLockedResourceID =GenerateResourceID();

	//begin transaction (only when all or nothing, otherwise lock what u can!
	DbSqlQuery query(pStatus,m_pDbManager); 
	if(bAllOrNothing)query.BeginTransaction(pStatus);
	if(!pStatus.IsOK())return false;

	query.Prepare(pStatus, "INSERT INTO CORE_LOCKING (CORL_RECORD_ID, CORL_RESOURCE_ID, CORL_SESSION_ID, CORL_TABLE_ID,CORL_USER_SESSION_ID) VALUES (?, ?, ?, ?,?)");

	for (int i = 0; i < LockList.size(); ++i)
	{
		query.bindValue(0, LockList.at(i));
		query.bindValue(1, strLockedResourceID);
		query.bindValue(2, m_strUserSession);
		query.bindValue(3, TableID);
		query.bindValue(4, m_intSessionRecordID);

		if (!query.ExecutePrepared(pStatus))
		{
			if(bAllOrNothing)
			{
				query.Rollback();
				return false;
			}
			else
			{
				//add as error row in returned list
				if(pLstStatusRows!=NULL)
				{
					pLstStatusRows->addRow();
					pLstStatusRows->setData(pLstStatusRows->getRowCount()-1,"ID",LockList.at(i));
					pLstStatusRows->setData(pLstStatusRows->getRowCount()-1,"STATUS_CODE",StatusCodeSet::ERR_SQL_LOCKING_FAIL);
					pLstStatusRows->setData(pLstStatusRows->getRowCount()-1,"STATUS_TEXT","");
				}
			}

		}
	}

	if(bAllOrNothing)query.Commit(pStatus);
	return true;
}



DbLockList BusinessLocker::GetParentLockList(Status& pStatus, int TableID, DbLockList &LockList)
{
	QString QryStr = DbSqlTableDefinition::GetParentSqlRecordLockString(TableID);

	DbLockList ParentLockList;

	int nSize=LockList.size();
	if (nSize==0)
		return ParentLockList;

	QryStr += " (";
	//Construct query string from lock list.
	for (int i = 0; i < nSize; ++i)
	{
		QryStr += QVariant(LockList.at(i)).toString();
		QryStr += ",";
	}
	QryStr.truncate(QryStr.size()-1);
	QryStr += ") ";

	DbSqlQuery query(pStatus, m_pDbManager);

	query.Execute(pStatus, QryStr);

	//If there is something returned, then it is locked.
	
	while (query.next())
	{
		if (!ParentLockList.contains(query.value(0).toInt()))
			ParentLockList.append(query.value(0).toInt());
	}

	return ParentLockList;
}


//test lock per user session!
bool BusinessLocker::IsLockedInternal(Status& pStatus, int TableID, DbLockList &LockList)
{
	DbSqlQuery query(pStatus, m_pDbManager);

	QString strSqlStart = "SELECT CORL_RECORD_ID,CORL_SESSION_ID FROM CORE_LOCKING WHERE CORL_TABLE_ID = ";
	strSqlStart += QVariant(TableID).toString();
	strSqlStart += " AND CORL_RECORD_ID IN (";

	int nChunk = 200;	//chunks of 200 max
	QString strWhere,strSQL;

	int nSize=LockList.size();
	for(int i=0;i<nSize;++i)
	{
		strWhere+= QVariant(LockList.at(i)).toString()+",";

		//execute chunk:
		if(i>nChunk||i==nSize-1)
		{
			strWhere.chop(1);
			strSQL=strSqlStart+strWhere+QString(") ");	
			nChunk+=200; //next 200
			if(!query.Execute(pStatus,strSQL))
			{
				return false;							
			}
			else
			{
				if (query.next())
				{
					int nCount=query.value(0).toInt();
					if (nCount>0)
					{		
						pStatus.setError(StatusCodeSet::ERR_SQL_ALREADY_LOCKED,query.value(1).toString()); //as text return session id of locked resource (first)
						return true;
					}
				}
			}
			strWhere="";
		}
	}



	return false;
}




void BusinessLocker::FormatStatusRowsLst(DbRecordSet *pLstStatusRows)
{
	if (pLstStatusRows == NULL)
		return;

	pLstStatusRows->destroy();
	pLstStatusRows->addColumn(QVariant::Int, QString("ID"));
	pLstStatusRows->addColumn(QVariant::Int, QString("STATUS_CODE"));
	pLstStatusRows->addColumn(QVariant::String, QString("STATUS_TEXT"));
}



//generate unique resource id:
QString BusinessLocker::GenerateResourceID()
{
	//locked resource refresh
	m_nUniqueLockKey++;
	return m_strUserSession + "_" + QVariant((quint64)m_nUniqueLockKey).toString();

}

