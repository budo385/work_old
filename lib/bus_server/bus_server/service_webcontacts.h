#ifndef SERVICE_WEBCONTACTS_H
#define SERVICE_WEBCONTACTS_H

#include "bus_interface/bus_interface/interface_webcontacts.h"
#include "businessservice.h"

class Service_WebContacts : public Interface_WebContacts, public BusinessService
{
public:
	//parent ops:
	void ReadContacts(Status &Ret_pStatus, DbRecordSet &Ret_Contacts); 
	void ReadContactsWithFilter(Status &Ret_pStatus, int &Ret_nTotalCount,DbRecordSet &Ret_Contacts, int nFilter=-1,int nFromN=-1, int nToN=-1, QString strFromLetter="", QString strToLetter=""); 
	void ReadContactDetails(Status &Ret_pStatus, int nContactID, DbRecordSet &Ret_ContactData,DbRecordSet &Ret_Emails,DbRecordSet &Ret_WebSites,DbRecordSet &Ret_Phones,DbRecordSet &Ret_Sms,DbRecordSet &Ret_Sykpe,DbRecordSet &Ret_Addresses); //GET ONE: fav details+addresses
	//void ModifyContact(Status &Ret_pStatus, DbRecordSet &Ret_ContactData){}; //EDIT ONE	
	//void AddContact(Status &Ret_pStatus, DbRecordSet &ContactData, int &Ret_nNewContactID){}; //ADD ONE	
	//void DeleteContact(Status &Ret_pStatus, int nContactID){}; //DEL ONE	

	//child ops:
	void SearchContacts(Status &Ret_pStatus, int &Ret_nTotalCount, DbRecordSet &Ret_Contacts,int nSearchType, QString strField1_Name, QString strField1_Value, QString strField2_Name, QString strField2_Value, int nGroupID=-1,int nFromN=-1, int nToN=-1, QString strFromLetter="", QString strToLetter="");
	void ReadFavorites(Status &Ret_pStatus, DbRecordSet &Ret_Favorites);
	void ReadContactAddresses(Status &Ret_pStatus, int nContactID, DbRecordSet &Ret_Emails,DbRecordSet &Ret_WebSites,DbRecordSet &Ret_Phones,DbRecordSet &Ret_Sms,DbRecordSet &Ret_Sykpe,DbRecordSet &Ret_Addresses);

	//documents/emails:
	void ReadContactDocuments(Status &Ret_pStatus, int &Ret_nTotalCount,DbRecordSet &Ret_Documents,  int nContactID,int nDocType=-1, int nFromN=-1, int nToN=-1);
	void ReadContactEmails(Status &Ret_pStatus, int &Ret_nTotalCount, DbRecordSet &Ret_Emails, int nContactID, int nFromN=-1, int nToN=-1);
	
	//ce entity
	void ReadCommEntityCount(Status &Ret_pStatus, int nContactID, int &Ret_nFileDocCount, int &Ret_nWebSiteCount, int &Ret_nNotesCount,int &Ret_nEmailCount);


};

#endif // SERVICE_WEBCONTACTS_H
