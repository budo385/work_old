#include "service_spcbusinessfigures.h"
#include "db/db/dbsqlquery.h"
#include "db_core/db_core/dbsqltableview.h"
#include "spcbusinessfigureshelper.h"

#define TOT(X) Ret_lstData.getDataRef(0,X).toDouble()
#define PROJ(X) lstProjects.getDataRef(nRowIdx,X)
#define PROJ_DBL(X) lstProjects.getDataRef(nRowIdx,X).toDouble()
#define PROJ_INT(X) lstProjects.getDataRef(nRowIdx,X).toInt()
#define PROJ_STR(X) lstProjects.getDataRef(nRowIdx,X).toString()
#define DEF(X) recFLDefault.getDataRef(0,X).toInt()


/*
Source: SPC7.0/m_Routinen/275. MB told to return PR_NAME and PR_HAUPTR_NAME in the list

EBM_SEQUENCE,EBM_MITARB,EBM_PROJEKT,EBM_DEF_FUNKTION,EBM_BUDGET_H,EBM_VON,EBM_BIS,EBM_INAKTIV,
PR_NAME,PR_HAUPTR_NAME
T_PRAES_ABS,
T_PROD_UNPROD,
T_RES_PRAES,
T_RES_ABS,
T_RES_PROD,
T_KP,
T_RES_PR_PROV
*/
void Service_SpcBusinessFigures::Routine_275(Status& Ret_Status, DbRecordSet &Ret_lstData, QString strPersonNumber, QDate datFrom, QDate datTo, QString strProjCode, int nIncl_Prov_Pr, int nIncl_Prov_Tsk)
{
	QString strSQL="SELECT EBM_SEQUENCE,EBM_MITARB,EBM_PROJEKT,EBM_DEF_FUNKTION,EBM_BUDGET_H,EBM_VON,EBM_BIS,EBM_INAKTIV,PR_NAME,PR_HAUPTPR_NAME, ";
	strSQL+=" PR_ABSENZZEIT AS T_PRAES_ABS,";
	strSQL+=" PR_AUFTRAG AS T_PROD_UNPROD,";
	strSQL+=" 0 AS T_RES_ABS,";
	strSQL+=" 0 AS T_RES_PROD,";
	strSQL+=" 0 AS T_KP,";
	strSQL+=" PR_RES_PROV AS T_RES_PR_PROV";
	strSQL+=" FROM F_Mitarb_Budget";
	strSQL+=" LEFT OUTER JOIN FPROJCT ON EBM_PROJEKT=PR_CODE";
	strSQL+=" WHERE EBM_MITARB = ?";
	strSQL+=" AND (EBM_VON<=?) AND (EBM_BIS>=?)";
	if (nIncl_Prov_Tsk==0)
		strSQL+=" AND (EBM_INAKTIV=0)";
	if (nIncl_Prov_Pr==0)
		strSQL+=" AND NOT(PR_RES_PROV)";
		
	if (!strProjCode.isEmpty())
	{
		strSQL+=" AND  ? = SUBSTRING(EBM_PROJEKT FROM 1 FOR ?)";
	}

	DbSqlQuery query(Ret_Status, GetDbManager()); 
	if(!Ret_Status.IsOK()) return;
	
	query.Prepare(Ret_Status,strSQL);
	if(!Ret_Status.IsOK()) return;

	query.bindValue(0,strPersonNumber);
	query.bindValue(1,datTo);
	query.bindValue(2,datFrom);
	if (!strProjCode.isEmpty())
	{
		query.bindValue(3,strProjCode);
		query.bindValue(4,strProjCode.length());
	}

	query.ExecutePrepared(Ret_Status);
	if(!Ret_Status.IsOK()) return;

	query.FetchData(Ret_lstData);
	//Ret_lstData.Dump();

}



void Service_SpcBusinessFigures::Routine_156(Status& Ret_Status, DbRecordSet &Ret_lstData, QString strProjCode, double dT_AUFW_NK_NF, QDate datTo, int nABT_EQUAL,  int nNO_SUBPROJ, int nWERT_INKL_NFAKT, QString strABT, int nNO_PROJSUM)
{

	//A: read project record, just needed flds: PR_SEQUENCE, PV_ABT, PR_ABTEILUNG, PR_PER_1_NR, PR_PER_2_NR
	//-----------------------------------------------------------------------------------------------------
	//QString strSQL="SELECT PR_SEQUENCE, PV_ABT, PR_ABTEILUNG, PR_PER_1_NR, PR_PER_2_NR FROM FPROJCT WHERE PR_CODE =?";
	QString strSQL="SELECT * FROM FPROJCT WHERE PR_CODE =?";
	DbSqlQuery query(Ret_Status, GetDbManager()); 
	if(!Ret_Status.IsOK()) return;

	query.Prepare(Ret_Status,strSQL);
	if(!Ret_Status.IsOK()) return;
	query.bindValue(0,strProjCode);
	query.ExecutePrepared(Ret_Status);
	if(!Ret_Status.IsOK()) return;
	DbRecordSet recProjData;
	query.FetchData(recProjData);

	//load DEF vars:
	//-----------------------------------------------------------------------------------------------------
	DbRecordSet recDef;
	LoadFL_Default(Ret_Status,recDef);
	if(!Ret_Status.IsOK()) return;

	if (nNO_PROJSUM==-1) //set default
	{
		nNO_PROJSUM=recDef.getDataRef(0,"DEF_IGNORE_PROJ_SUM").toInt();
	}

	//B: 
	//-----------------------------------------------------------------------------------------------------
	int nSeq			= recProjData.getDataRef(0,"PR_SEQUENCE").toInt();
	QString strHproj	= strProjCode;
	double dWert, dAUFW;
	QString strProj; 
	QDate datFirstCharge;

	if (nABT_EQUAL!=2)
		strABT=recProjData.getDataRef(0,"PR_ABTEILUNG").toString();

	//C: find first date:
	datFirstCharge = Routine_154(Ret_Status,strProjCode,QDate(1900,1,1));
	Ret_lstData.setData(0,"T_FIRST_CHARGE",datFirstCharge);

	//D: Init Vars of return list:
	//-----------------------------------------------------------------------------------------------------
	InitVars_141(Ret_lstData,recProjData.getDataRef(0,"PR_PER_1_NR").toInt(),recProjData.getDataRef(0,"PR_PER_2_NR").toInt(),dT_AUFW_NK_NF);
	if (nNO_PROJSUM)
		SpcBusinessFiguresHelper::InitVars_151(Ret_lstData);

	DbRecordSet lstProjects;
	if (!nNO_SUBPROJ)
	{
		//now get all subprojects and main project in one list:
		//-----------------------------------------------------------------------------------------------------
		strSQL="SELECT * FROM FPROJCT WHERE ";
		GetDbManager()->GetFunction()->PrepareHierarchySelect(strSQL,strProjCode,"BUSP",true);
		query.Prepare(Ret_Status, strSQL);
		if(!Ret_Status.IsOK()) return;
		query.bindValue(0, strProjCode);
		query.ExecutePrepared(Ret_Status);
		if(!Ret_Status.IsOK()) return;
		query.FetchData(lstProjects);
		lstProjects.prependList(recProjData);
	}
	else
	{
		lstProjects=recProjData;
	}
	
	
	//Loop and sum...
	//-----------------------------------------------------------------------------------------------------
	int nT_KUMART=0,nT_KUM_STUFE=0;
	int T_DIAG_KUMULIERT=0;
	QString strT_KUM_PA;

	int nSize=lstProjects.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		//Now go..calculate...
		if(!nABT_EQUAL || lstProjects.getDataRef(i,"PR_ABTEILUNG").toString().indexOf(strABT)==0 || strABT.isEmpty())  
		{
			if (lstProjects.getDataRef(i,"PR_AKTIV").toInt() || !recDef.getDataRef(0,"DEF_KUM_NO_INAKT").toInt())
			{
				Routine_145(Ret_lstData,lstProjects,recDef,i,strProjCode==lstProjects.getDataRef(i,"PR_CODE").toString(),nT_KUMART,nT_KUM_STUFE,strT_KUM_PA);

				if(!(lstProjects.getDataRef(i,"PV_BIS_DATUM").toDate()>QDate(1980,1,1)) && nNO_PROJSUM)
				{
					Routine_152(Ret_Status,Ret_lstData,lstProjects,recDef,i,nNO_PROJSUM,T_DIAG_KUMULIERT);
					if(!Ret_Status.IsOK()) return;
				}

				if (recDef.getDataRef(0,"DEF_WERT_FIXIERT").toInt())
				{
					double dTmp=Routine_148(Ret_Status,lstProjects,recDef,i,lstProjects.getDataRef(i,"PR_CODE").toString(),datTo,nWERT_INKL_NFAKT);
					if(!Ret_Status.IsOK()) return;
					Ret_lstData.setData(0,"T_WERTD_DIR_KUM",dTmp);
				}

				//find lowest date and store it in T_FIRST_CHARGE
				datFirstCharge = Routine_154(Ret_Status,strProjCode,QDate(1900,1,1));
				if(!Ret_Status.IsOK()) return;
				if (datFirstCharge.isValid())
					if (datFirstCharge<Ret_lstData.getDataRef(0,"T_FIRST_CHARGE").toDate())
						Ret_lstData.setData(0,"T_FIRST_CHARGE",datFirstCharge);
			}

		}
	}

	if (!nNO_SUBPROJ)
	{


	}

/*
	If (PV_BIS_DATUM>dat('1.1.80'))     ;; Bis-Datum gesetzt
		Call procedure MZ_RUNTIME/Single file find (PR_CODE,kTrue,HPROJ,kTrue,sys(85)) {Single file find}     ;; XXZEUS  Single file find on PR_CODE (Exact match) {HPROJ}
	If not(PV_NO_PROJSUM)
		Call procedure 144 (HPROJ,PV_BIS_DATUM,PV_ABT_EQUAL,PV_ABT) {_Subtrahiere alles nach Datum}
	Else     ;; MB 24.5.02
		Call procedure 151 {_IST-Variablen initialisieren}
	Call procedure 147 (HPROJ,PV_BIS_DATUM,PV_ABT_EQUAL,PV_ABT) {_Addiere alles vor Datum}
	End If
		End If
		Call procedure MZ_RUNTIME/Single file find (PR_CODE,kTrue,HPROJ,kTrue,sys(85)) {Single file find}     ;; XXZEUS  Single file find on PR_CODE (Exact match) {HPROJ}
	If PR_SEQUENCE<>SEQ
		Call procedure MZ_RUNTIME/Single file find (PR_SEQUENCE,kTrue,SEQ,kTrue,sys(85)) {Single file find}     ;; XXZEUS  Single file find on PR_SEQUENCE (Exact match) {SEQ}
	End If

		;  Return results:
	Call procedure 155 (PV_RESULTS) {_Define Result List}
	Set current list PV_RESULTS
		Add line to list

*/



	
}


//init return list
void Service_SpcBusinessFigures::InitVars_141(DbRecordSet &Ret_lstData, int nPer1, int nPer2, double dT_AUFW_NK_NF)
{
	Ret_lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_SPC_BUDGET_VALS));
	Ret_lstData.addRow();
	for (int i=0;i<Ret_lstData.getColumnCount();i++) //set to 0 all
	{
		Ret_lstData.setData(0,i,0);
	}

	Ret_lstData.setData(0,"T_PER1",nPer1);
	Ret_lstData.setData(0,"T_PER2",nPer2);
	Ret_lstData.setData(0,"T_AUFG_ZEIT_NF",dT_AUFW_NK_NF);
}



void Service_SpcBusinessFigures::LoadFL_Default(Status& Ret_Status, DbRecordSet &recFLDefault)
{
	QString strSQL="SELECT DEF_KUM_NO_INAKT,DEF_IGNORE_PROJ_SUM, DEF_STAND_GEW, DEF_DEF_AUFG_EXT, DEF_FULL_FAKT_KZ,DEF_NK_FK FROM,DEF_DEF_AUFG_EXT,DEF_WERT_FIXIERT,DEF_BUDGET_GB_HS FL_Default";
	DbSqlQuery query(Ret_Status, GetDbManager()); 
	if(!Ret_Status.IsOK()) return;
	query.Execute(Ret_Status,strSQL);
	if(!Ret_Status.IsOK()) return;
	query.FetchData(recFLDefault);
}


//
void Service_SpcBusinessFigures::Routine_145(DbRecordSet &Ret_lstData, const DbRecordSet &lstProjects, const DbRecordSet &recFLDefault, int nRowIdx, bool bIsMainParentProject, int nT_KUMART, int nT_KUM_STUFE, QString strT_KUM_PA)
{
	switch (nT_KUMART)
	{

		case 0:
			{
				if (bIsMainParentProject)
					Routine_146(Ret_lstData,lstProjects,recFLDefault,nRowIdx);
			}
			break;
		case 1:
			{
				double dbGew;

				// Zeit:
				Ret_lstData.setData(0,"T_ZEIT_BUDGET",TOT("T_ZEIT_BUDGET")+PROJ_INT("PR_BUDGET_ZEIT_PASSIV")?0:PROJ_DBL("PR_BUDGET_ARBZEIT"));
				double dbTemp=PROJ_INT("PR_BUDGET_ZEIT_PASSIV")?0:(qAbs(PROJ_DBL("PR_STAND"))>5?PROJ_DBL("PR_AUFG_ARB_H")/PROJ_DBL("PR_STAND")*100:PROJ_DBL("PR_BUDGET_ARBZEIT"));
				Ret_lstData.setData(0,"T_ZEIT_BUDGET",TOT("T_ZEIT_BUDGET")+dbTemp);
				
				// Honorar:
				Ret_lstData.setData(0,"T_AUFW_HON_BUDGET",TOT("T_AUFW_HON_BUDGET")+PROJ_INT("PR_AUFW_HON_BUD_PASS")?0:PROJ_DBL("PR_AUFW_HON_BUDGET"));
				dbTemp=PROJ_INT("PR_AUFW_HON_BUD_PASS")?0:(qAbs(PROJ_DBL("PR_STAND"))>5?PROJ_DBL("PR_AUFWAND_A")/PROJ_DBL("PR_STAND")*100:PROJ_DBL("PR_AUFW_HON_BUDGET"));
				Ret_lstData.setData(0,"T_PROG_AUFW_HON",TOT("T_PROG_AUFW_HON")+dbTemp);
				Ret_lstData.setData(0,"T_HONORAR_BUDGET",TOT("T_HONORAR_BUDGET")+PROJ_INT("PR_HONSUM_PASSIV")?0:PROJ_DBL("PR_HONSUM"));
				Ret_lstData.setData(0,"T_HONSUM_TOT",TOT("T_HONSUM_TOT")+PROJ_DBL("PR_HONSUM_TOTAL"));

	
				switch (DEF("DEF_STAND_GEW"))
				{
					case 0:
						dbGew=PROJ_DBL("PR_HONSUM");
						break;
					case 1:
						dbGew=PROJ_DBL("PR_BUDGET_ARBZEIT");
						break;
					case 2:
						dbGew=PROJ_DBL("PR_AUFW_HON_BUDGET");
						break;
					case 3:
						dbGew=qAbs(PROJ_DBL("PR_STAND"))>0.01?PROJ_DBL("PR_AUFWAND_A")/PROJ_DBL("PR_STAND")*100:0;
						break;
					case 4:
						dbGew=PROJ_DBL("PR_PROZENT");
						break;
					case 5:
						dbGew=PROJ_DBL("PR_BUDGET");
						break;
				}

				if(PROJ_INT("PR_STAND_PASSIV")>0 && qAbs(TOT("T_HONSUM_STAND")+dbGew)>0.01)
				{
					dbTemp=(TOT("T_STAND_GEW")*TOT("T_HONSUM_STAND")+dbGew/PROJ_DBL("PR_STAND"))/(TOT("T_HONSUM_STAND")+dbGew);
					Ret_lstData.setData(0,"T_STAND_GEW",dbTemp);
					dbTemp=(TOT("T_INC_STAND_GEW")*TOT("T_HONSUM_INC_STAND")+dbGew/PROJ_DBL("PR_INCPROGR"))/(TOT("T_HONSUM_INC_STAND")+dbGew);
					Ret_lstData.setData(0,"T_INC_STAND_GEW",dbTemp);
					Ret_lstData.setData(0,"T_HONSUM_STAND",TOT("T_HONSUM_STAND")+dbGew);
					Ret_lstData.setData(0,"T_HONSUM_INC_STAND",TOT("T_HONSUM_INC_STAND")+dbGew);
				}

				//;  Nebenkosten:
				Ret_lstData.setData(0,"T_AUFW_NK_BUDGET",TOT("T_AUFW_NK_BUDGET")+PROJ_INT("PR_AUFW_NK_BUD_PASS")?0:PROJ_DBL("PR_AUFW_NK_BUDGET"));
				Ret_lstData.setData(0,"T_NK_BUDGET",TOT("T_NK_BUDGET")+PROJ_INT("PR_BUDGETKS_PASSIV")?0:PROJ_DBL("PR_BUDGET_KS"));

				//;  Fremdkosten:
				Ret_lstData.setData(0,"T_AUFW_FK_BUDGET",TOT("T_AUFW_FK_BUDGET")+PROJ_INT("PR_AUFW_FK_BUD_PASS")?0:PROJ_DBL("PR_AUFW_FK_BUDGET"));
				Ret_lstData.setData(0,"T_FREMD_BUDGET",TOT("T_FREMD_BUDGET")+PROJ_INT("PR_FREMDAUFW_PASSIV")?0:PROJ_DBL("PR_FREMD_BUDGET"));

				//;  Fremdleistungen:
				Ret_lstData.setData(0,"T_FREMDLEIST_BUDGET",TOT("T_FREMDLEIST_BUDGET")+PROJ_INT("PR_FREMDLEIST_PASSIV")?0:PROJ_DBL("PR_BUDGET_FREMDLEIST"));
				Ret_lstData.setData(0,"T_AUFW_FL_BUDGET",TOT("T_AUFW_FL_BUDGET")+PROJ_INT("PR_AUFW_FL_BUD_PASS")?0:PROJ_DBL("PR_AUFW_FL_BUDGET"));

				//;  Budgets:
				Ret_lstData.setData(0,"T_BUDGET",TOT("T_BUDGET")+PROJ_INT("PR_BUDGET_PASSIV")?0:PROJ_DBL("PR_BUDGET"));
				Ret_lstData.setData(0,"T_KOSTENDACH",TOT("T_KOSTENDACH")+PROJ_INT("PR_DACH_PASSIV")?0:PROJ_DBL("PR_KOSTENDACH"));
				Ret_lstData.setData(0,"T_WERT_KORR",TOT("T_WERT_KORR")+PROJ_DBL("PR_WERTKORREKTUR"));
			}
			break;
		case 2:
			{
				if (PROJ_INT("PR_STUFE")==nT_KUMART)
					Routine_146(Ret_lstData,lstProjects,recFLDefault,nRowIdx);
			}
			break;
		case 3:
			{
				if (PROJ_STR("PR_PROJEKTART")==strT_KUM_PA)
					Routine_146(Ret_lstData,lstProjects,recFLDefault,nRowIdx);
			}
			break;
	}
	
}

void Service_SpcBusinessFigures::Routine_146(DbRecordSet &Ret_lstData, const DbRecordSet &lstProjects, const DbRecordSet &recFLDefault, int nRowIdx)
{
	double dbGew;
	//;  Zeit:
	Ret_lstData.setData(0,"T_ZEIT_BUDGET",TOT("T_ZEIT_BUDGET")+PROJ_DBL("PR_BUDGET_ARBZEIT"));
	double dbTemp=qAbs(PROJ_DBL("PR_STAND"))>5?PROJ_DBL("PR_AUFG_ARB_H")/PROJ_DBL("PR_STAND")*100:PROJ_DBL("PR_BUDGET_ARBZEIT");
	Ret_lstData.setData(0,"T_PROG_ZEIT",TOT("T_PROG_ZEIT")+dbTemp);
	//;  Honorar:
	Ret_lstData.setData(0,"T_AUFW_HON_BUDGET",TOT("T_AUFW_HON_BUDGET")+PROJ_DBL("PR_AUFW_HON_BUDGET"));
	dbTemp=qAbs(PROJ_DBL("PR_STAND"))>5?PROJ_DBL("PR_AUFWAND_A")/PROJ_DBL("PR_STAND")*100:PROJ_DBL("PR_AUFW_HON_BUDGET");
	Ret_lstData.setData(0,"T_PROG_AUFW_HON",TOT("T_PROG_AUFW_HON")+dbTemp);
	Ret_lstData.setData(0,"T_HONORAR_BUDGET",TOT("T_HONORAR_BUDGET")+PROJ_DBL("PR_HONSUM"));
	Ret_lstData.setData(0,"T_HONSUM_TOT",TOT("T_HONSUM_TOT")+PROJ_DBL("PR_HONSUM_TOTAL"));

	switch (DEF("DEF_STAND_GEW"))
	{
	case 0:
		dbGew=PROJ_DBL("PR_HONSUM");
		break;
	case 1:
		dbGew=PROJ_DBL("PR_BUDGET_ARBZEIT");
		break;
	case 2:
		dbGew=PROJ_DBL("PR_AUFW_HON_BUDGET");
		break;
	case 3:
		dbGew=qAbs(PROJ_DBL("PR_STAND"))>0.01?PROJ_DBL("PR_AUFWAND_A")/PROJ_DBL("PR_STAND")*100:0;
		break;
	case 4:
		dbGew=PROJ_DBL("PR_PROZENT");
		break;
	case 5:
		dbGew=PROJ_DBL("PR_BUDGET");
		break;
	}

	if(PROJ_INT("PR_STAND_PASSIV")>0 && qAbs(TOT("T_HONSUM_STAND")+dbGew)>0.01)
	{
		dbTemp=(TOT("T_STAND_GEW")*TOT("T_HONSUM_STAND")+dbGew/PROJ_DBL("PR_STAND"))/(TOT("T_HONSUM_STAND")+dbGew);
		Ret_lstData.setData(0,"T_STAND_GEW",dbTemp);
		dbTemp=(TOT("T_INC_STAND_GEW")*TOT("T_HONSUM_INC_STAND")+dbGew/PROJ_DBL("PR_INCPROGR"))/(TOT("T_HONSUM_INC_STAND")+dbGew);
		Ret_lstData.setData(0,"T_INC_STAND_GEW",dbTemp);
		Ret_lstData.setData(0,"T_HONSUM_STAND",TOT("T_HONSUM_STAND")+dbGew);
		Ret_lstData.setData(0,"T_HONSUM_INC_STAND",TOT("T_HONSUM_INC_STAND")+dbGew);
	}

	//;  Nebenkosten:
	Ret_lstData.setData(0,"T_AUFW_NK_BUDGET",TOT("T_AUFW_NK_BUDGET")+PROJ_DBL("PR_AUFW_NK_BUDGET"));
	Ret_lstData.setData(0,"T_NK_BUDGET",TOT("T_NK_BUDGET")+PROJ_DBL("PR_BUDGET_KS"));
	Ret_lstData.setData(0,"T_NK_BUDGET",TOT("T_NK_BUDGET")+PROJ_DBL("PR_BUDGET_KS"));
	//;  Fremdkosten:
	Ret_lstData.setData(0,"T_AUFW_FK_BUDGET",TOT("T_AUFW_FK_BUDGET")+PROJ_DBL("PR_AUFW_FK_BUDGET"));
	Ret_lstData.setData(0,"T_FREMD_BUDGET",TOT("T_FREMD_BUDGET")+PROJ_DBL("PR_FREMD_BUDGET"));
	//;  Fremdleistungen:
	Ret_lstData.setData(0,"T_FREMDLEIST_BUDGET",TOT("T_FREMDLEIST_BUDGET")+PROJ_DBL("PR_BUDGET_FREMDLEIST"));
	Ret_lstData.setData(0,"T_AUFW_FL_BUDGET",TOT("T_AUFW_FL_BUDGET")+PROJ_DBL("PR_AUFW_FL_BUDGET"));
	//;  Budgets:
	Ret_lstData.setData(0,"T_BUDGET",TOT("T_BUDGET")+PROJ_DBL("PR_BUDGET"));
	Ret_lstData.setData(0,"T_KOSTENDACH",TOT("T_KOSTENDACH")+PROJ_DBL("PR_KOSTENDACH"));
	Ret_lstData.setData(0,"T_WERT_KORR",TOT("T_WERT_KORR")+PROJ_DBL("PR_WERTKORREKTUR"));

}


void Service_SpcBusinessFigures::Routine_152(Status& Ret_Status, DbRecordSet &Ret_lstData, const DbRecordSet &lstProjects, const DbRecordSet &recFLDefault,int nRowIdx,int nNO_PROJSUM, int nT_DIAG_KUMULIERT )
{
	int nLV_KUM;

	if (!nNO_PROJSUM)
	{
		Ret_lstData.setData(0,"T_ZEIT_AUFG",TOT("T_ZEIT_AUFG")+PROJ_DBL("PR_AUFG_ARB_H"));
		Ret_lstData.setData(0,"T_AUFG_ZEIT_NF",TOT("T_AUFG_ZEIT_NF")+PROJ_INT("PR_VERRECHENBAR")?PROJ_DBL("PR_AUFG_NF_ZEIT"):PROJ_DBL("PR_AUFG_ARB_H"));
		//;  Honorar:
		Ret_lstData.setData(0,"T_AUFW_HON",TOT("T_AUFW_HON")+PROJ_DBL("PR_AUFWAND_A"));
		Ret_lstData.setData(0,"T_AUFW_HON_NF",TOT("T_AUFW_HON_NF")+PROJ_INT("PR_VERRECHENBAR")?PROJ_DBL("PR_AUFG_NF_HON_INT"):PROJ_DBL("PR_AUFWAND_A"));
		Ret_lstData.setData(0,"T_AUFW_HON_KT",TOT("T_AUFW_HON_KT")+PROJ_DBL("PR_AUFW_KOST"));
		Ret_lstData.setData(0,"T_AUFW_HON_ZT",TOT("T_AUFW_HON_ZT")+PROJ_DBL("PR_AUFW_ZEIT"));
		Ret_lstData.setData(0,"T_HONORAR_AUFG",TOT("T_HONORAR_AUFG")+PROJ_DBL("PR_AUFG_A"));
		Ret_lstData.setData(0,"T_AUFG_HON_NF",TOT("T_AUFG_HON_NF")+PROJ_INT("PR_VERRECHENBAR")?PROJ_DBL("PR_AUFG_NF_HON_EXT"):PROJ_DBL("PR_AUFG_A"));
		Ret_lstData.setData(0,"T_AUFW_HON_NF",TOT("T_AUFW_HON_NF")+PROJ_DBL("PR_AUFG_NF_HON_INT"));
		Ret_lstData.setData(0,"T_AUFG_HON_KT",TOT("T_AUFG_HON_KT")+PROJ_DBL("PR_AUFG_KOST"));
		Ret_lstData.setData(0,"T_AUFG_HON_ZT",TOT("T_AUFG_HON_ZT")+PROJ_DBL("PR_AUFG_ZEIT"));		
		//;  Nebenkosten:
		Ret_lstData.setData(0,"T_AUFW_NK",TOT("T_AUFW_NK")+PROJ_DBL("PR_AUFWAND_SK"));		
		Ret_lstData.setData(0,"T_AUFW_NK_NF",TOT("T_AUFW_NK_NF")+PROJ_INT("PR_VERRECHENBAR")?PROJ_DBL("PR_AUFG_NF_NK_INT"):PROJ_DBL("PR_AUFWAND_SK"));
		Ret_lstData.setData(0,"T_NK_AUFG",TOT("T_NK_AUFG")+PROJ_DBL("PR_AUFG_SK"));	
		double dbTemp=PROJ_INT("PR_VERRECHENBAR")?PROJ_DBL("PR_AUFG_NF_NK_EXT"):(PROJ_DBL("PR_AUFG_SK")+DEF("DEF_NK_FK")?PROJ_DBL("PR_AUFG_FREMD"):0);
		Ret_lstData.setData(0,"T_AUFG_NK_NF",TOT("T_AUFG_NK_NF")+dbTemp);	
		//;  Fremdkosten:
		Ret_lstData.setData(0,"T_FREMDKOST_AUFW",TOT("T_FREMDKOST_AUFW")+PROJ_DBL("PR_AUFW_FREMD"));	
		Ret_lstData.setData(0,"T_FREMDKOST_AUFG",TOT("T_FREMDKOST_AUFG")+PROJ_DBL("PR_AUFG_FREMD"));	
		//;  Fremdleistungen:
		Ret_lstData.setData(0,"T_FREMDLEIST_AUFW",TOT("T_FREMDLEIST_AUFW")+PROJ_DBL("PR_AUFW_FREMDLEIST"));	
		Ret_lstData.setData(0,"T_FREMDLEIST_AUFG",TOT("T_FREMDLEIST_AUFG")+PROJ_DBL("PR_AUFG_FREMDLEIST"));	
		//;  Rechnungen:
		Ret_lstData.setData(0,"T_FAKT_HON",TOT("T_FAKT_HON")+PROJ_DBL("PR_FAKT_KOSTEN")+PROJ_DBL("PR_FAKT_ZEIT")+PROJ_DBL("PR_FAKT_ZMITT")+PROJ_DBL("PR_FAKT_EXT_HON"));	
		Ret_lstData.setData(0,"T_FAKT_NK",TOT("T_FAKT_NK")+PROJ_DBL("PR_FAKT_NEBEN")+PROJ_DBL("PR_FAKT_EXT_NK"));	
		Ret_lstData.setData(0,"T_FAKT_KOSTEN",TOT("T_FAKT_KOSTEN")+PROJ_DBL("PR_FAKT_KOSTEN"));	
		Ret_lstData.setData(0,"T_FAKT_ZEIT",TOT("T_FAKT_ZEIT")+PROJ_DBL("PR_FAKT_ZEIT"));	
		Ret_lstData.setData(0,"T_FAKT_ZMITT",TOT("T_FAKT_ZMITT")+PROJ_DBL("PR_FAKT_ZMITT"));	
		Ret_lstData.setData(0,"T_FAKT_NEBEN",TOT("T_FAKT_NEBEN")+PROJ_DBL("PR_FAKT_NEBEN"));	
		Ret_lstData.setData(0,"T_FAKT_EXT_HON",TOT("T_FAKT_EXT_HON")+PROJ_DBL("PR_FAKT_EXT_HON"));	
		Ret_lstData.setData(0,"T_FAKT_EXT_NK",TOT("T_FAKT_EXT_NK")+PROJ_DBL("PR_FAKT_EXT_NK"));	
		Ret_lstData.setData(0,"T_MWST",TOT("T_MWST")+PROJ_DBL("PR_WUST"));	
		Ret_lstData.setData(0,"T_RABATT",TOT("T_RABATT")+PROJ_DBL("PR_RABATTE"));	
		Ret_lstData.setData(0,"T_FAKT_EXTERN",TOT("T_FAKT_EXTERN")+PROJ_DBL("PR_FAKT_EXT_HON"));	
		Ret_lstData.setData(0,"T_FAKT_SPEZ",TOT("T_FAKT_SPEZ")+PROJ_DBL("PR_FAKT_SPEZ"));	

		if (DEF("DEF_DEF_AUFG_EXT") || DEF("DEF_FULL_FAKT_KZ"))
		{
			SpcBusinessFiguresHelper::InitVars_110(Ret_lstData);
			Routine_113_SP(Ret_Status,Ret_lstData,lstProjects,recFLDefault,nRowIdx,PROJ_STR("PR_CODE"),QDate(1900,1,1),QDate(2100,1,1),0,0,"");
			//Call procedure 110 (PR_CODE,dat('1.1.1900'),dat('1.1.2100')) {Summe Rechnungen ein Projekt}     ;; Alle Rechnungen summieren: Nur wegen Rabatten f�r Honorare und Nebenkosten, da diese nicht in den Projektsummen gef�hrt werden! // MB 29.01.2013
			if (DEF("DEF_DEF_AUFG_EXT"))
			{
				Ret_lstData.setData(0,"T_FAKT_RAB_ARB",TOT("T_FAKT_RAB_ARB")+PROJ_DBL("T_KUM_F_RAB_ARB"));	
				Ret_lstData.setData(0,"T_FAKT_RAB_NK",TOT("T_FAKT_RAB_NK")+PROJ_DBL("T_KUM_F_RAB_NK"));	
			}
			Ret_lstData.setData(0,"T_FAKT_FK",TOT("T_FAKT_FK")+PROJ_DBL("T_KUM_F_FK"));	
			Ret_lstData.setData(0,"T_FAKT_NK_O_FK",TOT("T_FAKT_NK_O_FK")+PROJ_DBL("T_KUM_F_NK_O_FK"));	
		}
		//;  Zahlungen:
		Ret_lstData.setData(0,"T_ZAHL_HON",TOT("T_ZAHL_HON")+PROJ_DBL("PR_ZAHL_KOST")+PROJ_DBL("PR_ZAHL_ZEIT")+PROJ_DBL("PR_ZAHL_ZEITM"));	
		Ret_lstData.setData(0,"T_ZAHL_NK",TOT("T_ZAHL_NK")+PROJ_DBL("PR_ZAHL_NEBEN"));	
		Ret_lstData.setData(0,"T_ZAHL_KOSTEN",TOT("T_ZAHL_KOSTEN")+PROJ_DBL("PR_ZAHL_KOST"));	
		Ret_lstData.setData(0,"T_ZAHL_ZEIT",TOT("T_ZAHL_ZEIT")+PROJ_DBL("PR_ZAHL_ZEIT"));	
		Ret_lstData.setData(0,"T_ZAHL_ZMITT",TOT("T_ZAHL_ZMITT")+PROJ_DBL("PR_ZAHL_ZEITM"));
	}
	else
	{
		Routine_147(Ret_Status,Ret_lstData,lstProjects,recFLDefault,nRowIdx,0,PROJ_STR("PR_CODE"),QDate(2000,1,1));
	}
}







void Service_SpcBusinessFigures::Routine_113_SP(Status& Ret_Status, DbRecordSet &Ret_lstData, const DbRecordSet &lstProjects, const DbRecordSet &recFLDefault, int nRowIdx, QString strProjCode, QDate datFrom, QDate datTo, int nINCLUDE_SP, int nABT_EQUAL/*=0*/, QString strABT )
{
	DbSqlQuery query(Ret_Status, GetDbManager()); 
	if(!Ret_Status.IsOK()) return;

	QString strSQL="EXECUTE PROCEDURE PROC_FAKTURA_CUMULATION ?,?,?,?,?,?,1";
	query.Prepare(Ret_Status,strSQL);
	if(!Ret_Status.IsOK()) return;

	query.bindValue(0,strProjCode);
	query.bindValue(1,datFrom);
	query.bindValue(2,datTo);
	query.bindValue(3,nINCLUDE_SP);
	query.bindValue(4,nABT_EQUAL);
	query.bindValue(5,strABT);
	
	query.ExecutePrepared(Ret_Status);
	if(!Ret_Status.IsOK()) return;

	query.Execute(Ret_Status,"SELECT * FROM TEMP_FAKTURA_CUMULATION");
	if(!Ret_Status.IsOK()) return;

	DbRecordSet lstKumData;
	query.FetchData(lstKumData);

	Ret_lstData.assignRow(0,lstKumData.getRow(0));
}

void Service_SpcBusinessFigures::Routine_105_SP(Status& Ret_Status, DbRecordSet &Ret_lstData, const DbRecordSet &lstProjects, const DbRecordSet &recFLDefault, int nRowIdx, QString strProjCode, QDate datFrom, QDate datTo, int nP_UZ_ONLY, int nP_UZ_STUFE, int nP_ABT_FREMD_ONLY, int nPINIT, int nINCLUDE_SP, int nABT_EQUAL, QString strABT)
{
	DbSqlQuery query(Ret_Status, GetDbManager()); 
	if(!Ret_Status.IsOK()) return;
	
	QString strSQL="EXECUTE PROCEDURE PROC_CHARGE_CUMULATION ?,?,?,?,?,?,?,?,?,?,1";
	query.Prepare(Ret_Status,strSQL);
	if(!Ret_Status.IsOK()) return;

	query.bindValue(0,strProjCode);
	query.bindValue(1,datFrom);
	query.bindValue(2,datTo);
	query.bindValue(3,nP_UZ_ONLY);
	query.bindValue(4,nP_UZ_STUFE);
	query.bindValue(5,nP_ABT_FREMD_ONLY);
	query.bindValue(6,nPINIT);
	query.bindValue(7,nINCLUDE_SP);
	query.bindValue(8,nABT_EQUAL);
	query.bindValue(9,strABT);

	query.ExecutePrepared(Ret_Status);
	if(!Ret_Status.IsOK()) return;

	query.Execute(Ret_Status,"SELECT * FROM TEMP_CHARGE_CUMULATION");
	if(!Ret_Status.IsOK()) return;

	DbRecordSet lstKumData;
	query.FetchData(lstKumData);

	for (int i=1;i<lstKumData.getRowCount();i++)
	{
		for (int j=0;j<lstKumData.getColumnCount();j++)
		{
			lstKumData.setData(0,j,lstKumData.getDataRef(0,j).toDouble()+lstKumData.getDataRef(i,j).toDouble());
		}
	}
		
	Ret_lstData.assignRow(0,lstKumData.getRow(0));
}


void Service_SpcBusinessFigures::Routine_104_SP(Status& Ret_Status, DbRecordSet &Ret_lstData, const DbRecordSet &lstProjects, const DbRecordSet &recFLDefault, int nRowIdx, QString strProjCode, QDate datFrom, QDate datTo, int nINCLUDE_SP, int nABT_EQUAL, QString strABT)
{
	DbSqlQuery query(Ret_Status, GetDbManager()); 
	if(!Ret_Status.IsOK()) return;

	QString strSQL="EXECUTE PROCEDURE PROC_PAYMENT_CUMULATION ?,?,?,?,?,?,1";
	query.Prepare(Ret_Status,strSQL);
	if(!Ret_Status.IsOK()) return;

	query.bindValue(0,strProjCode);
	query.bindValue(1,datFrom);
	query.bindValue(2,datTo);
	query.bindValue(3,nINCLUDE_SP);
	query.bindValue(4,nABT_EQUAL);
	query.bindValue(5,strABT);

	query.ExecutePrepared(Ret_Status);
	if(!Ret_Status.IsOK()) return;

	query.Execute(Ret_Status,"SELECT * FROM TEMP_PAYMENT_CUMULATION");
	if(!Ret_Status.IsOK()) return;

	DbRecordSet lstKumData;
	query.FetchData(lstKumData);

	Ret_lstData.assignRow(0,lstKumData.getRow(0));
}


void Service_SpcBusinessFigures::Routine_147( Status& Ret_Status, DbRecordSet &Ret_lstData, const DbRecordSet &lstProjects, const DbRecordSet &recFLDefault, int nRowIdx, int nT_DIAG_KUMULIERT, QString strProjCode, QDate datTo, int nABT_EQUAL/*=0*/, QString strABT/*="" */ )
{
	if (nT_DIAG_KUMULIERT)
	{
		SpcBusinessFiguresHelper::InitVars_102(Ret_lstData);
		Routine_105_SP(Ret_Status,Ret_lstData,lstProjects,recFLDefault,nRowIdx,strProjCode,QDate(1900,1,1),datTo);
		SpcBusinessFiguresHelper::InitVars_112(Ret_lstData);
		Routine_113_SP(Ret_Status,Ret_lstData,lstProjects,recFLDefault,nRowIdx,strProjCode,QDate(1900,1,1),datTo,0);
		SpcBusinessFiguresHelper::InitVars_117(Ret_lstData);
		Routine_104_SP(Ret_Status,Ret_lstData,lstProjects,recFLDefault,nRowIdx,strProjCode,QDate(1900,1,1),datTo,0);
	}
	else
	{
		Routine_105_SP(Ret_Status,Ret_lstData,lstProjects,recFLDefault,nRowIdx,strProjCode,QDate(1900,1,1),datTo,0,0,0,0,1,nABT_EQUAL,strABT);
		Routine_103_SP(Ret_Status,Ret_lstData,lstProjects,recFLDefault,nRowIdx,strProjCode,QDate(1900,1,1),datTo,1,nABT_EQUAL,strABT);
		Routine_104_SP(Ret_Status,Ret_lstData,lstProjects,recFLDefault,nRowIdx,strProjCode,QDate(1900,1,1),datTo,1,nABT_EQUAL,strABT);
	}
	
	//;  Zeit:
	Ret_lstData.setData(0,"T_ZEIT_AUFG",TOT("T_ZEIT_AUFG")+PROJ_DBL("T_KUM_ZEIT"));	
	Ret_lstData.setData(0,"T_AUFG_ZEIT_NF",TOT("T_AUFG_ZEIT_NF")+PROJ_DBL("T_KUM_AUFG_ZEIT_NF"));	
	//	;  Honorar:
	Ret_lstData.setData(0,"T_AUFW_HON",TOT("T_AUFW_HON")+PROJ_DBL("T_KUM_AUFW_HON"));	
	Ret_lstData.setData(0,"T_HONORAR_AUFG",TOT("T_HONORAR_AUFG")+PROJ_DBL("T_KUM_HON"));	
	Ret_lstData.setData(0,"T_AUFG_HON_NF",TOT("T_AUFG_HON_NF")+PROJ_DBL("T_KUM_AUFG_HON_NF"));	
	Ret_lstData.setData(0,"T_AUFW_HON_NF",TOT("T_AUFW_HON_NF")+PROJ_DBL("T_KUM_AUFW_HON_NF"));	
	//	;  Nebenkosten:
	Ret_lstData.setData(0,"T_AUFW_NK",TOT("T_AUFW_NK")+PROJ_DBL("T_KUM_AUFW_NEBEN"));	
	Ret_lstData.setData(0,"T_NK_AUFG",TOT("T_NK_AUFG")+PROJ_DBL("T_KUM_NEBEN"));
	Ret_lstData.setData(0,"T_AUFG_NK_NF",TOT("T_AUFG_NK_NF")+TOT("T_KUM_AUFG_NK_NF")+DEF("DEF_NK_FK")?PROJ_DBL("T_KUM_AUFG_FK_NF"):0);
	Ret_lstData.setData(0,"T_AUFW_NK_NF",TOT("T_AUFW_NK_NF")+TOT("T_KUM_AUFW_NK_NF")-DEF("DEF_NK_FK")?PROJ_DBL("T_KUM_AUFW_FK_NF"):0);
	Ret_lstData.setData(0,"T_NF_NK_EXT",TOT("T_NF_NK_EXT")+PROJ_DBL("T_KUM_AUFG_NK_NF"));	
	Ret_lstData.setData(0,"T_NF_NK_INT",TOT("T_NF_NK_INT")+PROJ_DBL("T_KUM_AUFW_NK_NF"));	
	//	;  Fremdkosten:
	Ret_lstData.setData(0,"T_FREMDKOST_AUFW",TOT("T_FREMDKOST_AUFW")+PROJ_DBL("T_KUM_AUFW_FR_KOST"));	
	Ret_lstData.setData(0,"T_FREMDKOST_AUFG",TOT("T_FREMDKOST_AUFG")+PROJ_DBL("T_KUM_AUFG_FR_KOST"));	
	Ret_lstData.setData(0,"T_NF_FK_EXT",TOT("T_NF_FK_EXT")+PROJ_DBL("T_KUM_AUFG_FK_NF"));	
	Ret_lstData.setData(0,"T_NF_FK_INT",TOT("T_NF_FK_INT")+PROJ_DBL("T_KUM_AUFW_FK_NF"));	
	//;  Fremdleistungen:
	Ret_lstData.setData(0,"T_FREMDLEIST_AUFW",TOT("T_FREMDLEIST_AUFW")+PROJ_DBL("T_KUM_AUFW_FR_LEIST"));	
	Ret_lstData.setData(0,"T_FREMDLEIST_AUFG",TOT("T_FREMDLEIST_AUFG")+PROJ_DBL("T_KUM_AUFG_FR_LEIST"));	
	//	;  Rechnungen:
	Ret_lstData.setData(0,"T_FAKT_HON",TOT("T_FAKT_HON")+PROJ_DBL("T_KUM_F_HONORAR")+PROJ_DBL("T_KUM_F_EXT_HON"));	
	Ret_lstData.setData(0,"T_FAKT_NK",TOT("T_FAKT_NK")+PROJ_DBL("T_KUM_F_NEBENKOST")+PROJ_DBL("T_KUM_F_EXT_NK"));	
	Ret_lstData.setData(0,"T_FAKT_RAB_ARB",TOT("T_FAKT_RAB_ARB")+PROJ_DBL("T_KUM_F_RAB_ARB"));	
	Ret_lstData.setData(0,"T_FAKT_RAB_NK",TOT("T_FAKT_RAB_NK")+PROJ_DBL("T_KUM_F_RAB_NK"));	
	Ret_lstData.setData(0,"T_FAKT_FK",TOT("T_FAKT_FK")+PROJ_DBL("T_KUM_F_FK"));	
	Ret_lstData.setData(0,"T_FAKT_NK_O_FK",TOT("T_FAKT_NK_O_FK")+PROJ_DBL("T_KUM_F_NK_O_FK"));	
	//	;  Zahlungen:
	Ret_lstData.setData(0,"T_ZAHL_HON",TOT("T_ZAHL_HON")+PROJ_DBL("T_KUM_Z_BETRAG")+PROJ_DBL("T_KUM_Z_SKONTO")+PROJ_DBL("T_KUM_Z_BET_MWSTA")+PROJ_DBL("T_KUM_Z_BET_MWSTB"));	
	//	;  Fakturierte Belastungen (MB 16.11.2008)
	Ret_lstData.setData(0,"T_ARB_AUFG_FAKT",TOT("T_ARB_AUFG_FAKT")+PROJ_DBL("T_KUM_ARB_AUFG_FAKT"));	
	Ret_lstData.setData(0,"T_ARB_AUFW_FAKT",TOT("T_ARB_AUFW_FAKT")+PROJ_DBL("T_KUM_ARB_AUFW_FAKT"));	
	Ret_lstData.setData(0,"T_NK_AUFG_FAKT",TOT("T_NK_AUFG_FAKT")+PROJ_DBL("T_KUM_NK_AUFG_FAKT"));	
	Ret_lstData.setData(0,"T_NK_AUFW_FAKT",TOT("T_NK_AUFW_FAKT")+PROJ_DBL("T_KUM_NK_AUFW_FAKT"));	
	Ret_lstData.setData(0,"T_FK_AUFG_FAKT",TOT("T_FK_AUFG_FAKT")+PROJ_DBL("T_KUM_FK_AUFG_FAKT"));	
	Ret_lstData.setData(0,"T_FK_AUFW_FAKT",TOT("T_FK_AUFW_FAKT")+PROJ_DBL("T_KUM_FK_AUFW_FAKT"));	

}

void Service_SpcBusinessFigures::Routine_103_SP( Status& Ret_Status, DbRecordSet &Ret_lstData, const DbRecordSet &lstProjects, const DbRecordSet &recFLDefault, int nRowIdx, QString strProjCode, QDate datFrom, QDate datTo, int nINCLUDE_SP/*=0*/, int nABT_EQUAL/*=0*/, QString strABT/*=""*/ )
{
	DbSqlQuery query(Ret_Status, GetDbManager()); 
	if(!Ret_Status.IsOK()) return;

	QString strSQL="EXECUTE PROCEDURE PROC_FAKTURA_CUMULATION ?,?,?,?,?,?,1";
	query.Prepare(Ret_Status,strSQL);
	if(!Ret_Status.IsOK()) return;

	query.bindValue(0,strProjCode);
	query.bindValue(1,datFrom);
	query.bindValue(2,datTo);
	query.bindValue(3,nINCLUDE_SP);
	query.bindValue(4,nABT_EQUAL);
	query.bindValue(5,strABT);

	query.ExecutePrepared(Ret_Status);
	if(!Ret_Status.IsOK()) return;

	query.Execute(Ret_Status,"SELECT * FROM TEMP_FAKTURA_CUMULATION");
	if(!Ret_Status.IsOK()) return;

	DbRecordSet lstKumData;
	query.FetchData(lstKumData);

	Ret_lstData.assignRow(0,lstKumData.getRow(0));

}


double Service_SpcBusinessFigures::Routine_148( Status& Ret_Status, const DbRecordSet &lstProjects, const DbRecordSet &recFLDefault, int nRowIdx, QString strProjCode, QDate datDate, int nWERT_INKL_NFAKT/*=0*/ )
{
	QDate DAT1,DAT2,WDAT1,WDAT2;
	double WERT0, WERT1,WERT2,HONORAR,NEBENKOSTEN;

	if (datDate==PROJ("PR_SD").toDate())
	{
		return PROJ("PR_WERT_EST").toDouble();
	}
	else if (datDate==PROJ("PR_DATINCPROG").toDate())
	{
		return PROJ("PR_WERT_EST_PREV").toDouble();
	}


	if (PROJ("PR_SD").toDate().isNull() && PROJ("PR_DATINCPROG").toDate().isNull())
	{
		DAT1=QDate(1900,1,1);
		WERT0=0;
	}
	else
	{
		if (PROJ("PR_SD").toDate() > PROJ("PR_DATINCPROG").toDate())
		{
			WDAT1=PROJ("PR_DATINCPROG").toDate();
			WDAT2=PROJ("PR_SD").toDate();
			WERT1=PROJ("PR_WERT_EST_PREV").toDouble();
			WERT2=PROJ("PR_WERT_EST").toDouble();
		}
		else
		{
			WDAT1=PROJ("PR_SD").toDate();
			WDAT2=PROJ("PR_DATINCPROG").toDate();
			WERT1=PROJ("PR_WERT_EST").toDouble();
			WERT2=PROJ("PR_WERT_EST_PREV").toDouble();
		}

		if (WDAT2.isValid() && WDAT2<datDate)
		{
			DAT1=WDAT2.addDays(1);
			WERT0=WERT2;
		}
		else if (WDAT1.isValid() && WDAT1<datDate)
		{
			DAT1=WDAT1.addDays(1);
			WERT0=WERT1;
		}
		else
		{
			DAT1=QDate(1900,1,1);
			WERT0=0;
		}
	}

	DAT2=datDate;


	//;  Summieren der Belastungen zwischen Dat1 und Dat2:
	Routine_153(Ret_Status,recFLDefault,strProjCode,DAT1,DAT2,DEF("DEF_BUDGET_GB_HS"), nWERT_INKL_NFAKT,HONORAR,NEBENKOSTEN);
	if(!Ret_Status.IsOK()) return 0;

	return WERT0+HONORAR+NEBENKOSTEN;


}

void Service_SpcBusinessFigures::Routine_153( Status& Ret_Status, const DbRecordSet &recFLDefault, QString strProjCode, QDate datDate1, QDate datDate2, int nBUDGET_GB_HS, int nINKL_NOT_FAKT, double &nHONORAR, double &nNEBENKOSTEN)
{
	DbSqlQuery query(Ret_Status, GetDbManager()); 
	if(!Ret_Status.IsOK()) return;

	QString strSQL="SELECT sum(FB_KOSTEN),sum(FB_SPESEN) FROM FASSIGN WHERE FASSIGN.FB_DATUM >=? AND FASSIGN.FB_DATUM <= ? AND FASSIGN.FB_PROJ_NR = ?";
	if (!nINKL_NOT_FAKT)
	{
		strSQL+=" AND FB_NICHT_FAKTURIERBAR=0";
	}
	strSQL+=" PLAN (FASSIGN INDEX (IND_FB_CI_PD)";

	query.Prepare(Ret_Status,strSQL);
	if(!Ret_Status.IsOK()) return;

	query.bindValue(0,datDate1);
	query.bindValue(1,datDate2);
	query.bindValue(2,strProjCode);

	query.ExecutePrepared(Ret_Status);
	if(!Ret_Status.IsOK()) return;

	DbRecordSet lstKumData;
	query.FetchData(lstKumData);

	nHONORAR=0;
	nNEBENKOSTEN=0;

	if (lstKumData.getRowCount()>0)
	{
		nHONORAR=lstKumData.getDataRef(0,0).toDouble();
		nNEBENKOSTEN=lstKumData.getDataRef(0,1).toDouble();
		if (DEF("DEF_BUDGET_GB_HS")>0)
			nNEBENKOSTEN=0;
	}
}

QDate Service_SpcBusinessFigures::Routine_154(Status& Ret_Status, QString strProjCode, QDate datDate)
{
	QDate datFirstCharge;
	DbSqlQuery query(Ret_Status, GetDbManager()); 
	if(!Ret_Status.IsOK()) return QDate();
	
	QString strSQL="SELECT FIRST 1 FB_DATUM FROM FASSIGN WHERE FASSIGN.FB_DATUM >=? AND FASSIGN.FB_PROJ_NR = ? PLAN (FASSIGN INDEX (IND_FB_CI_PD)) ORDER BY FB_DATUM ASC";
	query.Prepare(Ret_Status,strSQL);
	if(!Ret_Status.IsOK()) return QDate();
	query.bindValue(0,datDate);
	query.bindValue(1,strProjCode);
	query.ExecutePrepared(Ret_Status);
	if(!Ret_Status.IsOK()) return QDate();
	if(query.next())
	{
		datFirstCharge = query.value(0).toDate();
	}

	return datFirstCharge;
}