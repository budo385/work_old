#include "service_busnmrx.h"
#include "db/db/dbsimpleorm.h"
#include "db_core/db_core/dbsqltableview.h"
#include "businesslocker.h"
#include "bus_core/bus_core/globalconstants.h"
#include "common/common/entity_id_collection.h"
#include "bus_core/bus_core/nmrxmanager.h"

#include "businessserviceset.h"
extern BusinessServiceSet* g_BusinessServiceSet;



//------------------------------------------------------------------------
//					NMR RELATIONS
//------------------------------------------------------------------------


//returned in TVIEW_BUS_NMRX_RELATION_SELECT format
void Service_BusNMRX::ReadOnePair(Status &Ret_pStatus, int nM_TableID,int nN_TableID, int nKey1,int nKey2,DbRecordSet &Ret_Data,int nX_TableID)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_NMRX_RELATION, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;


	QString strWhere=" WHERE BNMR_TABLE_1= "+QVariant(nM_TableID).toString()+" AND BNMR_TABLE_2 ="+QVariant(nN_TableID).toString();
	if (nKey1!=-1)
		strWhere+=" AND BNMR_TABLE_KEY_ID_1="+QVariant(nKey1).toString();

	if (nKey2!=-1)
		strWhere+=" AND BNMR_TABLE_KEY_ID_2="+QVariant(nKey2).toString();

	strWhere+=" AND BNMR_TYPE="+QString::number(GlobalConstants::NMRX_TYPE_ASSIGN);

	//load all
	TableOrm.Read(Ret_pStatus,Ret_Data,strWhere,DbSqlTableView::TVIEW_BUS_NMRX_RELATION_SELECT);

	if (nX_TableID!=-1)
	{
		QString strPKColName=DbSqlTableDefinition::GetTablePrimaryKey(nX_TableID);

		DbSimpleOrm TableOrm_X(Ret_pStatus, nX_TableID, GetDbManager()); 
		if(!Ret_pStatus.IsOK()) return;

		int nSize=Ret_Data.getRowCount();
		for(int i=0;i<nSize;i++)
		{
			int nX_TableRecordID=Ret_Data.getDataRef(i,"BNMR_X_TABLE_ID").toInt();
			if (nX_TableRecordID>0)
			{
				DbRecordSet rowRecordX;
				TableOrm_X.Read(Ret_pStatus,rowRecordX," WHERE "+strPKColName+"="+QString::number(nX_TableRecordID));
				if(!Ret_pStatus.IsOK()) return;
				Ret_Data.setData(i,"X_RECORD",rowRecordX);
			}
		}
	}

}


//pair list is in read format, if key is -1 then skip
//of one fails, all fail
void Service_BusNMRX::ReadMultiPairs(Status &Ret_pStatus, DbRecordSet &DataPairs, DbRecordSet &Ret_Data,int nX_TableID)
{

	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_NMRX_RELATION, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	int nTable1Idx=DataPairs.getColumnIdx("BNMR_TABLE_1");
	int nTable2Idx=DataPairs.getColumnIdx("BNMR_TABLE_2");
	int nKey1Idx=DataPairs.getColumnIdx("BNMR_TABLE_KEY_ID_1");
	int nKey2Idx=DataPairs.getColumnIdx("BNMR_TABLE_KEY_ID_2");


	DbRecordSet lstData;
	Ret_Data.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION_SELECT));

	int nSize=DataPairs.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		int nM_TableID=DataPairs.getDataRef(i,nTable1Idx).toInt();
		int nN_TableID=DataPairs.getDataRef(i,nTable2Idx).toInt();
		int nKey1=DataPairs.getDataRef(i,nKey1Idx).toInt();
		int nKey2=DataPairs.getDataRef(i,nKey2Idx).toInt();

		QString strWhere=" WHERE BNMR_TABLE_1= "+QVariant(nM_TableID).toString()+" AND BNMR_TABLE_2 ="+QVariant(nN_TableID).toString();
		if (nKey1!=-1)
			strWhere+=" AND BNMR_TABLE_KEY_ID_1="+QVariant(nKey1).toString();

		if (nKey2!=-1)
			strWhere+=" AND BNMR_TABLE_KEY_ID_2="+QVariant(nKey2).toString();

		strWhere+=" AND BNMR_TYPE="+QString::number(GlobalConstants::NMRX_TYPE_ASSIGN);

		//load all
		TableOrm.Read(Ret_pStatus,lstData,strWhere,DbSqlTableView::TVIEW_BUS_NMRX_RELATION_SELECT);
		if(!Ret_pStatus.IsOK()) return;

		Ret_Data.merge(lstData);
	}

	if (nX_TableID!=-1)
	{
		QString strPKColName=DbSqlTableDefinition::GetTablePrimaryKey(nX_TableID);

		DbSimpleOrm TableOrm_X(Ret_pStatus, nX_TableID, GetDbManager()); 
		if(!Ret_pStatus.IsOK()) return;

		int nSize=Ret_Data.getRowCount();
		for(int i=0;i<nSize;i++)
		{
			int nX_TableRecordID=Ret_Data.getDataRef(i,"BNMR_X_TABLE_ID").toInt();
			if (nX_TableRecordID>0)
			{
				DbRecordSet rowRecordX;
				TableOrm_X.Read(Ret_pStatus,rowRecordX," WHERE "+strPKColName+"="+QString::number(nX_TableRecordID));
				if(!Ret_pStatus.IsOK()) return;
				Ret_Data.setData(i,"X_RECORD",rowRecordX);
			}
		}
	}
}

//for write in NORMAL (non-lazy write mode)...(hehe, showmazgoon)
void Service_BusNMRX::WriteSimple(Status &Ret_pStatus, DbRecordSet &RetOut_Data,QString strLockRes,int nX_TableID)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_NMRX_RELATION, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	//write X table FIRST!!!:
	if (nX_TableID!=-1)
	{
		DbRecordSet lstRecordX;
		lstRecordX.defineFromView(DbSqlTableView::getView(DbSqlTableDefinition::GetFullViewID(nX_TableID)));

		int nSize=RetOut_Data.getRowCount();
		for(int i=0;i<nSize;i++)
		{
			DbRecordSet rowRecordX=RetOut_Data.getDataRef(i,"X_RECORD").value<DbRecordSet>();
			if (rowRecordX.getRowCount()==0)
			{
				NMRXManager::DefineDefaultRecordX(nX_TableID,rowRecordX);
			}
			lstRecordX.merge(rowRecordX);
		}

		DbSimpleOrm TableOrm_X(Ret_pStatus, nX_TableID, GetDbManager()); 
		if(!Ret_pStatus.IsOK()) return;

		TableOrm_X.Write(Ret_pStatus,lstRecordX);
		if (!Ret_pStatus.IsOK())
		{
			TableOrm.GetDbSqlQuery()->Rollback();
			return;
		}

		for(int i=0;i<nSize;i++) //store back new X records:
		{
			DbRecordSet rowRecordX=lstRecordX.getRow(i);
			RetOut_Data.setData(i,"X_RECORD",rowRecordX);
			RetOut_Data.setData(i,"BNMR_X_TABLE_ID",rowRecordX.getDataRef(0,"BCIV_ID").toInt());
		}
	}

	int nSkipLastCols=DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION_SELECT).m_nSkipLastColsWrite;
	//write
	TableOrm.Write(Ret_pStatus,RetOut_Data,-1,nSkipLastCols);

	//if write ok & locked, unlock:
	if(Ret_pStatus.IsOK() && !strLockRes.isEmpty())
	{
		BusinessLocker locker(Ret_pStatus,BUS_NMRX_RELATION,GetDbManager(), TableOrm.GetDbConnection());
		locker.UnLock(Ret_pStatus,strLockRes);
	}

}


//data must be in select format to write
void Service_BusNMRX::Write(Status &Ret_pStatus, DbRecordSet &RetOut_Data,QString strLockRes,int nX_TableID)
{
	Ret_pStatus.setError(0);
	if (RetOut_Data.getRowCount()==0) //warning: deletes old ones, but nmrx pattern widget already deletes on site
	{
		return;
	}

	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_NMRX_RELATION, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	TableOrm.GetDbSqlQuery()->BeginTransaction(Ret_pStatus);
	if(!Ret_pStatus.IsOK()) return;


	//write X table FIRST!!!:
	if (nX_TableID!=-1)
	{
		DbRecordSet lstRecordX;
		lstRecordX.defineFromView(DbSqlTableView::getView(DbSqlTableDefinition::GetFullViewID(nX_TableID)));

		int nSize=RetOut_Data.getRowCount();
		for(int i=0;i<nSize;i++)
		{
			DbRecordSet rowRecordX=RetOut_Data.getDataRef(i,"X_RECORD").value<DbRecordSet>();
			if (rowRecordX.getRowCount()==0)
			{
				NMRXManager::DefineDefaultRecordX(nX_TableID,rowRecordX);
			}
			lstRecordX.merge(rowRecordX);
		}

		DbSimpleOrm TableOrm_X(Ret_pStatus, nX_TableID, GetDbManager()); 
		if(!Ret_pStatus.IsOK()) return;

		TableOrm_X.Write(Ret_pStatus,lstRecordX);
		if (!Ret_pStatus.IsOK())
		{
			TableOrm.GetDbSqlQuery()->Rollback();
			return;
		}

		for(int i=0;i<nSize;i++) //store back new X records:
		{
			DbRecordSet rowRecordX=lstRecordX.getRow(i);
			RetOut_Data.setData(i,"X_RECORD",rowRecordX);
			RetOut_Data.setData(i,"BNMR_X_TABLE_ID",rowRecordX.getDataRef(0,"BCIV_ID").toInt());
		}
	}

	//if key1 is same: use it as 'parent' and delete all prev associations
	int nSkipLastCols=DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION_SELECT).m_nSkipLastColsWrite;
	int nSize=RetOut_Data.getRowCount();
	int nId_1=RetOut_Data.getDataRef(0,"BNMR_TABLE_KEY_ID_1").toInt();
	int nTable_1=RetOut_Data.getDataRef(0,"BNMR_TABLE_1").toInt();
	int nTable_2=RetOut_Data.getDataRef(0,"BNMR_TABLE_2").toInt();
	DbRecordSet tmpLst;
	tmpLst.copyDefinition(RetOut_Data);
	tmpLst.merge(RetOut_Data.getRow(0));
	int nWritenRows=0;
	for(int i=1;i<nSize;i++)
	{
		if (nTable_1!= RetOut_Data.getDataRef(i,"BNMR_TABLE_1").toInt() || nTable_2!= RetOut_Data.getDataRef(i,"BNMR_TABLE_2").toInt() || nId_1!=RetOut_Data.getDataRef(i,"BNMR_TABLE_KEY_ID_1").toInt())
		{
			//tmpLst.Dump();
			QString strWhere = "BNMR_TABLE_1 =%1 AND BNMR_TABLE_2=%2 AND BNMR_TYPE=0";
			strWhere=strWhere.arg(nTable_1).arg(nTable_2);

			TableOrm.WriteSubData(Ret_pStatus,BUS_NMRX_RELATION,RetOut_Data,"BNMR_TABLE_KEY_ID_1",nId_1,true,nSkipLastCols,strWhere);
			if (!Ret_pStatus.IsOK())
			{
				TableOrm.GetDbSqlQuery()->Rollback();
				return;
			}

			nWritenRows+=tmpLst.getRowCount();
			tmpLst.clear();
			nId_1=RetOut_Data.getDataRef(i,"BNMR_TABLE_KEY_ID_1").toInt();
			nTable_1=RetOut_Data.getDataRef(i,"BNMR_TABLE_1").toInt();
			nTable_2=RetOut_Data.getDataRef(i,"BNMR_TABLE_2").toInt();
			tmpLst.merge(RetOut_Data.getRow(i));
		}
		else
		{
			tmpLst.merge(RetOut_Data.getRow(i));
		}
	}

	if (nWritenRows!=nSize && tmpLst.getRowCount()>0)
	{
		//tmpLst.Dump();

		QString strWhere = "BNMR_TABLE_1 =%1 AND BNMR_TABLE_2=%2 AND BNMR_TYPE=0";
		strWhere=strWhere.arg(nTable_1).arg(nTable_2);
		TableOrm.WriteSubData(Ret_pStatus,BUS_NMRX_RELATION,RetOut_Data,"BNMR_TABLE_KEY_ID_1",nId_1,true,nSkipLastCols,strWhere);
		if (!Ret_pStatus.IsOK())
		{
			TableOrm.GetDbSqlQuery()->Rollback();
			return;
		}
	}


	TableOrm.GetDbSqlQuery()->Commit(Ret_pStatus);
	if (!Ret_pStatus.IsOK())
	{
		TableOrm.GetDbSqlQuery()->Rollback();
		return;
	}

	//if write ok & locked, unlock:
	if(Ret_pStatus.IsOK() && !strLockRes.isEmpty())
	{
		BusinessLocker locker(Ret_pStatus,BUS_NMRX_RELATION,GetDbManager(), TableOrm.GetDbConnection());
		locker.UnLock(Ret_pStatus,strLockRes);
	}

}

//DataForDelete must contain BNMR_ID col
void Service_BusNMRX::Delete(Status &Ret_pStatus, DbRecordSet &DataForDelete )
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_NMRX_RELATION, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	TableOrm.DeleteFast(Ret_pStatus,DataForDelete);
}

void Service_BusNMRX::Lock(Status &Ret_pStatus, int nNMRX_ID,QString &Ret_strLockRes)
{

	//Make Locker Obj & lock:
	BusinessLocker locker(Ret_pStatus,BUS_NMRX_RELATION,GetDbManager());
	QList<int> lstLock;
	lstLock<<nNMRX_ID;
	locker.Lock(Ret_pStatus,lstLock,Ret_strLockRes);

}

void Service_BusNMRX::LockMulti(Status &Ret_pStatus, DbRecordSet lstLockData,QString &Ret_strLockRes)
{
	//Make Locker Obj & lock:
	BusinessLocker locker(Ret_pStatus,BUS_NMRX_RELATION,GetDbManager());
	locker.Lock(Ret_pStatus,lstLockData,Ret_strLockRes);
}

void Service_BusNMRX::Unlock(Status &Ret_pStatus, QString strLockRes)
{
	BusinessLocker locker(Ret_pStatus,BUS_NMRX_RELATION,GetDbManager());
	locker.UnLock(Ret_pStatus,strLockRes);

}


//------------------------------------------------------------------------
//					ROLES
//------------------------------------------------------------------------




void Service_BusNMRX::ReadRole(Status &Ret_pStatus, int nN_TableID,int nM_TableID, DbRecordSet &Ret_Data)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_NMRX_ROLE, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;


	//load all id -1 -1
	if (nM_TableID==-1 && nN_TableID==-1)
		TableOrm.Read(Ret_pStatus,Ret_Data);
	else
		TableOrm.Read(Ret_pStatus,Ret_Data," WHERE BNRO_TABLE_1= "+QVariant(nN_TableID).toString()+" AND BNRO_TABLE_2 ="+QVariant(nM_TableID).toString());
	

}


void Service_BusNMRX::WriteRole(Status &Ret_pStatus, DbRecordSet &RetOut_Data,DbRecordSet &DataForDelete,QString strLockRes)
{

	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_NMRX_ROLE, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;


	//tran
	TableOrm.GetDbSqlQuery()->BeginTransaction(Ret_pStatus);
	if(!Ret_pStatus.IsOK()) return;

	//delete:
	if(DataForDelete.getRowCount()>0)
		TableOrm.DeleteFast(Ret_pStatus,DataForDelete);
		if(!Ret_pStatus.IsOK())
		{	
			TableOrm.GetDbSqlQuery()->Rollback();
			return;
		}
	
	//Write
	TableOrm.Write(Ret_pStatus,RetOut_Data);
	if(!Ret_pStatus.IsOK())
	{
		TableOrm.GetDbSqlQuery()->Rollback();
		return;
	}

	//if write ok & locked, unlock:
	if(Ret_pStatus.IsOK() && !strLockRes.isEmpty())
	{
		BusinessLocker locker(Ret_pStatus,BUS_NMRX_ROLE,GetDbManager(), TableOrm.GetDbConnection());
		locker.UnLock(Ret_pStatus,strLockRes);
	}

	TableOrm.GetDbSqlQuery()->Commit(Ret_pStatus);


}


void Service_BusNMRX::DeleteRole(Status &Ret_pStatus, DbRecordSet &DataForDelete )
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_NMRX_ROLE, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	
	//delete:
	TableOrm.DeleteFast(Ret_pStatus,DataForDelete);

}


void Service_BusNMRX::LockRole(Status &Ret_pStatus, int nN_TableID,int nM_TableID, QString &Ret_strLockRes)
{
	//load all:
	//Make local query, can fail if connection reservation fails.
	DbSqlQuery query(Ret_pStatus, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	//load id's:
	query.Execute(Ret_pStatus,"SELECT BNRO_ID FROM BUS_NMRX_ROLE WHERE BNRO_TABLE_1= "+QVariant(nN_TableID).toString()+" AND BNRO_TABLE_2 ="+QVariant(nM_TableID).toString());
	if(!Ret_pStatus.IsOK())	return;
	DbRecordSet lstIDs;
	query.FetchData(lstIDs);

	//Make Locker Obj & lock:
	BusinessLocker locker(Ret_pStatus,BUS_NMRX_ROLE,GetDbManager(), query.GetDbConnection());
	locker.Lock(Ret_pStatus,lstIDs,Ret_strLockRes);



}

void Service_BusNMRX::UnlockRole(Status &Ret_pStatus, QString strLockRes)
{

	BusinessLocker locker(Ret_pStatus,BUS_NMRX_ROLE,GetDbManager());
	locker.UnLock(Ret_pStatus,strLockRes);
}



//from lstCeIDs, takes CENT_ID and pairs up in NMRX relation as 1st table/record id to record2 inside RetOut_Data, returns whole list back
//BNMR_SYSTEM_ROLE is already set..
//warning: must be inside transaction and called from server: no client interface
void Service_BusNMRX::WriteCEContactLink(Status &Ret_pStatus, DbRecordSet lstCeIDs, DbRecordSet &RetOut_Data)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_NMRX_RELATION, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	//prepare data: for each cent id copy whole link list
	DbRecordSet lstNewNMRX;
	lstNewNMRX.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION));

	RetOut_Data.setColValue("BNMR_TABLE_1",CE_COMM_ENTITY);
	RetOut_Data.setColValue("BNMR_TABLE_2",BUS_CM_CONTACT);
	RetOut_Data.setColValue("BNMR_TYPE",GlobalConstants::NMRX_TYPE_ASSIGN);

	int nSize=lstCeIDs.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		RetOut_Data.setColValue("BNMR_ID",QVariant(QVariant::Int));
		TableOrm.WriteSubData(Ret_pStatus,BUS_NMRX_RELATION,RetOut_Data,"BNMR_TABLE_KEY_ID_1",lstCeIDs.getDataRef(i,"CENT_ID").toInt(),true,0,NMRX_CE_CONTACT_WHERE);
		if(!Ret_pStatus.IsOK()) return;
		lstNewNMRX.merge(RetOut_Data);
	}

	RetOut_Data=lstNewNMRX;
}


//from lstCeIDs, takes CENT_ID and pairs up in NMRX relation as 1st table/record id to record2 inside RetOut_Data, returns whole list back
//BNMR_SYSTEM_ROLE is set as null
//warning: must be inside transaction and called from server: no client interface
void Service_BusNMRX::WriteCEProjectLink(Status &Ret_pStatus, DbRecordSet lstCeIDs, DbRecordSet &RetOut_Data)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_NMRX_RELATION, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	//prepare data: for each cent id copy whole link list
	DbRecordSet lstNewNMRX;
	lstNewNMRX.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION));

	RetOut_Data.setColValue("BNMR_TABLE_1",CE_COMM_ENTITY);
	RetOut_Data.setColValue("BNMR_TABLE_2",BUS_PROJECT);
	RetOut_Data.setColValue("BNMR_TYPE",GlobalConstants::NMRX_TYPE_ASSIGN);
	RetOut_Data.setColValue("BNMR_SYSTEM_ROLE",QVariant(QVariant::Int));

	int nSize=lstCeIDs.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		//qDebug()<<lstCeIDs.getDataRef(i,"CENT_ID").toInt();
		RetOut_Data.setColValue("BNMR_ID",QVariant(QVariant::Int));
		TableOrm.WriteSubData(Ret_pStatus,BUS_NMRX_RELATION,RetOut_Data,"BNMR_TABLE_KEY_ID_1",lstCeIDs.getDataRef(i,"CENT_ID").toInt(),true,0,NMRX_CE_PROJECT_WHERE);
		if(!Ret_pStatus.IsOK()) return;
		lstNewNMRX.merge(RetOut_Data);
	}

	RetOut_Data=lstNewNMRX;
}


//check if role with name exists,  if not writes
void Service_BusNMRX::CheckRole(Status &Ret_pStatus, int nN_TableID,int nM_TableID,QString strRoleName, DbRecordSet &Ret_Data, bool &Ret_bRoleInserted)
{
	Ret_bRoleInserted=false;

	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_NMRX_ROLE, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	QString strSQL=" SELECT "+DbSqlTableView::getSQLColumnsFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_ROLE))+" FROM BUS_NMRX_ROLE ";
	//load all id -1 -1
	if (nM_TableID==-1 && nN_TableID==-1)
		strSQL+=" WHERE BNRO_NAME=?";
	else
		strSQL+=" WHERE BNRO_TABLE_1= "+QVariant(nN_TableID).toString()+" AND BNRO_TABLE_2 ="+QVariant(nM_TableID).toString()+" AND BNRO_NAME=?";
		

	TableOrm.GetDbSqlQuery()->Prepare(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK()) return;
	TableOrm.GetDbSqlQuery()->bindValue(0,strRoleName);
	TableOrm.GetDbSqlQuery()->ExecutePrepared(Ret_pStatus);
	TableOrm.GetDbSqlQuery()->FetchData(Ret_Data);
	if(!Ret_pStatus.IsOK()) return;

	if (Ret_Data.getRowCount()!=0)
		return;

	//insert if not exists:
	Ret_Data.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_ROLE));
	Ret_Data.addRow();
	Ret_Data.setData(0,"BNRO_TABLE_1",nN_TableID);
	Ret_Data.setData(0,"BNRO_TABLE_2",nM_TableID);
	Ret_Data.setData(0,"BNRO_NAME",strRoleName);

	TableOrm.Write(Ret_pStatus,Ret_Data);
	Ret_bRoleInserted=true;
}

//create default assignmnet: shadow copy of client object at: NMRXRelationWidget
void Service_BusNMRX::CreateAssignmentData(Status &Ret_pStatus,DbRecordSet &lstNMRXData,int nMasterTableID,int nAssigmentTableID,DbRecordSet &lstIDForAssign, QString strDefaultRoleName,int nMasterTableRecordID,int nX_TableID, DbRecordSet &lstPersons,bool &Ret_bInserted)
{
	//check role
	DbRecordSet rowRole;
	if (!strDefaultRoleName.isEmpty())
	{
		Ret_bInserted=false;
		CheckRole(Ret_pStatus,nMasterTableID,nAssigmentTableID,strDefaultRoleName,rowRole,Ret_bInserted);
		if(!Ret_pStatus.IsOK()) return;
	}

	if (lstPersons.getRowCount()==0)
	{
		DbRecordSet empty;
		g_BusinessServiceSet->MainEntitySelector->ReadData(Ret_pStatus,ENTITY_BUS_PERSON,empty,lstPersons);
		if(!Ret_pStatus.IsOK()) return;
	}

	lstNMRXData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION_SELECT));
	int nSize=lstIDForAssign.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		lstNMRXData.addRow();
		int nRow=lstNMRXData.getRowCount()-1;
		lstNMRXData.setData(nRow,"BNMR_TABLE_1",nMasterTableID);
		lstNMRXData.setData(nRow,"BNMR_TABLE_2",nAssigmentTableID);
		lstNMRXData.setData(nRow,"BNMR_TABLE_KEY_ID_1",nMasterTableRecordID);
		lstNMRXData.setData(nRow,"BNMR_TABLE_KEY_ID_2",lstIDForAssign.getDataRef(i,0).toInt());

		DbRecordSet rowRecordX;
		NMRXManager::DefineDefaultRecordX(nX_TableID,rowRecordX);

		//issue 2217: set invite by S_C if contact is user!
		if (nAssigmentTableID==BUS_CM_CONTACT)
			if (lstPersons.find("BPER_CONTACT_ID",lstIDForAssign.getDataRef(i,0).toInt(),true)>=0)
			{
				rowRecordX.setData(0,"BCIV_BY_NOTIFICATION",1);
			}
			lstNMRXData.setData(nRow,"X_RECORD",rowRecordX);

			if (rowRole.getRowCount()>0)
			{
				lstNMRXData.setData(nRow,"BNMR_ROLE_ID",rowRole.getDataRef(0,"BNRO_ID"));
				lstNMRXData.setData(nRow,"BNRO_NAME",rowRole.getDataRef(0,"BNRO_NAME"));
				lstNMRXData.setData(nRow,"BNRO_ASSIGMENT_TEXT",rowRole.getDataRef(0,"BNRO_ASSIGMENT_TEXT"));
				lstNMRXData.setData(nRow,"BNRO_DESCRIPTION",rowRole.getDataRef(0,"BNRO_DESCRIPTION").toString());

				lstNMRXData.assignRow(nRow,rowRole,true); //set new role id if defined
			}
	}

	lstNMRXData.setColValue("BNMR_TYPE",QVariant(GlobalConstants::NMRX_TYPE_ASSIGN).toInt());

}

//lstRelations: [id1, id1, role (text)] 
void Service_BusNMRX::WriteRelations(Status &Ret_pStatus, DbRecordSet lstRelations, int nTable1_ID, int nTable2_ID)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_NMRX_RELATION, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	int nSize=lstRelations.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		DbRecordSet rowNewNMRX;
		rowNewNMRX.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION));
		rowNewNMRX.addRow();
		rowNewNMRX.setData(0,"BNMR_TABLE_1",nTable1_ID);
		rowNewNMRX.setData(0,"BNMR_TABLE_2",nTable2_ID);
		rowNewNMRX.setData(0,"BNMR_TABLE_KEY_ID_1",lstRelations.getDataRef(i,0));
		rowNewNMRX.setData(0,"BNMR_TABLE_KEY_ID_2",lstRelations.getDataRef(i,1));
		rowNewNMRX.setData(0,"BNMR_TYPE",GlobalConstants::NMRX_TYPE_ASSIGN);

		QString strRole= lstRelations.getDataRef(i,2).toString();
		if (!strRole.isEmpty())
		{
			DbRecordSet rowRole;
			bool bInserted;
			CheckRole(Ret_pStatus,nTable1_ID,nTable2_ID,strRole,rowRole,bInserted);
			if(!Ret_pStatus.IsOK()) return;
			rowNewNMRX.setData(0,"BNMR_ROLE_ID",rowRole.getDataRef(0,"BNRO_ID"));
		}
		//MR asked to skip if exists:
		QString strWhere=" WHERE BNMR_TABLE_1 ="+QString::number(nTable1_ID)+" AND BNMR_TABLE_2="+QString::number(nTable2_ID);
		strWhere+=QString(" AND BNMR_TABLE_KEY_ID_1 =")+rowNewNMRX.getDataRef(0,"BNMR_TABLE_KEY_ID_1").toString()+" AND BNMR_TABLE_KEY_ID_2="+rowNewNMRX.getDataRef(0,"BNMR_TABLE_KEY_ID_2").toString();
		strWhere+=QString(" AND BNMR_TYPE = 0");
		strWhere+=QString(" AND BNMR_SYSTEM_ROLE =")+rowNewNMRX.getDataRef(0,"BNMR_SYSTEM_ROLE").toString();

		DbRecordSet dataRead;
		TableOrm.Read(Ret_pStatus,dataRead,strWhere);
		if (dataRead.getRowCount()>0)
			continue;

		TableOrm.Write(Ret_pStatus,rowNewNMRX);
		if(!Ret_pStatus.IsOK()) return;
	}
}


//Ret_lstRelations
void Service_BusNMRX::ReadRelations(Status &Ret_pStatus, int nTable1_ID, int nKey1_ID,DbRecordSet &Ret_lstRelations)
{

}
