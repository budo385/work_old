#include "service_appserversession.h"
#include "common/common/config.h"
#include "db/db/dbsimpleorm.h"
#include "common/common/logger.h"


extern Logger				g_Logger;
#include "systemserviceset.h"
extern SystemServiceSet* g_SystemServiceSet;	
#include <QMutexLocker>





Service_AppSrvSession::Service_AppSrvSession()
:mutex(QMutex::Recursive) //same thread can lock N times
{


}

//#include "bus_core/bus_core/servercontrolabstract.h"
//extern ServerControlAbstract *g_AppServer;
//#define _APP_SERVER(funct)	if(g_AppServer)g_AppServer->funct;

/*!
	Reads app. server session data

	\param pStatus				- if err occur, returned here
	\param nAppServerID			- session id
	\param lstRecords			- full view of readed records
	\param pDbConn				- db conn
*/
void Service_AppSrvSession::Read(Status &pStatus,int  nAppServerID,DbRecordSet& lstRecords)
{
	//make local query, can fail if connection reservation fails
	DbSimpleOrm TableOrm(pStatus,CORE_APP_SRV_SESSION,GetDbManager()); if(!pStatus.IsOK()) return;

	//read:
	TableOrm.Read(pStatus,lstRecords,"WHERE COAS_ID= "+QVariant(nAppServerID).toString());

}



/*!
	Reads all entries from table for application servers only. For load balancing process

	\param pStatus				- if err occur, returned here
	\param lstRecords			- list in TVIEW_CORE_APP_SRV_SESSION_STATUS format
	\param pDbConn				- db conn
*/
void Service_AppSrvSession::ReadLBOList(Status &pStatus,DbRecordSet& lstRecords)
{
	//get active sessions:
	DbRecordSet lstData=GetSessions(pStatus,true,QVariant(SRV_ROLE_APPSERVER).toInt());
	lstRecords.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CORE_APP_SRV_SESSION_STATUS));
	lstRecords.merge(lstData);
}





/*!
	Deletes app. srv session entry :cascade delete -> user session -> user locking

	\param pStatus				if err occur, returned here
	\param nAppServerID			ID of app session to delete
	\param pDbConn				db conn
*/
void Service_AppSrvSession::StopSession(Status &pStatus,int  nAppServerID)
{
	QMutexLocker locker(&mutex);

	//make local query, can fail if connection reservation fails
	DbSimpleOrm TableOrm(pStatus,CORE_APP_SRV_SESSION,GetDbManager()); 
	if(!pStatus.IsOK()) return;

	QList<int> lstID;
	lstID<<nAppServerID;

	//delete all session data along with user data (locked)
	TableOrm.DeleteFast(pStatus,lstID);
}






/*!
	1. Check if license ID (keyfile) is valid (all app. servers must use same), if any1 uses other one, abort (only for APP_SERVERS), return ERROR
	2. On startup, act ac master gbc -> clean old expired sessions, or take over if app. server
	3. If 1. and 2. passes (clean start), insert new session, start it!!!

	\param pStatus				if err occur, returned here
	\param nOldAppSrvSessionID	old session id, 0 if non valid, used for garbage collector if server crashes, compared by COAS_ID
	\param nServerMode			THICK or APP. SERVER -> see serversessiondata.h
	\param lstRecords			filled with new session data, TVIEW_CORE_APP_SRV_SESSION full view;
	\param pDbConn				db conn
*/
void Service_AppSrvSession::StartSession(Status &pStatus, int nServerMode,DbRecordSet& lstRecords)
{
	QMutexLocker locker(&mutex);

	//make local query, can fail if connection reservation fails
	DbSimpleOrm TableOrm(pStatus,CORE_APP_SRV_SESSION,GetDbManager()); 
		if(!pStatus.IsOK()) return;

	//user query from ORM direct (faster):
	//DbSqlQuery* query=TableOrm.GetDbSqlQuery();
	bool bEdit=false;


	if(nServerMode!=SRV_ROLE_THICK)
	{

		//TEST UNIQUE LICENSE ID (not for thicks):
		//-------------------------------------------------
		/*
		int nUniqueLicenseID=lstRecords.getDataRef(0,"COAS_UNIQUE_LICENSE_ID").toInt();
		QString strSQL="SELECT COUNT(*) FROM CORE_APP_SRV_SESSION WHERE COAS_ROLE_ID <> "+QVariant((int)SRV_ROLE_THICK).toString()+" AND COAS_UNIQUE_LICENSE_ID <> "+QVariant(nUniqueLicenseID).toString();
		query->Execute(pStatus,strSQL);
		if(!pStatus.IsOK()) return;
		//get count:
		int nCount=0;
		if(query->GetQSqlQuery()->next())
			nCount=query->GetQSqlQuery()->value(0).toInt();
		//if exists, it means: some1 is using older module license, or this is older license
		if(nCount>0)
		{
			pStatus.setError(StatusCodeSet::ERR_SYSTEM_MOD_LICENSE_INVALID);
			return;
		}
		*/

		//check if other LBO or MASTER exists in system, just fire warning
		//----------------------------------------------------------------
		if (lstRecords.getDataRef(0,"COAS_IS_LBO").toInt()!=0 || lstRecords.getDataRef(0,"COAS_IS_MASTER").toInt()!=0)
		{
			int nLBO,nMaster;
			CheckServers(pStatus,nLBO,nMaster);
			if(!pStatus.IsOK()) return;

			if (nLBO!=0 || nMaster!=0)
			{
				QString strMsg=QVariant(nLBO).toString()+";"+QVariant(nMaster).toString();
				g_Logger.logMessage(StatusCodeSet::TYPE_WARNING,StatusCodeSet::ERR_SYSTEM_MASTER_DETECTED,strMsg);
			}
		}

	}



	//write new session:
	TableOrm.Write(pStatus,lstRecords);
	if(!pStatus.IsOK()) return;

	//get new app id:
	int nAppID=lstRecords.getDataRef(0,"COAS_ID").toInt();

	//clean expired sessions:
	if (nServerMode==SRV_ROLE_THICK)
	{
		CleanExpiredThickSessions(pStatus);  //clean only thick sessions
	}
	else
	{
		CheckServerSessions(pStatus,nAppID); //cleans all: thick and app. srv
	}
}


/*!
	Update last act, current connections.

	\param pStatus				if err occur, returned here
	\param nAppSrvSessionID		App srv session ID (int)
	\param pDbConn				db conn
*/
void Service_AppSrvSession::UpdateLastActivity(Status &pStatus,int nAppSrvSessionID)
{
	QMutexLocker locker(&mutex);

	//make local query, can fail if connection reservation fails
	DbSqlQuery query(pStatus,GetDbManager());
	if(!pStatus.IsOK()) return;


	//PREPARE
	QString strSQL="UPDATE CORE_APP_SRV_SESSION SET COAS_DAT_LAST_ACTIVITY= ? WHERE COAS_ID=?";
	query.Prepare(pStatus,strSQL);
	if(!pStatus.IsOK())
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_APP_SESSION_UPDATE_FAILED,"UpdateLastActivity prepare failed "+pStatus.getErrorText());
		g_Logger.FlushFileLog();
		return;
	}

	//bind vars:
	query.bindValue(0,QDateTime::currentDateTime());
	query.bindValue(1,nAppSrvSessionID);

	//exec & auto destroy any connection held:
	query.ExecutePrepared(pStatus);
	if(!pStatus.IsOK())
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_APP_SESSION_UPDATE_FAILED,"UpdateLastActivity failed "+pStatus.getErrorText());
		g_Logger.FlushFileLog();
		return;
	}

	strSQL="SELECT COAS_DAT_LAST_ACTIVITY FROM CORE_APP_SRV_SESSION WHERE COAS_ID="+QVariant(nAppSrvSessionID).toString();
	query.Execute(pStatus,strSQL);
	if(!pStatus.IsOK())
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_APP_SESSION_UPDATE_FAILED,"UpdateLastActivity select failed "+pStatus.getErrorText());
		g_Logger.FlushFileLog();
		return;
	}

	DbRecordSet tmp;
	query.FetchData(tmp);
	if (tmp.getRowCount()==1)
	{
		QDateTime dat=tmp.getDataRef(0,"COAS_DAT_LAST_ACTIVITY").toDateTime();
		if (QDateTime::currentDateTime()<dat)
		{
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_APP_SESSION_UPDATE_FAILED,"UpdateLastActivity update failed, date is not updated to new value "+pStatus.getErrorText());
			g_Logger.FlushFileLog();
			return;
		}
	}
}




/*!
	Counts all connections for one app. server: only active ones!!

	\param pStatus				- if err occur, returned here
	\param nAppSrvSessionID		- App srv session ID (int)
	\param pDbConn				- db conn
*/
void Service_AppSrvSession::UpdateCurrentConnection(Status &pStatus,int nAppSrvSessionID)
{
	//B.T.: do not update session count anymore:
	return;

	QMutexLocker locker(&mutex);

	//make local query, can fail if connection reservation fails
	DbSqlQuery query(pStatus,GetDbManager());
	if(!pStatus.IsOK()) return;


	//PREPARE: count all sessions:
	QString strSQL="UPDATE CORE_APP_SRV_SESSION SET COAS_CURRENT_CONNECTIONS = (SELECT COUNT(*) FROM CORE_USER_SESSION WHERE COUS_ACTIVE_FLAG=1 AND COUS_APP_SRV_SESSION_ID=?) WHERE COAS_ID =?";
	query.Prepare(pStatus,strSQL);
	if(!pStatus.IsOK()) //unlock if error:
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_APP_SESSION_UPDATE_FAILED,"UpdateCurrentConnection prepare failed "+pStatus.getErrorText());
		return;
	}


	//bind vars:
	query.bindValue(0,nAppSrvSessionID);
	query.bindValue(1,nAppSrvSessionID);

	//exec & auto destroy any connection held:
	query.ExecutePrepared(pStatus);
	if(!pStatus.IsOK()) //unlock if error:
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_APP_SESSION_UPDATE_FAILED,"UpdateCurrentConnection failed "+pStatus.getErrorText());
		return;
	}
	
}



/*!
	Checks all entries in CORE_APP_SRV_SESSION if COAS_DAT_LAST_ACTIVITY expired APP_SESSION_REPORT_INTERVAL interval
	If detects expired server session (or thick or job server,..):
	- lock's all user session entries, marks them inactive, and set it as owner
	- lock's and deletes entry

	NEGATIVE:
	1. new session takes valid ID...ID must not reuse old numbers...
	2. just writes, when update...

	\param pStatus				- if err occur, returned here
	\param nAppSrvSessionID		- App srv session ID (int)
	\param pDbConn				- db conn
*/
void Service_AppSrvSession::CheckServerSessions(Status &pStatus,int nAppServerSessionID)
{
	QMutexLocker locker(&mutex);

	//make local query, can fail if connection reservation fails
	DbSqlQuery query(pStatus,GetDbManager());
	if(!pStatus.IsOK()) return;

	//clean THICK's (do not transfer them:
	CleanExpiredThickSessions(pStatus);
	if(!pStatus.IsOK()) return;

	//get expired sessions:
	DbRecordSet lstExpired=GetSessions(pStatus,false,QVariant(SRV_ROLE_APPSERVER).toInt());
	if(!pStatus.IsOK()) return;

	
	//get expired sessions:
	if(lstExpired.getRowCount()==0) return; //if 0 everything OK:

	QList<int> lstID;
	DbSqlTableDefinition::CopyFromRecordSet2List(CORE_APP_SRV_SESSION,lstID,lstExpired);

	
	//begin tran
	query.BeginTransaction(pStatus);
	if(!pStatus.IsOK()) return;

	//transfer session from dead server to here
	int nSize=lstID.size();
	for(int i=0;i<nSize;++i)
	{
		//if itself session expired->fire an error, but DO NOT delete it:
		if (lstID.at(i)==nAppServerSessionID)
		{
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_APP_SESSION_MASTER_ENCOUNTER_OWN);
			continue;
		}

		//transfer them:
		g_SystemServiceSet->UserSession->TransferSessions(pStatus,nAppServerSessionID,lstID.at(i));
		if(!pStatus.IsOK())
		{
			query.Rollback();
			return;
		}
		
		//delete invalid app. session entry:
		QString strSQL="DELETE FROM CORE_APP_SRV_SESSION WHERE COAS_ID ="+QVariant(lstID.at(i)).toString();
		query.Execute(pStatus,strSQL);
		if(!pStatus.IsOK())
		{
			query.Rollback();
			return;
		}
		g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_APP_SESSION_DELETED,QVariant(lstID.at(i)).toString());
	}

	//commit
	query.Commit(pStatus);
}


//gets either active or inactive seesions from core_app_srv_session
//returned list is full table TVIEW_CORE_APP_SRV_SESSION
DbRecordSet Service_AppSrvSession::GetSessions(Status &pStatus,bool bActive,int nRole)
{
	//make local query, can fail if connection reservation fails
	DbSqlQuery query(pStatus,GetDbManager());
	if(!pStatus.IsOK()) return DbRecordSet();

	QString strSQLCols=DbSqlTableView::getSQLColumnsFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CORE_APP_SRV_SESSION));

	QDateTime datTimeExpired=GetDbManager()->GetCurrentDateTime(pStatus,query.GetDbConnection()).addSecs(-APP_SESSION_REPORT_INTERVAL_CHECK);
	if(!pStatus.IsOK()) return DbRecordSet();



	QString strSQL;
	if (!bActive)
		strSQL="SELECT "+strSQLCols+" FROM CORE_APP_SRV_SESSION WHERE COAS_ROLE_ID = "+QVariant(nRole).toString()+" AND COAS_DAT_LAST_ACTIVITY<?";
	else
		strSQL="SELECT "+strSQLCols+" FROM CORE_APP_SRV_SESSION WHERE COAS_ROLE_ID = "+QVariant(nRole).toString()+" AND COAS_DAT_LAST_ACTIVITY>?";
	
	query.Prepare(pStatus,strSQL);
	if(!pStatus.IsOK()) return DbRecordSet();
	query.bindValue(0,datTimeExpired);
	query.ExecutePrepared(pStatus);
	if(!pStatus.IsOK()) return DbRecordSet();

	//get expired sessions:
	DbRecordSet lstSessions;
	query.FetchData(lstSessions);
	return lstSessions;
}



//cleans session table from expired thick sessions
void Service_AppSrvSession::CleanExpiredThickSessions(Status &pStatus)
{
	QMutexLocker locker(&mutex);

	//make local query, can fail if connection reservation fails
	DbSimpleOrm TableORM(pStatus,CORE_APP_SRV_SESSION,GetDbManager());
	if(!pStatus.IsOK()) return;

	//get expired sessions:
	DbRecordSet lstExpired=GetSessions(pStatus,false,QVariant(SRV_ROLE_THICK).toInt());
	if(!pStatus.IsOK()) return;

	//get expired sessions:
	if(lstExpired.getRowCount()==0) return; //if 0 everything OK:

	//delete them:
	TableORM.DeleteFast(pStatus,lstExpired);
}


//cleans session table: after restore
void Service_AppSrvSession::CleanAllSessions(Status &pStatus)
{
	QMutexLocker locker(&mutex);

	//make local query, can fail if connection reservation fails
	DbSqlQuery query(pStatus,GetDbManager());
	if(!pStatus.IsOK()) return;

	//delete invalid app. session entry:
	QString strSQL="DELETE FROM CORE_APP_SRV_SESSION";
	query.Execute(pStatus,strSQL);

}


void Service_AppSrvSession::CheckServers(Status &pStatus,int& nCnt_ActiveLBOServers, int& nCnt_ActiveMasterServers)
{
	//get sesions
	DbRecordSet lstSessions=GetSessions(pStatus,true,QVariant(SRV_ROLE_APPSERVER).toInt());
	if(!pStatus.IsOK()) return;

	nCnt_ActiveLBOServers=lstSessions.find("COAS_IS_LBO",1);
	nCnt_ActiveMasterServers=lstSessions.find("COAS_IS_MASTER",1);
}

void Service_AppSrvSession::GetUpdateDate(Status &pStatus,QDate &dateUpdate)
{
	//make local query, can fail if connection reservation fails
	DbSqlQuery query(pStatus,GetDbManager());
	if(!pStatus.IsOK()) return;

	//delete invalid app. session entry:
	QString strSQL="SELECT  CDI_SERVER_UPDATE_DATE FROM CORE_DATABASE_INFO";
	query.Execute(pStatus,strSQL);

	dateUpdate=QDate(); //set to  invalid date

	if (query.next())
	{
		dateUpdate=query.value(0).toDate();
	}
	
}