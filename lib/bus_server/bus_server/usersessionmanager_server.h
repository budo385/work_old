#ifndef USERSESSIONMANAGER_THIN_H
#define USERSESSIONMANAGER_THIN_H

#include "bus_core/bus_core/usersessionmanager.h"
#include "systemserviceset.h"
#include <QWriteLocker>
#include <QReadLocker>
#include <QList>
#include "common/common/config.h"


/*!
    \class UserSessionManager_Server
    \brief Used for managing user sessions on server
    \ingroup Bus_Server

	Keeps track of all user session info at app. server. Caches all user context key data such as 
	socket_id, thread_id, session_id, user_id,...
	Multithread Safe!!
	Used for session validation process by httprpc call dispatcher, by garbage colllector and fetching other user context data
	on server side from business objects.
	This is last line of defense for business layer.
	Used in thick/thin mode.
	Supports DFO process (take over some1 other app. server user session).
	Supports LBO process (redirecting client).
	NOTE: current session is recognized by Thread descriptor, 
	QThread in which code is executing must be inherited from ServerThread, 
	otherwise thread will be recognized as global/unique with -1 thread ID (thick)!

	New entry in session list is made from httpserver dispatcher or from Login BO procedure.
	When session is DEAD, its status is set to EXPIRED, session data + socket is deleted by garbage collector later on.

	MODUS OPERANDI

	//ValidateUserRequest():

	//1. Find by ThreadID
	//2. If not found->new request
	//3. If found compare sessions
	//4. If thread/sesson match, check status=must be OK for normal ops (if not issue log-reauth), reauth, login for Login


	//2. Log new request, set session empty
	//2.1. If not login and session='' reject
	//2.1.2 If not/login and session<>'':
	//2.1.5 If incoming session<>'': (result new entry with empty session &login/reauth)
		//2.1 Check incoming session, if not found in list, issue reauth (if not already login), else if login pass
		//2.2 Check incoming session, if found under different Thread, , issue reauth (if not already login), else if login pass
		
	//3. If found Thread IDcompare sessions:
	//3.05 If expired buzz off!!  
	//3.1 NonEmpty both:
	//3.1 If not same (different in list and incoming call), possbile error, set SET_EXPIRED, disconnect, reject call)
	//3.2 Empty inc, non empty in list:
	//3.2 possbile error, , set SET_EXPIRED, disconnect, reject call(kill session)
	//3.3 Empty inc, empty in list:
	//3.3 if login try, then OK, probably failed to login, else set SET_EXPIRED, ERR_SYSTEM_KICKBAN, disconnect, reject call (didnt even tried to login and it wants to access)
	//3.4 NonEmpty inc, empty in list:
	//3.5 if login try, then OK, DFO in process, else set SET_EXPIRED,ERR_SYSTEM_KICKBAN, disconnect, reject call (didnt even tried to login and it wants to access)


	//4. If thread/sesson match
	//OK



	//From Login Method
	//1. Inc Session=empty
	//1. Test AC rights, if not allowed or err, return error, dont even touch session list...it will be expired and removed by garbage
	//1.1 Login OK, call CreateSession(), everything OK, session in list must be empty before...()...
	//2. Inc Session=Nonempty
	//2.1 Test AC rights (reauth), if not allowed or err, dont even touch session list)
	//2.2 If Ok call UpdateSession(strSession,usr,grp, module);

	//From UpdateSession
	//1. ThreadID must exists in list or return FATAL error (recover: set expired, return error)!
	//2. When Find ID, compare Sessions inc is nonEmpty:
	//2.1 NonEmpty in list, -> FATAL error (recover: set expired, return error)!
	//2.2 Empty in list: 
	//2.2.1 - Check if inc session exists in list with another ID = set expired->clear session to "", delete it!!!!!
	//2.2.2 - If (stored=false), find in session table, if found check app server is alive, 
	//		  if this is our app. server then nothing DO, else update to new app_id, set to true, else just insert new
	//2.2.3 - update store new session_id in list, clear status.

	//If app.server is alive or another Err, return FATAL ERROR, set expired


*/
class UserSessionManager_Server : public UserSessionManager //public QObject
{ 
	//Q_OBJECT

public:

	UserSessionManager_Server(
		SystemServiceSet* pServices,
		LicenseAccessRightsBase *pModuleLicenseManager,
		int nUserSessionTimeOut, 
		int nAppServerSessionID, 
		QString strServerIP,
		int nMaxSessions, 
		bool bAcceptDFOConnections=false,
		int nAuthTimeout=SESSION_AUTHENTICATION_TIMEOUT, 
		int nAuthTries=SESSION_AUTHENTICATION_MAX_ATTEMPTS, 
		int nKickBanPeriod=SESSION_KICKBAN_PERIOD,
		int nCheckSession=SESSION_CHECK_INTERVAL);
	~UserSessionManager_Server();


	/*! SessionState defines state of session */
	enum SessionState {
		STATE_OK=0,				///<Session is alive and authenticated, reset counters
		STATE_LOGIN,			///<Session is empty, login in process, set counter=0, set lastActivity or track both
		STATE_REAUTHENTICATE,	///<Session was given order to reauthenticate, set counter=0, set lastActivity or track both
		STATE_EXPIRED			///<Session expired
		};			


	//managing functions
	void CreateSession(Status &pStatus,QString &strSession,DbRecordSet &rowUserData, QString strModuleCode, QString strMLIID,bool bIsSystemThread=false,QString strParentSession="", int nClientTimeZoneOffsetMinutes=0);
	void CreateSessionForWebService(Status &pStatus,QString &strSession,DbRecordSet rowUserData, int nClientTimeZoneOffsetMinutes=0);
	void DeleteSession(Status &pStatus, QString strSession);
	void InactiveSession(Status &pStatus,int nThreadID);		
	void UpdateSession(Status &pStatus,QString strSession,DbRecordSet &rowUserData, QString strModuleCode, QString strMLIID);
	void ValidateUserRequest(Status &pStatus,QString strSession,int &nClientTimeZoneOffsetMinutes, QString strClientIP="",bool IsLogin=false,int nSocketID=0);

	bool AuthenticateUser(QString strCookieValue,QString &strSessionID);
	void ValidateWebUserRequest(Status &pStatus,QString strSession,const HTTPContext &ctx, int &nClientTimeZoneOffsetMinutes);

	//garbage collector functions
	void DeleteExpiredUserSessions(Status &err,UserSessionList &pListExpiredSessions);
	void DeleteOrphanUserSessions(Status &err,UserSessionList lstToDelete);
	void TestExpiredUserSessions(Status &err,UserSessionList &pListExpiredSessions);
	void GetSessionList(UserSessionList &pListSessions); 
	void ClientIsAlive(int nSocketID);

	//context helper functions
	QString GetSessionID();
	int		GetUserID(QString strSession="");
	int		GetSessionRecordID(QString strSession=""); //ID stored in the DB
	int		GetPersonID(QString strSession="");
	int		GetContactID(QString strSession="");
	bool	IsSystemUser(QString strSession="");
	int		GetSocketID(QString strSession="");
	QString GetClientIP(QString strSession="");
	void	GetUserData(DbRecordSet &rowUserData,QString strSession="");
	void	GetModuleInfo(QString& strModuleCode, QString& strMLIID); //different for each logged user
	void	LockThreadActive();
	void	UnLockThreadActive();
	QStringList GetSocketListForPersonID(int nPersonID);
	void	ClearUserSessionStorage(QString strSession="");
	int		GetClientTimeZoneOffsetMinutes(QString strSession="");

	void	DeleteSessionByThreadID(Status &pStatus,int nThreadID);		
	void	DeleteSessionBySocketID(Status &pStatus,int nSocketID);		
	int		GetThreadBySocketID(int nSocketID);
	int		GetSessionCount();


//protected:
	//QString GetSessionIDBySocketID(int nSocketID);
	
private:

	UserSessionList m_SessionList;			///<dynamic, access is serialized, memory list of all sessions at app.server
	QList<QString> m_lstServerNonce;

	int		FindSession(QString strSession);
	int		FindSocket(int nSocketID);
	void	ReAuthenticateSession(Status &err, int State, UserSessionData &SessionRow);
	void	CheckSession(Status &err, UserSessionData &SessionRow);
	void	LBORedirect(Status &err);
	void	CheckMaxSessions(Status &err);
	void	RemoveChildSessionsFromMemory(QString strParent);
	
	QReadWriteLock m_SessionListRWLock;		///< RWlock for accessing m_SessionList
	QReadWriteLock m_ServerNonceListRWLock;		///< RWlock for accessing m_SessionList

	SystemServiceSet* m_Services;
	int m_nUserSessionTimeOut;
	int m_nAppServerSessionID;
	int m_nAuthTimeout;
	int m_nAuthTries;
	int m_nMaxSessions;
	int m_nKickBanPeriod;
	int m_nCheckSessionTimePeriod;
	int m_bAcceptDFOConnections;
};


#endif //USERSESSIONMANAGER_THIN_H



