#ifndef SERVICE_BUSGRIDVIEWS_H
#define SERVICE_BUSGRIDVIEWS_H


#include "businessservice.h"
#include "bus_interface/bus_interface/interface_busgridviews.h"



/*!
	\class Service_BusGridViews
	\brief Addr schemas
	\ingroup Bus_Services
*/

class Service_BusGridViews : public Interface_BusGridViews, public BusinessService
{
public:

	//basic ops:
	void Write(Status &Ret_pStatus, DbRecordSet &RetOut_Data,QString strLockRes,QString strOldView,int nGridID);
	void Read(Status &Ret_pStatus, int nGridID,DbRecordSet &Ret_Data);
	void Delete(Status &Ret_pStatus, QString strView,int nGridID);
	void Lock(Status &Ret_pStatus, QString strView,QString &Ret_strLockRes,int nGridID);
	void Unlock(Status &Ret_pStatus, QString strLockRes);

};

#endif // SERVICE_BUSGRIDVIEWS_H
