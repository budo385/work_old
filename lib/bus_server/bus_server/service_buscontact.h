#ifndef SERVICE_CONTACTSELECTOR_H
#define SERVICE_CONTACTSELECTOR_H



#include "businessservice.h"
#include "bus_interface/bus_interface/interface_buscontact.h"


/*!
	\class Service_BusContact
	\brief Special implementation for complex contact data
	\ingroup Bus_Services

*/

class Service_BusContact : public Interface_BusContact, public BusinessService
{
public:
	void ReadShortContactList(Status &Ret_pStatus, DbRecordSet Filter, DbRecordSet &Ret_Data);
	void ReadContactDataSideBar(Status &Ret_pStatus, int nContactID,DbRecordSet &Ret_Data,DbRecordSet &DataNMRXPersonFilter,DbRecordSet &Ret_DataNMRXPerson,DbRecordSet &DataNMRXProjectFilter,DbRecordSet &Ret_DataNMRXProject,bool bLoadPicture, DbRecordSet &Ret_Pictures);
	void DeleteContacts(Status &Ret_pStatus, DbRecordSet &DataForDelete,bool bLock);
	void BatchReadContactData(Status &Ret_pStatus, int nContactID,DbRecordSet &Ret_Data,DbRecordSet &DataNMRXPersonFilter,DbRecordSet &Ret_DataNMRXPerson,DbRecordSet &DataNMRXProjectFilter,DbRecordSet &Ret_DataNMRXProject,int nSubDataFilter,DbRecordSet &Ret_UAR,DbRecordSet &Ret_GAR);
	void ReadNMRXContactsFromContact(Status &Ret_pStatus, int nContactID,DbRecordSet &Ret_Data);
	void ReadNMRXContactsFromContact_Ext(Status &Ret_pStatus, int nContactID,DbRecordSet &Ret_NMRXData,DbRecordSet &Ret_ContactData);
	void ReadNMRXUsersFromContact_Ext(Status &Ret_pStatus, int nContactID,DbRecordSet &Ret_NMRXData,DbRecordSet &Ret_UserData);
	void ReadNMRXContactsFromProject(Status &Ret_pStatus, int nProjectID,DbRecordSet &Ret_Data);
	void ReadNMRXProjects(Status &Ret_pStatus, int nContactID,DbRecordSet &Ret_Data);
	void ReadContactDefaults(Status &Ret_pStatus, int nContactID, DbRecordSet &Ret_Data);
	void ReadContactReminderData(Status &Ret_pStatus, int nContactID, int nPersonID,QString strEmail, QString strMobilePhoneNumber);
	void ReadContactDataForVoiceCall(Status &Ret_pStatus, int nContactID, DbRecordSet &Ret_Phones,DbRecordSet &Ret_Pictures,DbRecordSet &Ret_NMRXContacts,DbRecordSet &Ret_NMRXProjects);
	void AddContactEmail(Status &Ret_pStatus, DbRecordSet &RetOut_EmailData);
	void UpdateOrganization(Status &Ret_pStatus,QString strPrevOrg,QString strNewOrg);
	void CreatePersonFromContact(Status &Ret_pStatus, DbRecordSet &lstContacts,int nOrgID, int &Ret_nPersonCreated);
	
	//pic general
	void ReadBigPicture(Status &Ret_pStatus, int nBigPicID,DbRecordSet &Ret_Data);
	void WriteBigPicture(Status &Ret_pStatus, DbRecordSet &RetOut_Data);
	void WriteEntityPictures(Status &Ret_pStatus, DbRecordSet &RetOut_Data, QString strBigPicIDColName, QString strPreviewPicColName);
	void ReadEntityPictures(Status &Ret_pStatus,DbRecordSet &Ret_Data,QString strBigPicIDColName);
	//contact pic_
	void ReadPreviewPicture(Status &Ret_pStatus, int nContactID,DbRecordSet &Ret_Data);
	void AssignPictureContact(Status &Ret_pStatus, int nContactID, QByteArray &PreviewPicture,QByteArray &BigPicture, int &Ret_nBigPicID);
	void RemoveAssignedPictureContact(Status &Ret_pStatus, int nContactID);
	//journal:
	void ReadContactJournals(Status &Ret_pStatus, int nContactID, DbRecordSet &Ret_Data);
	void WriteJournal(Status &Ret_pStatus, DbRecordSet &RetOut_Data,DbRecordSet &DataForDelete,QString strLockRes);
	void DeleteJournal(Status &Ret_pStatus, DbRecordSet &DataForDelete,bool bLock);
	void LockJournal(Status &Ret_pStatus, int nJournalID, QString &Ret_strLockRes);
	void UnlockJournal(Status &Ret_pStatus, QString strLockRes);
	//Create person from contact (used in startup):
	void CreateDefaultPerson(Status &Ret_pStatus,int nContactID);
	void CopyPerson2OrgContact(Status &Ret_pStatus,DbRecordSet Contacts,DbRecordSet lstGroups, DbRecordSet &Ret_Data);
	void FindDuplicates(Status &Ret_pStatus,DbRecordSet &Contacts,DbRecordSet &Ret_Data);
	void GetMaximumDebtorCode(Status &Ret_pStatus,QString &Ret_DebtorCode);
	//Main:
	void ReadData(Status &Ret_pStatus, DbRecordSet Filter, DbRecordSet &Ret_Data,int nSubDataFilter=0);
	void WriteData(Status &Ret_pStatus,DbRecordSet &RetOut_Data,int nSubDataFilter, QString pLockResourceID, DbRecordSet &RetOut_UAR,DbRecordSet &RetOut_GAR);
	void WriteDataFromImport(Status &Ret_pStatus,DbRecordSet &RetOut_Data);
	//Groups:
	void WriteGroupAssignments(Status &Ret_pStatus,DbRecordSet &RetOut_Data);
	void ReadContactGroup(Status &Ret_pStatus, int nContactID, DbRecordSet &Ret_Data);
	void ReadContactsFromGroup(Status &Ret_pStatus, QString strUniqueGroupIdentifier,DbRecordSet &Ret_Data);
	void ReadActiveContactsFromGroups(Status &Ret_pStatus, QString strGroupIDs,DbRecordSet &Ret_Data);

	//Phones:
	void WritePhones(Status &Ret_pStatus,int nContactID, DbRecordSet &RetOut_Data);
	void ReadContactPhones(Status &Ret_pStatus, int nContactID,DbRecordSet &Ret_Data, int nQueryRead=-1,DbRecordSet lstBySystemType=DbRecordSet());
	
	//Address:
	void WriteAddress(Status &Ret_pStatus,int nContactID, DbRecordSet &RetOut_Data);
	void ReadContactAddress(Status &Ret_pStatus, DbRecordSet &lstOfContactIds, DbRecordSet &Ret_Data);
	void GetAllContactsOfOrganizationByAddress(Status &Ret_pStatus, int nContactIDOrg, DbRecordSet rowAddress,DbRecordSet &Ret_AddressIds);
	void UpdateContactsOfOrganizationByAddress(Status &Ret_pStatus, DbRecordSet &lstAddressIds, DbRecordSet rowAddress);

	//Assign Debtors 2 Project
	void AssignDebtors2Project(Status &Ret_pStatus, int nProjectID, DbRecordSet &lstDebtors);

private:
	void WriteOneContact(Status &Ret_pStatus,DbRecordSet &RetOut_Data,int nSubDataFilter,DbRecordSet &lstUAR,DbRecordSet &lstGAR);

};

#endif // SERVICE_CONTACTSELECTOR_H
