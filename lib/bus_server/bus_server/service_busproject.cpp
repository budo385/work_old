#include "service_busproject.h"
#include "db/db/dbsimpleorm.h"
#include "bus_core/bus_core/globalconstants.h"

#include "bus_core/bus_core/usersessionmanager.h"
extern UserSessionManager	*g_UserSessionManager;
#include "businessserviceset.h"
extern BusinessServiceSet* g_BusinessServiceSet;

#include "bus_core/bus_core/useraccessright.h"
extern UserAccessRight *g_AccessRight;				//global access right tester

Service_BusProject::Service_BusProject()
{
	m_ProjectData.Initialize(BUS_PROJECT,GetDbManager());
}

void Service_BusProject::ReadData(Status &Ret_pStatus, int nProjectID,DbRecordSet &Ret_Data,DbRecordSet &DataNMRXPersonFilter,DbRecordSet &Ret_DataNMRXPerson,DbRecordSet &Ret_UAR,DbRecordSet &Ret_GAR, DbRecordSet &Ret_CustomFlds)
{
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_PROJECT, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	QString strWhere;
	strWhere=" WHERE BUSP_ID = "+QVariant(nProjectID).toString()+" ORDER BY BUSP_CODE"; 

	//filter by UAR
	g_AccessRight->SQLFilterRecords(BUS_PROJECT, strWhere);

	//load projects
	TableOrm.Read(Ret_pStatus,Ret_Data,strWhere,DbSqlTableView::TVIEW_BUS_PROJECT);
	if(!Ret_pStatus.IsOK()) return;
	//read NMRX
	g_BusinessServiceSet->BusNMRX->ReadMultiPairs(Ret_pStatus,DataNMRXPersonFilter,Ret_DataNMRXPerson);
	if(!Ret_pStatus.IsOK()) return;
	//read uar/gar:
	g_BusinessServiceSet->AccessRights->ReadGUAR(Ret_pStatus,nProjectID,BUS_PROJECT,Ret_UAR,Ret_GAR);
	if(!Ret_pStatus.IsOK()) return;

	//read custom data:
	g_BusinessServiceSet->BusCustomFields->ReadUserCustomData(Ret_pStatus,BUS_PROJECT,nProjectID,Ret_CustomFlds);

}

void Service_BusProject::WriteData(Status &Ret_pStatus,DbRecordSet &RetOut_Data, QString pLockResourceID, DbRecordSet &RetOut_UAR,DbRecordSet &RetOut_GAR, DbRecordSet &RetOut_CustomFlds)
{
	//Make local query, can fail if connection reservation fails.
	DbSqlQuery query(Ret_pStatus, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	query.BeginTransaction(Ret_pStatus);
	if(!Ret_pStatus.IsOK()) 
	{
		return;
	}

	//write project (lock/unlock)....tricky as it is inside transaction...
	m_ProjectData.Write(Ret_pStatus,RetOut_Data,pLockResourceID,"");
	if(!Ret_pStatus.IsOK()) 
	{
		query.Rollback();
		return;
	}


	int nProjectID=RetOut_Data.getDataRef(0,"BUSP_ID").toInt();

	//write custom flds:
	if (RetOut_CustomFlds.getRowCount()>0)
	{
		g_BusinessServiceSet->BusCustomFields->WriteUserCustomData(Ret_pStatus,BUS_PROJECT,nProjectID,RetOut_CustomFlds);
		if(!Ret_pStatus.IsOK()) 
		{
			query.Rollback();
			return;
		}
	}
	
	//write project UAR/GAR 
	g_BusinessServiceSet->AccessRights->WriteGUAR(Ret_pStatus,nProjectID,BUS_PROJECT,RetOut_UAR,RetOut_GAR);
	if(!Ret_pStatus.IsOK()) 
	{
		query.Rollback();
		return;
	}
	g_BusinessServiceSet->AccessRights->UpdateProjectGUAR(Ret_pStatus,RetOut_Data);
	if(!Ret_pStatus.IsOK()) 
	{
		query.Rollback();
		return;
	}
	//re-read uar/gar (only for inherit-enabled entity: project, voice_call, email, doc)
	g_BusinessServiceSet->AccessRights->ReadGUAR(Ret_pStatus,nProjectID,BUS_PROJECT,RetOut_UAR,RetOut_GAR);
	if(!Ret_pStatus.IsOK()) 
	{
		query.Rollback();
		return;
	}
		
	query.Commit(Ret_pStatus);
}

void Service_BusProject::ReadProjectDataSideBar(Status &Ret_pStatus, int nProjectID,DbRecordSet &Ret_Data,DbRecordSet &DataNMRXPersonFilter,DbRecordSet &Ret_DataNMRXPerson)
{
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_PROJECT, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	QString strWhere;
	strWhere=" WHERE BUSP_ID = "+QVariant(nProjectID).toString()+" ORDER BY BUSP_CODE"; 

	//filter by UAR
	g_AccessRight->SQLFilterRecords(BUS_PROJECT, strWhere);

	//load all
	TableOrm.Read(Ret_pStatus,Ret_Data,strWhere,DbSqlTableView::TVIEW_BUS_PROJECT);
	if(!Ret_pStatus.IsOK()) return;

	//Ret_Data.Dump();

	g_BusinessServiceSet->BusNMRX->ReadMultiPairs(Ret_pStatus,DataNMRXPersonFilter,Ret_DataNMRXPerson);
	if(!Ret_pStatus.IsOK()) return;
}

void Service_BusProject::NewProjectsFromTemplate(Status &Ret_pStatus, DbRecordSet &RetOut_Data, bool bCopyCE)
{
#ifdef _DEBUG
	//RetOut_Data.Dump();
#endif
	DbSimpleOrm BUS_COMM_VIEW_ORM(Ret_pStatus, BUS_COMM_VIEW, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	//
	// STEP 1. fetch more data
	//

	//read info on selected templates from database
	DbRecordSet lstTemplates = RetOut_Data;
	int nIDCol = lstTemplates.getColumnIdx("BUSP_ID");
	lstTemplates.removeColumn(nIDCol);
	nIDCol = lstTemplates.getColumnIdx("TEMPLATE_ID");
	lstTemplates.renameColumn(nIDCol, "BUSP_ID");
#ifdef _DEBUG
	//lstTemplates.Dump();
#endif
	DbRecordSet lstTemplates1;
	lstTemplates1.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_PROJECT));
	lstTemplates1.merge(lstTemplates);

	int nMax = lstTemplates1.getRowCount();
	int i;
	QString strWhere = " WHERE BUSP_ID IN (";
	for(i=0; i<nMax; i++){
		strWhere += QString().sprintf("%d", lstTemplates1.getDataRef(i, "BUSP_ID").toInt());
		if(i+1 < nMax)
			strWhere += ",";
	}
	strWhere += ")";
	//qDebug() << strWhere;

	g_BusinessServiceSet->ClientSimpleORM->Read(Ret_pStatus, BUS_PROJECT, lstTemplates1, strWhere, -1, false);
	if(!Ret_pStatus.IsOK())	return;

	//overwrite BUSP_CODEs, BUSP_NAMEs from the original sent data
	int nMax1 = lstTemplates1.getRowCount();
	for(i=0; i<nMax1; i++){
		lstTemplates1.setData(i, "BUSP_CODE", RetOut_Data.getDataRef(i, "BUSP_CODE").toString());
		lstTemplates1.setData(i, "BUSP_NAME", RetOut_Data.getDataRef(i, "BUSP_NAME").toString());
	}
	int nColID2 = lstTemplates1.getColumnIdx("BUSP_TEMPLATE_FLAG");
	lstTemplates1.setColValue(nColID2, 0);

	//now rename BUSP_ID back (we must not use original IDs)
	lstTemplates1.renameColumn("BUSP_ID", "TEMPLATE_ID");

	//now Write all the the data into the Database
	DbRecordSet lstActual;
	lstActual.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_PROJECT));
	//lstRecords1.merge(RetOut_Data);
	lstActual.merge(lstTemplates1);
/*
	//write data: 
	DbRecordSet rowWrite;
	rowWrite.defineFromView(DbSqlTableView::getView(DbSqlTableView::MVIEW_BOENTITYRECORD_WRITE));
	rowWrite.addRow();
	rowWrite.setData(0,0,lstRecords1);

	DbRecordSet rowSuperWrite;
	rowSuperWrite.defineFromView(DbSqlTableView::getView(DbSqlTableView::MVIEW_BOENTITYGROUP_DATA));
	rowSuperWrite.addRow();
	rowSuperWrite.setData(0,0,0);
	rowSuperWrite.setData(0,1,rowWrite);
*/
	//--------------------------------------
	//START TRANSACTION
	//--------------------------------------
	BUS_COMM_VIEW_ORM.GetDbSqlQuery()->BeginTransaction(Ret_pStatus);
	if(!Ret_pStatus.IsOK()) return;

	g_BusinessServiceSet->ClientSimpleORM->WriteHierarchicalData(Ret_pStatus, BUS_PROJECT,lstActual,"","",-1);
	if (!Ret_pStatus.IsOK())
	{
		BUS_COMM_VIEW_ORM.GetDbSqlQuery()->Rollback();
		return;
	}
/*
	//prepare the data for the cache
	DbRecordSet lstTmp, lstActual;
	rowSuperWrite.getData(0, 1, rowWrite);
	rowWrite.getData(0,0,lstTmp);
	lstActual.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_PROJECT_SELECT));
	lstActual.merge(lstTmp);
*/
	if(bCopyCE)
	{
		DbRecordSet lstNewData = lstActual; 

		//RetOut_Data.Dump();
		//lstTemplates1.Dump();
		//lstTemplates.Dump();
		//lstNewData.Dump();

		//for each project row
		int nCount = RetOut_Data.getRowCount();
		for(int i=0; i<nCount; i++)
		{
			int nOrigProjID = lstTemplates1.getDataRef(i, "TEMPLATE_ID").toInt();
			int nNewProjID = lstNewData.getDataRef(i, "BUSP_ID").toInt();

			//list all CE rows with all data linked to this project
			DbRecordSet lstRes, set;
			set.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CE_COMM_ENTITY));
			lstRes.copyDefinition(set, false);
			set.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_TASKS));
			lstRes.copyDefinition(set, false);
			set.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_DM_DOCUMENTS));
			lstRes.copyDefinition(set, false);
			set.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_VOICECALLS));
			lstRes.copyDefinition(set, false);
			set.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_EMAIL));
			lstRes.copyDefinition(set, false);

			//shorter list to read only docs through the CE_PROJECT_LINKS table (only docs use this table)
			DbRecordSet lstDocs;	
			set.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CE_COMM_ENTITY));
			lstDocs.copyDefinition(set, false);
			set.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_TASKS));
			lstDocs.copyDefinition(set, false);
			set.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_DM_DOCUMENTS));
			lstDocs.copyDefinition(set, false);
			set.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION));
			lstDocs.copyDefinition(set, false);

			//read CE_PROJECT_LINKS - copy all attached document CE entities
			QString strWhere= "FROM CE_COMM_ENTITY LEFT OUTER JOIN BUS_DM_DOCUMENTS ON BDMD_COMM_ENTITY_ID=CENT_ID";
			strWhere+=" LEFT OUTER JOIN BUS_NMRX_RELATION ON BNMR_TABLE_KEY_ID_1=CENT_ID AND "+QString(NMRX_CE_PROJECT_WHERE);
			strWhere+=" LEFT OUTER JOIN BUS_TASKS ON CENT_TASK_ID=BTKS_ID WHERE BNMR_TABLE_KEY_ID_2=%1";
			strWhere=strWhere.arg(nOrigProjID);

			g_BusinessServiceSet->ClientSimpleORM->ReadAdv(Ret_pStatus, lstDocs, strWhere);
			if (!Ret_pStatus.IsOK()){
				BUS_COMM_VIEW_ORM.GetDbSqlQuery()->Rollback();
				return;
			}

			lstRes.merge(lstDocs);

			//read voice calls separately
			DbRecordSet lstCalls;
			set.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CE_COMM_ENTITY));
			lstRes.copyDefinition(set, false);
			set.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_TASKS));
			lstRes.copyDefinition(set, false);
			set.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_VOICECALLS));
			lstRes.copyDefinition(set, false);

			strWhere= QString("\
							  FROM CE_COMM_ENTITY \
							  LEFT OUTER JOIN BUS_VOICECALLS ON BVC_COMM_ENTITY_ID=CENT_ID \
							  LEFT OUTER JOIN BUS_TASKS ON CENT_TASK_ID=BTKS_ID \
							  WHERE BVC_PROJECT_ID=%1").arg(nOrigProjID);

			g_BusinessServiceSet->ClientSimpleORM->ReadAdv(Ret_pStatus, lstCalls, strWhere);
			if (!Ret_pStatus.IsOK()){
				BUS_COMM_VIEW_ORM.GetDbSqlQuery()->Rollback();
				return;
			}

			lstRes.merge(lstCalls);

			//read emails separately
			DbRecordSet lstEmails;
			set.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CE_COMM_ENTITY));
			lstEmails.copyDefinition(set, false);
			set.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_TASKS));
			lstEmails.copyDefinition(set, false);
			set.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_EMAIL));
			lstEmails.copyDefinition(set, false);

			strWhere= QString("\
							  FROM CE_COMM_ENTITY \
							  LEFT OUTER JOIN BUS_EMAIL ON BEM_COMM_ENTITY_ID=CENT_ID \
							  LEFT OUTER JOIN BUS_TASKS ON CENT_TASK_ID=BTKS_ID \
							  WHERE BEM_PROJECT_ID=%1").arg(nOrigProjID);

			g_BusinessServiceSet->ClientSimpleORM->ReadAdv(Ret_pStatus, lstEmails, strWhere);
			if (!Ret_pStatus.IsOK()){
				BUS_COMM_VIEW_ORM.GetDbSqlQuery()->Rollback();
				return;
			}

			lstRes.merge(lstEmails);

			int nRecords = lstRes.getRowCount();
			if (nRecords > 0)
			{
				//copy tasks records
				DbRecordSet lstTasks, lstTasks1;
				lstTasks.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_TASKS));
				lstTasks.merge(lstRes);

				DbSimpleOrm BUS_TASKS_ORM(Ret_pStatus, BUS_TASKS, GetDbManager()); 
				if (!Ret_pStatus.IsOK()){
					BUS_COMM_VIEW_ORM.GetDbSqlQuery()->Rollback();
					return;
				}

				//write only task rows having non-empty ID (due to join)
				int nCol_BTKS_ID	  = lstTasks.getColumnIdx("BTKS_ID");

				int nMax = lstTasks.getRowCount();
				for(int j=0; j<nMax; j++)
				{
					if(!lstTasks.getDataRef(j, nCol_BTKS_ID).isNull())
					{
						DbRecordSet lstSingleTask = lstTasks.getRow(j);
						lstSingleTask.setColValue(lstTasks.getColumnIdx("BTKS_ID"), QVariant(QVariant::Int)); //reset task ID to NULL

						BUS_TASKS_ORM.Write(Ret_pStatus, lstSingleTask);
						if (!Ret_pStatus.IsOK()){
							BUS_COMM_VIEW_ORM.GetDbSqlQuery()->Rollback();
							return;
						}				
						//update ID column
						lstTasks.setData(j, nCol_BTKS_ID, lstSingleTask.getDataRef(0, nCol_BTKS_ID).toInt());
					}
				}

				//copy CE records
				DbRecordSet lstEntities;
				lstEntities.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CE_COMM_ENTITY));
				lstEntities.merge(lstRes);
				lstEntities.setColValue(lstEntities.getColumnIdx("CENT_ID"), QVariant(QVariant::Int)); // reset ID to NULL

				int nCol_CENT_TASK_ID = lstEntities.getColumnIdx("CENT_TASK_ID");


				//fix task ID numbers
				for(int j=0; j<nRecords; j++)
					lstEntities.setData(j, nCol_CENT_TASK_ID, lstTasks.getDataRef(j, nCol_BTKS_ID).isNull() ? QVariant(QVariant::Int) : lstTasks.getDataRef(j, nCol_BTKS_ID).toInt());

				DbSimpleOrm CE_COMM_ENTITY_ORM(Ret_pStatus, CE_COMM_ENTITY, GetDbManager()); 
				if (!Ret_pStatus.IsOK()){
					BUS_COMM_VIEW_ORM.GetDbSqlQuery()->Rollback();
					return;
				}
				CE_COMM_ENTITY_ORM.Write(Ret_pStatus, lstEntities);
				if (!Ret_pStatus.IsOK()){
					BUS_COMM_VIEW_ORM.GetDbSqlQuery()->Rollback();
					return;
				}

				int nCol_CENT_SYSTEM_TYPE_ID = lstEntities.getColumnIdx("CENT_SYSTEM_TYPE_ID");
				int nCol_CENT_ID = lstEntities.getColumnIdx("CENT_ID");

				//create copy of these for new project ID
				for(int j=0; j<nRecords; j++)
				{
					int nCentID = lstEntities.getDataRef(j, nCol_CENT_ID).toInt();

					//copy concrete data related to a single CE record
					int nType = lstEntities.getDataRef(j, nCol_CENT_SYSTEM_TYPE_ID).toInt();
					switch(nType){
						case GlobalConstants::CE_TYPE_VOICE_CALL:
							{
								DbRecordSet lstVoice;
								lstVoice.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_VOICECALLS));
								lstVoice.merge(lstRes.getRow(j));
								lstVoice.setColValue(lstVoice.getColumnIdx("BVC_ID"), QVariant(QVariant::Int));
								lstVoice.setColValue(lstVoice.getColumnIdx("BVC_COMM_ENTITY_ID"), nCentID);
								lstVoice.setColValue(lstVoice.getColumnIdx("BVC_PROJECT_ID"), nNewProjID);

								DbSimpleOrm BUS_VOICECALLS_ORM(Ret_pStatus, BUS_VOICECALLS, GetDbManager()); 		
								BUS_VOICECALLS_ORM.Write(Ret_pStatus, lstVoice);
								if (!Ret_pStatus.IsOK()){
									BUS_COMM_VIEW_ORM.GetDbSqlQuery()->Rollback();
									return;
								}
							}
							break;
						case GlobalConstants::CE_TYPE_DOCUMENT:
							{
								DbRecordSet lstDocument;
								lstDocument.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_DM_DOCUMENTS));
								DbRecordSet lstSource=lstRes.getRow(j);
								lstDocument.merge(lstSource);
								lstDocument.setColValue(lstDocument.getColumnIdx("BDMD_ID"), QVariant(QVariant::Int));
								lstDocument.setColValue(lstDocument.getColumnIdx("BDMD_COMM_ENTITY_ID"), nCentID);

								DbSimpleOrm BUS_DM_DOCUMENTS_ORM(Ret_pStatus, BUS_DM_DOCUMENTS, GetDbManager()); 		
								BUS_DM_DOCUMENTS_ORM.Write(Ret_pStatus, lstDocument);
								if (!Ret_pStatus.IsOK()){
									BUS_COMM_VIEW_ORM.GetDbSqlQuery()->Rollback();
									return;
								}

								//BT:issue 1478: copy head revision to new document:
								//----------------------------------------------------------
								DbRecordSet lstRevisions;
								int nSize=lstRes.getRowCount();
								if (lstSource.getRowCount()>0)
								{
									int nDocID=lstSource.getDataRef(0,"BDMD_ID").toInt();
									DbRecordSet rowRevision;
									DbRecordSet chkinfo;
									g_BusinessServiceSet->BusDocuments->CheckOut(Ret_pStatus,nDocID,g_UserSessionManager->GetPersonID(),rowRevision,true,chkinfo,"",true,-1);
									if (!Ret_pStatus.IsOK()){
										BUS_COMM_VIEW_ORM.GetDbSqlQuery()->Rollback();
										return;
									}
									if (rowRevision.getRowCount()>0 && lstDocument.getRowCount()>0)
									{
										int nNewDocID=lstDocument.getDataRef(0,"BDMD_ID").toInt();
										rowRevision.setData(0,"BDMR_DOCUMENT_ID",nNewDocID);
										rowRevision.setData(0,"BDMR_ID",QVariant(QVariant::Int)); //isnert null
									}

									if (lstRevisions.getColumnCount()==0)
										lstRevisions=rowRevision;
									else
										lstRevisions.merge(rowRevision);
								}


								if (lstRevisions.getRowCount()>0)
								{
									DbSimpleOrm BUS_DM_DOCUMENTS_REVISIONS_ORM(Ret_pStatus, BUS_DM_REVISIONS, GetDbManager()); 		
									if (!Ret_pStatus.IsOK())
									{
										BUS_DM_DOCUMENTS_REVISIONS_ORM.GetDbSqlQuery()->Rollback();
										return;
									}
									BUS_DM_DOCUMENTS_REVISIONS_ORM.Write(Ret_pStatus, lstRevisions);
									if (!Ret_pStatus.IsOK())
									{
										BUS_DM_DOCUMENTS_REVISIONS_ORM.GetDbSqlQuery()->Rollback();
										return;
									}
								}
								//BT:issue 1478: copy head revision to new document:
								//----------------------------------------------------------



								//copy and tweak CE_PROJECT_LINK table links
								DbRecordSet lstLinks;
								lstLinks.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION));
								lstLinks.merge(lstRes.getRow(j));
								lstLinks.setColValue(lstLinks.getColumnIdx("BNMR_ID"), QVariant(QVariant::Int));
								lstLinks.setColValue(lstLinks.getColumnIdx("BNMR_TABLE_KEY_ID_2"), nNewProjID);
								lstLinks.setColValue(lstLinks.getColumnIdx("BNMR_TABLE_KEY_ID_1"), nCentID);
								lstLinks.setColValue("BNMR_TABLE_1",CE_COMM_ENTITY);
								lstLinks.setColValue("BNMR_TABLE_2",BUS_PROJECT);
								lstLinks.setColValue("BNMR_TYPE",GlobalConstants::NMRX_TYPE_ASSIGN);
								lstLinks.setColValue("BNMR_SYSTEM_ROLE",QVariant(QVariant::Int));

								DbSimpleOrm NMRX_ORM(Ret_pStatus, BUS_NMRX_RELATION, GetDbManager()); 		
								NMRX_ORM.Write(Ret_pStatus, lstLinks);
								if (!Ret_pStatus.IsOK()){
									BUS_COMM_VIEW_ORM.GetDbSqlQuery()->Rollback();
									return;
								}
							}
							break;
						case GlobalConstants::CE_TYPE_EMAIL:
							{
								DbRecordSet lstEmails, lstEmails1;
								lstEmails.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_EMAIL));
								lstEmails.merge(lstRes.getRow(j));
								lstEmails1 = lstEmails;
								lstEmails.setColValue(lstEmails.getColumnIdx("BEM_ID"), QVariant(QVariant::Int));
								lstEmails.setColValue(lstEmails.getColumnIdx("BEM_COMM_ENTITY_ID"), nCentID);
								lstEmails.setColValue(lstEmails.getColumnIdx("BEM_PROJECT_ID"), nNewProjID);

								DbSimpleOrm BUS_EMAIL_ORM(Ret_pStatus, BUS_EMAIL, GetDbManager()); 		
								BUS_EMAIL_ORM.Write(Ret_pStatus, lstEmails);
								if (!Ret_pStatus.IsOK()){
									BUS_COMM_VIEW_ORM.GetDbSqlQuery()->Rollback();
									return;
								}

								//copy all attachments for these new emails
								int nEmailCnt = lstEmails1.getRowCount();
								for(int k=0; k<nEmailCnt; k++)
								{
									int nEmailIDOld = lstEmails1.getDataRef(k, "BEM_ID").toInt();
									int nEmailIDNew = lstEmails.getDataRef(k, "BEM_ID").toInt();

									DbRecordSet lstAtt;
									lstAtt.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_EMAIL_ATTACHMENT));

									DbSimpleOrm BUS_EMAIL_ATTACHMENT_ORM(Ret_pStatus, BUS_EMAIL_ATTACHMENT, GetDbManager()); 		

									//read old email's attachments
									QString strWhere = QString(" WHERE BEA_EMAIL_ID=%1").arg(nEmailIDOld);
									BUS_EMAIL_ATTACHMENT_ORM.Read(Ret_pStatus, lstAtt, strWhere);
									if (!Ret_pStatus.IsOK()){
										BUS_COMM_VIEW_ORM.GetDbSqlQuery()->Rollback();
										return;
									}

									//adapt data
									lstAtt.setColValue(lstAtt.getColumnIdx("BEA_ID"), QVariant(QVariant::Int)); //reset to NULL
									lstAtt.setColValue(lstAtt.getColumnIdx("BEA_EMAIL_ID"), nEmailIDNew);

									//write as new email's attachments
									BUS_EMAIL_ATTACHMENT_ORM.Write(Ret_pStatus, lstAtt);
									if (!Ret_pStatus.IsOK()){
										BUS_COMM_VIEW_ORM.GetDbSqlQuery()->Rollback();
										return;
									}
								}
							}
							break;
					}
				}
			}
		}
	}

	//--------------------------------------
	//COMMIT TRANSACTION
	//--------------------------------------
	BUS_COMM_VIEW_ORM.GetDbSqlQuery()->Commit(Ret_pStatus);

	RetOut_Data = lstActual;	//prepare cache list
}



