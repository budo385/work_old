#include "systemserviceset.h"


//init static pointers:
DbSqlManager * SystemService::m_DbManager=NULL;


/*!
	When creating system service, place service instance here

	\param pDbManager		- global DB manager
	\param strSysSession	- System session as SYS_AppSerialID

*/
SystemServiceSet::SystemServiceSet(DbSqlManager *pDbManager)//,QString strSysSession)
{
	m_DbManager=pDbManager;
	Q_ASSERT_X(m_DbManager!=NULL,"Service set","DB manager empty");

	//init all services:
	InitServiceSet();

	UserSession= new Service_UserSession; m_ServiceList.append(UserSession);
	AppSrvSession = new Service_AppSrvSession; m_ServiceList.append(AppSrvSession);
	IPAccessList = new Serivce_IPAccessList; m_ServiceList.append(IPAccessList);

	
}


//delete all services from list
SystemServiceSet::~SystemServiceSet()
{
	qDeleteAll(m_ServiceList);  //new faster way to delete all objects
	/*
		int nSize=m_ServiceList.size();
		for(int i=0;i<nSize;++i)
		{
			delete m_ServiceList.at(i);
		}
	*/
}


//init all services
void SystemServiceSet::InitServiceSet()
{
	SystemService::SetDbManager(m_DbManager);

}

