<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="1.1" language="de_CH">
<context>
    <name>BusinessLocker</name>
    <message>
        <location filename="" line="4368828"/>
        <source>Locked by: </source>
        <translation type="obsolete">Gesprerrt von: </translation>
    </message>
    <message>
        <location filename="" line="4368828"/>
        <source>Record is locked by another user!</source>
        <translation type="obsolete">Datensatz druch anderen Benutzer gesperrt!</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="" line="4368828"/>
        <source>Locked by Another User</source>
        <translation type="obsolete">Durch anderen Benutzer gesperrt</translation>
    </message>
    <message>
        <location filename="" line="4368828"/>
        <source>There are already </source>
        <translation type="obsolete">Bereits </translation>
    </message>
    <message>
        <location filename="" line="4368828"/>
        <source> LBO Servers active in systems</source>
        <translation type="obsolete"> LBO-Server sind im System aktiv</translation>
    </message>
    <message>
        <location filename="" line="4368828"/>
        <source> and </source>
        <translation type="obsolete"> und </translation>
    </message>
    <message>
        <location filename="" line="4368828"/>
        <source> MASTER Servers active.</source>
        <translation type="obsolete"> MASTER-Server aktiv.</translation>
    </message>
    <message>
        <location filename="" line="4368828"/>
        <source>Operation failed, contact with id=</source>
        <translation type="obsolete">Operation misslungen, Kontakt mit id=</translation>
    </message>
    <message>
        <location filename="" line="4368828"/>
        <source> does not exists!</source>
        <translation type="obsolete"> existiert nicht!</translation>
    </message>
    <message>
        <location filename="service_buscontact.cpp" line="1007"/>
        <source>Operation failed, default person does not exists!</source>
        <translation>Operation misslungen, Default-Benutzer nicht gefunden!</translation>
    </message>
    <message>
        <location filename="service_servercontrol.cpp" line="93"/>
        <source>Template Data could not be fetched!</source>
        <translation>Vorlage konnte nicht geladen werden!</translation>
    </message>
    <message>
        <location filename="service_servercontrol.cpp" line="93"/>
        <source>Locked by: </source>
        <translation type="obsolete">Gesprerrt von: </translation>
    </message>
    <message>
        <location filename="service_servercontrol.cpp" line="93"/>
        <source>Error while fetching number of licenses in use!</source>
        <translation type="obsolete">Fehler beim Lesen der momentan benutzten Lizenzzahl!</translation>
    </message>
    <message>
        <location filename="service_busimport.cpp" line="4772"/>
        <source>Can not open file</source>
        <translation>Datei kann nicht geöffnet werden</translation>
    </message>
    <message>
        <location filename="service_busimport.cpp" line="4843"/>
        <source>Can not open .hed file</source>
        <translation>.hed-Datei kann nicht geöffnet werden</translation>
    </message>
    <message>
        <location filename="service_busimport.cpp" line="3512"/>
        <source>Document does not contain %1 fields for each record!</source>
        <translation>Das Dokument enthält nicht %1 Felder pro Datensatz!</translation>
    </message>
</context>
<context>
    <name>Service_CoreServices</name>
    <message>
        <location filename="" line="627"/>
        <source>No default organization entered!</source>
        <translation type="obsolete">Keine Default-Organisation erfasst!</translation>
    </message>
</context>
<context>
    <name>Service_UserSession</name>
    <message>
        <location filename="" line="627"/>
        <source>Error while fetching number of licenses in use!</source>
        <translation type="obsolete">Fehler beim Lesen der momentan benutzten Lizenzzahl!</translation>
    </message>
</context>
</TS>
