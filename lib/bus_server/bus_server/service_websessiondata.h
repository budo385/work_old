#ifndef SERVICE_WEBSESSIONDATA_H
#define SERVICE_WEBSESSIONDATA_H


#include "bus_interface/bus_interface/interface_websessiondata.h"
#include "businessservice.h"
#include "serverlocalcache.h"


class Service_WebSessionData : public Interface_WebSessionData, public BusinessService
{
public:
	void GetUserSessionData(Status &Ret_Status,DbRecordSet &Ret_ARSet,DbRecordSet &Ret_UserData, DbRecordSet &Ret_ContactData,  QString &Ret_strEmailCalConfAddress,  int &Ret_nIsAnestezistClient,  int &Ret_nCSID);

private:
	void GetOptionsAndSettings(Status &pStatus, int nPersonID, DbRecordSet &recSettings, DbRecordSet &recOptions);
	
};

#endif // SERVICE_WEBSESSIONDATA_H
