#include "service_accessrights.h"
#include "bus_core/bus_core/accessrightsorganizer.h"
#include "bus_core/bus_core/accessrightset.h"
#include "bus_core/bus_core/accessright.h"
#include "accessrightsmanipulator.h"
#include "db/db/dbconnectionreserver.h"
#include "db/db/dbsimpleorm.h"
#include "bus_core/bus_core/accuarcore.h"
#include "bus_core/bus_core/mainentitycalculatedname.h"
#include "common/common/entity_id_collection.h"
#include "bus_core/bus_core/mainentityfilter.h"
#include "bus_core/bus_core/globalconstants.h"

#include "businessserviceset.h"
extern BusinessServiceSet* g_BusinessServiceSet;
#include "privateserviceset.h"
extern PrivateServiceSet* g_PrivateServiceSet; 




Service_AccessRights::Service_AccessRights()
{
	m_ProjectData.Initialize(BUS_PROJECT,GetDbManager());

}
//------------------------------------
//			Person section
//------------------------------------
/*!
Get list of roles for person (CROL_ROLE_TYPE > 0  - business and system business visible).

\param pStatus					- If err occur, returned here
\param pPersonRoleListRecordSet	- Returned Recordset with roles.
\param pDbConn				- Db conn.
*/
void Service_AccessRights::GetRoleList(Status &pStatus, DbRecordSet &pPersonRoleListRecordSet /*= NULL*/)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(pStatus, CORE_ROLE, GetDbManager()); 
	if(!pStatus.IsOK()) return;

	//Where clause.
	//Show business and system-visible to business roles (1,2).
	QString strWhere = "WHERE CROL_ROLE_TYPE > 0";

	//Read.
	TableOrm.Read(pStatus, pPersonRoleListRecordSet, strWhere, DbSqlTableView::TVIEW_CORE_ROLE_VIEW);
}

/*!
Get list of persons.

\param pStatus				- If err occur, returned here
\param pPersonsRecordSet	- Returned Recordset with roles.
\param pDbConn				- Db conn.
*/
void Service_AccessRights::GetPersons(Status &pStatus, DbRecordSet &pPersonsRecordSet /*= NULL*/)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(pStatus, BUS_PERSON, GetDbManager()); 
	if(!pStatus.IsOK()) return;

	//Read.
	TableOrm.Read(pStatus, pPersonsRecordSet, QString(), DbSqlTableView::TVIEW_PERSON_TREE_VIEW);
}

/*!
Get single person list of roles.

\param pStatus					- If err occur, returned here
\param pPersonRoleListRecordSet	- Returned Recordset with roles.
\param PersonID					- Person ID.
\param pDbConn					- Db conn.
*/
void Service_AccessRights::GetPersonRoleList(Status &pStatus, DbRecordSet &pPersonRoleListRecordSet, int PersonID /*= NULL*/)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(pStatus, BUS_PERSON, GetDbManager()); 
	if(!pStatus.IsOK()) return;

	//Where clause.
	QString strWhere = "WHERE BPRL_PERSON_ID = %1 AND BPRL_ROLE_ID = CROL_ID";

	//Read.
	TableOrm.Read(pStatus, pPersonRoleListRecordSet, strWhere.arg(QVariant(PersonID).toString()), DbSqlTableView::TVIEW_PERSON_ROLE_LIST);
}

/*!
Insert new role to person.

\param pStatus					- If err occur, returned here
\param PersonID					- Person ID.
\param RoleID					- Role ID.
\param pDbConn					- Db conn.
*/
void Service_AccessRights::InsertNewRoleToPerson(Status &pStatus, int PersonID, int RoleID /*= NULL*/)
{
	DbConnectionReserver conn(pStatus, GetDbManager()); //reserve one connection to avoid multi connection lock
	if(!pStatus.IsOK())
		return;

	AccessRightsManipulator AccRManipulator(GetDbManager());
	AccRManipulator.InsertNewRoleToPerson(pStatus, PersonID, RoleID);
}

/*!
Delete role from person.

\param pStatus					- If err occur, returned here
\param PersonID					- Person ID.
\param RoleID					- Role ID.
\param pDbConn					- Db conn.
*/
void Service_AccessRights::DeleteRoleFromPerson(Status &pStatus, int PersonID, int RoleID /*= NULL*/)
{
	DbConnectionReserver conn(pStatus, GetDbManager()); //reserve one connection to avoid multi connection lock
	if(!pStatus.IsOK())
		return;

	AccessRightsManipulator AccRManipulator(GetDbManager());
	AccRManipulator.DeleteRoleFromPerson(pStatus, PersonID, RoleID);
}

//------------------------------------
//			User section
//------------------------------------
/*!
Get list of roles for users (CROL_ROLE_TYPE < 2 - system and system business visible).

\param pStatus					- If err occur, returned here
\param pUserRoleListRecordSet	- Returned Recordset with roles.
\param pDbConn					- Db conn.
*/
void Service_AccessRights::GetRoleListForUsers(Status &pStatus, DbRecordSet &pUserRoleListRecordSet /*= NULL*/)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(pStatus, CORE_ROLE, GetDbManager()); 
	if(!pStatus.IsOK()) return;

	//Where clause.
	//Show system roles (0,1).
	QString strWhere = "WHERE CROL_ROLE_TYPE < 2";

	//Read.
	TableOrm.Read(pStatus, pUserRoleListRecordSet, strWhere, DbSqlTableView::TVIEW_CORE_ROLE_VIEW);
}

/*!
Get list of users.

\param pStatus					- If err occur, returned here
\param pUsersRecordSet			- Returned Recordset with roles.
\param pDbConn					- Db conn.
*/
void Service_AccessRights::GetUsers(Status &pStatus, DbRecordSet &pUsersRecordSet /*= NULL*/)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(pStatus, CORE_USER, GetDbManager()); 
	if(!pStatus.IsOK()) return;

	//Read.
	TableOrm.Read(pStatus, pUsersRecordSet, QString(), DbSqlTableView::TVIEW_USER_TREE_VIEW);
}

/*!
Get single user list of roles.

\param pStatus					- If err occur, returned here
\param pUserRoleListRecordSet	- Returned Recordset with roles.
\param UserID					- User ID.
\param pDbConn					- Db conn.
*/
void Service_AccessRights::GetUserRoleList(Status &pStatus, DbRecordSet &pUserRoleListRecordSet, int UserID /*= NULL*/)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(pStatus, CORE_USER, GetDbManager()); 
	if(!pStatus.IsOK()) return;

	//Where clause.
	QString strWhere = "WHERE CURL_USER_ID = %1 AND CURL_ROLE_ID = CROL_ID";

	//Read.
	TableOrm.Read(pStatus, pUserRoleListRecordSet, strWhere.arg(QVariant(UserID).toString()), DbSqlTableView::TVIEW_USER_ROLE_LIST);
}

/*!
Insert new role to user.

\param pStatus					- If err occur, returned here
\param UserID					- User ID.
\param RoleID					- Role ID.
\param pDbConn					- Db conn.
*/
void Service_AccessRights::InsertNewRoleToUser(Status &pStatus, int UserID, int RoleID /*= NULL*/)
{
	DbConnectionReserver conn(pStatus, GetDbManager()); //reserve one connection to avoid multi connection lock
	if(!pStatus.IsOK())
		return;

	AccessRightsManipulator AccRManipulator(GetDbManager());
	AccRManipulator.InsertNewRoleToUser(pStatus, UserID, RoleID);
}

/*!
Delete role from user.

\param pStatus					- If err occur, returned here
\param UserID					- User ID.
\param RoleID					- Role ID.
\param pDbConn					- Db conn.
*/
void Service_AccessRights::DeleteRoleFromUser(Status &pStatus, int UserID, int RoleID /*= NULL*/)
{
	DbConnectionReserver conn(pStatus, GetDbManager()); //reserve one connection to avoid multi connection lock
	if(!pStatus.IsOK())
		return;

	AccessRightsManipulator AccRManipulator(GetDbManager());
	AccRManipulator.DeleteRoleFromUser(pStatus, UserID, RoleID);
}

/*!
Get user access rights (and his person if it has it) - used for testing access rights.

\param pStatus				- If err occur, returned here.
\param UserID				- User ID.
\param pUserAccRRecordSet	- User Access rights recordset.
\param pDbConn				- Db conn.
*/
void Service_AccessRights::GetUserAccRightsRecordSet(Status &pStatus, DbRecordSet &pUserAccRRecordSet, int UserID /*= NULL*/)
{
	//Setup locals.
	int PersonRowID = -1;
	QList<int> UserRoleList;

	//Simple ORM.
	DbSimpleOrm TableOrm(pStatus, CORE_USER, GetDbManager()); 
	if(!pStatus.IsOK()) 
		return;

	//Read user recordset to test does it has person.
	DbRecordSet UserRecordSet;
	QString strWhere = "WHERE CUSR_ID = %1";
	TableOrm.Read(pStatus, UserRecordSet, strWhere.arg(QVariant(UserID).toString()), DbSqlTableView::TVIEW_CORE_USER);
	if(!pStatus.IsOK()) 
		return;

	if (UserRecordSet.getRowCount())
		PersonRowID = UserRecordSet.getDataRef(0, "CUSR_PERSON_ID").toInt();

	//Now let's get user role list.
	UserRecordSet.clear();
	strWhere = "WHERE CURL_USER_ID = %1";
	TableOrm.Read(pStatus, UserRecordSet, strWhere.arg(QVariant(UserID).toString()), DbSqlTableView::TVIEW_CORE_USERROLE);
	if(!pStatus.IsOK()) 
		return;

	int RowCount = UserRecordSet.getRowCount();
	for (int i = 0; i < RowCount; ++i)
		UserRoleList << UserRecordSet.getDataRef(i, "CURL_ROLE_ID").toInt();

	//Get person role list.
	if (PersonRowID != -1)
	{
		//Let's get user role list.
		UserRecordSet.clear();
		strWhere = "WHERE BPRL_PERSON_ID = %1";
		TableOrm.Read(pStatus, UserRecordSet, strWhere.arg(QVariant(PersonRowID).toString()), DbSqlTableView::TVIEW_BUS_PERSONROLE);
		if(!pStatus.IsOK()) 
			return;

		RowCount = UserRecordSet.getRowCount();
		for (int i = 0; i < RowCount; ++i)
		{
			//First test does it have already that role in list.
			if (!UserRoleList.contains(UserRecordSet.getDataRef(i, "BPRL_ROLE_ID").toInt()))
				UserRoleList << UserRecordSet.getDataRef(i, "BPRL_ROLE_ID").toInt();
		}
	}

	//If no roles associated.
	if (!(UserRoleList.count() > 0))
		return;

	//Finally get Roles.
	strWhere = "WHERE CAR_ROLE_ID IN (";
	
	QListIterator<int> iter(UserRoleList);
	while (iter.hasNext())
	{
		strWhere += QVariant(iter.next()).toString();
		strWhere += ",";
	}
	strWhere.truncate(strWhere.length()-1);
	strWhere += ")";

	TableOrm.Read(pStatus, pUserAccRRecordSet, strWhere, DbSqlTableView::TVIEW_CORE_ACCESSRIGHTS);
	if(!pStatus.IsOK()) 
		return;
}

//------------------------------------
//			Role definition section
//------------------------------------
/*!
Get of roles.

\param pStatus				- If err occur, returned here
\param pRoleListRecordSet	- Returned Recordset with roles.
\param pDbConn				- Db conn.
*/
void Service_AccessRights::GetRoles(Status &pStatus, DbRecordSet &pRoleListRecordSet /*= NULL*/)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(pStatus, CORE_ROLE, GetDbManager()); 
	if(!pStatus.IsOK()) return;

	//Read.
	TableOrm.Read(pStatus, pRoleListRecordSet, QString(), DbSqlTableView::TVIEW_CORE_ROLE_VIEW);
}

void Service_AccessRights::GetBERoles(Status &Ret_pStatus, DbRecordSet &Ret_pRoleListRecordSet /*= NULL*/)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, CORE_ROLE, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	QString strWhere = " WHERE (CROL_CODE IN (0,1,2,3) OR CROL_CODE IS NULL)";

	//Read.
	TableOrm.Read(Ret_pStatus, Ret_pRoleListRecordSet, strWhere);
}

void Service_AccessRights::GetTERoles(Status &Ret_pStatus, DbRecordSet &Ret_pRoleListRecordSet /*= NULL*/)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, CORE_ROLE, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	QString strWhere = " WHERE CROL_CODE IN (0,4) ";

	//Read.
	TableOrm.Read(Ret_pStatus, Ret_pRoleListRecordSet, strWhere);
}

/*!
Get single role access right sets.

\param pStatus						- If err occur, returned here
\param pAccessRightsListRecordSet	- Returned Recordset with roles.
\param RoleID						- Role ID.
\param pDbConn						- Db conn.
*/
void Service_AccessRights::GetRoleAccessRightSets(Status &pStatus, DbRecordSet &pAccessRightsListRecordSet, int RoleID /*= NULL*/)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(pStatus, CORE_ACCRSETROLE, GetDbManager()); 
	if(!pStatus.IsOK()) return;

	//Where clause.
	QString strWhere = "WHERE CASTR_ROLE_ID = %1";

	//Read CORE_ACCRSETROLE.
	TableOrm.Read(pStatus, pAccessRightsListRecordSet, strWhere.arg(QVariant(RoleID).toString()), DbSqlTableView::TVIEW_CORE_ACCRSETROLE);

	//Make new query to get AccrightsSets.
	DbSimpleOrm CORE_ACCRSET_TableOrm(pStatus, CORE_ACCRSET, GetDbManager());
	if(!pStatus.IsOK()) return;

	//Where clause.
	QString CORE_ACCRSET_strWhere = "WHERE CAST_ID IN (";

	int RowCount = pAccessRightsListRecordSet.getRowCount();
	if (RowCount == 0)
	{
		pAccessRightsListRecordSet.clear();
		return;
	}

	for (int i = 0; i < RowCount; ++i)
	{
		CORE_ACCRSET_strWhere += pAccessRightsListRecordSet.getDataRef(i, 4).toString();
		CORE_ACCRSET_strWhere += ",";
	}
	//Trim last colon.
	CORE_ACCRSET_strWhere.truncate(CORE_ACCRSET_strWhere.length()-1);
	CORE_ACCRSET_strWhere += ")";

	//Clear recordset.
	pAccessRightsListRecordSet.clear();

	//Read access rights.
	CORE_ACCRSET_TableOrm.Read(pStatus, pAccessRightsListRecordSet, CORE_ACCRSET_strWhere, DbSqlTableView::TVIEW_CORE_ACCRSET);
}

/*!
Get list of access right sets.

\param pStatus							- If err occur, returned here
\param pAccessRightsSetListRecordSet	- Returned Recordset with roles.
\param pDbConn							- Db conn.
*/
void Service_AccessRights::GetAccessRightsSets(Status &pStatus, DbRecordSet &pAccessRightsSetListRecordSet /*= NULL*/)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(pStatus, CORE_ACCRSET, GetDbManager()); 
	if(!pStatus.IsOK()) return;

	//Read.
	TableOrm.Read(pStatus, pAccessRightsSetListRecordSet, QString(), DbSqlTableView::TVIEW_CORE_ACCRSET);
}

/*!
Delete role.

\param pStatus	- If err occur, returned here
\param RoleID	- Role ID.
\param pDbConn	- Db conn.
*/
void Service_AccessRights::DeleteRole(Status &pStatus, int RoleID /*= NULL*/)
{
	DbConnectionReserver conn(pStatus, GetDbManager()); //reserve one connection to avoid multi connection lock
	if(!pStatus.IsOK())
		return;

	AccessRightsManipulator AccRManipulator(GetDbManager());
	AccRManipulator.DeleteRole(pStatus, RoleID);
}

/*!
Delete access right set from role.

\param pStatus		- If err occur, returned here
\param RoleID		- Role ID.
\param AccRSetID	- Access right ID.
\param pDbConn		- Db conn.
*/
void Service_AccessRights::DeleteAccRSetFromRole(Status &pStatus, int RoleID, int AccRSetID /*= NULL*/)
{
	DbConnectionReserver conn(pStatus, GetDbManager()); //reserve one connection to avoid multi connection lock
	if(!pStatus.IsOK())
		return;

	AccessRightsManipulator AccRManipulator(GetDbManager());
	AccRManipulator.DeleteAccRSetFromRole(pStatus, RoleID, AccRSetID);
}

/*!
Insert new access right set to role.

\param pStatus		- If err occur, returned here
\param RoleID		- Role ID.
\param AccRSetID	- Access right ID.
\param pDbConn		- Db conn.
*/
void Service_AccessRights::InsertNewAccRSetToRole(Status &pStatus, int RoleID, int AccRSetID /*= NULL*/)
{
	DbConnectionReserver conn(pStatus, GetDbManager()); //reserve one connection to avoid multi connection lock
	if(!pStatus.IsOK())
		return;

	AccessRightsManipulator AccRManipulator(GetDbManager());
	AccRManipulator.InsertNewAccRSetToRole(pStatus, RoleID, AccRSetID);
}

/*!
Insert new role.

\param pStatus			- If err occur, returned here
\param nNewRoleRowID	- Role ID.
\param RoleName			- Role name.
\param RoleDescription	- Role description.
\param RoleType			- Role type.
\param pDbConn			- Db conn.
*/
void Service_AccessRights::InsertNewRole(Status &pStatus, int &nNewRoleRowID, QString RoleName, QString RoleDescription, int RoleType /*= NULL*/)
{
	DbConnectionReserver conn(pStatus, GetDbManager()); //reserve one connection to avoid multi connection lock
	if(!pStatus.IsOK())
		return;

	AccessRightsManipulator AccRManipulator(GetDbManager());
	AccRManipulator.InsertNewRole(pStatus, nNewRoleRowID, RoleName, RoleDescription, RoleType);
}

/*!
Rename role.

\param pStatus				- If err occur, returned here
\param RoleID				- Role ID.
\param NewRoleName			- Role name.
\param NewRoleDescription	- Role description.
\param pDbConn				- Db conn.
*/
void Service_AccessRights::RenameRole(Status &pStatus, int RoleID, QString NewRoleName, QString NewRoleDescription /*= NULL*/)
{
	DbConnectionReserver conn(pStatus, GetDbManager()); //reserve one connection to avoid multi connection lock
	if(!pStatus.IsOK())
		return;

	AccessRightsManipulator AccRManipulator(GetDbManager());
	AccRManipulator.RenameRole(pStatus, RoleID, NewRoleName, NewRoleDescription);
}

/*!
Insert access right to role.

\param pStatus						- If err occur, returned here
\param pUserRoleListRecordSet		- Access rights recordset.
\param RoleID						- Search by RoleID (if -1 then search without role).
\param AccRSetID					- Search by AccRSetID (if -1 then search without access right set).
\param pDbConn						- Db conn.
*/
void Service_AccessRights::GetAccessRights(Status &pStatus, DbRecordSet &pAccessRightsRecordSet, int RoleID, int AccRSetID /*= NULL*/)
{
	DbSimpleOrm TableOrm(pStatus, CORE_ACCESSRIGHTS, GetDbManager()); 
	if(!pStatus.IsOK()) 
		return;

	//Where clause.
	QString str;
	QString strWhere;

	if (RoleID >= 0)
	{
		str = "WHERE CAR_ROLE_ID = %1";

		if (AccRSetID >= 0)
		{
			str += " AND CAR_ACCRSET_ID = %2";
			strWhere = str.arg(QVariant(RoleID).toString(), QVariant(AccRSetID).toString());	
		}
		else
			strWhere = str.arg(QVariant(RoleID).toString());	
	}
	else
	{

		strWhere = "WHERE CAR_ACCRSET_ID = " + QVariant(AccRSetID).toString();
	}

	//Read.
	TableOrm.Read(pStatus, pAccessRightsRecordSet, strWhere, DbSqlTableView::TVIEW_CORE_ACCESSRIGHTS);
}

/*!
Set access right value.

\param pStatus			- If err occur, returned here
\param AccRightID		- Access right ID.
\param Value			- Access right value.
\param pDbConn			- Db conn.
*/
void Service_AccessRights::SetAccessRightValue(Status &pStatus, int AccRightID, QString Value /*= NULL*/)
{
	DbSimpleOrm TableOrm(pStatus, CORE_ACCESSRIGHTS, GetDbManager()); 
	if(!pStatus.IsOK()) 
		return;

	//Where clause.
	QString strWhere = "WHERE CAR_ID = %1";

	//Recordset.
	DbRecordSet RecordSet;

	//Read.
	TableOrm.Read(pStatus, RecordSet, strWhere.arg(QVariant(AccRightID).toString()), DbSqlTableView::TVIEW_CORE_ACCESSRIGHTS);

	//Get access right type.
	int AccRType = RecordSet.getDataRef(0, "CAR_TYPE").toInt();

	//If type is string then write description.
	if (AccRType == 2)
		RecordSet.setData(0, "CAR_VALUE_DESCR", Value);
	//Else fill value.
	else
		RecordSet.setData(0, "CAR_VALUE", QVariant(Value).toInt());

	TableOrm.Write(pStatus, RecordSet);
}

/*!
Get default roles (for new person).

\param pStatus					- If err occur, returned here
\param RoleType					- Role type (0 - user visible, 1 - person visible).
\param pDefaultRoleRecordSet	- Access right set ID.
\param pDbConn					- Db conn.
*/
void Service_AccessRights::GetDefaultRoles(Status &pStatus, int RoleType, DbRecordSet &pDefaultRoleRecordSet /*= NULL*/)
{
	DbSimpleOrm TableOrm(pStatus, CORE_ROLE, GetDbManager()); 
	if(!pStatus.IsOK()) 
		return;
	
	//Where clause.
	QString strWhere;

	//User
	if (!RoleType)
		strWhere = "WHERE CROL_ROLE_TYPE = 0 AND CROL_DEFAULT_FLAG = 1";
	else
		strWhere = "WHERE CROL_ROLE_TYPE IN (1,2) AND CROL_DEFAULT_FLAG = 1";

	//Read.
	TableOrm.Read(pStatus, pDefaultRoleRecordSet, strWhere, DbSqlTableView::TVIEW_CORE_ROLE);
}

/*!
	Write changed access rights.

	\param pStatus					- If err occur, returned here
	\param ChangedRightsRecordSet	- Recordset with changed rights.
	\param pDbConn					- Db conn.
*/
void Service_AccessRights::ChangeAccessRights(Status &Ret_pStatus, DbRecordSet &ChangedRightsRecordSet /*= NULL*/)
{
	DbSimpleOrm TableOrm(Ret_pStatus, CORE_ACCESSRIGHTS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) 
		return;

	//List for skipping row (if this access right code is already inserted).
	QList<int> skipRowList;

	//Loop through recordset, if row ID < 0 (means new inserted value), find value by RoleID, ARSID, ARCode, and change value.
	int recCount = ChangedRightsRecordSet.getRowCount();
	for (int i = 0; i < recCount; ++i)
	{
		if (skipRowList.contains(i))
			continue;
		
		int RowID = ChangedRightsRecordSet.getDataRef(i, 0).toInt();
		//Means that we change old access right - just write value.
		if (RowID >= 0)
		{
			DbRecordSet tmpRecordSet = ChangedRightsRecordSet.getRow(i);
			TableOrm.Write(Ret_pStatus, tmpRecordSet);
			if(!Ret_pStatus.IsOK()) 
				return;
		}
		//If newly inserted access rights, first find it by RoleID, ARSID and ARCode..
		else
		{

			int ARCode  = ChangedRightsRecordSet.getDataRef(i, 9).toInt();
			int nRoleID = ChangedRightsRecordSet.getDataRef(i, 4).toInt();
			int nARSID  = ChangedRightsRecordSet.getDataRef(i, 3).toInt();
			QString nValType= ChangedRightsRecordSet.getDataRef(i, 6).toString();
			QVariant varValue;
			
			//First see are there more identical inserted access right codes.
			ChangedRightsRecordSet.clearSelection();
			ChangedRightsRecordSet.find(9, ARCode);
			//If true merge values.
			int count = ChangedRightsRecordSet.getSelectedCount();
			if (count > 1)
			{
				//First insert skip rows.
				QList<int> rows; 
				rows << ChangedRightsRecordSet.getSelectedRows();
				skipRowList << rows;
				
				//If bool value.
				if (nValType == "1")
				{
					bool bValue = false;
					//Get values.
					foreach(int Row, rows)
						bValue = bValue || ChangedRightsRecordSet.getDataRef(Row, 7).toBool();
					//Set value.
					foreach(int Row, rows)
						ChangedRightsRecordSet.setData(Row, 7, bValue);

					varValue = bValue;
				}
				//If integer - FOR NOW PICK THE GREATEST VALUE AND INSERT IT.
				else if (nValType == "0")
				{
					int nValue = 0;
					//Get values.
					foreach(int Row, rows)
						if (ChangedRightsRecordSet.getDataRef(Row, 7).toInt() > nValue)
							nValue = ChangedRightsRecordSet.getDataRef(Row, 7).toInt();
					//Set value.
					foreach(int Row, rows)
						ChangedRightsRecordSet.setData(Row, 7, nValue);

					varValue = nValue;
				}
				//If string.
				else
				{
					QString strValue;
					QStringList list;
					foreach(int Row, rows)
					{
						QStringList tmpLst;
						tmpLst << ChangedRightsRecordSet.getDataRef(Row, 8).toString().split(";", QString::SkipEmptyParts);
						list << tmpLst;
					}
					strValue = list.join(";");
					if (!strValue.isEmpty())
						strValue += ";";

					foreach(int Row, rows)
						ChangedRightsRecordSet.setData(Row, 8, strValue);

					varValue = strValue;
				}
			}
			//If only one value changed.
			else
			{
				if (nValType == "2")
					varValue = ChangedRightsRecordSet.getDataRef(i, 8);
				else
					varValue = ChangedRightsRecordSet.getDataRef(i, 7);
			}

			DbRecordSet readRecordSet;
			//Get real values from database.
			QString strWhere = " WHERE CAR_ACCRSET_ID = %1 AND CAR_ROLE_ID = %2 AND CAR_CODE = %3 ";
			TableOrm.Read(Ret_pStatus, readRecordSet, strWhere.arg(QVariant(nARSID).toString()).arg(QVariant(nRoleID).toString()).arg(QVariant(ARCode).toString()));

			//Set identical values to all instances of this access right.
			int rCount = readRecordSet.getRowCount();
			for (int j = 0; j < rCount; ++j)
			{
				if(nValType != "2")
					readRecordSet.setData(j, 7, varValue.toInt());
				else
					readRecordSet.setData(j, 8, varValue.toString());
			}

			//Write recordset.
			TableOrm.Write(Ret_pStatus, readRecordSet);
			if (!Ret_pStatus.IsOK())
				return;
		}
	}
}

/*!
Insert access right to role.

\param pStatus				- If err occur, returned here
\param RoleID				- Role ID.
\param ARSID				- Access right set ID.
\param pDbConn				- Db conn.
*/
void Service_AccessRights::InsertAccessRights(Status &pStatus, int RoleID, int ARSID /*= NULL*/)
{
	DbConnectionReserver conn(pStatus, GetDbManager()); //reserve one connection to avoid multi connection lock
	if(!pStatus.IsOK())
		return;

	AccessRightsManipulator AccRManipulator(GetDbManager());
	AccRManipulator.InsertAccessRights(pStatus, RoleID, ARSID);
}



//------------------------------------
//			UAR/GAR system
//------------------------------------

//reads both group and user ar per record/table
void Service_AccessRights::ReadGUAR(Status &Ret_pStatus, int nRecord, int nTableID,DbRecordSet &Ret_UAR, DbRecordSet &Ret_GAR)
{
	DbSimpleOrm TableOrm_UAR(Ret_pStatus, CORE_ACC_USER_REC, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	DbSimpleOrm TableOrm_GAR(Ret_pStatus, CORE_ACC_GROUP_REC, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	QString strWhere=" WHERE CUAR_TABLE_ID="+QVariant(nTableID).toString()+" AND CUAR_RECORD_ID="+QVariant(nRecord).toString();
	TableOrm_UAR.Read(Ret_pStatus,Ret_UAR,strWhere);
	if(!Ret_pStatus.IsOK()) return;

	strWhere=" WHERE CGAR_TABLE_ID="+QVariant(nTableID).toString()+" AND CGAR_RECORD_ID="+QVariant(nRecord).toString();
	TableOrm_GAR.Read(Ret_pStatus,Ret_GAR,strWhere);
}

//Writes UAR/GAR for 1 record:
//if GAR is empty and UAR only contains 1 record: with PERSON=NULL, ->delete all UAR/GAR
//if nRecord = -1 then uar and gar tables contains records for more then 1 records: sort, write each
void Service_AccessRights::WriteGUAR(Status &Ret_pStatus, int nRecord, int nTableID,DbRecordSet &RetOut_UAR, DbRecordSet &RetOut_GAR)
{
	DbSimpleOrm TableOrm_UAR(Ret_pStatus, CORE_ACC_USER_REC, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	DbSimpleOrm TableOrm_GAR(Ret_pStatus, CORE_ACC_GROUP_REC, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	if (nRecord!=-1)
	{
		WriteOneGUAR(Ret_pStatus,nRecord,nTableID,RetOut_UAR,RetOut_GAR,TableOrm_UAR,TableOrm_GAR);
		return;
	}

	//for more: sort and isolate (do not return nothing):
	RetOut_UAR.sort("CUAR_RECORD_ID");
	RetOut_GAR.sort("CGAR_RECORD_ID");
	
	DbSqlQuery *query=TableOrm_UAR.GetDbSqlQuery();
	query->BeginTransaction(Ret_pStatus);
	if(!Ret_pStatus.IsOK()) return;


	int nSize=RetOut_UAR.getRowCount();
	for(int i=0;i<nSize;i++)
	{
		int nRecordTmp=RetOut_UAR.getDataRef(i,"CUAR_RECORD_ID").toInt();
		RetOut_UAR.find("CUAR_RECORD_ID",nRecordTmp);
		RetOut_GAR.find("CGAR_RECORD_ID",nRecordTmp);
		DbRecordSet lstUAR,lstGAR;
		lstUAR.copyDefinition(RetOut_UAR);
		lstGAR.copyDefinition(RetOut_GAR);
		lstUAR.merge(RetOut_UAR,true);
		lstGAR.merge(RetOut_GAR,true);

		WriteOneGUAR(Ret_pStatus,nRecordTmp,nTableID,lstUAR,lstGAR,TableOrm_UAR,TableOrm_GAR);
		if(!Ret_pStatus.IsOK()) 
		{
			query->Rollback();
			return;
		}
	}

	query->Commit(Ret_pStatus);
}



//fills all fields needed for GUI display: person data, group data, also redefines lists RetOut_GAR & RetOut_UAR if not already defined
//if lists RetOut_GAR & RetOut_UAR already defined in GUI format, then only UAR group dependent records are updated!!
void Service_AccessRights::PrepareGUAR(Status &Ret_pStatus,int nRecord, int nTableID, DbRecordSet &RetOut_UAR, DbRecordSet &RetOut_GAR, DbRecordSet &Ret_lstGroupUsers,int &Ret_ParentRecord,int &Ret_ParentTableID,QString &Ret_ParentName)
{
	DbSimpleOrm TableOrm_Person(Ret_pStatus, BUS_PERSON, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	//only when first time: update BPER fields & TREE field and return Parent
	bool bFirstTime=false;
	if (RetOut_UAR.getColumnIdx("CX_READ")==-1)
		bFirstTime=true;

	AccUARCore::DefineGroupUserList(Ret_lstGroupUsers);
	Ret_ParentTableID=-1;
	Ret_ParentRecord=-1;
	AccUARCore::UARList2DisplayList(&RetOut_UAR,&RetOut_GAR);

	MainEntityCalculatedName filler;
	filler.Initialize(ENTITY_BUS_PERSON,RetOut_UAR);

	//find person data:
	if (bFirstTime)
	{
		//----------------------UAR update (only first time)
		DbRecordSet lstPersons;
		TableOrm_Person.ReadFromParentIDs(Ret_pStatus,lstPersons,"BPER_ID",RetOut_UAR,"CUAR_PERSON_ID");
		if(!Ret_pStatus.IsOK()) return;

		int nSize=lstPersons.getRowCount();
		for(int i=0;i<nSize;i++)
		{
			int nRow=RetOut_UAR.find("CUAR_PERSON_ID",lstPersons.getDataRef(i,"BPER_ID").toInt(),true);
			if (nRow>=0)
			{
				RetOut_UAR.setData(nRow,"BPER_CODE",lstPersons.getDataRef(i,"BPER_CODE"));
				RetOut_UAR.setData(nRow,"BPER_FIRST_NAME",lstPersons.getDataRef(i,"BPER_FIRST_NAME"));
				RetOut_UAR.setData(nRow,"BPER_LAST_NAME",lstPersons.getDataRef(i,"BPER_LAST_NAME"));
			}
		}
		//calculated names: 
		filler.FillCalculatedNames(RetOut_UAR,RetOut_UAR.getColumnIdx("BPER_NAME"));

		//----------------------GAR update (only first time)

		//find all users from GAR:
		QString strSQLIN;
		nSize=RetOut_GAR.getRowCount();
		for(int i=0;i<nSize;i++)
		{
			strSQLIN.append(RetOut_GAR.getDataRef(i,"CGAR_GROUP_ID").toString());
			strSQLIN+=",";
		}
		if (!strSQLIN.isEmpty())
		{
			strSQLIN.chop(1);
			strSQLIN="WHERE BGIT_ID IN ("+strSQLIN+")";
			strSQLIN+=" ORDER BY BGIT_ID";
			QString strSQL=" SELECT BGIT_ID,BGIT_CODE,BGIT_NAME,BGIT_LEVEL,BGIT_ICON,BGIT_PARENT,BGIT_TREE_ID,BGTR_NAME FROM BUS_GROUP_ITEMS";
			strSQL+=" INNER JOIN BUS_GROUP_TREE ON BGIT_TREE_ID=BGTR_ID ";
			strSQL+=strSQLIN;
			TableOrm_Person.GetDbSqlQuery()->Execute(Ret_pStatus,strSQL);
			if(!Ret_pStatus.IsOK()) return;
			DbRecordSet lstTemp;
			TableOrm_Person.GetDbSqlQuery()->FetchData(lstTemp);

			int nSize=lstTemp.getRowCount();
			for(int i=0;i<nSize;i++)
			{
				int nRow=RetOut_GAR.find("CGAR_GROUP_ID",lstTemp.getDataRef(i,"BGIT_ID").toInt(),true);
				if (nRow>=0){
					DbRecordSet row = lstTemp.getRow(i);
					RetOut_GAR.assignRow(nRow,row,true);
				}
			}
		}

		//----------------------Calc Parent (only first time)
		GetParentGUARRecord(Ret_pStatus,RetOut_UAR,RetOut_GAR,Ret_ParentRecord,Ret_ParentTableID,Ret_ParentName);
		if(!Ret_pStatus.IsOK()) return;
	}


	//----------------------UAR FROM GAR-> always (remove old records, add new)

	//reset all UAR group_dep:
	RetOut_UAR.find("CUAR_GROUP_DEPENDENT",1);
	RetOut_UAR.deleteSelectedRows();

	//find all users from GAR:
	QString strSQLIN;
	int nSize=RetOut_GAR.getRowCount();
	for(int i=0;i<nSize;i++)
	{
		strSQLIN.append(RetOut_GAR.getDataRef(i,"CGAR_GROUP_ID").toString());
		strSQLIN+=",";
	}
	if (!strSQLIN.isEmpty())
	{
		strSQLIN.chop(1);
		strSQLIN="WHERE BGCN_ITEM_ID IN ("+strSQLIN+")";
		strSQLIN+=" ORDER BY BGIT_ID,BPER_CODE";

		QString strSQL=" SELECT BPER_ID,BPER_CODE,BPER_FIRST_NAME,BPER_LAST_NAME,BGIT_ID,BGIT_CODE,BGIT_NAME,BGIT_LEVEL,BGIT_ICON,BGIT_PARENT,BGIT_TREE_ID,BGTR_NAME FROM BUS_CM_GROUP";
		strSQL+=" INNER JOIN BUS_PERSON ON BGCN_CONTACT_ID=BPER_CONTACT_ID ";
		strSQL+=" INNER JOIN BUS_GROUP_ITEMS ON BGCN_ITEM_ID=BGIT_ID ";
		strSQL+=" INNER JOIN BUS_GROUP_TREE ON BGIT_TREE_ID=BGTR_ID ";
		strSQL+=strSQLIN;

		TableOrm_Person.GetDbSqlQuery()->Execute(Ret_pStatus,strSQL);
		if(!Ret_pStatus.IsOK()) return;
		Ret_lstGroupUsers.destroy();
		TableOrm_Person.GetDbSqlQuery()->FetchData(Ret_lstGroupUsers);

		//prepare list:
		Ret_lstGroupUsers.addColumn(QVariant::Int,"CGAR_RIGHT");
		Ret_lstGroupUsers.addColumn(QVariant::String,"BPER_NAME");

		MainEntityCalculatedName fillerGroup;
		fillerGroup.Initialize(ENTITY_BUS_PERSON,Ret_lstGroupUsers);
		fillerGroup.FillCalculatedNames(Ret_lstGroupUsers,Ret_lstGroupUsers.getColumnIdx("BPER_NAME"));

		//remove all users from grouplist that are marked as group non-dependent inside UAR
		/*
		nSize=RetOut_UAR.getRowCount();
		for(int i=0;i<nSize;i++)
		{
			int nPersID=RetOut_UAR.getDataRef(i,"CUAR_PERSON_ID").toInt();
			Ret_lstGroupUsers.find("BPER_ID",nPersID);
			Ret_lstGroupUsers.deleteSelectedRows();
		}
		*/

		AccUARCore::UpdateUARGroupDependentRights(Ret_lstGroupUsers,RetOut_UAR,RetOut_GAR); //update/create UAR records from GAR data
		AccUARCore::UARList2DisplayList(&RetOut_UAR,&RetOut_GAR); //refill UAR records with CX
	}


	RetOut_UAR.setColValue(RetOut_UAR.getColumnIdx("CUAR_RECORD_ID"),nRecord);
	RetOut_UAR.setColValue(RetOut_UAR.getColumnIdx("CUAR_TABLE_ID"),nTableID);
	RetOut_GAR.setColValue(RetOut_GAR.getColumnIdx("CGAR_RECORD_ID"),nRecord);
	RetOut_GAR.setColValue(RetOut_GAR.getColumnIdx("CGAR_TABLE_ID"),nTableID);

}


void Service_AccessRights::GetParentGUARRecord(Status &Ret_pStatus, DbRecordSet &RetOut_UAR, DbRecordSet &RetOut_GAR, int &Ret_ParentRecord,int &Ret_ParentTableID,QString &Ret_ParentName)
{
	DbSqlQuery query(Ret_pStatus, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	Ret_ParentTableID=-1;
	Ret_ParentRecord=-1;

	int nSize=RetOut_UAR.getRowCount();
	int nUARParentID=0;
	for(int i=0;i<nSize;i++)
	{
		nUARParentID=RetOut_UAR.getDataRef(i,"CUAR_PARENT_ID").toInt();
		if (nUARParentID!=0)
			break;
	}

	if (nUARParentID>0)
	{
		DbRecordSet recParentData;
		QString strSQL=" SELECT CUAR_TABLE_ID,CUAR_RECORD_ID FROM CORE_ACC_USER_REC WHERE CUAR_ID="+QVariant(nUARParentID).toString();
		query.Execute(Ret_pStatus,strSQL);
		if(!Ret_pStatus.IsOK()) return;
		query.FetchData(recParentData);
		if (recParentData.getRowCount()>0)
		{
			Ret_ParentTableID=recParentData.getDataRef(0,"CUAR_TABLE_ID").toInt();
			Ret_ParentRecord=recParentData.getDataRef(0,"CUAR_RECORD_ID").toInt();
		}
	}
	else
	{
		int nGARParentID=0;
		int nSize=RetOut_GAR.getRowCount();
		for(int i=0;i<nSize;i++)
		{
			nGARParentID=RetOut_GAR.getDataRef(i,"CGAR_PARENT_ID").toInt();
			if (nGARParentID!=0)
				break;
		}
		DbRecordSet recParentData;
		QString strSQL=" SELECT CGAR_TABLE_ID,CGAR_RECORD_ID FROM CORE_ACC_GROUP_REC WHERE CGAR_ID="+QVariant(nGARParentID).toString();
		query.Execute(Ret_pStatus,strSQL);
		if(!Ret_pStatus.IsOK()) return;
		query.FetchData(recParentData);
		if (recParentData.getRowCount()>0)
		{
			Ret_ParentTableID=recParentData.getDataRef(0,"CGAR_TABLE_ID").toInt();
			Ret_ParentRecord=recParentData.getDataRef(0,"CGAR_RECORD_ID").toInt();
		}
	}


	//find name
	if (Ret_ParentTableID!=-1 && Ret_ParentRecord!=-1)
	{
		switch(Ret_ParentTableID)
		{
		case BUS_PROJECT:
			{
				QString strWhere =" BUSP_ID = "+QVariant(Ret_ParentRecord).toString();
				MainEntityFilter filter;
				filter.SetFilter(MainEntityFilter::FILTER_SQL_WHERE,strWhere);
				DbRecordSet recData;
				g_BusinessServiceSet->MainEntitySelector->ReadData(Ret_pStatus,ENTITY_BUS_PROJECT,filter.Serialize(),recData);
				if (recData.getRowCount()>0)
					Ret_ParentName=recData.getDataRef(0,"BUSP_CODE").toString() + QString(" ")+recData.getDataRef(0,"BUSP_NAME").toString();
				return;
			}
			break;
		default:
			return;
		    break;
		}
	}


}

void Service_AccessRights::GetProjectChildren(Status &Ret_pStatus,int nProjectID,DbRecordSet &lstProject_Ids,DbSqlQuery *query)
{
	QString strCode;
	m_ProjectData.GetCodeFromID(Ret_pStatus,nProjectID,strCode,"");
	if(!Ret_pStatus.IsOK()) return;
	DbRecordSet lstChilds;
	m_ProjectData.GetDescendantsNodes(strCode,lstChilds,"");
	lstProject_Ids.destroy();
	lstProject_Ids.addColumn(QVariant::Int,"BUSP_ID");
	lstProject_Ids.merge(lstChilds);
}

//get all comm entities from projects inside lstProject_Ids<BUSP_ID>, for document:<BDMD_ID,BUSP_ID,CUAR_PARENT_ID>
void Service_AccessRights::GetProjectCommEntities(Status &Ret_pStatus,int nTableID,const DbRecordSet &lstProject_Ids,DbRecordSet &lstIds,DbSqlQuery *query)
{
	int nCount=0;
	QString strSQL,strWhereIn;

	switch(nTableID)
	{
	case BUS_VOICECALLS:
		{
			lstIds.destroy();
			lstIds.addColumn(QVariant::Int,"BVC_ID");

			while (nCount>=0)
			{
				//compile FK in (n1,n2,...) statement
				nCount=DbSqlTableDefinition::GenerateChunkedWhereStatement(lstProject_Ids,"BUSP_ID",strWhereIn,nCount);
				if (nCount<0) break;
				strSQL="SELECT BVC_ID FROM BUS_VOICECALLS WHERE BVC_PROJECT_ID IN "+strWhereIn;
				query->Execute(Ret_pStatus,strSQL);
				if(!Ret_pStatus.IsOK()) return;
				DbRecordSet lstVC;
				query->FetchData(lstVC);
				lstIds.merge(lstVC);
			}

			return;
		}
		break;
	case BUS_EMAIL:
		{
			lstIds.destroy();
			lstIds.addColumn(QVariant::Int,"BEM_ID");
			while (nCount>=0)
			{
				//compile FK in (n1,n2,...) statement
				nCount=DbSqlTableDefinition::GenerateChunkedWhereStatement(lstProject_Ids,"BUSP_ID",strWhereIn,nCount);
				if (nCount<0) break;
				strSQL="SELECT BEM_ID FROM BUS_EMAIL WHERE BEM_PROJECT_ID IN "+strWhereIn;
				query->Execute(Ret_pStatus,strSQL);
				if(!Ret_pStatus.IsOK()) return;
				DbRecordSet lstEmail;
				query->FetchData(lstEmail);
				lstIds.merge(lstEmail);
			}

		}
		break;
	case BUS_DM_DOCUMENTS: 
		{
			lstIds.destroy();
			lstIds.addColumn(QVariant::Int,"BDMD_ID");
			while (nCount>=0)
			{
				//compile FK in (n1,n2,...) statement
				nCount=DbSqlTableDefinition::GenerateChunkedWhereStatement(lstProject_Ids,"BUSP_ID",strWhereIn,nCount);
				if (nCount<0) break;
				strSQL="SELECT BDMD_ID FROM BUS_NMRX_RELATION INNER JOIN CE_COMM_ENTITY ON CENT_ID=BNMR_TABLE_KEY_ID_1 AND "+QString(NMRX_CE_PROJECT_WHERE);
				strSQL +=" INNER JOIN BUS_DM_DOCUMENTS ON CENT_ID=BDMD_COMM_ENTITY_ID";
				strSQL +=" WHERE BNMR_TABLE_KEY_ID_2 IN "+strWhereIn;;
				strSQL +=" AND CENT_SYSTEM_TYPE_ID= "+QVariant(GlobalConstants::CE_TYPE_DOCUMENT).toString();
				query->Execute(Ret_pStatus,strSQL);
				if(!Ret_pStatus.IsOK()) return;
				DbRecordSet lstDocs;
				query->FetchData(lstDocs);
				lstIds.merge(lstDocs);
			}
		}
	    break;
	default:
		Q_ASSERT(false);
	    break;
	}
}

//returns assigned document projects that have UAR and inherit=1 and their parents: lstResult<BDMD_ID,BUSP_ID,PARENT_PROJ_ID>
//for one document there cen be more entries, for each BUSP_ID, one parent can exists
//bDocIds = lstIds is <BDMD_ID> list or false= <BUSP_ID>
void Service_AccessRights::GetProjectDocuments(Status &Ret_pStatus,const DbRecordSet &lstIds,bool bDocIds,DbRecordSet &lstResult,DbSqlQuery *query)
{
	//get all from CE_LINK table, join to UAR to test if inherit is enable, same to GAR

	//------------------TEST UAR+CE_LINK:
	lstResult.destroy();
	lstResult.addColumn(QVariant::Int,"BDMD_ID");
	lstResult.addColumn(QVariant::Int,"BUSP_ID");
	lstResult.addColumn(QVariant::Int,"PARENT_PROJ_ID");

	int nCount=0;
	QString strSQL,strWhereIn;
	while (nCount>=0)
	{
		//compile FK in (n1,n2,...) statement
		if (bDocIds)
			nCount=DbSqlTableDefinition::GenerateChunkedWhereStatement(lstIds,"BDMD_ID",strWhereIn,nCount);
		else
			nCount=DbSqlTableDefinition::GenerateChunkedWhereStatement(lstIds,"BUSP_ID",strWhereIn,nCount);
		if (nCount<0) break;

		strSQL="SELECT DISTINCT BDMD_ID,BNMR_TABLE_KEY_ID_2 AS BUSP_ID, B.CUAR_RECORD_ID AS PARENT_PROJ_ID FROM BUS_NMRX_RELATION INNER JOIN CE_COMM_ENTITY ON CENT_ID=BNMR_TABLE_KEY_ID_1 AND "+QString(NMRX_CE_PROJECT_WHERE);
		strSQL +=" INNER JOIN BUS_DM_DOCUMENTS ON CENT_ID=BDMD_COMM_ENTITY_ID";
		strSQL +=" INNER JOIN CORE_ACC_USER_REC A ON A.CUAR_RECORD_ID=BNMR_TABLE_KEY_ID_2 AND A.CUAR_TABLE_ID="+QVariant(BUS_PROJECT).toString()+" AND A.CUAR_CHILD_COMM_INHERIT=1";
		strSQL +=" LEFT OUTER JOIN CORE_ACC_USER_REC B ON A.CUAR_PARENT_ID=B.CUAR_ID";
		if (bDocIds)
			strSQL +=" WHERE BDMD_ID IN "+strWhereIn;
		else
			strSQL +=" WHERE BNMR_TABLE_KEY_ID_2 IN "+strWhereIn;
		strSQL +=" AND CENT_SYSTEM_TYPE_ID= "+QVariant(GlobalConstants::CE_TYPE_DOCUMENT).toString();
		query->Execute(Ret_pStatus,strSQL);
		if(!Ret_pStatus.IsOK()) return;
		DbRecordSet lstTemp;
		query->FetchData(lstTemp);
		lstResult.merge(lstTemp);
	}

	//------------------TEST GAR+CE_LINK:
	DbRecordSet lstResultGAR;
	lstResultGAR.destroy();
	lstResultGAR.addColumn(QVariant::Int,"BDMD_ID");
	lstResultGAR.addColumn(QVariant::Int,"BUSP_ID");
	lstResultGAR.addColumn(QVariant::Int,"PARENT_PROJ_ID");

	nCount=0;
	strWhereIn="";

	while (nCount>=0)
	{
		//compile FK in (n1,n2,...) statement
		if (bDocIds)
			nCount=DbSqlTableDefinition::GenerateChunkedWhereStatement(lstIds,"BDMD_ID",strWhereIn,nCount);
		else
			nCount=DbSqlTableDefinition::GenerateChunkedWhereStatement(lstIds,"BUSP_ID",strWhereIn,nCount);
		if (nCount<0) break;

		strSQL="SELECT DISTINCT BDMD_ID,BNMR_TABLE_KEY_ID_2 AS BUSP_ID, B.CGAR_RECORD_ID AS PARENT_PROJ_ID FROM BUS_NMRX_RELATION INNER JOIN CE_COMM_ENTITY ON CENT_ID=BNMR_TABLE_KEY_ID_1 AND "+QString(NMRX_CE_PROJECT_WHERE);
		strSQL +=" INNER JOIN BUS_DM_DOCUMENTS ON CENT_ID=BDMD_COMM_ENTITY_ID";
		strSQL +=" INNER JOIN CORE_ACC_GROUP_REC A ON A.CGAR_RECORD_ID=BNMR_TABLE_KEY_ID_2 AND A.CGAR_TABLE_ID="+QVariant(BUS_PROJECT).toString()+" AND A.CGAR_CHILD_COMM_INHERIT=1";
		strSQL +=" LEFT JOIN CORE_ACC_GROUP_REC B ON A.CGAR_PARENT_ID=B.CGAR_ID";
		if (bDocIds)
			strSQL +=" WHERE BDMD_ID IN "+strWhereIn;
		else
			strSQL +=" WHERE BNMR_TABLE_KEY_ID_2 IN "+strWhereIn;
		strSQL +=" AND CENT_SYSTEM_TYPE_ID= "+QVariant(GlobalConstants::CE_TYPE_DOCUMENT).toString();
		query->Execute(Ret_pStatus,strSQL);
		if(!Ret_pStatus.IsOK()) return;
		DbRecordSet lstTemp;
		query->FetchData(lstTemp);
		lstResultGAR.merge(lstTemp);
	}

	//filter tables: remove duplicates:
	if (lstResultGAR.getRowCount()>0)
	{
		lstResultGAR.clearSelection();
		int nSize=lstResultGAR.getRowCount();
		for(int i=0;i<nSize;i++)
		{
			int nDocID=lstResultGAR.getDataRef(i,0).toInt();
			int nRow=lstResult.find(0,nDocID,true);
			if (nRow>=0)
			{
				if (lstResultGAR.getDataRef(i,1).toInt()==lstResult.getDataRef(nRow,1).toInt() && lstResultGAR.getDataRef(i,2).toInt()==lstResult.getDataRef(nRow,2).toInt())
				{
					lstResultGAR.selectRow(i);
				}
			}
		}
		lstResultGAR.deleteSelectedRows();
		lstResult.merge(lstResultGAR);
	}
}


void Service_AccessRights::ResetParentOnProjectChildren(Status &Ret_pStatus,const DbRecordSet &RetOut_UAR, const DbRecordSet &RetOut_GAR,int nChildrenTableID,DbSqlQuery *query)
{
	//UAR
	//update parent=NULL:
	int nCount=0;
	QString strSQL,strWhereIn;
	while (nCount>=0)
	{
		//compile FK in (n1,n2,...) statement
		nCount=DbSqlTableDefinition::GenerateChunkedWhereStatement(RetOut_UAR,"CUAR_ID",strWhereIn,nCount);
		if (nCount<0) break;
		strSQL="UPDATE CORE_ACC_USER_REC SET CUAR_PARENT_ID=NULL WHERE CUAR_PARENT_ID IN "+strWhereIn+ " AND CUAR_TABLE_ID ="+QVariant(nChildrenTableID).toString();
		query->Execute(Ret_pStatus,strSQL);
		if(!Ret_pStatus.IsOK()) return;
	}
	//GAR
	nCount=0;
	strWhereIn="";
	while (nCount>=0)
	{
		//compile FK in (n1,n2,...) statement
		nCount=DbSqlTableDefinition::GenerateChunkedWhereStatement(RetOut_GAR,"CGAR_ID",strWhereIn,nCount);
		if (nCount<0) break;
		strSQL="UPDATE CORE_ACC_GROUP_REC SET CGAR_PARENT_ID=NULL WHERE CGAR_PARENT_ID IN "+strWhereIn+" AND CGAR_TABLE_ID ="+QVariant(nChildrenTableID).toString();
		query->Execute(Ret_pStatus,strSQL);
		if(!Ret_pStatus.IsOK()) return;
	}
}

//rest UAR/GAR records on DB using record id's stored inside lstIds in column strPrimaryKeyCol
void Service_AccessRights::ResetParentOnGUARRecords(Status &Ret_pStatus,int nTableID,const DbRecordSet &lstIds,QString strPrimaryKeyCol,DbSqlQuery *query)
{
	//update parent=NULL:
	int nCount=0;
	QString strSQL,strWhereIn;
	while (nCount>=0)
	{
		//compile FK in (n1,n2,...) statement
		nCount=DbSqlTableDefinition::GenerateChunkedWhereStatement(lstIds,strPrimaryKeyCol,strWhereIn,nCount);
		if (nCount<0) break;
		strSQL="UPDATE CORE_ACC_USER_REC SET CUAR_PARENT_ID=NULL WHERE CUAR_RECORD_ID IN "+strWhereIn+ " AND CUAR_TABLE_ID ="+QVariant(nTableID).toString();
		query->Execute(Ret_pStatus,strSQL);
		if(!Ret_pStatus.IsOK()) return;
	}
	//GAR
	nCount=0;
	strWhereIn="";
	while (nCount>=0)
	{
		//compile FK in (n1,n2,...) statement
		nCount=DbSqlTableDefinition::GenerateChunkedWhereStatement(lstIds,strPrimaryKeyCol,strWhereIn,nCount);
		if (nCount<0) break;
		strSQL="UPDATE CORE_ACC_GROUP_REC SET CGAR_PARENT_ID=NULL WHERE CGAR_RECORD_ID IN "+strWhereIn+" AND CGAR_TABLE_ID ="+QVariant(nTableID).toString();
		query->Execute(Ret_pStatus,strSQL);
		if(!Ret_pStatus.IsOK()) return;
	}

}

//test list of ID's lstTest_IDs for given person, table and access level
//Ret_lstResult_IDs is returned with either allowed id's (bReturnAllowed=true) or forbidden (bReturnAllowed=false)
//nAccessLevel=AccUARCore::AccRightLevel <ACC_LVL_NO_RIGHT,ACC_LVL_READ...>
void Service_AccessRights::TestUAR(Status &Ret_pStatus, int nTableID, int nPersonID, int nAccessLevel, DbRecordSet &lstTest_IDs, DbRecordSet &Ret_lstResult_IDs, bool bReturnAllowed)
{
	DbSqlQuery query(Ret_pStatus, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	QString strPrimaryKey=DbSqlTableDefinition::GetTablePrimaryKey(nTableID);
	QString strTableName=DbSqlTableDefinition::GetTableName(nTableID);
	
	QString strSQL="SELECT "+strPrimaryKey+" FROM "+strTableName;
	strSQL+=" LEFT OUTER JOIN CORE_ACC_USER_REC UAR_A ON UAR_A.CUAR_RECORD_ID="+strPrimaryKey+" AND UAR_A.CUAR_TABLE_ID="+QVariant(nTableID).toString()+" AND UAR_A.CUAR_PERSON_ID="+QVariant(nPersonID).toString();
	strSQL+=" LEFT OUTER JOIN CORE_ACC_USER_REC UAR_B ON UAR_B.CUAR_RECORD_ID="+strPrimaryKey+" AND UAR_B.CUAR_TABLE_ID="+QVariant(nTableID).toString()+" AND UAR_B.CUAR_PERSON_ID IS NULL";

	QString strWhere;
	if (bReturnAllowed)
	{
		strWhere=" WHERE ((UAR_A.CUAR_RIGHT IS NULL AND UAR_B.CUAR_RIGHT IS NULL) OR";
		strWhere+=" (UAR_A.CUAR_RIGHT>="+QVariant(nAccessLevel).toString()+" OR UAR_A.CUAR_RIGHT IS NULL AND UAR_B.CUAR_RIGHT>="+QVariant(nAccessLevel).toString()+"))";
	}
	else
	{
		strWhere=" WHERE (UAR_A.CUAR_RIGHT<"+QVariant(nAccessLevel).toString()+" OR UAR_A.CUAR_RIGHT IS NULL AND UAR_B.CUAR_RIGHT<"+QVariant(nAccessLevel).toString()+")";
	}

	int nCount=0;
	QString strWhereIn;
	Ret_lstResult_IDs.copyDefinition(lstTest_IDs);
	while (nCount>=0)
	{
		//compile FK in (n1,n2,...) statement
		nCount=DbSqlTableDefinition::GenerateChunkedWhereStatement(lstTest_IDs,strPrimaryKey,strWhereIn,nCount);
		if (nCount<0) break;
		QString strSQLExec=strSQL+strWhere+" AND "+strPrimaryKey+" IN "+strWhereIn;
		query.Execute(Ret_pStatus,strSQLExec);
		if(!Ret_pStatus.IsOK()) return;
		DbRecordSet lstChunk;
		query.FetchData(lstChunk);
		Ret_lstResult_IDs.merge(lstChunk);
	}

}

//update UAR/GAR records from inherited RetOut_UAR and RetOut_GAR on lstIds
void Service_AccessRights::UpdateGUARRecords(Status &Ret_pStatus,int nTableID,const DbRecordSet &lstIds,DbRecordSet &lstUAR, DbRecordSet &lstGAR, DbSimpleOrm *ormUAR,DbSimpleOrm *ormGAR)
{
	QString strPK=DbSqlTableDefinition::GetTablePrimaryKey(nTableID);

	//DELETE OLD UAR/GAR:
	//get all UAR/GAR records connected to those ID's -> delete'em all
	ormUAR->DeleteFromParentIDs(Ret_pStatus,"CUAR_RECORD_ID",lstIds,strPK,""," AND CUAR_TABLE_ID ="+QVariant(nTableID).toString());
	if(!Ret_pStatus.IsOK()) return;
	ormGAR->DeleteFromParentIDs(Ret_pStatus,"CGAR_RECORD_ID",lstIds,strPK,""," AND CGAR_TABLE_ID ="+QVariant(nTableID).toString());
	if(!Ret_pStatus.IsOK()) return;

	//save UAR/GAR
	if (lstUAR.getRowCount()>0)
	{
		ormUAR->Write(Ret_pStatus,lstUAR);
		if(!Ret_pStatus.IsOK()) return;
	}
	if (lstGAR.getRowCount()>0)
	{
		ormGAR->Write(Ret_pStatus,lstGAR);
		if(!Ret_pStatus.IsOK()) return;	
	}
}


//update children project UAR if insert (and parent has UAR defined) and if renamed/edit (move'd) to another parent or without parent 
void Service_AccessRights::UpdateProjectGUAR(Status &Ret_pStatus, DbRecordSet &lstProject_IDs)
{
	DbSimpleOrm TableOrm_UAR(Ret_pStatus, CORE_ACC_USER_REC, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	DbSimpleOrm TableOrm_GAR(Ret_pStatus, CORE_ACC_GROUP_REC, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	//resolve project without parent->lstAssignedProjects
	//resolve project with parent->lstUnAssignedProjects
	DbRecordSet lstAssignedProjects, lstUnAssignedProjects;
	lstAssignedProjects.addColumn(QVariant::Int,"BUSP_ID");
	lstAssignedProjects.addColumn(QVariant::Int,"BUSP_PARENT");
	lstUnAssignedProjects.copyDefinition(lstAssignedProjects);

	int nSize=lstProject_IDs.getRowCount();
	for(int i=0;i<nSize;i++)
	{
		if (lstProject_IDs.getDataRef(i,"BUSP_PARENT").toInt()==0)
		{
			lstUnAssignedProjects.addRow();//projects with parent
			lstUnAssignedProjects.setData(lstUnAssignedProjects.getRowCount()-1,"BUSP_ID",lstProject_IDs.getDataRef(i,"BUSP_ID").toInt());
			lstUnAssignedProjects.setData(lstUnAssignedProjects.getRowCount()-1,"BUSP_PARENT",lstProject_IDs.getDataRef(i,"BUSP_PARENT").toInt());
		}
		else
		{
			lstAssignedProjects.addRow(); //orphan projects
			lstAssignedProjects.setData(lstAssignedProjects.getRowCount()-1,"BUSP_ID",lstProject_IDs.getDataRef(i,"BUSP_ID").toInt());
			lstAssignedProjects.setData(lstAssignedProjects.getRowCount()-1,"BUSP_PARENT",lstProject_IDs.getDataRef(i,"BUSP_PARENT").toInt());
		}
	}

	DbSqlQuery *query=TableOrm_UAR.GetDbSqlQuery();

	//start transaction, go go update:
	query->BeginTransaction(Ret_pStatus);
	if(!Ret_pStatus.IsOK()) return;
	UpdateGUARAfterProjectAssignment(Ret_pStatus,BUS_PROJECT,"BUSP_PARENT",lstAssignedProjects,lstUnAssignedProjects,&TableOrm_UAR,&TableOrm_GAR);
	if(!Ret_pStatus.IsOK()) 
	{
		query->Rollback();
		return;
	}
	query->Commit(Ret_pStatus);

}


//if email, document or voice call is assigned to the project, if project has inherit flag, set new UAR/GAR
void Service_AccessRights::UpdateProjectCommEntityGUAR(Status &Ret_pStatus, int nTableID, DbRecordSet &lstData)
{
	DbSimpleOrm TableOrm_UAR(Ret_pStatus, CORE_ACC_USER_REC, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	DbSimpleOrm TableOrm_GAR(Ret_pStatus, CORE_ACC_GROUP_REC, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	DbSqlQuery *query=TableOrm_UAR.GetDbSqlQuery();
	QString strPK=DbSqlTableDefinition::GetTablePrimaryKey(nTableID);

	//A: INSERT NEW -> update to new from parent
	//B: EDIT: NO_PROJECT -> reset parent
	//C: EDIT: NEW_PROJECT WITH INHERIT -> update to new from parent
	//D: EDIT: NEW_PRJECT WITHOUT INHERIT -> reset parent

	DbRecordSet lstAssignedProjects;
	if (nTableID==BUS_DM_DOCUMENTS) //if document is already from parent, test assignment: if found inside list assign that specified row.
	{
		//get all document/project pairs
		GetProjectDocuments(Ret_pStatus,lstData,true,lstAssignedProjects,query);
		if(!Ret_pStatus.IsOK()) return;


		//if documents and if already have parent project in UAR which is different then assigned parent, then skip 'em
		DbRecordSet lstGUARProjects;
		GetParentGUARProjects(Ret_pStatus,nTableID,lstData,lstGUARProjects,query);
		if(!Ret_pStatus.IsOK()) return;

		int nSize=lstGUARProjects.getRowCount();
		for(int i=0;i<nSize;i++)
		{
			int nDocID=lstGUARProjects.getDataRef(i,"BDMD_ID").toInt();
			int nProjID=lstGUARProjects.getDataRef(i,"BUSP_ID").toInt();
			lstAssignedProjects.find("BDMD_ID",nDocID);
			DbRecordSet lstTemp;
			lstTemp.copyDefinition(lstAssignedProjects);
			lstTemp.merge(lstAssignedProjects,true);
			if (lstTemp.getRowCount()>0) //if parent record found, try to filter that one, else ignore->will be overwritten
			{
				int nRow=lstTemp.find("BUSP_ID",nProjID,true);
				if (nRow<0)
					nRow=lstTemp.find("PARENT_PROJ_ID",nProjID,true);
				if (nRow>=0)
				{
					lstAssignedProjects.deleteSelectedRows();
					lstAssignedProjects.addRow();
					DbRecordSet row = lstTemp.getRow(nRow);
					lstAssignedProjects.assignRow(lstAssignedProjects.getRowCount()-1,row); //if parent is found set it in list:
				}
			}
		}
		lstAssignedProjects.removeDuplicates(0);
	}
	else
	{
		GetCommEntityProjects(Ret_pStatus,nTableID,lstData,lstAssignedProjects,query);
		if(!Ret_pStatus.IsOK()) return;
	}

	//parse through list, find unassigned and duplicates
	lstAssignedProjects.sort(0); //by 1st col
	DbRecordSet lstUnAssignedProjects;
	lstUnAssignedProjects.copyDefinition(lstAssignedProjects);
	int nSize=lstData.getRowCount();
	for(int i=0;i<nSize;i++)
	{
		int nID=lstData.getDataRef(i,strPK).toInt();
		lstAssignedProjects.find(strPK,nID);
		int nCnt=lstAssignedProjects.getSelectedCount();
		if (nCnt==0) //if not assigned, go unsigned
		{
			lstUnAssignedProjects.addRow();
			lstUnAssignedProjects.setData(lstUnAssignedProjects.getRowCount()-1,0,nID);
		}
		else //if (nCnt>0) //if something selected, just take first, others delete: random choose: assign to first which comes...
		{
			lstAssignedProjects.selectRow(lstAssignedProjects.getSelectedRow(),false); //deselect first -> keep it
			lstAssignedProjects.deleteSelectedRows(); // remove others..
		}
	}

	//start transaction, go go update:
	query->BeginTransaction(Ret_pStatus);
	if(!Ret_pStatus.IsOK()) return;
	UpdateGUARAfterProjectAssignment(Ret_pStatus,nTableID,"BUSP_ID",lstAssignedProjects,lstUnAssignedProjects,&TableOrm_UAR,&TableOrm_GAR);
	if(!Ret_pStatus.IsOK()) 
	{
		query->Rollback();
		return;
	}
	query->Commit(Ret_pStatus);
}



//Returns all projects to which comm entity records are assigned: 
//lstData is comm entity records, lstProject_Ids is returned as <PK,BUSP_ID> if project not assigned, no entry inside list
//WARNING: for DOCUMENTS: returns only actual projects that are assigned and have UAR=inherited on comm childs
void Service_AccessRights::GetCommEntityProjects(Status &Ret_pStatus,int nTableID,DbRecordSet &lstData,DbRecordSet &lstProject_Ids,DbSqlQuery *query)
{
	QString strPK=DbSqlTableDefinition::GetTablePrimaryKey(nTableID);
	lstProject_Ids.destroy();
	lstProject_Ids.addColumn(QVariant::Int,strPK);
	lstProject_Ids.addColumn(QVariant::Int,"BUSP_ID");

	int nCount=0;
	QString strSQL,strWhereIn;

	switch(nTableID)
	{
	case BUS_VOICECALLS:
		{
			lstProject_Ids.renameColumn(1,"BVC_PROJECT_ID");
			lstProject_Ids.merge(lstData);
			lstProject_Ids.renameColumn(1,"BUSP_ID");
		}
		break;
	case BUS_EMAIL:
		{
			lstProject_Ids.renameColumn(1,"BEM_PROJECT_ID");
			lstProject_Ids.merge(lstData);
			lstProject_Ids.renameColumn(1,"BUSP_ID");
		}
		break;
	case BUS_DM_DOCUMENTS:
		{
			int nCount=0;
			QString strSQL,strWhereIn;
			while (nCount>=0)
			{
				//compile FK in (n1,n2,...) statement
				nCount=DbSqlTableDefinition::GenerateChunkedWhereStatement(lstData,"BDMD_ID",strWhereIn,nCount);
				if (nCount<0) break;
				strSQL="SELECT DISTINCT BDMD_ID,BNMR_TABLE_KEY_ID_2 AS BUSP_ID FROM BUS_NMRX_RELATION INNER JOIN CE_COMM_ENTITY ON CENT_ID=BNMR_TABLE_KEY_ID_1 AND "+QString(NMRX_CE_PROJECT_WHERE);
				strSQL +=" INNER JOIN BUS_DM_DOCUMENTS ON CENT_ID=BDMD_COMM_ENTITY_ID";
				strSQL +=" INNER JOIN CORE_ACC_USER_REC ON CUAR_RECORD_ID=BNMR_TABLE_KEY_ID_2 AND CUAR_TABLE_ID="+QVariant(BUS_PROJECT).toString()+" AND CUAR_CHILD_COMM_INHERIT=1";
				strSQL +=" WHERE BDMD_ID IN "+strWhereIn;;
				strSQL +=" AND CENT_SYSTEM_TYPE_ID= "+QVariant(GlobalConstants::CE_TYPE_DOCUMENT).toString();
				query->Execute(Ret_pStatus,strSQL);
				if(!Ret_pStatus.IsOK()) return;
				DbRecordSet lstTemp;
				query->FetchData(lstTemp);
				lstProject_Ids.merge(lstTemp);
			}
		}
		break;
	default:
		Q_ASSERT(false);
		break;
	}

	lstProject_Ids.find(1,(int)0);
	lstProject_Ids.deleteSelectedRows(); //delete all with parent=NULL;
}


//from entity records inside lstData<PK> fetch parent UAR/GAR records, stores parent project id inside lstParent_Ids<PK,BUSP_ID>, if not found then it will be not in list lstParent_Ids
//if nTable=PROJECT then return list is <PK,BUSP_ID_PARENT>
void Service_AccessRights::GetParentGUARProjects(Status &Ret_pStatus,int nTableID,const DbRecordSet &lstData,DbRecordSet &lstProject_Ids,DbSqlQuery *query)
{
	QString strParentColName="BUSP_ID";
	if (nTableID==BUS_PROJECT)
	{
		strParentColName="BUSP_ID_PARENT";
	}

	QString strPK=DbSqlTableDefinition::GetTablePrimaryKey(nTableID);
	lstProject_Ids.destroy();
	lstProject_Ids.addColumn(QVariant::Int,strPK);
	lstProject_Ids.addColumn(QVariant::Int,strParentColName);

	//----------------------------------------TEST UAR
	QString strSQL_Pre=" SELECT DISTINCT A.CUAR_RECORD_ID AS "+strPK+", B.CUAR_RECORD_ID AS "+strParentColName+" FROM CORE_ACC_USER_REC A";
	strSQL_Pre+=" LEFT OUTER JOIN CORE_ACC_USER_REC B ON A.CUAR_PARENT_ID=B.CUAR_ID WHERE A.CUAR_RECORD_ID IN ";
	int nCount=0;
	QString strSQL,strWhereIn;
	
	while (nCount>=0)
	{
		//compile FK in (n1,n2,...) statement
		nCount=DbSqlTableDefinition::GenerateChunkedWhereStatement(lstData,strPK,strWhereIn,nCount);
		if (nCount<0) break;
		strSQL=strSQL_Pre+ strWhereIn+" AND A.CUAR_TABLE_ID ="+QVariant(nTableID).toString();
		query->Execute(Ret_pStatus,strSQL);
		if(!Ret_pStatus.IsOK()) return;
		DbRecordSet lstTemp;
		query->FetchData(lstTemp);
		lstProject_Ids.merge(lstTemp);
	}

	//----------------------------------------TEST GAR
	DbRecordSet lstLeftOverIds;
	lstLeftOverIds.addColumn(QVariant::Int,strPK);
	int nSize=lstData.getRowCount();
	for(int i=0;i<nSize;i++)
	{
		if (lstProject_Ids.find(strPK,lstData.getDataRef(i,strPK).toInt(),true)<0)
		{
			lstLeftOverIds.addRow();
			lstLeftOverIds.setData(lstLeftOverIds.getRowCount()-1,strPK,lstData.getDataRef(i,strPK).toInt());
		}
	}

	lstProject_Ids.find(1,0);
	lstProject_Ids.deleteSelectedRows(); //delete all with parent=NULL;

	if (lstLeftOverIds.getRowCount()==0) //all ID's tested
		return;

	strSQL_Pre=" SELECT DISTINCT A.CGAR_RECORD_ID AS "+strPK+", B.CGAR_RECORD_ID AS "+strParentColName+" FROM CORE_ACC_GROUP_REC A";
	strSQL_Pre+=" INNER JOIN CORE_ACC_GROUP_REC B ON A.CGAR_PARENT_ID=B.CGAR_ID WHERE A.CGAR_RECORD_ID IN ";
	nCount=0;
	strWhereIn="";

	while (nCount>=0)
	{
		//compile FK in (n1,n2,...) statement
		nCount=DbSqlTableDefinition::GenerateChunkedWhereStatement(lstLeftOverIds,strPK,strWhereIn,nCount);
		if (nCount<0) break;
		strSQL=strSQL_Pre+ strWhereIn+" AND A.CGAR_TABLE_ID ="+QVariant(nTableID).toString();
		query->Execute(Ret_pStatus,strSQL);
		if(!Ret_pStatus.IsOK()) return;
		DbRecordSet lstTemp;
		query->FetchData(lstTemp);
		lstProject_Ids.merge(lstTemp);
	}

}

//Call inside transaction
//After communication object or sub project is assigned to project, propagate assigned/parent project UAR/GAR on 'em
//lstAssignProject_Ids<PK,strParentColName> -> for each PK, retrieves UAR/GAR and copies to new PK, old one is deleted
//lstUnAssignProject_Ids<PK,strParentColName> -> for each PK, resets parent_id on existing UAR/GAR
void Service_AccessRights::UpdateGUARAfterProjectAssignment(Status &Ret_pStatus, int nTableID,QString strParentColName,DbRecordSet &lstAssignProject_Ids, DbRecordSet &lstUnAssignProject_Ids,DbSimpleOrm *ormUAR,DbSimpleOrm *ormGAR)
{
	QString strPK=DbSqlTableDefinition::GetTablePrimaryKey(nTableID);
	DbSqlQuery *query=ormUAR->GetDbSqlQuery();

	//update parent=NULL -> for unassigned, keep UAR/GAR but reset parent
	//-------------------------------------------------------------------------
	if (lstUnAssignProject_Ids.getRowCount()>0)
	{
		ResetParentOnGUARRecords(Ret_pStatus,nTableID,lstUnAssignProject_Ids,strPK,query);
		if(!Ret_pStatus.IsOK()) return;
	}


	if (lstAssignProject_Ids.getRowCount()>0)
	{
		//Read UAR/GAR from project that are assigned to our entities: reuslt could be: 1) no UAR/GAR->reset parent 2) UAR/GAR with no inherit->reset parent 3) UAR/GAR inherit ->create new, delete old
		//-------------------------------------------------------------------------
		DbRecordSet lstUAR,lstGAR;
		ormUAR->ReadFromParentIDs(Ret_pStatus,lstUAR,"CUAR_RECORD_ID",lstAssignProject_Ids,strParentColName,""," AND CUAR_TABLE_ID ="+QVariant(BUS_PROJECT).toString());
		if(!Ret_pStatus.IsOK()) return;
		ormGAR->ReadFromParentIDs(Ret_pStatus,lstGAR,"CGAR_RECORD_ID",lstAssignProject_Ids,strParentColName,""," AND CGAR_TABLE_ID ="+QVariant(BUS_PROJECT).toString());
		if(!Ret_pStatus.IsOK()) return;

		//C: if parent UAR/GAR is not inherit: update own UAR/GAR, set parent=NULL
		//-------------------------------------------------------------------------
		//D: if parent UAR/GAR is inherit: delete own UAR/GAR, inherit from parent
		//-------------------------------------------------------------------------
		lstUAR.sort("CUAR_RECORD_ID");
		lstGAR.sort("CGAR_RECORD_ID");
		DbRecordSet lstUAR_1,lstGAR_1;
		DbRecordSet lstUAR_ForWrite,lstGAR_ForWrite;
		lstUAR_1.copyDefinition(lstUAR);
		lstGAR_1.copyDefinition(lstGAR);
		lstUAR_ForWrite.copyDefinition(lstUAR);
		lstGAR_ForWrite.copyDefinition(lstGAR);
		lstAssignProject_Ids.clearSelection();

		int nSize=lstAssignProject_Ids.getRowCount();
		for(int i=0;i<nSize;i++)
		{
			int nParentID=lstAssignProject_Ids.getDataRef(i,strParentColName).toInt();
			lstUAR_1.clear();
			lstGAR_1.clear();
			lstUAR.find("CUAR_RECORD_ID",nParentID);
			lstGAR.find("CGAR_RECORD_ID",nParentID);
			lstUAR_1.merge(lstUAR,true);
			lstGAR_1.merge(lstGAR,true);
			//test to see if project is not to be inherited:
			if (nTableID==BUS_PROJECT) //for project, test subchild inheritance
			{
				if(lstUAR_1.find("CUAR_CHILD_INHERIT",1,true)<0)
					if(lstGAR_1.find("CGAR_CHILD_INHERIT",1,true)<0)
					{
						lstAssignProject_Ids.selectRow(i); //mark for parent reset as no inheritance needed, keep uar/s but reset old parent
						continue;
					}
			}
			else //for comm object, test comm inheritance
			{
				if(lstUAR_1.find("CUAR_CHILD_COMM_INHERIT",1,true)<0)
					if(lstGAR_1.find("CGAR_CHILD_COMM_INHERIT",1,true)<0)
					{
						lstAssignProject_Ids.selectRow(i); //mark for parent reset as no inheritance needed, keep uar/s but reset old parent
						continue;
					}
			}
			//create new UAR/GAR for project: copy from parent, set parent = _id, set _id=NULL
			DbRecordSet lstRecords;
			lstRecords.addColumn(QVariant::Int,strPK);
			lstRecords.addRow();
			lstRecords.setData(0,0,lstAssignProject_Ids.getDataRef(i,strPK).toInt());
			DbRecordSet lstTargetUAR,lstTargetGAR;
			AccUARCore::CopyGUAR(nTableID,lstRecords,strPK,lstUAR_1,lstGAR_1,lstTargetUAR,lstTargetGAR);
			lstUAR_ForWrite.merge(lstTargetUAR);
			lstGAR_ForWrite.merge(lstTargetGAR);
		}

		//C: if parent UAR/GAR is not inherit: update own UAR/GAR, set parent=NULL
		DbRecordSet lstProj_Reset; 
		lstProj_Reset.copyDefinition(lstAssignProject_Ids);
		lstProj_Reset.merge(lstAssignProject_Ids,true);
		if (lstProj_Reset.getRowCount()>0)
		{
			ResetParentOnGUARRecords(Ret_pStatus,nTableID,lstProj_Reset,strPK,query);
			if(!Ret_pStatus.IsOK()) return;
		}

		//D: if parent UAR/GAR is inherit: delete own UAR/GAR, inherit from parent
		//-------------------------------------------------------------------------
		lstAssignProject_Ids.deleteSelectedRows();
		if (lstAssignProject_Ids.getRowCount()>0)
		{
			UpdateGUARRecords(Ret_pStatus,nTableID,lstAssignProject_Ids,lstUAR_ForWrite,lstGAR_ForWrite,ormUAR,ormGAR);
			if(!Ret_pStatus.IsOK()) return;
		}
	}
}


//after contact is added to the group, removed from group generate UAR's
//if group is deleted/GUAR changed->update user GAUR's!!!
//if new group is added as subgroup!!!???
//lstData group nodes for write <BGIT_ID,....>
//nOperation= 0-insert, 1-edit, 2-before delete
void Service_AccessRights::UpdateGroupContactsGUAR(Status &Ret_pStatus, DbRecordSet &lstData)
{
	RecalculateUARGroupContacts(Ret_pStatus,lstData);

	//EDIT: update RIGHT on existing, add new, remove if not exists in other groups
	
	//1. get group GAR records, 
	//2. for given CGAR_RECORD_ID's, get all groups and their contact/users, calc (max right) from all groups for each contact/user
	//3. get (group GAR records) their UAR with group_dep=1, compare two lists: add new UAR ->MAX RIGHT, update existing MAX RIGHT or remove old UAR
	
	//DELETE: remove UAR if not inside other groups, if other groups exists, updat RIGHT
	//1. same as EDIT from 1, but skip groups to be deleted!

	//INSERT: 
	//0. copy GAR records from parent if parent has some, if not, skip leave as is
	//1. same as EDIT from 1

	//PARENT_ID on GAR is not related to child->parent depedency
	//Rules: if parent is changed all childs are changed
	//Rules: if edited, find parent, take GAR from him 



}



//based on groups inside lstGroups, read all records for all tables from GAR, from records, read all other groups, get their contacts/users for all
//lstGroups<BGIT_ID>
void Service_AccessRights::RecalculateUARGroupContacts(Status &Ret_pStatus, DbRecordSet &lstGroups)
{
	DbSimpleOrm TableOrm_UAR(Ret_pStatus, CORE_ACC_USER_REC, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	DbSqlQuery *query=TableOrm_UAR.GetDbSqlQuery();

	if (lstGroups.getRowCount()==0)
		return;

	int nCount=0;
	QString strSQL,strWhereIn;

	//get all groups on records from target group...
	DbRecordSet lstAllGroups;
	lstAllGroups.addColumn(QVariant::Int,"CGAR_GROUP_ID");
	while (nCount>=0)
	{
		nCount=DbSqlTableDefinition::GenerateChunkedWhereStatement(lstGroups,"BGIT_ID",strWhereIn,nCount);
		if (nCount<0) break;
		QString strSQL=" SELECT DISTINCT A.CGAR_GROUP_ID FROM CORE_ACC_GROUP_REC A";
		strSQL+=" INNER JOIN CORE_ACC_GROUP_REC B ON A.CGAR_RECORD_ID=B.CGAR_RECORD_ID ";
		strSQL+=" WHERE B.CGAR_GROUP_ID IN "+strWhereIn;

		query->Execute(Ret_pStatus,strSQL);
		if(!Ret_pStatus.IsOK()) return;
		DbRecordSet lstTemp;
		query->FetchData(lstTemp);
		lstAllGroups.merge(lstTemp);
	}

	if (lstAllGroups.getRowCount()==0)
		return;


	//get max right for all users on all groups on all records/tables
	DbRecordSet lstGARUARRecords;
	lstGARUARRecords.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CORE_ACC_USER_REC));
	strWhereIn="";
	nCount=0;
	while (nCount>=0)
	{
		nCount=DbSqlTableDefinition::GenerateChunkedWhereStatement(lstAllGroups,"CGAR_GROUP_ID",strWhereIn,nCount);
		if (nCount<0) break;
		QString strSQL=" SELECT CGAR_TABLE_ID AS CUAR_TABLE_ID,CGAR_RECORD_ID AS CUAR_RECORD_ID,BPER_ID AS CUAR_PERSON_ID, MAX(CGAR_RIGHT) AS CUAR_RIGHT FROM CORE_ACC_GROUP_REC";
		strSQL+=" INNER JOIN BUS_CM_GROUP ON CGAR_GROUP_ID=BGCN_ITEM_ID ";
		strSQL+=" INNER JOIN BUS_PERSON ON BGCN_CONTACT_ID=BPER_CONTACT_ID ";
		strSQL+=" WHERE CGAR_GROUP_ID IN "+strWhereIn;
		strSQL+=" GROUP BY CGAR_TABLE_ID,CGAR_RECORD_ID,BPER_ID ";

		query->Execute(Ret_pStatus,strSQL);
		if(!Ret_pStatus.IsOK()) return;
		DbRecordSet lstTemp;
		query->FetchData(lstTemp);
		lstGARUARRecords.merge(lstTemp);
	}

	if (lstGARUARRecords.getRowCount()==0)
		return;

	lstGARUARRecords.setColValue("CUAR_CHILD_INHERIT",0); //not relevant as group data contain info
	lstGARUARRecords.setColValue("CUAR_CHILD_COMM_INHERIT",0); //not relevant as group data contain info
	lstGARUARRecords.setColValue("CUAR_GROUP_DEPENDENT",1);

	//B.T. issue 2608
	//select all records from UAR tables on table-record id with group_depedent flag =0 ->purge 'em from incoming list
	DbRecordSet lstIndividualRights;
	lstIndividualRights.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CORE_ACC_USER_REC));
	nCount=0;
	while (nCount>=0)
	{
		nCount=DbSqlTableDefinition::GenerateChunkedWhereStatement(lstAllGroups,"CGAR_GROUP_ID",strWhereIn,nCount);
		if (nCount<0) break;
		QString strSQL=" SELECT DISTINCT CUAR_TABLE_ID,CUAR_RECORD_ID,CUAR_PERSON_ID FROM CORE_ACC_USER_REC";
		strSQL+=" INNER JOIN CORE_ACC_GROUP_REC ON CUAR_TABLE_ID=CGAR_TABLE_ID AND CUAR_RECORD_ID=CGAR_RECORD_ID";
		strSQL+=" WHERE CUAR_GROUP_DEPENDENT=0 AND CGAR_GROUP_ID IN "+strWhereIn;

		query->Execute(Ret_pStatus,strSQL);
		if(!Ret_pStatus.IsOK()) return;
		DbRecordSet lstTemp;
		query->FetchData(lstTemp);
		lstIndividualRights.merge(lstTemp);
	}
	//lstIndividualRights.Dump();
	//lstGARUARRecords.Dump();

	int nSize=lstIndividualRights.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		int nSize2=lstGARUARRecords.getRowCount();
		for (int k=0;k<nSize2;k++)
		{
			if (lstIndividualRights.getDataRef(i,"CUAR_TABLE_ID")==lstGARUARRecords.getDataRef(k,"CUAR_TABLE_ID") && 
				lstIndividualRights.getDataRef(i,"CUAR_RECORD_ID")==lstGARUARRecords.getDataRef(k,"CUAR_RECORD_ID") &&
				lstIndividualRights.getDataRef(i,"CUAR_PERSON_ID")==lstGARUARRecords.getDataRef(k,"CUAR_PERSON_ID"))
			{
				lstGARUARRecords.deleteRow(k);
				break;
			}
		}
	}

	//lstGARUARRecords.Dump();


	//B.T. issue 2608
	//select all records from UAR tables on table-record id with group_depedent flag =1 ->those are old records..delete 'em
	DbRecordSet lstGroupRightsOld;
	lstGroupRightsOld.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CORE_ACC_USER_REC));
	nCount=0;
	while (nCount>=0)
	{
		nCount=DbSqlTableDefinition::GenerateChunkedWhereStatement(lstAllGroups,"CGAR_GROUP_ID",strWhereIn,nCount);
		if (nCount<0) break;
		QString strSQL=" SELECT DISTINCT CUAR_ID FROM CORE_ACC_USER_REC";
		strSQL+=" INNER JOIN CORE_ACC_GROUP_REC ON CUAR_TABLE_ID=CGAR_TABLE_ID AND CUAR_RECORD_ID=CGAR_RECORD_ID";
		strSQL+=" WHERE CUAR_GROUP_DEPENDENT=1 AND CGAR_GROUP_ID IN "+strWhereIn;

		query->Execute(Ret_pStatus,strSQL);
		if(!Ret_pStatus.IsOK()) return;
		DbRecordSet lstTemp;
		query->FetchData(lstTemp);
		lstGroupRightsOld.merge(lstTemp);
	}
	//lstGroupRightsOld.Dump();


	//Now, write all to DB:
	query->BeginTransaction(Ret_pStatus);
	if(!Ret_pStatus.IsOK()) return;

	//DELETE OLD UAR:
	strWhereIn="";
	nCount=0;
	while (nCount>=0)
	{
		nCount=DbSqlTableDefinition::GenerateChunkedWhereStatement(lstGroupRightsOld,"CUAR_ID",strWhereIn,nCount);
		if (nCount<0) break;
		QString strSQL=" DELETE FROM CORE_ACC_USER_REC WHERE CUAR_ID IN "+strWhereIn;
		query->Execute(Ret_pStatus,strSQL);
		if(!Ret_pStatus.IsOK()) 
		{
			query->Rollback();
			return;
		}
	}

	//WRITE NEW:
	if (lstGARUARRecords.getRowCount()>0)
	{
		TableOrm_UAR.Write(Ret_pStatus,lstGARUARRecords);
		if(!Ret_pStatus.IsOK()) 
		{
			query->Rollback();
			return;
		}
	}

	query->Commit(Ret_pStatus);
}


void Service_AccessRights::WriteOneGUAR(Status &Ret_pStatus, int nRecord, int nTableID,DbRecordSet &RetOut_UAR, DbRecordSet &RetOut_GAR,DbSimpleOrm &TableOrm_UAR, DbSimpleOrm &TableOrm_GAR)
{
	//test lists: if empty define:
	if (RetOut_UAR.getColumnCount()==0)
		RetOut_UAR.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CORE_ACC_USER_REC));
	if (RetOut_GAR.getColumnCount()==0)
		RetOut_GAR.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CORE_ACC_GROUP_REC));

	//read old UAR/GAR from DB, later used for testing inherit flags:
	DbRecordSet lstOldUAR,lstOldGAR;
	bool bOldSubProjectsInherit=false;
	bool bOldCommEntityInherit=false;
	if (nTableID==BUS_PROJECT)
	{
		ReadGUAR(Ret_pStatus,nRecord,nTableID,lstOldUAR,lstOldGAR);
		if(!Ret_pStatus.IsOK()) return;
		if(lstOldUAR.find("CUAR_CHILD_INHERIT",1,true)>=0)
			bOldSubProjectsInherit=true;
		if(lstOldUAR.find("CUAR_CHILD_COMM_INHERIT",1,true)>=0)
			bOldCommEntityInherit=true;
		if(lstOldGAR.find("CGAR_CHILD_INHERIT",1,true)>=0)
			bOldSubProjectsInherit=true;
		if(lstOldGAR.find("CGAR_CHILD_COMM_INHERIT",1,true)>=0)
			bOldCommEntityInherit=true;
	}

	DbSqlQuery *query=TableOrm_UAR.GetDbSqlQuery();
	query->BeginTransaction(Ret_pStatus);
	if(!Ret_pStatus.IsOK()) return;

	//prepare (just to be sure):
	RetOut_UAR.setColValue(RetOut_UAR.getColumnIdx("CUAR_RECORD_ID"),nRecord);
	RetOut_UAR.setColValue(RetOut_UAR.getColumnIdx("CUAR_TABLE_ID"),nTableID);
	RetOut_GAR.setColValue(RetOut_GAR.getColumnIdx("CGAR_RECORD_ID"),nRecord);
	RetOut_GAR.setColValue(RetOut_GAR.getColumnIdx("CGAR_TABLE_ID"),nTableID);

	//if only 1 record in UAR and if for all users->delete all!
	if (RetOut_UAR.getRowCount()==1 && RetOut_GAR.getRowCount()==0)
		if (RetOut_UAR.getDataRef(0,"CUAR_PERSON_ID").toInt()==0)
		{
			RetOut_UAR.clear();
		}

		//write UAR (delete orphan records, edit existing, insert new)
		TableOrm_UAR.WriteSubData(Ret_pStatus,CORE_ACC_USER_REC,RetOut_UAR,"CUAR_RECORD_ID",nRecord,true,0,"CUAR_TABLE_ID="+QVariant(nTableID).toString());
		if(!Ret_pStatus.IsOK()) 
		{
			query->Rollback();
			return;
		}
		//write GAR (delete orphan records, edit existing, insert new)
		TableOrm_GAR.WriteSubData(Ret_pStatus,CORE_ACC_GROUP_REC,RetOut_GAR,"CGAR_RECORD_ID",nRecord,true,0,"CGAR_TABLE_ID="+QVariant(nTableID).toString());
		if(!Ret_pStatus.IsOK()) 
		{
			query->Rollback();
			return;
		}

		//if table is project then test flags:
		if (nTableID==BUS_PROJECT)
		{
			//Test flag change: read old records, if flag from 1->0 then: reset flags on all children!!
			bool bSubProjectsInherit=false;
			bool bCommEntityInherit=false;

			if(RetOut_UAR.find("CUAR_CHILD_INHERIT",1,true)>=0)
				bSubProjectsInherit=true;
			if(RetOut_UAR.find("CUAR_CHILD_COMM_INHERIT",1,true)>=0)
				bCommEntityInherit=true;
			if(RetOut_GAR.find("CGAR_CHILD_INHERIT",1,true)>=0)
				bSubProjectsInherit=true;
			if(RetOut_GAR.find("CGAR_CHILD_COMM_INHERIT",1,true)>=0)
				bCommEntityInherit=true;

			//project childs:
			DbRecordSet lstChilds;
			lstChilds.addColumn(QVariant::Int,"BUSP_ID");

			if (bSubProjectsInherit)
			{
				//get childs:
				GetProjectChildren(Ret_pStatus,nRecord,lstChilds,query);
				if(!Ret_pStatus.IsOK()) 
				{
					query->Rollback();
					return;
				}

				//get all UAR/GAR records connected to those ID's -> delete'em all
				TableOrm_UAR.DeleteFromParentIDs(Ret_pStatus,"CUAR_RECORD_ID",lstChilds,"BUSP_ID",""," AND CUAR_TABLE_ID ="+QVariant(nTableID).toString());
				if(!Ret_pStatus.IsOK()) 
				{
					query->Rollback();
					return;
				}
				TableOrm_GAR.DeleteFromParentIDs(Ret_pStatus,"CGAR_RECORD_ID",lstChilds,"BUSP_ID",""," AND CGAR_TABLE_ID ="+QVariant(nTableID).toString());
				if(!Ret_pStatus.IsOK()) 
				{
					query->Rollback();
					return;
				}

				//copy UAR/GAR -> replace record ID, set parent id (not on group dep UAR's)
				DbRecordSet lstUAR,lstGAR;
				AccUARCore::CopyGUAR(nTableID,lstChilds,"BUSP_ID",RetOut_UAR,RetOut_GAR,lstUAR,lstGAR);

				//save UAR/GAR
				if (lstUAR.getRowCount()>0)
				{
					TableOrm_UAR.Write(Ret_pStatus,lstUAR);
					if(!Ret_pStatus.IsOK()) 
					{
						query->Rollback();
						return;
					}
				}
				if (lstGAR.getRowCount()>0)
				{
					TableOrm_GAR.Write(Ret_pStatus,lstGAR);
					if(!Ret_pStatus.IsOK()) 
					{
						query->Rollback();
						return;
					}
				}
			}
			else if (bOldSubProjectsInherit) //parent inherit was reset from 1->0, reset parent FK on all subprojects, leave UAR's as is
			{
				ResetParentOnProjectChildren(Ret_pStatus,RetOut_UAR,RetOut_GAR,BUS_PROJECT,query);
				if(!Ret_pStatus.IsOK()) 
				{
					query->Rollback();
					return;
				}
			}

			if (bCommEntityInherit) //propagate ar on all documents/voicecalls/comm entities of all sub projects including main project!!!!-> only subprojects if bSubProjectsInherit=true
			{
				//add nRecord as parent:
				lstChilds.addRow();
				lstChilds.setData(lstChilds.getRowCount()-1,0,nRecord);

				//VOICE CALLS:
				//get all comm cent id's from main project (nRecord) and all subprojects (lstChilds):
				DbRecordSet lstIds;
				GetProjectCommEntities(Ret_pStatus,BUS_VOICECALLS,lstChilds,lstIds,query);
				if(!Ret_pStatus.IsOK()) 
				{
					query->Rollback();
					return;
				}
				//copy UAR/GAR -> replace record ID, set parent id (not on group dep UAR's)
				DbRecordSet lstUAR,lstGAR;
				AccUARCore::CopyGUAR(BUS_VOICECALLS,lstIds,"BVC_ID",RetOut_UAR,RetOut_GAR,lstUAR,lstGAR);

				UpdateGUARRecords(Ret_pStatus,BUS_VOICECALLS,lstIds,lstUAR,lstGAR,&TableOrm_UAR,&TableOrm_GAR);
				if(!Ret_pStatus.IsOK()) 
				{
					query->Rollback();
					return;
				}

				//BUS_EMAIL:
				//get all comm cent id's from main project (nRecord) and all subprojects (lstChilds):
				lstIds.destroy();
				GetProjectCommEntities(Ret_pStatus,BUS_EMAIL,lstChilds,lstIds,query);
				if(!Ret_pStatus.IsOK()) 
				{
					query->Rollback();
					return;
				}
				AccUARCore::CopyGUAR(BUS_EMAIL,lstIds,"BEM_ID",RetOut_UAR,RetOut_GAR,lstUAR,lstGAR);

				UpdateGUARRecords(Ret_pStatus,BUS_EMAIL,lstIds,lstUAR,lstGAR,&TableOrm_UAR,&TableOrm_GAR);
				if(!Ret_pStatus.IsOK()) 
				{
					query->Rollback();
					return;
				}

				//BUS_DM_DOCUMENTS:
				lstIds.destroy();
				GetProjectCommEntities(Ret_pStatus,BUS_DM_DOCUMENTS,lstChilds,lstIds,query);
				if(!Ret_pStatus.IsOK()) 
				{
					query->Rollback();
					return;
				}
				UpdateProjectCommEntityGUAR(Ret_pStatus,BUS_DM_DOCUMENTS,lstIds); //update all documents as if this is normal op
				if(!Ret_pStatus.IsOK()) 
				{
					query->Rollback();
					return;
				}

			}
			else if (bOldCommEntityInherit) //parent inherit was reset from 1->0, reset parent FK on all comm entities that belong to the project, leave UAR's as is
			{
				ResetParentOnProjectChildren(Ret_pStatus,RetOut_UAR,RetOut_GAR,CE_COMM_ENTITY,query);
				if(!Ret_pStatus.IsOK()) 
				{
					query->Rollback();
					return;
				}
			}
		}

		query->Commit(Ret_pStatus);
}


