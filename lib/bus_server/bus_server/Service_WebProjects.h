#ifndef SERVICE_WEBPROJECTS_H
#define SERVICE_WEBPROJECTS_H

#include "bus_interface/bus_interface/interface_webprojects.h"
#include "businessservice.h"

class Service_WebProjects : public Interface_WebProjects, public BusinessService
{
public:
	void ReadProjectLists(Status &Ret_pStatus, DbRecordSet &Ret_Lists, int &Ret_nStartListID);
	void ReadProjectListData(Status &Ret_pStatus, int nListID,DbRecordSet &Ret_Data, QString strParentProjectCode="", int nLevelsDeep=-1);
	void ReadCommEntityCount(Status &Ret_pStatus, int nProjectID, int &Ret_nFileDocCount, int &Ret_nWebSiteCount, int &Ret_nNotesCount,int &Ret_nMailCount);

	//documents/emails:
	void ReadProjectDocuments(Status &Ret_pStatus, int &Ret_nTotalCount,DbRecordSet &Ret_Documents,  int nProjectID,int nDocType=-1, int nFromN=-1, int nToN=-1);
	void ReadProjectEmails(Status &Ret_pStatus, int &Ret_nTotalCount, DbRecordSet &Ret_Emails, int nProjectID, int nFromN=-1, int nToN=-1);


private:
	
};

#endif // SERVICE_WEBPROJECTS_H
