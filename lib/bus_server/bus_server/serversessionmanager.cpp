#include "db_core/db_core/dbsqltabledefinition.h"
#include "db_core/db_core/dbsqltableview.h"
#include "common/common/threadid.h"
#include "serversessionmanager.h"
#include "common/common/config.h"


#include "common/common/logger.h"
extern Logger				g_Logger;

#include "bus_core/bus_core/servercontrolabstract.h"
extern ServerControlAbstract *g_AppServer;
#define _APP_SERVER(funct)	if(g_AppServer)g_AppServer->funct;

#include "bus_core/bus_core/messagedispatcher.h"
extern MessageDispatcher* g_MessageDispatcher;


//constructore
ServerSessionManager::ServerSessionManager(SystemServiceSet* pSystemServices)
{
	m_SystemServices=pSystemServices;
	Q_ASSERT_X(m_SystemServices!=NULL,"ServerSessionManager","NULL POINTER");
	m_nAppSrvSessionID=0;
	connect(&m_updateTimer, SIGNAL(timeout()), this, SLOT(OnUpdateTime()));
	connect(this, SIGNAL(StartShutdownTimer(int)), this, SLOT(OnStartTimer(int)));
	m_nWarning=-1;
	m_nRestart=-1;
	m_nWarningDemoDb=-1;
}




/*!
	Starts app. server session, system services & db must be loaded first

	\param pStatus				if err occur, returned here
	\param nOldAppSessionID		last session ID stored in INI, if not exists or not crashed then it will be 0
	\param nLicenseID			license ID fetched from license file
	\param strIPAddress			IP:port address of App. server
	\param nRoleID				role of server
	\param nMaxConnections		max number of connections
	\param nIsLBOActive			Is LBO active
	\param nIsMaster			Is this master
	\param strSerial			serial number (from INI)
	\param strName				app. server name (from INI)
	
	\return						new App server session id (int)
*/

int ServerSessionManager::StartSession(Status &pStatus,QString strIPAddress,int nLicenseID,int nRoleID,int nMaxConnections,int nIsLBOActive, int nIsMaster,QString strUpdateTime, QString strWarningTime,QString strSerial,QString strName)
{

	DbRecordSet appsession;
	appsession.defineFromView(DbSqlTableView::getView(DbSqlTableDefinition::GetFullViewID(CORE_APP_SRV_SESSION)));

	m_strUpdateTime=strUpdateTime;
	m_strWarningUpdateTime=strWarningTime;

	//fill data:
	appsession.addRow();
	appsession.setData(0,"COAS_IP_ADDRESS",strIPAddress);
	appsession.setData(0,"COAS_DAT_STARTED",QDateTime::currentDateTime());
	appsession.setData(0,"COAS_DAT_LAST_ACTIVITY",QDateTime::currentDateTime());
	appsession.setData(0,"COAS_ROLE_ID",nRoleID);
	appsession.setData(0,"COAS_CURRENT_CONNECTIONS",0);
	appsession.setData(0,"COAS_MAX_CONNECTIONS",nMaxConnections);
	appsession.setData(0,"COAS_APP_SRV_SERIAL_ID",strSerial);
	appsession.setData(0,"COAS_APP_SRV_NAME",strName);
	appsession.setData(0,"COAS_UNIQUE_LICENSE_ID",nLicenseID);
	appsession.setData(0,"COAS_IS_LBO",nIsLBOActive);
	appsession.setData(0,"COAS_IS_MASTER",nIsMaster);
	
	
	//start session (automatic garbage clean)
	m_SystemServices->AppSrvSession->StartSession(pStatus,nRoleID,appsession);

	//return new ID:
	if(pStatus.IsOK())
	{
		m_nAppSrvSessionID=appsession.getDataRef(0,"COAS_ID").toInt();
		return m_nAppSrvSessionID;
	}
	else
		return 0;
}



/*!
	Stops app. server session

	\param pStatus				if err occur, returned here
*/

void ServerSessionManager::StopSession(Status &pStatus)
{
	//if no session started return:
	if(m_nAppSrvSessionID==0)return;
	
	//automatically cleans any garbage in DB:
	m_SystemServices->AppSrvSession->StopSession(pStatus,m_nAppSrvSessionID);
}

void ServerSessionManager::CleanAllSessions(Status &pStatus)
{
	m_SystemServices->AppSrvSession->CleanAllSessions(pStatus);
}

/*!
	Reports status, writes last activity in table
*/
void ServerSessionManager::ExecuteTask(int nCallerID,int nTaskID,QString strData)
{
	//LOG ACTIVITY:
	//------------------------------------------------
	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_SRV_SESSION_START,QVariant(ThreadIdentificator::GetCurrentThreadID()).toString());
	Status err;
	m_SystemServices->AppSrvSession->UpdateLastActivity(err,m_nAppSrvSessionID);
	if (!err.IsOK())
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_SRV_SESSION_FAILED,"Failed to update last activity time: "+err.getErrorText());
		return;
	}

	//DB REORGANIZATION SCHEDULE:
	//------------------------------------------------
	if (!m_strUpdateTime.isEmpty())
	{
		//read if current datetime<=date_update+time_update then set timer if interval < date_update+time_update-datetime
		QDate dateUpdate;
		m_SystemServices->AppSrvSession->GetUpdateDate(err,dateUpdate);
		if (!err.IsOK())
		{
			g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_SRV_SESSION_FAILED,"Failed to get update date: "+err.getErrorText());
			return;
		}

		if (dateUpdate.isValid())
		{
			QTime timeUpdate=QTime::fromString(m_strUpdateTime,"hh:mm");
			QDateTime dateTimeUpdate(dateUpdate,timeUpdate);
			QDateTime dateTimeNow=QDateTime::currentDateTime().addSecs(-30);


			bool bOK;
			int nWarningMin=m_strWarningUpdateTime.toInt(&bOK);
			QDateTime datWarning=dateTimeUpdate.addSecs(-nWarningMin*60);

			//qDebug()<<dateTimeUpdate;
			//qDebug()<<datWarning;
			//qDebug()<<dateTimeNow;

			if (datWarning.isValid() && QDateTime::currentDateTime()<datWarning)
				m_nWarning=g_MessageDispatcher->SendMessageX(StatusCodeSet::TYPE_WARNING,StatusCodeSet::ERR_SYSTEM_SERVER_UPDATE_AT,dateTimeUpdate.toString("dd.MM.yyyy hh:mm"),NULL,-1,m_nWarning,dateTimeUpdate,datWarning);

			if (dateTimeNow<dateTimeUpdate)
			{
				QDateTime current=QDateTime::currentDateTime();
				int nSecTo=current.secsTo(dateTimeUpdate);
				if (nSecTo<APP_SESSION_REPORT_INTERVAL && !m_updateTimer.isActive())
				{
					emit StartShutdownTimer(nSecTo);
					g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_SRV_SESSION_UPDATE_IS_SCHEDULED,QVariant(nSecTo).toString());
					m_nRestart=g_MessageDispatcher->SendMessageX(StatusCodeSet::TYPE_WARNING,StatusCodeSet::ERR_SYSTEM_SERVER_RESETART_IN,QVariant(nSecTo/60).toString(),NULL,-1,m_nRestart,dateTimeUpdate);
				}
			}
		}
	}


	//TEST FRO DEMO DATABASE
	//------------------------------------------------

	bool bFireWarning=false;
	int nCurr,nMax;
	_APP_SERVER(TestDemoDatabaseSize(err,bFireWarning,nCurr,nMax));
	if (bFireWarning)
	{
		QDateTime datExpire=QDateTime::currentDateTime().addSecs(30*60); //????
		m_nWarningDemoDb=g_MessageDispatcher->SendMessageX(StatusCodeSet::TYPE_WARNING,StatusCodeSet::ERR_SYSTEM_DEMO_DATABASE_WARNING,QVariant(nCurr/1024).toString()+";"+QVariant(nMax/1024).toString(),NULL,-1,m_nWarningDemoDb,datExpire);
	}
}



/*!
	Load kick ban list based on list name:
	\param pStatus			if err occur, returned here
	\param strListName		list name
	\param lstAccess		kick IP list 

*/
void ServerSessionManager::LoadKickBanList(Status &pStatus,QString strListName, DbRecordSet &lstAccess)
{
	lstAccess.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CORE_IPACCESSLIST));
	if(strListName.isEmpty()) return;
	//load lists:
	m_SystemServices->IPAccessList->Read(pStatus,strListName,lstAccess);
}




/*!
	Save kick ban list based on list name:
	\param pStatus			if err occur, returned here
	\param strListName		list name, if emtpy, one will be generated as APP_SRV+Id+DYN and returned
	\param lstAccess		kick IP list 

*/
void ServerSessionManager::SaveKickBanList(Status &pStatus,QString &strListName, DbRecordSet &lstDynamicAccess)
{

	QString strDynamicListName;
	if(strListName.isEmpty())
		strListName="APPSRV_"+QVariant(m_nAppSrvSessionID).toString()+"_DYN"; //app_srv_id+_DYN

	lstDynamicAccess.setColValue(lstDynamicAccess.getColumnIdx("CIPR_LIST_NAME"),strListName);

	m_SystemServices->IPAccessList->Write(pStatus,strListName,lstDynamicAccess);

}


void ServerSessionManager::OnUpdateTime()
{
	m_updateTimer.stop();
	
	QString strApp="\""+QCoreApplication::applicationDirPath()+"/update/ApplicationServerUpdate.exe\"";
	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_SRV_SESSION_UPDATE_START,strApp);

	QStringList lstArgs;
	lstArgs<<"-update";
	if(!QProcess::startDetached(strApp,lstArgs))
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_SYSTEM_SRV_SESSION_UPDATE_FAILED);
	}
	else
	{
		_APP_SERVER(StopDelayed());
	}
}

void ServerSessionManager::OnStartTimer(int nSecTo)
{

	if (!m_updateTimer.isActive())
		m_updateTimer.start(nSecTo*1000);

}