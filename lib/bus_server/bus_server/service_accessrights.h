#ifndef SERVICE_ACCESSRIGHTS_H
#define SERVICE_ACCESSRIGHTS_H

#include "bus_interface/bus_interface/interface_accessrights.h"
#include "businessservice.h"
#include "hierarchicaldata.h"
#include "db/db/dbsqlquery.h"
#include "db/db/dbsimpleorm.h"

/*!
\class Service_AccessRights
\brief Class for Access Rights manipulation.
\ingroup Bus_System

Provides methods for manipulating access rights.

*/

class Service_AccessRights : public Interface_AccessRights, public BusinessService
{
public:

	Service_AccessRights();
	~Service_AccessRights(){};

	//------------------------------------
	//			Person section
	//------------------------------------
	//Get Persons Role's list.
	void GetRoleList(Status &pStatus, DbRecordSet &pPersonRoleListRecordSet);
	void GetPersons(Status &Ret_pStatus, DbRecordSet &Ret_pPersonsRecordSet);
	void GetPersonRoleList(Status &Ret_pStatus, DbRecordSet &Ret_pPersonRoleListRecordSet, int PersonID);
	void InsertNewRoleToPerson(Status &Ret_pStatus, int PersonID, int RoleID);
	void DeleteRoleFromPerson(Status &Ret_pStatus, int PersonID, int RoleID);

	//------------------------------------
	//			User section
	//------------------------------------
	void GetRoleListForUsers(Status &Ret_pStatus, DbRecordSet &Ret_pUserRoleListRecordSet);
	void GetUsers(Status &Ret_pStatus, DbRecordSet &Ret_pUsersRecordSet);
	void GetUserRoleList(Status &Ret_pStatus, DbRecordSet &Ret_pUserRoleListRecordSet, int UserID);
	void InsertNewRoleToUser(Status &Ret_pStatus, int UserID, int RoleID);
	void DeleteRoleFromUser(Status &Ret_pStatus, int UserID, int RoleID);
	void GetUserAccRightsRecordSet(Status &Ret_pStatus, DbRecordSet &Ret_pUserAccRRecordSet, int UserID);

	//------------------------------------
	//			Role definition section
	//------------------------------------
	void GetRoles(Status &Ret_pStatus, DbRecordSet &Ret_pRoleListRecordSet);
	void GetBERoles(Status &Ret_pStatus, DbRecordSet &Ret_pRoleListRecordSet);
	void GetTERoles(Status &Ret_pStatus, DbRecordSet &Ret_pRoleListRecordSet);
	void GetRoleAccessRightSets(Status &Ret_pStatus, DbRecordSet &Ret_pAccessRightsListRecordSet, int RoleID);
	void GetAccessRightsSets(Status &Ret_pStatus, DbRecordSet &Ret_pAccessRightsSetListRecordSet);
	void DeleteRole(Status &Ret_pStatus, int RoleID);
	void DeleteAccRSetFromRole(Status &Ret_pStatus, int RoleID, int AccRSetID);
	void InsertNewAccRSetToRole(Status &Ret_pStatus, int RoleID, int AccRSetID);
	void InsertNewRole(Status &Ret_pStatus, int &Ret_nNewRoleRowID, QString RoleName, QString RoleDescription, int RoleType);
	void RenameRole(Status &Ret_pStatus, int RoleID, QString NewRoleName, QString NewRoleDescription);
	void GetAccessRights(Status &Ret_pStatus, DbRecordSet &Ret_pAccessRightsRecordSet, int RoleID, int AccRSetID);
	void SetAccessRightValue(Status &Ret_pStatus, int AccRightID, QString Value);
	void GetDefaultRoles(Status &Ret_pStatus, int RoleType, DbRecordSet &Ret_pDefaultRoleRecordSet);
	void ChangeAccessRights(Status &Ret_pStatus, DbRecordSet &ChangedRightsRecordSet);

	//------------------------------------
	//			UAR/GAR system
	//------------------------------------
	void ReadGUAR(Status &Ret_pStatus, int nRecord, int nTableID,DbRecordSet &Ret_UAR, DbRecordSet &Ret_GAR);
	void WriteGUAR(Status &Ret_pStatus, int nRecord, int nTableID,DbRecordSet &RetOut_UAR, DbRecordSet &RetOut_GAR);
	void PrepareGUAR(Status &Ret_pStatus, int nRecord, int nTableID,DbRecordSet &RetOut_UAR, DbRecordSet &RetOut_GAR, DbRecordSet &Ret_lstGroupUsers,int &Ret_ParentRecord,int &Ret_ParentTableID,QString &Ret_ParentName);
	void TestUAR(Status &Ret_pStatus, int nTableID, int nPersonID, int nAccessLevel, DbRecordSet &lstTest_IDs, DbRecordSet &Ret_lstResult_IDs, bool bReturnAllowed=true);
	void UpdateProjectGUAR(Status &Ret_pStatus, DbRecordSet &lstProject_IDs);
	void UpdateGroupContactsGUAR(Status &Ret_pStatus, DbRecordSet &lstData);
	void UpdateProjectCommEntityGUAR(Status &Ret_pStatus, int nTableID, DbRecordSet &lstData);

private:
	void InsertAccessRights(Status &pStatus, int RoleID, int ARSID);
	void WriteOneGUAR(Status &Ret_pStatus, int nRecord, int nTableID,DbRecordSet &RetOut_UAR, DbRecordSet &RetOut_GAR,DbSimpleOrm &TableOrm_UAR, DbSimpleOrm &TableOrm_GAR);
	void GetParentGUARRecord(Status &Ret_pStatus, DbRecordSet &RetOut_UAR, DbRecordSet &RetOut_GAR, int &Ret_ParentRecord,int &Ret_ParentTableID,QString &Ret_ParentName);
	void GetParentGUARProjects(Status &Ret_pStatus,int nTableID,const DbRecordSet &lstData,DbRecordSet &lstProject_Ids,DbSqlQuery *query);
	void GetProjectChildren(Status &Ret_pStatus,int nProjectID,DbRecordSet &lstProject_Ids,DbSqlQuery *query);
	void GetProjectCommEntities(Status &Ret_pStatus,int nTableID,const DbRecordSet &lstProject_Ids,DbRecordSet &lstIds,DbSqlQuery *query);
	void GetProjectDocuments(Status &Ret_pStatus,const DbRecordSet &lstIds,bool bDocIds,DbRecordSet &lstResult,DbSqlQuery *query);
	void GetCommEntityProjects(Status &Ret_pStatus,int nTableID,DbRecordSet &lstData,DbRecordSet &lstProject_Ids,DbSqlQuery *query);
	void UpdateGUARRecords(Status &Ret_pStatus,int nTableID,const DbRecordSet &lstIds,DbRecordSet &lstUAR, DbRecordSet &lstGAR, DbSimpleOrm *ormUAR,DbSimpleOrm *ormGAR);
	void UpdateGUARAfterProjectAssignment(Status &Ret_pStatus, int nTableID,QString strParentColName,DbRecordSet &lstAssignProject_Ids, DbRecordSet &lstUnAssignProject_Ids,DbSimpleOrm *ormUAR,DbSimpleOrm *ormGAR);
	void ResetParentOnProjectChildren(Status &Ret_pStatus,const DbRecordSet &RetOut_UAR, const DbRecordSet &RetOut_GAR,int nChildrenTableID,DbSqlQuery *query);
	void ResetParentOnGUARRecords(Status &Ret_pStatus,int nTableID,const DbRecordSet &lstIds,QString strPrimaryKeyCol,DbSqlQuery *query);
	void RecalculateUARGroupContacts(Status &Ret_pStatus, DbRecordSet &lstGroups);


    HierarchicalData m_ProjectData;
};

#endif // SERVICE_ACCESSRIGHTS_H
