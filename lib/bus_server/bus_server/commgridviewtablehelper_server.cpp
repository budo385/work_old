#include "commgridviewtablehelper_server.h"

#include "bus_core/bus_core/usersessionmanager.h"
extern UserSessionManager	*g_UserSessionManager;

#include "commgridviewhelper_server.h"

CommGridViewTableHelper_Server::CommGridViewTableHelper_Server(QObject *parent/*=0*/)
	: CommGridViewTableHelper(parent)
{
	InitializeHelper();
}

CommGridViewTableHelper_Server::~CommGridViewTableHelper_Server()
{

}

int CommGridViewTableHelper_Server::GetLoggedPersonID()
{
	return g_UserSessionManager->GetPersonID();
}

QString CommGridViewTableHelper_Server::GetLoggedPersonInitials()
{
	return QString();
}

void CommGridViewTableHelper_Server::InitializeHelper()
{
	m_pCmGrdHelper=new CommGridViewHelper_Server;

}