#ifndef SERVICE_WEBEMAILS_H
#define SERVICE_WEBEMAILS_H


#include "bus_interface/bus_interface/interface_webemails.h"
#include "businessservice.h"

class Service_WebEmails : public Interface_WebEmails, public BusinessService
{
public:
	void ReadEmails(Status &Ret_pStatus, int &Ret_nTotalCount, DbRecordSet &Ret_Emails, int nFromN=-1, int nToN=-1);
	void SearchEmails(Status &Ret_pStatus, int &Ret_nTotalCount,DbRecordSet &Ret_Emails,  int nSortOrder=-1, int nFromN=-1, int nToN=-1, int nContactID=-1, int nProjectID=-1,  int nOwnerUserID=-1, QDate datFrom=QDate(), QDate datTo=QDate(), QString strEmailAcc="", QString strEmailSender="", int nMailBox=0);
	void ReadEmailDetails(Status &Ret_pStatus, int nEmailID, DbRecordSet &Ret_Email, DbRecordSet &Ret_Attachments);
	void GetAttachmentURL(Status &Ret_pStatus, int nAttachmentID, QString &Ret_strDocumentURL);
	void GetAttachmentUnzippedFiles(Status &Ret_pStatus, int nAttachmentID, DbRecordSet &Ret_AttachmentFiles);
	void ReadEmailAsPdf(Status &Ret_pStatus, int nEmailID,QString &Ret_strDocumentURL);
	void ReadEmailAsHtml(Status &Ret_pStatus, int nEmailID,QString &Ret_strDocumentURL);
	void ReadEmailAsPdfBinary(Status &Ret_pStatus, int nEmailID,QByteArray &Ret_byteData);
	void ReadEmailAsHtmlBinary(Status &Ret_pStatus, int nEmailID,QByteArray &Ret_byteData);
	void GetAttachmentBinary(Status &Ret_pStatus, int nAttachmentID, QByteArray &Ret_byteData);

private:
	void GetEmailAttachmentCount(Status &Ret_pStatus, DbRecordSet &lstEmails);
	void GetEmailAttachments(Status &Ret_pStatus, int nEmailID,DbRecordSet &Ret_Attachments);
	void GetFromToContacts(Status &Ret_pStatus, DbRecordSet &lstEmails);
	void SaveAttachment(Status &Ret_pStatus, int nAttachmentID, DbRecordSet &Ret_Attachments, bool bUnzip);
	void UnpackCIDToTemp(DbRecordSet &rowEmail,DbRecordSet &lstAttachments,QString strTempPath);
	void EmbedHtmlPix(QString &strHtmlData, QString strAttachmentDir);
	void EmbedCidAsURI(DbRecordSet &rowEmail,DbRecordSet &lstAttachments,QString strTempPath, QString &strBody, DbRecordSet &lstCIDInfo, QString strTagStart, QString strSettingStart, bool &bChanged);
	void LoadPersonEmailAccData(int nPersonID, DbRecordSet &lstPersonAccData);
	void ResetLastRefreshDate(int nPersonID);
};


#endif // SERVICE_WEBEMAILS_H
