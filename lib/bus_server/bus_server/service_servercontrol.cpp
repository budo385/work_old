#include "service_servercontrol.h"

#include "businessserviceset.h"
extern BusinessServiceSet* g_BusinessServiceSet;
#include "common/common/logger.h"
extern Logger				g_Logger;
#include "bus_core/bus_core/servercontrolabstract.h"
extern ServerControlAbstract *g_AppServer;
#include "bus_core/bus_core/backupmanager.h"
extern BackupManager *g_pBackupManager;
#include "bus_core/bus_core/importexportmanagerabstract.h"
extern ImportExportManagerAbstract *g_pImportExportManager;
#include "bus_core/bus_core/serverremindermanagerabstract.h"
extern ServerReminderManagerAbstract *g_pReminderManager;


#define _APP_SERVER(funct)	if(g_AppServer){g_AppServer->funct;}else{Ret_pStatus.setError(StatusCodeSet::ERR_SYSTEM_APP_SERVER_NOT_EXISTS); return;}
#define _BCP_MANAGER(funct)	if(g_pBackupManager){g_pBackupManager->funct;}else{Ret_pStatus.setError(StatusCodeSet::ERR_SYSTEM_BACKUP_MANAGER_NOT_EXISTS); return;}
#define _IMP_MANAGER(funct)	if(g_pImportExportManager){g_pImportExportManager->funct;}else{Ret_pStatus.setError(StatusCodeSet::ERR_SYSTEM_IMP_EXP_NOT_EXISTS); return;}
#define _REM_MANAGER(funct)	if(g_pReminderManager){g_pReminderManager->funct;}else{Ret_pStatus.setError(StatusCodeSet::ERR_SYSTEM_REMINDER_MANAGER_NOT_EXISTS); return;}



void Service_ServerControl::GetBackupData(Status &Ret_pStatus,int& Ret_nBackupFreq,QString& Ret_strBackupPath,QString& Ret_datBackupLastDate,QString& Ret_strRestoreOnNextStart,int& Ret_nBackupDay,QString& Ret_strBackupTime,int& Ret_nDeleteOldBackupsDay)
{
	_BCP_MANAGER(GetBackupData(Ret_pStatus,Ret_nBackupFreq,Ret_strBackupPath,Ret_datBackupLastDate,Ret_strRestoreOnNextStart,Ret_nBackupDay,Ret_strBackupTime,Ret_nDeleteOldBackupsDay))
}
void Service_ServerControl::SetBackupData(Status &Ret_pStatus,int nBackupFreq, QString strBackupPath, int nBackupDay,QString strBackupTime,int nDeleteOldBackupsDay)
{
	_BCP_MANAGER(SetBackupData(Ret_pStatus,nBackupFreq,strBackupPath,nBackupDay,strBackupTime,nDeleteOldBackupsDay))

	_APP_SERVER(SaveSettings());

}
void Service_ServerControl::LoadBackupList(Status &Ret_pStatus,DbRecordSet &RetOut_Data)
{
	_BCP_MANAGER(LoadBackupList(Ret_pStatus,RetOut_Data))

}
void Service_ServerControl::Backup(Status &Ret_pStatus)
{
	_BCP_MANAGER(Backup(Ret_pStatus)) //delayed backup:
}
void Service_ServerControl::Restore(Status &Ret_pStatus,QString strRestoreFile)
{
	_BCP_MANAGER(Restore(Ret_pStatus,strRestoreFile))
}

void Service_ServerControl::LoadTemplateList(Status &Ret_pStatus,DbRecordSet &RetOut_Data)
{
	_BCP_MANAGER(LoadTemplateList(Ret_pStatus,RetOut_Data)) //fix in future: backup start, then leave from server...

}
void Service_ServerControl::RestoreFromTemplate(Status &Ret_pStatus,QString strRestoreFile)
{
	_BCP_MANAGER(RestoreFromTemplate(Ret_pStatus,strRestoreFile)) //fix in future: backup start, then leave from server...
}

void Service_ServerControl::SetTemplateData(Status &Ret_pStatus,QString strTemplateInUse,int nAskOnStartup, int nStartStartUpWizard)
{

	DbSqlQuery query(Ret_pStatus,GetDbManager());
	if(!Ret_pStatus.IsOK()) return;

	//reset template flag=0, do not ask for template
	QString strSQL = "UPDATE CORE_DATABASE_INFO SET CDI_ASK_FOR_TEMPLATE_DB=?, CDI_TEMPLATE_DB_USED=?, CDI_START_SETUP_WIZARD=?";
	query.Prepare(Ret_pStatus, strSQL);
	if(!Ret_pStatus.IsOK()) return;
	query.bindValue(0,nAskOnStartup);
	query.bindValue(1,strTemplateInUse);
	query.bindValue(2,nStartStartUpWizard);
	query.ExecutePrepared(Ret_pStatus);

}
void Service_ServerControl::GetTemplateData(Status &Ret_pStatus,QString &Ret_strTemplateInUse,int &Ret_nAskOnStartup,int &Ret_nStartStartUpWizard)
{

	DbSqlQuery query(Ret_pStatus,GetDbManager());
	if(!Ret_pStatus.IsOK()) return;

	//reset template flag=0, do not ask for template
	QString strSQL = "SELECT CDI_TEMPLATE_DB_USED,CDI_ASK_FOR_TEMPLATE_DB, CDI_START_SETUP_WIZARD FROM CORE_DATABASE_INFO";
	query.Execute(Ret_pStatus, strSQL);
	if(!Ret_pStatus.IsOK()) return;

	if (query.next())
	{
		Ret_strTemplateInUse=query.value(0).toString();
		Ret_nAskOnStartup=query.value(1).toInt();
		Ret_nStartStartUpWizard=query.value(2).toInt();
	}
	else
		Ret_pStatus.setError(1,QObject::tr("Template Data could not be fetched!"));


}
void Service_ServerControl::LoadBackupFile(Status &Ret_pStatus, QString strBackupName,QByteArray &Ret_bcpContent)
{
	_BCP_MANAGER(LoadBackupFile(Ret_pStatus,strBackupName,Ret_bcpContent));
}
void Service_ServerControl::SaveBackupFile(Status &Ret_pStatus, QString strBackupName,QByteArray &Destroy_bcpContent)
{
	_BCP_MANAGER(SaveBackupFile(Ret_pStatus,strBackupName,Destroy_bcpContent));
}
void Service_ServerControl::DeleteBackupFile(Status &Ret_pStatus, QString strBackupName)
{
	_BCP_MANAGER(DeleteBackupFile(Ret_pStatus,strBackupName));
}

void Service_ServerControl::SaveImportExportSettings(Status &Ret_pStatus, DbRecordSet lstSettings)
{
	_IMP_MANAGER(Client_SaveSettings(Ret_pStatus,lstSettings));
}
void Service_ServerControl::LoadImportExportSettings(Status &Ret_pStatus, DbRecordSet &Ret_lstSettings)
{
	g_BusinessServiceSet->ClientSimpleORM->Read(Ret_pStatus,CORE_IMPORT_EXPORT,Ret_lstSettings);
}
void Service_ServerControl::StartProcessProject(Status &Ret_pStatus)
{
	_IMP_MANAGER(Client_StartTaskDelayed(Ret_pStatus,ImportExportManagerAbstract::PROCESS_TYPE_PROJECT));
}
void Service_ServerControl::StartProcessUser(Status &Ret_pStatus)
{
	_IMP_MANAGER(Client_StartTaskDelayed(Ret_pStatus,ImportExportManagerAbstract::PROCESS_TYPE_USER));
}
void Service_ServerControl::StartProcessDebtor(Status &Ret_pStatus)
{
	_IMP_MANAGER(Client_StartTaskDelayed(Ret_pStatus,ImportExportManagerAbstract::PROCESS_TYPE_DEBTOR));
}
void Service_ServerControl::StartProcessContact(Status &Ret_pStatus)
{
	_IMP_MANAGER(Client_StartTaskDelayed(Ret_pStatus,ImportExportManagerAbstract::PROCESS_TYPE_CONTACT));
}
void Service_ServerControl::StartProcessStoredProjectList(Status &Ret_pStatus)
{
	_IMP_MANAGER(Client_StartTaskDelayed(Ret_pStatus,ImportExportManagerAbstract::PROCESS_TYPE_SPL));
}


//read by chunks from file from current position backwards: for start read from end:RetOut_nCurrentPosition=-1, RetOut_nLogFileSizeOnStartOfChunkRead=-1, else keep values.
void Service_ServerControl::GetLog(Status &Ret_pStatus,QByteArray &Ret_logData,int &RetOut_nCurrentPosition,int &RetOut_nLogFileSizeOnStartOfChunkRead,int nChunkSize)
{
	Ret_pStatus.setError(0);
	//we set limit to 1000Mb log file size...so qint64 is not really needed...hmm?
	qint64 fileSize=RetOut_nLogFileSizeOnStartOfChunkRead;
	qint64 pos=RetOut_nCurrentPosition;
	bool bOK=g_Logger.GetLog(Ret_logData,pos,fileSize,nChunkSize);
	RetOut_nLogFileSizeOnStartOfChunkRead=fileSize;
	RetOut_nCurrentPosition=pos;

	if (!bOK)
	{
		Ret_pStatus.setError(1,"Log can not be read!");
	}
	
}

void Service_ServerControl::GetLogData(Status &Ret_pStatus, int &Ret_nLogLevel, int &Ret_nLogMaxSize, int &Ret_nBufferSize, int &Ret_nCurrentSize)
{
	Ret_nLogLevel=g_Logger.GetLogLevel();
	Ret_nLogMaxSize=g_Logger.GetMaxLogSize();
	Ret_nBufferSize=g_Logger.GetMemoryBufferSize();
	Ret_nCurrentSize=g_Logger.GetCurrentLogSize();
}
void Service_ServerControl::SetLogData(Status &Ret_pStatus, int nLogLevel, int nLogMaxSize, int nBufferSize)
{
	g_Logger.SetLogLevel(nLogLevel);
	g_Logger.SetMaxLogSize(nLogMaxSize);
	g_Logger.SetMemoryBufferSize(nBufferSize);
	_APP_SERVER(SaveSettings());
}

void Service_ServerControl::GetAllActiveRemindersForUser(Status &Ret_pStatus,int nPersonID,DbRecordSet &Ret_Reminders)
{
	_REM_MANAGER(GetAllActiveRemindersForUser(Ret_pStatus,nPersonID,Ret_Reminders))
}
void Service_ServerControl::DeleteReminders(Status &Ret_pStatus,DbRecordSet &lstReminders)
{
	_REM_MANAGER(DeleteReminders(Ret_pStatus,lstReminders))
}
void Service_ServerControl::PostPoneReminders(Status &Ret_pStatus,DbRecordSet &lstReminders, int nMinutes)
{
	_REM_MANAGER(PostPoneReminders(Ret_pStatus,lstReminders,nMinutes))
}
