#ifndef SERVICE_BUSEMAIL_H
#define SERVICE_BUSEMAIL_H

#include "bus_interface/bus_interface/interface_busemail.h"
#include "businessservice.h"

/*!
    \class Service_BusEmail
    \brief Service class to store data on email messages
    \ingroup Bus_Interface_Collection
		
*/

class Service_BusEmail : public Interface_BusEmail, public BusinessService
{
public:
	void Read(Status &Ret_pStatus, int nEmailID, DbRecordSet &RetOut_Data, DbRecordSet &Ret_ContactLink,DbRecordSet &Ret_ScheduleTask,DbRecordSet &Ret_ContactEmails, DbRecordSet &Ret_Attachments,DbRecordSet &Ret_UAR,DbRecordSet &Ret_GAR );
	void Write(Status &Ret_pStatus, DbRecordSet &RetOut_Data, DbRecordSet &RetOut_ContactLink, DbRecordSet &RetOut_ScheduleTask, DbRecordSet &RetOut_Attachments,QString strLockRes,DbRecordSet &RetOut_UAR,DbRecordSet &RetOut_GAR);
	void UpdateEmailFromClient(Status &Ret_pStatus, DbRecordSet &Emails, DbRecordSet &Attachments);
	void SetEmailRead(Status &Ret_pStatus, int nCE_ID);
	void WriteMultiple(Status &Ret_pStatus, DbRecordSet &RetOut_Data, DbRecordSet &RetOut_ContactLink, DbRecordSet &RetOut_ScheduleTask, DbRecordSet &RetOut_Attachments,QString strLockRes, bool bSkipExisting, int &RetOut_nSkippedOrReplacedCnt, bool bAddDupsAsNewRow);
	void WriteEmailLog(Status &Ret_pStatus, DbRecordSet &RetOut_Data, QString strLockRes);
	void List(Status &Ret_pStatus, DbRecordSet &RetOut_lstData, QDate &datDateFrom, QDate &datDateTo, int nOwnerID, int nFilterID);
	void Delete(Status &Ret_pStatus, DbRecordSet &RetOut_lstIDs);
	void WriteAttachments(Status &Ret_pStatus,int nEmailID, DbRecordSet &RetOut_Attachments);
	void WriteEmailAttachmentsFromUserStorage(Status &Ret_pStatus,int nEmailID, DbRecordSet &Attachments);

protected:
	//int FindEmail(QByteArray &arData);
	int FindEmailByUID(DbRecordSet &rowEmail, int nEmailOwnerID);
	QString CalcCheckSum(DbRecordSet &rowEmail, int nEmailOwnerID);
	void LoadAllUserEmailCheckSumUID(Status &pStatus, DbRecordSet &lstChekSumIDs, int nEmailOwnerID);
};


#endif //SERVICE_BUSEMAIL_H
