#include "service_busaddressschemas.h"
#include "bus_server/bus_server/businesslocker.h"
#include "db/db/dbsimpleorm.h"
#include "db_core/db_core/dbsqltableview.h"



void Service_BusAddressSchemas::Read(Status &Ret_pStatus, DbRecordSet &Ret_Data)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_CM_ADDRESS_SCHEMAS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	//load all
	TableOrm.Read(Ret_pStatus,Ret_Data);

}

void Service_BusAddressSchemas::Write(Status &Ret_pStatus, DbRecordSet &RetOut_Data,QString strLockRes)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_CM_ADDRESS_SCHEMAS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	//load all
	TableOrm.Write(Ret_pStatus,RetOut_Data);

	//if write ok & locked, unlock:
	if(Ret_pStatus.IsOK() && !strLockRes.isEmpty())
	{
		BusinessLocker locker(Ret_pStatus,BUS_CM_ADDRESS_SCHEMAS,GetDbManager(), TableOrm.GetDbConnection());
		locker.UnLock(Ret_pStatus,strLockRes);
	}

}

void Service_BusAddressSchemas::Delete(Status &Ret_pStatus, int nRecordID)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_CM_ADDRESS_SCHEMAS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	//delete:
	QList<int> lstDel;
	lstDel<<nRecordID;
	TableOrm.DeleteFast(Ret_pStatus,lstDel);
}


void Service_BusAddressSchemas::Lock(Status &Ret_pStatus, int nRecordID,QString &Ret_strLockRes)
{
	//Make Locker Obj & lock:
	BusinessLocker locker(Ret_pStatus,BUS_CM_ADDRESS_SCHEMAS,GetDbManager());
	QList<int> lstLock;
	lstLock<<nRecordID;
	locker.Lock(Ret_pStatus,lstLock,Ret_strLockRes);
}


void Service_BusAddressSchemas::Unlock(Status &Ret_pStatus, QString strLockRes)
{
	BusinessLocker locker(Ret_pStatus,BUS_CM_ADDRESS_SCHEMAS,GetDbManager());
	locker.UnLock(Ret_pStatus,strLockRes);
}