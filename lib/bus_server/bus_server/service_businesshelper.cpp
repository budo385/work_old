#include "service_businesshelper.h"
#include "common/common/status.h"
#include "db/db/dbsimpleorm.h"
#include "bus_core/bus_core/globalconstants.h"
#include "bus_core/bus_core/optionsandsettingsid.h"
#include "bus_core/bus_core/optionsandsettingsmanager.h"

#include "bus_core/bus_core/usersessionmanager.h"
extern UserSessionManager	*g_UserSessionManager;
#include "bus_core/bus_core/messagedispatcher.h"
extern MessageDispatcher* g_MessageDispatcher;
#include "bus_server/bus_server/businessserviceset.h"
extern BusinessServiceSet* g_BusinessServiceSet;

#define AVATAR_PROJECT				0
#define AVATAR_CONTACT				1
#define AVATAR_TASK_EMAIL			2
#define AVATAR_TASK_CALL			3
#define AVATAR_TASK_ADDRESS			4
#define AVATAR_TASK_INTERNET_FILE	5
#define AVATAR_TASK_LOCAL_FILE		6
#define AVATAR_TASK_NOTE			7
#define AVATAR_TASK_PAPER_DOCS		8

/*!
	Writes 1:N data from list inside parent list (e.g. LST_PHONES from listoFContacts)
	No transaction, all statuses are  returned in lstRet_SubStatus.
	Id form parent is assigned to the subdata record.
	Parent with bad status are skipped.
	Results are stored back to parent in sublist.
	If there are errors, parent status list is updated.

	Note: lstParentData must have ID on first col
	Note: lstParentStatus is defined as <id,status_code,status_text>
	Note: lstRet_SubData must be defined as subdata before entering this function


	\param nSubData_TableID			- returns error (RETURNED)
	\param nSubDataCol_Idx			- filter parameters stored in the DbRecordSet
	\param nSubListFKtoParentIdx	- id of parent FK inside subdata list
	\param nSubDataSkipColWrite		- 0, number of cols to skip in write from end of subdata list
	\param lstParentData			- parent records, contain sub data list in column nSubDataCol_Idx
	\param lstParentStatus			- list of statuses of parent write(), row count same as lstParentData, has <id,status_code,status_text>, only rows with status_code==0 are processed, if error on child, status will be set
	\param lstRet_SubData			- returned list of all subdata, with assigned ID's, must be defined!!
	\param lstRet_SubStatus			- returned list of statuses of lstRet_SubData write(), row count same as lstRet_SubData, has <id,status_code,status_text>, only rows with status_code==0 are processed
*/
void Service_BusinessHelper::WriteSubList(int nSubData_TableID, int nSubDataCol_Idx, int nSubListFKtoParentIdx,int nSubDataSkipColWrite,DbRecordSet &lstParentData, DbRecordSet& lstParentStatus,DbRecordSet& lstRet_SubData,DbRecordSet& lstRet_SubStatus)
{

	lstRet_SubData.clear();
	lstRet_SubStatus.clear();


	Q_ASSERT(lstParentStatus.getRowCount()==lstParentData.getRowCount());	//must be same
	Q_ASSERT(lstParentData.getColumnCount()!=0);							//must be defined

	int nSize=lstParentData.getRowCount();
	for (int i=0;i<nSize;++i)
	{
		if (lstParentStatus.getDataRef(i,1).toInt()!=0) continue; //skip if error (status_code!=0)

		DbRecordSet lstSubData = lstParentData.getDataRef(i,nSubDataCol_Idx).value<DbRecordSet>();
		//lstSubData.Dump();
		Q_ASSERT(lstSubData.getColumnCount()!=0);	//list must be defined
		lstSubData.setColValue(nSubListFKtoParentIdx,lstParentData.getDataRef(i,0).toInt());		//set id's FK=CntID
		lstRet_SubData.merge(lstSubData);

	}

	//lstRet_SubData.Dump();
	//if empty return:
	if (lstRet_SubData.getRowCount()==0)
		return;


	//write:
	//lstRet_SubData.Dump();
/*
	if (nSubData_TableID==BUS_CM_ADDRESS)
	{
		lstRet_SubData.Dump();
	
		int nSize=lstRet_SubData.getRowCount();
		int nZip=lstRet_SubData.getColumnIdx("BCMA_ZIP");
		for(int i=0;i<nSize;++i)
		{
			qDebug()<<"row:"<<i<<" "<<lstRet_SubData.getDataRef(i,nZip).toString();
		}
		
		
	}
	*/
	

	//_DUMP(lstRet_SubData)
	
	Status err;
	DbSimpleOrm TableOrm(err, nSubData_TableID, GetDbManager()); 
	TableOrm.Write(err,lstRet_SubData,-1,nSubDataSkipColWrite,&lstRet_SubStatus);
	
	//_DUMP(lstRet_SubStatus)

	if(!err.IsOK())
		AssignErrorsToParentStatuses(lstRet_SubData,lstParentStatus,lstRet_SubStatus,nSubListFKtoParentIdx);

	//lstParentStatus.Dump();

}


//if error occurs, try to find ID of contact in contact list and assign error code/text:
void Service_BusinessHelper::AssignErrorsToParentStatuses(DbRecordSet &lstSubData, DbRecordSet &lstParentStatus, DbRecordSet& lstSubStatuses,int nCntIDIdx)
{

	//loop through statuses: they contain all rows as in lstSubData, but with set status_code/text
	int nSize=lstSubStatuses.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		if (lstSubStatuses.getDataRef(i,1).toInt()==0) continue; //loop if no error

		//if found, find contact: ContID=0 index
		int nRow=lstParentStatus.find(0,lstSubData.getDataRef(i,nCntIDIdx).toInt(),true);  //<id, code, text>
		Q_ASSERT(nRow!=-1); //must be valid
		lstParentStatus.setData(nRow,1,lstSubStatuses.getDataRef(i,1));  //code
		lstParentStatus.setData(nRow,2,lstSubStatuses.getDataRef(i,2));  //text
	}


}


//IMPORTANT:  lstParent must be sorted by ID
//based on FK=ParentID assign all records from child list to the parent sublists:
void Service_BusinessHelper::AssignChildToParent(DbRecordSet &lstParent,QString strChildSubListCol,DbRecordSet &lstChilds,QString strFKCol,QString strParentIDCol)
{

	//determine parent ID in parent list
	int nParentID=0;
	if (!strParentIDCol.isEmpty())
		nParentID=lstParent.getColumnIdx(strParentIDCol);

	//get sublist col:
	int nSubListIdx=lstParent.getColumnIdx(strChildSubListCol);

	//get FK Idx:
	int nFKID=lstChilds.getColumnIdx(strFKCol);
	

	//3x times faster algorithm then before when using sort() and doing sequentially
	DbRecordSet lstTemp;
	lstTemp.copyDefinition(lstChilds);
	if (lstChilds.getRowCount()>0)
	{
		
		int nSizeChild=lstChilds.getRowCount();

		//setup temp list
		lstChilds.sort(nFKID); //sort by FK

		int nPrevFKID=-1, nRow=0;
		int nSize=lstParent.getRowCount();
		for (int i=0;i<nSize;++i)
		{	
			if (nRow==nSizeChild) break;
			lstTemp.clear();
			int nID=lstParent.getDataRef(i,nParentID).toInt();
			while (nID==lstChilds.getDataRef(nRow,nFKID).toInt())
			{
				lstTemp.merge(lstChilds.getRow(nRow));
				nRow++;
				if (nRow==nSizeChild) break;
			}
			lstParent.setData(i,nSubListIdx,lstTemp);
		}

	}
	else
	{
		lstParent.setColValue(nSubListIdx,lstTemp);
	}
}

int Service_BusinessHelper::GetRecordIDFromTask(Status &Ret_pStatus, int nTaskID)
{
	//STEP 1: get info on CE entity
	QString strSQL = QString("SELECT CENT_ID, CENT_SYSTEM_TYPE_ID FROM CE_COMM_ENTITY WHERE CENT_TASK_ID=%1").arg(nTaskID);
	DbRecordSet lstRecord;
	DbSqlQuery query(Ret_pStatus, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return -1;
	query.Execute(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK())	return -1;
	query.FetchData(lstRecord);
	//lstRecord.Dump();
	Q_ASSERT(lstRecord.getRowCount()==1 && lstRecord.getColumnCount()==2);
	if(lstRecord.getRowCount() < 1)
		return -1;

	int nCentID   = lstRecord.getDataRef(0, 0).toInt();
	int nCentType = lstRecord.getDataRef(0, 1).toInt();

	//STEP 2: depends on exact type, find record
	if(GlobalConstants::CE_TYPE_VOICE_CALL == nCentType)
		strSQL = QString("SELECT BVC_ID FROM BUS_VOICECALLS WHERE BVC_COMM_ENTITY_ID=%1").arg(nCentID);
	else if(GlobalConstants::CE_TYPE_DOCUMENT == nCentType)
		strSQL = QString("SELECT BDMD_ID FROM BUS_DM_DOCUMENTS WHERE BDMD_COMM_ENTITY_ID=%1").arg(nCentID);
	else if(GlobalConstants::CE_TYPE_EMAIL == nCentType)
		strSQL = QString("SELECT BEM_ID FROM BUS_EMAIL WHERE BEM_COMM_ENTITY_ID=%1").arg(nCentID);
	else
	{
		//unsupported or invalid CE type
		Q_ASSERT(false);
		return -1;
	}

	query.Execute(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK())	return -1;
	lstRecord.destroy();
	query.FetchData(lstRecord);
	//lstRecord.Dump();
	Q_ASSERT(lstRecord.getRowCount()==1 && lstRecord.getColumnCount()==1);
	if(lstRecord.getRowCount() < 1)
		return -1;

	int nRecordID = lstRecord.getDataRef(0, 0).toInt();
	return nRecordID;
}


void Service_BusinessHelper::ClientTaskNotification(Status &Ret_pStatus,DbRecordSet &recScheduledTask, bool bNewTask)
{
	Ret_pStatus.setError(0);

	if (recScheduledTask.getRowCount() == 0)
		return;

	if (bNewTask)
	{
		//if owner <> originator
		//if (recScheduledTask.getDataRef(0,"BTKS_OWNER").toInt()!=recScheduledTask.getDataRef(0,"BTKS_ORIGINATOR").toInt())
		//{
			//is owner logged? find 0, 1 or more socket id's:
			int nTargetPersonID=recScheduledTask.getDataRef(0,"BTKS_OWNER").toInt();
			int nOrigPersonID=recScheduledTask.getDataRef(0,"BTKS_ORIGINATOR").toInt();
			if (nTargetPersonID<=0) return;
			//if (nTargetPersonID==g_UserSessionManager->GetPersonID( ) //if our session, skip //issue 2231: send to all
			//	return;

			QStringList lstPersons;
			lstPersons.append(QString::number(nTargetPersonID)); //invite will be stored in app. server memory until user is logged!

			if (lstPersons.size()==0) return; //not currently logged (what if there is N servers)...

			//GET ID:
			int nTaskID=recScheduledTask.getDataRef(0,"BTKS_ID").toInt();
			QString strSQL="SELECT ";
			DbRecordSet lstRecord;
			int nTaskSystemType=recScheduledTask.getDataRef(0,"BTKS_SYSTEM_TYPE").toInt();
			if(GlobalConstants::TASK_TYPE_SCHEDULED_DOC == nTaskSystemType)
			{
				//lstRecord.addColumn(QVariant::Int, "BDMD_ID");
				strSQL += QString("BDMD_ID FROM BUS_DM_DOCUMENTS INNER JOIN CE_COMM_ENTITY ON CENT_ID = BDMD_COMM_ENTITY_ID INNER JOIN BUS_TASKS ON CENT_TASK_ID = BTKS_ID WHERE BTKS_ID=%1").arg(nTaskID);
			}
			else if(GlobalConstants::TASK_TYPE_SCHEDULED_EMAIL == nTaskSystemType)
			{
				//lstRecord.addColumn(QVariant::Int, "BEM_ID");
				strSQL += QString("BEM_ID FROM BUS_EMAIL INNER JOIN CE_COMM_ENTITY ON CENT_ID = BEM_COMM_ENTITY_ID INNER JOIN BUS_TASKS ON CENT_TASK_ID = BTKS_ID WHERE BTKS_ID=%1").arg(nTaskID);
			}
			else if(GlobalConstants::TASK_TYPE_SCHEDULED_VOICE_CALL == nTaskSystemType)
			{
				//lstRecord.addColumn(QVariant::Int, "BVC_ID");
				strSQL += QString("BVC_ID FROM BUS_VOICECALLS INNER JOIN CE_COMM_ENTITY ON CENT_ID = BVC_COMM_ENTITY_ID INNER JOIN BUS_TASKS ON BTKS_ID=CENT_TASK_ID WHERE BTKS_ID=%1").arg(nTaskID);
			}
			else	
				return;

			//Make local query, can fail if connection reservation fails.
			DbSqlQuery query(Ret_pStatus, GetDbManager()); 
			if(!Ret_pStatus.IsOK()) return;
			//load id:
			query.Execute(Ret_pStatus,strSQL);
			if(!Ret_pStatus.IsOK())	return;
			query.FetchData(lstRecord);
			//lstRecord.Dump();
			Q_ASSERT(lstRecord.getRowCount()==1 && lstRecord.getColumnCount()==1);

			//id,type, sys_type, sys_status, entity_id (";" delimited)!
			QStringList lstMessage;
			lstMessage.append(recScheduledTask.getDataRef(0,"BTKS_ID").toString());
			lstMessage.append(recScheduledTask.getDataRef(0,"BTKS_TASK_TYPE_ID").toString());
			lstMessage.append(recScheduledTask.getDataRef(0,"BTKS_SYSTEM_TYPE").toString());
			lstMessage.append(recScheduledTask.getDataRef(0,"BTKS_SYSTEM_STATUS").toString());
			lstMessage.append(lstRecord.getDataRef(0,0).toString());
			lstMessage.append(QString::number(nOrigPersonID));
			QString strMsg=lstMessage.join(";");


			//FIX #2700: register new task as an avatar card ("open on startup") if the task was created for someone else than yourself
			int nPersonID = recScheduledTask.getDataRef(0,"BTKS_OWNER").toInt();
			int nOriginator = recScheduledTask.getDataRef(0,"BTKS_ORIGINATOR").toInt();
			if (nPersonID != nOriginator)
			{
				//get options and settings:
				//----------------------------------
				DbRecordSet recSettings;
				g_BusinessServiceSet->CoreServices->GetPersonalSettingsByPersonID(Ret_pStatus,nPersonID,recSettings);
				if (!Ret_pStatus.IsOK()) return;

				OptionsAndSettingsManager settings;
				settings.SetSettingsDataSource(&recSettings,nPersonID);

				QString strList = settings.GetPersonSetting(AVATAR_STARTUP_LIST).toString();

				//NOTE: AVATAR_PROJECT and AVATAR_CONTACT are not used for tasks
				//TOFIX: does AVATAR_TASK_ADDRESS, AVATAR_TASK_INTERNET_FILE, AVATAR_TASK_LOCAL_FILE, AVATAR_TASK_PAPER_DOCS
				int nAvatarType = -1;
				int nTaskType = recScheduledTask.getDataRef(0,"BTKS_SYSTEM_TYPE").toInt();
				Q_ASSERT(GlobalConstants::TASK_TYPE_NO_TYPE != nTaskType);
				if(GlobalConstants::TASK_TYPE_SCHEDULED_DOC == nTaskType)
					nAvatarType = AVATAR_TASK_NOTE;
				else if(GlobalConstants::TASK_TYPE_SCHEDULED_EMAIL == nTaskSystemType)
					nAvatarType = AVATAR_TASK_EMAIL;
				else if(GlobalConstants::TASK_TYPE_SCHEDULED_VOICE_CALL == nTaskSystemType)
					nAvatarType = AVATAR_TASK_CALL;
				else
					return;

				int nTaskID = recScheduledTask.getDataRef(0,"BTKS_ID").toInt();
				int nRecordID = GetRecordIDFromTask(Ret_pStatus, nTaskID);
				if(nRecordID < 1)
					return;

				QString strNewAvatar;
				strNewAvatar += QString("%1").arg(nAvatarType);
				strNewAvatar += ",";
				strNewAvatar += QString("%1").arg(nRecordID);
				strNewAvatar += ":";	//divides fixed from non-fixed data
				strNewAvatar += QString("%1").arg(0);
				strNewAvatar += ",";
				strNewAvatar += QString("%1").arg(0);
				strNewAvatar += ";";

				//append new avatar
				if(strList.isEmpty())
					strList += ";";
				strList += strNewAvatar;

				settings.SetPersonSetting(AVATAR_STARTUP_LIST, strList);

				//store back to database
				DbRecordSet recDummy;
				settings.GetDataSource(recDummy, recSettings);
				//recSettings.Dump();
				g_BusinessServiceSet->CoreServices->SavePersonalSettingsByPersonID(Ret_pStatus,nPersonID,recSettings);
				if (!Ret_pStatus.IsOK()) return;
			}

						g_MessageDispatcher->SendMessageX(StatusCodeSet::TYPE_SILENT_SERVER_NOTIFICATION,StatusCodeSet::ERR_BUS_NEW_TASK_NOTIFICATION,strMsg,&lstPersons,g_UserSessionManager->GetSocketID());
		//}
	}
	else
	{
		//issue 1972: if some1 changes task: if chkbox is true then ->orig and owner always
		//if (recScheduledTask.getDataRef(0,"BTKS_NOTIFY_ORIGINATOR").toInt()>0 && recScheduledTask.getDataRef(0,"BTKS_OWNER").toInt()!=recScheduledTask.getDataRef(0,"BTKS_ORIGINATOR").toInt())
		//{
			//is owner logged? find 0, 1 or more socket id's:
			int nOrigPersonID=recScheduledTask.getDataRef(0,"BTKS_ORIGINATOR").toInt();
			int nOwnerID=recScheduledTask.getDataRef(0,"BTKS_OWNER").toInt();
			//int nCurrentID=g_UserSessionManager->GetPersonID(); //issue 2231
			///if (nOrigPersonID==nCurrentID)
			//	nOrigPersonID=0;
			//if (nOwnerID==nCurrentID)
			//	nOwnerID=0;

			QStringList lstPersons;
			if (nOrigPersonID>0 && recScheduledTask.getDataRef(0,"BTKS_NOTIFY_ORIGINATOR").toInt()>0)
			{
				lstPersons.append(QString::number(nOrigPersonID));
			}
			if (nOwnerID)
			{
				lstPersons.append(QString::number(nOwnerID));
			}

			if (lstPersons.size()==0) return;

			//GET ID:
			int nTaskID=recScheduledTask.getDataRef(0,"BTKS_ID").toInt();
			QString strSQL="SELECT ";
			DbRecordSet lstRecord;
			int nTaskSystemType=recScheduledTask.getDataRef(0,"BTKS_SYSTEM_TYPE").toInt();
			if(GlobalConstants::TASK_TYPE_SCHEDULED_DOC == nTaskSystemType)
			{
				//lstRecord.addColumn(QVariant::Int, "BDMD_ID");
				strSQL += QString("BDMD_ID FROM BUS_DM_DOCUMENTS INNER JOIN CE_COMM_ENTITY ON CENT_ID = BDMD_COMM_ENTITY_ID INNER JOIN BUS_TASKS ON CENT_TASK_ID = BTKS_ID WHERE BTKS_ID=%1").arg(nTaskID);
			}
			else if(GlobalConstants::TASK_TYPE_SCHEDULED_EMAIL == nTaskSystemType)
			{
				//lstRecord.addColumn(QVariant::Int, "BEM_ID");
				strSQL += QString("BEM_ID FROM BUS_EMAIL INNER JOIN CE_COMM_ENTITY ON CENT_ID = BEM_COMM_ENTITY_ID INNER JOIN BUS_TASKS ON CENT_TASK_ID = BTKS_ID WHERE BTKS_ID=%1").arg(nTaskID);
			}
			else if(GlobalConstants::TASK_TYPE_SCHEDULED_VOICE_CALL == nTaskSystemType)
			{
				//lstRecord.addColumn(QVariant::Int, "BVC_ID");
				strSQL += QString("BVC_ID FROM BUS_VOICECALLS INNER JOIN CE_COMM_ENTITY ON CENT_ID = BVC_COMM_ENTITY_ID INNER JOIN BUS_TASKS ON BTKS_ID=CENT_TASK_ID WHERE BTKS_ID=%1").arg(nTaskID);
			}
			else	
				return;

			//Make local query, can fail if connection reservation fails.
			DbSqlQuery query(Ret_pStatus, GetDbManager()); 
			if(!Ret_pStatus.IsOK()) return;
			//load id:
			query.Execute(Ret_pStatus,strSQL);
			if(!Ret_pStatus.IsOK())	return;
			query.FetchData(lstRecord);
			//lstRecord.Dump();
			Q_ASSERT(lstRecord.getRowCount()==1 && lstRecord.getColumnCount()==1);

			//id,type, sys_type, sys_status, entity_id,originator_id (";" delimited)!
			QStringList lstMessage;
			lstMessage.append(recScheduledTask.getDataRef(0,"BTKS_ID").toString());
			lstMessage.append(recScheduledTask.getDataRef(0,"BTKS_TASK_TYPE_ID").toString());
			lstMessage.append(recScheduledTask.getDataRef(0,"BTKS_SYSTEM_TYPE").toString());
			lstMessage.append(recScheduledTask.getDataRef(0,"BTKS_SYSTEM_STATUS").toString());
			lstMessage.append(lstRecord.getDataRef(0,0).toString());
			lstMessage.append(QString::number(nOrigPersonID));
			QString strMsg=lstMessage.join(";");


			//FIX #2700: register new task as an avatar card ("open on startup") if the task was created for someone else than yourself
			int nPersonID = recScheduledTask.getDataRef(0,"BTKS_OWNER").toInt();
			int nOriginator = recScheduledTask.getDataRef(0,"BTKS_ORIGINATOR").toInt();
			if (nPersonID != nOriginator)
			{
				//get options and settings:
				//----------------------------------
				DbRecordSet recSettings;
				g_BusinessServiceSet->CoreServices->GetPersonalSettingsByPersonID(Ret_pStatus,nPersonID,recSettings);
				if (!Ret_pStatus.IsOK()) return;

				OptionsAndSettingsManager settings;
				settings.SetSettingsDataSource(&recSettings,nPersonID);

				QString strList = settings.GetPersonSetting(AVATAR_STARTUP_LIST).toString();

				//NOTE: AVATAR_PROJECT and AVATAR_CONTACT are not used for tasks
				//TOFIX: does AVATAR_TASK_ADDRESS, AVATAR_TASK_INTERNET_FILE, AVATAR_TASK_LOCAL_FILE, AVATAR_TASK_PAPER_DOCS
				int nAvatarType = -1;
				int nTaskType = recScheduledTask.getDataRef(0,"BTKS_SYSTEM_TYPE").toInt();
				Q_ASSERT(GlobalConstants::TASK_TYPE_NO_TYPE != nTaskType);
				if(GlobalConstants::TASK_TYPE_SCHEDULED_DOC == nTaskType)
					nAvatarType = AVATAR_TASK_NOTE;
				else if(GlobalConstants::TASK_TYPE_SCHEDULED_EMAIL == nTaskSystemType)
					nAvatarType = AVATAR_TASK_EMAIL;
				else if(GlobalConstants::TASK_TYPE_SCHEDULED_VOICE_CALL == nTaskSystemType)
					nAvatarType = AVATAR_TASK_CALL;
				else
					return;

				int nTaskID = recScheduledTask.getDataRef(0,"BTKS_ID").toInt();
				int nRecordID = GetRecordIDFromTask(Ret_pStatus, nTaskID);
				if(nRecordID < 1)
					return;

				QString strNewAvatar;
				strNewAvatar += QString("%1").arg(nAvatarType);
				strNewAvatar += ",";
				strNewAvatar += QString("%1").arg(nRecordID);
				strNewAvatar += ":";	//divides fixed from non-fixed data
				strNewAvatar += QString("%1").arg(0);
				strNewAvatar += ",";
				strNewAvatar += QString("%1").arg(0);
				strNewAvatar += ";";

				//append new avatar
				if(strList.isEmpty())
					strList += ";";
				strList += strNewAvatar;

				settings.SetPersonSetting(AVATAR_STARTUP_LIST, strList);

				//store back to database
				DbRecordSet recDummy;
				settings.GetDataSource(recDummy, recSettings);
				recSettings.Dump();
				g_BusinessServiceSet->CoreServices->SavePersonalSettingsByPersonID(Ret_pStatus,nPersonID,recSettings);
				if (!Ret_pStatus.IsOK()) return;
			}


			g_MessageDispatcher->SendMessageX(StatusCodeSet::TYPE_SILENT_SERVER_NOTIFICATION,StatusCodeSet::ERR_BUS_TASK_NOTIFY_ORIGINATOR,strMsg,&lstPersons,g_UserSessionManager->GetSocketID());

		//}
	}
}


//from TVIEW_BUS_CONTACT_FULL, use pic_id and bpic_picture->if non empty->edit or insert->take back id
//NOTE pcitures are delete by trigger->update trigger on contact->if set bigpic_id=0, prev value is deleted
void Service_BusinessHelper::WriteEntityPictures(Status &Ret_pStatus, DbRecordSet &RetOut_Data, QString strBigPicIDColName, QString strPreviewPicColName)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_BIG_PICTURE, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	
	//get data for write:
	DbRecordSet lstPictureWrite,lstPictureDelete;

	//writes picture data from picture entity (only images)
	lstPictureWrite.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_BIG_PICTURE));
	lstPictureDelete.copyDefinition(lstPictureWrite);

	RetOut_Data.clearSelection();

	//copy pic data for save (edit or insert)
	//delete is automatic on DB (trigger)
	int nSize=RetOut_Data.getRowCount();
	for (int i =0;i<nSize;i++)
	{

		//check if picture exists: (both prev && full must be empty)
		if(RetOut_Data.getDataRef(i,"BPIC_PICTURE").toByteArray().size()==0 && RetOut_Data.getDataRef(i,strPreviewPicColName).toByteArray().size()==0) 
		{
			//if pic is null & ID is valid, set it to zero, trigger on BUS_PERSON must automatically delete pics
			QVariant nullVar(QVariant::Int);
			RetOut_Data.setData(i,strBigPicIDColName,nullVar);
			continue;

		}

		//only write pic if loaded:
		if(RetOut_Data.getDataRef(i,"BPIC_PICTURE").toByteArray().size()!=0 )
		{
			lstPictureWrite.addRow();
			lstPictureWrite.setData(lstPictureWrite.getRowCount()-1,"BPIC_ID",RetOut_Data.getDataRef(i,strBigPicIDColName));
			lstPictureWrite.setData(lstPictureWrite.getRowCount()-1,"BPIC_PICTURE",RetOut_Data.getDataRef(i,"BPIC_PICTURE"));
			RetOut_Data.selectRow(i); //tagg row for write
		}

	}


	//save and returns ID's back to list
	if(lstPictureWrite.getRowCount()>0)
	{
		//save pictures first:
		TableOrm.Write(Ret_pStatus,lstPictureWrite);
		if(!Ret_pStatus.IsOK())	return;

		//copy back new ID's:
		int j=0;
		for (int i =0;i<nSize;i++)
		{
			if(RetOut_Data.isRowSelected(i))
			{
				RetOut_Data.setData(i,strBigPicIDColName,lstPictureWrite.getDataRef(j,0));
				j++;
			}
		}
	}

}

void Service_BusinessHelper::ReadEntityPictures(Status &Ret_pStatus,DbRecordSet &Ret_Data,QString strBigPicIDColName)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_BIG_PICTURE, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	QString strWhere;
	int nSize=Ret_Data.getRowCount();
	if (nSize==0)
		return;
	int nActualPicsToRead=0;
	for(int i=0;i<nSize;++i)
	{
		if (Ret_Data.getDataRef(i,strBigPicIDColName).toInt()>0)
		{
			strWhere+=Ret_Data.getDataRef(i,strBigPicIDColName).toString()+",";
			nActualPicsToRead++;
		}
	}
	if (nActualPicsToRead==0)
		return;

	if (Ret_Data.getColumnIdx("BPIC_PICTURE")<0) //check
	{
		Ret_pStatus.setError(1,"BPIC_PICTURE does not exist, operation aborted!");
		return;
	}

	strWhere.chop(1);
	strWhere=" WHERE BPIC_ID IN ("+strWhere+") ";

	Status err;
	DbRecordSet lstPics;
	TableOrm.Read(Ret_pStatus,lstPics,strWhere);
	if (Ret_pStatus.IsOK())
	{
		for(int i=0;i<nSize;++i)
		{
			int nRow=lstPics.find("BPIC_ID",Ret_Data.getDataRef(i,strBigPicIDColName).toInt(),true);
			if (nRow!=-1)
			{
				Ret_Data.setData(i,"BPIC_PICTURE",lstPics.getDataRef(nRow,"BPIC_PICTURE"));
			}
		}
	}
}

//strSQLSelect must in form of SELECT BLOB WHERE XXX
void Service_BusinessHelper::Blob2FileChunked(Status &Ret_pStatus,QString strSQLSelect, QString strFilePath,bool bDecompress)
{
	Ret_pStatus.setError(0);

	DbSqlQuery query(Ret_pStatus, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	query.Execute(Ret_pStatus,strSQLSelect);
	if(!Ret_pStatus.IsOK()) 	
		return;

#ifdef _WIN32 //no chinked blob
	QFile file(strFilePath);
	if(!file.open(QIODevice::WriteOnly))
	{
		Ret_pStatus.setError(StatusCodeSet::ERR_BUS_CHUNKED_BLOB2FILE_FAILED,query.GetQSqlQuery()->lastError().text());
		return;
	}  

	QVariant buffer;
	bool beof=true;
	bool bOk=true;
	do 
	{
		bOk=query.GetQSqlQuery()->fetchBlobChunk(buffer,beof); //chunk size!?
		if (bOk)
		{
			if(file.write(buffer.toByteArray())==-1)
			{
				file.close();
				Ret_pStatus.setError(StatusCodeSet::ERR_BUS_CHUNKED_BLOB2FILE_FAILED,file.errorString());
				return;
			}
		}
	} while(bOk && !beof);

	file.close();
	if (!bOk)
	{
		Ret_pStatus.setError(StatusCodeSet::ERR_BUS_CHUNKED_BLOB2FILE_FAILED,query.GetQSqlQuery()->lastError().text());
		return;
	}

	if (bDecompress)
	{
		//read all from strFile, compress, remove file, save back from memory...
		if(!file.open(QIODevice::ReadOnly)) //lock acess
		{
			Ret_pStatus.setError(StatusCodeSet::ERR_BUS_CHUNKED_BLOB2FILE_FAILED," error when decompressing file ");
			return;
		}
		QByteArray byteFileContent=file.readAll();
		byteFileContent=qUncompress(byteFileContent);
		file.close();
		if(!file.open(QIODevice::WriteOnly)) //lock acess
		{
			Ret_pStatus.setError(StatusCodeSet::ERR_BUS_CHUNKED_BLOB2FILE_FAILED," error when decompressing file ");
			return;

		}
		int nErr=file.write(byteFileContent);
		if (nErr==-1)
		{
			Ret_pStatus.setError(StatusCodeSet::ERR_BUS_CHUNKED_BLOB2FILE_FAILED," error when decompressing file ");
			return;

		}
		file.close();
	}
#endif
}

//strSQLUpdate must in form of UPDATE BLOB WHERE XXX
void Service_BusinessHelper::File2BlobChunked(Status &Ret_pStatus,QString strSQLUpdate, QString strFilePath)
{
	Ret_pStatus.setError(0);

	DbSqlQuery query(Ret_pStatus, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	query.Prepare(Ret_pStatus,strSQLUpdate);
	if(!Ret_pStatus.IsOK()) 	
		return;

#ifdef _WIN32 //no chinked blob
	QFile file(strFilePath);
	if(!file.open(QIODevice::ReadOnly))
	{
		Ret_pStatus.setError(StatusCodeSet::ERR_BUS_CHUNKED_FILE2BLOB_FAILED,query.GetQSqlQuery()->lastError().text());
		return;
	}  

	bool beof=false;
	while(file.bytesAvailable()>0)
	{
		QByteArray buffer=file.read(256000); //256Kb
		beof=(file.bytesAvailable()==0);
		if (!query.GetQSqlQuery()->writeBlobChunk(buffer,beof))
		{
			file.close();
			Ret_pStatus.setError(StatusCodeSet::ERR_BUS_CHUNKED_FILE2BLOB_FAILED,query.GetQSqlQuery()->lastError().text());
			return;
		}
	}
	file.close();
#endif
}

void Service_BusinessHelper::GetOptionsAndSettings(Status &pStatus, int nPersonID, DbRecordSet &recSettings, DbRecordSet &recOptions)
{
	//Get person ID.
	QString strPersonID = QVariant(nPersonID).toString();

	DbSimpleOrm OPTIONS_SETTINGS_ORM(pStatus, BUS_OPT_SETTINGS, GetDbManager()); 
	if(!pStatus.IsOK()) return;

	QString strWhere = "WHERE BOUS_PERSON_ID = " + strPersonID;
	if (nPersonID>0)
	{
		//First person settings.
		OPTIONS_SETTINGS_ORM.Read(pStatus, recSettings, strWhere);
		if(!pStatus.IsOK()) return;
	}
	else
	{
		recSettings.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_OPT_SETTINGS));
	}

	//_DUMP(recSettings);

	strWhere = "WHERE BOUS_PERSON_ID IS NULL ";
	OPTIONS_SETTINGS_ORM.Read(pStatus, recOptions, strWhere);
	if(!pStatus.IsOK()) return;
}

