#ifndef USERACCESSRIGHT_SERVER_H
#define USERACCESSRIGHT_SERVER_H

#include "bus_core/bus_core/useraccessright.h"

/*!
	\class UserAccessRight_Server
	\brief Global server side object/cache for testing user access rights
	\ingroup Bus_Server
*/
class UserAccessRight_Server : public UserAccessRight
{

public:
    UserAccessRight_Server();
    ~UserAccessRight_Server();

protected:
	int		GetUserID(); 
	int		GetPersonID();
	bool	IsSystemUser();
	void	TestUAR(Status &Ret_pStatus, int nTableID, int nPersonID, int nAccessLevel, DbRecordSet &lstTest_IDs, DbRecordSet &Ret_lstResult_IDs, bool bReturnAllowed=true);

   
};

#endif // USERACCESSRIGHT_SERVER_H
