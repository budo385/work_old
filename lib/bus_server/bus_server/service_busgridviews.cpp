#include "service_busgridviews.h"
#include "db/db/dbsimpleorm.h"
#include "businesslocker.h"

void Service_BusGridViews::Read(Status &Ret_pStatus, int nGridID,DbRecordSet &Ret_Data)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_OPT_GRID_VIEWS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	//load all
	TableOrm.Read(Ret_pStatus,Ret_Data," WHERE BOGW_GRID_ID= "+QVariant(nGridID).toString());

}

void Service_BusGridViews::Write(Status &Ret_pStatus, DbRecordSet &RetOut_Data,QString strLockRes,QString strOldView,int nGridID)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_OPT_GRID_VIEWS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;


	//tran
	TableOrm.GetDbSqlQuery()->BeginTransaction(Ret_pStatus);
	if(!Ret_pStatus.IsOK()) return;


	//delete old:
	if (!strOldView.isEmpty())
	{
		TableOrm.GetDbSqlQuery()->Prepare(Ret_pStatus,"DELETE FROM BUS_OPT_GRID_VIEWS WHERE BOGW_VIEW_NAME=? AND BOGW_GRID_ID= "+QVariant(nGridID).toString());
		if(!Ret_pStatus.IsOK())
		{
			TableOrm.GetDbSqlQuery()->Rollback();
			return;
		}
		TableOrm.GetDbSqlQuery()->bindValue(0,strOldView);
		TableOrm.GetDbSqlQuery()->ExecutePrepared(Ret_pStatus);
		if(!Ret_pStatus.IsOK())
		{
			TableOrm.GetDbSqlQuery()->Rollback();
			return;
		}
	}



	//write
	TableOrm.Write(Ret_pStatus,RetOut_Data);
	if(!Ret_pStatus.IsOK())
	{
		TableOrm.GetDbSqlQuery()->Rollback();
		return;
	}

	//if write ok & locked, unlock:
	if(Ret_pStatus.IsOK() && !strLockRes.isEmpty())
	{
		BusinessLocker locker(Ret_pStatus,BUS_OPT_GRID_VIEWS,GetDbManager(), TableOrm.GetDbConnection());
		locker.UnLock(Ret_pStatus,strLockRes);
	}

	TableOrm.GetDbSqlQuery()->Commit(Ret_pStatus);

}

void Service_BusGridViews::Delete(Status &Ret_pStatus, QString strView,int nGridID)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_OPT_GRID_VIEWS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	TableOrm.GetDbSqlQuery()->Prepare(Ret_pStatus,"DELETE FROM BUS_OPT_GRID_VIEWS WHERE BOGW_VIEW_NAME=? AND BOGW_GRID_ID= "+QVariant(nGridID).toString());
	if(!Ret_pStatus.IsOK())
		return;

	TableOrm.GetDbSqlQuery()->bindValue(0,strView);
	TableOrm.GetDbSqlQuery()->ExecutePrepared(Ret_pStatus);

}


void Service_BusGridViews::Lock(Status &Ret_pStatus, QString strView,QString &Ret_strLockRes,int nGridID)
{

	//load all:
	//Make local query, can fail if connection reservation fails.
	DbSqlQuery query(Ret_pStatus, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	//load id's:
	query.Prepare(Ret_pStatus,"SELECT BOGW_ID FROM BUS_OPT_GRID_VIEWS WHERE BOGW_VIEW_NAME=? AND BOGW_GRID_ID= "+QVariant(nGridID).toString());
	if(!Ret_pStatus.IsOK())	return;
	query.bindValue(0,strView);
	query.ExecutePrepared(Ret_pStatus);
	if(!Ret_pStatus.IsOK())	return;
	DbRecordSet lstIDs;
	query.FetchData(lstIDs);

	//Make Locker Obj & lock:
	BusinessLocker locker(Ret_pStatus,BUS_OPT_GRID_VIEWS,GetDbManager(), query.GetDbConnection());
	locker.Lock(Ret_pStatus,lstIDs,Ret_strLockRes);
}


void Service_BusGridViews::Unlock(Status &Ret_pStatus, QString strLockRes)
{
	BusinessLocker locker(Ret_pStatus,BUS_OPT_GRID_VIEWS,GetDbManager());
	locker.UnLock(Ret_pStatus,strLockRes);
}
