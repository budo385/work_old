#include "service_coreuser.h"
#include "common/common/status.h"
#include "db_core/db_core/dbsqltableview.h"
#include "db/db/dbconnectionreserver.h"
#include "db/db/dbsimpleorm.h"
#include "common/common/authenticator.h"

#include "bus_core/bus_core/usersessionmanager.h"
extern UserSessionManager	*g_UserSessionManager;

/*!
	Fetches one user record by username/ used by login

	\param Ret_Status			- if err occur, returned here
	\param strUsername			- user name, must be ASCII compatible string (no special chars)
	\param Ret_Data				- TVIEW_CORE_USER_SELECTION formmatted list
	\param pDbConn				- db conn
*/
void Service_CoreUser::GetUserByUserName(Status &Ret_pStatus, QString strUsername, DbRecordSet &Ret_Data)
{

	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, CORE_USER, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	//load 
	TableOrm.Read(Ret_pStatus,Ret_Data," WHERE CUSR_USERNAME='"+strUsername+"'",DbSqlTableView::TVIEW_CORE_USER_SELECTION);

}


/*!
Fetches one user record by session id 

	\param Ret_Status			- if err occur, returned here
	\param strSession			- session 
	\param Ret_Data				- TVIEW_CORE_USER_SELECTION formatted list
	\param pDbConn				- db conn
*/
void Service_CoreUser::GetUserBySession(Status &Ret_pStatus, QString strSession, DbRecordSet &Ret_Data)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, CORE_USER, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	//load 
	TableOrm.Read(Ret_pStatus,Ret_Data," INNER JOIN CORE_USER_SESSION ON COUS_CORE_USER_ID=CUSR_ID WHERE COUS_USER_SESSION_ID='"+strSession+"'",DbSqlTableView::TVIEW_CORE_USER_SELECTION);
}



/*!
	Fetches one user record by user id

	\param Ret_Status			- if err occur, returned here
	\param strSession			- session 
	\param Ret_Data				- TVIEW_CORE_USER_SELECTION formatted list
	\param pDbConn				- db conn
*/
void Service_CoreUser::GetUserByID(Status &Ret_pStatus, int nUserID, DbRecordSet &Ret_Data)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, CORE_USER, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	//load 
	TableOrm.Read(Ret_pStatus,Ret_Data," WHERE CUSR_ID="+QVariant(nUserID).toString(),DbSqlTableView::TVIEW_CORE_USER_SELECTION);

}





//------------------------------------------------------------------------
//						USER CONTEXT DATA
//------------------------------------------------------------------------

/*!
Creates new system wide unique session id. 
Loads AR and other data in user cache.
Cache is organized either by thread id or by user id but with counter (allows same user to login more then once to system)

\param pStatus				- if err occur, returned here
\param strSession			- returned session id, if all goes well
\param rowUserData			- row in TVIEW_CORE_USER_SELECTION with user data
\param strModuleCode		- client module code
\param strMLIID				- MLIID
\param pDbConn				- db conn

*/
void Service_CoreUser::BuildUserContextData(Status &pStatus,QString &strSession,DbRecordSet &rowUserData, QString strModuleCode, QString strMLIID, QString strParentSession,int nClientTimeZoneOffsetMinutes)
{


	//-------------------------------------------
	//SESSION
	//-------------------------------------------
	//Create new session or retest old one (cheks if user is active, license coutn, etc..):
	if(strSession.isEmpty())
		//create new session
		g_UserSessionManager->CreateSession(pStatus,strSession,rowUserData,strModuleCode,strMLIID,false,strParentSession, nClientTimeZoneOffsetMinutes);
	else
		//keep old one, if old one is bad (not in db nor in list) it means that it has expired, return error
		g_UserSessionManager->UpdateSession(pStatus,strSession,rowUserData,strModuleCode,strMLIID);

	if(!pStatus.IsOK()) return;

	//rowUserData.Dump();


	//-------------------------------------------
	//AR
	//-------------------------------------------
	/*
	int nUserID=rowUserData.getDataRef(0,"CUSR_ID").toBool();
	bool bIsSystem=rowUserData.getDataRef(0,"CUSR_IS_SYSTEM").toBool();
	g_AccessRight->LoadUserARRecordSet(pStatus,nUserID,bIsSystem);
	if(!pStatus.IsOK())//rollback session creation if error:
	{
		Status err;
		g_UserSessionManager->DeleteSession(err,strSession);
		Q_ASSERT(err.IsOK()); //only check in debug
		return;
	}
	*/
}


/*!
	B.T. DFO mechanism: when connection is broken: mark session inactive:
	a) connection is broken when Thread ID is sent here (delete from memory, keep in database as Inactive)!
	b) connection is logout-ed broken when Session ID is sent here:

	Inactive session are deleted by GBC, but only when last_modif<expired (last modif will be updated when set to inactive)
	All sessions will be deleted when server stops working!



	When user logout or session expires, call this to release memory from user cached data

	\param pStatus				- if err occur, returned here
	\param strSession			- user session, can be empty, but then thread must be valid
	\param nUserThreadID		- user thread id - must be valid!!!
	\param pDbConn				- db conn

*/
void Service_CoreUser::DestroyUserContextData(Status &pStatus,QString strSession,int nUserThreadID)
{

	/*
	//delete AR record only if session is valid:
	if(!strSession.isEmpty())
	{
		int nUserID=g_UserSessionManager->GetUserID(strSession);
		if(nUserID!=-1)
			g_AccessRight->DeleteUserARRecordSet(nUserID);
	}
	*/

	//delete session
	if(!strSession.isEmpty())
	{
		g_UserSessionManager->DeleteSession(pStatus,strSession);
		g_UserSessionManager->ClearUserSessionStorage(strSession); //in all other cases GB will remove all garbage
	}
	else if (nUserThreadID!=-1)
		g_UserSessionManager->InactiveSession(pStatus,nUserThreadID);		//DFO: connection broken (client thread is dead, tcp socket is dead), keep session, do not delete, maybe client will emerge from dead
	else
		Q_ASSERT(false); //session and thread can not be invalid in same time

	
}