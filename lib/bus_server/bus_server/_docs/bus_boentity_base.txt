/**
 *
 * \defgroup Bus_Entity_Base  Base classes for Business Entity Services
 * 
 *
 * \ingroup Bus_Entity
 *
 * Contains business entity classes
 *
 *
*/