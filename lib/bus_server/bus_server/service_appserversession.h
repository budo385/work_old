#ifndef SERVICE_APPSRVSESSION_H
#define SERVICE_APPSRVSESSION_H


#include "systemservice.h"



/*!
    \class Service_AppSrvSession
    \brief Access to DB AppSrv Session table
    \ingroup Bus_System

	Provides methods for chaning app. srv session data in DB table.
	One application server has only 1 entry in the CORE_APP_SRV_SESSION table

	WARNING: because various threads can update same record at same time: introduce mutex to avoid deadlock on database.
	MUTEX only on WRITE op: specially on UpdateLastActivity and UpdateCurrentConnection
*/

class Service_AppSrvSession: public SystemService
{
public:
	
	Service_AppSrvSession();

	//basic
	void Read(Status &pStatus,int  nAppServerID,DbRecordSet& lstRecords);
	void ReadLBOList(Status &pStatus,DbRecordSet& lstRecords);

	//session handlers:
	void StartSession(Status &pStatus, int nServerMode,DbRecordSet& lstRecords);
	void StopSession(Status &pStatus,int  nAppServerID);

	//report
	void UpdateLastActivity(Status &pStatus,int nAppSrvSessionID);
	void UpdateCurrentConnection(Status &pStatus,int nAppSrvSessionID);

	//master: global garbage collector (cleans app and THICK):
	void CheckServerSessions(Status &pStatus,int nAppServerSessionID);

	//THICK gbc (on startup check all THICK expired sessions):
	void CleanExpiredThickSessions(Status &pStatus);	
	void CleanAllSessions(Status &pStatus);		//after backup, call this!!

	void GetUpdateDate(Status &pStatus,QDate &dateUpdate);	

private:

	void CheckServers(Status &pStatus,int& nCnt_ActiveLBOServers, int& nCnt_ActiveMasterServers);
	DbRecordSet GetSessions(Status &pStatus,bool bActive,int nRole);
	QMutex mutex;
};


#endif //SERVICE_APPSRVSESSION_H
