#include "serverlocalcache.h"
#include "common/common/entity_id_collection.h"
#include "bus_core/bus_core/mainentityfilter.h"
#include "businessserviceset.h"
extern BusinessServiceSet* g_BusinessServiceSet;

ServerLocalCache::ServerLocalCache()
{
}

bool ServerLocalCache::LoadTypes(int nEntityTypeID)
{
	Status err;
	DbRecordSet filter;

	if (nEntityTypeID!=-1)
	{
		QString strWhere =" BCMT_ENTITY_TYPE = "+QVariant(nEntityTypeID).toString();
		MainEntityFilter filterObj;
		filterObj.SetFilter(MainEntityFilter::FILTER_SQL_WHERE,strWhere);
		filter=filterObj.GetRecordSet();
	}

	g_BusinessServiceSet->MainEntitySelector->ReadData(err,ENTITY_BUS_CM_TYPES,filter,m_lstTypes);
	if (err.IsOK())
		return true;
	else
		return false;
}



//based on system type, fetch user defined type:
QVariant ServerLocalCache::GetUserTypeBasedOnSystemType(int nEntityTypeID, int nSystemTypeID)
{
	QVariant varType(QVariant::Int); //NULL TYPE
	
	if (m_lstTypes.getRowCount()==0)
		LoadTypes();

	if(m_lstTypes.getRowCount()==0)
	{
		return varType;
	}

	//select all
	m_lstTypes.find(m_lstTypes.getColumnIdx("BCMT_ENTITY_TYPE"),nEntityTypeID);
	//select defaults:
	int nSelected=m_lstTypes.find(m_lstTypes.getColumnIdx("BCMT_SYSTEM_TYPE"),nSystemTypeID,false,true,true);
	if(nSelected==1)
		return m_lstTypes.getDataRef(m_lstTypes.getSelectedRow(),"BCMT_ID");
	else
		return varType;

}
