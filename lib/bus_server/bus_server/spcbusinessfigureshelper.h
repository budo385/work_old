#ifndef SPCBUSINESSFIGURESHELPER_H
#define SPCBUSINESSFIGURESHELPER_H

#include "common/common/dbrecordset.h"

class SpcBusinessFiguresHelper
{
public:
	SpcBusinessFiguresHelper();
	~SpcBusinessFiguresHelper();

	static void InitVars_151(DbRecordSet &Ret_lstData);
	static void InitVars_110(DbRecordSet &Ret_lstData);
	static void InitVars_102(DbRecordSet &Ret_lstData);
	static void InitVars_112(DbRecordSet &Ret_lstData);
	static void InitVars_117(DbRecordSet &Ret_lstData);

private:
	
};

#endif // SPCBUSINESSFIGURESHELPER_H
