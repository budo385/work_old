#include "commgridviewhelper_server.h"

#include "db/db/dbconnectionreserver.h"
#include "db/db/dbsimpleorm.h"
#include "bus_core/bus_core/usersessionmanager.h"
extern UserSessionManager	*g_UserSessionManager;
#include "businessserviceset.h"
extern BusinessServiceSet* g_BusinessServiceSet;


#include "db_core/db_core/dbtableiddefinition.h"
/*#include "db/db/dbsimpleorm.h"
#include "businessserviceset.h"
extern BusinessServiceSet* g_BusinessServiceSet;*/

void CommGridViewHelper_Server::GetDocApplicationsFromServer(DbRecordSet &lstData, Status &err)
{
	if (m_lstApplicationsCache.getColumnCount()==0 && m_lstApplicationsCache.getRowCount()==0)
	{
		DbSimpleOrm BUS_DM_APPLICATIONS_ORM(err, BUS_DM_APPLICATIONS, m_DbManager);
		if(!err.IsOK()) return;

		//Read.
		BUS_DM_APPLICATIONS_ORM.Read(err, lstData);
		m_lstApplicationsCache=lstData;
	}
	else
		lstData=m_lstApplicationsCache;
}
