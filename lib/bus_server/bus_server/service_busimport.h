#ifndef SERVICE_BUSIMPORT_H
#define SERVICE_BUSIMPORT_H

#include "businessservice.h"
#include "bus_interface/bus_interface/interface_busimport.h"
#include "hierarchicaldata.h"
#include "db/db/dbsimpleorm.h"

/*!
	\class Service_BusImport
	\brief Special implementation for importing data
	\ingroup Bus_Services

*/

class Service_BusImport : public Interface_BusImport, public BusinessService
{
public:
	Service_BusImport();

	void ImportProjects(Status &Ret_pStatus, DbRecordSet &RetOut_Data,int nOperation);
	void ImportPersons(Status &Ret_pStatus, DbRecordSet &RetOut_Data,int nOperation, bool bCreateUsers, bool bCreateContacts, int nRoleID);
	void ImportContacts(Status &Ret_pStatus, DbRecordSet &RetOut_Data, DbRecordSet &RetOut_ErrRow,int nOperation, bool bCreateUsers);
	void ImportUserContactRelationships(Status &Ret_pStatus, DbRecordSet &RetOut_Data);
	void ImportContactContactRelationships(Status &Ret_pStatus, DbRecordSet &RetOut_Data);
	void GetContactContactRelationships(Status &Ret_pStatus, DbRecordSet &RetOut_Data);
	void ImportContactCustomData(Status &Ret_pStatus, QString strCustomFieldName, int nTableID, DbRecordSet &RetOut_Data);

	//automated data exchange
	void ProcessProjects_Import(Status &Ret_pStatus, QString strFileExt, QString strFilePrefix, QString strFileExtOfProcessed, QString strDirPath, DbRecordSet &Ret_ProcessedFiles, DbRecordSet &Ret_pErrors);
	void ProcessProjects_Export(Status &Ret_pStatus, QString strFileExt, QString strFilePrefix, QString strFileExtOfProcessed, QString strDirPath, int nExportLasN, bool bExportAddDateTimeOnExportFile, DbRecordSet &Ret_ProcessedFiles, DbRecordSet &Ret_pErrors, DbRecordSet &ProjectsForExport, bool bClientSideExport, QByteArray &Ret_File, bool bUtf8);

	void ProcessUsers_Import(Status &Ret_pStatus, QString strFileExt, QString strFilePrefix, QString strFileExtOfProcessed, QString strDirPath, DbRecordSet &Ret_ProcessedFiles, DbRecordSet &Ret_pErrors, int nRoleID);
	void ProcessUsers_Export(Status &Ret_pStatus, QString strFileExt, QString strFilePrefix, QString strFileExtOfProcessed, QString strDirPath, int nExportLasN, bool bExportAddDateTimeOnExportFile, DbRecordSet &Ret_ProcessedFiles, DbRecordSet &Ret_pErrors, DbRecordSet &UsersForExport, bool bClientSideExport, QByteArray &Ret_File, bool bUtf8);

	//void ProcessDebtors_Import(Status &Ret_pStatus, QString strFileExt, QString strFilePrefix, QString strFileExtOfProcessed, QString strDirPath, DbRecordSet &Ret_ProcessedFiles, DbRecordSet &Ret_pErrors);
	void ProcessDebtors_Export(Status &Ret_pStatus, QString strFileExt, QString strFilePrefix, QString strFileExtOfProcessed, QString strDirPath, int nExportLasN, bool bExportAddDateTimeOnExportFile, DbRecordSet &Ret_ProcessedFiles, DbRecordSet &Ret_pErrors, int nAddressTypeFilter, bool bUtf8);

	void ProcessContacts_Import(Status &Ret_pStatus, QString strFileExt, QString strFilePrefix, QString strFileExtOfProcessed, QString strDirPath, DbRecordSet &Ret_ProcessedFiles, DbRecordSet &Ret_pErrors);
	void ProcessContacts_Export(Status &Ret_pStatus, QString strFileExt, QString strFilePrefix, QString strFileExtOfProcessed, QString strDirPath, int nExportLasN, bool bExportAddDateTimeOnExportFile, DbRecordSet &Ret_ProcessedFiles, DbRecordSet &Ret_pErrors, DbRecordSet &UsersForExport, bool bClientSideExport, QByteArray &Ret_File, bool bUtf8, bool bSokratesHdr, bool bColumnTitles, bool bOutlookExport, QString strLang, QByteArray datHedFile);
	void ProcessContactContactRelations_Export(Status &Ret_pStatus, QString strFileExt, QString strFilePrefix, QString strFileExtOfProcessed, QString strDirPath, int nExportLasN, bool bExportAddDateTimeOnExportFile, DbRecordSet &Ret_ProcessedFiles, DbRecordSet &Ret_pErrors, DbRecordSet &UsersForExport, bool bClientSideExport, QByteArray &Ret_File, bool bUtf8, bool bSokratesHdr, bool bColumnTitles, bool bOutlookExport, QString strLang, QByteArray datHedFile);

	void ProcessStoredProjectList_Import(Status &Ret_pStatus, QString strFileExt, QString strFilePrefix, QString strFileExtOfProcessed, QString strDirPath, DbRecordSet &Ret_ProcessedFiles, DbRecordSet &Ret_pErrors);

	//void ProcessStoredProjectList_Import(Status &Ret_pStatus, QByteArray datHeader, QByteArray datDetail);

	bool LoadColMap(QString strFile, QString lstFormatHdrLine, QMap<int, int> &mapCols, QString &strNamesHeader, bool bReverseMap = false);
	bool LoadHCodeMap(QString strFile, QString lstFormatHdrLine, QMap<QString, QString> &mapCodes);

	static void StripHTML(QString &strData);

private:
	//contacts:
	void WriteImportListContacts(Status &Ret_pStatus, DbRecordSet &RetOut_Data);
	void WritePersonUserFromContact(Status &Ret_pStatus, DbRecordSet &RetOut_Data);
	void WriteGroupContactData(Status &Ret_pStatus, int nOperation,DbRecordSet &RetOut_Data);
	void WriteGroupContactRecord(Status &Ret_pStatus, int nOperation,DbRecordSet &RetOut_Data, DbSimpleOrm &orm,int nContactID);
	
	//username generation
	void GenerateUsername(Status &Ret_pStatus,DbRecordSet &lstUser);

	void FindContactDuplicates(Status &status, const DbRecordSet& contact, DbRecordSet& resultIDs, DbSqlQuery &query);
	void FindContactDuplicatesFull(Status &status, const DbRecordSet& contact, DbRecordSet& resultRows, DbSqlQuery &query);
	void UpdateContactPhones(Status &status, int nContactID, const DbRecordSet& data, DbSimpleOrm &orm);
	void UpdateContactEmails(Status &status, int nContactID, const DbRecordSet& data, DbSimpleOrm &orm);
	void UpdateContactInternet(Status &status, int nContactID, const DbRecordSet& data, DbSimpleOrm &orm);
	void UpdateContactAddresses(Status &status, int nContactID, const DbRecordSet& data, DbSimpleOrm &orm);
	void UpdateContactJournal(Status &status, int nContactID, const DbRecordSet& data, DbSimpleOrm &orm);
	void UpdateContactDebtor(Status &status, int nContactID, const DbRecordSet& data, DbSimpleOrm &orm);

	void FindProjectDuplicates(Status &status, const DbRecordSet& contact, DbRecordSet& resultIDs, DbSqlQuery &query);
	void WriteHierarchicalData(Status &status, DbRecordSet &RetOut_Data);

	void FindPersonDuplicates(Status &status, const DbRecordSet& contact, DbRecordSet& resultIDs, DbSqlQuery &query);
	void GetOrganizationInfo(Status &status, QVariant &nOrgID, QVariant &nDeptID, DbRecordSet& lstData, DbSqlQuery &query, QString &strResOrgName, QString &strResDeptName, int nDefaultOrgID=0);

	void ConvertFullContactTextList_2_ContactFull(Status &Ret_Status,DbRecordSet &lstInput, DbRecordSet &lstOut);
	void WriteProjectDebtors(Status &Ret_pStatus, DbRecordSet &RetOut_Data);

	int GetDefaultAdddressType();

	void GenerateRoleAndUserFromPerson(Status &Ret_pStatus, int nRoleID, DbRecordSet &lstWrite, DbSimpleOrm &orm1, bool bForceLoginUpdate = false);
	
	void UpdateProjectSPCFields(Status &Ret_pStatus,DbSqlQuery &query, DbRecordSet &rowWrite, DbRecordSet rowImport);
	void UpdateProjectSPCField_One(Status &Ret_pStatus,DbSqlQuery &query, DbRecordSet &rowWrite, DbRecordSet &rowImport, QString strProjCodeColName, QString strProjIDColName, QString strIDColName, QString strCodeColName,int nTableID,bool bIsTreeStructure);
	int  GetDefaultOrganization(Status &Ret_pStatus);

protected:
	HierarchicalData m_TreeItemManager;
};

#endif // SERVICE_BUSIMPORT_H
