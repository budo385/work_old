#include "service_webmwcloudservice.h"
#include "db/db/dbsqlquery.h"
#include "common/common/sha2.h"
#include "common/common/authenticator.h"
#include "bus_core/bus_core/businessdatatransformer.h"
#include "bus_core/bus_core/accessrightsid.h"
#include "trans/trans/xmlutil.h"
#include "bus_core/bus_core/optionsandsettingsid.h"
#include "emailimportthread.h"

#include "businessserviceset.h"
extern BusinessServiceSet* g_BusinessServiceSet;
#include "bus_core/bus_core/usersessionmanager.h"
extern UserSessionManager	*g_UserSessionManager;
#include "common/common/logger.h"
extern Logger							g_Logger;
#include "bus_core/bus_core/servercontrolabstract.h"
extern ServerControlAbstract *g_AppServer;

//flags as used by personal settings ("Email In")
#define EML_TYPE_POP3 1
#define EML_TYPE_IMAP 2

#define EML_SECURITY_NONE	  1
#define EML_SECURITY_STARTTLS 2
#define EML_SECURITY_SSLTLS	  3

//QString GenerateAccUID();
//void ImportMessageFromMime(Status &status, int nPersonID, int nIsOutgoing, QString strFrom, QString strTo, QString strSubject, QString strMime, QString strFullFrom, QString strCC, QString strBCC);

void Service_WebMWCloudService::CreateUser(Status &Ret_pStatus, QString strUsername, QString strPass, QString strFirstName, QString strLastName)
{
	DbSimpleOrm TableContact(Ret_pStatus, BUS_CM_CONTACT, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	DbSqlQuery *query = TableContact.GetDbSqlQuery();
	

	//MB: if user exists: just use same data, set new pass:
	QString strSQL="SELECT CUSR_ID FROM CORE_USER WHERE CUSR_USERNAME='"+strUsername+"'";
	query->Execute(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK()) return;

	int nUserID=-1;
	if (query->next())
	{
		nUserID=query->value(0).toInt();
	}

	//if userid<>0 just set new pass
	if (nUserID>0)
	{
		ChangeUserPassword(Ret_pStatus,strUsername,strPass);
		return;
	}
		


	//g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Service_WebMWCloudService::CreateUser 1");

	//create person record:
	DbRecordSet m_lstPerson;
	m_lstPerson.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_PERSON));
	m_lstPerson.addColumn(QVariant::ByteArray, "BPIC_PICTURE");
	m_lstPerson.addColumn(QVariant::Int, "USER_ID");
	
	m_lstPerson.addRow();
	m_lstPerson.setData(0,"BPER_FIRST_NAME",strFirstName);
	m_lstPerson.setData(0,"BPER_LAST_NAME",strLastName);
	m_lstPerson.setData(0,"BPER_ACTIVE_FLAG",1);
	QString strCode;
	GeneratePersonCode(Ret_pStatus,strCode);

	//g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Service_WebMWCloudService::CreateUser 2");

	//spec funct how to generate these two: select by dat_last modified last-> if empty or has other format then 8 digits, start from 00000001
	m_lstPerson.setData(0,"BPER_CODE",strCode);
	m_lstPerson.setData(0,"BPER_INITIALS",strCode);

	//create user:
	QByteArray pass=Authenticator::GeneratePassword(strUsername,strPass);
	//QByteArray pass=strPass.toLatin1();
	
	DbRecordSet m_lstCoreUser;
	m_lstCoreUser.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CORE_USER));
	m_lstCoreUser.addRow();
	m_lstCoreUser.setData(0,"CUSR_FIRST_NAME",strFirstName);
	m_lstCoreUser.setData(0,"CUSR_LAST_NAME",strLastName);
	m_lstCoreUser.setData(0,"CUSR_ACTIVE_FLAG",1);
	m_lstCoreUser.setData(0,"CUSR_IS_SYSTEM",0);
	m_lstCoreUser.setData(0,"CUSR_LOCAL_ACCESS_ONLY",0);
	m_lstCoreUser.setData(0,"CUSR_USERNAME",strUsername);
	m_lstCoreUser.setData(0,"CUSR_PASSWORD",pass);

	//g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Service_WebMWCloudService::CreateUser 3");
		
	//role
	int nRoleID;
	GetDefaultPersonRole(Ret_pStatus,nRoleID);
	if (!Ret_pStatus.IsOK())return;

	//open transaction:
	
	query->BeginTransaction(Ret_pStatus);
	if (!Ret_pStatus.IsOK())return;
	
	//g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Service_WebMWCloudService::CreateUser 4");

	//create contact first:
	DbRecordSet lstContact;
	BusinessDataTransformer::Person2Contact(m_lstPerson, lstContact,true,false,true);
	TableContact.Write(Ret_pStatus,lstContact);
	if (!Ret_pStatus.IsOK())
	{
		query->Rollback();
		return;
	}
		
	//g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Service_WebMWCloudService::CreateUser 5");

	//update contact link
	m_lstPerson.getDataRef(0, "BPER_CONTACT_ID") = lstContact.getDataRef(0, "BCNT_ID");
	
	//write person & core user
	g_BusinessServiceSet->BusPerson->WriteData(Ret_pStatus,m_lstPerson,m_lstCoreUser,"",nRoleID);
	if (!Ret_pStatus.IsOK())
	{
		query->Rollback();
		return;
	}

	//g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,"Service_WebMWCloudService::CreateUser 6");

	query->Commit(Ret_pStatus);
}

void Service_WebMWCloudService::ChangeUserPassword(Status &Ret_pStatus, QString strUsername, QString strPass)
{
	DbSqlQuery query(Ret_pStatus,GetDbManager());
	if(!Ret_pStatus.IsOK())return;

	QString strSQL="UPDATE CORE_USER SET CUSR_PASSWORD=? WHERE CUSR_USERNAME=?";
	query.Prepare(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK())
	{
		return;
	}
	//create user:
	QByteArray newPass=Authenticator::GeneratePassword(strUsername,strPass);

	query.bindValue(0,newPass);
	query.bindValue(1,strUsername);
	query.ExecutePrepared(Ret_pStatus);
}

void Service_WebMWCloudService::RemoveUserEmailAccount(Status &Ret_pStatus, QString strUsername, QString strConce, QString strAuthToken, QString strEmail)
{
	QString strSession = LogOn(strUsername,strConce,strAuthToken);
	if (strSession.isEmpty())
	{
		Ret_pStatus.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_WRONG_PASS_USER);
		return;
	}
	int nPersonID=g_UserSessionManager->GetPersonID();

	//load user accounts
	DbRecordSet recPersonAccounts;
	LoadPersonEmailInAccounts(Ret_pStatus, nPersonID, recPersonAccounts);
	if(Ret_pStatus.IsOK())
	{
		int nRowIdx = FindEmailInAccountIdx(strEmail, recPersonAccounts);
		if(nRowIdx >= 0)
		{
			//remove account
			recPersonAccounts.deleteRow(nRowIdx);

			//save user accounts
			SavePersonEmailInAccounts(Ret_pStatus, nPersonID, recPersonAccounts);
		}
		else{
			//not found
			Ret_pStatus.setError(1, QObject::tr("No such account!"));		
		}
	}

	LogOut(strSession);
}


//Warning nEmailID = BEM_ID if using CENT_ID then adjust this procedure...
void Service_WebMWCloudService::DeleteEmail(Status &Ret_pStatus, QString strUsername, QString strConce, QString strAuthToken, int nEmailID)
{
	QString strSession = LogOn(strUsername,strConce,strAuthToken);
	if (strSession.isEmpty())
	{
		Ret_pStatus.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_WRONG_PASS_USER);
		return;
	}

	//RetOut_lstIDs.Dump();
	Ret_pStatus.setError(0);

	//Make local query, can fail if connection reservation fails.
	DbSqlQuery query(Ret_pStatus,GetDbManager());
	if(!Ret_pStatus.IsOK()) return;
	
	//delete rows in chunks
	QString strSQL = "SELECT BEM_COMM_ENTITY_ID FROM BUS_EMAIL WHERE BEM_ID="+QString::number(nEmailID);
	query.Execute(Ret_pStatus, strSQL);
	if(!Ret_pStatus.IsOK()) return;

	int nCentID=-1;
	if (query.next())
	{
		nCentID=query.value(0).toInt();
	}

	strSQL = "DELETE FROM CE_COMM_ENTITY WHERE CENT_ID="+QString::number(nCentID);
	query.Execute(Ret_pStatus, strSQL);

	LogOut(strSession);
}


void Service_WebMWCloudService::DeleteMultipleEmails(Status &Ret_pStatus,  QString strUsername, QString strConce, QString strAuthToken, QString strEmailIDs)
{

	QString strSession = LogOn(strUsername,strConce,strAuthToken);
	if (strSession.isEmpty())
	{
		Ret_pStatus.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_WRONG_PASS_USER);
		return;
	}

	//RetOut_lstIDs.Dump();
	Ret_pStatus.setError(0);

	//Make local query, can fail if connection reservation fails.
	DbSqlQuery query(Ret_pStatus,GetDbManager());
	if(!Ret_pStatus.IsOK()) return;

	strEmailIDs = strEmailIDs.trimmed();
	if (strEmailIDs.right(1)==",")
		strEmailIDs.chop(1);

	//delete rows in chunks
	QString strSQL = "SELECT BEM_COMM_ENTITY_ID FROM BUS_EMAIL WHERE BEM_ID IN ("+strEmailIDs+")";
	query.Execute(Ret_pStatus, strSQL);
	if(!Ret_pStatus.IsOK()) return;

	QString strCentIDs;
	int nCentID=-1;
	while (query.next())
	{
		strCentIDs+=query.value(0).toString()+",";
	}
	if (strCentIDs.isEmpty())
		return;

	strCentIDs.chop(1);

	strSQL = "DELETE FROM CE_COMM_ENTITY WHERE CENT_ID IN ("+strCentIDs+")";
	query.Execute(Ret_pStatus, strSQL);

	LogOut(strSession);

}

void Service_WebMWCloudService::GetEmailAddressBlackList(Status &Ret_pStatus, QString strUsername, QString strConce, QString strAuthToken, QString &RetOut_strList)
{
	QString strSession = LogOn(strUsername,strConce,strAuthToken);
	if (strSession.isEmpty())
	{
		Ret_pStatus.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_WRONG_PASS_USER);
		return;
	}
	int nPersonID=g_UserSessionManager->GetPersonID();

	Ret_pStatus.setError(0);

	//define list to receive the blacklist
	DbRecordSet lstData;
	lstData.addColumn(QVariant::String, "LIST");
	lstData.addRow();

	LoadPersonSettingValue(Ret_pStatus, nPersonID, lstData, EMAIL_IN_SETTINGS, EMAIL_IN_SETTINGS_BLACKLIST, "LIST",	"BOUS_VALUE_STRING");
	if(Ret_pStatus.IsOK())
	{	
		RetOut_strList = lstData.getDataRef(0, "LIST").toString();
	}

	LogOut(strSession);
}

void Service_WebMWCloudService::BlackListEmailAddress(Status &Ret_pStatus, QString strUsername, QString strConce, QString strAuthToken, QString strEmail, bool bActivateBlocking)
{
	QString strSession = LogOn(strUsername,strConce,strAuthToken);
	if (strSession.isEmpty())
	{
		Ret_pStatus.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_WRONG_PASS_USER);
		return;
	}
	int nPersonID=g_UserSessionManager->GetPersonID();

	//define list to receive the blacklist
	DbRecordSet lstData;
	lstData.addColumn(QVariant::String, "LIST");
	lstData.addRow();

	LoadPersonSettingValue(Ret_pStatus, nPersonID, lstData, EMAIL_IN_SETTINGS, EMAIL_IN_SETTINGS_BLACKLIST, "LIST",	"BOUS_VALUE_STRING");
	if(Ret_pStatus.IsOK())
	{	
		QString strBlackList = lstData.getDataRef(0, "LIST").toString();

		//list is stored as ; delimited string of emails (with delimiter at the beggining and at the end of the string)
		QString strSearchPattern = ";" + strEmail + ";";

		if(bActivateBlocking){
			//add only if not already found in the list
			if(strBlackList.indexOf(strSearchPattern) < 0){
				if(strBlackList.isEmpty())
					strBlackList += ";";
				strBlackList += strEmail + ";";
			}
		}
		else{
			//remove email if exists in the list
			int nPos = strBlackList.indexOf(strSearchPattern);
			if(nPos >= 0){
				strBlackList.remove(nPos+1, strSearchPattern.length()-1);	//do not touch first ; delimiter
			}
		}

		//write back to DB
		lstData.setData(0, "LIST", strBlackList);
		SavePersonSettingValue(Ret_pStatus, nPersonID, lstData, EMAIL_IN_SETTINGS, EMAIL_IN_SETTINGS_BLACKLIST, "LIST",	"BOUS_VALUE_STRING");
	}
	
	LogOut(strSession);
}

void Service_WebMWCloudService::GeneratePersonCode(Status &Ret_pStatus,QString &strCode)
{
	DbSqlQuery query(Ret_pStatus, GetDbManager());
	if(!Ret_pStatus.IsOK()) return;

	//reset template flag=0, do not ask for template
	QString strSQL = "SELECT FIRST 1 BPER_CODE FROM BUS_PERSON ORDER BY BPER_ID DESC";
	query.Execute(Ret_pStatus, strSQL);
	if(!Ret_pStatus.IsOK()) return;

	if (query.next())
	{
		strCode=query.value(0).toString();
		bool bOk;
		int nCodeID=strCode.toInt(&bOk);
		if (bOk)
		{
			nCodeID++;
			strCode=QString::number(nCodeID);
			if (strCode.length()<8)
			{
				for (int i=0;i<8-strCode.length();i++)
				{
					strCode.prepend("0");
				}
			}
			return;
		}
	}

	strCode="00000001";
}

void Service_WebMWCloudService::GetDefaultPersonRole(Status &Ret_pStatus,int &nRoleID)
{
	//role
	DbRecordSet lstRoles;
	g_BusinessServiceSet->AccessRights->GetRoleList(Ret_pStatus,lstRoles);
	if (!Ret_pStatus.IsOK())return;

	int nRowCount = lstRoles.getRowCount();
	for (int i = 0; i < nRowCount; ++i)
	{
		if (lstRoles.getDataRef(i, "CROL_CODE").toInt() == USER_BE_ROLE) //default role
		{
			nRoleID = lstRoles.getDataRef(i, "CROL_ID").toInt();
			return;
		}
	}

	nRoleID=-1;
}

int Service_WebMWCloudService::GetPersonID(Status &Ret_pStatus, QString strUserEmail)
{
	DbSqlQuery query(Ret_pStatus, GetDbManager());
	if(!Ret_pStatus.IsOK()) return -1;

	int nPersonID;

	//reset template flag=0, do not ask for template
	QString strSQL = "SELECT CUSR_PERSON_ID FROM CORE_USER WHERE CUSR_USERNAME='"+strUserEmail+"'";
	query.Execute(Ret_pStatus, strSQL);
	if(!Ret_pStatus.IsOK()) return -1;

	if (query.next())
		nPersonID=query.value(0).toInt();
	else
		nPersonID=-1;

	return nPersonID;
}

void Service_WebMWCloudService::SavePersonEmailInAccounts(Status &Ret_pStatus, int nPersonID, DbRecordSet &lstPersonAccData)
{
	DbSqlQuery query(Ret_pStatus, g_BusinessServiceSet->GetDbManager());
	if(!Ret_pStatus.IsOK()) return;

	QByteArray binPersonSettings = XmlUtil::ConvertRecordSet2ByteArray_Fast(lstPersonAccData);
	QString strSQL = QString("UPDATE BUS_OPT_SETTINGS SET BOUS_VALUE_BYTE=? WHERE BOUS_SETTING_SET_ID=%1 AND BOUS_SETTING_ID=%2 AND BOUS_PERSON_ID=%3").arg(EMAIL_IN_SETTINGS).arg(EMAIL_IN_SETTINGS_ACCOUNTS).arg(nPersonID);
	query.Prepare(Ret_pStatus, strSQL);
	if (!Ret_pStatus.IsOK()) return;
	query.bindValue(0, binPersonSettings);
	query.ExecutePrepared(Ret_pStatus);
}

//load all email in accounts for a single person
void Service_WebMWCloudService::LoadPersonEmailInAccounts(Status &Ret_pStatus, int nPersonID, DbRecordSet &lstPersonAccData)
{
	//fetch all data for all "email in" user settings
	DbSqlQuery query(Ret_pStatus, g_BusinessServiceSet->GetDbManager());
	if(!Ret_pStatus.IsOK()) return;
	QString strSQL = QString("SELECT BOUS_VALUE_BYTE FROM BUS_OPT_SETTINGS WHERE BOUS_SETTING_SET_ID=%1 AND BOUS_SETTING_ID=%2 AND BOUS_PERSON_ID=%3").arg(EMAIL_IN_SETTINGS).arg(EMAIL_IN_SETTINGS_ACCOUNTS).arg(nPersonID);
	query.Execute(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK()) return;
	DbRecordSet data;
	query.FetchData(data);

	//for each person (should be only one)
	int nPersons = data.getRowCount();
	Q_ASSERT(nPersons <= 1);
	for(int x=0; x<nPersons; x++)
	{
		QByteArray binPersonSettings;
		data.getData(x, 0, binPersonSettings);
		if (binPersonSettings.size()>0)
		{
			DbRecordSet dataPerson = XmlUtil::ConvertByteArray2RecordSet_Fast(binPersonSettings);
			if (dataPerson.getRowCount()>0){
				//dataPerson.Dump();
				if (lstPersonAccData.getColumnCount()==0)
					lstPersonAccData.copyDefinition(dataPerson);
				lstPersonAccData.merge(dataPerson);
			}
		}
	}	
	//lstPersonAccData.Dump();
}

int Service_WebMWCloudService::FindEmailInAccountIdx(const QString &strEmail, DbRecordSet &lstPersonAccData)
{
	int nResIdx = -1;
	int nCount = lstPersonAccData.getRowCount();
	for(int i=0; i<nCount; i++){
		QString strCurEmail;
		lstPersonAccData.getData(i, "POP3_EMAIL", strCurEmail);
		if(strCurEmail == strEmail){
			nResIdx = i;
			break;
		}
	}
	return nResIdx;
}

void Service_WebMWCloudService::AddUserEmailAccountIn( Status &Ret_pStatus, QString strUsername, QString strConce, QString strAuthToken, QString strEmail, QString strLogin, QString strPassword, QString strServer, int nPort, bool nIsImap, int nConnectionType, bool bAccountActive, int nDeleteEmailsAfterNoDays)
{
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("AddUserEmailAccountIn started, user %1 tries to add email account %2").arg(strUsername).arg(strEmail));

	QString strSession = LogOn(strUsername,strConce,strAuthToken);
	if (strSession.isEmpty())
	{
		Ret_pStatus.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_WRONG_PASS_USER);
		return;
	}
	int nPersonID=g_UserSessionManager->GetPersonID();

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("AddUserEmailAccountIn authenticated, user %1 tries to add email account %2").arg(strUsername).arg(strEmail));

	//load user accounts
	DbRecordSet recPersonAccounts;
	LoadPersonEmailInAccounts(Ret_pStatus, nPersonID, recPersonAccounts);
	if(Ret_pStatus.IsOK())
	{
		if(recPersonAccounts.getColumnCount() < 1){
			//ensure that the recordset is defined	
			recPersonAccounts.addColumn(QVariant::String, "POP3_EMAIL");
			recPersonAccounts.addColumn(QVariant::String, "POP3_HOST");
			recPersonAccounts.addColumn(QVariant::String, "POP3_PORT");
			recPersonAccounts.addColumn(QVariant::String, "POP3_USER");
			recPersonAccounts.addColumn(QVariant::String, "POP3_PASS");
			recPersonAccounts.addColumn(QVariant::Int, "POP3_USE_SSL");
			recPersonAccounts.addColumn(QVariant::Int, "POP3_KNOWN_CONTACT_ONLY");
			recPersonAccounts.addColumn(QVariant::Int, "POP3_CHECK_EVERY_MIN");
			recPersonAccounts.addColumn(QVariant::DateTime, "POP3_LAST_CHECK");
			recPersonAccounts.addColumn(QVariant::DateTime, "POP3_LAST_MESSAGE");
			recPersonAccounts.addColumn(QVariant::Int, "POP3_SERVER_TYPE");
			recPersonAccounts.addColumn(QVariant::Int, "POP3_ACCOUNT_ACTIVE");
			recPersonAccounts.addColumn(QVariant::Int, "POP3_DEL_EMAIL_AFTER_DAYS");
			recPersonAccounts.addColumn(QVariant::Int, "POP3_FULL_SCAN_MODE");	
			recPersonAccounts.addColumn(QVariant::String, "POP3_ACC_UID");		//unique ID for this account (needs to be unique only locally within a set of accounts for a single person)
			recPersonAccounts.addColumn(QVariant::String, "ERROR_EMAIL_UID");	//UID that failed last time we did import
			recPersonAccounts.addColumn(QVariant::String, "ACC_ERROR_MESSAGES");	//log with recent issues with this account
		}
		else{
			//some of these may not exist, not present since beginning
			if(recPersonAccounts.getColumnIdx("POP3_SERVER_TYPE")<0)
				recPersonAccounts.addColumn(QVariant::Int, "POP3_SERVER_TYPE");
			if(recPersonAccounts.getColumnIdx("POP3_KNOWN_CONTACT_ONLY")<0)	
				recPersonAccounts.addColumn(QVariant::Int, "POP3_KNOWN_CONTACT_ONLY");
			if(recPersonAccounts.getColumnIdx("POP3_ACCOUNT_ACTIVE")<0)	
				recPersonAccounts.addColumn(QVariant::Int, "POP3_ACCOUNT_ACTIVE");
			if(recPersonAccounts.getColumnIdx("POP3_DEL_EMAIL_AFTER_DAYS")<0)	
				recPersonAccounts.addColumn(QVariant::Int, "POP3_DEL_EMAIL_AFTER_DAYS");
			if(recPersonAccounts.getColumnIdx("POP3_FULL_SCAN_MODE")<0)	
				recPersonAccounts.addColumn(QVariant::Int, "POP3_FULL_SCAN_MODE");
			if(recPersonAccounts.getColumnIdx("POP3_ACC_UID")<0)
				recPersonAccounts.addColumn(QVariant::String, "POP3_ACC_UID");
			if(recPersonAccounts.getColumnIdx("ERROR_EMAIL_UID")<0)
				recPersonAccounts.addColumn(QVariant::String, "ERROR_EMAIL_UID");	//UID that failed last time we did import
			if(recPersonAccounts.getColumnIdx("ACC_ERROR_MESSAGES")<0)
				recPersonAccounts.addColumn(QVariant::String, "ACC_ERROR_MESSAGES");	//log with recent issues with this account
		}

		//if existing row exists, overwrite it (match by email)
		bool bNewRow = false;
		int nRow = FindEmailInAccountIdx(strEmail, recPersonAccounts);
		if(nRow < 0){
			//add new row
			bNewRow = true;
			recPersonAccounts.addRow();
			nRow = recPersonAccounts.getRowCount()-1;
		}

		//fill in the data
		recPersonAccounts.setData(nRow, "POP3_EMAIL", strEmail);
		recPersonAccounts.setData(nRow, "POP3_HOST",  strServer);
		recPersonAccounts.setData(nRow, "POP3_PORT",  QString::number(nPort));
		recPersonAccounts.setData(nRow, "POP3_USER",  strLogin);
		recPersonAccounts.setData(nRow, "POP3_PASS",  strPassword);
		recPersonAccounts.setData(nRow, "POP3_SERVER_TYPE",  nIsImap ? EML_TYPE_IMAP : EML_TYPE_POP3);
		recPersonAccounts.setData(nRow, "POP3_USE_SSL",  nConnectionType+1);	//+1 maps input to internal flags, Input: 0=plain 1= start TLS 2= TLS/SSL, Output=+1
		recPersonAccounts.setData(nRow, "POP3_ACCOUNT_ACTIVE",  (int)(bAccountActive?1:0));
		recPersonAccounts.setData(nRow, "POP3_DEL_EMAIL_AFTER_DAYS",  nDeleteEmailsAfterNoDays);
		if(bNewRow){
			bool bIsGoogle = strEmail.endsWith("google.com") || strEmail.endsWith("gmail.com");
			recPersonAccounts.setData(nRow, "POP3_CHECK_EVERY_MIN",  bIsGoogle ? 7 : 12);	//default, only if new row
		}
		if(bNewRow)
			recPersonAccounts.setData(nRow, "POP3_KNOWN_CONTACT_ONLY",  0);	//default, only if new row

		//calculate when to start importing messages from (to skip importing old messages)
		QDateTime dtStart = QDateTime::currentDateTime();
		if(nDeleteEmailsAfterNoDays > 0)
			dtStart = dtStart.addDays(-nDeleteEmailsAfterNoDays);

		recPersonAccounts.setData(nRow, "POP3_LAST_MESSAGE",dtStart);	
		
		if(bNewRow) {
			QString strUID=EmailImportThread::GenerateAccUID();
			recPersonAccounts.setData(nRow, "POP3_ACC_UID", strUID);
		}
		
		//save user accounts
		SavePersonEmailInAccounts(Ret_pStatus, nPersonID, recPersonAccounts);
	}

	if (!Ret_pStatus.IsOK())
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("AddUserEmailAccountIn error: %1 user: %2").arg(Ret_pStatus.getErrorText()).arg(strUsername));
	else
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("AddUserEmailAccountIn success: %1 ").arg(strUsername));

	LogOut(strSession);
}

void Service_WebMWCloudService::AddUserEmailAccountOut( Status &Ret_pStatus, QString strUsername, QString strConce, QString strAuthToken, QString strEmail, QString strLogin, QString strPassword, QString strServer, int nPort, int nConnectionType, bool bAuthenticationRequired)
{
	QString strSession = LogOn(strUsername,strConce,strAuthToken);
	if (strSession.isEmpty())
	{
		Ret_pStatus.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_WRONG_PASS_USER);
		return;
	}
	int nPersonID=g_UserSessionManager->GetPersonID();

	//does the job
	SetPersonSmtpSetting(Ret_pStatus, nPersonID, strEmail, strLogin, strPassword, strServer, nPort, nConnectionType, bAuthenticationRequired);

	LogOut(strSession);
}


//check user credentials
//compare: strEmail+conce+SECRET_SPICE_PASSWORD+clientpass == strAuthToken
QString Service_WebMWCloudService::LogOn(QString strEmail, QString strConce, QString strAuthToken)
{
	Status pStatus;
	QString strSession;
	int nPersID,nContactID;
	g_BusinessServiceSet->WebServer->Login(pStatus,strSession,nContactID,nPersID,strEmail,strAuthToken,strConce);
	if(!pStatus.IsOK()) return "";
	
	return strSession;
}

void Service_WebMWCloudService::LogOut( QString strSession )
{
	Status pStatus;
	g_BusinessServiceSet->WebServer->Logout(pStatus,strSession);
}

void Service_WebMWCloudService::LoadPersonSettingValue(Status &Ret_pStatus, int nPersonID, DbRecordSet &lstData, int nValueSet, int nValueCode, const QString &strResFieldName, const QString &strSqlField)
{
	//fetch all data for all "email in" user settings
	DbSqlQuery query(Ret_pStatus, g_BusinessServiceSet->GetDbManager());
	if(!Ret_pStatus.IsOK()) return;
	QString strSQL = QString("SELECT %1 FROM BUS_OPT_SETTINGS WHERE BOUS_SETTING_SET_ID=%2 AND BOUS_SETTING_ID=%3 AND BOUS_PERSON_ID=%4").arg(strSqlField).arg(nValueSet).arg(nValueCode).arg(nPersonID);
	query.Execute(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK()) return;
	DbRecordSet data;
	query.FetchData(data);

	//for each person (should be only one)
	int nPersons = data.getRowCount();
	Q_ASSERT(nPersons <= 1);
	for(int x=0; x<nPersons; x++)
	{
		//store data into the 1st row
		if(lstData.getRowCount() > 0 && lstData.getColumnIdx(strResFieldName) >= 0){
			lstData.setData(0, strResFieldName, data.getDataRef(0, 0));
		}
		else{
			Q_ASSERT(false);
		}
	}
#ifdef _DEBUG
	data.Dump();
#endif
}

void Service_WebMWCloudService::SavePersonSettingValue(Status &Ret_pStatus, int nPersonID, DbRecordSet &lstData, int nValueSet, int nValueCode, const QString &strResFieldName, const QString &strSqlField)
{
	//fetch all data for all "email in" user settings
	DbSqlQuery query(Ret_pStatus, g_BusinessServiceSet->GetDbManager());
	if(!Ret_pStatus.IsOK()) return;
	QString strSQL = QString("UPDATE BUS_OPT_SETTINGS SET %1=? WHERE BOUS_SETTING_SET_ID=%1 AND BOUS_SETTING_ID=%2 AND BOUS_PERSON_ID=%3").arg(strSqlField).arg(nValueSet).arg(nValueCode).arg(nPersonID);
	query.Prepare(Ret_pStatus, strSQL);
	if (!Ret_pStatus.IsOK()) return;
	query.bindValue(0, lstData.getDataRef(0, strResFieldName));
	query.ExecutePrepared(Ret_pStatus);
}

void Service_WebMWCloudService::LoadPersonSmtpSetting(Status &Ret_pStatus, int nPersonID, DbRecordSet &lstPersonAccData)
{
	//define list to store the set
	lstPersonAccData.destroy();
	lstPersonAccData.addColumn(QVariant::String, "EMAIL_SETTINGS_SMTP_NAME");
	lstPersonAccData.addColumn(QVariant::Int,	 "EMAIL_SETTINGS_SMTP_PORT");
	lstPersonAccData.addColumn(QVariant::String, "EMAIL_SETTINGS_USER_NAME");
	lstPersonAccData.addColumn(QVariant::String, "EMAIL_SETTINGS_USER_EMAIL");
	lstPersonAccData.addColumn(QVariant::String, "EMAIL_SETTINGS_SMTP_ACC_NAME");
	lstPersonAccData.addColumn(QVariant::String, "EMAIL_SETTINGS_SMTP_ACC_PASS");
	lstPersonAccData.addColumn(QVariant::Int,	 "EMAIL_SETTINGS_USE_AUTH");
	lstPersonAccData.addColumn(QVariant::String, "EMAIL_SETTINGS_SEND_COPY_CC");
	lstPersonAccData.addColumn(QVariant::String, "EMAIL_SETTINGS_SEND_COPY_BCC");
	lstPersonAccData.addColumn(QVariant::Int,	 "EMAIL_SETTINGS_USE_SSL");
	lstPersonAccData.addRow();
	
	//fetch each of these fields one by one (one query per value, these are NOT stored as a single binary blob like POP3/IMAP!!!)
	LoadPersonSettingValue(Ret_pStatus, nPersonID, lstPersonAccData, EMAIL_SETTINGS, EMAIL_SETTINGS_SMTP_NAME, "EMAIL_SETTINGS_SMTP_NAME",			"BOUS_VALUE_STRING");
	LoadPersonSettingValue(Ret_pStatus, nPersonID, lstPersonAccData, EMAIL_SETTINGS, EMAIL_SETTINGS_SMTP_PORT, "EMAIL_SETTINGS_SMTP_PORT",			"BOUS_VALUE");
	LoadPersonSettingValue(Ret_pStatus, nPersonID, lstPersonAccData, EMAIL_SETTINGS, EMAIL_SETTINGS_USER_NAME, "EMAIL_SETTINGS_USER_NAME",			"BOUS_VALUE_STRING");
	LoadPersonSettingValue(Ret_pStatus, nPersonID, lstPersonAccData, EMAIL_SETTINGS, EMAIL_SETTINGS_USER_EMAIL, "EMAIL_SETTINGS_USER_EMAIL",		"BOUS_VALUE_STRING");
	LoadPersonSettingValue(Ret_pStatus, nPersonID, lstPersonAccData, EMAIL_SETTINGS, EMAIL_SETTINGS_SMTP_ACC_NAME, "EMAIL_SETTINGS_SMTP_ACC_NAME",	"BOUS_VALUE_STRING");
	LoadPersonSettingValue(Ret_pStatus, nPersonID, lstPersonAccData, EMAIL_SETTINGS, EMAIL_SETTINGS_SMTP_ACC_PASS, "EMAIL_SETTINGS_SMTP_ACC_PASS",	"BOUS_VALUE_STRING");
	LoadPersonSettingValue(Ret_pStatus, nPersonID, lstPersonAccData, EMAIL_SETTINGS, EMAIL_SETTINGS_USE_AUTH, "EMAIL_SETTINGS_USE_AUTH",			"BOUS_VALUE");
	LoadPersonSettingValue(Ret_pStatus, nPersonID, lstPersonAccData, EMAIL_SETTINGS, EMAIL_SETTINGS_SEND_COPY_CC, "EMAIL_SETTINGS_SEND_COPY_CC",	"BOUS_VALUE_STRING");
	LoadPersonSettingValue(Ret_pStatus, nPersonID, lstPersonAccData, EMAIL_SETTINGS, EMAIL_SETTINGS_SEND_COPY_BCC, "EMAIL_SETTINGS_SEND_COPY_BCC",	"BOUS_VALUE_STRING");
	LoadPersonSettingValue(Ret_pStatus, nPersonID, lstPersonAccData, EMAIL_SETTINGS, EMAIL_SETTINGS_USE_SSL, "EMAIL_SETTINGS_USE_SSL",	"BOUS_VALUE");
	
	//lstPersonAccData.Dump();
}

void Service_WebMWCloudService::SetPersonSmtpSetting(Status &Ret_pStatus, int nPersonID, QString strEmail, QString strLogin, QString strPassword, QString strServer, int nPort, int nConnectionType, bool bAuthenticationRequired)
{
	//load user accounts
	DbRecordSet recPersonAccounts;
	LoadPersonSmtpSetting(Ret_pStatus, nPersonID, recPersonAccounts);
	if(Ret_pStatus.IsOK())
	{
		//fill in the data
		recPersonAccounts.setData(0, "EMAIL_SETTINGS_USER_EMAIL",  strEmail);
		recPersonAccounts.setData(0, "EMAIL_SETTINGS_SMTP_NAME",  strServer);
		recPersonAccounts.setData(0, "EMAIL_SETTINGS_SMTP_PORT",  nPort);
		recPersonAccounts.setData(0, "EMAIL_SETTINGS_SMTP_ACC_NAME",  strLogin);
		recPersonAccounts.setData(0, "EMAIL_SETTINGS_SMTP_ACC_PASS",  strPassword);
		recPersonAccounts.setData(0, "EMAIL_SETTINGS_USE_AUTH",  bAuthenticationRequired ? 1 : 0);
		recPersonAccounts.setData(0, "EMAIL_SETTINGS_USE_SSL",  nConnectionType);
		
		//save SMTP account fields one by one (one query per value, these are NOT stored as a single binary blob like POP3/IMAP!!!)
		SavePersonSettingValue(Ret_pStatus, nPersonID, recPersonAccounts, EMAIL_SETTINGS, EMAIL_SETTINGS_SMTP_NAME, "EMAIL_SETTINGS_SMTP_NAME",			"BOUS_VALUE_STRING");
		SavePersonSettingValue(Ret_pStatus, nPersonID, recPersonAccounts, EMAIL_SETTINGS, EMAIL_SETTINGS_SMTP_PORT, "EMAIL_SETTINGS_SMTP_PORT",			"BOUS_VALUE");
		SavePersonSettingValue(Ret_pStatus, nPersonID, recPersonAccounts, EMAIL_SETTINGS, EMAIL_SETTINGS_USER_NAME, "EMAIL_SETTINGS_USER_NAME",			"BOUS_VALUE_STRING");
		SavePersonSettingValue(Ret_pStatus, nPersonID, recPersonAccounts, EMAIL_SETTINGS, EMAIL_SETTINGS_USER_EMAIL, "EMAIL_SETTINGS_USER_EMAIL",		"BOUS_VALUE_STRING");
		SavePersonSettingValue(Ret_pStatus, nPersonID, recPersonAccounts, EMAIL_SETTINGS, EMAIL_SETTINGS_SMTP_ACC_NAME, "EMAIL_SETTINGS_SMTP_ACC_NAME",	"BOUS_VALUE_STRING");
		SavePersonSettingValue(Ret_pStatus, nPersonID, recPersonAccounts, EMAIL_SETTINGS, EMAIL_SETTINGS_SMTP_ACC_PASS, "EMAIL_SETTINGS_SMTP_ACC_PASS",	"BOUS_VALUE_STRING");
		SavePersonSettingValue(Ret_pStatus, nPersonID, recPersonAccounts, EMAIL_SETTINGS, EMAIL_SETTINGS_USE_AUTH, "EMAIL_SETTINGS_USE_AUTH",			"BOUS_VALUE");
		//SavePersonSettingValue(Ret_pStatus, nPersonID, recPersonAccounts, EMAIL_SETTINGS, EMAIL_SETTINGS_SEND_COPY_CC, "EMAIL_SETTINGS_SEND_COPY_CC",	"BOUS_VALUE_STRING");		//TOFIX not updated with this API
		//SavePersonSettingValue(Ret_pStatus, nPersonID, recPersonAccounts, EMAIL_SETTINGS, EMAIL_SETTINGS_SEND_COPY_BCC, "EMAIL_SETTINGS_SEND_COPY_BCC",	"BOUS_VALUE_STRING");	//TOFIX not updated with this API
		SavePersonSettingValue(Ret_pStatus, nPersonID, recPersonAccounts, EMAIL_SETTINGS, EMAIL_SETTINGS_USE_SSL, "EMAIL_SETTINGS_USE_SSL",				"BOUS_VALUE");
	}
}

void Service_WebMWCloudService::StoreEmail(Status &Ret_pStatus, QString strUsername, QString strConce, QString strAuthToken, int nIsOutgoing, QString strFrom, QString strTo, QString strSubject, QString strMime, QString strFullFrom, QString strCC, QString strBCC)
{
	QString strSession = LogOn(strUsername,strConce,strAuthToken);
	if (strSession.isEmpty())
	{
		Ret_pStatus.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_WRONG_PASS_USER);
		return;
	}
	int nPersonID=g_UserSessionManager->GetPersonID();

	//does the job
	EmailImportThread dummy;
	dummy.ImportMessageFromMime(Ret_pStatus, nPersonID, nIsOutgoing, strFrom, strTo, strSubject, strMime, strFullFrom, strCC, strBCC);

	LogOut(strSession);
}

void Service_WebMWCloudService::GetUsersEmailCnt( Status &Ret_pStatus, DbRecordSet &Ret_lstData )
{
	DbSqlQuery query(Ret_pStatus,GetDbManager());
	if(!Ret_pStatus.IsOK())return;

	
	//select all from BUS_EMAIL by from and to, cnt
	//parse all names 

	QString strSQL="SELECT CUSR_USERNAME, COUNT(BEM_ID) CNT FROM BUS_EMAIL INNER JOIN CE_COMM_ENTITY ON BEM_COMM_ENTITY_ID = CENT_ID INNER JOIN CORE_USER ON CUSR_PERSON_ID = CENT_OWNER_ID WHERE CENT_OWNER_ID IS NOT NULL GROUP BY CUSR_USERNAME";
	query.Execute(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK())
			return;
	
	query.FetchData(Ret_lstData);

	//Ret_lstData.Dump();
}

void Service_WebMWCloudService::GetUnreadEmailsCount(Status &Ret_pStatus, QString strUsername, QString strConce, QString strAuthToken, int &Ret_nCount)
{
	Ret_nCount=0;
	QString strSession = LogOn(strUsername,strConce,strAuthToken);
	if (strSession.isEmpty())
	{
		Ret_pStatus.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_WRONG_PASS_USER);
		return;
	}
	int nPersonID=g_UserSessionManager->GetPersonID();

	//RetOut_lstIDs.Dump();
	Ret_pStatus.setError(0);

	//Make local query, can fail if connection reservation fails.
	DbSqlQuery query(Ret_pStatus,GetDbManager());
	if(!Ret_pStatus.IsOK()) return;

	//delete rows in chunks
	QString strSQL="SELECT COUNT(BEM_ID) CNT FROM BUS_EMAIL INNER JOIN CE_COMM_ENTITY ON BEM_COMM_ENTITY_ID = CENT_ID WHERE BEM_UNREAD_FLAG=1 AND CENT_OWNER_ID ="+QString::number(nPersonID);
	query.Execute(Ret_pStatus, strSQL);
	if(!Ret_pStatus.IsOK()) return;

	if (query.next())
	{
		Ret_nCount=query.value(0).toInt();
	}

	LogOut(strSession);
}

void Service_WebMWCloudService::SendUserErrorPushMessage(Status &Ret_pStatus, QString strUsername, QString strEmailFrom, QString strErrorMsg)
{
	int nPersonID = GetPersonID(Ret_pStatus,strUsername);
	if(!Ret_pStatus.IsOK()) return;
	g_AppServer->SendErrorMessage(nPersonID,strErrorMsg,1,strEmailFrom);
}

void Service_WebMWCloudService::RegisterPushNotificationID(Status &Ret_pStatus, QString strUsername, QString strConce, QString strAuthToken, QString strPushToken, int nOSType)
{
	QString strSession = LogOn(strUsername,strConce,strAuthToken);
	if (strSession.isEmpty())
	{
		Ret_pStatus.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_WRONG_PASS_USER);
		return;
	}
	int nPersonID=g_UserSessionManager->GetPersonID();


	if (strPushToken.trimmed().isEmpty())
	{
		Ret_pStatus.setError(1,"Push token is empty!");
		return;
	}

	//when user already has token then skip registration
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_PUSH_TOKEN_PERSON, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	DbRecordSet lstData;
	TableOrm.Read(Ret_pStatus,lstData,QString(" WHERE BPT_PERSON_FK_ID=%1").arg(nPersonID));
	if(!Ret_pStatus.IsOK()) return;
		
	if (lstData.getRowCount()>0)
	{
		int nRow=lstData.find("BPT_PUSH_TOKEN",strPushToken,true);
		if (nRow>=0) //already exists
			return; 
	}

	//MB: 2014-07-28: when token is used by other user, delete it from other user
	TableOrm.GetDbSqlQuery()->Prepare(Ret_pStatus,"SELECT BPT_ID FROM BUS_PUSH_TOKEN_PERSON WHERE BPT_PUSH_TOKEN=?");
	if(!Ret_pStatus.IsOK()) return;
	TableOrm.GetDbSqlQuery()->bindValue(0,strPushToken);
	TableOrm.GetDbSqlQuery()->ExecutePrepared(Ret_pStatus);
	if(!Ret_pStatus.IsOK()) return;
	QList<int> lstIDForDelete;
	if (TableOrm.GetDbSqlQuery()->next())
		lstIDForDelete.append(TableOrm.GetDbSqlQuery()->value(0).toInt());

	if (!lstIDForDelete.isEmpty())
	{
		TableOrm.DeleteFast(Ret_pStatus,lstIDForDelete);
		if(!Ret_pStatus.IsOK()) return;
	}



	lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_PUSH_TOKEN_PERSON));
	lstData.addRow();
	lstData.setData(0,"BPT_PUSH_TOKEN",strPushToken);
	lstData.setData(0,"BPT_OS_TYPE",nOSType);
	lstData.setData(0,"BPT_DAT_LAST_SUCCESS_SEND",QDateTime::currentDateTime());
	lstData.setData(0,"BPT_PERSON_FK_ID",nPersonID);

	TableOrm.Write(Ret_pStatus,lstData);
	if(!Ret_pStatus.IsOK()) return;

	LogOut(strSession);
}

void Service_WebMWCloudService::ActivatePushNotification(Status &Ret_pStatus, QString strUsername, QString strConce, QString strAuthToken, int nEnablePush)
{
	QString strSession = LogOn(strUsername,strConce,strAuthToken);
	if (strSession.isEmpty())
	{
		Ret_pStatus.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_WRONG_PASS_USER);
		return;
	}
	int nPersonID=g_UserSessionManager->GetPersonID();


	DbSimpleOrm TableOrm(Ret_pStatus, BUS_OPT_SETTINGS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	DbRecordSet lstData;
	TableOrm.Read(Ret_pStatus,lstData,QString("WHERE BOUS_SETTING_ID=%1 AND BOUS_PERSON_ID =%2").arg(EMAIL_IN_SETTINGS_PUSH_SENDING_ACTIVE).arg(nPersonID));
	if(!Ret_pStatus.IsOK()) return;

	if (lstData.getRowCount()==0)
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("Service_WebMWCloudService::ActivatePushNotification: Adding row as data list is empty, person_id=%1 ").arg(nPersonID));
		lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_OPT_SETTINGS));
		lstData.addRow();
	}

	lstData.setData(0,"BOUS_SETTING_SET_ID",EMAIL_IN_SETTINGS);
	lstData.setData(0,"BOUS_SETTING_ID",EMAIL_IN_SETTINGS_PUSH_SENDING_ACTIVE);
	lstData.setData(0,"BOUS_SETTING_VISIBLE",1);
	lstData.setData(0,"BOUS_SETTING_TYPE",0);
	lstData.setData(0,"BOUS_PERSON_ID",nPersonID);
	lstData.setData(0,"BOUS_VALUE",nEnablePush);

	TableOrm.Write(Ret_pStatus,lstData);
	if(!Ret_pStatus.IsOK()) 
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("Service_WebMWCloudService::ActivatePushNotification: error: %1, value of ID %2, rows %3, person_id=%4 ").arg(Ret_pStatus.getErrorText()).arg(lstData.getDataRef(0,"BOUS_ID").toInt()).arg(lstData.getRowCount()).arg(nPersonID));
		return;
	}

	LogOut(strSession);
}

void Service_WebMWCloudService::SetNewEmailsPushNotifications( Status &Ret_pStatus, QString strUsername, QString strConce, QString strAuthToken, int nState, int nSilentMode, QString strContacts )
{
	QString strSession = LogOn(strUsername,strConce,strAuthToken);
	if (strSession.isEmpty())
	{
		Ret_pStatus.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_WRONG_PASS_USER);
		return;
	}
	int nPersonID=g_UserSessionManager->GetPersonID();


	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("Service_WebMWCloudService::SetNewEmailsPushNotifications: inc parameters: state: %1, silent: %2, contacts %3, person_id=%4 ").arg(nState).arg(nSilentMode).arg(strContacts).arg(nPersonID));


	DbSimpleOrm TableOrm(Ret_pStatus, BUS_OPT_SETTINGS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	DbRecordSet lstData;
	TableOrm.Read(Ret_pStatus,lstData,QString("WHERE BOUS_SETTING_ID IN(%1,%2,%3) AND BOUS_PERSON_ID =%4").arg(EMAIL_IN_SETTINGS_PUSH_NEW_EMAIL_STATE).arg(EMAIL_IN_SETTINGS_PUSH_NEW_EMAIL_SILENT_MODE).arg(EMAIL_IN_SETTINGS_PUSH_NEW_EMAIL_ALLOWED_EMAIL_LIST).arg(nPersonID));
	if(!Ret_pStatus.IsOK()) return;

	if (lstData.getRowCount()==0)
	{
		lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_OPT_SETTINGS));
	}

	int nRow=lstData.find("BOUS_SETTING_ID",EMAIL_IN_SETTINGS_PUSH_NEW_EMAIL_STATE,true);
	if (nRow>=0)
	{
		lstData.setData(nRow,"BOUS_VALUE",nState);
	}
	else
	{
		lstData.addRow();
		lstData.setData(lstData.getRowCount()-1,"BOUS_SETTING_SET_ID",EMAIL_IN_SETTINGS);
		lstData.setData(lstData.getRowCount()-1,"BOUS_SETTING_ID",EMAIL_IN_SETTINGS_PUSH_NEW_EMAIL_STATE);
		lstData.setData(lstData.getRowCount()-1,"BOUS_SETTING_VISIBLE",1);
		lstData.setData(lstData.getRowCount()-1,"BOUS_SETTING_TYPE",0);
		lstData.setData(lstData.getRowCount()-1,"BOUS_PERSON_ID",nPersonID);
		lstData.setData(lstData.getRowCount()-1,"BOUS_VALUE",nState);
	}


	nRow=lstData.find("BOUS_SETTING_ID",EMAIL_IN_SETTINGS_PUSH_NEW_EMAIL_SILENT_MODE,true);
	if (nRow>=0)
	{
		lstData.setData(nRow,"BOUS_VALUE",nSilentMode);
	}
	else
	{
		lstData.addRow();
		lstData.setData(lstData.getRowCount()-1,"BOUS_SETTING_SET_ID",EMAIL_IN_SETTINGS);
		lstData.setData(lstData.getRowCount()-1,"BOUS_SETTING_ID",EMAIL_IN_SETTINGS_PUSH_NEW_EMAIL_SILENT_MODE);
		lstData.setData(lstData.getRowCount()-1,"BOUS_SETTING_VISIBLE",1);
		lstData.setData(lstData.getRowCount()-1,"BOUS_SETTING_TYPE",0);
		lstData.setData(lstData.getRowCount()-1,"BOUS_VALUE",nSilentMode);
		lstData.setData(lstData.getRowCount()-1,"BOUS_PERSON_ID",nPersonID);
	}

	strContacts=strContacts.toLower();

	nRow=lstData.find("BOUS_SETTING_ID",EMAIL_IN_SETTINGS_PUSH_NEW_EMAIL_ALLOWED_EMAIL_LIST,true);
	if (nRow>=0)
	{
		lstData.setData(nRow,"BOUS_VALUE_STRING",strContacts);
	}
	else
	{
		lstData.addRow();
		lstData.setData(lstData.getRowCount()-1,"BOUS_SETTING_SET_ID",EMAIL_IN_SETTINGS);
		lstData.setData(lstData.getRowCount()-1,"BOUS_SETTING_ID",EMAIL_IN_SETTINGS_PUSH_NEW_EMAIL_ALLOWED_EMAIL_LIST);
		lstData.setData(lstData.getRowCount()-1,"BOUS_SETTING_VISIBLE",1);
		lstData.setData(lstData.getRowCount()-1,"BOUS_SETTING_TYPE",2);
		lstData.setData(lstData.getRowCount()-1,"BOUS_VALUE_STRING",strContacts);
		lstData.setData(lstData.getRowCount()-1,"BOUS_PERSON_ID",nPersonID);
	}
	
	TableOrm.Write(Ret_pStatus,lstData);
	if(!Ret_pStatus.IsOK()) return;


	SaveLastErrorMsgDateTime(Ret_pStatus,"",nPersonID);


	LogOut(strSession);

}

void Service_WebMWCloudService::SetEmailAccountPushNotifications( Status &Ret_pStatus, QString strUsername, QString strConce, QString strAuthToken, QString strEmailAccount,int nEnablePush )
{

	QString strSession = LogOn(strUsername,strConce,strAuthToken);
	if (strSession.isEmpty())
	{
		Ret_pStatus.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_WRONG_PASS_USER);
		return;
	}
	int nPersonID=g_UserSessionManager->GetPersonID();

	strEmailAccount=strEmailAccount.toLower();
	if (strEmailAccount.isEmpty())
	{
		Ret_pStatus.setError(1,"Email account can not be empty!!");
		return;
	}

	DbSimpleOrm TableOrm(Ret_pStatus, BUS_OPT_SETTINGS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	DbRecordSet lstData;
	TableOrm.Read(Ret_pStatus,lstData,QString("WHERE BOUS_SETTING_ID = %1 AND BOUS_PERSON_ID =%2").arg(EMAIL_IN_SETTINGS_PUSH_SENDING_ACTIVE_EMAIL_ACCOUNT_LIST).arg(nPersonID));
	if(!Ret_pStatus.IsOK()) return;

	if (lstData.getRowCount()==0)
	{
		QString strVal=strEmailAccount+","+QString::number(nEnablePush);
		lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_OPT_SETTINGS));
		lstData.addRow();
		lstData.setData(lstData.getRowCount()-1,"BOUS_SETTING_SET_ID",EMAIL_IN_SETTINGS);
		lstData.setData(lstData.getRowCount()-1,"BOUS_SETTING_ID",EMAIL_IN_SETTINGS_PUSH_SENDING_ACTIVE_EMAIL_ACCOUNT_LIST);
		lstData.setData(lstData.getRowCount()-1,"BOUS_SETTING_VISIBLE",1);
		lstData.setData(lstData.getRowCount()-1,"BOUS_SETTING_TYPE",2);
		lstData.setData(lstData.getRowCount()-1,"BOUS_VALUE_STRING",strVal);
		lstData.setData(lstData.getRowCount()-1,"BOUS_PERSON_ID",nPersonID);
	}
	else
	{
		QString strVal=lstData.getDataRef(0,"BOUS_VALUE_STRING").toString();
		if (strVal.isEmpty())
		{
			strVal=strEmailAccount+","+QString::number(nEnablePush);
		}
		else
		{
			//try to find email:
			int nPos=strVal.indexOf(strEmailAccount);
			if (nPos>=0)
			{
				strVal = strVal.replace(strEmailAccount+",0","");
				strVal = strVal.replace(strEmailAccount+",1","");

				QStringList strList=strVal.split(";",QString::SkipEmptyParts);
				if (strList.size()>0)
					strVal=strList.join(";");
			}
			if (!strVal.isEmpty())
				strVal.append(";"+strEmailAccount+","+QString::number(nEnablePush));
			else
				strVal.append(strEmailAccount+","+QString::number(nEnablePush));
		}
		lstData.setData(lstData.getRowCount()-1,"BOUS_VALUE_STRING",strVal);
	}

	TableOrm.Write(Ret_pStatus,lstData);
	if(!Ret_pStatus.IsOK()) return;

	LogOut(strSession);
}

//send email push notification:
void Service_WebMWCloudService::SendPushNotification(Status &Ret_pStatus, QString strUsername, QString strConce, QString strAuthToken, QString strMessage, int nOSType, QString strPushToken, QString strPushSoundName, int nBadgeNumber)
{
	QString strSession = LogOn(strUsername,strConce,strAuthToken);
	if (strSession.isEmpty())
	{
		Ret_pStatus.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_WRONG_PASS_USER);
		return;
	}
	int nPersonID=g_UserSessionManager->GetPersonID();

	if (strPushToken=="0" || strPushToken.isEmpty())
	{
		Ret_pStatus.setError(1,"Failed to send push message, push token is 0 or empty!!");
		return;
	}

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,0,QString("Service_WebMWCloudService::SendPushNotification push token: %1").arg(strPushToken));

	g_AppServer->SendPushNotification(nPersonID,strMessage,strPushToken,nOSType,strPushSoundName,nBadgeNumber);

	LogOut(strSession);
}


void Service_WebMWCloudService::SaveLastErrorMsgDateTime(Status &pStatus,QString strDateErr, int nPersonID)
{
	DbSimpleOrm TableOrm(pStatus, BUS_OPT_SETTINGS, GetDbManager()); 
	if(!pStatus.IsOK()) return;

	DbRecordSet lstData;
	TableOrm.Read(pStatus,lstData,QString("WHERE BOUS_SETTING_ID =%1 AND BOUS_PERSON_ID =%2").arg(EMAIL_IN_SETTINGS_PUSH_ERROR_LAST_DATETIME).arg(nPersonID));
	if(!pStatus.IsOK()) return;

	if (lstData.getRowCount()==0)
	{
		lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_OPT_SETTINGS));
		lstData.addRow();
		lstData.setData(lstData.getRowCount()-1,"BOUS_SETTING_SET_ID",EMAIL_IN_SETTINGS);
		lstData.setData(lstData.getRowCount()-1,"BOUS_SETTING_ID",EMAIL_IN_SETTINGS_PUSH_ERROR_LAST_DATETIME);
		lstData.setData(lstData.getRowCount()-1,"BOUS_SETTING_VISIBLE",1);
		lstData.setData(lstData.getRowCount()-1,"BOUS_SETTING_TYPE",0);
		lstData.setData(lstData.getRowCount()-1,"BOUS_PERSON_ID",nPersonID);
		lstData.setData(lstData.getRowCount()-1,"BOUS_VALUE_STRING",strDateErr);
	}
	else
		lstData.setData(0,"BOUS_VALUE_STRING",strDateErr);

	TableOrm.Write(pStatus,lstData);
}

//update bus email record:
void Service_WebMWCloudService::MoveEmail( Status &Ret_pStatus, QString strUsername, QString strConce, QString strAuthToken, int nEmailID, int nMailbox, QString strDueDate, QString strReminderType )
{
	QString strSession = LogOn(strUsername,strConce,strAuthToken);
	if (strSession.isEmpty())
	{
		Ret_pStatus.setError(StatusCodeSet::ERR_BUS_LOGIN_FAILED_WRONG_PASS_USER);
		return;
	}

	//RetOut_lstIDs.Dump();
	Ret_pStatus.setError(0);

	//Make local query, can fail if connection reservation fails.
	DbSqlQuery query(Ret_pStatus,GetDbManager());
	if(!Ret_pStatus.IsOK()) return;

	QDate datDue=QDate::fromString("yyyyMMdd-hhmm");

	//delete rows in chunks
	QString strSQL = "UPDATE BUS_EMAIL SET BEM_MAILBOX_TYPE = ?, BEM_REMINDER_TYPE=?, BEM_REMINDER_DUE_DATE=? WHERE BEM_ID="+QString::number(nEmailID);
	query.Prepare(Ret_pStatus, strSQL);
	if(!Ret_pStatus.IsOK()) return;

	query.bindValue(0,nMailbox);
	query.bindValue(1,strReminderType);
	query.bindValue(2,datDue);

	query.ExecutePrepared(Ret_pStatus);
	if(!Ret_pStatus.IsOK()) return;

	LogOut(strSession);
}

void Service_WebMWCloudService::EnableUserAccount( Status &Ret_pStatus, QString strUserEmail, bool bEnable )
{
	DbSqlQuery query(Ret_pStatus,GetDbManager());
	if(!Ret_pStatus.IsOK())return;

	QString strSQL="UPDATE CORE_USER SET CUSR_ACTIVE_FLAG=? WHERE CUSR_USERNAME=?";
	query.Prepare(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK())
	{
		return;
	}

	query.bindValue(0,(int)bEnable?1:0);
	query.bindValue(1,strUserEmail);
	query.ExecutePrepared(Ret_pStatus);
}

void Service_WebMWCloudService::DeleteUser( Status &Ret_pStatus, QString strUserEmail )
{
	DbSqlQuery query(Ret_pStatus,GetDbManager());
	if(!Ret_pStatus.IsOK())return;

	QString strSQL="SELECT CUSR_PERSON_ID FROM CORE_USER WHERE CUSR_USERNAME=?";
	query.Prepare(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK())
		return;
	query.bindValue(0,strUserEmail);
	query.ExecutePrepared(Ret_pStatus);

	int nPersonID=0;
	if (query.next())
	{
		nPersonID=query.value(0).toInt();
	}

	if (nPersonID>0)
	{
		QString strSQL="DELETE FROM BUS_PERSON WHERE BPER_ID="+QString::number(nPersonID);
		query.Execute(Ret_pStatus,strSQL);
		if(!Ret_pStatus.IsOK())
			return;
	}
}