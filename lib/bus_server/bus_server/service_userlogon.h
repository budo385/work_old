#ifndef SERVICE_USERLOGON_H
#define SERVICE_USERLOGON_H

#include "bus_interface/bus_interface/interface_userlogon.h"
#include "businessservice.h"

/*!
    \class Service_UserLogon
    \brief Handles user authentication, uses AR, ML & usersession manager
    \ingroup Bus_Services

	Provides methods for client Login/Logout
		
*/

class Service_UserLogon: public Interface_UserLogon, public BusinessService
{
public:

	//Thin:
	void Login(Status& Ret_Status, QString& RetOut_strSessionID,QString strUsername, QByteArray strAuthToken,QByteArray strClientNonce,QString strModuleCode, QString strMLIID="", QString strParentSession="", bool bIsWebLogin=false, int nClientTimeZoneOffsetMinutes=0);		
	void Logout(Status& Ret_Status);
	void IsAlive(Status& Ret_Status);
	void ChangePassword(Status& Ret_Status,QString strUsername, QByteArray strAuthToken,QByteArray strClientNonce,QByteArray newPass,QString strNewUserName);

private:
	//left over from past:
	void Login(Status& Ret_Status, QString strUsername, QString strPassword,QString strModuleCode, QString strMLIID="",QString strParentSession="", int nClientTimeZoneOffsetMinutes=0){};		

};


#endif //SERVICE_USERLOGON_H
