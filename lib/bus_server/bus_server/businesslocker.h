#ifndef BUSINESSLOCKER_H
#define BUSINESSLOCKER_H

#include <QList>
#include "db/db/dbsqlmanager.h"
#include "common/common/status.h"

typedef QList<int> DbLockList;

/*!
	\class BusinessLocker
	\brief Locker class
	\ingroup Bus_Server

	USE:
	- as local object on server side
	- can be used in two modes: direct locking & business locking:
	direct locking is used only as short life lock in server fucntion
	business locking is practically EDIT mode on client GUI
	Business class for locking, same as DbRecordLocker, except it returns detail error on who locked records
*/
class BusinessLocker
{

public:

	BusinessLocker (Status& pStatus, int nTableId, DbSqlManager* pDatabaseManager, bool bCheckParents = false);
	~BusinessLocker();

	//Set to use direct locking (faster, but can only be used on server side, short life locks):
	//void SetDirectLocking(){m_bUseDirectLocking=true;}
	//BT: issue 974, lock direct on FIREBIRD does not work


	//convenient, uses nTableID:
	bool Lock(Status &pStatus, QList<int> &pLstForLocking, QString &pResourceID, DbRecordSet *pLstStatusRows=NULL, bool bAllOrNothing=true, int nTryLockTimeOut=0);
	bool Lock(Status &pStatus , const DbRecordSet &pLstForLocking, QString &pResourceID, DbRecordSet *pLstStatusRows=NULL, bool bAllOrNothing=true, int nTryLockTimeOut=0);
	bool UnLock(Status &pStatus, QString &pResourceID);
	bool UnLock(Status &pStatus, int nTableID, int nRecordID);

	//state own tableid:
	//bool LockRecords(Status& pStatus, int TableID, DbLockList &LockList,QString& strLockedResourceID, DbRecordSet *pLstStatusRows=NULL, bool bAllOrNothing=true);
	bool IsLocked(Status& pStatus, int TableID, DbLockList &LockList);
	int IsLockedByMe(Status& pStatus, int TableID, DbLockList &LockList);

	//returns inside objects for others to use:
	QSqlDatabase* GetDbConnection(){return m_pDb;};



private:
	bool LockRecords(Status& pStatus, int TableID, DbLockList &LockList,QString& strLockedResourceID, DbRecordSet *pLstStatusRows=NULL, bool bAllOrNothing=true, int nTryLockTimeOut=0);
	bool UnlockByResourceID(Status& pStatus, QString& pLockedResourceID);
	bool UnlockByUserSessionID(Status& pStatus, QString& pUserSessionID);
	bool UnlockByRecordID(Status& pStatus, int nTableID, int nRecordID);

	bool LockRecordsInternal(Status& pStatus, int TableID, DbLockList &LockList, QString& strLockedResourceID, DbRecordSet *pLstStatusRows=NULL,bool bAllOrNothing=true);
	bool IsLockedInternal(Status& pStatus, int TableID, DbLockList &LockList);
	DbLockList GetParentLockList(Status& pStatus, int TableID, DbLockList &LockList);
	void FormatStatusRowsLst(DbRecordSet *pLstStatusRows);

	QString GenerateResourceID();



	DbSqlManager	*m_pDbManager;
	QSqlDatabase	*m_pDb;
	QString			m_strUserSession;
	int 			m_intSessionRecordID;
	static quint64	m_nUniqueLockKey;
	bool			m_bKeepConnectionFlag;
	bool			m_bCheckParents;

	void ConvertIsLockedError(Status &pStatus);
	int GetPrimaryColumnNo(const DbRecordSet &pLst);

	int m_nTableID;
	QString m_strPrimaryKey;
	QString m_strTableName;
};

#endif // BUSINESSLOCKER_H

