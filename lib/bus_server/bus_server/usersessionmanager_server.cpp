#include "usersessionmanager_server.h"
#include "common/common/sha256hash.h"
#include "common/common/threadid.h"
#include "db_core/db_core/dbsqltabledefinition.h"
#include "common/common/datahelper.h"

#include "common/common/authenticator.h"

//GLOBAL OBJECT:
#include "loadbalancer.h"
#include "useraccessright_server.h"
#include "common/common/logger.h"

extern LoadBalancer *g_LoadBalancer;
extern UserAccessRight *g_AccessRight;			//global access right tester
extern Logger				g_Logger;
#include "bus_core/bus_core/servercontrolabstract.h"
extern ServerControlAbstract *g_AppServer;

#include "bus_core/bus_core/messagedispatcher.h"
extern MessageDispatcher* g_MessageDispatcher;
#include "privateserviceset.h"
extern PrivateServiceSet* g_PrivateServiceSet; 

/*!
	Constructor: sets init parameters
	\param pServices				system services pointer
	\param nUserSessionTimeOut		in seconds defines inactivity time after user session is expired
	\param nAppServerSessionID		application server session id
	\param strServerIP				IP:port address of app. server
	\param nMaxSessions				defines maximum load of server (maximum sessions)
	\param bAcceptDFOConnections	if true, sessions that are not from this server will be accepted and processed if possible
	\param nAuthTimeout				(seconds)authentication process timeout (time in which client must login or disconnect)
	\param nAuthTries				defines in how many tries user must login
	\param nKickBanPeriod			(seconds) for how long client IP address is banned for accessing server (0 for infinite)
	\param nCheckSessionPeriod		(seconds)time interval in which user session will be checked into database (at any new request)

*/
UserSessionManager_Server::UserSessionManager_Server(SystemServiceSet* pServices,LicenseAccessRightsBase *pModuleLicenseManager,int nUserSessionTimeOut, int nAppServerSessionID, QString strServerIP,int nMaxSessions, bool bAcceptDFOConnections, int nAuthTimeout, int nAuthTries, int nKickBanPeriod,int nCheckSessionPeriod)
:UserSessionManager(pModuleLicenseManager)
{
	m_nUserSessionTimeOut=nUserSessionTimeOut;
	m_nAppServerSessionID=nAppServerSessionID;
	m_nAuthTimeout=nAuthTimeout;
	m_nAuthTries=nAuthTries;
	m_nKickBanPeriod=nKickBanPeriod;
	m_nMaxSessions=nMaxSessions;
	m_Services=pServices;
	m_nCheckSessionTimePeriod=nCheckSessionPeriod;
	m_bAcceptDFOConnections=bAcceptDFOConnections;

}

//destroy dyanmic objects
UserSessionManager_Server::~UserSessionManager_Server()
{

}


//------------------------------------------------------------------------------------------------------
//							CREATE, UPDATE,VALIDATE SESSION METHODS
//------------------------------------------------------------------------------------------------------


/*!
	Creates new system wide unique session id. Inserts in database with app.server_id.
	Called by Login method after successfull Login process!
	Stores in dynamic session list in memory with provided user info (id, group, module, etc..)
	If entry does exists in session list: update status = set OK.

	\param pStatus				if err occur, returned here
	\param strSession			returned session id, if all goes well
	\param rowUserData			row in TVIEW_CORE_USER_SELECTION with user data
	\param strModuleCode		client module code
	\param strMLIID				MLIID

*/
void UserSessionManager_Server::CreateSession(Status &pStatus,QString &strSession,DbRecordSet &rowUserData, QString strModuleCode, QString strMLIID,bool bIsSystemThread,QString strParentSession, int nClientTimeZoneOffsetMinutes)
{

	Sha256Hash Hasher;
	//call Businessobject for creating unique session
	//------------------------------------------------
	int nTries=10;
	int nStoredSessionID;
	int nCurrentThreadID=ThreadIdentificator::GetCurrentThreadID();
	pStatus.setError(0);
	//extract user and person id if exists:
	int nUserID=rowUserData.getDataRef(0,"CUSR_ID").toInt();
	int nPersonID=rowUserData.getDataRef(0,"CUSR_PERSON_ID").toInt();

	bool bSkipLicenseCount=bIsSystemThread;

	//if child: seaarch for session in db, if active and ok, then allow: do not count, mark as not countable
	int nParentSessionID=0;
	if (!strParentSession.isEmpty())
	{
		DbRecordSet recData;
		m_Services->UserSession->ReadSessionBySessionID(pStatus,strParentSession,recData);
		if (recData.getRowCount()==0)
		{
			pStatus.setError(StatusCodeSet::ERR_SYSTEM_PARENT_SESSION_INVALID);
			return;
		}
		if (recData.getDataRef(0,"COUS_ACTIVE_FLAG").toInt()==0)
		{
			pStatus.setError(StatusCodeSet::ERR_SYSTEM_PARENT_SESSION_INVALID);
			return;
		}
		nParentSessionID=recData.getDataRef(0,"COUS_ID").toInt();
		bSkipLicenseCount=true;

		//later on: all same, but: when parent is deleted: remove all childs: search list, remove from memory, remove from db
		//if parent is set inactive: set all childs...
		//?: redirect, reactivate, transfer....
	}



	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Creating session, thread:"+QVariant(nCurrentThreadID).toString());

	//get max license count:
	int nMaxCount=-1;
	if (!bSkipLicenseCount) //do not count if system thread (-1 is unlimited)
	{
		if (m_pModuleLicense)
			nMaxCount=m_pModuleLicense->GetMaxLicenseCount(strModuleCode,strMLIID);
		if(nMaxCount==-1)
		{
			pStatus.setError(StatusCodeSet::ERR_SYSTEM_MOD_LICENSE_NOT_EXISTS); 
			return;
		}
		else if (nMaxCount==0) //issue 1951: 0 users:_ no access
		{
			pStatus.setError(StatusCodeSet::ERR_SYSTEM_MOD_LICENSE_MAX_REACHED);
			return;
		}
	}


	//send maxcount to server: count them, if <= then ok, else, error

	while(nTries>0)
	{
		//create session as thread_id+userid+appserid+datetime:
		QByteArray byteRandomString=QTime::currentTime().toString("hh:mm:ss.zzz").toLatin1()+QVariant(nCurrentThreadID).toString().toLatin1()+QVariant(nUserID).toString().toLatin1()+QVariant(m_nAppServerSessionID).toString().toLatin1();
		strSession=Hasher.GetHash(byteRandomString).toBase64(); //32-50byte length hashed session as base 64 encoded string
		
		//try to insert system wide unique session, if fail iterate again
		//Unique constraint on SESSION Db field assures that no 2 same user session exists in system
		m_Services->UserSession->InsertSession(pStatus,strSession,m_nAppServerSessionID,nMaxCount,strModuleCode,strMLIID,nUserID,nStoredSessionID,nParentSessionID);
		if(pStatus.IsOK()) break;
		nTries--;
	}
	//means that in 10 tries we didn't succeed in creating unique Session ID, whata bad luck!
	if(nTries==0 && !pStatus.IsOK())
	{
		//pStatus.setErrorDetails(pStatus.getErrorText());//store DB error in details:
		//pStatus.setError(StatusCodeSet::ERR_SYSTEM_SESSION_CREATION_FAILED); 
		return;
	}


	//store all in list
	//------------------------------------------------
	QWriteLocker locker(&m_SessionListRWLock);

	//insert or update
	UserSessionData newSession(nCurrentThreadID,STATE_OK);
	if( m_SessionList.contains(nCurrentThreadID))//if exists, just copy IP,socket
	{
		newSession.nSocketID=m_SessionList.value(nCurrentThreadID).nSocketID;
		newSession.strClientIP=m_SessionList.value(nCurrentThreadID).strClientIP;
	}
	else
	{
		//when creating session manually then pass it:
		newSession.nSocketID=0;
		newSession.strClientIP="";
		//Q_ASSERT_X(false,"UserSessionManager","Session not properly initialized");
		//pStatus.setError(StatusCodeSet::ERR_SYSTEM_SESSION_CREATION_FAILED); 
		//return;

	}

	newSession.bStored=true;
	newSession.nStoredSessionID=nStoredSessionID;
	newSession.strSession=strSession;
	newSession.nUserID=nUserID;
	newSession.nPersonID=nPersonID;
	newSession.strModuleCode=strModuleCode;
	newSession.strMLIID=strMLIID;
	newSession.rowUserData=rowUserData;
	newSession.bIsSystemThread=bIsSystemThread;
	newSession.nThreadID=nCurrentThreadID;
	newSession.strParentSession=strParentSession;
	int nServerOffset=DataHelper::GetTimeZoneOffsetUTC();
	nClientTimeZoneOffsetMinutes = nClientTimeZoneOffsetMinutes  - nServerOffset;
	newSession.nClientTimeZoneOffsetMinutes=nClientTimeZoneOffsetMinutes;

	//store changes back into list: (NO DUPLICATE nCurrentThreadID must exists!!)
	m_SessionList[nCurrentThreadID]=newSession;
	

	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_USER_SESSIONS_CURRENT,QVariant(m_SessionList.size()).toString());
	
	//init other global caches, if error rollback session
	//Status err;
	//BuildUserContextData(pStatus,&newSession);
	//if(!pStatus.IsOK())
	//	DeleteSession(err,strSession);

}

//Same as CreateSession but:
//coz WEB can open multiple socket to server with same session: when socket not found by thread, try to find by authtoken (it will be unique)
//if found, copy all data from session row, create new

void UserSessionManager_Server::CreateSessionForWebService(Status &pStatus,QString &strSession,DbRecordSet rowUserData, int nClientTimeZoneOffsetMinutes)
{
	HTTPContext ctx=g_AppServer->GetThreadContext(ThreadIdentificator::GetCurrentThreadID());
	if (ctx.m_nSocketID==-1)
	{
		pStatus.setError(1,"Invalid connection context");
		return;
	}

	//create new session:
	Sha256Hash Hasher;
	pStatus.setError(0);
	int nTries=10;
	int nStoredSessionID;
	int nUserID=rowUserData.getDataRef(0,"CUSR_ID").toInt();
	int nPersonID=rowUserData.getDataRef(0,"CUSR_PERSON_ID").toInt();
	int nCurrentThreadID=ThreadIdentificator::GetCurrentThreadID();
	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Creating session, thread:"+QVariant(nCurrentThreadID).toString());

	//insert or update
	UserSessionData newSession(nCurrentThreadID,0,ctx.m_strPeerIP,ctx.m_nSocketID);

	QString strModuleCode="SC-BE";
	QString strMLIID="";

	//get max license count:
	int nMaxCount=-1;
	if (m_pModuleLicense)
		nMaxCount=m_pModuleLicense->GetMaxLicenseCount(strModuleCode,strMLIID);
	if(nMaxCount==-1)
	{
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_MOD_LICENSE_NOT_EXISTS); 
		return;
	}
	else if (nMaxCount==0) //issue 1951: 0 users:_ no access
	{
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_MOD_LICENSE_MAX_REACHED);
		return;
	}
	

	//create unique session:
	bool bSessionCreated=false;
	while(nTries>0)
	{
		QByteArray byteRandomString=QTime::currentTime().toString("hh:mm:ss.zzz").toLatin1()+QVariant(nCurrentThreadID).toString().toLatin1()+QVariant(ctx.m_nSocketID).toString().toLatin1()+QVariant(ctx.m_strPeerIP).toString().toLatin1();
		strSession=Hasher.GetHash(byteRandomString).toBase64(); //32-50byte length hashed session as base 64 encoded string

		m_Services->UserSession->InsertSession(pStatus,strSession,m_nAppServerSessionID,nMaxCount,strModuleCode,strMLIID,nUserID,nStoredSessionID);
		if(pStatus.IsOK()) 
			break;
		nTries--;
	}
	if(nTries==0 && !pStatus.IsOK())
	{
		return;
	}

	//store all in list
	//------------------------------------------------
	QWriteLocker locker(&m_SessionListRWLock);

	newSession.nSocketID=ctx.m_nSocketID;
	newSession.strClientIP=ctx.m_strPeerIP;
	newSession.bStored=true;
	newSession.nStoredSessionID=nStoredSessionID;
	newSession.strSession=strSession;
	newSession.nUserID=nUserID;
	newSession.nPersonID=nPersonID;
	newSession.strModuleCode=strModuleCode;
	newSession.strMLIID=strMLIID;
	newSession.rowUserData=rowUserData;
	newSession.bIsSystemThread=false;
	newSession.nThreadID=nCurrentThreadID;
	newSession.bIsWebSession=true;
	int nServerOffset=DataHelper::GetTimeZoneOffsetUTC();
	nClientTimeZoneOffsetMinutes = nClientTimeZoneOffsetMinutes  - nServerOffset;
	newSession.nClientTimeZoneOffsetMinutes=nClientTimeZoneOffsetMinutes;

	m_SessionList[nCurrentThreadID]=newSession;

	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_USER_SESSIONS_CURRENT,QVariant(m_SessionList.size()).toString());



}




/*!
	Called by server Login method for DFO process. When client already has session and it passed authentication.
	Modus Operandi:
	1. ThreadID must exists in list or return FATAL error (recover: set expired, return error)!
	2. When Find ID, compare Sessions inc is nonEmpty:
	2.1 NonEmpty in list, -> FATAL error (recover: set expired, return error)!
	2.2 Empty in list: 
	2.2.1 - Check if inc session exists in list with another ID = set expired->clear session to "", delete it!!!!!
	2.2.2 - If (stored=false), find in session table, if found check app server is alive, 
			  if this is our app. server then nothing DO, else update to new app_id, set to true, else just insert new
	2.2.3 - update store new session_id in list, clear status.
	If app.server is alive or another Err, return FATAL ERROR, set expired


	\param pStatus				if err occur, returned here
	\param strSession			non empty old client session (must be verified)
	\param rowUserData			row in TVIEW_CORE_USER_SELECTION with user data
	\param strModuleCode		client module code
	\param strMLIID				MLIID

*/
void UserSessionManager_Server::UpdateSession(Status &pStatus,QString strSession,DbRecordSet &rowUserData, QString strModuleCode, QString strMLIID)
{
	UserSessionData SessionRow;
	int nCurrentThreadID=ThreadIdentificator::GetCurrentThreadID();
	int nStoredSessionID;
	pStatus.setError(0);

	//extract user and person id if exists:
	int nUserID=rowUserData.getDataRef(0,"CUSR_ID").toInt();
	int nPersonID=rowUserData.getDataRef(0,"CUSR_PERSON_ID").toInt();


	//take write lock
	QWriteLocker locker(&m_SessionListRWLock);

	//If list doesnt have ThreadID, FATAL error
	if(!m_SessionList.contains(nCurrentThreadID))
	{
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_SESSION_CREATION_FAILED); return ;
	}

	//fetch session data:
	SessionRow=m_SessionList.value(nCurrentThreadID);

	//if NonEmpty-FATAL
	if(!SessionRow.strSession.isEmpty())
	{
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_SESSION_CREATION_FAILED); return ;
	}

	//check if old session already exists in list,if so deleted it: means client was reconected
	int nOldSessionEntry=FindSession(strSession);
	if(nOldSessionEntry!=-1)
	{
		m_SessionList.remove(nOldSessionEntry);
		/*
		UserSessionData OldSessionRow=m_SessionList.value(nOldSessionEntry);
		OldSessionRow.nRequestStatus=STATE_EXPIRED;
		OldSessionRow.strSession="";
		m_SessionList[nOldSessionEntry]=OldSessionRow;
		*/
	}

	//get max license count:
	int nMaxCount=-1;
	if (m_pModuleLicense)
		nMaxCount=m_pModuleLicense->GetMaxLicenseCount(strModuleCode,strMLIID);
	if(nMaxCount==-1)
	{
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_MOD_LICENSE_NOT_EXISTS); 
		return;
	}

	//test if old session exists in DB, if so activate it
	m_Services->UserSession->ActivateSession(pStatus,m_nAppServerSessionID,strSession,nStoredSessionID,nMaxCount);
	if(!pStatus.IsOK())
	{
		
		pStatus.setError(StatusCodeSet::ERR_SYSTEM_SESSION_EXPIRED); 
		return ;
	}


	//session is stored in DB: put it in session list:
	SessionRow.nRequestStatus=STATE_OK;
	SessionRow.nRequestCounter=0;
	SessionRow.bStored=true;
	SessionRow.nStoredSessionID=nStoredSessionID;
	SessionRow.strSession=strSession;
	SessionRow.nUserID=nUserID;
	SessionRow.nPersonID=nPersonID;
	SessionRow.strModuleCode=strModuleCode;
	SessionRow.strMLIID=strMLIID;
	SessionRow.rowUserData=rowUserData;
	

	//store changes back into list: (NO DUPLICATE nCurrentThreadID must exists!!)
	m_SessionList[nCurrentThreadID]=SessionRow;
	
	
	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_USER_SESSIONS_REAUTH, (rowUserData.getRowCount()>0?rowUserData.getDataRef(0,"CUSR_USERNAME").toString():"")+";"+strSession+";"+SessionRow.strClientIP+";"+QVariant(nUserID).toString());
	

	//init other global caches, if error rollback session
	//Status err;
	//BuildUserContextData(pStatus,&SessionRow);
	//if(!pStatus.IsOK())
	//	DeleteSession(err,strSession);

}


//update isalive interval internally:
void UserSessionManager_Server::ClientIsAlive(int nSocketID)
{
	UserSessionData SessionRow;
	int nCurrentThreadID=ThreadIdentificator::GetCurrentThreadID();
	QWriteLocker locker(&m_SessionListRWLock);

	//if list do not contain, exit
	if(!m_SessionList.contains(nCurrentThreadID))
		return;

	SessionRow=m_SessionList.value(nCurrentThreadID);
	SessionRow.datLastActivity=QDateTime::currentDateTime();
	m_SessionList[nCurrentThreadID]=SessionRow; 

}



/*!
	Validate user request by session_id. Used only in thin client mode at appserver.
	Can redirect client or refused connection if server is full. (LBO)
	Can issue client for reauthentication if dropped connection or if its trying to access from another server (DFO)
	Counts tries of user loing attempts, if exceeds maximum, client IP address is put on kickban list.
	If client tries to access some method with invalid session it will be challenged for reauthentication.

	\param err				Status of validation (see below):
	\param strSession		session sent by user request
	\param strClientIP		strClientIP client IP
	\param IsLogin			true if requested server method is Login
	\param nSocketID		sokcet id for http server
	\param pDbConn			Db connection
	

	ERR_SYSTEM_KICKBAN			- closes connection (Client IP is added in http server black list, pErrorText Contains Time in minutes for Ban period)
	ERR_SYSTEM_REDIRECT			- closes connection (pErrorText Contains new IP:port)
	ERR_SYSTEM_SESSIONEXPIRED	- closes connection (error status returned)
	ERR_SYSTEM_SERVERFULL		- closes connection (server overloaded, error status returned, no redirection could be made, so sorry)
	ERR_SYSTEM_REAUTHENTICATE	- keeps connection, sends back client authentication challenge
	OK							- client is authorized to access business layer

*/

void UserSessionManager_Server::ValidateUserRequest(Status &err, QString strSession, int &nClientTimeZoneOffsetMinutes, QString strClientIP,bool IsLogin,int nSocketID)
{

	nClientTimeZoneOffsetMinutes=0;
	UserSessionData SessionRow;
	int nCurrentThreadID=ThreadIdentificator::GetCurrentThreadID();
	err.setError(0);

	//take write lock
	QWriteLocker locker(&m_SessionListRWLock);
	


	//----------------------------------
	//			NEW REQUEST
	//----------------------------------
	//NEW REQUEST (new client connection is made, must be authenticated):
	if(!m_SessionList.contains(nCurrentThreadID))
	{

		//FIRST check if we reached maximum sessions allowed:
		CheckMaxSessions(err);
		if(!err.IsOK()) return; //server full: redirect or disconnect

		//new session
		SessionRow=UserSessionData(nCurrentThreadID,STATE_LOGIN,strClientIP,nSocketID);
		nClientTimeZoneOffsetMinutes=SessionRow.nClientTimeZoneOffsetMinutes;
		err.setError(0);
		//it should be Login in 99% but test for intruders and DFO process:
		if(!IsLogin)
		{
			if(strSession.isEmpty()) //not login with empty session, kill it, intruder!!:
			{	
				err.setError(StatusCodeSet::ERR_SYSTEM_KICKBAN,QVariant(m_nKickBanPeriod).toString());
				return;
			}
			else //if session <> '' & not login, probably DFO broken connection, reuthenticate, coz I dont have valid ThreadID, but store it in list!!!
			{
				ReAuthenticateSession(err,STATE_REAUTHENTICATE,SessionRow);
				m_SessionList[nCurrentThreadID]=SessionRow;  //store session row (reauthentication or expired)
				return;
			}
		}

		//LBO redirection (for new clean LOGIN)
		LBORedirect(err);
		if(err.IsOK())
			m_SessionList[nCurrentThreadID]=SessionRow;  //no redirection, store, this is normal path in 99%

		return;
	}


	//----------------------------------
	//			OLD REQUEST
	//----------------------------------

	//old client connection:
	//fetch session data:
	SessionRow=m_SessionList.value(nCurrentThreadID);
	nClientTimeZoneOffsetMinutes=SessionRow.nClientTimeZoneOffsetMinutes;

	// IF session is set to EXPIRED, disconnect client
	if(SessionRow.nRequestStatus==STATE_EXPIRED)
	{
		err.setError(StatusCodeSet::ERR_SYSTEM_SESSION_EXPIRED);
		
		return;
	}


	//--------------------------------------
	//			NORMAL USE: check sessions
	//--------------------------------------
	//if both sessions != EMPTY, compare and pass if OK
	if(!strSession.isEmpty()&&!SessionRow.strSession.isEmpty())
	{
		if(strSession==SessionRow.strSession)
			if(SessionRow.nRequestStatus==STATE_OK) //sessions are matched, status is OK (99% calls will pass here)
			{
				//check if session exists in DB (prevent DB hacking), only occur at time period set by m_nCheckSessionTimePeriod
				CheckSession(err,SessionRow);
			}
			else	//sessions are match, but status is invalid, plz reauthenticate
			{
				if(!IsLogin)//not trying to log in: return call
					{	ReAuthenticateSession(err,STATE_REAUTHENTICATE,SessionRow); }
				else
					{
						ReAuthenticateSession(err,STATE_LOGIN,SessionRow);
						m_SessionList[nCurrentThreadID]=SessionRow; //store back
					}
			}
		else //session are not matched->kickban
		{
			err.setError(StatusCodeSet::ERR_SYSTEM_KICKBAN,QVariant(m_nKickBanPeriod).toString());
		}
	
		//update last Activity time (only if call is valid)
		//Update last Act time is only happening here, coz every other abnormal operation has Timeout Period to protect system from external attacks!
		if(err.IsOK())
		{
			SessionRow.datLastActivity=QDateTime::currentDateTime();
			m_SessionList[nCurrentThreadID]=SessionRow; 
		}
		
		return;
	}


	//----------------------------------
	//			DFO
	//----------------------------------

	//if DFO is disabled return error/disconnect him:
	if(!m_bAcceptDFOConnections)
	{
		SessionRow.nRequestStatus=STATE_EXPIRED;				//mark as expired, garbage will clean this later on
		m_SessionList[nCurrentThreadID]=SessionRow;
		err.setError(StatusCodeSet::ERR_SYSTEM_SESSION_EXPIRED);
		return;
	}


	if(strSession.isEmpty())
	{
		if(SessionRow.strSession.isEmpty())
			if(!IsLogin)//inc session is empty, in list is empty, not loging, kick MOFO!!!
				{err.setError(StatusCodeSet::ERR_SYSTEM_KICKBAN,QVariant(m_nKickBanPeriod).toString());}
			else		//inc session is empty, in list is empty, loging, increase counters
				{ReAuthenticateSession(err,STATE_LOGIN,SessionRow);m_SessionList[nCurrentThreadID]=SessionRow;}
		else			//inc session is empty, in list is non empty, wtf is he trying to do? kick MOFO!!!
			{err.setError(StatusCodeSet::ERR_SYSTEM_KICKBAN,QVariant(m_nKickBanPeriod).toString());}
	}
	else
	{
		if(SessionRow.strSession.isEmpty())
			if(!IsLogin) //inc session is non empty, in list is empty, not loging, send reauth
				{ReAuthenticateSession(err,STATE_REAUTHENTICATE,SessionRow);m_SessionList[nCurrentThreadID]=SessionRow;}
			else  //inc session is non empty, in list is empty, loging - OK
				{ReAuthenticateSession(err,STATE_LOGIN,SessionRow);m_SessionList[nCurrentThreadID]=SessionRow;}

	}
}


void UserSessionManager_Server::ValidateWebUserRequest(Status &err,QString strSession,const HTTPContext &ctx, int &nClientTimeZoneOffsetMinutes)
{
	nClientTimeZoneOffsetMinutes=0;
	UserSessionData SessionRow;
	err.setError(0);
	//set write lock
	QWriteLocker locker(&m_SessionListRWLock);
	int nIdx=FindSession(strSession);
	if (nIdx>=0)
	{
		SessionRow=m_SessionList.value(nIdx);
		int nCurrentThreadID=ThreadIdentificator::GetCurrentThreadID();
		//B.T: 02.05.2013: 3G net changes IP address as crazy...ignore IP change!!!
		/*
		if (ctx.m_strPeerIP!=SessionRow.strClientIP) //must originate from same IP: allow other socket/thread
		{
			err.setError(StatusCodeSet::ERR_SYSTEM_SESSION_EXPIRED);
			return;
		}
		*/

		CheckSession(err,SessionRow); 
		if (!err.IsOK())
			return;

		if (nIdx!=nCurrentThreadID)
			m_SessionList.remove(nIdx); //remove old thread, set new

		SessionRow.nSocketID=ctx.m_nSocketID;
		SessionRow.nThreadID=nCurrentThreadID;
		SessionRow.strClientIP=ctx.m_strPeerIP;
		SessionRow.datLastActivity=QDateTime::currentDateTime();

		nClientTimeZoneOffsetMinutes=SessionRow.nClientTimeZoneOffsetMinutes;
		m_SessionList[nCurrentThreadID]=SessionRow; 
	}
	else
	{
		err.setError(StatusCodeSet::ERR_SYSTEM_SESSION_EXPIRED);
	}
}



//------------------------------------------------------------------------------------------------------
//							SESSION EXPIRE HANDLERS
//------------------------------------------------------------------------------------------------------


/*!
	Delete session from session list & DB
	\param pStatus	   - error if exists
	\param strSession  - expired session
*/
void UserSessionManager_Server::DeleteSession(Status &pStatus, QString strSession)
{

	pStatus.setError(0);
	Q_ASSERT(!strSession.isEmpty()); //emptY!!!!!!!  cant be empty
	
	//take read lock
	m_SessionListRWLock.lockForRead();

	//find session:
	int nThreadID=FindSession(strSession);
	if(nThreadID==-1)
	{
		qDebug() <<"Session - not found ";
		return;
	}
	
	//remove read lock:
	m_SessionListRWLock.unlock();

	DeleteSessionByThreadID(pStatus,nThreadID);
}

/*!
	Delete session from session list & DB by socket  ID
	\param pStatus		- error if exists
	\param nSocketID	- socket ID
*/

void UserSessionManager_Server::DeleteSessionBySocketID(Status &pStatus, int nSocketID)
{
	//take read lock
	pStatus.setError(0);
	m_SessionListRWLock.lockForRead();
	
	int nThreadID=FindSocket(nSocketID);
	if(nThreadID==-1) //if not valid, return
		{m_SessionListRWLock.unlock(); return;}
	
	//remove read lock:
	m_SessionListRWLock.unlock();

	DeleteSessionByThreadID(pStatus,nThreadID);

}

/*!
	Delete session by thread id

	\param pStatus		- error if exists
	\param nThreadID	- thread ID
*/
void UserSessionManager_Server::DeleteSessionByThreadID(Status &pStatus,int nThreadID)
{

	UserSessionData SessionRow;
	pStatus.setError(0);

	//take write lock
	QWriteLocker locker(&m_SessionListRWLock);

	//fetch session data & remove from list &unlock list:
	SessionRow=m_SessionList.value(nThreadID);


	m_SessionList.remove(nThreadID);

	//delete from DB if stored:
	if(SessionRow.bStored)
	{
		QList<int> lstSessionsForDelete;
		lstSessionsForDelete.append(SessionRow.nStoredSessionID);
		m_Services->UserSession->DeleteSession(pStatus,m_nAppServerSessionID,lstSessionsForDelete);
	}
	//remove socket from message list:
	g_MessageDispatcher->ClearUserSocketFromMessages(SessionRow.nSocketID);

	RemoveChildSessionsFromMemory(SessionRow.strSession);

	g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Deleting session: "+SessionRow.strSession);
	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_USER_SESSIONS_CURRENT,QVariant(m_SessionList.size()).toString());
}

/*!
	Delete session from memory by thread id, but keeps in DB as inactive (not influencing license count) for DFO

	\param pStatus		- error if exists
	\param nThreadID	- thread ID
*/
void UserSessionManager_Server::InactiveSession(Status &pStatus,int nThreadID)
{
	UserSessionData SessionRow;
	pStatus.setError(0);

	//take write lock
	QWriteLocker locker(&m_SessionListRWLock);

	//fetch session data & remove from list &unlock list:
	SessionRow=m_SessionList.value(nThreadID,UserSessionData());
	if (SessionRow.nThreadID==0) //session does not exists: normal operation when logout!
	{
		pStatus.setError(0);
		return;
	}

	if (SessionRow.bIsWebSession)
	{
		qDebug()<<"Web Sesion disconnected. Keeping session until timeout or logout"; 
		return;
	}

	m_SessionList.remove(nThreadID);

	//remove socket from message list:
	g_MessageDispatcher->ClearUserSocketFromMessages(SessionRow.nSocketID);

	RemoveChildSessionsFromMemory(SessionRow.strSession); //if childs exist in db: will expire...

	//delete from DB if stored:
	if(SessionRow.bStored)
	{
		m_Services->UserSession->InactivateSessions(pStatus,m_nAppServerSessionID,SessionRow.nStoredSessionID);
		g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_USER_SESSIONS_INACTIVE,(SessionRow.rowUserData.getRowCount()>0?SessionRow.rowUserData.getDataRef(0,"CUSR_USERNAME").toString():"")+";"+SessionRow.strSession+";"+SessionRow.strClientIP+";"+QVariant(SessionRow.nUserID).toString());
	}

	
}


/*!
	Delete expired sessions from session list & DB

	\param err					- error from DB
	\param pListExpiredSessions - list of expired sessions that are removed from main session list & DB
	\param pDbConn				- DB connection

*/
void UserSessionManager_Server::DeleteExpiredUserSessions(Status &err,UserSessionList& pListExpiredSessions)
{
	err.setError(0);
	UserSessionData SessionRow;
	UserSessionListIterator i(m_SessionList);
	QList<int> lstSessionsForDelete;
	QList<QString>  lstParentSessionsForDelete;


	m_SessionListRWLock.lockForWrite();

	pListExpiredSessions.clear();

	while (i.hasNext()) 
	{
		i.next();
		SessionRow=m_SessionList.value(i.key()); //get session

		if (SessionRow.bThreadActive || SessionRow.bIsSystemThread)		//skip system sessions and active ones
			continue;


		if (!SessionRow.bIsWebSession)
		{
			if(SessionRow.nRequestStatus==STATE_EXPIRED || SessionRow.datLastActivity.addSecs(m_nUserSessionTimeOut)<QDateTime::currentDateTime())
			{
				m_SessionList.remove(i.key());				//remove from memory session list
				pListExpiredSessions[i.key()]=SessionRow;	//add to return list (can be used for disconnect)
				if(SessionRow.bStored)						//add for removal only if stored inoto DB
					lstSessionsForDelete.append(SessionRow.nStoredSessionID);
				lstParentSessionsForDelete.append(SessionRow.strSession);

				//remove socket from message list:
				g_MessageDispatcher->ClearUserSocketFromMessages(SessionRow.nSocketID);
			}
		}
		else
		{
			//for webservices use 4x ordinary session expire period CLIENT_IS_ALIVE_INTERVAL_CHECK (320)=5.20=20min
			if(SessionRow.nRequestStatus==STATE_EXPIRED || SessionRow.datLastActivity.addSecs(m_nUserSessionTimeOut*4)<QDateTime::currentDateTime())
			{
				m_SessionList.remove(i.key());				//remove from memory session list
				pListExpiredSessions[i.key()]=SessionRow;	//add to return list (can be used for disconnect)
				if(SessionRow.bStored)						//add for removal only if stored inoto DB
					lstSessionsForDelete.append(SessionRow.nStoredSessionID);
				lstParentSessionsForDelete.append(SessionRow.strSession);

				//remove socket from message list:
				g_MessageDispatcher->ClearUserSocketFromMessages(SessionRow.nSocketID);
			}
		}

	}

	//remove child's:
	int nSize=lstParentSessionsForDelete.size();
	for(int k=0;k<nSize;k++)
	{
		RemoveChildSessionsFromMemory(lstParentSessionsForDelete.at(k));
	}

	m_SessionListRWLock.unlock();
	

	//if DB deletion fails: nothing matter, they will be left out in Session Table, App. server will erase them eventually!!!
	//IF app server in DB <> m_nAppServerSessionID left them...
	if(lstSessionsForDelete.size()!=0)
		m_Services->UserSession->DeleteSession(err,m_nAppServerSessionID,lstSessionsForDelete);
	
}


void UserSessionManager_Server::DeleteOrphanUserSessions(Status &err,UserSessionList lstToDelete)
{
	err.setError(0);
	UserSessionData SessionRow;
	UserSessionListIterator i(lstToDelete);
	QList<int> lstSessionsForDelete;
	QList<QString>  lstParentSessionsForDelete;

	m_SessionListRWLock.lockForWrite();
	while (i.hasNext()) 
	{
		i.next();
		SessionRow=m_SessionList.value(i.key());						//get session

		if(SessionRow.bIsWebSession)
			continue; //ignore if webservice
		
		m_SessionList.remove(i.key());									//remove from memory session list
		if(SessionRow.bStored)											//add for removal only if stored inoto DB
			lstSessionsForDelete.append(SessionRow.nStoredSessionID);
		lstParentSessionsForDelete.append(SessionRow.strSession);
		//remove socket from message list:
		g_MessageDispatcher->ClearUserSocketFromMessages(SessionRow.nSocketID);
	}
	//remove child's:
	int nSize=lstParentSessionsForDelete.size();
	for(int k=0;k<nSize;k++)
		RemoveChildSessionsFromMemory(lstParentSessionsForDelete.at(k));

	m_SessionListRWLock.unlock();

	if(lstSessionsForDelete.size()!=0)
		m_Services->UserSession->DeleteSession(err,m_nAppServerSessionID,lstSessionsForDelete);

}

void UserSessionManager_Server::TestExpiredUserSessions(Status &err,UserSessionList &pListExpiredSessions)
{
	err.setError(0);
	UserSessionData SessionRow;
	UserSessionListIterator i(m_SessionList);
	pListExpiredSessions.clear();

	//check if there are active stored sessions that are not in user session memory-> clean them at once
	//if inactive and expired-> garbage collector will delete 'em
	DbRecordSet lstSessionsInDB;
	m_Services->UserSession->ReadActiveSessionByAppSrvID(err,m_nAppServerSessionID,lstSessionsInDB); 
	if (!err.IsOK())return;
	lstSessionsInDB.clearSelection();
	
	m_SessionListRWLock.lockForRead();
	while (i.hasNext()) 
	{
		i.next();
		SessionRow=m_SessionList.value(i.key()); //get session

		if (SessionRow.bStored)
			lstSessionsInDB.find("COUS_ID",SessionRow.nStoredSessionID,false,false,false,true); //select ones that exists both in session list in memory and in db

		if (SessionRow.bThreadActive)		//skip system sessions and active ones
			continue;

		//check dates and if some1 already set to expire
		if (!SessionRow.bIsWebSession)
		{
			if(SessionRow.nRequestStatus==STATE_EXPIRED || SessionRow.datLastActivity.addSecs(m_nUserSessionTimeOut)<QDateTime::currentDateTime())
			{
				pListExpiredSessions[i.key()]=SessionRow;	//add to return list (can be used for disconnect)
				qDebug()<<"Normal Sesion expired, socket: "<<SessionRow.nSocketID<<", thread: "<<SessionRow.nThreadID;
			}
		}
		else
		{
			//for webservices use 4x ordinary session expire period CLIENT_IS_ALIVE_INTERVAL_CHECK (320)=5.20=20min
			if(SessionRow.nRequestStatus==STATE_EXPIRED || SessionRow.datLastActivity.addSecs(m_nUserSessionTimeOut*4)<QDateTime::currentDateTime())
			{
				pListExpiredSessions[i.key()]=SessionRow;	//add to return list (can be used for disconnect)
				qDebug()<<"Web Sesion expired, socket: "<<SessionRow.nSocketID<<", thread: "<<SessionRow.nThreadID;
			}
		}

	}

	m_SessionListRWLock.unlock();

	if (lstSessionsInDB.getSelectedCount()!=lstSessionsInDB.getRowCount())
	{
		lstSessionsInDB.deleteSelectedRows(); //delete ones that are in memory
		QList<int> lstSessionsForDelete;
		int nSize=lstSessionsInDB.getRowCount();
		for(int i=0;i<nSize;i++)
		{
			lstSessionsForDelete.append(lstSessionsInDB.getDataRef(i,"COUS_ID").toInt());
		}
		if(lstSessionsForDelete.size()!=0) //delete sessions that are left out in db as active ones!!!
		{
			m_Services->UserSession->DeleteSession(err,m_nAppServerSessionID,lstSessionsForDelete);
			if (err.IsOK())
				g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::MSG_SYSTEM_USER_SESSIONS_CLEAN_INACTIVE,QVariant(lstSessionsForDelete.size()).toString());
			else
				g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_USER_SESSIONS_FAILED,"Failed to delete inactive sessions: "+err.getErrorText());
		}
	}
}

void UserSessionManager_Server::GetSessionList(UserSessionList &pListSessions)
{
	QReadLocker locker(&m_SessionListRWLock);
	pListSessions=m_SessionList;
}




//------------------------------------------------------------------------------------------------------
//							PUBLIC GET CONTEXT HANDLERS
//------------------------------------------------------------------------------------------------------




/*!
	Gets session ID from current thread.
	Warning: this always should return valid value, ASSERT is fired if thread doesnt exists in list
	READ lock on session list!

	\return unique session id for current thread

*/
QString UserSessionManager_Server::GetSessionID()
{

	int nCurrentThreadID=ThreadIdentificator::GetCurrentThreadID();
	QString strSession;

	QReadLocker locker(&m_SessionListRWLock);

	if( m_SessionList.contains(nCurrentThreadID))//if exists, just copy IP
		strSession=m_SessionList.value(nCurrentThreadID).strSession;
	else
	{
		//Q_ASSERT_X(false,"Illegal Session","There is no entry for requested Thread In session list!!");
		strSession="";
	}


	
	return strSession;
};



void UserSessionManager_Server::LockThreadActive()
{
	int nCurrentThreadID=ThreadIdentificator::GetCurrentThreadID();
	QWriteLocker locker(&m_SessionListRWLock);

	if( m_SessionList.contains(nCurrentThreadID))
		m_SessionList[nCurrentThreadID].bThreadActive=true;
}

void UserSessionManager_Server::UnLockThreadActive()
{
	int nCurrentThreadID=ThreadIdentificator::GetCurrentThreadID();
	QWriteLocker locker(&m_SessionListRWLock);

	if( m_SessionList.contains(nCurrentThreadID))
	{
		m_SessionList[nCurrentThreadID].bThreadActive=false;
		m_SessionList[nCurrentThreadID].datLastActivity=QDateTime::currentDateTime();
	}
}


/*!
	Gets session ID from socket, .

	\param	nSocketID	- socket
	\return				- unique session id or empty if not found
*/
/*
QString UserSessionManager_Server::GetSessionIDBySocketID(int nSocketID)
{
	//take read lock
	QReadLocker locker(&m_SessionListRWLock);
	
	UserSessionListIterator i(m_SessionList);
	while (i.hasNext()) 
	{
		i.next();
		if(i.value().nSocketID==nSocketID)
			return i.value().strSession;
	}

	return "";	
	
}
*/


/*!
	Gets thread from socket.

	\param	nSocketID	- socket
	\return				- unique session id or -1 if not found
*/
int UserSessionManager_Server::GetThreadBySocketID(int nSocketID)
{

	//take read lock
	QReadLocker locker(&m_SessionListRWLock);

	UserSessionListIterator i(m_SessionList);
	while (i.hasNext()) 
	{
		i.next();
		if(i.value().nSocketID==nSocketID)
			return i.key(); 
	}

	return -1;	 //not found

}



/*!
	Gets user ID from current thread.
	Warning: returns invalid value if session not found, ASSERT if session is from current thread!


	\param		- session
	\return		- user id for current thread

*/
int UserSessionManager_Server::GetUserID(QString strSession)
{
	QReadLocker locker(&m_SessionListRWLock);
	int nCurrentThreadID;
	
	if(strSession.isEmpty())
	{
		nCurrentThreadID=ThreadIdentificator::GetCurrentThreadID();
		Q_ASSERT(nCurrentThreadID!=-1); //msut be valid
	}
	else
	{
		nCurrentThreadID=FindSession(strSession);
		if(nCurrentThreadID==-1) return -1;//return invalid ID if not found
	}
	
	int nUserID;
	if( m_SessionList.contains(nCurrentThreadID))//if exists, just copy IP
		nUserID=m_SessionList.value(nCurrentThreadID).nUserID;
	else
	{
		Q_ASSERT_X(false,"Illegal Session","There is no entry for requested Thread In session list!!");
		nUserID=0;
	}

	return nUserID;
};

int	UserSessionManager_Server::GetClientTimeZoneOffsetMinutes(QString strSession)
{

	QReadLocker locker(&m_SessionListRWLock);
	int nCurrentThreadID;

	if(strSession.isEmpty())
	{
		nCurrentThreadID=ThreadIdentificator::GetCurrentThreadID();
		Q_ASSERT(nCurrentThreadID!=-1); //msut be valid
	}
	else
	{
		nCurrentThreadID=FindSession(strSession);
		if(nCurrentThreadID==-1) return -1;//return invalid ID if not found
	}

	int nClientTimeZoneOffsetMinutes;
	if( m_SessionList.contains(nCurrentThreadID))//if exists, just copy IP
		nClientTimeZoneOffsetMinutes=m_SessionList.value(nCurrentThreadID).nClientTimeZoneOffsetMinutes;
	else
	{
		Q_ASSERT_X(false,"Illegal Session","There is no entry for requested Thread In session list!!");
		nClientTimeZoneOffsetMinutes=0;
	}

	return nClientTimeZoneOffsetMinutes;
}


int UserSessionManager_Server::GetSocketID(QString strSession)
{
	QReadLocker locker(&m_SessionListRWLock);
	int nCurrentThreadID;

	if(strSession.isEmpty())
	{
		nCurrentThreadID=ThreadIdentificator::GetCurrentThreadID();
		Q_ASSERT(nCurrentThreadID!=-1); //msut be valid
	}
	else
	{
		nCurrentThreadID=FindSession(strSession);
		if(nCurrentThreadID==-1) return -1;//return invalid ID if not found
	}

	int nSocketID;

	if( m_SessionList.contains(nCurrentThreadID))//if exists, just copy IP
		nSocketID=m_SessionList.value(nCurrentThreadID).nSocketID;
	else
	{
		Q_ASSERT_X(false,"Illegal Session","There is no entry for requested Thread In session list!!");
		nSocketID=-1;
	}



	return nSocketID;

}

QString UserSessionManager_Server::GetClientIP(QString strSession)
{
	QReadLocker locker(&m_SessionListRWLock);
	int nCurrentThreadID;

	if(strSession.isEmpty())
	{
		nCurrentThreadID=ThreadIdentificator::GetCurrentThreadID();
		Q_ASSERT(nCurrentThreadID!=-1); //msut be valid
	}
	else
	{
		nCurrentThreadID=FindSession(strSession);
		if(nCurrentThreadID==-1) return "";//return invalid ID if not found
	}

	QString strClientIP;

	if( m_SessionList.contains(nCurrentThreadID))//if exists, just copy IP
		strClientIP=m_SessionList.value(nCurrentThreadID).strClientIP;
	else
	{
		Q_ASSERT_X(false,"Illegal Session","There is no entry for requested Thread In session list!!");
		strClientIP="";
	}


	return strClientIP;

}

int UserSessionManager_Server::GetSessionRecordID(QString strSession)
{
	QReadLocker locker(&m_SessionListRWLock);
	int nCurrentThreadID;

	if(strSession.isEmpty())
	{
		nCurrentThreadID=ThreadIdentificator::GetCurrentThreadID();
		Q_ASSERT(nCurrentThreadID!=-1); //msut be valid
	}
	else
	{
		nCurrentThreadID=FindSession(strSession);
		if(nCurrentThreadID==-1) return -1;//return invalid ID if not found
	}






	int nSessionID;


	if( m_SessionList.contains(nCurrentThreadID))//if exists, just copy IP
		nSessionID=m_SessionList.value(nCurrentThreadID).nStoredSessionID;
	else
	{
		Q_ASSERT_X(false,"Illegal Session","There is no entry for requested Thread In session list!!");
		nSessionID=0;
	}



	return nSessionID;

}


/*!
	Gets group ID from current thread.
	Warning: returns invalid value if session not found, ASSERT if session is from current thread!

	\return group id for current thread

*/
int UserSessionManager_Server::GetPersonID(QString strSession)
{
	QReadLocker locker(&m_SessionListRWLock);
	int nCurrentThreadID;

	if(strSession.isEmpty())
	{
		nCurrentThreadID=ThreadIdentificator::GetCurrentThreadID();
		Q_ASSERT(nCurrentThreadID!=-1); //msut be valid
	}
	else
	{
		nCurrentThreadID=FindSession(strSession);
		if(nCurrentThreadID==-1) return -1;//return invalid ID if not found
	}

	int nPersonID;

	if( m_SessionList.contains(nCurrentThreadID))//if exists, just copy IP
		nPersonID=m_SessionList.value(nCurrentThreadID).nPersonID;
	else
	{
		Q_ASSERT_X(false,"Illegal Session","There is no entry for requested Thread In session list!!");
		nPersonID=0;
	}
	
	return nPersonID;
};
int UserSessionManager_Server::GetContactID(QString strSession)
{
	DbRecordSet rowUserData;
	GetUserData(rowUserData,strSession);

	if (rowUserData.getRowCount()>0)
	{
		return rowUserData.getDataRef(0,"BPER_CONTACT_ID").toInt();
	}
	return 0;
};


bool UserSessionManager_Server::IsSystemUser(QString strSession)
{
	DbRecordSet rowData;
	GetUserData(rowData,strSession);
	return (bool)rowData.getDataRef(0,"CUSR_IS_SYSTEM").toInt();
}


/*!
	Gets user row data
	Warning: returns invalid value if session not found, ASSERT if session is from current thread!

	\param rowUserData	- user data (lastname, first name,..) in TVIEW_CORE_USER_SELECTION format

*/
void UserSessionManager_Server::GetUserData(DbRecordSet &rowUserData,QString strSession)
{

	QReadLocker locker(&m_SessionListRWLock);
	int nCurrentThreadID;
	DbRecordSet empty;

	if(strSession.isEmpty())
	{
		nCurrentThreadID=ThreadIdentificator::GetCurrentThreadID();
		Q_ASSERT(nCurrentThreadID!=-1); //msut be valid
	}
	else
	{
		nCurrentThreadID=FindSession(strSession);
		if(nCurrentThreadID==-1) {rowUserData=empty;return;} //return invalid row if not found
	}



	if( m_SessionList.contains(nCurrentThreadID))//if exists, just copy IP
		rowUserData=m_SessionList.value(nCurrentThreadID).rowUserData;
	else
	{
		Q_ASSERT_X(false,"Illegal Session","There is no entry for requested Thread In session list!!");
	}


	
};

/*!
	Gets module info from current thread.
	Warning: this always should return valid value, ASSERT is fired if thread doesnt exists in list
	READ lock on session list!

	\param strModuleCode		client module code
	\param strMLIID				MLIID


*/
void UserSessionManager_Server::GetModuleInfo(QString& strModuleCode, QString& strMLIID){

	int nCurrentThreadID=ThreadIdentificator::GetCurrentThreadID();

	QReadLocker locker(&m_SessionListRWLock);

	if( m_SessionList.contains(nCurrentThreadID))//if exists, just copy IP
	{
		strModuleCode=m_SessionList.value(nCurrentThreadID).strModuleCode;
		strMLIID=m_SessionList.value(nCurrentThreadID).strMLIID;
	}
	else
	{
		Q_ASSERT_X(false,"Illegal Session","There is no entry for requested Thread In session list!!");
	}

	
};








//------------------------------------------------------------------------------------------------------
//							PRIVATE HELPER
//------------------------------------------------------------------------------------------------------




/*!
	Finds Session in list. WARNING: no locking is done, but if u want safe way plz do lock before calling
	\return key or -1 if not found

*/
int UserSessionManager_Server::FindSession(QString strSession)
{
	UserSessionListIterator i(m_SessionList);
	while (i.hasNext()) 
	{
		i.next();
		if(i.value().strSession==strSession)
			return i.key();
	}
		
	return -1;		
}


/*!
	Finds Socket in list. WARNING: no locking is done, but if u want safe way plz do lock before calling
	\return key or -1 if not found

*/
int UserSessionManager_Server::FindSocket(int nSocketID)
{
		UserSessionListIterator i(m_SessionList);
		while (i.hasNext()) 
		{
			i.next();
			if(i.value().nSocketID==nSocketID)
				return i.key();
		}
		
	return -1;		
}




/*!
	Sets Status of session to REAUTHENTICATE or LOGIN, checks if maximum login tries is reached
	\param err			- returned status:ERR_SYSTEM_REAUTHENTICATE or ERR_SYSTEM_KICKBAN or 0 if login is in process
	\param SessionRow	- SessionData modified, counter && status modifed

*/
void UserSessionManager_Server::ReAuthenticateSession(Status &err, int State, UserSessionData &SessionRow)
{
	
	if(State==STATE_LOGIN) //tries to login pass it:
	{
		err.setError(0);	
		SessionRow.nRequestStatus=STATE_LOGIN;
	}
	else //do not try to login return him auth challenge:
	{
		err.setError(StatusCodeSet::ERR_SYSTEM_REAUTHENTICATE);
		g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::ERR_SYSTEM_REAUTHENTICATE,(SessionRow.rowUserData.getRowCount()>0?SessionRow.rowUserData.getDataRef(0,"CUSR_USERNAME").toString():"")+";"+SessionRow.strSession+";"+SessionRow.strClientIP+";"+QVariant(SessionRow.nUserID).toString());
		SessionRow.nRequestStatus=STATE_REAUTHENTICATE;
		SessionRow.datLastActivity=QDateTime::currentDateTime(); //BT: if long time out on client:is alive can be dead, refresh dat last activity..
	}

	SessionRow.nRequestCounter++;

	//return;

	//test last_activity+counter, set error && expired if out of limit:
	if(SessionRow.nRequestCounter>m_nAuthTries||SessionRow.datLastActivity.addSecs(m_nAuthTimeout)<QDateTime::currentDateTime())
	{
			SessionRow.nRequestStatus=STATE_EXPIRED;	//mark as expired, garbage will clean this later on
			err.setError(StatusCodeSet::ERR_SYSTEM_KICKBAN,QVariant(m_nKickBanPeriod).toString());
			g_Logger.logMessage(StatusCodeSet::TYPE_WARNING,StatusCodeSet::ERR_SECURITY_SESSION_REAUTH_EXPIRED,(SessionRow.rowUserData.getRowCount()>0?SessionRow.rowUserData.getDataRef(0,"CUSR_USERNAME").toString():"")+";"+SessionRow.strSession+";"+SessionRow.strClientIP+";"+QVariant(SessionRow.nUserID).toString());
	}

}


/*!
	Checks session based, every time period defined by m_nCheckSessionTimePeriod (sec) session is checked in DB
	\param err			- returned status:0 or SESSION_EXPIRE or UNKNOWN ERROR
	\param SessionRow	- SessionData modified, session check
	\param pDbConn		- Db connection


*/
void UserSessionManager_Server::CheckSession(Status &err, UserSessionData &SessionRow)
{
	err.setError(0);
	
	//test datLastSessionCheck, if time, then check session
	if(SessionRow.datLastSessionCheck.addSecs(m_nCheckSessionTimePeriod)<QDateTime::currentDateTime())
	{
			SessionRow.datLastSessionCheck=QDateTime::currentDateTime();
			m_Services->UserSession->CheckSession(err,SessionRow.nStoredSessionID);
			if(!err.IsOK())
			{
				SessionRow.nRequestStatus=STATE_EXPIRED;				//mark as expired, garbage will clean this later on
				err.setError(StatusCodeSet::ERR_SYSTEM_SESSION_EXPIRED);
				return;
			}
	}

}




/*!
	If LBO process is active, tests LBO list and redirects client to server with lowest load
	Warning: this should not be executed if client is in DFO process

	\param err			- returned status:0 - no redirction or ERR_SYSTEM_REDIRECT (with ip address in err text)
*/
void UserSessionManager_Server::LBORedirect(Status &err)
{
	err.setError(0);
	Q_ASSERT(g_LoadBalancer!=NULL);			   //can be null in thick mode, so test

	if(!g_LoadBalancer->IsActive()) 
		return;
	else
		g_LoadBalancer->IsRedirectionNeeded(err); //redirect if needed
}




/*!
	Check max connections:
	- if < m_nMaxSessions, it's OK, do not change err
	- if reached maximum, try to redirect to server with min load (never mind if LBO is active, this is DFO process)
	
	\param err			- returned status: 0 or ERR_SYSTEM_SERVERFULL or ERR_SYSTEM_REDIRECT

*/
void UserSessionManager_Server::CheckMaxSessions(Status &err)
{
	err.setError(0);

	//everything ok:
	if (m_SessionList.size()<m_nMaxSessions)
		return;
	
	Q_ASSERT(g_LoadBalancer!=NULL);			   //can be null in thick mode, so test


	//if server full try to fetch server with min load
	QString strIP=g_LoadBalancer->GetServerWithMinLoad(true); //skip this server

	if(strIP.isEmpty())
	{	//IP is invalid
		
		g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_USER_SESSIONS_MAX_CONNECTIONS_REACH);
		err.setError(StatusCodeSet::ERR_SYSTEM_SERVERFULL);
	}
	else
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,StatusCodeSet::MSG_SYSTEM_USER_SESSIONS_LBO_REDIRECT,QVariant(strIP).toString());
		//IP is OK, tell client to redirect to this server, everything is OK :)
		err.setError(StatusCodeSet::ERR_SYSTEM_REDIRECT,strIP);
	}


}

int UserSessionManager_Server::GetSessionCount()
{
	QReadLocker locker(&m_SessionListRWLock);
	return m_SessionList.size();
}

QStringList UserSessionManager_Server::GetSocketListForPersonID(int nPersonID)
{
	QStringList lstReturn;
	UserSessionList lst;
	GetSessionList(lst);

	UserSessionListIterator i(lst);
	while (i.hasNext()) 
	{
		i.next();
		if(i.value().nPersonID==nPersonID)
			lstReturn.append(QVariant(i.value().nSocketID).toString());
	}
	return lstReturn;
}


/*!
	Removes all childs from memory: WARNING: no locking is done, but if u want safe way plz do lock before calling
	If inactive: leave child sessions to expire...as they are short living...
*/
void UserSessionManager_Server::RemoveChildSessionsFromMemory(QString strParentSession)
{
	if (strParentSession.isEmpty()) //when user is not logged and destroy is executed: failed login!
		return;

	QList<int> lstDataForRemove;
	UserSessionListIterator i(m_SessionList);
	while (i.hasNext()) 
	{
		i.next();
		if(i.value().strParentSession==strParentSession)
		{
			lstDataForRemove.append(i.key());
			g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("Deleting child session %1 from parent session %2").arg(i.value().strSession).arg(strParentSession) );
		}
	}

	int nSize=lstDataForRemove.size();
	for(int k=0;k<nSize;k++)
	{
		m_SessionList.remove(lstDataForRemove.at(k));
	}

}

void UserSessionManager_Server::ClearUserSessionStorage(QString strSession)
{
	if(strSession.isEmpty())
		strSession=GetSessionID();
	if (strSession.isEmpty())
		return;

	QString strDirName=DataHelper::EncodeSession2Directory(strSession);
	DataHelper::RemoveAllFromDirectory(m_strUserStorageDirectoryPath+"/"+strDirName,true,true);
}




bool UserSessionManager_Server::AuthenticateUser(QString strCookieValue,QString &strSessionID)
{
	QStringList lstAuth=strCookieValue.split(";");
	int nSize=lstAuth.size();
	for (int i=0;i<nSize;++i)
	{
		QString strCookie=lstAuth.at(i).trimmed();
		if (strCookie.left(QString("web_session").length())=="web_session")
		{
			QString strCookieSession=QUrl::fromPercentEncoding(lstAuth.at(i).toLatin1());

			int nIdx=strCookieSession.indexOf("=");
			if (nIdx>=0)
			{
				QReadLocker locker(&m_SessionListRWLock);
				strSessionID=strCookieSession.mid(nIdx+1).trimmed();
				if(FindSession(strSessionID)>=0)
				{
					return true;
				}
			}
		}
	}

	return false;
}