#ifndef SERVERSESSIONMANAGER_H
#define SERVERSESSIONMANAGER_H

#include "common/common/status.h"
#include "systemserviceset.h"
#include "common/common/backgroundtaskinterface.h"
#include <QTimer>



/*!
    \class ServerSessionManager
    \brief Manages Application  sessions for Application server & THICK client
    \ingroup Bus_Server

	Creates new session entry in App. srv. session table. Reports status to this table every N minutes.
	If server is started, check if entry already exists, if so deletes locked resources (for THICK) 
	or set them inactive  (APP.SERVER).
	This mechanism makes cleaning garbage easier especiially in multi app. server & thick client enivoremnt
	
	Use:
	- app. server & thick client stores last session ID by which they are identified in case of crash
	- this ID is only used to easy garbage collection, it is not neccessary
	- In separate thread, every SESSION_REPORT_INTERVAL field (COAS_DAT_LAST_ACTIVITY) is updated in the CORE_APP_SRV_SESSION
	- If some other controll process is checking system status, by testing this field (COAS_DAT_LAST_ACTIVITY)it can clean garbage.

*/

class ServerSessionManager : public  QObject, public BackgroundTaskInterface
{
	Q_OBJECT
	
public:

	ServerSessionManager(SystemServiceSet* pSystemServices);

	int StartSession(Status &pStatus,QString strIPAddress,int nLicenseID,int nRoleID,int nMaxConnections,int nIsLBOActive, int nIsMaster,QString strUpdateTime, QString strWarningTime,QString strSerial="",QString strName="");
	void StopSession(Status &pStatus);
	void CleanAllSessions(Status &pStatus);

	void LoadKickBanList(Status &pStatus,QString strListName, DbRecordSet &lstAccess);
	void SaveKickBanList(Status &pStatus,QString &strListName, DbRecordSet &lstRecords);

	void ExecuteTask(int nCallerID=-1,int nTaskID=-1,QString strData="");

signals:
	void StartShutdownTimer(int);
private:

	void ReportStatus(Status& pStatus){};

	int m_nAppSrvSessionID;
	SystemServiceSet *m_SystemServices; 
	QTimer m_updateTimer;
	QString m_strUpdateTime;
	QString m_strWarningUpdateTime;
	int m_nRestart,m_nWarning,m_nWarningDemoDb;

private slots:
	void OnUpdateTime();
	void OnStartTimer(int);

};


#endif //SERVERSESSIONMANAGER_H



