#include "service_buscustomfields.h"
#include "bus_server/bus_server/businesslocker.h"
#include "db/db/dbsimpleorm.h"
#include "db_core/db_core/dbsqltableview.h"
#include "bus_core/bus_core/globalconstants.h"

#include "businessserviceset.h"
extern BusinessServiceSet* g_BusinessServiceSet;


//strLockedRes is only filled in gui at edit mode...
void Service_BusCustomFields::Write(Status &Ret_pStatus, DbRecordSet &RetOut_Data,DbRecordSet &RetOut_Selections,QString strLockedRes)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_CUSTOM_FIELDS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	DbSimpleOrm TableOrmSelections(Ret_pStatus, BUS_CUSTOM_SELECTIONS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	//start tran:
	TableOrm.GetDbSqlQuery()->BeginTransaction(Ret_pStatus);
	if(!Ret_pStatus.IsOK()) return;

	//write field:
	TableOrm.Write(Ret_pStatus,RetOut_Data);
	if(!Ret_pStatus.IsOK())
	{
		TableOrm.GetDbSqlQuery()->Rollback();
		return;
	}

	int nID=RetOut_Data.getDataRef(0,"BCF_ID").toInt();
	RetOut_Selections.setColValue("BCS_CUSTOM_FIELD_ID",nID);
	
	//first delete old selections:
	QString strDEL="DELETE FROM BUS_CUSTOM_SELECTIONS WHERE BCS_CUSTOM_FIELD_ID="+QVariant(nID).toString();
	TableOrm.GetDbSqlQuery()->Execute(Ret_pStatus,strDEL);
	if(!Ret_pStatus.IsOK())
	{
		TableOrm.GetDbSqlQuery()->Rollback();
		return;
	}

	//write selections 
	TableOrmSelections.Write(Ret_pStatus,RetOut_Selections);
	if(!Ret_pStatus.IsOK())
	{
		TableOrm.GetDbSqlQuery()->Rollback();
		return;
	}

	if (!strLockedRes.isEmpty())
	{
		bool bOK;
		g_BusinessServiceSet->ClientSimpleORM->UnLock(Ret_pStatus,BUS_CUSTOM_FIELDS,bOK,strLockedRes);
		if(!Ret_pStatus.IsOK())
		{
			TableOrm.GetDbSqlQuery()->Rollback();
			return;
		}
	}


	TableOrm.GetDbSqlQuery()->Commit(Ret_pStatus);
	
}

void Service_BusCustomFields::Read(Status &Ret_pStatus, int nCustomFieldID, DbRecordSet &Ret_Data,DbRecordSet &Ret_Selections)
{
	QString strWhere=" WHERE BCF_ID="+QVariant(nCustomFieldID).toString();

	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_CUSTOM_FIELDS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	DbSimpleOrm TableOrmSelections(Ret_pStatus, BUS_CUSTOM_SELECTIONS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	//load all
	TableOrm.Read(Ret_pStatus,Ret_Data,strWhere);
	if(!Ret_pStatus.IsOK()) return;


	strWhere=" WHERE BCS_CUSTOM_FIELD_ID="+QVariant(nCustomFieldID).toString();
	TableOrmSelections.Read(Ret_pStatus,Ret_Selections,strWhere);
}


//returns fields for given table
void Service_BusCustomFields::ReadFields(Status &Ret_pStatus, int nTableID, DbRecordSet &Ret_Data)
{
	QString strWhere=" WHERE BCF_TABLE_ID="+QVariant(nTableID).toString()+" ORDER BY BCF_SORT";

	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_CUSTOM_FIELDS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	//load all
	TableOrm.Read(Ret_pStatus,Ret_Data,strWhere);
	if(!Ret_pStatus.IsOK()) return;
}


//RetOut_Data in same format as from read, but selections are empty....
void Service_BusCustomFields::WriteUserCustomData(Status &Ret_pStatus, int nTableID, int nRecordID, DbRecordSet &RetOut_Data)
{
	//return;
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_CUSTOM_FIELDS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	DbSqlQuery *pQuery = TableOrm.GetDbSqlQuery();

	pQuery->BeginTransaction(Ret_pStatus);
	if(!Ret_pStatus.IsOK()) return;
	
	int nSize=RetOut_Data.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		//get data for user
		int nDataType = RetOut_Data.getDataRef(i,"BCF_DATA_TYPE").toInt();
		switch (nDataType)
		{
		case GlobalConstants::TYPE_CUSTOM_DATA_BOOL:
			{
				DbRecordSet rowData=RetOut_Data.getDataRef(i,"REC_BOOL_DATA").value<DbRecordSet>();
				if (rowData.getRowCount()==0)
					continue;
				rowData.setData(0,"BCB_TABLE_ID",nTableID);
				rowData.setData(0,"BCB_RECORD_ID",nRecordID);
				pQuery->WriteData(Ret_pStatus,"BUS_CUSTOM_BOOL",rowData,3);
				if(!Ret_pStatus.IsOK()) 
				{
					pQuery->Rollback();
					return;
				}
				RetOut_Data.setData(i,"REC_BOOL_DATA",rowData); //get back id's
			}
			break;
		case GlobalConstants::TYPE_CUSTOM_DATA_INT:
			{
				DbRecordSet rowData=RetOut_Data.getDataRef(i,"REC_INT_DATA").value<DbRecordSet>();
				if (rowData.getRowCount()==0)
					continue;
				rowData.setData(0,"BCI_TABLE_ID",nTableID);
				rowData.setData(0,"BCI_RECORD_ID",nRecordID);
				pQuery->WriteData(Ret_pStatus,"BUS_CUSTOM_INT",rowData,3);
				if(!Ret_pStatus.IsOK()) 
				{
					pQuery->Rollback();
					return;
				}
				RetOut_Data.setData(i,"REC_INT_DATA",rowData); //get back id's
			}
			break;
		case GlobalConstants::TYPE_CUSTOM_DATA_FLOAT:
			{
				DbRecordSet rowData=RetOut_Data.getDataRef(i,"REC_FLOAT_DATA").value<DbRecordSet>();
				if (rowData.getRowCount()==0)
					continue;
				rowData.setData(0,"BCFL_TABLE_ID",nTableID);
				rowData.setData(0,"BCFL_RECORD_ID",nRecordID);
				pQuery->WriteData(Ret_pStatus,"BUS_CUSTOM_FLOAT",rowData,3);
				if(!Ret_pStatus.IsOK()) 
				{
					pQuery->Rollback();
					return;
				}
				RetOut_Data.setData(i,"REC_FLOAT_DATA",rowData); //get back id's
			}
			break;
		case GlobalConstants::TYPE_CUSTOM_DATA_TEXT:
			{
				DbRecordSet rowData=RetOut_Data.getDataRef(i,"REC_TEXT_DATA").value<DbRecordSet>();
				if (rowData.getRowCount()==0)
					continue;
				rowData.setData(0,"BCT_TABLE_ID",nTableID);
				rowData.setData(0,"BCT_RECORD_ID",nRecordID);
				pQuery->WriteData(Ret_pStatus,"BUS_CUSTOM_TEXT",rowData,3);
				if(!Ret_pStatus.IsOK()) 
				{
					pQuery->Rollback();
					return;
				}
				RetOut_Data.setData(i,"REC_TEXT_DATA",rowData); //get back id's
			}
			break;
		case GlobalConstants::TYPE_CUSTOM_DATA_LONGTEXT:
			{
				DbRecordSet rowData=RetOut_Data.getDataRef(i,"REC_LONGTEXT_DATA").value<DbRecordSet>();
				if (rowData.getRowCount()==0)
					continue;
				rowData.setData(0,"BCLT_TABLE_ID",nTableID);
				rowData.setData(0,"BCLT_RECORD_ID",nRecordID);
				pQuery->WriteData(Ret_pStatus,"BUS_CUSTOM_LONGTEXT",rowData,3);
				if(!Ret_pStatus.IsOK()) 
				{
					pQuery->Rollback();
					return;
				}
				RetOut_Data.setData(i,"REC_LONGTEXT_DATA",rowData); //get back id's
			}
			break;
		case GlobalConstants::TYPE_CUSTOM_DATA_DATE:
			{
				DbRecordSet rowData=RetOut_Data.getDataRef(i,"REC_DATE_DATA").value<DbRecordSet>();
				if (rowData.getRowCount()==0)
					continue;
				rowData.setData(0,"BCD_TABLE_ID",nTableID);
				rowData.setData(0,"BCD_RECORD_ID",nRecordID);
				pQuery->WriteData(Ret_pStatus,"BUS_CUSTOM_DATE",rowData,3);
				if(!Ret_pStatus.IsOK()) 
				{
					pQuery->Rollback();
					return;
				}
				RetOut_Data.setData(i,"REC_DATE_DATA",rowData); //get back id's
			}
			break;
		case GlobalConstants::TYPE_CUSTOM_DATA_DATETIME:
			{
				DbRecordSet rowData=RetOut_Data.getDataRef(i,"REC_DATETIME_DATA").value<DbRecordSet>();
				if (rowData.getRowCount()==0)
					continue;
				rowData.setData(0,"BCDT_TABLE_ID",nTableID);
				rowData.setData(0,"BCDT_RECORD_ID",nRecordID);
				pQuery->WriteData(Ret_pStatus,"BUS_CUSTOM_DATETIME",rowData,3);
				if(!Ret_pStatus.IsOK()) 
				{
					pQuery->Rollback();
					return;
				}
				RetOut_Data.setData(i,"REC_DATETIME_DATA",rowData); //get back id's
			}

			break;
		}
		
	}

	pQuery->Commit(Ret_pStatus);
}


//for given table read all custom fields+selections, then read user data's...
void Service_BusCustomFields::ReadUserCustomData(Status &Ret_pStatus, int nTableID, int nRecordID, DbRecordSet &Ret_Data)
{
	//return;
	QString strWhere=" WHERE BCF_TABLE_ID="+QVariant(nTableID).toString()+" ORDER BY BCF_SORT";

	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_CUSTOM_FIELDS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	DbSimpleOrm TableOrmSelections(Ret_pStatus, BUS_CUSTOM_SELECTIONS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	//load all custom fields
	TableOrm.Read(Ret_pStatus,Ret_Data,strWhere,DbSqlTableView::TVIEW_BUS_CUSTOM_FIELDS_DATA);
	if(!Ret_pStatus.IsOK()) return;

	//load selections
	DbRecordSet lstSelections;
	TableOrmSelections.ReadFromParentIDs(Ret_pStatus,lstSelections,"BCS_CUSTOM_FIELD_ID",Ret_Data,"BCF_ID");
	if(!Ret_pStatus.IsOK()) return;

	//lstSelections.Dump();

	DbRecordSet lstDataBool;
	DbRecordSet lstDataInt;
	DbRecordSet lstDataFloat;
	DbRecordSet lstDataText;
	DbRecordSet lstDataLongText;
	DbRecordSet lstDataDate;
	DbRecordSet lstDataDateTime;
	bool bDataBoolLoaded=false;
	bool bDataIntLoaded=false;
	bool bDataFloatLoaded=false;
	bool bDataTextLoaded=false;
	bool bDataLongTextLoaded=false;
	bool bDataDateLoaded=false;
	bool bDataDateTimeLoaded=false;
	
	int nSize=Ret_Data.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		//get selections for field:
		int nFldID = Ret_Data.getDataRef(i,"BCF_ID").toInt();
		lstSelections.find("BCS_CUSTOM_FIELD_ID",nFldID,false,false,true,true);
		DbRecordSet rowSelections;
		rowSelections.copyDefinition(lstSelections);
		rowSelections.merge(lstSelections,true);
		Ret_Data.setData(i,"LST_SELECTIONS",rowSelections);

		rowSelections.Dump();
		
		//get data for user
		int nDataType = Ret_Data.getDataRef(i,"BCF_DATA_TYPE").toInt();
		switch (nDataType)
		{
		case GlobalConstants::TYPE_CUSTOM_DATA_BOOL:
			{
				if (!bDataBoolLoaded)
				{
					LoadValueBool(Ret_pStatus,TableOrm.GetDbSqlQuery(),lstDataBool,nTableID,nRecordID);
					if(!Ret_pStatus.IsOK()) return;
					bDataBoolLoaded=true;
				}
				int nRow=lstDataBool.find("BCB_CUSTOM_FIELD_ID",nFldID,true,false,true,true);
				if (nRow>=0)
				{
					Ret_Data.setData(i,"REC_BOOL_DATA",lstDataBool.getRow(nRow));
				}
			}
			break;
		case GlobalConstants::TYPE_CUSTOM_DATA_INT:
			{
				if (!bDataIntLoaded)
				{
					LoadValueInt(Ret_pStatus,TableOrm.GetDbSqlQuery(),lstDataInt,nTableID,nRecordID);
					if(!Ret_pStatus.IsOK()) return;
					bDataIntLoaded=true;
				}
				int nRow=lstDataInt.find("BCI_CUSTOM_FIELD_ID",nFldID,true,false,true,true);
				if (nRow>=0)
				{
					Ret_Data.setData(i,"REC_INT_DATA",lstDataInt.getRow(nRow));
				}
			}
			break;
		case GlobalConstants::TYPE_CUSTOM_DATA_FLOAT:
			{
				if (!bDataFloatLoaded)
				{
					LoadValueFloat(Ret_pStatus,TableOrm.GetDbSqlQuery(),lstDataFloat,nTableID,nRecordID);
					if(!Ret_pStatus.IsOK()) return;
					bDataFloatLoaded=true;
				}
				int nRow=lstDataFloat.find("BCFL_CUSTOM_FIELD_ID",nFldID,true,false,true,true);
				if (nRow>=0)
				{
					Ret_Data.setData(i,"REC_FLOAT_DATA",lstDataFloat.getRow(nRow));
				}
			}
			break;
		case GlobalConstants::TYPE_CUSTOM_DATA_TEXT:
			{
				if (!bDataTextLoaded)
				{
					LoadValueText(Ret_pStatus,TableOrm.GetDbSqlQuery(),lstDataText,nTableID,nRecordID);
					if(!Ret_pStatus.IsOK()) return;
					bDataTextLoaded=true;
				}
				int nRow=lstDataText.find("BCT_CUSTOM_FIELD_ID",nFldID,true,false,true,true);
				if (nRow>=0)
				{
					Ret_Data.setData(i,"REC_TEXT_DATA",lstDataText.getRow(nRow));
				}
			}
			break;
		case GlobalConstants::TYPE_CUSTOM_DATA_LONGTEXT:
			{
				if (!bDataLongTextLoaded)
				{
					LoadValueLongText(Ret_pStatus,TableOrm.GetDbSqlQuery(),lstDataLongText,nTableID,nRecordID);
					if(!Ret_pStatus.IsOK()) return;
					bDataLongTextLoaded=true;
				}
				int nRow=lstDataLongText.find("BCLT_CUSTOM_FIELD_ID",nFldID,true,false,true,true);
				if (nRow>=0)
				{
					Ret_Data.setData(i,"REC_LONGTEXT_DATA",lstDataLongText.getRow(nRow));
				}
			}
			break;
		case GlobalConstants::TYPE_CUSTOM_DATA_DATE:
			{
				if (!bDataDateLoaded)
				{
					LoadValueDate(Ret_pStatus,TableOrm.GetDbSqlQuery(),lstDataDate,nTableID,nRecordID);
					if(!Ret_pStatus.IsOK()) return;
					bDataDateLoaded=true;
				}
				int nRow=lstDataDate.find("BCD_CUSTOM_FIELD_ID",nFldID,true,false,true,true);
				if (nRow>=0)
				{
					Ret_Data.setData(i,"REC_DATE_DATA",lstDataDate.getRow(nRow));
				}
			}
			break;
		case GlobalConstants::TYPE_CUSTOM_DATA_DATETIME:
			{
				if (!bDataDateTimeLoaded)
				{
					LoadValueDateTime(Ret_pStatus,TableOrm.GetDbSqlQuery(),lstDataDateTime,nTableID,nRecordID);
					if(!Ret_pStatus.IsOK()) return;
					bDataDateTimeLoaded=true;
				}
				int nRow=lstDataDateTime.find("BCDT_CUSTOM_FIELD_ID",nFldID,true,false,true,true);
				if (nRow>=0)
				{
					Ret_Data.setData(i,"REC_DATETIME_DATA",lstDataDateTime.getRow(nRow));
				}
			}
			break;
		}
	}
}

void Service_BusCustomFields::ReadUserCustomDataForInsert(Status &Ret_pStatus, int nTableID,DbRecordSet &Ret_Data)
{
	//return;
	QString strWhere=" WHERE BCF_TABLE_ID="+QVariant(nTableID).toString()+" ORDER BY BCF_SORT";

	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_CUSTOM_FIELDS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	DbSimpleOrm TableOrmSelections(Ret_pStatus, BUS_CUSTOM_SELECTIONS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	//load all custom fields
	TableOrm.Read(Ret_pStatus,Ret_Data,strWhere,DbSqlTableView::TVIEW_BUS_CUSTOM_FIELDS_DATA);
	if(!Ret_pStatus.IsOK()) return;

	//load selections
	DbRecordSet lstSelections;
	TableOrmSelections.ReadFromParentIDs(Ret_pStatus,lstSelections,"BCS_CUSTOM_FIELD_ID",Ret_Data,"BCF_ID");
	if(!Ret_pStatus.IsOK()) return;

	//lstSelections.Dump();

	int nSize=Ret_Data.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		//get selections for field:
		int nFldID = Ret_Data.getDataRef(i,"BCF_ID").toInt();
		lstSelections.find("BCS_CUSTOM_FIELD_ID",nFldID,false,false,true,true);
		DbRecordSet rowSelections;
		rowSelections.copyDefinition(lstSelections);
		rowSelections.merge(lstSelections,true);
		Ret_Data.setData(i,"LST_SELECTIONS",rowSelections);

		//rowSelections.Dump();
	}

}


void Service_BusCustomFields::LoadValueBool(Status &Ret_pStatus,DbSqlQuery *pQuery, DbRecordSet &lstData,int nTableID, int nRecordID)
{
	//load all values for each type:
	QString strSQL="SELECT * FROM BUS_CUSTOM_BOOL WHERE BCB_TABLE_ID="+QVariant(nTableID).toString()+" AND  BCB_RECORD_ID="+QVariant(nRecordID).toString();
	pQuery->Execute(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK()) return;
	pQuery->FetchData(lstData);
}
void Service_BusCustomFields::LoadValueInt(Status &Ret_pStatus,DbSqlQuery *pQuery, DbRecordSet &lstData,int nTableID, int nRecordID)
{
	//load all values for each type:
	QString strSQL="SELECT * FROM BUS_CUSTOM_INT WHERE BCI_TABLE_ID="+QVariant(nTableID).toString()+" AND  BCI_RECORD_ID="+QVariant(nRecordID).toString();
	pQuery->Execute(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK()) return;
	pQuery->FetchData(lstData);
}
void Service_BusCustomFields::LoadValueFloat(Status &Ret_pStatus,DbSqlQuery *pQuery, DbRecordSet &lstData,int nTableID, int nRecordID)
{
	//load all values for each type:
	QString strSQL="SELECT * FROM BUS_CUSTOM_FLOAT WHERE BCFL_TABLE_ID="+QVariant(nTableID).toString()+" AND  BCFL_RECORD_ID="+QVariant(nRecordID).toString();
	pQuery->Execute(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK()) return;
	pQuery->FetchData(lstData);

}

void Service_BusCustomFields::LoadValueText(Status &Ret_pStatus,DbSqlQuery *pQuery, DbRecordSet &lstData,int nTableID, int nRecordID)
{
	//load all values for each type:
	QString strSQL="SELECT * FROM BUS_CUSTOM_TEXT WHERE BCT_TABLE_ID="+QVariant(nTableID).toString()+" AND BCT_RECORD_ID="+QVariant(nRecordID).toString();
	pQuery->Execute(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK()) return;
	pQuery->FetchData(lstData);
}

void Service_BusCustomFields::LoadValueLongText(Status &Ret_pStatus,DbSqlQuery *pQuery, DbRecordSet &lstData,int nTableID, int nRecordID)
{
	//load all values for each type:
	QString strSQL="SELECT * FROM BUS_CUSTOM_LONGTEXT WHERE BCLT_TABLE_ID="+QVariant(nTableID).toString()+" AND  BCLT_RECORD_ID="+QVariant(nRecordID).toString();
	pQuery->Execute(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK()) return;
	pQuery->FetchData(lstData);
}
void Service_BusCustomFields::LoadValueDate(Status &Ret_pStatus,DbSqlQuery *pQuery, DbRecordSet &lstData,int nTableID, int nRecordID)
{
	//load all values for each type:
	QString strSQL="SELECT * FROM BUS_CUSTOM_DATE WHERE BCD_TABLE_ID="+QVariant(nTableID).toString()+" AND  BCD_RECORD_ID="+QVariant(nRecordID).toString();
	pQuery->Execute(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK()) return;
	pQuery->FetchData(lstData);
}
void Service_BusCustomFields::LoadValueDateTime(Status &Ret_pStatus,DbSqlQuery *pQuery, DbRecordSet &lstData,int nTableID, int nRecordID)
{
	//load all values for each type:
	QString strSQL="SELECT * FROM BUS_CUSTOM_DATETIME WHERE BCDT_TABLE_ID="+QVariant(nTableID).toString()+" AND  BCDT_RECORD_ID="+QVariant(nRecordID).toString();
	pQuery->Execute(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK()) return;
	pQuery->FetchData(lstData);
}

