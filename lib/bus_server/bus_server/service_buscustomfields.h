#ifndef SERVICE_BUSCUSTOMFIELDS_H
#define SERVICE_BUSCUSTOMFIELDS_H


#include "businessservice.h"
#include "bus_interface/bus_interface/interface_buscustomfields.h"
#include "db/db/dbsqlquery.h"



/*!
	\class Service_BusCustomFields
	\brief Custom fields
	\ingroup Bus_Services

*/

class Service_BusCustomFields : public Interface_BusCustomFields, public BusinessService
{
public:

	//basic ops:
	void Write(Status &Ret_pStatus, DbRecordSet &RetOut_Data,DbRecordSet &RetOut_Selections,QString strLockedRes);
	void Read(Status &Ret_pStatus, int nCustomFieldID, DbRecordSet &Ret_Data,DbRecordSet &Ret_Selections);
	void ReadFields(Status &Ret_pStatus, int nTableID, DbRecordSet &Ret_Data);
	
	//for table record:
	void ReadUserCustomData(Status &Ret_pStatus, int nTableID, int nRecordID, DbRecordSet &Ret_Data);
	void ReadUserCustomDataForInsert(Status &Ret_pStatus, int nTableID, DbRecordSet &Ret_Data);
	void WriteUserCustomData(Status &Ret_pStatus, int nTableID, int nRecordID, DbRecordSet &RetOut_Data);
	

private:
	void LoadValueBool(Status &Ret_pStatus,DbSqlQuery *pQuery, DbRecordSet &lstData,int nTableID, int nRecordID);
	void LoadValueInt(Status &Ret_pStatus,DbSqlQuery *pQuery, DbRecordSet &lstData,int nTableID, int nRecordID);
	void LoadValueFloat(Status &Ret_pStatus,DbSqlQuery *pQuery, DbRecordSet &lstData,int nTableID, int nRecordID);
	void LoadValueText(Status &Ret_pStatus,DbSqlQuery *pQuery, DbRecordSet &lstData,int nTableID, int nRecordID);
	void LoadValueLongText(Status &Ret_pStatus,DbSqlQuery *pQuery, DbRecordSet &lstData,int nTableID, int nRecordID);
	void LoadValueDate(Status &Ret_pStatus,DbSqlQuery *pQuery, DbRecordSet &lstData,int nTableID, int nRecordID);
	void LoadValueDateTime(Status &Ret_pStatus,DbSqlQuery *pQuery, DbRecordSet &lstData,int nTableID, int nRecordID);
};



#endif // SERVICE_BUSCUSTOMFIELDS_H
