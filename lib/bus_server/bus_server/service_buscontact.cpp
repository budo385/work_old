#include "service_buscontact.h"
#include "bus_core/bus_core/mainentityfilter.h"
#include "bus_core/bus_core/mainentitycalculatedname.h"
#include "bus_core/bus_core/businessdatatransformer.h"
#include "bus_core/bus_core/contacttypemanager.h"
#include "common/common/entity_id_collection.h"
#include "db/db/dbconnectionreserver.h"
#include "businesslocker.h"
#include "bus_core/bus_core/accessrightsid.h"
#include "bus_core/bus_core/picturehelpercore.h"
#include "bus_core/bus_core/useraccessright.h"
#include "bus_core/bus_core/formataddress.h"
#include "bus_core/bus_core/globalconstants.h"

#include "bus_core/bus_core/usersessionmanager.h"
extern UserSessionManager	*g_UserSessionManager;
#include "privateserviceset.h"
extern PrivateServiceSet* g_PrivateServiceSet; 
#include "businessserviceset.h"
extern BusinessServiceSet* g_BusinessServiceSet;
#include "bus_core/bus_core/useraccessright.h"
extern UserAccessRight *g_AccessRight;				//global access right tester
#include "common/common/logger.h"
extern Logger				g_Logger;


/*!
	Read short contacts (faster)

	\param Ret_pStatus		- returns error (RETURNED)
	\param Filter			- filter parameters stored in the DbRecordSet
	\param Ret_Data			- returned data in TVIEW_BUS_CONTACT_SELECTION
	\param pDbConn			- db connection

*/
void Service_BusContact::ReadShortContactList(Status &Ret_pStatus, DbRecordSet Filter, DbRecordSet &Ret_Data)
{

	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_CM_CONTACT, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	//--------------------------------------------------------------
	//		LOAD CONTACTS base on filter (Build Contact List)
	//--------------------------------------------------------------

	//if filter exists try to set filter:
	QString strWhere;if(Filter.getRowCount()!=0)
	{
		strWhere=MainEntityFilter::GetWhereStatement(Filter,true);
	}
	//issue nr.789
	/*
	if (strWhere.indexOf("WHERE")==-1)
		strWhere += " WHERE";
	else
		strWhere += " AND ";

	strWhere += " (BCNT_OWNER_ID IS NULL OR BCNT_OWNER_ID="+QVariant(g_UserSessionManager->GetPersonID()).toString()+" OR (BCNT_OWNER_ID <> "+QVariant(g_UserSessionManager->GetPersonID()).toString()+" AND  BCNT_ACCESS >0 ))";
*/
	//strWhere+=" ORDER BY BCNT_ORGANIZATIONNAME, BCNT_LASTNAME,BCNT_FIRSTNAME";	//default result sorting


	g_AccessRight->SQLFilterRecords(BUS_CM_CONTACT,strWhere);
	TableOrm.Read(Ret_pStatus, Ret_Data, strWhere, DbSqlTableView::TVIEW_BUS_CONTACT_SELECTION);
	if(!Ret_pStatus.IsOK()) return;


	//calculated names: 
	MainEntityCalculatedName filler;
	filler.Initialize(ENTITY_BUS_CONTACT,Ret_Data);
	filler.FillCalculatedNames(Ret_Data,Ret_Data.getColumnIdx("BCNT_NAME"));

	//Ret_Data.Dump();
}

//return active contacts in TVIEW_BUS_CONTACT_SELECTION format from given groups inside strGroupIDs= (id1, id2, id3)
void Service_BusContact::ReadActiveContactsFromGroups(Status &Ret_pStatus, QString strGroupIDs,DbRecordSet &Ret_Data)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_CM_CONTACT, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	QString strWhere = " INNER JOIN bus_cm_group ON BGCN_CONTACT_ID = BCNT_ID WHERE BGCN_ITEM_ID IN "+strGroupIDs;
	g_AccessRight->SQLFilterRecords(BUS_CM_CONTACT,strWhere);

	TableOrm.Read(Ret_pStatus, Ret_Data, strWhere, DbSqlTableView::TVIEW_BUS_CONTACT_SELECTION_WITH_ACTIVE_GROUP_PERIOD);
	if(!Ret_pStatus.IsOK()) return;

	//filter only active ones: 2678:

	//find active:
	QDate today = QDate::currentDate();
	int nSize=Ret_Data.getRowCount();
	Ret_Data.clearSelection();
	for(int i=0;i<nSize;++i)
	{
		QDate from=Ret_Data.getDataRef(i,"BGCN_CMCA_VALID_FROM").toDate();
		QDate to=Ret_Data.getDataRef(i,"BGCN_CMCA_VALID_TO").toDate();

		if (from.isNull() && to.isNull())
			continue;

		if (from.isNull() && !to.isNull())
		{
			if (to>=today)
				continue;
			else
			{
				Ret_Data.selectRow(i);
				continue;
			}
		}

		if (!from.isNull() && to.isNull())
		{
			if (from<=today)
				continue;
			else
			{
				Ret_Data.selectRow(i);
				continue;
			}
		}

		if (from<=today && to>=today)
		{
			continue;
		}
		else
		{
			Ret_Data.selectRow(i);
			continue;
		}
	}
	Ret_Data.deleteSelectedRows();
	Ret_Data.removeColumn(Ret_Data.getColumnIdx("BGCN_CMCA_VALID_FROM"));
	Ret_Data.removeColumn(Ret_Data.getColumnIdx("BGCN_CMCA_VALID_TO"));

	//calculated names: 
	MainEntityCalculatedName filler;
	filler.Initialize(ENTITY_BUS_CONTACT,Ret_Data);
	filler.FillCalculatedNames(Ret_Data,Ret_Data.getColumnIdx("BCNT_NAME"));

}


/*!
	Read contacts in TVIEW_BUS_CONTACT_FULL format, BUT:
	- BGCN,BCMD fields are not filled
	- based on nSubDataFilter: LST_JOURNAL,LST_GROUPS, LST_CREDITOR, LST_DEBTOR,LST_PICTURES are not filled (default is none of these lists are read)
	- all other LST's and BCNT_NAME is filled

	nSubDataFilter: bit or'ed flags from contacttypemanager:
	enum SubDataFilter //used on contact read/write to skip some subdata that is not needed in TVIEW_BUS_CONTACT_FULL
	{
	USE_SUBDATA_NONE=0,
	USE_SUBDATA_ALL=1,
	USE_SUBDATA_PICTURE=2,
	USE_SUBDATA_CREDITOR=4,
	USE_SUBDATA_DEBTOR=8,
	USE_SUBDATA_GROUP=16,
	USE_SUBDATA_JOURNAL=32,	
	};

	\param Ret_pStatus		- returns error (RETURNED)
	\param Filter			- filter parameters stored in the DbRecordSet
	\param Ret_Data			- returned data in TVIEW_BUS_CONTACT_FULL
	\param pDbConn			- db connection

*/
void Service_BusContact::ReadData(Status &Ret_pStatus, DbRecordSet Filter, DbRecordSet &Ret_Data,int nSubDataFilter)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_CM_CONTACT, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;


	//--------------------------------------------------------------
	//		LOAD CONTACTS base on filter (Build Contact List)
	//--------------------------------------------------------------

	//QTime time;
	//time.start();
	//if filter exists try to set filter:
	QString strWhere;
	
	//WARNING: 
	if(Filter.getRowCount()!=0)
	{
		strWhere=MainEntityFilter::GetWhereStatement(Filter,true);
	}
	//issue nr.789
	/*
	if (strWhere.indexOf("WHERE")==-1)
		strWhere += " WHERE";
	else
		strWhere += " AND ";

	strWhere += " (BCNT_OWNER_ID IS NULL OR BCNT_OWNER_ID="+QVariant(g_UserSessionManager->GetPersonID()).toString()+" OR (BCNT_OWNER_ID <> "+QVariant(g_UserSessionManager->GetPersonID()).toString()+" AND  BCNT_ACCESS >0 ))";
	*/
	strWhere += " ORDER BY BCNT_ID";		//this is essential




	//load all
	TableOrm.Read(Ret_pStatus, Ret_Data, strWhere, DbSqlTableView::TVIEW_BUS_CONTACT_FULL);
	if(!Ret_pStatus.IsOK()) return;

	//calculated names: 
	MainEntityCalculatedName filler;
	filler.Initialize(ENTITY_BUS_CONTACT,Ret_Data);
	filler.FillCalculatedNames(Ret_Data,Ret_Data.getColumnIdx("BCNT_NAME"));


	/*
	//-----------------------------------------------------
	//		LOAD ADDRESSES
	//-----------------------------------------------------
	DbRecordSet lstAddress;
	TableOrm.ReadFromParentIDs(Ret_pStatus, lstAddress,"BCMA_CONTACT_ID",Ret_Data,"BCNT_ID","", "",DbSqlTableView::TVIEW_BUS_CM_ADDRESS_SELECT);
	if(!Ret_pStatus.IsOK()) return;
	//-----------------------------------------------------
	//		LOAD ADDRESS TYPES
	//-----------------------------------------------------
	DbRecordSet lstAddressTypes;
	TableOrm.ReadFromParentIDs(Ret_pStatus, lstAddressTypes,"BCMAT_ADDRESS_ID",lstAddress,"BCMA_ID"," LEFT OUTER JOIN bus_cm_types ON BCMT_ID=BCMAT_TYPE_ID ", "",DbSqlTableView::TVIEW_BUS_CM_ADDRESS_TYPES_SELECT);
	if(!Ret_pStatus.IsOK()) return;
	*/
	//-----------------------------------------------------
	//		LOAD PHONES
	//-----------------------------------------------------
	DbRecordSet lstPhones;
	TableOrm.ReadFromParentIDs(Ret_pStatus, lstPhones,"BCMP_CONTACT_ID",Ret_Data,"BCNT_ID"," LEFT OUTER JOIN bus_cm_types ON BCMT_ID=BCMP_TYPE_ID ", "",DbSqlTableView::TVIEW_BUS_CM_PHONE_SELECT);
	if(!Ret_pStatus.IsOK()) return;
	//-----------------------------------------------------
	//		LOAD EMAILS
	//-----------------------------------------------------
	DbRecordSet lstEmails;
	TableOrm.ReadFromParentIDs(Ret_pStatus, lstEmails,"BCME_CONTACT_ID",Ret_Data,"BCNT_ID"," LEFT OUTER JOIN bus_cm_types ON BCMT_ID=BCME_TYPE_ID ", "",DbSqlTableView::TVIEW_BUS_CM_EMAIL_SELECT);
	if(!Ret_pStatus.IsOK()) return;
	//-----------------------------------------------------
	//		LOAD INTERNET
	//-----------------------------------------------------
	DbRecordSet lstInternet;
	TableOrm.ReadFromParentIDs(Ret_pStatus, lstInternet,"BCMI_CONTACT_ID",Ret_Data,"BCNT_ID"," LEFT OUTER JOIN bus_cm_types ON BCMT_ID=BCMI_TYPE_ID ", "",DbSqlTableView::TVIEW_BUS_CM_INTERNET_SELECT);
	if(!Ret_pStatus.IsOK()) return;
	//-----------------------------------------------------
	//		LOAD CREDITOR
	//-----------------------------------------------------
	int nCnt=Ret_Data.getRowCount();
	DbRecordSet lstCreditor;
	lstCreditor.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_CREDITOR));
	if (nSubDataFilter & ContactTypeManager::USE_SUBDATA_CREDITOR || nSubDataFilter==ContactTypeManager::USE_SUBDATA_ALL)
	{
		TableOrm.ReadFromParentIDs(Ret_pStatus, lstCreditor,"BCMC_CONTACT_ID",Ret_Data,"BCNT_ID","","",DbSqlTableView::TVIEW_BUS_CM_CREDITOR);
		if(!Ret_pStatus.IsOK()) return;
	}
	//-----------------------------------------------------
	//		LOAD DEBTOR
	//-----------------------------------------------------
	DbRecordSet lstDebtor;
	lstDebtor.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_DEBTOR));
	if (nSubDataFilter & ContactTypeManager::USE_SUBDATA_DEBTOR || nSubDataFilter==ContactTypeManager::USE_SUBDATA_ALL)
	{
		TableOrm.ReadFromParentIDs(Ret_pStatus, lstDebtor,"BCMD_CONTACT_ID",Ret_Data,"BCNT_ID","","",DbSqlTableView::TVIEW_BUS_CM_DEBTOR);
		if(!Ret_pStatus.IsOK()) return;
	}
	//-----------------------------------------------------
	//		LOAD CUSTOM DATA
	//-----------------------------------------------------
	if (nSubDataFilter & ContactTypeManager::USE_SUBDATA_CUSTOM_FIELDS || nSubDataFilter==ContactTypeManager::USE_SUBDATA_ALL)
	{
		for (int i=0;i<nCnt;i++)
		{
			DbRecordSet lstCustomData;
			lstCustomData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CUSTOM_FIELDS_DATA));
			int nContactID=Ret_Data.getDataRef(i,"BCNT_ID").toInt();
			g_BusinessServiceSet->BusCustomFields->ReadUserCustomData(Ret_pStatus,BUS_CM_CONTACT,nContactID,lstCustomData);
			if(!Ret_pStatus.IsOK()) return;
			Ret_Data.setData(i,"LST_CUSTOM_FIELDS",lstCustomData);
		}
	}

	//-----------------------------------------------------
	//		LOAD GROUPS
	//-----------------------------------------------------
	DbRecordSet lstGroups;
	lstGroups.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_GROUP_SELECT));
	if (nSubDataFilter & ContactTypeManager::USE_SUBDATA_GROUP || nSubDataFilter==ContactTypeManager::USE_SUBDATA_ALL)
	{
		TableOrm.ReadFromParentIDs(Ret_pStatus, lstGroups,"BGCN_CONTACT_ID",Ret_Data,"BCNT_ID",""," ORDER BY BGTR_NAME,BGIT_CODE,BGIT_NAME",DbSqlTableView::TVIEW_BUS_CM_GROUP_SELECT);
		if(!Ret_pStatus.IsOK()) return;
	}
	//-----------------------------------------------------
	//		LOAD JOURNAL
	//-----------------------------------------------------
	DbRecordSet lstJournal;
	lstJournal.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_JOURNAL_SELECT));
	if (nSubDataFilter & ContactTypeManager::USE_SUBDATA_JOURNAL || nSubDataFilter==ContactTypeManager::USE_SUBDATA_ALL)
	{
		TableOrm.ReadFromParentIDs(Ret_pStatus, lstJournal,"BCMJ_CONTACT_ID",Ret_Data,"BCNT_ID","","",DbSqlTableView::TVIEW_BUS_CM_JOURNAL_SELECT);
		if(!Ret_pStatus.IsOK()) return;
		//re-arrange list: assign BPER_NAME column:
		MainEntityCalculatedName filler;
		filler.Initialize(ENTITY_BUS_PERSON,lstJournal);
		filler.FillCalculatedNames(lstJournal,lstJournal.getColumnIdx("BPER_NAME"));
	}
	//-----------------------------------------------------
	//		LOAD PICTURES
	//-----------------------------------------------------
	DbRecordSet lstPictures;
	lstPictures.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_PICTURE));
	if (nSubDataFilter & ContactTypeManager::USE_SUBDATA_PICTURE || nSubDataFilter==ContactTypeManager::USE_SUBDATA_ALL)
	{
		TableOrm.ReadFromParentIDs(Ret_pStatus, lstPictures,"BCMPC_CONTACT_ID",Ret_Data,"BCNT_ID","","",DbSqlTableView::TVIEW_BUS_CM_PICTURE);
		if(!Ret_pStatus.IsOK()) return;
	}
	lstPictures.addColumn(QVariant::ByteArray,"BPIC_PICTURE"); //add dummy for later: this column is loaded on client by request(view, edit, insert)


	//-----------------------------------------------------
	//		LOAD ADDRESSES
	//-----------------------------------------------------
	DbRecordSet lstAddress;
	ReadContactAddress(Ret_pStatus,Ret_Data,lstAddress);
	if(!Ret_pStatus.IsOK()) return;

/*
	//-----------------------------------------------------
	//		REASSIGN CHILD's TO PARENTS:
	//-----------------------------------------------------

	//types -> addresses
	//---------------------------------------------------------------
	//lstAddress.addColumn(DbRecordSet::GetVariantType(),"LST_TYPES");
	g_PrivateServiceSet->BusinessHelper->AssignChildToParent(lstAddress,"LST_TYPES",lstAddressTypes,"BCMAT_ADDRESS_ID");
	//special function for adress: fill BCMT_TYPE_NAME field with all typ
	//read all address types->cache?
	//loop on addresses->their types
	//lstAddressTypes.Dump();
	int nSize=lstAddress.getRowCount();
	int nLstTypesColIdx=lstAddress.getColumnIdx("LST_TYPES");
	int nLstTypeNameColIdx=lstAddressTypes.getColumnIdx("BCMT_TYPE_NAME");
	int nAddrTypeNameColIdx=lstAddress.getColumnIdx("BCMT_TYPE_NAME");
	QString strTypeName;
	int nSize2;
	for(int i=0;i<nSize;++i)
	{
		DbRecordSet lstTypes=lstAddress.getDataRef(i,nLstTypesColIdx).value<DbRecordSet>();
		strTypeName="";
		nSize2=lstTypes.getRowCount();
		for(int k=0;k<nSize2;++k)
			strTypeName+=lstTypes.getDataRef(k,nLstTypeNameColIdx).toString()+",";
		lstAddress.setData(i,nAddrTypeNameColIdx,strTypeName.remove(strTypeName.size()-1,1)); //remove last ,
	}
*/
	g_PrivateServiceSet->BusinessHelper->AssignChildToParent(Ret_Data,"LST_ADDRESS",lstAddress,"BCMA_CONTACT_ID");
	g_PrivateServiceSet->BusinessHelper->AssignChildToParent(Ret_Data,"LST_PHONE",lstPhones,"BCMP_CONTACT_ID");
	g_PrivateServiceSet->BusinessHelper->AssignChildToParent(Ret_Data,"LST_EMAIL",lstEmails,"BCME_CONTACT_ID");
	g_PrivateServiceSet->BusinessHelper->AssignChildToParent(Ret_Data,"LST_INTERNET",lstInternet,"BCMI_CONTACT_ID");
	g_PrivateServiceSet->BusinessHelper->AssignChildToParent(Ret_Data,"LST_CREDITOR",lstCreditor,"BCMC_CONTACT_ID");
	g_PrivateServiceSet->BusinessHelper->AssignChildToParent(Ret_Data,"LST_DEBTOR",lstDebtor,"BCMD_CONTACT_ID");
	g_PrivateServiceSet->BusinessHelper->AssignChildToParent(Ret_Data,"LST_GROUPS",lstGroups,"BGCN_CONTACT_ID");
	g_PrivateServiceSet->BusinessHelper->AssignChildToParent(Ret_Data,"LST_JOURNAL",lstJournal,"BCMJ_CONTACT_ID");
	g_PrivateServiceSet->BusinessHelper->AssignChildToParent(Ret_Data,"LST_PICTURES",lstPictures,"BCMPC_CONTACT_ID");

	//time.restart();
}

//locking????
void Service_BusContact::DeleteContacts(Status &Ret_pStatus, DbRecordSet &DataForDelete,bool bLock)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_CM_CONTACT, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	//filter out contacts with Access rights:
	int nCount=0;
	QString strWhereIn,strWhere,strRelationTableName;
	DbRecordSet lstDataForDelete;
	while (nCount>=0)
	{
		nCount=DbSqlTableDefinition::GenerateChunkedWhereStatement(DataForDelete,"BCNT_ID",strWhereIn,nCount);
		if (nCount<0) break;
		strWhere = " WHERE BCNT_ID IN"+strWhereIn; //+" AND (BCNT_OWNER_ID IS NULL OR BCNT_OWNER_ID="+QVariant(g_UserSessionManager->GetPersonID()).toString()+" OR (BCNT_OWNER_ID <> "+QVariant(g_UserSessionManager->GetPersonID()).toString()+" AND  BCNT_ACCESS >1 ))";
		QString strSql = "SELECT BCNT_ID FROM BUS_CM_CONTACT"+strWhere;
		//qDebug()<<strWhereIn;
		TableOrm.GetDbSqlQuery()->Execute(Ret_pStatus,strSql);
		if(!Ret_pStatus.IsOK())
		{
			return;
		}
		DbRecordSet tmp;
		TableOrm.GetDbSqlQuery()->FetchData(tmp);
		if (lstDataForDelete.getColumnCount()==0)
			lstDataForDelete=tmp;
		else
			lstDataForDelete.merge(tmp);
	}

	//lstDataForDelete.Dump();




	QString strLocked;
	BusinessLocker locker(Ret_pStatus,BUS_CM_CONTACT,GetDbManager(), TableOrm.GetDbConnection());
	if(!Ret_pStatus.IsOK()) return;

	//try to lock contacts:
	if (bLock)
	{
		locker.Lock(Ret_pStatus,lstDataForDelete,strLocked);
		if(!Ret_pStatus.IsOK()) return;
	}

	//delete:
	TableOrm.DeleteFast(Ret_pStatus,lstDataForDelete);

	//unlock
	if (bLock)
	{
		Status err;
		locker.UnLock(err,strLocked);
	}


	//notify client that some could not be deleted:
	if (lstDataForDelete.getRowCount()!=DataForDelete.getRowCount())
	{
		Ret_pStatus.setError(StatusCodeSet::ERR_BUS_ACESS_REFUSED_ON_DELETE);
		return;
	}

}

//read picture
void Service_BusContact::ReadBigPicture(Status &Ret_pStatus, int nBigPicID,DbRecordSet &Ret_Data )
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_BIG_PICTURE, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	//load pic:
	TableOrm.Read(Ret_pStatus,Ret_Data," WHERE BPIC_ID="+QVariant(nBigPicID).toString());
}



//write picture
void Service_BusContact::WriteBigPicture(Status &Ret_pStatus, DbRecordSet &Ret_Data)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_BIG_PICTURE, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	TableOrm.Write(Ret_pStatus,Ret_Data);

}




//load all phones for 1 contact: used in voice call:
void Service_BusContact::ReadContactPhones(Status &Ret_pStatus, int nContactID,DbRecordSet &Ret_Data, int nQueryRead,DbRecordSet lstBySystemType)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_CM_PHONE, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	if (lstBySystemType.getRowCount()==0)
	{
		TableOrm.Read(Ret_pStatus,Ret_Data," WHERE BCMP_CONTACT_ID ="+QString::number(nContactID),nQueryRead);
	}
	else
	{
		QString strWhere;
		int nSize=lstBySystemType.getRowCount();
		for (int i=0;i<nSize;i++)
		{
			strWhere+=lstBySystemType.getDataRef(i,0).toString()+",";
		}
		strWhere.chop(1);
		strWhere=" BCMT_SYSTEM_TYPE IN ("+strWhere+")";
		strWhere=" INNER JOIN BUS_CM_TYPES ON BCMP_TYPE_ID=BCMT_ID WHERE BCMP_CONTACT_ID ="+QString::number(nContactID)+" AND "+strWhere;

		TableOrm.Read(Ret_pStatus,Ret_Data,strWhere,nQueryRead);
	}

}


//----------------------------------------------------
//					JOURNAL
//----------------------------------------------------

void Service_BusContact::ReadContactJournals(Status &Ret_pStatus, int nContactID, DbRecordSet &Ret_Data)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_CM_JOURNAL, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	TableOrm.Read(Ret_pStatus,Ret_Data," WHERE BCMJ_CONTACT_ID ="+QVariant(nContactID).toString(),DbSqlTableView::TVIEW_BUS_CM_JOURNAL_SELECT);

	//adapt list for person name;:
	if(!Ret_pStatus.IsOK()) return;

	//fill name (calculated field):)
	//PERS
	int nLastNameIdx=Ret_Data.getColumnIdx("BPER_LAST_NAME");
	int nFirstNameIdx=Ret_Data.getColumnIdx("BPER_FIRST_NAME");
	int nNameIdx=Ret_Data.getColumnIdx("BPER_NAME");

	int nSize=Ret_Data.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		QString strName=Ret_Data.getDataRef(i,nLastNameIdx).toString()+", "+Ret_Data.getDataRef(i,nFirstNameIdx).toString();
		Ret_Data.setData(i,nNameIdx,strName);
	}
	

}

void Service_BusContact::WriteJournal(Status &Ret_pStatus, DbRecordSet &RetOut_Data,DbRecordSet &DataForDelete,QString strLockRes)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_CM_JOURNAL, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;


	//tran
	TableOrm.GetDbSqlQuery()->BeginTransaction(Ret_pStatus);
	if(!Ret_pStatus.IsOK()) return;

	//delete:
	if(DataForDelete.getRowCount()>0)
		TableOrm.DeleteFast(Ret_pStatus,DataForDelete);
	if(!Ret_pStatus.IsOK())
	{	
		TableOrm.GetDbSqlQuery()->Rollback();
		return;
	}

	//skip because list is in TVIEW_BUS_CM_JOURNAL_SELECT format:
	int nSkipLastCols=4; //hardcoded!!!

	//Write
	TableOrm.Write(Ret_pStatus,RetOut_Data,-1,nSkipLastCols);
	if(!Ret_pStatus.IsOK())
	{
		TableOrm.GetDbSqlQuery()->Rollback();
		return;
	}

	//if write ok & locked, unlock:
	if(Ret_pStatus.IsOK() && !strLockRes.isEmpty())
	{
		BusinessLocker locker(Ret_pStatus,BUS_CM_JOURNAL,GetDbManager(), TableOrm.GetDbConnection());
		locker.UnLock(Ret_pStatus,strLockRes);
	}

	TableOrm.GetDbSqlQuery()->Commit(Ret_pStatus);

}

//locking???
void Service_BusContact::DeleteJournal(Status &Ret_pStatus, DbRecordSet &DataForDelete,bool bLock )
{

	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_CM_JOURNAL, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	QString strLocked;
	BusinessLocker locker(Ret_pStatus,BUS_CM_JOURNAL,GetDbManager(), TableOrm.GetDbConnection());
	if(!Ret_pStatus.IsOK()) return;

	//try to lock contacts:
	if (bLock)
	{
		locker.Lock(Ret_pStatus,DataForDelete,strLocked);
		if(!Ret_pStatus.IsOK()) return;
	}

	//delete:
	TableOrm.DeleteFast(Ret_pStatus,DataForDelete);

	//unlock
	if (bLock)
	{
		Status err;
		locker.UnLock(err,strLocked);
	}

}


void Service_BusContact::LockJournal(Status &Ret_pStatus, int nJournalID, QString &Ret_strLockRes)
{
	//Make Locker Obj & lock:
	BusinessLocker locker(Ret_pStatus,BUS_CM_JOURNAL,GetDbManager());
	QList<int> lstLock;
	lstLock<<nJournalID;
	locker.Lock(Ret_pStatus,lstLock,Ret_strLockRes);

}
void Service_BusContact::UnlockJournal(Status &Ret_pStatus, QString strLockRes)
{
	BusinessLocker locker(Ret_pStatus,BUS_CM_JOURNAL,GetDbManager());
	locker.UnLock(Ret_pStatus,strLockRes);
}

//return list in format: BCNT_ID,BCNT_PICTURE, BCNT_BIG_PICTURE_ID 
void Service_BusContact::ReadPreviewPicture(Status &Ret_pStatus, int nContactID,DbRecordSet &Ret_Data)
{

	//Make local query, can fail if connection reservation fails.
	DbSqlQuery query(Ret_pStatus, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;


	QString strSQL =" SELECT BCNT_ID,BCNT_PICTURE, BCNT_BIG_PICTURE_ID FROM BUS_CM_CONTACT WHERE BCNT_ID="+QVariant(nContactID).toString();


	query.Execute(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK()) return;

	query.FetchData(Ret_Data);

}



//purpose: to avoid multi server calls: from 3 to 1....not bad....
void Service_BusContact::BatchReadContactData(Status &Ret_pStatus, int nContactID,DbRecordSet &Ret_Data,DbRecordSet &DataNMRXPersonFilter,DbRecordSet &Ret_DataNMRXPerson,DbRecordSet &DataNMRXProjectFilter,DbRecordSet &Ret_DataNMRXProject,int nSubDataFilter,DbRecordSet &Ret_UAR,DbRecordSet &Ret_GAR)
{
	DbConnectionReserver conn(Ret_pStatus,GetDbManager());
	if (!Ret_pStatus.IsOK()) return;

	QString strWhere =" BCNT_ID = "+QVariant(nContactID).toString();
	MainEntityFilter filter;
	filter.SetFilter(MainEntityFilter::FILTER_SQL_WHERE,strWhere);
	ReadData(Ret_pStatus,filter.Serialize(),Ret_Data,nSubDataFilter);
	if (!Ret_pStatus.IsOK()) return;

	g_BusinessServiceSet->BusNMRX->ReadMultiPairs(Ret_pStatus,DataNMRXPersonFilter,Ret_DataNMRXPerson);
	if(!Ret_pStatus.IsOK()) return;
	g_BusinessServiceSet->BusNMRX->ReadMultiPairs(Ret_pStatus,DataNMRXProjectFilter,Ret_DataNMRXProject);
	if(!Ret_pStatus.IsOK()) return;

	//read uar/gar:
	g_BusinessServiceSet->AccessRights->ReadGUAR(Ret_pStatus,nContactID,BUS_CM_CONTACT,Ret_UAR,Ret_GAR);
}

//read contact list in short format based on contact nmrx
void Service_BusContact::ReadNMRXContactsFromContact(Status &Ret_pStatus, int nContactID,DbRecordSet &Ret_Data)
{
	//just to avoid non stop connection reservation
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_CM_CONTACT, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	//Read 1st NMRX: Contact on KEY1
	DbRecordSet lstNMRX_1;
	g_BusinessServiceSet->BusNMRX->ReadOnePair(Ret_pStatus,BUS_CM_CONTACT,BUS_CM_CONTACT,nContactID,-1,lstNMRX_1);
	if(!Ret_pStatus.IsOK()) return;


	TableOrm.ReadFromParentIDs(Ret_pStatus,Ret_Data,"BCNT_ID",lstNMRX_1,"BNMR_TABLE_KEY_ID_2","","",DbSqlTableView::TVIEW_BUS_CONTACT_SELECTION);
	if(!Ret_pStatus.IsOK()) return;



	//Read 2nd NMRX: Contact on KEY2
	g_BusinessServiceSet->BusNMRX->ReadOnePair(Ret_pStatus,BUS_CM_CONTACT,BUS_CM_CONTACT,-1,nContactID,lstNMRX_1);
	if(!Ret_pStatus.IsOK()) return;


	DbRecordSet tempList;
	TableOrm.ReadFromParentIDs(Ret_pStatus,tempList,"BCNT_ID",lstNMRX_1,"BNMR_TABLE_KEY_ID_1","","",DbSqlTableView::TVIEW_BUS_CONTACT_SELECTION);
	if(!Ret_pStatus.IsOK()) return;

	//merge
	Ret_Data.merge(tempList);

	//calculated names: 
	MainEntityCalculatedName filler;
	filler.Initialize(ENTITY_BUS_CONTACT,Ret_Data);
	filler.FillCalculatedNames(Ret_Data,Ret_Data.getColumnIdx("BCNT_NAME"));

}

//read role data and contact data of all contacts assigned to the nContactID
//Ret_NMRXData: TVIEW_BUS_NMRX_RELATION_SELECT
//Ret_ContactData: TVIEW_BUS_CM_CONTACT
//one on one lists..
void Service_BusContact::ReadNMRXContactsFromContact_Ext(Status &Ret_pStatus, int nContactID,DbRecordSet &Ret_NMRXData,DbRecordSet &Ret_ContactData)
{
	//just to avoid non stop connection reservation
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_CM_CONTACT, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	Ret_ContactData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_CONTACT));

	//Read 1st NMRX: Contact on KEY1
	g_BusinessServiceSet->BusNMRX->ReadOnePair(Ret_pStatus,BUS_CM_CONTACT,BUS_CM_CONTACT,nContactID,-1,Ret_NMRXData);
	if(!Ret_pStatus.IsOK()) return;

	int nSize=Ret_NMRXData.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		DbRecordSet rowData;
		TableOrm.Read(Ret_pStatus,rowData," WHERE BCNT_ID="+Ret_NMRXData.getDataRef(i,"BNMR_TABLE_KEY_ID_2").toString());
		Ret_ContactData.merge(rowData);
	}

	Ret_NMRXData.addColumn(QVariant::Int,"IS_AB");
	Ret_NMRXData.setColValue("IS_AB",1);

	//Read 2nd NMRX: Contact on KEY2
	DbRecordSet lstNMRX_1;
	g_BusinessServiceSet->BusNMRX->ReadOnePair(Ret_pStatus,BUS_CM_CONTACT,BUS_CM_CONTACT,-1,nContactID,lstNMRX_1);
	if(!Ret_pStatus.IsOK()) return;

	lstNMRX_1.addColumn(QVariant::Int,"IS_AB");
	lstNMRX_1.setColValue("IS_AB",0);

	Ret_NMRXData.merge(lstNMRX_1);

	nSize=lstNMRX_1.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		DbRecordSet rowData;
		TableOrm.Read(Ret_pStatus,rowData," WHERE BCNT_ID="+lstNMRX_1.getDataRef(i,"BNMR_TABLE_KEY_ID_1").toString());
		Ret_ContactData.merge(rowData);
	}
}

//read role data and contact data of all contacts assigned to the nContactID
//Ret_NMRXData: TVIEW_BUS_NMRX_RELATION_SELECT
//Ret_ContactData: TVIEW_BUS_CM_CONTACT
//one on one lists..
void Service_BusContact::ReadNMRXUsersFromContact_Ext(Status &Ret_pStatus, int nContactID,DbRecordSet &Ret_NMRXData,DbRecordSet &Ret_UserData)
{
	//just to avoid non stop connection reservation
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_CM_CONTACT, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	DbSimpleOrm TableOrm_Pers(Ret_pStatus, BUS_PERSON, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	DbRecordSet rowContact;
	rowContact.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_CONTACT));
	Ret_UserData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_PERSON));
	Ret_UserData.copyDefinition(rowContact,false);


	//Read 1st NMRX: Contact on KEY1
	g_BusinessServiceSet->BusNMRX->ReadOnePair(Ret_pStatus,BUS_CM_CONTACT,BUS_PERSON,nContactID,-1,Ret_NMRXData);
	if(!Ret_pStatus.IsOK()) return;

	int nSize=Ret_NMRXData.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		DbRecordSet rowDataPers;
		TableOrm_Pers.Read(Ret_pStatus,rowDataPers," WHERE BPER_ID="+Ret_NMRXData.getDataRef(i,"BNMR_TABLE_KEY_ID_2").toString());

		int nPersCntID=rowDataPers.getDataRef(0,"BPER_CONTACT_ID").toInt();
		DbRecordSet rowDataCnt;
		TableOrm.Read(Ret_pStatus,rowDataCnt," WHERE BCNT_ID="+QString::number(nPersCntID));

		if (rowDataCnt.getRowCount()==0)
			rowDataCnt.addRow();
		DbRecordSet rowData;
		rowData.copyDefinition(Ret_UserData);
		rowData.merge(rowDataPers);
		if (rowData.getRowCount()==1)
			rowData.assignRow(0,rowDataCnt,true);
		Ret_UserData.merge(rowData);
	}

	Ret_NMRXData.addColumn(QVariant::Int,"IS_AB");
	Ret_NMRXData.setColValue("IS_AB",1);

	//Read 2nd NMRX: Contact on KEY2
	DbRecordSet lstNMRX_1;
	g_BusinessServiceSet->BusNMRX->ReadOnePair(Ret_pStatus,BUS_PERSON,BUS_CM_CONTACT,-1,nContactID,lstNMRX_1);
	if(!Ret_pStatus.IsOK()) return;

	lstNMRX_1.addColumn(QVariant::Int,"IS_AB");
	lstNMRX_1.setColValue("IS_AB",0);

	Ret_NMRXData.merge(lstNMRX_1);

	nSize=lstNMRX_1.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		DbRecordSet rowDataPers;
		TableOrm_Pers.Read(Ret_pStatus,rowDataPers," WHERE BPER_ID="+Ret_NMRXData.getDataRef(i,"BNMR_TABLE_KEY_ID_1").toString());

		int nPersCntID=rowDataPers.getDataRef(0,"BPER_CONTACT_ID").toInt();
		DbRecordSet rowDataCnt;
		TableOrm.Read(Ret_pStatus,rowDataCnt," WHERE BCNT_ID="+QString::number(nPersCntID));

		if (rowDataCnt.getRowCount()==0)
			rowDataCnt.addRow();
		DbRecordSet rowData;
		rowData.copyDefinition(Ret_UserData);
		rowData.merge(rowDataPers);
		if (rowData.getRowCount()==1)
			rowData.assignRow(0,rowDataCnt,true);
		Ret_UserData.merge(rowData);

	}
}


//read contact list in short format based on project 
void Service_BusContact::ReadNMRXContactsFromProject(Status &Ret_pStatus, int nProjectID,DbRecordSet &Ret_Data)
{
	//just to avoid non stop connection reservation
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_CM_CONTACT, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	//Read 2nd NMRX: Contact on KEY1
	DbRecordSet lstNMRX_1;
	g_BusinessServiceSet->BusNMRX->ReadOnePair(Ret_pStatus,BUS_CM_CONTACT,BUS_PROJECT,-1,nProjectID,lstNMRX_1);
	if(!Ret_pStatus.IsOK()) return;


	TableOrm.ReadFromParentIDs(Ret_pStatus,Ret_Data,"BCNT_ID",lstNMRX_1,"BNMR_TABLE_KEY_ID_1","","",DbSqlTableView::TVIEW_BUS_CONTACT_SELECTION);

	//calculated names: 
	MainEntityCalculatedName filler;
	filler.Initialize(ENTITY_BUS_CONTACT,Ret_Data);
	filler.FillCalculatedNames(Ret_Data,Ret_Data.getColumnIdx("BCNT_NAME"));


}

//read project list in short format based on contact (only contact->project)
void Service_BusContact::ReadNMRXProjects(Status &Ret_pStatus, int nContactID,DbRecordSet &Ret_Data)
{
	//just to avoid non stop connection reservation
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_PROJECT, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	//Read 2nd NMRX: Contact on KEY1
	DbRecordSet lstNMRX_1;
	g_BusinessServiceSet->BusNMRX->ReadOnePair(Ret_pStatus,BUS_CM_CONTACT,BUS_PROJECT,nContactID,-1,lstNMRX_1);
	if(!Ret_pStatus.IsOK()) return;


	TableOrm.ReadFromParentIDs(Ret_pStatus,Ret_Data,"BUSP_ID",lstNMRX_1,"BNMR_TABLE_KEY_ID_2",""," ORDER BY BUSP_CODE" /*LEVEL,BUSP_PARENT"*/,DbSqlTableView::TVIEW_BUS_PROJECT_SELECT);
}



//reads contact default: phone, search phone, type, email,type, internet,type, address full, type
//view: TVIEW_BUS_CM_DEAFULTS
void Service_BusContact::ReadContactDefaults(Status &Ret_pStatus, int nContactID, DbRecordSet &Ret_Data)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_CM_CONTACT, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;


	//read defaults:
	QString strWhere=" LEFT OUTER JOIN BUS_CM_EMAIL ON BCNT_ID=BCME_CONTACT_ID AND BCME_IS_DEFAULT=1";
	strWhere+=" LEFT OUTER JOIN BUS_CM_INTERNET ON BCNT_ID=BCMI_CONTACT_ID AND BCMI_IS_DEFAULT=1";
	strWhere+=" LEFT OUTER JOIN BUS_CM_PHONE ON BCNT_ID=BCMP_CONTACT_ID AND BCMP_IS_DEFAULT=1";
	strWhere+=" LEFT OUTER JOIN BUS_CM_ADDRESS ON BCNT_ID=BCMA_CONTACT_ID AND BCMA_IS_DEFAULT=1";
	strWhere+=" WHERE BCNT_ID="+QVariant(nContactID).toString();

	TableOrm.Read(Ret_pStatus,Ret_Data,strWhere,DbSqlTableView::TVIEW_BUS_CM_DEAFULTS);
}

void Service_BusContact::ReadContactReminderData(Status &Ret_pStatus, int nContactID, int nPersonID,QString strEmail, QString strMobilePhoneNumber)
{
	//Make local query, can fail if connection reservation fails.
	DbSqlQuery query(Ret_pStatus, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	//-------------get Person ID from contact ID, if not then -1
	nPersonID=-1;
	QString strSQL="SELECT BPER_ID FROM BUS_PERSON";
	strSQL+=" WHERE BPER_CONTACT_ID="+QVariant(nContactID).toString();

	query.Execute(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK()) return;

	if (query.next())
		nPersonID=query.value(0).toInt();

	//-------------get default email address for contact
	strSQL="SELECT BCME_ADDRESS FROM BUS_CM_EMAIL";
	strSQL+=" WHERE BCME_CONTACT_ID="+QVariant(nContactID).toString()+" AND BCME_IS_DEFAULT=1";

	query.Execute(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK()) return;

	if (query.next())
		strEmail=query.value(0).toString();

	//-------------get sms phone number
	strSQL="SELECT BCMP_FULLNUMBER FROM BUS_CM_PHONE";
	strSQL+=" LEFT OUTER JOIN BUS_CM_TYPES ON BCMP_TYPE_ID=BCMT_ID";
	strSQL+=" WHERE BCMP_CONTACT_ID="+QVariant(nContactID).toString()+" AND (BCMT_SYSTEM_TYPE ="+QString::number(ContactTypeManager::SYSTEM_PHONE_MOBILE);
	strSQL+=" OR BCMT_SYSTEM_TYPE ="+QString::number(ContactTypeManager::SYSTEM_PHONE_PRIVATE_MOBILE)+") SORT BY BCMT_SYSTEM_TYPE";

	query.Execute(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK()) return;

	if (query.next())
		strMobilePhoneNumber=query.value(0).toString();
}
void Service_BusContact::AddContactEmail(Status &Ret_pStatus, DbRecordSet &RetOut_EmailData)
{
	//just to avoid non stop connection reservation
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_CM_EMAIL, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	TableOrm.Write(Ret_pStatus, RetOut_EmailData);
}


//to avoid 4x time call to server: put all in one method:
void Service_BusContact::ReadContactDataForVoiceCall(Status &Ret_pStatus, int nContactID, DbRecordSet &Ret_Phones,DbRecordSet &Ret_Pictures,DbRecordSet &Ret_NMRXContacts,DbRecordSet &Ret_NMRXProjects )
{
	DbConnectionReserver conn(Ret_pStatus,GetDbManager());
	if (!Ret_pStatus.IsOK()) return;

	ReadContactPhones(Ret_pStatus,nContactID,Ret_Phones);
	if (!Ret_pStatus.IsOK()) return;
	
	ReadPreviewPicture(Ret_pStatus,nContactID,Ret_Pictures);//to fix one call
	if (!Ret_pStatus.IsOK()) return;

	ReadNMRXContactsFromContact(Ret_pStatus,nContactID,Ret_NMRXContacts);
	if (!Ret_pStatus.IsOK()) return;

	ReadNMRXProjects(Ret_pStatus,nContactID,Ret_NMRXProjects);
	if (!Ret_pStatus.IsOK()) return;
}



//on startup, contact is created, copy all data to person:
void Service_BusContact::CreateDefaultPerson(Status &Ret_pStatus,int nContactID)
{
	//just to avoid non stop connection reservation
	DbSimpleOrm TableOrmContact(Ret_pStatus, BUS_CM_CONTACT, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	DbSimpleOrm TableOrmPerson(Ret_pStatus, BUS_PERSON, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	DbSimpleOrm TableOrmOrg(Ret_pStatus, BUS_ORGANIZATION, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	//load contact
	DbRecordSet lstContact;
	TableOrmContact.Read(Ret_pStatus,lstContact," WHERE BCNT_ID="+QVariant(nContactID).toString());
	if(!Ret_pStatus.IsOK()) return;
	if (lstContact.getRowCount()==0)
	{
		Ret_pStatus.setError(StatusCodeSet::ERR_BUS_CONTACT_NOT_EXISTS,QVariant(nContactID).toString());
		return;
	}

	//load big pic if exists:
	int nBigPicID=lstContact.getDataRef(0,"BCNT_BIG_PICTURE_ID").toInt();
	DbRecordSet lstPic;
	if (nBigPicID!=0)
	{
		ReadBigPicture(Ret_pStatus,nBigPicID,lstPic);
		if(!Ret_pStatus.IsOK()) return;
	}

	//load org
	DbRecordSet lstOrg;
	TableOrmOrg.Read(Ret_pStatus,lstOrg," WHERE BORG_CODE='ORG.'");
	if(!Ret_pStatus.IsOK()) return;


	//load default pers, if not found->insert mode
	DbRecordSet lstPerson;
	TableOrmPerson.Read(Ret_pStatus,lstPerson," WHERE CUSR_IS_DEFAULT_ACC=1" ,DbSqlTableView::TVIEW_BUS_PERSON_SELECTION);
	if(!Ret_pStatus.IsOK()) return;
	int nPersonID;
	if (lstPerson.getRowCount()>0)
		nPersonID=lstPerson.getDataRef(0,"BPER_ID").toInt();
	else
	{
		Ret_pStatus.setError(StatusCodeSet::ERR_BUS_PERSON_NOT_EXISTS);
		return;
	}

	//_DUMP(lstPerson)

	//qDebug()<<lstContact.getDataRef(0,"BCNT_PICTURE").toByteArray().size();
	//qDebug()<<lstPerson.getDataRef(0,"BPER_PICTURE").toByteArray().size();

	//transform data
	BusinessDataTransformer::Contact2Person(lstContact,lstPerson);
	if (lstPerson.getRowCount()==0)
	{
		Ret_pStatus.setError(1,QObject::tr("Operation failed, default person does not exists!"));
		return;
	}

	//set org id:
	lstPerson.setData(0,"BPER_ID",nPersonID);
	if (lstOrg.getRowCount()>0)
	{
		lstPerson.setData(0,"BPER_ORGANIZATION_ID",lstOrg.getDataRef(0,"BORG_ID"));
	}
	
	//copy picture perview:
	lstPerson.setData(0,"BPER_PICTURE",lstContact.getDataRef(0,"BCNT_PICTURE"));

	//---------------------------------------------
	//		START CREAATION:
	//---------------------------------------------

	//qDebug()<<lstContact.getDataRef(0,"BCNT_PICTURE").toByteArray().size();
	//qDebug()<<lstPerson.getDataRef(0,"BPER_PICTURE").toByteArray().size();


	TableOrmContact.GetDbSqlQuery()->BeginTransaction(Ret_pStatus);
	if(!Ret_pStatus.IsOK()) return;


	//save pic:
	if (nBigPicID!=0 && lstPic.getRowCount()!=0)
	{
		lstPic.setData(0,"BPIC_ID",QVariant(QVariant::Int)); //set to NULL
		WriteBigPicture(Ret_pStatus,lstPic);
		if(!Ret_pStatus.IsOK())
		{
			TableOrmContact.GetDbSqlQuery()->Rollback();
			return;
		}

		nBigPicID=lstPic.getDataRef(0,"BPIC_ID").toInt();
		lstPerson.setData(0,"BPER_BIG_PICTURE_ID",nBigPicID);
	}


	//save person:
	TableOrmPerson.Write(Ret_pStatus,lstPerson,-1,1);
	if(!Ret_pStatus.IsOK())
	{
		TableOrmContact.GetDbSqlQuery()->Rollback();
		return;
	}

	//get default org:
	if (lstOrg.getRowCount()>0)
	{
		lstOrg.setData(0,"BORG_NAME",lstContact.getDataRef(0,"BCNT_ORGANIZATIONNAME"));
		TableOrmOrg.Write(Ret_pStatus,lstOrg);
		if(!Ret_pStatus.IsOK())
		{
			TableOrmContact.GetDbSqlQuery()->Rollback();
			return;
		}
	}


	//commit:
	TableOrmContact.GetDbSqlQuery()->Commit(Ret_pStatus);
}



//copy pers 2 org: Contacts (BCNT_ID), Ret_Data is returned with new orgs in format: TVIEW_BUS_CONTACT_SELECTION
void Service_BusContact::CopyPerson2OrgContact(Status &Ret_pStatus,DbRecordSet Contacts,DbRecordSet lstGroups, DbRecordSet &Ret_Data)
{
	DbConnectionReserver conn(Ret_pStatus, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	//------------------------------------READ PERSON CONTACTS
	int nCount=0;
	QString strWhere,strWhereIn;
	DbRecordSet lstChunkRead;
	DbRecordSet lstContactDataPerson;
	lstContactDataPerson.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CONTACT_FULL));

	while (nCount>=0)
	{
		//compile FK in (n1,n2,...) statement
		nCount=DbSqlTableDefinition::GenerateChunkedWhereStatement(Contacts,"BCNT_ID",strWhereIn,nCount);
		if (nCount<0) break;
		strWhere=" WHERE BCNT_ID IN "+strWhereIn;
		MainEntityFilter filter;
		filter.SetFilter(MainEntityFilter::FILTER_SQL_WHERE,strWhere);
		ReadData(Ret_pStatus,filter.Serialize(),lstChunkRead,ContactTypeManager::USE_SUBDATA_ALL);
		if(!Ret_pStatus.IsOK())return;
		lstContactDataPerson.merge(lstChunkRead);    //merge
	}


	//read big picture for all main records
	g_PrivateServiceSet->BusinessHelper->ReadEntityPictures(Ret_pStatus,lstContactDataPerson,"BCNT_BIG_PICTURE_ID");
	if(!Ret_pStatus.IsOK())return;
	lstContactDataPerson.setColValue(lstContactDataPerson.getColumnIdx("BCNT_ID"),0);
	lstContactDataPerson.setColValue(lstContactDataPerson.getColumnIdx("BCNT_BIG_PICTURE_ID"),0);
	lstContactDataPerson.setColValue(lstContactDataPerson.getColumnIdx("BCNT_TYPE"),ContactTypeManager::CM_TYPE_ORGANIZATION);


	//------------------------------------FILTER & PREPARE DATA BASED ON GROUPS:
	int nSize=lstContactDataPerson.getRowCount();
	for(int i=0;i<nSize;i++)
	{
		//alter data (issue 813):
		if (lstContactDataPerson.getDataRef(i,"BCNT_ORGANIZATIONNAME").toString().isEmpty())
		{
			QString strOrg;
			if (lstContactDataPerson.getDataRef(i,"BCNT_LOCATION").toString().isEmpty())
				strOrg=lstContactDataPerson.getDataRef(i,"BCNT_LASTNAME").toString();
			else
				strOrg=lstContactDataPerson.getDataRef(i,"BCNT_LASTNAME").toString()+", "+lstContactDataPerson.getDataRef(i,"BCNT_LOCATION").toString();
			lstContactDataPerson.setData(i,"BCNT_ORGANIZATIONNAME",strOrg);
		}
		lstContactDataPerson.setData(i,"BCNT_LASTNAME","");
		lstContactDataPerson.setData(i,"BCNT_FIRSTNAME","");

		//alter subgroups:
		DbRecordSet m_lstPhones=lstContactDataPerson.getDataRef(i,"LST_PHONE").value<DbRecordSet>();
		DbRecordSet m_lstEmails=lstContactDataPerson.getDataRef(i,"LST_EMAIL").value<DbRecordSet>();
		DbRecordSet m_lstInternet=lstContactDataPerson.getDataRef(i,"LST_INTERNET").value<DbRecordSet>();
		DbRecordSet m_lstAddress=lstContactDataPerson.getDataRef(i,"LST_ADDRESS").value<DbRecordSet>();
		DbRecordSet m_lstPictures=lstContactDataPerson.getDataRef(i,"LST_PICTURES").value<DbRecordSet>();
		DbRecordSet m_lstDebtor=lstContactDataPerson.getDataRef(i,"LST_DEBTOR").value<DbRecordSet>();

		if (lstGroups.find(0,QVariant(ContactTypeManager::GROUP_PICTURE).toInt(),true)>=0)
		{
			//read big pictures for pic records, reset id's
			g_PrivateServiceSet->BusinessHelper->ReadEntityPictures(Ret_pStatus,m_lstPictures,"BCMPC_BIG_PICTURE_ID");
			if (!Ret_pStatus.IsOK()) return;
			m_lstPictures.setColValue(m_lstPictures.getColumnIdx("BCMPC_ID"),0);
			m_lstPictures.setColValue(m_lstPictures.getColumnIdx("BCMPC_BIG_PICTURE_ID"),0);
		}
		else
			m_lstPictures.clear();

		m_lstPhones.setColValue(m_lstPhones.getColumnIdx("BCMP_ID"),0);
		m_lstEmails.setColValue(m_lstEmails.getColumnIdx("BCME_ID"),0);
		m_lstInternet.setColValue(m_lstInternet.getColumnIdx("BCMI_ID"),0);
		m_lstDebtor.setColValue(m_lstDebtor.getColumnIdx("BCMD_ID"),0);

		//reset all types sublists:
		int nSize=m_lstAddress.getRowCount();
		for(int k=0;k<nSize;k++)
		{
			DbRecordSet lstType=m_lstAddress.getDataRef(k,"LST_TYPES").value<DbRecordSet>();
			lstType.setColValue(lstType.getColumnIdx("BCMAT_ID"),0);
			m_lstAddress.setData(k,"LST_TYPES",lstType);
		}
		m_lstAddress.setColValue(m_lstAddress.getColumnIdx("BCMA_ID"),0);

		if (lstGroups.find(0,QVariant(ContactTypeManager::GROUP_ADDRESS).toInt(),true)<0)
			m_lstAddress.clear();
		if (lstGroups.find(0,QVariant(ContactTypeManager::GROUP_PHONE).toInt(),true)<0)
			m_lstPhones.clear();
		if (lstGroups.find(0,QVariant(ContactTypeManager::GROUP_EMAIL).toInt(),true)<0)
			m_lstEmails.clear();
		if (lstGroups.find(0,QVariant(ContactTypeManager::GROUP_INTERNET).toInt(),true)<0)
			m_lstInternet.clear();
		//if (lstGroups.find(0,ContactTypeManager::GROUP_CREDITOR,true)<0)
		//	m_lstPictures.clear();
		if (lstGroups.find(0,QVariant(ContactTypeManager::GROUP_DEBTOR).toInt(),true)<0)
			m_lstDebtor.clear();

		lstContactDataPerson.setData(i,"LST_PHONE",m_lstPhones);
		lstContactDataPerson.setData(i,"LST_ADDRESS",m_lstAddress);
		lstContactDataPerson.setData(i,"LST_EMAIL",m_lstEmails);
		lstContactDataPerson.setData(i,"LST_INTERNET",m_lstInternet);
		lstContactDataPerson.setData(i,"LST_PICTURES",m_lstPictures);
		lstContactDataPerson.setData(i,"LST_DEBTOR",m_lstDebtor);
	}

	//------------------------------------WRITE NEW DATA:
	DbRecordSet dummy;
	WriteData(Ret_pStatus,lstContactDataPerson,ContactTypeManager::USE_SUBDATA_ALL,"",dummy,dummy);
	if(!Ret_pStatus.IsOK()) return;

	//------------------------------------READ NEW DATA IN SHORT FORMAT:
	strWhereIn="";
	DbSqlTableDefinition::GenerateChunkedWhereStatement(lstContactDataPerson,"BCNT_ID",strWhereIn,0,-1);
	MainEntityFilter filter;
	filter.SetFilter(MainEntityFilter::FILTER_SQL_WHERE," WHERE BCNT_ID IN "+strWhereIn);
	ReadShortContactList(Ret_pStatus,filter.Serialize(),Ret_Data);

}



//find duplicates:
//Contacts is in TVIEW_BUS_CONTACT_SELECTION or (find duplos on actual list)
//in TVIEW_BUS_CM_CONTACT_SELECT_PHONETS (with defined phonets) (before insert)
void Service_BusContact::FindDuplicates(Status &Ret_pStatus,DbRecordSet &Contacts,DbRecordSet &Ret_Data)
{
	//just to avoid non stop connection reservation
	DbSimpleOrm TableOrmContact(Ret_pStatus, BUS_CM_CONTACT, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;


	DbRecordSet lstData;
	lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_CONTACT_SELECT_PHONETS));
	Ret_Data.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CONTACT_SELECTION));

	//if short list, first read all phonet contact data
	if (Contacts.getColumnIdx("BCNT_LASTNAME_PHONET")==-1)
	{
		TableOrmContact.ReadFromParentIDs(Ret_pStatus,lstData,"BCNT_ID",Contacts,"BCNT_ID","","",DbSqlTableView::TVIEW_BUS_CM_CONTACT_SELECT_PHONETS);
		if(!Ret_pStatus.IsOK()) return;
	}
	else
	{
		lstData.merge(Contacts);
	}

	QString strSelectionCols=DbSqlTableView::getSQLColumnsFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CONTACT_SELECTION));
	
	//QString strSelectionOrg=" AND BCNT_LOCATION_PHONET NOT IS NULL AND BCNT_LOCATION_PHONET <> '' AND BCNT_LOCATION_PHONET=?";
	


	//read one by one duplicates from DB: duplicates are even if contact has empty org->only by first, last
	//ORG? only by org!!!
	int nSize=lstData.getRowCount();
	DbSqlQuery *query=TableOrmContact.GetDbSqlQuery();;
	
	for(int i=0;i<nSize;++i)
	{
		if (lstData.getDataRef(i,"BCNT_TYPE").toInt()==ContactTypeManager::CM_TYPE_ORGANIZATION)
		{
			QString strSelectionOrg="SELECT "+ strSelectionCols+ " FROM BUS_CM_CONTACT WHERE BCNT_ORGANIZATIONNAME_PHONET=? AND BCNT_TYPE="+QVariant(ContactTypeManager::CM_TYPE_ORGANIZATION).toString()+" AND NOT (BCNT_ID= ?)";
			bool bLoc=false;
			//if got location: issue 1617
			if (!lstData.getDataRef(i,"BCNT_LOCATION_PHONET").toString().isEmpty())
			{
				bLoc=true;
				strSelectionOrg+=" AND (BCNT_LOCATION_PHONET =? AND BCNT_LOCATION_PHONET<>'' OR BCNT_LOCATION_PHONET='')"; 
			}

			query->Prepare(Ret_pStatus,strSelectionOrg);
			if(!Ret_pStatus.IsOK()) return;
			query->bindValue(0,lstData.getDataRef(i,"BCNT_ORGANIZATIONNAME_PHONET").toString());
			query->bindValue(1,lstData.getDataRef(i,"BCNT_ID").toInt());
			if (bLoc)
			{
				query->bindValue(2,lstData.getDataRef(i,"BCNT_LOCATION_PHONET").toString());
			}

			query->ExecutePrepared(Ret_pStatus);
			if(!Ret_pStatus.IsOK()) return;
			
			DbRecordSet lstFetch;
			query->FetchData(lstFetch);
		
			if (lstFetch.getRowCount()>0)
			{
				Ret_Data.merge(lstFetch);
			}
		}
		else
		{
			//search by first, last
			bool bOrg=false;
			bool bLoc=false;
			QString strSelectionPers="SELECT "+ strSelectionCols+ " FROM BUS_CM_CONTACT WHERE BCNT_LASTNAME_PHONET=? AND BCNT_FIRSTNAME_PHONET=? AND BCNT_TYPE="+QVariant(ContactTypeManager::CM_TYPE_PERSON).toString();
			strSelectionPers+=+" AND NOT (BCNT_ID= "+QVariant(lstData.getDataRef(i,"BCNT_ID").toInt()).toString()+")";

			//if got org:
			if (!lstData.getDataRef(i,"BCNT_ORGANIZATIONNAME_PHONET").toString().isEmpty())
			{
				bOrg=true;
				strSelectionPers+=" AND (BCNT_ORGANIZATIONNAME_PHONET =? AND BCNT_ORGANIZATIONNAME_PHONET<>'' OR BCNT_ORGANIZATIONNAME_PHONET='')"; 
				//issue 811: Two person records are considered duplicates, if the first name and the last name are equal and if or at least one has no organisation or both have an equal organization assigned.
			}
			//if got location: issue 1617
			if (!lstData.getDataRef(i,"BCNT_LOCATION_PHONET").toString().isEmpty())
			{
				bLoc=true;
				strSelectionPers+=" AND (BCNT_LOCATION_PHONET =? AND BCNT_LOCATION_PHONET<>'' OR BCNT_LOCATION_PHONET='')"; 
			}

			query->Prepare(Ret_pStatus,strSelectionPers);
			if(!Ret_pStatus.IsOK()) return;
			query->bindValue(0,lstData.getDataRef(i,"BCNT_LASTNAME_PHONET").toString());
			query->bindValue(1,lstData.getDataRef(i,"BCNT_FIRSTNAME_PHONET").toString());
			if (bOrg)
			{
				query->bindValue(2,lstData.getDataRef(i,"BCNT_ORGANIZATIONNAME_PHONET").toString());
			}
			if (bLoc)
			{
				if (bOrg)
					query->bindValue(3,lstData.getDataRef(i,"BCNT_LOCATION_PHONET").toString());
				else
					query->bindValue(2,lstData.getDataRef(i,"BCNT_LOCATION_PHONET").toString());
			}


			query->ExecutePrepared(Ret_pStatus);
			if(!Ret_pStatus.IsOK()) return;

			DbRecordSet lstFetch;
			query->FetchData(lstFetch);
			if (lstFetch.getRowCount()>0)
			{
				Ret_Data.merge(lstFetch);
			}

		}

	}


	//Ret_Data.Dump();

	//calculated names: 
	MainEntityCalculatedName filler;
	filler.Initialize(ENTITY_BUS_CONTACT,Ret_Data);
	filler.FillCalculatedNames(Ret_Data,Ret_Data.getColumnIdx("BCNT_NAME"));

}

void Service_BusContact::GetMaximumDebtorCode(Status &Ret_pStatus,QString &Ret_DebtorCode)
{
	//Make local query, can fail if connection reservation fails.
	DbSqlQuery query(Ret_pStatus, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;


	QString strSQL =" SELECT MAX(BCMD_DEBTORCODE) FROM BUS_CM_DEBTOR";

	query.Execute(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK()) return;

	Ret_DebtorCode="";
	if(query.next())
		Ret_DebtorCode = query.value(0).toString();
}

void Service_BusContact::ReadContactDataSideBar(Status &Ret_pStatus, int nContactID,DbRecordSet &Ret_Data,DbRecordSet &DataNMRXPersonFilter,DbRecordSet &Ret_DataNMRXPerson,DbRecordSet &DataNMRXProjectFilter,DbRecordSet &Ret_DataNMRXProject,bool bLoadPicture, DbRecordSet &Ret_Pictures)
{

	DbConnectionReserver conn(Ret_pStatus,GetDbManager());
	if (!Ret_pStatus.IsOK()) return;

	MainEntityFilter filter;
	QString strWhere ="BCNT_ID = "+QVariant(nContactID).toString();
	filter.SetFilter(MainEntityFilter::FILTER_SQL_WHERE,strWhere);
	ReadData(Ret_pStatus,filter.Serialize(),Ret_Data,0);
	if(!Ret_pStatus.IsOK()) return;

	if (bLoadPicture)
	{
		ReadPreviewPicture(Ret_pStatus,nContactID,Ret_Pictures);//to fix one call
		if (!Ret_pStatus.IsOK()) return;
	}

	g_BusinessServiceSet->BusNMRX->ReadMultiPairs(Ret_pStatus,DataNMRXPersonFilter,Ret_DataNMRXPerson);
	if(!Ret_pStatus.IsOK()) return;

	g_BusinessServiceSet->BusNMRX->ReadMultiPairs(Ret_pStatus,DataNMRXProjectFilter,Ret_DataNMRXProject);
	if(!Ret_pStatus.IsOK()) return;

}


/*!
	Write  contacts in TVIEW_BUS_CONTACT_FULL format, 
	BUT:
	- BGCN fields are ignored
	- LST_JOURNAL and LST_GROUPS, LST_PICTURES, LST_DEBTOR,LST_CREDITOR are ignored

	NOTE:	if operation is 0 or 1, then all operations are aborted if one reocord failed->transaction Ret_pStatus=last error
			if op=2, everything is written without transaction if error then it is returned in fields STATUS_CODE & TEXT

	\param Ret_pStatus		- returns error (RETURNED)
	\param Ret_Data			- returned data in TVIEW_BUS_CONTACT_FULL
	\param pDbConn			- db connection

*/
void Service_BusContact::WriteDataFromImport(Status &Ret_pStatus,DbRecordSet &RetOut_Data)
{

	DbSimpleOrm TableOrm(Ret_pStatus, BUS_CM_CONTACT, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	DbSimpleOrm TableOrm_2(Ret_pStatus, BUS_CM_DEBTOR, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;


	//calc cols for contact:
	DbRecordSet lstContactStatuses;


	//-------------------------------------
	//			WRITE PICTURES:
	//-------------------------------------
	g_PrivateServiceSet->BusinessHelper->WriteEntityPictures(Ret_pStatus,RetOut_Data,"BCNT_BIG_PICTURE_ID","BCNT_PICTURE");
	if(!Ret_pStatus.IsOK()) return;


	//-------------------------------------
	//			WRITE CONTACTS
	//-------------------------------------
	int nSkipLastCols=DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CONTACT_FULL).m_nSkipLastCols;
	TableOrm.Write(Ret_pStatus,RetOut_Data,-1,nSkipLastCols,&lstContactStatuses);
	if(!Ret_pStatus.IsOK()) return;

	//copy statuses back to list:
	RetOut_Data.assignRow(lstContactStatuses,true,false);


	//-------------------------------------
	//			WRITE PHONES
	//-------------------------------------
	DbRecordSet lstSubData,lstSubDataStatuses;
	int nContIDIdx;
	if(lstContactStatuses.find(1,0,false,false,true,false)<RetOut_Data.getRowCount()) //only if some contacts are free of errors
	{
		lstSubData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_PHONE));
		nContIDIdx		=	lstSubData.getColumnIdx("BCMP_CONTACT_ID");
		g_PrivateServiceSet->BusinessHelper->WriteSubList(BUS_CM_PHONE,RetOut_Data.getColumnIdx("LST_PHONE"),nContIDIdx,0,RetOut_Data,lstContactStatuses,lstSubData,lstSubDataStatuses);
	}


	//-------------------------------------
	//			WRITE EMAIL
	//-------------------------------------
	if(lstContactStatuses.find(1,0,false,false,true,false)<RetOut_Data.getRowCount()) //only if some contacts are free of errors
	{
		lstSubData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_EMAIL));
		nContIDIdx		=	lstSubData.getColumnIdx("BCME_CONTACT_ID");
		g_PrivateServiceSet->BusinessHelper->WriteSubList(BUS_CM_EMAIL,RetOut_Data.getColumnIdx("LST_EMAIL"),nContIDIdx,0,RetOut_Data,lstContactStatuses,lstSubData,lstSubDataStatuses);
	}

	//-------------------------------------
	//			WRITE INTERNET
	//-------------------------------------
	if(lstContactStatuses.find(1,0,false,false,true,false)<RetOut_Data.getRowCount()) //only if some contacts are free of errors
	{
		lstSubData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_INTERNET));
		nContIDIdx	=	lstSubData.getColumnIdx("BCMI_CONTACT_ID");
		g_PrivateServiceSet->BusinessHelper->WriteSubList(BUS_CM_INTERNET,RetOut_Data.getColumnIdx("LST_INTERNET"),nContIDIdx,0,RetOut_Data,lstContactStatuses,lstSubData,lstSubDataStatuses);
	}

	//-------------------------------------
	//			WRITE JOURNAL
	//-------------------------------------
	if(lstContactStatuses.find(1,0,false,false,true,false)<RetOut_Data.getRowCount()) //only if some contacts are free of errors
	{
		lstSubData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_JOURNAL));
		nContIDIdx	=	lstSubData.getColumnIdx("BCMJ_CONTACT_ID");
		g_PrivateServiceSet->BusinessHelper->WriteSubList(BUS_CM_JOURNAL,RetOut_Data.getColumnIdx("LST_JOURNAL"),nContIDIdx,0,RetOut_Data,lstContactStatuses,lstSubData,lstSubDataStatuses);
	}


	//-------------------------------------
	//			WRITE ADDRESS
	//-------------------------------------
	DbRecordSet lstAddress,lstAddressStatus;
	if(lstContactStatuses.find(1,0,false,false,true,false)<RetOut_Data.getRowCount()) //only if some contacts are free of errors
	{
		lstAddress.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_ADDRESS));
		lstAddress.addColumn(DbRecordSet::GetVariantType(),"LST_TYPES");	//add special col
		nContIDIdx	=	lstAddress.getColumnIdx("BCMA_CONTACT_ID");
		g_PrivateServiceSet->BusinessHelper->WriteSubList(BUS_CM_ADDRESS,RetOut_Data.getColumnIdx("LST_ADDRESS"),nContIDIdx,1,RetOut_Data,lstContactStatuses,lstAddress,lstAddressStatus);
	}

	//--------------------------------------------------
	//			WRITE ADDRESS TYPES
	//---------------------------------------------------
	if(lstContactStatuses.find(1,0,false,false,true,false)<RetOut_Data.getRowCount()) //only if some contacts are free of errors
	{
		lstSubData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_ADDRESS_TYPES));
		nContIDIdx	=	lstSubData.getColumnIdx("BCMAT_ADDRESS_ID");
		g_PrivateServiceSet->BusinessHelper->WriteSubList(BUS_CM_ADDRESS_TYPES,lstAddress.getColumnIdx("LST_TYPES"),nContIDIdx,0,lstAddress,lstAddressStatus,lstSubData,lstSubDataStatuses);

		//if one of types failed, status is written inside lstAddressStatus, copy now to contacts:
		nContIDIdx	=	lstAddress.getColumnIdx("BCMA_CONTACT_ID");
		g_PrivateServiceSet->BusinessHelper->AssignErrorsToParentStatuses(lstAddress,lstContactStatuses,lstAddressStatus,nContIDIdx);
	}

	//-------------------------------------
	//			WRITE DEBTOR for import
	//-------------------------------------
	DbRecordSet lstDebtors;
	lstDebtors.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_DEBTOR));
	lstDebtors.merge(RetOut_Data);
	int nSize=lstDebtors.getRowCount();
	for(int i=0;i<nSize;++i)
		lstDebtors.setData(i,"BCMD_CONTACT_ID",RetOut_Data.getDataRef(i,"BCNT_ID"));
	lstDebtors.find("BCMD_DEBTORCODE",QString(""));
	lstDebtors.deleteSelectedRows();
	//lstDebtors.Dump();
	if (lstDebtors.getRowCount()>0)
	{
		TableOrm_2.Write(Ret_pStatus,lstDebtors);
		//no error check
	}


	//--------------------------------------------------
	//			COMPILE list of error codes (only for import)
	//---------------------------------------------------
	//At this point: all contacts that are marked with STATUS_CODE!=0 inside lstContactStatuses must be deleted if are inserted, ID!=0;
	RetOut_Data.assignRow(lstContactStatuses,true,false);
	lstContactStatuses.find(1,0,false,false,true,false);
	lstContactStatuses.deleteUnSelectedRows();
	//lstContactStatuses.Dump();

	//error occurred:
	if (lstContactStatuses.getRowCount()>0)
	{
			//select all with ID!=0 in errored
			Ret_pStatus.setError(lstContactStatuses.getDataRef(0,1).toInt(),lstContactStatuses.getDataRef(0,2).toString());
			lstContactStatuses.find(0,0,false,true,true,true);
			lstContactStatuses.deleteSelectedRows(); //delete cont with NULL
			//lstContactStatuses.Dump();
			if (lstContactStatuses.getRowCount()>0)
			{
				lstContactStatuses.setColumn(0,QVariant::Int,"BCNT_ID");
				Status err;
				TableOrm.DeleteFast(err,lstContactStatuses);
			}
	}

}




//writes big picture and preview and returns big pic id
void Service_BusContact::AssignPictureContact(Status &Ret_pStatus, int nContactID, QByteArray &PreviewPicture, QByteArray &BigPicture, int &Ret_nBigPicID)
{



	DbRecordSet	lstPictureWrite;
	lstPictureWrite.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_BIG_PICTURE));

	lstPictureWrite.addRow();
	lstPictureWrite.setData(0,"BPIC_PICTURE",BigPicture);
	
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_BIG_PICTURE, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	//first lock contact:
	BusinessLocker locker(Ret_pStatus,BUS_CM_CONTACT,GetDbManager(), TableOrm.GetDbConnection());
	QList<int> lstLock;
	lstLock.append(nContactID);
	QString strLockRes;
	locker.Lock(Ret_pStatus,lstLock,strLockRes);
	if(!Ret_pStatus.IsOK()) return;



	TableOrm.GetDbSqlQuery()->BeginTransaction(Ret_pStatus);
	if(!Ret_pStatus.IsOK())
	{
		Status err;
		locker.UnLock(err,strLockRes);
		return;
	}
	TableOrm.Write(Ret_pStatus,lstPictureWrite);
	
	if(!Ret_pStatus.IsOK()) 
	{
		TableOrm.GetDbSqlQuery()->Rollback();
		Status err;
		locker.UnLock(err,strLockRes);
		return;
	}

	Ret_nBigPicID=lstPictureWrite.getDataRef(0,"BPIC_ID").toInt();

	QString strSQL="UPDATE BUS_CM_CONTACT SET BCNT_PICTURE = ?, BCNT_BIG_PICTURE_ID = ? WHERE BCNT_ID = ?";
	TableOrm.GetDbSqlQuery()->Prepare(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK()) 
	{
		TableOrm.GetDbSqlQuery()->Rollback();
		Status err;
		locker.UnLock(err,strLockRes);
		return;
	}

	TableOrm.GetDbSqlQuery()->bindValue(0,PreviewPicture);
	TableOrm.GetDbSqlQuery()->bindValue(1,Ret_nBigPicID);
	TableOrm.GetDbSqlQuery()->bindValue(2,nContactID);

	
	TableOrm.GetDbSqlQuery()->ExecutePrepared(Ret_pStatus);
	if(!Ret_pStatus.IsOK()) 
	{
		TableOrm.GetDbSqlQuery()->Rollback();
		Status err;
		locker.UnLock(err,strLockRes);
		return;
	}

	TableOrm.GetDbSqlQuery()->Commit(Ret_pStatus);
	if(!Ret_pStatus.IsOK())
	{
		TableOrm.GetDbSqlQuery()->Rollback();
		Status err;
		locker.UnLock(err,strLockRes);
		return;
	}

	locker.UnLock(Ret_pStatus,strLockRes);

}

//updating big pic=0, big pic is removed
void Service_BusContact::RemoveAssignedPictureContact(Status &Ret_pStatus, int nContactID)
{
	//Make local query, can fail if connection reservation fails.
	DbSqlQuery query(Ret_pStatus, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	QString strSQL="UPDATE BUS_CM_CONTACT SET BCNT_BIG_PICTURE_ID = 0, BCNT_PICTURE = NULL WHERE BCNT_ID="+QVariant(nContactID).toString();
	query.Execute(Ret_pStatus,strSQL);

}

void Service_BusContact::UpdateOrganization(Status &Ret_pStatus,QString strPrevOrg,QString strNewOrg)
{

	//Make local query, can fail if connection reservation fails.
	DbSqlQuery query(Ret_pStatus, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	QString strSQL="UPDATE BUS_CM_CONTACT SET BCNT_ORGANIZATIONNAME = ? WHERE BCNT_ORGANIZATIONNAME = ?";
	query.Prepare(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK()) return;
	query.bindValue(0,strNewOrg);
	query.bindValue(1,strPrevOrg);

	query.ExecutePrepared(Ret_pStatus);
}


//lstContacts only BCNT_ID or selection format
void Service_BusContact::CreatePersonFromContact(Status &Ret_pStatus, DbRecordSet &lstContacts,int nOrgID, int &Ret_nPersonCreated)
{

	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_CM_CONTACT, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	DbSimpleOrm TableOrmPers(Ret_pStatus, BUS_PERSON, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	DbSimpleOrm TableOrmUser(Ret_pStatus, CORE_USER, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	QString strWhere=" AND BPER_ID IS NULL AND BCNT_TYPE <> "+QString::number(ContactTypeManager::CM_TYPE_ORGANIZATION);
	//read full contact data
	DbRecordSet lstContactDetails;
	TableOrm.ReadFromParentIDs(Ret_pStatus,lstContactDetails,"BCNT_ID",lstContacts,"BCNT_ID","",strWhere,DbSqlTableView::TVIEW_BUS_CM_CONTACT_SELECT_SHORT, true);
	if(!Ret_pStatus.IsOK()) return;


	//read all org and dept:
	DbRecordSet lstOrgs; 
	TableOrm.GetDbSqlQuery()->Execute(Ret_pStatus,"SELECT BORG_ID,BORG_NAME FROM BUS_ORGANIZATIONS");
	TableOrm.GetDbSqlQuery()->FetchData(lstOrgs);
	DbRecordSet lstDepts; 
	TableOrm.GetDbSqlQuery()->Execute(Ret_pStatus,"SELECT BDEPT_ID, BDEPT_NAME FROM BUS_DEPARTMENTS");
	TableOrm.GetDbSqlQuery()->FetchData(lstDepts);



	//convert2person
	DbRecordSet lstPersons;
	BusinessDataTransformer::Contact2Person(lstContactDetails,lstPersons,true,true, &lstOrgs, &lstDepts);

	//issue 2311:
	if (nOrgID>0)
		lstPersons.setColValue("BPER_ORGANIZATION_ID",nOrgID);

	//check if loign already exists, set username=initials, if exists add 1,2,3...
	DbRecordSet lstLogins;
	TableOrm.GetDbSqlQuery()->Execute(Ret_pStatus,"SELECT DISTINCT CUSR_USERNAME FROM CORE_USER");
	TableOrm.GetDbSqlQuery()->FetchData(lstLogins);

	int nSize=lstPersons.getRowCount();
	for(int i=0;i<nSize;i++)
	{
		QString strInitialsStart=lstPersons.getDataRef(i,"BPER_INITIALS").toString();
		QString strInitialsTest=strInitialsStart;
		int nTry=0;
		while(lstLogins.find(0,strInitialsTest,true)>=0)
		{
			nTry++;
			strInitialsTest=strInitialsStart+" "+QVariant(nTry).toString();
		}
		lstPersons.setData(i,"BPER_INITIALS",strInitialsTest);
	}

	
	//get Admin role:
	QString strModuleCode,strMLID;
	DbRecordSet recRoles;
	int nAdminRoleID=0;
	g_UserSessionManager->GetModuleInfo(strModuleCode,strMLID);
	if("SC-TE" == strModuleCode)
		g_BusinessServiceSet->AccessRights->GetTERoles(Ret_pStatus, recRoles);
	else
		g_BusinessServiceSet->AccessRights->GetBERoles(Ret_pStatus, recRoles);
	int nRowCount = recRoles.getRowCount();
	for (int i = 0; i < nRowCount; ++i)
	{
		int nRoleID = recRoles.getDataRef(i, "CROL_ID").toInt();
		if (recRoles.getDataRef(i, "CROL_CODE").toInt() == ADMINISTRATOR_ROLE)
			nAdminRoleID = nRoleID;
	}


	TableOrm.GetDbSqlQuery()->BeginTransaction(Ret_pStatus);
	if (!Ret_pStatus.IsOK()) return;

	//write person pioctures
	g_PrivateServiceSet->BusinessHelper->WriteEntityPictures(Ret_pStatus,lstPersons,"BPER_BIG_PICTURE_ID","BPER_PICTURE");
	if (!Ret_pStatus.IsOK())
	{
		TableOrm.GetDbSqlQuery()->Rollback();
		return;
	}

	//write persons
	TableOrmPers.Write(Ret_pStatus,lstPersons,-1,1);
	if (!Ret_pStatus.IsOK())
	{
		TableOrm.GetDbSqlQuery()->Rollback();
		return;
	}

	///assign admin role:
	nSize=lstPersons.getRowCount();
	for(int i=0;i<nSize;i++)
	{
		g_BusinessServiceSet->AccessRights->InsertNewRoleToPerson(Ret_pStatus, lstPersons.getDataRef(i,"BPER_ID").toInt(), nAdminRoleID);
		if (!Ret_pStatus.IsOK())
		{
			TableOrm.GetDbSqlQuery()->Rollback();
			return;
		}
	}
	
	//generate user rec/write
	DbRecordSet lstUserLogins;
	BusinessDataTransformer::Person2User(lstPersons,lstUserLogins);


	//try to detect duplicate initials: if duplicate then add counter..

	//QTime time;
	//time.start();
	int nCol=lstUserLogins.getColumnIdx("CUSR_USERNAME");
	lstUserLogins.sort(nCol); //sort asc
	//qDebug()<<"sort:"<<time.elapsed();

	int z=lstUserLogins.getRowCount()-1;
	while(z>0)
	{
		QString value = lstUserLogins.getDataRef(z,nCol).toString(); //get last value
		while(z>0)
		{
			z--;
			if(value==lstUserLogins.getDataRef(z,nCol).toString())
			{
				lstUserLogins.setData(z,nCol,lstUserLogins.getDataRef(z,nCol).toString()+QString::number(z+1));
			}
			else
				break;  //if not same store new value...
		}
	}


	

	TableOrmUser.Write(Ret_pStatus,lstUserLogins);
	if (!Ret_pStatus.IsOK())
	{
		TableOrm.GetDbSqlQuery()->Rollback();
		return;
	}

	Ret_nPersonCreated=nSize;
	TableOrm.GetDbSqlQuery()->Commit(Ret_pStatus);
}


/*!
	Write single/multiple row's of contact's in TVIEW_BUS_CONTACT_FULL format, inside transaction: one fail, all fail!
	- BGCN,BCMD fields are ignored
	- LST_GROUPS is ignored
	- all other LST's are written depending on nSubDataFilter flag:
	nSubDataFilter: bit or'ed flags from contacttypemanager:
	enum SubDataFilter //used on contact read/write to skip some subdata that is not needed in TVIEW_BUS_CONTACT_FULL
	{
	USE_SUBDATA_NONE=0,
	USE_SUBDATA_ALL=1,
	USE_SUBDATA_PICTURE=2,
	USE_SUBDATA_CREDITOR=4,
	USE_SUBDATA_DEBTOR=8,
	USE_SUBDATA_GROUP=16, //not written here
	USE_SUBDATA_JOURNAL=32,	
	};

	\param Ret_pStatus		- returns error (RETURNED)
	\param Ret_Data			- returned data in TVIEW_BUS_CONTACT_FULL
	\param nSubDataFilter	- flags for skipping: LST_PICTURES, LST_CREDITOR, LST_DEBTOR
	\param pLockResourceID	- after write unlock this ressource if non empty
	\param lstUAR			- only valid if 1 contact (or same guar for all contacts): insert/edits in transaction UAR records/fill with id, if list empty, UAR is not changed
	\param lstGAR			- only valid if 1 contact (or same guar for all contacts): insert/edits in transaction UAR records/fill with id, if list empty, UAR is not changed
	\param nSubDataFilter	- flags for skipping: LST_PICTURES, LST_CREDITOR, LST_DEBTOR
	\param pDbConn			- db connection

*/
void Service_BusContact::WriteData(Status &Ret_pStatus,DbRecordSet &RetOut_Data,int nSubDataFilter, QString pLockResourceID, DbRecordSet &RetOut_UAR,DbRecordSet &RetOut_GAR)
{
	//Make local query, can fail if connection reservation fails.
	DbSqlQuery query(Ret_pStatus, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	//lock if not alredy locked
	//-----------------------------------------------
	BusinessLocker Locker(Ret_pStatus, BUS_CM_CONTACT, GetDbManager());
	if(!Ret_pStatus.IsOK()) return;
	bool bLockedFromHere = false;
	if (pLockResourceID.isEmpty())
	{
		Locker.Lock(Ret_pStatus, RetOut_Data, pLockResourceID);
		if(!Ret_pStatus.IsOK()) 
			return;
		bLockedFromHere = true;
	}
	//-----------------------------------------------

	query.BeginTransaction(Ret_pStatus);
	if(!Ret_pStatus.IsOK()) 
	{
		Status err;
		if (bLockedFromHere && !pLockResourceID.isEmpty())
			Locker.UnLock(err, pLockResourceID);
		return;
	}

	int nSize=RetOut_Data.getRowCount();
	for(int i=0;i<nSize;i++)
	{
		DbRecordSet recContact=RetOut_Data.getRow(i);

		WriteOneContact(Ret_pStatus,recContact,nSubDataFilter,RetOut_UAR,RetOut_GAR);
		if(!Ret_pStatus.IsOK()) 
		{
			query.Rollback();
			Status err;
			if (bLockedFromHere && !pLockResourceID.isEmpty())
				Locker.UnLock(err, pLockResourceID);
			return;
		}
		RetOut_Data.assignRow(i,recContact);
	}
	query.Commit(Ret_pStatus);

	if (!pLockResourceID.isEmpty())
		Locker.UnLock(Ret_pStatus, pLockResourceID);
}



/*!
	Write single row of contact in TVIEW_BUS_CONTACT_FULL format, no transaction, start transaction from 'outside'
	- BGCN,BCMD fields are ignored
	- LST_GROUPS is ignored
	- all other LST's are written depending on nSubDataFilter flag:
	nSubDataFilter: bit or'ed flags from contacttypemanager:
	enum SubDataFilter //used on contact read/write to skip some subdata that is not needed in TVIEW_BUS_CONTACT_FULL
	{
	USE_SUBDATA_NONE=0,
	USE_SUBDATA_ALL=1,
	USE_SUBDATA_PICTURE=2,
	USE_SUBDATA_CREDITOR=4,
	USE_SUBDATA_DEBTOR=8,
	USE_SUBDATA_GROUP=16, //not written here
	USE_SUBDATA_JOURNAL=32,	
	};

	\param Ret_pStatus		- returns error (RETURNED)
	\param Ret_Data			- returned data in TVIEW_BUS_CONTACT_FULL
	\param nSubDataFilter	- flags for skipping: LST_PICTURES, LST_CREDITOR, LST_DEBTOR
	\param pDbConn			- db connection

*/
void Service_BusContact::WriteOneContact(Status &Ret_pStatus,DbRecordSet &RetOut_Data,int nSubDataFilter,DbRecordSet &lstUAR,DbRecordSet &lstGAR)
{
	Ret_pStatus.setError(0);
	if (RetOut_Data.getRowCount()!=1)
		return;

	DbSimpleOrm TableOrm(Ret_pStatus, BUS_CM_CONTACT, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	Q_ASSERT(GetDbManager()->GetCurrentTransactionCount(TableOrm.GetDbConnection())>0); //must be inside transaction

	//-------------------------------------
	//			WRITE CONTACT PICTURE:
	//-------------------------------------
	g_PrivateServiceSet->BusinessHelper->WriteEntityPictures(Ret_pStatus,RetOut_Data,"BCNT_BIG_PICTURE_ID","BCNT_PICTURE");
	if(!Ret_pStatus.IsOK()) return;

	//-------------------------------------
	//			WRITE CONTACTS
	//-------------------------------------
	int nSkipLastCols=DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CONTACT_FULL).m_nSkipLastColsWrite;
	TableOrm.Write(Ret_pStatus,RetOut_Data,-1,nSkipLastCols);
	if(!Ret_pStatus.IsOK()) return;

	int nContactID=RetOut_Data.getDataRef(0,"BCNT_ID").toInt();

	//-------------------------------------
	//			UAR & GAR
	//-------------------------------------
	g_BusinessServiceSet->AccessRights->WriteGUAR(Ret_pStatus,nContactID,BUS_CM_CONTACT,lstUAR,lstGAR);
	if(!Ret_pStatus.IsOK()) return;

	//-------------------------------------
	//			WRITE PHONES
	//-------------------------------------
	DbRecordSet lstPhones = RetOut_Data.getDataRef(0,"LST_PHONE").value<DbRecordSet>();
	nSkipLastCols=DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_PHONE_SELECT).m_nSkipLastColsWrite;
	TableOrm.WriteSubData(Ret_pStatus,BUS_CM_PHONE,lstPhones,"BCMP_CONTACT_ID",nContactID,true,nSkipLastCols,"");
	if(!Ret_pStatus.IsOK()) return;
	RetOut_Data.setData(0,"LST_PHONE",lstPhones);
	//-------------------------------------
	//			WRITE EMAIL
	//-------------------------------------
	DbRecordSet lstEmails = RetOut_Data.getDataRef(0,"LST_EMAIL").value<DbRecordSet>();
	nSkipLastCols=DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_EMAIL_SELECT).m_nSkipLastColsWrite;
	TableOrm.WriteSubData(Ret_pStatus,BUS_CM_EMAIL,lstEmails,"BCME_CONTACT_ID",nContactID,true,nSkipLastCols,"");
	if(!Ret_pStatus.IsOK()) return;
	RetOut_Data.setData(0,"LST_EMAIL",lstEmails);
	//-------------------------------------
	//			WRITE INTERNET
	//-------------------------------------
	DbRecordSet lstInternet = RetOut_Data.getDataRef(0,"LST_INTERNET").value<DbRecordSet>();
	nSkipLastCols=DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_INTERNET_SELECT).m_nSkipLastColsWrite;
	TableOrm.WriteSubData(Ret_pStatus,BUS_CM_INTERNET,lstInternet,"BCMI_CONTACT_ID",nContactID,true,nSkipLastCols,"");
	if(!Ret_pStatus.IsOK()) return;
	RetOut_Data.setData(0,"LST_INTERNET",lstInternet);
	//-------------------------------------
	//			WRITE ADDRESS
	//-------------------------------------
	DbRecordSet lstAddress = RetOut_Data.getDataRef(0,"LST_ADDRESS").value<DbRecordSet>();
	nSkipLastCols=DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_ADDRESS_SELECT).m_nSkipLastColsWrite;
	TableOrm.WriteSubData(Ret_pStatus,BUS_CM_ADDRESS,lstAddress,"BCMA_CONTACT_ID",nContactID,true,nSkipLastCols,"");
	if(!Ret_pStatus.IsOK()) return;
	//--------------------------------------------------
	//			WRITE ADDRESS TYPES -> for each address row extract LST_TYPES and write with new address id/type id in NM table TVIEW_BUS_CM_ADDRESS_TYPES
	//---------------------------------------------------
	int nSize=lstAddress.getRowCount();
	nSkipLastCols=DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_ADDRESS_TYPES_SELECT).m_nSkipLastColsWrite;
	for(int i=0;i<nSize;i++)
	{
		DbRecordSet lstAddressTypes = lstAddress.getDataRef(i,"LST_TYPES").value<DbRecordSet>();
		TableOrm.WriteSubData(Ret_pStatus,BUS_CM_ADDRESS_TYPES,lstAddressTypes,"BCMAT_ADDRESS_ID",lstAddress.getDataRef(i,"BCMA_ID").toInt(),true,nSkipLastCols,"");
		if(!Ret_pStatus.IsOK()) return;
		lstAddress.setData(i,"LST_TYPES",lstAddressTypes);
	}
	RetOut_Data.setData(0,"LST_ADDRESS",lstAddress);

	//write only if needed based on nSubDataFilter

	//-------------------------------------
	//			WRITE JOURNAL
	//-------------------------------------
	if (nSubDataFilter & ContactTypeManager::USE_SUBDATA_JOURNAL || nSubDataFilter==ContactTypeManager::USE_SUBDATA_ALL)
	{
		DbRecordSet lstJournal = RetOut_Data.getDataRef(0,"LST_JOURNAL").value<DbRecordSet>();
		nSkipLastCols=DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_JOURNAL_SELECT).m_nSkipLastColsWrite;
		TableOrm.WriteSubData(Ret_pStatus,BUS_CM_JOURNAL,lstJournal,"BCMJ_CONTACT_ID",nContactID,true,nSkipLastCols,"");
		if(!Ret_pStatus.IsOK()) return;
		RetOut_Data.setData(0,"LST_JOURNAL",lstJournal);
	}
	//-------------------------------------
	//			WRITE PICTURES
	//-------------------------------------
	if (nSubDataFilter & ContactTypeManager::USE_SUBDATA_PICTURE || nSubDataFilter==ContactTypeManager::USE_SUBDATA_ALL)
	{
		DbRecordSet lstPics = RetOut_Data.getDataRef(0,"LST_PICTURES").value<DbRecordSet>();
		//----------------------------write big pictures from BPIC_PICTURE field----------------------------------
		DbRecordSet lstActualWrite=PictureHelperCore::PreProcessSavePictureIfNeeded(lstPics,"BCMPC_BIG_PICTURE_ID"); //extract row of big pictures to be written
		if (lstActualWrite.getRowCount()>0)
		{
			WriteBigPicture(Ret_pStatus,lstActualWrite);
			if(!Ret_pStatus.IsOK())	return;
			PictureHelperCore::PostProcessSavePictureIfNeeded(lstPics,lstActualWrite,"BCMPC_BIG_PICTURE_ID"); //reassign id's back
		}
		//----------------------------write big pictures from BPIC_PICTURE field----------------------------------
		nSkipLastCols=1; //BPIC_PICTURE is added manually...
		TableOrm.WriteSubData(Ret_pStatus,BUS_CM_PICTURE,lstPics,"BCMPC_CONTACT_ID",nContactID,true,1,"");
		if(!Ret_pStatus.IsOK()) return;
		RetOut_Data.setData(0,"LST_PICTURES",lstPics);
	}
	//-------------------------------------
	//			WRITE DEBTOR
	//-------------------------------------
	if (nSubDataFilter & ContactTypeManager::USE_SUBDATA_DEBTOR || nSubDataFilter==ContactTypeManager::USE_SUBDATA_ALL)
	{
		DbRecordSet lstDebtor = RetOut_Data.getDataRef(0,"LST_DEBTOR").value<DbRecordSet>();
		TableOrm.WriteSubData(Ret_pStatus,BUS_CM_DEBTOR,lstDebtor,"BCMD_CONTACT_ID",nContactID,true,0,"");
		if(!Ret_pStatus.IsOK()) return;
		RetOut_Data.setData(0,"LST_DEBTOR",lstDebtor);
	}
	//-------------------------------------
	//			WRITE CREDITOR
	//-------------------------------------
	if (nSubDataFilter & ContactTypeManager::USE_SUBDATA_CREDITOR || nSubDataFilter==ContactTypeManager::USE_SUBDATA_ALL)
	{
		DbRecordSet lstCreditor = RetOut_Data.getDataRef(0,"LST_CREDITOR").value<DbRecordSet>();
		TableOrm.WriteSubData(Ret_pStatus,BUS_CM_CREDITOR,lstCreditor,"BCMC_CONTACT_ID",nContactID,true,0,"");
		if(!Ret_pStatus.IsOK()) return;
		RetOut_Data.setData(0,"LST_CREDITOR",lstCreditor);
	}

	//-------------------------------------
	//			WRITE CUSTOM FIELDS
	//-------------------------------------
	if (nSubDataFilter & ContactTypeManager::USE_SUBDATA_CUSTOM_FIELDS || nSubDataFilter==ContactTypeManager::USE_SUBDATA_ALL)
	{
		DbRecordSet lstCustomData = RetOut_Data.getDataRef(0,"LST_CUSTOM_FIELDS").value<DbRecordSet>();
		g_BusinessServiceSet->BusCustomFields->WriteUserCustomData(Ret_pStatus,BUS_CM_CONTACT,nContactID,lstCustomData);

		//TableOrm.WriteSubData(Ret_pStatus,BUS_CM_CREDITOR,lstCreditor,"BCMC_CONTACT_ID",nContactID,true,0,"");
		if(!Ret_pStatus.IsOK()) return;
		RetOut_Data.setData(0,"LST_CUSTOM_FIELDS",lstCustomData);
	}


}


void Service_BusContact::WriteEntityPictures(Status &Ret_pStatus, DbRecordSet &RetOut_Data, QString strBigPicIDColName, QString strPreviewPicColName)
{
	g_PrivateServiceSet->BusinessHelper->WriteEntityPictures(Ret_pStatus,RetOut_Data,strBigPicIDColName,strPreviewPicColName);
}
void Service_BusContact::ReadEntityPictures(Status &Ret_pStatus,DbRecordSet &Ret_Data,QString strBigPicIDColName)
{
	g_PrivateServiceSet->BusinessHelper->ReadEntityPictures(Ret_pStatus,Ret_Data,strBigPicIDColName);
}

//lstOfContactIds only col BCNT_ID
//Ret_Data = TVIEW_BUS_CM_ADDRESS_SELECT
void Service_BusContact::ReadContactAddress(Status &Ret_pStatus, DbRecordSet &lstOfContactIds, DbRecordSet &Ret_Data)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_CM_CONTACT, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	//-----------------------------------------------------
	//		LOAD ADDRESSES
	//-----------------------------------------------------
	TableOrm.ReadFromParentIDs(Ret_pStatus, Ret_Data,"BCMA_CONTACT_ID",lstOfContactIds,"BCNT_ID","", "",DbSqlTableView::TVIEW_BUS_CM_ADDRESS_SELECT);
	if(!Ret_pStatus.IsOK()) return;
	//-----------------------------------------------------
	//		LOAD ADDRESS TYPES
	//-----------------------------------------------------
	DbRecordSet lstAddressTypes;
	TableOrm.ReadFromParentIDs(Ret_pStatus, lstAddressTypes,"BCMAT_ADDRESS_ID",Ret_Data,"BCMA_ID"," LEFT OUTER JOIN bus_cm_types ON BCMT_ID=BCMAT_TYPE_ID ", "",DbSqlTableView::TVIEW_BUS_CM_ADDRESS_TYPES_SELECT);
	if(!Ret_pStatus.IsOK()) return;

	//types -> addresses
	//---------------------------------------------------------------
	g_PrivateServiceSet->BusinessHelper->AssignChildToParent(Ret_Data,"LST_TYPES",lstAddressTypes,"BCMAT_ADDRESS_ID");
	int nSize=Ret_Data.getRowCount();
	int nLstTypesColIdx=Ret_Data.getColumnIdx("LST_TYPES");
	int nLstTypeNameColIdx=lstAddressTypes.getColumnIdx("BCMT_TYPE_NAME");
	int nAddrTypeNameColIdx=Ret_Data.getColumnIdx("BCMT_TYPE_NAME");
	QString strTypeName;
	int nSize2;
	for(int i=0;i<nSize;++i)
	{
		DbRecordSet lstTypes=Ret_Data.getDataRef(i,nLstTypesColIdx).value<DbRecordSet>();
		strTypeName="";
		nSize2=lstTypes.getRowCount();
		for(int k=0;k<nSize2;++k)
			strTypeName+=lstTypes.getDataRef(k,nLstTypeNameColIdx).toString()+",";
		Ret_Data.setData(i,nAddrTypeNameColIdx,strTypeName.remove(strTypeName.size()-1,1)); //remove last ,
	}

}



//Data must be format in: DbSqlTableView::TVIEW_GROUP_IMPORT
void Service_BusContact::WriteGroupAssignments(Status &Ret_pStatus,DbRecordSet &Data)
{
	//Data.Dump();

	DbRecordSet lstErr;
	lstErr.copyDefinition(Data);
	lstErr.addColumn(QVariant::String,"ERROR_TEXT");


	Data.addColumn(QVariant::Int,"BCNT_ID");
	Data.addColumn(QVariant::Int,"BGIT_ID");

	DbSimpleOrm TableOrm(Ret_pStatus, BUS_CM_GROUP, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	DbSqlQuery *query = TableOrm.GetDbSqlQuery();

	
	QString strSQL_0 ="SELECT BCNT_ID FROM BUS_CM_CONTACT WHERE BCNT_FIRSTNAME IS NULL AND BCNT_LASTNAME IS NULL AND BCNT_ORGANIZATIONNAME = ?";
	QString strSQL_1 ="SELECT BCNT_ID FROM BUS_CM_CONTACT WHERE BCNT_FIRSTNAME = ? AND BCNT_LASTNAME = ? AND BCNT_ORGANIZATIONNAME = ? AND BCNT_MIDDLENAME IS NULL";
	QString strSQL_2 ="SELECT BCNT_ID FROM BUS_CM_CONTACT WHERE BCNT_FIRSTNAME = ? AND BCNT_LASTNAME = ? AND BCNT_ORGANIZATIONNAME = ? AND BCNT_MIDDLENAME = ?";
	QString strSQL_3 ="SELECT BCNT_ID FROM BUS_CM_CONTACT WHERE BCNT_FIRSTNAME = ? AND BCNT_LASTNAME = ? AND BCNT_ORGANIZATIONNAME IS NULL AND BCNT_MIDDLENAME IS NULL";
	QString strSQL_4 ="SELECT BCNT_ID FROM BUS_CM_CONTACT WHERE BCNT_FIRSTNAME = ? AND BCNT_LASTNAME = ? AND BCNT_ORGANIZATIONNAME IS NULL AND BCNT_MIDDLENAME = ?";

	int nSize=Data.getRowCount();
	if (nSize==0)
		return;

	query->Prepare(Ret_pStatus,strSQL_0);
	if(!Ret_pStatus.IsOK()) return;
	for (int i=0;i<nSize;i++)
	{
		if (!Data.getDataRef(i,"BCNT_ORGANIZATIONNAME").toString().isEmpty() && Data.getDataRef(i,"BCNT_FIRSTNAME").toString().isEmpty() && Data.getDataRef(i,"BCNT_LASTNAME").toString().isEmpty())
		{
			query->bindValue(0,Data.getDataRef(i,"BCNT_ORGANIZATIONNAME").toString());

			query->ExecutePrepared(Ret_pStatus);
			DbRecordSet lstChunkData;
			query->FetchData(lstChunkData);
			if (lstChunkData.getRowCount()>0)
				Data.setData(i,"BCNT_ID",lstChunkData.getDataRef(0,0));

		}
	}


	query->Prepare(Ret_pStatus,strSQL_1);
	if(!Ret_pStatus.IsOK()) return;
	for (int i=0;i<nSize;i++)
	{
		if (!Data.getDataRef(i,"BCNT_ORGANIZATIONNAME").toString().isEmpty())
		{
			query->bindValue(0,Data.getDataRef(i,"BCNT_FIRSTNAME").toString());
			query->bindValue(1,Data.getDataRef(i,"BCNT_LASTNAME").toString());
			query->bindValue(2,Data.getDataRef(i,"BCNT_ORGANIZATIONNAME").toString());

			query->ExecutePrepared(Ret_pStatus);
			DbRecordSet lstChunkData;
			query->FetchData(lstChunkData);
			if (lstChunkData.getRowCount()>0)
				Data.setData(i,"BCNT_ID",lstChunkData.getDataRef(0,0));

		}
	}

	query->Prepare(Ret_pStatus,strSQL_2);
	if(!Ret_pStatus.IsOK()) return;
	for (int i=0;i<nSize;i++)
	{
		if (!Data.getDataRef(i,"BCNT_ORGANIZATIONNAME").toString().isEmpty() && Data.getDataRef(i,"BCNT_FIRSTNAME").toString().indexOf(" ")>0)
		{
			QString strFirstAndMiddleName=Data.getDataRef(i,"BCNT_FIRSTNAME").toString();
			int nPos=strFirstAndMiddleName.indexOf(" ");

			QString strFirstName=strFirstAndMiddleName.left(nPos);
			QString strMiddleName=strFirstAndMiddleName.mid(nPos+1).trimmed();

			query->bindValue(0,strFirstName);
			query->bindValue(1,Data.getDataRef(i,"BCNT_LASTNAME").toString());
			query->bindValue(2,Data.getDataRef(i,"BCNT_ORGANIZATIONNAME").toString());
			query->bindValue(3,strMiddleName);

			query->ExecutePrepared(Ret_pStatus);
			DbRecordSet lstChunkData;
			query->FetchData(lstChunkData);
			if (lstChunkData.getRowCount()>0)
				Data.setData(i,"BCNT_ID",lstChunkData.getDataRef(0,0));

		}
	}



	query->Prepare(Ret_pStatus,strSQL_3);
	if(!Ret_pStatus.IsOK()) return;
	for (int i=0;i<nSize;i++)
	{
		if (Data.getDataRef(i,"BCNT_ORGANIZATIONNAME").toString().isEmpty())
		{
			query->bindValue(0,Data.getDataRef(i,"BCNT_FIRSTNAME").toString());
			query->bindValue(1,Data.getDataRef(i,"BCNT_LASTNAME").toString());

			query->ExecutePrepared(Ret_pStatus);
			DbRecordSet lstChunkData;
			query->FetchData(lstChunkData);
			if (lstChunkData.getRowCount()>0)
				Data.setData(i,"BCNT_ID",lstChunkData.getDataRef(0,0));

		}
	}


	query->Prepare(Ret_pStatus,strSQL_4);
	if(!Ret_pStatus.IsOK()) return;
	for (int i=0;i<nSize;i++)
	{
		if (Data.getDataRef(i,"BCNT_ORGANIZATIONNAME").toString().isEmpty() && Data.getDataRef(i,"BCNT_FIRSTNAME").toString().indexOf(" ")>0)
		{
			QString strFirstAndMiddleName=Data.getDataRef(i,"BCNT_FIRSTNAME").toString();
			int nPos=strFirstAndMiddleName.indexOf(" ");

			QString strFirstName=strFirstAndMiddleName.left(nPos);
			QString strMiddleName=strFirstAndMiddleName.mid(nPos+1).trimmed();

			query->bindValue(0,strFirstName);
			query->bindValue(1,Data.getDataRef(i,"BCNT_LASTNAME").toString());
			query->bindValue(2,strMiddleName);

			query->ExecutePrepared(Ret_pStatus);
			DbRecordSet lstChunkData;
			query->FetchData(lstChunkData);
			if (lstChunkData.getRowCount()>0)
				Data.setData(i,"BCNT_ID",lstChunkData.getDataRef(0,0));
		}
	}



	//find groups
	//-------------------------
	QString strSQL_G1 ="SELECT BGIT_ID FROM BUS_GROUP_ITEMS WHERE BGIT_CODE = ?";
	QString strSQL_G2 ="SELECT BGIT_ID FROM BUS_GROUP_ITEMS WHERE BGIT_EXTERNAL_CATEGORY = ?";


	query->Prepare(Ret_pStatus,strSQL_G2);
	if(!Ret_pStatus.IsOK()) return;
	for (int i=0;i<nSize;i++)
	{
		if (!Data.getDataRef(i,"BGIT_EXTERNAL_CATEGORY").toString().isEmpty())
		{
			query->bindValue(0,Data.getDataRef(i,"BGIT_EXTERNAL_CATEGORY").toString());

			query->ExecutePrepared(Ret_pStatus);
			DbRecordSet lstChunkData;
			query->FetchData(lstChunkData);
			if (lstChunkData.getRowCount()>0)
				Data.setData(i,"BGIT_ID",lstChunkData.getDataRef(0,0));
		}
	}

	query->Prepare(Ret_pStatus,strSQL_G1);
	if(!Ret_pStatus.IsOK()) return;
	for (int i=0;i<nSize;i++)
	{
		if (!Data.getDataRef(i,"BGIT_CODE").toString().isEmpty() && Data.getDataRef(i,"BGIT_ID").toInt()<=0)
		{
			query->bindValue(0,Data.getDataRef(i,"BGIT_CODE").toString() );

			query->ExecutePrepared(Ret_pStatus);
			DbRecordSet lstChunkData;
			query->FetchData(lstChunkData);
			if (lstChunkData.getRowCount()>0)
				Data.setData(i,"BGIT_ID",lstChunkData.getDataRef(0,0));
		}
		//else
		//	qDebug()<<Data.getDataRef(i,"BGIT_CODE").toString();
		//	qDebug()<<Data.getDataRef(i,"BGIT_EXTERNAL_CATEGORY").toString();
	}

	//Data.Dump();


	//now delete all that have either BGIT_ID or BCNT_ID missing -> can not find it-> write to log on server

	for (int i=nSize-1;i>=0;i--)
	{
		if (Data.getDataRef(i,"BGIT_ID").toInt()<=0 || Data.getDataRef(i,"BCNT_ID").toInt()<=0)
		{
			//Data.getRow(i).Dump();

			QString strMsg;
			if (Data.getDataRef(i,"BGIT_ID").toInt()<=0)
			{
				if (!Data.getDataRef(i,"BGIT_CODE").toString().isEmpty())
				{
					strMsg="Group with "+Data.getDataRef(i,"BGIT_CODE").toString()+" group code";
				}
				else
				{
					strMsg="Group with "+Data.getDataRef(i,"BGIT_EXTERNAL_CATEGORY").toString()+" group external category code";
				}
				strMsg+=" can not be found in the database. Group assignment not done!";

			}
			else
			{
				if (!Data.getDataRef(i,"BCNT_ORGANIZATIONNAME").toString().isEmpty() && Data.getDataRef(i,"BCNT_FIRSTNAME").toString().isEmpty() && Data.getDataRef(i,"BCNT_LASTNAME").toString().isEmpty())
				{
					strMsg="Organization "+Data.getDataRef(i,"BCNT_ORGANIZATIONNAME").toString();
				}
				else
				{
					strMsg="Contact "+Data.getDataRef(i,"BCNT_FIRSTNAME").toString()+" "+Data.getDataRef(i,"BCNT_LASTNAME").toString();
				}
				
				strMsg+=" can not be found in the database. Group assignment not done!";
			}
			g_Logger.logMessage(StatusCodeSet::TYPE_WARNING,StatusCodeSet::ERR_SYSTEM_IMP_EXP_FAIL,strMsg);

			lstErr.merge(Data.getRow(i)); //return bad ones
			lstErr.setData(lstErr.getRowCount()-1,"ERROR_TEXT",strMsg);
			Data.deleteRow(i);
		}
	}

	nSize=Data.getRowCount();



	//check for duplicates:
	for (int i=nSize-1;i>=0;i--)
	{
		int nGroupID=Data.getDataRef(i,"BGIT_ID").toInt();
		int nCntID=Data.getDataRef(i,"BCNT_ID").toInt();

		QString strMsg;

		
		Data.find(Data.getColumnIdx("BGIT_ID"),nGroupID,false,false,true,true);
		if(Data.find(Data.getColumnIdx("BCNT_ID"),nCntID,false,true,true,true)>1)
		{

			//Data.getRow(i).Dump();

			if (!Data.getDataRef(i,"BCNT_ORGANIZATIONNAME").toString().isEmpty() && Data.getDataRef(i,"BCNT_FIRSTNAME").toString().isEmpty() && Data.getDataRef(i,"BCNT_LASTNAME").toString().isEmpty())
			{
				strMsg="Organization "+Data.getDataRef(i,"BCNT_ORGANIZATIONNAME").toString();
			}
			else
			{
				strMsg="Contact "+Data.getDataRef(i,"BCNT_FIRSTNAME").toString()+" "+Data.getDataRef(i,"BCNT_LASTNAME").toString();
			}

			strMsg+=" assignment to the ";
			if (!Data.getDataRef(i,"BGIT_CODE").toString().isEmpty())
			{
				strMsg+="group with "+Data.getDataRef(i,"BGIT_CODE").toString()+" group code";
			}
			else
			{
				strMsg+="group with "+Data.getDataRef(i,"BGIT_EXTERNAL_CATEGORY").toString()+" group external category code";
			}
			strMsg+=" already exists in import list, record skipped.";

			g_Logger.logMessage(StatusCodeSet::TYPE_WARNING,StatusCodeSet::ERR_SYSTEM_IMP_EXP_FAIL,strMsg);
			
			lstErr.merge(Data.getRow(i)); //return bad ones
			lstErr.setData(lstErr.getRowCount()-1,"ERROR_TEXT",strMsg);
			Data.deleteRow(i);
		}
	}

	nSize=Data.getRowCount();

	//search in database for existing records: if found then if mode=yy delete else edit, if not found, insert

	DbRecordSet lstRecordsWrite;
	DbRecordSet lstRecordsDelete;

	lstRecordsWrite.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_GROUP));
	lstRecordsDelete.addColumn(QVariant::Int,"BGCN_ID");

	for (int i=0;i<nSize;i++)
	{
		QString strSQL_G1 ="WHERE BGCN_ITEM_ID = %1 AND BGCN_CONTACT_ID = %2";
		strSQL_G1=strSQL_G1.arg(QString::number(Data.getDataRef(i,"BGIT_ID").toInt())).arg(QString::number(Data.getDataRef(i,"BCNT_ID").toInt()));

		DbRecordSet recOld;
		TableOrm.Read(Ret_pStatus,recOld,strSQL_G1);
		if (!Ret_pStatus.IsOK()) return;

		if (recOld.getRowCount()>0)
		{
			DbRecordSet recNew;
			recNew.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_GROUP));
			recNew.addRow();
			recNew.setData(0,"BGCN_ITEM_ID",Data.getDataRef(i,"BGIT_ID"));
			recNew.setData(0,"BGCN_CONTACT_ID",Data.getDataRef(i,"BCNT_ID"));
			recNew.setData(0,"BGCN_ID",recOld.getDataRef(0,"BGCN_ID"));

			//if 0 overwrite, else only overwrite if source is not NULL
			if (Data.getDataRef(i,"UPDATE_MODE").toInt()==0)
			{
				recNew.setData(0,"BGCN_CMCA_VALID_FROM",Data.getDataRef(0,"BGCN_CMCA_VALID_FROM"));
				recNew.setData(0,"BGCN_CMCA_VALID_TO",Data.getDataRef(0,"BGCN_CMCA_VALID_TO"));
			}
			else
			{
				if (!Data.getDataRef(0,"BGCN_CMCA_VALID_FROM").toDate().isNull())
					recNew.setData(0,"BGCN_CMCA_VALID_FROM",Data.getDataRef(0,"BGCN_CMCA_VALID_FROM"));
				if (!Data.getDataRef(0,"BGCN_CMCA_VALID_TO").toDate().isNull())
					recNew.setData(0,"BGCN_CMCA_VALID_TO",Data.getDataRef(0,"BGCN_CMCA_VALID_TO"));
			}

			if (Data.getDataRef(i,"MODE").toInt()==0)
				lstRecordsWrite.merge(recNew); //existing record found
			else
				lstRecordsDelete.merge(recOld);

		}
		else if (Data.getDataRef(i,"MODE").toInt()==0) //create new record only if mode not delete
		{
			//create new record:
			DbRecordSet recNew;
			recNew.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_GROUP));
			recNew.merge(Data.getRow(i)); //dates
			recNew.setData(0,"BGCN_ITEM_ID",Data.getDataRef(i,"BGIT_ID"));
			recNew.setData(0,"BGCN_CONTACT_ID",Data.getDataRef(i,"BCNT_ID"));

			lstRecordsWrite.merge(recNew);
		}
	}

	
	//lstRecordsWrite.Dump();
	//lstRecordsDelete.Dump();



	//based on mode, update or delete
	//-------------------------------
	if (lstRecordsWrite.getRowCount()>0)
	{
		TableOrm.Write(Ret_pStatus,lstRecordsWrite);
		if (!Ret_pStatus.IsOK()) return;
	}

	if (lstRecordsDelete.getRowCount()>0)
	{
		TableOrm.DeleteFast(Ret_pStatus,lstRecordsDelete);
		if (!Ret_pStatus.IsOK()) return;
	}

	Data=lstErr;
	//Data.Dump();


}


//-----------------------------------------------------
//		LOAD GROUPS
//-----------------------------------------------------

void Service_BusContact::ReadContactGroup(Status &Ret_pStatus, int nContactID, DbRecordSet &Ret_Data)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_CM_GROUP, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	QString strWhere = " WHERE BGCN_CONTACT_ID = "+QString::number(nContactID);

	//load all
	TableOrm.Read(Ret_pStatus, Ret_Data, strWhere, DbSqlTableView::TVIEW_BUS_CM_GROUP_SELECT);
	if(!Ret_pStatus.IsOK()) return;
}


//-------------------------------------
//			WRITE PHONES
//-------------------------------------
void Service_BusContact::WritePhones(Status &Ret_pStatus,int nContactID, DbRecordSet &RetOut_Data)
{
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_CM_PHONE, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	RetOut_Data.setColValue("BCMP_CONTACT_ID",nContactID);
	TableOrm.Write(Ret_pStatus,RetOut_Data);

}


//-----------------------------------------------------------------------------------------
//			WRITE ADDRESS: list in TVIEW_BUS_CM_ADDRESS_SELECT format
//-----------------------------------------------------------------------------------------
void Service_BusContact::WriteAddress(Status &Ret_pStatus,int nContactID, DbRecordSet &RetOut_Data)
{
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_CM_ADDRESS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	RetOut_Data.setColValue("BCMA_CONTACT_ID",nContactID);

	TableOrm.GetDbSqlQuery()->BeginTransaction(Ret_pStatus);
	if(!Ret_pStatus.IsOK()) return;

	//-------------------------------------
	//			WRITE ADDRESS
	//-------------------------------------
	//RetOut_Data.Dump();

	TableOrm.Write(Ret_pStatus,RetOut_Data,DbSqlTableView::TVIEW_BUS_CM_ADDRESS_SELECT);
	if(!Ret_pStatus.IsOK()) 
	{
		TableOrm.GetDbSqlQuery()->Rollback();
		return;
	}

	//----------------------------------------------------------------------------------------------------------------------------------------------------------
	//			WRITE ADDRESS TYPES -> for each address row extract LST_TYPES and write with new address id/type id in NM table TVIEW_BUS_CM_ADDRESS_TYPES
	//----------------------------------------------------------------------------------------------------------------------------------------------------------
	int nSize=RetOut_Data.getRowCount();
	int nSkipLastCols=DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CM_ADDRESS_TYPES_SELECT).m_nSkipLastColsWrite;
	for(int i=0;i<nSize;i++)
	{
		DbRecordSet lstAddressTypes = RetOut_Data.getDataRef(i,"LST_TYPES").value<DbRecordSet>();
		TableOrm.WriteSubData(Ret_pStatus,BUS_CM_ADDRESS_TYPES,lstAddressTypes,"BCMAT_ADDRESS_ID",RetOut_Data.getDataRef(i,"BCMA_ID").toInt(),true,nSkipLastCols,"");
		if(!Ret_pStatus.IsOK())
		{
			TableOrm.GetDbSqlQuery()->Rollback();
			return;
		}
		RetOut_Data.setData(i,"LST_TYPES",lstAddressTypes);
	}

	TableOrm.GetDbSqlQuery()->Commit(Ret_pStatus);

}

//issue 2534:
//by nContactIDOrg get all contact persons with same organization name
//compare address row of each contact person with rowAddress
//if all same then return ID...

void Service_BusContact::GetAllContactsOfOrganizationByAddress(Status &Ret_pStatus, int nContactIDOrg, DbRecordSet rowAddress,DbRecordSet &Ret_AddressIds)
{
	DbSqlQuery query(Ret_pStatus, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	if (rowAddress.getRowCount()==0)
		return;

	QString strSQL="SELECT BCNT_ORGANIZATIONNAME FROM BUS_CM_CONTACT WHERE BCNT_ID="+QString::number(nContactIDOrg);
	query.Execute(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK()) return;

	QString strOrgName;
	if (query.next())
		strOrgName=query.value(0).toString();


	strSQL="SELECT * FROM BUS_CM_ADDRESS INNER JOIN BUS_CM_CONTACT ON BCMA_CONTACT_ID = BCNT_ID";
	strSQL += " WHERE BCNT_ORGANIZATIONNAME=? AND BCNT_TYPE=?";

	query.Prepare(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK()) return;

	query.bindValue(0,strOrgName);
	query.bindValue(1,ContactTypeManager::CM_TYPE_PERSON);

	query.ExecutePrepared(Ret_pStatus);
	if(!Ret_pStatus.IsOK()) return;
	query.FetchData(Ret_AddressIds); //all address of persons contacts within target organization

	//Ret_AddressIds.Dump();

	int nSize=Ret_AddressIds.getRowCount();
	Ret_AddressIds.clearSelection();
	for (int i=0;i<nSize;i++)
	{
		if (Ret_AddressIds.getDataRef(i,"BCMA_ORGANIZATIONNAME_2").toString()!=rowAddress.getDataRef(0,"BCMA_ORGANIZATIONNAME_2").toString())
			continue;
		if (Ret_AddressIds.getDataRef(i,"BCMA_STREET").toString()!=rowAddress.getDataRef(0,"BCMA_STREET").toString())
			continue;
		if (Ret_AddressIds.getDataRef(i,"BCMA_POBOX").toString()!=rowAddress.getDataRef(0,"BCMA_POBOX").toString())
			continue;
		if (Ret_AddressIds.getDataRef(i,"BCMA_POBOXZIP").toString()!=rowAddress.getDataRef(0,"BCMA_POBOXZIP").toString())
			continue;
		if (Ret_AddressIds.getDataRef(i,"BCMA_ZIP").toString()!=rowAddress.getDataRef(0,"BCMA_ZIP").toString())
			continue;
		if (Ret_AddressIds.getDataRef(i,"BCMA_CITY").toString()!=rowAddress.getDataRef(0,"BCMA_CITY").toString())
			continue;
		if (Ret_AddressIds.getDataRef(i,"BCMA_REGION").toString()!=rowAddress.getDataRef(0,"BCMA_REGION").toString())
			continue;
		if (Ret_AddressIds.getDataRef(i,"BCMA_COUNTRY_CODE").toString()!=rowAddress.getDataRef(0,"BCMA_COUNTRY_CODE").toString())
			continue;
		if (Ret_AddressIds.getDataRef(i,"BCMA_COUNTRY_NAME").toString()!=rowAddress.getDataRef(0,"BCMA_COUNTRY_NAME").toString())
			continue;
	
		Ret_AddressIds.selectRow(i);
	}

	Ret_AddressIds.deleteUnSelectedRows();
	DbRecordSet lstAdd;
	lstAdd.addColumn(QVariant::Int,"BCMA_ID");
	lstAdd.merge(Ret_AddressIds);

	Ret_AddressIds=lstAdd;
	//Ret_AddressIds.Dump();



}


//issue 2534:
//by lstAddressIds update all address data to rowAddress data (only specified fields)
void Service_BusContact::UpdateContactsOfOrganizationByAddress(Status &Ret_pStatus, DbRecordSet &lstAddressIds, DbRecordSet rowAddress)
{
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_CM_ADDRESS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	//--------------------------
	//Address schemas
	//--------------------------
	DbRecordSet lstSchemas;
	g_BusinessServiceSet->BusAddressSchemas->Read(Ret_pStatus,lstSchemas);
	if(!Ret_pStatus.IsOK()) return;
	FormatAddress m_AdrFormatter;

	//rowAddress.Dump();
	if (rowAddress.getRowCount()==0)
		return;

	//read address records:
	DbRecordSet lstRead;
	TableOrm.ReadFromParentIDs(Ret_pStatus,lstRead,"BCMA_ID",lstAddressIds,"BCMA_ID");

	//manually update 'em coz of formatted field:

	int nSize=lstRead.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		DbRecordSet rowCurrentAddress=lstRead.getRow(i);

		rowCurrentAddress.setData(0,"BCMA_ORGANIZATIONNAME",rowAddress.getDataRef(0,"BCMA_ORGANIZATIONNAME"));
		rowCurrentAddress.setData(0,"BCMA_ORGANIZATIONNAME_2",rowAddress.getDataRef(0,"BCMA_ORGANIZATIONNAME_2"));
		rowCurrentAddress.setData(0,"BCMA_STREET",rowAddress.getDataRef(0,"BCMA_STREET"));
		rowCurrentAddress.setData(0,"BCMA_POBOX",rowAddress.getDataRef(0,"BCMA_POBOX"));
		rowCurrentAddress.setData(0,"BCMA_POBOXZIP",rowAddress.getDataRef(0,"BCMA_POBOXZIP"));
		rowCurrentAddress.setData(0,"BCMA_ZIP",rowAddress.getDataRef(0,"BCMA_ZIP"));
		rowCurrentAddress.setData(0,"BCMA_CITY",rowAddress.getDataRef(0,"BCMA_CITY"));
		rowCurrentAddress.setData(0,"BCMA_REGION",rowAddress.getDataRef(0,"BCMA_REGION"));
		rowCurrentAddress.setData(0,"BCMA_COUNTRY_CODE",rowAddress.getDataRef(0,"BCMA_COUNTRY_CODE"));
		rowCurrentAddress.setData(0,"BCMA_COUNTRY_NAME",rowAddress.getDataRef(0,"BCMA_COUNTRY_NAME"));


		//Format address by stored ID or by default if not found one:
		int nSchemID=lstRead.getDataRef(i,"BCMA_FORMATSCHEMA_ID").toInt();
		
		//get default schema:
		FormatAddress m_AdrFormatter;
		QString strSchema;
		int nRow=lstSchemas.find(lstSchemas.getColumnIdx("BCMAS_ID"),nSchemID,true);
		if(nRow!=-1)
			strSchema=lstSchemas.getDataRef(nRow,"BCMAS_SCHEMA").toString();
		else
			strSchema=m_AdrFormatter.m_strDefaultSchema;


		m_AdrFormatter.AddressFormat(rowCurrentAddress,strSchema,false);
		lstRead.assignRow(i,rowCurrentAddress);
	}

	TableOrm.Write(Ret_pStatus,lstRead);
	
}


//strUniqueGroupIdentifier is in "Firmenkontakte::HBS. Firmenkontakte HBS" format
//:: is delimiter for tree+(code+name)
//' ' is delimiter for (code+name)
void Service_BusContact::ReadContactsFromGroup(Status &Ret_pStatus, QString strUniqueGroupIdentifier,DbRecordSet &Ret_Data)
{
	int nPos=strUniqueGroupIdentifier.indexOf("::");
	if (nPos<0)
	{
		Ret_pStatus.setError(1,"Can not parse unique group identifier!");
		return;
	}
	QString strGroupCode, strTreeName;
	strTreeName = strUniqueGroupIdentifier.left(nPos);
	strUniqueGroupIdentifier = strUniqueGroupIdentifier.mid(nPos+2);

	nPos=strUniqueGroupIdentifier.indexOf(" ");
	if (nPos<0)
	{
		Ret_pStatus.setError(1,"Can not parse unique group identifier!");
		return;
	}
	strGroupCode = strUniqueGroupIdentifier.left(nPos);

	//get all contact id's from these data:
	DbSqlQuery query(Ret_pStatus, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	QString strSQL="SELECT BGCN_CONTACT_ID,"+DbSqlTableView::getSQLColumnsFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_CONTACT_SELECTION))+ " FROM BUS_CM_GROUP";
	strSQL +=" INNER JOIN BUS_GROUP_ITEMS ON BGIT_ID = BGCN_ITEM_ID";
	strSQL +=" INNER JOIN BUS_GROUP_TREE ON BGTR_ID = BGIT_TREE_ID";
	strSQL +=" INNER JOIN BUS_CM_CONTACT ON BGCN_CONTACT_ID = BCNT_ID";
	strSQL +=" WHERE BGTR_NAME=? AND BGIT_CODE=?";

	g_AccessRight->SQLFilterRecords(BUS_CM_CONTACT,strSQL);
	
	query.Prepare(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK()) return;

	query.bindValue(0,strTreeName);
	query.bindValue(1,strGroupCode);

	query.ExecutePrepared(Ret_pStatus);
	if(!Ret_pStatus.IsOK()) return;


	query.FetchData(Ret_Data);

	Ret_Data.addColumn(QVariant::String,"BCNT_NAME");

	//calculated names: 
	MainEntityCalculatedName filler;
	filler.Initialize(ENTITY_BUS_CONTACT,Ret_Data);
	filler.FillCalculatedNames(Ret_Data,Ret_Data.getColumnIdx("BCNT_NAME"));

	Ret_Data.sort("BCNT_NAME");

}


//lstDebtors <debt_code,role_name>
void Service_BusContact::AssignDebtors2Project(Status &Ret_pStatus, int nProjectID, DbRecordSet &lstDebtors)
{

	//lstDebtors.Dump();

	lstDebtors.addColumn(QVariant::Int,"Contact_ID");
	lstDebtors.addColumn(QVariant::Int,"Role_ID");

	//-----------------------------------------------------------
	//					FIND CONTACTS
	//-----------------------------------------------------------

	//get all contact id's from these data:
	DbSqlQuery query(Ret_pStatus, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	//first traverse list of debtors-> find all contacts
	//then search role_names in db->find id's or insert new one
	//then engage nmrx pattern: contact (role) project and ciaos bambinos.

	QString strSQL="SELECT BCNT_ID FROM BUS_CM_CONTACT INNER JOIN BUS_CM_DEBTOR ON BCMD_CONTACT_ID = BCNT_ID WHERE BCMD_DEBTORCODE =?";
	query.Prepare(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK()) return;

	int nSize=lstDebtors.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		QString strDebtorCode=lstDebtors.getDataRef(i,0).toString();
		query.bindValue(0,strDebtorCode);
		query.ExecutePrepared(Ret_pStatus);
		if(!Ret_pStatus.IsOK()) return;

		int nCntID=0;
		if (query.next())
			nCntID=query.value(0).toInt();

		lstDebtors.setData(i,"Contact_ID",nCntID);
	}

	//-----------------------------------------------------------
	//					ROLES
	//-----------------------------------------------------------

	//Prepare roles, read existing
	strSQL="SELECT BNRO_ID FROM BUS_NMRX_ROLE WHERE BNRO_NAME =?";
	query.Prepare(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK()) return;

	for (int i=0;i<nSize;i++)
	{
		QString strRoleName=lstDebtors.getDataRef(i,1).toString();
		query.bindValue(0,strRoleName);
		query.ExecutePrepared(Ret_pStatus);
		if(!Ret_pStatus.IsOK()) return;

		int nCntID=0;
		if (query.next())
			nCntID=query.value(0).toInt();

		lstDebtors.setData(i,"Role_ID",nCntID);
	}

	//insert non existing
	DbRecordSet lstRoles;
	lstRoles.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_ROLE));

	int nRoleRow=-1;
	for (int i=0;i<nSize;i++)
	{
		QString strRoleName=lstDebtors.getDataRef(i,1).toString();
		if (lstDebtors.getDataRef(i,"Role_ID").toInt()==0)
		{
			nRoleRow++;
			lstRoles.addRow();
			lstRoles.setData(nRoleRow,"BNRO_NAME",strRoleName);
			lstRoles.setData(nRoleRow,"BNRO_ASSIGMENT_TEXT",strRoleName);
			lstRoles.setData(nRoleRow,"BNRO_TABLE_1",BUS_CM_CONTACT);
			lstRoles.setData(nRoleRow,"BNRO_TABLE_2",BUS_PROJECT);
		}
	}

	DbRecordSet lstDeletedRoles;
	//write new roles:
	g_BusinessServiceSet->BusNMRX->WriteRole(Ret_pStatus,lstRoles,lstDeletedRoles,"");
	if(!Ret_pStatus.IsOK()) return;

	//assign back role ids:
	nRoleRow=-1;
	nSize=lstDebtors.getRowCount();
	for (int i=0;i<nSize;i++)
	{
		if (lstDebtors.getDataRef(i,"Role_ID").toInt()==0)
		{
			nRoleRow++;
			lstDebtors.setData(i,"Role_ID",lstRoles.getDataRef(nRoleRow,"BNRO_ID").toInt());
		}
	}

	//lstDebtors <debtor_code, role_name, contact_id, role_id>

	DbRecordSet lstNMRX;
	lstNMRX.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION_SELECT));
	lstNMRX.addRow(nSize);

	//lstDebtors.Dump();

	for (int i=0;i<nSize;i++)
	{
		lstNMRX.setData(i,"BNMR_TABLE_1",BUS_CM_CONTACT);
		lstNMRX.setData(i,"BNMR_TABLE_2",BUS_PROJECT);
		lstNMRX.setData(i,"BNMR_TABLE_KEY_ID_1",lstDebtors.getDataRef(i,"Contact_ID"));
		lstNMRX.setData(i,"BNMR_TABLE_KEY_ID_2",nProjectID);
		lstNMRX.setData(i,"BNMR_ROLE_ID",lstDebtors.getDataRef(i,"Role_ID"));
		lstNMRX.setData(i,"BNMR_TYPE",QVariant(GlobalConstants::NMRX_TYPE_ASSIGN).toInt());
	}

	//lstNMRX.Dump();

	g_BusinessServiceSet->BusNMRX->WriteSimple(Ret_pStatus,lstNMRX,"");

}