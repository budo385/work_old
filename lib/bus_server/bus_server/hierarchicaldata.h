#ifndef HIERARCHICALDATA_H
#define HIERARCHICALDATA_H

#include "common/common/status.h"
#include "common/common/dbrecordset.h"
#include "db/db/dbsqlmanager.h"

#include "db_core/db_core/dbsqltabledefinition.h"


//For inserting/editing/deleting hier data, call initialize first
//WARNING: used on serverside as multithread shared object: do not change state of object or use mutex

class HierarchicalData 
{
public:

	HierarchicalData();
	~HierarchicalData();

	void Initialize(int nTableID,DbSqlManager* pDatabaseManager,int nWriteView=-1,bool bReturnShortViewAfterWrite=false);
	void Write(Status &Ret_pStatus, DbRecordSet &Data, QString pLockResourceID = "", QString strSQLWhereFilter="") const;
	void Delete(Status &Ret_pStatus, DbRecordSet &Data, QString pLockResourceID = "", QString strSQLWhereFilter="") const;
	bool GetDescendantsNodes(const QString &strCode, DbRecordSet &Data, QString strGroupTreeFilter, bool bOnlyActiveProjects=true, int nLevelsDeep=-1) const;
	bool GetCodeFromID(Status &status, int nRecordID, QString &strCode, QString strGroupTreeFilter) const;

protected:
	void DefineHctReturnList(DbRecordSet& pList) const;
	bool GetAncestorNodes(const QString &strCode, DbRecordSet &Data, QString strGroupTreeFilter) const;
	bool GetChildNodes(const QString &strCode, DbRecordSet &Data, QString strGroupTreeFilter) const;
	
	//B.T. fixed all on 22.03.2013: levels are now fixed, based on number of dots...
	bool OnCodeChanged(Status &status, QString strSQLWhereFilter,QString strOldCode, QString strNewCode) const;
	bool UpdateChildren(Status &status, DbRecordSet &lstParentNodes,QString strSQLWhereFilter) const;
	bool UpdateParent(Status &status, DbRecordSet &lstChildNodes,QString strSQLWhereFilter) const;
	bool UpdateHasChildren(Status &status, int nParentID,QString strSQLWhereFilter,int bSetHasChildrenNoCheck=false) const;

	int  CalculateSibling(const QString &strCode, int nParentID, QString strGroupTreeFilter) const;
	int	 CalcParent(const QString &strCode, int &nParentLevel, QString strGroupTreeFilter) const;
	QString GetSelectNames(const DbRecordSet &lstNodes) const;
	int CalculateChildCount(const QString &strCode, QString strGroupTreeFilter) const;
	int CalculateChildCount(int nRecordID, QString strGroupTreeFilter) const;
	bool GetIDFromCode(Status &status, const QString &strCode, int &nRecordID, QString strGroupTreeFilter) const;
	
	DbSqlManager	*m_pDbManager;
	int				m_nTableID;
	DbTableKeyData	m_pTableKeyData;
	int				m_nWriteView;					//< Alternative Write Query, when writing data (must have only fields from parent table)
	bool			m_bReturnShortViewAfterWrite;
};

#endif // HIERARCHICALDATA_H
