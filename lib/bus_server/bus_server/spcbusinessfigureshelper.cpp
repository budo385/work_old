#include "spcbusinessfigureshelper.h"

#define TOT(X) Ret_lstData.getDataRef(0,X).toDouble()
#define PROJ(X) lstProjects.getDataRef(nRowIdx,X).toDouble()
#define PROJ_INT(X) lstProjects.getDataRef(nRowIdx,X).toInt()
#define PROJ_STR(X) lstProjects.getDataRef(nRowIdx,X).toString()
#define DEF(X) recFLDefault.getDataRef(0,X).toInt()


SpcBusinessFiguresHelper::SpcBusinessFiguresHelper()
{

}

SpcBusinessFiguresHelper::~SpcBusinessFiguresHelper()
{

}


//init list once more
void SpcBusinessFiguresHelper::InitVars_151(DbRecordSet &Ret_lstData)
{
	Ret_lstData.setData(0,"T_ZEIT_AUFG",0);
	Ret_lstData.setData(0,"T_AUFG_ZEIT_NF",0);

	Ret_lstData.setData(0,"T_AUFW_HON",0);
	Ret_lstData.setData(0,"T_HONORAR_AUFG",0);
	Ret_lstData.setData(0,"T_AUFG_HON_NF",0);
	Ret_lstData.setData(0,"T_AUFW_HON_NF",0);

	Ret_lstData.setData(0,"T_AUFW_NK",0);
	Ret_lstData.setData(0,"T_NK_AUFG",0);
	Ret_lstData.setData(0,"T_AUFG_NK_NF",0);
	Ret_lstData.setData(0,"T_AUFW_NK_NF",0);

	Ret_lstData.setData(0,"T_FREMDKOST_AUFW",0);
	Ret_lstData.setData(0,"T_FREMDKOST_AUFG",0);
	Ret_lstData.setData(0,"T_AUFG_FK_NF_NULL",0);

	Ret_lstData.setData(0,"T_FREMDLEIST_AUFW",0);
	Ret_lstData.setData(0,"T_FREMDLEIST_AUFG",0);

	Ret_lstData.setData(0,"T_FAKT_HON",0);
	Ret_lstData.setData(0,"T_FAKT_NK",0);
	Ret_lstData.setData(0,"T_FAKT_RAB_ARB",0);
	Ret_lstData.setData(0,"T_FAKT_RAB_NK",0);
	Ret_lstData.setData(0,"T_FAKT_FK",0);
	Ret_lstData.setData(0,"T_FAKT_NK_O_FK",0);

	Ret_lstData.setData(0,"T_ZAHL_HON",0);
	Ret_lstData.setData(0,"T_KUM_GWJ_PROG",0);
	Ret_lstData.setData(0,"T_AUFG_FK_NF_NULL",0);

	Ret_lstData.setData(0,"T_NF_NK_INT",0);
	Ret_lstData.setData(0,"T_NF_NK_EXT",0);
	Ret_lstData.setData(0,"T_NF_FK_INT",0);
	Ret_lstData.setData(0,"T_NF_FK_EXT",0);

	Ret_lstData.setData(0,"T_ARB_AUFG_FAKT",0);
	Ret_lstData.setData(0,"T_ARB_AUFW_FAKT",0);

}

void SpcBusinessFiguresHelper::InitVars_110(DbRecordSet &Ret_lstData)
{
	int nColIdx_Start=Ret_lstData.getColumnIdx("T_KUM_F_HONORAR");
	int nColIdx_End=Ret_lstData.getColumnIdx("T_KUM_F_NK_O_FK");

	for (int i=nColIdx_Start;i<=nColIdx_End;i++)
	{
		Ret_lstData.setData(0,i,0.00);
	}
}

void SpcBusinessFiguresHelper::InitVars_102(DbRecordSet &Ret_lstData)
{




}

void SpcBusinessFiguresHelper::InitVars_112( DbRecordSet &Ret_lstData )
{

}

void SpcBusinessFiguresHelper::InitVars_117( DbRecordSet &Ret_lstData )
{

}