#ifndef SERVICE_BUSINESSHELPER_H
#define SERVICE_BUSINESSHELPER_H


#include "businessservice.h"



/*!
	\class Service_BusinessHelper
	\brief Core User API's
	\ingroup Bus_Services

	Provides methods for server side helper function in complex insert/edit operations.
	Operations are more or less common to all tables, so this is try to summarize all this in one place.


*/

class Service_BusinessHelper : public BusinessService
{
public:

	void ClientTaskNotification(Status &Ret_pStatus,DbRecordSet &recScheduledTask, bool bNewTask=true);
	void AssignChildToParent(DbRecordSet &lstParent,QString strChildSubListCol,DbRecordSet &lstChilds,QString strFKCol,QString strParentIDCol="");
	void AssignErrorsToParentStatuses(DbRecordSet &lstSubData, DbRecordSet &lstParentStatuses, DbRecordSet& lstSubStatuses,int nCntIDIdx);
	void WriteSubList(int nSubData_TableID, int nSubDataCol_Idx, int nSubListFKtoParentIdx,int nSubDataSkipColWrite,DbRecordSet &lstParentData, DbRecordSet& lstParentStatus,DbRecordSet& lstRet_SubData,DbRecordSet& lstRet_SubStatus);
	void WriteEntityPictures(Status &Ret_pStatus, DbRecordSet &RetOut_Data, QString strBigPicIDColName, QString strPreviewPicColName);
	void ReadEntityPictures(Status &Ret_pStatus,DbRecordSet &Ret_Data,QString strBigPicIDColName);
	void Blob2FileChunked(Status &Ret_pStatus,QString strSQLSelect, QString strFilePath,bool bDecompress=false);
	void File2BlobChunked(Status &Ret_pStatus,QString strSQLUpdate, QString strFilePath);
	void GetOptionsAndSettings(Status &pStatus, int nPersonID, DbRecordSet &recSettings, DbRecordSet &recOptions);
	int  GetRecordIDFromTask(Status &Ret_pStatus, int nTaskID);
};

#endif // SERVICE_BUSINESSHELPER_H
