#ifndef BUSINESSSERVICESET_H
#define BUSINESSSERVICESET_H

#include "bus_interface/bus_interface/interface_businessserviceset.h"
#include "businessservice_collection.h"
#include "bus_core/bus_core/usersessionmanager.h"
#include "db/db/dbsqlmanager.h"


/*!
    \class BusinessServiceSet
    \brief Repository of all business objects, use on app. server side
    \ingroup Bus_Services

	Instanced by BusinessServiceSetProxy in thick client mode. Redirects all business calls directly to business services.

	Use:
	-when making new business object you must register it here (constructor)
	-pointer of bo object must be declared in interface
*/

class BusinessServiceSet: public Interface_BusinessServiceSet
{
public:
	BusinessServiceSet(DbSqlManager *pDbManager,UserSessionManager* pSessionManager);
	~BusinessServiceSet();

	//pointer of your BO must be set in Interface_BusinessServiceSet
	DbSqlManager* GetDbManager(){return m_DbManager;};

protected:

	//init all services
	void InitServiceSet();
	void CheckServiceSet();

	QList<BusinessService *> m_ServiceList;	//< Used to store pointers to bus services
	DbSqlManager *m_DbManager;
	UserSessionManager* m_SessionManager;

};




#endif //BUSINESSSERVICESET_H


