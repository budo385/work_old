#ifndef SERVICE_CLIENTSIMPLEORM_H
#define SERVICE_CLIENTSIMPLEORM_H

#include "bus_interface/bus_interface/interface_clientsimpleorm.h"
#include "businessservice.h"

/*!
\class Interface_ClientSimpleORM
\brief Client simple ORM.
\ingroup Bus_Interface_Collection

Provides methods for simple ORM from client side.

*/
class Service_ClientSimpleORM : public Interface_ClientSimpleORM, public BusinessService
{
public:
	void Write(Status &Ret_pStatus, int nTableID, DbRecordSet &RetOut_pLstForWrite, QString pLockResourceID, int nQueryView, int nSkipLastColumns, DbRecordSet &RetOut_pLstForDelete);
	void WriteParentSubData(Status &Ret_pStatus, int nTableID, DbRecordSet &RetOut_pLstForWrite, int nParentId, QString strColParentName,QString pLockResourceID = "",int nQueryView = -1);
	void Read (Status &Ret_pStatus, int nTableID, DbRecordSet &Ret_pLstRead, QString strWhereClause = "", int nQueryView = -1, bool bDistinct = false);
	void ReadAdv (Status &Ret_pStatus, DbRecordSet &RetOut_pLstRead, QString strJoinClause);
	void ReadFromParentIDs(Status &Ret_pStatus, int nTableID,DbRecordSet &Ret_pLstRead, QString strColFK2ParentIDName, DbRecordSet &pLstOfID,QString strColIDName,QString strWhereClauseBefore="",QString strWhereClauseAfter="",int nQueryView=-1, bool bDistinct = false);
	void GetRowCount(Status &Ret_pStatus, int nTableID, QString strWhereClause, int &Ret_nCount);
	void Delete(Status &Ret_pStatus, int nTableID, DbRecordSet &pLstForDelete, QString pLockResourceID, DbRecordSet &Ret_pLstStatusRows, bool pBoolTransaction);
	void Lock(Status &Ret_pStatus, int nTableID, DbRecordSet &pLstForLocking, QString &Ret_pResourceID, DbRecordSet &Ret_pLstStatusRows, bool bAllOrNothing);
	void UnLock(Status &Ret_pStatus, int nTableID, bool &Ret_bUnLocked, QString &pResourceID);
	void UnLockByRecordID(Status &Ret_pStatus, int nTableID, DbRecordSet &pLstForUnLocking, bool &Ret_bUnLocked);
	void IsLocked(Status &Ret_pStatus, int nTableID, bool &Ret_bLocked, DbRecordSet &LockList);
	void ExecuteSQL(Status &Ret_pStatus, QString strSQL, DbRecordSet &Ret_LstReturn);
	void WriteHierarchicalData(Status &Ret_pStatus, int nTableID,DbRecordSet &RetOut_pLstForWrite, QString pLockResourceID = "", QString strSQLWhereFilter="", int nQueryView = -1);
	void DeleteHierarchicalData(Status &Ret_pStatus, int nTableID,DbRecordSet &pLstForDelete, QString pLockResourceID = "", QString strSQLWhereFilter="");

private:
	int  GetPrimaryColumnNo(int nTableID, DbRecordSet &pLst);
};

#endif // SERVICE_CLIENTSIMPLEORM_H
