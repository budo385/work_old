#ifndef SERVICE_BUSNPROJECT_H
#define SERVICE_BUSNPROJECT_H

#include "businessservice.h"
#include "bus_interface/bus_interface/interface_busproject.h"
#include "hierarchicaldata.h"

/*!
	\class Service_BusProject
	\brief Service_BusProject pattern
	\ingroup Bus_Services
*/

class Service_BusProject : public Interface_BusProject, public BusinessService
{
public:
	Service_BusProject();
	void ReadProjectDataSideBar(Status &Ret_pStatus, int nProjectID,DbRecordSet &Ret_Data,DbRecordSet &DataNMRXPersonFilter,DbRecordSet &Ret_DataNMRXPerson);
	void NewProjectsFromTemplate(Status &Ret_pStatus, DbRecordSet &RetOut_Data, bool bCopyCE);
	void ReadData(Status &Ret_pStatus, int nProjectID,DbRecordSet &Ret_Data,DbRecordSet &DataNMRXPersonFilter,DbRecordSet &Ret_DataNMRXPerson,DbRecordSet &Ret_UAR,DbRecordSet &Ret_GAR, DbRecordSet &Ret_CustomFlds);
	void WriteData(Status &Ret_pStatus,DbRecordSet &RetOut_Data, QString pLockResourceID, DbRecordSet &RetOut_UAR,DbRecordSet &RetOut_GAR, DbRecordSet &RetOut_CustomFlds);
private:
	HierarchicalData m_ProjectData;
};
#endif // SERVICE_BUSNMRX_H