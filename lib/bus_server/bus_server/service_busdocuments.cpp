#include "service_busdocuments.h"
#include "db/db/dbsimpleorm.h"
#include "bus_server/bus_server/businesslocker.h"
#include "bus_core/bus_core/mainentitycalculatedname.h"
#include "bus_core/bus_core/globalconstants.h"
#include "common/common/entity_id_collection.h"
#include "common/common/datahelper.h"

#include "common/common/zipcompression.h"

#include "businessserviceset.h"
extern BusinessServiceSet* g_BusinessServiceSet;
#include "privateserviceset.h"
extern PrivateServiceSet* g_PrivateServiceSet; 
#include "bus_core/bus_core/useraccessright.h"
extern UserAccessRight *g_AccessRight;				//global access right tester
#include "bus_core/bus_core/usersessionmanager.h"
extern UserSessionManager	*g_UserSessionManager;
//#include "common/common/logger.h"
//extern Logger				g_Logger;


void Service_BusDocuments::ReadUserPaths(Status &Ret_pStatus, int nPersonID,DbRecordSet &Ret_Data)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_DM_USER_PATHS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	QString strWhere ="WHERE BDMU_OWNER_ID="+QVariant(nPersonID).toString();

	//load
	TableOrm.Read(Ret_pStatus, Ret_Data, strWhere, DbSqlTableView::TVIEW_BUS_DM_USER_PATHS_SELECT);
	if(!Ret_pStatus.IsOK()) return;

}

void Service_BusDocuments::ReadUserPathsForApplication(Status &Ret_pStatus, int nApplicationID,DbRecordSet &Ret_Data)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_DM_USER_PATHS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	QString strWhere ="WHERE BDMU_APPLICATION_ID="+QVariant(nApplicationID).toString();

	//load
	TableOrm.Read(Ret_pStatus, Ret_Data, strWhere, DbSqlTableView::TVIEW_BUS_DM_USER_PATHS_SELECT);
	if(!Ret_pStatus.IsOK()) return;

}

//read all templates for all users filter private flag:
void Service_BusDocuments::ReadTemplates(Status &Ret_pStatus, int nPersonID,DbRecordSet &Ret_Data)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_DM_DOCUMENTS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	//B.T. to fix: no more private
	QString strWhere =" WHERE BDMD_TEMPLATE_FLAG=1";// AND CENT_OWNER_ID="+QVariant(nPersonID).toString()+" OR (CENT_OWNER_ID <> "+QVariant(nPersonID).toString()+" AND  CENT_IS_PRIVATE =0 AND BDMD_TEMPLATE_FLAG=1)";

	//load
	g_AccessRight->SQLFilterRecords(BUS_DM_DOCUMENTS,strWhere);
	TableOrm.Read(Ret_pStatus, Ret_Data, strWhere,DbSqlTableView::TVIEW_BUS_DM_DOCUMENTS_COMM_ENTITY);
	if(!Ret_pStatus.IsOK()) return;

}

//returns whole lists back (id's)
//WARNING: can write multiple docs: if so, then contact link/project links are copied for each, task count must match doc count!
//Ret_DocumentRevision: only returned ID
void Service_BusDocuments::Write(Status &Ret_pStatus, DbRecordSet &RetOut_Data, DbRecordSet &RetOut_ProjectLink,DbRecordSet &RetOut_ContactLink,DbRecordSet &RetOut_ScheduleTask,DbRecordSet &RetOut_DocumentRevision,QString strLockRes,DbRecordSet &RetOut_UAR,DbRecordSet &RetOut_GAR)
{
	if (RetOut_Data.getRowCount()==0)
		return;
	

	//Make all TableORms:
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_DM_DOCUMENTS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	//make orm for all tables:
	DbSimpleOrm TableOrm_CE_COMM_ENTITY(Ret_pStatus, CE_COMM_ENTITY, GetDbManager());
	if(!Ret_pStatus.IsOK()) return;
	DbSimpleOrm TableOrm_BUS_TASKS(Ret_pStatus, BUS_TASKS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	DbSimpleOrm TableOrm_BUS_DM_REVISIONS(Ret_pStatus, BUS_DM_REVISIONS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	//---------------------------------------
	//			START TRANSACTION
	//---------------------------------------
	TableOrm.GetDbSqlQuery()->BeginTransaction(Ret_pStatus);
	if(!Ret_pStatus.IsOK()) return;
	

	//---------------------------------------
	//write TASK
	//---------------------------------------
	bool bNewTaskCreated=false;
	if (RetOut_ScheduleTask.getRowCount() > 0)
	{
		if (RetOut_ScheduleTask.getDataRef(0,"BTKS_ID").toInt()<=0)
			bNewTaskCreated=true;

		Q_ASSERT(RetOut_ScheduleTask.getRowCount()==RetOut_Data.getRowCount()); //must be same
		TableOrm_BUS_TASKS.Write(Ret_pStatus,RetOut_ScheduleTask);
		if(!Ret_pStatus.IsOK())
		{
			TableOrm.GetDbSqlQuery()->Rollback();
			return;
		}

		//assign tasks back: 1-1
		int nSize=RetOut_ScheduleTask.getRowCount();
		for(int i=0;i<nSize;++i)
		{
			RetOut_Data.setData(i,"CENT_TASK_ID",RetOut_ScheduleTask.getDataRef(i,"BTKS_ID"));
		}
	}
	

	//---------------------------------------
	//write CE_COMM_ENTITY
	//---------------------------------------
	DbRecordSet rowCommEntity;
	rowCommEntity.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_CE_COMM_ENTITY));
	rowCommEntity.merge(RetOut_Data);

	TableOrm_CE_COMM_ENTITY.Write(Ret_pStatus,rowCommEntity);
	if(!Ret_pStatus.IsOK())
	{
		TableOrm.GetDbSqlQuery()->Rollback();
		return;
	}

	//assign doc id back: 1-1
	int nSize=rowCommEntity.getRowCount();
	for(int i=0;i<nSize;++i)
	{
		RetOut_Data.setData(i,"BDMD_COMM_ENTITY_ID",rowCommEntity.getDataRef(i,"CENT_ID"));
		RetOut_Data.setData(i,"CENT_ID",rowCommEntity.getDataRef(i,"CENT_ID"));
	}
	
		
	//---------------------------------------
	//write DOC
	//---------------------------------------
	DbRecordSet rowDoc;
	rowDoc.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_DM_DOCUMENTS));
	rowDoc.merge(RetOut_Data);

	//issue 2073: for all note documents update size to size of text field:desc:
	int nSizeT=rowDoc.getRowCount();
	for(int i=0;i<nSizeT;i++)
	{
		if (rowDoc.getDataRef(i,"BDMD_DOC_TYPE").toInt()==GlobalConstants::DOC_TYPE_NOTE)
		{
			int nSize=rowDoc.getDataRef(i,"BDMD_DESCRIPTION").toString().size();
			rowDoc.setData(i,"BDMD_SIZE",nSize);
		}
	}

	
	//--------------------------------
	//issue 1870: read template flag, test if it is reseted from 1 to 0:
	//--------------------------------

	int nDocID=rowDoc.getDataRef(0,"BDMD_ID").toInt();
	int nTemplateFlag=0;
	if (nDocID>0)
	{
		QString strSQL="SELECT BDMD_TEMPLATE_FLAG FROM BUS_DM_DOCUMENTS WHERE BDMD_ID="+QVariant(nDocID).toString();
		TableOrm.GetDbSqlQuery()->Execute(Ret_pStatus,strSQL);
		if(!Ret_pStatus.IsOK())
		{
			TableOrm.GetDbSqlQuery()->Rollback();
			return;
		}
		DbRecordSet rowData;
		TableOrm.GetDbSqlQuery()->FetchData(rowData);
		if (rowData.getRowCount()>0 && rowData.getColumnCount()>0)
		{
			nTemplateFlag=rowData.getDataRef(0,0).toInt();
		}
	}

	//issue 1986: reset template ID if no longer templates
	CheckTemplates(Ret_pStatus,rowDoc,BUS_DM_DOCUMENTS,TableOrm.GetDbSqlQuery());
	if(!Ret_pStatus.IsOK())
	{
		TableOrm.GetDbSqlQuery()->Rollback();
		return;
	}
	

	TableOrm.Write(Ret_pStatus,rowDoc);
	if(!Ret_pStatus.IsOK())
	{
		TableOrm.GetDbSqlQuery()->Rollback();
		return;
	}

	//assign doc id back: 1-1
	int nRevisionDocIdx=RetOut_DocumentRevision.getColumnIdx("BDMR_DOCUMENT_ID");
	int nSize3=rowDoc.getRowCount();
	int nSizeRev=RetOut_DocumentRevision.getRowCount();

	for(int i=0;i<nSize3;++i)
	{
		int nDocID=rowDoc.getDataRef(i,"BDMD_ID").toInt();
		RetOut_Data.setData(i,"BDMD_ID",nDocID);
		if (nRevisionDocIdx!=-1 && nSizeRev==nSize3)
			RetOut_DocumentRevision.setData(i,nRevisionDocIdx,nDocID);
	}


	//---------------------------------------
	//write REVISION
	//---------------------------------------
	if (RetOut_DocumentRevision.getRowCount()>0)
	{
		TableOrm_BUS_DM_REVISIONS.Write(Ret_pStatus,RetOut_DocumentRevision);
		if(!Ret_pStatus.IsOK())
		{
			TableOrm.GetDbSqlQuery()->Rollback();
			return;
		}

		//return only ID:
		DbRecordSet lstTempReturn;
		lstTempReturn.addColumn(QVariant::Int,"BDMR_ID");
		lstTempReturn.merge(RetOut_DocumentRevision);
		RetOut_DocumentRevision=lstTempReturn;
	}

	//---------------------------------------
	//write PROJECT
	//---------------------------------------
	//RetOut_ProjectLink.Dump();

	g_BusinessServiceSet->BusNMRX->WriteCEProjectLink(Ret_pStatus,rowCommEntity,RetOut_ProjectLink);
	if (!Ret_pStatus.IsOK())
	{
		TableOrm.GetDbSqlQuery()->Rollback();
		return;
	}

	//---------------------------------------
	//write CONTACT
	//---------------------------------------
	RetOut_ContactLink.setColValue("BNMR_SYSTEM_ROLE",GlobalConstants::CONTACT_LINK_ROLE_DOC_OWNER); //set doc roles to owner
	g_BusinessServiceSet->BusNMRX->WriteCEContactLink(Ret_pStatus,rowCommEntity,RetOut_ContactLink);
	if (!Ret_pStatus.IsOK())
	{
		TableOrm.GetDbSqlQuery()->Rollback();
		return;
	}

	//--------------------------------
	//issue 1870: reset all flags if doc is no longer template: BDMD_TEMPLATE_ID in this row and in APP row
	//--------------------------------
	if (nDocID>0 && nTemplateFlag==1 && rowDoc.getDataRef(0,"BDMD_TEMPLATE_FLAG").toInt()==0)
	{
		QString strSQL="UPDATE BUS_DM_DOCUMENTS SET BDMD_TEMPLATE_ID=NULL WHERE BDMD_TEMPLATE_ID="+QVariant(nDocID).toString();
		TableOrm.GetDbSqlQuery()->Execute(Ret_pStatus,strSQL);
		if(!Ret_pStatus.IsOK())
		{
			TableOrm.GetDbSqlQuery()->Rollback();
			return;
		}
		strSQL="UPDATE BUS_DM_APPLICATIONS SET BDMA_TEMPLATE_ID=NULL WHERE BDMA_TEMPLATE_ID="+QVariant(nDocID).toString();
		TableOrm.GetDbSqlQuery()->Execute(Ret_pStatus,strSQL);
		if(!Ret_pStatus.IsOK())
		{
			TableOrm.GetDbSqlQuery()->Rollback();
			return;
		}
		strSQL="UPDATE BUS_DM_APPLICATIONS SET BDMA_INSTALL_APP_TEMPL_ID=NULL WHERE BDMA_INSTALL_APP_TEMPL_ID="+QVariant(nDocID).toString();
		TableOrm.GetDbSqlQuery()->Execute(Ret_pStatus,strSQL);
		if(!Ret_pStatus.IsOK())
		{
			TableOrm.GetDbSqlQuery()->Rollback();
			return;
		}
		strSQL="UPDATE BUS_DM_APPLICATIONS SET BDMA_INSTALL_VIEWER_TEMPL_ID=NULL WHERE BDMA_INSTALL_VIEWER_TEMPL_ID="+QVariant(nDocID).toString();
		TableOrm.GetDbSqlQuery()->Execute(Ret_pStatus,strSQL);
		if(!Ret_pStatus.IsOK())
		{
			TableOrm.GetDbSqlQuery()->Rollback();
			return;
		}
	}

	//UAR/GAR (set UAR/GAR for all rows
	if (RetOut_Data.getRowCount()>0)
	{
		//if (RetOut_GAR.getRowCount()>0 || RetOut_UAR.getRowCount()>0)
		//{
			int nSize=RetOut_Data.getRowCount();
			for(int i=0;i<nSize;i++)
			{
				int nID=RetOut_Data.getDataRef(0,"BDMD_ID").toInt();
				g_BusinessServiceSet->AccessRights->WriteGUAR(Ret_pStatus,nID,BUS_DM_DOCUMENTS,RetOut_UAR,RetOut_GAR);
				if (!Ret_pStatus.IsOK())
				{
					TableOrm.GetDbSqlQuery()->Rollback();
					return;
				}
			}
		//}
		g_BusinessServiceSet->AccessRights->UpdateProjectCommEntityGUAR(Ret_pStatus,BUS_DM_DOCUMENTS,RetOut_Data);
		if(!Ret_pStatus.IsOK()) 
		{
			TableOrm.GetDbSqlQuery()->Rollback();
			return;
		}
		int nID=RetOut_Data.getDataRef(0,"BDMD_ID").toInt();
		//re-read uar/gar (only for inherit-enabled entity: project, voice_call, email, doc)
		g_BusinessServiceSet->AccessRights->ReadGUAR(Ret_pStatus,nID,BUS_DM_DOCUMENTS,RetOut_UAR,RetOut_GAR);
		if(!Ret_pStatus.IsOK()) 
		{
			TableOrm.GetDbSqlQuery()->Rollback();
			return;
		}
	}

	//---------------------------------------
	//			COMIT TRANSACTION
	//---------------------------------------
	TableOrm.GetDbSqlQuery()->Commit(Ret_pStatus);
	if(!Ret_pStatus.IsOK()) return;

	//---------------------------------------
	//			UNLOCK:
	//---------------------------------------

	if (!strLockRes.isEmpty())
	{
		BusinessLocker Locker(Ret_pStatus, CE_COMM_ENTITY, GetDbManager());
		if(!Ret_pStatus.IsOK())return;
		Locker.UnLock(Ret_pStatus,strLockRes);
	}


	if (Ret_pStatus.IsOK())
	{
		g_PrivateServiceSet->BusinessHelper->ClientTaskNotification(Ret_pStatus,RetOut_ScheduleTask,bNewTaskCreated);
	}

}


void Service_BusDocuments::Read(Status &Ret_pStatus, int nDocumentID, DbRecordSet &Ret_Data, DbRecordSet &Ret_ProjectLink,DbRecordSet &Ret_ContactLink,DbRecordSet &Ret_ScheduleTask,DbRecordSet &Ret_Revisions, DbRecordSet &Ret_CheckOutInfo,DbRecordSet &Ret_UAR,DbRecordSet &Ret_GAR)
{

	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_DM_DOCUMENTS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	QString strWhere ="WHERE BDMD_ID="+QVariant(nDocumentID).toString();

	//load
	TableOrm.Read(Ret_pStatus, Ret_Data, strWhere, DbSqlTableView::TVIEW_BUS_DM_DOCUMENTS_COMM_ENTITY);
	if(!Ret_pStatus.IsOK()) return;

	//empty row?
	if (Ret_Data.getRowCount()==0)
	{
		Ret_pStatus.setError(StatusCodeSet::ERR_BUS_FAILED_TO_LOAD_RECORD);
		return;
	}

	int nCommEntityID=Ret_Data.getDataRef(0,"CENT_ID").toInt();
	int nTaskEntityID=Ret_Data.getDataRef(0,"CENT_TASK_ID").toInt();

	//read linkage tables:
	if (nCommEntityID!=0)
	{
		//read contact link:
		strWhere ="WHERE BNMR_TABLE_KEY_ID_1="+QVariant(nCommEntityID).toString()+" AND "+NMRX_CE_CONTACT_WHERE;
		g_BusinessServiceSet->ClientSimpleORM->Read(Ret_pStatus,BUS_NMRX_RELATION,Ret_ContactLink,strWhere,-1,false);
		if(!Ret_pStatus.IsOK()) return;

		//read project link
		strWhere ="WHERE BNMR_TABLE_KEY_ID_1="+QVariant(nCommEntityID).toString()+" AND "+NMRX_CE_PROJECT_WHERE;
		g_BusinessServiceSet->ClientSimpleORM->Read(Ret_pStatus,BUS_NMRX_RELATION,Ret_ProjectLink,strWhere,-1,false);
		if(!Ret_pStatus.IsOK()) return;
	}


	//read task records:
	if (nTaskEntityID!=0)
	{
		strWhere ="WHERE BTKS_ID="+QVariant(nTaskEntityID).toString();
		g_BusinessServiceSet->ClientSimpleORM->Read(Ret_pStatus,BUS_TASKS,Ret_ScheduleTask,strWhere,-1,false);
		if(!Ret_pStatus.IsOK()) return;
	}

	//reload revisions:
	ReloadRevisions(Ret_pStatus,nDocumentID,Ret_CheckOutInfo,Ret_Revisions);
	if(!Ret_pStatus.IsOK()) return;
	//read uar/gar:
	g_BusinessServiceSet->AccessRights->ReadGUAR(Ret_pStatus,nDocumentID,BUS_DM_DOCUMENTS,Ret_UAR,Ret_GAR);

}

void Service_BusDocuments::ReadNoteDocument(Status &Ret_pStatus, int nDocumentID, QString &Ret_strData)
{
	//Make local query, can fail if connection reservation fails.
	DbSqlQuery query(Ret_pStatus, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	QString strSQL ="SELECT BDMD_DESCRIPTION FROM BUS_DM_DOCUMENTS WHERE BDMD_ID="+QVariant(nDocumentID).toString();
	query.Execute(Ret_pStatus, strSQL);
	if(!Ret_pStatus.IsOK()) return;

	if (query.next())
	{
		Ret_strData=query.value(0).toString();
	}
}


//reload revisions
void Service_BusDocuments::ReloadRevisions(Status &Ret_pStatus, int nDocumentID, DbRecordSet &Ret_CheckOutInfo, DbRecordSet &Ret_Revisions )
{
	//Make all TableORms:
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_DM_DOCUMENTS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	DbSimpleOrm TableOrm_BUS_DM_REVISIONS(Ret_pStatus, BUS_DM_REVISIONS, GetDbManager()); 


	//load check info:
	QString strWhere ="WHERE BDMD_ID="+QVariant(nDocumentID).toString();
	TableOrm.Read(Ret_pStatus, Ret_CheckOutInfo, strWhere, DbSqlTableView::TVIEW_BUS_DM_CHECK_OUT_INFO);
	if(!Ret_pStatus.IsOK()) return;

	//load revisions:
	strWhere ="WHERE BDMR_DOCUMENT_ID="+QVariant(nDocumentID).toString();
	TableOrm_BUS_DM_REVISIONS.Read(Ret_pStatus, Ret_Revisions, strWhere, DbSqlTableView::TVIEW_BUS_DM_REVISIONS_SELECT);
	if(!Ret_pStatus.IsOK()) return;

	//fill last col with name:
	//calculated names: 
	MainEntityCalculatedName filler;
	filler.Initialize(ENTITY_BUS_PERSON,Ret_Revisions);
	filler.FillCalculatedNames(Ret_Revisions,Ret_Revisions.getColumnIdx("USER_NAME"));

}

//check in, if not already checked: lock record directly on DB, while updating....
void Service_BusDocuments::CheckIn(Status &Ret_pStatus, int nDocumentID, DbRecordSet &RetOut_DocumentRevision,bool bOverWrite,DbRecordSet &Ret_CheckOutInfo, bool bSkipLock,int nSize )
{
	if (RetOut_DocumentRevision.getRowCount()==0)
		return;

	int nCheckInPersonID=RetOut_DocumentRevision.getDataRef(0,"BDMR_CHECK_IN_USER_ID").toInt();

	DbSimpleOrm TableOrm(Ret_pStatus, BUS_DM_DOCUMENTS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	DbSimpleOrm TableOrm_BUS_DM_REVISIONS(Ret_pStatus, BUS_DM_REVISIONS, GetDbManager()); 
	BusinessLocker Locker(Ret_pStatus, BUS_DM_DOCUMENTS, GetDbManager());
	if(!Ret_pStatus.IsOK()) return;

	QString strCE_EntityLock;
	if (!bSkipLock)
	{
		strCE_EntityLock=LockDocument(Ret_pStatus,nDocumentID);
		if(!Ret_pStatus.IsOK()) return;
	}


	//select doc record, lock, test if flag is set
	QString strLockRes;
	DbLockList listLock;
	listLock<<nDocumentID;

	//lock if not already:
	bool bLocked=false;
	int nLocked=Locker.IsLockedByMe(Ret_pStatus,BUS_DM_DOCUMENTS,listLock);
	if (nLocked==1 )
		bLocked=true;

	if(!Ret_pStatus.IsOK()) 
	{
		UnLockDocument(strCE_EntityLock);
		return;
	}

	if (!bLocked)
	{
		Locker.Lock(Ret_pStatus,listLock,strLockRes);
		if(!Ret_pStatus.IsOK()) 
		{
			UnLockDocument(strCE_EntityLock);
			return;
		}
	}

	//load check info:
	QString strWhere ="WHERE BDMD_ID="+QVariant(nDocumentID).toString();
	TableOrm.Read(Ret_pStatus, Ret_CheckOutInfo, strWhere, DbSqlTableView::TVIEW_BUS_DM_CHECK_OUT_INFO);
	if(!Ret_pStatus.IsOK()) 	
	{
		Status err;
		if (!strLockRes.isEmpty())
			Locker.UnLock(err,strLockRes);
		UnLockDocument(strCE_EntityLock);
		return;
	}

	//if doc record not found exit
	if (Ret_CheckOutInfo.getRowCount()==0)
	{
		Status err;
		if (!strLockRes.isEmpty())
			Locker.UnLock(err,strLockRes);
		UnLockDocument(strCE_EntityLock);
		Ret_pStatus.setError(StatusCodeSet::ERR_BUS_FAILED_TO_LOAD_RECORD); 
		return;
	}

	//security check: did we check out doc?
	//if overwrite flag is not set and some1 checked out document, some other user
	if (!bOverWrite && Ret_CheckOutInfo.getDataRef(0,"BDMD_IS_CHECK_OUT").toInt()>0 && nCheckInPersonID!=Ret_CheckOutInfo.getDataRef(0,"BDMD_CHECK_OUT_USER_ID").toInt())
	{
		Status err;
		if (!strLockRes.isEmpty())
			Locker.UnLock(err,strLockRes);

		Ret_pStatus.setError(StatusCodeSet::ERR_BUS_FAILED_TO_CHECK_IN_DOCUMENT); 
		UnLockDocument(strCE_EntityLock);
		return;
	}

	//begin tran
	TableOrm.GetDbSqlQuery()->BeginTransaction(Ret_pStatus);
	if(!Ret_pStatus.IsOK()) 
	{
		Status err;
		if (!strLockRes.isEmpty())
			Locker.UnLock(err,strLockRes);

		UnLockDocument(strCE_EntityLock);
		return;
	}


	//_DUMP(RetOut_DocumentRevision);

	//write revision
	TableOrm_BUS_DM_REVISIONS.Write(Ret_pStatus,RetOut_DocumentRevision); 
	if(!Ret_pStatus.IsOK()) 
	{
		TableOrm.GetDbSqlQuery()->Rollback();
		Status err;
		if (!strLockRes.isEmpty())
			Locker.UnLock(err,strLockRes);

		UnLockDocument(strCE_EntityLock);
		return;
	}


	//clear check out status:
	QVariant intNull(QVariant::Int);
	Ret_CheckOutInfo.setData(0,"BDMD_IS_CHECK_OUT",0);
	Ret_CheckOutInfo.setData(0,"BDMD_CHECK_OUT_USER_ID",intNull);
	Ret_CheckOutInfo.setData(0,"BDMD_SIZE",nSize);
	

	TableOrm.Write(Ret_pStatus,Ret_CheckOutInfo,DbSqlTableView::TVIEW_BUS_DM_CHECK_OUT_INFO); 
	if(!Ret_pStatus.IsOK()) 
	{
		TableOrm.GetDbSqlQuery()->Rollback();
		Status err;
		if (!strLockRes.isEmpty())
			Locker.UnLock(err,strLockRes);

		UnLockDocument(strCE_EntityLock);
		return;
	}

	TableOrm.GetDbSqlQuery()->Commit(Ret_pStatus);



	Status err;
	if (!strLockRes.isEmpty())
		Locker.UnLock(err,strLockRes);

	UnLockDocument(strCE_EntityLock);


	//return only ID for revision:
	DbRecordSet lstTempReturn;
	lstTempReturn.addColumn(QVariant::Int,"BDMR_ID");
	lstTempReturn.merge(RetOut_DocumentRevision);
	RetOut_DocumentRevision=lstTempReturn;

}


//checks out doc: if already checked fires error:  ERR_BUS_FAILED_TO_CHECK_OUT_DOCUMENT, Ret_CheckOutInfo contains info about checked out person
//Ret_DocumentRevision is returned but without content data..
void Service_BusDocuments::CheckOut(Status &Ret_pStatus, int nDocumentID, int nCheckOutPersonID,DbRecordSet &Ret_DocumentRevision,bool bReadOnly,DbRecordSet &Ret_CheckOutInfo,QString strCheckOutPath,bool bSkipLock,int nRevisionID)
{
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_DM_DOCUMENTS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	DbSimpleOrm TableOrm_BUS_DM_REVISIONS(Ret_pStatus, BUS_DM_REVISIONS, GetDbManager()); 
	BusinessLocker Locker(Ret_pStatus, BUS_DM_DOCUMENTS, GetDbManager());
	if(!Ret_pStatus.IsOK()) return;

	QString strCE_EntityLock;
	if (!bSkipLock && !bReadOnly)
	{
		strCE_EntityLock=LockDocument(Ret_pStatus,nDocumentID);
		if(!Ret_pStatus.IsOK()) return;
	}

	//select doc record, lock, test if flag is set
	QString strLockRes;
	DbLockList listLock;
	listLock<<nDocumentID;
	//lock if not already:
	bool bLocked=false;
	int nLocked=Locker.IsLockedByMe(Ret_pStatus,BUS_DM_DOCUMENTS,listLock);
	if (nLocked==1 )
		bLocked=true;

	if (!bLocked)
	{
		Locker.Lock(Ret_pStatus,listLock,strLockRes);
		if(!Ret_pStatus.IsOK()) 
		{
			UnLockDocument(strCE_EntityLock);
			return;
		}
	}



	//load check info:
	QString strWhere ="WHERE BDMD_ID="+QVariant(nDocumentID).toString();
	TableOrm.Read(Ret_pStatus, Ret_CheckOutInfo, strWhere, DbSqlTableView::TVIEW_BUS_DM_CHECK_OUT_INFO);
	if(!Ret_pStatus.IsOK()) 	
	{
		Status err;
		if (!strLockRes.isEmpty())
			Locker.UnLock(err,strLockRes);
		UnLockDocument(strCE_EntityLock);
		return;
	}

	//if doc record not found exit
	if (Ret_CheckOutInfo.getRowCount()==0)
	{
		Status err;
		if (!strLockRes.isEmpty())
			Locker.UnLock(err,strLockRes);
		UnLockDocument(strCE_EntityLock);
		Ret_pStatus.setError(StatusCodeSet::ERR_BUS_FAILED_TO_LOAD_RECORD); 
		return;
	}


	//check out:
	if (!bReadOnly)
	{
		if (Ret_CheckOutInfo.getDataRef(0,"BDMD_IS_CHECK_OUT").toInt()>0 )
		{
			Status err;
			if (!strLockRes.isEmpty())
				Locker.UnLock(err,strLockRes);
			UnLockDocument(strCE_EntityLock);
			Ret_pStatus.setError(StatusCodeSet::ERR_BUS_FAILED_TO_CHECK_OUT_DOCUMENT); 
			//Ret_CheckOutInfo.Dump();
			return;
		}

		//set check in info:
		//Ret_CheckOutInfo.clear();
		//Ret_CheckOutInfo.addRow();
		Ret_CheckOutInfo.setData(0,"BDMD_IS_CHECK_OUT",1);
		Ret_CheckOutInfo.setData(0,"BDMD_CHECK_OUT_USER_ID",nCheckOutPersonID);
		Ret_CheckOutInfo.setData(0,"BDMD_CHECK_OUT_DATE",QDateTime::currentDateTime());
		Ret_CheckOutInfo.setData(0,"BDMD_DOC_PATH",strCheckOutPath);
		

		//Ret_CheckOutInfo.Dump();

		TableOrm.Write(Ret_pStatus,Ret_CheckOutInfo,DbSqlTableView::TVIEW_BUS_DM_CHECK_OUT_INFO);  //skip  person
		if(!Ret_pStatus.IsOK()) 
		{
			Status err;
			if (!strLockRes.isEmpty())
				Locker.UnLock(err,strLockRes);
			UnLockDocument(strCE_EntityLock);
			return;
		}

		//reload info to get person name:
		QString strWhere ="WHERE BDMD_ID="+QVariant(nDocumentID).toString();
		TableOrm.Read(Ret_pStatus, Ret_CheckOutInfo, strWhere, DbSqlTableView::TVIEW_BUS_DM_CHECK_OUT_INFO);
		if(!Ret_pStatus.IsOK()) 
		{
			Status err;
			if (!strLockRes.isEmpty())
				Locker.UnLock(err,strLockRes);
			UnLockDocument(strCE_EntityLock);
			return;
		}
	}

	//get latest revision:
	//DbRecordSet lstRevisions;
	if (nRevisionID==-1)
		strWhere ="WHERE BDMR_DOCUMENT_ID="+QString::number(nDocumentID)+" AND BDMR_DAT_CHECK_IN = (SELECT MAX(BDMR_DAT_CHECK_IN) FROM BUS_DM_REVISIONS WHERE BDMR_DOCUMENT_ID="+QString::number(nDocumentID)+")";
	else
		strWhere ="WHERE BDMR_DOCUMENT_ID="+QString::number(nDocumentID)+" AND BDMR_ID="+QString::number(nRevisionID);

	TableOrm_BUS_DM_REVISIONS.Read(Ret_pStatus,Ret_DocumentRevision,strWhere); 

	if(!Ret_pStatus.IsOK()) 
	{
		Status err;
		if (!strLockRes.isEmpty())
			Locker.UnLock(err,strLockRes);
		UnLockDocument(strCE_EntityLock);
		return;
	}

	if (Ret_DocumentRevision.getRowCount()==0) //rev not found:
	{
		Ret_pStatus.setError(StatusCodeSet::ERR_BUS_DOC_REVISION_NOT_FOUND);
		Status err;
		if (!strLockRes.isEmpty())
			Locker.UnLock(err,strLockRes);
		UnLockDocument(strCE_EntityLock);
		return;
	}

	//unlock
	Status err;
	if (!strLockRes.isEmpty())
		Locker.UnLock(err,strLockRes);
	UnLockDocument(strCE_EntityLock);

}



void Service_BusDocuments::CheckOutRevision(Status &Ret_pStatus, int nRevisionID, DbRecordSet &Ret_DocumentRevision )
{
	//Make all TableORms:
	DbSimpleOrm TableOrm_BUS_DM_REVISIONS(Ret_pStatus, BUS_DM_REVISIONS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	//load revision:
	QString strWhere ="WHERE BDMR_ID="+QVariant(nRevisionID).toString();
	TableOrm_BUS_DM_REVISIONS.Read(Ret_pStatus, Ret_DocumentRevision, strWhere); //, DbSqlTableView::TVIEW_BUS_DM_REVISIONS_SELECT);
	if(!Ret_pStatus.IsOK()) return;


}
void Service_BusDocuments::UpdateRevision(Status &Ret_pStatus, int nRevisionID, QString strRevisionTag )
{
	//Make query
	DbSqlQuery query(Ret_pStatus, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	//prepare
	QString strSQL="UPDATE bus_dm_revisions SET BDMR_REVISION_TAG=? WHERE BDMR_ID=?";
	query.Prepare(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK()) return;

	//set values
	query.bindValue(0,strRevisionTag);
	query.bindValue(1,nRevisionID);

	//execute:
	query.ExecutePrepared(Ret_pStatus);
}



//sets status to free
void Service_BusDocuments::CancelCheckOut(Status &Ret_pStatus, int nDocumentID, int nCheckOutPersonID,bool bSkipLock)
{

	DbSimpleOrm TableOrm(Ret_pStatus, BUS_DM_DOCUMENTS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	BusinessLocker Locker(Ret_pStatus, BUS_DM_DOCUMENTS, GetDbManager());
	if(!Ret_pStatus.IsOK()) return;

	QString strCE_EntityLock;
	if (!bSkipLock)
	{
		strCE_EntityLock=LockDocument(Ret_pStatus,nDocumentID);
		if(!Ret_pStatus.IsOK()) return;
	}

	//select doc record, lock, test if flag is set
	QString strLockRes;
	DbLockList listLock;
	listLock<<nDocumentID;
	//Locker.SetDirectLocking();
	Locker.Lock(Ret_pStatus,listLock,strLockRes);
	if(!Ret_pStatus.IsOK()) return;

	DbRecordSet Ret_CheckOutInfo;

	//load check info:
	QString strWhere ="WHERE BDMD_ID="+QVariant(nDocumentID).toString();
	TableOrm.Read(Ret_pStatus, Ret_CheckOutInfo, strWhere, DbSqlTableView::TVIEW_BUS_DM_CHECK_OUT_INFO);
	if(!Ret_pStatus.IsOK()) return;

	//if doc record not found exit
	if (Ret_CheckOutInfo.getRowCount()==0)
	{
		Status err;
		Locker.UnLock(err,strLockRes);
		UnLockDocument(strCE_EntityLock);
		Ret_pStatus.setError(StatusCodeSet::ERR_BUS_FAILED_TO_LOAD_RECORD); 
		return;
	}

	//security check: did we check out doc?
	//if overwrite flag is not set and some1 checked out document, some other user
	//issue: 2543, any other user can cancel check out of other user...
	/*
	if (Ret_CheckOutInfo.getDataRef(0,"BDMD_IS_CHECK_OUT").toInt()>0 && nCheckOutPersonID!=Ret_CheckOutInfo.getDataRef(0,"BDMD_CHECK_OUT_USER_ID").toInt())
	{
		Status err;
		Locker.UnLock(err,strLockRes);
		UnLockDocument(strCE_EntityLock);
		Ret_pStatus.setError(StatusCodeSet::ERR_BUS_FAILED_TO_CHECK_IN_DOCUMENT); 
		return;
	}
	*/


	//clear check out status:
	QVariant intNull(QVariant::Int);
	Ret_CheckOutInfo.setData(0,"BDMD_IS_CHECK_OUT",0);
	Ret_CheckOutInfo.setData(0,"BDMD_CHECK_OUT_USER_ID",intNull);

	TableOrm.Write(Ret_pStatus,Ret_CheckOutInfo,DbSqlTableView::TVIEW_BUS_DM_CHECK_OUT_INFO); 
	if(!Ret_pStatus.IsOK()) 
	{
		Status err;
		Locker.UnLock(err,strLockRes);
		UnLockDocument(strCE_EntityLock);
		return;
	}


	Status err;
	Locker.UnLock(err,strLockRes);
	UnLockDocument(strCE_EntityLock);

}


void Service_BusDocuments::DeleteRevision(Status &Ret_pStatus, int nDocumentID,int nRevisionID, bool bLock)
{
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_DM_REVISIONS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;


	QString strCE_EntityLock;
	//lock cent
	if (bLock)
	{
		strCE_EntityLock=LockDocument(Ret_pStatus,nDocumentID);
		if(!Ret_pStatus.IsOK()) return;
	}

	//delete rev
	//load revision:
	QString strWhere ="WHERE BDMR_ID="+QVariant(nRevisionID).toString();
	QList<int> lstDelete;
	lstDelete<<nRevisionID;
	TableOrm.DeleteFast(Ret_pStatus, lstDelete); 
	if(!Ret_pStatus.IsOK()) return;


	if (bLock)
	{
		UnLockDocument(strCE_EntityLock);
	}
}


void Service_BusDocuments::GetDocumentFileNameAndSize(Status &Ret_pStatus, int nDocumentID, QString &Ret_strFileName, int &Ret_nSize,int nRevisionID)
{
	//Make query
	DbSqlQuery query(Ret_pStatus, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	
	//issue 1472: use filename defined in document, if not defined, use old method:
	QString strSQL="SELECT BDMD_DOC_PATH, BDMD_SIZE FROM bus_dm_documents WHERE BDMD_ID ="+QVariant(nDocumentID).toString();
	query.Execute(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK()) return;

	Ret_strFileName="";
	Ret_nSize=0;
	if(query.next())
	{
		Ret_strFileName = QString::fromUtf8(query.value(0).toByteArray());
		Ret_nSize = query.value(1).toInt();
	}

	//issue 2472: if revision, then use size defined in revision table: need for CRC checksum procedure!
	if (nRevisionID>0)
	{
		QString strSQL="SELECT BDMR_SIZE FROM bus_dm_revisions WHERE BDMR_ID ="+QVariant(nRevisionID).toString();
		query.Execute(Ret_pStatus,strSQL);
		if(!Ret_pStatus.IsOK()) return;
		if(query.next())
			Ret_nSize = query.value(0).toInt();
	}


	if (!Ret_strFileName.isEmpty())
	{
		QFileInfo info(Ret_strFileName);
		Ret_strFileName=info.fileName();
		return;
	}


	strSQL="SELECT DISTINCT BDMR_NAME FROM bus_dm_revisions WHERE BDMR_DOCUMENT_ID ="+QVariant(nDocumentID).toString();
	query.Execute(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK()) return;

	Ret_strFileName="";
	if(query.next())
		Ret_strFileName = query.value(0).toString();

}


void Service_BusDocuments::IsCheckedOut(Status &Ret_pStatus, int nDocumentID, int &Ret_nCheckOutPersonID,QString &Ret_strCheckedOutPath )
{
	//load is checked out
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_DM_DOCUMENTS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	DbRecordSet Ret_CheckOutInfo;

	//load check info:
	QString strWhere ="WHERE BDMD_ID="+QVariant(nDocumentID).toString();
	TableOrm.Read(Ret_pStatus, Ret_CheckOutInfo, strWhere, DbSqlTableView::TVIEW_BUS_DM_CHECK_OUT_INFO);
	if(!Ret_pStatus.IsOK()) return;

	//if doc record not found exit
	if (Ret_CheckOutInfo.getRowCount()==0)
	{
		Ret_pStatus.setError(StatusCodeSet::ERR_BUS_FAILED_TO_LOAD_RECORD); 
		return;
	}


	//if checked return user id, else -1
	if (Ret_CheckOutInfo.getDataRef(0,"BDMD_IS_CHECK_OUT").toInt()>0)
	{
		Ret_nCheckOutPersonID=Ret_CheckOutInfo.getDataRef(0,"BDMD_CHECK_OUT_USER_ID").toInt();
		Ret_strCheckedOutPath=Ret_CheckOutInfo.getDataRef(0,"BDMD_DOC_PATH").toString();
	}
	else
	{
		Ret_nCheckOutPersonID=-1;
		Ret_strCheckedOutPath="";
	}

}


QString Service_BusDocuments::LockDocument(Status &err, int nDocumentID)
{
	BusinessLocker Locker(err, CE_COMM_ENTITY, GetDbManager());
	if(!err.IsOK()) return "";

	//Make query
	DbSqlQuery query(err, GetDbManager()); 
	if(!err.IsOK()) return "";

	QString strSQL="SELECT BDMD_COMM_ENTITY_ID FROM BUS_DM_DOCUMENTS WHERE BDMD_ID ="+QVariant(nDocumentID).toString();
	query.Execute(err,strSQL);
	if(!err.IsOK()) return "";

	int nCE_ID=0;
	if(query.next())
		nCE_ID = query.value(0).toInt();

	if (nCE_ID==0)
	{
		err.setError(StatusCodeSet::ERR_BUS_FAILED_TO_LOAD_RECORD);
		return "";
	}

	QString strLockRes;
	DbLockList listLock;
	listLock<<nCE_ID;
	Locker.Lock(err,listLock,strLockRes);
	return strLockRes;
}


void Service_BusDocuments::UnLockDocument(QString strLocked )
{
	if (strLocked.isEmpty()) 
		return;
	Status err;
	BusinessLocker Locker(err, CE_COMM_ENTITY, GetDbManager());
	if(!err.IsOK()) return;

	Locker.UnLock(err,strLocked);
}

void Service_BusDocuments::GetDocumentProjectAndContact(Status &Ret_pStatus, int nDocumentID, int &Ret_nProjectID,int &Ret_nContactID)
{
	DbSqlQuery query(Ret_pStatus, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	QString strSQL="SELECT BNMR_TABLE_KEY_ID_2 FROM BUS_NMRX_RELATION";
	strSQL+=" INNER JOIN ce_comm_entity ON BNMR_TABLE_KEY_ID_1=CENT_ID";
	strSQL+=" INNER JOIN bus_dm_documents ON BDMD_COMM_ENTITY_ID=CENT_ID";
	strSQL+=" WHERE BDMD_ID="+QVariant(nDocumentID).toString()+" AND "+NMRX_CE_PROJECT_WHERE;


	query.Execute(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK()) return;
	
	if(query.next())
		Ret_nProjectID = query.value(0).toInt();
	else
		Ret_nProjectID=-1;


	strSQL="SELECT BNMR_TABLE_KEY_ID_2 FROM BUS_NMRX_RELATION";
	strSQL+=" INNER JOIN ce_comm_entity ON BNMR_TABLE_KEY_ID_1=CENT_ID";
	strSQL+=" INNER JOIN bus_dm_documents ON BDMD_COMM_ENTITY_ID=CENT_ID";
	strSQL+=" WHERE BDMD_ID="+QVariant(nDocumentID).toString()+" AND "+NMRX_CE_CONTACT_WHERE;


	query.Execute(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK()) return;

	if(query.next())
		Ret_nContactID = query.value(0).toInt();
	else
		Ret_nContactID=-1;


}


//when check.out by drag. do not keep path
void Service_BusDocuments::SetCheckOutPath(Status &Ret_pStatus, int nDocumentID, QString strPath)
{
	DbSqlQuery query(Ret_pStatus, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	QString strSQL="UPDATE BUS_DM_DOCUMENTS SET BDMD_DOC_PATH=? WHERE BDMD_ID=?";

	query.Prepare(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK()) return;

	QByteArray byteStr=strPath.toUtf8(); //path is binary...brb
	query.bindValue(0,byteStr);
	query.bindValue(1,nDocumentID);

	query.ExecutePrepared(Ret_pStatus);
	if(!Ret_pStatus.IsOK()) return;

}

//are templates are really templates: changes recData if not...
void Service_BusDocuments::CheckTemplates(Status &Ret_pStatus,DbRecordSet &recData,int nTableID, DbSqlQuery *query)
{
	QString strSQL="SELECT BDMD_ID FROM BUS_DM_DOCUMENTS WHERE BDMD_TEMPLATE_FLAG = 1 AND BDMD_ID IN ";
	QString strWhere;

	if (nTableID==BUS_DM_DOCUMENTS)
	{
		if (recData.getDataRef(0,"BDMD_TEMPLATE_ID").toInt()>0)
			strWhere="("+recData.getDataRef(0,"BDMD_TEMPLATE_ID").toString()+")";
	}
	else
	{
		if (recData.getDataRef(0,"BDMA_TEMPLATE_ID").toInt()>0)
			strWhere+=recData.getDataRef(0,"BDMA_TEMPLATE_ID").toString();
		if (recData.getDataRef(0,"BDMA_INSTALL_APP_TEMPL_ID").toInt()>0)
			strWhere+=","+recData.getDataRef(0,"BDMA_INSTALL_APP_TEMPL_ID").toString();
		if (recData.getDataRef(0,"BDMA_INSTALL_VIEWER_TEMPL_ID").toInt()>0)
			strWhere+=","+recData.getDataRef(0,"BDMA_INSTALL_VIEWER_TEMPL_ID").toString();
	}

	if (strWhere.isEmpty())
		return;
	if (strWhere.at(0)==QChar(','))
		strWhere=strWhere.remove(0,1);

	query->Execute(Ret_pStatus,strSQL+"("+strWhere+")");
	if(!Ret_pStatus.IsOK()) return;

	DbRecordSet lstData;
	query->FetchData(lstData);

	if (nTableID==BUS_DM_DOCUMENTS)
	{
		if (recData.getDataRef(0,"BDMD_TEMPLATE_ID").toInt()>0)
			if(lstData.find(0,recData.getDataRef(0,"BDMD_TEMPLATE_ID").toInt(),true)<0)
				recData.setData(0,"BDMD_TEMPLATE_ID",QVariant(QVariant::Int));
	}
	else
	{
		if (recData.getDataRef(0,"BDMA_TEMPLATE_ID").toInt()>0)
			if(lstData.find(0,recData.getDataRef(0,"BDMA_TEMPLATE_ID").toInt(),true)<0)
				recData.setData(0,"BDMA_TEMPLATE_ID",QVariant(QVariant::Int));
		if (recData.getDataRef(0,"BDMA_INSTALL_APP_TEMPL_ID").toInt()>0)
			if(lstData.find(0,recData.getDataRef(0,"BDMA_INSTALL_APP_TEMPL_ID").toInt(),true)<0)
				recData.setData(0,"BDMA_INSTALL_APP_TEMPL_ID",QVariant(QVariant::Int));
		if (recData.getDataRef(0,"BDMA_INSTALL_VIEWER_TEMPL_ID").toInt()>0)
			if(lstData.find(0,recData.getDataRef(0,"BDMA_INSTALL_VIEWER_TEMPL_ID").toInt(),true)<0)
				recData.setData(0,"BDMA_INSTALL_VIEWER_TEMPL_ID",QVariant(QVariant::Int));
	}

}

void Service_BusDocuments::WriteApplication(Status &Ret_pStatus, DbRecordSet &RetOut_Data,QString strLockRes)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_DM_APPLICATIONS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	BusinessLocker Locker(Ret_pStatus, BUS_DM_APPLICATIONS, GetDbManager());
	if(!Ret_pStatus.IsOK()) return;

	DbSqlQuery *query=TableOrm.GetDbSqlQuery();
	CheckTemplates(Ret_pStatus,RetOut_Data,BUS_DM_APPLICATIONS,query);
	if(!Ret_pStatus.IsOK()) return;

	TableOrm.Write(Ret_pStatus,RetOut_Data);
	if(!Ret_pStatus.IsOK()) return;

	if (!strLockRes.isEmpty())
	{
		Locker.UnLock(Ret_pStatus,strLockRes);
	}
}

//document is checked out by chunks into server storage directory: saved by filename in strCheckOutPath
void Service_BusDocuments::CheckOutToUserStorage(Status &Ret_pStatus, int nDocumentID, int nCheckOutPersonID,bool bReadOnly,DbRecordSet &Ret_DocumentRevision,DbRecordSet &Ret_CheckOutInfo,QString strCheckOutPath,bool bSkipLock,int nRevisionID)
{
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_DM_DOCUMENTS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	DbSimpleOrm TableOrm_BUS_DM_REVISIONS(Ret_pStatus, BUS_DM_REVISIONS, GetDbManager()); 
	BusinessLocker Locker(Ret_pStatus, BUS_DM_DOCUMENTS, GetDbManager());
	if(!Ret_pStatus.IsOK()) return;


	if (nRevisionID!=-1)bReadOnly=true;

	QString strCE_EntityLock;
	if (!bSkipLock && !bReadOnly)
	{
		strCE_EntityLock=LockDocument(Ret_pStatus,nDocumentID);
		if(!Ret_pStatus.IsOK()) return;
	}

	//select doc record, lock, test if flag is set
	QString strLockRes;
	DbLockList listLock;
	listLock<<nDocumentID;
	//lock if not already:
	bool bLocked=false;
	int nLocked=Locker.IsLockedByMe(Ret_pStatus,BUS_DM_DOCUMENTS,listLock);
	if (nLocked==1 )
		bLocked=true;

	if (!bLocked)
	{
		Locker.Lock(Ret_pStatus,listLock,strLockRes);
		if(!Ret_pStatus.IsOK()) 
		{
			UnLockDocument(strCE_EntityLock);
			return;
		}
	}

	//load check info:
	QString strWhere ="WHERE BDMD_ID="+QVariant(nDocumentID).toString();
	TableOrm.Read(Ret_pStatus, Ret_CheckOutInfo, strWhere, DbSqlTableView::TVIEW_BUS_DM_CHECK_OUT_INFO);
	if(!Ret_pStatus.IsOK()) 	
	{
		Status err;
		if (!strLockRes.isEmpty())
			Locker.UnLock(err,strLockRes);
		UnLockDocument(strCE_EntityLock);
		return;
	}

	//if doc record not found exit
	if (Ret_CheckOutInfo.getRowCount()==0)
	{
		Status err;
		if (!strLockRes.isEmpty())
			Locker.UnLock(err,strLockRes);
		UnLockDocument(strCE_EntityLock);
		Ret_pStatus.setError(StatusCodeSet::ERR_BUS_FAILED_TO_LOAD_RECORD); 
		return;
	}

	//get revision:
	if (nRevisionID==-1)
		strWhere ="WHERE BDMR_DOCUMENT_ID="+QString::number(nDocumentID)+" AND BDMR_DAT_CHECK_IN = (SELECT MAX(BDMR_DAT_CHECK_IN) FROM BUS_DM_REVISIONS WHERE BDMR_DOCUMENT_ID="+QString::number(nDocumentID)+")";
	else
		strWhere ="WHERE BDMR_DOCUMENT_ID="+QString::number(nDocumentID)+" AND BDMR_ID="+QString::number(nRevisionID);

	TableOrm_BUS_DM_REVISIONS.Read(Ret_pStatus, Ret_DocumentRevision, strWhere, DbSqlTableView::TVIEW_BUS_DM_REVISIONS_SELECT);
	if(!Ret_pStatus.IsOK())
	{
		Ret_pStatus.setError(StatusCodeSet::ERR_BUS_DOC_REVISION_NOT_FOUND);
		Status err;
		if (!strLockRes.isEmpty())
			Locker.UnLock(err,strLockRes);
		UnLockDocument(strCE_EntityLock);
		return;
	}
	int nRevID=0;
	if (Ret_DocumentRevision.getRowCount()==0) //rev not found:
	{
		Ret_pStatus.setError(StatusCodeSet::ERR_BUS_DOC_REVISION_NOT_FOUND);
		Status err;
		if (!strLockRes.isEmpty())
			Locker.UnLock(err,strLockRes);
		UnLockDocument(strCE_EntityLock);
		return;
	}
	nRevID=Ret_DocumentRevision.getDataRef(0,"BDMR_ID").toInt();


	//path can be empty: set it here from last rev or path in doc record..
	if (strCheckOutPath.isEmpty())
	{
		strCheckOutPath=Ret_DocumentRevision.getDataRef(0,"BDMR_NAME").toString();
		if (strCheckOutPath.isEmpty())
			strCheckOutPath=Ret_CheckOutInfo.getDataRef(0,"BDMD_DOC_PATH").toString();
		if (strCheckOutPath.isEmpty())
			strCheckOutPath="unknown_name_file";
	}


	//check out:
	if (!bReadOnly)
	{
		if (Ret_CheckOutInfo.getDataRef(0,"BDMD_IS_CHECK_OUT").toInt()>0 )
		{
			Status err;
			if (!strLockRes.isEmpty())
				Locker.UnLock(err,strLockRes);
			UnLockDocument(strCE_EntityLock);
			Ret_pStatus.setError(StatusCodeSet::ERR_BUS_FAILED_TO_CHECK_OUT_DOCUMENT); 
			//Ret_CheckOutInfo.Dump();
			return;
		}

		//set check in info:
		//Ret_CheckOutInfo.clear();
		//Ret_CheckOutInfo.addRow();
		Ret_CheckOutInfo.setData(0,"BDMD_IS_CHECK_OUT",1);
		Ret_CheckOutInfo.setData(0,"BDMD_CHECK_OUT_USER_ID",nCheckOutPersonID);
		Ret_CheckOutInfo.setData(0,"BDMD_CHECK_OUT_DATE",QDateTime::currentDateTime());
		Ret_CheckOutInfo.setData(0,"BDMD_DOC_PATH",strCheckOutPath);


		//Ret_CheckOutInfo.Dump();

		TableOrm.Write(Ret_pStatus,Ret_CheckOutInfo,DbSqlTableView::TVIEW_BUS_DM_CHECK_OUT_INFO);  //skip  person
		if(!Ret_pStatus.IsOK()) 
		{
			Status err;
			if (!strLockRes.isEmpty())
				Locker.UnLock(err,strLockRes);
			UnLockDocument(strCE_EntityLock);
			return;
		}

		//reload info to get person name:
		QString strWhere ="WHERE BDMD_ID="+QVariant(nDocumentID).toString();
		TableOrm.Read(Ret_pStatus, Ret_CheckOutInfo, strWhere, DbSqlTableView::TVIEW_BUS_DM_CHECK_OUT_INFO);
		if(!Ret_pStatus.IsOK()) 
		{
			Status err;
			if (!strLockRes.isEmpty())
				Locker.UnLock(err,strLockRes);
			UnLockDocument(strCE_EntityLock);
			return;
		}
	}


	//unlock
	Status err;
	if (!strLockRes.isEmpty())
		Locker.UnLock(err,strLockRes);
	UnLockDocument(strCE_EntityLock);


	//////////////////------------------CHUNK
	//1. create/find user storage directory...!?->session manager...
	QString strServerFilePath=g_UserSessionManager->InitializeUserStorageDirectory(Ret_pStatus,DataHelper::EncodeSession2Directory(g_UserSessionManager->GetSessionID()));
	if(!Ret_pStatus.IsOK()) 	
		return;
	QFileInfo info(strCheckOutPath);
	strServerFilePath=strServerFilePath+"/"+info.fileName();

	//2. check for disk quota: find blob size!??
	int nSize=Ret_CheckOutInfo.getDataRef(0,"BDMD_SIZE").toInt();
	if(!g_UserSessionManager->IsEnoughDiskSpaceForUserStorage(nSize))
	{
		Ret_pStatus.setError(StatusCodeSet::ERR_BUS_USER_STORAGE_NO_DISK_SPACE);
		return;
	}
	
	//3. store into file by chunks:
	QString strSQL ="SELECT BDMR_CONTENT FROM BUS_DM_REVISIONS WHERE BDMR_ID="+QString::number(nRevID);
	g_PrivateServiceSet->BusinessHelper->Blob2FileChunked(Ret_pStatus,strSQL,strServerFilePath,Ret_DocumentRevision.getDataRef(0,"BDMR_IS_COMPRESSED").toBool());
	//90% files are compressed when inserted into DB: (old system legacy), so please uncompress 'em to be comptaible with web client:

}
// QString strSubDirectory="" subdir inside userstorage..if needed
void Service_BusDocuments::CheckInFromUserStorage(Status &Ret_pStatus, int nDocumentID, int nCheckInPersonID, bool bOverWrite,DbRecordSet &Ret_DocumentRevision,DbRecordSet &Ret_CheckOutInfo, bool bSkipLock,QString strTag, QString strSubDirectory)
{
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_DM_DOCUMENTS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	DbSimpleOrm TableOrm_BUS_DM_REVISIONS(Ret_pStatus, BUS_DM_REVISIONS, GetDbManager()); 
	BusinessLocker Locker(Ret_pStatus, BUS_DM_DOCUMENTS, GetDbManager());
	if(!Ret_pStatus.IsOK()) return;

	QString strCE_EntityLock;
	if (!bSkipLock)
	{
		strCE_EntityLock=LockDocument(Ret_pStatus,nDocumentID);
		if(!Ret_pStatus.IsOK()) return;
	}


	//select doc record, lock, test if flag is set
	QString strLockRes;
	DbLockList listLock;
	listLock<<nDocumentID;

	//lock if not already:
	bool bLocked=false;
	int nLocked=Locker.IsLockedByMe(Ret_pStatus,BUS_DM_DOCUMENTS,listLock);
	if (nLocked==1 )
		bLocked=true;

	if(!Ret_pStatus.IsOK()) 
	{
		UnLockDocument(strCE_EntityLock);
		return;
	}

	if (!bLocked)
	{
		Locker.Lock(Ret_pStatus,listLock,strLockRes);
		if(!Ret_pStatus.IsOK()) 
		{
			UnLockDocument(strCE_EntityLock);
			return;
		}
	}

	//load check info:
	QString strWhere ="WHERE BDMD_ID="+QVariant(nDocumentID).toString();
	TableOrm.Read(Ret_pStatus, Ret_CheckOutInfo, strWhere, DbSqlTableView::TVIEW_BUS_DM_CHECK_OUT_INFO);
	if(!Ret_pStatus.IsOK()) 	
	{
		Status err;
		if (!strLockRes.isEmpty())
			Locker.UnLock(err,strLockRes);
		UnLockDocument(strCE_EntityLock);
		return;
	}

	//if doc record not found exit
	if (Ret_CheckOutInfo.getRowCount()==0)
	{
		Status err;
		if (!strLockRes.isEmpty())
			Locker.UnLock(err,strLockRes);
		UnLockDocument(strCE_EntityLock);
		Ret_pStatus.setError(StatusCodeSet::ERR_BUS_FAILED_TO_LOAD_RECORD); 
		return;
	}

	//security check: did we check out doc?
	//if overwrite flag is not set and some1 checked out document, some other user
	if (!bOverWrite && Ret_CheckOutInfo.getDataRef(0,"BDMD_IS_CHECK_OUT").toInt()>0 && nCheckInPersonID!=Ret_CheckOutInfo.getDataRef(0,"BDMD_CHECK_OUT_USER_ID").toInt())
	{
		Status err;
		if (!strLockRes.isEmpty())
			Locker.UnLock(err,strLockRes);

		Ret_pStatus.setError(StatusCodeSet::ERR_BUS_FAILED_TO_CHECK_IN_DOCUMENT); 
		UnLockDocument(strCE_EntityLock);
		return;
	}

	//////////////////--------------------------------------------
	//begin tran
	TableOrm.GetDbSqlQuery()->BeginTransaction(Ret_pStatus);
	if(!Ret_pStatus.IsOK()) 
	{
		Status err;
		if (!strLockRes.isEmpty())
			Locker.UnLock(err,strLockRes);

		UnLockDocument(strCE_EntityLock);
		return;
	}

	//////////////////--------------------------------------------
	//1. create/find user storage directory...!?->session manager...
	QString strCheckOutPath=Ret_CheckOutInfo.getDataRef(0,"BDMD_DOC_PATH").toString();
	QString strServerFilePath=g_UserSessionManager->InitializeUserStorageDirectory(Ret_pStatus,DataHelper::EncodeSession2Directory(g_UserSessionManager->GetSessionID()));
	if (!strSubDirectory.isEmpty())
		strServerFilePath=strServerFilePath+"/"+strSubDirectory;
	if(!Ret_pStatus.IsOK()) 	
	{
		TableOrm.GetDbSqlQuery()->Rollback();
		Status err;
		if (!strLockRes.isEmpty())
			Locker.UnLock(err,strLockRes);

		UnLockDocument(strCE_EntityLock);
		return;
	}
	QFileInfo info(strCheckOutPath);
	strServerFilePath=strServerFilePath+"/"+info.fileName();

	QFileInfo info_size(strServerFilePath);
	int nFileSize=(int)info_size.size();
	
	//////////////////--------------------------------------------
	//2. insert revision:
	//DbRecordSet RetOut_DocumentRevision;
	Ret_DocumentRevision.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_DM_REVISIONS));
	Ret_DocumentRevision.addRow();
	Ret_DocumentRevision.setData(0,"BDMR_DOCUMENT_ID",nDocumentID);
	Ret_DocumentRevision.setData(0,"BDMR_CHECK_IN_USER_ID",nCheckInPersonID);
	Ret_DocumentRevision.setData(0,"BDMR_DAT_CHECK_IN",QDateTime::currentDateTime());
	Ret_DocumentRevision.setData(0,"BDMR_NAME",info.fileName());
	Ret_DocumentRevision.setData(0,"BDMR_REVISION_TAG",strTag);
	Ret_DocumentRevision.setData(0,"BDMR_IS_COMPRESSED",0); //not compressed into DB...chunked always not compressed..
	Ret_DocumentRevision.setData(0,"BDMR_SIZE",nFileSize);

	//g_Logger.logMessage(StatusCodeSet::TYPE_DEBUG_INFORMATION,StatusCodeSet::ERR_GENERAL,QString("Doc id= %1, file Size: %2").arg(nDocumentID).arg(nFileSize));

	//write revision
	TableOrm_BUS_DM_REVISIONS.Write(Ret_pStatus,Ret_DocumentRevision); 
	if(!Ret_pStatus.IsOK()) 
	{
		TableOrm.GetDbSqlQuery()->Rollback();
		Status err;
		if (!strLockRes.isEmpty())
			Locker.UnLock(err,strLockRes);

		UnLockDocument(strCE_EntityLock);
		return;
	}

	//////////////////--------------------------------------------
	//3. insert from file by chunks:
	int nRevID=Ret_DocumentRevision.getDataRef(0,"BDMR_ID").toInt();
	QString strSQL ="UPDATE BUS_DM_REVISIONS SET BDMR_CONTENT=? WHERE BDMR_ID="+QString::number(nRevID);
	g_PrivateServiceSet->BusinessHelper->File2BlobChunked(Ret_pStatus,strSQL,strServerFilePath);
	if(!Ret_pStatus.IsOK()) 
	{
		TableOrm.GetDbSqlQuery()->Rollback();
		Status err;
		if (!strLockRes.isEmpty())
			Locker.UnLock(err,strLockRes);

		UnLockDocument(strCE_EntityLock);
		return;
	}

	//////////////////--------------------------------------------
	//4. clear check out status:
	QVariant intNull(QVariant::Int);
	Ret_CheckOutInfo.setData(0,"BDMD_IS_CHECK_OUT",0);
	Ret_CheckOutInfo.setData(0,"BDMD_CHECK_OUT_USER_ID",intNull);
	Ret_CheckOutInfo.setData(0,"BDMD_SIZE",nFileSize);

	TableOrm.Write(Ret_pStatus,Ret_CheckOutInfo,DbSqlTableView::TVIEW_BUS_DM_CHECK_OUT_INFO); 
	if(!Ret_pStatus.IsOK()) 
	{
		TableOrm.GetDbSqlQuery()->Rollback();
		Status err;
		if (!strLockRes.isEmpty())
			Locker.UnLock(err,strLockRes);

		UnLockDocument(strCE_EntityLock);
		return;
	}
	TableOrm.GetDbSqlQuery()->Commit(Ret_pStatus);
	Status err;
	if (!strLockRes.isEmpty())
		Locker.UnLock(err,strLockRes);

	UnLockDocument(strCE_EntityLock);

}

//delete's all cent info+asssigned project/contact links and uar/gar/revisions by trigger!!!
void Service_BusDocuments::DeleteDocument(Status &Ret_pStatus, int nDocumentID,QString strLockRes)
{
	//Make local query, can fail if connection reservation fails.
	DbSimpleOrm TableOrm(Ret_pStatus, BUS_DM_DOCUMENTS, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	QString strSQL="SELECT BDMD_COMM_ENTITY_ID FROM BUS_DM_DOCUMENTS WHERE BDMD_ID ="+QString::number(nDocumentID);
	TableOrm.GetDbSqlQuery()->Execute(Ret_pStatus,strSQL);
	if(!Ret_pStatus.IsOK()) return;

	DbRecordSet dataForDelete;
	TableOrm.GetDbSqlQuery()->FetchData(dataForDelete);
	dataForDelete.renameColumn("BDMD_COMM_ENTITY_ID","CENT_ID");
	//_DUMP(dataForDelete);

	DbRecordSet lstStatusRows;
	bool pBoolTransaction = true;
	g_BusinessServiceSet->ClientSimpleORM->Delete(Ret_pStatus,CE_COMM_ENTITY,dataForDelete,strLockRes, lstStatusRows, pBoolTransaction);
}


/*
	Note:
	1. on server create userstorage_attachment_name dir za client file unzip
	2. Method: Attachment2File(int nOpt=0=whole file, 1=all inside unziped)
	3. -> unzip all to special dir
	4. -> check in one by one...
	5. return ok: if returned: refresh/fire signal: new documents */

void Service_BusDocuments::CheckInFromEmailAttachment(Status &Ret_pStatus, int nAttachmentID, int nOperation, int nProjectID, int nContactID, DbRecordSet &RetOut_DocumentIDs)
{
	DbSimpleOrm TableOrm_Attachments(Ret_pStatus, BUS_EMAIL_ATTACHMENT, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	QString strDirectory=DataHelper::EncodeSession2Directory(g_UserSessionManager->GetSessionID());
	QString strServerFilePath=g_UserSessionManager->InitializeUserStorageDirectory(Ret_pStatus,strDirectory);
	if(!Ret_pStatus.IsOK()) 	
		return;

	//get attachment into memory:
	//--------------------------------
	DbRecordSet lstDataAttach;
	QString strWhere ="WHERE BEA_ID="+QVariant(nAttachmentID).toString();
	TableOrm_Attachments.Read(Ret_pStatus, lstDataAttach, strWhere);
	if(!Ret_pStatus.IsOK()) return;

	if (lstDataAttach.getRowCount()!=1)
		return;


	//create temp sub directory to avoid messing with user storage
	//strServerFilePath=strServerFilePath+"/_email_attach_"+QString::number(nAttachmentID);
	QString strAttachmentSubDirAlone="_email_attach_"+QString::number(nAttachmentID);
	QDir attach(strServerFilePath+"/"+strAttachmentSubDirAlone);
	if (attach.exists())
	{
		Ret_pStatus.setError(StatusCodeSet::ERR_BUS_DOCUMENT_FROM_ATTACH,"Directory "+strAttachmentSubDirAlone+" already exists!");
		return ;
	}
	else
	{
		QDir attach_create(strServerFilePath);
		if(!attach_create.mkdir(strAttachmentSubDirAlone))
		{
			Ret_pStatus.setError(StatusCodeSet::ERR_BUS_DOCUMENT_FROM_ATTACH,"Failed to create directory "+strAttachmentSubDirAlone);
			return;
		}
	}

	QString strAttachmentSubDir=strServerFilePath+"/"+strAttachmentSubDirAlone;


	//save onto disk
	//----------------------------------------------------------------
	QString strAttachmentFileName=lstDataAttach.getDataRef(0,"BEA_NAME").toString();
	QString strFilePath=strAttachmentSubDir+"/"+strAttachmentFileName;
	QFile file(strFilePath);
	if(!file.open(QIODevice::WriteOnly))
	{
		Ret_pStatus.setError(StatusCodeSet::ERR_BUS_DOCUMENT_FROM_ATTACH,"Failed to create file "+strAttachmentFileName);
		DataHelper::RemoveAllFromDirectory(strAttachmentSubDir,true,true);
		return;
	}
	QByteArray	byteFileContent=qUncompress(lstDataAttach.getDataRef(0,"BEA_CONTENT").toByteArray()); 
	int nErr=file.write(byteFileContent);
	if (nErr==-1)
	{
		Ret_pStatus.setError(StatusCodeSet::ERR_BUS_DOCUMENT_FROM_ATTACH,"Failed to write to file "+strAttachmentFileName);
		DataHelper::RemoveAllFromDirectory(strAttachmentSubDir,true,true);
		return;
	}
	file.close();

	//unzip if requested: all files are inside /dir/ in lstFiles
	//----------------------------------------------------------------
	QStringList lstFiles;
	lstFiles<<strAttachmentFileName;
	if (nOperation==1)
	{
		//unzip:
		lstFiles.clear();
		if(!ZipCompression::Unzip(strFilePath,strAttachmentSubDir,lstFiles,true))
		{
			Ret_pStatus.setError(StatusCodeSet::ERR_BUS_DOCUMENT_FROM_ATTACH,"Failed to unzip file "+strAttachmentFileName);
			DataHelper::RemoveAllFromDirectory(strAttachmentSubDir,true,true);
			return;
		}
	}


	//get applications:
	Status err;
	DbRecordSet lstApps;
	g_BusinessServiceSet->ClientSimpleORM->Read(err,BUS_DM_APPLICATIONS,lstApps);
	if (!err.IsOK())
	{
		Ret_pStatus.setError(StatusCodeSet::ERR_BUS_DOCUMENT_FROM_ATTACH,"Failed to load applications");
		DataHelper::RemoveAllFromDirectory(strAttachmentSubDir,true,true);
		return;
	}

	//---------------------------------------
	//			START TRANSACTION
	//---------------------------------------
	TableOrm_Attachments.GetDbSqlQuery()->BeginTransaction(Ret_pStatus);
	if(!Ret_pStatus.IsOK()) return;

	RetOut_DocumentIDs.destroy();
	RetOut_DocumentIDs.addColumn(QVariant::Int,"BDMD_ID");

	int nSize=lstFiles.size();
	for (int i=0;i<nSize;i++)
	{

		//take only documents from 1 level: other ignore:
		QFileInfo onlyFileName(lstFiles.at(i));
		int nFileSize;
		QFileInfo info(strAttachmentSubDir+"/"+onlyFileName.fileName());
		if (!info.exists()) //if does not exists loop to another
			continue;
		nFileSize=info.size();

		//create new doc: name/type that's it. owner=logged user

		//start tran:
		DbRecordSet rowDoc=CreateNewDocumentRecord(info.fileName(),GlobalConstants::DOC_TYPE_INTERNET_FILE,lstApps);

		DbRecordSet rowContactLink;
		DbRecordSet rowProjectLink;
		rowContactLink.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION));
		rowProjectLink.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_NMRX_RELATION));

		if (nContactID>0)
		{
			rowContactLink.addRow();
			rowContactLink.setData(0,"BNMR_TABLE_2",BUS_CM_CONTACT);
			rowContactLink.setData(0,"BNMR_TABLE_KEY_ID_2",nContactID);
		}
		if (nProjectID>0)
		{
			rowProjectLink.addRow();
			rowContactLink.setData(0,"BNMR_TABLE_2",BUS_PROJECT);
			rowProjectLink.setData(0,"BNMR_TABLE_KEY_ID_2",nProjectID);
		}

		//write:
		DbRecordSet empty;
		Write(Ret_pStatus,rowDoc,rowProjectLink,rowContactLink,empty,empty,"",empty, empty);
		if (!Ret_pStatus.IsOK())
		{
			DataHelper::RemoveAllFromDirectory(strAttachmentSubDir,true,true);
			TableOrm_Attachments.GetDbSqlQuery()->Rollback();
			return;
		}

		int nDocID=rowDoc.getDataRef(0,"BDMD_ID").toInt();
		RetOut_DocumentIDs.addRow();
		RetOut_DocumentIDs.setData(0,0,nDocID);

		//check in revision
		DbRecordSet Ret_DocumentRevision,Ret_CheckOutInfo;

		CheckInFromUserStorage(Ret_pStatus, nDocID, g_UserSessionManager->GetPersonID(),true,Ret_DocumentRevision,Ret_CheckOutInfo, true, "", strAttachmentSubDirAlone);
		if (!Ret_pStatus.IsOK())
		{
			DataHelper::RemoveAllFromDirectory(strAttachmentSubDir,true,true);
			TableOrm_Attachments.GetDbSqlQuery()->Rollback();
			return;
		}
	}

	DataHelper::RemoveAllFromDirectory(strAttachmentSubDir,true,true);
	TableOrm_Attachments.GetDbSqlQuery()->Commit(Ret_pStatus);
}


DbRecordSet Service_BusDocuments::CreateNewDocumentRecord(QString strFileName, int nDocumentType, DbRecordSet &lstApps)
{
	DbRecordSet lstWrite;
	lstWrite.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_DM_DOCUMENTS_COMM_ENTITY));
	lstWrite.addRow();
	lstWrite.setData(0,"CENT_SYSTEM_TYPE_ID",GlobalConstants::CE_TYPE_DOCUMENT);
	lstWrite.setData(0,"BDMD_DAT_CREATED",QDateTime::currentDateTime());
	lstWrite.setData(0,"CENT_OWNER_ID",g_UserSessionManager->GetPersonID());
	lstWrite.setData(0,"BDMD_NAME",strFileName);
	lstWrite.setData(0,"BDMD_DOC_PATH",strFileName);
	lstWrite.setData(0,"BDMD_DOC_TYPE",nDocumentType);
	lstWrite.setData(0,"BDMD_TEMPLATE_FLAG",0);
	lstWrite.setData(0,"BDMD_CATEGORY","");
	if (lstApps.getRowCount()>0)
		lstWrite.setData(0,"BDMD_APPLICATION_ID",GetApplicationFromExtension(strFileName,lstApps)); //set to app
	return lstWrite;
}

QVariant Service_BusDocuments::GetApplicationFromExtension(QString strFileName, DbRecordSet &lstApps,bool bUseAsExtension)
{
	QVariant valAppID(QVariant::Int);

	QString strExtension=strFileName.toUpper();
	if (!bUseAsExtension)
	{
		QFileInfo fileInfo(strFileName);
		strExtension=fileInfo.suffix().toUpper();	
		if (strExtension.isEmpty())
			return valAppID;
	}

	int nExtIdx=lstApps.getColumnIdx("BDMA_EXTENSIONS");
	int nSizeApp=lstApps.getRowCount();
	for (int k=0;k<nSizeApp;++k)
	{
		if (lstApps.getDataRef(k,nExtIdx).toString().toUpper().indexOf(strExtension)>=0)
		{
			return lstApps.getDataRef(k,"BDMA_ID");
		}
	}

	return valAppID; 
}