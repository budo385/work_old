#ifndef SERVICE_MODULELICENSEMANAGER_H
#define SERVICE_MODULELICENSEMANAGER_H

#include <QtCore>
#include "common/common/status.h"
#include "db_core/db_core/dbcore_include.h"
#include "businessservice.h"

/*!
    \class Service_ModuleLicenseManager
    \brief Private service that loads keyfile
    \ingroup Bus_Private


	WARNING:
	This class uses global ModuleLicense object g_ModuleLicense (with FP defintions)
	Only GetFPList is multithread safe, other are only used by APP. SERVER
		
*/

class Service_ModuleLicenseManager : public BusinessService
{
public:
	virtual ~Service_ModuleLicenseManager(){};

	/*
	void LoadKeyFile(Status& Ret_pStatus, QString &strKeyFile);
	
	int GetUniqueLicenseID(Status& Ret_pStatus);
	int GetMaxLicenseCount(Status& Ret_pStatus, QString& strModuleCode, QString& strMLIID);

	bool IsFPAvailable(Status& Ret_pStatus, int FPcode, QString& strModuleCode,QString& strMLIID, int &Ret_nValue);
	*/
	void GetFPList(Status& Ret_pStatus, QString& strModuleCode, QString& strMLIID, DbRecordSet& RetOut_pList){};

};

#endif //SERVICE_MODULELICENSEMANAGER_H
