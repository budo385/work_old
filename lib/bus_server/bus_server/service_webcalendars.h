#ifndef SERVICE_WEBCALENDARS_H
#define SERVICE_WEBCALENDARS_H


#include "bus_interface/bus_interface/interface_webcalendars.h"
#include "businessservice.h"
#include "serverlocalcache.h"

class Service_WebCalendars : public Interface_WebCalendars, public BusinessService
{
public:
	void ReadCalendars(Status &Ret_pStatus, int &Ret_nTotalCount, DbRecordSet &Ret_Calendars, QDate datFrom, QDate datTo,QString &Ret_strFrom, QString &Ret_strTo); 

private:
	int GetStatus(QDateTime datFrom, int nPresenceID,ServerLocalCache &Types, int nStateID);
	
};

#endif // SERVICE_WEBCALENDARS_H
