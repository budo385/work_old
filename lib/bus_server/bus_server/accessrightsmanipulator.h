#ifndef ACCESSRIGHTSMANIPULATOR_H
#define ACCESSRIGHTSMANIPULATOR_H

#include "db/db/dbsqlmanager.h"
#include "common/common/status.h"


class AccessRightsManipulator
{
public:
	AccessRightsManipulator(DbSqlManager *pManager);
	~AccessRightsManipulator();

	//Person.
	void InsertNewRoleToPerson(Status &Ret_pStatus, int PersonID, int RoleID);
	void DeleteRoleFromPerson(Status &Ret_pStatus, int PersonID, int RoleID);
	//User.
	void InsertNewRoleToUser(Status &Ret_pStatus, int UserID, int RoleID);
	void DeleteRoleFromUser(Status &Ret_pStatus, int UserID, int RoleID);
	//Roles.
	void DeleteRole(Status &Ret_pStatus, int RoleID);
	void DeleteAccRSetFromRole(Status &Ret_pStatus, int RoleID, int AccRSetID);
	void InsertNewAccRSetToRole(Status &Ret_pStatus, int RoleID, int AccRSetID);
	void InsertNewRole(Status &Ret_pStatus, int &Ret_nNewRoleRowID, QString RoleName, QString RoleDescription, int RoleType);
	void RenameRole(Status &Ret_pStatus, int RoleID, QString NewRoleName, QString NewRoleDescription);
	void InsertAccessRights(Status &pStatus, int RoleID, int ARSID);
	//Only for reorganizing.
	void InsertNewAccRSetToDatabase(Status &pStatus, int nARSID);
	void InsertNewAccessRightToAccRSet(Status &pStatus, int nARSID, int nAccessRightID);
	void InsertNewAccRSetToDefaultRole(Status &Ret_pStatus, int nRoleCode, int nAccRSetCode);

private:
	DbSqlManager	*m_pManager;			//db manager (extern)
};

#endif // ACCESSRIGHTSMANIPULATOR_H
