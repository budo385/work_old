#include "businesslocker.h"
#include "businessserviceset.h"
#include <QDomDocument>

#include "bus_core/bus_core/useraccessright.h"
extern UserAccessRight *g_AccessRight;				//global access right tester
#include "common/common/logger.h"
extern Logger				g_Logger;

Service_StoredProjectLists::Service_StoredProjectLists()
{
}

void Service_StoredProjectLists::GetListNames(Status &Ret_pStatus, DbRecordSet &RetOut_Data)
{
	QString strQuery = "SELECT BSPL_ID, BSPL_NAME FROM BUS_STORED_PROJECT_LIST ORDER BY BSPL_NAME";

	//filter by UAR
	//B.T.: AR can accept whole SQL, not just where statement
	//QString strWhere;
	g_AccessRight->SQLFilterRecords(BUS_STORED_PROJECT_LIST, strQuery);
	//strQuery += " " + strWhere;

	DbSqlQuery query(Ret_pStatus,GetDbManager());
	if(!Ret_pStatus.IsOK()) return;
	query.Prepare(Ret_pStatus, strQuery);
	if(!Ret_pStatus.IsOK())	return;
	query.ExecutePrepared(Ret_pStatus);
	if(!Ret_pStatus.IsOK())	return;

	query.FetchData(RetOut_Data);
}

void Service_StoredProjectLists::CreateList(Status &Ret_pStatus, QString strListName, DbRecordSet &lstData, int &RetOut_nListID, bool bIsSelection, QString strSelectionXML, bool bIsFilter, QString strFilterXML)
{
	//create an record in the list titles table
	DbRecordSet lstListName;
	lstListName.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_STORED_PROJECT_LIST));
	lstListName.addRow();
	lstListName.setData(0, "BSPL_NAME", strListName);
	
	//TOFIX set owner?
	DbSimpleOrm orm(Ret_pStatus, BUS_STORED_PROJECT_LIST, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	orm.Write(Ret_pStatus, lstListName, DbSqlTableView::TVIEW_BUS_STORED_PROJECT_LIST);

	//write list item records
	int nListID = lstListName.getDataRef(0, "BSPL_ID").toInt();
	SetListData(Ret_pStatus, nListID, lstData, bIsSelection, strSelectionXML, bIsFilter, strFilterXML); 
	
	//return ID
	RetOut_nListID = nListID;
}

void Service_StoredProjectLists::DeleteList(Status &Ret_pStatus, int nListID)
{
	DbSimpleOrm orm(Ret_pStatus, BUS_STORED_PROJECT_LIST, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	BusinessLocker locker(Ret_pStatus,BUS_STORED_PROJECT_LIST, GetDbManager(),orm.GetDbConnection());
	if(!Ret_pStatus.IsOK()) return;

	//lock the record
	DbRecordSet lstList;
	lstList.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_STORED_PROJECT_LIST));
	lstList.addRow();
	lstList.setData(0, "BSPL_ID", nListID);
	QString strLockedRes;
	locker.Lock(Ret_pStatus, lstList, strLockedRes);
	if(!Ret_pStatus.IsOK()) return;

	//delete it
	QString strQuery = QString("DELETE FROM BUS_STORED_PROJECT_LIST WHERE BSPL_ID=%1").arg(nListID);
	orm.GetDbSqlQuery()->Execute(Ret_pStatus, strQuery);

	//unlock
	locker.UnLock(Ret_pStatus, strLockedRes);
}

void Service_StoredProjectLists::GetListData(Status &Ret_pStatus, int nListID, DbRecordSet &RetOut_lstData, bool &Ret_bIsSelection, QString &Ret_strSelectionXML, bool &Ret_bIsFilter, QString &Ret_strFilterXML, bool bGetDescOnly)
{
	//read header fields
	//1. first read existing header values (like ID)
	DbRecordSet lstListName;
	lstListName.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_STORED_PROJECT_LIST));
	DbSimpleOrm orm1(Ret_pStatus, BUS_STORED_PROJECT_LIST, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	orm1.Read(Ret_pStatus, lstListName, QString("WHERE BSPL_ID=%1").arg(nListID));
	if(lstListName.getRowCount()<1) return;
	Ret_bIsSelection = lstListName.getDataRef(0, "BSPL_IS_SELECTION").toBool();
	lstListName.getData(0, "BSPL_SELECTION_DATA", Ret_strSelectionXML);
	Ret_bIsFilter = lstListName.getDataRef(0, "BSPL_IS_FILTER").toBool();
	lstListName.getData(0, "BSPL_FILTER_DATA", Ret_strFilterXML);
	
	//skip filling the tree items data if not needed
	if(bGetDescOnly && (Ret_bIsSelection || Ret_bIsFilter))
		return;

	QTime timer;
	timer.start();
	
	QString strQuery  = "SELECT DISTINCT "+DbSqlTableView::getSQLColumnsFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_PROJECT_SELECT));
	if(!Ret_bIsSelection && !Ret_bIsFilter){
		//the extra field only has sense for old "group" type of SPL
		strQuery  += ", BSPLD_EXPANDED FROM BUS_PROJECTS INNER JOIN BUS_STORED_PROJECT_LIST_DATA ON BSPLD_PROJECT_ID=BUSP_ID";
	}
	else {
		strQuery  += " FROM BUS_PROJECTS ";
	}

	QString strJoin, strWhere;
	if(Ret_bIsSelection){
		SelectionXml2Sql(Ret_pStatus, Ret_strSelectionXML, strJoin, strWhere);
		if(!Ret_pStatus.IsOK()) return;
		if(!strWhere.isEmpty()) strWhere.insert(0, " WHERE ");
	}
	else if(Ret_bIsFilter){
		//we can not map collection to SQL, but fetch a records immediately here!
		ProcessCollectionXml(Ret_pStatus, Ret_strFilterXML, RetOut_lstData);
		if(!Ret_pStatus.IsOK()) return;
	}
	else{
		strWhere = QString(" WHERE BSPLD_TREE_ID = %1 ORDER BY BUSP_CODE").arg(nListID);
	}
	strQuery += " " + strJoin + " " + strWhere;

	if(!Ret_bIsFilter)
	{
		//filter by UAR
		g_AccessRight->SQLFilterRecords(BUS_PROJECT, strQuery);
		DbSqlQuery query(Ret_pStatus,GetDbManager());
		if(!Ret_pStatus.IsOK()) return;
		query.Prepare(Ret_pStatus, strQuery);
		if(!Ret_pStatus.IsOK())	return;
		query.ExecutePrepared(Ret_pStatus);
		if(!Ret_pStatus.IsOK())	return;
		query.FetchData(RetOut_lstData);

		qDebug()<<timer.elapsed();
		timer.restart();

		//only for old "groups" type
		DbRecordSet lstSubData;
		lstSubData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_PROJECT_SELECT));

		if(!Ret_bIsSelection && !Ret_bIsFilter)
		{
			HierarchicalData ProjectData;
			ProjectData.Initialize(BUS_PROJECT,GetDbManager());

			//dynamically recursively expand any unexpanded item
			DbRecordSet lstSubBranch;
			lstSubBranch.copyDefinition(lstSubData);

			//get all children 1st level
			//BT: fixed bug when fetchin unexpanded node children -> now: get all children from all nodes from stored project list!!!
			int nCount = RetOut_lstData.getRowCount();
			for(int i=0; i<nCount; i++)
			{
				int nProjectID=RetOut_lstData.getDataRef(i, "BUSP_ID").toInt();
				if (lstSubData.find("BUSP_ID",nProjectID,true)<0) //if not already loaded, load children, if already in list then skip...
				{
					QString strCode = RetOut_lstData.getDataRef(i, "BUSP_CODE").toString();
					ProjectData.GetDescendantsNodes(strCode, lstSubBranch, "");
					//lstSubBranch.Dump();
					lstSubData.merge(lstSubBranch);
				}
		/*
				if(RetOut_lstData.getDataRef(i, "BSPLD_EXPANDED").toInt() < 1){
					QString strCode = RetOut_lstData.getDataRef(i, "BUSP_CODE").toString();
					ProjectData.GetDescendantsNodes(strCode, lstSubBranch, "");
					lstSubData.merge(lstSubBranch);
				}
		*/
			}

			qDebug()<<timer.elapsed();
			int ncnt = lstSubData.getRowCount();
			timer.restart();

			//add descendants
			RetOut_lstData.merge(lstSubData);
			//lstSubData.Dump();
		}
	}

	//RetOut_lstData.Dump();
	
	int nIDColIdx = RetOut_lstData.getColumnIdx("BUSP_ID");
	RetOut_lstData.removeDuplicates(nIDColIdx);
	RetOut_lstData.sort("BUSP_CODE");
	if (RetOut_lstData.getColumnIdx("BUSP_EXPANDED")>=0)
		RetOut_lstData.removeColumn(RetOut_lstData.getColumnIdx("BUSP_EXPANDED"));
	if (RetOut_lstData.getColumnIdx("BSPLD_EXPANDED")<0)	//not present for filters and selections
	{
		RetOut_lstData.addColumn(QVariant::Int, "BSPLD_EXPANDED");
		RetOut_lstData.setColValue("BSPLD_EXPANDED", 1);	//filters and selections should be always expanded
	}
	RetOut_lstData.renameColumn("BSPLD_EXPANDED", "BUSP_EXPANDED");

	qDebug()<<timer.elapsed();
}

/* 
	if strParentProjectCode<=0 then return first level of stored list + expanded nodes  one level deep by default or if nLevelsDeep <=0 then all 
	if strParentProjectCode>0 then ignore nListID and return expanded children one level deep by default or if nLevelsDeep <=0 then all 
*/
void Service_StoredProjectLists::GetListDataLevelByLevel(Status &Ret_pStatus, int nListID, DbRecordSet &Ret_lstData, QString strParentProjectCode,  int nLevelsDeep)
{
	//get all children nLevelsDeep level or just by one (if flag expanded then +1 level deep)
	if (nLevelsDeep<=0)
		nLevelsDeep=1;

	if (nListID>0)
	{
		QString strQuery  = "SELECT DISTINCT "+DbSqlTableView::getSQLColumnsFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_PROJECT_SELECT))+", BSPLD_EXPANDED FROM BUS_PROJECTS INNER JOIN BUS_STORED_PROJECT_LIST_DATA ON BSPLD_PROJECT_ID=BUSP_ID";
		QString strWhere;
		strWhere = QString(" WHERE BSPLD_TREE_ID = %1 ORDER BY BUSP_CODE").arg(nListID);
		strQuery += " " + strWhere;

		//filter by UAR
		g_AccessRight->SQLFilterRecords(BUS_PROJECT, strQuery);
		DbSqlQuery query(Ret_pStatus,GetDbManager());
		if(!Ret_pStatus.IsOK()) return;
		query.Prepare(Ret_pStatus, strQuery);
		if(!Ret_pStatus.IsOK())	return;
		query.ExecutePrepared(Ret_pStatus);
		if(!Ret_pStatus.IsOK())	return;
		query.FetchData(Ret_lstData);

		HierarchicalData ProjectData;
		ProjectData.Initialize(BUS_PROJECT,GetDbManager());

		//dynamically recursively expand any unexpanded item
		DbRecordSet lstSubData;
		lstSubData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_PROJECT_SELECT));
		DbRecordSet lstSubBranch;
		lstSubBranch.copyDefinition(lstSubData);

		//because of features of the dTree control we shall eridacate parent_id from root nodes of stored lists:
		int nRootCodeLen=0;
		int nCount = Ret_lstData.getRowCount();
		if (nCount>0)
			nRootCodeLen=Ret_lstData.getDataRef(0,"BUSP_CODE").toString().length();

		for(int i=0; i<nCount; i++)
		{
			int nProjectID=Ret_lstData.getDataRef(i, "BUSP_ID").toInt();
			QString strCode = Ret_lstData.getDataRef(i, "BUSP_CODE").toString();

			if (lstSubData.find("BUSP_ID",nProjectID,true)<0) //if not already loaded, load children, if already in list then skip...
			{
				//qDebug()<<strCode<<"exp: "<<Ret_lstData.getDataRef(i, "BSPLD_EXPANDED").toInt();

				if(Ret_lstData.getDataRef(i, "BSPLD_EXPANDED").toInt() == 1 && nLevelsDeep == 1)
					ProjectData.GetDescendantsNodes(strCode, lstSubBranch, "",true,2);
				else
					ProjectData.GetDescendantsNodes(strCode, lstSubBranch, "",true,nLevelsDeep);
				lstSubData.merge(lstSubBranch);
			}

			//reset root parent id to 0 coz of dTree web control
			if (nRootCodeLen==strCode.length())
			{
				Ret_lstData.setData(i,"BUSP_PARENT",0);
			}
		}

		//RetOut_lstData.Dump();
		//lstSubData.Dump();

		//add descendants
		int nIDColIdx = Ret_lstData.getColumnIdx("BUSP_ID");
		Ret_lstData.merge(lstSubData);
		Ret_lstData.removeDuplicates(nIDColIdx);
		Ret_lstData.sort("BUSP_CODE");
		if (Ret_lstData.getColumnIdx("BUSP_EXPANDED")>=0)
			Ret_lstData.removeColumn(Ret_lstData.getColumnIdx("BUSP_EXPANDED"));
		Ret_lstData.renameColumn("BSPLD_EXPANDED", "BUSP_EXPANDED");

	}
	else /* use strParentProjectCode */
	{
		Ret_lstData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_PROJECT_SELECT));
		HierarchicalData ProjectData;
		ProjectData.Initialize(BUS_PROJECT,GetDbManager());

		//get all-> issue for WebConnect when ListID<0 and projectcode="" then return all projects from root by leveldeep
		if (strParentProjectCode.isEmpty())
		{
			QString strQuery  = "SELECT "+DbSqlTableView::getSQLColumnsFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_PROJECT_SELECT))+" FROM BUS_PROJECTS";
			QString strWhere;
			strWhere = " WHERE BUSP_PARENT IS NULL ORDER BY BUSP_CODE"; //strWhere = " WHERE BUSP_LEVEL=MIN(BUSP_LEVEL) ORDER BY BUSP_CODE";
			strQuery += " " + strWhere;

			//filter by UAR
			g_AccessRight->SQLFilterRecords(BUS_PROJECT, strQuery);
			DbSqlQuery query(Ret_pStatus,GetDbManager());
			if(!Ret_pStatus.IsOK()) return;

			query.Execute(Ret_pStatus,strQuery);
			if(!Ret_pStatus.IsOK())	return;

			query.FetchData(Ret_lstData);

			HierarchicalData ProjectData;
			ProjectData.Initialize(BUS_PROJECT,GetDbManager());

			//dynamically recursively expand any unexpanded item
			DbRecordSet lstSubData;
			lstSubData.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_PROJECT_SELECT));
			DbRecordSet lstSubBranch;
			lstSubBranch.copyDefinition(lstSubData);

			int nCount = Ret_lstData.getRowCount();
			for(int i=0; i<nCount; i++)
			{
				int nProjectID=Ret_lstData.getDataRef(i, "BUSP_ID").toInt();
				if (lstSubData.find("BUSP_ID",nProjectID,true)<0) //if not already loaded, load children, if already in list then skip...
				{
					QString strCode = Ret_lstData.getDataRef(i, "BUSP_CODE").toString();
					if (strCode.isEmpty()) //B.T. noticed in HHM db: BUSP_CODE="", import problem!?
					{
						//qDebug()<<"Code EMPTY!!!";
						//Ret_lstData.getRow(i).Dump();
						continue;
					}
					ProjectData.GetDescendantsNodes(strCode, lstSubBranch, "",true,nLevelsDeep);
					lstSubData.merge(lstSubBranch);
				}
			}

			//add descendants
			int nIDColIdx = Ret_lstData.getColumnIdx("BUSP_ID");
			Ret_lstData.merge(lstSubData);
			Ret_lstData.removeDuplicates(nIDColIdx);
			Ret_lstData.sort("BUSP_CODE");
			if (Ret_lstData.getColumnIdx("BUSP_EXPANDED")<0)
				Ret_lstData.addColumn(QVariant::Int,"BUSP_EXPANDED");
			Ret_lstData.setColValue("BUSP_EXPANDED",0);
		}
		else
		{
			//get descendants
			ProjectData.GetDescendantsNodes(strParentProjectCode, Ret_lstData, "",true,nLevelsDeep);
			//Ret_lstData.Dump();

			//add descendants
			int nIDColIdx = Ret_lstData.getColumnIdx("BUSP_ID");
			Ret_lstData.removeDuplicates(nIDColIdx);
			Ret_lstData.sort("BUSP_CODE");
			if (Ret_lstData.getColumnIdx("BUSP_EXPANDED")<0)
				Ret_lstData.addColumn(QVariant::Int,"BUSP_EXPANDED");
			Ret_lstData.setColValue("BUSP_EXPANDED",0);
		}
	}
}

void Service_StoredProjectLists::SetListData(Status &Ret_pStatus, int nListID, DbRecordSet &lstData, bool bIsSelection, QString strSelectionXML, bool bIsFilter, QString strFilterXML)
{
	//update header fields:
	//1. first read existing header values (like ID)
	DbRecordSet lstListName;
	lstListName.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_STORED_PROJECT_LIST));
	DbSimpleOrm orm1(Ret_pStatus, BUS_STORED_PROJECT_LIST, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;
	orm1.Read(Ret_pStatus, lstListName, QString(" WHERE BSPL_ID=%1").arg(nListID));
	if(!Ret_pStatus.IsOK()) return;
	if(lstListName.getRowCount()<1) return;
	//2. now overwrite some of them with new values
	lstListName.setData(0, "BSPL_IS_SELECTION", bIsSelection ? 1 : 0);
	lstListName.setData(0, "BSPL_SELECTION_DATA", strSelectionXML);
	lstListName.setData(0, "BSPL_IS_FILTER", bIsFilter ? 1 : 0);
	lstListName.setData(0, "BSPL_FILTER_DATA", strFilterXML);
	orm1.Write(Ret_pStatus, lstListName, DbSqlTableView::TVIEW_BUS_STORED_PROJECT_LIST);
	if(!Ret_pStatus.IsOK()) return;

	//prepare data to be written (from BUS_PROJECT to BUS_STORED_PROJECT_LIST_DATA)
	DbRecordSet lstDataItems1;
	lstDataItems1.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_STORED_PROJECT_LIST_DATA));
	if(!bIsSelection && !bIsFilter){
		lstData.renameColumn("BUSP_ID", "BSPLD_PROJECT_ID"); // so we can merge
		lstData.renameColumn("BUSP_EXPANDED", "BSPLD_EXPANDED");
		lstDataItems1.merge(lstData);
	}
	lstDataItems1.setColValue(lstDataItems1.getColumnIdx("BSPLD_TREE_ID"), nListID);

	//write ...
	DbSimpleOrm orm(Ret_pStatus, BUS_STORED_PROJECT_LIST_DATA, GetDbManager()); 
	if(!Ret_pStatus.IsOK()) return;

	DbSqlQuery *query=orm.GetDbSqlQuery();
	query->BeginTransaction(Ret_pStatus);
	if(!Ret_pStatus.IsOK()) return;

	//delete previous tree data rows
	QString strQuery = QString("DELETE FROM BUS_STORED_PROJECT_LIST_DATA WHERE BSPLD_TREE_ID=%1").arg(nListID);
	orm.GetDbSqlQuery()->Execute(Ret_pStatus, strQuery);
	if(!Ret_pStatus.IsOK()) 
	{
		query->Rollback();
		return;
	}
		
	//write new data from this tree
	//lstDataItems1.Dump();
	orm.Write(Ret_pStatus, lstDataItems1, DbSqlTableView::TVIEW_BUS_STORED_PROJECT_LIST_DATA);
	if(!Ret_pStatus.IsOK()) 
	{
		query->Rollback();
		return;
	}
	query->Commit(Ret_pStatus);
}

QString DecodeOperatorValue(QString strValue)
{
	if(strValue == "eq") 
		return "=";
	else if(strValue == "lt") 
		return "<";			
	else if(strValue == "le") 
		return "<=";			
	else if(strValue == "gt") 
		return ">";			
	else if(strValue == "ge") 
		return ">=";			
	else if(strValue == "ne") 
		return "<>";		
	else 
		return strValue;
}

int Service_StoredProjectLists::FindListByName(QString strName)
{
	QString strQuery = "SELECT BSPL_ID FROM BUS_STORED_PROJECT_LIST WHERE BSPL_NAME=?";

	Status status;
	DbSqlQuery query(status, GetDbManager());
	if(!status.IsOK()) return -1;
	query.Prepare(status, strQuery);
	if(!status.IsOK())	return -1;
	query.bindValue(0, strName);
	query.ExecutePrepared(status);
	if(!status.IsOK())	return -1;

	DbRecordSet lstData;
	query.FetchData(lstData);
	if(lstData.getRowCount()>0){
		return lstData.getDataRef(0, 0).toInt();
	}
	return -1;
}

/*
<xml>
	<selection>
		<params>
			<param>
				<pnum>1</pnum>
				<pname>Date1</pname>
				<ptype>date</ptype>
			</param>
			<param>
				<pnum>2</pnum>
				<pname>Date2</pname>
				<ptype>date</ptype>
			</param>
		</params>
		<terms>
			<term>
				<log>AND</log>
				<par>PR_STARTDAT</par>
				<op>ge</op>									//">="	
				<const>Date1</const>
			</term>
			<term>
				<log>AND</log>
				<par>PR_STARTDAT</par>
				<op>le</op>									//"<="
				<const>Date2</const>
			</term>
			<term>
				<log>AND</log>
				<par>PR_STUFE</par>
				<op>ge</op>									//">="
				<const>2</const>
			</term>
			<term>
				<log>AND</log>
				<par>PR_STUFE</par>
				<op>le</op>									//"<="
				<const>2</const>
			</term>
			<term>
				<log>AND</log>
				<par>PR_CODE</par>
				<op>BEGINS WITH</op>
				<const>P.E.</const>
			</term>
		</terms>
	</selection>
</xml>
*/
void Service_StoredProjectLists::SelectionXml2Sql(Status &Ret_pStatus, QString strXML, QString &strJoin, QString &strWhere)
{
	Q_ASSERT(!strXML.isEmpty());

	static QMap<QString, QString> mapField2Join;
	static QMap<QString, QString> mapField2Where;
	if(mapField2Join.empty())
	{
		mapField2Join.insert("BUSP_CODE", "");	//no join (same table BUS_PROJECTS)
		mapField2Join.insert("BDEPT_CODE", "INNER JOIN BUS_DEPARTMENTS ON BUSP_DEPARTMENT_ID=BDEPT_ID");
		mapField2Join.insert("BORG_CODE", "INNER JOIN BUS_ORGANIZATIONS ON BUSP_ORGANIZATION_ID=BORG_ID");
		mapField2Join.insert("SEPB_CODE", "INNER JOIN SPC_PR_AREA ON BUSP_AREA_ID=SEPB_ID");
		mapField2Join.insert("SEFG_CODE", "INNER JOIN SPC_SECTOR ON BUSP_SECTOR_ID=SEFG_ID");
		mapField2Join.insert("SEWG_CODE", "INNER JOIN SPC_INDUSTRY_GROUP ON BUSP_INDUSTRY_ID=SEWG_ID");
		mapField2Join.insert("SEAP_CODE", "INNER JOIN SPC_PR_TYPE ON BUSP_TYPE_ID=SEAP_ID");
		mapField2Join.insert("SELA_CODE", "INNER JOIN SPC_SERVICE_TYPE ON BUSP_SERVICE_TYPE_ID=SELA_ID");
		mapField2Join.insert("BPER_CODE_PROJ_LEADER", "INNER JOIN BUS_PERSON AS PER1 ON BUSP_LEADER_ID=BPER_ID");
		mapField2Join.insert("BPER_CODE_RESPONSIBLE", "INNER JOIN BUS_PERSON AS PER2 ON BUSP_RESPONSIBLE_ID=BPER_ID");
		mapField2Join.insert("SFPS_CODE", "INNER JOIN SPC_PR_STATUS ON BUSP_STATUS_ID=SFPS_ID");
		mapField2Join.insert("SETK_CODE", "INNER JOIN SPC_RATE_CAT ON BUSP_RATE_CAT_ID=SETK_ID");

		mapField2Where.insert("BUSP_CODE", "BUSP_CODE");			//Project Code
		mapField2Where.insert("BDEPT_CODE", "BDEPT_CODE");			//Department Code
		mapField2Where.insert("BORG_CODE", "BORG_CODE");			//Organization Code
		mapField2Where.insert("SEPB_CODE", "SEPB_CODE");			//Area Code
		mapField2Where.insert("SEFG_CODE", "SEFG_CODE");			//Sector Code
		mapField2Where.insert("SEWG_CODE", "SEWG_CODE");			//Industry Group Code
		mapField2Where.insert("SEAP_CODE", "SEAP_CODE");			//Project Type
		mapField2Where.insert("SELA_CODE", "SELA_CODE");			//Service Type
		mapField2Where.insert("BPER_CODE_PROJ_LEADER", "PER1.BPER_CODE");	//Project Leader (BUSP_LEADER_ID) - this is fictious field name
		mapField2Where.insert("BPER_CODE_RESPONSIBLE", "PER2.BPER_CODE");	//Responsible (BUSP_RESPONSIBLE_ID) - this is fictious field name
		mapField2Where.insert("SFPS_CODE", "SFPS_CODE");			//Status
		mapField2Where.insert("SETK_CODE", "SETK_CODE");			//Rate Category
	}

	//initialize
	strWhere = "";
	strJoin = "";

	QDomDocument doc;
	if (!doc.setContent(strXML)) {
		Ret_pStatus.setError(1, "Error parsing selection XML definition");
		return;
	}

	QDomNodeList paramList = doc.elementsByTagName("param");
	int nParamCnt = paramList.size();
	//TOFIX parse, store and use parameters here?

	int  nBracketCnt = 0;	//each bracket must be terminated later
	bool bEmptyBracketOpened = false;	//only for "(" operator
	bool bBracketOpened = false;		//for all operators like "(", "NOT (", "AND ("

	QDomNodeList termList = doc.elementsByTagName("term");
	int nTermCnt = termList.size();
	for(int i=0; i<nTermCnt; i++)
	{
		bool bStartOfExpression = (0 == i || bEmptyBracketOpened);
		QString strLogicalOp = termList.at(i).firstChildElement("log").text();
		if("NOT AND(" == strLogicalOp)
			strLogicalOp = "AND NOT (";			//convert to valid SQL
		if(!bStartOfExpression && "(" == strLogicalOp)
			strLogicalOp = "AND (";				//make sure to prepend AND to prevent invalid SQL like "AND Project Code = K. (Department Code = H.)"
		if(!bStartOfExpression && "NOT (" == strLogicalOp)
			strLogicalOp = "AND NOT (";			//make sure to prepend AND to prevent invalid SQL like "AND Project Code = K. NOT (Department Code = H.)"

		//process a single term
		strWhere += " ";

		//do not add logical operators at the start of the expression or at the start of subexpression inside "("
		bool bSkipLogOperator = (strLogicalOp == "AND" || strLogicalOp == "OR") && bStartOfExpression;
		if(!bSkipLogOperator) {
			strWhere += strLogicalOp;
		}

		//STEP 1. each field is mapped to a possible JOIN clause
		QString strField = termList.at(i).firstChildElement("par").text();
		if(!strField.isEmpty())
		{
			QString strJoinPart = mapField2Join[strField];
			if(!strJoinPart.isEmpty()){
				if(!strJoin.isEmpty())
					strJoin += " ";
				strJoin += strJoinPart;
			}
		}

		//check if bracket opened
		bEmptyBracketOpened = false;
		bBracketOpened = false;
		if(strLogicalOp.indexOf('(') >= 0){
			nBracketCnt ++;
			bEmptyBracketOpened = strField.isEmpty(); 
			bBracketOpened = true;
		}
		else if(strLogicalOp.indexOf(')') >= 0){
			nBracketCnt --;
		}

		//STEP 2. each field adds to a WHERE clause
		QString strRealField = mapField2Where[strField];
		QString strOperator = DecodeOperatorValue(termList.at(i).firstChildElement("op").text());
		QString strConstant = termList.at(i).firstChildElement("const").text();

		if(!strRealField.isEmpty())	//it is possible to have logical operators without operator/constant part (ie. only "(" with no attached "Project Code = K.")
		{
			strWhere += " ";
			strWhere += strRealField;
			strWhere += " ";
			
			//TOFIX test if the const is a parameter name
			if(strOperator == "BEGINS WITH")
			{
				//"BEGINS WITH" operator (only for strings) maps to SQL: P BETWEEN 'C' AND 'C~'
				strWhere += "BETWEEN ";
				strWhere += " '";
				strWhere += strConstant;
				strWhere += "' AND '";
				strWhere += strConstant;
				strWhere += "~'";
			}
			else{
				strWhere += strOperator;
				strWhere += " '";
				strWhere += strConstant;
				strWhere += "'";
			}
		}
	}

	//force closing any remaining bracket
	for(int i=0; i<nBracketCnt; i++){
		strWhere += ")";
	}
}	

/*
<xml>
	<collection>
		<params>
			<param>
				<pnum>1</pnum>
				<pname>MyDate1</pname>
				<ptype>date</ptype>
			</param>
			<param>
				<pnum>2</pnum>
				<pname>MyDate2</pname>
				<ptype>date</ptype>
			</param>
		</params>
		<subelemement>
			<op>JOIN</op>
			<secode>"Externe Projekte"</secode>
			<pass_param>MyDate1</pass_param>
			<pass_param>MyDate2</pass_param>
		</subelement>
		<subelemement>
			<op>JOIN</op>
			<secode>"Projekte I.1."</secode>
			<pass_param>MyDate1</pass_param>
		</subelement>
		<subelemement>
			<op>JOIN</op>
			<secode>"Projekte P.E."</secode>
		</subelement>
	</collection>
</xml>
*/
void Service_StoredProjectLists::ProcessCollectionXml(Status &Ret_pStatus, QString strXML, DbRecordSet &Ret_lstData)
{
	Q_ASSERT(!strXML.isEmpty());
	
	QDomDocument doc;
	if (!doc.setContent(strXML)) {
		Ret_pStatus.setError(1, "Error parsing collection XML definition");
		return;
	}

	QDomNodeList paramList = doc.elementsByTagName("param");

	QDomNodeList termList = doc.elementsByTagName("subelement");
	int nTermCnt = termList.size();
	for(int i=0; i<nTermCnt; i++)
	{
		QString strOp = termList.at(i).firstChildElement("op").text();	//JOIN, REMOVE or INTERSECT
		QString strSelection = termList.at(i).firstChildElement("secode").text();

		QDomNode param = termList.at(i).firstChildElement("pass_param");
		while(!param.isNull()){
			//TOFIX store and use all parameters
			param = param.nextSibling();
		}

		//STEP 1: get XML from database for given selection name
		int nListID = FindListByName(strSelection);
		if(nListID >= 0){
			DbRecordSet lstData;
			bool bIsSelection;
			QString strSelectionXML;
			bool bIsFilter;
			QString strFilterXML;
			bool bGetDescOnly = true;
			GetListData(Ret_pStatus, nListID, lstData, bIsSelection, strSelectionXML, bIsFilter, strFilterXML, bGetDescOnly);
			if(!Ret_pStatus.IsOK()){
				return;
			}
			if(bIsSelection){
				//STEP 2: call SelectionXml2Sql to convert XML to SQL
				QString strJoin, strWhere;
				SelectionXml2Sql(Ret_pStatus, strSelectionXML, strJoin, strWhere);
				if(!Ret_pStatus.IsOK()) return;
				if(!strWhere.isEmpty()) strWhere.insert(0, " WHERE ");
				
				QString strQuery  = "SELECT DISTINCT "+DbSqlTableView::getSQLColumnsFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_BUS_PROJECT_SELECT));
				strQuery += " FROM BUS_PROJECTS ";
				strQuery += strJoin + " " + strWhere;

				//filter by UAR
				DbRecordSet lstData;
				g_AccessRight->SQLFilterRecords(BUS_PROJECT, strQuery);
				DbSqlQuery query(Ret_pStatus,GetDbManager());
				if(!Ret_pStatus.IsOK()) return;
				query.Prepare(Ret_pStatus, strQuery);
				if(!Ret_pStatus.IsOK()) return;
				query.ExecutePrepared(Ret_pStatus);
				if(!Ret_pStatus.IsOK()) return;
				query.FetchData(lstData);

				int nIDIdx = lstData.getColumnIdx("BUSP_ID");

				//perform JOIN, REMOVE or INTERSECT operation
				if("JOIN" == strOp)
				{
					Ret_lstData.joinList(lstData, nIDIdx);
				}
				else if("REMOVE" == strOp)
				{
					Ret_lstData.removeList(lstData, nIDIdx);
				}
				else if("INTERSECT" == strOp)
				{
					Ret_lstData.intersectList(lstData, nIDIdx);
				}
			}
		}
	}
}

