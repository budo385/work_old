#ifndef PRIVATESERVICESET_H
#define PRIVATESERVICESET_H

#include "privateservice_collection.h"
#include "businessservice.h"


/*!
    \class PrivateServiceSet
    \brief Repository of all private/server side business objects which are not accessible by client
    \ingroup Bus_Private

	Use:
	When you use business object as server side only, you dont have to define interface, just inherit from 
	BusinessService and put it here
*/

class PrivateServiceSet
{
public:
	PrivateServiceSet(DbSqlManager *pDbManager,UserSessionManager* pSessionManager);
	~PrivateServiceSet();
	
	//privateserviceobject objects:
	
	//Service_ModuleLicenseManager *ModuleLicenseManager;
	Service_CoreUser *CoreUser;
	Service_BusinessHelper *BusinessHelper;


protected:

	//call to init set (in inher. constructor)
	void InitServiceSet();
	void CheckServiceSet();

	QList<BusinessService *> m_ServiceList;	//< Used to store pointers to bus services
	DbSqlManager *m_DbManager;
	UserSessionManager* m_SessionManager;
};

#endif //PRIVATESERVICESET_H
