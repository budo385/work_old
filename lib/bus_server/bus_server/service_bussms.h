#ifndef SERVICE_BusSms_H
#define SERVICE_BusSms_H

#include <QtCore>
#include "common/common/status.h"

#include "bus_interface/bus_interface/interface_bussms.h"
#include "businessservice.h"

/*!
    \class Service_BusSms
    \brief Service class to store data on voice calls
    \ingroup Bus_Interface_Collection
		
*/

class Service_BusSms : public Interface_BusSms, public BusinessService
{
public:
	virtual ~Service_BusSms(){};

	void ReadSms(Status &Ret_pStatus, int nVoiceCallID,DbRecordSet &Ret_CallData, DbRecordSet &Ret_UAR,DbRecordSet &Ret_GAR);
	void WriteSms(Status &Ret_pStatus, DbRecordSet &RetOut_CallData, QString strLockRes,DbRecordSet &RetOut_ScheduleTask,DbRecordSet &RetOut_UAR,DbRecordSet &RetOut_GAR);
	void ListSmsMessages(Status &Ret_pStatus, DbRecordSet &RetOut_lstCallData, QDate &datDate, int nOwnerID);
	void DeleteSmsMessages(Status &Ret_pStatus, DbRecordSet &RetOut_lstIDs);
};


#endif //SERVICE_BusSms_H
