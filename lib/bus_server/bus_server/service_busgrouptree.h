#ifndef SERVICE_BUSGROUPTREE_H
#define SERVICE_BUSGROUPTREE_H

#include "businessservice.h"
#include "bus_interface/bus_interface/interface_busgrouptree.h"
#include "hierarchicaldata.h"

/*!
	\class Service_BusGroupTree
	\brief Group Tree pattern
	\ingroup Bus_Services
*/

class Service_BusGroupTree : public Interface_BusGroupTree, public BusinessService
{
public:

	Service_BusGroupTree();

	//TREE OPERATIONS
	void ReadTree(Status &Ret_pStatus, int nGroupEntityTypeID,DbRecordSet &Ret_Data);
	void WriteTree(Status &Ret_pStatus, DbRecordSet &RetOut_Data,QString strLockRes);
	void DeleteTree(Status &Ret_pStatus, int nTreeID);
	void LockTree(Status &Ret_pStatus, int nTreeID,QString &Ret_strLockRes);
	void UnLockTree(Status &Ret_pStatus, QString strLockRes);

	//ITEM-GROUP OPERATIONS (node)
	void ReadGroupTree(Status &Ret_pStatus, int nTreeID,DbRecordSet &Ret_Data);
	void WriteGroupItem(Status &Ret_pStatus, int nTreeID,DbRecordSet &RetOut_Data);
	void DeleteGroupItem(Status &Ret_pStatus, int nTreeID,DbRecordSet &DataForDelete);
	void ChangeGroupContent(Status &Ret_pStatus, int nGroupEntityTypeID,int nGroupItemID,int nOperation,DbRecordSet &RetOut_GroupContent);
	void ReadGroupContent(Status &Ret_pStatus, int nGroupEntityTypeID,DbRecordSet &GroupItems,DbRecordSet &Ret_GroupContent);
	
	//special purpose fast functions:
	void WriteGroupItemData(Status &Ret_pStatus, int nGroupItemID, QString strExtCategory, QString strDescription);
	void WriteGroupContentData(Status &Ret_pStatus, int nGroupEntityTypeID, DbRecordSet &GroupItems);
	void RemoveGroupContentItem(Status &Ret_pStatus, int nGroupEntityTypeID,DbRecordSet &GroupItemsForRemove,DbRecordSet &Groups);
	void GetChildrenGroups(Status &Ret_pStatus, int nGroupParentID,DbRecordSet &Ret_ChildrenGroups);

private:

	HierarchicalData m_TreeItemManager;
	void ChangeContent(Status &Ret_pStatus, int nGroupEntityTypeID,int nGroupItemID,int nOperation, DbRecordSet &RetOut_GroupContent,int nTableNMRelation_ID,QString strGroupKeyID,QString strRelationKeyID);
	void DeleteContent(Status &Ret_pStatus, int nGroupEntityTypeID,DbRecordSet &Groups,DbRecordSet &GroupItemsForRemove,int nTableNMRelation_ID,QString strGroupKeyID,QString strRelationKeyID);
};

#endif // SERVICE_BUSGROUPTREE_H
