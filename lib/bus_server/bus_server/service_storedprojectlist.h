#ifndef SERVICE_StoredProjectList_H
#define SERVICE_StoredProjectList_H

#include "businessservice.h"
#include "bus_interface/bus_interface/interface_storedprojectlists.h"


/*!
	\class Service_StoredProjectList
	\brief Special implementation for stored project lists
	\ingroup Bus_Services

*/

class Service_StoredProjectLists : public Interface_StoredProjectLists, public BusinessService
{
public:
	Service_StoredProjectLists();

	void GetListNames(Status &Ret_pStatus, DbRecordSet &Ret_Data);
	void DeleteList(Status &Ret_pStatus, int nListID);
	void CreateList(Status &Ret_pStatus, QString strListName, DbRecordSet &lstData, int &RetOut_nListID, bool bIsSelection, QString strSelectionXML, bool bIsFilter, QString strFilterXML);
	void GetListData(Status &Ret_pStatus, int nListID, DbRecordSet &Ret_lstData, bool &Ret_bIsSelection, QString &Ret_strSelectionXML, bool &Ret_bIsFilter, QString &Ret_strFilterXML, bool bGetDescOnly);
	void SetListData(Status &Ret_pStatus, int nListID, DbRecordSet &lstData, bool bIsSelection, QString strSelectionXML, bool bIsFilter, QString strFilterXML);
	void GetListDataLevelByLevel(Status &Ret_pStatus, int nListID, DbRecordSet &Ret_lstData, QString strParentProjectCode="", int nLevelsDeep=-1);

protected:
	void SelectionXml2Sql(Status &Ret_pStatus, QString strXML, QString &strJoin, QString &strWhere);
	void ProcessCollectionXml(Status &Ret_pStatus, QString strXML, DbRecordSet &Ret_lstData);
	int  FindListByName(QString strName);
};

#endif // SERVICE_StoredProjectList_H
