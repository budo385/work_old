TEMPLATE	= lib
CONFIG		= qt warn_on release staticlib
HEADERS		= bus_server/boentity.h \
		  bus_server/boentity_context.h \
		  bus_server/boentity_serversideinterface.h \
		  bus_server/boentityservice_busperson.h \
		  bus_server/boentityservice_buspersoncontact.h \
		  bus_server/boentityservice_buspersoninfo.h \
		  bus_server/boentityservice_buspersonmain.h \
		  bus_server/boentityservice_testcontact.h \
		  bus_server/boentityservice_testcontactinfo.h \
		  bus_server/boentityservice_testcontactphone.h \
		  bus_server/boentityservice_testentitycontactrecord.h \
		  bus_server/boentityservice_testtree.h \
		  bus_server/boentityset.h \
		  bus_server/bosimpleentityrecord.h \
		  bus_server/bosimpleentitytimeline.h \
		  bus_server/bosimpleentityudf.h \
		  bus_server/businessservice.h \
		  bus_server/businessservice_collection.h \
		  bus_server/businessservicenested.h \
		  bus_server/businessserviceset.h \
		  bus_server/privateservice_collection.h \
		  bus_server/privateserviceset.h \
		  bus_server/serversessiondata.h \
		  bus_server/serversessionmanager.h \
		  bus_server/service_accessrights.h \
		  bus_server/service_appserversession.h \
		  bus_server/service_buseventlog.h \
		  bus_server/service_mainentityselector.h \
		  bus_server/service_modulelicense.h \
		  bus_server/service_modulelicensemanager.h \
		  bus_server/service_syseventlog.h \
		  bus_server/service_testbusinessobject.h \
		  bus_server/service_testcontact.h \
		  bus_server/service_treeview.h \
		  bus_server/service_userlogon.h \
		  bus_server/service_usersession.h \
		  bus_server/systemservice.h \
		  bus_server/systemservice_collection.h \
		  bus_server/systemserviceset.h \
		  bus_server/usersessiondata.h \
		  bus_server/usersessionmanager.h \
		  bus_server/usersessionmanager_thick.h \
		  bus_server/usersessionmanager_thin.h
SOURCES		= bus_server/boentityservice_busperson.cpp \
		  bus_server/boentityservice_testcontact.cpp \
		  bus_server/boentityset.cpp \
		  bus_server/bosimpleentityrecord.cpp \
		  bus_server/bosimpleentitytimeline.cpp \
		  bus_server/bosimpleentityudf.cpp \
		  bus_server/businessserviceset.cpp \
		  bus_server/privateserviceset.cpp \
		  bus_server/serversessionmanager.cpp \
		  bus_server/service_accessrights.cpp \
		  bus_server/service_appserversession.cpp \
		  bus_server/service_buseventlog.cpp \
		  bus_server/service_mainentityselector.cpp \
		  bus_server/service_modulelicense.cpp \
		  bus_server/service_modulelicensemanager.cpp \
		  bus_server/service_syseventlog.cpp \
		  bus_server/service_testbusinessobject.cpp \
		  bus_server/service_testcontact.cpp \
		  bus_server/service_treeview.cpp \
		  bus_server/service_userlogon.cpp \
		  bus_server/service_usersession.cpp \
		  bus_server/systemserviceset.cpp \
		  bus_server/usersessionmanager_thick.cpp \
		  bus_server/usersessionmanager_thin.cpp
INTERFACES	= 
TARGET		= bus_server
INCLUDEPATH	+= ../	/usr/lib/qt4/include/QtSql
