# GIT setup for Visual #

* install GIT (https://git-scm.com/downloadshttps://git-scm.com/downloads) use windows command shell
* install GitExtensions from http://gitextensions.github.io/ (I also installed kDiff as DiffManager, you can use any other tool for that as WinMerge)
* in Visual, go to Tools/Extensions Manager/Online Gallery, install Git Source Control Provider
* in Visual go to Tools/Options/Source Control, choose Git Source Control Provider
* in Visual/Git in Global Settings set username and email
* install c:\Program Files (x86)\GitExtensions\GitCredentialWinStore\git-credential-winstore.exe to enable password prompt when pushing
* commit, push, pull, merge with right clik on project/Git/...