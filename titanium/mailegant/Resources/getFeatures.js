function getFeatures()
{
    var evalStr='';
    if(Ti.App.Properties.getString('BASIC_TEMPLATE_PACKAGE')=='true')
    {
		evalStr='com.mailegant.mailegantwriter.basictemplatepackage';
    }
    
    if(Ti.App.Properties.getString('PERSONAL_TEMPLATE_PACKAGE')=='true')
    {
		if(evalStr=='')
		{
		    evalStr='com.mailegant.mailegantwriter.personaltemplates';
		}
		else
		{
		    evalStr=evalStr+',com.mailegant.mailegantwriter.personaltemplates';
		}
    }

	return evalStr;    
}

exports.getFeatures=getFeatures;