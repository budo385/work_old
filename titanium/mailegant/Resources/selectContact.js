function select(editor_webview)
{
    //alert('selectcontact');
    var selectedFirstName='';
    var selectedLastName='';
    var selectedEmail = '';
    var selectedOrganization = '';

    var values = {
	animated: true,
	fields:['email'],
	cancel: function ()
	{
	},

	selectedProperty: function(e)
	{
	    selectedFirstName=e.person.firstName;
	    selectedLastName=e.person.lastName;
	    selectedEmail=e.value;
	    selectedOrganization=e.person.organization;
	    //Ti.API.info(selectedFirstName+' '+selectedLastName+' '+selectedEmail+' '+selectedOrganization);
	    
	    editor_webview.evalJS("setContactEmail('"+selectedFirstName+"','"+selectedLastName
				  +"','"+selectedOrganization+"','"+selectedEmail+"')");
	}
    };
    
    //Ti.API.info('show contacts');
    
    //first select contact
    Titanium.Contacts.showContacts(values);
}

exports.select=select;