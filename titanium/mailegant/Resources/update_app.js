var appLocationXML = '';
var	appheadlocXML = '';

function getAppData(isRelease,appVersionInit,zipFilename,_openMainWin_callback)
{
	var url='http://mailegant.com/me.xml';
	if(isRelease)
	{
		url='http://mailegant.com/med.xml';
	}

    var download_xhr = require('http_module');
	download_xhr.httpCall({
	    type:"GET",
	    url:url,
	    timeout:10000,
		error:function (error){
			if(Ti.App.Properties.getString('appVersion')==null || Ti.App.Properties.getString('appVersion')=='')
			{
			    //First time unzip data.
				var unzip=require('unzip_initial_version');
				unzip.unzipInit(appVersionInit, zipFilename);
			}
			_openMainWin_callback();
			return false;
	    },
	    success:function (response) {
	    	var xmlResponse= Titanium.XML.parseString(response.text);
			appVersion = xmlResponse.documentElement.getElementsByTagName('version').item(0).text;
			appLocationXML = xmlResponse.documentElement.getElementsByTagName('apploc').item(0).text;
			appheadlocXML = xmlResponse.documentElement.getElementsByTagName('appheadloc').item(0).text;
			var appdownloadLocation = xmlResponse.documentElement.getElementsByTagName('appdownload').item(0).text;
		
			if(appVersion!=Ti.App.Properties.getString('appVersion'))
			{
			    var version = require('getNewVersion');
			    version.getNewVersion(appVersion,appdownloadLocation,appLocationXML,appheadlocXML,_openMainWin_callback);
		    	return true;
			}
			else
			{
	    		//Ti.App.fireEvent('open_win');
	    		_openMainWin_callback();
				return false;
			}
	    }
    });
}

exports.getAppData=getAppData;
