function scalePic(uri, maxWidth, maxHeight, win)
{
	var loadIndicator=require('load_indicator');
    loadIndicator.showLoadIndicator(win);

    var download_xhr = Titanium.Network.createHTTPClient();
    download_xhr.onload = function()
    {
		loadIndicator.hideLoadIndicator(win);
		var blob=this.responseData;

		var resizer=require('resizeImage');
		var newImageBlob = resizer.resize(blob, maxWidth, maxHeight);

//		var newImageBlob=blob.imageAsResized(maxWidth, maxHeight);

		var base64utils=require('base64utils');
		var data=base64utils.convert2base64(newImageBlob, dataType);
		
    	var filename=base64utils.randomNameGenerator();
	    var newFile = Titanium.Filesystem.getFile(Titanium.Filesystem.tempDirectory,filename);
		newFile.write(newImageBlob);

		Ti.App.fireEvent('selectPicsOut',{data:data, name:newFile.nativePath});
    };
    download_xhr.onerror = function()
    {
		loadIndicator.hideLoadIndicator(win);
		return null;
    };
    
    // open the client
    download_xhr.open('GET',uri);
    download_xhr.send();
}
