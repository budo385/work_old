//Load indicator.
var actInd = null;
/*function showSmallLoadIndicator(indWin)
{
	// loading indicator
	actInd = Titanium.UI.createActivityIndicator({
		style:Titanium.UI.iPhone.ActivityIndicatorStyle.PLAIN,
        //zIndex:20,
        width:40,
        height:40
    });
	//indWin.add(actInd);

	actInd.show();
}*/

function showLoadIndicator(indWin)
{
    if(actInd!=null)
    {
	return;
    }
	// loading indicator
	actInd = Titanium.UI.createActivityIndicator({
		style:Titanium.UI.iPhone.ActivityIndicatorStyle.PLAIN,
        zIndex:20
    });
	indWin.add(actInd);

	actInd.show();
    actInd.width=indWin.width;
    actInd.height=indWin.height;
}

function showLoadIndicatorDark(indWin)
{
	// loading indicator
	actInd = Titanium.UI.createActivityIndicator({
		style:Titanium.UI.iPhone.ActivityIndicatorStyle.DARK,
	zIndex:20
    });
	indWin.add(actInd);

	actInd.show();
    actInd.width=indWin.width;
    actInd.height=indWin.height;
}

function hideLoadIndicator(indWin)
{
    if(actInd!=null)
    {
        actInd.hide();
        actInd=null;
    }
}

exports.showLoadIndicator=showLoadIndicator;
exports.showLoadIndicatorDark=showLoadIndicatorDark;
exports.hideLoadIndicator=hideLoadIndicator;