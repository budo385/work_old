function unzipInit(appVersionInit,zipFilename)
{
    var Compression = require('ti.compression');
    var fileName=Ti.Filesystem.resourcesDirectory+zipFilename;
	var inputDirectory = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory,'html');

	//Ti.API.info('input dir '+inputDirectory);

	var result = Compression.unzip(inputDirectory.nativePath, fileName, true);
	Ti.API.info('Init Unzip: ' + result + ', to: ' + inputDirectory);

    Ti.App.Properties.setString('appVersion',appVersionInit);
    Ti.App.Properties.setString('appLocation',Ti.Filesystem.applicationDataDirectory+'html/index.html');
//    Ti.App.Properties.setString('appLocation',Ti.Filesystem.applicationDataDirectory+'html/index.html?type=content');

	return result;
};

exports.unzipInit=unzipInit;
