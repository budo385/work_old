function convURI(uri, win)
{
	var loadIndicator=require('load_indicator');
    loadIndicator.showLoadIndicator(win);
    //Ti.API.info(' win '+win);
    Ti.API.info(' conv2uri 1');
    var download_xhr = Titanium.Network.createHTTPClient();
    download_xhr.onload = function()
    {
    Ti.API.info(' conv2uri 2');
		loadIndicator.hideLoadIndicator(win);
		var blob=this.responseData;

		var base64utils=require('base64utils');
		var dataType=base64utils.getURItype(uri);
		var data=base64utils.convert2base64(blob, dataType);

    Ti.API.info(' conv2uri 2.2'+data);
    	var filename=base64utils.randomNameGenerator();
	    var newFile = Titanium.Filesystem.getFile(Titanium.Filesystem.tempDirectory,filename);
		newFile.write(blob);
    Ti.API.info(' conv2uri 2.3'+blob);

		Ti.App.fireEvent('selectPicsOut',{data:data, name:newFile.nativePath});
    Ti.API.info(' conv2uri 2.4'+newFile.nativePath);
     };
    download_xhr.onerror = function()
    {
    Ti.API.info(' conv2uri 3');
		loadIndicator.hideLoadIndicator(win);
		return null;
    };
    
    Ti.API.info(' conv2uri 4');
    // open the client
    download_xhr.open('GET',uri);
    Ti.API.info(' conv2uri 5');
    download_xhr.send();
}

exports.convURI=convURI;
