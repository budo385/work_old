(function()
{
	var isRelease=false;
	
	var zipFilename='';
	if(isRelease)
	{
		zipFilename='meappdepl.zip';
	}
	else
	{
		zipFilename='meapp613.zip';
	}
	
	var appVersionInit='608';
	
	//Inapps
	if(Ti.App.Properties.getString('BASIC_TEMPLATE_PACKAGE')==null || Ti.App.Properties.getString('BASIC_TEMPLATE_PACKAGE')=='')
	{
	    Ti.App.Properties.setString('BASIC_TEMPLATE_PACKAGE','false');
	}
	if(Ti.App.Properties.getString('PERSONAL_TEMPLATE_PACKAGE')==null || Ti.App.Properties.getString('PERSONAL_TEMPLATE_PACKAGE')=='')
	{
	    Ti.App.Properties.setString('PERSONAL_TEMPLATE_PACKAGE','false');
	}
	
	var update=require('update_app');
	update.getAppData(isRelease,appVersionInit,zipFilename,_openMainWin_callback);
	
	// open win.
	function _openMainWin_callback()
	{
		var mainWindow = require('appWindow');
		new mainWindow().open();
	}
})();
