function open(strTo,strBcc,strCC,subject,body,strAttachments)
{
	var To=strTo.split(',');
	var Bcc=strBcc.split(',');
	var CC=strCC.split(',');
	var attachments=strAttachments.split(',');

	var emailDialog = Titanium.UI.createEmailDialog();
	emailDialog.setSubject(subject);
	emailDialog.setToRecipients(To);
	emailDialog.setCcRecipients(CC);
	emailDialog.setBccRecipients(Bcc);
	
	emailDialog.setMessageBody(body);
	emailDialog.setHtml(true);
	emailDialog.setBarColor('black');
	
	//Attachments.
	var attLength=attachments.length;
	for(var i=0; i<attLength; i++)
	{
		var attachURL=attachments[i];
		if(attachURL!=null && attachURL!='')
		{
			var f = Titanium.Filesystem.getFile(Titanium.Filesystem.tempDirectory, attachURL);
			emailDialog.addAttachment(f);
		}
	}
	
	emailDialog.addEventListener('complete',function(e)
	{
		if (e.result == emailDialog.SENT)
		{
			if (Ti.Platform.osname != 'android') {
				// android doesn't give us useful result codes.
				// it anyway shows a toast.
				//alert("Message Sent!");
			}
		}
		else if (e.result == emailDialog.FAILED)
		{
			alert("Message was not sent. Result = " + e.error);
		}
	});
	emailDialog.open();
}

exports.open=open;
