function resizeKeepAspectRatioNewWidth(blob, imageWidth, imageHeight, newWidth) {
    // only run this function if suitable values have been entered
    if (imageWidth <= 0 || imageHeight <= 0 || newWidth <= 0)
        return blob;

    var ratio = imageWidth / imageHeight;

    var w = newWidth;
    var h = newWidth / ratio;

    Ti.API.info('ratio: ' + ratio);
    Ti.API.info('w: ' + w);
    Ti.API.info('h: ' + h);

    return blob.imageAsResized(w,h);
}

function getScreenShot(xOffSet,yOffSet,cropWidth,cropHeight,resizeWidth)
{
	Titanium.Media.takeScreenshot(function(event)
	{
		//Crop image
		var image =  event.media.imageAsCropped({x:xOffSet,y:yOffSet,width:cropWidth,height:cropHeight});
		var resizer=require('resizeImage');
		var resizedImage = resizer.resize(image, resizeWidth, cropHeight);
		
//		var resizedImage=resizeKeepAspectRatioNewWidth(image, cropWidth, cropHeight, resizeWidth);
		
		var base64utils=require('base64utils');
	    var data=base64utils.convert2base64(resizedImage,'jpg');
//	    Ti.API.info('data '+data);
	    var filename=base64utils.randomNameGenerator();

	    var newFile = Titanium.Filesystem.getFile(Titanium.Filesystem.tempDirectory,filename);
		newFile.write(resizedImage);

		Ti.App.fireEvent('selectPicsOut',{data:data, name:newFile.nativePath});
	});
}

exports.getScreenShot=getScreenShot;
