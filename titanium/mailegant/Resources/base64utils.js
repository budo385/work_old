function randomNameGenerator()
{
	//var random=Math.floor(Math.random()*10000000);
	var random=new Date().getTime();
	var filename=random+'.jpg';
	return filename;
}

function convert2base64(blob, dataType)
{
//	Ti.API.info('blob '+blob);
	var base64string="data:image/"+dataType+";base64, "+Titanium.Utils.base64encode(blob);
//	Ti.API.info('base64string '+base64string);
	return base64string;
}

function getURItype(uri)
{
    var uriArray=uri.split(".");
    var dataType=uriArray[uriArray.length-1];
    return dataType;
}

exports.randomNameGenerator=randomNameGenerator;
exports.convert2base64=convert2base64;
exports.getURItype=getURItype;