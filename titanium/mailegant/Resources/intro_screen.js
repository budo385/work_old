function IntroView(osName)
{
	var startupView =Titanium.UI.createView({
		backgroundColor:'black',
		top:0,
		bottom:0,
		left:0,
		right:0,
		zIndex:10,
		backgroundImage:'Default@2x.png'
	});
	
	var images = [];
	for (var i=1;i<13;i++)
	{
		images.push('./images/' + i+'.png');
	}
	
	var loadingView =Titanium.UI.createImageView({
		bottom:150,
		images:images,
		duration:100, // in milliseconds, the time before next frame is shown
		repeatCount:0  // 0 means animation repeats indefinitely, use > 1 to control repeat count
	});
	
	startupView.add(loadingView);
	
	loadingView.addEventListener('load', function(e)
	{
		// start animation
		loadingView.start();
	});
	
	startupView.getStarterImage=function (orientation)
	{
	    startupView.backgroundImage='Default@2x.png';
	
	    if(orientation==Titanium.UI.PORTRAIT || orientation==Titanium.UI.UPSIDE_PORTRAIT)
	    {
			if(osName=='ipad')
			{
				if(Titanium.Platform.displayCaps.density=='medium')
				{
			    	startupView.backgroundImage='Default-Portrait.png';
				}
				else
				{
			    	startupView.backgroundImage='Default-Portrait@2x.png';
				}
			}
	    }
	    else if(orientation==Titanium.UI.LANDSCAPE_LEFT || orientation==Titanium.UI.LANDSCAPE_RIGHT)
	    {
			if(osName=='ipad')
			{
				if(Titanium.Platform.displayCaps.density=='medium')
				{
			    	startupView.backgroundImage='Default-Landscape.png';
				}
				else
				{
			    	startupView.backgroundImage='Default-Landscape@2x.png';
				}
			}
	    }
	}
	
	startupView.getStarterImage(Ti.Gesture.getOrientation());
	
	var a1 = Titanium.UI.createAnimation();
	a1.opacity = 0;
	a1.duration = 300;
	a1.repeat = 1;
	
	var startupOpen=true;
	
	startupView.closeStarter = function()
	{
		Ti.API.info('close starter');
	
		if(startupOpen)
		{
		    setTimeout(function() {
			    startupView.animate(a1);
			    a1.addEventListener('complete', function(e)
			    {
			    	loadingView.stop();
			    });
			}, 2000);
	
	//    	startupView.animate(a1);
			startupOpen=false;
		}
	
	}
	return startupView;		
};

module.exports=IntroView;