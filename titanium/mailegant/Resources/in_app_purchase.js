function inAppPurchase()
{
	var loadIndicator=require('load_indicator');
	
    var self = Titanium.UI.createWindow({
		navBarHidden:false
    });

	Titanium.Storekit = Ti.Storekit = require('ti.storekit');
	self.barColor='black';
	self.backgroundImage='images/bgh.png';
	self.backgroundRepeat=true;
	self.backgroundGradient={
	            type: 'radial',
	            startPoint: { x:'50%', y:'50%' },
	            endPoint: {x:'50%', y:'50%' },
	            colors: [ 'transparent', 'black'],
	            startRadius: '0%',
	            endRadius: '170%',
	            backfillStart: true
	       };
	var b = Ti.UI.createButton({
		title:"Back",
		width:120,
		height:40
	});
	b.addEventListener('click',function()
	{
		self.close();
	});
	self.leftNavButton=b;
	
	var tableview = Titanium.UI.createTableView({
	    //data:data,
	    top:0,
	    left:0,
	    bottom:50,
	    right:0,
	    rowHeight:140.00,
	    separatorColor:'black',
	    backgroundColor:'transparent'//,
	    //style:Titanium.UI.iPhone.TableViewStyle.GROUPED
	});
	
	//Document package.
	var row0 = Ti.UI.createTableViewRow();
	row0.rightImage='./images/arrow_white.png';
	var imageView0 = Ti.UI.createImageView({
	    image:'./images/Logo_mailegance_144.png'
	});
	var view0 = Ti.UI.createView({
	    height:75,
	    width:75,
	    top:35,
	    left:10
	});
	view0.add(imageView0);
	var titleLabel = Ti.UI.createLabel({
	    height:20,
	    top:15,
	    right:0,
	    color:'white',
	    font:{fontSize:16,fontWeight:'bold',fontFamily:'Helvetica Neue'},
	    left:95,
	    text:'Basic Template Package'
	});
	var decrLabel = Ti.UI.createLabel({
	    height:105,
	    top:30,
	    right:5,
	    color:'white',
	    font:{fontSize:11,fontWeight:'bold',fontFamily:'Helvetica Neue'},
	    left:95,
	    text:'Basic package of templates for private and business e-mails.'
	});
	row0.add(titleLabel);
	row0.add(decrLabel);
	row0.add(view0);
	
	//SSL package.
	var row1 = Ti.UI.createTableViewRow();
	row1.rightImage='./images/arrow_white.png';
	var imageView1 = Ti.UI.createImageView({
	    image:'./images/Logo_mailegance_144.png'
	});
	var view1 = Ti.UI.createView({
	    height:75,
	    width:75,
	    top:35,
	    left:10
	});
	view1.add(imageView1);
	var titleLabel1 = Ti.UI.createLabel({
	    height:20,
	    top:30,
	    right:0,
	    color:'white',
	    font:{fontSize:16,fontWeight:'bold',fontFamily:'Helvetica Neue'},
	    left:95,
	    text:'Personal Templates'
	});
	var decrLabel1 = Ti.UI.createLabel({
	    height:105,
	    top:30,
	    right:5,
	    color:'white',
	    font:{fontSize:11,fontWeight:'bold',fontFamily:'Helvetica Neue'},
	    left:95,
	    text:'Define an unlimited number of your own personal e-mail templates.'
	});
	row1.add(titleLabel1);
	row1.add(decrLabel1);
	row1.add(view1);
	
	//test package.
	/*var row2 = Ti.UI.createTableViewRow();
	row2.rightImage='./images/arrow_white.png';
	var imageView2 = Ti.UI.createImageView({
	    image:'./images/EverDOCsBridge128.png'
	});
	var view2 = Ti.UI.createView({
	    height:75,
	    width:75,
	    top:35,
	    left:10
	});
	view2.add(imageView2);
	var titleLabel2 = Ti.UI.createLabel({
	    height:20,
	    top:30,
	    right:0,
	    color:'white',
	    font:{fontSize:16,fontWeight:'bold',fontFamily:'Helvetica Neue'},
	    left:95,
	    text:L("test package")
	});
	var decrLabel2 = Ti.UI.createLabel({
	    height:105,
	    top:30,
	    right:5,
	    color:'white',
	    font:{fontSize:11,fontWeight:'bold',fontFamily:'Helvetica Neue'},
	    left:95,
	    text:L("test package")
	});
	row2.add(titleLabel2);
	row2.add(decrLabel2);
	row2.add(view2);*/
	
	tableview.addEventListener('click', function(e) {
	    var product = e.row.product;
	    Ti.Storekit.purchase(product, function(r)
	    {
	        loadIndicator.showLoadIndicator(self);
	
		if (r.state == Ti.Storekit.FAILED)
		{
		    loadIndicator.hideLoadIndicator(self);
	
		    Ti.UI.createAlertDialog({
	                title:'Error',
	                message:'Error purchasing product!'+' '+ r.message
	            }).show();
	        }
	        else if (r.state == Ti.Storekit.PURCHASED || r.state == Ti.Storekit.RESTORED)
		{
		    //loadIndicator.hideLoadIndicator(self);
	
	            var receipt = r.receipt;
	            Ti.Storekit.verifyReceipt({
			receipt: receipt,
			//sandbox: true,
			callback:function(e)
				{
				    //alert('e.success '+e.success);
				    loadIndicator.hideLoadIndicator(self);
				    if (e.success)
				    {
					if (e.valid)
					{
					    if(product.identifier=='com.mailegant.mailegantwriter.basictemplatepackage')
					    {
							Ti.App.Properties.setString('BASIC_TEMPLATE_PACKAGE','true');
							Ti.App.fireEvent('callWrapper',{eventType: 'getFeatures'});
					    }
					    if(product.identifier=='com.mailegant.mailegantwriter.personaltemplates')
					    {
							Ti.App.Properties.setString('PERSONAL_TEMPLATE_PACKAGE','true');
							Ti.App.fireEvent('callWrapper',{eventType: 'getFeatures'});
					    }
		    
					    Ti.UI.createAlertDialog({
						title:'Success!',
						message:'Confirmation verified!'/*+'\n'+L("restore_dialog_message2")*/
					    }).show();
					    
					    self.close();
					}
					else
					{
					    Ti.UI.createAlertDialog({
						title:'Failure?!',
						message:'Confirmation Verification Failed!'
					    }).show();
		    
					    self.close();
					}
				    }
				    else
				    {
					Ti.UI.createAlertDialog({
					    title:'Error',
					    message:'Error verifying receipt!'/*+' '+e.message*/
					}).show();
					
					self.close();
				    }
				}
		    });
	        }
	        else
		{
		    loadIndicator.hideLoadIndicator(self);
	        }
	    });
	});
	
	var restoreAlertDialog = Ti.UI.createAlertDialog({
	    title:'Restore'
	});
	
	restoreAlertDialog.addEventListener('click', function() {
	    self.close();
	});
	
	var restoreCompletedTransactions = Ti.UI.createButton({
	    title:'Reload Already Purchased Add-Ons',
	    bottom: 5, height: 40, left: 5, right: 5
	});
	restoreCompletedTransactions.addEventListener('click', function() {
	    loadIndicator.showLoadIndicator(self);
	    Ti.Storekit.restoreCompletedTransactions();
	});
	
	Ti.Storekit.addEventListener('restoredCompletedTransactions', function(evt)
	{
	    loadIndicator.hideLoadIndicator(self);
	    if (evt.error)
	    {
		alert(evt.error);
	    }
	    else if (evt.transactions == null || evt.transactions.length == 0)
	    {
		alert('There were no transactions to restore!');
	    }
	    else
	    {
		var purchasedProducts='Restored products:';
		for (var i = 0; i < evt.transactions.length; i++)
		{
		    var t = evt.transactions[i];
	
		    if(t.productIdentifier=='com.mailegant.mailegantwriter.basictemplatepackage' && (t.state==Ti.Storekit.RESTORED || t.state==Ti.Storekit.PURCHASED))
		    {
				Ti.App.Properties.setString('BASIC_TEMPLATE_PACKAGE','true');
				Ti.App.fireEvent('callWrapper',{eventType: 'getFeatures'});
				purchasedProducts=purchasedProducts+'\n'+'Basic Template Package';
		    }
		    if(t.productIdentifier=='com.mailegant.mailegantwriter.personaltemplates' && (t.state==Ti.Storekit.RESTORED || t.state==Ti.Storekit.PURCHASED))
		    {
				Ti.App.Properties.setString('PERSONAL_TEMPLATE_PACKAGE','true');
				Ti.App.fireEvent('callWrapper',{eventType: 'getFeatures'});
				purchasedProducts=purchasedProducts+'\n'+'Personal Templates';
		    }
	
		    /*alert({
			state: t.state,
			identifier: t.identifier,
			productIdentifier: t.productIdentifier,
			quantity: t.quantity,
			date: t.date
		    });*/
		}
		purchasedProducts=purchasedProducts+'\n reloaded!';
		restoreAlertDialog.message=purchasedProducts;
		restoreAlertDialog.show();
	    }
	});
	setTimeout(function()
	{
	    //Can we make payments.
	    loadIndicator.showLoadIndicator(self);
	    if (!Ti.Storekit.canMakePayments)
	    {
			//loadIndicator.hideLoadIndicator(self);
			var appDialog=Ti.UI.createAlertDialog({
			    title:'Error',
			    message:'Can not make payments to the in-app purchase store!'
			});
			
			appDialog.show();
			appDialog.addEventListener('click', function()
			{
			    self.close();
			});
	    }
	    else
	    {
			Ti.Storekit.requestProducts(['com.mailegant.mailegantwriter.basictemplatepackage','com.mailegant.mailegantwriter.personaltemplates'], function(e)
			{
			    if (e.success)
			    {
				var len=e.products.length;
				for (var i = 0; i < len; i++)
				{
				    if(e.products[i].identifier=='com.mailegant.mailegantwriter.basictemplatepackage' && Ti.App.Properties.getString('BASIC_TEMPLATE_PACKAGE')=='false')
				    {
						row0.product=e.products[i];
						tableview.appendRow(row0);
				    }
				    
				    if(e.products[i].identifier=='com.mailegant.mailegantwriter.personaltemplates' && Ti.App.Properties.getString('PERSONAL_TEMPLATE_PACKAGE')=='false')
				    {
						row1.product=e.products[i];
						tableview.appendRow(row1);
				    }
		
		/*		    if(e.products[i].identifier=='TEST_PURCHASE')
				    {
					row2.product=e.products[i];
					tableview.appendRow(row2);
				    }*/
				}
				
				self.add(tableview);
				self.add(restoreCompletedTransactions);
				loadIndicator.hideLoadIndicator(self);
		    }
		    else
		    {
				loadIndicator.hideLoadIndicator(self);
				Ti.UI.createAlertDialog({
				    title: 'Error',
				    message: 'Getting products: ' + e.message
				}).show();
				self.close();
				return;
		    }
		});
	    }
	},0);
	
	return self; 
}

module.exports=inAppPurchase;