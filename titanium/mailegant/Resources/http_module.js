function httpCall(options)
{
	Ti.API.info('http call '+options.url);

   //create the titanium HTTPClient
    var httpClient = Titanium.Network.createHTTPClient();

    //set the httpclient's properties with the provided options
    if(options.timeout!=null)
    {
    	httpClient.setTimeout(options.timeout);
    }
    
    httpClient.onerror = options.error;

    //if and when response comes back
    //the success function is called
    httpClient.onload = function(){
        options.success({
            data: httpClient.responseData,
            text: httpClient.responseText
        });
    };

    //open the connection
    httpClient.open(options.type, options.url);

    //send the request
    httpClient.send();
}

exports.httpCall=httpCall;