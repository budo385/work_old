function fireUpTheCamera(maxWidth, maxHeight) {
	Titanium.Media.showCamera({
	
		success:function(event)
		{
			//var cropRect = event.cropRect;
			//var image = event.media;
	
			//Ti.API.debug('Our type was: '+event.mediaType);
			if(event.mediaType == Ti.Media.MEDIA_TYPE_PHOTO)
			{
			    var blob = event.media;
//			    var newImageBlob=blob.imageAsResized(maxWidth, maxHeight);

				var resizer=require('resizeImage');
				var newImageBlob = resizer.resize(blob, maxWidth, maxHeight);

				var base64utils=require('base64utils');
			    var filename=base64utils.randomNameGenerator();

    			var newFile = Titanium.Filesystem.getFile(Titanium.Filesystem.tempDirectory,filename);
				newFile.write(newImageBlob);

			    var data=base64utils.convert2base64(newImageBlob,'jpg');

				Ti.App.fireEvent('selectPicsOut',{data:data, name:newFile.nativePath});
			}
			else
			{
				alert("got the wrong type back ="+event.mediaType);
			}
		},
		cancel:function()
		{
		},
		error:function(error)
		{
			// create alert
			var a = Titanium.UI.createAlertDialog({title:'Camera'});
	
			// set message
			if (error.code == Titanium.Media.NO_CAMERA)
			{
				a.setMessage('Please run this test on device');
			}
			else
			{
				a.setMessage('Unexpected error: ' + error.code);
			}
	
			// show alert
			a.show();
		},
		saveToPhotoGallery:true,
		allowEditing:false,
		mediaTypes:[Ti.Media.MEDIA_TYPE_VIDEO,Ti.Media.MEDIA_TYPE_PHOTO]
	});
}

exports.fireUpTheCamera=fireUpTheCamera;
