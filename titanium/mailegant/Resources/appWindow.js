function MainWin()
{
	var osName=Titanium.Platform.osname;
	var platformHeight = Ti.Platform.displayCaps.platformHeight,
		platformWidth = Ti.Platform.displayCaps.platformWidth;
	var timeoutID=0;

	//
	// create master window
	//
	var self = Titanium.UI.createWindow({
	    title:'Mailegant',
	    navBarHidden: true,
	    tabBarHidden: true,
	    fullscreen:true,
	    backgroundColor:'black'//,
	    //backgroundImage:'Default.png'
	});
	
	self.backgroundImage='images/bgh.png';
	self.backgroundRepeat=true;
	self.backgroundGradient={
	    type: 'radial',
	    startPoint: { x:'50%', y:'50%' },
	    endPoint: {x:'50%', y:'50%' },
	    colors: [ 'transparent', 'black'],
	    startRadius: '0%',
	    endRadius: '170%',
	    backfillStart: true
	};
	
	var htmlUrl=Ti.App.Properties.getString('appLocation')+'?type=iosti';
	Ti.API.info('htmlUrl '+htmlUrl);
	var editor_webview = Ti.UI.createWebView({
	    top:0,
	    bottom:0,
	    left:0,
	    right:0,
		zIndex:8,
		url:htmlUrl
	});
	
	var popoverView=null;
	var arrowDirection=null;
	
	if (osName == 'ipad')
	{
		// photogallery displays in a popover on the ipad and we
		// want to make it relative to our image with a left arrow
		arrowDirection = Ti.UI.iPad.POPOVER_ARROW_DIRECTION_LEFT;
		popoverView = editor_webview;
	}
	
	//Crate startup view.	
	var IntroView = require('/intro_screen');
	var StartupView = new IntroView(osName); 

	timeoutID=setTimeout(function(){
	    Ti.App.fireEvent('callWrapper',{eventType: 'liftCurtain'});
	    Ti.API.info('lift courtain timeout fire');
	}, 15000);
	

	function liftCourtainF()
	{
	    clearTimeout(timeoutID);
	    timeoutID=0;
	    Ti.API.info('lift courtain');
	    StartupView.closeStarter();
	}

	Ti.Gesture.addEventListener('orientationchange',function(e)
	{
		if(e.orientation==Titanium.UI.PORTRAIT || e.orientation==Titanium.UI.UPSIDE_PORTRAIT || 
			e.orientation==Titanium.UI.LANDSCAPE_LEFT || e.orientation==Titanium.UI.LANDSCAPE_RIGHT)
		{
			if(e.orientation==Titanium.UI.PORTRAIT || e.orientation==Titanium.UI.UPSIDE_PORTRAIT)
			{
				editor_webview.evalJS("doRefresh('portrait','"+osName+"')");
			}
			else if(e.orientation==Titanium.UI.LANDSCAPE_LEFT || e.orientation==Titanium.UI.LANDSCAPE_RIGHT)
			{
		    	editor_webview.evalJS("doRefresh('landscape','"+osName+"')");
			}
		    StartupView.getStarterImage(e.orientation);
		}
	});

	Ti.App.addEventListener('callWrapper',function(e)
	{
	    if(e.eventType=='eventScotty')
	    {
		    editor_webview.evalJS("eventDispatcher('"+e.ScottyEventName+"')");
	    }
	    else if(e.eventType=='selectPics')
	    {
			var selectPics=require('selectPics');
	    	selectPics.select(e.numPar1,e.numPar2,editor_webview,arrowDirection);
	    }
	    else if(e.eventType=='getScreenPic')
	    {
			var xOffSet=parseInt(e.strPar1);
			var yOffSet=parseInt(e.strPar2);
			var cropWidth=parseInt(e.strPar3);
			var cropHeight=parseInt(e.strPar4);
			var resizeWidth=parseInt(e.strPar5);
		
			var scrShot=require('screenShot');
			scrShot.getScreenShot(xOffSet,yOffSet,cropWidth,cropHeight,resizeWidth);
	    }
	    else if(e.eventType=='conv2DataURI')
	    {
			var conv2DataURI=require('conv2DataURI');
	    	conv2DataURI.convURI(e.strPar1,self);
	    }
	    else if(e.eventType=='convFile2DataURI')
	    {
//	    		var inAppWindow = require('in_app_purchase');
//	new inAppWindow().open({modal:true});


			var convFile2DataURI=require('convFile2DataURI');
	    	convFile2DataURI.convURI(e.strPar1,e.numPar1,e.numPar2);
	    }
	    else if(e.eventType=='scalePic')
	    {
			var scalePic=require('scalePic');
			scalePic.scalePic(e.strPar1, e.numPar1, e.numPar2, self);
	    }
	    else if(e.eventType=='openEmailDialog')
	    {
			var strTo=e.strPar1;
			var strBcc=e.strPar2;
			var strCC=e.strPar3;
			var subject=e.strPar4;
			var body=e.strPar5;
			var strAttachments=e.strPar6;
		
			var mailDialog=require('openMailDialog');
			mailDialog.open(strTo,strBcc,strCC,subject,body,strAttachments);
	    }
	    else if(e.eventType=='setContentHeight')
	    {
			editor_webview.height=e.numPar1;
	    }
	/*    else if(e.eventType=='setHeaderHeight')
	    {
			setHeadHeight(e.numPar1);
	    }
	    else if(e.eventType=='setSidebarWidth')
	    {
			setSidebarWidth(e.numPar1);
	    }*/
	    else if(e.eventType=='selectContact')
	    {
			var selectContact=require('selectContact');
			selectContact.select(editor_webview);
	    }
	    else if(e.eventType=='getFeatures')
	    {
			var getFeatures=require('getFeatures');
			var evalStr=getFeatures.getFeatures();
	    	editor_webview.evalJS("registerFeatures('"+evalStr+"')");
	    }
	    else if(e.eventType=='inAppPurchase')
	    {
			var inAppWindow = require('in_app_purchase');
			new inAppWindow().open({modal:true});
	    }
	    else if(e.eventType=='liftCurtain')
	    {
			liftCourtainF();
	    }
	    else if(e.eventType=='takePics')
	    {
			var fireUpCamera=require('fireUpCamera');
	    	fireUpCamera.fireUpTheCamera(e.numPar1, e.numPar2);
	    }
	    else if(e.eventType=='ReadFile')
	    {
Ti.API.info('ReadFile main '+e.strPar1 +' '+e.strPar2+' '+ e.strPar3);
			var fileManager=require('file_manager');
	    	fileManager.readFile(e.strPar1, e.strPar2, e.strPar3, editor_webview);
	    }
	    else if(e.eventType=='WriteFile')
	    {
//Ti.API.info('WriteFile '+e.strPar1+' '+e.strPar2);
			var fileManager=require('file_manager');
	    	fileManager.writeFile(e.strPar1, e.strPar2);
	    }
	    else if(e.eventType=='DeleteFile')
	    {
//Ti.API.info('DeleteFile '+e.strPar1);
			var fileManager=require('file_manager');
	    	fileManager.deleteFile(e.strPar1);
	    }
	    else if(e.eventType=='CreateDir')
	    {
//Ti.API.info('CreateDir '+e.strPar1);
			var fileManager=require('file_manager');
	    	fileManager.createDir(e.strPar1);
	    }
	});
	

	self.add(StartupView);
	self.add(editor_webview);

	return self;	
}

module.exports=MainWin;