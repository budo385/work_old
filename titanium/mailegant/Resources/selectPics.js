function select(maxWidth,maxHeight,popoverView,arrowDirection)
{
	//Ti.API.info('SELECT10 ' +maxWidth+' '+ maxHeight+' '+ popoverView+' '+ arrowDirection);
    
    Titanium.Media.openPhotoGallery(
    {
		success:function(event)
		{
		    var blob = event.media;
		    
	    	var resizer=require('resizeImage');
			var newImageBlob = resizer.resize(blob, maxWidth, maxHeight);

//		    var newImageBlob=blob.imageAsResized(maxWidth, maxHeight);

			var base64utils=require('base64utils');
		    var data=base64utils.convert2base64(newImageBlob,'jpg');
//		    Ti.API.info('data '+data);
		    var filename=base64utils.randomNameGenerator();

		    var newFile = Titanium.Filesystem.getFile(Titanium.Filesystem.tempDirectory,filename);
			newFile.write(newImageBlob);

//			Ti.API.info('SELECT2 ' +newFile.nativePath);

			Ti.App.fireEvent('selectPicsOut',{data:data, name:newFile.nativePath});
		},
		cancel:function()
		{
		    return null;
		},
		error:function(error)
		{
		    return null;
		},
		allowEditing:false,
		showControls:true,
		//popoverView:popoverView,
		//arrowDirection:arrowDirection,
		mediaTypes:[Ti.Media.MEDIA_TYPE_PHOTO]
    });
}

exports.select=select;
