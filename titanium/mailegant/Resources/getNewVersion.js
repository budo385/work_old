function getNewVersion(appVersion,appdownloadLocation,appLocationXML,appheadlocXML,_openMainWin_callback){

	var nativePath='';
    var download_xhr = require('http_module');
    download_xhr.httpCall({
	    type:"GET",
	    url:appdownloadLocation,
	    timeout:10000,
	   error:function (error) 
	    {
    		//Ti.App.fireEvent('open_win');
			_openMainWin_callback();
			alert('error download');
	    	return nativePath;
	    },
	    success:function (response) 
	    {
	        var unzip=require('unzip_new_version');
	        nativePath = unzip.unzipNewVersion(appVersion,response.data);
	    	Ti.App.Properties.setString('appVersion',appVersion);
	    	Ti.App.Properties.setString('appLocation',nativePath +appLocationXML);
	    	Ti.App.Properties.setString('appheadloc',nativePath +appheadlocXML);
    		//Ti.App.fireEvent('open_win');
	    	_openMainWin_callback();
	    	return nativePath;
	    }
    });
}

exports.getNewVersion=getNewVersion;