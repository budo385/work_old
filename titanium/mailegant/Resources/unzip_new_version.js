function unzipNewVersion(appVersion, data)
{
	var nativePath='';
	var fileName=appVersion+'.zip';
	var tmp = Titanium.Filesystem.tempDirectory;
	var f = Titanium.Filesystem.getFile(tmp,fileName);
	//    	Ti.API.info('f '+f);
	f.write(data);

	var dir = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory,'html');
	if(dir.exists())
	{
		dir.deleteDirectory(true);
	}
	dir.createDirectory();
	
    var Compression = require('ti.compression');
	var result = Compression.unzip(dir.nativePath, tmp+fileName, true);
	Ti.API.info('Unzip: ' + result + ', to: ' + dir);
	Ti.API.info('external directoryListing12 = ' + dir.getDirectoryListing());
	
	if (result == 'success')
	{
	   	nativePath=dir.nativePath;
		return nativePath;
	}
	else
	{
	    alert('error unzip');
    	return nativePath;
	}
	
};

exports.unzipNewVersion=unzipNewVersion;
