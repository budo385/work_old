Titanium.include('./win_close_timeout.js');

Titanium.UI.currentWindow.barColor = '#0F3752'; 

if(Titanium.Platform.osname=='ipad')
{
    Titanium.UI.currentWindow.backgroundImage = './images/Pooltable_768x1024.jpg';
}
else
{
    Titanium.UI.currentWindow.backgroundImage = './images/Pooltable_320x480.jpg';
}

Ti.Gesture.addEventListener('orientationchange',function(e)
{
    if(e.orientation==Titanium.UI.PORTRAIT || e.orientation==Titanium.UI.UPSIDE_PORTRAIT)
    {
	if(Titanium.Platform.osname=='ipad')
	{
	    Titanium.UI.currentWindow.backgroundImage = './images/Pooltable_768x1024.jpg';
	}
	else
	{
	    Titanium.UI.currentWindow.backgroundImage = './images/Pooltable_320x480.jpg';
	}
    }
    else if(e.orientation==Titanium.UI.LANDSCAPE_LEFT || e.orientation==Titanium.UI.LANDSCAPE_RIGHT)
    {
	if(Titanium.Platform.osname=='ipad')
	{
	    Titanium.UI.currentWindow.backgroundImage = './images/Pooltable_1024x768.jpg';
	}
	else
	{
	    Titanium.UI.currentWindow.backgroundImage = './images/Pooltable_480x320.jpg';
	}
    }
});
