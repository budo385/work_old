function getThumb(fileType)
{
    var thumb_link='./images/file_type_thumbs/';
    if(fileType=='doc' || fileType=='docx')
    {
        thumb_link=thumb_link+'MyWord90.png';
    }
    else if(fileType=='xls' || fileType=='xlsx')
    {
        thumb_link=thumb_link+'MyExcel90.png';
    }
    else if(fileType=='ppt' || fileType=='pptx' || fileType=='pps' || fileType=='ppsx')
    {
        thumb_link=thumb_link+'MyPowerPoint90.png';
    }
    else if(fileType=='pdf')
    {
        thumb_link=thumb_link+'AcrobatReader.png';
    }
    else if(fileType=='txt' || fileType=='html' || fileType=='js' || fileType=='css' || fileType=='csv'
            || fileType=='php' || fileType=='c' || fileType=='cpp' || fileType=='h' || fileType=='log'
            || fileType=='ini')
    {
        thumb_link=thumb_link+'txt.png';
    }
    else if(fileType=='pages')
    {
        thumb_link=thumb_link+'document.png';
    }
    else if(fileType=='numbers')
    {
        thumb_link=thumb_link+'kchart_chrt.png';
    }
    else if(fileType=='key')
    {
        thumb_link=thumb_link+'pps.png';
    }
    else if(fileType=='png' || fileType=='jpg' || fileType=='tiff' || fileType=='tif'
            || fileType=='gif' || fileType=='jpeg' || fileType=='bmp')
    {
        thumb_link=thumb_link+'image.png';
    }
    else if(fileType=='webloc' || fileType=='url')
    {
        thumb_link=thumb_link+'network.png';
    }
    else
    {
        thumb_link=thumb_link+'shellscript.png';
    }

    return thumb_link;
}
