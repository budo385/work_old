//Navigation bar.
function openCatalogInfoWin()
{
    var w = Titanium.UI.createWindow({
	    url:'./info.js',
	    previousWindows:previousWindows
    });
    Titanium.UI.currentTab.open(w);
}

function openInAppPurchase()
{
    var w = Titanium.UI.createWindow({
	    url:'./in_app_purchase.js',
	    previousWindows:previousWindows,
	    connectToServerPreviousWindows:connectToServerPreviousWindows,
	    bFromConnectWin:0
    });
//	w.open({modal:true/*,modalTransitionStyle:style,modalStyle:presentation,navBarHidden:true*/});
    Titanium.UI.currentTab.open(w);
}

var buttonObjectsCatalog = [];
if(Ti.App.Properties.getString('DOCUMENT_PACKAGE_ENABLED')=='false' ||
   Ti.App.Properties.getString('SSL_PACKAGE_ENABLED')=='false')
{
    buttonObjectsCatalog.push({title:L("add_ons_button"), width:65});
}

buttonObjectsCatalog.push({title:L("info"), width:40});

var buttonBarCatalog = Titanium.UI.createButtonBar({
    labels:buttonObjectsCatalog,
    backgroundColor:'#0F3752',
    style:Titanium.UI.iPhone.SystemButtonStyle.BAR
});

buttonBarCatalog.addEventListener('click', function(e)
{
    if(Ti.App.Properties.getString('DOCUMENT_PACKAGE_ENABLED')=='false' ||
       Ti.App.Properties.getString('SSL_PACKAGE_ENABLED')=='false')
    {
	if(e.index==0)
	{
	    openInAppPurchase();	
	}
	else
	{
	    openCatalogInfoWin();
	}
    }
    else
    {
	openCatalogInfoWin();
    }
});

Titanium.UI.currentWindow.rightNavButton=buttonBarCatalog;