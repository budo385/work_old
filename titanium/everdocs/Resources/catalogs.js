Titanium.include('./load_indicator.js','./bo_call_new.js','login_calls.js',
		 './background_orientation.js','navigation_right_button.js');

var catalogsWin = Titanium.UI.currentWindow;
//catalogsWin.titleControl = null;
catalogsWin.titleImage = './images/everPics_TitleBar.png';

var catalogPageSize=Ti.App.Properties.getString('CATALOG_PAGE_SIZE');
var path = Titanium.Filesystem.resourcesDirectory;
var bNewCatalogCreated=false;
var data=[];
var thumbSize=75;
var tableViewRowHeight=95;

var logoutButton = Titanium.UI.createButton({
    title:L("logout")
});

var fileWindowsStack=[];

var connectToServerPreviousWindows = catalogsWin.connectToServerPreviousWindows;
connectToServerPreviousWindows.push(catalogsWin);
var previousWindows = catalogsWin.previousWindows;
previousWindows.push(catalogsWin);

logoutButton.addEventListener('click', function()
{
    var REST_LOGOUT = Titanium.App.Properties.getString('REST_LOGOUT');
    BO_LogOut(REST_LOGOUT);
    Titanium.UI.currentWindow.close();
});

catalogsWin.leftNavButton=logoutButton;

// initialize to all modes
catalogsWin.orientationModes = [
    Titanium.UI.PORTRAIT,
    Titanium.UI.UPSIDE_PORTRAIT,
    Titanium.UI.LANDSCAPE_LEFT,
    Titanium.UI.LANDSCAPE_RIGHT
];

//------------------
//Table view.
//------------------
var serverNameLabel = Titanium.UI.createLabel({
	text:catalogsWin.serverName,
	height:35,
	left:10,
	top:5,
	color:'white',
	font:{fontSize:22,fontWeight:'bold',fontFamily:'Helvetica Neue'},
	textAlign:'center'
});
var addOnsLabel = Titanium.UI.createLabel({
	height:50,
	left:10,
	right:10,
	top:35,
	color:'white',
	font:{fontWeight:'normal',fontFamily:'Helvetica Neue', fontStyle:'italic'},
	textAlign:'left'
});

//Upper table view.
var tableView = Titanium.UI.createTableView({
    top:40,
    rowHeight:tableViewRowHeight,
    backgroundColor:'transparent',
    separatorColor:'transparent'
});

if(Ti.App.Properties.getString('DOCUMENT_PACKAGE_ENABLED')=='false')
{
    catalogsWin.add(addOnsLabel);
    tableView.top=90;
    if(Titanium.Platform.osname=='ipad')
    {
	addOnsLabel.text=L("doc_not_enabled_catalog_message_ipad");
    }
    else
    {
	addOnsLabel.text=L("doc_not_enabled_catalog_message_ipone");
    }
}

catalogsWin.add(serverNameLabel);
catalogsWin.add(tableView);

//------------------------------------------------------
// BO call methods (get catalogs, fill the list)
//------------------------------------------------------
function openFilesWin(catalogName,catalogCount,catalogCountTotal,canDownload,canUpload,canDelete,canInvite,canSendEmail)
{
    var w = Titanium.UI.createWindow({
        url:'./files.js',
	previousWindows:previousWindows,
	connectToServerPreviousWindows:connectToServerPreviousWindows
    });
    

    w.catalogName=catalogName;
    w.originalCatalogName=catalogName;
    w.isFolder=false;
    w.catalogCount = catalogCount; //count of pictures only in this catalog
    w.catalogCountTotal=catalogCountTotal; //count of pics with pictures in subfolders.
    w.canDownload=canDownload;
    w.canUpload=canUpload;
    w.canDelete=canDelete;
    w.canInvite=canInvite;
    w.canSendEmail=canSendEmail;
    w.inFolder=0;

    Titanium.UI.currentTab.open(w);
}

function createCatalog(catalogName,catalogCount,catalogCountTotal,catalogThumb,canDownload,canUpload,
		       canDelete,canInvite,canSendEmail,isDrive,driveType,filesCount,subdirsCount)
{
    	var row = Ti.UI.createTableViewRow();
	row.rightImage='./images/arrow_white.png';
        row.catalogName = catalogName;
	row.catalogCount=catalogCount;
	row.catalogCountTotal=catalogCountTotal;
	row.backgroundImage='./images/PTB_TableElement.jpg';
	row.selectedBackgroundImage='./images/PTB_TableElement_Sel.png';
	row.canDownload=canDownload;
	row.canUpload=canUpload;
	row.canDelete=canDelete;
	row.canInvite=canInvite;
	row.canSendEmail=canSendEmail;
	row.isDrive=isDrive;

	var imageView = Ti.UI.createImageView({
	});
	if(catalogThumb=='' || catalogThumb==null)
	{
	    if(isDrive==1)
	    {
		if(driveType==2)
		{
		    imageView.image='./images/Icon_RemovableDrive75.png';
		}
		else if(driveType==4)
		{
		    imageView.image='./images/Icon_Netdrive75.png';
		}
		else if(driveType==5)
		{
		    imageView.image='./images/Icon_Optical75.png';
		}
		else
		{
		    imageView.image='./images/Icon_Disk75.png';
		}
	    }
	    else
	    {
		imageView.image='./images/Icon_Catalog75.png';
	    }
	}
	else
	{
	    imageView.image=Titanium.Utils.base64decode(catalogThumb);
	}
        
var thumbSize=75;
var tableViewRowHeight=95;
        var view = Ti.UI.createView({
            height:thumbSize,
            width:thumbSize,
            top:10,
            left:10
        });
	
        view.add(imageView);

	var catalogCountLabel='';
	if(catalogCountTotal>0)
	{
	    catalogCountLabel=" ("+catalogCountTotal+")";
	}

	var nameLabel = Ti.UI.createLabel({
	height:45,
	top:0,
	color:'white',
	font:{fontWeight:'bold',fontFamily:'Helvetica Neue'},
	left:tableViewRowHeight,
	text:catalogName+catalogCountLabel
	//text:catalogName+" ("+catalogCountTotal+" docs, "+subdirsCount+" subdirs, "+filesCount+" files)"
	});	
	
	row.add(view);
	row.add(nameLabel);
	
	data.push(row);
}

var path = Titanium.Filesystem.resourcesDirectory;
function parseCatalogXML(xml) 
{
    //alert(xhrBOCall.responseText);
    //Ti.API.info('catalog response '+xhrBOCall.responseText);
    data=[];
    //catalogsWin.remove(tableView);
    //parse incoming XML DOM and process it immediately
    var catalogs = xml.documentElement.getElementsByTagName('catalog');
    if(catalogs===null)
    {
	hideLoadIndicator(catalogsWin);
	return;
    }
    
    for (var i=0;i<catalogs.length;i++)  
    {
	var catalogName=catalogs.item(i).getElementsByTagName('ALIAS').item(0).text;
	catalogName=xmlDecode(catalogName);
	var catalogCountTotal=catalogs.item(i).getElementsByTagName('PIC_CNT').item(0).text;
	var catalogCount=catalogs.item(i).getElementsByTagName('CNT').item(0).text;
	var catalogThumbData=catalogs.item(i).getElementsByTagName('THUMB').item(0).text;
	var canDownload=catalogs.item(i).getElementsByTagName('CAN_DOWNLOAD').item(0).text;
	var canUpload=catalogs.item(i).getElementsByTagName('CAN_UPLOAD').item(0).text;
	var canDelete=catalogs.item(i).getElementsByTagName('CAN_DELETE').item(0).text;
	var canInvite=catalogs.item(i).getElementsByTagName('CAN_SEND_INVITE').item(0).text;
	var canSendEmail=catalogs.item(i).getElementsByTagName('CAN_SEND_EMAIL').item(0).text;
	var canSendEmail=catalogs.item(i).getElementsByTagName('CAN_SEND_EMAIL').item(0).text;
	var isDrive=catalogs.item(i).getElementsByTagName('IS_DRIVE').item(0).text;
	var driveType=catalogs.item(i).getElementsByTagName('TYPE_DRIVE').item(0).text;
	var filesCount=catalogs.item(i).getElementsByTagName('CNT_FILES').item(0).text;
	var subdirsCount=catalogs.item(i).getElementsByTagName('CNT_SUBDIRS').item(0).text;

	createCatalog(catalogName,catalogCount,catalogCountTotal,catalogThumbData,
		      canDownload,canUpload,canDelete,canInvite,canSendEmail,isDrive,
		      driveType,filesCount,subdirsCount);
    }
	
    tableView.data=data;
    hideLoadIndicator(catalogsWin);
}

function onBoCallError(xml)
{
    hideLoadIndicator(catalogsWin);
    alert(L("catalog_error_alert"));
}

function getCatalogs()
{

    var REST_WEB_ROOT_URL=Titanium.App.Properties.getString('REST_WEB_ROOT_URL');
    showLoadIndicator(catalogsWin);
    BO_Call("GET",REST_WEB_ROOT_URL+"/service/catalogs",parseCatalogXML, onBoCallError, '');
}

getCatalogs();

tableView.addEventListener('click', function(e)
{
    openFilesWin(e.rowData.catalogName,e.rowData.catalogCount,e.rowData.catalogCountTotal,
		e.rowData.canDownload,e.rowData.canUpload,e.rowData.canDelete,e.rowData.canInvite,e.rowData.canSendEmail);
});

Ti.App.addEventListener('reloadAllCatalogs', function()
{
    getCatalogs();
});
