Titanium.include('./config_titanium.js','./bo_call_before_send_call.js','./bo_parse_error.js');

//Locals
var routingServerID='';
var bSecondRegisterServerCall=false;

function BO_TestConnectivity(strHostSSL,strURL,strPort)
{
	var xhr = Titanium.Network.createHTTPClient();
	var url=strHostSSL+strURL+":"+strPort+"/rest/ping_server";
	xhr.validateSecureCertificate = false;
	xhr.timeout=12000;
	xhr.open("GET",url,false);
	xhr.send("");
	if(xhr.responseText=='1')
	{
		return true;
	}
	else
	{
		return false;
	}
}

function PrepareRegisterServerPostData(serverID)
{	
	var PFH_program_code=Ti.App.Properties.getString('PROGRAM_CODE');
	var PFH_program_version_major = PROGRAM_VERSION_MAJOR;
	var PFH_program_version_minor = PROGRAM_VERSION_MINOR;
	var PFH_program_version_revision = PROGRAM_VERSION_REVISION;
	var PFH_program_platform_name = Titanium.Platform.osname;
	var PFH_program_platform_UID = '';
	var PFH_client_identity = '';
	if(Titanium.App.Properties.hasProperty('clientID'))
	{
	    PFH_client_identity = Titanium.App.Properties.getString('clientID');
	}
	var PFH_host_identity = serverID;
	var PFH_host_URL ='';
	var PFH_host_port ='';
	var PFH_host_stats_nat_used = '';
	var strPost='<?xml version="1.0" encoding="UTF-8"?><REQUEST><PARAMETERS><PROGRAM_CODE>'+PFH_program_code+'</PROGRAM_CODE>'+'<PROGRAM_VERSION_MAJOR>'+PFH_program_version_major+'</PROGRAM_VERSION_MAJOR>'+'<PROGRAM_VERSION_MINOR>'+PFH_program_version_minor+'</PROGRAM_VERSION_MINOR>'+'<PROGRAM_VERSION_REVISION>'+PFH_program_version_revision+'</PROGRAM_VERSION_REVISION>'+'<PROGRAM_PLATFORM_NAME>'+PFH_program_platform_name+'</PROGRAM_PLATFORM_NAME>'+'<PROGRAM_PLATFORM_UID>'+PFH_program_platform_UID+'</PROGRAM_PLATFORM_UID>'+'<CLIENT_IDENTITY xsi:nil="true"/><HOST_IDENTITY>'+PFH_host_identity+'</HOST_IDENTITY>'+'<HOST_URL>'+PFH_host_URL+'</HOST_URL><HOST_PORT>'+PFH_host_port+'</HOST_PORT>'+'<HOST_STATS_NAT_USED>'+PFH_host_stats_nat_used+'</HOST_STATS_NAT_USED><HOST_SSL></HOST_SSL><HOST_LOCAL_IP></HOST_LOCAL_IP></PARAMETERS></REQUEST>';
	return strPost;
}

function BO_RegisterMasterServerID(serverID, pfCallBackSucess, pfCallBackError)
{
	var xhr = Titanium.Network.createHTTPClient();
	var strPost=PrepareRegisterServerPostData(serverID);
	routingServerID=serverID;
	xhr.onerror = function(e)
	{
		if(!bSecondRegisterServerCall)
		{
			bSecondRegisterServerCall=true;
			BO_RegisterSlaveServerID(routingServerID, pfCallBackSucess, pfCallBackError);
		}
		else
		{
			BO_ParseError(xhr,e.error);
			pfCallBackError(xhr.responseXML);
		}
	};

	xhr.onload = function()
	{
		pfCallBackSucess(xhr.responseXML);
	};

	xhr.validateSecureCertificate = false;
	xhr.timeout=12000;
	xhr.open('POST',FFH_CONNECT_SERVER_MASTER_IP+'/rest/service/register',true);
	BO_Call_beforeSend(xhr);
	xhr.send(strPost);
}

function BO_RegisterSlaveServerID(serverID, pfCallBackSucess, pfCallBackError)
{
	var xhr = Titanium.Network.createHTTPClient();
	xhr.timeout=12000;
	xhr.onload = function()
	{
		FFH_CONNECT_SERVER_MASTER_IP="http://"+xhr.responseText;
		BO_RegisterMasterServerID(serverID, pfCallBackSucess, pfCallBackError);
	};
	xhr.onerror = function(e)
	{
		BO_ParseError(xhr,e.error);
		pfCallBackError(xhr.responseXML);
	};

	xhr.open('GET',FFH_CONNECT_SERVER_LIST,true);
	xhr.send("");
}