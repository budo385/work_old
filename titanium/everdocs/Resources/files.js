Titanium.include('./load_indicator.js','./bo_call_new.js','./background_orientation.js','get_thumb.js'
		 ,'supported_file_types.js','navigation_right_button.js','timer.js');

var filesWin = Titanium.UI.currentWindow;
filesWin.titleControl = null;
filesWin.titleImage = './images/everPics_TitleBar.png';
filesWin.backButtonTitle=L("back");
filesWin.fullscreen=false;
var documentPackageEnabled=Ti.App.Properties.getString('DOCUMENT_PACKAGE_ENABLED');
var isSimulator=Titanium.Platform.model;

var connectToServerPreviousWindows = filesWin.connectToServerPreviousWindows;
connectToServerPreviousWindows.push(filesWin);

var previousWindows = filesWin.previousWindows;
previousWindows.push(filesWin);

var thumbSize=75;
var tableViewRowHeight=95;
var REST_WEB_DOCS_URL=Titanium.App.Properties.getString('REST_WEB_DOCS_URL');
var lastDistance = 0; // calculate location to determine direction
var updating = false;
var startRow=-1;
var lastRow=-1;
var rowNumberToDisplay=100;
var currentPage=0;
var nFilesTotalCount=0;
var nFoldersTotalCount=0;
var next=false;
var previous=false;

var nextRow = Ti.UI.createTableViewRow();
var nextRowimageView = Ti.UI.createImageView({
    image:'./images/MoreDown.png'
});
var nextRowView = Ti.UI.createView({
    height:thumbSize,
    width:thumbSize,
    top:10,
    left:10
});
nextRowView.add(nextRowimageView);
var nextRowNameLabel = Ti.UI.createLabel({
    height:tableViewRowHeight,
    //color:'white',
    top:0,
    text:L("show_next"),
    left:tableViewRowHeight
});
nextRow.add(nextRowView);
nextRow.add(nextRowNameLabel);
nextRow.backgroundColor='#DEE1E3';
nextRow.isNext = true;

var previousRow = Ti.UI.createTableViewRow();
var previousRowimageView = Ti.UI.createImageView({
    image:'./images/MoreUp.png'
});
var previousRowView = Ti.UI.createView({
    height:thumbSize,
    width:thumbSize,
    top:10,
    left:10
});
previousRowView.add(previousRowimageView);
var previousRowNameLabel = Ti.UI.createLabel({
    height:tableViewRowHeight,
    //color:'white',
    top:0,
    text:L("show_previous"),
    left:tableViewRowHeight
});
previousRow.add(previousRowView);
previousRow.add(previousRowNameLabel);
previousRow.backgroundColor='#DEE1E3';
previousRow.isPrevious = true;

var tableData=[];

filesWin.orientationModes = [
    Titanium.UI.PORTRAIT,
    Titanium.UI.UPSIDE_PORTRAIT,
    Titanium.UI.LANDSCAPE_LEFT,
    Titanium.UI.LANDSCAPE_RIGHT
];

var buttonObjectsCatalog = [];
buttonObjectsCatalog.push({title:L("back"), width:50});
buttonObjectsCatalog.push({image:'./images/home.png', width:35});

var buttonBarCatalog = Titanium.UI.createButtonBar({
    labels:buttonObjectsCatalog,
    backgroundColor:'#0F3752',
    style:Titanium.UI.iPhone.SystemButtonStyle.BAR
});

buttonBarCatalog.addEventListener('click', function(e)
{
    if(e.index==0)
    {
	filesWin.close();
    }
    else
    {
	killWindowsToHome();
    }
});

filesWin.leftNavButton=buttonBarCatalog;

function catalogDialog(dialogType)
{
    var w = Ti.UI.createWindow({
	    url:'./modal_dialog.js'
    });

    if(dialogType==0)
    {
	w.dialogType=0;
	w.catalogName=filesWin.originalCatalogName;
    }
    else if(dialogType==1)
    {
	w.dialogType=1;
    }
    
    w.open({modal:true,modalTransitionStyle:Ti.UI.iPhone.MODAL_TRANSITION_STYLE_CROSS_DISSOLVE,navBarHidden:true,fullscreen:true});
}

Ti.App.addEventListener('renameCatalog', function(e)
{
    renameCatalog(filesWin.catalogName, e.name);
    //Ti.API.info('rename '+e.name);
});

Ti.App.addEventListener('createCatalog', function(e)
{
    createNewCatalog(e.name);
//    Ti.API.info('create '+e.name);
});

function editCatalog()
{
    var w = Titanium.UI.createWindow({
        url:'./edit_pictures.js',
        barColor:'#111',
	previousWindows:previousWindows
    });

    w.catalogName = filesWin.catalogName;
    w.canDelete=filesWin.canDelete;
    Titanium.UI.currentTab.open(w);
}

var catalogNameLabel = Ti.UI.createLabel({
    height:20,
    top:5,
    left:10,
    right:0,
    color:'white',
    textAlign:'center',
    text:filesWin.catalogName
});

filesWin.add(catalogNameLabel);

//Toolbar.
var contactsButton = Titanium.UI.createButton({
    backgroundImage:'./images/Contacts_Icon30-1.png',
    height:30,
    width:30
});

var uploadButton = Titanium.UI.createButton({
    backgroundImage:'./images/Upload.png',
    height:30,
    width:30
});

contactsButton.addEventListener('click', function()
{
    if(filesWin.canInvite==0)
    {
	var alertDialog = Ti.UI.createAlertDialog({
	    title:L("access_right_title"),
	    message: strLastBO_ErrorText
        });
        alertDialog.show();
	return;
    }

    var w = Titanium.UI.createWindow({
        url:'./invitation.js',
        barColor:'#111',
	previousWindows:previousWindows
    });

    w.catalogName = filesWin.catalogName;
    Titanium.UI.currentTab.open(w);
});

uploadButton.addEventListener('click', function()
{
    if(filesWin.canUpload==0)
    {
	var alertDialog = Ti.UI.createAlertDialog({
	    title:L("access_right_title"),
	    message: strLastBO_ErrorText
        });
        alertDialog.show();
	return;
    }
    
    var w = Titanium.UI.createWindow({
        url:'./send_files.js',
        barColor:'#111',
	previousWindows:previousWindows
    });

    w.canUpload=filesWin.canUpload;
    w.catalogName = filesWin.catalogName;
    Titanium.UI.currentTab.open(w);
});

var flexSpace = Titanium.UI.createButton({
	systemButton:Titanium.UI.iPhone.SystemButton.FLEXIBLE_SPACE
});

var editButton = Titanium.UI.createButton({
    backgroundImage:'./images/Edit_Icon30-4.png',
    height:30,
    width:30
//    systemButton:Titanium.UI.iPhone.SystemButton.COMPOSE
});

editButton.addEventListener('click', function()
{
    var w = Titanium.UI.createWindow({
        url:'./edit_files.js',
        barColor:'#111',
	previousWindows:previousWindows
    });

    w.canDelete=filesWin.canDelete;
    w.catalogName = filesWin.catalogName;
    Titanium.UI.currentTab.open(w);
});

var toolbar = Titanium.UI.createToolbar({
	items:[flexSpace,contactsButton,flexSpace,uploadButton,flexSpace/*,editButton,flexSpace*/],
	bottom:0,
	borderTop:false,
	borderBottom:false,
	translucent:true,
	barColor:'#0F3752'
});

filesWin.add(toolbar);

function openFolder(catalogName,fileName,folderCountTotal,canDownload,canUpload,canDelete,canInvite,canSendEmail)
{
    w = Titanium.UI.createWindow({
	url:'./files.js',
	barColor:'#111',
	previousWindows:previousWindows,
	connectToServerPreviousWindows:connectToServerPreviousWindows
    });
    w.catalogName = catalogName + "/" + fileName;	//enter sub catalog
    w.originalCatalogName=filesWin.originalCatalogName;
    w.isFolder=true;
    w.catalogPixCountTotal=folderCountTotal; //count of filesWin in this directory and subdirectories
    w.canDownload=canDownload;
    w.canUpload=canUpload;
    w.canDelete=canDelete;
    w.canInvite=canInvite;
    w.canSendEmail=canSendEmail;
    w.inFolder=1;

    Titanium.UI.currentTab.open(w);
}

var tableView = Titanium.UI.createTableView({
    top:30,
    left:0,
    bottom:50,
    right:0,
    rowHeight:tableViewRowHeight,
    backgroundColor:'transparent',
    separatorColor:'transparent'
});

function closethis()
{
//    Ti.UI.currentTab.close(pictures,{animated:true});
    setTimeout(function(){Ti.UI.currentTab.close(filesWin,{animated:true});}, nCloseWinTimeout);
}

filesWin.add(tableView);

function createFileRow(filename,doc_link,displayName,fileSize,fileLastModify,fileType)
{
    var thumb_link=getThumb(fileType);
    
    var row = Ti.UI.createTableViewRow({
	backgroundImage:'./images/PTB_TableElement.jpg',
	selectedBackgroundImage:'./images/PTB_TableElement_Sel.png',
	rightImage:'./images/arrow_white.png',
	fileName:filename,
	displayName:displayName,
	catalogName:filesWin.catalogName,
	doc_link:doc_link,
	thumb_link:thumb_link,
	fileType:fileType,
    	className:'files',
	isFolder:false
    });
    
    //var row = Ti.UI.createTableViewRow({title:displayName});

    var imageView = Ti.UI.createImageView({
    image:thumb_link
    });
    
    view = Ti.UI.createView({
    height:thumbSize,
    width:thumbSize,
    top:10,
    left:10
    });

    view.add(imageView);

    var nameLabel = Ti.UI.createLabel({
	height:tableViewRowHeight,
	color:'white',
	top:0,
	text:displayName+'\n'+fileSize+'\n'+fileLastModify,
	//text:'test',
	left:tableViewRowHeight
    });

    row.add(view);
    row.add(nameLabel);
    
    return row;
}

function createFolderRow(folderName,folderCountTotal,filesCount,subdirsCount)
{
    var rowFolder = Ti.UI.createTableViewRow();
    rowFolder.backgroundImage='./images/PTB_TableElement.jpg';
    rowFolder.selectedBackgroundImage='./images/PTB_TableElement_Sel.png';
    rowFolder.rightImage='./images/arrow_white.png';
    rowFolder.fileName = folderName; 
    rowFolder.catalogName = filesWin.catalogName;
    rowFolder.isFolder = true;
    rowFolder.folderCountTotal=folderCountTotal;
    rowFolder.className='folder';
    
    var imageView = Ti.UI.createImageView({
	image:'./images/Icon_Folder75.png'
    });
    
    var view = Ti.UI.createView({
    height:thumbSize,
    width:thumbSize,
    top:10,
    left:10
    });
    
    view.add(imageView);
    
    var folderCountLabel='';
    if(folderCountTotal>0)
    {
	folderCountLabel=" ("+folderCountTotal+")";
    }

    var nameLabel = Ti.UI.createLabel({
    height:tableViewRowHeight,
    top:0,
    color:'white',
    left:tableViewRowHeight,
    text:folderName+folderCountLabel
    //text:folderName+" ("+folderCountTotal+" docs, "+subdirsCount+" subdirs, "+filesCount+" files)"
    });	

    rowFolder.add(view);
    rowFolder.add(nameLabel);
    
    return rowFolder;
}

function createTableRow(rowDat)
{
    var row;
    if(rowDat.isFolder==1)
    {
	row=createFolderRow(rowDat.folderName,rowDat.folderCountTotal,rowDat.filesCount,rowDat.subdirsCount);
    }
    else
    {
	row=createFileRow(rowDat.filename,rowDat.doc_link,rowDat.displayName,
	rowDat.fileSize,rowDat.fileLastModify,rowDat.fileType);
    }
    
    return row;
}

function nextPage()
{
    next=true;
    currentPage++;
    getCatalog();
}

function previousPage()
{
    previous=true;
    currentPage--;
    getCatalog();
}

function drawData(folderData, filesData)
{
    tableData=[];

    var nCount=folderData.length;
Ti.API.info('nCount folder '+nCount);
    for(var i=0;i<nCount;i++)
    {
	tableData[i]=createTableRow(folderData[i]);
    }

    nCount=filesData.length;
Ti.API.info('nCount files '+nCount);
    for(var j=0;j<nCount;j++)
    {
	tableData[i+j]=createTableRow(filesData[j]);
    }
    
    var totalPages=Math.ceil((parseInt(nFilesTotalCount)+parseInt(nFoldersTotalCount)) / rowNumberToDisplay);
    if((totalPages-1)>currentPage)
    {
	tableData.push(nextRow);
    }
    if(currentPage>0)
    {
	tableData.unshift(previousRow);
    }

    tableView.data=tableData;

    if(next)
    {
	tableView.scrollToIndex(0,{animated:true,position:Ti.UI.iPhone.TableViewScrollPosition.TOP});
    }
    if(previous)
    {
	tableView.scrollToIndex(tableData.length-1,{animated:true,position:Ti.UI.iPhone.TableViewScrollPosition.BOTTOM});
    }

    next=false;
    previous=false;
}

function pfCallBackSucess(xml)
{
    //doTimer();
//    Ti.API.info('files.js response '+xhrBOCall.responseText);
    if((xml.documentElement.getElementsByTagName('ERROR')!=null && xml.documentElement.getElementsByTagName('ERROR').length>0)
       || xml.documentElement.getElementsByTagName('nTotalCount')==null)
    {
	hideLoadIndicator(filesWin);
	var alertDialogError = Ti.UI.createAlertDialog({
	    title:L("files_get_error_title"),
	    message:L("files_get_error_message")
//	    message: xml.documentElement.getElementsByTagName('ERROR').item(0).getElementsByTagName('ERROR_TEXT').item(0).text
	});
	alertDialogError.show();

//	Ti.API.info('error ');
	Titanium.App.Properties.setBool('RELOAD_CATALOGS', true);
	closethis();
	return;
    }
    
    var doc = xml.documentElement;
    var files = doc.getElementsByTagName('file');
    var folders = doc.getElementsByTagName('folder');

    if(currentPage==0)
    {
	nFilesTotalCount=doc.getElementsByTagName('nTotalCount').item(0).text;
	nFoldersTotalCount=doc.getElementsByTagName('nTotalCount_SubFolder').item(0).text;
    }
    
    var foldersData=[];

    if(folders != null)
    {
	var foldersCount=folders.length;
	for (var i=0;i<foldersCount;i++) 
	{
	    var folderItem=folders.item(i);
	    
	    var folderNameTag=folderItem.getElementsByTagName('NAME');
	    var folderCountTotalTag=folderItem.getElementsByTagName('PIC_CNT');
	    var filesCountTag=folderItem.getElementsByTagName('CNT_FILES');
	    var subdirsCountTag=folderItem.getElementsByTagName('CNT_SUBDIRS');
    
	    var folderName=folderNameTag.item(0).text;
	    var folderCountTotal=folderCountTotalTag.item(0).text;
	    var filesCount=filesCountTag.item(0).text;
	    var subdirsCount=subdirsCountTag.item(0).text;
	    
   	    var rowFolderData={
		isFolder:1,
		folderName:folderName,
		folderCountTotal:folderCountTotal,
		filesCount:filesCount,
		subdirsCount:subdirsCount
	    };

	    foldersData[i]=rowFolderData;
	}
    }

    var filesData=[];

    if(files != null)
    {
	var filesCount = files.length;
	var counter=0;
	for (var j=0;j<filesCount;j++) 
	{
	    var Item=files.item(j);
	    //var fileTypeTag = Item.getElementsByTagName('EXT');
	    var fileType=Item.getElementsByTagName('EXT').item(0).text;
	    if(documentPackageEnabled=='false' && fileType!='pdf' && isSimulator!='Simulator')
	    {
		continue;
	    }
	    var filenameTag = Item.getElementsByTagName('FILENAME');
	    var doc_linkTag = Item.getElementsByTagName('DOC_LINK');
	    var displayNameTag = Item.getElementsByTagName('DISPLAYNAME');
	    var fileSizeTag = Item.getElementsByTagName('SIZE_STRING');
	    var fileLastModifyTag = Item.getElementsByTagName('DATE_STRING');
	    
	    var filename = filenameTag.item(0).text;
	    var doc_link = doc_linkTag.item(0).text;
  	    var displayName = displayNameTag.item(0).text;
	    var fileSize=fileSizeTag.item(0).text;
	    var fileLastModify=fileLastModifyTag.item(0).text;

	    var rowData={
		isFolder:0,
		filename:filename,
		doc_link:doc_link,
		displayName:displayName,
		fileSize:fileSize,
		fileLastModify:fileLastModify,
		fileType:fileType
	    };
	    
	    filesData[counter]=rowData;
	    counter++;
        }
    }

    drawData(foldersData, filesData);

    hideLoadIndicator(filesWin);
//    stopCount();
}

function pfCallBackError(xml)
{
//    Ti.API.info('pfCallBackError '+xhrBOCall.responseText);

    hideLoadIndicator(filesWin);
    //alert(L("catalog_error_alert"));

    var alertDialogBOError = Ti.UI.createAlertDialog({
        title:L("catalog_error_alert"),
        message: strLastBO_ErrorText
    });
    alertDialogBOError.show();
}

function getCatalog()
{
    var catalog = filesWin.catalogName;
    catalog=xmlEncode(catalog);

    var startIndex=rowNumberToDisplay*currentPage;
    var endIndex=rowNumberToDisplay*currentPage+rowNumberToDisplay-1;

    var strPost=
    '<?xml version="1.0" encoding="UTF-8"?><REQUEST><PARAMETERS><strAliasDirectory>'+catalog+'</strAliasDirectory>'+
    '<nFromNFiles>'+startIndex+'</nFromNFiles><nToNFiles>'+endIndex+'</nToNFiles>'+
    '<nFromNFolders>'+startIndex+'</nFromNFolders><nToNFolders>'+endIndex+'</nToNFolders>'+
    '<nSortOrder>0</nSortOrder></PARAMETERS></REQUEST>';	

//    Ti.API.info('strPost getCatalog '+strPost);

    var REST_WEB_ROOT_URL=Titanium.App.Properties.getString('REST_WEB_ROOT_URL');
    showLoadIndicator(filesWin);
    BO_Call("POST",REST_WEB_ROOT_URL+"/service/catalog/read",pfCallBackSucess,pfCallBackError,strPost);
}

getCatalog();

Ti.App.addEventListener('reloadCatalog', function()
{
    tableData=[];
    calculateFirstAndLastRow();
    getCatalog();
});

//Focus event.
filesWin.addEventListener('focus', function()
{
    //filesWin.showNavBar();
});

filesWin.addEventListener('close', function()
{
});

Ti.App.addEventListener('sendFiles_sent', function(e)
{
    if(filesWin.inFolder==1)
    {
	return;
    }

    var alertDialog = Ti.UI.createAlertDialog({
	title:L("send_files_success_dialog_title"),
	message:L("send_files_email_success_dialog_message")
    });
    alertDialog.show();
});

Ti.App.addEventListener('invitation_sent', function(e)
{
    if(filesWin.inFolder==1)
    {
	return;
    }

    var alertDialog = Ti.UI.createAlertDialog({
	title:L("invitation_email_success_dialog_title"),
	message:L("invitation_email_success_dialog_message")
    });
    alertDialog.show();
});

function beginUpdate()
{
//doTimer();
    if((lastRow==-1 && startRow==-1) || (lastRow==(tableData.length)))
    {
	return;
    }

//Ti.API.info("11 "+counter);

    tableView.removeEventListener('scroll',function(e){});
    updating = true;
    showLoadIndicator(filesWin);

    //Ti.API.info(startRow+' '+lastRow);

//Ti.API.info("12 "+counter);

    tableView.appendRow(loadingRow);
//Ti.API.info("13 "+counter);
    tableView.scrollToIndex(lastRow,{animated:true,position:Ti.UI.iPhone.TableViewScrollPosition.BOTTOM});
//Ti.API.info("14 "+counter);

    startRow=lastRow;

    if((lastRow+rowNumberToDisplay)>tableData.length)
    {
	lastRow=tableData.length;
    }
    else
    {
	lastRow=lastRow+rowNumberToDisplay;
    }


//Ti.API.info("startRow "+startRow);
//Ti.API.info("lastRow "+lastRow);
//Ti.API.info("15 "+counter);

    for (var c=startRow;c<lastRow;c++)
    {
	var rowDat=tableData[c];
	tableView.appendRow(createTableRow(rowDat),{animationStyle:Titanium.UI.iPhone.RowAnimationStyle.NONE});
    }

//Ti.API.info("16 "+counter);
    tableView.deleteRow(startRow,{animationStyle:Titanium.UI.iPhone.RowAnimationStyle.NONE});
//Ti.API.info("17 "+counter);

    tableView.scrollToIndex(startRow-1,{animated:true,position:Ti.UI.iPhone.TableViewScrollPosition.BOTTOM});

//Ti.API.info("18 "+counter);
    hideLoadIndicator(filesWin);
//Ti.API.info("19 "+counter);
    updating = false;
    tableView.addEventListener('scroll',scrollFunction);
//Ti.API.info("120 "+counter);
//stopCount();
}

function scrollFunction(e)
{
   // alert(startRow + ' ' + lastRow);

    var offset = e.contentOffset.y;
    var height = e.size.height;
    var total = offset + height;
    var theEnd = e.contentSize.height;
    var distance = theEnd - total;

    // going down is the only time we dynamically load,
    // going up we can safely ignore -- note here that
    // the values will be negative so we do the opposite
    if (distance < lastDistance)
    {
	    // adjust the % of rows scrolled before we decide to start fetching
	    var nearEnd = theEnd*1;

	    if (!updating && (total >= nearEnd))
	    {
		//Ti.API.info('update0');
		beginUpdate();
	    }
    }

    lastDistance = distance;
}

//tableView.addEventListener('scroll',scrollFunction);

tableView.addEventListener('click',function(e)
{
    var w;
    if(e.rowData.isNext)
    {
	nextPage();
    }
    else if(e.rowData.isPrevious)
    {
        previousPage();
    }
    else if(e.rowData.isFolder)
    {
	openFolder(e.rowData.catalogName,e.rowData.fileName,e.rowData.folderCountTotal,
		   filesWin.canDownload,filesWin.canUpload,filesWin.canDelete,filesWin.canInvite, filesWin.canSendEmail);
    }
    else
    {
	if(checkSupportedFileType(e.rowData.fileType))
	{
	    w = Titanium.UI.createWindow({
		url:'./file_viewer.js',
		barColor:'#111',
		previousWindows:previousWindows
	    });
	    
	    var file_link = REST_WEB_DOCS_URL+e.rowData.doc_link+'?web_session='+Titanium.App.Properties.getString('web_session');
	    w.file_link=file_link;
	    w.catalogName = e.rowData.catalogName;
	    w.fileName = e.rowData.fileName;
	    w.fileType = e.rowData.fileType;
	    //w.navBarHidden=true;
	    //w.fullscreen=true;
	    w.canDownload=filesWin.canDownload;
	    w.canUpload=filesWin.canUpload;
	    w.canDelete=filesWin.canDelete;
	    w.canInvite=filesWin.canInvite;
	    w.canSendEmail=filesWin.canSendEmail;
	    
	    Titanium.UI.currentTab.open(w);
	}
	else
	{
	    var unsupportedDialogError = Ti.UI.createAlertDialog({
		title: L('unsupporte_file_type_title'),
		message:L('unsupporte_file_type_message')
	    });
	    unsupportedDialogError.show();
	}
    }
});

/*filesWin.addEventListener('orientationchange', function(e)
{
    catalogNameLabel.top=5;
    tableView.top =30;
//    createNewCatalog(e.name);
//    Ti.API.info('create '+e.name);
});*/
