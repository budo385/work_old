Titanium.include('./load_indicator.js','./background_orientation.js','./bo_parse_error.js');

var inAppPurchaseWin = Titanium.UI.currentWindow;
Titanium.Storekit = Ti.Storekit = require('ti.storekit');

inAppPurchaseWin.titleImage = './images/everPics_TitleBar.png';
inAppPurchaseWin.backButtonTitle=L("back");

/*var connectToServerPreviousWindows = inAppPurchaseWin.connectToServerPreviousWindows;
connectToServerPreviousWindows.push(inAppPurchaseWin);

var previousWindows=[];
if(inAppPurchaseWin.bFromConnectWin==0)
{
    previousWindows = inAppPurchaseWin.previousWindows;
    previousWindows.push(inAppPurchaseWin);
}*/

/*var buttonObjectsCatalog = [
    {title:L("info"), width:50}
];

var buttonBarCatalog = Titanium.UI.createButtonBar({
    labels:buttonObjectsCatalog,
    backgroundColor:'#0F3752',
    style:Titanium.UI.iPhone.SystemButtonStyle.BAR
});

buttonBarCatalog.addEventListener('click', function(e)
{
    openCatalogInfoWin();
});

//inAppPurchaseWin.leftNavButton=logoutButton;
inAppPurchaseWin.rightNavButton=buttonBarCatalog;*/

// initialize to all modes
inAppPurchaseWin.orientationModes = [
    Titanium.UI.PORTRAIT,
    Titanium.UI.UPSIDE_PORTRAIT,
    Titanium.UI.LANDSCAPE_LEFT,
    Titanium.UI.LANDSCAPE_RIGHT
];

var tableview = Titanium.UI.createTableView({
    //data:data,
    top:0,
    left:0,
    bottom:50,
    right:0,
    rowHeight:140.00,
    separatorColor:'transparent',
    backgroundColor:'transparent'//,
    //style:Titanium.UI.iPhone.TableViewStyle.GROUPED
});

//Document package.
var row0 = Ti.UI.createTableViewRow();
row0.rightImage='./images/arrow_white.png';
var imageView0 = Ti.UI.createImageView({
    image:'./images/EverDOCsBridge128.png'
});
var view0 = Ti.UI.createView({
    height:75,
    width:75,
    top:35,
    left:10
});
view0.add(imageView0);
var titleLabel = Ti.UI.createLabel({
    height:20,
    top:15,
    right:0,
    color:'white',
    font:{fontSize:16,fontWeight:'bold',fontFamily:'Helvetica Neue'},
    left:95,
    text:L("document_package")
});
var decrLabel = Ti.UI.createLabel({
    height:105,
    top:30,
    right:5,
    color:'white',
    font:{fontSize:11,fontWeight:'bold',fontFamily:'Helvetica Neue'},
    left:95,
    text:L("in_app_row0_message")
});
row0.add(titleLabel);
row0.add(decrLabel);
row0.add(view0);

//SSL package.
var row1 = Ti.UI.createTableViewRow();
row1.rightImage='./images/arrow_white.png';
var imageView1 = Ti.UI.createImageView({
    image:'./images/EverDOCsBridge128.png'
});
var view1 = Ti.UI.createView({
    height:75,
    width:75,
    top:35,
    left:10
});
view1.add(imageView1);
var titleLabel1 = Ti.UI.createLabel({
    height:20,
    top:30,
    right:0,
    color:'white',
    font:{fontSize:16,fontWeight:'bold',fontFamily:'Helvetica Neue'},
    left:95,
    text:L("ssl_package")
});
var decrLabel1 = Ti.UI.createLabel({
    height:105,
    top:30,
    right:5,
    color:'white',
    font:{fontSize:11,fontWeight:'bold',fontFamily:'Helvetica Neue'},
    left:95,
    text:L("in_app_row1_message")
});
row1.add(titleLabel1);
row1.add(decrLabel1);
row1.add(view1);

//test package.
/*var row2 = Ti.UI.createTableViewRow();
row2.rightImage='./images/arrow_white.png';
var imageView2 = Ti.UI.createImageView({
    image:'./images/EverDOCsBridge128.png'
});
var view2 = Ti.UI.createView({
    height:75,
    width:75,
    top:35,
    left:10
});
view2.add(imageView2);
var titleLabel2 = Ti.UI.createLabel({
    height:20,
    top:30,
    right:0,
    color:'white',
    font:{fontSize:16,fontWeight:'bold',fontFamily:'Helvetica Neue'},
    left:95,
    text:L("test package")
});
var decrLabel2 = Ti.UI.createLabel({
    height:105,
    top:30,
    right:5,
    color:'white',
    font:{fontSize:11,fontWeight:'bold',fontFamily:'Helvetica Neue'},
    left:95,
    text:L("test package")
});
row2.add(titleLabel2);
row2.add(decrLabel2);
row2.add(view2);*/

tableview.addEventListener('click', function(e) {
    var product = e.row.product;
    Ti.Storekit.purchase(product, function(r)
    {
        showLoadIndicator(inAppPurchaseWin);

	if (r.state == Ti.Storekit.FAILED)
	{
	    hideLoadIndicator(inAppPurchaseWin);

	    Ti.UI.createAlertDialog({
                title: L("error"),
                message:L("purchasing_message")+' '+ r.message
            }).show();
        }
        else if (r.state == Ti.Storekit.PURCHASED || r.state == Ti.Storekit.RESTORED)
	{
	    //hideLoadIndicator(inAppPurchaseWin);

            var receipt = r.receipt;
            Ti.Storekit.verifyReceipt({
		receipt: receipt,
		//sandbox: true,
		callback:function(e)
			{
			    //alert('e.success '+e.success);
			    hideLoadIndicator(inAppPurchaseWin);
			    if (e.success)
			    {
				if (e.valid)
				{
				    //alert('product.identifier '+product.identifier);
				    if(product.identifier=='com.everDOCs.everDOCs.document')
				    {
					Ti.App.Properties.setString('DOCUMENT_PACKAGE_ENABLED','true');
					if(Ti.App.Properties.getString('SSL_PACKAGE_ENABLED')=='true')
					{
					    Ti.App.Properties.setString('PROGRAM_CODE','11000003');
					}
					else
					{
					    Ti.App.Properties.setString('PROGRAM_CODE','11000002');
					}
					//alert('11');
					//setTimeout(function(){killWindowsToLogin();},500);
				    }
				    if(product.identifier=='com.everDOCs.everDOCs.sslPackage')
				    {
					Ti.App.Properties.setString('SSL_PACKAGE_ENABLED','true');
					if(Ti.App.Properties.getString('DOCUMENT_PACKAGE_ENABLED')=='true')
					{
					    Ti.App.Properties.setString('PROGRAM_CODE','11000003');
					}
					else
					{
					    Ti.App.Properties.setString('PROGRAM_CODE','11000001');
					}
					//setTimeout(function(){killWindowsToConnectToServer();},500);
				    }
	    
				    Ti.UI.createAlertDialog({
					title:L("success"),
					message:L("receipt_verified")/*+'\n'+L("restore_dialog_message2")*/
				    }).show();
				    
				    inAppPurchaseWin.close();
				}
				else
				{
				    Ti.UI.createAlertDialog({
					title:L("failure_title"),
					message:L("failure_message")
				    }).show();
	    
				    inAppPurchaseWin.close();
				}
			    }
			    else
			    {
				Ti.UI.createAlertDialog({
				    title:L("error"),
				    message:L("verifying_receipt_message")/*+' '+e.message*/
				}).show();
				
				inAppPurchaseWin.close();
			    }
			}
	    });
        }
        else
	{
	    hideLoadIndicator(inAppPurchaseWin);
        }
    });
});

var restoreAlertDialog = Ti.UI.createAlertDialog({
    title: L("restore_dialog_title")
});

restoreAlertDialog.addEventListener('click', function() {
    inAppPurchaseWin.close();
});

var restoreCompletedTransactions = Ti.UI.createButton({
    title:L("restore_purchases_button"),
    bottom: 5, height: 40, left: 5, right: 5
});
restoreCompletedTransactions.addEventListener('click', function() {
    showLoadIndicator(inAppPurchaseWin);
    Ti.Storekit.restoreCompletedTransactions();
});

Ti.Storekit.addEventListener('restoredCompletedTransactions', function(evt)
{
    hideLoadIndicator(inAppPurchaseWin);
    if (evt.error)
    {
	alert(evt.error);
    }
    else if (evt.transactions == null || evt.transactions.length == 0)
    {
	alert(L("restore_purchases_no_trans_message"));
    }
    else
    {
	var purchasedProducts=L("restore_dialog_message");
	for (var i = 0; i < evt.transactions.length; i++)
	{
	    var t = evt.transactions[i];

	    if(t.productIdentifier=='com.everDOCs.everDOCs.document' && (t.state==Ti.Storekit.RESTORED || t.state==Ti.Storekit.PURCHASED))
	    {
		Ti.App.Properties.setString('DOCUMENT_PACKAGE_ENABLED','true');
		if(Ti.App.Properties.getString('SSL_PACKAGE_ENABLED')=='true')
		{
		    Ti.App.Properties.setString('PROGRAM_CODE','11000003');
		}
		else
		{
		    Ti.App.Properties.setString('PROGRAM_CODE','11000002');
		}
		purchasedProducts=purchasedProducts+'\n'+L("document_package");
	    }
	    if(t.productIdentifier=='com.everDOCs.everDOCs.sslPackage' && (t.state==Ti.Storekit.RESTORED || t.state==Ti.Storekit.PURCHASED))
	    {
		Ti.App.Properties.setString('SSL_PACKAGE_ENABLED','true');
		if(Ti.App.Properties.getString('DOCUMENT_PACKAGE_ENABLED')=='true')
		{
		    Ti.App.Properties.setString('PROGRAM_CODE','11000003');
		}
		else
		{
		    Ti.App.Properties.setString('PROGRAM_CODE','11000001');
		}
		purchasedProducts=purchasedProducts+'\n'+L("ssl_package");
	    }

	    /*alert({
		state: t.state,
		identifier: t.identifier,
		productIdentifier: t.productIdentifier,
		quantity: t.quantity,
		date: t.date
	    });*/
	}
	purchasedProducts=purchasedProducts+'\n'+L("restore_dialog_message1")+'\n'+L("restore_dialog_message2");
	restoreAlertDialog.message=purchasedProducts;
	restoreAlertDialog.show();
    }
});
setTimeout(function()
{
    //Can we make payments.
    showLoadIndicator(inAppPurchaseWin);
    if (!Ti.Storekit.canMakePayments)
    {
	//hideLoadIndicator(inAppPurchaseWin);
	var appDialog=Ti.UI.createAlertDialog({
	    title:L("error"),
	    message:L("in_app_unsuported_message")
	});
	
	appDialog.show();
	appDialog.addEventListener('click', function()
	{
	    inAppPurchaseWin.close();
	});
    }
    else
    {
	Ti.Storekit.requestProducts(['com.everDOCs.everDOCs.document','com.everDOCs.everDOCs.sslPackage'], function(e)
	{
	    if (e.success)
	    {
		var len=e.products.length;
		for (var i = 0; i < len; i++)
		{
		    if(e.products[i].identifier=='com.everDOCs.everDOCs.document' && Ti.App.Properties.getString('DOCUMENT_PACKAGE_ENABLED')=='false')
		    {
			row0.product=e.products[i];
			tableview.appendRow(row0);
		    }
		    
		    if(e.products[i].identifier=='com.everDOCs.everDOCs.sslPackage' && Ti.App.Properties.getString('SSL_PACKAGE_ENABLED')=='false')
		    {
			row1.product=e.products[i];
			tableview.appendRow(row1);
		    }

/*		    if(e.products[i].identifier=='TEST_PURCHASE')
		    {
			row2.product=e.products[i];
			tableview.appendRow(row2);
		    }*/
		}
		
		inAppPurchaseWin.add(tableview);
		inAppPurchaseWin.add(restoreCompletedTransactions);
		hideLoadIndicator(inAppPurchaseWin);
	    }
	    else
	    {
		hideLoadIndicator(inAppPurchaseWin);
		Ti.UI.createAlertDialog({
		    title: 'Error',
		    message: 'Getting products: ' + e.message
		}).show();
		inAppPurchaseWin.close();
		return;
	    }
	});
    }
},0);
