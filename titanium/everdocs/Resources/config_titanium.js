//Application wide variables.
function setURLVariables()
{
    var REST_WEB_ROOT_URL = Titanium.App.Properties.getString('serverURL')+'/rest';
    var ROOT_URL = Titanium.App.Properties.getString('serverURL');
    var REST_LOGOUT = REST_WEB_ROOT_URL+"/service/logout";
    var REST_LOGIN = REST_WEB_ROOT_URL+"/service/login";
    var REST_WEB_DOCS_URL = ROOT_URL+"/user_storage/";
    
    Titanium.App.Properties.setString('REST_WEB_ROOT_URL',REST_WEB_ROOT_URL);
    Titanium.App.Properties.setString('ROOT_URL',ROOT_URL);
    Titanium.App.Properties.setString('REST_LOGOUT',REST_LOGOUT);
    Titanium.App.Properties.setString('REST_LOGIN',REST_LOGIN);
    Titanium.App.Properties.setString('REST_WEB_DOCS_URL',REST_WEB_DOCS_URL);
}

setURLVariables();

//Constants.
var FFH_CONNECT_SERVER_MASTER_IP = "http://www.everxconnect.com:11000";
var FFH_CONNECT_SERVER_LIST = "http://www.everpics.com/everxconnect.txt";

var PROGRAM_VERSION_MAJOR=1;
var PROGRAM_VERSION_MINOR=0;
var PROGRAM_VERSION_REVISION=5;