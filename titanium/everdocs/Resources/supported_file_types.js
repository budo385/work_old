function checkSupportedFileType(fileType)
{
    var supportedFileTypes=[
    'doc','docx','xls','xlsx','ppt','pptx','pdf','txt','pages',
    'numbers','key','png','jpg','tiff','tif','gif','jpeg','bmp','webloc','url',
    'html','js','css','csv','php','c','cpp','h','log','ini','pps','ppsx'
    ];

    var len=supportedFileTypes.length;
    for(var i=0;i<len;i++)
    {
	if(fileType==supportedFileTypes[i])
	{
	    return true;
	}
    }
    return false;
}
