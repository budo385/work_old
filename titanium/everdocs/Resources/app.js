Titanium.include('./mem_pool.js','./bo_call_new.js');

var mainWin = Titanium.UI.createWindow({  
    backgroundColor:'#0F3752',
//    barColor:'#111',
    navBarHidden: false,
    tabBarHidden: true
});

var timeoutID=0;
var introDuration=15000;
if(Titanium.Platform.osname=='ipad')
{
    introDuration=12000;
}

var path = Titanium.Filesystem.resourcesDirectory;
mainWin.backgroundImage = './images/PooltableBlue256.png';
mainWin.barColor = '#0F3752';

var webview = Titanium.UI.createWebView();
webview.backgroundImage = './images/PooltableBlue256.png';
webview.backgroundColor='#0F3752';
var checkEmailStatusInterval=60*1000; //every 60 secs.

if(Titanium.Platform.osname=='ipad')
{
    webview.url='./html/animation_ipad/index.html';
}
else
{
    webview.url='./html/animation_iphone/index.html';
}
mainWin.fullscreen=false;

// initialize to all modes
mainWin.orientationModes = [
    Titanium.UI.PORTRAIT
];

var skipIntroButton = Titanium.UI.createButton({
	title:L("skip_intro")
});

mainWin.rightNavButton=skipIntroButton;

//App session storage.
if(Titanium.App.Properties.getBool('RELOAD_CATALOGS')==null)
{
    Titanium.App.Properties.setBool('RELOAD_CATALOGS', false);
}
if(Ti.App.Properties.getString('skipIntro')==null)
{
    Ti.App.Properties.setString('skipIntro','false');
}
if(Ti.App.Properties.getString('closePictureGrid')==null)
{
    Ti.App.Properties.setString('closePictureGrid','false');
}
Ti.App.Properties.setInt('win_orientation',Titanium.UI.PORTRAIT);

if(Ti.App.Properties.getString('PROGRAM_CODE')==null)
{
    Ti.App.Properties.setString('PROGRAM_CODE','11000000');
}

//In App Purchase.
if(Ti.App.Properties.getString('SSL_PACKAGE_ENABLED')==null || Ti.App.Properties.getString('SSL_PACKAGE_ENABLED')=='')
{
    Ti.App.Properties.setString('SSL_PACKAGE_ENABLED','false');
}
if(Ti.App.Properties.getString('DOCUMENT_PACKAGE_ENABLED')==null || Ti.App.Properties.getString('DOCUMENT_PACKAGE_ENABLED')=='')
{
    Ti.App.Properties.setString('DOCUMENT_PACKAGE_ENABLED','false');
}

// create tab group
var tabGroup = Titanium.UI.createTabGroup();

var tab1 = Titanium.UI.createTab({  
    title:'',
    window:mainWin
});

function closeFullScreen()
{
    var mem_pool = new MemoryPool();
    mem_pool.clean(webview);
    
    var win = null;
    if (Ti.Platform.name == "android")
    {
	win = Titanium.UI.createWindow({
	url:'./connect_server.js',
	navBarHidden:false,
	tabBarHidden:true
	});	
    }
    else
    {
	win = Titanium.UI.createWindow({
	url:'./connect_server.js',
	navBarHidden:false,
	tabBarHidden:true
	});
    }
	
    //Titanium.UI.currentTab.open(win,{animated:true});
    tab1.open(win,{animated:false});
}

skipIntroButton.addEventListener('click', function()
{
    if(timeoutID!=0)
    {
        clearTimeout(timeoutID);
        timeoutID=0;
    }

    closeFullScreen();
});

mainWin.addEventListener('open', function(e)
{
   mainWin.add(webview);
   if(Ti.App.Properties.getBool('skipIntro'))
   {
	closeFullScreen();
   }
   else
   {
	timeoutID=setTimeout(closeFullScreen, introDuration);
   }
});

Ti.Gesture.addEventListener('orientationchange',function(e)
{
    Ti.App.Properties.setInt('win_orientation',e.orientation);
});

var emailIDList=[];
var emailTaskID=null;
function clearEmailCheckInterval()
{
    clearInterval(emailTaskID);
    emailTaskID=null;
    emailIDList=[];
}

function checkEmailStatusSuccess(xml)
{
    var doc = xml.documentElement;
    var statusList = doc.getElementsByTagName('EmailTaskStatusList');
    var statuses = statusList.item(0).getElementsByTagName('EmailTaskStatus');

    var statusesCount=statuses.length;
    Ti.API.info("statusesCount "+statusesCount);
    for (var i=0;i<statusesCount;i++) 
    {
	var status=statuses.item(i);
	var ID=status.getElementsByTagName('ID').item(0).text;
	var Status=status.getElementsByTagName('Status').item(0).text;
	
	Ti.API.info('ID '+ID+' Status '+Status);

	if(Status=='OK' || Status=='FAILED' || Status=='NOT IN LIST')
	{
	    var indexOfID=emailIDList.indexOf(ID);
	    emailIDList.splice(indexOfID,1);
	    
	    if(Status=='FAILED')
	    {
		var alertDialogCheckMailError = Ti.UI.createAlertDialog({
		    title:L("failed_send_title"),
		    message:L("failed_send_message")
		});
		alertDialogCheckMailError.show();
	    }
	}
	
	if(emailIDList.length==0)
	{
	    clearEmailCheckInterval();
	}
    }
}

function checkEmailStatusError(xml)
{
    clearEmailCheckInterval();
}

function createIDList()
{
    var strIDList='<lstEmailTasks>';
    for(var i=0;i<emailIDList.length;i++)
    {
        var ID = emailIDList[i];
        strIDList+='<row><ID>'+ID+'</ID></row>';
    }
    strIDList+='</lstEmailTasks>';
    return strIDList;
}

function checkEmailStatusCall()
{
    var strIDList=createIDList();

    var strPost=
    '<?xml version="1.0" encoding="UTF-8"?><REQUEST><PARAMETERS>'+strIDList+'</PARAMETERS></REQUEST>';	
    var REST_WEB_ROOT_URL=Titanium.App.Properties.getString('REST_WEB_ROOT_URL');
    //Ti.API.info('check_email_status  strPost '+strPost);
    BO_Call("POST",REST_WEB_ROOT_URL+"/service/catalog/check_invite",checkEmailStatusSuccess,checkEmailStatusError,strPost);
}

function checkEmailStatus()
{
    if(emailTaskID!=null)
    {
	return;
    }
    
    emailTaskID=setInterval(checkEmailStatusCall,checkEmailStatusInterval);
}

Titanium.App.addEventListener('check_email_status', function(e)
{
    //return;
    emailIDList.push(e.emailID);
    Ti.API.info('check_email_status '+e.emailID);
    checkEmailStatus();
});

//  add tab
tabGroup.addTab(tab1);  
// open tab group
tabGroup.open();
//tabGroup.open({transition:Titanium.UI.iPhone.AnimationStyle.FLIP_FROM_LEFT});
