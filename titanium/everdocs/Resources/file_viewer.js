Titanium.include('./load_indicator.js','./bo_call_new.js','./background_orientation.js');
var fullImageViewer = Titanium.UI.currentWindow;
fullImageViewer.titleControl = null;
fullImageViewer.showNavBar();
fullImageViewer.titleImage = './images/everPics_TitleBar.png';

//Titanium.Quicklook = Ti.Quicklook = require('ti.quicklook');
//var doc=[];
//var quickView = Ti.Quicklook.createView({});
//fullImageViewer.add(quickView);

//fullImageViewer.backButtonTitle = 'Catalog Files';
//fullImageViewer.backButtonTitleImage='./images/Cobalt_Button_Left.png';
var selectedPictures=[];
selectedPictures.push(fullImageViewer.fileName);

var previousWindows = fullImageViewer.previousWindows;
previousWindows.push(fullImageViewer);

// initialize to all modes
fullImageViewer.orientationModes = [
	Titanium.UI.PORTRAIT,
	Titanium.UI.UPSIDE_PORTRAIT,
	Titanium.UI.LANDSCAPE_LEFT,
	Titanium.UI.LANDSCAPE_RIGHT
];

//Navigation bar.
var sendButton = Titanium.UI.createButton({
    title:L("send"),
    style:Titanium.UI.iPhone.SystemButtonStyle.BORDERED
});

fullImageViewer.rightNavButton=sendButton;

var webview = Ti.UI.createWebView({
	scalesPageToFit:true
});

function parseWebLinks(data,fileType)
{
	var webLink='';
	if(fileType == 'webloc')
	{
		var dict = xml.documentElement.getElementsByTagName('dict');
		var key = dict.item(0).getElementsByTagName('key').item(0).text;
		if(key=='URL')
		{
			webLink = dict.item(0).getElementsByTagName('string').item(0).text;
		}
	}
	else if(fileType == 'url')
	{
		var splitedData=data.split('\n');
		var url=splitedData[1];
		webLink=url.substring(url.indexOf('URL')+4,url.indexOf('\r'));
	}
	
	return webLink;
}

//Ti.API.info(fullImageViewer.file_link);

var xhr = Titanium.Network.createHTTPClient();
xhr.validateSecureCertificate = false;

xhr.onload = function()
{
	if(fullImageViewer.fileType== 'webloc')
	{
		webview.url=parseWebLinks(this.responseXML, fullImageViewer.fileType);
	}
	else if(fullImageViewer.fileType== 'url')
	{
		webview.url=parseWebLinks(this.responseText, fullImageViewer.fileType);
	}
	else
	{
		var tmp = Titanium.Filesystem.tempDirectory;
		var f = Titanium.Filesystem.getFile(tmp,fullImageViewer.fileName);
		f.write(this.responseData);
		webview.url = f.nativePath;
		//doc.push(f.nativePath);
		//quickView.data=doc;
	}

	hideLoadIndicator(fullImageViewer);
};

xhr.onerror = function()
{
	hideLoadIndicator(fullImageViewer);
	var alertDialogError = Ti.UI.createAlertDialog({
		title:L("file_viewer_get_error_title"),
		message:L("files_get_error_message")
//	    message: xml.documentElement.getElementsByTagName('ERROR').item(0).getElementsByTagName('ERROR_TEXT').item(0).text
	});
	alertDialogError.show();

};

// open the client
xhr.open('GET',fullImageViewer.file_link);
showLoadIndicatorDark(fullImageViewer);
xhr.send();   

fullImageViewer.add(webview);

sendButton.addEventListener('click', function()
{
	if(fullImageViewer.canDownload==0)
	{
	    var alertDialog = Ti.UI.createAlertDialog({
	    title:L("access_right_title"),
	    message: strLastBO_ErrorText
	    });
	    alertDialog.show();
	    return;
	}
	
	var w = Titanium.UI.createWindow({
	    url:'./invitation.js',
	    barColor:'#111',
	    fromSendWin:2,
	    previousWindows:previousWindows
	});

	w.selectedPictures=selectedPictures;
	w.catalogName = fullImageViewer.catalogName;
	Titanium.UI.currentTab.open(w);
});

Ti.App.addEventListener('sendFiles_sent_fromFileViewer', function(e)
{
	var alertDialog = Ti.UI.createAlertDialog({
			title:L("send_files_success_dialog_title"),
			message:L("send_files_email_success_dialog_message")
	});
	alertDialog.show();
});

webview.addEventListener('beforeload', function(e)
{
	if(fullImageViewer.fileType=='xls','xlsx')
	{
		return;
	}
	showLoadIndicatorDark(fullImageViewer);
});

webview.addEventListener('load', function(e)
{
	hideLoadIndicator(fullImageViewer);
});
