Titanium.include('./load_indicator.js','./bo_call_new.js','./background_orientation.js','get_thumb.js');

var editPictures = Titanium.UI.currentWindow;
editPictures.backButtonTitle=L("catalog");
editPictures.title =L("edit");

var previousWindows = editPictures.previousWindows;
previousWindows.push(editPictures);

var selectedPictures=[];
var imageID=0;
var errorCount=0;

var progressBar=Titanium.UI.createProgressBar({
	width:Titanium.Platform.displayCaps.platformWidth,
	min:0,
	height:70,
	bottom:0,
	value:0,
	color:'#fff',
	font:{fontSize:14, fontWeight:'bold'},
	style:Titanium.UI.iPhone.ProgressBarStyle.PLAIN,
	backgroundColor:'#0F3752',
	zIndex:10
});

editPictures.add(progressBar);

var alertDeleteDialog = Titanium.UI.createAlertDialog({
	title:L("delete"),
	message:L("delete_dialog_message"),
	buttonNames:[L("ok")],
	cancel:0
});

var deleteDialogConfirmation = Titanium.UI.createAlertDialog({
	title:L("delete"),
	buttonNames:[L("ok"),L("cancel")],
	cancel:1
});

function onDelete()
{
	if(editPictures.canDelete==0)
	{
	    var alertDialog = Ti.UI.createAlertDialog({
		title:L("access_right_title"),
		message: strLastBO_ErrorText
	    });
	    alertDialog.show();
	    return;
	}
	
	if(selectedPictures.length==0)
	{
	    alertDeleteDialog.show();
	    return;
	}
	deleteDialogConfirmation.message=L("delete_confirmation_dialog_message")+' '+ selectedPictures.length +' '+L("delete_confirmation_dialog_message_1")+'?';
	deleteDialogConfirmation.show();
}

var errorDialog = Titanium.UI.createAlertDialog(
{
	buttonNames:[L("ok")],
	cancel:0
});

errorDialog.addEventListener('click', function(e)
{
    editPictures.close();
});

var buttonObjects = [
    {title:L("delete"), width:55}
];

var buttonBar = Titanium.UI.createButtonBar({
    labels:buttonObjects,
    backgroundColor:'red',
    style:Titanium.UI.iPhone.SystemButtonStyle.BAR
});

buttonBar.addEventListener('click', function(e)
{
	onDelete();
});

editPictures.rightNavButton=buttonBar;

// initialize to some modes
editPictures.orientationModes = [
	Titanium.UI.PORTRAIT,
	Titanium.UI.UPSIDE_PORTRAIT,
	Titanium.UI.LANDSCAPE_LEFT,
	Titanium.UI.LANDSCAPE_RIGHT
];

var completeDialog = Titanium.UI.createAlertDialog({
	title:L("edit_pictures_delete_complete_dialog_title"),
	message:L("edit_pictures_delete_complete_dialog_message"),
	buttonNames:[L("ok")],
	cancel:0
});

completeDialog.addEventListener('click', function(e)
{
    editPictures.close();
    Ti.App.fireEvent('reloadCatalog');
    Ti.App.fireEvent('reloadAllCatalogs');
});

var headerLabel = Ti.UI.createLabel({
    height:20,
    top:5,
    left:0,
    right:0,
    color:'white',
    textAlign:'center',
    text:L("edit_docs_header_label")
});

editPictures.add(headerLabel);

var tableView = Titanium.UI.createTableView({
    //data:data,
    top:40,
    rowHeight:60.00,
    backgroundColor:'transparent',
    separatorColor:'white',
    style:Titanium.UI.iPhone.TableViewStyle.GROUPED
}); 

function addRemovePicNameToArray(pictureName,bAdd)
{
    var nIndex=-1;
    for(var i=0;i<selectedPictures.length;i++)
    {
        if(pictureName==selectedPictures[i])
        {
            nIndex=i;
        }
    }
    
    if(bAdd)
    {
        if(nIndex<0)
        {
            selectedPictures.push(pictureName);
        }
    }
    else
    {
        if(nIndex>=0)
        {
            var newArray = [];
            var nCount=0;
            for(var j=0;j<selectedPictures.length;j++)
            {
                if(j!=nIndex)
                {
                    newArray[nCount]=selectedPictures[j];
                    nCount++;
                }
            }
            selectedPictures=newArray;
        }
    }
}

tableView.addEventListener('click',function(e)
{
	var checkImageView = e.row.children[2];

	if(e.rowData.checked)
	{
		checkImageView.image=null;
		e.rowData.checked=0;
		addRemovePicNameToArray(e.rowData.pictureName,false);
	}
	else
	{
		checkImageView.image='./images/Checked.png';
		e.rowData.checked=1;
		addRemovePicNameToArray(e.rowData.pictureName,true);
	}
});

editPictures.add(tableView);

function pfCallBackSucess(xml)
{
	//Ti.API.info('pfCallBackSucess '+xhrBOCall.responseText);
    catalogTotalCount = xml.documentElement.getElementsByTagName('nTotalCount').item(0).text;
    files = xml.documentElement.getElementsByTagName('file');

    if(files==null)
    {
	hideLoadIndicator(editPictures);
	return;
    }

    var data = []; 
   // Ti.API.info(files.length);
    for (var i=0;i<files.length;i++) 
	{
		var filename = files.item(i).getElementsByTagName('FILENAME').item(0).text;
		var displayName = files.item(i).getElementsByTagName('DISPLAYNAME').item(0).text;
		var fileType=files.item(i).getElementsByTagName('EXT').item(0).text;
		var fileSize=files.item(i).getElementsByTagName('SIZE_STRING').item(0).text;
		var fileLastModify=files.item(i).getElementsByTagName('DATE_STRING').item(0).text;

		var thumb_link=getThumb(fileType);

		var row = Ti.UI.createTableViewRow();
		row.height=95;
		row.rightImage=null;
		row.checked=0;
		row.pictureName = filename; 
		row.catalogName = editPictures.catalogName;
		row.pictureIndex = i;
	
		var imageView = Ti.UI.createImageView({
		    image:thumb_link
		});
		    
		var view = Ti.UI.createView({
		    height:75,
		    width:75,
		    top:10,
		    left:10
		});
		
		view.add(imageView);
	
		var nameLabel = Ti.UI.createLabel({
		    height:95,
		    top:0,
		    right:30,
		    color:'white',
		    left:95
		});	
	
		var checkImageView = Ti.UI.createImageView({
		    top:0,
		    right:5,
		    width:20,
		    bottom:0
		});
	
		nameLabel.text=filename+'\n'+fileSize+'\n'+fileLastModify;
		
		row.add(view);
		row.add(nameLabel);
		row.add(checkImageView);
		data.push(row); 
	}
		
	tableView.data = data;
	tableViewLoaded=1;
	hideLoadIndicator(editPictures);
}

function pfCallBackError(xml)
{
    hideLoadIndicator(editPictures);
    var alertDialogBOError = Ti.UI.createAlertDialog({
        title:L("edit_pictures_get_catalog_error_title"),
        message: strLastBO_ErrorText
    });
    alertDialogBOError.show();
}

function getCatalog()
{
    var catalog = editPictures.catalogName;
    catalog=xmlEncode(catalog);

    //Get catalog pictures.
    var strPost=
	'<?xml version="1.0" encoding="UTF-8"?><REQUEST><PARAMETERS><strAliasDirectory>'+catalog+'</strAliasDirectory>'+
	'<nFromN>-1</nFromN><nToN>-1</nToN><nSortOrder>0</nSortOrder></PARAMETERS></REQUEST>';	
    var REST_WEB_ROOT_URL=Titanium.App.Properties.getString('REST_WEB_ROOT_URL');
    showLoadIndicator(editPictures);
	BO_Call("POST",REST_WEB_ROOT_URL+"/service/catalog/read",pfCallBackSucess,pfCallBackError,strPost); 
}

getCatalog();

function pfDeletePictureSucess(xml)
{
	//Ti.API.info('pfCallBackSucess '+xhrBOCall.responseText);
//Ti.API.info('image success');
	imageID++;
	progressBar.message=L("deleting")+' '+imageID+' '+L("of")+' '+selectedPictures.length;
	progressBar.value=imageID;
	editPictures.fireEvent('deleteNext');
}

function pfDeletePictureError(xml)
{
//Ti.API.info('image error');
	errorCount++;
	pfDeletePictureSucess(xml);
}

function deleteImage()
{
    var fileName = selectedPictures[imageID];
    
    var catalog=editPictures.catalogName;
    catalog=xmlEncode(catalog);

//Ti.API.info('image delet');
    var strPost='<?xml version="1.0" encoding="UTF-8"?><REQUEST><PARAMETERS><strAlias>'+catalog+'</strAlias>'+
    '<strFileName>'+fileName+'</strFileName></PARAMETERS></REQUEST>';
    
//Ti.API.info(strPost);
    var REST_WEB_ROOT_URL=Titanium.App.Properties.getString('REST_WEB_ROOT_URL');
    showLoadIndicator(editPictures);
    BO_Call("POST",REST_WEB_ROOT_URL+"/service/document/delete",pfDeletePictureSucess,pfDeletePictureError,strPost); 
}

editPictures.addEventListener('deleteNext', function(e)
{
	if(imageID<selectedPictures.length)
	{
		deleteImage();
	}
	else
	{
		Titanium.App.Properties.setBool('RELOAD_CATALOGS', true);
		hideLoadIndicator(editPictures);
		progressBar.hide();
		if(errorCount==0)
		{
			completeDialog.title=L("edit_pictures_delete_complete_dialog_title");
			completeDialog.message=L("edit_pictures_delete_complete_dialog_message");
			completeDialog.show();
		}
		else
		{
			completeDialog.title=L("edit_pictures_delete_complete_dialog_title");
			completeDialog.message=(selectedPictures.length-errorCount)+' '+L("of")+' '+selectedPictures.length + ' '+L("edit_pictures_delete_complete_dialog_message_suffix");
			completeDialog.show();
		}
	}
});

deleteDialogConfirmation.addEventListener('click',function(e)
{
	if(e.index==1)
	{
		return;
	}

	imageID=0;

	progressBar.max=selectedPictures.length;
	progressBar.message=L("deleting")+' '+(imageID+1)+' '+L("of")+' '+selectedPictures.length;
	progressBar.value=imageID+1;
	editPictures.fireEvent('deleteNext');
});