// this sets the background color of the master UIView (when there are no windows/tab groups on it)
Titanium.include('./connect_server_table.js','./bo_call_new.js','./load_indicator.js','./database_calls.js',
		 './register_server_calls.js','./background_orientation.js','ssl_option_dialog.js');

var connectToServerWin = Titanium.UI.currentWindow;
var path = Titanium.Filesystem.resourcesDirectory;
connectToServerWin.titleControl = null;
connectToServerWin.titleImage = './images/everPics_TitleBar.png';
connectToServerWin.barColor = '#0F3752';

var connectToServerPreviousWindows=[];

// initialize to all modes
connectToServerWin.orientationModes = [
    Titanium.UI.PORTRAIT/*,
    Titanium.UI.UPSIDE_PORTRAIT,
    Titanium.UI.LANDSCAPE_LEFT,
    Titanium.UI.LANDSCAPE_RIGHT*/
];

//Timer
var counter=0;
var t;
var timer_is_on=0;

function timedCount()
{
	counter=counter+.1;
	t=setTimeout(timedCount,100);
}

function doTimer()
{
	if (!timer_is_on)
	{
		timer_is_on=1;
		timedCount();
	}
}

function stopCount()
{
	clearTimeout(t);
	counter=0;
	timer_is_on=0;
}
//Timer

var serverID='';
var bHasInvite=false;
var inviteUsername='';
var invitePassword='';
var bHasIPPredefined=false;
var PredefinedURL='';
var PredefinedPort='';

var serversButton = Titanium.UI.createButton({
	title:L("servers")
});

serversButton.addEventListener('click', function()
{
    var win = Titanium.UI.createWindow({
	url:'./select_server.js',
	barColor:'#111',
	navBarHidden:false,
	tabBarHidden:true
    });
	
    Titanium.UI.currentTab.open(win,{animated:false});
});

var buttonObjects = [
    {title:L("website"), width:70},
    {title:L("info"), width:50}
];

var buttonBar = Titanium.UI.createButtonBar({
    labels:buttonObjects,
    backgroundColor:'#0F3752',
    style:Titanium.UI.iPhone.SystemButtonStyle.BAR
});

buttonBar.addEventListener('click', function(e)
{
    if(e.index==0)
    {
	Titanium.Platform.openURL('http://www.everdocs.com');
    }
    else if(e.index==1)
    {
	var w = Titanium.UI.createWindow({
		url:'./info.js',
		bFromIDWindow:true
	});
	Titanium.UI.currentTab.open(w);
    }
});

connectToServerWin.rightNavButton=buttonBar;
connectToServerWin.leftNavButton=serversButton;

//Get server data callback.
function serverDataSucess(xml)
{
	Ti.API.info('getServerData success '+ counter);

    var xdom   = xml.documentElement;
    var serverName = xdom.getElementsByTagName('ServerName').item(0).text;
    var emailSignature = xdom.getElementsByTagName('EmailSignature').item(0).text;
    Titanium.App.Properties.setString('EMAIL_SIGNATURE',emailSignature);
    var serverPicture64 = xdom.getElementsByTagName('ServerPicture').item(0).text;
    var serverPicture = Titanium.Utils.base64decode(serverPicture64);
    serverID=Titanium.App.Properties.getString('serverID');
    insertServer(serverID, serverName, serverPicture64);
    var jamesVersionMinor=xdom.getElementsByTagName('nMinor').item(0).text;
    var jamesVersionMajor=xdom.getElementsByTagName('nMajor').item(0).text;
    var jamesProgramRevision=xdom.getElementsByTagName('nRev').item(0).text;
    
    hideLoadIndicator(connectToServerWin);
    if(jamesVersionMajor==PROGRAM_VERSION_MAJOR && jamesVersionMinor==PROGRAM_VERSION_MINOR)
    {
	var loginWin = Titanium.UI.createWindow({
	    url:'./login.js'
	});
	loginWin.serverName = serverName;
	loginWin.serverPicture64 = serverPicture64;
	loginWin.bHasInvite=bHasInvite;
	loginWin.inviteUsername=inviteUsername;
	loginWin.invitePassword=invitePassword;
	loginWin.connectToServerPreviousWindows=connectToServerPreviousWindows;
    
	bHasInvite=false;
    
	Ti.API.info('open login.js '+ counter);
	//stopCount();
    
	Titanium.UI.currentTab.open(loginWin);
    }
    else
    {
	var alertDialogServerVersion = Ti.UI.createAlertDialog({
	    title: L("version_mismatch_title"),
	    message: L("version_mismatch_message")
	});
	alertDialogServerVersion.show();
    }

}

function serverDataError(xml)
{
    	Ti.API.info('get server error');
//Ti.API.info('REST_WEB_ROOT_URL2222 ' /*+ REST_WEB_ROOT_URL*/);
    hideLoadIndicator(connectToServerWin);
    var alertDialogServerData = Ti.UI.createAlertDialog({
        title: L("connect_server_title"),
        message: strLastBO_ErrorText
    });
    alertDialogServerData.show();
}

function getServerData()
{
	Ti.API.info('getServerData start '+ counter);

    //Get server data.
    //alert(REST_WEB_ROOT_URL);
    var REST_WEB_ROOT_URL = Titanium.App.Properties.getString('REST_WEB_ROOT_URL');
    //Ti.API.info('REST_WEB_ROOT_URL ' + REST_WEB_ROOT_URL);
    showLoadIndicator(connectToServerWin);
	Ti.API.info('get server data start');
    BO_Call('GET',REST_WEB_ROOT_URL+"/service/settings/server_data",serverDataSucess,serverDataError, "");
}

function registerCallBackSucess(xml)
{
    //Ti.API.info('connect_server.js response '+xml);
//	Ti.API.info('registerCallBackSucess '+ h);

    var xdom   = xml.documentElement;
    var code   = xdom.getElementsByTagName('STATUS_CODE').item(0).text;
    var text   = xdom.getElementsByTagName('STATUS_TEXT').item(0).text;
    var host_url    = xdom.getElementsByTagName('HOST_URL').item(0).text;
    var host_url_local= xdom.getElementsByTagName('HOST_LOCAL_IP').item(0).text;
    var host_port   = xdom.getElementsByTagName('HOST_PORT').item(0).text;
    var clientID = xdom.getElementsByTagName('CLIENT_IDENTITY').item(0).text;
    var hostSSl= xdom.getElementsByTagName('HOST_SSL').item(0).text;
    //Ti.API.info('host ssl '+hostSSl);

	var ServerURL;
	//Ti.API.info('hostSSL '+ hostSSl);
	if(hostSSl==='1')
	{
		if(Ti.App.Properties.getString('SSL_PACKAGE_ENABLED')=='false')
		{
			hideLoadIndicator(connectToServerWin);
			sslDialog.show();
			return;
		}
		
		ServerURL= "https://";
	}
	else
	{
	    ServerURL= "http://";
	}

//Ti.API.info('dss'+code);
//Ti.API.info(host_url);
//Ti.API.info(host_url_local);
//Ti.API.info(host_port);
//3-not foud ili 6-server down.
    if(code==0 || code==2)
    {
	//If no local host returned from tarantula.
	if(host_url_local=='' || host_url_local==null)
	{
	    if(bHasIPPredefined)
	    {
		ServerURL= ServerURL+PredefinedURL+":"+PredefinedPort;
	    }
	    else
	    {
		ServerURL= ServerURL+host_url+":"+host_port;
	    }
	}
	else
	{
	    if(BO_TestConnectivity(ServerURL,host_url_local,host_port))
	    {
		ServerURL= ServerURL+host_url_local+":"+host_port;
	    }
	    else
	    {
		if(bHasIPPredefined)
		{
		    if(BO_TestConnectivity(ServerURL,PredefinedURL,PredefinedPort))
		    {
			ServerURL= ServerURL+host_url_local+":"+host_port;
		    }
		    else
		    {
			//Ti.API.info('connectivity');
			hideLoadIndicator(connectToServerWin);
			var alertDialogServer = Ti.UI.createAlertDialog({
			    title:L("connect_server_title"),
			    message:L("connect_server_message")
			});
			alertDialogServer.show();
			return; 
		    }
		}
		else
		{
		    ServerURL= ServerURL+host_url+":"+host_port;
		}
	    }
	}
        
	if(bHasIPPredefined)
	{
//		Ti.API.info('3333');
//		Ti.API.info(ServerURL);
//		Ti.API.info(PredefinedURL);
//		Ti.API.info(PredefinedPort);
//			Ti.API.info(bHasIPPredefined);
//			Ti.API.info(serverID);
//			Ti.API.info(PredefinedURL);
//			Ti.API.info(PredefinedPort);
		bHasIPPredefined=false;
		var tmp= serverID+'/'+PredefinedURL+":"+PredefinedPort;
		Titanium.App.Properties.setString('serverID',tmp);
	}
	else
	{
//		Ti.API.info('4444');
//		Ti.API.info(ServerURL);
//		Ti.API.info(PredefinedURL);
//		Ti.API.info(PredefinedPort);
//			Ti.API.info('trlabrla');
		Titanium.App.Properties.setString('serverID',serverID);
	}
	Titanium.App.Properties.setString('serverURL',ServerURL);
	Titanium.App.Properties.setString('clientID',clientID);
	Titanium.App.Properties.setString('HOST_SSL',hostSSl);
        setURLVariables();
        //Ti.API.info('ServerURL');
        //Ti.API.info(ServerURL);
	hideLoadIndicator(connectToServerWin);
        getServerData();
    }
    else if(code==6)
    {
	if(bHasIPPredefined)
	{
//		Ti.API.info('222');
//		Ti.API.info(ServerURL);
//		Ti.API.info(PredefinedURL);
//		Ti.API.info(PredefinedPort);
		
		if(BO_TestConnectivity(ServerURL,PredefinedURL,PredefinedPort))
		{
//		Ti.API.info('333');
			ServerURL= ServerURL+PredefinedURL+":"+PredefinedPort;
		}
		else
		{
//		Ti.API.info('444');
		    hideLoadIndicator(connectToServerWin);
		    var alertDialogServer1 = Ti.UI.createAlertDialog({
			title:L("connect_server_title"),
			message:L("connect_server_message")
		    });
		    alertDialogServer1.show();
		    return; 
		}

		if(bHasIPPredefined)
		{
//			Ti.API.info('2111 '+bHasIPPredefined);
//			Ti.API.info('2111 '+serverID);
//			Ti.API.info('2111 '+PredefinedURL);
//			Ti.API.info('2111 '+PredefinedPort);
			var tmp= serverID+'/'+PredefinedURL+":"+PredefinedPort;
			Titanium.App.Properties.setString('serverID',tmp);
		}
		else
		{
//			Ti.API.info('trlabrla');
			Titanium.App.Properties.setString('serverID',serverID);
		}
		Titanium.App.Properties.setString('serverURL',ServerURL);
		Titanium.App.Properties.setString('clientID',clientID);
		Titanium.App.Properties.setString('HOST_SSL',hostSSl);
		setURLVariables();
//	        Ti.API.info('wwwewewwe ServerURL');
//	        Ti.API.info(ServerURL);
		hideLoadIndicator(connectToServerWin);
		getServerData();
	}
	else
	{
		hideLoadIndicator(connectToServerWin);
		var alertDialogServer2 = Ti.UI.createAlertDialog({
		    title:L("connect_routing_server_title"),
		    message:text
		});
		alertDialogServer2.show();
		return; 
	}
    }
    else
    { 
	hideLoadIndicator(connectToServerWin);
        var alertDialogServer3 = Ti.UI.createAlertDialog({
            title:L("connect_routing_server_title"),
            message:text
        });
        alertDialogServer3.show();
        return; 
    }
}

function registerCallBackError(xml)
{
	//Ti.API.info('connect server.js register call back error');
    hideLoadIndicator(connectToServerWin);
    var alertDialogServerDataCallBack = Ti.UI.createAlertDialog({
        title:L("connect_routing_server_title"),
	message:L("connect_routing_server_message")
//        message: strLastBO_ErrorText
    });
    alertDialogServerDataCallBack.show();
}

connectToServerButton.addEventListener('click', function(e)
{
    serverID='';

//doTimer();

//Ti.API.info('connect to click');

    if(textField.value == '')
    {
        alert(L("insert_id_alert"));
        return;
    }
    else
    {
        var input = textField.value;
        if(input.search("<<")!=-1 && input.search(">>")!=-1)//<<qrdzz42118//admin//>>
        {
		var startPos=input.search("<<");
		var endPos=input.search(">>");
		//Ti.API.info('parse '+input.substring(startPos+2, endPos));
		bHasInvite=true;
		input= input.substring(startPos+2, endPos);
		var array = input.split("//");
		serverID = array[0];
		inviteUsername=array[1];
		invitePassword=array[2];
        }
        else if(input.match("/") && input.match(":"))//wjdos82202/192.168.101.23:55000
	{
		bHasIPPredefined=true;
		var array1 = input.split("/");
		serverID = array1[0];
		var IPandPort=array1[1];
		var IPArray=IPandPort.split(":");
		PredefinedURL=IPArray[0];
		PredefinedPort=IPArray[1];
        }
        else
        {
		serverID = textField.value;
        }
    }

    //Do registration from here - if possible.
    showLoadIndicator(connectToServerWin);
//    Ti.API.info("registermasterserver");
    BO_RegisterMasterServerID(serverID, registerCallBackSucess, registerCallBackError);
});

connectToServerWin.add(scrollview);

function setSelectedServerID()
{
//    Ti.API.info('setSelectedServerID');
    serverID=Titanium.App.Properties.getString('serverID');
    textField.value=serverID;
}

Ti.App.addEventListener('serverIdChanged', function()
{
    setTimeout(setSelectedServerID, 300);
});

connectToServerWin.addEventListener('open', function(e)
{
//    Ti.API.info('connect open');
    serverID=Titanium.App.Properties.getString('serverID');
//    Ti.API.info(serverID);
    textField.value=serverID;
    /*if(serverID!=null)
    {
	if(serverID!='')
	{
	    connectToServerButton.fireEvent('click');
	}
    }*/
});

textField.addEventListener('change', function(e)
{
    Titanium.App.Properties.removeProperty('serverID');
    Titanium.App.Properties.removeProperty('Username');
    Titanium.App.Properties.removeProperty('Password');
});
