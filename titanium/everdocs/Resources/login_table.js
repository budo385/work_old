//Layout for ipad and iphonoids.
var scrollview = Ti.UI.createScrollView({
    contentWidth:'auto',
    contentHeight:'auto',
    top:0,
    bottom:0,
    left:0,
    right:0,
    showVerticalScrollIndicator:true,
    showHorizontalScrollIndicator:false
});

//Server image.
var headerImage = Titanium.UI.createImageView({
	image:'./images/EPL_Logo_M.png'
});

var backgroundView = Titanium.UI.createView({
	backgroundImage:'./images/EntryArea_M.png',
	left:0,
	right:0
});

//Server name label.
var serverNameLabel = Titanium.UI.createLabel({
	text:L("server_name_label"),
	color:'white',
	font:{fontWeight:'bold',fontSize:22, fontFamily:'Helvetica Neue'},
	textAlign:'center'
});

//Server image.
var serverImage = Titanium.UI.createImageView({
});

var loginButton = Titanium.UI.createButton({
	title:L("login_button_title"),
	left:5,
	right:5,
	height:40,
	top:5,
	font:{fontWeight:'bold',fontSize:20, fontFamily:'Helvetica Neue'},
	backgroundImage:'./images/greenButton.png'
});

var usernameTextField = Titanium.UI.createTextField({
	hintText:L("username_textfield_hint"),
	keyboardType:Titanium.UI.KEYBOARD_DEFAULT,
	returnKeyType:Titanium.UI.RETURNKEY_DEFAULT,
	autocapitalization:Titanium.UI.TEXT_AUTOCAPITALIZATION_NONE,
	borderStyle:Titanium.UI.INPUT_BORDERSTYLE_ROUNDED
});

var passwordTextField = Titanium.UI.createTextField({
	hintText:L("password_textfield_hint")  ,
	keyboardType:Titanium.UI.KEYBOARD_DEFAULT,
	returnKeyType:Titanium.UI.RETURNKEY_DEFAULT,
	borderStyle:Titanium.UI.INPUT_BORDERSTYLE_ROUNDED,
	backgroundColor:'transparent',
	passwordMask:true
});

if(Titanium.Platform.osname=='ipad')
{
	headerImage.top=90;
	headerImage.width=300;
	headerImage.height=300;

	backgroundView.top=535;
	backgroundView.height=360;

	serverNameLabel.top=25;
	serverNameLabel.height=30;

	serverImage.top=80;
	serverImage.width=75;
	serverImage.height=75;
	
	usernameTextField.top=180;
	usernameTextField.height=30;
	usernameTextField.left=10;
	usernameTextField.right=10;

	passwordTextField.top=220;
	passwordTextField.height=30;
	passwordTextField.left=10;
	passwordTextField.right=10;

	loginButton.top=260;
	loginButton.left=10;
	loginButton.right=10;
	loginButton.height=40;

	scrollview.add(headerImage);
	backgroundView.add(serverNameLabel);
	backgroundView.add(serverImage);
}
else
{
	backgroundView.top=250;
	backgroundView.height=150;

	serverNameLabel.top=60;
	serverNameLabel.height=30;

	serverImage.top=120;
	serverImage.width=75;
	serverImage.height=75;
	
	usernameTextField.top=25;
	usernameTextField.height=30;
	usernameTextField.left=5;
	usernameTextField.right=5;

	passwordTextField.top=60;
	passwordTextField.height=30;
	passwordTextField.left=5;
	passwordTextField.right=5;

	loginButton.top=95;
	loginButton.left=5;
	loginButton.right=5;
	loginButton.height=40;

	scrollview.add(serverNameLabel);
	scrollview.add(serverImage);
}

backgroundView.add(usernameTextField);
backgroundView.add(passwordTextField);
backgroundView.add(loginButton);

scrollview.add(backgroundView);
