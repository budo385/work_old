
//--------------------------------------------------------------------
//				BUSINESS CALL INTERFACE
//--------------------------------------------------------------------
Titanium.include('./bo_encode_decode.js','./bo_call_before_send_call.js','./bo_parse_error.js');

//BO Call callback.
var functCallBackSucess_BOCall=null;
var functCallBackError_BOCall=null;
var FFH_CONNECT_SERVER_TIMEOUT = 20*1000; //20 sec
var xhrBOCall=null;

function BO_Call_success()
{
	//Ti.API.info('bo_call_success '+xhrBOCall.responseText)
	functCallBackSucess_BOCall(xhrBOCall.responseXML);
}

function BO_Call_error(e)
{
	//Ti.API.info('bo_call_error '+xhrBOCall.responseText)
	BO_ParseError(xhrBOCall,e.error);
	functCallBackError_BOCall(xhrBOCall.responseXML);
}

function BO_Call(strHttpMethod,strURL,pfCallBackSucess,pfCallBackError,dataToSend)
{
	functCallBackSucess_BOCall=null;
	functCallBackError_BOCall=null;
	xhrBOCall=null;
	
	xhrBOCall=Titanium.Network.createHTTPClient();
	functCallBackSucess_BOCall=pfCallBackSucess;
	functCallBackError_BOCall=pfCallBackError;
	xhrBOCall.onerror = BO_Call_error;
	xhrBOCall.onload = BO_Call_success;
	xhrBOCall.validateSecureCertificate = false;
	xhrBOCall.timeout=FFH_CONNECT_SERVER_TIMEOUT;
	xhrBOCall.open(strHttpMethod,strURL,true);
	BO_Call_beforeSend(xhrBOCall);
	xhrBOCall.send(dataToSend);
	/*var xhr=Titanium.Network.createHTTPClient();
	xhrBOCall=xhr;
	functCallBackSucess_BOCall=pfCallBackSucess;
	functCallBackError_BOCall=pfCallBackError;
	xhr.onerror = BO_Call_error;
	xhr.onload = BO_Call_success;
	xhr.validateSecureCertificate = false;
	xhr.timeout=FFH_CONNECT_SERVER_TIMEOUT;
	xhr.open(strHttpMethod,strURL,true);
	BO_Call_beforeSend(xhr);
	xhr.send(dataToSend);*/
}
