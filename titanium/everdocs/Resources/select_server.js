Titanium.include('./database_calls.js','./background_orientation.js');

var selectServerWin=Titanium.UI.currentWindow;
selectServerWin.title=L("servers");
selectServerWin.backButtonTitle=L("back");

// initialize to some modes
selectServerWin.orientationModes = [
    Titanium.UI.PORTRAIT,
    Titanium.UI.UPSIDE_PORTRAIT,
    Titanium.UI.LANDSCAPE_LEFT,
    Titanium.UI.LANDSCAPE_RIGHT
];

var deleteButton = Titanium.UI.createButton({
    title:L("delete"),
    style:Titanium.UI.iPhone.SystemButtonStyle.BORDERED
});

selectServerWin.rightNavButton=deleteButton;

//------------------
//Table view.
//------------------
//Add items.
var selectServerLabel = Titanium.UI.createLabel({
	text:L("select_visited_server_label"),
	font:{fontWeight:'bold', fontSize:18/*, fontFamily:'Helvetica Neue'*/},
	left:10,
	height:30,
	top:10,
	color:'white',
	textAlign:'left'});

var headerRow = Titanium.UI.createView();
headerRow.height=40;
headerRow.add(selectServerLabel);

var footerRow = Titanium.UI.createView();
footerRow.height=90;

//Upper table view.
var tableView = Titanium.UI.createTableView({
    //data:data,
    top:0,
    rowHeight:60.00,
    headerView:headerRow,
//    headerTitle:'Choose a Known Server to Visit:',
    footerView:footerRow,
    separatorColor:'white',
    backgroundColor:'transparent',
    style:Titanium.UI.iPhone.TableViewStyle.GROUPED
});

deleteButton.addEventListener('click', function()
{
    tableView.editing = true;
    selectServerWin.rightNavButton=null;
});

//Focus event.
selectServerWin.addEventListener('focus', function()
{
    //Get server list.
    var data = [];
    var serverList;
    serverList = getServers();
    var i = 0;
    while (serverList.isValidRow())
    {
        var row = Ti.UI.createTableViewRow();
        row.height=95;
        var nameLabel = Ti.UI.createLabel({
        height:45,
       	color:'white',
	top:0,
        left:95
        });	
        var idLabel = Ti.UI.createLabel({
        height:40,
        top:45,
       	color:'white',
        left:95
        });
        var imageView = Ti.UI.createImageView({
        height:75,
        width:75,
        top:10,
       	color:'white',
        left:10
        });
        
        //Row views.
        nameLabel.text = serverList.fieldByName('slst_name');
        idLabel.text = serverList.fieldByName('slst_server_id');
        var image64 = serverList.fieldByName('slst_image');
        if(image64.length == 0)
        {
            imageView.image = './images/user75.png';
        }
        else
        {
            imageView.image = Titanium.Utils.base64decode(image64);
        }
        
        //Row data.
        row.serverName = serverList.fieldByName('slst_name');
        row.serverID = serverList.fieldByName('slst_server_id');
        row.serverRowID = serverList.fieldByName('slst_id');
        
        row.add(nameLabel);
        row.add(idLabel);
        row.add(imageView);
        data.push(row);
        serverList.next();
        i++;
    }
    
    serverList.close();
    tableView.data = data;
});

tableView.addEventListener('delete',function(e)
{
    var serverRowID = e.rowData.serverRowID;
    deleteServer(serverRowID);
});

tableView.addEventListener('click', function(e)
{
    Titanium.App.Properties.setString('serverID',e.rowData.serverID);
    Ti.App.fireEvent('serverIdChanged');
    selectServerWin.close();
});

selectServerWin.add(tableView);