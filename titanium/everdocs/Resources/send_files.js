Titanium.include('./load_indicator.js','./bo_call_new.js','./background_orientation.js','get_thumb.js');

var sendFilesWin = Titanium.UI.currentWindow;
sendFilesWin.backButtonTitle=L("catalog");
sendFilesWin.title =L("send");

var previousWindows = sendFilesWin.previousWindows;
previousWindows.push(sendFilesWin);

var documentPackageEnabled=Ti.App.Properties.getString('DOCUMENT_PACKAGE_ENABLED');
var isSimulator=Titanium.Platform.model;

var selectedPictures=[];
var imageID=0;
var errorCount=0;
var rowNumberToDisplay=100;
var thumbSize=75;
var tableViewRowHeight=95;
var currentPage=0;
var nFilesTotalCount=0;
var nFoldersTotalCount=0;
var next=false;
var previous=false;
var tableData=[];

var nextRow = Ti.UI.createTableViewRow();
var nextRowimageView = Ti.UI.createImageView({
    image:'./images/MoreDown.png'
});
var nextRowView = Ti.UI.createView({
    height:thumbSize,
    width:thumbSize,
    top:10,
    left:10
});
nextRowView.add(nextRowimageView);
var nextRowNameLabel = Ti.UI.createLabel({
    height:tableViewRowHeight,
    //color:'white',
    top:0,
    text:L("show_next"),
    left:tableViewRowHeight
});
nextRow.add(nextRowView);
nextRow.add(nextRowNameLabel);
nextRow.backgroundColor='#DEE1E3';
nextRow.isNext = true;

var previousRow = Ti.UI.createTableViewRow();
var previousRowimageView = Ti.UI.createImageView({
    image:'./images/MoreUp.png'
});
var previousRowView = Ti.UI.createView({
    height:thumbSize,
    width:thumbSize,
    top:10,
    left:10
});
previousRowView.add(previousRowimageView);
var previousRowNameLabel = Ti.UI.createLabel({
    height:tableViewRowHeight,
    //color:'white',
    top:0,
    text:L("show_previous"),
    left:tableViewRowHeight
});
previousRow.add(previousRowView);
previousRow.add(previousRowNameLabel);
previousRow.backgroundColor='#DEE1E3';
previousRow.isPrevious = true;

var progressBar=Titanium.UI.createProgressBar({
	width:Titanium.Platform.displayCaps.platformWidth,
	min:0,
	height:70,
	bottom:0,
	value:0,
	color:'#fff',
	font:{fontSize:14, fontWeight:'bold'},
	style:Titanium.UI.iPhone.ProgressBarStyle.PLAIN,
	backgroundColor:'#0F3752',
	zIndex:10
});

sendFilesWin.add(progressBar);

var alertSendDialog = Titanium.UI.createAlertDialog({
	title:L("send"),
	message:L("delete_dialog_message"),
	buttonNames:[L("ok")],
	cancel:0
});

function onSend()
{
	if(sendFilesWin.canDownload==0)
	{
	    var alertDialog = Ti.UI.createAlertDialog({
		title:L("access_right_title"),
		message: strLastBO_ErrorText
	    });
	    alertDialog.show();
	    return;
	}
	
	if(selectedPictures.length==0)
	{
	    alertSendDialog.show();
	    return;
	}
	
    var w = Titanium.UI.createWindow({
        url:'./invitation.js',
        barColor:'#111',
	fromSendWin:1,
	previousWindows:previousWindows
    });

	Ti.API.info('Pass doc list size:' + selectedPictures.length);
	
	w.selectedPictures=selectedPictures;
    w.catalogName = sendFilesWin.catalogName;
    Titanium.UI.currentTab.open(w);
}

var buttonObjects = [
    {title:L("send"), width:55}
];

var buttonBar = Titanium.UI.createButtonBar({
    labels:buttonObjects,
    backgroundColor:'#0F3752',
    style:Titanium.UI.iPhone.SystemButtonStyle.BAR
});

buttonBar.addEventListener('click', function(e)
{
	onSend();
});

sendFilesWin.rightNavButton=buttonBar;

// initialize to some modes
sendFilesWin.orientationModes = [
	Titanium.UI.PORTRAIT,
	Titanium.UI.UPSIDE_PORTRAIT,
	Titanium.UI.LANDSCAPE_LEFT,
	Titanium.UI.LANDSCAPE_RIGHT
];

var completeDialog = Titanium.UI.createAlertDialog({
	title:L("edit_pictures_delete_complete_dialog_title"),
	message:L("edit_pictures_delete_complete_dialog_message"),
	buttonNames:[L("ok")],
	cancel:0
});

completeDialog.addEventListener('click', function(e)
{
    sendFilesWin.close();
});

var headerLabel = Ti.UI.createLabel({
    height:20,
    top:5,
    left:0,
    right:0,
    color:'white',
    textAlign:'center',
    text:L("edit_docs_header_label")
});

sendFilesWin.add(headerLabel);

var tableView = Titanium.UI.createTableView({
    //data:data,
    top:40,
    rowHeight:60.00,
    rowHeight:tableViewRowHeight,
    backgroundColor:'transparent',
    separatorColor:'white'
}); 

function addRemovePicNameToArray(pictureName,bAdd)
{
    var nIndex=-1;
    for(var i=0;i<selectedPictures.length;i++)
    {
        if(pictureName==selectedPictures[i])
        {
            nIndex=i;
        }
    }
    
    if(bAdd)
    {
        if(nIndex<0)
        {
            selectedPictures.push(pictureName);
        }
    }
    else
    {
        if(nIndex>=0)
        {
            var newArray = [];
            var nCount=0;
            for(var j=0;j<selectedPictures.length;j++)
            {
                if(j!=nIndex)
                {
                    newArray[nCount]=selectedPictures[j];
                    nCount++;
                }
            }
            selectedPictures=newArray;
        }
    }
}

sendFilesWin.add(tableView);

function closethis()
{
//    Ti.UI.currentTab.close(pictures,{animated:true});
    setTimeout(function(){Ti.UI.currentTab.close(sendFilesWin,{animated:true});}, nCloseWinTimeout);
}

function createFileRow(filename,doc_link,displayName,fileSize,fileLastModify,fileType)
{
    var thumb_link=getThumb(fileType);
    
    var row = Ti.UI.createTableViewRow({
//	backgroundImage:'./images/PTB_TableElement.jpg',
//	selectedBackgroundImage:'./images/PTB_TableElement_Sel.png',
	//rightImage:'./images/arrow_white.png',
	pictureName:filename,
	displayName:displayName,
	catalogName:sendFilesWin.catalogName,
	doc_link:doc_link,
	thumb_link:thumb_link,
	fileType:fileType,
	isFolder:false,
	checked:0
    });
    
    //var row = Ti.UI.createTableViewRow({title:displayName});

    var imageView = Ti.UI.createImageView({
    image:thumb_link
    });
    
	var checkImageView = Ti.UI.createImageView({
	    top:0,
	    right:5,
	    width:20,
	    bottom:0
	});

    view = Ti.UI.createView({
    height:thumbSize,
    width:thumbSize,
    top:10,
    left:10
    });

    view.add(imageView);

    var nameLabel = Ti.UI.createLabel({
	height:tableViewRowHeight,
	color:'white',
	top:0,
	right:25,
	text:displayName+'\n'+fileSize+'\n'+fileLastModify,
	//text:'test',
	left:tableViewRowHeight
    });

    row.add(view);
    row.add(checkImageView);
    row.add(nameLabel);
    
    return row;
}

function createFolderRow(folderName,folderCountTotal,filesCount,subdirsCount)
{
    var rowFolder = Ti.UI.createTableViewRow();
//    rowFolder.backgroundImage='./images/PTB_TableElement.jpg';
//    rowFolder.selectedBackgroundImage='./images/PTB_TableElement_Sel.png';
    //rowFolder.rightImage='./images/arrow_white.png';
    rowFolder.fileName = folderName; 
    rowFolder.catalogName = filesWin.catalogName;
    rowFolder.isFolder = true;
    rowFolder.folderCountTotal=folderCountTotal;
    
    var imageView = Ti.UI.createImageView({
	image:'./images/Icon_Folder75.png'
    });
    
    var view = Ti.UI.createView({
    height:thumbSize,
    width:thumbSize,
    top:10,
    left:10
    });
    
    view.add(imageView);
    
    var folderCountLabel='';
    if(folderCountTotal>0)
    {
	folderCountLabel=" ("+folderCountTotal+")";
    }

    var nameLabel = Ti.UI.createLabel({
    height:tableViewRowHeight,
    top:0,
    color:'white',
    left:tableViewRowHeight,
    text:folderName+folderCountLabel
    //text:folderName+" ("+folderCountTotal+" docs, "+subdirsCount+" subdirs, "+filesCount+" files)"
    });	

    rowFolder.add(view);
    rowFolder.add(nameLabel);
    
    return rowFolder;
}

function createTableRow(rowDat)
{
    var row;
    if(rowDat.isFolder==1)
    {
	row=createFolderRow(rowDat.folderName,rowDat.folderCountTotal,rowDat.filesCount,rowDat.subdirsCount);
    }
    else
    {
	row=createFileRow(rowDat.pictureName,rowDat.doc_link,rowDat.displayName,
	rowDat.fileSize,rowDat.fileLastModify,rowDat.fileType);
    }
    
    return row;
}

function nextPage()
{
    next=true;
    currentPage++;
    getCatalog();
}

function previousPage()
{
    previous=true;
    currentPage--;
    getCatalog();
}

function drawData(folderData, filesData)
{
    tableData=[];

Ti.API.info('nCount folder '+nCount);

    var nCount=filesData.length;
Ti.API.info('nCount files '+nCount);
    for(var j=0;j<nCount;j++)
    {
	tableData[j]=createTableRow(filesData[j]);
    }
    
    var totalPages=Math.ceil((parseInt(nFilesTotalCount)+parseInt(nFoldersTotalCount)) / rowNumberToDisplay);

    if((totalPages-1)>currentPage)
    {
	tableData.push(nextRow);
    }
    if(currentPage>0)
    {
	tableData.unshift(previousRow);
    }

    tableView.data=tableData;

    if(next)
    {
	tableView.scrollToIndex(0,{animated:true,position:Ti.UI.iPhone.TableViewScrollPosition.TOP});
    }
    if(previous)
    {
	tableView.scrollToIndex(tableData.length-1,{animated:true,position:Ti.UI.iPhone.TableViewScrollPosition.BOTTOM});
    }

    next=false;
    previous=false;
}

function pfCallBackSucess(xml)
{
//	Ti.API.info('send_files.js response '+xhrBOCall.responseText);

    if(xml.documentElement.getElementsByTagName('ERROR')!=null || xml.documentElement.getElementsByTagName('nTotalCount')==null)
    {
	hideLoadIndicator(filesWin);
	var alertDialogError = Ti.UI.createAlertDialog({
	    title:L("files_get_error_title"),
	    message:L("files_get_error_message")
	});
	alertDialogError.show();

	closethis();
	return;
    }
	
    var doc = xml.documentElement;
    var files = doc.getElementsByTagName('file');

    if(currentPage==0)
    {
	nFilesTotalCount=doc.getElementsByTagName('nTotalCount').item(0).text;
	nFoldersTotalCount=0;
    }
    
    var foldersData=[];
    var filesData=[];

    if(files != null)
    {
	var filesCount = files.length;
	for (var j=0;j<filesCount;j++) 
	{
	    var Item=files.item(j);
	    var fileTypeTag = Item.getElementsByTagName('EXT');
	    var fileType=fileTypeTag.item(0).text;
	    if(documentPackageEnabled=='false' && fileType!='pdf' && isSimulator!='Simulator')
	    {
		continue;
	    }
	    var filenameTag = Item.getElementsByTagName('FILENAME');
	    var doc_linkTag = Item.getElementsByTagName('DOC_LINK');
	    var displayNameTag = Item.getElementsByTagName('DISPLAYNAME');
	    var fileSizeTag = Item.getElementsByTagName('SIZE_STRING');
	    var fileLastModifyTag = Item.getElementsByTagName('DATE_STRING');
	    
	    var filename = filenameTag.item(0).text;
	    var doc_link = doc_linkTag.item(0).text;
  	    var displayName = displayNameTag.item(0).text;
	    var fileSize=fileSizeTag.item(0).text;
	    var fileLastModify=fileLastModifyTag.item(0).text;

	    var rowData={
		isFolder:0,
		pictureName:filename,
		doc_link:doc_link,
		displayName:displayName,
		fileSize:fileSize,
		fileLastModify:fileLastModify,
		fileType:fileType,
		checked:0
	    };
	    
	    filesData[j]=rowData;
        }
    }

    drawData(foldersData, filesData);

   // Ti.API.info(files.length);
   /* for (var i=0;i<files.length;i++) 
	{
		var filename = files.item(i).getElementsByTagName('FILENAME').item(0).text;
		var displayName = files.item(i).getElementsByTagName('DISPLAYNAME').item(0).text;
		var fileType=files.item(i).getElementsByTagName('EXT').item(0).text;
		var fileSize=files.item(i).getElementsByTagName('SIZE_STRING').item(0).text;
		var fileLastModify=files.item(i).getElementsByTagName('DATE_STRING').item(0).text;

		var thumb_link=getThumb(fileType);

		var row = Ti.UI.createTableViewRow();
		row.height=95;
		row.rightImage=null;
		row.checked=0;
		row.pictureName = filename; 
		row.catalogName = sendFilesWin.catalogName;
		row.pictureIndex = i;
	
		var imageView = Ti.UI.createImageView({
		    image:thumb_link
		});
		    
		var view = Ti.UI.createView({
		    height:75,
		    width:75,
		    top:10,
		    left:10
		});
		
		view.add(imageView);
	
		var nameLabel = Ti.UI.createLabel({
		    height:95,
		    top:0,
		    right:30,
		    color:'white',
		    left:95
		});	
	
		var checkImageView = Ti.UI.createImageView({
		    top:0,
		    right:5,
		    width:20,
		    bottom:0
		});
	
		nameLabel.text=displayName+'\n'+fileSize+'\n'+fileLastModify;
		
		row.add(view);
		row.add(nameLabel);
		row.add(checkImageView);
		data.push(row); 
	}
		
	tableView.data = data;
	tableViewLoaded=1;*/
	hideLoadIndicator(sendFilesWin);
}

function pfCallBackError(xml)
{
    hideLoadIndicator(sendFilesWin);
    var alertDialogBOError = Ti.UI.createAlertDialog({
        title:L("edit_pictures_get_catalog_error_title"),
        message: strLastBO_ErrorText
    });
    alertDialogBOError.show();
}

function getCatalog()
{
    var catalog = sendFilesWin.catalogName;
    catalog=xmlEncode(catalog);

    var startIndex=rowNumberToDisplay*currentPage;
    var endIndex=rowNumberToDisplay*currentPage+rowNumberToDisplay-1;

    var strPost=
    '<?xml version="1.0" encoding="UTF-8"?><REQUEST><PARAMETERS><strAliasDirectory>'+catalog+'</strAliasDirectory>'+
    '<nFromNFiles>'+startIndex+'</nFromNFiles><nToNFiles>'+endIndex+'</nToNFiles>'+
    '<nFromNFolders>-1</nFromNFolders><nToNFolders>-1</nToNFolders>'+
    '<nSortOrder>0</nSortOrder></PARAMETERS></REQUEST>';	
    var REST_WEB_ROOT_URL=Titanium.App.Properties.getString('REST_WEB_ROOT_URL');
    showLoadIndicator(sendFilesWin);
	BO_Call("POST",REST_WEB_ROOT_URL+"/service/catalog/read",pfCallBackSucess,pfCallBackError,strPost); 
}

getCatalog();

function closeInvitationSuccess()
{
	sendFilesWin.close();
}

Ti.App.addEventListener('close_invitation', function(e)
{
    setTimeout(closeInvitationSuccess, nCloseWinTimeout);
});

tableView.addEventListener('click',function(e)
{
	var checkImageView = e.row.children[1];

//	Ti.API.info('checkImageView '+checkImageView.image);

	if(e.rowData.isNext)
	{
	    nextPage();
	}
	else if(e.rowData.isPrevious)
	{
	    previousPage();
	}
	else if(e.rowData.checked)
	{
		checkImageView.image=null;
		e.rowData.checked=0;
//		Ti.API.info('checked to unckecked e.rowData.pictureName '+e.rowData.pictureName);
		addRemovePicNameToArray(e.rowData.pictureName,false);
	}
	else
	{
		checkImageView.image='./images/Checked.png';
		e.rowData.checked=1;
//		Ti.API.info('unckecked to checked e.rowData.pictureName '+e.rowData.pictureName);
		addRemovePicNameToArray(e.rowData.pictureName,true);
	}
});
