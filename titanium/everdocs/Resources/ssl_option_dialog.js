var sslOptionsDialogOpts = {
	options:[L("buy_now"),L("cancel")],
	destructive:1,
	cancel:1,
	title:L("ssl_buy_title")
};

var sslDialog = Titanium.UI.createOptionDialog(sslOptionsDialogOpts);

sslDialog.addEventListener('click',function(e)
{
	if(e.index==0)
	{
	    var w = Titanium.UI.createWindow({
		    url:'./in_app_purchase.js',
		    bFromConnectWin:1,
		    connectToServerPreviousWindows:connectToServerPreviousWindows
		    //previousWindows:previousWindows
		    //tabBarHidden:false
	    });
	  //  w.open({modal:true/*,modalTransitionStyle:style,modalStyle:presentation,navBarHidden:true*/});
	   Titanium.UI.currentTab.open(w);
	}
	else
	{
	    return;
	}
});
