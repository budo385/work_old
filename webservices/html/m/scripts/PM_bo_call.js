//--------------------------------------------------------------------
//				BUSINESS CALL INTERFACE
//--------------------------------------------------------------------
//All functions are async calls
//Return status is stored in variables: nLastBO_ErrorCode & strLastBO_ErrorText
//Return data is pass to the call back function. call back func must check errorcode to determine if call was OK
//nLastBO_ErrorCode == 0 : call ok, !=0 : error detected. nLastBO_ErrorCode == 1000 -> login failed.

//FUNCTION INTERFACE:
//1. BO_Call(strHttpMethod,strURL,pfCallBack,dataToSend);  
//2. BO_Login(strURL,user,pass,pfCallBack);



var nLastBO_ErrorCode=0;
var strLastBO_ErrorText='';

var functCallBack_Login=null;
var functCallBack_BOCall=null;


//--------------------------------------------------------------------
//				BUSINESS CALL HANDLER
//--------------------------------------------------------------------
// Makes async BO call, calls pfCallBack function when finish with returned data
// nLastBO_ErrorCode and strLastBO_ErrorText contain last error: if nLastBO_ErrorCode <> 0 then error
// Automatically uses prviously stored username/password for re-authentication, if Login failed nLastBO_ErrorCode=1000



//strHttpMethod - GET or other HTTP method
//strURL 		- relative URL to REST_WEB_ROOT_URL
//pfCallBack 	- function called when call is finished, function must accept data string result and test nLastBO_ErrorCode to see if call succeed
//data 			- request xml body, leave empty if no request body needed

function BO_Call(strHttpMethod,strURL,pfCallBack,dataToSend) 
{
	functCallBack_BOCall=pfCallBack; 
	strURL=REST_WEB_ROOT_URL+strURL;
	
	if(dataToSend.length>0)
	{
		
	 $.ajax({		   
		   type: strHttpMethod,
		   url: strURL,
		   dataType: "text",
		   processData: 0,
		   data: dataToSend,
		   //cache: true,
   		   success: BO_Call_success,
		   error: BO_Call_error
		   // complete: function(xhr, status) {alert(xhr.responseText);} -> display raw XML
		});	
	
	}
	else
	{
	  $.ajax({		   
		   type: strHttpMethod,
		   url: strURL,
		   dataType: "text",
		   //cache: true,
		   success: BO_Call_success,
		   error: BO_Call_error
		});
	}
}

function BO_Call_success(data, textStatus) 
{
	functCallBack_BOCall(data);
}


function BO_Call_error(xhr, textStatus, errorThrown) 
{
	BO_ParseError(xhr,textStatus);
	functCallBack_BOCall(textStatus);
}





//--------------------------------------------------------------------
//				LOGIN HANDLER
//--------------------------------------------------------------------
// Logins on the server, makes async call, calls pfCallBack function when finish
// nLastBO_ErrorCode and strLastBO_ErrorText contain last error: if nLastBO_ErrorCode <> 0 then error
// if Login failed nLastBO_ErrorCode=1000
// pfCallBack receives http body data if any


//user		 	- username
//pass 			- password
//pfCallBack 	- function called when call is finished, function must accept data string result and test nLastBO_ErrorCode to see if call succeed, if nLastBO_ErrorCode=1000 -> login failed

function BO_Login(user,pass,pfCallBack,progCode,progVer,clientID,platform) 
{
	deleteCookie('web_session');
	
	var nUTCtimezoneOffsetMinutes=GetTimeZoneOffsetUTC(); //B.T. get time zone from UTC

	var strXMLRequest=BO_GenerateLoginRequest(user,pass,progCode,progVer,clientID,platform,nUTCtimezoneOffsetMinutes);
	
	functCallBack_Login=pfCallBack;
		
	 $.ajax({		   
		   type: "POST",
		   url: REST_LOGIN,
		   processData: 0,
		   dataType: "xml",
		   data: strXMLRequest,
		  // cache: true,
		   success: BO_OnLogin_success,
		   error: BO_OnLogin_error
		});
		
}

function BO_IsLogged() 
{
	var strSession=getCookie('web_session');
	if(strSession.length==0)
		return false;
	else
		return true;
}

function BO_LogOut() 
{
	var strSession=getCookie('web_session');
	var strXMLRequest=BO_GenerateLogoutRequest(strSession);
	
	 $.ajax({		   
	   type: "POST",
	   url: REST_LOGOUT,
	   processData: 0,
	   dataType: "xml",
	 //  cache: true,
	   data: strXMLRequest
	});
		
	deleteCookie('web_session');
}


function BO_OnLogin_success(data, textStatus) 
{

	var strSession=BO_ParseLoginResponse(data);
	
	if(data.length==0)
	{
		nLastBO_ErrorCode=1000;
		strLastBO_ErrorText="Login failed! Invalid username or password!";
	}
	else
	{
		nLastBO_ErrorCode=0;
		strLastBO_ErrorText='';
	}

	setCookie('web_session', strSession, COOKIE_EXPIRE_MIN);
	
	if (functCallBack_Login!=null)
		functCallBack_Login(data);
		
}


function BO_OnLogin_error(xhr, textStatus, errorThrown) 
{
	BO_ParseError(xhr,textStatus);
	deleteCookie('web_session');
	
	if (functCallBack_Login!=null)
		functCallBack_Login(textStatus);
}


function BO_GenerateLoginRequest(user,pass,progCode,progVer,clientID,platform,nUTCtimezoneOffsetMinutes)
{
	var exdate=new Date();
	var strClientNonce=user+":"+exdate.toLocaleTimeString();
	strClientNonce=SHA256(strClientNonce);
	
	var bytePassHash=user+":"+pass;
	bytePassHash=SHA256(bytePassHash);
	var authToken=user+":"+bytePassHash+":"+strClientNonce;
	authToken=SHA256(authToken);
	
	
	var strBody ="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
	strBody +="\n\r";
	strBody +="<REQUEST>";
	strBody +="<PARAMETERS>";

	strBody +="<strOldSession></strOldSession>";
	strBody +="<strUserName>"+user+"</strUserName>";
	strBody +="<strAuthToken>"+authToken+"</strAuthToken>";
	strBody +="<strClientNonce>"+strClientNonce+"</strClientNonce>";
	strBody +="<nProgCode>"+progCode+"</nProgCode>";
	strBody +="<strProgVer>"+progVer+"</strProgVer>";
	strBody +="<strClientID>"+clientID+"</strClientID>";
	strBody +="<strPlatform>"+platform+"</strPlatform>";
	strBody +="<nUTCtimezoneOffsetMinutes>"+nUTCtimezoneOffsetMinutes+"</nUTCtimezoneOffsetMinutes>";

	strBody +="</PARAMETERS>";
	strBody +="</REQUEST>";
	
	return strBody;
}


function BO_ParseError(xhr,textStatus) 
{
	
	if(xhr.status == 0 && xhr.responseText.length == 0)
	{
		nLastBO_ErrorCode=1;	
		strLastBO_ErrorText="Server Connection Timeout. Please log on again!";
		return;
	}


	if(xhr.status == 401)
	{
		nLastBO_ErrorCode=1000;
		strLastBO_ErrorText="Login failed! Invalid username or password!";
	}
	else
	{
		if(BO_ParseErrorBody(xhr.responseText)==0)
		{
			nLastBO_ErrorCode=1;	//general error: can be timeout or something:
			
			if(textStatus.length!=0)
				strLastBO_ErrorText=textStatus;
			else
				strLastBO_ErrorText=xhr.responseText.substr(0,1000); //only first 1000 chars
				
			if(strLastBO_ErrorText.length == 0)
				strLastBO_ErrorText="Unknown Error";
		}
		
		//alert("no mans land");
	}
}

function BO_ParseErrorBody(xml_body) 
{
	if(xml_body.length == 0)
		return 0;
	nLastBO_ErrorCode=$(xml_body).find("error_code").text();
	strLastBO_ErrorText=$(xml_body).find("error_text").text();
	if (nLastBO_ErrorCode>0)
		return 1;
	else
		return 0;
}

function BO_ParseLoginResponse(xml_body) 
{

	if(xml_body.length == 0)
		return "";
		
	var nContactID = xmlDecode($(xml_body).find("Ret_nContactID").text());
	var nPersonID = xmlDecode($(xml_body).find("Ret_nPersonID").text());
	
	setCookie('nContactID',nContactID);
	setCookie('nPersonID',nPersonID);
			
	return xmlDecode($(xml_body).find("Session").text());
}




function BO_GenerateLogoutRequest(strSessionID)
{
	var strBody ="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
	strBody +="\n\r";
	strBody +="<REQUEST>";
	strBody +="<PARAMETERS>";

	strBody +="<strSessionID>"+strSessionID+"</strSessionID>";

	strBody +="</PARAMETERS>";
	strBody +="</REQUEST>";
	
	return strBody;
}

function GetTimeZoneOffsetUTC()
{
	var d = new Date();
	var n = d.getTimezoneOffset();
	return n;
}


//--------------------------------------------------------------------
//				XML DECODER:
//--------------------------------------------------------------------

//------------------BASE64 EN/DECODER------------------------------------
//use functions from base64.js:
//$.base64Encode("I'm Persian.");
//$.base64Decode("SSdtIFBlcnNpYW4u");


//------------------STRING EN/DECODER------------------------------------

var AMP = '&';
var rawEntity = [ "&",      "<",    ">",     "\'",     "\""];
var xmlEntity = [ "&amp;", "&lt;", "&gt;",  "&apos;", "&quot;"];

//xml- string from server to decode
//returns decoded string
function xmlDecode(xml)
{
	// check if no AMP there is no need to decode
	var pos=xml.indexOf(AMP);
	if(pos<0) return xml;

	var byteDecoded=xml;

	// iterate through list backwards: its very important to pars &amp last
	for (var iEntity=4; iEntity>=0; iEntity--)
		byteDecoded.replace(xmlEntity[iEntity],rawEntity[iEntity]);

	return byteDecoded;
}

//raw- string to encode for sending to the server
//returns encoded string
function xmlEncode(raw)
{

	var strEncoded=raw;

	// iterate through list forwad: its very important to encode &amp first
	if(strEncoded.indexOf("&")>=0)strEncoded.replace("&","&amp;");
	if(strEncoded.indexOf('<')>=0)strEncoded.replace("<","&lt;");
	if(strEncoded.indexOf(">")>=0)strEncoded.replace(">","&gt;");
	if(strEncoded.indexOf("\'")>=0)	strEncoded.replace("\'","&apos;");
	if(strEncoded.indexOf("\"")>=0)	strEncoded.replace("\"","&quot;");

	//	for (uint iEntity=2; iEntity<5; ++iEntity)
	//		strEncoded.replace(rawEntity[iEntity],xmlEntity[iEntity]);

	return strEncoded;
}


//------------------DATE EN/DECODER------------------------------------

//dateString in YYYY-MM-DDTHH:MM:SSZ

function dateTimeDecode(dateString) 
{
	if(dateString.length === 0) 
	{
		return "";
	}
	

	var nT_Idx=dateString.indexOf("T");
	if(nT_Idx===-1) 
	{
	   return "";
	}
	   
	var strDatePart=dateString.substr(0,nT_Idx);
	var strDatePartArray = strDatePart.split("-");
	if(strDatePartArray.length!=3) 
	{
		return "";
	}
	
	var strTimePart=dateString.substr(nT_Idx+1,8);
	var strTimePartArray = strTimePart.split(":");
	
	if(strTimePartArray.length!=3) 
	{ 
		return "";
	}

	var d =  new Date();
	

	d.setUTCFullYear(strDatePartArray[0], strDatePartArray[1]-1, strDatePartArray[2]);
	d.setUTCHours(strTimePartArray[0], strTimePartArray[1], strTimePartArray[2],0);
			
	return d;
	
}


//-----------------------STORAGE OPTIONS----------------------------

function setItem(c_name,value)
{
	//sessionStorage.setItem(c_name,value);
	setSessionkey(c_name,value); 
}

function getItem(c_name)
{
	return sessionStorage.getItem(c_name);
}


function setCookie(c_name,value,expire_min)
{
	var exdate=new Date();
	if(expire_min!=null)
	{
		exdate.setMinutes(exdate.getMinutes()+expire_min);
	}
	document.cookie=c_name+ "=" +escape(value)+
	((expire_min==null) ? "" : ";expires="+exdate.toUTCString())+';path=/'; 
}

function getCookie(c_name)
{
if (document.cookie.length>0)
  {
  c_start=document.cookie.indexOf(c_name + "=");
  if (c_start!=-1)
    {
    c_start=c_start + c_name.length+1;
    c_end=document.cookie.indexOf(";",c_start);
    if (c_end==-1) c_end=document.cookie.length;
    return unescape(document.cookie.substring(c_start,c_end));
    }
  }
return "";
}


function deleteCookie(c_name) 
{
	document.cookie=c_name+ "= ;expires=Thu, 01-Jan-1970 00:00:01 GMT";
}