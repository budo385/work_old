// JQM CONFIG:
$(document).bind("mobileinit", function()
{
	   $.mobile.ajaxEnabled = false;
       $.mobile.linkBindingEnabled = false;
       $.mobile.hashListeningEnabled = false;
   	   $.mobile.pushStateEnabled = false;
});

/*
******************************************************************************
                              CONFIG
******************************************************************************
*/

// REST Config:
var protocol 			= document.location.protocol;
var hostname			= document.location.hostname;
var port 				= document.location.port;						
var ROOT_URL 			= protocol+'//'+hostname+":"+port;
var REST_WEB_ROOT_URL 	= ROOT_URL+'/rest';
var REST_WEB_DOCS_URL 	= ROOT_URL+"/user_storage/";
var REST_LOGIN 			= REST_WEB_ROOT_URL+"/server/login";
var REST_LOGOUT 		= REST_WEB_ROOT_URL+"/server/logout";
var COOKIE_EXPIRE_MIN	= 20;

// Global constants:
var nDocListMaxRows 	= 50;
var strFooter 			= '<div id="footer"><a href="http://www.sokrates.ch/">Powered by <strong>SOKRATES<sup>&reg;</sup><em>Bridge</em></strong></a></div>';

// Session/Local Storage Patch:
function setSessionkey(key, val){
	sessionStorage.removeItem(key);
	sessionStorage.setItem(key, val);
} 
function setLocalkey(key, val){
	localStorage.removeItem(key);
	localStorage.setItem(key, val);
}
// Register Current Url in History Array:
function nav_registerURL(){
	var strURL = sessionStorage.nav_current_url;
	if (typeof strURL === "undefined") {		// pm: unfinished
		sessionStorage.nav_current_url = '';
		strURL = sessionStorage.nav_current_url;
	}
	var nIdx=strURL.indexOf(document.URL);
	if (nIdx>0){		 //strip all pages after me if exsist...(occur when user clicks on back)
		strURL=strURL.substr(0,nIdx-1); //strip last ";"
	}
	if (nIdx==0){
		strURL='';
	}
	if (strURL.length>0){
		strURL += ";";
	}
	strURL += document.URL;
	sessionStorage.nav_current_url = strURL;
}
function nav_clear() {
	sessionStorage.nav_current_url = '';
}
//get last url from history and removes it from array (back navigation)
function nav_getLastURL(){
	var urls= sessionStorage.nav_current_url.split(";");
	var nLength=urls.length;
	if (nLength>1){
		var lastURL=urls[nLength-2]; //last-1 is current we need last -2
		
		urls.pop();
		urls.pop();
		if (urls.length > 0)
			sessionStorage.nav_current_url = urls.join(";");
		else
			sessionStorage.nav_current_url = '';
		return lastURL;
	}
	else{
		return document.referrer;
	}
}
// get last part of the last url, readonly, ex: filename.html
function nav_getLastURL_lastpart(){
	var urls=sessionStorage.nav_current_url.split(";");
	var nLength=urls.length;
	var lastUrl;
	if (nLength>1){
		lastUrl=urls[nLength-2];	
	}	
	else{
		lastUrl=document.referrer;
	}
	lastUrl=lastUrl.substring(lastUrl.lastIndexOf("/") + 1);
	var k = lastUrl.indexOf("?");
	if(k!=-1){
		lastUrl=lastUrl.substring(0,k);
	}
	return lastUrl;
}
// Convert size in bytes to human readable format
function bytesToSize(bytes, precision){  
	  var kilobyte = 1024;
	  var megabyte = kilobyte * 1024;
	  var gigabyte = megabyte * 1024;
	  var terabyte = gigabyte * 1024;
	  if ((bytes >= 0) && (bytes < kilobyte)) {
		  return bytes + ' B';
	  } else if ((bytes >= kilobyte) && (bytes < megabyte)) {
		  return (bytes / kilobyte).toFixed(precision) + ' KB';
   
	  } else if ((bytes >= megabyte) && (bytes < gigabyte)) {
		  return (bytes / megabyte).toFixed(precision) + ' MB';
   
	  } else if ((bytes >= gigabyte) && (bytes < terabyte)) {
		  return (bytes / gigabyte).toFixed(precision) + ' GB';
   
	  } else if (bytes >= terabyte) {
		  return (bytes / terabyte).toFixed(precision) + ' TB';
	  } 
	  else {
		  return bytes + ' B';
	  }
}
// get current filename 
function getFileName() {
	// get the full url
	var url = document.location.href;
	// removes the anchor at the end, if any
	url = url.substring(0, (url.indexOf("#") == -1) ? url.length : url.indexOf("#"));
	// removes the query after the file name, if any
	url = url.substring(0, (url.indexOf("?") == -1) ? url.length : url.indexOf("?"));
	// removes everything before the last slash in the path
	url = url.substring(url.lastIndexOf("/") + 1, url.length);
	
return url;
}

function getDocumentType(ext){
	var strExtensionImg = '';
	switch (ext){
		case "doc":
			strExtensionImg = '<img alt="doc" src="images/MyWord.png" class="ui-li-icon"/>';
			break;
		case "docx":
			strExtensionImg = '<img alt="docx" src="images/MyWord.png" class="ui-li-icon"/>';
			break;
		case "xls":
			strExtensionImg = '<img alt="xls" src="images/MyExcel.png" class="ui-li-icon"/>';
			break;
		case "xlsx":
			strExtensionImg = '<img alt="xlsx" src="images/MyExcel.png" class="ui-li-icon"/>';
			break;		
		case "ppt":
			strExtensionImg = '<img alt="ppt" src="images/MyPowerPoint.png" class="ui-li-icon"/>';
			break;
		case "pptx":
			strExtensionImg = '<img alt="pptx" src="images/MyPowerPoint.png" class="ui-li-icon"/>';
			break;
		case "pdf":
			strExtensionImg = '<img alt="pdf" src="images/pdf.png" class="ui-li-icon"/>';
			break;
		case "txt":
			strExtensionImg = '<img alt="txt" src="images/MyNotepad.png" class="ui-li-icon"/>';
			break;
		case "zip":
			strExtensionImg = '<img alt="zip" src="images/MyZip.png" class="ui-li-icon"/>';
			break;		
		case "jpg":
			strExtensionImg = '<img alt="jpg" src="images/jpg.png" class="ui-li-icon"/>';
			break;
		case "tiff":
			strExtensionImg = '<img alt="tiff" src="images/jpg.png" class="ui-li-icon"/>';
			break;
		case "png":
			strExtensionImg = '<img alt="png" src="images/jpg.png" class="ui-li-icon"/>';
			break;
		default : strExtensionImg = '<img alt="file" src="images/file.png" class="ui-li-icon"/>'; 
	}	
	return (strExtensionImg);
}

function checkErrorCode()
{
	if(nLastBO_ErrorCode!=0) 
	{										// an error occured
	  alert(strLastBO_ErrorText); 			// alert this general error
	  if(nLastBO_ErrorCode==1000){ 			// it's a login error
		  window.location="index.html";
	  }
	  else {
		  window.location=nav_getLastURL();	// another error -> go back one page
	  }
	  return;
    }	
}

function isBiggerScreen(){
	if ($(document).height()>=500 && $(document).width()>=500){
		return true;
	}
	else{
		return false;	
	}
}

/*function getParameterByName(name){
  name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
  var regexS = "[\\?&]" + name + "=([^&#]*)";
  var regex = new RegExp(regexS);
  var results = regex.exec(window.location.search);
  if(results == null)
    return "";
  else
    return decodeURIComponent(results[1].replace(/\+/g, " "));
}*/


/*window.applicationCache.addEventListener('checking',logEvent,false);
window.applicationCache.addEventListener('noupdate',logEvent,false);
window.applicationCache.addEventListener('downloading',logEvent,false);
window.applicationCache.addEventListener('cached',logEvent,false);
window.applicationCache.addEventListener('updateready',logEvent,false);
window.applicationCache.addEventListener('obsolete',logEvent,false);
window.applicationCache.addEventListener('error',logEvent,false);   
function logEvent(event) {
   console.log(event.type);
}
*/
/* 
============================================================================================
This script is used by contactfind.html and provides utf8 encoding needed for the find 
feature. Generally, when POST is used, utf8 encoding is needed when sending to the appserver.
Also the search_types we can choose from in the drop-down box are listened in the below array
============================================================================================
 */


var Utf8={encode:function(string){string=string.replace(/\r\n/g,"\n");var utftext="";for(var n=0;n<string.length;n++){var c=string.charCodeAt(n);if (c<128) {utftext+=String.fromCharCode(c);}else if((c>127)&&(c<2048)){utftext+=String.fromCharCode((c>>6)|192);utftext+=String.fromCharCode((c&63)|128);}else{utftext+=String.fromCharCode((c>>12)|224);utftext+=String.fromCharCode(((c>>6)&63)|128);utftext+=String.fromCharCode((c&63)|128);}}return utftext;},decode:function(utftext){var string="";var i=0;var c=c1=c2=0;while(i<utftext.length){c=utftext.charCodeAt(i);if(c<128){string+=String.fromCharCode(c);i++;}else if((c>191)&&(c<224)){c2=utftext.charCodeAt(i+1);string+=String.fromCharCode(((c&31)<<6)|(c2&63));i+=2;}else{c2=utftext.charCodeAt(i+1);c3=utftext.charCodeAt(i+2);string+=String.fromCharCode(((c&15)<<12)|((c2&63)<<6)|(c3&63));i+=3;}}return string;}}	

var arrValues=["BCNT_LASTNAME","BCNT_FIRSTNAME","BCNT_DEPARTMENTNAME","BCNT_BIRTHDAY","BCNT_ORGANIZATIONNAME", "BCNT_PROFESSION","BCNT_FUNCTION","BCMP_FULLNUMBER","BCMI_ADDRESS","BCME_ADDRESS","BCMA_FORMATEDADDRESS","BCNT_LOCATION",];

