// All swipe events are bound here to prevent multiple loading

$(document).bind("pageinit", function()  // pageinit is called for *every* opened page exactly once
{
	// swipe left on Page A: open Page B
	$("#page_a").swipeleft(function() {
		$.mobile.changePage("b.html", {reverse: false, transition: "slide"});
	});	

	// swipe right on Page B: go back to Page A
	$("#page_b").swiperight(function() {
		$.mobile.changePage("a.html", {reverse: true, transition: "slide"});		
	});

	$("#page_a #btnOpenB").click(function(event){
		$.mobile.changePage("b.html", {reverse: false, transition: "slide"});		
	});
	
	$("#page_b #btnOpenA").click(function(event){
		$.mobile.changePage("a.html", {reverse: true, transition: "slide"});	
	});
	
});

// Pages opened by links without a click handler can only be built *after* the page is visible (example Page A):

$("#page_a").live('pageshow', function(event) {
	logme("Show Page","A");
	build_A();	// Content of page A is built *after* the page is displayed -> the construction process is visible (flickering)
});

$("#page_b").live('pageshow', function(event) {
	logme("Show Page","B");	
	// Page b is normally built before displaying.
	// This is a fallback if it has not yet been constructed:
	// This happens if Button A is pressed, because pre-display events are not fired then.
	if ($('#page_b #not_initialized').length > 0) {    // Only if the element already exists in the DOM
		build_B();
	};
});


// Enter initialization code for each page here. It will be executed
// when the page opens:

$(document).bind("pagebeforechange", function(e, data ) {
	
	// We only want to handle changePage() calls where the caller is
	// asking us to load a page by URL.
	if ( typeof data.toPage === "string" ) {


		// Page a
		if (data.toPage=="a.html") {
			logme("Build Page","a");
		};
		
		// Page b
		if (data.toPage=="b.html") {
		// Content of page B is built *before* the page is displayed -> the construction process is not visible
		// This event is not fired the first time a page is created, but for subsequent usages of the page.
		// The first time creation is done in the "pagebeforecreate" event.
			logme("Build Page","b");

			// Get the time to determine if this is a (false) double event:
			now = new Date();

			// Read the last time this event has been fired for this page:
			lastBuild_B = sessionStorage.getItem("lastBuild_B");
			
			// Construct the contents only if it has not been constructed in the last 1000ms (prevent false double events!):
			if (now.getTime() - lastBuild_B > 1000) {  
				if ($('#cntB').length > 0) {    // Only if the element already exists in the DOM
					// Construct the contents using AJAX:
					build_B();
					
					// Remember the actual time:
					sessionStorage.setItem("lastBuild_B",now.getTime())	;					//alert("B injected");
					
					logme("B injected in Build Page");
				};
				
			} else {
				logme("B skipped in Build Page to avoid double initialization - time difference "+ (now.getTime() - lastBuild_B) + "<1000ms");
			};
		};
	};
	
}); 

$(document).delegate("#page_b","pagebeforecreate", function() {
	// This event is used to create the contents of a new dynamic page for the first time.
	// Subsequent constructions take place in the "pagebeforechange" event.
	// The code here is only called once, so it can not be used at every time the page is displayed
	logme("Build Page Delegated","b");
	// Construct the contents using AJAX:
	build_B();
}); 

// The pagechange event is used to mark the page as built. 
// If not called, it can be rebuilt on certain browsers.
$(document).bind("pagechange", function(e, data ) {

	// We only want to handle changePage() calls where the caller is
	// asking us to load a page by URL.
	if ( typeof data.toPage === "string" ) {

		// Page a
		if (data.toPage=="a.html") {
			// initialize the contents here
			logme("Page Built","a");
		};
		
		// Page b
		if (data.toPage=="b.html") {
			// initialize the contents here
			logme("Page Built","b");
		};

	};
	
}); 

// Disable/Enable JQM settings and global or per page initializations:
$(document).bind("mobileinit", function()
{
   	   // $.mobile.pushStateEnabled = true; 
	   sessionStorage.setItem("lastBuild_B",0);
});


// Utilities:

function logme(name,value) {
	var strPrint;
	console.log(''+name+' ='+value);
};


// Page building functions:

function build_A() {
	 
	//$("#cntA").empty();
	// Dynamically build the page contents with AJAX:
	$("#cntA").load('a_cnt.html #cntA');
	$("#cntB").html('<p id="not_initialized"></p>');  // emtpy the now hidden page
	logme("Page A has been constructed");
};

function build_B() {
	//$("#cntB").empty();
	// Dynamically build the page contents with AJAX:
	$("#cntB").load('b_cnt.html #cntB');
	$("#cntA").html('<p id="not_initialized"></p>');  // emtpy the now hidden page
	logme("Page B has been constructed");
};



