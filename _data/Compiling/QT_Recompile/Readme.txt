QT_4.8.0 recompile (19.12.2011): 
--------------------------------------------
1) libpng.lib save from old QT version and copy in lib (no vcproj to recompile)
2) re-arrange SQL module (just copy, see details in ..\New_SQL_interface
3) Modify C:\Qt\4.7.0\src\network\ssl\ssl.pri: add line: win32:INCLUDEPATH += C:\openssl\include (use ssl_patched_4.6.0.pri)
4) Install PERL before configure....
5) copy config.bat in main Qt directory, where configure.exe is and execute
6) rebuild all SQL modules:
ibase, sqlite, oracle, mysql and jpg2000 imageformat



QT_4.7.2 recompile (26.04.2011): same as 4.7.0 procedure: rebuild all SQL modules, reconfigure before and libpng, that's it


QT_4.7.0 recompile (15.10.2010): As from 4.6.2: there is no need to recompile whole QT: just SQL interface
1. open project C:\Qt\4.7.0\src\3rdparty\libpng\projects\visualc61\libpng.sln and compile LIB as release and debug, copy to QT/lib directory
2. if build does not succeed, then set include path: $(QTDIR)\include\Qt, and build only libpng
3. Re-arrange QTSQL moduel, see instruction at ..\New_SQL_interface
4. Modify C:\Qt\4.7.0\src\network\ssl\ssl.pri: add line: win32:INCLUDEPATH += C:\openssl\include (use ssl_patched_4.6.0.pri)
5. copy go_all.bat in main Qt directory, where configure.exe is and execute


QT_4.6.0 recompile (02.12.2009);
1. open project C:\Qt\4.3.2\src\3rdparty\libpng\projects\visualc71\libpng.sln and compile LIB as release and debug, copy to QT/lib directory
2. if build does not succeed, then set include path: $(QTDIR)\include\Qt, and build only libpng
3. Re-arrange QTSQL moduel, see instruction at ..\New_SQL_interface
4. Modify C:\Qt\4.6.0\src\network\ssl\ssl.pri: add line: win32:INCLUDEPATH += C:\openssl\include (use ssl_patched_4.6.0.pri)
5. copy go_all.bat in main Qt directory, where configure.exe is and execute


Use: clean_all for clean up


----------------------------------

To recompile QT:
1. open project C:\Qt\4.3.2\src\3rdparty\libpng\projects\visualc71\libpng.sln and compile LIB as release and debug, copy to QT/lib directory
2. if build does not succeed, then set include path: $(QTDIR)\include\Qt, and build only libpng
2. copy config.bat and build.bat in main Qt directory, where configure.exe is
2. execute config.bat, change settings if wish
3. execute build.bat

OPENSSL:
1. replace or manually add include path for SSL (if using -openssl) into C:\Qt\4.3.3\src\network\network.pro:
	
	win32:INCLUDEPATH += C:\openssl\include
	win32:LIBS += lC:\OpenSSL\lib\VC\static -lssleay32MD -llibeay32MD

WARNING: change path accordingly to local installation

NOTE: every module can be separately build: look for vcproj in /src directory after configure
