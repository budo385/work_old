2013-08-06:
1. 16018 is last QT 4.8.2 version, so revert to these changes on SOKRATES_XP
2. I failed to rebuild QT 5.1.0 msvc2010 on VS2010+sp1 on XP so I do following:

-----------------------------
QT how to rebuild:
-----------------------------
1. Install VS2010 prof+sp1
2. Install AcivePerl, Python, Ruby, win_flex_bison-latest.zip, bison-2.4.1-setup.exe,gperf-3.0.1.exe
   Install flex into C:\flex and bison and gperf into C:\GnuWin32
3. Install QT (win32-2010)+at-add-in, store license file in specified directory, see instructions: http://qt-project.org/doc/qt-5.0/qtdoc/install-win.html
4. Rebuild OpenSSL: create C:\OpenSSL dir and C:\OpenSSLSource, extract latest openssl source code here.
   Copy all bat files from /OpenSSL_buildVS2010, execute configure, build and CopyFiles batch files.
5. Create QTDIR envvar (c:\Qt\5.0.2\5.0.2\msvc2010) and set QTDIR/bin in path
6. Go to c:\Qt\5.0.2\5.0.2\Src\qtbase\, copy config.bat and build.bat
7. Change SQL source files, see NEW_SQL_interaface and change openssl.pri. See details in SokratesXP/_data/Compiling
7.1. In c:\Qt\5.0.2\5.0.2\Src\qtbase\network\ssl\openssl.pri just add line: win32:INCLUDEPATH += C:\openssl\include
7.2. And for new SQL module you must change total 6 files: 
qsql_ibase.cpp/h, qsqlquery.cpp/h and qsqlresult.cpp/h
8. install DirectX 7.0, see Appendix A below
9. run config
10. Copy qconfig.h from c:\Qt\Qt5.1.0\5.1.0\msvc2010\include\QtCore\ to c:\Qt\Qt5.1.0\5.1.0\Src\qtbase\src\corelib\global\
11. go to c:\Qt\Qt5.1.0\5.1.0\Src\qtbase\src\network\ and create vcproj file: qmake -tp vc network.pro
12. same for sql module: c:\Qt\Qt5.1.0\5.1.0\Src\qtbase\src\sql\ qmake -tp vc sql.pro
13. Compile debug/release for both network and sql. Lib and dll's are placed in the: c:\Qt\Qt5.1.0\5.1.0\Src\qtbase\lib\
14. Recompile Firebird driver: copy Build_Firebird from c:\SokratesXP\_data\Compiling\New_SQL_interface\ to the c:\Qt\Qt5.1.0\5.1.0\Src\qtbase\src\plugins\sqldrivers\ibase\
	and run, in c:\Qt\Qt5.1.0\5.1.0\Src\qtbase\plugins\sqldrivers\ there should be new qsqlibase.dll
	
15. That's it. QT is ready for use: just assemble new headers and recompiled dll's to specified directories and that's it.
16. Note: with new QT version run moc.bat inside c:\SokratesXP\lib\trans\trans\ and in c:\SokratesXP\lib\gui_core\gui_core\property_browser\
17. Change qftp.cpp on line 1220: path to the qobject_p.h and in the qhttp.cpp
18. Refresh zlib.lib and zlibd.lib in the C:\SokratesXP\lib\zlib\zlib\ (Latest is from zlib1.2.3 build)
19. Also refresh SSL libs in C:\SokratesXP\lib\openssl\win32_build\

Note about can not add qtguiclass:
- to enable project to use QtAddIn to add QT class just insert this line at bottom of the vcprojx files:
before:       <UserProperties MocDir=".\GeneratedFiles\$(ConfigurationName)" QtVersion="4.5.1" QtVersion_x0020_Win32="4.8.0" UicDir=".\GeneratedFiles" />
replace with: <UserProperties UicDir=".\GeneratedFiles" MocDir=".\GeneratedFiles\$(ConfigurationName)" MocOptions="" RccDir=".\GeneratedFiles" lupdateOnBuild="0" lupdateOptions="" lreleaseOptions="" Qt5Version_x0020_Win32="msvc2010" />





/* OLD--------------------------- */



1. Install VS2010 prof+sp1
2. Install AcivePerl, Python, Ruby, win_flex_bison-latest.zip, bison-2.4.1-setup.exe,gperf-3.0.1.exe
   Install flex into C:\flex and bison and gperf into C:\GnuWin32
3. Install QT (win32-2010)+at-add-in, store license file in specified directory, see instructions: http://qt-project.org/doc/qt-5.0/qtdoc/install-win.html
4. Rebuild OpenSSL: create C:\OpenSSL dir and C:\OpenSSLSource, extract latest openssl source code here.
   Copy all bat files from /OpenSSL_buildVS2010, execute configure, build and CopyFiles batch files.
5. Create QTDIR envvar (c:\Qt\5.0.2\5.0.2\msvc2010) and set QTDIR/bin in path
6. Go to c:\Qt\5.0.2\5.0.2\Src\qtbase\, copy config.bat and build.bat
7. Change SQL source files, see NEW_SQL_interaface and change openssl.pri. See details in SokratesXP/_data/Compiling
7.1. In c:\Qt\5.0.2\5.0.2\Src\qtbase\network\ssl\openssl.pri just add line: win32:INCLUDEPATH += C:\openssl\include
7.2. And for new SQL module you must change total 6 files: 
qsql_ibase.cpp/h, qsqlquery.cpp/h and qsqlresult.cpp/h

8.0) See Appendix A for directx build
8.1) Build will fail on opengl but it will rebuild network and sql module!!!!!
9. Recompile Firebird is not possible so just use their original version as is->you will need their VC80 CRT files inside main dir...
/*9. Recompile Firebird: can not doit for msvc10: use *.zip deployment that dependes on mscrt80.dll's: copy these files along with mscrt10 files. Rebuild rfunc.dll ->firebird/udf and copy mscrt10.dlls*/
10. To recompil rfunc.dll set FIREBIRD_PATH to Firebird installation: e.g. C:\Firebird
10. Then recompile ibase slqdriver

Appendix A
Note: prior to building QT: 
-------------------------------------
1) Install DirectSDK: http://www.microsoft.com/en-us/download/details.aspx?id=6812
2) before install remove sp1 VS2010 redist package then reinstall it from:c:\Program Files\Microsoft SDKs\Windows\v7.0A\Bootstrapper\Packages\vcredist_x86\
Here is the official answer from Microsoft: http://blogs.msdn.com/b/chuckw/archive/2011/12/09/known-issue-directx-sdk-june-2010-setup-and-the-s1023-error.aspx
Summary if you'd rather not click through:
    Remove the Visual C++ 2010 Redistributable Package version 10.0.40219 (Service Pack 1) from the system (both x86 and x64 if applicable). This can be easily done via a command-line with administrator rights:
    MsiExec.exe /passive /X{F0C3E5D1-1ADE-321E-8167-68EF0DE699A5}
    MsiExec.exe /passive /X{1D8E6291-B0D5-35EC-8441-6616F567A0F7}
    Install the DirectX SDK (June 2010)
    Reinstall the Visual C++ 2010 Redistributable Package version 10.0.40219 (Service Pack 1). On an x64 system, you should install both the x86 and x64 versions of the C++ REDIST. Be sure to install the most current version available, which at this point is the KB2565063 with a security fix.
Windows SDK: The Windows SDK 7.1 has exactly the same issue as noted in KB 2717426.




http://qt-project.org/forums/viewthread/27448/

/* OLD--------------------------- */






/*--------------------------- */
SOKRATES_XP changes:
/*--------------------------- */

1) To overcome problem with c:\qt\5.0.2\5.0.2\msvc2010\include\qtcore\qdatetime.h(123): warning C4003: not enough actual parameters for macro 'min'
you must define nominmax before windows include:

#define NOMINMAX
#include <windows.h>

2) All styles are extensively changed in os_specific (classes such as windowsxpstyle are not available anymore)
3) QWindowsMime is also obsloete. So old code from QT4.8.0. is copied into WindowsMimeOutlook class: some wierd includes are done....be aware..
4) QFtp, QHttp, QHttpHeader are obsoleted. So old code from QT4.8.0. is copied into trans lib
5) MAPI is taken care by Miro...he told that he just switch order of includes...
6) All widgets and rest of classes are now moved to different paths and libs...so take care...
7) qFindChildren() global funct changed..see http://qt-project.org/wiki/Transition_from_Qt_4.x_to_Qt5#42fd90f01b98e4ec50d9c8cb059db77a
8) Rebuild old moc filed for property browser before building gui_core, execute batch: c:\SokratesXP\lib\gui_core\gui_core\property_browser\
9) qVariantValue<int>(val) is deprecated -> reformat as val.value<int>()
10) toAscii() -> toLatin()
11) Visual 2010, starts wrong program and give warnings if output file is different then targetname. Change target name and rest in the "General" property page"
$(OutDir), $(TargetPath) and $(TargetExt) are exposed on the "General" property page, as "Output Directory", "Target Name",
12) Can not add QT5 class to fui_collection or any other project.....arghhhh
13) Mimeoutlook works in native, just using this piece of code:

	if (mime->hasFormat("application/x-qt-windows-mime;value=\"RenPrivateMessages\""))
	{
		//import emails:
		QByteArray arData = mime->data("application/x-qt-windows-mime;value=\"RenPrivateMessages\"");
		

