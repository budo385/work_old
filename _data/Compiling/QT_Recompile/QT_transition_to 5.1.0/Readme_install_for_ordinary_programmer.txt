Dragi prijatelji:

na danasnji dan idemo na:

-openssl 1.0.1e
-firebird 2.5.2
-QT 5.1.0
-Visual 2010 +sp1

Precizno sljedite donje upute i sve ce biti u redu

1. Remove qt addin, QT 4.8.0, Ankh, Tortoise, VisualAssistX, Visual 2008, Firebird, reset comp
2. Install VS2010 + SP1 (use DVD's)
3. Install QT 5.1.0: copy P:\DevelopmentTools\QT\QT5.1.0\qt-enterprise-5.1.0-windows-msvc2010-x86-offline.exe to your local disk and execute. Do not install Src (default option)
   Use license: .qt-license (key inside)
4. Install qt-vs-addin-1.2.2.exe
5. Set enviroment vars: 
	QTDIR = C:\Qt\Qt5.1.0\5.1.0\msvc2010
	To Path var add at end C:\Qt\Qt5.1.0\5.1.0\msvc2010\bin
6. Install new TortoiseSVN-1.7.13.24257-win32-svn-1.7.10.msi and AnkhSvn-2.4.12012.msi from P:\DevelopmentTools\Svn\
7. Install VA_X_Setup1940.exe from P:\DevelopmentTools\Visual Assist X\, use license: VA_X_License_BT_2014.txt
8. Install new Firebird from P:\Database\FireBird\ver2.5.2_2013_05_14\Firebird-2.5.2.26540_0_Win32.exe
	use superserver
9. Now Copy all from U:\_Qt_dlls\5.1.0\ all files to the C:\Qt\Qt5.1.0
10. OpenSSL: Extract new OpenSSL from P:\DevelopmentTools\QT\QT5.1.0\_Recompile_methods_2013_08_01\OpenSSL_buildVS2010\1.0.1e_OpenSSL.zip
	to C:\OpenSSL
11. Open AdminTool, clean & rebuild, copy admintool.exe, dbactualize.dll to appserver/debug. 
	Copy new firebird.dll to appserver/debug
	Copy ssleay32.dll, libeay32.dll from U:\_Qt_dlls\ to appserve/debug
	Use new firebird.dll and ssleay32.dll, libeay32.dll in all projects
12. Clean & rebuild appserver i sokrates_exe

13. Note: for appserver use -e in Debugging/Command Arguments and -e Hajduk for hattrick

So help us god!

	


