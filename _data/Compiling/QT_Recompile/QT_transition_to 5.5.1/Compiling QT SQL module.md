GOAL
--------------------------------------------------	
In current arhitecture all documents from document manager component are stored inside database. Server uploads file in chunks from client to the server in temporaray directory. When file is stored on server file system it must be written into database. If using ordinary writeblob SQL command it will break code and produce error (timeout, buffer overflow, etc..). So we implemented writing and reading blob from file into/from database by chunks. This is already supported by native Firebird driver but it's not supported by QT SQL module, so there is need to extend QT SQL module API to support these operations. In this manner we can save file up to 100Gb or more into database (we never tested limit).


New extended API's for blob chunked read/write
--------------------------------------------------	
bool QSqlResult::writeBlobChunk(const QVariant &val, bool bEof);
bool QSqlResult::fetchBlobChunk(QVariant &val, bool &bEof);

bool QSqlQuery::fetchBlobChunk(QVariant &val, bool &bEof);
bool QSqlQuery::writeBlobChunk(const QVariant &val, bool bEof=false);


Instruction for QT 5.5.1 on VS2013  (2016-02-05)
--------------------------------------------------	


Step1: prepare code
--------------------------------------------------	

**Note: **we are modifying QT sql module: Qt5Sql.dll and plugin sql driver for firebird: qsqlibase.dll

1. to recompile any QT module, QT source code must be downloaded and placed inside directory c:\Qt\5.5\Src\ (5.5 is current QT version)
2. backup existing sql ibase driver files from "c:\Qt\5.5\Src\qtbase\src\sql\drivers\ibase\" into "\_data\Compiling\New_SQL_interface\5.5.1\"
3. now from previous version of modified ibase driver qsql_ibase.cpp located at "\_data\Compiling\New_SQL_interface\"  insert four function defintions into new "c:\Qt\5.5\Src\qtbase\src\sql\drivers\ibase\qsql_ibase.cpp":
QIBaseResult::fetchBlobChunk, QIBaseResult::writeBlobChunk, QIBaseResultPrivate::fetchBlobChunk, QIBaseResultPrivate::writeBlobChunk
and everything connected with them including defintion and intialization of handlers:

QIBaseResultPrivate::public:
isc_blob_handle last_blob_chunk_handle;
ISC_QUAD		*last_blob_bId;
    
QIBaseResultPrivate::QIBaseResultPrivate(QIBaseResult *d, const QIBaseDriver *ddb):
q(d), db(ddb), trans(0), stmt(0), ibase(ddb->d_func()->ibase), sqlda(0), inda(0), queryType(-1), tc(ddb->d_func()->tc), last_blob_chunk_handle(0), last_blob_bId(0)

Also check if initialization of the Time is done inside static QDateTime fromTimeStamp(char *buffer), it must be:
QTime t(0, 0);

Insert function defintions inside: new "c:\Qt\5.5\Src\qtbase\src\sql\drivers\ibase\qsql_ibase_p.h":
	bool fetchBlobChunk(QVariant &val,bool &bEof);
    bool writeBlobChunk(const QVariant &val, bool bEof=false);

Insert new API's (writeBlobChunk and fetchBlobChunk) inside "c:\Qt\5.5\Src\qtbase\src\sql\kernel\" qsqlquery.h,qsqlquery.cpp,qsqlresult.h,qsqlresult.cpp, see old modified code inside "\_data\Compiling\New_SQL_interface\kernel"


Step2: configure and rebuild dll's
--------------------------------------------------	

1. Install VS2013 and QT with all QT env variables set
2. Install AcivePerl, Python, Ruby and DirectX SDK 
3. Qt comercial license file is stored in user directory. On Windows 10 it is placed inside: "C:\Users\<user>\AppData\Roaming\Qt\" and it's called qtlicenses.ini. We had problems with compiling because Licensee name contained our letters (ćžčćšđ) and this info is somehow incorporated into config files needed for QT build. So make sure there is not special chars inside this file or escape sequences like \x103 
4. make sure you have QTDIR enviroment variable set (c:\Qt\5.5.1\5.5.1\msvc2013) and set QTDIR/bin in path
6. Go to "_data\Compiling\QT_Recompile\QT_transition_to 5.5.1\" copy config.bat into "c:\Qt\5.5\Src\qtbase\". Check paths and execute config.bat
7. open Visual, go to QT menu and open QT project file sql.pro file from c:\Qt\5.5\Src\qtbase\src\sql\. Build release and debug, save sln.
8. new QT SQL release and debug dlls are placed inside c:\Qt\5.5\Src\qtbase\lib\. Copy dlls to the c:\Qt\5.5\msvc2013\bin\ and libs to the c:\Qt\5.5\msvc2013\lib\ and modified qsqlresult.h and qsqlquery.h into c:\Qt\5.5\msvc2013\include\QtSql\
9. Recompile Firebird driver plugin: copy Build_Firebird.bat from "_data\Compiling\FireBird\" to the "c:\Qt\5.5\Src\qtbase\src\plugins\sqldrivers\ibase\". Make sure that new firevird is installed in the C:\Firebird. Run Build_firebird.bat. In c:\Qt\5.5\Src\qtbase\plugins\sqldrivers\ there should be new qsqlibase.dll release and debug
10. copy qsqlibase.dll and qsqlibased.dllinto c:\Qt\5.5\msvc2013\plugins\sqldrivers\
11. That's it. QT is ready for use: assemble new headers and recompiled dll's to specified directories for others to easily patch new QT installation without any problems.