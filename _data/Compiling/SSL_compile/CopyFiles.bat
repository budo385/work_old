@ECHO OFF

RMDIR /Q /S C:\OpenSSL
MKDIR C:\OpenSSL
MKDIR C:\OpenSSL\bin
MKDIR C:\OpenSSL\include
MKDIR C:\OpenSSL\lib\VC\static

XCOPY /S /E /Y "C:\OpenSSLSource\inc32" C:\OpenSSL\include
COPY /Y "C:\OpenSSLSource\out32dll\*.exe" C:\OpenSSL\bin
COPY /Y "C:\OpenSSLSource\out32dll\*.dll" C:\OpenSSL\bin
COPY /Y "C:\OpenSSLSource\out32dll\*.cnf" C:\OpenSSL\bin
COPY /Y "C:\OpenSSLSource\out32dll\libeay32.lib" C:\OpenSSL\lib\VC\static\libeay32MD.lib
COPY /Y "C:\OpenSSLSource\out32dll\ssleay32.lib" C:\OpenSSL\lib\VC\static\ssleay32MD.lib
COPY /Y "C:\OpenSSLSource\out32dll\libeay32.lib" C:\OpenSSL\lib\VC\static\libeay32MDd.lib
COPY /Y "C:\OpenSSLSource\out32dll\ssleay32.lib" C:\OpenSSL\lib\VC\static\ssleay32MDd.lib




