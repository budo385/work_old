2016-02-03: (with version 1.0.2f) 
---------------------------------------	
1. download and install ActivePerl: ActivePerl-5.22.1.2201-MSWin32-x64-299574.msi (make sure perl is available from command line)
2. download and install NASM assembler compiler from http://www.nasm.us (I used zipped standalone installation, so I changed system env PATH to point to NASM.exe, so nasm is also available from command line)
3. create directory C:\OpenSSLSource and extract openssl-1.0.2f.tar.gz into this directory from https://www.openssl.org/
2. copy configure.bat, copyfiles.bat and build.bat and adjust path to vsvars32.bat from Visual Studio (it's now set to the VS 2013)
3. execute configure, build and copyfiles in this order, you should have C:\OpenSSL directory with all libs, dlls and include headers created (use libeay32.dll and ssleay32.dll to enable SSL in your app)
4. to create self-signed certificates, certificate requests and other things regarding certificates, use all exe's and dll's inside C:\OpenSSLSource\out32dll
5. 


2013-05-08:
1. download OpenSSL openssl-0.9.8g.tar.gz and Perl: ActivePerl-5.10.0.1002-MSWin32-x86-283697.msi
2. extract SSL to c:/OpenSSLSource
3. copy Configure.bat in c:/OpenSSLSource dir and execute
5. To build debug version, edit ms/do_ms.bat and add 'debug' keyword in the "perl util\mk1mf.pl debug dll no-asm VC-WIN32 >ms\ntdll.mak"
6. Else just leave as is..copy all output files to 
	C:\OpenSSL\include
	C:\OpenSSL\lib\VC\static\
	C:\OpenSSL\bin
	Execute copy.bat

---------------------------------------	
1. download OpenSSL openssl-0.9.8g.tar.gz and Perl: ActivePerl-5.10.0.1002-MSWin32-x86-283697.msi
2. extract SSL to c:/OpenSSLSource
3. copy Configure.bat in c:/OpenSSLSource dir and execute
4. copy Configure.bat in c:/OpenSSLSource dir and execute (Visual Studio must be on C: and ml.exe present)
5. create cert and keys subdir inside c:/OpenSSLSource
6. copy all content of out32dll to c:/OpenSSLSource
7. copy Configure.bat in c:/OpenSSLSource dir and execute, use MASTERPASSWORD for phrase, edit to extend period
8. keys/sokrates.pkey and keys/sokrates.pkey are result
9. use libeay32.dll and ssleay32.dll with exe files

How to craete new certificate: copy _create.bat to c:/OpenSSLSource,e execute! Amen

