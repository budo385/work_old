openssl genrsa -des3 -out keys/sokrates.pkey 1024
openssl req -config openssl.cnf -new -x509 -days 1001 -key keys/sokrates.pkey -out certs/sokrates.cert
