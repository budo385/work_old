----------------------------------
JPEG 2000
----------------------------------

20.12.2011:
--------------------------------------------
- jasper needs glut lib, but do not bother, follow this:
- extract qtjp2imageformat-2.2-commercial.zip to D:\Temp
- extract qtjp2imageformat.pri from old_rebuilds_Compiling_jpg2000.zip/new_qtjp2imageformat-2.2-commercial/src and copy to previusly extracted dir
- copy build.bat and execute
- result is qtjp2d2.dll in the C:\Qt\4.8.0\plugins\imageformats\



JPEG2000 Plugin built from:
- jasper-1.701.0.zip
- qtjp2imageformat-2.1-commercial.zip

For development:
Copy all files to  C:\Qt\4.2.0\plugins\imageformats\


Quick Instructions how to build:
1. Extract jasper-1.701.0.zip in C:\jasper (no version)
2. Open c:\jasper\src\msvc\jasper.dsw, set properties to all projects: Preprocesor: _CRT_SECURE_NO_DEPRECATE &  CodeGeneration: /MD (release) & /MDd (debug)
3. Compile: in c:\jasper\src\msvc\win32_debug & release must contain jasper.lib
4. Extract solution, replace/set paths in src/qtjp2imageformat.pri (set paths) 
6. copy build.bat in in extract solution dir, run it!
7. output is in C:\Qt\4.2.0\plugins\imageformats\

Deploy:
1. add sub directory in app. path : imageformats (MUST be this name)
2. place release version of qtjp22.dll (from here) 


----------------------------------
JPEG
----------------------------------

For any plugin recompilation:
1. open console, use %comspec% /k "D:\Program Files\Microsoft Visual Studio 8\Common7\Tools\vsvars32.bat"
2. use qmake and namke release/debug in /src/plugins directiory
