QT 5.1.0
1) changed all 5.1.0 files, Backup of 5.1.0 ibase is in /5.1.0 subdir. In /kernel are new QT5.1.0 changed files. In root there is new ibase from 5.1.0.
Note: qsql_ibase_p.h is new file from QT5.1.0
2) copy all h/cpp files to specified location..then copy Build_Firebird.bat to Src\qtbase\src\plugins\sqldrivers\ibase\ and run!


NEW issue: TIMESTAMP part of firebird driver:


;---------------------------------------------------------------------------------------------------------------------------------------
qdatetime.cpp

 
QTime QTime::addMSecs(int ms) const
{
    QTime t;
    if (ms < 0) {
        // % not well-defined for -ve, but / is.
        int negdays = (MSECS_PER_DAY - ms) / MSECS_PER_DAY;
        t.mds = (ds() + ms + negdays * MSECS_PER_DAY) % MSECS_PER_DAY;
    } else {
        t.mds = (ds() + ms) % MSECS_PER_DAY;
    }
#if defined(Q_OS_WINCE)
    if (startTick > NullTime)
        t.startTick = (startTick + ms) % MSECS_PER_DAY;
#endif
    return t;
}

And this is qdatetime.cpp in Qt5.0

QTime QTime::addMSecs(int ms) const
{
    QTime t;
    if (isValid()) {
        if (ms < 0) {
            // % not well-defined for -ve, but / is.
            int negdays = (MSECS_PER_DAY - ms) / MSECS_PER_DAY;
            t.mds = (ds() + ms + negdays * MSECS_PER_DAY) % MSECS_PER_DAY;
        } else {
            t.mds = (ds() + ms) % MSECS_PER_DAY;
        }
    }
#if defined(Q_OS_WINCE)
    if (startTick > NullTime)
        t.startTick = (startTick + ms) % MSECS_PER_DAY;
#endif
    return t;
}

The difference is that it's calling isValid() fuction.
The next is qsql_ibase.cpp (no difference between Qt4.8.4 and Qt5.0.0)

static QDateTime fromTimeStamp(char *buffer)
{
    static const QDate bd(1858, 11, 17);
    QTime t;//########## this will fail on isValid() since it's null Time
    QDate d;

    // have to demangle the structure ourselves because isc_decode_time
    // strips the msecs
    t = t.addMSecs(int(((ISC_TIMESTAMP*)buffer)->timestamp_time / 10));
    d = bd.addDays(int(((ISC_TIMESTAMP*)buffer)->timestamp_date));

    return QDateTime(d, t);
}

I guess this should be fixed as next

static QDateTime fromTimeStamp(char *buffer)
{
    static const QDate bd(1858, 11, 17);
    QTime t(0, 0, 0);//########## should be fixed like this to pass on isValid() 
    QDate d;

    // have to demangle the structure ourselves because isc_decode_time
    // strips the msecs
    t = t.addMSecs(int(((ISC_TIMESTAMP*)buffer)->timestamp_time / 10));
    d = bd.addDays(int(((ISC_TIMESTAMP*)buffer)->timestamp_date));

    return QDateTime(d, t);
}

;---------------------------------------------------------------------------------------------------------------------------------------







QT 5.0.2
1) changed all 5.0.2 files, just some minor changes detected. Backup of 5.0.2 ibase is in /5.0.2 subdir. In root there is new ibase from 5.0.2


QT 4.8.0
1) 4.7.2 and 4.8.0. are same, just copy newest version
2) Warning: 4.8.0 does not have vcproj, so copy build_sql_module.bat to C:\Qt\4.8.0\src\sql\ and run
3) aftewards open QtSql.vcproj and execute rebuild release and debug then check if new QtSQL4 and QtSQL4d.dll are alive in /bin directory




QT 4.7.2
- qsql_ibase.h i cpp same as before (only license changed)->overwrite
- qresult modified and backuped


QT 4.7.0
- completely rewritten from 4.6.2 -> no need to update error handling
- all same as before


Last QT: 4.6.02 comptabile with 4.6.0

Last QT: 4.6.0: (use all soucrce from 4.6.0 directory)

Goal: rebuild QTSQL moduel to support chunked blob read...

1. replace kernel/qsqlquery and kernel/qsqlresult (new api on QSQLQuery: fetchBlobChunk and  writeBlobChunk)
2. replace src/sqldrivers/ibase/qsql_ibase.cpp/h -> winmerge
3. reconfigure QT: (conf.bat)
4. run in src/sql/vcproj -> rebuild QSQL.dll
5. rebuild /src/plugins/... ibase and sqlite (also needed)

Note: as from 4.6.0. it seems that qsql_ibase.cpp is patched to correctly parse error from FB 
Note: if rebuilding whole QT then prior to rebuild, change all these files..


---------------------------------------------------------

Last QT: 4.5.0
1. replace kernel/qsqlquery and kernel/qsqlresult (new api on QSQLQuery: fetchBlobChunk and  writeBlobChunk)
2. replace /sqldrivers/ibase /winmerge
3. reconfigure QT: (conf.bat)
4. run in src/sql/vcproj -> rebuild QSQL.dll
5. rebuild /src/plugins/... ibase and sqllite

LAST REV: 4.5.1 patched ibase.cpp

