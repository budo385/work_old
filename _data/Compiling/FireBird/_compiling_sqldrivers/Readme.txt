15.01.2008, FIREBIRD compile (new):
--------------------------------
1. install new FireBird into C:\
2. patch qsql_ibase.cpp static bool getIBaseError(QString& msg, ISC_STATUS* status, long &sqlcode) into C:\Qt\4.3.2\src\sql\drivers\ibase\
3. copy Build_firebird.bat into C:\Qt\4.2.0\src\plugins\sqldrivers\ibase (Visual Studio is on D:\)
4. build it, in C:\Qt\4.3.2\plugins\sqldrivers\ new dll's are!

Deploy FireBird:
----------------------------------
1. to use multiple services, use ziped FireBird installation, 
2. use instsvc.exe, instreg.exe and install scripts (install_super.bat..) from 2.1.xx to install as separate services 
(e.g. install_super service1, install_super service1, uninstall service1,etc..)



----------------------------------
MySQL
----------------------------------


MySQL Plugin built from:
- Qt.4.2.2
- MySQL 5.0.24-community Edition

For development:
Copy all files to  C:\Qt\4.2.0\plugins\sqldrivers\

MySQL:
Quick Instructions how to build:
1. Go to C:\Qt\4.2.0\src\plugins\sqldrivers\mysql\
2. open command prompt, run vsvar32.bat from this command prompt (%comspec% /k "D:\Program Files\Microsoft Visual Studio 8\Common7\Tools\vsvars32.bat")
3. run qmake and adjust include paths and give path to libmysql.lib: qmake -o Makefile "INCLUDEPATH+=C:\Qt\4.2.0\src\plugins\sqldrivers\mysql\include" "LIBS+=C:\Qt\4.2.0\src\plugins\sqldrivers\mysql\libmysql.lib" mysql.pro
4. run nmake debug or nmake release
5. output is in C:\Qt\4.2.0\plugins\sqldrivers\

Deploy:
1. add sub directory in app. path : sqldrivers (MUST be this name)
2. place release version of qsqlmysql.dll (from here) and libmysql.dll (from MySQL bin directory or opt)



----------------------------------
FIREBIRD
----------------------------------


FireBird Plugin built from:
- Qt.4.2.0
- Firebird-1.5.3.4870-0-Win32.exe


FIREBIRD Embeded compile:

- embeded lib does not exists so use: 
fbclient_ms.lib & ib_util_ms.lib from FireBird installation (oridnary clients)
- rename fbclient_ms to gds32_ms.lib, place in build folder with include headers (C:\Qt\4.2.0\src\plugins\sqldrivers\uibasel\)
- run vsvar32.bat/qmake
- run qmake -o Makefile "INCLUDEPATH+=C:\Qt\4.2.0\src\plugins\sqldrivers\uibase" ibase.pro
- run nmake release or deebug

Deploy:
1. add sub directory in app. path : sqldrivers (MUST be this name)
2. place release version of qsqlibase.dll 
3. Place  fbembed.dll from Emdeded installation  and rename it as fbclient.dll in same folder where is app, copy these files:
	- firebird.msg 
	- ib_util.dll
	- ??
4. Note to use FireBird super server, just use oridnary fbclient and in connection params set host: localhost or ip address!


FireBird short info:
- Before using embeded database, database file must be created
- Connection parameters used inside SOKRATES_XP for file database xx.fdb placed on c:\ are:

	ConnSettings.m_strDbType=DBTYPE_FIREBIRD;
	ConnSettings.m_strDbHostName="";
	ConnSettings.m_strDbUserName="SYSDBA";
	ConnSettings.m_strDbPassword="masterkey";
	ConnSettings.m_strDbName="C:\\xx.fdb";
	ConnSettings.m_nDbPort=0;
	
- Create DB: Install Firebird superserver, in bin open isql (semicolon is must at end of command)
- CREATE DATABASE "C:\xx.fdb" user 'SYSDBA' password 'masterkey';
- CONNECT "C:\xx.fdb" user 'SYSDBA' password 'masterkey'; to connect to DB
- WARNING: only single connection is allowed to FireBird file!




How can i make an auto-incrementing primary key column? and is the NOT NULL constraint mandatory for a primary key?
In Firebird, you achieve an auto-incrementing PK using a generator and a BEFORE INSERT trigger:

    CREATE GENERATOR GEN_PK_ATABLE;
    COMMIT;

Define the column, e.g., ATABLE_ID, as BIGINT or INTEGER and yes, the NOT NULL constraint is mandatory for a primary key.

The trigger for automatic population of the key is this:

    CREATE TRIGGER BI_ATABLE FOR ATABLE
    ACTIVE BEFORE INSERT
    AS
    BEGIN
      IF(NEW.ATABLE_ID IS NULL) THEN 
        NEW.ATABLE_ID = GEN_ID(GEN_PK_ATABLE, 1);
    END

Back to TOP
How can I get the value of my generated primary key after an insert ?
Firebird doesn't currently return values from insert statements and, in multi-user, it isn't safe to query the generator afterwards (GEN_ID(Generator_name, 0) because you have no way to know whether the current "latest" value was yours or someone else's. The trick is to get the generator value into your application as a variable before you post your insert. This way, you make it available not just for your insert but for any dependent rows you need to create for it inside the same transaction.

   SELECT GEN_ID(Generator_name, 1) AS MyVar
     FROM RDB$DATABASE;

Some data access interfaces and drivers provide ways to do this automatically for you. For example, IB Objects implements the GeneratorLinks property for statement classes.

Back to TOP