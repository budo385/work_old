17.10.2010:
- as of FB 2.5.0: there is no need to change anything
- when compiling FB QT driver copy consts_pub.h from source in C:\Firebird\include
- Warning: When new binary build is available (from c:\FireBirdSource\output_Win32\) do not forget to copy our V2008sp1 dll's into firebird/bin directory as old ones will not be valid. Ahahahahaha!!!!



To recompile FireBird database:
1. download source
2. download sed utility from http://gnuwin32.sourceforge.net/packages.html 
3. set path to sed utility/restart
4. extract source to some folder (DO NOT use space in folder name)
5. read docs/build.msvc
6. go to build/win32 execute: make_icu, make_boot and make_All


BUG with Firebird-2.1.3.18185-0_Win32, preprocess.bat is not generating gen/dudley/extract.cpp and exe.cpp
Just move line: @for %%i in (exe, extract) do @call :PREPROCESS dudley %%i at start of MASTER section


Note: for 2008VS: 
1. Use new setenvvar.bat -> reroute to use MSVC8 tags

2. comment all #define vsnprintf in files: 
c:\FireBird3\src\include\gen\autoconfig_msvc.h
c:\FireBird3\src\config\Stream.cpp
c:\FireBird3\src\config\ArgsException.cpp
c:\FireBird3\src\config\AdminException.cpp

3. modify c:\FireBird3\src\jrd\os\win32\mod_loader.cpp line 97: set "msvcr90.dll", instead of error


4.open and convert with VS 2008 to new version:
	c:\FireBird3\extern\icu\source\allinone\allinone_8.sln
	c:\FireBird3\builds\win32\msvc8\ all sln here
	
5. 	modify preprocess.bat see above

6. modify src/jrd/os/win32/guid.cpp -> #define _WIN32_WINNT 0x0500
7. modify c:\FireBird3\builds\win32\msvc8\fbcontrol.vcproj -> set _WIN32_WINNT 0x0500 in release /debug

GO GO!!


