Qt 4.2.2. (patch for Outlook Drop)
Compile QtGui4.dll:

Qt 4.2.2: patched:
QtGuid4.dll and QtGui4.dll, place in your Qt\bin directory

1. Copy qmime_win_new.cpp to c:\Qt\4.2.2\src\gui\kernel\, replace old one (without _new extension)
2. Run c:\Qt\4.2.2\configure -qt-libpng
3. Open c:\Qt\4.2.2\src\3rdparty\libpng\projects\visualc71\libpng.sln
4. Set direcotry to c:\Qt\4.2.2\src\corelib\global\ in Visual Studio/Tools/Options/Project & solutions/ VC++ Directory  (Include files)
5. Build project as static lib both in release & debug-> copy all to c:\Qt\4.2.2\lib
6. run script Build.bat from c:\Qt\4.2.2\ (check paths before)
