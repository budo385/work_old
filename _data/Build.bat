CALL "C:\Program Files\Microsoft Visual Studio 9.0\Common7\Tools\vsvars32.bat"

REM change to SOKRATES_XP_RELEASE for public build
set BUILD_SOURCE=%SOKRATES_XP%

REM clean all
REM devenv "%BUILD_SOURCE%\exe\sokrates_spc\sokrates_spc.sln" /clean release
REM devenv "%BUILD_SOURCE%\exe\appserver\appserver.sln" /clean release
REM devenv "%BUILD_SOURCE%\tool\ApplicationServerConfig\ApplicationServerConfig.sln" /clean release
REM devenv "%BUILD_SOURCE%\tool\AdminTool\AdminTool.sln" /clean release
REM devenv "%BUILD_SOURCE%\tool\Sokrates_Register\Sokrates_Register.sln" /clean release
REM devenv "%BUILD_SOURCE%\lib\phonet\phonet.sln" /clean release
REM devenv "%BUILD_SOURCE%\lib\mapi_ex\MAPIEx.sln" /clean release
REM devenv "%BUILD_SOURCE%\tool\KeyGenerator\KeyGenerator.sln" /clean release

REM build all
devenv "%BUILD_SOURCE%\exe\sokrates_spc\sokrates_spc.sln" /build release
devenv "%BUILD_SOURCE%\exe\appserver\appserver.sln" /build release
devenv "%BUILD_SOURCE%\tool\ApplicationServerConfig\ApplicationServerConfig.sln" /build release
devenv "%BUILD_SOURCE%\tool\AdminTool\AdminTool.sln" /build release
devenv "%BUILD_SOURCE%\tool\Sokrates_Register\Sokrates_Register.sln" /build release
devenv "%BUILD_SOURCE%\lib\phonet\phonet.sln" /build release
devenv "%BUILD_SOURCE%\lib\mapi_ex\MAPIEx.vcproj" /build release
devenv "%BUILD_SOURCE%\tool\ApplicationServerUpdate\ApplicationServerUpdate.sln" /build release
devenv "%BUILD_SOURCE%\tool\KeyGenerator\KeyGenerator.sln" /build release
devenv "%BUILD_SOURCE%\tool\Sokrates_Progress\Sokrates_Progress.sln" /build release
CALL "%BUILD_SOURCE%\lib\gui_core\gui_core\Resources\Themes\Build_themes.bat"

REM COPY all
copy /Y "%BUILD_SOURCE%\exe\appserver\release\appserver.exe" U:\SokratesXP\_new_exe\AppServerDeploy
copy /Y "%BUILD_SOURCE%\exe\sokrates_spc\release\sokrates.exe" U:\SokratesXP\_new_exe\SPCDeploy
copy /Y "%BUILD_SOURCE%\tool\AdminTool\release\AdminTool.exe" U:\SokratesXP\_new_exe\AppServerDeploy
copy /Y "%BUILD_SOURCE%\tool\AdminTool\release\db_actualize.dll" U:\SokratesXP\_new_exe\SPCDeploy
copy /Y "%BUILD_SOURCE%\tool\AdminTool\release\db_actualize.dll" U:\SokratesXP\_new_exe\AppServerDeploy
copy /Y "%BUILD_SOURCE%\tool\ApplicationServerConfig\release\ApplicationServerConfig.exe" U:\SokratesXP\_new_exe\AppServerDeploy
copy /Y "%BUILD_SOURCE%\tool\ApplicationServerUpdate\release\ApplicationServerUpdate.exe" U:\SokratesXP\_new_exe\AppServerDeploy
copy /Y "%BUILD_SOURCE%\tool\Sokrates_Progress\release\Sokrates_Progress.exe" U:\SokratesXP\_new_exe\SPCDeploy
copy /Y "%BUILD_SOURCE%\tool\Sokrates_Register\release\Sokrates_Register.exe" U:\SokratesXP\_new_exe\SokratesRegisterDeploy
copy /Y "%BUILD_SOURCE%\lib\phonet\release\phonet.dll" U:\SokratesXP\_new_exe\SPCDeploy
copy /Y "%BUILD_SOURCE%\lib\phonet\release\phonet.dll" U:\SokratesXP\_new_exe\AppServerDeploy
copy /Y "%BUILD_SOURCE%\lib\mapi_ex\release\MAPIEx.dll" U:\SokratesXP\_new_exe\SPCDeploy
copy /Y "%BUILD_SOURCE%\lib\report_definitions\report_definitions\*.xml" U:\SokratesXP\_new_exe\SPCDeploy\report_definitions
copy /Y "%BUILD_SOURCE%\_data\Translation\sokrates_de.qm" U:\SokratesXP\_new_exe\SPCDeploy
copy /Y "%BUILD_SOURCE%\lib\os_specific\os_specific\mimepp\bin\mimepp.dll" U:\SokratesXP\_new_exe\SPCDeploy
copy /Y "%BUILD_SOURCE%\lib\os_specific\os_specific\mailpp\bin\mailpp.dll" U:\SokratesXP\_new_exe\SPCDeploy
copy /Y "%BUILD_SOURCE%\tool\KeyGenerator\release\KeyGenerator.exe" U:\SokratesXP\_new_exe\KeyFiles
copy /Y "%BUILD_SOURCE%\tool\KeyGenerator\release\FpAdmin.exe" U:\SokratesXP\_new_exe\KeyFiles
copy /Y "%BUILD_SOURCE%\lib\gui_core\gui_core\Resources\Themes\*.rcc" U:\SokratesXP\_new_exe\SPCDeploy\themes

XCOPY /S /E /Y "%BUILD_SOURCE%\_data\XML\REST_Template_XSD\webservices" U:\SokratesXP\_new_exe\SPCDeploy\themes\webservices






