@ECHO OFF
REM 
REM Script to delete all Test project garbage
REM 


CD "%SOKRATES_XP%"/projects/test_proj/BusinessTest/_docs/
CALL _build_clean.bat

CD "%SOKRATES_XP%"/projects/test_proj/DbConnectionSettings.root\DbConnectionSettings\_docs/
CALL _build_clean.bat

CD "%SOKRATES_XP%"/projects/test_proj/DBRecordsetTest/_docs/
CALL _build_clean.bat

CD "%SOKRATES_XP%"/projects/test_proj/DBSQLTableDefTest/_docs/
CALL _build_clean.bat

CD "%SOKRATES_XP%"/projects/test_proj/IniTest/_docs/
CALL _build_clean.bat

CD "%SOKRATES_XP%"/projects/test_proj/IPAccessListTest/_docs/
CALL _build_clean.bat

CD "%SOKRATES_XP%"/projects/test_proj/Logger_test/_docs/
CALL _build_clean.bat

CD "%SOKRATES_XP%"/projects/test_proj/QDBConnectionPool.root/QDBConnectionPool/_docs/
CALL _build_clean.bat

CD "%SOKRATES_XP%"/projects/test_proj/QTestServer/_docs/
CALL _build_clean.bat


CD "%SOKRATES_XP%"/projects/test_proj/Test_StatusObject/_docs/
CALL _build_clean.bat

CD "%SOKRATES_XP%"/projects/test_proj/ThickClient/_docs/
CALL _build_clean.bat

CD "%SOKRATES_XP%"/projects/test_proj/ThinClient/_docs/
CALL _build_clean.bat


CD "%SOKRATES_XP%"/projects/test_proj/XmlRpcTest/_docs/
CALL _build_clean.bat