@ECHO OFF
REM 
REM Script to delete all Test project garbage
REM 


CD "%SOKRATES_XP%"/projects/lib_proj/appserver/_docs/
CALL _build_clean.bat

CD "%SOKRATES_XP%"/projects/lib_proj/bus_client/_docs/
CALL _build_clean.bat

CD "%SOKRATES_XP%"/projects/lib_proj/bus_core/_docs/
CALL _build_clean.bat

CD "%SOKRATES_XP%"/projects/lib_proj/bus_interface/_docs/
CALL _build_clean.bat

CD "%SOKRATES_XP%"/projects/lib_proj/bus_server/_docs/
CALL _build_clean.bat

CD "%SOKRATES_XP%"/projects/lib_proj/bus_trans_client/_docs/
CALL _build_clean.bat

CD "%SOKRATES_XP%"/projects/lib_proj/bus_trans_server/_docs/
CALL _build_clean.bat

CD "%SOKRATES_XP%"/projects/lib_proj/ccenter/_docs/
CALL _build_clean.bat

CD "%SOKRATES_XP%"/projects/lib_proj/db/_docs/
CALL _build_clean.bat


CD "%SOKRATES_XP%"/projects/lib_proj/db_actualize/_docs/
CALL _build_clean.bat

CD "%SOKRATES_XP%"/projects/lib_proj/db_core/_docs/
CALL _build_clean.bat

CD "%SOKRATES_XP%"/projects/lib_proj/gui_core/_docs/
CALL _build_clean.bat


CD "%SOKRATES_XP%"/projects/lib_proj/trans/_docs/
CALL _build_clean.bat

CD "%SOKRATES_XP%"/projects/lib_proj/trans/trans/_docs/
CALL _build_clean.bat