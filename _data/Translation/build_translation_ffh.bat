@ECHO OFF
REM 
REM This is a batch script to rebuild Sokrates_SPC translation
REM CALL: build_translation-bat 2>log.txt
REM TOFIX: include rebuilding .pri files to catch addition of new forms
REM

REM ECHO "Did you rename your C:\Qt\ to some garbage while building this? If not, translation might fail!!!!"
REM PAUSE

REM We need to use exact version of the lupdate/lrelease tools (from Qt4.3) because new version are buggy
SET PWD=%~dp0
REM echo %PWD%
SET LUPDATE_PATH="%PWD%lupdate.exe"
SET LRELEASE_PATH="%PWD%lrelease.exe"

SET WORKDIR=..\..
SET DESTDIR=D:\

REM PUSHD %WORKDIR%
%LUPDATE_PATH% %WORKDIR%\exe\appserver_james\appserver_james\appserver_james.pro
copy %WORKDIR%\exe\appserver_james\appserver_james\appserver_james_de.ts %DESTDIR%

%LUPDATE_PATH% %WORKDIR%\exe\appserver_james\appserver_james_common\appserver_james_common.pro
copy %WORKDIR%\exe\appserver_james\appserver_james_common\appserver_james_common_de.ts %DESTDIR%

%LUPDATE_PATH% %WORKDIR%\exe\appserver_james\appserver_james_gui\appserver_james_gui.pro
copy %WORKDIR%\exe\appserver_james\appserver_james_gui\appserver_james_gui_de.ts %DESTDIR%

%LUPDATE_PATH% %WORKDIR%\lib\common\common\common.pro
copy %WORKDIR%\lib\common\common\common_de.ts %DESTDIR%

%LUPDATE_PATH% %WORKDIR%\lib\trans\trans\trans.pro
copy %WORKDIR%\lib\trans\trans\trans_de.ts %DESTDIR%

%LUPDATE_PATH% %WORKDIR%\lib\NATTraversal\NATTraversal\NATTraversal.pro
copy %WORKDIR%\lib\NATTraversal\NATTraversal\nat_traversal_de.ts %DESTDIR%

PUSHD %WORKDIR%
D:
cd %DESTDIR%
%LRELEASE_PATH% -verbose appserver_james_de.ts appserver_james_common_de.ts appserver_james_gui_de.ts common_de.ts trans_de.ts nat_traversal_de.ts -qm EXB_Manager_de.qm
POPD

COPY /Y %DESTDIR%EXB_Manager_de.qm .\EXB_Manager_de.qm

