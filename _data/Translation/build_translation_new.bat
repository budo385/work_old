@ECHO OFF
REM 
REM This is a batch script to rebuild Sokrates_SPC translation
REM CALL: build_translation-bat 2>log.txt
REM TOFIX: include rebuilding .pri files to catch addition of new forms
REM

REM ECHO "Did you rename your C:\Qt\ to some garbage while building this? If not, translation might fail!!!!"
REM PAUSE

REM We need to use exact version of the lupdate/lrelease tools (from Qt4.3) because new version are buggy
SET PWD=%~dp0
REM echo %PWD%
SET LUPDATE_PATH="%QTDIR%\bin\lupdate.exe"
SET LRELEASE_PATH="%QTDIR%\bin\lrelease.exe"

SET WORKDIR=..\..
SET DESTDIR=%PWD%result\

REM PUSHD %WORKDIR%
%LUPDATE_PATH% %WORKDIR%\exe\sokrates_spc\sokrates_spc\sokrates_spc.pro
xcopy %WORKDIR%\exe\sokrates_spc\sokrates_spc\sokrates_de.ts %DESTDIR%

%LUPDATE_PATH% %WORKDIR%\lib\fui_collection\fui_collection\fui_collection.pro
xcopy %WORKDIR%\lib\fui_collection\fui_collection\fui_collection_de.ts %DESTDIR%

%LUPDATE_PATH% %WORKDIR%\lib\gui_core\gui_core\gui_core.pro
xcopy %WORKDIR%\lib\gui_core\gui_core\gui_core_de.ts %DESTDIR%

%LUPDATE_PATH% %WORKDIR%\lib\bus_client\bus_client\bus_client.pro
xcopy %WORKDIR%\lib\bus_client\bus_client\bus_client_de.ts %DESTDIR%

%LUPDATE_PATH% %WORKDIR%\lib\bus_core\bus_core\bus_core.pro
xcopy %WORKDIR%\lib\bus_core\bus_core\bus_core_de.ts %DESTDIR%

%LUPDATE_PATH% %WORKDIR%\lib\bus_interface\bus_interface\bus_interface.pro
xcopy %WORKDIR%\lib\bus_interface\bus_interface\bus_interface_de.ts %DESTDIR%

%LUPDATE_PATH% %WORKDIR%\lib\bus_server\bus_server\bus_server.pro
xcopy %WORKDIR%\lib\bus_server\bus_server\bus_server_de.ts %DESTDIR%

%LUPDATE_PATH% %WORKDIR%\lib\bus_trans_client\bus_trans_client\bus_trans_client.pro
xcopy %WORKDIR%\lib\bus_trans_client\bus_trans_client\bus_trans_client_de.ts %DESTDIR%

%LUPDATE_PATH% %WORKDIR%\lib\common\common\common.pro
xcopy %WORKDIR%\lib\common\common\common_de.ts %DESTDIR%

%LUPDATE_PATH% %WORKDIR%\lib\db\db\db.pro
xcopy %WORKDIR%\lib\db\db\db_de.ts %DESTDIR%

%LUPDATE_PATH% %WORKDIR%\lib\db_core\db_core\db_core.pro
xcopy %WORKDIR%\lib\db_core\db_core\db_core_de.ts %DESTDIR%

%LUPDATE_PATH% %WORKDIR%\lib\report_core\report_core\report_core.pro
xcopy %WORKDIR%\lib\report_core\report_core\report_core_de.ts %DESTDIR%

%LUPDATE_PATH% %WORKDIR%\lib\report_definitions\report_definitions\report_definitions.pro
xcopy %WORKDIR%\lib\report_definitions\report_definitions\report_definitions_de.ts %DESTDIR%

%LUPDATE_PATH% %WORKDIR%\lib\trans\trans\trans.pro
xcopy %WORKDIR%\lib\trans\trans\trans_de.ts %DESTDIR%

%LUPDATE_PATH% %WORKDIR%\lib\db_actualize\db_actualize\db_actualize.pro
xcopy %WORKDIR%\lib\db_actualize\db_actualize\db_actualize_de.ts %DESTDIR%

%LUPDATE_PATH% %WORKDIR%\lib\os_specific\os_specific\os_specific.pro
xcopy %WORKDIR%\lib\os_specific\os_specific\os_specific_de.ts %DESTDIR%

%LUPDATE_PATH% %WORKDIR%\lib\admin_tools\admin_tools\admin_tools.pro
xcopy %WORKDIR%\lib\admin_tools\admin_tools\admin_tools_de.ts %DESTDIR%

PUSHD %WORKDIR%
REM .\result\
cd %DESTDIR%
REM %LUPDATE_PATH% -no-obsolete -ts sokrates_de.ts fui_collection_de.ts gui_core_de.ts bus_client_de.ts bus_core_de.ts bus_interface_de.ts bus_server_de.ts bus_trans_client_de.ts common_de.ts  db_de.ts db_core_de.ts report_core_de.ts report_definitions_de.ts trans_de.ts db_actualize_de.ts os_specific_de.ts
%LRELEASE_PATH% -verbose sokrates_de.ts fui_collection_de.ts gui_core_de.ts bus_client_de.ts bus_core_de.ts bus_interface_de.ts bus_server_de.ts bus_trans_client_de.ts common_de.ts  db_de.ts db_core_de.ts report_core_de.ts report_definitions_de.ts trans_de.ts db_actualize_de.ts os_specific_de.ts admin_tools_de.ts -qm sokrates_de.qm
POPD

xcopy /Y %DESTDIR%sokrates_de.qm .

