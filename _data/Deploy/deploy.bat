REM usage: %1=ver %2=tommorowdate -> e.g. build 139 12.12.2007
REM ---sign & build -----

CALL D:\Deploy\certificate\sign_exe.bat
"C:\Program Files\Inno Setup 5\Compil32.exe" /cc D:\Deploy\AppServerDeploy\install.iss
copy /Y D:\Deploy\AppServerDeploy\Output\appserversetup.exe D:\Deploy\setup_sasc_v2008-%1.exe
"C:\Program Files\Inno Setup 5\Compil32.exe" /cc D:\Deploy\SPCDeploy\install.iss
copy /Y D:\Deploy\SPCDeploy\Output\setup.exe D:\Deploy\setup_scoc_v2008-%1.exe
CALL D:\Deploy\certificate\sign.bat %1

REM ---copy new exe to update directories of all servers -----
COPY /Y D:\Deploy\AppServerDeploy\*.exe "D:\Sokrates Servers\_update\update" 
COPY /Y D:\Deploy\AppServerDeploy\db_actualize.dll "D:\Sokrates Servers\_update\update"
COPY /Y D:\Deploy\AppServerDeploy\appserver.exe "D:\Sokrates Servers\_update\update\template_data"
COPY /Y D:\Deploy\AppServerDeploy\phonet.dll "D:\Sokrates Servers\_update\update"
CALL "D:\Sokrates Servers\app_update.bat"

REM ---set date for achedule on all app servers -----
CALL "D:\Sokrates Servers\_update\set_date" %2
