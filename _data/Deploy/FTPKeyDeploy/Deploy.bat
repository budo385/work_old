@echo off
echo .
echo .
echo ------------------------------------------------ 
echo Deploy Script by Branimir Trumbic
echo ------------------------------------------------ 
echo .
echo .
echo Please enter user name / directory - subdirectory in www.sokrates-communicator.com/keys
echo (no spaces, no special chars):
set /p var_dir=Directory: 
echo .
echo .
echo Please enter user password (no spaces, no special chars):
set /p var_pass=Password: 

if exist .htaccess del .htaccess
echo AuthUserFile /www/home/scommunicator/www/keys/%var_dir%/.htpasswd >> .htaccess
echo AuthGroupFile /dev/null >> .htaccess
echo AuthName EnterPassword >> .htaccess
echo AuthType Basic >> .htaccess
echo require user %var_dir% >> .htaccess

if exist .htpasswd del .htpasswd
htpasswd -bc .htpasswd %var_dir% %var_pass%

if exist go_ftp.bat del go_ftp.bat
echo open casablanca.solutionpark.ch >> go_ftp.bat
echo user scommunicator pond37 >> go_ftp.bat
echo cd www/keys >> go_ftp.bat
echo mkdir %var_dir% >> go_ftp.bat
echo cd %var_dir% >> go_ftp.bat
echo type binary >> go_ftp.bat
echo put %1 >> go_ftp.bat
REM echo type ascii >> go_ftp.bat
echo put .htpasswd >> go_ftp.bat
echo put .htaccess >> go_ftp.bat
echo quit >> go_ftp.bat
ftp -n -s:go_ftp.bat

