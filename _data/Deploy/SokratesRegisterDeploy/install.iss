;
; SokratesXP installation script file (Inno Setup)
;
; SEE THE DOCUMENTATION FOR DETAILS ON CREATING .ISS SCRIPT FILES!



[Setup]
OutputBaseFilename=sokrates_register
AppName=SOKRATES Register
AppVerName=SOKRATES Register
AppCopyright=Copyright � 2007 Helix Business Soft development team
DefaultDirName={tmp}
DefaultGroupName=SOKRATES Register
;it is possible to skip creating program group
AllowNoIcons=yes
PrivilegesRequired=none
Uninstallable=yes
DisableDirPage=yes
DisableFinishedPage=yes
DisableProgramGroupPage=yes
DisableReadyPage=yes
DisableReadyMemo=yes
SetupIconFile=sokrates.ico

[Messages]
WelcomeLabel1=Register your SOKRATES� software
WelcomeLabel2=
ClickNext=Click Next to continue, or Cancel to exit the Registration Utility
ExitSetupMessage= Are you sure you want to cancel the registration process?


[Files]
;Core:
Source: "Sokrates_Register.exe"; DestDir: "{app}"; Flags: promptifolder;
;//Source: "QtCore4.dll"; DestDir: "{app}"; Flags: ignoreversion;
;//Source: "QtGui4.dll"; DestDir: "{app}"; Flags: ignoreversion;
Source: "sokrates.key"; DestDir: "{app}"; Flags: ignoreversion skipifsourcedoesntexist;
Source: "network_connections.cfg"; DestDir: "{app}"; Flags: ignoreversion skipifsourcedoesntexist;


;Side by Side, private assembly by MS guidelines (this will fail to work on Win98,Me)
;Do not use vcredist_x86.exe, because we do not want to make our installation to modify system files
;All manifests & dll's must be copied into plugins subfolders (coz they can not use it from there)
;//Source: "Microsoft.VC80.CRT\*.*"; DestDir: "{app}"; Flags: recursesubdirs;


[Run]
;Filename: "{app}\uninstall.bat";  WorkingDir: "{app}"; Flags: waituntilterminated
Filename: "{app}\Sokrates_Register.exe";  WorkingDir: "{app}"; Flags: waituntilterminated  hidewizard; Check: CopyDLL


[Code]
function CopyFiles(regValue: String): Boolean;
var
	installDir: String;
begin

      installDir:= ExpandConstant('{app}');

     if FileCopy(regValue+'\QtCore4.dll', installDir++'\QtCore4.dll', False)=False then
     begin
			Result := False;
			Exit;
     end
     if FileCopy(regValue+'\QtGui4.dll', installDir++'\QtGui4.dll', False)=False then
     begin
			Result := False;
			Exit;
     end
     if FileCopy(regValue+'\msvcr80.dll', installDir++'\msvcr80.dll', False)=False then
     begin
			Result := False;
			Exit;
     end
     if FileCopy(regValue+'\msvcp80.dll', installDir++'\msvcp80.dll', False)=False then
     begin
			Result := False;
			Exit;
     end
     if FileCopy(regValue+'\msvcm80.dll', installDir++'\msvcm80.dll', False)=False then
     begin
			Result := False;
			Exit;
     end
     if FileCopy(regValue+'\Microsoft.VC80.CRT.manifest', installDir++'\Microsoft.VC80.CRT.manifest', False)=False then
     begin
			Result := False;
			Exit;
     end

     Result := True;
end;


function CopyDLL(): Boolean;
var
	regValue: String;
begin

  //find SOKRATES dir installation
  if RegQueryStringValue(HKEY_LOCAL_MACHINE, 'Software\Helix Business Soft\SOKRATES Communicator Team Edition\Settings','InstallPath', regValue) then
  begin
			Result := CopyFiles(regValue);
			if Result = False then
        MsgBox('Failed to find SOKRATES installation. Aborting registration!', mbInformation, MB_OK);
			Exit;
  end
  if RegQueryStringValue(HKEY_LOCAL_MACHINE, 'Software\Helix Business Soft\SOKRATES Communicator Personal Edition\Settings','InstallPath', regValue) then
  begin
			Result := CopyFiles(regValue);
			if Result = False then
        MsgBox('Failed to find SOKRATES installation. Aborting registration!', mbInformation, MB_OK);
			Exit;
  end
  if RegQueryStringValue(HKEY_LOCAL_MACHINE, 'Software\Helix Business Soft\SOKRATES Communicator Business Edition\Settings','InstallPath', regValue) then
  begin
			Result := CopyFiles(regValue);
			if Result = False then
        MsgBox('Failed to find SOKRATES installation. Aborting registration!', mbInformation, MB_OK);
			Exit;
  end

  MsgBox('Failed to find SOKRATES installation. Aborting registration!', mbInformation, MB_OK);
  Result := False;
end;
  







