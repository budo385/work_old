//--------------------------------------------------------------------
//				BUSINESS CALL INTERFACE
//--------------------------------------------------------------------
//All functions are async calls
//Return status is stored in variables: nLastBO_ErrorCode & strLastBO_ErrorText
//Return data is pass to the call back function. call back func must check errorcode to determine if call was OK
//nLastBO_ErrorCode == 0 : call ok, !=0 : error detected. nLastBO_ErrorCode == 1000 -> login failed.

//FUNCTION INTERFACE:
//1. BO_Call(strHttpMethod,strURL,pfCallBack,dataToSend);  
//2. BO_Login(strURL,user,pass,pfCallBack);


//for REST service use /rest for login entry point as strURL


var strUserName='';
var strPassword='';
var nLastBO_ErrorCode=0;
var strLastBO_ErrorText='';

var strServerNonce='';
var nLoginTries=0;
var strAuthToken='';
var nUseStoredPassHash=0;

var functCallBack_Login=null;
var functCallBack_BOCall=null;

var nBOCallTries=0;
var strLastBOURL='';
var strLastBOURLMethod='';
var strLastBOURLDataToSend='';





//--------------------------------------------------------------------
//				BUSINESS CALL HANDLER
//--------------------------------------------------------------------
// Makes async BO call, calls pfCallBack function when finish with returned data
// nLastBO_ErrorCode and strLastBO_ErrorText contain last error: if nLastBO_ErrorCode <> 0 then error
// Automatically uses prviously stored username/password for re-authentication, if Login failed nLastBO_ErrorCode=1000



//strHttpMethod - GET or other HTTP method
//strURL 		- relative URL to REST_WEB_ROOT_URL
//pfCallBack 	- function called when call is finished, function must accept data string result and test nLastBO_ErrorCode to see if call succeed
//data 			- request xml body, leave empty if no request body needed

function BO_Call(strHttpMethod,strURL,pfCallBack,dataToSend) 
{
	functCallBack_BOCall=pfCallBack;
	strLastBOURLMethod=strHttpMethod;
	nBOCallTries=0;
	strURL=REST_WEB_ROOT_URL+strURL;
	strLastBOURL=strURL;
	strLastBOURLDataToSend=dataToSend;
	
	//alert(strHttpMethod);
	
	if(dataToSend.length>0)
	{
	 $.ajax({		   
		   type: strHttpMethod,
		   url: strURL,
		   dataType: "text",
		   processData: 0,
		   data: dataToSend,
   		   success: BO_Call_success,
		   beforeSend: BO_Call_beforeSend,
		   error: BO_Call_error
		});	
	
	}
	else
	{
	 $.ajax({		   
		   type: strHttpMethod,
		   url: strURL,
		   dataType: "text",
		   success: BO_Call_success,
		   beforeSend: BO_Call_beforeSend,
		   error: BO_Call_error
		});
	}
}

function BO_Call_success(data, textStatus) 
{
	//alert('BO call sucess '+data.length);
	functCallBack_BOCall(data);
}

function BO_Call_beforeSend(xhr) 
{
	xhr.setRequestHeader("Authorization", strAuthToken); 
}
function BO_Call_error(xhr, textStatus, errorThrown) 
{
	//alert('BO call error '+xhr.status);
	
	if(xhr.status == 401 && nBOCallTries < 1)
	{
		nBOCallTries++;
		//alert("Use login again!!!");
		BO_Login('','',BO_Call_TryAgain);
		return; 
	}
	
	//alert('error BO');
	//parse error then return:
	BO_ParseError(xhr,textStatus);
	functCallBack_BOCall(textStatus);
}

//try again to call BO:
function BO_Call_TryAgain(data) 
{
	//alert('BO_Call_TryAgain');
	
	if(nLastBO_ErrorCode==0)
	{
		if(strLastBOURLDataToSend.length>0)
		{
		 $.ajax({		   
			   type: strLastBOURLMethod,
			   url: strLastBOURL,
			   dataType: "text",
			   data: strLastBOURLDataToSend,
			   processData: false,
			   success: BO_Call_success,
			   beforeSend: BO_Call_beforeSend,
			   error: BO_Call_error
			});	
		}
		else
		{
			 $.ajax({		   
			   type: strLastBOURLMethod,
			   url: strLastBOURL,
			   dataType: "text",
			   success: BO_Call_success,
			   beforeSend: BO_Call_beforeSend,
			   error: BO_Call_error
			});
		}
		return; 
	}
	
	//if failed again just pass error text
	functCallBack_BOCall(strLastBO_ErrorText);
}




//--------------------------------------------------------------------
//				LOGIN HANDLER
//--------------------------------------------------------------------
// Logins on the server, makes async call, calls pfCallBack function when finish
// nLastBO_ErrorCode and strLastBO_ErrorText contain last error: if nLastBO_ErrorCode <> 0 then error
// if Login failed nLastBO_ErrorCode=1000
// pfCallBack receives http body data if any


//user		 	- username
//pass 			- password
//pfCallBack 	- function called when call is finished, function must accept data string result and test nLastBO_ErrorCode to see if call succeed, if nLastBO_ErrorCode=1000 -> login failed

function BO_Login(user,pass,pfCallBack) 
{
	//alert("Login GO GO");
	

	if(user=='' && pass=='')
	{
		//alert("Use stored login");
		nUseStoredPassHash=1;
	}
	else
	{
		nUseStoredPassHash=0;
		strUserName=user;
		strPassword=pass;
	}
	
	
	nLoginTries=0;
	nLastBO_ErrorCode=0;
	strLastBO_ErrorText='';
	functCallBack_Login=pfCallBack;

	//alert(' login at' + REST_WEB_ROOT_URL);
	
	 $.ajax({		   
		   type: "GET",
		   url: REST_WEB_ROOT_URL,
		   dataType: "text",
		   success: BO_OnLogin_success,
		   beforeSend: BO_OnLogin_beforeSend,
		   error: BO_OnLogin_error
		});
}

function BO_OnLogin_success(data, textStatus) 
{
	//alert("login sucess");
	
	//alert(textStatus);
	nLastBO_ErrorCode=0;
	strLastBO_ErrorText='';
	//alert("Sucess");
	if (functCallBack_Login!=null)
		functCallBack_Login(data);
}

function BO_OnLogin_beforeSend(xhr) 
{
	//alert("login before send");
	if(nUseStoredPassHash!=1)
	{
		$.cookie('strAuthToken', null);
		$.cookie("strUser", null);
		$.cookie("strPassToken", null);
	
		var bytePassHash=strUserName+":"+strPassword;
		bytePassHash=SHA256(bytePassHash);
		var authToken=strUserName+":"+bytePassHash;
		$.cookie("strPassToken", authToken);
		
		var authToken=authToken+":"+strServerNonce;
		authToken=SHA256(authToken);
		strAuthToken='SokratesAuth user="'+strUserName+'" token="'+authToken+'" nonce="'+strServerNonce+'"';
		
		//save all in temp cookie for next page:	
		$.cookie('strAuthToken', strAuthToken);
		$.cookie("strUser", strUserName);
		
	}
	else
	{
		if(strServerNonce.length>0)
		{
			//alert("Use stored 1");
			strUserName=$.cookie("strUser");	
			//alert(strUserName);
			//alert($.cookie("strPassToken"));
			var authToken=$.cookie("strPassToken")+":"+strServerNonce;
			authToken=SHA256(authToken);
			
			strAuthToken='SokratesAuth user="'+strUserName+'" token="'+authToken+'" nonce="'+strServerNonce+'"';
			$.cookie("strAuthToken", strAuthToken);
		}
		else
		{
			//alert("Use stored 2");
			strAuthToken=$.cookie('strAuthToken');
			//alert(strAuthToken);
		}
	}
	
	
	//alert(strAuthToken);
	xhr.setRequestHeader("Authorization", strAuthToken); 
}

function BO_OnLogin_error(xhr, textStatus, errorThrown) 
{
	//alert("login error "+textStatus);
	//alert(xhr.statusText);
	
	//if 401 then handle login, in all other cases: show default error page
	if(xhr.status == 401 && nLoginTries < 1)
	{
		//WWW-Authenticate: SokratesAuth nonce="84e0a095cfd25153b2e4014ea87a0980"
		var AuthHeader = xhr.getResponseHeader("WWW-Authenticate");
		var arrayOfStrings=AuthHeader.split("\"");
		
		nLoginTries++;
		
		if (arrayOfStrings.length >=2)
		{
			strServerNonce = arrayOfStrings[1];
			
			//alert('send:'+strServerNonce);
			
			 $.ajax({		   
			   type: "GET",
			   url: REST_WEB_ROOT_URL,
			   dataType: "text",
			   success: BO_OnLogin_success,
			   beforeSend: BO_OnLogin_beforeSend,
			   error: BO_OnLogin_error
			});
			
			return;
		} 
	}
	
	BO_ParseError(xhr,textStatus);
	//alert(nLastBO_ErrorCode);
	$.cookie('strAuthToken', null);
	$.cookie("strUser", null);
	$.cookie("strPassToken", null);
	
	if (functCallBack_Login!=null)
		functCallBack_Login(textStatus);
}


function BO_ParseError(xhr,textStatus) 
{
	if(xhr.status == 401)
	{
		nLastBO_ErrorCode=1000;
		strLastBO_ErrorText="Login failed! Invalid username or password!";
	}
	else
	{
		if(BO_ParseErrorBody(xhr.responseText)==0)
		{
			nLastBO_ErrorCode=1;	//general error: can be timeout or something:
			strLastBO_ErrorText=xhr.responseText;
			if(strLastBO_ErrorText.length == 0)
				strLastBO_ErrorText=textStatus;
		}
	}
}

function BO_ParseErrorBody(xml_body) 
{
	if(xml_body.length == 0)
		return 0;
	nLastBO_ErrorCode=$(xml_body).find("error_code").text();
	strLastBO_ErrorText=$(xml_body).find("error_text").text();
	if (nLastBO_ErrorCode>0)
		return 1;
	else
		return 0;
}



//--------------------------------------------------------------------
//				XML DECODER:
//--------------------------------------------------------------------

//------------------BASE64 EN/DECODER------------------------------------
//use functions from base64.js:
//$.base64Encode("I'm Persian.");
//$.base64Decode("SSdtIFBlcnNpYW4u");


//------------------STRING EN/DECODER------------------------------------

var AMP = '&';
var rawEntity = [ "&",      "<",    ">",     "\'",     "\""];
var xmlEntity = [ "&amp;", "&lt;", "&gt;",  "&apos;", "&quot;"];

//xml- string from server to decode
//returns decoded string
function xmlDecode(xml)
{
	// check if no AMP there is no need to decode
	var pos=xml.indexOf(AMP);
	if(pos<0) return xml;

	var byteDecoded=xml;

	// iterate through list backwards: its very important to pars &amp last
	for (var iEntity=4; iEntity>=0; iEntity--)
		byteDecoded.replace(xmlEntity[iEntity],rawEntity[iEntity]);

	return byteDecoded;
}

//raw- string to encode for sending to the server
//returns encoded string
function xmlEncode(raw)
{

	var strEncoded=raw;

	// iterate through list forwad: its very important to encode &amp first
	if(strEncoded.indexOf("&")>=0)strEncoded.replace("&","&amp;");
	if(strEncoded.indexOf('<')>=0)strEncoded.replace("<","&lt;");
	if(strEncoded.indexOf(">")>=0)strEncoded.replace(">","&gt;");
	if(strEncoded.indexOf("\'")>=0)	strEncoded.replace("\'","&apos;");
	if(strEncoded.indexOf("\"")>=0)	strEncoded.replace("\"","&quot;");

	//	for (uint iEntity=2; iEntity<5; ++iEntity)
	//		strEncoded.replace(rawEntity[iEntity],xmlEntity[iEntity]);

	return strEncoded;
}


//------------------DATE EN/DECODER------------------------------------

//dateString in YYYY-MM-DD incoming format, return date variable
function dateDecode(dateString) 
{
/*
	var arrayOfStrings=dateString.split("-");
	if(arrayOfStrings.length!=3)
		return new Date; //empty date;
		
	alert(dateString);
*/
	var d =  new Date(dateString);
	return d;
}

