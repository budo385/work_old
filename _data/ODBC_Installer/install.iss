;
; ODBC installation script file (Inno Setup)
;

[Setup]
OutputBaseFilename=SokSCODBC_Setup
AppName=SOKRATES Communicator ODBC Setup
AppVerName=SOKRATES Communicator ODBC Setup
AppCopyright=Copyright � 2012 Helix Business Soft
DefaultDirName={tmp}
DefaultGroupName=SOKRATES Communicator ODBC Setup
;it is possible to skip creating program group
AllowNoIcons=yes
PrivilegesRequired=admin
DisableDirPage=yes
DisableFinishedPage=yes
DisableProgramGroupPage=yes
DisableReadyPage=yes
DisableReadyMemo=yes

[Code]
var
  bSetupRun: Boolean;
	strODBCSource: String;
	strDescription: String;
  strDatabase: String;
  strClientDLLPath: String;

function ReadIniData(): Boolean;
var
	strIniPath: String;
begin
  // calculate .ini path
  strIniPath := ExpandConstant('{src}') + '\setup.ini';

  strODBCSource    := GetIniString('Setup', 'DataSourceName',  '', strIniPath);
  strDescription   := GetIniString('Setup', 'Description', '', strIniPath);
  strDatabase      := GetIniString('Setup', 'Database',    '', strIniPath);
  strClientDLLPath := GetIniString('Setup', 'ClientDLL',   '', strIniPath);

  // MsgBox(strIniPath, mbInformation, MB_OK);
  // MsgBox(strDatabase, mbInformation, MB_OK);
  Result := True;
end;

function DetectVistaOrWindows7(): Boolean;
var
  Version: TWindowsVersion;
begin
  GetWindowsVersionEx(Version);

  // http://stackoverflow.com/questions/5115023/problem-with-checking-the-windows-version-from-inno-setup
  // Windows 7 version is 6.1 (workstation)
  // Windows 2003 version is 5.2 (server)
  if (Version.Major > 5)
  then
    Result := True
  else
    Result := False;
end;

function GetODBCSource(Param: String): String;
begin
  Result := strODBCSource;
end;

function GetClientDLL(Param: String): String;
begin
  Result := strClientDLLPath;
end;

function GetDescription(Param: String): String;
begin
  Result := strDescription;
end;

function GetDatabase(Param: String): String;
begin
  Result := strDatabase;
end;

// Param is required to call the function from the [Run] section
function GetODBCToolPath(Param: String): String;
begin
  if (IsWin64())
  then begin
    //use 32-bit tool on 64-bit platform
    Result := GetSysWow64Dir() + '\odbcad32.exe';
  end else begin
    if (DetectVistaOrWindows7())
    then begin
      Result := GetSystemDir() + '\odbcad32.exe';
    end else begin
      Result := GetSystemDir() + '\odbcad32.exe';
    end;
  end;
end;

function GetDriverFirebird32(Param: String): String;
begin
  if (IsWin64())
  then begin
    //use 32-bit tool on 64-bit platform
    Result := GetSysWow64Dir() + '\OdbcFb.dll';
  end else begin
    if (DetectVistaOrWindows7())
    then begin
      Result := GetSystemDir() + '\OdbcFb.dll';
    end else begin
      Result := GetSystemDir() + '\OdbcFb.dll';
    end;
  end;
end;

function InitializeSetup(): Boolean;
begin
  //http://forums.contractoruk.com/technical/63913-odbcad32-exe-win-7-enterprise-adding-user-sql-server-dsn.html
  ReadIniData();

  if (strODBCSource = '') then
  begin
    MsgBox('Data source name was not defined.'+ #13+#10+'Please check your setup.ini file and restart the installation.', mbError, MB_OK);
    Result := false; 
    bSetupRun := false;
  end else begin
    Result := true;
    bSetupRun := true;
  end;
end;

[Files]
Source: "Firebird_ODBC_2.0.2.153_Win32.exe"; DestDir: "{app}"; Flags: deleteafterinstall;

[Run]
;Filename: "{app}\Firebird_ODBC_2.0.0.151_Win32.exe"; WorkingDir: "{app}"; Flags: waituntilterminated hidewizard;
; invisible
Filename: "{app}\Firebird_ODBC_2.0.2.153_Win32.exe"; Parameters: "/SILENT"; WorkingDir: "{app}"; Flags: waituntilterminated runhidden;
; no need to start the ODBC admin
;Filename: "{code:GetODBCToolPath}";  WorkingDir: "{app}"; Flags: waituntilterminated hidewizard;

;The user DSNs are stored under the following registry subkey:
;HKEY_CURRENT_USER\Software\ODBC\ODBC.INI
;To work around this problem, use the appropriate version of the ODBC Administrator tool. If you build and then run an application as a 32-bit application on a 64-bit operating system, you must create the ODBC data source by using the ODBC Administrator tool in %windir%\SysWOW64\odbcad32.exe. To indicate the type of DSN, you can add "_32" to the 32-bit user DSNs and "_64" to the 64-bit user DSNs.
[Registry]
; name index
Root: HKCU; SubKey: Software\ODBC\ODBC.INI\ODBC Data Sources; ValueType: string; ValueName: {code:GetODBCSource}; ValueData: Firebird/InterBase(r) driver; Flags: createvalueifdoesntexist uninsdeletevalue
; ODBC definition
;Root: HKCU; SubKey: Software\ODBC\ODBC.INI\{code:GetODBCSource}; ValueType: string; ValueName: CharacterSet; ValueData: UTF8; Flags: uninsdeletevalue
Root: HKCU; SubKey: Software\ODBC\ODBC.INI\{code:GetODBCSource}; ValueType: string; ValueName: CharacterSet; ValueData: ISO8859_1; Flags: uninsdeletevalue
Root: HKCU; SubKey: Software\ODBC\ODBC.INI\{code:GetODBCSource}; ValueType: string; ValueName: Dialect; ValueData: 3; Flags: uninsdeletevalue
Root: HKCU; SubKey: Software\ODBC\ODBC.INI\{code:GetODBCSource}; ValueType: string; ValueName: Driver; ValueData: {code:GetDriverFirebird32}; Flags: uninsdeletevalue
; HARDCODED db user name: SOKRATES
Root: HKCU; SubKey: Software\ODBC\ODBC.INI\{code:GetODBCSource}; ValueType: string; ValueName: User; ValueData: SOKRATES; Flags: uninsdeletevalue
; HARDCODED db password: xan09 (encoded form required by registry)
Root: HKCU; SubKey: Software\ODBC\ODBC.INI\{code:GetODBCSource}; ValueType: string; ValueName: Password; ValueData: ALEBMNAPHOIIJECGAOENAGOHIOANBMIDDLOGAMOKABIOEPALLJIONBKHAKIIAMCFAAAJFHIJHIGIAICGNBANNLDMCN; Flags: uninsdeletevalue
; db path: (read from .ini)
Root: HKCU; SubKey: Software\ODBC\ODBC.INI\{code:GetODBCSource}; ValueType: string; ValueName: Dbname; ValueData: {code:GetDatabase}; Flags: uninsdeletevalue
; client .dll name: (read from .ini)
Root: HKCU; SubKey: Software\ODBC\ODBC.INI\{code:GetODBCSource}; ValueType: string; ValueName: Client; ValueData: {code:GetClientDLL}; Flags: uninsdeletevalue
; description:  (read from .ini)
Root: HKCU; SubKey: Software\ODBC\ODBC.INI\{code:GetODBCSource}; ValueType: string; ValueName: Description; ValueData: {code:GetDescription}; Flags: uninsdeletevalue
