;
; SokratesXP installation script file (Inno Setup)
;
; SEE THE DOCUMENTATION FOR DETAILS ON CREATING .ISS SCRIPT FILES!

[Setup]
AppName=SOKRATES� Communicator
AppVerName=SOKRATES� Communicator
AppendDefaultDirName=false
AppCopyright=Copyright � 2016 Helix Business Soft development team
DefaultDirName={pf}
DefaultGroupName=SOKRATES Communicator
UninstallDisplayIcon={app}\sokrates.exe
PrivilegesRequired=none
;it is possible to skip creating program group
AllowNoIcons = yes
UsePreviousAppDir = yes
SetupIconFile = sokrates.ico
LicenseFile = license.txt
ShowLanguageDialog = yes
UninstallDisplayName =  {code:GetVersionName}
;issue 2736
DirExistsWarning = no 

[Languages]
Name: "en"; MessagesFile: "compiler:Default.isl,Sokrates_English.isl";
Name: "de"; MessagesFile: "compiler:Languages\German.isl,Sokrates_German.isl";

[Messages]
en.SelectDirBrowseLabel=To continue, click Next. If you would like to select a different folder, click Browse.%nYou'll need admin rights if you want to install the program within "Program Files" folder.
de.SelectDirBrowseLabel=Dr�cken Sie �Weiter� um weiterzufahren. Falls Sie ein anderes Verzeichnis ausw�hlen m�chten, dr�cken Sie �Durchsuchen�. %n Sie brauchen Administratorenrechte, falls Sie das das Programm im Verzeichnis �C:\Programme� installieren m�chten.

[Tasks]
Name: desktopicon; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Check: NOT(IsVersionPortable);
Name: quicklaunchicon; Description: "{cm:CreateQuickLaunchIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked; Check: NOT(IsVersionPortable);
Name: startmenuicon; Description: "{cm:Sokrates_CreateStartMenuIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked; Check: NOT(IsVersionPortable);

[Files]
;Core:
Source: "sokrates.exe"; DestDir: "{app}"; Flags: promptifolder;
;Source: "assistant.exe"; DestDir: "{app}"; Flags: promptifolder;

Source: "*.dll"; DestDir: "{app}"; Flags: ignoreversion;
;Source: "*.chm"; DestDir: "{app}"; Flags: ignoreversion;
Source: "ding.wav"; DestDir: "{app}"; Flags: ignoreversion;

;Source: "help\*.*"; DestDir: "{app}\help"; Flags: recursesubdirs;
Source: "support\NV_support.exe"; DestDir: "{app}\support"; Flags: ignoreversion;
;Source: "firebird\*.*"; DestDir: "{app}"; Flags: recursesubdirs ignoreversion;
Source: "data\*.*"; DestDir: "{app}\data"; Flags: recursesubdirs ignoreversion;
Source: "template_data\*.*"; DestDir: "{app}\template_data"; Flags: recursesubdirs ignoreversion;
Source: "report_definitions\*.*"; DestDir: "{app}\report_definitions"; Flags: recursesubdirs ignoreversion;
Source: "themes\*.*"; DestDir: "{app}\themes"; Flags: recursesubdirs ignoreversion;
;Source: "phonon_backend\*.*"; DestDir: "{app}\phonon_backend"; Flags: recursesubdirs ignoreversion;
Source: "sokrates_de.qm"; DestDir: "{app}"; Flags: ignoreversion ignoreversion;
Source: "*.hed"; DestDir: "{app}"; Flags: ignoreversion ignoreversion;
Source: "sok.dat"; DestDir: "{app}"; Flags: ignoreversion ignoreversion;
Source: "Sokrates_Progress.exe"; DestDir: "{app}"; Flags: ignoreversion;
Source: "unzip.exe"; DestDir: "{app}"; Flags: ignoreversion;
Source: "zip.exe"; DestDir: "{app}"; Flags: ignoreversion;
Source: "sokrates.pdb"; DestDir: "{app}"; Flags: ignoreversion;


Source: "imageformats\*.*"; DestDir: "{app}\imageformats"; Flags: recursesubdirs ignoreversion;
Source: "sqldrivers\*.*"; DestDir: "{app}\sqldrivers"; Flags: recursesubdirs ignoreversion;
Source: "platforms\*.*"; DestDir: "{app}\platforms"; Flags: recursesubdirs ignoreversion;
Source: "playlistformats\*.*"; DestDir: "{app}\playlistformats"; Flags: recursesubdirs ignoreversion;
Source: "printsupport\*.*"; DestDir: "{app}\printsupport"; Flags: recursesubdirs ignoreversion;
Source: "qmltooling\*.*"; DestDir: "{app}\qmltooling"; Flags: recursesubdirs ignoreversion;
Source: "phonon_backend\*.*"; DestDir: "{app}\phonon_backend"; Flags: recursesubdirs ignoreversion;
;needed for progres exe + qt4core & gui dll

;Side by Side, private assembly by MS guidelines (this will fail to work on Win98,Me)
;Do not use vcredist_x86.exe, because we do not want to make our installation to modify system files
;All manifests & dll's must be copied into plugins subfolders (coz they can not use it from there)
Source: "Microsoft.CRT\*.*"; DestDir: "{app}"; Flags: recursesubdirs;
Source: "Microsoft.CRT\*.*"; DestDir: "{app}\sqldrivers"; Flags: recursesubdirs;
Source: "Microsoft.CRT\*.*"; DestDir: "{app}\imageformats"; Flags: recursesubdirs;
Source: "Microsoft.CRT\*.*"; DestDir: "{app}\platforms"; Flags: recursesubdirs;
Source: "Microsoft.CRT\*.*"; DestDir: "{app}\playlistformats"; Flags: recursesubdirs;
Source: "Microsoft.CRT\*.*"; DestDir: "{app}\printsupport"; Flags: recursesubdirs;
Source: "Microsoft.CRT\*.*"; DestDir: "{app}\qmltooling"; Flags: recursesubdirs;
Source: "Microsoft.VC90.CRT\*.*"; DestDir: "{app}"; Flags: recursesubdirs;
Source: "Microsoft.VC90.CRT\*.*"; DestDir: "{app}\phonon_backend"; Flags: recursesubdirs;
;needed for progres exe + qt4core & gui dll


;Configuration
Source: "settings\sokrates.key"; DestDir: "{app}\template_settings"; Flags: ignoreversion;
Source: "settings\network_connections.cfg"; DestDir: "{app}\template_settings"; Flags: ignoreversion;
Source: "settings\client_pe.ini"; DestDir: "{app}\template_settings"; DestName: "client.ini"; Flags: ignoreversion; Check: GetPeFileCopyFlags(''); AfterInstall: SetLanguage(0)
Source: "settings\client_te.ini"; DestDir: "{app}\template_settings"; DestName: "client.ini"; Flags: ignoreversion; Check: GetTeFileCopyFlags(''); AfterInstall: SetLanguage(0)
Source: "settings\client_be.ini"; DestDir: "{app}\template_settings"; DestName: "client.ini"; Flags: ignoreversion; Check: GetBeFileCopyFlags(''); AfterInstall: SetLanguage(0)
;this is essential on first installation->to make registration sucess (if install+registration in one step)
Source: "settings\client_pe.ini"; DestDir: "{code:GetSettngsDirectory}\settings"; DestName: "client.ini"; Flags: ignoreversion onlyifdoesntexist; Check: GetPeFileCopyFlags(''); AfterInstall: SetLanguage(1)
Source: "settings\client_te.ini"; DestDir: "{code:GetSettngsDirectory}\settings"; DestName: "client.ini"; Flags: ignoreversion onlyifdoesntexist; Check: GetTeFileCopyFlags(''); AfterInstall: SetLanguage(1)
Source: "settings\client_be.ini"; DestDir: "{code:GetSettngsDirectory}\settings"; DestName: "client.ini"; Flags: ignoreversion onlyifdoesntexist; Check: GetBeFileCopyFlags(''); AfterInstall: SetLanguage(1)
Source: "settings\version_portable"; DestDir: "{code:GetSettngsDirectory}\template_settings"; DestName: "version_portable"; Flags: ignoreversion onlyifdoesntexist; Check: IsVersionPortable;


;issue 1412: delete old templates:
[InstallDelete]
Type: filesandordirs; Name: "{app}\template_data";
Type: filesandordirs; Name: "{app}\imageformats";
Type: filesandordirs; Name: "{app}\platforms";
Type: filesandordirs; Name: "{app}\playlistformats";
Type: filesandordirs; Name: "{app}\printsupport";
Type: filesandordirs; Name: "{app}\qmltooling";


 ;delete content of settings directory -> instructed by MB (Brane mail, from 22.05.07)
[UninstallDelete]
Type: filesandordirs; Name: "{code:GetSettngsDirectory}";
;Type: files; Name: "{code:GetSettngsDirectory}\settings\sokrates.key";
;Type: files; Name: "{code:GetSettngsDirectory}\settings\network_connections.cfg";
;Type: files; Name: "{code:GetSettngsDirectory}\settings\database_connections.cfg";
;Type: files; Name: "{code:GetSettngsDirectory}\settings\client.log";
;Type: files; Name: "{code:GetSettngsDirectory}\settings\client.ini";
;Type: files; Name: "{code:GetSettngsDirectory}\data\DATA.FDB";
;Type: dirifempty ; Name: "{code:GetSettngsDirectory}\data";


[Registry]
;register installation path->remove on deinstall
Root: HKLM; Subkey: "Software\Helix Business Soft\SOKRATES Communicator Personal Edition\Settings"; ValueType: string; ValueName: "InstallPath"; ValueData: "{app}"; Flags: uninsdeletekey; Check: GetPeFileCopyFlags('WriteRegistry')
Root: HKLM; Subkey: "Software\Helix Business Soft\SOKRATES Communicator Team Edition\Settings"; ValueType: string; ValueName: "InstallPath"; ValueData: "{app}"; Flags: uninsdeletekey; Check: GetTeFileCopyFlags('WriteRegistry')
Root: HKLM; Subkey: "Software\Helix Business Soft\SOKRATES Communicator Business Edition\Settings"; ValueType: string; ValueName: "InstallPath"; ValueData: "{app}"; Flags: uninsdeletekey; Check: GetBeFileCopyFlags('WriteRegistry')



[Icons]
Name: "{group}\SOKRATES Communicator Personal Edition"; Filename: "{app}\sokrates.exe"; WorkingDir: "{app}";  Check: GetPeFileCopyFlags('UseIcon')
Name: "{group}\SOKRATES Communicator Team Edition"; Filename: "{app}\sokrates.exe"; WorkingDir: "{app}";  Check: GetTeFileCopyFlags('UseIcon')
Name: "{group}\SOKRATES Communicator Business Edition"; Filename: "{app}\sokrates.exe"; WorkingDir: "{app}";  Check: GetBeFileCopyFlags('UseIcon')

Name: "{group}\Uninstall SOKRATES Communicator Personal Edition"; Filename: "{uninstallexe}";  Check: GetPeFileCopyFlags('UseIcon')
Name: "{group}\Uninstall SOKRATES Communicator Team Edition"; Filename: "{uninstallexe}";  Check: GetTeFileCopyFlags('UseIcon')
Name: "{group}\Uninstall SOKRATES Communicator Business Edition"; Filename: "{uninstallexe}";  Check: GetBeFileCopyFlags('UseIcon')

Name: "{userappdata}\Microsoft\Internet Explorer\Quick Launch\SOKRATES Communicator Personal Edition"; Filename: "{app}\sokrates.exe"; Tasks: quicklaunchicon;  WorkingDir: "{app}";  Check: GetPeFileCopyFlags('UseIcon')
Name: "{userappdata}\Microsoft\Internet Explorer\Quick Launch\SOKRATES Communicator Team Edition"; Filename: "{app}\sokrates.exe"; Tasks: quicklaunchicon;  WorkingDir: "{app}";  Check: GetTeFileCopyFlags('UseIcon')
Name: "{userappdata}\Microsoft\Internet Explorer\Quick Launch\SOKRATES Communicator Business Edition"; Filename: "{app}\sokrates.exe"; Tasks: quicklaunchicon;  WorkingDir: "{app}";  Check: GetBeFileCopyFlags('UseIcon')

Name: "{userdesktop}\SOKRATES Communicator Personal Edition"; Filename: "{app}\sokrates.exe"; Tasks: desktopicon; WorkingDir: "{app}";  Check: GetPeFileCopyFlags('UseIcon')
Name: "{userdesktop}\SOKRATES Communicator Team Edition"; Filename: "{app}\sokrates.exe"; Tasks: desktopicon; WorkingDir: "{app}";  Check: GetTeFileCopyFlags('UseIcon')
Name: "{userdesktop}\SOKRATES Communicator Business Edition"; Filename: "{app}\sokrates.exe"; Tasks: desktopicon; WorkingDir: "{app}";  Check: GetBeFileCopyFlags('UseIcon')

Name: "{userstartmenu}\SOKRATES Communicator Personal Edition"; Filename: "{app}\sokrates.exe"; Tasks: startmenuicon; WorkingDir: "{app}";  Check: GetPeFileCopyFlags('UseIcon')
Name: "{userstartmenu}\SOKRATES Communicator Team Edition"; Filename: "{app}\sokrates.exe"; Tasks: startmenuicon; WorkingDir: "{app}";  Check: GetTeFileCopyFlags('UseIcon')
Name: "{userstartmenu}\SOKRATES Communicator Business Edition"; Filename: "{app}\sokrates.exe"; Tasks: startmenuicon; WorkingDir: "{app}";  Check: GetBeFileCopyFlags('UseIcon')

[Run]
;Filename: "{app}\vcredist_x86_2008_sp1.exe"; Parameters: "/q"; WorkingDir: "{app}"; Flags: runhidden runascurrentuser;
Filename: "{code:LaunchApplication}";  Description: "{code:LaunchApplicationString}"; WorkingDir: "{app}"; Flags: postinstall nowait runascurrentuser;
;Check: NOT(IsWindowsVista)
;Filename: "{code:LaunchApplication}";  Description: "{code:LaunchApplicationString}"; WorkingDir: "{app}"; Flags: postinstall nowait unchecked; Check: IsWindowsVista


[Code]
#include "page1.isd"
//procedure InitializeWizard();
//var
//  LastId: Integer;
//begin
//  LastId := wpLicense;
//  LastId := CreatePage_Page1(LastId); // add custom page after the License wizard page
//  LastId := wpSelectDir;
//end;



