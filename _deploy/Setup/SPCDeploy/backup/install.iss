;
; SokratesXP installation script file (Inno Setup)
;
; SEE THE DOCUMENTATION FOR DETAILS ON CREATING .ISS SCRIPT FILES!

[Setup]
AppName=SOKRATES� Communicator
AppVerName=SOKRATES� Communicator
AppCopyright=Copyright � 2007 Helix Business Soft development team
DefaultDirName={pf}\SOKRATES Communicator
DefaultGroupName=SOKRATES Communicator
UninstallDisplayIcon={app}\sokrates.exe
PrivilegesRequired=none
;it is possible to skip creating program group
AllowNoIcons=yes
UsePreviousAppDir=no
SetupIconFile=sokrates.ico
LicenseFile=license.txt
ShowLanguageDialog=yes

[Languages]
Name: "en"; MessagesFile: "compiler:Default.isl,Sokrates_English.isl"
Name: "de"; MessagesFile: "compiler:Languages\German.isl,Sokrates_German.isl"

[Messages]
en.SelectDirBrowseLabel=To continue, click Next. If you would like to select a different folder, click Browse.%nYou'll need admin rights if you want to install the program within "Program Files" folder.
de.SelectDirBrowseLabel=Dr�cken Sie �Weiter� um weiterzufahren. Falls Sie ein anderes Verzeichnis ausw�hlen m�chten, dr�cken Sie �Durchsuchen�. %n Sie brauchen Administratorenrechte, falls Sie das das Programm im Verzeichnis �C:\Programme� installieren m�chten.

[Tasks]
Name: desktopicon; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}";
Name: quicklaunchicon; Description: "{cm:CreateQuickLaunchIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked
Name: startmenuicon; Description: "{cm:Sokrates_CreateStartMenuIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked

[Files]
;Core:
Source: "sokrates.exe"; DestDir: "{app}"; Flags: promptifolder;
Source: "assistant.exe"; DestDir: "{app}"; Flags: promptifolder;

Source: "*.dll"; DestDir: "{app}"; Flags: ignoreversion;
Source: "*.chm"; DestDir: "{app}"; Flags: ignoreversion;
Source: "ding.wav"; DestDir: "{app}"; Flags: ignoreversion;

;Source: "QtCore4.dll"; DestDir: "{app}"; Flags: ignoreversion;
;Source: "QtGui4.dll"; DestDir: "{app}"; Flags: ignoreversion;
;Source: "QtNetwork4.dll"; DestDir: "{app}"; Flags: ignoreversion;
;Source: "QtSql4.dll"; DestDir: "{app}"; Flags: ignoreversion;
;Source: "QtXml4.dll"; DestDir: "{app}"; Flags: ignoreversion;
;Source: "QtAssistantClient4.dll"; DestDir: "{app}"; Flags: ignoreversion;
;Source: "db_actualize.dll"; DestDir: "{app}"; Flags: ignoreversion;
;Source: "phonet.dll"; DestDir: "{app}"; Flags: ignoreversion;
;Source: "MAPIEx.dll"; DestDir: "{app}"; Flags: ignoreversion;
;Source: "TWXAPI32.DLL"; DestDir: "{app}"; Flags: ignoreversion;
;Source: "SHW32.DLL"; DestDir: "{app}"; Flags: ignoreversion;
;Source: "backup\*.*"; DestDir: "{app}\backup"; Flags: recursesubdirs;
Source: "help\*.*"; DestDir: "{app}\help"; Flags: recursesubdirs;
Source: "Support\NV_support.exe"; DestDir: "{app}\Support"; Flags: ignoreversion;
Source: "firebird\*.*"; DestDir: "{app}"; Flags: recursesubdirs;
Source: "data\*.*"; DestDir: "{app}\data"; Flags: recursesubdirs;
Source: "template_data\*.*"; DestDir: "{app}\template_data"; Flags: recursesubdirs;
Source: "imageformats\*.*"; DestDir: "{app}\imageformats"; Flags: recursesubdirs;
Source: "report_definitions\*.*"; DestDir: "{app}\report_definitions"; Flags: recursesubdirs;
Source: "sqldrivers\*.*"; DestDir: "{app}\sqldrivers"; Flags: recursesubdirs;
Source: "sokrates_de.qm"; DestDir: "{app}"; Flags: ignoreversion;

;Side by Side, private assembly by MS guidelines (this will fail to work on Win98,Me)
;Do not use vcredist_x86.exe, because we do not want to make our installation to modify system files
;All manifests & dll's must be copied into plugins subfolders (coz they can not use it from there)
Source: "Microsoft.VC80.CRT\*.*"; DestDir: "{app}"; Flags: recursesubdirs;
Source: "Microsoft.VC80.MFC\*.*"; DestDir: "{app}"; Flags: recursesubdirs;
Source: "Microsoft.VC80.CRT\*.*"; DestDir: "{app}\imageformats"; Flags: recursesubdirs;
Source: "Microsoft.VC80.CRT\*.*"; DestDir: "{app}\sqldrivers"; Flags: recursesubdirs;



;--------------------------------COPY TO BACUP FOLDER-------------------------------------
;Source: "sokrates.exe"; DestDir: "{app}\backup"; Flags: promptifolder;
;Source: "assistant.exe"; DestDir: "{app}\backup"; Flags: promptifolder;
;Source: "*.chm"; DestDir: "{app}\backup"; Flags: ignoreversion;
;Source: "*.dll"; DestDir: "{app}"; Flags: ignoreversion;
;Source: "help\*.*"; DestDir: "{app}\backup\help"; Flags: recursesubdirs;
;Source: "Support\NV_support.exe"; DestDir: "{app}\backup\Support"; Flags: ignoreversion;
;Source: "firebird\*.*"; DestDir: "{app}\backup"; Flags: recursesubdirs;
;Source: "data\*.*"; DestDir: "{app}\backup\data"; Flags: recursesubdirs;
;Source: "template_data\*.*"; DestDir: "{app}\backup\template_data"; Flags: recursesubdirs;
;Source: "imageformats\*.*"; DestDir: "{app}\backup\imageformats"; Flags: recursesubdirs;
;Source: "report_definitions\*.*"; DestDir: "{app}\report_definitions"; Flags: recursesubdirs;
;Source: "sqldrivers\*.*"; DestDir: "{app}\backup\sqldrivers"; Flags: recursesubdirs;
;Source: "sokrates_de.qm"; DestDir: "{app}\backup"; Flags: ignoreversion;
;Source: "Microsoft.VC80.CRT\*.*"; DestDir: "{app}\backup"; Flags: recursesubdirs;
;Source: "Microsoft.VC80.MFC\*.*"; DestDir: "{app}\backup"; Flags: recursesubdirs;
;Source: "Microsoft.VC80.CRT\*.*"; DestDir: "{app}\backup\imageformats"; Flags: recursesubdirs;
;Source: "Microsoft.VC80.CRT\*.*"; DestDir: "{app}\backup\sqldrivers"; Flags: recursesubdirs;
;--------------------------------COPY TO BACUP FOLDER-------------------------------------




;Configuration
Source: "settings\sokrates.key"; DestDir: "{app}\settings"; Flags: ignoreversion;
Source: "settings\network_connections.cfg"; DestDir: "{app}\settings"; Flags: ignoreversion;
Source: "settings\client_pe.ini"; DestDir: "{app}\settings"; DestName: "client.ini"; Flags: ignoreversion; Check: GetPeFileCopyFlags; AfterInstall: SetLanguage(0)
Source: "settings\client_te.ini"; DestDir: "{app}\settings"; DestName: "client.ini"; Flags: ignoreversion; Check: GetTeFileCopyFlags; AfterInstall: SetLanguage(0)
Source: "settings\client_be.ini"; DestDir: "{app}\settings"; DestName: "client.ini"; Flags: ignoreversion; Check: GetBeFileCopyFlags; AfterInstall: SetLanguage(0)
;this is essential on first installation->to make registration sucess (if install+registration in one step)
Source: "settings\client_pe.ini"; DestDir: "{code:GetSettngsDirectory}\settings"; DestName: "client.ini"; Flags: ignoreversion onlyifdoesntexist; Check: GetPeFileCopyFlags; AfterInstall: SetLanguage(1)
Source: "settings\client_te.ini"; DestDir: "{code:GetSettngsDirectory}\settings"; DestName: "client.ini"; Flags: ignoreversion onlyifdoesntexist; Check: GetTeFileCopyFlags; AfterInstall: SetLanguage(1)
Source: "settings\client_be.ini"; DestDir: "{code:GetSettngsDirectory}\settings"; DestName: "client.ini"; Flags: ignoreversion onlyifdoesntexist; Check: GetBeFileCopyFlags; AfterInstall: SetLanguage(1)


 ;delete content of settings directory -> instructed by MB (Brane mail, from 22.05.07)
[UninstallDelete]
Type: files; Name: "{code:GetSettngsDirectory}\settings\sokrates.key";
Type: files; Name: "{code:GetSettngsDirectory}\settings\network_connections.cfg";
Type: files; Name: "{code:GetSettngsDirectory}\settings\database_connections.cfg";
Type: files; Name: "{code:GetSettngsDirectory}\settings\client.log";
Type: files; Name: "{code:GetSettngsDirectory}\settings\client.ini";
Type: files; Name: "{code:GetSettngsDirectory}\data\DATA.FDB";
Type: dirifempty ; Name: "{code:GetSettngsDirectory}\data";


[Registry]
;register installation path->remove on deinstall
Root: HKLM; Subkey: "Software\Helix Business Soft\SOKRATES Communicator Personal Edition\Settings"; ValueType: string; ValueName: "InstallPath"; ValueData: "{app}"; Flags: uninsdeletekey; Check: GetPeFileCopyFlagsRegistry
Root: HKLM; Subkey: "Software\Helix Business Soft\SOKRATES Communicator Team Edition\Settings"; ValueType: string; ValueName: "InstallPath"; ValueData: "{app}"; Flags: uninsdeletekey; Check: GetTeFileCopyFlagsRegistry
Root: HKLM; Subkey: "Software\Helix Business Soft\SOKRATES Communicator Business Edition\Settings"; ValueType: string; ValueName: "InstallPath"; ValueData: "{app}"; Flags: uninsdeletekey; Check: GetBeFileCopyFlagsRegistry



[Icons]
Name: "{group}\SOKRATES Communicator Personal Edition"; Filename: "{app}\sokrates.exe"; WorkingDir: "{app}";  Check: GetPeFileCopyFlags
Name: "{group}\SOKRATES Communicator Team Edition"; Filename: "{app}\sokrates.exe"; WorkingDir: "{app}";  Check: GetTeFileCopyFlags
Name: "{group}\SOKRATES Communicator Business Edition"; Filename: "{app}\sokrates.exe"; WorkingDir: "{app}";  Check: GetBeFileCopyFlags

Name: "{group}\Uninstall SOKRATES Communicator Personal Edition"; Filename: "{uninstallexe}";  Check: GetPeFileCopyFlags
Name: "{group}\Uninstall SOKRATES Communicator Team Edition"; Filename: "{uninstallexe}";  Check: GetTeFileCopyFlags
Name: "{group}\Uninstall SOKRATES Communicator Business Edition"; Filename: "{uninstallexe}";  Check: GetBeFileCopyFlags

Name: "{userappdata}\Microsoft\Internet Explorer\Quick Launch\SOKRATES Communicator Personal Edition"; Filename: "{app}\sokrates.exe"; Tasks: quicklaunchicon;  WorkingDir: "{app}";  Check: GetPeFileCopyFlags
Name: "{userappdata}\Microsoft\Internet Explorer\Quick Launch\SOKRATES Communicator Team Edition"; Filename: "{app}\sokrates.exe"; Tasks: quicklaunchicon;  WorkingDir: "{app}";  Check: GetTeFileCopyFlags
Name: "{userappdata}\Microsoft\Internet Explorer\Quick Launch\SOKRATES Communicator Business Edition"; Filename: "{app}\sokrates.exe"; Tasks: quicklaunchicon;  WorkingDir: "{app}";  Check: GetBeFileCopyFlags

Name: "{userdesktop}\SOKRATES Communicator Personal Edition"; Filename: "{app}\sokrates.exe"; Tasks: desktopicon; WorkingDir: "{app}";  Check: GetPeFileCopyFlags
Name: "{userdesktop}\SOKRATES Communicator Team Edition"; Filename: "{app}\sokrates.exe"; Tasks: desktopicon; WorkingDir: "{app}";  Check: GetTeFileCopyFlags
Name: "{userdesktop}\SOKRATES Communicator Business Edition"; Filename: "{app}\sokrates.exe"; Tasks: desktopicon; WorkingDir: "{app}";  Check: GetBeFileCopyFlags

Name: "{userstartmenu}\SOKRATES Communicator Personal Edition"; Filename: "{app}\sokrates.exe"; Tasks: startmenuicon; WorkingDir: "{app}";  Check: GetPeFileCopyFlags
Name: "{userstartmenu}\SOKRATES Communicator Team Edition"; Filename: "{app}\sokrates.exe"; Tasks: startmenuicon; WorkingDir: "{app}";  Check: GetTeFileCopyFlags
Name: "{userstartmenu}\SOKRATES Communicator Business Edition"; Filename: "{app}\sokrates.exe"; Tasks: startmenuicon; WorkingDir: "{app}";  Check: GetBeFileCopyFlags

[Run]
Filename: "{code:LaunchApplication}";  Description: "{code:LaunchApplicationString}"; WorkingDir: "{app}"; Flags: postinstall nowait


[Code]
#include "page1.isd"

procedure InitializeWizard();
var
  LastId: Integer;
begin

  LastId := wpLicense;
  LastId := CreatePage_Page1(LastId); // add custom page after the License wizard page
//  LastId := wpSelectDir;

end;

