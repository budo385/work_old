; This file contains custom messages used in the Sokrates Installation Program

[CustomMessages]
; These are Inno-specific

; These are Sokrates-specific
Sokrates_CreateStartMenuIcon=Create a &start menu icon
Sokrates_Register=Register
Sokrates_LaunchApp=Launch application
SkeletonCaption=SOKRATES� Communicator Editions
SkeletonDescription=Select an edition to install
Warning_SameSettings=There are already existing installations that use same subdirectory name,please change installation subdirectory path (Last part of path)
Warning_NoRemovableDrive=No removable drive detected
Msg_RemovableInstall=Installation on removable device (USB stick, disk)
Msg_RemovableInstallLabel=Drive: