// REST Config:
var protocol 			= document.location.protocol;
var hostname			= document.location.hostname;
var port 				= document.location.port;						
var ROOT_URL 			= protocol+'//'+hostname+":"+port;
var REST_WEB_ROOT_URL 	= ROOT_URL+'/rest';
var REST_WEB_DOCS_URL 	= ROOT_URL+"/user_storage/";
var REST_LOGIN 			= REST_WEB_ROOT_URL+"/server/login";
var REST_LOGOUT 		= REST_WEB_ROOT_URL+"/server/logout";
var COOKIE_EXPIRE_MIN	= 20;
var nDocListMaxRows 	= 50;

function resetRootUrl()
{
	// paste root URL for rest api
	$('#strApiUrl').val(REST_WEB_ROOT_URL); 	
}

function checkErrorCode()
{
	if(nLastBO_ErrorCode!=0) // error occured
	{									 			
	  if(nLastBO_ErrorCode==1000){ 			
		  alert("[login error]:"+strLastBO_ErrorText);  // login error
	  }
	  else {
		   alert("[error]:"+strLastBO_ErrorText);  // alert this general error
	  }
	  return;
    }	
}

// Generally, when POST is used, utf8 encoding is needed when sending strings to appserver.
var Utf8={encode:function(string){string=string.replace(/\r\n/g,"\n");var utftext="";for(var n=0;n<string.length;n++){var c=string.charCodeAt(n);if (c<128) {utftext+=String.fromCharCode(c);}else if((c>127)&&(c<2048)){utftext+=String.fromCharCode((c>>6)|192);utftext+=String.fromCharCode((c&63)|128);}else{utftext+=String.fromCharCode((c>>12)|224);utftext+=String.fromCharCode(((c>>6)&63)|128);utftext+=String.fromCharCode((c&63)|128);}}return utftext;},decode:function(utftext){var string="";var i=0;var c=c1=c2=0;while(i<utftext.length){c=utftext.charCodeAt(i);if(c<128){string+=String.fromCharCode(c);i++;}else if((c>191)&&(c<224)){c2=utftext.charCodeAt(i+1);string+=String.fromCharCode(((c&31)<<6)|(c2&63));i+=2;}else{c2=utftext.charCodeAt(i+1);c3=utftext.charCodeAt(i+2);string+=String.fromCharCode(((c&15)<<12)|((c2&63)<<6)|(c3&63));i+=3;}}return string;}}	

//search_types we can choose from for api: contact/search
var arrValues=["BCNT_LASTNAME","BCNT_FIRSTNAME","BCNT_DEPARTMENTNAME","BCNT_BIRTHDAY","BCNT_ORGANIZATIONNAME", "BCNT_PROFESSION","BCNT_FUNCTION","BCMP_FULLNUMBER","BCMI_ADDRESS","BCME_ADDRESS","BCMA_FORMATEDADDRESS","BCNT_LOCATION",];
