/*
	
(*)  Improved listener for "pagebeforechange" event: No need for timer, session storage, traversing dom size, ..etc
	 Fixed: Fast navigating would not fire dom build process
	 Fixed: No doubling events for swipe
	
(*)  Temp. are the build_ functions called from their respective "pageshow" events due to a few bugs:

	 - Required DOM modifications not possible in pagebeforechange (because it fires so early that some elems don't exist yet)
	 - Sharing var's between EVENTS (global vars? cloning handlers?)
	 
*/

var oniOS = false; 	   // true: runs on iOS
var onAndroid = false; // true: runs on Android

function clog(text) {
	if (console) console.log(text);
}

$(document).bind("pagebeforechange", function( e, data ) 
{
	// Detect mobile platforms:    // *** MB
	oniOS =    (navigator.userAgent.match(/iPhone/i) == 'iPhone') || 
			   (navigator.userAgent.match(/iPod/i) == 'iPod') ||
			   (navigator.userAgent.match(/iPad/i) == 'iPad') ||
			   (navigator.userAgent.match(/iPad/i) == 'iWatch') ||
			   (navigator.userAgent.match(/iPad/i) == 'iTV');
			   
onAndroid =    (navigator.userAgent.match(/Android/i) == 'Android');	

	

	
	// only allows strings to pass
	if (typeof data.toPage !== 'string' ) {
        return
    }
	else
	{	
		clog("toPage = "+data.toPage);
		
		if (data.toPage=="menu.html"){
			// build_Menu();	
		}
		if (data.toPage=="contacts.html") {
			// build_Contacts();
		}
		if (data.toPage=="contactfavorites.html") {
			//	build_contactFavorites();
		}
		if (data.toPage=="contactfind.html") {
			// build_FindContact
		}
		if (data.toPage=="whitepages.html") {
			// Moved to "pageshow"
				// bug_1:  transitioning stops for o moment (when navigation from contacts -> whitepages)
				// bug_2:  octobutton doesn't work (page reload not working). 
			init_OctoButton();
			// build_Whitepages();	
		}	
		if (data.toPage=="yellowpages.html") {
			init_OctoButton();
			// build_Yellowpages();	
		}
		if (data.toPage=="contact.html") {	
			//build_Contact();			
		}
		if (data.toPage=="desktop.html") {	
			// build_Desktop();			
		}
		if (data.toPage=="views.html"){
			// build_Views();	
		}
		if (data.toPage=="documents.html") {	
			// build_Documents();		
		}
		if (data.toPage=="notes.html") {	
			// build_Notes();		
			// build_NoteDisplay();
		}
		if (data.toPage=="emails.html") {	
			// build_Emails();	
			// build_EmailDisplay();	
		}
		if (data.toPage=="websites.html") {	
			// build_Websites();		
		}
		if (data.toPage=="projects.html") {	
			// build_Projects();		
		}
		if (data.toPage=="calendar.html") {	
			// build_Calendar();
			// build_CalendarAS()		
		}
		
	}
}); 

/*---------------------------------------------
	index.html
----------------------------------------------*/		
$(document).on('pageinit', '#page_login', function()    
{	
	var g_pw = ''; // TOFIX security vulnerability


	// *** MB
	// alert('LOGIN');

	// Load the CUSTID form id.xml if existent.  *** MB
	// While CSID comes from the database, CUSTID comes from an optional id.xml on the server
	// id.xml is available *before* login!
	// ($.get does not require a place in the DOM)
	setSessionkey("CUSTID",0);	// *** Store CUSTID for global access
	var jqxhr = $.get('id.xml', 
		  function(data, status, xhr) {
				var custid = parseInt($(data).find('custid').text());
				setSessionkey("CUSTID",custid);	// *** Store CUSTID for global access
				//alert(custid);
				// Modify customized GUI:
				if (custid == CLIENT_HHM) {
					$('#logmenu_simplelogin').css('display','block');
				} else {
					$('#logmenu_container').css('display','block');
				};
			})
		.fail(
			function() {
				//alert('FAILED');
				// Setup default GUI:
				$('#logmenu_container').css('display','block');
			}); 


	
	// if new cache available on load
	window.addEventListener('load', function(e) 
	{
		//clog("load cache");
		// PM: commented because temp not using cache due to not having all resources in this version yet
		
	  	/*window.applicationCache.addEventListener('updateready', function(e) {
		if (window.applicationCache.status == window.applicationCache.UPDATEREADY) {
		   clog("Browser downloaded a new app cache, swap it in and reload the page");
		   window.applicationCache.swapCache();
		   window.location.reload();
		} else {
		  clog("Manifest didn't changed. Nothing new to server.");
		}
	  }, false);*/
	}, false);
 		
	$("#page_login").swipeleft(function() {
		$.mobile.changePage("info.html", {transition: "slide"});
	});	
	
	// login handler  
	$("#logmenu_container a").click(function(event){
		event.preventDefault();
		var user=$('#idUser').val();
		var pass=$('#idPass').val();
		//g_pw = pass;
		
		if(!user){
			alert("Username Required");	
		}
		else{
 		  	setLocalkey("user", user);
			switch (event.target.id)
			{
				case "log_desktop":
					setSessionkey("menu_sel", "desktop.html");
				break;
				case "log_contacts":
					setSessionkey("menu_sel", "contacts.html");
				break;
				case "log_projects":
					setSessionkey("menu_sel", "projects.html");
				break;
				case "btnClickLogin":   // *** MB
					setSessionkey("menu_sel", "projects.html");
				break;
				case "log_calendar":
					setSessionkey("menu_sel", "calendar.html");
				break;
				default : setSessionkey("menu_sel", "menu.html");
			 }
			BO_Login(user,pass,Login_CallBack,'','','','');
		}	
	}); 
	
	// login handler  simple login  *** MB
	$("#btnClickLogin").click(function(event){
		event.preventDefault();
		var user=$('#idUser').val();
		var pass=$('#idPass').val();
		//g_pw = pass;
		
		if(!user){
			alert("Username Required");	
		}
		else{
 		  	setLocalkey("user", user);
			setSessionkey("menu_sel", "projects.html");
			BO_Login(user,pass,Login_CallBack,'','','','');
		}	
	}); 	
	
  function Login_CallBack(data)
  {
	if(nLastBO_ErrorCode==0)
	{	
	  //sessionStorage.setItem("g_pw", g_pw);
	  BO_Call("GET","/sessiondata/user_opt_ar",parseXml,''); // Handle AS clients
	}
	else {
	  alert(strLastBO_ErrorText);
	}
  }  
  
  function parseXml(xml)
  {
	  var xdom = $(xml);
	  clog(xml);
	  var nCSID = parseInt(xdom.find("Ret_nCSID").text());
	  // nCSID = "55";  // *** Temporary Patch MB
	  setSessionkey("CSID",nCSID);	// *** Store CSID for global access
	  var bIsAS = xdom.find("Ret_nIsAnestezistClient").text();
	  if(bIsAS == "1") 
		setSessionkey("IsAnestezistClient",true); 
	  
	  var strFullName = xmlDecode(xdom.find("UserData row > BPER_FIRST_NAME").text())+' '+xmlDecode(xdom.find("UserData row > BPER_LAST_NAME").text());
	  var strDepartmentCode = xmlDecode(xdom.find("UserData row > BDEPT_CODE").text());  // *** MB
	  var strEmailCalConfAddress = xmlDecode(xdom.find("strEmailCalConfAddress").text());
	  setSessionkey("strFullName",strFullName);
	  setSessionkey("strEmailCalConfAddress",strEmailCalConfAddress);  
	  setSessionkey("strDepartmentCode",strDepartmentCode);  // *** MB
	  
	  // ..continue Login
	  if(sessionStorage.menu_sel == "desktop.html")
	  {
		  var nCookie = getCookie('nContactID');
		  sessionStorage.setItem("nContactID", nCookie);
		  sessionStorage.setItem("userType","mydesk"); 		// mydesk -> logged user | project | contact
		  $.mobile.changePage("desktop.html", {transition: "slideup"});			// in progres ... 
	  } 
	  else if(sessionStorage.menu_sel == "calendar.html" && sessionStorage.IsAnestezistClient == "true")
	  {
		  $.mobile.changePage("calendarAs.html", {transition: "slideup"});	
	  }
	  else
	  {
		  $.mobile.changePage(sessionStorage.menu_sel, {transition: "slideup"});	
	  }
  }
});
$(document).on('pageshow', '#page_login', function() 
{	 
  sessionStorage.clear();
  nav_clear();
  nav_registerURL();	

	
	// paste username
	if(localStorage.user)
	  $('#idUser').val(localStorage.user);	  
});

/*---------------------------------------------
	menu.html (in progress..)
----------------------------------------------*/
$(document).on('pageinit', '#page_menu', function()	
{
});	
$(document).on('pageshow', '#page_menu', function()
{	
	build_Menu();	
});
 
/*---------------------------------------------
	info.html
----------------------------------------------*/
$(document).on('pageinit', '#page_info', function()	
{
  $("#page_info").swiperight(function() {
	$.mobile.changePage("index.html", {reverse: true,transition: "slide"});
  });	
});
$(document).on('pageinit', '#page_info', function()	
{
});

/*---------------------------------------------
	contacts.html
----------------------------------------------*/
$(document).on('pageinit', '#page_contacts', function()	
{	
});
$(document).on('pageshow', '#page_contacts', function()	
{
	build_Contacts();	
});

/*---------------------------------------------
	contactfavorites.html
----------------------------------------------*/
$(document).on('pageinit', '#page_favorites', function()		
{	
});
$(document).on('pageshow', '#page_favorites', function()
{	
	build_contactFavorites();
});

/*---------------------------------------------
	whitepages.html
----------------------------------------------*/
$(document).on('pageinit', '#page_whitelist', function()		
{	
});
$(document).on('pageshow', '#page_whitelist', function()
{		
	build_Whitepages();	
}); 	

/*---------------------------------------------
	yellowpages.html
----------------------------------------------*/
$(document).on('pageinit', '#page_yellowlist', function()		
{	
});
$(document).on('pageshow', '#page_yellowlist', function()
{	
	build_Yellowpages();
}); 	

/*---------------------------------------------
	contactfind.html
----------------------------------------------*/
$(document).on('pageinit', '#page_contactfind', function()		
{	
});
$(document).on('pageshow', '#page_contactfind', function()
{	
	build_FindContact();	
}); 

/*---------------------------------------------
	contact.html
----------------------------------------------*/
$(document).on('pageinit', '#page_contact', function()	
{
});	
$(document).on('pageshow', '#page_contact', function()	
{
	build_Contact();
});	

/*---------------------------------------------
	desktop.html
----------------------------------------------*/
$(document).on('pageinit', '#page_desktop', function()		
{	
});
$(document).on('pageshow', '#page_desktop', function()
{
	build_Desktop();	
});

/*---------------------------------------------
	views.html
----------------------------------------------*/
$(document).on('pageinit', '#page_views', function()		
{	
});
$(document).on('pageshow', '#page_views', function()
{
	build_Views();	
});

/*---------------------------------------------
	documents.html
----------------------------------------------*/
$(document).on('pageinit', '#page_documents', function()		
{	
});
$(document).on('pageshow', '#page_documents', function()
{	
	build_Documents();
}); 	

/*---------------------------------------------
	notes.html
----------------------------------------------*/
$(document).on('pageinit', '#page_notes', function()		
{	
});
$(document).on('pageshow', '#page_notes', function()
{	
	build_Notes();
}); 	

/*---------------------------------------------
	notedisplay.html
----------------------------------------------*/
$(document).on('pageinit', '#page_notedisplay', function()		
{	
});
$(document).on('pageshow', '#page_notedisplay', function()
{	
	build_NoteDisplay();	
}); 

/*---------------------------------------------
	websites.html.html
----------------------------------------------*/
$(document).on('pageinit', '#page_websites', function()		
{	
});
$(document).on('pageshow', '#page_websites', function()
{	
	build_Websites();	
}); 	

/*---------------------------------------------
	emails.html
----------------------------------------------*/
$(document).on('pageinit', '#page_emails', function()		
{	
});
$(document).on('pageshow', '#page_emails', function()
{	
	build_Emails();
}); 

/*---------------------------------------------
	emaildisplay.html
----------------------------------------------*/
$(document).on('pageinit', '#page_emaildisplay', function()		
{	
});
$(document).on('pageshow', '#page_emaildisplay', function()
{	
	build_EmailDisplay();
})	

/*---------------------------------------------
	projects.html
----------------------------------------------*/
$(document).on('pageinit', '#page_projects', function()		
{	
});
$(document).on('pageshow', '#page_projects', function()
{	
	var myCSID = sessionStorage.getItem("CSID");
	if (myCSID == CLIENT_HHM) {	// *** 55
		$("#ProjectSelectorWidget").css('display','none');
		$("#ProjectSelectorTitle").css('display','none');
	}
	build_Projects();
})	
/*---------------------------------------------
	calendar.html
----------------------------------------------*/
$(document).on('pageinit', '#page_calendar', function()		
{	
});
$(document).on('pageshow', '#page_calendar', function()
{
	if(sessionStorage.IsAnestezistClient == "true"){
		build_CalendarAS();
	}
	else{
		build_Calendar();
	}	
	
})	



/* ------------------------------------------------------------------------------
              DOM BUILDERS (used by pagebeforechange)
------------------------------------------------------------------------------  */
function build_Menu()
{
	initPageStatus(true);
	
	// reset person name
	if(sessionStorage.ssCalcName){		
		setSessionkey("ssCalcName",""); 
	}  
	// logout
	$("#logoutbtn").click(function(){  
		BO_LogOut();
		sessionStorage.clear();
		window.location = "index.html"; // full refresh
  	});
  	
	// PM: Not in Use!
	// BT: load user settings and AR system then based on that disable/enable each of entry in WebConnect  	
  	// BO_Call("GET","/sessiondata/user_opt_ar",parseXml,'');
	

	if (sessionStorage.CSID == CLIENT_HHM) {
		$('#page_menu #logmenu_simplelogin').css('display','block');
		$("#btnClickLogin").click(function(event){
			event.preventDefault();
			$.mobile.changePage("projects.html", {transition: "slide"});
		});	

	} else {
		$('#page_menu #logmenu_container').css('display','block');
	
		// MB: Register module icons:
		$("#logmenu #log_desktop").click(function(event){
			event.preventDefault();
			var nCookie = getCookie('nContactID');
			sessionStorage.setItem("nContactID", nCookie);
			sessionStorage.setItem("userType","mydesk"); 		// mydesk -> logged user | project | contact
			$.mobile.changePage("desktop.html", {transition: "slideup"});	
			// in progres ... 
			//window.location=("desktop.html?id="+getCookie('nContactID')+"&mydesk=1");
			//$.mobile.changePage( "contacts.html" );
		});	
		$("#logmenu #log_contacts").click(function(event){
			event.preventDefault();
			$.mobile.changePage("contacts.html", {transition: "slide"});	
		});	
		$("#logmenu #log_projects").click(function(event){
			event.preventDefault();
			$.mobile.changePage("projects.html", {transition: "slide"});
		});	
		$("#logmenu #log_calendar").click(function(event){
			event.preventDefault();
			
			if(sessionStorage.IsAnestezistClient == "true"){
				$.mobile.changePage("calendarAs.html", {transition: "slideup"});	
			}
			else{
				$.mobile.changePage("calendar.html", {transition: "slideup"} );
			}
		});	
	};

	/* PM: not in use!
	function parseXml(xml){	
		checkErrorCode();
		var xdom = $(xml);
		var strFname = xmlDecode(xdom.find("UserData row > BPER_FIRST_NAME").text());
		var strLname = xmlDecode(xdom.find("UserData row > BPER_LAST_NAME").text());
		var strFullName = strFname+' '+strLname;
		var strEmailCalConfAddress = xmlDecode(xdom.find("strEmailCalConfAddress").text());
		setSessionkey("strFullName",strFullName);
		setSessionkey("strEmailCalConfAddress",strEmailCalConfAddress);
		//now find AR set: 
		var bShowEmailsDocs=true;
		var bShowContacts=true;
		var bShowProjects=true;
		var bShowcalendars=true;
		xdom.find("ARSet row").each(function(){
			var node = $(this);		  
			var nArType = parseInt(node.find('CAR_CODE').text());
			var nValue = parseInt(node.find('CAR_VALUE').text());			
			if(nArType==ENTER_MODIFY_CONTACTS &&  nValue==0)
				bShowContacts=false;
			if(nArType==ENTER_MODIFY_PROJECT_MAIN_DATA &&  nValue==0)
				bShowProjects=false;
			if(nArType==OPEN_CALENDAR &&  nValue==0)
				bShowcalendars=false;
			if(nArType==ASSIGN_CE_ENTITIES_TO_PROJECTS_AND_CONTACTS &&  nValue==0)
				bShowEmailsDocs=false;
		});
		// visualy add/remove items (tasks is always visible)
		if(bShowEmailsDocs){
		  var uniqueId=getCookie('nContactID');
		  setSessionkey("desktopID",uniqueId);
		  var strMenuEntry_DocsEmails = '<li><a id="my_docs" href="desktop.html" rel="external"><img alt="list" src="images/Desktop48.png" />';
		  strMenuEntry_DocsEmails +='<span>Documents & Emails</span></a></li>';
		  $('#menu_list').append(strMenuEntry_DocsEmails);
		}
		if(bShowContacts){
		  var strMenuEntry='<li class="menu"> <a href="contacts.html"><img alt="list" src="images/contacts.png"/> ';
		  strMenuEntry +='<span class="name">Contacts</span><span class="arrow"></span></a></li>';
		  $('#menu_list').append(strMenuEntry);
		}
		var strMenuEntry='<li class="menu"> <a href="#"> <img alt="list" src="images/desktop.png"/>';
		strMenuEntry +='<span class="name">Tasks</span><span class="arrow"></span></a></li>';
		$('#menu_list').append(strMenuEntry);
		if(bShowcalendars){
		  var strMenuEntry='<li class="menu"> <a href="calendar.html"> <img alt="list" src="images/calendar.png" />';
		  strMenuEntry +='<span class="name">Calendar</span><span class="arrow"></span></a></li>';
		  $('#menu_list').append(strMenuEntry);
		}
		if(bShowProjects){
		  var strMenuEntry='<li class="menu"> <a href="projects.html"> <img alt="list" src="images/projects.png" />';
		  strMenuEntry +='<span class="name">Projects</span><span class="arrow"></span></a></li>';
		  $('#menu_list').append(strMenuEntry);
		}
	  }	 	
	  */
	  
	  // DIALOG POPUP
	  $('#inputUserName').val(localStorage.getItem("user"));
	  
	  $("#btnSaveUserAccount").click(function(event){
			 event.preventDefault();
			 ChangeUserAccount();
	  });
	  function ChangeUserAccount()
	  {
		  // user must be logged
		  if (BO_IsLogged()==false){	
        		window.location="index.html"; 
				return;
		  }
		  // old and new pass must be entered
		  var strUserName = $('#inputUserName').val();
		  var strOldPass = $('#OldPw').val();
		  var strNewPass = $('#NewPw').val();
		  
		  if(strOldPass==null || strNewPass==null){
		  	alert("Error: You must Specify Old and New Password");
			return;
		  }
		  BO_ChangePassword(strUserName,strOldPass,strNewPass,cb_ChangePassword); 
	  }
	  function cb_ChangePassword(xml)
	  { 	
		if(nLastBO_ErrorCode!=0)
 		{ 
			alert(strLastBO_ErrorText);
			return;
 		}
		else{
			$( "#dlgSettings" ).popup( "close" );
		}
	  }    
}

function build_Contacts()
{
  initPageStatus(true);
  sessionStorage.removeItem("navBtnActive");
  
  $("#openContactFavorites").click(function(event){
		event.preventDefault();
		$.mobile.changePage("contactfavorites.html", {transition: "slide"});	
  });
  $("#openWhitePages").click(function(event){
		event.preventDefault();
		$.mobile.changePage("whitepages.html", {transition: "slide"});	
		
  });
  $("#openYellowPages").click(function(event){
		event.preventDefault();
		$.mobile.changePage("yellowpages.html", {transition: "slide"});
		
  });
}


function build_Whitepages()
{	
	initPageStatus();
	var strFrom = sessionStorage.from;
	var strTo = sessionStorage.to;

	if( (!strFrom && !strTo) || (strFrom=='' && strTo=='')){ //if(strFrom=='' && strTo=='' || !sessionStorage.navBtnActive)
		strFrom='';
		strTo='d';
	}
	
	BO_Call("POST","/contact/filter",parseXml,'<REQUEST><PARAMETERS><nFilter>1</nFilter><nFromN>-1</nFromN><nToN>-1</nToN><strFromLetter>'+strFrom+'</strFromLetter><strToLetter>'+strTo+'</strToLetter></PARAMETERS></REQUEST>');	
	
	function parseXml(xml)
	{	
	  checkErrorCode();
	 
	  // alphabet nav first init
	  if (typeof(sessionStorage.navBtnActive)=="undefined" || !sessionStorage.navBtnActive){
		  setSessionkey("navBtnActive",0);
		  $("div:jqmData(role='navbar') li:eq(0) a").addClass('ui-btn-active');
	  }
	  else{
		  $("div:jqmData(role='navbar') li:eq("+sessionStorage.navBtnActive+") a").addClass('ui-btn-active');
	  }
	
	  // alphabet nav click handler	
	  $("div:jqmData(role='navbar') ul li").click(function(event)
	  {
		  event.preventDefault();
		  var idx = $(this).index();	
		  switch (idx)
		  {
			  case 0:{
				  setSessionkey("from",""); 
				  setSessionkey("to","d");
			  }
			  break;
			  case 1:{
				  setSessionkey("from","d"); 
				  setSessionkey("to","g");
			  }
			  break;
			  case 2:{
				  setSessionkey("from","g"); 
				  setSessionkey("to","j"); 
			  }
			  break;
			  case 3:{
				  setSessionkey("from","j"); 
				  setSessionkey("to","m");
			  }
			  break;
			  case 4:{
				  setSessionkey("from","m"); 
				  setSessionkey("to","p"); 
			  }
			  break;
			  case 5:{
				  setSessionkey("from","p"); 
				  setSessionkey("to","s");
			  }
			  break;
			  case 6:{
				  setSessionkey("from","s"); 
				  setSessionkey("to","v");
			  }
			  break;
			  case 7:{
				  setSessionkey("from","v"); 
				  setSessionkey("to","");
			  }
			  break;
			  default :{
				  setSessionkey("from",""); 
				  setSessionkey("to","d");	
			  } 
		  }
		  $("div:jqmData(role='navbar') li:eq("+sessionStorage.navBtnActive+") a").removeClass('ui-btn-active');
		  setSessionkey("navBtnActive",idx);
		  $.mobile.changePage( "#page_whitelist", {allowSamePageTransition: true, reloadPage: true, transition: 'fade'} );
		  // fade | flip | flow | pop | slide | slidedown | slidefade | slideup | turn | none
		});
	    
		parseContactList($(xml));
	}
	
	function parseContactList(xmldom) 
	{
		var strHtml	= '<ul data-role="listview" data-theme="c" data-filter="true" data-filter-placeholder="Jump to a Contact in this Section">';
		var node = null;
		var lastChar = null;
		var ch = null;
		var IsPerson = null;
		var nContactId = null; 
		var strOrgName = null;
		var strFirstName = null;
		var strLastName = null;
		
		xmldom.find('contact').each(function()
		{		  					
			node 			= $(this);
			IsPerson		= xmlDecode(node.find('Type').text());
			nContactId 		= node.find("Contact_ID").text();
			strOrgName 		= xmlDecode(node.find('OrganizationName').text());
			
			if(IsPerson == 1) {
				ch = xmlDecode(node.find("LastName").text()).charAt(0).toLowerCase(); 
				strFirstName	= xmlDecode(node.find("FirstName").text());
				strLastName		= xmlDecode(node.find("LastName").text());
			}	
			else{
				ch = xmlDecode(node.find("OrganizationName").text()).charAt(0).toLowerCase(); 
			} 
				
			//if we reach end char then exit, for last part: set to=''
			if(lastChar!=ch){
				strHtml+='<li data-role="list-divider">'+ch.toUpperCase()+'</li>';
				lastChar=ch;
			}	
			if(IsPerson == 1) {
				strHtml+=
				'<li><a href="#" name="openContact" data-hbs-contactid="'+nContactId+'" onclick="">'+
				'<h3>'+strLastName+' '+strFirstName+'</h3><p>'+strOrgName+'</p></a></li>';
			}
			else{
				strHtml+='<li><a href="#" name="openContact" data-hbs-contactid="'+nContactId+'" onclick=""><h3>'+strOrgName+'</h3></a></li>';
			}	
		});
		strHtml+='</ul>'; 
		$('div[data-role="content"]').append(strHtml);
		$('div[data-role="page"]').append(strFooter);
		$('ul[data-role="listview"]').listview();
		
		$("a[name='openContact']").on( "click", function(event) {	
			event.preventDefault();	
			var id = $(this).data("hbs-contactid");
			sessionStorage.setItem("nContactId", id);
			$.mobile.changePage("contact.html", {transition: "slide"});
		});
	}
}

function build_Yellowpages()
{	
	initPageStatus();
	var strFrom = sessionStorage.from;
	var strTo = sessionStorage.to;
	
	if( (!strFrom && !strTo) || (strFrom=='' && strTo=='')){
		strFrom='';
		strTo='d';
	}
	
	BO_Call("POST","/contact/filter",parseXml,'<REQUEST><PARAMETERS><nFilter>0</nFilter><nFromN>-1</nFromN><nToN>-1</nToN><strFromLetter>'+strFrom+'</strFromLetter><strToLetter>'+strTo+'</strToLetter></PARAMETERS></REQUEST>');		
	
	// PARSER PREREQUISITES
	function parseXml(xml)
	{	
		checkErrorCode();
		
		// alphabet nav first init
		if (typeof(sessionStorage.navBtnActive)=="undefined" || !sessionStorage.navBtnActive){
			setSessionkey("navBtnActive",0);
			$("div:jqmData(role='navbar') li:eq(0) a").addClass('ui-btn-active');
		}
		else{
			$("div:jqmData(role='navbar') li:eq("+sessionStorage.navBtnActive+") a").addClass('ui-btn-active');
		}
		
		// alphabet nav click handler	
		$("div:jqmData(role='navbar') ul li").click(function(event)
		{
			event.preventDefault();
			var idx = $(this).index();	
			switch (idx)
			{
				case 0:{
					setSessionkey("from",""); 
					setSessionkey("to","d");
				}
				break;
				case 1:{
					setSessionkey("from","d"); 
					setSessionkey("to","g");
				}
				break;
				case 2:{
					setSessionkey("from","g"); 
					setSessionkey("to","j"); 
				}
				break;
				case 3:{
					setSessionkey("from","j"); 
					setSessionkey("to","m");
				}
				break;
				case 4:{
					setSessionkey("from","m"); 
					setSessionkey("to","p"); 
				}
				break;
				case 5:{
					setSessionkey("from","p"); 
					setSessionkey("to","s");
				}
				break;
				case 6:{
					setSessionkey("from","s"); 
					setSessionkey("to","v");
				}
				break;
				case 7:{
					setSessionkey("from","v"); 
					setSessionkey("to","");
				}
				break;
				default :{
					setSessionkey("from",""); 
					setSessionkey("to","d");	
				} 
			}
			$("div:jqmData(role='navbar') li:eq("+sessionStorage.navBtnActive+") a").removeClass('ui-btn-active');
			setSessionkey("navBtnActive",idx);
		
			//location.reload(); 
			$.mobile.changePage( "#page_yellowlist", {allowSamePageTransition: true, reloadPage: true, transition: 'fade'} );
		});
		
		parseContactList($(xml));
		
	}//END: ParseXML

	// MAIN PARSER 
	function parseContactList(xmldom) 
	{
		var strHtml = '<ul data-role="listview" data-theme="c" data-filter="true" data-filter-placeholder="Jump to a Contact in this Section">';
		var strOrgPic = '<img alt="organization" src="images/organization_white.png" class="ui-li-icon"/>';
		var node = null;
		var ch = null;
		var lastChar = null;
		var IsOrg = null;
		var nContactId = null; 
		var strFirstName = null;
		var strLastName = null;
		var strOrgName = null;
		xmldom.find('contact').each(function()
		{		  					
			node = $(this);	
			IsOrg = xmlDecode(node.find('Type').text());
			nContactId = node.find('Contact_ID').text();
			strFirstName = xmlDecode(node.find("FirstName").text());
			strLastName = xmlDecode(node.find("LastName").text());
			strOrgName = xmlDecode(node.find('OrganizationName').text());
			
			if(IsOrg == 2){
				ch = xmlDecode(node.find("OrganizationName").text()).charAt(0); 
				ch = ch.toLowerCase();
			}
			if(lastChar!=ch){
				
				strHtml+='<li data-role="list-divider">'+ch.toUpperCase()+'</li>'; 
				lastChar=ch;
			}	
			if(IsOrg==2) {	
				strHtml+=
					'<li>'+strOrgPic+''+
					'<a href="#" name="openContact" class="indent" data-hbs-contactid="'+nContactId+'" onclick="">'+ 
					'<h3>'+strOrgName+'</h3></a></li>';
			}
			else{
				strHtml+=
					'<li><a href="#" name="openContact" data-hbs-contactid="'+nContactId+'" onclick="">'+
					'<h3>'+strLastName+' '+strFirstName+'</h3><p>'+strOrgName+'</p></a></li>';
			}	
		});
		strHtml+='</ul>'; 
		$('div[data-role="content"]').append(strHtml);
		$('div[data-role="page"]').append(strFooter);
		$('ul[data-role="listview"]').listview();
		
		$("a[name='openContact']").on( "click", function(event) {	
			event.preventDefault();	
			var id = $(this).data("hbs-contactid");
			sessionStorage.setItem("nContactId", id);
			$.mobile.changePage("contact.html", {transition: "slide"});
		});
	}
}

// used by whitepages & yellowpages
// musthave for octobutton menu(JQM does not provide more than 5 or so items in a grid.. )
function init_OctoButton()
{
  (function( $, undefined ) {
		
		$.fn.grid = function( options ) {
			return this.each(function() {
		
				var $this = $( this ),
					o = $.extend({
						grid: null
					},options),
					$kids = $this.children(),
					gridCols = {solo:1, a:2, b:3, c:4, d:5, e:6, f:7, g:8},
					grid = o.grid,
					iterator;
		
					if ( !grid ) {
						if ( $kids.length <= 8 ) {
							for ( var letter in gridCols ) {
								if ( gridCols[ letter ] === $kids.length ) {
									grid = letter;
								}
							}
						} else {
							grid = "a";
						}
					}
					iterator = gridCols[grid];
		
				$this.addClass( "ui-grid-" + grid );
		
				$kids.filter( ":nth-child(" + iterator + "n+1)" ).addClass( "ui-block-a" );
		
				if ( iterator > 1 ) {
					$kids.filter( ":nth-child(" + iterator + "n+2)" ).addClass( "ui-block-b" );
				}
				if ( iterator > 2 ) {
					$kids.filter( ":nth-child(3n+3)" ).addClass( "ui-block-c" );
				}
				if ( iterator > 3 ) {
					$kids.filter( ":nth-child(4n+4)" ).addClass( "ui-block-d" );
				}
				if ( iterator > 4 ) {
					$kids.filter( ":nth-child(5n+5)" ).addClass( "ui-block-e" );
				}
				if(iterator > 5)	{   
					$kids.filter(':nth-child(6n+6)').addClass('ui-block-f');
				}
				if(iterator > 6)	{   
					$kids.filter(':nth-child(7n+7)').addClass('ui-block-g');
				}
		
			});
		};
		})( jQuery );	
}

function build_contactFavorites()	// contactfavorites.html
{
	initPageStatus();
	
	BO_Call("GET","/contact/favorites",parseFavorites,'');	
	
	function parseFavorites(xml) 
	{	
	  checkErrorCode();
	  var xmldom = $(xml);
	  var strHtml = '<ul data-role="listview" data-theme="c">';
	  var strContactPic = null;
	  var nContactId = null;
	  var strOrgName = null;
	  var strFirstName = null;
	  var strLastName = null;
	  var nContactType = null;
	  var node = null;
	
	  xmldom.find('contact').each(function()
	  {  	
		  node = $(this);
		  nContactType = node.find('Type').text();
		  strContactPic = node.find("ContactPicture").text();
		  nContactId = node.find('Contact_ID').text();
		  strOrgName = xmlDecode(node.find('OrganizationName').text());
		  strLastName = xmlDecode(node.find("LastName").text());
		  strFirstName = xmlDecode(node.find("FirstName").text());
		
		if(nContactType == 1) 
		{	
			if(strContactPic.length > 0){ 
				strHtml+='<li><a href="#" name="openFavorites"  data-hbs-contactid="'+nContactId+'" onclick="">'+
				'<img alt="photo" src="data:image/png;base64,'+strContactPic+'"/><h3>'+strLastName+' '+strFirstName+'</h3>'+
				'<p>'+strOrgName+'</p></a></li>';
			}
			else{	
				strHtml+='<li><a href="#" name="openFavorites"  data-hbs-contactid="'+nContactId+'" onclick="">'+
				'<img alt="photo" src="images/person64.png"/><h3>'+strLastName+' '+strFirstName+'</h3><p>'+strOrgName+'</p></a></li>';	
			}
		}	
		else 
		{  
			strHtml+='<li class="organization"><a href="#" name="openFavorites"  data-hbs-contactid="'+nContactId+'" onclick="">'+
			'<h3>'+strOrgName+'</h3></a></li>';
		}							
	});	
	  strHtml+='</ul>';
	  $('div[data-role="content"]').append(strHtml);
	  $('ul[data-role="listview"]').listview();
	
	  
	  
	  	  $("a[name='openFavorites']").on( "click", function(event) {
		  event.preventDefault();	
		  var id = $(this).data("hbs-contactid");
		  sessionStorage.setItem("nContactId", id);
		  $.mobile.changePage("contact.html", {transition: "slide"});
	  });
	  
	}	
}

function build_FindContact()
{
	initPageStatus();
	
	$("#btnClickFind").click(function(event)
	{
		event.preventDefault();
		$('#results').empty();

		var A1 = $("#cb1 option:selected").val()-1;
		var a1 = xmlEncode(arrValues[A1]);	
		var a2 = xmlEncode($("#x1").val());	
		var B1 = $("#cb2 option:selected").val()-1;
		var b1 = xmlEncode(arrValues[B1]);
		var b2 = xmlEncode($("#x2").val()); 
		
	   	if(!(a2||b2) ){ 
        	alert("At Least one Search Criteria is Required");
      	}
		else
		{
           BO_Call("POST","/contact/search",parseXml,'<REQUEST><PARAMETERS><nSearchType>0</nSearchType><strField1_Name>'+a1+'</strField1_Name><strField1_Value>'+a2+'</strField1_Value><strField2_Name>'+b1+'</strField2_Name><strField2_Value>'+b2+'</strField2_Value><nGroupID xsi:nil="true"/><nFromN>-1</nFromN><nToN>-1</nToN><strFromLetter xsi:nil="true"/><strToLetter xsi:nil="true"/></PARAMETERS></REQUEST>');
		}
	});	
	
	function parseXml(xml)
	{			
		checkErrorCode();
		var XDOM 			= $(xml);
		var node 	 		= null;
		var nContactType 	= null;
		var nContactId 	 	= null;
		var strFirstName	= null;
		var strLastName		= null;
		var strOrgName		= null;
		var strHtml 		= '<ul data-role="listview" data-inset="true" data-theme="c" data-divider-theme="f"><li data-role="list-divider">Search Results</li>';
		
		
		XDOM.find('contact').each(function()
		{  
			node 			= $(this);
			nContactType 	= node.find('Type').text();
			nContactId 		= node.find('Contact_ID').text();
			strFirstName	= xmlEncode(node.find('FirstName').text());
			strLastName		= xmlEncode(node.find('LastName').text());
			strOrgName		= xmlEncode(node.find('OrganizationName').text());
	
			if(nContactType == 1){	
				strHtml+='<li><a href="#" name="openContact" data-hbs-contactid="'+nContactId+'" onclick="">'+
				'<h3>'+strLastName+' '+strFirstName+'</h3><p>'+strOrgName+'</p></a></li>';
				
			}
			else{
				strHtml+='<li><a href="#" name="openContact" data-hbs-contactid="'+nContactId+'" onclick=""><h3>'+strOrgName+'</h3></a></li>';
			}  
		});
	
		strHtml+='</ul>'; 
		$('#results').append(strHtml);	 				
		$('ul[data-role="listview"]').listview();
	
	
		$("a[name='openContact']").on( "click", function(event) 
		{	
			event.preventDefault();	
			var id = $(this).data("hbs-contactid");
			sessionStorage.setItem("nContactId", id);
			$.mobile.changePage("contact.html", {transition: "slide"});
		});
	}	
	
}


function build_Contact()
{
	initPageStatus();
	
	$("#btnBackNavHandler").click(function(event){
		event.preventDefault();
		var strLastPage = getFileName(nav_getLastURL());
		$.mobile.changePage(strLastPage, {reverse: true,transition: "slide"}); //history.back();
	});	
	
	var nContactId = sessionStorage.nContactId;    // nContactID	
	if(nContactId=='' || !nContactId) {
		$("#btnBackNavHandler").trigger('click');
		return;
	}

	BO_Call("GET","/contact/"+nContactId,parseXml,'');
	
	function parseXml(xml)
	{	
		checkErrorCode();
		
		var xmldom 				= $(xml);	
		var strFirstName 		= xmlDecode(xmldom.find("ContactData contact > FirstName").text())+' ';
		var strLastName 		= xmlDecode(xmldom.find("ContactData contact > LastName").text());
		var strFullName 		= strFirstName+' '+strLastName;
		var strCalculatedName 	= xmlDecode(xmldom.find("ContactData contact > CalculatedName").text());
		var strOrgName 			= xmlDecode(xmldom.find("ContactData contact > OrganizationName").text());
		var nConType 			= xmldom.find("ContactData contact > Type").text();	
		var nConId 				= xmlDecode(xmldom.find("Contact_ID").text());	
		
		setSessionkey("ssCalcName", strCalculatedName); 
		
		// locals
		var node;
		var strNumber 	= '';
		var strDisplay 	= '';
		
		// TITLE
		if(nConType == '1') 
			$("div:jqmData(role='header')").append('<h1 class="ui-title" tabindex="0" role="heading" aria-level="1">'+strFullName+'</h1>');
		else 
			$("div:jqmData(role='header')").append('<h1 class="ui-title" tabindex="0" role="heading" aria-level="1">'+strOrgName+'</h1>');
			
		// DESKTOP link
		var strHtml = 
				'<ul data-role="listview" data-inset="true" data-theme="c"><li>'+
				'<img src="images/desktop.png" alt="documents" class="ui-li-icon">'+
				'<a href="#" onclick="" name="openContactDesk" data-hbs-contactid="'+nConId+'" class="indent">Documents, Emails & Links</a></li></ul>';		
		
		// PHONE
		var bPHONE = xmldom.find("Phones").children().length;
		if(bPHONE>0) 
		{
			  strHtml+='<ul data-role="listview" data-inset="true" data-theme="c"><li data-role="list-divider">Phone Numbers</li>';
			  xmldom.find("Phones phone").each(function()
			  {
				node = $(this);		  
				nConType = xmlDecode(node.find('Type').text());
				strNumber = xmlDecode(node.find('PhoneNumber_Truncated').text());	
				strDisplay = xmlDecode(node.find('PhoneNumber').text());
				
				strHtml+='<li><a href="tel:'+strNumber+'" title="'+strNumber+'">'+strDisplay+'<span class="fieldType"><i> ('+nConType+')</i></span></a></li>';
			}); 
			strHtml+='</ul>';
		 }		
  
		// SMS
		var bSMS = xmldom.find("Sms").children().length;
		if(bSMS>0)
		{
			strHtml+='<ul data-role="listview" data-inset="true" data-theme="c"><li data-role="list-divider">Send SMS</li>';
			xmldom.find("Sms sms").each(function()
			{
			  node = $(this);		  
			  nConType = xmlDecode(node.find('Type').text()); 
			  strNumber = xmlDecode(node.find('PhoneNumber_Truncated').text());
			  strDisplay = xmlDecode(node.find('PhoneNumber').text());
			  
			  strHtml+='<li><a href="sms:'+strNumber+'" title="'+strNumber+'">'+strDisplay+'<span class="fieldType"><i> ('+nConType+')</i></span></a></li>';
			});	
			strHtml+='</ul>';
		}
	     
		// Skype (callto?)
		var bSKYPE = xmldom.find("Skype").children().length;
		if(bSKYPE>0)
		{
			strHtml+='<ul data-role="listview" data-inset="true" data-theme="c"><li data-role="list-divider">Skype</li>';
			xmldom.find("Skype skype").each(function()
			{
			  node = $(this);		  
			  nConType = xmlDecode(node.find('Type').text());
			  strNumber = xmlDecode(node.find('SykpeUserName').text());		
			 
			  strHtml+=
					'<li><a href="skype:'+strNumber+'?call">'+strNumber+'<span class="fieldType"><i> (Skype Call)</i></span></a></li>'+
					'<li><a href="skype:'+strNumber+'?chat">'+strNumber+'<span class="fieldType"><i> (Skype Chat)</i></span></a></li>';	
			});	
			strHtml+='</ul>';
		}
	
		// EMAILS
		var bEMAIL = xmldom.find("Emails").children().length;
		if(bEMAIL>0)
		{
	
			strHtml+='<ul data-role="listview" data-inset="true" data-theme="c"><li data-role="list-divider">Emails</li>';
			xmldom.find("Emails email").each(function()
			{
			  node = $(this);		  
			  nConType = xmlDecode(node.find('Type').text()); 
			  strNumber = xmlDecode(node.find('EmailAddress').text());	
			  strHtml+='<li><a href="mailto:'+strNumber+'" title="'+strNumber+'">'+strNumber+'</a></li>';
			});	
			strHtml+='</ul>';
		}  
		
		// WEBSITE		
		var bWEBSITES = xmldom.find("WebSites").children().length;
		if(bWEBSITES>0)
		{
			strHtml+='<ul data-role="listview" data-inset="true" data-theme="c"><li data-role="list-divider">Websites</li>';
			xmldom.find("WebSites website").each(function()
			{
			  node = $(this);		  
			  nConType = xmlDecode(node.find('Type').text()); 
			  strNumber = xmlDecode(node.find('WebAddress').text());	  
			  
			  strHtml+='<li><a href="'+strNumber+'" title="'+strNumber+'">'+strNumber+'<span class="fieldType"><i> ('+nConType+')</i></span></a></li>';
			});	
			strHtml+='</ul>';
		}
		
		// ADDRESS
		var bADDRESS = xmldom.find("Addresses").children().length;
		if(bADDRESS>0)	
		{
			strHtml+='<ul data-role="listview" data-inset="true" data-theme="c"><li data-role="list-divider">Addresses</li>';
			xmldom.find("Addresses address").each(function()
			{
			  node = $(this);		  
			  nConType = xmlDecode(node.find('ADDITIONAL_ADDRESS_TYPE_NAMES').text()); 
			  var strFormatAddress = xmlDecode(node.find('CompleteAddress').text());	
			  var CountryCode = xmlDecode(node.find('CountryCode').text());
			  var CountryName = xmlDecode(node.find('CountryName').text());
			  var Street = xmlDecode(node.find('Street').text());
			  var City = xmlDecode(node.find('City').text());
			  var Zip = xmlDecode(node.find('ZIP').text());
			  
			  if(!strFormatAddress || strFormatAddress=="")
				  strFormatAddress = Street+' '+Zip+','+' '+City+','+' '+CountryName;
			  
			  strHtml+=
				'<li><a href="http://maps.google.com/maps?q='+CountryCode+','+'%20'+City+'%20'+Zip+','+'%20'+Street+'%20'+'" title="'+strFormatAddress+'" target="_blank">'+
					'<h3>'+strFormatAddress+'</h3>'+ 
				'</a></li>';
			});	
			strHtml+='</ul>';	
		}
	
		  // DOM insertion
		  if(strHtml!=''){
			  $('div[data-role="content"]').append(strHtml);
			  $('ul[data-role="listview"]').listview();	
		  }
		
		  $("a[name='openContactDesk']").on( "click", function(event) {	
			  event.preventDefault();	
			  var id = $(this).data("hbs-contactid");
			  sessionStorage.setItem("nContactID", id);
			  sessionStorage.setItem("userType","contact");
			  $.mobile.changePage("desktop.html", {transition: "slide"});
		  });
		}
}

function build_Views()
{
	initPageStatus();
	var nPrevNext;
	var nMaxRecords;
	
	/*-------------------------------
				Init
	---------------------------------*/	
	
	var viewTimeControls =sessionStorage.getItem("viewTimeControls");
	var viewName =sessionStorage.getItem("viewName");
	
	clog("viewTimeControls= "+viewTimeControls);
	clog("viewName= "+viewName);
	
	
	// showhide time navigation (date-range nav) 
	if(viewTimeControls == "0" || !viewTimeControls)
	{ 
		$("div:jqmData(role='controlgroup')").hide().trigger( 'updatelayout' );
	}
	
	// append header title
	var bShowHeaderTitle = $('div:jqmData(role="header") h1');
	if (bShowHeaderTitle.length == 0) 
	{
		$("div:jqmData(role='header')").prepend('<h1 class="ui-title" tabindex="0" role="heading" aria-level="1">'+viewName+'</h1>');
	}
	
	/*-------------------------------
				Button Handlers
	---------------------------------*/	
	
	// return to views selection menu
	$("div:jqmData(role='header') #btnBackNavHandler").click(function(event){
		event.preventDefault();
		window.location=nav_getLastURL();		
	});	

	// reset view (now)	
	$("div:jqmData(role='controlgroup') #btnNow").click(function(event){
		event.preventDefault();
		sessionStorage.sessionPrevNext = 0;
		location.reload();
	});	
	
	// prev period (prev click)
	$("div:jqmData(role='controlgroup') #btnPrev").click(function(event){
		event.preventDefault();
		sessionStorage.sessionPrevNext = parseInt(nPrevNext)-1;
		location.reload();
	});	
	
	// next period	(next click)
	$("div:jqmData(role='controlgroup') #btnNext").click(function(event){
		event.preventDefault();
		sessionStorage.sessionPrevNext = parseInt(nPrevNext)+1;
		location.reload();
	});	
	
	/*-------------------------------
				RPC
	---------------------------------*/	
	
	// todo..
		
}
function build_Desktop()
{
	initPageStatus();
	clog("userType = "+sessionStorage.userType);
	
	var strHtml		= '';
	var uniqueId 	= '';
		
	sessionStorage.ReturnFromDetails="false";	// dom cache?
	

	// PM: 
	// deprecated:
	// isMyDesktop = sessionStorage.mydesk;		// determine wether you are the logged user OR selected contact 
	//	if(!isMyDesktop)	
	//		isMyDesktop = 0;
	// uniqueId = ($.query.get('id')) ? $.query.get('id') : sessionStorage.desktopID; 	
	
	
	// new: 
	//sessionStorage.userType == 
		//	mydesk 
		//  contact
		//  project
		
	
	uniqueId = sessionStorage.nContactID;			
	if(uniqueId=='' || !uniqueId){
		window.location=nav_getLastURL(); 
		return;
	} 
	else {
		setSessionkey("desktopID",uniqueId); 
	}
	
	setSessionkey("nFrom","");
	setSessionkey("nTo","");
	sessionStorage.removeItem("viewFrom");
	sessionStorage.removeItem("viewTo");
	sessionStorage.removeItem("viewId");
	sessionStorage.removeItem("nTotalPages");
	sessionStorage.removeItem("nShowedPageNumber");
	
	if(sessionStorage.userType == "mydesk") // Desktop from logged user
	{
		$("div:jqmData(role='header')").prepend('<a href="#" data-icon="home" class="ui-btn-left ui-btn ui-shadow ui-btn-corner-all ui-btn-icon-left ui-btn-hover-f ui-btn-up-f" data-corners="true" data-shadow="true" data-iconshadow="true" data-wrapperels="span" data-theme="f"><span class="ui-btn-inner ui-btn-corner-all"><span class="ui-btn-text">Home</span><span class="ui-icon ui-icon-home ui-icon-shadow">&nbsp;</span></span></a>');

		// $("#homebtn .ui-btn-text").text("PETAR");
		// $('#homebtn').button('refresh')
		// $("div:jqmData(role='header') > h1").replaceWith('<h1 class="ui-title" tabindex="0" role="heading" aria-level="1">Desktop</h1>');
   }
   else{
	   $("div:jqmData(role='header')").prepend('<a href="#" data-icon="back" data-iconpos="notext" rel="external" class="ui-btn-left ui-btn ui-btn-icon-notext ui-btn-corner-all ui-shadow ui-btn-up-f" title="" data-theme="f"><span class="ui-btn-inner ui-btn-corner-all" aria-hidden="true"><span class="ui-icon ui-icon-back ui-icon-shadow"></span></span></a>');
		
		strHtml+='<ul data-role="listview" data-theme="c" style="margin-bottom: 10px;"><li style="overflow:hidden; text-overflow:ellipsis; white-space:nowrap;"><em>'+sessionStorage.ssCalcName+'</em></li></ul>';		
	} 	
	
	
	if(sessionStorage.userType == "project"){  	// isProject = $.query.get('isProject');     
		BO_Call("GET","/project/"+uniqueId+"/comm_entity/count",cbDesktop_parseCount,'');	
	}
	else
	{	
		if(sessionStorage.userType == "mydesk") //B.T. added -> different for desktop: search by logged user ID:
			BO_Call("GET","/communication/user/"+getCookie('nPersonID')+"/comm_entity/count",cbDesktop_parseCount,'');	
		else
			BO_Call("GET","/contact/"+uniqueId+"/comm_entity/count",cbDesktop_parseCount,'');	
	}
	
	$("div:jqmData(role='header') > a").click(function(event)
	{	
		event.preventDefault();
		
		if(sessionStorage.userType == "mydesk"){
			$.mobile.changePage("menu.html", {reverse: true,transition: "slide"});
		}
		else{
			var strLastPage = getFileName(nav_getLastURL());
			$.mobile.changePage(strLastPage, {reverse: true,transition: "slide"});
		}
	});	
	
	function cbDesktop_parseCount(xml)	
	{		
		checkErrorCode();
	
		var xdom = $(xml);	
		var nFileDocCount=xmlDecode(xdom.find("nFileDocCount").text());
		var nNotesCount=xmlDecode(xdom.find("nNotesCount").text());
		var nEmailCount=xmlDecode(xdom.find("nEmailCount").text());
		var nWebSiteCount=xmlDecode(xdom.find("nWebSiteCount").text());

	// DOCUMENT FINDER
	strHtml+=
	'<div class="ui-grid-d" id="desktopmenu_container">'+
	'<div class="ui-block-a" id="desktopmenu"><div class="ui-bar ui-bar-e">'+
	'<a href="docfind.html" id="desktopmenu_find">Find</a></div></div>';
		
	// DOCUMENTS
	strHtml+='<div class="ui-block-b" id="desktopmenu"><div class="ui-bar ui-bar-e">'+
	'<a name="doclink" href="#" id="desktopmenu_documents" data-param={"url":"documents.html","count":"'+nFileDocCount+'"}>Documents</a>'; 
	// '<a href="documents.html?mydesk='+isMyDesktop+'" id="desktopmenu_documents">Documents</a>'; 		
	//  "mydesk":"'+isMyDesktop+'"
	if (nFileDocCount > 0) { 
		strHtml+= '<span class="ui-li-count">'+nFileDocCount+'</span>' 
	}
	
	// NOTES	
	strHtml+= '</div></div>'+
	'<div class="ui-block-c" id="desktopmenu"><div class="ui-bar ui-bar-e">'+
	'<a name="doclink" href="#" id="desktopmenu_notes" data-param={"url":"notes.html","count":"'+nNotesCount+'"}>Notes</a>'; 
	//'<a href="notes.html?mydesk='+isMyDesktop+'" id="desktopmenu_notes">Notes</a>'; 
	if (nNotesCount > 0) { 
		strHtml+= '<span class="ui-li-count">'+nNotesCount+'</span>' 
	}

	// EMAILS
	strHtml+= '</div></div>'+
	'<div class="ui-block-d" id="desktopmenu"><div class="ui-bar ui-bar-e">'+
	'<a name="doclink" href="#" id="desktopmenu_emails" data-param={"url":"emails.html","count":"'+nEmailCount+'"}>Emails</a>'; 
	//'<a href="emails.html?mydesk='+isMyDesktop+'" id="desktopmenu_emails">Emails</a>';					
	if (nEmailCount > 0) { 
		strHtml+= '<span class="ui-li-count">'+nEmailCount+'</span>' 
	}
	
	// WEBSITES
	strHtml+= '</div></div>'+
	'<div class="ui-block-e" id="desktopmenu"><div class="ui-bar ui-bar-e">'+
	'<a name="doclink" href="#" id="desktopmenu_websites" data-param={"url":"websites.html","count":"'+nWebSiteCount+'"}>Websites</a>'; 
	//'<a href="websites.html?mydesk='+isMyDesktop+'" id="desktopmenu_websites">Websites</a>';						
	if (nWebSiteCount > 0) { 
		strHtml+= '<span class="ui-li-count">'+nWebSiteCount+'</span>' 
	}
	strHtml+= '</div></div></div>';
	
     
	/*
	//----------------------------------------------------------------------
	// Get Views List: user views | contact views | project views
	//----------------------------------------------------------------------
	*/
	
	// isMyDesktop
	if(sessionStorage.userType=="mydesk"){ 
		setSessionkey("viewmode","user");
		BO_Call("GET","/communication/user/views/",cbDesktop_parseViews,'');
	}
	
	//isProject
	else if(sessionStorage.userType=="project"){
		setSessionkey("viewmode","project");
		BO_Call("GET","/communication/project/views/",cbDesktop_parseViews,'');
	}
	// contact : !isMyDesktop && !isProject
	else if(sessionStorage.userType=="contact") {
		setSessionkey("viewmode","contact");
		BO_Call("GET","/communication/contact/views/",cbDesktop_parseViews,'');
	}
	else{
		alert("Session Storage Error Detected. Please login again");
		window.location = "index.html";
	}
}

function cbDesktop_parseViews(xml)
{
	checkErrorCode();
	
	var xdom = $(xml);	
	var strViewName, nViewID, bViewUseFromTo, bShowViewRange, strViewFrom, strViewTo, node;
	var strFrom, strTo;
	
 	strHtml+='<p><ul id="viewmenu" data-role="listview" data-inset="true" data-theme="c">';
	strHtml+='<li data-role="list-divider">Custom Views:</li>';
	
	
	xdom.find('view').each(function(){
		node 				= $(this);
		nViewID 			= node.find("BUSCV_ID").text();
		strViewName 		= xmlDecode(node.find("BUSCV_NAME").text());
		bViewUseFromTo 		= node.find("USE_FROM_TO").text();
		bShowViewRange 		= node.find("DATE_RANGE").text();
		strViewFrom 		= xmlDecode(node.find("DATE_FROM").text());
		strViewTo 			= xmlDecode(node.find("DATE_TO").text());
	    
		strHtml+=
			'<li id="openView">'+
				'<a class="openView" name="view_read" href="" rel="external" onclick="" '+    //href="'+strViewName+'"
				'data-entry={"id":"'+nViewID+'","flag":"'+bViewUseFromTo+'","range":"'+bShowViewRange+'","from":"'+strViewFrom+'","to":"'+strViewTo+'"}>'+strViewName+'</a>'+
			'</li>';
					
	});
	strHtml+='</ul></p>';	
	
	$('div[data-role="content"]').append(strHtml);
	$('div[data-role="page"]').append(strFooter);
	$('ul[data-role="listview"]').listview();
	
	
	// read view handler
	$("a[name='view_read']").on("click", function(event)  
	{	
		event.preventDefault(); 
		setSessionkey("viewName", $(this).text());						// view name
		setSessionkey("viewId", $(this).data("entry").id);				// view id 
		setSessionkey("viewTimeControls", $(this).data("entry").flag);	// view use FromTo (bool)
		setSessionkey("bShowViewRange", $(this).data("entry").range);	// view date Range (step value for prev & next)
		setSessionkey("viewFrom", $(this).data("entry").from);			// view From Date
		setSessionkey("viewTo", $(this).data("entry").to);				// view To Date	
		
		//window.location="views.html?id="+uniqueId;
		$.mobile.changePage("views.html", {transition: "slide"});
	});
	
	// document handler (documents, notes, emails, websites
	$("a[name='doclink']").on("click", function(event)
	{ 
		 event.preventDefault();
		 var strDocLoc = $(this).data("param").url;
		 var strDocCount = $(this).data("param").count; 
		 clog(strDocLoc);
		 clog(strDocCount);
		  
		 if(parseInt(strDocCount)!=0)
		 {
			clog("save a state");
			localStorage.setItem("testme","yes"); 
			//sessionStorage.setItem("nContactId", id);
			//sessionStorage.setItem("nContactId", id);
			$.mobile.changePage(strDocLoc, {transition: "slide"});
		 }
		 else
		 	return false;					
	});
	
	
		//collapsible views list (gui)
		/*$("div[data-role='collapsible']")
		.on('expand', function(){setSessionkey("viewsCollapsed", false);})
		.on('collapse', function(){setSessionkey("viewsCollapsed", true);});*/
	}	
}


function build_Documents()
{
	// isMyDesktop = $.query.get('mydesk'); //B.T. added  //PM: isMyDesktop -> sessionStorage.userType (mydesk, project, user)
	
	initPageStatus();
	
	$("#btnBackNavHandler").click(function(event){
		event.preventDefault();
		$.mobile.changePage("desktop.html", {reverse: true,transition: "slide"}); 
	});	
	
	$('div[data-role="controlgroup"] a#btnPrev').addClass('ui-disabled');  // $("div:jqmData(role='controlgroup')").hide().trigger('updatelayout');		
	var nFrom = sessionStorage.nFrom;
	var nTo = sessionStorage.nTo;
	var nFixedEnd = null;
	
	if(parseInt(nFrom)>0 && nFrom!=''){ 
		$('div[data-role="controlgroup"] a#btnPrev').removeClass('ui-disabled');
	}
	if(nFrom==''||!nFrom) nFrom = 0;
	if(nTo==''||!nTo) nTo = nDocListMaxRows-1;
	
	var strPost = '<REQUEST><PARAMETERS><nDocType>4</nDocType><nFromN>'+nFrom+'</nFromN><nToN>'+nTo+'</nToN></PARAMETERS></REQUEST>';
	if( sessionStorage.userType == "project"){
		BO_Call("POST","/project/"+sessionStorage.desktopID+"/documents",parseXml,strPost);
	}
	else{
		if(sessionStorage.userType == "mydesk")
			BO_Call("POST","/communication/user/"+getCookie('nPersonID')+"/documents",parseXml,strPost);
		else 
			BO_Call("POST","/contact/"+sessionStorage.desktopID+"/documents",parseXml,strPost);		// contact
	}
	
	function parseXml(xml) 
	{	
		checkErrorCode();
		var xmldom = $(xml);
		var nTotalCount = xmlDecode(xmldom.find("PARAMETERS > nTotalCount").text());
		nFixedEnd = nTotalCount;
	
		if(nTotalCount < nDocListMaxRows) { 	
			$('div[data-role="controlgroup"] a#btnNext').addClass('ui-disabled'); 
		}
		if(nTotalCount >= nDocListMaxRows && (parseInt(nTo)+1) >= nFixedEnd) {
		  	$('div[data-role="controlgroup"] a#btnNext').addClass('ui-disabled');  
		}
	
		$("#btnNext").click(function(event){
			event.preventDefault();		
			sessionStorage.nFrom = parseInt(nTo)+1;
			sessionStorage.nTo = parseInt(nTo)+nDocListMaxRows;
			sessionStorage.nShowedPageNumber = parseInt(nShowedPageNumber)+1;
			$.mobile.changePage( "#page_Documents", {allowSamePageTransition: true, reloadPage: true, transition: 'fade'} );
		});
		$("#btnPrev").click(function(event){
			event.preventDefault();	
			sessionStorage.nFrom = parseInt(nFrom)-nDocListMaxRows;
			sessionStorage.nTo   = parseInt(nFrom)-1;
			sessionStorage.nShowedPageNumber = parseInt(nShowedPageNumber)-1;
			$.mobile.changePage( "#page_Documents", {allowSamePageTransition: true, reloadPage: true, transition: 'fade', reverse: true} );
		});
		
		/* Page Number */
		if(!sessionStorage.nTotalPages)
		{
			var nTotalPages = Math.floor(nTotalCount/nDocListMaxRows);
			if(nTotalPages==0){
				nTotalPages=1;
			}
			else if(nTotalCount % nDocListMaxRows != 0) {
				nTotalPages=parseInt(nTotalPages)+1;
			}
			sessionStorage.nTotalPages = nTotalPages;	
			sessionStorage.nShowedPageNumber = 1;
		}
		if(	parseInt(sessionStorage.nFrom) == 0 )
			nShowedPageNumber = 1;
		else
			nShowedPageNumber = sessionStorage.nShowedPageNumber;
		
		var strPageNumber = ''+nShowedPageNumber+'/'+sessionStorage.nTotalPages;
		if(sessionStorage.nTotalPages==1){
			strPageNumber = '';
			$("div:jqmData(role='controlgroup')").hide().trigger( 'updatelayout' );
		}
		parseList($(xml), strPageNumber);	
	}
	
	function parseList(xmldom, strPageNumber)
	{	
		var titleCount = '('+xmlDecode(xmldom.find("PARAMETERS > nTotalCount").text())+')';
		if(strPageNumber!=''){
			$("div:jqmData(role='header')").prepend('<h1 class="ui-title" tabindex="0" role="heading" aria-level="1">Documents '+titleCount+'<span class="nPageNum">'+strPageNumber+'</span></h1>');
		}
		else{
			$("div:jqmData(role='header')").prepend('<h1 class="ui-title" tabindex="0" role="heading" aria-level="1">Documents '+titleCount+'</h1>');
		}
		
		var strHtml = '<ul data-role="listview" data-theme="c">';
		var strExtensionImg = null;
		var thisExt  = null;
		var nFileSizeInBytes = null;
		var nDocumentId = null;
		var strName = null;
		var node = null;	
		xmldom.find('document').each(function()
		{ 
			node = $(this);
			thisExt = xmlDecode(node.find("Extension").text());
			nDocumentId = xmlDecode(node.find("Document_ID").text());
			strName = xmlDecode(node.find("Name").text());	
			strExtensionImg = getDocumentType(thisExt);
			nFileSizeInBytes = bytesToSize(xmlDecode(node.find("Size").text()),1);
			
			strHtml+=
				'<li>'+strExtensionImg+''+
				'<a name="doc_link" href="#" data-hbs-documentid="'+nDocumentId+'" class="indent" onclick="">'+strName+'</a>'+
				'<p class="ui-li-aside"><strong>'+nFileSizeInBytes+'</strong></p></li>';	
		});
		strHtml+='</ul>'; 
		$('div[data-role="content"]').append(strHtml);
		$('div[data-role="page"]').append(strFooter);
		$('ul[data-role="listview"]').listview();
		//ReturnFromDocs
		//sessionStorage.setItem("domRestore",strHtml);
		
		$("a[name='doc_link']").click(function(event){
			event.preventDefault();	
			var doc_id = $(this).data("hbs-documentid");
			BO_Call("GET","/document/"+doc_id+"/check_out_copy",docViewer,'');
			return;
		});	
	}

	function docViewer(xml)
	{
		checkErrorCode();
		
		//sessionStorage.setItem("ReturnFromDocs",true); 
		clog(sessionStorage.ReturnFromDocs);
		
		var xmldom 	= $(xml);
		var docURL 	= ROOT_URL+xmlDecode(xmldom.find("strDocumentURL").text());
		clog(docURL);
		
		//$.mobile.changePage("preview.html", 'pop');
		window.location  =  docURL;  
		// $.mobile.changePage(docURL, {transition: "slide", reloadPage: true});	
		// $.mobile.changePage("index.html", {reverse: true,transition: "slide"});
		// $.mobile.changePage( "#page_Documents", {allowSamePageTransition: true, reloadPage: true, transition: 'flip'} );
		// 
		
		
	}		
	
}


function build_Notes()
{
	initPageStatus();
	$("#btnBackNavHandler").click(function(event){
		event.preventDefault();
		$.mobile.changePage("desktop.html", {reverse: true,transition: "slide"});		
	});		
	$('div[data-role="controlgroup"] a#btnPrev').addClass('ui-disabled'); 
	var nFrom = sessionStorage.nFrom;
	var nTo = sessionStorage.nTo;
	var nFixedEnd = null;
	
	if(parseInt(nFrom)>0 && nFrom!='') 	$('div[data-role="controlgroup"] a#btnPrev').removeClass('ui-disabled');
	if(nFrom==''||!nFrom) nFrom = 0;
	if(nTo==''||!nTo) nTo = nDocListMaxRows-1;
	
	var strPost = '<REQUEST><PARAMETERS><nDocType>3</nDocType><nFromN>'+nFrom+'</nFromN><nToN>'+nTo+'</nToN></PARAMETERS></REQUEST>';
	if( sessionStorage.userType == "project"){
		BO_Call("POST","/project/"+sessionStorage.desktopID+"/documents",parseXml,strPost);	
	}
	else
	{
		if(sessionStorage.userType == "mydesk") 
			BO_Call("POST","/communication/user/"+getCookie('nPersonID')+"/documents",parseXml,strPost);
		else
			BO_Call("POST","/contact/"+sessionStorage.desktopID+"/documents",parseXml,strPost);
	}
	
	function parseXml(xml)
	{	
	  checkErrorCode();
	  var xmldom = $(xml);
	  var nTotalCount = xmlDecode(xmldom.find("PARAMETERS > nTotalCount").text());
	  nFixedEnd = nTotalCount;
	
	  if(nTotalCount < nDocListMaxRows) { 	
		  $('div[data-role="controlgroup"] a#btnNext').addClass('ui-disabled'); 
	  }
	  if(nTotalCount >= nDocListMaxRows && (parseInt(nTo)+1) >= nFixedEnd ) {
		$('div[data-role="controlgroup"] a#btnNext').addClass('ui-disabled');
	  }
	  
	  $("#btnNext").click(function(event){
		  event.preventDefault();		
		  sessionStorage.nFrom = parseInt(nTo)+1;
		  sessionStorage.nTo = parseInt(nTo)+nDocListMaxRows;
		  sessionStorage.nShowedPageNumber = parseInt(nShowedPageNumber)+1; 
		  $.mobile.changePage( "#page_notes", {allowSamePageTransition: true, reloadPage: true, transition: 'fade'} );
	  });
	  $("#btnPrev").click(function(event){
		  event.preventDefault();	
		  sessionStorage.nFrom = parseInt(nFrom)-nDocListMaxRows;
		  sessionStorage.nTo   = parseInt(nFrom)-1;
		  sessionStorage.nShowedPageNumber = parseInt(nShowedPageNumber)-1;
		  $.mobile.changePage( "#page_notes", {allowSamePageTransition: true, reloadPage: true, transition: 'fade'} );
	  });
	
	
	  /* Page Number */
	  if(!sessionStorage.nTotalPages)
	  {
		  var nTotalPages = Math.floor(nTotalCount/nDocListMaxRows);
		  if(nTotalPages==0){
			  nTotalPages=1;
		  }
		  else if(nTotalCount % nDocListMaxRows != 0) {
			  nTotalPages=parseInt(nTotalPages)+1;
		  }
		  sessionStorage.nTotalPages = nTotalPages;	
		  sessionStorage.nShowedPageNumber = 1;
	  }
	  
	  if(parseInt(sessionStorage.nFrom) == 0 )
		  nShowedPageNumber = 1;
	  else
		  nShowedPageNumber = sessionStorage.nShowedPageNumber;
	  
	  var strPageNumber = ''+nShowedPageNumber+'/'+sessionStorage.nTotalPages;
	  if(sessionStorage.nTotalPages==1){
		  strPageNumber = '';
		  $("div:jqmData(role='controlgroup')").hide().trigger( 'updatelayout' );
	  }
	  parseList($(xml), strPageNumber);
	} 

	function parseList(xmldom, strPageNumber)
	{	
		var titleCount = '('+xmlDecode(xmldom.find("PARAMETERS > nTotalCount").text())+')';
		if(strPageNumber!='')
			$("div:jqmData(role='header')").prepend('<h1 class="ui-title" tabindex="0" role="heading" aria-level="1">Notes '+titleCount+'<span class="nPageNum">'+strPageNumber+'</span></h1>');
		else
			$("div:jqmData(role='header')").prepend('<h1 class="ui-title" tabindex="0" role="heading" aria-level="1">Notes '+titleCount+'</h1>');
		
		var html = '<ul data-role="listview" data-theme="c">'; 
		var strExtensionImg = '<img alt="file" src="images/note32.png" class="ui-li-icon"/>'; 
		var node = null;
		var nFileSizeInBytes = null;
		var nDocumentId = null;
		var strName = null;
		var strLastModif = null;
		xmldom.find('document').each(function()
		{ 
			node = $(this);
			nDocumentId	= xmlDecode(node.find("Document_ID").text());
			strName = xmlDecode(node.find("Name").text());
			strLastModif = xmlDecode(node.find("LastModified").text());
			nFileSizeInBytes = bytesToSize(xmlDecode(node.find("Size").text()),1);
			
			html+=
				'<li>'+strExtensionImg+''+
				'<a name="doc_link" href="#" class="indent" data-hbs-id="'+nDocumentId+'" data-hbs-notename="'+strName+'" onclick="">'+			
				'<h3>'+strName+'</h3><p><strong>'+nFileSizeInBytes+'</strong> ['+strLastModif+']</p>'+
				'</a></li>';	
					
				/*
					<LastModified>2009-06-30T12:23:14Z</LastModified>
					<Created>2007-05-01T10:01:05Z</Created>
				*/	
		});
		html+='</ul>'; 
		$('div[data-role="content"]').append(html);
		$('div[data-role="page"]').append(strFooter);
		$('ul[data-role="listview"]').listview();
	
		$("a[name='doc_link']").click(function(event){
			event.preventDefault();	
			var doc_id = $(this).data("hbs-id");
			var noteName = $(this).data("hbs-notename");
			sessionStorage.setItem("noteName",noteName);
			BO_Call("GET","/document/"+doc_id+"/note",docViewer,'');
		});
		
		function docViewer(xml)
		{
		 checkErrorCode();
		 var xmldom = $(xml);
		 var docURL = ROOT_URL+xmlDecode(xmldom.find("strDocumentURL").text());
		
		 //window.location=docURL;		
		 sessionStorage.setItem("notedisplay",docURL);		
		 $.mobile.changePage("notedisplay.html", {transition: "slide"});
		}	
	}

	// CHECKOUT FUNCTIONS
	// pm: notedisplay got this inside iframe
	/*function checkOut(IDcheckout){
		event.preventDefault();	
		BO_Call("GET","/document/"+IDcheckout+"/note",docViewer,'');
	}*/
}

function build_NoteDisplay()
{
	initPageStatus();
	$("#btnBackNavHandler").click(function(event){
		event.preventDefault();
		sessionStorage.setItem("ReturnFromDetails","true");	
		
		var strLastPage = getFileName(nav_getLastURL());
		$.mobile.changePage(strLastPage, {reverse: true,transition: "slide"});
	});	
	  
	var htmlNote = sessionStorage.notedisplay;
	$("div:jqmData(role='header')").prepend('<h1 class="ui-title" tabindex="0" role="heading" aria-level="1">'+sessionStorage.noteName+'</h1>');	
	$('#insertMail iframe').attr("src", htmlNote);
	$('div[data-role="page"]').append(strFooter);
  
	$('iframe').load(function() {
		$("iframe").contents().find("body").css("white-space","pre-wrap !important");
		//$("iframe").contents().find("table").css("width","100% !important");
		calcHeight();
	});
	
	function calcHeight()
	{
	  //find the height of the internal page
	  var the_height=document.getElementById('mailFrame').contentWindow.document.body.scrollHeight;
	  //change the height of the iframe
	  document.getElementById('mailFrame').height=the_height; 
	}
}

function build_Websites()
{
	initPageStatus();
	$("#btnBackNavHandler").click(function(event){	//div:jqmData(role='header') 
		event.preventDefault();
		$.mobile.changePage("desktop.html", {reverse: true,transition: "slide"});	
	});
	$('div[data-role="controlgroup"] a#btnPrev').addClass('ui-disabled'); 
	var nFrom = sessionStorage.nFrom;
	var nTo = sessionStorage.nTo;
	var nFixedEnd = null;
	
	if(parseInt(nFrom)>0 && nFrom!='') $('div[data-role="controlgroup"] a#btnPrev').removeClass('ui-disabled');
	if(nFrom==''||!nFrom) nFrom = 0;
	if(nTo==''||!nTo) nTo = nDocListMaxRows-1;
	
	var strPost = '<REQUEST><PARAMETERS><nDocType>1</nDocType><nFromN>'+nFrom+'</nFromN><nToN>'+nTo+'</nToN></PARAMETERS></REQUEST>';
	if(sessionStorage.userType=="project"){
		BO_Call("POST","/project/"+sessionStorage.desktopID+"/documents",parseXml,strPost);
	}
	else
	{
	  if(sessionStorage.userType=="mydesk")
		  BO_Call("POST","/communication/user/"+getCookie('nPersonID')+"/documents",parseXml,strPost);	
	  else
		  BO_Call("POST","/contact/"+sessionStorage.desktopID+"/documents",parseXml,strPost);	
	}
			
	function parseXml(xml)
	{	
		checkErrorCode();
		var xmldom = $(xml);
		var nTotalCount = xmlDecode(xmldom.find("PARAMETERS > nTotalCount").text());
		nFixedEnd = nTotalCount;
		
		if(nTotalCount < nDocListMaxRows){ 	
			$('div[data-role="controlgroup"] a#btnNext').addClass('ui-disabled'); 
		}
		if(nTotalCount >= nDocListMaxRows && (parseInt(nTo)+1) >= nFixedEnd ) {
			  $('div[data-role="controlgroup"] a#btnNext').addClass('ui-disabled');
		}
		
		$("#btnNext").click(function(event){
			event.preventDefault();		
			sessionStorage.nFrom = parseInt(nTo)+1;
			sessionStorage.nTo = parseInt(nTo)+nDocListMaxRows;
			sessionStorage.nShowedPageNumber = parseInt(nShowedPageNumber)+1;
			$.mobile.changePage( "#page_websites", {allowSamePageTransition: true, reloadPage: true, transition: 'fade'} );
		});
		$("#btnPrev").click(function(event)
		{
			event.preventDefault();	
			sessionStorage.nFrom = parseInt(nFrom)-nDocListMaxRows;
			sessionStorage.nTo = parseInt(nFrom)-1;
			sessionStorage.nShowedPageNumber = parseInt(nShowedPageNumber)-1;
			$.mobile.changePage( "#page_websites", {allowSamePageTransition: true, reloadPage: true, transition: 'fade'} );
		});
		
		/* Page Number */
		if(!sessionStorage.nTotalPages)
		{
			var nTotalPages = Math.floor(nTotalCount/nDocListMaxRows);
			if(nTotalPages == 0){
				nTotalPages = 1;
			}
			else if(nTotalCount % nDocListMaxRows != 0)  {
					nTotalPages = parseInt(nTotalPages)+1;
			}
			sessionStorage.nTotalPages = nTotalPages;	
			sessionStorage.nShowedPageNumber = 1;
		}
		
		if(	parseInt(sessionStorage.nFrom) == 0 )
			nShowedPageNumber = 1;
		else
			nShowedPageNumber = sessionStorage.nShowedPageNumber;
		
		var strPageNumber = ''+nShowedPageNumber+'/'+sessionStorage.nTotalPages;
		if(sessionStorage.nTotalPages==1){
			strPageNumber = '';
			$("div:jqmData(role='controlgroup')").hide().trigger( 'updatelayout' );
		}
		parseList($(xml), strPageNumber);
	}

	function parseList(xmldom, strPageNumber)
	{
		var titleCount = '('+xmlDecode(xmldom.find("PARAMETERS > nTotalCount").text())+')';
		if(strPageNumber!=''){
			$("div:jqmData(role='header')").prepend('<h1 class="ui-title" tabindex="0" role="heading" aria-level="1">Websites '+titleCount+'<span class="nPageNum">'+strPageNumber+'</span></h1>');
		}	
		else{
			$("div:jqmData(role='header')").prepend('<h1 class="ui-title" tabindex="0" role="heading" aria-level="1">Websites '+titleCount+'</h1>');
		}
		
		var strHtml = '<ul data-role="listview" data-theme="c">';
		var strExtensionImg  = '<img alt="website" src="images/earth32.png" class="ui-li-icon"/>';		
		var node = null;
		var strLastPath = null;
		var strName = null;
		var strLastModif = null;
		xmldom.find('document').each(function()
		{
			node = $(this);	
			strLastPath = xmlDecode(node.find("LastPath").text());
			strName = xmlDecode(node.find("Name").text());
			strLastModif = xmlDecode(node.find("LastModified").text());
			
			strHtml+=
				'<li>'+strExtensionImg+''+
				'<a name="openWebsite" href="#" data-hbs-url="'+strLastPath+'" class="indent" onclick="">'+
				'<h3>'+strName+'</h3><p><strong>'+strLastModif+'</strong></p>'+
				'</a></li>';
		});
		strHtml+='</ul>'; 
		$('div[data-role="content"]').append(strHtml);
		$('div[data-role="page"]').append(strFooter);
		$('ul[data-role="listview"]').listview();
		
		$("a[name='openWebsite']").click(function(event){
			event.preventDefault();	
			var doc_url = $(this).data("hbs-url");
			window.location = doc_url;
		});	
	}	
}

function build_Emails()
{
	initPageStatus();
	$("#btnBackNavHandler").click(function(event){
		event.preventDefault();
		$.mobile.changePage("desktop.html", {reverse: true,transition: "slide"});	
	});	
	$('div[data-role="controlgroup"] a#btnNewer').addClass('ui-disabled');
	var nFrom = sessionStorage.nFrom;
	var nTo = sessionStorage.nTo;
	var nFixedEnd = null;

	if(parseInt(nFrom)>0 && nFrom!='') $('div[data-role="controlgroup"] a#btnNewer').removeClass('ui-disabled');
	if(nFrom =='' || !nFrom) nFrom = 0;
	if(nTo =='' || !nTo) nTo = nDocListMaxRows-1;
	
	/*if( sessionStorage.ReturnFromDetails =="true" ){
		sessionStorage.setItem("ReturnFromDetails","false");
		var htmlstrRestore = sessionStorage.ListA;
		var newVertPos = parseInt(sessionStorage.getItem("VertPos"));
		$('div[data-role="content"]').append(htmlstrRestore);
		$('div[data-role="page"]').append(strFooter);
		$('ul[data-role="listview"]').listview();
	}*/
	
	var strPost = '<REQUEST><PARAMETERS><nFromN>'+nFrom+'</nFromN><nToN>'+nTo+'</nToN></PARAMETERS></REQUEST>';
	if(sessionStorage.userType=="project"){
		BO_Call("POST","/project/"+sessionStorage.desktopID+"/emails",parseXml,strPost);
	}
	else
	{
		if(sessionStorage.userType=="mydesk") 
		  BO_Call("POST","/communication/user/"+getCookie('nPersonID')+"/emails",parseXml,strPost);	
		else
		  BO_Call("POST","/contact/"+sessionStorage.desktopID+"/emails",parseXml,strPost);	
	}	
	
	function parseXml(xml)
	{
		checkErrorCode();
		var xmldom = $(xml);
		var nTotalCount = xmlDecode(xmldom.find("PARAMETERS > nTotalCount").text());
		nFixedEnd = nTotalCount;
		
		if(nTotalCount < nDocListMaxRows) { 
			$('div[data-role="controlgroup"] a#btnOlder').addClass('ui-disabled');
		}
		if(nTotalCount >= nDocListMaxRows && (parseInt(nTo)+1) >= nFixedEnd) {
	  		$('div[data-role="controlgroup"] a#btnOlder').addClass('ui-disabled');
		}	
		
		$("#btnOlder").click(function(event){				
			event.preventDefault();		
	    	sessionStorage.nFrom = parseInt(nTo)+1;
			sessionStorage.nTo = parseInt(nTo)+nDocListMaxRows;
			sessionStorage.nShowedPageNumber = parseInt(nShowedPageNumber)+1;
			$.mobile.changePage( "#page_emails", {allowSamePageTransition: true, reloadPage: true, transition: 'fade'} );
			
		});
		$("#btnNewer").click(function(event){			
			event.preventDefault();	
			sessionStorage.nFrom = parseInt(nFrom)-nDocListMaxRows;
			sessionStorage.nTo   = parseInt(nFrom)-1;
			sessionStorage.nShowedPageNumber = parseInt(nShowedPageNumber)-1;
			$.mobile.changePage( "#page_emails", {allowSamePageTransition: true, reloadPage: true, transition: 'fade'} );
		});
		
		/* Page Number */
		if(!sessionStorage.nTotalPages)
		{
			var nTotalPages = Math.floor(nTotalCount/nDocListMaxRows);
			if(nTotalPages==0)
				nTotalPages=1;
			else if(nTotalCount % nDocListMaxRows != 0) 
				nTotalPages=parseInt(nTotalPages)+1;

			sessionStorage.nTotalPages = nTotalPages;	
			sessionStorage.nShowedPageNumber = 1;
		}
		
		if(	parseInt(sessionStorage.nFrom) == 0 )
			nShowedPageNumber = 1;
		else
			nShowedPageNumber = sessionStorage.nShowedPageNumber;
		
		var strPageNumber = ''+nShowedPageNumber+'/'+sessionStorage.nTotalPages;
		if(sessionStorage.nTotalPages==1){
			strPageNumber = '';
			$("div:jqmData(role='controlgroup')").hide().trigger( 'updatelayout' ); 
		}
		parseList($(xml), strPageNumber);
	}		
	
	function parseList(xmldom, strPageNumber)
	{
		var titleCount = '('+xmlDecode(xmldom.find("PARAMETERS > nTotalCount").text())+')';
		if(strPageNumber!='')
			$("div:jqmData(role='header')").prepend('<h1 class="ui-title" tabindex="0" role="heading" aria-level="1">Emails '+titleCount+'<span class="nPageNum">'+strPageNumber+'</span></h1>');
		else
			$("div:jqmData(role='header')").prepend('<h1 class="ui-title" tabindex="0" role="heading" aria-level="1">Emails '+titleCount+'</h1>');
			
		
		var strHtml = '<ul data-role="listview" data-theme="c" class="li-size3">'; 
		var datDate_Last  			= new Date();
		var datDate_Current  		= new Date();
		datDate_Last.setUTCFullYear(1970,1,1);
	    var strExtensionImg = null;
		var cmpLast = null;
		var cmpCurr = null;
		var NumberOfAttachments = null;
		var nIsOutgoing = null;
		var strFrom = null;
		var strTo = null;
		var strParts = null;
		var strSubject = null;
		var nEmailId = null;
		var node = null;
		xmldom.find('email').each(function()
  		{ 
		  node = $(this);		
		  nIsOutgoing	= xmlDecode(node.find("IsOutgoing").text());
		  nEmailId = xmlDecode(node.find("Email_ID").text());
		  datDate_Current	= dateTimeDecode(xmlDecode(node.find("SendRecvDateTime").text())); 
		  cmpLast = datDate_Last.toLocaleDateString();
		  
		  if(datDate_Current)
			  cmpCurr =  datDate_Current.toLocaleDateString();	
		  if(cmpLast != cmpCurr){
			  strHtml+='<li data-role="list-divider">'+datDate_Current.toLocaleDateString()+'</li>';
			  datDate_Last = datDate_Current;
		  }
		
		  // ICON LOADER
		  if(nIsOutgoing == 0)
			  strExtensionImg = '<img alt="incoming" src="images/Incoming32.png" class="ui-li-icon"/>';
		  else
			  strExtensionImg = '<img alt="outgoing" src="images/Outgoing32.png" class="ui-li-icon"/>';
							  
		  //resolve FROM & TO, get contact name if empty set email address as is
		  strFrom=xmlDecode(node.find("Contact_From_Name").text());
		  strTo=xmlDecode(node.find("Contact_To_Name").text());
		  
		  if(strFrom==''){
			  strFrom=xmlDecode(node.find("From").text()); 
		  }
		  //this is ; separated string
		  if(strTo=='') {
			  strTo=xmlDecode(node.find("To").text());
			  strParts = strTo.split(";");
			  
			  if(strParts.length>0)
				  strTo=strParts[0];
		  }
		  
		  strSubject=xmlDecode(node.find("Subject").text());
		  if(strSubject.length>40){
			  strSubject=strSubject.substr(0,40)+"...";
		  }	
		  // PM: strSubject += " ("+cmpCurr+")";
		  NumberOfAttachments = node.find("NumberOfAttachments").text();
		  if(NumberOfAttachments!=0)
		  {
			  strHtml+=
			  '<li>'+strExtensionImg+''+
			  '<a name="email_detail" href="#" data-hbs-emailid="'+nEmailId+'"  class="indent" onclick="">'+	
				'<h3>'+strSubject+'</h3>'+
				'<p><strong>From: </strong><em>'+strFrom+'</em></p>'+
				'<p><strong>To: </strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<em>'+strTo+'</em></p>'+
				'<span class="attach"><img src="images/paperclip.png" style="z-index: 3;"></span>'+
			  '</a></li>';
		  }
		  else{
			  strHtml+=
			  '<li>'+strExtensionImg+''+
			  '<a name="email_detail" href="#" data-hbs-emailid="'+nEmailId+'" class="indent" onclick="">'+	
				'<h3>'+strSubject+'</h3>'+
				'<p><strong>From: </strong><em>'+strFrom+'</em></p>'+
				'<p><strong>To: </strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<em>'+strTo+'</em></p>'+
			  '</a></li>';
		  }
	   });
	
		strHtml+='</ul>'; 
		$('div[data-role="content"]').append(strHtml);
		$('div[data-role="page"]').append(strFooter);
		$('ul[data-role="listview"]').listview();
		// Cache List
		//sessionStorage.setItem("ListA",strHtml);
	
	
		$("a[name='email_detail']").click(function(event)
		{
			event.preventDefault();	
			var doc_id = $(this).data("hbs-emailid"); 
			
			 /*sessionStorage.setItem("ReturnFromDetails","false"); 
			 var currVertPos = $(window).scrollTop(); 
			 sessionStorage.setItem("VertPos",currVertPos);*/
			 
			 //var noteName = $(this).data("hbs-notename");
			 //sessionStorage.setItem("noteName",noteName);
			//window.location="email.html?id="+doc_id;
			sessionStorage.setItem("email_id",doc_id);
			$.mobile.changePage("emaildisplay.html", {transition: "slide"});
		});	
	}	
}

function build_EmailDisplay()
{
	initPageStatus();
	$("#btnBackNavHandler").click(function(event){
		event.preventDefault();
		//sessionStorage.setItem("ReturnFromDetails","true");
		var strLastPage = getFileName(nav_getLastURL());
		$.mobile.changePage(strLastPage, {reverse: true,transition: "slide"});
	});	
	var email_id = sessionStorage.email_id;
  	var mailFrom = '';
  	var strHtml = '';
  	var copyHtmlBody = '';
	
	if(email_id=='' || !email_id) {
		 $("#btnBackNavHandler").trigger('click');
		 return; 
	}
	BO_Call("GET","/email/"+email_id,parseXML,'');
	
	function parseXML(xml) 
	{	
	  checkErrorCode();
	  var xmldom = $(xml);
	  var node = null;
	  
	  var datDate_Current	= '';
	  var strFrom = '';
	  var strTo = '';
	  var temp = '';
	  var arr = ''; 
	  var strParts = '';
	  var strTitle = '';
	  var strSubject = '';
	  var strLocalDate = '';
	  var strLocalTime = '';
	  var strMailTo = '';
	  
	  xmldom.find('email').each(function()
	  { 
		  node = $(this);		
		  datDate_Current	= dateTimeDecode(node.find("SendRecvDateTime").text());
		  
		  //resolve FROM & TO, get contact name if empty set email address as is
		  strFrom=xmlDecode(node.find("Contact_From_Name").text());
		  strTo=xmlDecode(node.find("Contact_To_Name").text());
  
		  // used for REPLY
		  mailFrom=xmlDecode(node.find("From").text()); 
		  strMailTo=xmlDecode(node.find("To").text());
		  
		  // ; delimited list of emails -> take the first only
		  temp = mailFrom;
		  temp = temp.substr(0,temp.length);
		  arr = temp.split(";");
		  mailFrom = arr[0]; 
  
		  if(strFrom == ''){
			  strFrom=xmlDecode(node.find("From").text());
		  }
		  if(strTo=='') //this is ; separated string
		  {
			  strTo=xmlDecode(node.find("To").text());
			  strParts = strTo.split(";");
			  if(strParts.length>0) strTo=strParts[0];
		  }
		 
		  // "-" line length
		  var strLine="-";
		  for (var i=0;i<75;i++){
			 strLine+="-";
		  }
		  
		  // Header Title
		  strTitle="Email: "+datDate_Current.toLocaleDateString();
		  $("div:jqmData(role='header')").prepend('<h1 class="ui-title" tabindex="0" role="heading" aria-level="1">'+strTitle+'</h1>');
		  
		  // From, To, Subject
		  strSubject=xmlDecode(node.find("Subject").text());
  
		  strHtml = '<ul data-role="listview" data-inset="true" data-theme="c">';
		  strHtml+='<li><span style="font-weight:normal;">From:</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<em>'+strFrom+'</em></li>';
		  strHtml+='<li><span style="font-weight:normal;">To:</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<em>'+strTo+'</em></li>';
		  strHtml+='<li><span style="font-weight:normal;">Subject:</span>&nbsp;&nbsp;&nbsp;<em>'+strSubject+'</em></li>';
		  strHtml+='</ul>';
		  
		  strLocalDate = datDate_Current.toLocaleDateString();
		  strLocalTime = datDate_Current.toLocaleTimeString();
  
		  var strReplyBody  = "%0A";
			  strReplyBody += "%0A";
			  strReplyBody += "%0A";
			  strReplyBody += strLine;
			  strReplyBody += "%0A";
			  strReplyBody += "This is a reply to the message from: "+mailFrom;
			  strReplyBody += "%0A";
			  strReplyBody += "To: "+strMailTo;								
			  strReplyBody += "%0A";
			  strReplyBody += "Sent: "+strLocalDate+"%20"+strLocalTime;
			  strReplyBody += "%0A";
			  strReplyBody += "Subject: "+strSubject;
			  strReplyBody += "%0A";
			  strReplyBody += strLine;
			  strReplyBody += "%0A";
			  strReplyBody += "%0A";
			  
		  var strReplyText = "mailto:"+mailFrom+"?subject=Re: "+encodeURIComponent(strSubject)+"&body="+strReplyBody;
		  $("div:jqmData(role='navbar') a#replyMailClick").attr("href", strReplyText); 
		  
		  //mailto:'+mailFrom+'?subject=Re: '+strSubject+'
		  $("div:jqmData(role='navbar') a#forwardMailClick").attr("href", 'mailto:?subject=Fw: '+strSubject+''); 			          //.navbar()
		  
	  });
	  
	  var nNumberOfAttachments = xmldom.find('Attachment').children().length;
	  if(nNumberOfAttachments<=0)
	  {
		  // end here..	
		  $('div[data-role="content"]>#headers').append(strHtml);
		  $('ul[data-role="listview"]').listview();
		  $('div[data-role="page"]').append(strFooter);
	  }
	  else
	  {
  
		  strHtml+='<div data-role="collapsible" data-collapsed="false" data-theme="c" data-content-theme="b" style="padding: 0.5em;">'+
				   '<h3>Attachments ('+nNumberOfAttachments+')</h3>'+
				   '<p><ul data-role="listview" data-inset="true" data-theme="c">';
		  
		  var strExtensionImg = '';
		  var LastExt  = '';
		  var thisExt  = '';
		  var isZip = '';
		  var strName = '';
		  var strAttachId = '';
		  
		  xmldom.find('Attachment document').each(function()
		  { 
			  node = $(this);		
			  thisExt   		= xmlDecode(node.find("Extension").text());
			  isZip			= xmlDecode(node.find("IsZipped").text());
			  strAttachId		= xmlDecode(node.find("Attachment_ID").text());
			  strName			= xmlDecode(node.find("Name").text());
			  strExtensionImg = getDocumentType(thisExt);
	  
			  strHtml +=
			  '<li>'+strExtensionImg+''+
			  '<a name="doc_link" isZip="'+isZip+'" href="'+strAttachId+'" class="indent" rel="external">'+strName+'</a></li>';	
			  //'<a name="doc_link" href="" class="indent" rel="external" data-hbs-iszip="'+isZip+'" data-hbs-id="'+strExtensionImg+'">'+strName+'</a></li>';	
			  
		  });
	  
		strHtml+='</ul></p></div>'; 
		$('div[data-role="content"]>#headers').append(strHtml);
		$('div[data-role=collapsible]').collapsible();
		$('ul[data-role="listview"]').listview();
		$('div[data-role="page"]').append(strFooter);	
	  }
	  
	  // now after adding attachment urls, update their Url directly, instead of doing this inside click handler/call backs
	  // 1) call api for attachment files ( not zipped )
	  // 2) in that CB, loop trough attachments, copy the coresponding Url's and paste in the DOM
	  BO_Call("GET","/email/"+email_id+"/html",cb_mailBody,'');
	  
	  $("a[name='doc_link']").click(function(event)
	  {								   
		  event.preventDefault();	  
		  var doc_id = $(this).attr("href");
		  var var_isZip = $(this).attr("isZip");
		  
		  //var doc_id = $(this).data("hbs-iszip");
		  //var var_isZip = $(this).data("hbs-id");
		  
		  if(var_isZip=='0'){
			  BO_Call("GET","/email/attachment/"+doc_id,docViewer_url,'');
		  }
		  else{
			  setSessionkey("emailZipName", $(this).text());
			  window.location="emailitems.html?id="+doc_id;
		  }
		  return;
	  });
  	}
	
	function cb_mailBody(xml)
	{
		checkErrorCode();
		var xmldom = $(xml);
		var docURL = ROOT_URL+xmlDecode(xmldom.find("strDocumentURL").text());
		copyHtmlBody = docURL;
		$('#insertMail iframe').attr("src", docURL); // $('#insertMail').load(docURL);
		
		$('iframe').load(function() 
		{
			$("iframe").contents().find("body").css("white-space","pre-wrap !important");
			calcHeight();
		});
	}
	
	function docViewer_url(xml)
	{
		event.preventDefault();	
		checkErrorCode(); 
		
		var xmldom = $(xml);
		var docURL = ROOT_URL+xmlDecode(xmldom.find("strDocumentURL").text());
		
		window.location = docURL;
		//$('#insertMail iframe').emtpy();
		//$('#insertMail iframe').attr("src", docURL);
	}
	
	function calcHeight(){
		//find the height of the internal page
		var the_height=document.getElementById('mailFrame').contentWindow.document.body.scrollHeight;
		//change the height of the iframe
		document.getElementById('mailFrame').height=the_height;
	}
}

function build_Projects()
{
	initPageStatus();
	
	var strCurrentListName='';
	var nCurrentListID=0;
	var nCurrentNode_idx=0;
	var nLevelsDeepToLoad=1;
	var showAlerts = false;
	
	// PM
	var browserWidth = $(window).width();
	var browserHeight = $(window).height();	
	if(browserWidth>=500 && browserHeight>=500){
	  $.getScript('scripts/dtree/dtree_large.js');
	}
	else{
		$.getScript('scripts/dtree/dtree.js');
	}

	// DIALOG
	$('#inputUserName').val(localStorage.getItem("user"));
	$("#page_projects #btnSaveUserAccount").click(function(event){
		   event.preventDefault();
		   ChangeUserAccount();
	});
	function ChangeUserAccount()
	{
		if (BO_IsLogged()==false){	
			  window.location="index.html"; 
			  return;
		}
		var strUserName = $('#inputUserName').val();
		var strOldPass = $('#OldPw').val();
		var strNewPass = $('#NewPw').val();
		
		if(strOldPass==null || strNewPass==null){
		  alert("Error: You must Specify Old and New Password");
		  return;
		}
		BO_ChangePassword(strUserName,strOldPass,strNewPass,cb_ChangePassword); 
	}
	function cb_ChangePassword(xml)
	{ 	
	  if(nLastBO_ErrorCode!=0)
	  { 
		  alert(strLastBO_ErrorText);
		  return;
	  }
	  else{
		  $( "#dlgProjectSettings" ).popup( "close" );
	  }
	}    	
	
	$("#cb2").change(function(event) {
		event.preventDefault();
		
		nCurrentListID = $("#cb2 option:selected").val();
		strCurrentListName = $("#cb2 option:selected").text();
		setSessionkey("ProjectStartupListID",nCurrentListID); 
		
		$('#results').empty();
		
		//-------------------------------------create tree ---------------------------------
		d = new dTree('d');
		d.config.inOrder = 'inOrder';
		d.config.useCookies = false;
		d.functCallBack_LoadNode = loadNodeData;
		d.add(0,-1,'Projects '+strCurrentListName);  //  MB
		//-------------------------------------create tree ---------------------------------
		
		loadNodeData(0);
	}); 
	
	$("div:jqmData(role='header') #btnBackNavHandler").click(function(event){
		event.preventDefault();
		
		if (sessionStorage.CSID == CLIENT_HHM) {    // *** MB
			BO_LogOut();
			sessionStorage.clear();
			window.location = "index.html"; // full refresh
		} else {
			$.mobile.changePage("menu.html", {reverse: true,transition: "slidedown"});
		};
	});	
	
	BO_Call("GET","/project/list",parseXML,'');

function parseXML(xml)
{	
	checkErrorCode();
	
	var XMLDOM = $(xml);
	var nStartUpListID = XMLDOM.find("StartupListID").text();
	
	if(parseInt(sessionStorage.ProjectStartupListID)>0 && sessionStorage.ProjectStartupListID!=''){
		nStartUpListID=sessionStorage.ProjectStartupListID;
	}

	var strStartUpListName 	= '';
	var strHtml 			= '';
	var $node 				= ''; 
	var id					= ''; 
	var name				= '';
	var csid = parseInt(sessionStorage.getItem("CSID"));	// *** MB
	var strCalcStartUpListName = '';
	
	function HHM_Project_List_Name () {  // *** MB
		var dpt = sessionStorage.getItem("strDepartmentCode");	// *** MB
		var prefix = "SBB.";
		
		var s = dpt.substr(dpt.indexOf(".")+1,dpt.length-1);
		// var listcode = prefix + dpt.substr(0,dpt.indexOf(".") + 1 + s.indexOf(".") + 1);   // OLD: Two levels until 21.05.2013
		var listcode = prefix + dpt;   // NEW: All levels 21.05.2013
		if (listcode == prefix) { listcode = '';};
		return listcode;
	};

	if (csid == CLIENT_HHM) {  // *** parseInt('55') == 55
		strCalcStartUpListName = HHM_Project_List_Name();	
	};
	
	XMLDOM.find('list').each(function()
	{ 
		$node 	= $(this);	
		id		= $node.find("List_ID").text();
		name	= xmlDecode($node.find("List_Name").text());
		
		if ((csid == CLIENT_HHM) && (name == strCalcStartUpListName)) // *** MB  Project List from Department Code found
		{
			strStartUpListName=name;
			nStartUpListID=id;
			strHtml+='<option value='+id+' selected="selected">'+name+'</option>';		
		}
		else if(id===nStartUpListID)
		{
			strStartUpListName=name;
			strHtml+='<option value='+id+' selected="selected">'+name+'</option>';
		}
		else
		{
			strHtml+='<option value='+id+'>'+name+'</option>';
		}		
	}); 
	
	if(strHtml.length>0){
		$('#cb2').append(strHtml);
		$('#cb2').selectmenu('refresh');
	}
	else{
		alert("empty project");
	}

	setSessionkey("ProjectStartupListID",nStartUpListID);
	
	$('#results').empty();

	strCurrentListName=strStartUpListName;
	nCurrentListID=nStartUpListID;

	//-------------------------------------create tree ---------------------------------
	d = new dTree('d');
	d.config.inOrder = 'inOrder';
	d.config.useCookies = false;
	d.functCallBack_LoadNode = loadNodeData;
	d.add(0,-1,'Projects '+strCurrentListName); //  MB
	
	//d.closeAll();
	//-------------------------------------create tree ---------------------------------
	
	loadNodeData(0);
}	

function loadNodeData(node_idx)
{
	nCurrentNode_idx=node_idx;
	var parent_code='';

	if(!node_idx)
	{
		// alert("1");
		// alert("nCurrentListID = "+nCurrentListID);
		// alert("parent_code = "+parent_code);
		// alert("nLevelsDeepToLoad = "+nLevelsDeepToLoad);
		//if(nCurrentListID == 5) showAlerts = true;
		
		BO_Call("POST","/project/list/data",parseXML_ProjectData,'<REQUEST><PARAMETERS><nListID>'+nCurrentListID+'</nListID><strParentProjectCode>'+parent_code+'</strParentProjectCode><nLevelsDeep>'+nLevelsDeepToLoad+'</nLevelsDeep></PARAMETERS></REQUEST>');
	}
	else
	{
		parent_code=d.aNodes[node_idx].title;
		BO_Call("POST","/project/list/data",parseXML_ProjectData,'<REQUEST><PARAMETERS><nListID>'+0+'</nListID><strParentProjectCode>'+parent_code+'</strParentProjectCode><nLevelsDeep>'+nLevelsDeepToLoad+'</nLevelsDeep></PARAMETERS></REQUEST>');
	}
}			
		
function parseXML_ProjectData(xml)
{	
	checkErrorCode();
	
	var XMLDOM = $(xml);
	var strLastCode="";
	var nLastNodeIdx=0;
	var bLastNodeHasChildren=false;
	var $node = ''; //$(this);		
	var id;
	var parent_id;
	var HasChildren;
	var code;
	var name;
	var myCSID = sessionStorage.getItem("CSID");  // *** MB
	
	//var nNumberOfProjects = XMLDOM.find('Projects').children().length;
	//alert("nNumberOfProjects ="+nNumberOfProjects);
	
	/*
	<project>
        <Project_ID>562</Project_ID>
        <Code>P.A.A.</Code>
        <Name>Projekte SOKRATES Helix</Name>
        <Expanded>1</Expanded>
        <Parent_Project_ID>6</Parent_Project_ID>
        <HasChildren>1</HasChildren>
     </project>
	*/		
	
	XMLDOM.find('project').each(function()
	{ 
		$node 			= $(this);		
		id				= $node.find("Project_ID").text();
		parent_id		= $node.find("Parent_Project_ID").text();
		code			= xmlDecode($node.find("Code").text());
		name			= xmlDecode($node.find("Name").text());
		HasChildren		= false; 
		
				
		if ($node.find("HasChildren").text()=="1") 
			HasChildren=true;
		
		if (parent_id=='')
		{
			parent_id=0;
		}	
		
		/*	
			B.T. 04.06.2013: Nodes that have parent_id in the list and parent node is not present in the lis of projects are not drawn by dTree
			to avoid this problem, we will use this logic: all node id's will be stored in separate list when project is about to be added to the dTree
			this list will be checked out, if parent id is not found then parent_id will be reset to 0. Coz projects must be sorted, this list must contain parent node
			if parent node exists. Exception is root node of stored list: parent id is always set to 0 on server....
		*/
		
		if(!d.isNodeExists(parent_id) && parent_id>0)
		{
			//console.log("splashed: "+code+" parent: "+parent_id);
			parent_id=0;
		}
				
		if (code.indexOf(strLastCode)!=0 && bLastNodeHasChildren && strLastCode!="")
		{
			d.aNodes[nLastNodeIdx].isLoaded=false;
		}
		
		if($node.find("Expanded").text()=='1')
		{

			nLastNodeIdx=d.add(id,parent_id,code + ' ' + name ,'id='+id+";"+code + ' ' + name,true,HasChildren,code) + "";
		}	
		else
		{
			 if (myCSID == CLIENT_HHM) {		// *** HHM  
				if (onAndroid == oniOS && 1 == 2) {
					nLastNodeIdx=d.add(id,parent_id,'<span class="nodeText">' + code + '</span><span class="nodeText">' + '&nbsp;&nbsp;&nbsp;' + name + '</span>','',false,HasChildren,code);  // *** HHM
				} else {
					if(browserWidth>=500 && browserHeight>=500){
						nLastNodeIdx=d.add(id,parent_id,'<div class="treeTextBox"><textarea rows="1" cols="25" class="ProjCode nodeText"  style="">' + code + '</textarea><textarea rows="1" cols="100" class="nodeText"  style="">' + '&nbsp;&nbsp;' + name + '</textarea></div>','',false,HasChildren,code);   // *** HHM  // MB 25 col proj code
					} else {
						nLastNodeIdx=d.add(id,parent_id,'<div class="treeTextBox"><div style="position: absolute; left: 0px; top: 0px;  height: 20px;" ><textarea rows="1" cols="25" class="ProjCode nodeText"  style="">' + code + '</textarea></div><div style="position: absolute; left: 0px; top: 20px;  height: 20px;" class="nodeText"  style="">' + '&nbsp;&nbsp;' + name + '</div></div>','',false,HasChildren,code);   // *** HHM  // MB 25 col proj code  // MB iOS
/* 						nLastNodeIdx=d.add(id,parent_id,'<div class="treeTextBox"><div style="position: absolute; left: 0px; top: 0px;  height: 20px;" class="ProjCode nodeText"  style="">' + code + '</div><div style="position: absolute; left: 0px; top: 20px;  height: 20px;" class="nodeText"  style="">' + '&nbsp;&nbsp;' + name + '</div></div>','',false,HasChildren,code);   // *** HHM  // MB 25 col proj code  // MB iOS
 */					};
					/* nLastNodeIdx=d.add(id,parent_id,'<div class="treeTextBox"><textarea rows="1" cols="20" readonly="readonly" class="ProjCode nodeText"  style="">' + code + '</textarea><textarea rows="1" cols="100" readonly="readonly" class="nodeText"  style="">' + '&nbsp;&nbsp;' + name + '</textarea></div>','',false,HasChildren,code);   */
				};
						
			 } else {
				nLastNodeIdx=d.add(id,parent_id,code + ' ' + name ,'id='+id+";"+code + ' ' + name,false,HasChildren,code);
			 };
			 
			/*
			  id, pid and name are required.
			  Parameters
			  Name 	Type 	Description
			  id 	Number 	Unique identity number.
			  pid 	Number 	Number refering to the parent node. The value for the root node has to be -1.
			  name 	String 	Text label for the node.
			  url 	String 	Url for the node.
			  title 	String 	Title for the node.
			  target 	String 	Target for the node.
			  icon 	String 	Image file to use as the icon. Uses default if not specified.
			  iconOpen 	String 	Image file to use as the open icon. Uses default if not specified.
			  open 	Boolean 	Is the node open.
			  
			  Example
			  
			  mytree.add(1, 0, 'My node', 'node.html', 'node title', 'mainframe', 'img/musicfolder.gif');
			*/
		}
		
		strLastCode=code;
		bLastNodeHasChildren=HasChildren;	
	});

							
	//means last node has children, set loaded=false
	if (bLastNodeHasChildren){
		d.aNodes[nLastNodeIdx].isLoaded=false;
	}
	
	if(!nCurrentNode_idx){

		//----------------------------------------draw root nodes and 1 level deep
		$('#results').append(d.toString()); //write tree
	}
	else{
		//set node as loaded:
		d.aNodes[nCurrentNode_idx].isLoaded=true;
		
		//----------------------------------------draw child nodes
		var arrBefore=d.aIndent;
		d.aIndent=d.aNodes[nCurrentNode_idx].arrIndent;
		

		var str = d.addNode(d.aNodes[nCurrentNode_idx]);
		$("#dd"+nCurrentNode_idx.toString()).append(str);
		//----------------------------------------draw child nodes

		//return old state:
		d.aIndent=arrBefore;
	}
		
	$('#results a').attr('rel', 'external'); // PM

	if((browserWidth<500 || browserHeight<500) && myCSID == CLIENT_HHM){    // MB
		/* $('.dTreeNode').css('height','40px!important');  */
		$('.dTreeNode').height(40);
	};
	
	//pass id + name
	// if ((myCSID == CLIENT_HHM) && !(onAndroid || oniOS)) {		// *** 55
	if (myCSID == CLIENT_HHM) {
		/* $(".ProjCode").live('vclick',function(event) { // *** 55
			mySelect(this);
		}); */
		$(".ProjCode").live('vmouseup',function(event) { // *** HHM
			mySelect(this);
		});
		
	};
	
	
	// select a textarea or input field:
	 function mySelect(elem) {  // *** MB
		//var input = document.getElementById ("myText");
		//elem.focus();
		elem.selectionStart = 0;
		elem.selectionEnd = elem.value.length;
		// elem.select();
		/* if ('selectionStart' in elem) {
            elem.selectionStart = 1;
			elem.selectionEnd = 10;
			elem.focus ();
		}
		else {  // Internet Explorer before version 9
			alert('*');
			var inputRange = elem.createTextRange ();
			inputRange.moveStart ("character", 1);
			inputRange.collapse ();
			inputRange.moveEnd ("character", 1);
			inputRange.select ();
		} */
	};
	

	$("a").live('click',function(event)   //live
	{	
		if (BO_IsLogged()==false){
        	window.location="index.html";
    	}    

		// OLD: var href = $(this).attr("href");	//id=4862;D. Dokumente
		
		//var strTemp = $(this).attr("href");	
		//var strTemp = event.target.href;		//  http://192.168.200.16:5555/m/id=4862;D.%20Dokumente
		var strTemp = $(event.target).closest('a').attr('href');
		clog(strTemp);		
		var nIdx = strTemp.indexOf("id=");
		var href = strTemp.substr(nIdx);		//  id=4862;D.%20Dokumente			
		href = decodeURIComponent(href);		//  id=4862;D. Dokumente	
		
		if(href.substr(0,3)=='id=')    			//id=4862;D. Dokumente	
		{
			event.preventDefault();

			var nT_Idx=href.indexOf(";");
			var projectId=href.substr(3,nT_Idx-3);								// 4;A.	
			var name=href.substr(nT_Idx+1);										// D. Dokumente	
			
			
		    sessionStorage.setItem("nContactID", projectId);
			sessionStorage.setItem("ssCalcName",name); 							//setSessionkey("ssCalcName",name); 
			sessionStorage.setItem("userType","project"); 						// mydesk | project | contact
			$.mobile.changePage("desktop.html", {transition: "slide"});	
		return;		
		}
	});	
}	
	
}

   
	
// extend date object with prev & next 
/*function extendDateObj()
{
	Date.prototype.nextMonth = nextMonth;
	Date.prototype.prevMonth = prevMonth;
	function prevMonth(){
		var thisMonth = this.getMonth();
		this.setMonth(thisMonth-1);
		if(this.getMonth() != thisMonth-1 && (this.getMonth() != 11 || (thisMonth == 11 && this.getDate() == 1)))
			this.setDate(0);
	}
	function nextMonth(){
		var thisMonth = this.getMonth();
		this.setMonth(thisMonth+1);
		if(this.getMonth() != thisMonth+1 && this.getMonth() != 0)
			this.setDate(0);
	}
}*/

function build_CalendarAS()
{	
	/* ***************************** */
	Date.prototype.nextMonth = nextMonth;
	Date.prototype.prevMonth = prevMonth;
	function prevMonth(){
		var thisMonth = this.getMonth();
		this.setMonth(thisMonth-1);
		if(this.getMonth() != thisMonth-1 && (this.getMonth() != 11 || (thisMonth == 11 && this.getDate() == 1)))
			this.setDate(0);
	}
	function nextMonth(){
		var thisMonth = this.getMonth();
		this.setMonth(thisMonth+1);
		if(this.getMonth() != thisMonth+1 && this.getMonth() != 0)
			this.setDate(0);
	}
	/* *****************************  */
	
	initPageStatus();
	
	var dateFrom = sessionStorage.newdateFrom;
	var dateTo = sessionStorage.newdateTo;
	var CalEvs = '';
	
		clog("dateFrom type= "+typeof(dateFrom));
		clog("dateTo type= "+typeof(dateTo));
	
	// init current month on first load
	if(dateFrom =='' || !dateFrom && dateTo =='' || !dateTo) {	
	    setCurrentMonth();
		dateFrom = sessionStorage.dateFrom;
		dateTo = sessionStorage.dateTo;
	}
	
	// main back
	$("div:jqmData(role='header') #btnBackNavHandler").click(function(event)
	{
		event.preventDefault();
		$.mobile.changePage("menu.html", {reverse: true,transition: "slide"});
	});	
	
	// send mail
	$("div:jqmData(role='header') #btnCalendarMail").click(function(event)
	{
		event.preventDefault(); 
		var recipient = sessionStorage.strEmailCalConfAddress;
		var subject = "SOKRATES Calendar Confirmation"; //%20
		var header = "Einsatzplan:%0A%0A";
		var footer = "%0A%0AAnmerkungen:%0A-%0A%0A" + sessionStorage.strFullName;
	    var mailBody = header+CalEvs+footer;	
		var mailTolink = 'mailto:'+recipient+'?subject='+subject+'&body='+mailBody;
		
		clog("recipient= "+recipient);
		clog("subject=" +subject);
		clog("mailBody= "+mailBody);
		
		window.location = mailTolink;
	});	
	
	// reset month to the current (NOW)	
	$("div:jqmData(role='controlgroup') #btnNow").click(function(event)
	{
		event.preventDefault();
		//if (BO_IsLogged()==false) window.location="index.html";
    	   
		sessionStorage.setItem("newdateFrom",sessionStorage.resetFrom);
		sessionStorage.setItem("newdateTo",sessionStorage.resetTo);
		sessionStorage.setItem("lastState",false);
		$.mobile.changePage( "#page_calendar", {allowSamePageTransition: true, reloadPage: true, transition: 'fade'} );
	});	
	
	// increase month (NEXT)
	$("div:jqmData(role='controlgroup') #btnNext").click(function(event)
	{
		event.preventDefault();
		//if (BO_IsLogged()==false) window.location="index.html";
    	
		getNextMonth();
		$.mobile.changePage( "#page_calendar", {allowSamePageTransition: true, reloadPage: true, transition: 'fade'} );
	});	
	
	// decrease month (PREV)
	$("div:jqmData(role='controlgroup') #btnPrev").click(function(event)
	{
		event.preventDefault();
		//if (BO_IsLogged()==false) window.location="index.html";
		
		getPrevMonth();
		$.mobile.changePage( "#page_calendar", {allowSamePageTransition: true, reloadPage: true, transition: 'fade'} );
	});	
	
	BO_Call("POST","/calendar/",parseCalendar,'<REQUEST><PARAMETERS><nFromN>'+dateFrom+'</nFromN><nToN>'+dateTo+'</nToN></PARAMETERS></REQUEST>');
	
	  // init dates on first load/current month schedule
	  function setCurrentMonth()
	  {
		  
		var dx1 = new Date();										 
		var month1 = parseInt(dx1.getMonth()+1);    				 	// current month (from 0-11) -> 1-12 (+1*)
		var year1 = dx1.getFullYear();  							 	// current year (four digits)
		var lastDayOfMonth = new Date(year1, month1, 0).getDate(); 	// current last month day (two digits)
	  
		createFromTo(year1,month1,lastDayOfMonth);
	  }	
		  
	  // previous month 
	  function getPrevMonth() 
	  {
		  var today;	
		  if(sessionStorage.lastState=="true"){
			  today = new Date(sessionStorage.lastYear,sessionStorage.lastMonth,sessionStorage.lastDay);
			  today.prevMonth(); 	
		  }
		  else{
			  today = new Date(); 
			  today.prevMonth();
		  }		
		  saveLastDates(today.getFullYear(),today.getMonth(),today.getDate());
	  
		  var newMonth = parseInt(today.getMonth()+1);
		  var newYear = today.getFullYear();
		  var newlastDayOfMonth = new Date(newYear, newMonth, 0).getDate();		
		  
		  var strnewMonth=newMonth.toString();
		  if(newMonth<10)
			  strnewMonth='0'+strnewMonth;
		  
		  var newdateFrom = newYear + '-' + strnewMonth + '-01';
		  var newdateTo = newYear + '-' + strnewMonth + '-' + newlastDayOfMonth; 
		  sessionStorage.setItem("newdateFrom",newdateFrom);
		  sessionStorage.setItem("newdateTo",newdateTo);
	  }
	  
	  // next month 
	  function getNextMonth()
	  {
		  var today;	
		  if(sessionStorage.lastState=="true"){
			  today = new Date(sessionStorage.lastYear,sessionStorage.lastMonth,sessionStorage.lastDay);
			  today.nextMonth(); 	
		  }
		  else{
			  today = new Date(); 
			  today.nextMonth();
		  }
		  saveLastDates(today.getFullYear(),today.getMonth(),today.getDate());
	  
		  var newMonth = parseInt(today.getMonth()+1);
		  var newYear = today.getFullYear();
		  var newlastDayOfMonth = new Date(newYear, newMonth, 0).getDate();		
		  
		  var strnewMonth=newMonth.toString();
		  if(newMonth<10)
			  strnewMonth='0'+strnewMonth;
		  
		  var newdateFrom = newYear + '-' + strnewMonth + '-01';
		  var newdateTo = newYear + '-' + strnewMonth + '-' + newlastDayOfMonth; 
		  sessionStorage.setItem("newdateFrom",newdateFrom);
		  sessionStorage.setItem("newdateTo",newdateTo);
	  }
	  
	    // store last used dates
		function saveLastDates(year,month,day) 
		{
			sessionStorage.setItem("lastState",true);
			sessionStorage.setItem("lastYear",year);
			sessionStorage.setItem("lastMonth",month);
			sessionStorage.setItem("lastDay",day);
		}	

	  // first strings create
	  function createFromTo(cYear,cMonth,cLastDay)
	  {	
		var strMonth=cMonth.toString();								
		if(cMonth<10) 
		  strMonth='0'+strMonth;
		
		var dateFrom = cYear+'-'+strMonth+'-01';
		var dateTo = cYear+'-'+strMonth+'-'+cLastDay; 	
	   
		setSessionkey("dateFrom", dateFrom);
		setSessionkey("dateTo", dateTo); 
		
		setSessionkey("resetFrom", dateFrom);
		setSessionkey("resetTo", dateTo);
	  }

	  function parseCalendar(xml)
	  {	 
		  checkErrorCode();
		  clog(xml);
		  var XMLDOM = $(xml);
		  var nTotalCount = '('+xmlDecode(XMLDOM.find("PARAMETERS > nTotalCount").text())+')';
		  
		  $("div:jqmData(role='header')").prepend('<h1 class="ui-title" tabindex="0" role="heading" aria-level="1"></h1>');
		  
		  // local From /Todate (period under header)
		  var Ret_strFrom = xmlDecode(XMLDOM.find("PARAMETERS > Ret_strFrom").text());
		  var Ret_strTo = xmlDecode(XMLDOM.find("PARAMETERS > Ret_strTo").text());
	  
		  var strHtml = '<ul data-role="listview" data-theme="c" style="margin:0;" class="period"  ><li style="text-align:center;">'+
						  '<span class="viewFromInfo">From: '+Ret_strFrom+'</span><span class="viewToInfo">   To: '+Ret_strTo+'</span>'+
						'</li></ul>'+			
						'<ul data-role="listview" data-theme="c" style="margin:0px;" class="li-size3">'; 
					  
		  var $node = '';
		  CalEvs = '';
		  
		  var BCEP_ID = '',
			  BCEP_DAT_LAST_MODIFIED = '',
			  BCEP_EVENT_ID = '',
			  CENT_ID = '',
			  BCEP_STATUS = '',
			  BCEP_PRESENCE_TYPE_ID = '',
			  BCEV_ID = '',
			  BCEV_TITLE = '',
			  BCEV_IS_INFORMATION = '',
			  BCEV_IS_PRELIMINARY = '',
			  BCOL_SUBJECT = '',
			  BCOL_FROM = '',
			  BCOL_TO = '',
			  BCOL_LOCATION = '',
			  BCOL_DESCRIPTION = '',
			  EVENT_TITLE = '',
			  TIME_FROM = '',
			  TIME_TO = '',
			  DATE = '',				// e.g: 2012-02-29
			  DATE_LOCAL_STR = '',	// local event date (localization from communicator server)
			  STATUS = '',
			  ASSIGNED_LIST = '';
		  
		  var LastDate = '',
			  NewDate = '',
			  imgStatusCircle = '';
			  
		  var strLine1 = '',
			  strLine2 = '',
			  strLine3 = '';
			  
		  var bHighlight = '';	
		  
		  var dtFrom = '';
		  var dtTo = '';
		  var strSplitFrom = '';
		  var strSplitTo = '';
		  
		  
		  var html = '';
		  var statusIcon = '';
		  
		  XMLDOM.find("row").each(function()
		  {
			  $node = $(this);
			  bHighlight = false;
			  
			  BCEP_ID = $node.find("BCEP_ID").text();
			  BCEP_DAT_LAST_MODIFIED = $node.find("BCEP_DAT_LAST_MODIFIED").text();
			  BCEP_EVENT_ID = $node.find("BCEP_EVENT_ID").text();
			  CENT_ID = $node.find("CENT_ID").text();
			  BCEP_STATUS = $node.find("BCEP_STATUS").text();
			  BCEP_PRESENCE_TYPE_ID = $node.find("BCEP_PRESENCE_TYPE_ID").text();
			  
			  BCEV_ID = $node.find("BCEV_ID").text();
			  BCEV_TITLE = $node.find("BCEV_TITLE").text();
			  BCEV_IS_INFORMATION = $node.find("BCEV_IS_INFORMATION").text();
			  BCEV_IS_PRELIMINARY = $node.find("BCEV_IS_PRELIMINARY").text();
			  
			  BCOL_SUBJECT = $node.find("BCOL_SUBJECT").text();
			  BCOL_FROM = $node.find("BCOL_FROM").text();
			  BCOL_TO = $node.find("BCOL_TO").text();
			  BCOL_LOCATION = $node.find("BCOL_LOCATION").text();
			  BCOL_DESCRIPTION = $node.find("BCOL_DESCRIPTION").text();
			  
			  EVENT_TITLE = $node.find("EVENT_TITLE").text();
			  TIME_FROM = $node.find("TIME_FROM").text();
			  TIME_TO = $node.find("TIME_TO").text();
			  DATE = $node.find("DATE").text();
			  STATUS = $node.find("STATUS").text();
			  ASSIGNED_LIST = $node.find("ASSIGNED_LIST").text();
			  DATE_LOCAL_STR = $node.find("DATE_LOCAL_STR").text();
			  LastDate = "1970-01-01";
			  NewDate = DATE.toString();
			  
			  
			  // FOR: AS specific!
			 
			  strLine1 = EVENT_TITLE;	// Line 1
			  strLine2 = DATE_LOCAL_STR+', '+ TIME_FROM+' - '+TIME_TO;   // Line 2 "CalEv for the same day"
			  strLine3 = ASSIGNED_LIST;	// Line 3
					  
			  // ICON LOADER
			  if(STATUS==0)
				  statusIcon = '<img alt="past" src="images/Status_Red.png" class="ui-li-icon"/>';
			  if(STATUS==1)
				  statusIcon = '<img alt="preliminary" src="images/Status_Yellow.png" class="ui-li-icon"/>';
			  if(STATUS==2)
				  statusIcon = '<img alt="final" src="images/Status_Green.png" class="ui-li-icon"/>';
			  if(STATUS>2)
				  statusIcon = '';
			  
			 
			//BCEV_IS_INFORMATION
			//BCEV_IS_INFORMATION=1;
			if(BCEV_IS_INFORMATION==1)			
			{	
				 strHtml+=
					  '<li style="background-color:rgb(23,54,93) !important;">'+''+			// highlight this	,	
						  '<a name="" href="#" class="indent">'+
							  '<h4>'+strLine1+'</h4>'+
							  '<p><strong><em>'+strLine2+'</em></strong></p>'+
							  '<p><strong><em>'+strLine3+'</em></strong></p>'+
						  '</a>'+
					  '</li>';
			}
			else
			{
				 strHtml+=
					  '<li>'+statusIcon+''+			
						  '<a name="" href="#" class="indent">'+
							  '<h4>'+strLine1+'</h4>'+
							  '<p><strong><em>'+strLine2+'</em></strong></p>'+
							  '<p><strong><em>'+strLine3+'</em></strong></p>'+
						  '</a>'+
					  '</li>';

			CalEvs += '%0A'+DATE_LOCAL_STR+': '+EVENT_TITLE;
			}  
			  
		  });
		  strHtml+='</ul>'; 
		  $('div[data-role="content"]').append(strHtml);
		  $('div[data-role="page"]').append(strFooter);
		  $('ul[data-role="listview"]').listview();
	  }
}

function build_Calendar()
{	
	/* ***************************** */
	Date.prototype.nextMonth = nextMonth;
	Date.prototype.prevMonth = prevMonth;
	function prevMonth(){
		var thisMonth = this.getMonth();
		this.setMonth(thisMonth-1);
		if(this.getMonth() != thisMonth-1 && (this.getMonth() != 11 || (thisMonth == 11 && this.getDate() == 1)))
			this.setDate(0);
	}
	function nextMonth(){
		var thisMonth = this.getMonth();
		this.setMonth(thisMonth+1);
		if(this.getMonth() != thisMonth+1 && this.getMonth() != 0)
			this.setDate(0);
	}
	/* *****************************  */
	
	initPageStatus();
	
	var dateFrom = sessionStorage.newdateFrom;
	var dateTo = sessionStorage.newdateTo;
	var CalEvs = '';
	
		clog("dateFrom type= "+typeof(dateFrom));
		clog("dateTo type= "+typeof(dateTo));
	
	// init current month on first load
	if(dateFrom =='' || !dateFrom && dateTo =='' || !dateTo) {	
	    setCurrentMonth();
		dateFrom = sessionStorage.dateFrom;
		dateTo = sessionStorage.dateTo;
	}
	
	// main back
	$("div:jqmData(role='header') #btnBackNavHandler").click(function(event)
	{
		event.preventDefault();    
		//$.mobile.changePage(strLastPage, {reverse: true,transition: "slide"});
		$.mobile.changePage("menu.html", {reverse: true,transition: "slide"});
	});	
	
	// reset month to the current (NOW)	
	$("div:jqmData(role='controlgroup') #btnNow").click(function(event)
	{
		event.preventDefault();
		//if (BO_IsLogged()==false) window.location="index.html";
    	   
		sessionStorage.setItem("newdateFrom",sessionStorage.resetFrom);
		sessionStorage.setItem("newdateTo",sessionStorage.resetTo);
		sessionStorage.setItem("lastState",false);
		$.mobile.changePage( "#page_calendar", {allowSamePageTransition: true, reloadPage: true, transition: 'fade'} );
	});	
	
	// increase month (NEXT)
	$("div:jqmData(role='controlgroup') #btnNext").click(function(event)
	{
		event.preventDefault();
		//if (BO_IsLogged()==false) window.location="index.html";
    	
		getNextMonth();
		$.mobile.changePage( "#page_calendar", {allowSamePageTransition: true, reloadPage: true, transition: 'fade'} );
	});	
	
	// decrease month (PREV)
	$("div:jqmData(role='controlgroup') #btnPrev").click(function(event)
	{
		event.preventDefault();
		//if (BO_IsLogged()==false) window.location="index.html";
		
		getPrevMonth();
		$.mobile.changePage( "#page_calendar", {allowSamePageTransition: true, reloadPage: true, transition: 'fade'} );
	});	
	
	BO_Call("POST","/calendar/",parseCalendar,'<REQUEST><PARAMETERS><nFromN>'+dateFrom+'</nFromN><nToN>'+dateTo+'</nToN></PARAMETERS></REQUEST>');
	
	  // init dates on first load/current month schedule
	  function setCurrentMonth()
	  {
		  
		var dx1 = new Date();										 
		var month1 = parseInt(dx1.getMonth()+1);    				 	// current month (from 0-11) -> 1-12 (+1*)
		var year1 = dx1.getFullYear();  							 	// current year (four digits)
		var lastDayOfMonth = new Date(year1, month1, 0).getDate(); 	// current last month day (two digits)
	  
		createFromTo(year1,month1,lastDayOfMonth);
	  }	
		  
	  // previous month 
	  function getPrevMonth() 
	  {
		  var today;	
		  if(sessionStorage.lastState=="true"){
			  today = new Date(sessionStorage.lastYear,sessionStorage.lastMonth,sessionStorage.lastDay);
			  today.prevMonth(); 	
		  }
		  else{
			  today = new Date(); 
			  today.prevMonth();
		  }		
		  saveLastDates(today.getFullYear(),today.getMonth(),today.getDate());
	  
		  var newMonth = parseInt(today.getMonth()+1);
		  var newYear = today.getFullYear();
		  var newlastDayOfMonth = new Date(newYear, newMonth, 0).getDate();		
		  
		  var strnewMonth=newMonth.toString();
		  if(newMonth<10)
			  strnewMonth='0'+strnewMonth;
		  
		  var newdateFrom = newYear + '-' + strnewMonth + '-01';
		  var newdateTo = newYear + '-' + strnewMonth + '-' + newlastDayOfMonth; 
		  sessionStorage.setItem("newdateFrom",newdateFrom);
		  sessionStorage.setItem("newdateTo",newdateTo);
	  }
	  
	  // next month 
	  function getNextMonth()
	  {
		  var today;	
		  if(sessionStorage.lastState=="true"){
			  today = new Date(sessionStorage.lastYear,sessionStorage.lastMonth,sessionStorage.lastDay);
			  today.nextMonth(); 	
		  }
		  else{
			  today = new Date(); 
			  today.nextMonth();
		  }
		  saveLastDates(today.getFullYear(),today.getMonth(),today.getDate());
	  
		  var newMonth = parseInt(today.getMonth()+1);
		  var newYear = today.getFullYear();
		  var newlastDayOfMonth = new Date(newYear, newMonth, 0).getDate();		
		  
		  var strnewMonth=newMonth.toString();
		  if(newMonth<10)
			  strnewMonth='0'+strnewMonth;
		  
		  var newdateFrom = newYear + '-' + strnewMonth + '-01';
		  var newdateTo = newYear + '-' + strnewMonth + '-' + newlastDayOfMonth; 
		  sessionStorage.setItem("newdateFrom",newdateFrom);
		  sessionStorage.setItem("newdateTo",newdateTo);
	  }
	  
	    // store last used dates
		function saveLastDates(year,month,day) 
		{
			sessionStorage.setItem("lastState",true);
			sessionStorage.setItem("lastYear",year);
			sessionStorage.setItem("lastMonth",month);
			sessionStorage.setItem("lastDay",day);
		}	

	  // first strings create
	  function createFromTo(cYear,cMonth,cLastDay)
	  {	
		var strMonth=cMonth.toString();								
		if(cMonth<10) 
		  strMonth='0'+strMonth;
		
		var dateFrom = cYear+'-'+strMonth+'-01';
		var dateTo = cYear+'-'+strMonth+'-'+cLastDay; 	
	   
		setSessionkey("dateFrom", dateFrom);
		setSessionkey("dateTo", dateTo); 
		
		setSessionkey("resetFrom", dateFrom);
		setSessionkey("resetTo", dateTo);
	  }

	  function parseCalendar(xml)
	  {	 
		  checkErrorCode();
		  var XMLDOM = $(xml);
		  var nTotalCount = '('+xmlDecode(XMLDOM.find("PARAMETERS > nTotalCount").text())+')';
		  $("div:jqmData(role='header')").prepend('<h1 class="ui-title" tabindex="0" role="heading" aria-level="1">Schedule '+nTotalCount+'</h1>');
		  
		  // local From /Todate (period under header)
		  var Ret_strFrom = xmlDecode(XMLDOM.find("PARAMETERS > Ret_strFrom").text());
		  var Ret_strTo = xmlDecode(XMLDOM.find("PARAMETERS > Ret_strTo").text());
	  
		  var strHtml = '<ul data-role="listview" data-theme="c" style="margin:0;" class="period"  ><li style="text-align:center;">'+
						  '<span class="viewFromInfo">From: '+Ret_strFrom+'</span><span class="viewToInfo">   To: '+Ret_strTo+'</span>'+
						'</li></ul>'+			
						'<ul data-role="listview" data-theme="c" style="margin:0px;" class="li-size3">'; 
					  
		  var $node = '';
		  CalEvs = '';
		  
		  var BCEP_ID = '',
			  BCEP_DAT_LAST_MODIFIED = '',
			  BCEP_EVENT_ID = '',
			  CENT_ID = '',
			  BCEP_STATUS = '',
			  BCEP_PRESENCE_TYPE_ID = '',
			  BCEV_ID = '',
			  BCEV_TITLE = '',
			  BCEV_IS_INFORMATION = '',
			  BCEV_IS_PRELIMINARY = '',
			  BCOL_SUBJECT = '',
			  BCOL_FROM = '',
			  BCOL_TO = '',
			  BCOL_LOCATION = '',
			  BCOL_DESCRIPTION = '',
			  EVENT_TITLE = '',
			  TIME_FROM = '',
			  TIME_TO = '',
			  DATE = '',				// e.g: 2012-02-29
			  DATE_LOCAL_STR = '',	// local event date (localization from communicator server)
			  STATUS = '',
			  ASSIGNED_LIST = '';
		  
		  var LastDate = '',
			  NewDate = '',
			  imgStatusCircle = '';
			  
		  var strLine1 = '',
			  strLine2 = '',
			  strLine3 = '';
			  
		  var bHighlight = '';	
		  
		  var dtFrom = '';
		  var dtTo = '';
		  var strSplitFrom = '';
		  var strSplitTo = '';
		  
		  
		  XMLDOM.find("row").each(function()
		  {
			  $node = $(this);
			  bHighlight = false;
			  
			  BCEP_ID = $node.find("BCEP_ID").text();
			  BCEP_DAT_LAST_MODIFIED = $node.find("BCEP_DAT_LAST_MODIFIED").text();
			  BCEP_EVENT_ID = $node.find("BCEP_EVENT_ID").text();
			  CENT_ID = $node.find("CENT_ID").text();
			  BCEP_STATUS = $node.find("BCEP_STATUS").text();
			  BCEP_PRESENCE_TYPE_ID = $node.find("BCEP_PRESENCE_TYPE_ID").text();
			  
			  BCEV_ID = $node.find("BCEV_ID").text();
			  BCEV_TITLE = $node.find("BCEV_TITLE").text();
			  BCEV_IS_INFORMATION = $node.find("BCEV_IS_INFORMATION").text();
			  BCEV_IS_PRELIMINARY = $node.find("BCEV_IS_PRELIMINARY").text();
			  
			  BCOL_SUBJECT = $node.find("BCOL_SUBJECT").text();
			  BCOL_FROM = $node.find("BCOL_FROM").text();
			  BCOL_TO = $node.find("BCOL_TO").text();
			  BCOL_LOCATION = $node.find("BCOL_LOCATION").text();
			  BCOL_DESCRIPTION = $node.find("BCOL_DESCRIPTION").text();
			  
			  EVENT_TITLE = $node.find("EVENT_TITLE").text();
			  TIME_FROM = $node.find("TIME_FROM").text();
			  TIME_TO = $node.find("TIME_TO").text();
			  DATE = $node.find("DATE").text();
			  STATUS = $node.find("STATUS").text();
			  ASSIGNED_LIST = $node.find("ASSIGNED_LIST").text();
			  DATE_LOCAL_STR = $node.find("DATE_LOCAL_STR").text();
			  LastDate = "1970-01-01";
			  NewDate = DATE.toString();
			  
			  // TITLE "Event" LOADER (date)
			  if(LastDate != NewDate) {
				  strHtml+='<li data-role="list-divider">'+DATE_LOCAL_STR+'</li>'; 
				  LastDate = NewDate;
			  }	
			  
			  // Line 1
			  if(BCEV_TITLE!=BCOL_SUBJECT && BCOL_SUBJECT!=''){
				  strLine1 = BCOL_SUBJECT+' / '+BCEV_TITLE;	
			  }
			  else if(BCOL_SUBJECT!=''){
				  strLine1 = BCOL_SUBJECT;
			  }
			  else{
				  strLine1 = BCEV_TITLE;
			  }
			  
			  // Line 2
			  // "Do, 22. März 2012 16:00 - Do, 29. März 2012 21:00"
			  strSplitFrom = BCOL_FROM.split('T'); 
			  strSplitFrom = strSplitFrom[0].split('-');
			  strSplitTo	= BCOL_TO.split('T'); 
			  strSplitTo	= strSplitTo[0].split('-');
			  dtFrom = new Date(strSplitFrom[0], strSplitFrom[1]-1, strSplitFrom[2]);
			  dtTo = new Date(strSplitTo[0], strSplitTo[1]-1, strSplitTo[2]);
			  
			  // Multi-day CalEv
			  if( dtFrom.getDate() != dtTo.getDate() ){
			  
				  //"Do, 22. März 2012 16:00 - Do, 29. März 2012 21:00"	
				  strLine2 = dtFrom.toDateString() + ' ' + TIME_FROM + ' - '+ dtTo.toDateString() + ' ' + TIME_TO + '&nbsp;&nbsp;' + BCOL_LOCATION;
		  
			  }
			  // CalEv for the same day
			  else{ 	
				  strLine2 = TIME_FROM+' - '+TIME_TO +'&nbsp;&nbsp;'+BCOL_LOCATION;
			  }
			  
			  
			  // Line 3
			  strLine3 = ASSIGNED_LIST;
			  
			  // red - past event
			  if(STATUS==0){
				  imgStatusCircle = '<img alt="past" src="images/Status_Red.png" class="ui-li-icon"/>';
			  }
			  
			  // yellow - preliminary
			  else if(STATUS==1){
				  imgStatusCircle = '<img alt="preliminary" src="images/Status_Yellow.png" class="ui-li-icon"/>';
			  }
			  
			  // 	green - final
			  else if(STATUS==2){
				  imgStatusCircle = '<img alt="final" src="images/Status_Green.png" class="ui-li-icon"/>';
			  }
			  
			  // light blue background, but no dot
			  else if(STATUS==3){
				  bHighlight = true;
			  }
			  else if(STATUS==4){
				  imgStatusCircle = '';
			  }
			  
	  
			  if(BCEV_IS_INFORMATION != 1)
			  {
				  if(bHighlight)
				  {
					  strHtml+=
					  '<li data-theme="c">'+	// change to blue color
						  '<a name="" href="#">'+
							  '<h4>'+strLine1+'</h4>'+
							  '<p><strong><em>'+strLine2+'</em></strong></p>'+
							  '<p><strong><em>'+strLine3+'</em></strong></p>'+
						  '</a>'+
					  '</li>';	
				  }
				  else
				  {
					  strHtml+=
					  '<li>'+imgStatusCircle+''+
						  '<a name="" href="#" class="indent">'+
							  '<h4>'+strLine1+'</h4>'+
							  '<p><strong><em>'+strLine2+'</em></strong></p>'+
							  '<p><strong><em>'+strLine3+'</em></strong></p>'+
						  '</a>'+
					  '</li>';	
				  }
				  
			  }
			  else	// BCEV_IS_INFORMATION = 1
			  {
				  strHtml+=
					  '<li data-theme="e">'+imgStatusCircle+''+
						  '<a name="" href="#" class="indent">'+
							  '<h4>'+strLine1+'</h4>'+
							  '<p><strong><em>'+strLine2+'</em></strong></p>'+
							  '<p><strong><em>'+strLine3+'</em></strong></p>'+
						  '</a>'+
					  '</li>';
	  
				  //CalEvs += '%0A'+DATE+': '+EVENT_TITLE;
			  }
			  
			  /*	First handle STATUS:
			  
				1 - preliminary (yellow)
				2 - final (green)
				3 - light blue background, but no dot
				4 - do not show any dot at all
			  
			  	afterwards handle BCEV_IS_INFORMATION:
			  	If 1 then light blue background (if it isn't already 3), else nothing
			  */
		  
		  });
		  strHtml+='</ul>'; 
		  $('div[data-role="content"]').append(strHtml);
		  $('div[data-role="page"]').append(strFooter);
		  $('ul[data-role="listview"]').listview();
	  }
}


// for each loaded page check logged status and register current URL (absolute) for Navigations
// if bReset = true reset the Url Navigation Array
function initPageStatus(bReset)
{
	if (BO_IsLogged()==false){	
        window.location="index.html"; 	// full refresh: user's session probably ended
	}
	else
	{	
	  if(bReset){
		  nav_clear();
		  nav_registerURL();
	  }
	  else{
		nav_registerURL();
	  }
    }    		
}


/* INFO:


$().bind('click',fn) VS. $().click(fn)

These handlers are the same thing.  For our purposes we would most of the time use .on or .live (deprecated) when handling click triggers for dynamic data. 
These mentioned handlers above are good for static content that always exist on a page: e.g: back buttons, etc...
		
Difference is hat you can also bind more than one event handler in one go using bind

$('#mydiv').bind('mouseover focus', function() {
	$(this).addClass('focus')
});

which is cleaner equivalent to:

var myFunc = function() {
	$(this).addClass('focus');
};
$('#mydiv')
	.mouseover(myFunc)
	.focus(myFunc);


.ON vs .LIVE 

.LIVE  "attach an event handler for all elements which match the current selector, now and in the future"

The .live() function is essentially the same as .bind() but it can capture events on new elements that didn't exist on the page when it was loaded. For example, if your web page has already loaded and you dynamically insert an image into the page. If we used bind to attach an event when the mouse hovers over the image it would not work. But if we use .live() it will work!

Noticably the .live() doesn’t have a selector parameter.

$(selector).live(events, data, handler);                // jQuery 1.3+
$(document).delegate(selector, events, data, handler);  // jQuery 1.4.3+
$(document).on(events, selector, data, handler);        // jQuery 1.7+

*/