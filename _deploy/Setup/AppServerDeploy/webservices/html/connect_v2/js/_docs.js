// Listen for any attempts to call changePage().
$(document).on( "pagebeforechange", function( e, data ) 
{
	// only allows strings to pass 
	if (typeof data.toPage !== 'string' ) {
        return
    }
	else
	{	
		if (data.toPage=="menu.html"){
			// build_Menu();	
		}
		if (data.toPage=="contacts.html") {
			// build_Contacts();
		}
		if (data.toPage=="contactfavorites.html") {
			//	build_contactFavorites();
		}
		if (data.toPage=="contactfind.html") {
			// build_FindContact
		}
		if (data.toPage=="whitepages.html") {
			// Moved to "pageshow"
			// bug_1:  transitioning stops for o moment (when navigation from contacts -> whitepages)
			// bug_2:  octobutton doesn't work (page reload not working). 
			
			init_OctoButton();
			// build_Whitepages();	
		}	
		if (data.toPage=="yellowpages.html") {
			init_OctoButton();
			// build_Yellowpages();	
		}
		if (data.toPage=="contact.html") {	
			//build_Contact();			
		}
		if (data.toPage=="desktop.html") {	
			// build_Desktop();			
		}
		if (data.toPage=="views.html"){
			// build_Views();	
		}
		if (data.toPage=="documents.html") {	
			// build_Documents();		
		}
		if (data.toPage=="notes.html") {	
			// build_Notes();		
			// build_NoteDisplay();
		}
		if (data.toPage=="emails.html") {	
			// build_Emails();	
			// build_EmailDisplay();	
		}
		if (data.toPage=="websites.html") {	
			// build_Websites();		
		}
		if (data.toPage=="projects.html") {	
			// build_Projects();		
		}
		if (data.toPage=="calendar.html") {	
			// build_Calendar();
			// build_CalendarAS()		
		}	
	}
}); 

/*
EVENTS
------------------------------------------------------------------------------------
http://stackoverflow.com/questions/14468659/jquery-mobile-document-ready-vs-page-events/14469041#14469041
http://stackoverflow.com/questions/16751875/jquery-mobile-pageinit-vs-pageshow
http://api.jquerymobile.com/category/events/

Page events transition order:
All events can be found here: http://api.jquerymobile.com/category/events/
Lets say we have a page A and a page B, this is a unload/load order:

page B - event pagebeforecreate
page B - event pagecreate
page B - event pageinit
page A - event pagebeforehide
page B - event pagebeforeshow
page A - event pageremove
page A - event pagehide
page B - event pagebeforeshow
page B - event pageshow


0. mobileinit()
There's also another special jQuery Mobile event and it is called mobileinit.When jQuery Mobile starts, it triggers a mobileinit event on the document object. To override default settings, bind them to mobileinit. One of a good examples of mobileinit usage is turning off ajax page loading, or changing default ajax 
loader behavior.

$(document).on("mobileinit", function(){
  //apply overrides here
});

1. pageinit()

Pageinit event will be executed every time page is about be be loaded and shown for the first time. 
It will not trigger again unless page is manually refreshed or ajax page loading is turned off. 

2. pagebeforeshow()
In case you want code to execute every time you visit a page it is better to use pagebeforeshow event.

3. pageshow()


*/

/*---------------------------------------------
	for each page fire events..
----------------------------------------------*/
/*$(document).on("pagebeforecreate",function(event){
  //alert("pagebeforecreate event fired!");
}); 
$(document).on("pagecreate",function(event){
  //alert("pagecreate event fired!");
});
$(document).on("pageinit",function(event){
  //alert("pageinit event fired!")
});
$(document).on('pagebeforeshow', function(){
  //alert("pagebeforeshow event fired!")
});
$(document).on('pageshow', function(){
 //alert("pageshow event fired!")	 
});*/