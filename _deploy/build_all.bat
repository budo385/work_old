@ECHO OFF

CALL "c:\Program Files (x86)\Microsoft Visual Studio 12.0\Common7\Tools\vsvars32.bat"
IF %ERRORLEVEL% NEQ 0 GOTO ERROR_HANDLER


REM determine current folder!!!!
setlocal enabledelayedexpansion
set this_filename=%CD%\build_all.bat
for %%a in ("%this_filename%") do (
  set currentfolder=%%~dpa
  for %%b in ("!currentfolder:~0,-1!") do (
	 set parentfolder=%%~dpb
  )
)
   
set BUILD_SOURCE=%parentfolder%
set DEPLOY_TARGET=%CD%\Setup\

REM issue 2676: refresh build date by reset common and gui...hope for the best
REM devenv "%BUILD_SOURCE%\lib\common\common.sln" /clean release
REM devenv "%BUILD_SOURCE%\lib\gui_core\gui_core.sln" /clean release


REM clean all
REM devenv "%BUILD_SOURCE%\exe\sokrates_spc\sokrates_spc.sln" /clean release
REM devenv "%BUILD_SOURCE%\exe\appserver\appserver.sln" /clean release
REM devenv "%BUILD_SOURCE%\tool\ApplicationServerConfig\ApplicationServerConfig.sln" /clean release
REM devenv "%BUILD_SOURCE%\tool\AdminTool\AdminTool.sln" /clean release
REM devenv "%BUILD_SOURCE%\tool\Sokrates_Register\Sokrates_Register.sln" /clean release
REM devenv "%BUILD_SOURCE%\lib\phonet\phonet.sln" /clean release
REM devenv "%BUILD_SOURCE%\lib\mapi_ex\MAPIEx.sln" /clean release
REM devenv "%BUILD_SOURCE%\tool\KeyGenerator\KeyGenerator.sln" /clean release


REM Refresh build date: rebuild common
REM devenv "%BUILD_SOURCE%\lib\common\common.sln" /clean release
REM devenv "%BUILD_SOURCE%\lib\gui_core\gui_core.sln" /clean release

REM build all
devenv "%BUILD_SOURCE%\exe\sokrates_spc\sokrates_spc.sln" /build release
IF %ERRORLEVEL% NEQ 0 GOTO ERROR_HANDLER
devenv "%BUILD_SOURCE%\exe\appserver\appserver.sln" /build release
IF %ERRORLEVEL% NEQ 0 GOTO ERROR_HANDLER
devenv "%BUILD_SOURCE%\tool\ApplicationServerConfig\ApplicationServerConfig.sln" /build release
IF %ERRORLEVEL% NEQ 0 GOTO ERROR_HANDLER
devenv "%BUILD_SOURCE%\tool\AdminTool\AdminTool.sln" /build release
IF %ERRORLEVEL% NEQ 0 GOTO ERROR_HANDLER
devenv "%BUILD_SOURCE%\tool\GenTool\GenTool.sln" /build release
IF %ERRORLEVEL% NEQ 0 GOTO ERROR_HANDLER

REM devenv "%BUILD_SOURCE%\exe\appserver_james\appserver_james.sln" /build release
REM IF %ERRORLEVEL% NEQ 0 GOTO ERROR_HANDLER
REM devenv "%BUILD_SOURCE%\exe\appserver_james\appserver_james.sln" /build release_no_admin
REM IF %ERRORLEVEL% NEQ 0 GOTO ERROR_HANDLER

REM devenv "%BUILD_SOURCE%\libq\trans\trans\trans.sln" /clean release

REM devenv "%BUILD_SOURCE%\exe\appserver_john\appserver_john.sln" /build release
REM IF %ERRORLEVEL% NEQ 0 GOTO ERROR_HANDLER
REM devenv "%BUILD_SOURCE%\exe\appserver_john\appserver_john.sln" /build release_no_admin
REM IF %ERRORLEVEL% NEQ 0 GOTO ERROR_HANDLER
devenv "%BUILD_SOURCE%\tool\Sokrates_Register\Sokrates_Register.sln" /build release
IF %ERRORLEVEL% NEQ 0 GOTO ERROR_HANDLER
devenv "%BUILD_SOURCE%\lib\phonet\phonet.sln" /build release
IF %ERRORLEVEL% NEQ 0 GOTO ERROR_HANDLER
devenv "%BUILD_SOURCE%\lib\mapi_ex\MAPIEx.sln" /build release
IF %ERRORLEVEL% NEQ 0 GOTO ERROR_HANDLER
devenv "%BUILD_SOURCE%\tool\ApplicationServerUpdate\ApplicationServerUpdate.sln" /build release
IF %ERRORLEVEL% NEQ 0 GOTO ERROR_HANDLER
devenv "%BUILD_SOURCE%\tool\KeyGenerator\KeyGenerator.sln" /build release
IF %ERRORLEVEL% NEQ 0 GOTO ERROR_HANDLER

devenv "%BUILD_SOURCE%\tool\Sokrates_Progress\Sokrates_Progress.sln" /build release
IF %ERRORLEVEL% NEQ 0 GOTO ERROR_HANDLER

CALL "%BUILD_SOURCE%\lib\gui_core\gui_core\Resources\Themes\Build_themes.bat"
IF %ERRORLEVEL% NEQ 0 GOTO ERROR_HANDLER
REM devenv "%BUILD_SOURCE%\tool\mailegant\mailegant.sln" /build release
REM IF %ERRORLEVEL% NEQ 0 GOTO ERROR_HANDLER


REM devenv "%BUILD_SOURCE%\exe\appserver_mwreplyserver\appserver_mwreplyserver.sln" /build release
REM IF %ERRORLEVEL% NEQ 0 GOTO ERROR_HANDLER
REM devenv "%BUILD_SOURCE%\tool\ServerPerformanceTesterReal\ServerPerformanceTester.sln" /build release
REM IF %ERRORLEVEL% NEQ 0 GOTO ERROR_HANDLER


REM CALL copy_files.bat D:\Deploy
REM IF %ERRORLEVEL% NEQ 0 GOTO ERROR_HANDLER

CALL copy_files.bat %DEPLOY_TARGET%
IF %ERRORLEVEL% NEQ 0 GOTO ERROR_HANDLER





echo.
echo Sucess!!!
GOTO QUIT

:ERROR_HANDLER
echo.
echo Error occured!!!
set %ERRORLEVEL% = 1
exit 1

:QUIT
echo.



