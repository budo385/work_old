@ECHO OFF

REM set BUILD_SOURCE=C:\SoftProjects\work

REM xcopy all
xcopy /Y "%BUILD_SOURCE%\exe\appserver\release\appserver.exe" %1\AppServerDeploy\
IF %ERRORLEVEL% NEQ 0 GOTO ERROR_HANDLER
xcopy /Y "%BUILD_SOURCE%\exe\appserver\release\appserver.pdb" %1\AppServerDeploy\
IF %ERRORLEVEL% NEQ 0 GOTO ERROR_HANDLER
xcopy /Y "%BUILD_SOURCE%\exe\sokrates_spc\release\sokrates.exe" %1\SPCDeploy\
IF %ERRORLEVEL% NEQ 0 GOTO ERROR_HANDLER
xcopy /Y "%BUILD_SOURCE%\exe\sokrates_spc\release\sokrates.pdb" %1\SPCDeploy\
IF %ERRORLEVEL% NEQ 0 GOTO ERROR_HANDLER
xcopy /Y "%BUILD_SOURCE%\tool\AdminTool\release\db_actualize.dll" %1\SPCDeploy\
IF %ERRORLEVEL% NEQ 0 GOTO ERROR_HANDLER
xcopy /Y "%BUILD_SOURCE%\tool\AdminTool\release\db_actualize.dll" %1\AppServerDeploy\
IF %ERRORLEVEL% NEQ 0 GOTO ERROR_HANDLER
xcopy /Y "%BUILD_SOURCE%\tool\AdminTool\release\AdminTool.exe" %1\AppServerDeploy\
IF %ERRORLEVEL% NEQ 0 GOTO ERROR_HANDLER
xcopy /Y "%BUILD_SOURCE%\tool\GenTool\release\GenTool.exe" %1\AppServerDeploy\
IF %ERRORLEVEL% NEQ 0 GOTO ERROR_HANDLER

REM xcopy /Y "%BUILD_SOURCE%\exe\appserver_james\release\*.exe" %1\FFHServer\
REM IF %ERRORLEVEL% NEQ 0 GOTO ERROR_HANDLER
REM xcopy /Y "%BUILD_SOURCE%\exe\appserver_james\release_no_admin\everPICs-Bridge_no_admin.exe" %1\FFHServer\
REM IF %ERRORLEVEL% NEQ 0 GOTO ERROR_HANDLER
REM xcopy /Y "%BUILD_SOURCE%\exe\appserver_james\release\*.dll" %1\FFHServer\
REM IF %ERRORLEVEL% NEQ 0 GOTO ERROR_HANDLER
REM xcopy /Y "%BUILD_SOURCE%\_data\Translation\EXB_Manager_de.qm" %1\FFHServer\
REM IF %ERRORLEVEL% NEQ 0 GOTO ERROR_HANDLER

REM xcopy /Y "%BUILD_SOURCE%\exe\appserver_john\release\*.exe" %1\JohnServer\
REM IF %ERRORLEVEL% NEQ 0 GOTO ERROR_HANDLER
REM xcopy /Y "%BUILD_SOURCE%\exe\appserver_john\release\*.dll" %1\JohnServer\
REM IF %ERRORLEVEL% NEQ 0 GOTO ERROR_HANDLER
REM  xcopy /Y "%BUILD_SOURCE%\exe\appserver_john\release_no_admin\everDOCs-Bridge_no_admin.exe" %1\JohnServer\
REM IF %ERRORLEVEL% NEQ 0 GOTO ERROR_HANDLER

REM xcopy /Y "%BUILD_SOURCE%\tool\mailegant\release\mailegant.exe" %1\Mailegant2\
REM IF %ERRORLEVEL% NEQ 0 GOTO ERROR_HANDLER
REM xcopy /Y "%BUILD_SOURCE%\exe\appserver_mwreplyserver\release\mailegant_connector.exe" %1\MW_CloudServer_Setup\
REM IF %ERRORLEVEL% NEQ 0 GOTO ERROR_HANDLER
REM RMDIR /Q /S  D:\Deploy\Setup\MW_CloudServer_Setup\webservices\
REM XCOPY /S /E /Y "%BUILD_SOURCE%\exe\appserver_mwreplyserver\appserver_mwreplyserver\webservices" D:\Deploy\Setup\MW_CloudServer_Setup\webservices\

REM xcopy /Y "%BUILD_SOURCE%\exe\appserver_mwreplyserver\release\mailegant_loadbalance.exe" %1\MW_LoadBalanceServer_Setup\
REM IF %ERRORLEVEL% NEQ 0 GOTO ERROR_HANDLER
REM RMDIR /Q /S  D:\Deploy\Setup\MW_LoadBalanceServer_Setup\webservices\
REM XCOPY /S /E /Y "%BUILD_SOURCE%\exe\appserver_mwreplyserver\appserver_mwloadbalance\webservices" D:\Deploy\Setup\MW_LoadBalanceServer_Setup\webservices\

REM xcopy /Y "%BUILD_SOURCE%\tool\ServerPerformanceTesterReal\Win32\release\ServerPerformanceTester.exe" %1\MW_ServerPerfomanceTester_Setup\
REM IF %ERRORLEVEL% NEQ 0 GOTO ERROR_HANDLER


xcopy /Y "%BUILD_SOURCE%\tool\ApplicationServerConfig\release\ApplicationServerConfig.exe" %1\AppServerDeploy\
IF %ERRORLEVEL% NEQ 0 GOTO ERROR_HANDLER
xcopy /Y "%BUILD_SOURCE%\tool\ApplicationServerUpdate\release\ApplicationServerUpdate.exe" %1\AppServerDeploy\
IF %ERRORLEVEL% NEQ 0 GOTO ERROR_HANDLER
REM xcopy /Y "%BUILD_SOURCE%\tool\Sokrates_Progress\release\Sokrates_Progress.exe" %1\SPCDeploy\
REM IF %ERRORLEVEL% NEQ 0 GOTO ERROR_HANDLER
xcopy /Y "%BUILD_SOURCE%\tool\Sokrates_Register\release\Sokrates_Register.exe" %1\SokratesRegisterDeploy\
IF %ERRORLEVEL% NEQ 0 GOTO ERROR_HANDLER
xcopy /Y "%BUILD_SOURCE%\lib\phonet\release\phonet.dll" %1\SPCDeploy\
IF %ERRORLEVEL% NEQ 0 GOTO ERROR_HANDLER
xcopy /Y "%BUILD_SOURCE%\lib\phonet\release\phonet.dll" %1\AppServerDeploy\
IF %ERRORLEVEL% NEQ 0 GOTO ERROR_HANDLER
xcopy /Y "%BUILD_SOURCE%\lib\mapi_ex\release\MAPIEx.dll" %1\SPCDeploy\
IF %ERRORLEVEL% NEQ 0 GOTO ERROR_HANDLER
xcopy /Y "%BUILD_SOURCE%\lib\report_definitions\report_definitions\*.xml" %1\SPCDeploy\report_definitions\
IF %ERRORLEVEL% NEQ 0 GOTO ERROR_HANDLER
xcopy /Y "%BUILD_SOURCE%\_data\Translation\sokrates_de.qm" %1\SPCDeploy\
IF %ERRORLEVEL% NEQ 0 GOTO ERROR_HANDLER

xcopy /Y "%BUILD_SOURCE%\lib\os_specific\os_specific\mimepp\bin\mimepp.dll" %1\SPCDeploy\
IF %ERRORLEVEL% NEQ 0 GOTO ERROR_HANDLER
xcopy /Y "%BUILD_SOURCE%\lib\os_specific\os_specific\mailpp\bin\mailpp.dll" %1\SPCDeploy\
IF %ERRORLEVEL% NEQ 0 GOTO ERROR_HANDLER
xcopy /Y "%BUILD_SOURCE%\tool\KeyGenerator\release\KeyGenerator.exe" %1\KeyFiles\
IF %ERRORLEVEL% NEQ 0 GOTO ERROR_HANDLER
xcopy /Y "%BUILD_SOURCE%\tool\KeyGenerator\release\FpAdmin.exe" %1\KeyFiles\
IF %ERRORLEVEL% NEQ 0 GOTO ERROR_HANDLER
xcopy /Y "%BUILD_SOURCE%\lib\gui_core\gui_core\Resources\Themes\*.rcc" %1\SPCDeploy\themes\
IF %ERRORLEVEL% NEQ 0 GOTO ERROR_HANDLER

REM HTML/REST
CALL copy_web.bat
REM RMDIR /Q /S  D:\Deploy\Setup\AppServerDeploy\webservices\
REM MKDIR   D:\Deploy\AppServerDeploy\webservices
REM XCOPY /S /E /Y "%BUILD_SOURCE%\webservices" D:\Deploy\AppServerDeploy\webservices\

REM FFH server html:
REM RMDIR /Q /S  D:\Deploy\Setup\FFHServer\webservices\html\iphone\
REM RMDIR /Q /S  D:\Deploy\Setup\FFHServer\webservices\html\web\
REM RMDIR /Q /S  D:\Deploy\Setup\FFHServer\webservices\html\iphone_g\
REM RMDIR /Q /S  D:\Deploy\Setup\FFHServer\webservices\html\assets\
 
REM XCOPY /S /E /Y "%BUILD_SOURCE%\exe\appserver_james\appserver_james\webservices\html\iphone" D:\Deploy\Setup\FFHServer\webservices\html\iphone\
REM XCOPY /S /E /Y "%BUILD_SOURCE%\exe\appserver_james\appserver_james\webservices\html\web" D:\Deploy\Setup\FFHServer\webservices\html\web\
REM XCOPY /S /E /Y "%BUILD_SOURCE%\exe\appserver_james\appserver_james\webservices\html\iphone_g" D:\Deploy\Setup\FFHServer\webservices\html\iphone_g\
REM XCOPY /S /E /Y "%BUILD_SOURCE%\exe\appserver_james\appserver_james\webservices\html\assets" D:\Deploy\Setup\FFHServer\webservices\html\assets\


REM FFH server html:
REM RMDIR /Q /S  D:\Deploy\Setup\JohnServer\webservices\html\iphone\
REM RMDIR /Q /S  D:\Deploy\Setup\JohnServer\webservices\html\web\
REM RMDIR /Q /S  D:\Deploy\Setup\JohnServer\webservices\html\iphone_g\
REM RMDIR /Q /S  D:\Deploy\Setup\JohnServer\webservices\html\assets\
 
REM XCOPY /S /E /Y "%BUILD_SOURCE%\exe\appserver_john\appserver_john\webservices\html\iphone" D:\Deploy\Setup\JohnServer\webservices\html\iphone\
REM XCOPY /S /E /Y "%BUILD_SOURCE%\exe\appserver_john\appserver_john\webservices\html\web" D:\Deploy\Setup\JohnServer\webservices\html\web\
REM XCOPY /S /E /Y "%BUILD_SOURCE%\exe\appserver_john\appserver_john\webservices\html\iphone_g" D:\Deploy\Setup\JohnServer\webservices\html\iphone_g\
REM XCOPY /S /E /Y "%BUILD_SOURCE%\exe\appserver_john\appserver_john\webservices\html\assets" D:\Deploy\Setup\JohnServer\webservices\html\assets\



echo.
echo Sucess!!!
GOTO QUIT

:ERROR_HANDLER
echo.
echo Error occured!!!
exit 1

:QUIT
echo.



