#ifndef TESTER_H
#define TESTER_H

#include <QObject>

class Tester : public QObject
{
	Q_OBJECT

public:
	Tester(QObject *parent);
	~Tester();

	
public slots:
	void Start();
	void End();
	void StartNewTester();

private:
	int GetSessions();
	void StartTesting();

	//test:
	bool test1(int &time);
	bool test2(int &time);
	bool test3(int &time);


	int m_nMaxInst;
	int m_nMyNumber;
	bool m_bTestStop;
	QString m_strDefaultConnectionName;
	int m_nPeriod;
	
};

#endif // TESTER_H
