#include "tester.h"
#include "bus_client/bus_client/client_global_objects_create.h
#include "common/common/status.h"
#include "trans/trans/networkconnections.h"
#include "common/common/config.h"
#include "common/common/sleeper.h"
#include "common/common/entity_id_collection.h"

Tester::Tester(QObject *parent)
:QObject(parent),m_bTestStop(false)
{
}

Tester::~Tester()
{

}

//param0=app name
//param1=network connection name inside network.cfg (def=localhost)
//param2=max allowed instances
//param3=client instance count
//param4=time in minutes


void Tester::Start()
{
	

	QStringList lstArgs=QCoreApplication::arguments();

	m_strDefaultConnectionName="localhost";
	if (lstArgs.size()>1)
		m_strDefaultConnectionName=lstArgs.at(1);

	if (lstArgs.size()>2)
		m_nMaxInst=lstArgs.at(2).toInt();
	else
		m_nMaxInst=30;

	if (lstArgs.size()>3)
		m_nMyNumber=lstArgs.at(3).toInt();
	else
		m_nMyNumber=0;

	if (lstArgs.size()>4)
		m_nPeriod=lstArgs.at(4).toInt();
	else
		m_nPeriod=-1;


	//create log file, enable error logging:
	QString strLogFile=lstArgs.at(0)+"_"+QVariant(m_nMyNumber).toString()+".log";
	g_Logger.EnableFileLogging(strLogFile);
	g_Logger.EnableConsoleLogging();
	g_Logger.SetLogLevel(Logger::LOG_LEVEL_ALL);
	Status::SetErrorLogger(&g_Logger);


	//read defaults (from INI)
	QString strDefaultUserName="Administrator"; 
	NetworkConnections m_objData;
	m_objData.Load(QCoreApplication::applicationDirPath()+"/network_connections.cfg", MASTER_PASSWORD);
	int nSize=m_objData.m_lstConnections.size();
	int nConnectionIdx=-1;
	for(int i=0;i<nSize;++i)
	{
		if (m_objData.m_lstConnections[i].m_strCustomName==m_strDefaultConnectionName)
		{
			nConnectionIdx=i;
			break;
		}
	}

	if (nConnectionIdx==-1)
	{
		g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,1,"Connection: "+m_strDefaultConnectionName+" not found in settings");
		QCoreApplication::quit();
		return;
	}

	//start BO service
	//if service already started, it will be stopped!
	Status err;
	g_BoSet->StartService(err,m_objData.m_lstConnections[nConnectionIdx]);
	if(!err.IsOK())	
	{	
		QCoreApplication::quit();
		return;
	}

	//login:
	g_BoSet->Login(err,"Administrator","xxxx");
	if(!err.IsOK())
	{
		QCoreApplication::quit();
		return;
	}


	//testing time: 15h
	//QTimer::singleShot(15*60*60*1000, this, SLOT(End()));	

	//start new instance in 30sec
	if (m_nMyNumber<m_nMaxInst)
	{
		if (m_nPeriod<=0)
			QTimer::singleShot(20*1000, this, SLOT(StartNewTester()));	//20ms
		else
			QTimer::singleShot(m_nPeriod*60*1000, this, SLOT(StartNewTester()));	
	}


	StartTesting(); //start testing in endless loop
	//if exists with error, quit:
	//terminate app:
	QCoreApplication::quit();
}

void Tester::End()
{
	m_bTestStop=true;
	//logout, stop service
	Status err;
	g_BoSet->Logout(err);
	{	
		return;
	}

	g_BoSet->StopService(err);
	
	//terminate app:
	QCoreApplication::quit();
}

//after some period start, new client:
void Tester::StartNewTester()
{
	QString strMyInstance=QCoreApplication::applicationFilePath();
	#ifdef _WIN32 //put in quotes for windows
		strMyInstance.prepend("\"");
		strMyInstance.append("\"");
	#endif

	QStringList lstArgs;
	lstArgs<<m_strDefaultConnectionName;
	lstArgs<<QVariant(m_nMaxInst).toString();
	lstArgs<<QVariant(m_nMyNumber+1).toString();

	QString strMsg="Starting new tester no: "+QVariant(m_nMyNumber+1).toString();
	g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0,strMsg);
	QProcess::startDetached(strMyInstance,lstArgs);
}


int Tester::GetSessions()
{
	Status err;
	int nSessions;
	g_BoSet->app->CoreServices->GetUserSessions(err,nSessions);
	if (!err.IsOK())
		return -1;
	else
		return nSessions;

}

void Tester::StartTesting()
{
	int nTime=0;
	int nSessionCount=0;


	while (!m_bTestStop)
	{

		//------------------------------TEST1----------------------------
		if(!test1(nTime)) return;
		nSessionCount=GetSessions();
		if (nSessionCount==-1)
			return;
		QString strMsg="test1_"+QVariant(m_nMyNumber).toString()+"_"+QVariant(nSessionCount).toString()+": "+QVariant(nTime).toString()+"ms";
		g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0,strMsg);
		ThreadSleeper::Sleep(500); 	//wait for some time to avoid 100% cpu
		QCoreApplication::processEvents();	//process all que events


		//------------------------------TEST2----------------------------
		if(!test2(nTime)) return;
		nSessionCount=GetSessions();
		if (nSessionCount==-1)
			return;
		strMsg="test2_"+QVariant(m_nMyNumber).toString()+"_"+QVariant(nSessionCount).toString()+": "+QVariant(nTime).toString()+"ms";
		g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0,strMsg);
		ThreadSleeper::Sleep(500); 	//wait for some time to avoid 100% cpu
		QCoreApplication::processEvents();	//process all que events
	}

}




//login proc:
bool Tester::test1(int &time)
{
	time=0;
	QTime timer_name;				
	timer_name.start();


	Status pStatus;

	//version check
	QString strAppServerVersion;
	g_BoSet->app->CoreServices->CheckVersion(pStatus,QString(APPLICATION_VERSION),strAppServerVersion);
	if(!pStatus.IsOK()) return false;

	//load after login data:
	g_LoggedUserData.ReadUserData(pStatus);
	if(!pStatus.IsOK()) return false;

	//load batch for contacts:
	DbRecordSet lstEntityID,lstFilters,lstResults;
	QList<int> lstIDs;
	lstIDs<<ENTITY_BUS_PERSON<<ENTITY_BUS_CM_ADDRESS_SCHEMAS<<ENTITY_CONTACT_ORGANIZATION<<ENTITY_CONTACT_DEPARTMENT<<ENTITY_CONTACT_FUNCTION<<ENTITY_CONTACT_PROFESSION<<ENTITY_BUS_CM_TYPES<<ENTITY_BUS_OPT_GRID_VIEWS;
	lstEntityID.addColumn(QVariant::Int,"ID");
	//find not loaded 
	int nSize=lstIDs.size();
	for(int i=0;i<nSize;++i)
	{
		lstEntityID.addRow();
		lstEntityID.setData(lstEntityID.getRowCount()-1,0,lstIDs.at(i));
	}
	g_BoSet->app->MainEntitySelector->ReadDataBatch(pStatus,lstEntityID,lstFilters,lstResults);
	if(!pStatus.IsOK()) return false;


	//CE grid view;
	DbRecordSet m_recFilterViews;
	g_BoSet->app->BusCommunication->GetCommFilterViews(pStatus, m_recFilterViews, g_LoggedUserData.GetPersonID(), 1 /*m_nGridType*/);
	if(!pStatus.IsOK()) return false;


	time=timer_name.elapsed();
	return true;

}

//load whole contact list, pick 2 contact, lock & edit them
bool Tester::test2(int &time)
{
	time=0;
	QTime timer_name;				
	timer_name.start();

	Status pStatus;
	DbRecordSet lstNewData,lstFilterData;
	g_BoSet->app->MainEntitySelector->ReadData(pStatus,ENTITY_BUS_CONTACT,lstFilterData,lstNewData);
	if(!pStatus.IsOK()) return false;


	int nSize=lstNewData.getRowCount();
	if (nSize>0)
	{
		int nRow=(nSize-m_nMyNumber)-QDateTime::currentDateTime().toString("zzz").toInt();
		if (nRow<=0) nRow=0;
		if (nRow>nSize-1)nRow=nSize-1;

		DbRecordSet row;
		int nCntID=lstNewData.getDataRef(nRow,"BCNT_ID").toInt();
		QString strWhere=" WHERE BCNT_ID="+QVariant(nCntID).toString();
		//read
		g_BoSet->app->ClientSimpleORM->Read(pStatus,BUS_CM_CONTACT,row,strWhere);
		if(!pStatus.IsOK()) return false;
		//_DUMP(row);
		if (row.getRowCount()>0)
		{
			//write & lock:
			g_BoSet->app->ClientSimpleORM->Write(pStatus,BUS_CM_CONTACT,row);
			if(!pStatus.IsOK()) 
			{
				if (pStatus.getErrorCode()!=StatusCodeSet::ERR_SQL_ALREADY_LOCKED)
					return false;
				else
					g_Logger.logMessage(StatusCodeSet::TYPE_INFORMATION,0,pStatus.getErrorText());
			}
				
		}
	}

	time=timer_name.elapsed();
	return true;
}


bool Tester::test3(int &time)
{

	return true;

}
