#include <QtCore/QCoreApplication>
#include "bus_client/bus_client/applicationstarter.h"
#include "tester.h"
#include <QTimer>


#ifndef QT_NO_DEBUG
	int g_BufferSize;
#endif

int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);
	a.setApplicationName("SOKRATES Tester");		

	
	ApplicationStarter *App=new ApplicationStarter(&a,true);
	Tester tester(&a);
	QTimer::singleShot(0, &tester, SLOT(Start()));	//wait to start until QCoreApp enters event loop
	return a.exec();
}
