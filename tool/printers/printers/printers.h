// printers.cpp : Defines the exported functions for the DLL application.
//

//this DLL is supposed to be used only dynamically loaded
#ifdef _WIN32
  #define EXPORT_API extern "C" __declspec(dllexport)
#endif

EXPORT_API void ListPrinters(char *szNames, int nNamesBufSize, char *szDefault, int nDefaultBufSize);

