// printers.cpp : Defines the exported functions for the DLL application.
//

#include "printers.h"
#include <windows.h>
#include <winspool.h>

//
bool LPW2ANSI(const LPWSTR pw, char *szBuffer, int nSize, UINT codepage = CP_ACP)
{
	int rc = WideCharToMultiByte(codepage,
			0,
			pw,-1,
			szBuffer, nSize,
			0,0);
	if (rc != 0) {
		szBuffer[nSize-1] = 0;
		return true;
	}
	return false;
}

// You are explicitly linking to GetDefaultPrinter because linking 
// implicitly on Windows 95/98 or NT4 results in a runtime error.
// This block specifies which text version you explicitly link to.
#ifdef UNICODE
  #define GETDEFAULTPRINTER "GetDefaultPrinterW"
#else
  #define GETDEFAULTPRINTER "GetDefaultPrinterA"
#endif
typedef BOOL (*FN_GETDEFAULTPRINTER)(LPTSTR pszBuffer, LPDWORD pcchBuffer);

// Size of internal buffer used to hold "printername,drivername,portname"
// string. You may have to increase this for huge strings.
#define MAXBUFFERSIZE 250

/*----------------------------------------------------------------*/ 
/* DPGetDefaultPrinter                                            */ 
/*                                                                */ 
/* Parameters:                                                    */ 
/*   pPrinterName: Buffer alloc'd by caller to hold printer name. */ 
/*   pdwBufferSize: On input, ptr to size of pPrinterName.        */ 
/*          On output, min required size of pPrinterName.         */ 
/*                                                                */ 
/* NOTE: You must include enough space for the NULL terminator!   */ 
/*                                                                */ 
/* Returns: TRUE for success, FALSE for failure.                  */ 
/*----------------------------------------------------------------*/ 
/*
BOOL DPGetDefaultPrinter(LPTSTR pPrinterName, LPDWORD pdwBufferSize)
{
  BOOL bFlag;
  OSVERSIONINFO osv;
  TCHAR cBuffer[MAXBUFFERSIZE];
  PRINTER_INFO_2 *ppi2 = NULL;
  DWORD dwNeeded = 0;
  DWORD dwReturned = 0;
  HMODULE hWinSpool = NULL;
  FN_GETDEFAULTPRINTER fnGetDefaultPrinter = NULL;
  
  // What version of Windows are you running?
  osv.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
  GetVersionEx(&osv);
  
  // If Windows 95 or 98, use EnumPrinters.
  if (osv.dwPlatformId == VER_PLATFORM_WIN32_WINDOWS)
  {
    // The first EnumPrinters() tells you how big our buffer must
    // be to hold ALL of PRINTER_INFO_2. Note that this will
    // typically return FALSE. This only means that the buffer (the 4th
    // parameter) was not filled in. You do not want it filled in here.
    SetLastError(0);
    bFlag = EnumPrinters(PRINTER_ENUM_DEFAULT, NULL, 2, NULL, 0, &dwNeeded, &dwReturned);
    {
      if ((GetLastError() != ERROR_INSUFFICIENT_BUFFER) || (dwNeeded == 0))
        return FALSE;
    }
    
    // Allocate enough space for PRINTER_INFO_2.
    ppi2 = (PRINTER_INFO_2 *)GlobalAlloc(GPTR, dwNeeded);
    if (!ppi2)
      return FALSE;
    
    // The second EnumPrinters() will fill in all the current information.
    bFlag = EnumPrinters(PRINTER_ENUM_DEFAULT, NULL, 2, (LPBYTE)ppi2, dwNeeded, &dwNeeded, &dwReturned);
    if (!bFlag)
    {
      GlobalFree(ppi2);
      return FALSE;
    }
    
    // If specified buffer is too small, set required size and fail.
    if ((DWORD)lstrlen(ppi2->pPrinterName) >= *pdwBufferSize)
    {
      *pdwBufferSize = (DWORD)lstrlen(ppi2->pPrinterName) + 1;
      GlobalFree(ppi2);
      return FALSE;
    }
    
    // Copy printer name into passed-in buffer.
    lstrcpy(pPrinterName, ppi2->pPrinterName);
    
    // Set buffer size parameter to minimum required buffer size.
    *pdwBufferSize = (DWORD)lstrlen(ppi2->pPrinterName) + 1;
  }
  
  // If Windows NT, use the GetDefaultPrinter API for Windows 2000,
  // or GetProfileString for version 4.0 and earlier.
  else if (osv.dwPlatformId == VER_PLATFORM_WIN32_NT)
  {
    if (osv.dwMajorVersion >= 5) // Windows 2000 or later (use explicit call)
    {
      hWinSpool = LoadLibrary(L"winspool.drv");
      if (!hWinSpool)
        return FALSE;
      fnGetDefaultPrinter = (FN_GETDEFAULTPRINTER)GetProcAddress(hWinSpool, GETDEFAULTPRINTER);
      if (!fnGetDefaultPrinter)
      {
        FreeLibrary(hWinSpool);
        return FALSE;
      }

      bFlag = fnGetDefaultPrinter(pPrinterName, pdwBufferSize);
        FreeLibrary(hWinSpool);
      if (!bFlag)
        return FALSE;
    }
    
    else // NT4.0 or earlier
    {
      // Retrieve the default string from Win.ini (the registry).
      // String will be in form "printername,drivername,portname".
      if (GetProfileStringA("windows", "device", ",,,", (LPSTR)cBuffer, MAXBUFFERSIZE) <= 0)
        return FALSE;
      
      // Printer name precedes first "," character.
      strtok((char *)cBuffer, ",");
      
      // If specified buffer is too small, set required size and fail.
      if ((DWORD)lstrlen(cBuffer) >= *pdwBufferSize)
      {
        *pdwBufferSize = (DWORD)lstrlen(cBuffer) + 1;
        return FALSE;
      }
      
      // Copy printer name into passed-in buffer.
      lstrcpy(pPrinterName, cBuffer);
      
      // Set buffer size parameter to minimum required buffer size.
      *pdwBufferSize = (DWORD)lstrlen(cBuffer) + 1;
    }
  }
  
  // Clean up.
  if (ppi2)
    GlobalFree(ppi2);
  
  return TRUE;
}
*/
#undef MAXBUFFERSIZE
#undef GETDEFAULTPRINTER

void ListPrinterNames(DWORD flags, char *szBuffer, int nBufSize)
{
	if(!szBuffer || nBufSize <= 0)
		return;

	//init buffer
	szBuffer[0] = '\0';
	
	int nAppendedSize = 0;

    //find required size for the buffer
	DWORD level = 2;
	DWORD needed, returned;
	EnumPrinters(flags, NULL, level, NULL, 0, &needed, &returned);
 
    //allocate array of PRINTER_INFO structures
    PRINTER_INFO_2* prninfo = (PRINTER_INFO_2*) GlobalAlloc(GPTR, needed);
 
    //call again
    if (!EnumPrinters(flags, NULL, level, (LPBYTE)prninfo, needed, &needed, &returned))
    {
		//error
        //cout << "EnumPrinters failed with error code " << GetLastError() << endl;
    }
    else
    {
        //cout << "Printers found: " << returned << endl << endl;
        for (DWORD i=0; i<returned; i++)
        {
			//convert from unicode to ANSI
			char szName[1000];
			LPW2ANSI(prninfo[i].pPrinterName, szName, sizeof(szName));

			int nFreeSpace = max(0, nBufSize-2-nAppendedSize);
			int nSize = strlen(szName);
			if(nFreeSpace > nSize){
				strncat(szBuffer, szName, nFreeSpace);
				nAppendedSize += nSize;
				strncat(szBuffer, ";", 1);
				nAppendedSize += 1;
			}
            //cout << prninfo[i].pPrinterName << " on " << prninfo[i].pPortName << endl;
            //cout << "Status=" << prninfo[i].Status << ", jobs=" << prninfo[i].cJobs << endl << endl;
        }
    }
 
    GlobalFree(prninfo);
}

void ListPrinters(char *szNames, int nNamesBufSize, char *szDefault, int nDefaultBufSize)
{
	ListPrinterNames(PRINTER_ENUM_LOCAL | PRINTER_ENUM_CONNECTIONS, szNames, nNamesBufSize);
	//ListPrinterNames(PRINTER_ENUM_DEFAULT, szDefault, nDefaultBufSize);

	DWORD dwSize = nDefaultBufSize;
	//DPGetDefaultPrinter((LPTSTR)szDefault, &dwSize);
}

