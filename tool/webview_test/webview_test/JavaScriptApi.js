(function(){
 IOS = {};
 
 IOS.PATH = 'ios://localhost/IOS';
 
 IOS.get = function(id)
 {
 return document.getElementById(id);
 };
 
 IOS.extend = function(a, b)
 {
 if (!b) return a;
 for (var key in b)
 {
 a[key] = b[key];
 }
 return a;
 }
 
 IOS.isString = function(o)
 {
 return ({}).toString.call(o) == '[object String]';
 };
 
 IOS.isNumber = function(o)
 {
 return ({}).toString.call(o) == '[object Number]';
 };
 
 IOS.isFunction = function(o)
 {
 return ({}).toString.call(o) == '[object Function]';
 };
 
 IOS.isArray = function(o)
 {
 return ({}).toString.call(o) == '[object Array]';
 };
 
 var ID = 0;
 IOS.create_uniqid = function()
 {
 return ID++;
 };
 
 
 IOS.each = function(a, cb)
 {
 if (!a) return;
 for (var i = 0; i < a.length; i ++)
 {
 if (cb(i, a[i]) === false) break;
 }
 };
 
 IOS.__callback__ = {};
 
 function param2string(obj)
 {
 if (IOS.isString(obj) || IOS.isNumber(obj))
 return obj + '';
 
 if (IOS.isFunction(obj))
 {
 var func_name = new Date().getTime() + Math.floor(Math.random() * 1000) + '';
 IOS.__callback__[func_name] = function()
 {
 IOS.__callback__[func_name] = null;
 obj.apply(null, arguments);
 };
 return 'window.IOS.__callback__["' + func_name + '"]';
 }
 
 if (IOS.isArray(obj))
 {
 var rt = [];
 var len = obj.length;
 IOS.each(obj, function(i, v)
 {
 rt.push(encodeURIComponent(v + ''));
 });
 return rt.join(':');
 }
 
 var rt = [];
 for (var key in obj)
 {
 rt.push(encodeURIComponent(key) + ':' + encodeURIComponent(obj[key] + ''));
 }
 return rt.join(';');
 }
 
 IOS.call = function()
 {
 if (arguments.length < 2)
 throw new Error('need more arguments');
 arguments = Array.prototype.slice.call(arguments, 0);
 
 var ns = arguments.shift();
 var method = arguments.shift();
 var args = arguments;
 
 var url = IOS.PATH + '?';
 url += 'ns=' + encodeURIComponent(ns);
 url += '&method=' + encodeURIComponent(method);
 
 
 IOS.each(args, function(i, arg)
 {
 url += '&p=' + encodeURIComponent(param2string(arg));
 });
 window.location.href = url;
 }
 })();
 /* JS framework end */
 
 
 
 
 
 
 
 /* JS util start */
 (function(){
 IOS.util = {};
 
 IOS.util.log = function(obj)
 {
 IOS.call('util', 'log', obj + '');
 };
 
 IOS.util.alert = function(title, msg, OK, cb)
 {
 title = title || '提示';
 msg = msg || '';
 OK = OK || '确定';
 cb = cb || function(){};
 IOS.call('util', 'alert', title, msg, OK, cb);
 };
 
 IOS.util.confirm = function(title, msg, OK, NO, cb)
 {
 title = title || '提示';
 msg = msg || '';
 OK = OK || '取消';
 IOS.call('util', 'alert', title, msg, OK, function(index)
 {
 cb && cb(index === '0');
 }, NO);
 };
 
 IOS.util.http_request = function(params)
 {
 var default_cfg = {
 url :'',
 method : 'GET',
 data : {},
 headers : {},
 success : function(){},
 error : function(){}
 };
 
 params = IOS.extend(default_cfg, params);
 
 if (!IOS.isString(params.data))
 {
 var data = [];
 for (var key in params.data)
 {
 data.push(key + '=' + encodeURIComponent(params.data[key]));
 }
 params.data = data.join('&');
 }
 
 IOS.call('util', 'http_request_create'
 , params.url
 , params.method
 , params.data
 , params.headers
 , params.success
 , params.error);
 };
 
 IOS.util.http_request.test = function()
 {
    IOS.call('mailegantCall', 'callWrapper', 'param1', 'param2', 'param3');
 };
 
 })();
