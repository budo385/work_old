//
//  ViewController.h
//  webview_test
//
//  Created by Marko Perutovic on 9/29/12.
//  Copyright (c) 2012 Marko Perutovic. All rights reserved.    
//

#import <UIKit/UIKit.h>
#import <CoreGraphics/CoreGraphics.h>
#import "TBXML.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <AddressBookUI/AddressBookUI.h>
#import <iAd/iAd.h>
//#import "iAd/ADBannerView.h"
#import "introView.h"
#import <MailCore/MailCore.h>
#import "JavascriptApi.h"
#import <sqlite3.h>

@interface ViewController : UIViewController <UIWebViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPopoverControllerDelegate, MFMailComposeViewControllerDelegate, ABPeoplePickerNavigationControllerDelegate, UIDocumentInteractionControllerDelegate, ADBannerViewDelegate>
{
    TBXML *tbxml;
    UIPopoverController *popoverController;
    CGSize newImageSize;
    introView *intro;
    sqlite3 *_database;
}

@property (nonatomic, retain) JavascriptApi *JSAPI;
@property (nonatomic, retain) IBOutlet UIWebView *webview;
@property (nonatomic, retain) ADBannerView *adView;
@property (nonatomic, strong) UIDocumentInteractionController *interactionController;

-(void)outURLwebviewCallback:(NSString*)callback;
-(void)openURLinSafari:(NSString*)url;
-(void)openSqliteDb;
-(void)closeSqliteDb;
-(void)executeSql:(NSString*) sqlStatement;
-(NSString*) encodePicsData:(NSData*) rawBytes fileExtension:(NSString*) fileExtension;
-(void)sendMimeMail:(NSString*)from to:(NSString*)to subject:(NSString*)subject mimebody:(NSString*)mimebody mimefrom:(NSString*)mimefrom cc:(NSString*)cc bcc:(NSString*)bcc strServerAddress:(NSString*)strServerAddress nPort:(int)nPort nSmtpConnectionType:(int)nSmtpConnectionType bSmtpUseAuth:(BOOL)bSmtpUseAuth txt_strUserName:(NSString*)txt_strUserName txt_strPassword:(NSString*)txt_strPassword;
-(void)readXML;
-(void)loadWebView;
-(void)downloadUpdate:(NSString*)downloadURL appVersion:(int)nVersion defaultIndex:(NSString*)defaultIndex;
-(void)selectPics:(CGSize)newSize;
-(void)fireUpCamera:(CGSize)newSize;
-(void)screenShot:(CGRect)cropRect newImageWidth:(CGFloat)newImageWidth;
- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;
-(NSString*)randomImageNameGenerator:(NSString*)dataType;
-(NSString*)writeImageToTmp:(NSData*)image imageType:(NSString*)imageType;
-(bool)downloadURIData:(NSString*)uri uriBase64:(NSString **)uriBase64 pathToImage:(NSString **)pathToImage;
-(bool)convURI2Data:(NSString*)uri;
-(bool)convURI2BaseData:(NSString*)uri;
-(bool)convFile2URI2Data:(NSString*)uri;
-(void)sendMail:(NSString*)subject body:(NSString*)body isHtml:(bool)isHtml toRecipients:(NSArray*)toRecipients
   ccRecipients:(NSArray*)ccRecipients bccRecipients:(NSArray*)bccRecipients;
-(void)selectContact;
-(void)getFeatures;
-(void)openInAppPurchases;
-(void)writeFile:(NSString*)relativePath data:(NSString*)data;
-(void)readFile:(NSString*)relativePath start:(int)start end:(int)end;
-(void)openDropBoxChooser:(NSString*)cbSuccess cbCancel:(NSString*)cbCancel sid:(NSString*)sid;
-(void)openDropBoxChooserFromAttachment;

@end
