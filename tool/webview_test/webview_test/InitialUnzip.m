//
//  InitialUnzip.m
//  webview_test
//
//  Created by Marko Perutovic on 11/3/12.
//  Copyright (c) 2012 Marko Perutovic. All rights reserved.
//

#import "InitialUnzip.h"
#import "ZipArchive.h"
#import "default_constants.h"

@implementation InitialUnzip

+(bool)unzipInitialVersion
{
    NSString *defaultZipPath = [[NSBundle mainBundle] pathForResource:INIT_HTML_FILE ofType:@""];
    return [InitialUnzip unzipDownloadedFile:defaultZipPath];
}

+(bool)unzipDownloadedFile:(NSString*)downloadedFilePath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:DEFAULT_HTML_DIR];
    
    //If doesn't exist create html dir.
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:nil];
    }
    else
    {
        //Delete previous html dir and create new one.
        [[NSFileManager defaultManager] removeItemAtPath:dataPath error:nil];
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:nil];
    }
    
    bool ret = false;
    
    //Unzip initial html file.
    ZipArchive *zipArchive = [[ZipArchive alloc] init];
    if([zipArchive UnzipOpenFile:downloadedFilePath])
    {
        ret = [zipArchive UnzipFileTo:dataPath overWrite:YES];
    }
    else
    {
        NSLog(@"Failure To Open Archive");
        ret = false;
    }
    
    [zipArchive UnzipCloseFile];
    [zipArchive release];
    return ret;
}

@end
