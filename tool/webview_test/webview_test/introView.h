//
//  introView.h
//  webview_test
//
//  Created by Marko Perutovic on 11/19/12.
//  Copyright (c) 2012 Marko Perutovic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface introView : UIView

@property (retain, nonatomic) IBOutlet UIImageView *backgroundImage;
@property (retain, nonatomic) IBOutlet UIImageView *animationImage;

@end
