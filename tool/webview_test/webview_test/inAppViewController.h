//
//  inAppViewController.h
//  webview_test
//
//  Created by Marko Perutovic on 11/7/12.
//  Copyright (c) 2012 Marko Perutovic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <StoreKit/StoreKit.h>
#import "default_constants.h"
#import "loadIndicator.h"


@interface inAppViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, UINavigationBarDelegate, UINavigationControllerDelegate,SKProductsRequestDelegate, SKPaymentTransactionObserver>
{
    SKProduct *proUpgradeProduct;
    SKProductsRequest *productsRequest;
    NSMutableArray *productsArray;
    loadIndicator *_loadIndicator;
}

@property (retain, nonatomic) IBOutlet UIView *view;
@property (retain, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)onReloadPurchases:(id)sender;

-(void)showLoadingView;
-(void)hideLoadingView;

-(bool)canMakePayments;
-(void)checkBoughtInApps;
-(void)requestProduct:(NSString*)productID;
-(void)loadStore;
-(void)buyProduct:(NSString*)productID;
-(void)buyFeature:(SKProduct*)product;
-(void)completeTransaction:(SKPaymentTransaction*)transaction;
-(void)restoreTransaction:(SKPaymentTransaction*)transaction;
-(void)failedTransaction:(SKPaymentTransaction*)transaction;
-(void)finishTransaction:(SKPaymentTransaction *)transaction wasSuccessful:(BOOL)wasSuccessful;

@end
