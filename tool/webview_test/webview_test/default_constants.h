//
//  default_constants.h
//  webview_test
//
//  Created by Marko Perutovic on 11/3/12.
//  Copyright (c) 2012 Marko Perutovic. All rights reserved.
//

#ifndef webview_test_default_constants_h
#define webview_test_default_constants_h

#define INIT_VERSION            3559
#define INIT_HTML_FILE          @"meapp855.zip"
#define DEFAULT_HTML_DIR        @"html"
#define DEFAULT_INDEX           @"index.html"
#define DEFAULT_INDEX_PAR_KEY   @"type"
#define DEFAULT_INDEX_PAR_VAL   @"iosoc"
#define DEFAULT_XML_URL         @"http://mailegant.com/me.xml"
#define DEFAULT_XML_URL_APPLE   @"http://mailegant.com/mep.xml"
#define DEFAULT_XML_URL_marko   @"http://mailegant.com/me_marko.xml"
#define BASIC_TEMPLATE_PACK     @"BASIC_TEMPLATE_PACK"
#define PERSONAL_TEMPLATE_PACK  @"PERSONAL_TEMPLATE_PACK"
#define BASIC_PACKAGE           @"com.mailegant.mailegantwriter.basictemplatepackage"
#define PERSONAL_PACKAGE        @"com.mailegant.mailegantwriter.personaltemplates"

#endif
