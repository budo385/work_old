//
//  smtpMailViewer.h
//  webview_test
//
//  Created by Marko Perutovic on 21/03/14.
//  Copyright (c) 2014 Marko Perutovic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "default_constants.h"
#import "loadIndicator.h"

@interface smtpMailViewer : UIViewController<UITableViewDataSource, UITableViewDelegate, UINavigationBarDelegate, UINavigationControllerDelegate>
{
    loadIndicator *_loadIndicator;
}
@property (retain, nonatomic) IBOutlet UIView *view;
@property (retain, nonatomic) IBOutlet UITableView *tableView;

@end