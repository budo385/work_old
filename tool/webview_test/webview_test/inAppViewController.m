//
//  inAppViewController.m
//  webview_test
//
//  Created by Marko Perutovic on 11/7/12.
//  Copyright (c) 2012 Marko Perutovic. All rights reserved.
//

#import "inAppViewController.h"

@implementation inAppViewController

#define MAINLABEL_TAG 1
#define SECONDLABEL_TAG 2
#define PHOTO_TAG 3

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    [self checkBoughtInApps];
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
//        productsArray = [[NSMutableArray arrayWithObjects:BASIC_PACKAGE,PERSONAL_PACKAGE,nil] retain];
        productsArray = [[NSMutableArray arrayWithObjects:BASIC_PACKAGE,nil] retain];

        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.opaque = NO;
        
        _loadIndicator = [[loadIndicator alloc] initWithParent:self.view];
        
        self.view.backgroundColor = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"bgh.png"]];
        
    }
    
    return self;
}

- (void)dealloc {
    [productsArray release];
    [_tableView release];
    [super dealloc];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillDisappear:animated];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    
    [self setTableView:nil];
    [super viewDidUnload];
}

- (IBAction)onReloadPurchases:(id)sender
{
    [self showLoadingView];
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}

//----------------------------------------------
//Load indicator view.
//----------------------------------------------

-(void)showLoadingView
{
    [_loadIndicator show];
}

-(void)hideLoadingView
{
    [_loadIndicator hide];
}

//----------------------------------------------
//In App Purchase methods.
//----------------------------------------------
-(bool)canMakePayments
{
    if ([SKPaymentQueue canMakePayments])
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

-(void)checkBoughtInApps
{
    if ([[NSUserDefaults standardUserDefaults] boolForKey:BASIC_TEMPLATE_PACK])
    {
        [productsArray removeObject:BASIC_PACKAGE];
    }
    if ([[NSUserDefaults standardUserDefaults] boolForKey:PERSONAL_TEMPLATE_PACK])
    {
        [productsArray removeObject:PERSONAL_PACKAGE];
    }
}

- (void)requestProduct:(NSString*)productID
{
    productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:[NSSet setWithObject:productID]];
    productsRequest.delegate = self;
    [productsRequest start];
    
    // we will release the request object in the delegate callback
}

-(void)loadStore
{
    // restarts any purchases if they were interrupted last time the app was open
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
}

-(void)buyProduct:(NSString*)productID
{
    [self loadStore];
    [self requestProduct:productID];
}

-(void)buyFeature:(SKProduct*)product
{
    SKPayment *payment = [SKPayment paymentWithProduct:product];
    [[SKPaymentQueue defaultQueue] addPayment:payment];
}

-(void)completeTransaction:(SKPaymentTransaction*)transaction
{
    NSLog(@"transaction.payment.productIdentifier %@", transaction.payment.productIdentifier);

    if ([transaction.payment.productIdentifier isEqualToString:BASIC_PACKAGE])
    {
        [[NSUserDefaults standardUserDefaults] setBool:true forKey:BASIC_TEMPLATE_PACK];
    }
    if ([transaction.payment.productIdentifier isEqualToString:PERSONAL_PACKAGE])
    {
        [[NSUserDefaults standardUserDefaults] setBool:true forKey:PERSONAL_TEMPLATE_PACK];
    }

    [self finishTransaction:transaction wasSuccessful:YES];
}

-(void)restoreTransaction:(SKPaymentTransaction*)transaction
{
    NSLog(@"transaction.payment.productIdentifier %@", transaction.payment.productIdentifier);

    if ([transaction.payment.productIdentifier isEqualToString:BASIC_PACKAGE])
    {
        [[NSUserDefaults standardUserDefaults] setBool:true forKey:BASIC_TEMPLATE_PACK];
    }
    if ([transaction.payment.productIdentifier isEqualToString:PERSONAL_PACKAGE])
    {
        [[NSUserDefaults standardUserDefaults] setBool:true forKey:PERSONAL_TEMPLATE_PACK];
    }

    [self finishTransaction:transaction wasSuccessful:YES];
}

- (void)finishTransaction:(SKPaymentTransaction *)transaction wasSuccessful:(BOOL)wasSuccessful
{
    [self hideLoadingView];
    
    // remove the transaction from the payment queue.
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
    
    UIAlertView *alert = nil;
    
    NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:transaction, @"transaction" , nil];
    if (wasSuccessful)
    {
        // send out a notification that we’ve finished the transaction
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Transaction completed!" object:self userInfo:userInfo];

        alert = [[UIAlertView alloc]
                 initWithTitle: @"InApp Purchase"
                 message: @"Transaction completed!"
                 delegate: nil
                 cancelButtonTitle:@"OK"
                 otherButtonTitles:nil];
    }
    else
    {
        // send out a notification for the failed transaction
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Transaction error!" object:self userInfo:userInfo];

        alert = [[UIAlertView alloc]
                 initWithTitle: @"InApp Purchase"
                 message: @"Transaction error!!"
                 delegate: nil
                 cancelButtonTitle:@"OK"
                 otherButtonTitles:nil];
    }

    [alert show];
    [alert release];
}

//----------------------------------------------
//In App Purchase observer.
//----------------------------------------------
- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response
{
    [self hideLoadingView];
    if ([[response products] count] == 1)
    {
        [self showLoadingView];
        [self buyFeature:[[response products] lastObject]];
    }
    else
    {
        NSLog(@"Invalid product id!");
    }

    [productsRequest release];
}

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
    for (SKPaymentTransaction *transaction in transactions)
    {
        switch (transaction.transactionState)
        {
            case SKPaymentTransactionStatePurchased:
                [self completeTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                [self failedTransaction:transaction];
                break;
            case SKPaymentTransactionStateRestored:
                [self restoreTransaction:transaction];
                break;
            default:
                break;
        }
    }
}

-(void)failedTransaction:(SKPaymentTransaction*)transaction
{
    [self hideLoadingView];
    if (transaction.error.code != SKErrorPaymentCancelled)
    {
        // error!
        [self finishTransaction:transaction wasSuccessful:NO];
    }
    else
    {
        // this is fine, the user just cancelled, so don’t notify
        [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
    }
}

- (void)paymentQueue:(SKPaymentQueue *)queue restoreCompletedTransactionsFailedWithError:(NSError *)error
{
    [self hideLoadingView];
    UIAlertView *tmp = [[UIAlertView alloc]
                        initWithTitle:@"Error Restoring"
                        message:[error localizedDescription]
                        delegate:self
                        cancelButtonTitle:nil
                        otherButtonTitles:@"OK", nil];
    [tmp show];
    [tmp release];
}

- (void) paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue
{
    [self hideLoadingView];
    NSLog(@"Restore completed transactions finished.");
    NSLog(@" Number of transactions in queue: %d", [[queue transactions] count]);

    if ([[queue transactions] count]==0)
    {
        UIAlertView *tmp = [[UIAlertView alloc]
                            initWithTitle:@"Purchases Restoring"
                            message:@"There were no transactions to restore!"
                            delegate:self
                            cancelButtonTitle:nil
                            otherButtonTitles:@"OK", nil];
        [tmp show];
        [tmp release];
    }
    else
    {
        NSMutableString *message = [NSMutableString stringWithString:@"Restored products:"];
        for (SKPaymentTransaction *trans in [queue transactions])
        {
            [self restoreTransaction:trans];
            
            if ([[[trans payment] productIdentifier] isEqualToString:BASIC_PACKAGE])
            {
                [message appendString:@"\nBasic Template Package"];
            }
            if ([[[trans payment] productIdentifier] isEqualToString:PERSONAL_PACKAGE])
            {
                [message appendString:@"\nPersonal Templates"];
            }
            
            NSLog(@" transaction id %@ for product %@.", [trans transactionIdentifier], [[trans payment] productIdentifier]);
            NSLog(@" original transaction id: %@ for product %@.", [[trans originalTransaction] transactionIdentifier],
                  [[[trans originalTransaction] payment]productIdentifier]);
        }
        UIAlertView *tmp = [[UIAlertView alloc]
                            initWithTitle:@"Purchases Restored"
                            message:message
                            delegate:self
                            cancelButtonTitle:nil
                            otherButtonTitles:@"OK", nil]; 
        [tmp show];
        [tmp release];
    }
}

//----------------------------------------------
//Table delegate.
//----------------------------------------------

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [productsArray count];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
 
    static NSString *CellIdentifier = @"ImageOnRightCell";
    
    UILabel *mainLabel, *secondLabel;
    UIImageView *photo;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.backgroundColor = [UIColor clearColor];

        UIImageView *arrowImageView = [[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"arrow_white" ofType:@"png"]]];
        cell.accessoryView = arrowImageView;
        [arrowImageView release];
        
        mainLabel = [[UILabel alloc] initWithFrame:CGRectMake(95.0, 20.0, 220.0, 40)];
        mainLabel.tag = MAINLABEL_TAG;
        mainLabel.font = [UIFont boldSystemFontOfSize:16.0];
        mainLabel.textColor = [UIColor whiteColor];
        mainLabel.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:mainLabel];
        
        secondLabel = [[UILabel alloc] initWithFrame:CGRectMake(95.0, 50.0, 220.0, 105.0)];
        secondLabel.tag = SECONDLABEL_TAG;
        secondLabel.font = [UIFont boldSystemFontOfSize:11.0];
        secondLabel.textColor = [UIColor whiteColor];
        secondLabel.numberOfLines=4;
        secondLabel.backgroundColor = [UIColor clearColor];
        [secondLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleRightMargin];
        [cell.contentView addSubview:secondLabel];
        
        photo = [[UIImageView alloc] initWithFrame:CGRectMake(10.0, 35.0, 75.0, 75.0)];
        photo.tag = PHOTO_TAG;
        [cell.contentView addSubview:photo];
    }
    else
    {
        mainLabel = (UILabel *)[cell.contentView viewWithTag:MAINLABEL_TAG];
        secondLabel = (UILabel *)[cell.contentView viewWithTag:SECONDLABEL_TAG];
        photo = (UIImageView *)[cell.contentView viewWithTag:PHOTO_TAG];
    }

    if([[productsArray objectAtIndex:indexPath.row] isEqualToString:BASIC_PACKAGE])
    {
        mainLabel.text = @"mailegant Premium";
        secondLabel.text = @"Additional features for mailegant users:\n1. Use more than one email address\n2. Store an unlimited number of Textlets (optionally formatted text modules)\n3. Store an unlimited number of your own email Templates";
    }
    if([[productsArray objectAtIndex:indexPath.row] isEqualToString:PERSONAL_PACKAGE])
    {
        mainLabel.text = @"Personal Templates";
        secondLabel.text = @"Define an unlimited number of your own personal e-mail templates.";
    }
    photo.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Logo_mailegance_144" ofType:@"png"]];

//    [mainLabel release];
//    [secondLabel release];
//    [photo release];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    [self showLoadingView];
    [self buyProduct:[productsArray objectAtIndex:indexPath.row]];
}

@end
