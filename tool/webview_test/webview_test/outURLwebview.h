//
//  outURLwebview.h
//  webview_test
//
//  Created by Marko Perutovic on 28/01/14.
//  Copyright (c) 2014 Marko Perutovic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface outURLwebview : UIViewController <UINavigationControllerDelegate>
{
    NSString* strCallback;
    NSURL* webviewURL;
    id thisViewParent;
}

@property (nonatomic, retain) IBOutlet UIWebView *webview_outUrl;

- (id)initWithURLandTitleandCallback:(NSString*)urlStr title:(NSString*)title callback:(NSString*)callback thisParent:(id)thisParent;

@end

