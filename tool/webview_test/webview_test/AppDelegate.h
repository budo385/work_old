//
//  AppDelegate.h
//  webview_test
//
//  Created by Marko Perutovic on 9/29/12.
//  Copyright (c) 2012 Marko Perutovic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
