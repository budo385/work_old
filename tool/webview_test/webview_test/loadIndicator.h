//
//  loadIndicator.h
//  webview_test
//
//  Created by Marko Perutovic on 11/16/12.
//  Copyright (c) 2012 Marko Perutovic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface loadIndicator : UIView
{
    UIView *parent;
}

- (id)initWithParent:(UIView*)parentView;

-(void)show;
-(void)hide;

@end
