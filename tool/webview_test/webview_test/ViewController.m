//
//  ViewController.m
//  webview_test
//
//  Created by Marko Perutovic on 9/29/12.
//  Copyright (c) 2012 Marko Perutovic. All rights reserved.
//

#import "ViewController.h"
#import "default_constants.h"
#import "TBXML+HTTP.h"
#import "InitialUnzip.h"
#import <QuartzCore/QuartzCore.h>
#import "Base64.h"
#import "Preferences.h"
#import "inAppViewController.h"
#import "outURLwebview.h"
#import "smtpMailViewer.h"
#import "NSURL+PathParameters.h"
#import <DBChooser/DBChooser.h>
#import <DBChooser/DBChooserResult.h>


@implementation ViewController

@synthesize webview;
@synthesize JSAPI;


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib
    
    intro = [[introView alloc] initWithFrame:self.view.frame];
    [self.view addSubview:intro];
    [intro release];
    
    JavascriptApi *api = [[JavascriptApi alloc] initWithWebView:webview];
//    JavascriptUtil *gate = [[JavascriptUtil alloc]init];
//    [api setDelegate:gate forNamespace:@"util"];
    [api setDelegate:self forNamespace:@"mailegantCall"];
//    [gate release];
    self.JSAPI = api;
    [api release];

    //Check for new version.
    [self readXML];
    
    webview.delegate=self;

    
//    NSString *sql=@"CREATE TABLE IF NOT EXISTS MW_DATA4A(ID INTEGER PRIMARY KEY ASC, LIST TEXT, NAME TEXT, VALUE TEXT, ITEM_POSITION INTEGER)";
//    [self executeSql:sql];
    
//    NSString *sql1=@"INSERT INTO MW_DATA4A (LIST, NAME, VALUE, ITEM_POSITION) VALUES('list1','name1','value1',2)";
//    [self executeSql:sql1];

//    [self openSqliteDb];
//    NSString *sql2=@"SELECT * FROM MW_DATA4A";
//    [self executeSql:sql2];
}

-(void)openSqliteDb
{
    NSString *ret;
    //Init database.
    if (_database == nil)
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
        NSString *dirPath = [documentsDirectory stringByAppendingPathComponent:@"mw"];
        NSString *dbPath = [dirPath stringByAppendingPathComponent:@"mw.db"];
        
        BOOL isDir;
        if(![[NSFileManager defaultManager] fileExistsAtPath:dbPath  isDirectory:&isDir])
        {
            if(![[NSFileManager defaultManager] fileExistsAtPath:dirPath  isDirectory:&isDir])
            {
                [[NSFileManager defaultManager] createDirectoryAtPath:dirPath withIntermediateDirectories:NO attributes:nil error:nil];
            }
            
        }
        
        if(!sqlite3_open([dbPath UTF8String], &_database) == SQLITE_OK)
        {
            sqlite3_close(_database);
            ret=@"0";
        }
        else
        {
            ret=@"1";
        }
    }
    ret=@"1";
    
    NSString *javaCall=[[NSArray arrayWithObjects:@"openIOSDatabase('", ret, @"')", nil] componentsJoinedByString:@""];
    [webview stringByEvaluatingJavaScriptFromString:javaCall];
}

-(void)closeSqliteDb
{
    NSString *ret;
    if(_database!=nil)
    {
        if(sqlite3_close(_database) == SQLITE_OK)
        {
            _database = nil;
            ret=@"1";
        }
        else
        {
            ret=@"0";
        }
    }
    ret=@"1";

    NSString *javaCall=[[NSArray arrayWithObjects:@"closeIOSDatabase('", ret, @"')", nil] componentsJoinedByString:@""];
    [webview stringByEvaluatingJavaScriptFromString:javaCall];
}

- (void)executeSql:(NSString*) sqlStatement
{
    NSMutableString *jsonStringCustom = [NSMutableString stringWithString:@"["];;
    NSString* strStatus=@"0";
    
    sqlite3_stmt *statement;
//    int statementRet = sqlite3_prepare_v2(_database, [sqlStatement UTF8String], -1, &statement, nil);
    if (sqlite3_prepare_v2(_database, [sqlStatement UTF8String], -1, &statement, nil) == SQLITE_OK)
    {
        NSMutableArray * colNames = [[NSMutableArray alloc] init];
        int cols = sqlite3_column_count(statement);
        for (int i=0; i<cols; i++)
        {
            [colNames addObject:[[NSString alloc] initWithUTF8String:(char *) sqlite3_column_name(statement, i)]];
        }

        while (sqlite3_step(statement) == SQLITE_ROW)
        {
            [jsonStringCustom appendString:@"{"];

            for (int i=0; i<cols; i++)
            {
                NSString *rowValue = [[NSString alloc] initWithUTF8String:(char *) sqlite3_column_text(statement, i) ];

                [jsonStringCustom appendString:@"\""];
                [jsonStringCustom appendString:[colNames objectAtIndex:i]];
                [jsonStringCustom appendString:@"\":\""];
                [jsonStringCustom appendString:rowValue];
                [jsonStringCustom appendString:@"\""];
                
                [rowValue release];
                
                if(i+1<cols)
                {
                    [jsonStringCustom appendString:@","];
                }
                else
                {
                    [jsonStringCustom appendString:@"}"];
                }
            }
            
            [jsonStringCustom appendString:@","];
        }

        if ([jsonStringCustom length]>1)
        {
            [jsonStringCustom deleteCharactersInRange:NSMakeRange([jsonStringCustom length]-1, 1)];
        }

        [colNames release];
        sqlite3_finalize(statement);
    }
    else
    {
        strStatus = [strStatus stringByReplacingOccurrencesOfString:@"0" withString:@"1"];
        NSLog(@"error jsonData %@", jsonStringCustom);
    }

    [jsonStringCustom appendString:@"]"];
    [jsonStringCustom stringByReplacingOccurrencesOfString:@"'" withString:@"&#39;"];
    //    jsonStringCustom = [jsonStringCustom stringByReplacingOccurrencesOfString:@"\"" withString:@"&quot;"];
    
    //NSLog(@"jsonString %@", jsonStringCustom);

    NSString *javaCall=[[NSArray arrayWithObjects:@"executeIOSDatabaseQuery('",strStatus,@"', '", jsonStringCustom, @"');", nil] componentsJoinedByString:@""];
    [webview stringByEvaluatingJavaScriptFromString:javaCall];
}


- (void)viewWillAppear:(BOOL)animated
{
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
}
- (void)dealloc
{
    [super dealloc];
    popoverController = nil;
    tbxml = nil;
    intro = nil;
    [self closeSqliteDb];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return (interfaceOrientation == UIInterfaceOrientationPortrait ||
            interfaceOrientation == UIInterfaceOrientationLandscapeLeft ||
            interfaceOrientation == UIInterfaceOrientationLandscapeRight ||
            interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown );
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if (UIInterfaceOrientationIsLandscape(toInterfaceOrientation))
    {
        _adView.currentContentSizeIdentifier =
        ADBannerContentSizeIdentifierLandscape;
    }
    else
    {
        _adView.currentContentSizeIdentifier =
        ADBannerContentSizeIdentifierPortrait;
    }

}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    if((fromInterfaceOrientation == UIDeviceOrientationLandscapeLeft) || (fromInterfaceOrientation == UIDeviceOrientationLandscapeRight))
    {
        if (intro!=nil)
        {
            UIImage *back=[UIImage imageNamed:@"Default-Portrait.png"];
            [intro.backgroundImage setImage:back];
        }
        
        if(_adView!=nil)
        {
            _adView.frame=  CGRectMake(0 , webview.frame.size.height, _adView.frame.size.width, _adView.frame.size.height);
        }
    }
    else  if((fromInterfaceOrientation == UIDeviceOrientationPortrait) || (fromInterfaceOrientation == UIDeviceOrientationPortraitUpsideDown))
    {
        if (intro!=nil)
        {
            UIImage *back=[UIImage imageNamed:@"Default-Landscape.png"];
            [intro.backgroundImage setImage:back];
        }

        if(_adView!=nil)
        {
            _adView.frame=  CGRectMake(0 , webview.frame.size.height, _adView.frame.size.width, _adView.frame.size.height);
        }
    }
}

-(NSString*) encodePicsData:(NSData*) rawBytes fileExtension:(NSString*) fileExtension
{
    NSString *imageAsBase64 = [Base64 encode:rawBytes];

    NSString *retString=@"data:";
    retString = [retString stringByAppendingString:[Preferences getMimeTypeForExtension:fileExtension]];
    retString = [retString stringByAppendingString:@";base64,"];
    retString = [retString stringByAppendingString:imageAsBase64];

    return retString;
}

//Send mail - MailCore API.
-(void)sendMimeMail:(NSString*)from to:(NSString*)to subject:(NSString*)subject mimebody:(NSString*)mimebody mimefrom:(NSString*)mimefrom cc:(NSString*)cc bcc:(NSString*)bcc strServerAddress:(NSString*)strServerAddress nPort:(int)nPort nSmtpConnectionType:(int)nSmtpConnectionType bSmtpUseAuth:(BOOL)bSmtpUseAuth txt_strUserName:(NSString*)txt_strUserName txt_strPassword:(NSString*)txt_strPassword
{
    MCOSMTPSession *smtpSession = [[MCOSMTPSession alloc] init];
    if(nSmtpConnectionType==1)
    {
        smtpSession.connectionType=MCOConnectionTypeClear;
    }
    //Start TLS.
    else if(nSmtpConnectionType==2)
    {
        smtpSession.connectionType=MCOConnectionTypeStartTLS;
    }
    //TLS.
    else if(nSmtpConnectionType==3)
    {
        smtpSession.connectionType=MCOConnectionTypeTLS;
    }
    
    //Use authentication.
    if(bSmtpUseAuth)
    {
        smtpSession.authType= MCOAuthTypeSASLPlain;
    }

    smtpSession.hostname = strServerAddress;
    smtpSession.port = nPort;
    smtpSession.username = txt_strUserName;
    smtpSession.password = txt_strPassword;
    
    NSString *strMimeBody = [[NSString alloc] initWithString:@""];
    strMimeBody= [strMimeBody stringByAppendingString:mimefrom];
    strMimeBody= [strMimeBody stringByAppendingString:@"\r\n"];
    strMimeBody= [strMimeBody stringByAppendingString:mimebody];
    
    NSData * rfc822Data = [strMimeBody dataUsingEncoding:NSUTF8StringEncoding];

    MCOSMTPSendOperation *sendOperation = [smtpSession sendOperationWithData:rfc822Data];
    [sendOperation start:^(NSError *error) {
        if(error)
        {
            NSLog(@"error sending %@", [error localizedDescription]);
            NSString *javaCall=[[NSArray arrayWithObjects:@"sendMailCallBack('false','Error code: ", [NSString stringWithFormat:@"%d", [error code]] , @"')", nil] componentsJoinedByString:@""];
            [webview stringByEvaluatingJavaScriptFromString:javaCall];
        }
        else
        {
            NSLog(@"success sending mail");
            NSString *javaCall=[[NSArray arrayWithObjects:@"sendMailCallBack('true','", @"", @"')", nil] componentsJoinedByString:@""];
            [webview stringByEvaluatingJavaScriptFromString:javaCall];
        }
    }];
}

-(void)testMimeMailConnectionMailCoreNewNoType:(NSString*)strServerAddress nPort:(int)nPort nSmtpConnectionType:(int)nSmtpConnectionType bSmtpUseAuth:(BOOL)bSmtpUseAuth txt_strUserName:(NSString*)txt_strUserName txt_strPassword:(NSString*)txt_strPassword txt_emailFrom:(NSString*)txt_emailFrom
{
    nSmtpConnectionType++;

    MCOSMTPSession *smtpSession = [[MCOSMTPSession alloc] init];
    //Use authentication.
    if(bSmtpUseAuth)
    {
        smtpSession.authType= MCOAuthTypeSASLPlain;
    }
    
    smtpSession.hostname = strServerAddress;
    smtpSession.port = nPort;
    smtpSession.username = txt_strUserName;
    smtpSession.password = txt_strPassword;
    smtpSession.timeout = 2;

    if(nSmtpConnectionType==1)
    {
        smtpSession.connectionType=MCOConnectionTypeClear;
    }
    //Start TLS.
    else if(nSmtpConnectionType==2)
    {
        smtpSession.connectionType=MCOConnectionTypeStartTLS;
    }
    //TLS.
    else if(nSmtpConnectionType==3)
    {
        smtpSession.connectionType=MCOConnectionTypeTLS;
        
    }
    
    if(nSmtpConnectionType>3)
    {
        nSmtpConnectionType=-1;
        NSString *javaCall=[[NSArray arrayWithObjects:@"testMailSettingsCallBack('false','", [NSString stringWithFormat:@"%d", nSmtpConnectionType], @"')", nil] componentsJoinedByString:@""];
        [webview stringByEvaluatingJavaScriptFromString:javaCall];
    }
    
    MCOSMTPOperation * op = [smtpSession checkAccountOperationWithFrom:[MCOAddress addressWithMailbox:txt_emailFrom]];
    [op start:^(NSError * error)
     {
         if (error==nil)
         {
             NSString *javaCall=[[NSArray arrayWithObjects:@"testMailSettingsCallBack('true','", [NSString stringWithFormat:@"%d", nSmtpConnectionType], @"')", nil] componentsJoinedByString:@""];
             [webview stringByEvaluatingJavaScriptFromString:javaCall];
         }
         else
         {
             [self testMimeMailConnectionMailCoreNewNoType:(NSString*)strServerAddress nPort:(int)nPort nSmtpConnectionType:(int)nSmtpConnectionType bSmtpUseAuth:(BOOL)bSmtpUseAuth txt_strUserName:(NSString*)txt_strUserName txt_strPassword:(NSString*)txt_strPassword txt_emailFrom:(NSString*)txt_emailFrom];
         }
     }];
}

-(void)testMimeMailConnection:(NSString*)strServerAddress nPort:(int)nPort nSmtpConnectionType:(int)nSmtpConnectionType bSmtpUseAuth:(BOOL)bSmtpUseAuth txt_strUserName:(NSString*)txt_strUserName txt_strPassword:(NSString*)txt_strPassword txt_emailFrom:(NSString*)txt_emailFrom
{
    //Automatic.
    if(nSmtpConnectionType==0)
    {
        [self testMimeMailConnectionMailCoreNewNoType:(NSString*)strServerAddress nPort:(int)nPort nSmtpConnectionType:(int)nSmtpConnectionType bSmtpUseAuth:(BOOL)bSmtpUseAuth txt_strUserName:(NSString*)txt_strUserName txt_strPassword:(NSString*)txt_strPassword txt_emailFrom:(NSString*)txt_emailFrom];
    }
    else
    {
        MCOSMTPSession *smtpSession = [[MCOSMTPSession alloc] init];
        //Use authentication.
        if(bSmtpUseAuth)
        {
            smtpSession.authType= MCOAuthTypeSASLPlain;
        }
        
        smtpSession.hostname = strServerAddress;
        smtpSession.port = nPort;
        smtpSession.username = txt_strUserName;
        smtpSession.password = txt_strPassword;

        //Plain.
        if(nSmtpConnectionType==1)
        {
            smtpSession.connectionType=MCOConnectionTypeClear;
        }
        //Start TLS.
        else if(nSmtpConnectionType==2)
        {
            smtpSession.connectionType=MCOConnectionTypeStartTLS;
        }
        //TLS.
        else if(nSmtpConnectionType==3)
        {
            smtpSession.connectionType=MCOConnectionTypeTLS;
            
        }
        
        MCOSMTPOperation * op = [smtpSession checkAccountOperationWithFrom:[MCOAddress addressWithMailbox:txt_emailFrom]];
        [op start:^(NSError * error)
         {
             if (error==nil)
             {
                 NSString *javaCall=[[NSArray arrayWithObjects:@"testMailSettingsCallBack('true','", [NSString stringWithFormat:@"%d", nSmtpConnectionType], @"')", nil] componentsJoinedByString:@""];
                 [webview stringByEvaluatingJavaScriptFromString:javaCall];
             }
             else
             {
                 NSString *javaCall=[[NSArray arrayWithObjects:@"testMailSettingsCallBack('false','", [NSString stringWithFormat:@"%d", nSmtpConnectionType], @"')", nil] componentsJoinedByString:@""];
                 [webview stringByEvaluatingJavaScriptFromString:javaCall];
             }
         }];
    }
}

//Read XML.
-(void)readXML
{
    NSLog(@"appVersion 1 %i", [[NSUserDefaults standardUserDefaults] integerForKey:@"appVersion"]);
    
    TBXMLSuccessBlock successBlock = ^(TBXML *tbxmlDocument)
    {
        NSLog(@"PROCESSING ASYNC CALLBACK");
        
        TBXMLElement *app = [TBXML childElementNamed:@"app" parentElement:tbxmlDocument.rootXMLElement];
        TBXMLElement *version = [TBXML childElementNamed:@"version" parentElement:app];
        TBXMLElement *appdownload = [TBXML childElementNamed:@"appdownload" parentElement:app];
        TBXMLElement *apploc = [TBXML childElementNamed:@"apploc" parentElement:app];
        NSLog(@"%@",[TBXML elementName:version]);
        NSLog(@"%@",[TBXML textForElement:version]);
        NSLog(@"%@",[TBXML elementName:appdownload]);
        NSLog(@"%@",[TBXML textForElement:appdownload]);

//        if (1)
        if ([[TBXML textForElement:version] intValue]>[[NSUserDefaults standardUserDefaults] integerForKey:@"appVersion"])
        {
            [self downloadUpdate:[TBXML textForElement:appdownload] appVersion:[[TBXML textForElement:version] intValue] defaultIndex:[TBXML textForElement:apploc]];
        }
        else
        {
            [self loadWebView];
        }

        [tbxml release];
    };
    
    // Create a failure block that gets called if something goes wrong
    TBXMLFailureBlock failureBlock = ^(TBXML *tbxmlDocument, NSError * error)
    {
        [self loadWebView];
        
        NSLog(@"Error! %@ %@", [error localizedDescription], [error userInfo]);

        [tbxml release];
    };

//    tbxml = [TBXML newTBXMLWithURL:[NSURL URLWithString:DEFAULT_XML_URL_APPLE] success:successBlock failure:failureBlock]; //release
    
    tbxml = [TBXML newTBXMLWithURL:[NSURL URLWithString:DEFAULT_XML_URL] success:successBlock failure:failureBlock]; //debug

//    tbxml = [TBXML newTBXMLWithURL:[NSURL URLWithString:DEFAULT_XML_URL_marko] success:successBlock failure:failureBlock]; //debug
}

//Webview delegate methods. 
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSURL *url = [request URL];
    if ([self.JSAPI matchURL:url])
    {
        return NO;
    }
    return YES;
}


- (void)webViewDidStartLoad:(UIWebView *)webView
{
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    if(intro!=nil)
    {
        [intro removeFromSuperview];
        intro = nil;
    }

//    [self openURLinSafari:@"http://monitor.hr"];
    //    [self sendMailCoreMail];
//   [self openOutURLwebview:@"http://mailegant.com/img/mwLogo.png" title:@"marko" callback:@"callback"];
    //    [self openInAppPurchases];
    //    [self getFeatures];
    //    [self selectContact];
    //    [self sendMail];
    //    [self selectPics:CGSizeMake(100, 100)];
    //    [self convURI2Data:@"http://mailegant.com/img/mwLogo.png"];
    //    [self screenShot:CGRectMake(100, 100, 150, 100) newImageWidth:150];
    //    NSString *defaultZipPath = [[NSBundle mainBundle] pathForResource:INIT_HTML_FILE ofType:@""];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
}

//Instance methods
- (void) jsCall:(JavascriptApi *)api callWrapper:(NSMutableArray *)params
{
    NSString *methodName = (NSString *)[params objectAtIndex:0];
//    NSString *methodName1 = (NSString *)[params objectAtIndex:1];
    
    if([methodName isEqualToString:[[NSString alloc] initWithString:@"eventScotty"]])
    {
        NSString *function=[NSString stringWithFormat:@"eventDispatcher('%@')", methodName];
        [webview stringByEvaluatingJavaScriptFromString:function];
    }
    else if([methodName isEqualToString:[[NSString alloc] initWithString:@"selectPics"]])
    {
        int maxWidth=[[params objectAtIndex:1] integerValue];
        int maxHeight=[[params objectAtIndex:2] integerValue];
        
        [self selectPics:CGSizeMake(maxWidth, maxHeight)];
    }
    else if([methodName isEqualToString:[[NSString alloc] initWithString:@"getScreenPic"]])
    {
        int xOffSet=[[params objectAtIndex:1] integerValue];
        int yOffSet=[[params objectAtIndex:2] integerValue];
        int cropWidth=[[params objectAtIndex:3] integerValue];
        int cropHeight=[[params objectAtIndex:4] integerValue];
        int resizeWidth=[[params objectAtIndex:5] integerValue];

        [self screenShot:CGRectMake(xOffSet, yOffSet, cropWidth, cropHeight) newImageWidth:resizeWidth];
    }
    else if([methodName isEqualToString:[[NSString alloc] initWithString:@"conv2DataURI"]])
    {
        NSString *uri=[params objectAtIndex:1];
        [self convURI2Data:uri];
    }
    else if([methodName isEqualToString:[[NSString alloc] initWithString:@"convURI2BaseData"]])
    {
        NSString *uri=[params objectAtIndex:1];
        [self convURI2BaseData:uri];
    }
    else if([methodName isEqualToString:[[NSString alloc] initWithString:@"convFile2DataURI"]])
    {
        NSString *uri=[params objectAtIndex:1];
        [self convFile2URI2Data:uri];
    }
    else if([methodName isEqualToString:[[NSString alloc] initWithString:@"scalePic"]])
    {
        int maxWidth=[[params objectAtIndex:1] integerValue];
        int maxHeight=[[params objectAtIndex:2] integerValue];
        
        [self selectPics:CGSizeMake(maxWidth, maxHeight)];
    }
    else if([methodName isEqualToString:[[NSString alloc] initWithString:@"openEmailDialog"]])
    {
//        var strTo=e.strPar1;
//        var strBcc=e.strPar2;
//        var strCC=e.strPar3;
//        var subject=e.strPar4;
//        var body=e.strPar5;
//        var strAttachments=e.strPar6;
		
//        var mailDialog=require('openMailDialog');
//        mailDialog.open(strTo,strBcc,strCC,subject,body,strAttachments);
    }
    else if([methodName isEqualToString:[[NSString alloc] initWithString:@"setContentHeight"]])
    {
        int height=[[params objectAtIndex:1] integerValue];
        CGRect rect = webview.frame;
        rect.size.height=height;
        [webview setFrame:rect];
    }
	/*    else if(methodName=='setHeaderHeight')
     {
     setHeadHeight(e.numPar1);
     }
     else if(methodName=='setSidebarWidth')
     {
     setSidebarWidth(e.numPar1);
     }*/
    else if([methodName isEqualToString:[[NSString alloc] initWithString:@"selectContact"]])
    {
        [self selectContact];
//        var selectContact=require('selectContact');
//        selectContact.select(editor_webview);
    }
    else if([methodName isEqualToString:[[NSString alloc] initWithString:@"getFeatures"]])
    {
        [self getFeatures];
        
//        var getFeatures=require('getFeatures');
//        var evalStr=getFeatures.getFeatures();
//        editor_webview.evalJS("registerFeatures('"+evalStr+"')");
    }
    else if([methodName isEqualToString:[[NSString alloc] initWithString:@"inAppPurchase"]])
    {
        [self openInAppPurchases];

//        var inAppWindow = require('in_app_purchase');
//        new inAppWindow().open({modal:true});
    }
    else if([methodName isEqualToString:[[NSString alloc] initWithString:@"liftCurtain"]])
    {
//        liftCourtainF();
    }
    else if([methodName isEqualToString:[[NSString alloc] initWithString:@"takePics"]])
    {
        int maxWidth=[[params objectAtIndex:1] integerValue];
        int maxHeight=[[params objectAtIndex:2] integerValue];
        
        [self fireUpCamera:CGSizeMake(maxWidth, maxHeight)];
    }
    else if([methodName isEqualToString:[[NSString alloc] initWithString:@"openDropBoxChooser"]])
    {
        NSString* cbSuccess = [params objectAtIndex:1];
        NSString* cbCancel = [params objectAtIndex:2];
        NSString* sid = [params objectAtIndex:3];

        [self openDropBoxChooser:cbSuccess cbCancel:cbCancel sid:sid];
    }
    else if([methodName isEqualToString:[[NSString alloc] initWithString:@"openDropBoxChooserFromAttachment"]])
    {
        [self openDropBoxChooserFromAttachment];
    }
    else if([methodName isEqualToString:[[NSString alloc] initWithString:@"WriteFile"]])
    {
        NSString* relativePath = [params objectAtIndex:1];
        NSString* data = [params objectAtIndex:2];

        [self writeFile:relativePath data:data];
    }
    else if([methodName isEqualToString:[[NSString alloc] initWithString:@"ReadFile"]])
    {
        NSString* relativePath = [params objectAtIndex:1];
        int start=[[params objectAtIndex:2] integerValue];
        int end=[[params objectAtIndex:3] integerValue];
        
        [self readFile:relativePath start:start end:end];
    }
    else if([methodName isEqualToString:[[NSString alloc] initWithString:@"sendemail"]])
    {
        NSString* from              = [params objectAtIndex:1];
        NSString* to                = [params objectAtIndex:2];
        NSString* subject           = [params objectAtIndex:3];
        NSString* sMime             = [params objectAtIndex:4];
        NSString* fullfrom          = [params objectAtIndex:5];
        NSString* cc                = [params objectAtIndex:6];
        NSString* bcc               = [params objectAtIndex:7];
        NSString* SmtpServer        = [params objectAtIndex:8];
        int SmtpServerPort          = [[params objectAtIndex:9] integerValue];
        int SmtpConnectionType      = [[params objectAtIndex:10] integerValue];
        BOOL SmtpUseAuth            = [[params objectAtIndex:11] boolValue];
        NSString* SmtpUserName      = [params objectAtIndex:12];
        NSString* SmtpPassword      = [params objectAtIndex:13];
        
        [self
        sendMimeMail:from
        to:to
        subject:subject
        mimebody:sMime
        mimefrom:fullfrom
        cc:cc
        bcc:bcc
        strServerAddress:SmtpServer
        nPort:SmtpServerPort
        nSmtpConnectionType:SmtpConnectionType
        bSmtpUseAuth:SmtpUseAuth
        txt_strUserName:SmtpUserName
        txt_strPassword:SmtpPassword];
    }
    else if([methodName isEqualToString:[[NSString alloc] initWithString:@"testEmailConnection"]])
    {
        NSString* SmtpServer        = [params objectAtIndex:1];
        int SmtpServerPort          = [[params objectAtIndex:2] integerValue];
        int SmtpConnectionType      = [[params objectAtIndex:3] integerValue];
        BOOL SmtpUseAuth            = [[params objectAtIndex:4] boolValue];
        NSString* SmtpUserName      = [params objectAtIndex:5];
        NSString* SmtpPassword      = [params objectAtIndex:6];
        NSString* mailFrom          = [params objectAtIndex:7];
        
        [self testMimeMailConnection:SmtpServer nPort:SmtpServerPort nSmtpConnectionType:SmtpConnectionType bSmtpUseAuth:SmtpUseAuth txt_strUserName:SmtpUserName txt_strPassword:SmtpPassword txt_emailFrom:mailFrom];
    }
    else if([methodName isEqualToString:[[NSString alloc] initWithString:@"openSqliteDb"]])
    {
        [self openSqliteDb];
    }
    else if([methodName isEqualToString:[[NSString alloc] initWithString:@"closeSqliteDb"]])
    {
        [self closeSqliteDb];
    }
    else if([methodName isEqualToString:[[NSString alloc] initWithString:@"executeSql"]])
    {
        NSString* sqlStatement = [params objectAtIndex:1];
        [self executeSql:sqlStatement];
    }
    else if([methodName isEqualToString:[[NSString alloc] initWithString:@"openWebview"]])
    {
        NSString* urlToOpen = [params objectAtIndex:1];
        NSString* title = [params objectAtIndex:2];
        NSString* callback = [params objectAtIndex:3];
        
        [self openOutURLwebview:urlToOpen title:title callback:callback];
    }
    else if([methodName isEqualToString:[[NSString alloc] initWithString:@"openDocInteractionController"]])
    {
        NSString* urlToOpen = [params objectAtIndex:1];
        BOOL isLocalFile    = [[params objectAtIndex:2] boolValue];
        
        [self openDocInteractionController:urlToOpen isLocalFile:isLocalFile];
    }
    else if([methodName isEqualToString:[[NSString alloc] initWithString:@"openURLinSafari"]])
    {
        NSString* urlToOpen = [params objectAtIndex:1];
        [self openURLinSafari:urlToOpen];
    }
    else if([methodName isEqualToString:[[NSString alloc] initWithString:@"openiADView"]])
    {
        [self openiADView];
    }
    else if([methodName isEqualToString:[[NSString alloc] initWithString:@"closeiADView"]])
    {
        [self closeiADView];
    }


    
}

-(void) openiADView
{
    _adView = [[ADBannerView alloc] initWithFrame:CGRectZero];
    _adView.requiredContentSizeIdentifiers = [NSSet setWithObjects: ADBannerContentSizeIdentifierPortrait, ADBannerContentSizeIdentifierLandscape, nil];
    
    if (UIInterfaceOrientationIsLandscape([UIDevice currentDevice].orientation))
    {
        [_adView setCurrentContentSizeIdentifier:
         ADBannerContentSizeIdentifierLandscape];
    }
    else
    {
        [_adView setCurrentContentSizeIdentifier:
         ADBannerContentSizeIdentifierPortrait];
    }

    CGRect rect = webview.frame;
    rect.size.height=rect.size.height - 66;//ipad size... //_adView.frame.size.height;
    [webview setFrame:rect];

    _adView.frame=  CGRectMake(0 , rect.size.height, _adView.frame.size.width, _adView.frame.size.height);
    [self.view addSubview:_adView];
}

-(void) closeiADView
{
    [_adView removeFromSuperview];
    [_adView release];
    _adView=nil;
    webview.frame = self.view.bounds;
}

-(void)loadWebView
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:DEFAULT_HTML_DIR];
    NSString *index = [dataPath stringByAppendingPathComponent:[[NSUserDefaults standardUserDefaults] stringForKey:@"default_index"]];

    NSURL *url =[[NSURL fileURLWithPath:index] URLByAppendingParameterName:DEFAULT_INDEX_PAR_KEY value:DEFAULT_INDEX_PAR_VAL];
    [webview  loadRequest:[NSURLRequest requestWithURL:url]];
}
-(void)downloadUpdate:(NSString*)downloadURL appVersion:(int)nVersion defaultIndex:(NSString*)defaultIndex
{ 
    NSURL *url = [NSURL URLWithString:downloadURL];
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    NSString* fileNameWithExtension = url.lastPathComponent;
    NSString *zipFilePath = [NSTemporaryDirectory() stringByAppendingPathComponent:fileNameWithExtension];
//    NSString *zipFilePath = [documentsDirectory stringByAppendingPathComponent:fileNameWithExtension];

    NSData *urlData = [NSData dataWithContentsOfURL:url];
    
    if ([urlData writeToFile:zipFilePath atomically:YES])
    {
        if ([InitialUnzip unzipDownloadedFile:zipFilePath])
        {
            [[NSFileManager defaultManager] removeItemAtPath:zipFilePath error:nil];
            [Preferences setAppPreferences:nVersion defaultIndex:defaultIndex];
        }
        [self loadWebView];
    }
    else
        [self loadWebView];
}

-(void)selectPics:(CGSize)newSize;
{
    if(popoverController){
        [popoverController dismissPopoverAnimated:YES];
        popoverController = nil;
        return;
    }

    newImageSize = newSize;
    
    //Check PhotoLibrary available or not
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
        return;

    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    imagePicker.mediaTypes = [NSArray arrayWithObjects:
                              (NSString *) kUTTypeImage,
                              nil];
    imagePicker.allowsEditing = YES;

    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:imagePicker];
        popover.delegate = self;
        popoverController = popover;
        [popoverController presentPopoverFromRect:CGRectMake(0.0, 0.0, 400.0, 400.0)
                                           inView:self.view
                         permittedArrowDirections:UIPopoverArrowDirectionAny
                                         animated:YES];
    }
    else
    {
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
    
    [imagePicker release];
}

-(void)fireUpCamera:(CGSize)newSize
{
    //Check are there any cameras.
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] || ![UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceFront])
        return;

    newImageSize = newSize;

    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    imagePicker.mediaTypes = [NSArray arrayWithObjects:
                              (NSString *) kUTTypeImage,
                              nil];
    imagePicker.allowsEditing = YES;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:imagePicker];
        popover.delegate = self;
        popoverController = popover;
        [popoverController presentPopoverFromRect:CGRectMake(0.0, 0.0, 400.0, 400.0)
                                           inView:self.view
                         permittedArrowDirections:UIPopoverArrowDirectionAny
                                         animated:YES];
    }
    else
    {
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
    
    [imagePicker release];
}

-(void)screenShot:(CGRect)cropRect newImageWidth:(CGFloat)newImageWidth
{
    UIGraphicsBeginImageContext(self.view.frame.size);
	[self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
	UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
    
    CGFloat newImageHeight = (cropRect.size.height/cropRect.size.width)*newImageWidth;
    CGSize newSize = CGSizeMake(newImageWidth, newImageHeight);
    
    CGImageRef croppedImage = CGImageCreateWithImageInRect(viewImage.CGImage, cropRect);
    
    UIImage *newResizedImage = [self imageWithImage:[UIImage imageWithCGImage:croppedImage] scaledToSize:newSize];
    NSData *imageData = UIImageJPEGRepresentation(newResizedImage, 1);
//    [croppedImage release];

    NSString *imageAsBase64 = [self encodePicsData:imageData fileExtension:@"jpg"];
    NSString *pathToImage = [self writeImageToTmp:imageData imageType:@"jpg"];
    //
    
    NSString *javaCall=[[NSArray arrayWithObjects:@"selectPicsOut_iOS('", imageAsBase64, @"','", pathToImage, @"')", nil] componentsJoinedByString:@""];
    [webview stringByEvaluatingJavaScriptFromString:javaCall];

    UIGraphicsEndImageContext();
	UIImageWriteToSavedPhotosAlbum(newResizedImage, nil, nil, nil);
    UIGraphicsEndImageContext();
}

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize
    { 
    //UIGraphicsBeginImageContext(newSize);
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

-(NSString*)randomImageNameGenerator:(NSString*)dataType
{
    NSTimeInterval time = [[NSDate date] timeIntervalSince1970];
    NSString *timeString = [NSString stringWithFormat: @"%i", (int)time];
    NSString *fileName = [timeString stringByAppendingString:@"."];
    fileName = [fileName stringByAppendingString:dataType];
    NSString *fileTempDir = NSTemporaryDirectory();
    NSString *filePath = [fileTempDir stringByAppendingString:fileName];
    
    return filePath;
}

-(NSString*)writeImageToTmp:(NSData*)image imageType:(NSString*)imageType
{
    NSString *pathToImage = [self randomImageNameGenerator:imageType];
    [image writeToFile:pathToImage atomically:YES];
    
    return pathToImage;
}

-(bool)downloadURIData:(NSString*)uri uriBase64:(NSString **)uriBase64 pathToImage:(NSString **)pathToImage
{
    NSURL *url = [NSURL URLWithString:uri];
    NSString *fileExtension =[uri pathExtension];
    NSString *fileName = [self randomImageNameGenerator:fileExtension];
    
    NSData *urlData = [NSData dataWithContentsOfURL:url];
    if ([urlData writeToFile:fileName atomically:YES])
    {
        *uriBase64 = [self encodePicsData:urlData fileExtension:fileExtension];
        *pathToImage = [self writeImageToTmp:urlData imageType:fileExtension];
        
        return true;
    }
    
    return false;
}

-(bool)convURI2Data:(NSString*)uri
{
    NSString *imageAsBase64 = @"";
    NSString *pathToImage = @"";
    
    if([self downloadURIData:uri uriBase64:&imageAsBase64 pathToImage:&pathToImage])
    {
        NSString *javaCall=[[NSArray arrayWithObjects:@"selectPicsOut_iOS('", imageAsBase64, @"','", pathToImage, @"')", nil] componentsJoinedByString:@""];
        [webview stringByEvaluatingJavaScriptFromString:javaCall];

        return true;
    }

    return false;
}

-(bool)convURI2BaseData:(NSString*)uri
{
    NSString *mimeAsBase64 = @"";
    NSString *pathToImage = @"";
    
    NSString *javaCall;
    
    if([self downloadURIData:uri uriBase64:&mimeAsBase64 pathToImage:&pathToImage])
    {
        NSString *mimeTypeString = [Preferences getMimeTypeForExtension:[uri pathExtension]];
        
        javaCall=[[NSArray arrayWithObjects:@"uriToMIME('", mimeAsBase64, @"','", pathToImage, @"','", mimeTypeString, @"')", nil] componentsJoinedByString:@""];
        [webview stringByEvaluatingJavaScriptFromString:javaCall];
        
        return true;
    }
    
    javaCall=[[NSArray arrayWithObjects:@"uriToMIME('','','')", nil] componentsJoinedByString:@""];
    [webview stringByEvaluatingJavaScriptFromString:javaCall];

    return false;
}

-(bool)convFile2URI2Data:(NSString*)uri
{
    return false;
}

-(void)sendMail:(NSString*)subject body:(NSString*)body isHtml:(bool)isHtml toRecipients:(NSArray*)toRecipients
   ccRecipients:(NSArray*)ccRecipients bccRecipients:(NSArray*)bccRecipients
{
    MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
    controller.mailComposeDelegate = self;
    [controller setSubject:subject];
    [controller setToRecipients:toRecipients];
    [controller setBccRecipients:bccRecipients];
    [controller setCcRecipients:ccRecipients];
    [controller setMessageBody:body isHTML:isHtml];
    [[controller navigationBar] setTintColor:[UIColor blackColor]];

    //4.3 support
    if (controller)
    {
        if ([self respondsToSelector:@selector(presentViewController:animated:completion:)])
        {
            [self presentViewController:controller animated:YES completion:nil];
        }
        else
        {
            [self presentModalViewController:controller animated:YES];
        }
    }
    
    [controller release];
}

-(void)selectContact
{
    ABPeoplePickerNavigationController *picker = [[ABPeoplePickerNavigationController alloc] init];
    picker.peoplePickerDelegate = self;
    // Display only a person's phone, email, and birthdate
    NSArray *displayedItems = [NSArray arrayWithObjects:[NSNumber numberWithInt:kABPersonEmailProperty], nil];
    picker.displayedProperties = displayedItems;
    // Show the picker
    [self presentModalViewController:picker animated:YES];
    [picker release];
}

-(void)getFeatures
{
    
    NSLog(@"BASIC_TEMPLATE_PACK %i", [[NSUserDefaults standardUserDefaults] boolForKey:BASIC_TEMPLATE_PACK]);
    NSLog(@"PERSONAL_TEMPLATE_PACK %i", [[NSUserDefaults standardUserDefaults] boolForKey:PERSONAL_TEMPLATE_PACK]);
    
    NSString *javaCall=[[NSArray arrayWithObjects:@"registerFeatures('", ([[NSUserDefaults standardUserDefaults] boolForKey:BASIC_TEMPLATE_PACK]? @"BASIC_TEMPLATE_PACK,":@"0,"), ([[NSUserDefaults standardUserDefaults] boolForKey:PERSONAL_TEMPLATE_PACK]? @"PERSONAL_TEMPLATE_PACK":@"0"), @"')", nil] componentsJoinedByString:@""];
    [webview stringByEvaluatingJavaScriptFromString:javaCall];

    
    /*NSString *retParam = success ? @"true" : @"false";
    NSString *javaCall=[[NSArray arrayWithObjects:@"sendMailCallBack('", retParam, @"')", nil] componentsJoinedByString:@""];
    [webview stringByEvaluatingJavaScriptFromString:javaCall];*/

}

-(void)openOutURLwebview:(NSString*)urlToOpen title:(NSString*)title callback:(NSString*)callback
{
    outURLwebview *outURLView  = [[outURLwebview alloc] initWithURLandTitleandCallback:urlToOpen title:title callback:callback thisParent:self];
    
    [self.navigationController pushViewController:outURLView animated:true];

    [outURLView release];
}

-(void) openDocInteractionController:(NSString*)urlToOpen isLocalFile:(BOOL)isLocalFile
{
    NSURL *localUrl=nil;
    NSString *file = nil;

    if(!isLocalFile)
    {
        NSURL *url = [NSURL URLWithString:urlToOpen];
        file = [[url pathComponents] lastObject];
//        NSString *fileExtension = [file pathExtension];
        NSString *fileName = file;
//        NSString *fileName = [self randomImageNameGenerator:fileExtension];
        NSString *fileTempDir = NSTemporaryDirectory();
        NSString *filePath = [fileTempDir stringByAppendingString:fileName];
        
        NSData *urlData = [NSData dataWithContentsOfURL:url];
        if ([urlData writeToFile:filePath atomically:YES])
        {
            localUrl=[NSURL fileURLWithPath:filePath];
        }
        
    }
    else
    {
        localUrl=[NSURL fileURLWithPath:urlToOpen];
        file = [[urlToOpen pathComponents] lastObject];
    }
    
    self.interactionController = [UIDocumentInteractionController interactionControllerWithURL:localUrl];
    [self.interactionController setName:file];
    
    // Configure Document Interaction Controller
    [self.interactionController setDelegate:self];
    
    [self.interactionController presentOptionsMenuFromRect:CGRectMake(0.0, 0.0, 400.0, 400.0) inView:self.view animated:YES];
}

- (UIViewController *)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)controller
{
    return self;
}

- (UIView *)documentInteractionControllerViewForPreview:(UIDocumentInteractionController *)controller
{
    return self.view;
}


-(void) outURLwebviewCallback:(NSString*)callback
{
    [webview stringByEvaluatingJavaScriptFromString:callback];
}

-(void)openURLinSafari:(NSString*)url
{
    NSURL *URL = [NSURL URLWithString:url];
    [[UIApplication sharedApplication] openURL:URL];
}

-(void)openSmtpMailViewer
{
    smtpMailViewer *smtpView  = [[smtpMailViewer alloc] init];
    [self.navigationController pushViewController:smtpView animated:true];
    [smtpView release];
}

-(void)openInAppPurchases
{
    inAppViewController *inAppView  = [[inAppViewController alloc] init];
    [self.navigationController pushViewController:inAppView animated:true];
    [inAppView release];
 }

-(void)writeFile:(NSString*)relativePath data:(NSString*)data
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:relativePath];
    NSString *dirPath = [documentsDirectory stringByAppendingPathComponent:@"mw"];
    
    BOOL isDir;
    if(![[NSFileManager defaultManager] fileExistsAtPath:dirPath  isDirectory:&isDir])
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:dirPath withIntermediateDirectories:NO attributes:nil error:nil];
    }
    
    NSData *theData = [data dataUsingEncoding:NSASCIIStringEncoding];

//    NSString *javaCall=[[NSArray arrayWithObjects:@"registerFileWriteCallBack(1,\"\")", nil] componentsJoinedByString:@""];

    //If doesn't exist create mw dir.
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
    {
        NSString *javaCall=[[NSArray arrayWithObjects:@"registerFileManagerWriteResult(\"", relativePath, @"\",",([[NSFileManager defaultManager] createFileAtPath:dataPath contents:theData attributes:nil]? @"1)":@"0)"), nil] componentsJoinedByString:@""];

        [webview stringByEvaluatingJavaScriptFromString:javaCall];
//        [[NSFileManager defaultManager] createFileAtPath:dataPath contents:theData attributes:nil];
    }
    else
    {
        NSString *javaCall=[[NSArray arrayWithObjects:@"registerFileManagerWriteResult(\"", relativePath, @"\",",([data writeToFile:dataPath atomically:YES encoding:NSASCIIStringEncoding error:nil]? @"1)":@"0)"), nil] componentsJoinedByString:@""];
        
        [webview stringByEvaluatingJavaScriptFromString:javaCall];
        //[data writeToFile:dataPath atomically:YES encoding:NSASCIIStringEncoding error:nil];
    }
}

-(void)readFile:(NSString*)relativePath start:(int)start end:(int)end
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:relativePath];

    NSString *javaCall=[[NSArray arrayWithObjects:@"registerFileManagerSuccessResult(1,\"\")", nil] componentsJoinedByString:@""];

    if ([[NSFileManager defaultManager] fileExistsAtPath:dataPath])
    {
        NSString *myText = [NSString stringWithContentsOfFile:dataPath encoding:NSASCIIStringEncoding error:nil];
        myText = [myText stringByReplacingOccurrencesOfString:@"\"" withString:@"&quot;"];
        
//        int ll = ret.length;
        if(myText.length>end)
        {
            myText = [[myText substringFromIndex:start] substringToIndex:(end-start)];
            javaCall=[[NSArray arrayWithObjects:@"registerFileManagerSuccessResult(0,\"", myText, @"\")", nil] componentsJoinedByString:@""];
        }
        else
        {
            myText = [myText substringFromIndex:start];
            javaCall=[[NSArray arrayWithObjects:@"registerFileManagerSuccessResult(1,\"", myText, @"\")", nil] componentsJoinedByString:@""];
        }
    }
    
    [webview stringByEvaluatingJavaScriptFromString:javaCall];
}

-(void)openDropBoxChooser:(NSString*)cbSuccess cbCancel:(NSString*)cbCancel sid:(NSString*)sid
{
    [[DBChooser defaultChooser] openChooserForLinkType:DBChooserLinkTypePreview fromViewController:self
                                            completion:^(NSArray *results)
     {
         if ([results count])
         {
             NSString *absLink = [[results[0] link] absoluteString];
             NSString *thumb64 = [[results[0] thumbnails] valueForKey:@"64x64"];
             if(thumb64==nil)
             {
                thumb64 = [[NSString alloc] initWithString:@""];
             }
             NSString *fname =[results[0] name];
             
             NSString *javaCall=[[NSArray arrayWithObjects:cbSuccess, @"(\"", absLink, @"\",\"", thumb64, @"\",\"", fname, @"\",", sid, @")", nil] componentsJoinedByString:@""];

             [webview stringByEvaluatingJavaScriptFromString:javaCall];
         }
         else
         {
             NSString *javaCall=[[NSArray arrayWithObjects:cbCancel, @"(\"",sid, @"\")", nil] componentsJoinedByString:@""];
             [webview stringByEvaluatingJavaScriptFromString:javaCall];
         }
     }];
}

-(void)openDropBoxChooserFromAttachment
{
    [[DBChooser defaultChooser] openChooserForLinkType:DBChooserLinkTypeDirect fromViewController:self
                                            completion:^(NSArray *results)
     {
         if ([results count])
         {
             NSString *absLink = [[results[0] link] absoluteString];
             NSString *fname =[results[0] name];
             
             NSString *mimeAsBase64 = @"";
             NSString *pathToImage = @"";
             
             if([self downloadURIData:absLink uriBase64:&mimeAsBase64 pathToImage:&pathToImage])
             {
                 NSString *javaCall=[[NSArray arrayWithObjects:@"addAttachment(\"", mimeAsBase64, @"\",\"", fname,@"\")", nil] componentsJoinedByString:@""];
                 [webview stringByEvaluatingJavaScriptFromString:javaCall];
             }
         }
         else
         {
             NSString *javaCall=[[NSArray arrayWithObjects:@"addAttachment(\"\",\"\")", nil] componentsJoinedByString:@""];
             [webview stringByEvaluatingJavaScriptFromString:javaCall];
         }
     }];
}

//delegate methods.

//Photo album picker.
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    if(info)
    {
        UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
//        NSURL *imageURL = [info objectForKey:UIImagePickerControllerReferenceURL];
        
        UIImage *newResizedImage = [self imageWithImage:image scaledToSize:newImageSize];
        NSData *imageData = UIImageJPEGRepresentation(newResizedImage, 1);

        NSString *imageAsBase64 = [self encodePicsData:imageData fileExtension:@"jpg"];
        NSString *pathToImage = [self writeImageToTmp:imageData imageType:@"jpg"];
        //<##>
        
        NSString *javaCall=[[NSArray arrayWithObjects:@"selectPicsOut_iOS('", imageAsBase64, @"','", pathToImage, @"')", nil] componentsJoinedByString:@""];
        
        [webview stringByEvaluatingJavaScriptFromString:javaCall];
        
        // If we were presented with a popover, dismiss it.
        if(popoverController)
        {
            [popoverController dismissPopoverAnimated:YES];
            popoverController = nil;
        }
        else
        {
            // Dismiss the modal view controller if we weren't presented from a popover.
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverControllerRet
{
    popoverController = nil;
}

//Mail composer.
- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error
{
    switch (result) {
        case MFMailComposeResultCancelled:
            NSLog(@"cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"sent");
            break;
        case MFMailComposeResultFailed: {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error sending email!",@"Error sending email!")
                                                            message:[error localizedDescription]
                                                           delegate:nil
                                                  cancelButtonTitle:NSLocalizedString(@"Cancel",@"Cancel")
                                                  otherButtonTitles:nil];
            [alert show];
            [alert release];
            break;
        }
        default:
            break;
    }
    
    //4.3 support
    if ([self respondsToSelector:@selector(dismissViewControllerAnimated:animated:completion:)])
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else
    {
        [self dismissModalViewControllerAnimated:YES];
    }
}

//Contact picker.
- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person
{
    return YES;
}

- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person
                                property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier
{
    
    NSString *firstName = (NSString *)(ABRecordCopyValue(person, kABPersonFirstNameProperty)) ? (NSString *)(ABRecordCopyValue(person, kABPersonFirstNameProperty)) : [[NSString alloc] initWithString:@""];
    NSString *lastName = (NSString *)(ABRecordCopyValue(person, kABPersonLastNameProperty)) ? (NSString *)(ABRecordCopyValue(person, kABPersonLastNameProperty)) : [[NSString alloc] initWithString:@""];
    NSString *organization = (NSString *)(ABRecordCopyValue(person, kABPersonOrganizationProperty)) ? (NSString *)(ABRecordCopyValue(person, kABPersonOrganizationProperty)) : [[NSString alloc] initWithString:@""];
    NSString* emailAddress = (NSString *)(ABMultiValueCopyValueAtIndex(ABRecordCopyValue(person, kABPersonEmailProperty),identifier)) ? (NSString *)(ABMultiValueCopyValueAtIndex(ABRecordCopyValue(person, kABPersonEmailProperty),identifier)) :[[NSString alloc] initWithString:@""];
    NSData  *imgData = (NSData *)ABPersonCopyImageDataWithFormat(person, kABPersonImageFormatThumbnail);
    NSString *imageAsBase64 = (imgData==nil ? @"" : [self encodePicsData:imgData fileExtension:@"jpg"]);
    
    NSLog(@"firstName %@", firstName);
    NSLog(@"lastName %@", lastName);
    NSLog(@"organization %@", organization);
    NSLog(@"email %@", emailAddress);
    

    NSString *javaCall=[[NSArray arrayWithObjects:@"setContactEmail('", firstName, @"','", lastName, @"','", organization, @"','", emailAddress, @"','", imageAsBase64, @"')", nil] componentsJoinedByString:@""];
    [webview stringByEvaluatingJavaScriptFromString:javaCall];

    [firstName release];
    [lastName release];
    [organization release];
    [emailAddress release];
    [imgData release];
    
    
    if ([self respondsToSelector:@selector(dismissViewControllerAnimated:animated:completion:)])
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else
    {
        [self dismissModalViewControllerAnimated:YES];
    }
    
    return NO;
}

- (void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker;
{
    if ([self respondsToSelector:@selector(dismissViewControllerAnimated:animated:completion:)])
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else
    {
        [self dismissModalViewControllerAnimated:YES];
    }
}

@end
