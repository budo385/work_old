//
//  introView.m
//  webview_test
//
//  Created by Marko Perutovic on 11/19/12.
//  Copyright (c) 2012 Marko Perutovic. All rights reserved.
//

#import "introView.h"

@implementation introView

@synthesize animationImage;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
//        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"intro_landscape" owner:self options:nil];
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"testView" owner:self options:nil];
        UIView *mainView = [subviewArray objectAtIndex:0];
        [self addSubview:mainView];
        
        // load all the frames of our animation
        // Do any additional setup after loading the view from its nib.
        animationImage.animationImages = [NSArray arrayWithObjects:
         [UIImage imageNamed:@"1.png"],
         [UIImage imageNamed:@"2.png"],
         [UIImage imageNamed:@"3.png"],
         [UIImage imageNamed:@"4.png"],
         [UIImage imageNamed:@"5.png"],
         [UIImage imageNamed:@"6.png"],
         [UIImage imageNamed:@"7.png"],
         [UIImage imageNamed:@"8.png"],
         [UIImage imageNamed:@"9.png"],
         [UIImage imageNamed:@"10.png"],
         [UIImage imageNamed:@"11.png"],
         [UIImage imageNamed:@"12.png"], nil];
         
         // all frames will execute in 1.75 seconds
         animationImage.animationDuration = 1;
         // repeat the annimation forever
         animationImage.animationRepeatCount = 0;
        [animationImage startAnimating];
    }
    
    UIInterfaceOrientation currentOrientation = [[UIApplication sharedApplication] statusBarOrientation];
    if ((currentOrientation == UIInterfaceOrientationPortrait) || (currentOrientation == UIInterfaceOrientationPortraitUpsideDown)) {
        UIImage *back=[UIImage imageNamed:@"Default-Portrait.png"];
        [self.backgroundImage setImage:back];
    }
    else if ((currentOrientation == UIInterfaceOrientationLandscapeRight) || (currentOrientation == UIInterfaceOrientationLandscapeLeft)) {
        UIImage *back=[UIImage imageNamed:@"Default-Landscape@2x.png"];
        [self.backgroundImage setImage:back];
    }

    [self setAutoresizesSubviews:YES];
    [self setAutoresizingMask:  UIViewAutoresizingFlexibleWidth |
     UIViewAutoresizingFlexibleHeight];

/*    [self.backgroundImage setAutoresizesSubviews:YES];
    [self.backgroundImage setAutoresizingMask:
     UIViewAutoresizingFlexibleWidth |
     UIViewAutoresizingFlexibleHeight];*/

    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)dealloc {
    [animationImage release];
    [super dealloc];
}
@end
