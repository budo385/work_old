//
//  InitialUnzip.h
//  webview_test
//
//  Created by Marko Perutovic on 11/3/12.
//  Copyright (c) 2012 Marko Perutovic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InitialUnzip : NSObject

+(bool)unzipInitialVersion;
+(bool)unzipDownloadedFile:(NSString*)downloadedFilePath;

@end
