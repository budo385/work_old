//
//  smtpMailViewer.m
//  webview_test
//
//  Created by Marko Perutovic on 21/03/14.
//  Copyright (c) 2014 Marko Perutovic. All rights reserved.
//

#import "smtpMailViewer.h"

@interface smtpMailViewer ()

@end

@implementation smtpMailViewer

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//----------------------------------------------
//Table delegate.
//----------------------------------------------

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 4;
//    return [productsArray count];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"ImageOnRightCell";
    
    UILabel *mainLabel, *secondLabel;
    UIImageView *photo;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        
        UIImageView *arrowImageView = [[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"arrow_white" ofType:@"png"]]];
        cell.accessoryView = arrowImageView;
        [arrowImageView release];
        
        mainLabel = [[UILabel alloc] initWithFrame:CGRectMake(95.0, 20.0, 220.0, 40)];
//        mainLabel.tag = MAINLABEL_TAG;
        mainLabel.font = [UIFont boldSystemFontOfSize:16.0];
        mainLabel.textColor = [UIColor whiteColor];
        mainLabel.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:mainLabel];
        
        secondLabel = [[UILabel alloc] initWithFrame:CGRectMake(95.0, 50.0, 220.0, 105.0)];
//        secondLabel.tag = SECONDLABEL_TAG;
        secondLabel.font = [UIFont boldSystemFontOfSize:11.0];
        secondLabel.textColor = [UIColor whiteColor];
        secondLabel.numberOfLines=4;
        secondLabel.backgroundColor = [UIColor clearColor];
        [secondLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleRightMargin];
        [cell.contentView addSubview:secondLabel];
        
        photo = [[UIImageView alloc] initWithFrame:CGRectMake(10.0, 35.0, 75.0, 75.0)];
//        photo.tag = PHOTO_TAG;
        [cell.contentView addSubview:photo];
    }
    else
    {
//        mainLabel = (UILabel *)[cell.contentView viewWithTag:MAINLABEL_TAG];
//        secondLabel = (UILabel *)[cell.contentView viewWithTag:SECONDLABEL_TAG];
//        photo = (UIImageView *)[cell.contentView viewWithTag:PHOTO_TAG];
    }
    
/*    if([[productsArray objectAtIndex:indexPath.row] isEqualToString:BASIC_PACKAGE])
    {
        mainLabel.text = @"mailegant Premium";
        secondLabel.text = @"Additional features for mailegant users:\n1. Use more than one email address\n2. Store an unlimited number of Textlets (optionally formatted text modules)\n3. Store an unlimited number of your own email Templates";
    }
    if([[productsArray objectAtIndex:indexPath.row] isEqualToString:PERSONAL_PACKAGE])
    {
        mainLabel.text = @"Personal Templates";
        secondLabel.text = @"Define an unlimited number of your own personal e-mail templates.";
    }
    photo.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Logo_mailegance_144" ofType:@"png"]];*/
 
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    [_loadIndicator show];
//    [self buyProduct:[productsArray objectAtIndex:indexPath.row]];
}

@end
