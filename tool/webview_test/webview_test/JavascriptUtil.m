//
//  JavascriptUtil.m
//  JavascriptApiDemo
//
//  Created by 华 鲁 on 11-4-24.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import "JavascriptApi.h"
#import "JavascriptUtil.h"



@interface AlertDelegate : NSObject <UIAlertViewDelegate> {
    
}
@property (nonatomic, retain) JavascriptApi * jsApi;
@property (nonatomic, retain) NSString * callback;
@end

@implementation AlertDelegate

@synthesize jsApi;
@synthesize callback;


- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSLog(@"alert other button clicked index:%d", buttonIndex);
    if (self.callback != nil)
    {
        
        NSString *index = [NSString stringWithFormat:@"%d", buttonIndex];
        NSMutableArray *params = [NSMutableArray arrayWithObject:index];
        [self.jsApi callJSMethod:self.callback withArgs:params];
    }
}


- (void) dealloc
{
    self.jsApi = nil;
    self.callback = nil;
    [super dealloc];
}
@end


@interface HttpDelegate : NSObject {
    NSMutableData *buf;
}

@property (nonatomic, retain) JavascriptApi *jsApi;
@property (nonatomic, retain) NSString *success_cb;
@property (nonatomic, retain) NSString *error_cb;
@end

@implementation HttpDelegate
@synthesize jsApi;
@synthesize success_cb;
@synthesize error_cb;

- (id) init
{
    [super init];
    buf = [[NSMutableData alloc] initWithLength:0];
    return (self);
}

- (void) dealloc
{
    self.jsApi = nil;
    self.success_cb = nil;
    self.error_cb = nil;
    [buf release];
    [super dealloc];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [buf appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSString *msg = [NSString stringWithFormat:@"%@", error];
    NSMutableArray *params = [NSMutableArray arrayWithObject:msg];
    [self.jsApi callJSMethod:self.error_cb withArgs:params];
    [connection release];
    [self release];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *data = [[[NSString alloc]initWithData:buf encoding:NSUTF8StringEncoding]autorelease];
    NSMutableArray *params = [NSMutableArray arrayWithObject:data];
    [self.jsApi callJSMethod:self.success_cb withArgs:params];
    [connection release];
    [self release];
}
@end


@implementation JavascriptUtil


//IOS.util.log

- (void) jsCall:(JavascriptApi *)api log:(NSMutableArray *)params
{
    if ([params count] < 1) return;
    NSLog(@"%@", (NSString *)[params objectAtIndex:0]);
}

//IOS.util.alert IOS.util.confirm
- (void) jsCall:(JavascriptApi *)api alert:(NSMutableArray *)params
{
    if ([params count] < 4) return;
    NSString *title = (NSString *)[params objectAtIndex:0];
    NSString *msg = (NSString *)[params objectAtIndex:1];
    NSString *cancel = (NSString *)[params objectAtIndex:2];
    NSString *callback  = (NSString *)[params objectAtIndex:3];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:
                          msg delegate:self cancelButtonTitle:cancel otherButtonTitles:nil];
    for (int i=4; i < [params count]; i ++) {
        NSString *addButton = (NSString *)[params objectAtIndex:i];
        [alert addButtonWithTitle:addButton];
    }
    
    AlertDelegate *gate = [[AlertDelegate alloc] init];
    gate.jsApi = api;
    gate.callback = callback;
    alert.delegate = gate;
    [alert show];
    [alert release];
}

//IOS.util.http_request

- (void) jsCall:(JavascriptApi *)api http_request_create:(NSMutableArray *)params
{
    if ([params count] < 6) return;
    NSString *url_str = [params objectAtIndex:0];
    NSString *method = [params objectAtIndex:1];
    NSString *data = [params objectAtIndex:2];
    NSMutableDictionary *headers = [JavascriptApi getDictParam:(NSString *)[params objectAtIndex:3]];
    NSString *success_cb = [params objectAtIndex:4];
    NSString *error_cb = [params objectAtIndex:5];
    
    NSURL *url  = [NSURL URLWithString:url_str];
    NSMutableURLRequest *req = [[[NSMutableURLRequest alloc]initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60]autorelease];
    [req setAllHTTPHeaderFields:headers];
    [req setHTTPMethod:method];
    [req setHTTPBody:[data dataUsingEncoding:NSUTF8StringEncoding]];
    
    HttpDelegate *gate = [[HttpDelegate alloc] init];
    gate.jsApi = api;
    gate.success_cb = success_cb;
    gate.error_cb = error_cb;
    NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:req delegate:gate];
    [conn start];
}

@end
