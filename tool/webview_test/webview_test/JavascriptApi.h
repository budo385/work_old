//
//  JavascriptApi.h
//  iPhone
//
//  Created by 华 鲁 on 11-8-3.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#define JavascriptApiPath @"ios://localhost/IOS"

@interface JavascriptApi : NSObject {
    NSMutableDictionary *_delegates;
}

@property (nonatomic, assign) UIWebView *webView;

+ (NSMutableArray *) getArrayParam:(NSString *) param;
+ (NSMutableDictionary *) getDictParam:(NSString *) param;
- (JavascriptApi *) initWithWebView:(UIWebView *) webView;
- (void) setDelegate:(NSObject *)delegate forNamespace:(NSString *)ns;
- (NSString *) evalJS:(NSString *) cmd;
- (NSString *) callJSMethod:(NSString *)func withArgs:(NSMutableArray *)args;
- (BOOL) matchURL:(NSURL *) url;
@end
