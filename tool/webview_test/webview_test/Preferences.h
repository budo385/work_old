//
//  Preferences.h
//  webview_test
//
//  Created by Marko Perutovic on 9/30/12.
//  Copyright (c) 2012 Marko Perutovic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Preferences : NSObject

+(void) loadDefaultPreferences;
+(void) setAppPreferences:(int)appVersion defaultIndex:(NSString*)defaultIndex;
+(void) setInAppPurchasePreferences:(bool)basicPackage personalPackage:(bool)personalPackage;
+(NSString*) getMimeTypeForExtension:(NSString*)extension;

@end
