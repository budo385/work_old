//
//  loadIndicator.m
//  webview_test
//
//  Created by Marko Perutovic on 11/16/12.
//  Copyright (c) 2012 Marko Perutovic. All rights reserved.
//

#import "loadIndicator.h"

@implementation loadIndicator

- (id)initWithParent:(UIView*)parentView
{
    parent = parentView;
    return [self initWithFrame:parent.frame];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.backgroundColor = [UIColor clearColor];
    }
    
    return self;
}

-(void)show
{
    self.frame = parent.frame;
    UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    spinner.center = parent.center;
    [spinner startAnimating];
    
    [self addSubview:spinner];
    [spinner release];
	[parent addSubview:self];
}

-(void)hide
{
	[self removeFromSuperview];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
