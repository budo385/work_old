//
//  outURLwebview.m
//  webview_test
//
//  Created by Marko Perutovic on 28/01/14.
//  Copyright (c) 2014 Marko Perutovic. All rights reserved.
//

#import "outURLwebview.h"

@implementation outURLwebview

/*- (id) init
{
    return [self initWithURLandTitleandCallback:nil title:@"" callback:@"" addOpenInButton:NO thisParent:nil];
}*/

- (void)dealloc
{
    thisViewParent=nil;
    [super dealloc];
    [webviewURL release];
    [strCallback release];
}

- (id)initWithURLandTitleandCallback:(NSString*)urlStr title:(NSString*)title callback:(NSString*)callback thisParent:(id)thisParent
{
    self = [super init];
    if (self)
    {
        webviewURL=[[NSURL alloc] initWithString:urlStr];
        strCallback=[[NSString alloc] initWithString:callback];
        thisViewParent=thisParent;
        self.navigationItem.title=title;
    }

    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)didMoveToParentViewController:(UIViewController *)parent
{
    if (![parent isEqual:self.parentViewController])
    {
        SEL selector = @selector(outURLwebviewCallback:);
        [thisViewParent performSelector:selector withObject:strCallback];

        NSLog(@"Back pressed");
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];

    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillDisappear:animated];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    

    [_webview_outUrl  loadRequest:[NSURLRequest requestWithURL:webviewURL]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
