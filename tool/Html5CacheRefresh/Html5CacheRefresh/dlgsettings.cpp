#include "dlgsettings.h"
#include <QDebug>
#include <QMessageBox>

extern QString g_strMatchLine;

dlgSettings::dlgSettings(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
	ui.editMatchLine->setText(g_strMatchLine);
}

dlgSettings::~dlgSettings()
{

}