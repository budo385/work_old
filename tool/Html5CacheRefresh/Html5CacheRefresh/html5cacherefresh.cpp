#include "html5cacherefresh.h"
#include <QFileDialog>
#include <QDebug>
#include <QMessageBox>
#include <QTextStream>
#include <QProcess>

//global objects
QString g_strMatchLine="# Offline cache";

Html5CacheRefresh::Html5CacheRefresh(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	ui.labelCurrentVersion->clear();
	ResetGui();
}

Html5CacheRefresh::~Html5CacheRefresh()
{
}

void Html5CacheRefresh::on_btnOpenFile_clicked()
{
	// open .manifest file
	m_fileName = QFileDialog::getOpenFileName(this, 
		tr("Open File"),
		QDir::homePath(),
		tr("Cache File (*.manifest*)"));
	
	if(m_fileName.isEmpty()){
		ResetGui();
		return;
	}
		
	m_FileHandle.setFileName(m_fileName);
	if (!m_FileHandle.open(QIODevice::ReadOnly | QIODevice::Text)){
		QMessageBox::information(this,tr("Error"),tr("Could not open file for Reading."));
		return;
	}
	
	// read file content to String list
	QTextStream treamIn(&m_FileHandle);
	while (!treamIn.atEnd())
		m_lstLines << treamIn.readLine();   
	 
	m_FileHandle.close();

	// set File info
	ui.editFileName->setText(m_fileName);	
	ui.editFileName->setToolTip(m_fileName);
	ui.btnIncrement->setEnabled(true);

	// Show current version
	RefreshCurrentVersion();
}

void Html5CacheRefresh::RefreshCurrentVersion(bool bUpdate)
{
	QApplication::setOverrideCursor(Qt::WaitCursor);
	int nSize = m_lstLines.size();
	for (int i=0; i<nSize; i++) 
	{
		QString strLine = m_lstLines.at(i);
		if (strLine.indexOf(g_strMatchLine) >= 0) //predetermined format: "# Offline cache v5.4321" # Offline cache v1.4512
		{
			int nPos = strLine.indexOf("v");
			m_strCurrentVersion = strLine.mid(nPos+1); // "5.432"

			if(bUpdate){
				// increment current version
				double dVersion = m_strCurrentVersion.toDouble() + 0.0001; //predetermined format: 3 decimals
				m_strCurrentVersion = QString::number(dVersion,'f',4);
				strLine = "# Offline cache v"+m_strCurrentVersion;

				// update list
				m_lstLines[i] = strLine;

				// update file
				SaveToFile();

				// update gui
				ui.labelCurrentVersion->setText(m_strCurrentVersion);

				//RestartServer(); TOFIX
			}
			else{
				// only show current version
				ui.labelCurrentVersion->setText(m_strCurrentVersion);
			}
			QApplication::restoreOverrideCursor();
			return;
		}
	}
	QMessageBox::information(this,tr("Error"),tr("Could not find matching version line [%1]").arg(g_strMatchLine));
	return;
}

void Html5CacheRefresh::on_btnIncrement_clicked()
{
	RefreshCurrentVersion(true);
}

void Html5CacheRefresh::SaveToFile()
{
	m_FileHandle.setFileName(m_fileName);
	if (!m_FileHandle.open(QIODevice::WriteOnly | QIODevice::Truncate | QIODevice::Text)){
		QMessageBox::information(this,tr("Error"),tr("Could not open file for writing."));
		return;
	}
	QApplication::setOverrideCursor(Qt::WaitCursor);
	
	QTextStream streamOut(&m_FileHandle);
	int nSize = m_lstLines.size();
	for(int i=0; i<nSize; i++) {
		streamOut << m_lstLines[i] << "\n";
	}
	m_FileHandle.close();
	
	QApplication::restoreOverrideCursor(); 
}

void Html5CacheRefresh::DebugList()
{
	int nSize = m_lstLines.size();
	for(int i=0; i<nSize; i++)
		qDebug() << m_lstLines[i];
}

void Html5CacheRefresh::on_actionOpen_triggered()
{
	on_btnOpenFile_clicked();
}

void Html5CacheRefresh::on_actionSettings_triggered()
{
	dlgSettings dlg;
	int nRes = dlg.exec();
	//dlg.SetServicesList(nServiceIdx);
	if(nRes > 0)
	{
		/*
		//save changes from the dialog
		lstServices = dlg.GetServicesList();
		RefreshComboServices();
		*/
	}
	else
	{
		// stay still
	}
}

void Html5CacheRefresh::ResetGui()
{
	ui.labelCurrentVersion->clear();
	ui.editFileName->clear();
	ui.btnIncrement->setEnabled(false);

	if(m_FileHandle.isOpen())
		m_FileHandle.close();
}

void Html5CacheRefresh::RestartServer()
{
	QProcess p;
	QString command = "cmd.exe";
	QStringList arguments = QStringList() << "/c" << "D:\\WORK\\SOKRATES_XP\\exe\\appserver\\Debug\\_start.bat";
	p.start(command, arguments);
	p.waitForFinished();
}