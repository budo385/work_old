#ifndef HTML5CACHEREFRESH_H
#define HTML5CACHEREFRESH_H

#include <QtWidgets/QMainWindow>
#include "ui_html5cacherefresh.h"
#include <QFile>
#include "dlgsettings.h"

class Html5CacheRefresh : public QMainWindow
{
	Q_OBJECT

public:
	Html5CacheRefresh(QWidget *parent = 0);
	~Html5CacheRefresh();

private:
	Ui::Html5CacheRefreshClass ui;
	void SaveToFile();
	void RefreshCurrentVersion(bool bUpdate=false);
	void ResetGui();
	void DebugList();
	void RestartServer();

	QList<QString> m_lstLines;
	QString m_fileName;
	QFile m_FileHandle;
	QString m_strCurrentVersion;

private slots:
	void on_btnOpenFile_clicked();
	void on_btnIncrement_clicked();
	void on_actionOpen_triggered();
	void on_actionSettings_triggered();
};

#endif // HTML5CACHEREFRESH_H
