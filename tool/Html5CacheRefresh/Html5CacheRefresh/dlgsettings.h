#ifndef DLGSETTINGS_H
#define DLGSETTINGS_H

#include <QDialog>
#include "ui_dlgsettings.h"

class dlgSettings : public QDialog
{
	Q_OBJECT

public:
	dlgSettings(QWidget *parent = 0);
	~dlgSettings();


private:
	Ui::dlgSettings ui;
};

#endif // DLGSETTINGS_H

