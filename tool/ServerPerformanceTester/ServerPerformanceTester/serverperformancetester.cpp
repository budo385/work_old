#include "serverperformancetester.h"
#include "qwt-6.1.0/src/qwt_plot_curve.h"
#include "qwt-6.1.0/src/qwt_series_data.h"

ServerPerformanceTester::ServerPerformanceTester(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);

	m_myPlot = new QwtPlot(ui.widgetGraph);

	QList<int> x_vals;
	QList<int> y_vals;
	QString strX_Label;
	QString strY_Label;
	QRgb line_color_rgb;

	//init data
	x_vals.push_back(1); y_vals.push_back(1);
	x_vals.push_back(2); y_vals.push_back(2);
	x_vals.push_back(3); y_vals.push_back(3);
	x_vals.push_back(4); y_vals.push_back(5);

	strX_Label = "(thread)";
	strY_Label = "(ms)";
	line_color_rgb = QColor(255,0,0).rgb();

	//draw graph
	Plot2DGraph(x_vals, y_vals, strX_Label, strY_Label, line_color_rgb);
}

ServerPerformanceTester::~ServerPerformanceTester()
{

}

void ServerPerformanceTester::Plot2DGraph(QList<int> x_vals, QList<int> y_vals, QString strX_Label, QString strY_Label, QRgb line_color_rgb)
{
	//set graph labels
	m_myPlot->setAxisTitle(QwtPlot::xBottom, strX_Label);
	m_myPlot->setAxisTitle(QwtPlot::yLeft, strY_Label);

	//data line definitions
 	QwtPlotCurve *curve1 = new QwtPlotCurve("Curve 1");
	
	//set data line color
	QColor lineColor(line_color_rgb);
	curve1->setPen(lineColor, 2.0);

	//set data points
	QwtPointSeriesData* myData = new QwtPointSeriesData;
	QVector<QPointF>* samples = new QVector<QPointF>;

	int nCnt = x_vals.size();
	for(int i=0; i<nCnt; i++)
		samples->push_back(QPointF(x_vals[i],y_vals[i]));
	/*
	samples->push_back(QPointF(1.0,1.0));
	samples->push_back(QPointF(2.0,2.0));
	samples->push_back(QPointF(3.0,3.0));
	samples->push_back(QPointF(4.0,5.0));
	*/

	myData->setSamples(*samples);
	curve1->setData(myData);
	curve1->attach(m_myPlot);
	m_myPlot->replot();
}
