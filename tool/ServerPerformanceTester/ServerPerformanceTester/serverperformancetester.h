#ifndef SERVERPERFORMANCETESTER_H
#define SERVERPERFORMANCETESTER_H

#include <QtWidgets/QMainWindow>
#include "ui_serverperformancetester.h"
#include "qwt-6.1.0/src/qwt_plot.h"

class ServerPerformanceTester : public QMainWindow
{
	Q_OBJECT

public:
	ServerPerformanceTester(QWidget *parent = 0);
	~ServerPerformanceTester();

	void Plot2DGraph(QList<int> x_vals, QList<int> y_vals, QString strX_Label, QString strY_Label, QRgb line_color_rgb); 

protected:
	QwtPlot *m_myPlot;

private:
	Ui::ServerPerformanceTesterClass ui;
};

#endif // SERVERPERFORMANCETESTER_H
