#ifndef DATAHELPER_H
#define DATAHELPER_H


#include <QString>
#include <QDate>



/*!
	\class DataHelper
	\brief 
	\ingroup Bus_Core
*/
class DataHelper 
{

public:


	static QDate ParseDateString(QString strColValue);
	static QString ResolveFilePath(QString strPath,QString &strFileName,QString &strExtension);
	static QString EncodeFileName(QString strFileName);

	//windows registry helpers (only if _WIN32):
	static QString		Registry_GetApplicationPathFromRegistry(QString strApplicationName);
	static QString		Registry_MapApplicationNameToExeName(QString strApplicationName);
	static QStringList	Registry_EnumerateSubKeys(QString strRootKey,QString strKey);
	static bool			Registry_GetKeyValue(QString strRootKey,QString strPathToValue,QString &strValue);

	//sarch for home path of application: create dir SOKRATES Communicator if not found, store all data there....
	static QString		GetApplicationHomeDir();
	static QString		GetMyDocumentsDir();
	static bool 		CopyFolderContent(QString strSource,QString strTarget);

private:
		

};

#endif // DATAHELPER_H
