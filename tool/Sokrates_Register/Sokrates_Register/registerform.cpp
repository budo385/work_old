#include "registerform.h"
#include <QSettings>
#include <QFileDialog>
#include <QTextEdit>
#include <QMessageBox>
#include <QFile>
#include <QProcess>
#include "datahelper.h"

RegisterForm::RegisterForm(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);

	this->setWindowTitle(tr("Register your SOKRATES product"));

	m_strHtmlTag="<html><head><meta name=\"qrichtext\" content=\"1\" /></head><body style=\" white-space: pre-wrap; font-family:MS Shell Dlg 2; font-size:8.25pt; font-weight:400; font-style:normal; text-decoration:none;\"><p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600; font-style:italic;\">";
	m_strEndHtmlTag="</span></p></body></html>";


	//find information in registry, fill combo, if not found or linux then ask
	QSettings settingsTE("HKEY_LOCAL_MACHINE\\Software\\Helix Business Soft\\SOKRATES Communicator Team Edition\\Settings",	QSettings::NativeFormat);
	QSettings settingsPE("HKEY_LOCAL_MACHINE\\Software\\Helix Business Soft\\SOKRATES Communicator Personal Edition\\Settings",	QSettings::NativeFormat);
	QSettings settingsBE("HKEY_LOCAL_MACHINE\\Software\\Helix Business Soft\\SOKRATES Communicator Business Edition\\Settings",	QSettings::NativeFormat);

	QString strPath=settingsTE.value("InstallPath",QString("")).toString();
	if (!strPath.isEmpty())
	{
		ui.comboBox->addItem(tr("SOKRATES Communicator Team Edition"),strPath);
	}
	strPath=settingsPE.value("InstallPath",QString("")).toString();
	if (!strPath.isEmpty())
	{
		ui.comboBox->addItem(tr("SOKRATES Communicator Personal Edition"),strPath);
	}
	strPath=settingsBE.value("InstallPath",QString("")).toString();
	if (!strPath.isEmpty())
	{
		ui.comboBox->addItem(tr("SOKRATES Communicator Business Edition"),strPath);
	}



	if (ui.comboBox->count()==0)
	{

		//check setup.ini and find: Installation_Dir = "C:" 0,3 \SOKRATES\Communicator\"
		QFile setup_install_file(QCoreApplication::applicationDirPath()+"/setup.ini");
		if (setup_install_file.open(QIODevice::ReadOnly))
		{
			QByteArray buffer = setup_install_file.readAll();
			int nIdx=buffer.indexOf("Installation_Dir");
			nIdx=buffer.indexOf("\"",nIdx);
			int nIdxEnd=buffer.indexOf("\"",nIdx+1);
			QString strPath=buffer.mid(nIdx+1, nIdxEnd-nIdx-1);
			ui.comboBox->addItem(tr("SOKRATES Communicator"),strPath);
		}
		else
		{
			//check default path:
			//C:\soc or C:\Program Files\sok
			if(QFile::exists("C:/SOKRATES Communicator Team Edition/sokrates.exe"))
				ui.comboBox->addItem(tr("SOKRATES Communicator Team Edition"),"C:\\SOKRATES Communicator Team Edition");
			if(QFile::exists("C:/SOKRATES Communicator Personal/sokrates.exe"))
				ui.comboBox->addItem(tr("SOKRATES Communicator Personal Edition"),"C:\\SOKRATES Communicator Personal Edition");
			if(QFile::exists("C:/SOKRATES Communicator Business Edition/sokrates.exe"))
				ui.comboBox->addItem(tr("SOKRATES Communicator Business Edition"),"C:\\SOKRATES Communicator Business Edition");

			QString strPfDir=qgetenv("PROGRAMFILES");
			if (strPfDir.isEmpty())
			{
				strPfDir="Program Files";
			}

			if(QFile::exists(strPfDir+"\\SOKRATES Communicator Team Edition\\sokrates.exe"))
				ui.comboBox->addItem(tr("SOKRATES Communicator Team Edition"),strPfDir+"\\SOKRATES Communicator Team Edition");
			if(QFile::exists(strPfDir+"\\SOKRATES Communicator Personal Edition\\sokrates.exe"))
				ui.comboBox->addItem(tr("SOKRATES Communicator Personal Edition"),strPfDir+"\\SOKRATES Communicator Personal Edition");
			if(QFile::exists(strPfDir+"\\SOKRATES Communicator Business Edition\\sokrates.exe"))
				ui.comboBox->addItem(tr("SOKRATES Communicator Business Edition"),strPfDir+"\\SOKRATES Communicator Business Edition");


			if (ui.comboBox->count()==0)
				on_btnFindApp_clicked();
		}



	}
	else
	{
		ui.comboBox->setCurrentIndex(0);
	}

}

RegisterForm::~RegisterForm()
{

}

bool RegisterForm::IsSingleInstallation()
{
	if (ui.comboBox->count()==1)
		return true;
	else
		return false;
}
bool RegisterForm::Register()
{
	return on_btnOK_clicked();
}

bool RegisterForm::on_btnOK_clicked()
{

	QTextEdit edit;
	edit.setHtml(ui.labelPath->text());
	QString strPath=edit.toPlainText();

	if (strPath.isEmpty())
	{
		QMessageBox::information(this,tr("Warning"),tr("Please select installation path before registering product!"));
		return false;
	}

	//determine setings dir, if not, create
	QDir dirApp(strPath);
	QString strAppName=dirApp.dirName();
	QCoreApplication::setApplicationName(strAppName);
	QString strSettingsDir=DataHelper::GetApplicationHomeDir()+"/settings";
	QDir setDir(strSettingsDir);
	if (!setDir.exists())
	{
		QString strAppDir=DataHelper::GetApplicationHomeDir();
		QDir bcpDirCreate(strAppDir);
		if(!bcpDirCreate.mkdir("settings"))
		{
			QMessageBox::information(this,tr("Warning"),tr("Failed to copy settings to the: ")+strSettingsDir);
			return false;
		}
	}

	//copy files there:
	QFile file1(QCoreApplication::applicationDirPath()+"/network_connections.cfg");
	if (file1.exists())
	{
		QFile target(strSettingsDir+"/network_connections.cfg");
		if (target.exists())target.remove();
		file1.copy(strSettingsDir+"/network_connections.cfg");
	}
	QFile file2(QCoreApplication::applicationDirPath()+"/sokrates.key");
	if (file2.exists())
	{
		QFile target(strSettingsDir+"/sokrates.key");
		if (target.exists())target.remove();
		file2.copy(strSettingsDir+"/sokrates.key");
	}


	//INI: if thick client->override key file path:
	//INI: set FLAG: Order Ask Form=0
	QFile ini(strSettingsDir+"/client.ini");
	if (ini.exists())
	{
		if(ini.open(QIODevice::ReadWrite | QIODevice::Text))
		{
			QString strContent=ini.readAll();
			QStringList strLines=strContent.split("\n");
			bool bLine=false;
			int nSize=strLines.size();
			for(int i=nSize-1;i>=0;--i)
			{
				if (strLines.at(i).indexOf("KeyFile Path")==0)
				{
					strLines.removeAt(i);
					bLine=true;
				}
				if (strLines.at(i).indexOf("Order Ask Form")==0)
				{
					strLines.removeAt(i);
					bLine=true;
				}
			}
			ini.close();
			if (bLine)
			{
				strContent=strLines.join("\n");
				ini.remove();
				ini.open(QIODevice::WriteOnly | QIODevice::Text);
				ini.write(strContent.toLatin1());
				ini.close();
			}
		}
	}

	QMessageBox::information(this,tr("Information"),tr("SOKRATES product successfully registered! SOKRATES will start after closing this dialog!"));

	//start exec:

	strPath="\""+strPath+"/sokrates.exe\"";
	strPath=QDir::toNativeSeparators(strPath);
	QProcess::startDetached(strPath);

	done(1);
	return true;
}
void RegisterForm::on_btnFindApp_clicked()
{
	//open dir selector:

	QString strStartDir=QCoreApplication::applicationDirPath();

	//open file dialog:
	QString strFile = QFileDialog::getExistingDirectory(
		NULL,
		QT_TR_NOOP("Installation Directory"),
		strStartDir);

	if(!strFile.isEmpty())
	{
		ui.labelPath->setText(m_strHtmlTag+strFile+m_strEndHtmlTag);
	}


}
void RegisterForm::on_btnCancel_clicked()
{
	done(0);
}



void RegisterForm::on_comboBox_currentIndexChanged(int nIndex)
{
	ui.labelPath->setText(m_strHtmlTag+ui.comboBox->itemData(nIndex).toString()+m_strEndHtmlTag);
}