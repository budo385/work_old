#include "datahelper.h"
#include <QCoreApplication>
#include <QFileInfo>
#include <QFileInfoList>
#include <QDir>


#ifdef _WIN32
	#include <windows.h>
	#include <shlobj.h>
	#define MAX_KEY_LENGTH 255
	#define MAX_VALUE_LENGTH 16383

	static HKEY MapString2HKEY(QString strRootKey)
	{
		HKEY hKeyRoot;
		QStringList lstKeys;

		if (strRootKey=="HKEY_CLASSES_ROOT")
		{
			hKeyRoot=HKEY_CLASSES_ROOT;
		}
		else if (strRootKey=="HKEY_CURRENT_USER")
		{
			hKeyRoot=HKEY_CURRENT_USER;
		}
		else if (strRootKey=="HKEY_LOCAL_MACHINE")
		{
			hKeyRoot=HKEY_LOCAL_MACHINE;
		}

		return hKeyRoot;
	}
#endif





//parses inputed user string: this is specific for us:
//takes local settings, DDMMYYY and american YYYY.MM.DD
QDate DataHelper::ParseDateString(QString strColValue)
{
	//try to convert:
	QDate datTemp=QDate::fromString(strColValue,"dd.MM.yyyy");
	if(datTemp.isNull())
		datTemp=QDate::fromString(strColValue,"d.M.yyyy");
	if(datTemp.isNull())
		datTemp=QDate::fromString(strColValue,Qt::ISODate);
	if(datTemp.isNull())
		datTemp=QDate::fromString(strColValue,Qt::LocaleDate);
	if(datTemp.isNull())
	{
		//add 100 years for >2000
		datTemp=QDate::fromString(strColValue,"d.M.yy");
		if(datTemp.isNull()) //exit if this also failes
			return datTemp;
		QDate tmp=datTemp;
		if(tmp.year()<2000)
			datTemp=datTemp.addYears(100);
	}

	return datTemp;
}






//checks if file exec, if sym link points to exec, extract real target, or original path
//return name without path/extension
//return extension, if executable, returns "exe".
//return extension, if executable, returns "dir".
QString DataHelper::ResolveFilePath(QString strPath,QString &strFileName,QString &strExtension)
{
	QFileInfo fileInfo(strPath);
	if (fileInfo.isSymLink())
	{
		strFileName=fileInfo.baseName();
		QString strFilePath=fileInfo.symLinkTarget();
		QFileInfo fileInfoExe(strFilePath);
		if (fileInfoExe.isExecutable())
		{
			
			strExtension="exe";
			//return strPath;				//return symlink
			return strFilePath;
		}
		else
		{
			strExtension=fileInfoExe.suffix();		//not link, target
			return strFilePath;						//not link, target
		}
	}
	else if (fileInfo.isDir())
	{
		strFileName=fileInfo.baseName();
		strExtension="dir";
		return strPath;
	}
	else if (fileInfo.isExecutable())
	{
		strFileName=fileInfo.baseName();
		strExtension="exe";
		return strPath;
	}
	else //not exec: try to figure out extension
	{
		strFileName=fileInfo.baseName();
		strExtension=fileInfo.suffix();
		return strPath;
	}

}






//strApplicationName can be shorten or full exe name (no path)
QString DataHelper::Registry_GetApplicationPathFromRegistry(QString strApplicationName)
{
	strApplicationName=Registry_MapApplicationNameToExeName(strApplicationName);
	QStringList lstKeys=Registry_EnumerateSubKeys("HKEY_CLASSES_ROOT","Applications");

	QString strApplicationKey;
	int nSize=lstKeys.size();
	for(int i=0;i<nSize;++i)
	{
		if (lstKeys.at(i).toUpper().indexOf(strApplicationName.toUpper(),Qt::CaseInsensitive)!=-1)
		{
			strApplicationKey=lstKeys.at(i);
			break;
		}
	}

	if (strApplicationKey.isEmpty())
		return "";


	//FETCH REGISTRY VALUE: for "open", "read", "execute" values
	QString strValue;
	QString strKey;
	strKey="Applications\\"+strApplicationKey;

	if(!Registry_GetKeyValue("HKEY_CLASSES_ROOT",strKey+"\\shell\\open\\command",strValue))
		if(!Registry_GetKeyValue("HKEY_CLASSES_ROOT",strKey+"\\shell\\read\\command",strValue))
			if(!Registry_GetKeyValue("HKEY_CLASSES_ROOT",strKey+"\\shell\\edit\\command",strValue))
				if(!Registry_GetKeyValue("HKEY_CLASSES_ROOT",strKey+"\\shell\\execute\\command",strValue))

	if (strValue.isEmpty())
		return "";

	strValue=strValue.trimmed();

	//extract path:
	int index=strValue.toUpper().indexOf(".EXE");
	if (index==-1) 	return "";
	strValue=strValue.left(index+4);
	strValue.replace("\"",""); //destry quotes if exists
	return strValue;
}


//try to set fully  name, if not, return same...brb....this is stupid
QString DataHelper::Registry_MapApplicationNameToExeName(QString strApplicationName)
{
	strApplicationName=strApplicationName.toUpper();

	//map half names to fully quilifed names:
	if (strApplicationName.indexOf("WORDPAD",Qt::CaseInsensitive)!=-1)
	{
		strApplicationName="WORDPAD.EXE";
	}
	//if (strApplicationName.indexOf("WORD",Qt::CaseInsensitive)!=-1)
	//{
	//	strApplicationName="WINWORD.EXE";
	//}
	else if (strApplicationName.indexOf("EXCEL",Qt::CaseInsensitive)!=-1)
	{
		strApplicationName="EXCEL.EXE";
	}
	else if (strApplicationName.indexOf("POWERPOINT",Qt::CaseInsensitive)!=-1)
	{
		strApplicationName="POWERPNT.EXE";
	}
	else if (strApplicationName.indexOf("FIREFOX",Qt::CaseInsensitive)!=-1)
	{
		strApplicationName="FIREFOX.EXE";
	}
	else if (strApplicationName.indexOf("FRONTPAGE",Qt::CaseInsensitive)!=-1)
	{
		strApplicationName="FRONTPG.EXE";
	}
	else if (strApplicationName.indexOf("ACROBAT",Qt::CaseInsensitive)!=-1)
	{
		strApplicationName="AcroRD32.exe";
	}
	else if (strApplicationName.indexOf("WINZIP",Qt::CaseInsensitive)!=-1)
	{
		strApplicationName="Winzip32.exe";
	}
	else if (strApplicationName.indexOf("IRFAN",Qt::CaseInsensitive)!=-1)
	{
		strApplicationName="i_view32.exe";
	}
	else if (strApplicationName.indexOf("PAINT",Qt::CaseInsensitive)!=-1)
	{
		strApplicationName="mspaint.exe";
	}

	return strApplicationName;

}

//strRootKey is HKEY_CLASSES_ROOT,HKEY_CURRENT_USER or other: 
//strKey is in format: "mailto\\shell\\open\\command"

QStringList DataHelper::Registry_EnumerateSubKeys(QString strRootKey,QString strKey)
{
	QStringList lstKeys;

#ifdef _WIN32

	HKEY hKey;
	HKEY hKeyRoot=MapString2HKEY(strRootKey);

	//find strKey inside root:
	strKey=strKey.toLatin1();
	if( RegOpenKeyEx( hKeyRoot,(LPCWSTR)strKey.utf16(), 0, KEY_ENUMERATE_SUB_KEYS, &hKey ) != ERROR_SUCCESS )
		return lstKeys;

	// Enumerate the subkeys, until RegEnumKeyEx fails.	
	TCHAR	buf[MAX_KEY_LENGTH];				// buffer for subkey name
	DWORD	bufferSize; //=MAX_KEY_LENGTH*sizeof(TCHAR);		// size of name string 
	//FILETIME ftLastWriteTime;      // last write time 
	DWORD  i=0,retCode =0; //dummy index

	do 
	{
		bufferSize=MAX_KEY_LENGTH;
		retCode=RegEnumKeyEx(hKey, i,
			buf, 
			&bufferSize, 
			NULL, 
			NULL, 
			NULL, 
			NULL);
		if (retCode==ERROR_SUCCESS)
		{
			QString strKey= QString::fromRawData((QChar*)buf, bufferSize)+"\0";
			lstKeys<<strKey;
			i++;
		}
	} while(retCode==ERROR_SUCCESS);


	RegCloseKey(hKey);
	return lstKeys;
#else

	return lstKeys;
#endif
}

//get default key value
bool DataHelper::Registry_GetKeyValue(QString strRootKey,QString strKey,QString &strValue)
{

#ifdef _WIN32
	HKEY rootKey=MapString2HKEY(strRootKey);
	HKEY hKey;
	TCHAR buf[MAX_VALUE_LENGTH];
	LONG dwBufLen=MAX_VALUE_LENGTH; //*sizeof(TCHAR);
	LONG lRet;

	/*
	lRet = RegOpenKeyEx( rootKey, TEXT(strKey.toAscii()), 0, KEY_QUERY_VALUE, &hKey );
	if( lRet != ERROR_SUCCESS )
		return false;
	*/
	strKey = strKey.toLatin1();
	//char * p=byteK.data();
	//HKEY_CLASSES_ROOT\Applications\WINWORD.EXE\shell\edit\command
	TCHAR subKey[] = L"Applications\\WINWORD.EXE\\shell\\edit\\command";  

	//lRet = RegQueryValue( rootKey, subKey,	buf, &dwBufLen); //get default or first value of key
	lRet = RegQueryValue( rootKey, (LPCWSTR)strKey.utf16(),	buf, &dwBufLen); //get default or first value of key
	//RegCloseKey( hKey );
	if( (lRet != ERROR_SUCCESS) || (dwBufLen > MAX_VALUE_LENGTH*sizeof(TCHAR)) )
		return false;
	
	strValue= QString::fromRawData((QChar*)buf, dwBufLen);
#endif
	return true;

}


//get home  dir:
//Windows: C:\My Documents And Seetings\User\Application Data\Sokrates Communicator
QString DataHelper::GetApplicationHomeDir()
{
#ifdef _WIN32
		QString strAppDir=qgetenv("APPDATA");
		QDir testDir(strAppDir);
		if (!testDir.exists())
		{
			strAppDir=QDir::homePath();
			testDir.setPath(strAppDir);
		}
		if (!testDir.exists())
		{
			strAppDir=QCoreApplication::applicationDirPath();
			testDir.setPath(strAppDir);
		}

		Q_ASSERT(QCoreApplication::applicationName()!=""); //must be set

		//test if our app dir exists
		if(!testDir.exists(QCoreApplication::applicationName()))
			if(!testDir.mkdir(QCoreApplication::applicationName()))
				return "";

		return testDir.path()+"/"+QCoreApplication::applicationName();

#else
		strAppDir=QDir::homePath();
		if (!testDir.exists())
		{
			strAppDir=QCoreApplication::applicationDirPath();
			testDir.setPath(strAppDir);
		}
		//test if our app dir exists
		if(!testDir.exists(QCoreApplication::applicationName()))
			if(!testDir.mkdir(QCoreApplication::applicationName()))
				return "";

		return testDir.path()+"/"+QCoreApplication::applicationName();

#endif

	return "";
	
}

//on windows: my docs, on linux same as applicationhomedir
//Windows: C:\My Documents And Seetings\User\My Documents\Sokrates Communicator
QString	DataHelper::GetMyDocumentsDir()
{

#ifdef _WIN32


	//get MY DOCUMENTS:
	QString strAppDir;
	TCHAR buf[MAX_VALUE_LENGTH];
	if(SHGetSpecialFolderPath(NULL,buf,CSIDL_PERSONAL,true))
		strAppDir=QString::fromUtf16((ushort*)buf);//, wsstrlen(buf));

	QDir testDir(strAppDir);
	if (!testDir.exists())
		return GetApplicationHomeDir();
	else
	{
		//test if our app dir exists
		if(!testDir.exists(QCoreApplication::applicationName()))
			if(!testDir.mkdir(QCoreApplication::applicationName()))
				return "";

		return testDir.path()+"/"+QCoreApplication::applicationName(); //return doc path
	}
#else
	return GetApplicationHomeDir();
#endif


	return "";
}




QString DataHelper::EncodeFileName(QString strFileName)
{

	//remove whites:
	strFileName=strFileName.simplified();

	//remove punct (:,.)
	int nSize=strFileName.size();
	for(int i=0;i<nSize;++i)
	{
		if (strFileName.at(i).isPunct())
			strFileName[i]='_';
	}
	return strFileName;

}


//copy all files from 1 to 2 (not overwrite)
bool DataHelper::CopyFolderContent(QString strSource,QString strTarget)
{
	//read all files from backup:
	QDir dirSource(strSource);
	if (!dirSource.exists())
	{
		return false;
	}

	QDir dirTarget(strSource);
	if (!dirTarget.exists())
	{
		return false;
	}


	//Algorithm finds first free slot: it will iterate 9999 times (max) before exiting...
	QFileInfoList	lstFiles=dirSource.entryInfoList();
	int nSize=lstFiles.size();
	for(int i=0;i<nSize;++i)
	{
		if (lstFiles.at(i).isFile())
		{
			QString fileName=lstFiles.at(i).fileName();
			QFile file(lstFiles.at(i).filePath());
			//if(!file.copy(strTarget+"/"+fileName))
			file.copy(strTarget+"/"+fileName);
				//return false;
		}
	}

	return true;
}