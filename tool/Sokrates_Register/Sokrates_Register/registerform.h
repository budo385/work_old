#ifndef REGISTERFORM_H
#define REGISTERFORM_H

#include <QDialog>
#include "ui_registerform.h"

class RegisterForm : public QDialog
{
	Q_OBJECT

public:
	RegisterForm(QWidget *parent = 0);
	~RegisterForm();

	bool IsSingleInstallation();
	bool Register();

private slots:
	bool on_btnOK_clicked();
	void on_btnFindApp_clicked();
	void on_btnCancel_clicked();
	void on_comboBox_currentIndexChanged(int);

private:
	Ui::RegisterFormClass ui;

	QString m_strHtmlTag;
	QString m_strEndHtmlTag;
};

#endif // REGISTERFORM_H
