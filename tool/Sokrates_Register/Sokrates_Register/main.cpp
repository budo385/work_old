#include <QApplication>
#include "registerform.h"
#include <QTimer>



int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	RegisterForm w;
	a.connect(&a, SIGNAL(lastWindowClosed()), &a, SLOT(quit()));


	if(w.IsSingleInstallation())
		if(w.Register())
			return 1;

	QTimer::singleShot(0, &w, SLOT(exec()));
	return a.exec();
}
