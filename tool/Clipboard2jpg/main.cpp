#include <QtCore/QCoreApplication>
#include <QClipboard>
#include <QApplication>
#include <QImage>
#include <qDebug>
#include <QFileInfo>
#include <QDir>
#include <QVariant>

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);

	if(argc < 2){
		qDebug() << "ERROR: File path not specified.";
		return -1;
	}

	if(QApplication::clipboard()->image().isNull()){
		qDebug() << "ERROR: No image on clipboard.";
		return -2;		
	}

	//test for command line flags "-b64", file name
	QString strFile;
	bool bBase64 = false;
	if(QString(argv[1]) == QString("-b64"))
	{
		bBase64 = true;

		if(argc < 3){
			qDebug() << "ERROR: File path not specified.";
			return -4;
		}
		else
			strFile = argv[2];
	}
	else
		strFile = argv[1];

	QString strOutFile;

	if(bBase64)
	{
		// save the image contents into the temp file
		QString strName = "SokratesTestImage.jpg";
		QFileInfo attInfo(strName);
		QString strTmpPath;
		//generate unique temp name
		int nTry = 0; bool bOK = false;
		while(nTry < 500 && !bOK){
			strTmpPath = QDir::tempPath();
			strTmpPath += "/";
			strTmpPath += attInfo.completeBaseName();
			strTmpPath += QVariant(nTry).toString();
			strTmpPath += ".";
			strTmpPath += attInfo.completeSuffix();
			nTry ++;
			bOK = !QFile::exists(strTmpPath);
		}
		if(!bOK){
			Q_ASSERT(false);	//could not create unused temp path
			qDebug() << "ERROR: Failed to calculate temporay file name. Clean your temp folder.";
			return -7;
		}
		strOutFile = strTmpPath;
	}
	else
		strOutFile = strFile;

	if(!QApplication::clipboard()->image().save(strOutFile, "JPG"))
	{
		qDebug() << "ERROR: Failed to save the image to file.";
		return -3;		
	}

	if(bBase64)
	{
		//convert the temporary jpg file to the base64
		QFile file(strOutFile);
		if(!file.open(QIODevice::ReadOnly))
		{
			qDebug() << "ERROR: Failed to open temporary .jpg file.";
			return -5;
		}
		QByteArray datPicture = file.readAll().toBase64();
		file.close();
		QFile::remove(strOutFile);

		//save to output
		QFile fileOut(strFile);
		if(!fileOut.open(QIODevice::WriteOnly))
		{
			qDebug() << "ERROR: Failed to open output file.";
			return -6;
		}
		fileOut.write(datPicture);
		fileOut.close();
	}

	return 0; //a.exec();
}
