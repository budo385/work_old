#ifndef APPCONFIGMAIN_H
#define APPCONFIGMAIN_H

#include <QMainWindow>
#include <QAction>
#include "common/common/status.h"
#include "applicationserverconfigurator.h"
#include "db_core/db_core/dbconnsettings.h"
#include "ui_appconfigmain.h"

class AppConfigMain : public QMainWindow
{
	Q_OBJECT

public:
	AppConfigMain(QWidget *parent = 0);
	~AppConfigMain();

private slots:
	void on_btnConnect_clicked();
	void on_btnEditSettings_clicked();
	void on_btnCreate_clicked();
	void on_btnOK_clicked();
	void on_btnCancel_clicked();
	void on_btnInstall_clicked();
	void on_cmbDbType2_currentIndexChanged ( int index );
	void on_btnSelectDatabasePath_clicked();
	void on_btnFlush_clicked();

	void OnMenu_About();

protected:
	void	timerEvent(QTimerEvent *event);

private:
	void ReloadSettings();
	void RefreshStatus();
	void SetupMenuActions();
	void CheckForDataBaseService(Status &err);
	void SendReloadWebPagesCommandToServer(Status &err);

	ApplicationServerConfigurator AppSrv;
	QString m_strDbConnectionsettingsPath;
	QList<DbConnectionSettings> m_lstConnections;

	Ui::AppConfigMainClass ui;
	int m_nTimerID;
	bool m_bStarting;
	
	QAction*	m_pMenuFile;
	QAction*	m_pMenuHelp;

	QAction*	m_pActQuit;
	QAction*	m_pActFUI_About;
};

#endif // APPCONFIGMAIN_H
