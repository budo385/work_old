#include "appconfigmain.h"
#include "gui_core/gui_core/aboutwidget.h"
//#include "applicationserverconfig.h"
#include "gui_core/gui_core/macros.h"
#include "db_core/db_core/dbconnectionslist.h"
#include "db/db/dbconnectionsetup.h"
#include "common/common/config.h"
#include "common/common/statuscodeset.h"
#include "db_core/db_core/db_drivers.h"
#include <QInputDialog>
#include "gui_core/gui_core/dbconnsettingsgui.h"
#include "bus_core/bus_core/dbactualizator.h"
#include <QDesktopServices>
#include <QFileDialog>
#include "trans/trans/resthttpclient.h"

#define HTML_START_F1	"<html><head><meta name=\"qrichtext\" content=\"1\" /></head><body style=\" white-space: pre-wrap; font-family:MS Shell Dlg 2; font-size:10pt; font-weight:bold; font-style:normal; text-decoration:none;\"><p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:10pt; font-weight:bold; font-style:italic;\">"
#define HTML_END_F1		"</span></p></body></html>"

AppConfigMain::AppConfigMain(QWidget *parent)
: QMainWindow(parent),m_nTimerID(-1),m_bStarting(false)
{
	ui.setupUi(this);

	this->statusBar()->setSizeGripEnabled(false);
	this->setWindowFlags(Qt::Window | Qt::MSWindowsFixedSizeDialogHint);

	setAttribute(Qt::WA_QuitOnClose);
	//setAttribute(Qt::WA_DeleteOnClose);

	//this->setAttribute(Qt::WA_DeleteOnClose, true);
	SetupMenuActions();

	QString strLineEdit="QLineEdit:disabled { color: black } QCheckBox:disabled { color: black } QComboBox:disabled { color: black } QRadioButton:disabled { color: black } ";
	this->setStyleSheet(strLineEdit);

	ui.ckbAutoStart->setChecked(true);

	//load from ini:
	Status err;
	AppSrv.LoadINIFile(err);
	_CHK_ERR_NO_RET(err);

	if (err.IsOK())
	{
		ui.txtName->setText(AppSrv.GetINIFile()->m_strDescriptor);
		ui.txtPort->setText(QVariant(AppSrv.GetINIFile()->m_nAppServerPort).toString());
		ui.txtInterface->setText(AppSrv.GetINIFile()->m_strAppServerIPAddress);
		ui.ckbSSL->setChecked(AppSrv.GetINIFile()->m_nSSLMode);
		ui.ckbEnableWeb->setChecked(AppSrv.GetINIFile()->m_nWWWEnable && AppSrv.GetINIFile()->m_nRestEnable);
		ui.ckbAutoStart->setChecked(AppSrv.GetINIFile()->m_nAutoStartService);
	}

	//if empty-> first time install:
	if (AppSrv.GetINIFile()->m_strDescriptor.isEmpty())
	{
		ui.ckbSSL->setChecked(true);
		ui.ckbAutoStart->setChecked(true);
		ui.ckbEnableWeb->setChecked(true);
		ui.txtName->setText("appserver");
	}


	if (!AppSrv.IsRunning(err))
	{
		AppSrv.GetINIFile()->m_nLastErrorCode=0;
		AppSrv.GetINIFile()->m_strLastErrorText="";
		AppSrv.GetINIFile()->Save();
	}
	else
	{
		m_nTimerID=startTimer(5000); //5seconds
	}
	
	RefreshStatus();


	//DB CREATE:
	ui.cmbDbType->addItem(tr("FireBird"),DBTYPE_FIREBIRD);
	ui.cmbDbType->addItem(tr("MySQL"),DBTYPE_MYSQL);

	ui.cmbDbType2->blockSignals(true);
	ui.cmbDbType2->addItem(tr("FireBird"),DBTYPE_FIREBIRD);
	ui.cmbDbType2->addItem(tr("MySQL"),DBTYPE_MYSQL);
	ui.cmbDbType2->blockSignals(false);


	ui.labelSelect->setVisible(false);
	ui.txtSettingsPath->setVisible(false);
	ui.btnSelectSettings->setVisible(false);


	//DB CONN:
	ui.btnSelectDatabasePath->setIcon(QIcon(":SAP_Select.png"));
	ui.btnSelectDatabasePath->setToolTip(tr("Select Database Path or leave empty to use default!"));

	ui.btnSelectSettings->setIcon(QIcon(":SAP_Select.png"));
	ui.btnSelectSettings->setToolTip(tr("Select Database Settings!"));
	ui.btnSelectSettings->setEnabled(false);

	ui.btnEditSettings->setIcon(QIcon(":SAP_Modify.png"));
	ui.btnEditSettings->setToolTip(tr("Edit Database Settings!"));
	m_strDbConnectionsettingsPath=QCoreApplication::applicationDirPath()+"/settings/database_connections.cfg";
	//ui.txtSettingsPath->setReadOnly(true);

	QFile file(m_strDbConnectionsettingsPath);
	if (file.exists())
	{
		//reload settings, set to last connection:
		if (!m_strDbConnectionsettingsPath.isEmpty())
		{
			ui.txtSettingsPath->setText(m_strDbConnectionsettingsPath);
			ReloadSettings();
			if (!AppSrv.GetINIFile()->m_strDBConnectionName.isEmpty())
			{
				int nSize=m_lstConnections.size();
				for(int i=0;i<nSize;++i)
				{
					if (m_lstConnections.at(i).m_strCustomName==AppSrv.GetINIFile()->m_strDBConnectionName)
					{
						ui.cboConnection->setCurrentIndex(i);
						break;
					}
				}
			}
		}
		else
		{
			ui.cboConnection->setEnabled(false);
			ui.btnEditSettings->setEnabled(false);
		}
	}
	else
	{
		ui.cboConnection->setEnabled(false);
		ui.btnEditSettings->setEnabled(false);
	}


	//tooltips:
	ui.btnInstall->setToolTip(tr("Install Database Manager and make utilities available in PATH"));
	ui.txtName->setToolTip(tr("Descriptor is only used if there is more then one application server instances on same machine, if not leave as is. Do not use special characters in descriptor name!"));
	ui.txtPort->setToolTip(tr("Port on which application server will listen"));
	ui.txtInterface->setToolTip(tr("If host machine have more then one NIC, set IP address of NIC on which application server will listen, if only one NIC is present leave as is (0.0.0.0)"));
	ui.ckbSSL->setToolTip(tr("If SSL enabled, application server will use predefined certificate and private key (in settings subdirectory). To use own certificate, replace them in settings subdirectory"));
	ui.ckbEnableWeb->setToolTip(tr("Enable REST and WWW service. HTML pages are usually located inside /webservices directory."));
	ui.txtDatabaseName->setToolTip(tr("Custom Database name. Do not use special characters in database name"));
	ui.cboConnection->setToolTip(tr("This database connection will be used by application server"));

	//refresh DB status:
	on_cmbDbType2_currentIndexChanged(0);

	QString strTitle = QString("SOKRATES")+QChar(174)+QString(tr(" Application Server Configuration Manager"));
	setWindowTitle(strTitle);
}



AppConfigMain::~AppConfigMain()
{
	Status err;
	if (!AppSrv.IsRunning(err) && AppSrv.GetINIFile())
	{
		AppSrv.GetINIFile()->m_strDescriptor=ui.txtName->text();
		AppSrv.GetINIFile()->m_nAppServerPort=ui.txtPort->text().toInt();
		AppSrv.GetINIFile()->m_strAppServerIPAddress=ui.txtInterface->text();
		AppSrv.GetINIFile()->m_nSSLMode=ui.ckbSSL->isChecked() ? 1 : 0;
		if (ui.ckbEnableWeb->isChecked())
		{
			AppSrv.GetINIFile()->m_nRestEnable=1;
			AppSrv.GetINIFile()->m_nWWWEnable=1;
		}
		else
		{
			AppSrv.GetINIFile()->m_nRestEnable=0;
			AppSrv.GetINIFile()->m_nWWWEnable=0;
		}
		AppSrv.GetINIFile()->m_strDBConnectionName=ui.cboConnection->currentText();
		AppSrv.GetINIFile()->Save();
	}
}




void AppConfigMain::ReloadSettings()
{
	//read all from file, put in combo:
	DbConnectionsList objData;
	if(!objData.Load(m_strDbConnectionsettingsPath, MASTER_PASSWORD))
	{
		QMessageBox::critical(this,tr("Error"),tr("Can not open database setting file!"));
		ui.cboConnection->setEnabled(false);
		ui.btnEditSettings->setEnabled(false);
		return;
	}

	m_lstConnections=objData.m_lstConnections;
	ui.cboConnection->clear();
	int nCnt = m_lstConnections.size();
	for(int i=0; i<nCnt; i++)
	{
		ui.cboConnection->addItem(m_lstConnections[i].m_strCustomName);
	}

	ui.cboConnection->setEnabled(true);
	ui.btnEditSettings->setEnabled(true);

}

void AppConfigMain::on_btnEditSettings_clicked()
{
	DbConnectionsList objData;
	if(!objData.Load(m_strDbConnectionsettingsPath, MASTER_PASSWORD))
	{
		QMessageBox::critical(this,tr("Error"),tr("Can not open database setting file!"));
		return;
	}

	//ask for password if the one was set
	if(objData.IsUserPasswordSet())
	{
		bool ok;
		QString text = QInputDialog::getText(NULL, "User Settings Password",
			"Password:", QLineEdit::Password,
			"", &ok);
		if (ok)	//empty pass allowed
		{
			if(!objData.MatchUserPassword(text)){
				QMessageBox::information(NULL, "Error", "Password does not match!");
				return;
			}
		}
		else
			return;
	}

	// Now display the GUI dialog
	DbConnSettingsGUI dlg;
	dlg.SetConnections(objData.m_lstConnections, ui.cboConnection->currentIndex());
	dlg.SetPassword(objData.IsUserPasswordSet(), 
		objData.GetPasswordHash());

	//display dialog	
	int nRes = dlg.exec();
	if(nRes > 0)
	{
		//save changes from the dialog
		objData.m_lstConnections = dlg.GetConnections();
		int nCurrentSelection=dlg.GetCurSelection();

		bool bUsePass = false;
		QString strHash;
		dlg.GetPassword(bUsePass, strHash);

		if(bUsePass)
			objData.SetUserPasswordHash(strHash);
		else
			objData.ClearUserPassword();

		objData.Save(m_strDbConnectionsettingsPath, MASTER_PASSWORD);

		ReloadSettings();
		ui.cboConnection->setCurrentIndex(nCurrentSelection);

	}


}

void AppConfigMain::on_btnConnect_clicked()
{
	int nCurrentSelection=ui.cboConnection->currentIndex();
	if (nCurrentSelection==-1)
		return;
	DbConnectionSettings settings=m_lstConnections.at(nCurrentSelection);

	DbActualizator act;
	act.Initialize(settings);
	Status err;
	act.InitConnection(err,QCoreApplication::applicationDirPath()+"/backup");

	if (err.IsOK())
	{
		QMessageBox::information(this,tr("Success"),tr("Connection successfully tested!"));
	}
	else
		_CHK_ERR_NO_RET(err);

	act.DisConnect(err);

}



//create db:
void AppConfigMain::on_btnCreate_clicked()
{
	//ask for username, password:

	QString strDb=ui.cmbDbType->itemData(ui.cmbDbType->currentIndex()).toString();

	if (ui.txtDatabaseName->text().isEmpty())
	{
		QMessageBox::warning(this,tr("Warning"),tr("Database name is mandatory"));
		return;
	}

	Status err;
	QString strConnection;
	DbConnectionSetup::SetupAppServerDatabase(err,ui.txtUser->text(),ui.txtPass->text(),strDb,ui.txtDatabaseName->text(),strConnection,ui.txtDatabasePath->text());
	if (!err.IsOK())
	{
		_CHK_ERR_NO_RET(err);
		return;
	}
	else
	{
		ReloadSettings();
		if (!AppSrv.GetINIFile()->m_strDBConnectionName.isEmpty())
		{
			int nSize=m_lstConnections.size();
			for(int i=0;i<nSize;++i)
			{
				if (m_lstConnections.at(i).m_strCustomName==strConnection)
				{
					ui.cboConnection->setCurrentIndex(i);
					break;
				}
			}
		}
	}


}
void AppConfigMain::RefreshStatus()
{
	Status err;
	AppSrv.LoadINIFile(err);

	bool bServerFailedToStart=false;
	bool bDisableStopButtton=false;
	QString strErrMsg="";

	switch(AppSrv.GetINIFile()->m_nLastErrorCode)
	{
	case StatusCodeSet::ERR_SYSTEM_SC_APPSERVER_STARTING:
		{
			err.setError(StatusCodeSet::ERR_SYSTEM_SC_APPSERVER_STARTING);
			strErrMsg=err.getErrorText();
			bDisableStopButtton=true;
		}
		break;
	case StatusCodeSet::ERR_SYSTEM_SC_APPSERVER_REORGANIZING_DATABASE:
		{
			err.setError(StatusCodeSet::ERR_SYSTEM_SC_APPSERVER_REORGANIZING_DATABASE);
			strErrMsg=err.getErrorText();
			bDisableStopButtton=true;
		}
		break;
	case StatusCodeSet::ERR_SYSTEM_SC_APPSERVER_STARTING_SERVICES:
		{
			err.setError(StatusCodeSet::ERR_SYSTEM_SC_APPSERVER_STARTING_SERVICES);
			strErrMsg=err.getErrorText();
			bDisableStopButtton=true;
		}
		break;
	case StatusCodeSet::ERR_SYSTEM_SC_APPSERVER_STARTED_OK:
		{
			err.setError(StatusCodeSet::ERR_SYSTEM_SC_APPSERVER_STARTED_OK);
			strErrMsg=err.getErrorText();
		}
		break;
	case StatusCodeSet::ERR_SYSTEM_SC_APPSERVER_FAILED_TO_START:
		{
			err.setError(StatusCodeSet::ERR_SYSTEM_SC_APPSERVER_FAILED_TO_START);
			strErrMsg=err.getErrorText().arg(AppSrv.GetINIFile()->m_strLastErrorText);
			bServerFailedToStart=true;
		}
		break;
	default:
		strErrMsg="";
		/*
		AppSrv.LastError();
		if (!strErrMsg.isEmpty())
		{
			QMessageBox::critical(this,tr("Error reported by application server"),strErrMsg);
			bServerFailedToStart=true;
		}
		*/
	    break;
	}



	//if seems ok, check if started
	if (!bServerFailedToStart)
	{
		bServerFailedToStart=!AppSrv.IsRunning(err);
	}

	if (!bServerFailedToStart)
	{
		if (strErrMsg.isEmpty()) 
		{
			if (m_bStarting)
				strErrMsg="Application server is initializing service. Please wait!";
			else
			{
				strErrMsg="Application server is STARTED!";
				m_bStarting=false;
			}
		}
		else
			m_bStarting=false;

		ui.labelStatus->setText(QString(HTML_START_F1)+strErrMsg+QString(HTML_END_F1));
		ui.groupBoxApp->setEnabled(false);
		ui.groupBoxCreateDb->setEnabled(false);
		ui.groupBoxSelectDb->setEnabled(false);
		ui.groupBoxCreateDb->setEnabled(false);
		ui.btnOK->setText(tr("Stop Server"));

		if (bDisableStopButtton)
			ui.btnOK->setEnabled(false);
		else
			ui.btnOK->setEnabled(true);

		ui.cmbDbType2->setEnabled(false);
		if (!m_bStarting)
			ui.btnFlush->setEnabled(true);
	}
	else
	{
		if (strErrMsg.isEmpty()) 
			strErrMsg="Application server is STOPPED!";

		ui.labelStatus->setText(QString(HTML_START_F1)+strErrMsg+QString(HTML_END_F1));
		ui.btnOK->setText(tr("Start Server"));
		ui.groupBoxApp->setEnabled(true);
		ui.groupBoxCreateDb->setEnabled(true);
		ui.groupBoxSelectDb->setEnabled(true);
		ui.groupBoxCreateDb->setEnabled(true);
		ui.btnFlush->setEnabled(false);
		ui.cmbDbType2->setEnabled(true);
		ui.btnOK->setEnabled(true);


	}

	//if (!AppSrv.GetINIFile()->m_strDescriptor.isEmpty())
	//	_CHK_ERR_NO_RET(err);
}

void AppConfigMain::on_btnOK_clicked()
{
	Status err;
	killTimer(m_nTimerID);
	m_bStarting=false;

	if (ui.groupBoxApp->isEnabled())
	{
		int nCurrentSelection=ui.cboConnection->currentIndex();
		if (nCurrentSelection==-1)
		{
			QMessageBox::critical(this,tr("Error"),tr("Can not start application if database connection is not set!"));
			return;
		}

		QString strName=ui.txtName->text();
		if (strName.indexOf(" ")!=-1 || strName.toLatin1().simplified()!=strName)
		{
			QMessageBox::critical(this,tr("Error"),tr("Service Descriptor can not contain spaces nor special characters (national)!"));
			return;

		}

		CheckForDataBaseService(err);
		if (!err.IsOK())
		{
			QMessageBox::critical(this,tr("Error"),tr("Failed to initialize database service: ")+err.getErrorText());
			return;
		}

		DbConnectionSettings settings=m_lstConnections.at(nCurrentSelection);

		Status errTmp;
		//AppSrv.UnInstallService(errTmp); //remove previous:
		if (AppSrv.IsRunning(errTmp))
			AppSrv.Stop(errTmp); //remove previous:

		//save all to ->INI/registry:
		AppSrv.GetINIFile()->m_strDescriptor=ui.txtName->text();
		AppSrv.GetINIFile()->m_nAppServerPort=ui.txtPort->text().toInt();
		AppSrv.GetINIFile()->m_strAppServerIPAddress=ui.txtInterface->text();
		AppSrv.GetINIFile()->m_nSSLMode=ui.ckbSSL->isChecked() ? 1 : 0;
		if (ui.ckbEnableWeb->isChecked())
		{
			AppSrv.GetINIFile()->m_nRestEnable=1;
			AppSrv.GetINIFile()->m_nWWWEnable=1;
		}
		else
		{
			AppSrv.GetINIFile()->m_nRestEnable=0;
			AppSrv.GetINIFile()->m_nWWWEnable=0;
		}

		AppSrv.GetINIFile()->m_strDBConnectionName=ui.cboConnection->currentText();
		AppSrv.GetINIFile()->m_strLastErrorText="";
		AppSrv.GetINIFile()->m_nLastErrorCode=StatusCodeSet::ERR_SYSTEM_SC_APPSERVER_STARTING;

		AppSrv.SetAutoStart(ui.ckbAutoStart->isChecked());

		if(!AppSrv.SaveINIFile())
		{
			QMessageBox::critical(this,tr("Error"),tr("Can not start application, saving settings failed!"));
			RefreshStatus();
			return;
		}

		//AppSrv.InstallService(err,settings.m_strDbType);
		AppSrv.StartOrInstall(err,settings.m_strDbType);
		_CHK_ERR(err);

		AppSrv.CreateUnInstallBat();

		//stop->run;
		Status err;
		AppSrv.Start(err);
		_CHK_ERR_NO_RET(err);


		//parse error log: display to user:
		if (!err.IsOK())
		{
			QString strLastError=AppSrv.LastError();
			if (!strLastError.isEmpty())
			{
				QMessageBox::critical(this,tr("Error reported by application server"),strLastError);
			}
		}

		m_bStarting=true;
	}
	else
	{
		//running->stop:
		Status err;
		AppSrv.Stop(err);
		_CHK_ERR(err);
	}

	
	RefreshStatus();
	m_nTimerID=startTimer(5000); //5seconds
}

void AppConfigMain::on_btnCancel_clicked()
{
	if (m_nTimerID>=0)
		killTimer(m_nTimerID);
	close();
}


void AppConfigMain::on_btnInstall_clicked()
{
	QString strDb=ui.cmbDbType2->itemData(ui.cmbDbType2->currentIndex()).toString();

	if (strDb==DBTYPE_FIREBIRD)
	{
		//execute FireBird/bin/install_super.bat:
		Status err;
		AppSrv.InstallFirebirdService(err);
		_CHK_ERR(err);
		QMessageBox::information(this,tr("Sucess"),tr("FireBird sucessfully installed"));
		on_cmbDbType2_currentIndexChanged(0);
		//create file for uninstall:

		//QDesktopServices::openUrl(QString("http://www.firebirdsql.org/index.php?op=files"));
	}
	else if (strDb==DBTYPE_MYSQL)
	{
		QDesktopServices::openUrl(QString("http://dev.mysql.com/downloads/"));
	}

}



//based on Db selected, test if service is installed and is running
void AppConfigMain::on_cmbDbType2_currentIndexChanged(int index)
{

	QString strDb=ui.cmbDbType2->itemData(ui.cmbDbType2->currentIndex()).toString();

	QString strStatus=QObject::tr("Not Installed");
	ui.btnInstall->setEnabled(true);

	ui.txtUser->setText("");
	ui.txtPass->setText("");

	if (strDb==DBTYPE_FIREBIRD)
	{
		if(AppSrv.IsServiceInstalled("FirebirdServerDefaultInstance"))
		{
			strStatus=QObject::tr("Installed");
			if(AppSrv.IsServiceRunning("FirebirdServerDefaultInstance"))
				strStatus=QObject::tr("Installed & Running");
			else
				strStatus=QObject::tr("Installed & Not Running");

			ui.btnInstall->setEnabled(false);
		}

		ui.txtUser->setText("SYSDBA");
		ui.txtPass->setText("masterkey");
	}
	else if (strDb==DBTYPE_MYSQL)
	{
		if(AppSrv.IsServiceInstalled("MySQL"))
		{
			strStatus=QObject::tr("Installed");
			if(AppSrv.IsServiceRunning("MySQL"))
				strStatus=QObject::tr("Installed & Running");
			else
				strStatus=QObject::tr("Installed & Not Running");

			ui.btnInstall->setEnabled(false);
		}
	}

	ui.labelStatusDb->setText(QString(HTML_START_F1)+strStatus+QString(HTML_END_F1));



}



void AppConfigMain::on_btnSelectDatabasePath_clicked()
{
	//open file dialog:
	QString strFile = QFileDialog::getExistingDirectory(
		NULL,
		tr("Database Directory"),
		QCoreApplication::applicationDirPath());

	if(!strFile.isEmpty())
	{
		ui.txtDatabasePath->setText(strFile);
	}


}


//hardcode all actions:
void AppConfigMain::SetupMenuActions()
{

	//------------------------FILE----------------------------
	m_pActQuit= new QAction(tr("Quit"),this);
	QKeySequence key(tr("Ctrl+Q"));
	m_pActQuit->setShortcut(key);
	connect(m_pActQuit, SIGNAL(triggered()), this, SLOT(close()));


	//------------------------HELP----------------------------
	m_pActFUI_About= new QAction(tr("About"),this);
	connect(m_pActFUI_About, SIGNAL(triggered()), this, SLOT(OnMenu_About()));


	//--------------------------------------------------------
	//						Menu Setup
	////--------------------------------------------------------

	//Create Menus:
	QMenuBar *menuBar=this->menuBar();

	QMenu *menuFile= new QMenu(tr("File"));
	menuFile->addAction(m_pActQuit);
	m_pMenuFile=menuBar->addMenu(menuFile);

	QMenu *menuHelp= new QMenu(tr("Help"));
	menuHelp->addAction(m_pActFUI_About);
	m_pMenuHelp=menuBar->addMenu(menuHelp);

}


void AppConfigMain::OnMenu_About()
{
	QString strTitle = QString("SOKRATES<sup>")+QChar(174)+QString("</sup> <i>Application Server</i>");
	AboutWidget dlg(this,strTitle);
	dlg.exec();
}

//check for database service: if installed and if started, return err if not
void AppConfigMain::CheckForDataBaseService(Status &err)
{
	QString strDb=ui.cmbDbType2->itemData(ui.cmbDbType2->currentIndex()).toString();
	if (strDb==DBTYPE_FIREBIRD)
	{
		if(!AppSrv.IsServiceInstalled("FirebirdServerDefaultInstance"))
		{
			AppSrv.InstallFirebirdService(err);
			on_cmbDbType2_currentIndexChanged(0);
		}
	}
	else if (strDb==DBTYPE_MYSQL)
	{
		if(!AppSrv.IsServiceInstalled("MySQL"))
			err.setError(1,"MySQL service is not installed and started.");
	}
}



void AppConfigMain::timerEvent(QTimerEvent *event)
{
	//out timer is here, then fire Time out error, save our asses!!!
	if(event->timerId()==m_nTimerID)
	{
		killTimer(m_nTimerID);
		m_nTimerID=-1;
		RefreshStatus();
		/*
		if (ui.btnOK->text() == tr("Start Server") && ui.btnOK->isEnabled())
		{
			return;
		}
		else
		{
		*/
		m_nTimerID=startTimer(5000); //5seconds
		//}
	}
}




void AppConfigMain::SendReloadWebPagesCommandToServer(Status &err)
{
	//engange REST client: call GET/service/settings/reload
	//CONNECT:
	RestHTTPClient client;


	client.SetConnectionSettings("127.0.0.1",AppSrv.GetINIFile()->m_nAppServerPort,AppSrv.GetINIFile()->m_nSSLMode,3);
	client.Connect(err);
	if (!err.IsOK())
	{
		return;
	}

	//SEND:
	//client.msg_send.AddParameter(&bSkipCatalogReload,"bSkipCatalogReload");
	client.RestSend(err,"GET","/rest/server/reloadwebpages");
}

void AppConfigMain::on_btnFlush_clicked()
{
	Status err;
	SendReloadWebPagesCommandToServer(err);
	_CHK_ERR(err);
}