#include "applicationserverconfigurator.h"
#include <QProcess>
#include <QSettings>
//#include "common/common/config.h"
#include "common/common/config_version_sc.h"
#include "common/common/datahelper.h"
#include "db_core/db_core/db_drivers.h"
#include <QMessageBox>
#include <QDir>
#include <QCoreApplication>

ApplicationServerConfigurator::ApplicationServerConfigurator()
{
	m_strAppSrvPath=QCoreApplication::applicationDirPath();
}

ApplicationServerConfigurator::~ApplicationServerConfigurator()
{

}


void ApplicationServerConfigurator::LoadINIFile(Status &pStatus)
{
	//load INI:
	m_strIniFilePath= m_strAppSrvPath+"/settings/app_server.ini";
	bool bOK=m_INIFile.Load(m_strIniFilePath);
	if(!bOK)
	{
		pStatus.setError(1,"INI file is missing,corrupted or has invalid values!");
		return;
	}
}


bool ApplicationServerConfigurator::IsRunning(Status &pStatus)
{
	pStatus.setError(0);


	QProcess app;
	app.setWorkingDirectory(m_strAppSrvPath);
	QStringList lstArgs;
	lstArgs<<"-v";
	app.start(m_strAppSrvPath+"/"+m_INIFile.m_strDescriptor+".exe",lstArgs);
	if (!app.waitForFinished(-1)) //wait forever
	{		
		pStatus.setError(1,QObject::tr("Failed to find service:")+m_INIFile.m_strDescriptor);
		return false;
	}
	int nExitCode=app.exitCode();
	QByteArray errContent=app.readAllStandardError();
	QByteArray fileContent=app.readAllStandardOutput();
	if (errContent.size()!=0)
	{
		pStatus.setError(1,QT_TR_NOOP("Failed to send command to service! Reason:"+errContent.left(4095)));
		return false;
	}

	//find: is installed and running:
	QString strOut(fileContent);
	if(strOut.indexOf("is installed and running")>=0)
		return true;
	else
		return false;
}


void ApplicationServerConfigurator::Stop(Status &pStatus)
{
	pStatus.setError(0);

	if (!IsRunning(pStatus))
		return;

	//QStringList lstArgs;
	//lstArgs<<"t";

	QProcess app;
	app.setWorkingDirectory(m_strAppSrvPath);
	QStringList lstArgs;
	lstArgs<<"-t";
	app.start(m_strAppSrvPath+"/"+m_INIFile.m_strDescriptor+".exe",lstArgs);
	//app.start(m_INIFile.m_strDescriptor+" -t");
	if (!app.waitForFinished(-1)) //wait forever
	{		
		pStatus.setError(1,QObject::tr("Failed to find service:")+m_INIFile.m_strDescriptor);
		return;
	}
	int nExitCode=app.exitCode();
	QByteArray errContent=app.readAllStandardError();
	QByteArray fileContent=app.readAllStandardOutput();
	if (errContent.size()!=0)
	{
		pStatus.setError(1,QT_TR_NOOP("Failed to send command to service! Reason:"+errContent.left(4095)));
		return;
	}

}
void ApplicationServerConfigurator::Start(Status &pStatus)
{
	pStatus.setError(0);
	if (IsRunning(pStatus))
		return;

	//QStringList lstArgs;
	//stArgs<<"t";

	QProcess app;
	app.setWorkingDirectory(m_strAppSrvPath);
	QStringList lstArgs;
	app.start(m_strAppSrvPath+"/"+m_INIFile.m_strDescriptor+".exe",lstArgs);
	if (!app.waitForFinished(-1)) //wait forever
	{		
		pStatus.setError(1,QObject::tr("Failed to find service: ")+m_INIFile.m_strDescriptor);
		return;
	}
	int nExitCode=app.exitCode();
	QByteArray errContent=app.readAllStandardError();
	QByteArray fileContent=app.readAllStandardOutput();
	if (errContent.size()!=0)
	{
		pStatus.setError(1,QT_TR_NOOP("Failed to send command to service! xxx Reason:"+errContent.left(4095)));
		return;
	}
}


bool ApplicationServerConfigurator::IsAutoStart()
{
	QString strAppService=QString(SERVER_DESCRIPTION_TAG)+QString(" - ")+m_INIFile.m_strDescriptor;
	//check if srv exists
	QSettings settingsApp("HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentCOntrolSet\\Services\\"+strAppService,	QSettings::NativeFormat);

	int nStart=settingsApp.value("Start",0).toInt();
	if (nStart==2)
		return true;
	else
		return false;
}




QString ApplicationServerConfigurator::LastError()
{

	//parse error log: display to user:
	QString strLastError;
	QFile log(m_strAppSrvPath+"/settings/appserver.log");
	if(log.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		QString strMsgErr="Msg("+QVariant(StatusCodeSet::ERR_SYSTEM_APP_SRV_FAILED_START).toString()+")";
		QString strContent=log.readAll();
		QStringList strLines=strContent.split("\n");
		int nSize=strLines.size();
		for(int i=nSize-1;i>=0;--i)
		{
			if (strLines.at(i).indexOf(strMsgErr)>=0)
			{
				QStringList strParts=strLines.at(i).split("\t");
				if (strParts.size()>0)
				{
					strLastError=strParts.at(strParts.size()-1);
					break;
				}
			}
		}
		log.close();
	}

	return strLastError;
}



//check if already exists, if not, create
//Dbtpye-> determines on what service is depenednt
void ApplicationServerConfigurator::InstallService(Status &pStatus,QString strDbType)
{
	QString strAppService=QString(SERVER_DESCRIPTION_TAG)+QString(" - ")+m_INIFile.m_strDescriptor;

	//if different copy:
	QFile::remove(m_strAppSrvPath+"/"+m_INIFile.m_strDescriptor+".exe");
	QFile::remove(m_strAppSrvPath+"/appserver.exe");
	QFile file(m_strAppSrvPath+"/template_data/appserver.exe");
	if(!file.copy(m_strAppSrvPath+"/"+m_INIFile.m_strDescriptor+".exe"))
	{
		pStatus.setError(1,QObject::tr("Failed to rename appserver.exe to new descriptor! Check if application server is running or shorten descriptor name!"));
		return;
	}


	//check if srv exists
	QSettings settingsApp("HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentCOntrolSet\\Services\\"+strAppService,	QSettings::NativeFormat);

	QString strPath=settingsApp.value("ImagePath",QString("")).toString();
	if (strPath.isEmpty())
	{
		//start with -i
		pStatus.setError(0);

		//QStringList lstArgs;
		//lstArgs<<"i";

		QProcess app;
		app.setWorkingDirectory(m_strAppSrvPath);
		QStringList lstArgs;
		lstArgs<<"-i";
		app.start(m_strAppSrvPath+"/"+m_INIFile.m_strDescriptor+".exe",lstArgs);

		if (!app.waitForFinished(-1)) //wait forever
		{		
			pStatus.setError(1,QObject::tr("Failed to find service: ")+m_INIFile.m_strDescriptor);
			return;
		}
		int nExitCode=app.exitCode();
		QByteArray errContent=app.readAllStandardError();
		QByteArray fileContent=app.readAllStandardOutput();
		if (errContent.size()!=0)
		{
			pStatus.setError(1,QT_TR_NOOP("Failed to send command to service! Reason:"+errContent.left(4095)));
			return;
		}

		//set: autostart:
		settingsApp.setValue("Start",2);
	}

	
	//->depent on:
	if (strDbType==DBTYPE_MYSQL)
	{
		DataHelper::Registry_SetKeyValue("HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentCOntrolSet\\Services\\"+strAppService,"DependOnService","MySQL");
		//settingsApp.setValue("DependOnService","MySQL");
	}
	else if (strDbType==DBTYPE_FIREBIRD)
	{
		DataHelper::Registry_SetKeyValue("HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentCOntrolSet\\Services\\"+strAppService,"DependOnService","FirebirdServerDefaultInstance");
		//settingsApp.setValue("DependOnService","FirebirdServerDefaultInstance");
	}

}

void ApplicationServerConfigurator::StartOrInstall(Status &pStatus,QString strDbType)
{
	QString strAppService=QString(SERVER_DESCRIPTION_TAG)+QString(" - ")+m_INIFile.m_strDescriptor;

	if(!IsServiceInstalled(strAppService))
	{
		InstallService(pStatus,strDbType);
		return;
	}
	else
	{
		Start(pStatus);
	}
}

void ApplicationServerConfigurator::UnInstallService(Status &pStatus)
{
	//start with -i
	pStatus.setError(0);

	QProcess app;
	app.setWorkingDirectory(m_strAppSrvPath);
	QStringList lstArgs;
	lstArgs<<"-u";
	app.start(m_strAppSrvPath+"/"+m_INIFile.m_strDescriptor+".exe",lstArgs);
	if (!app.waitForFinished(-1)) //wait forever
	{		
		pStatus.setError(1,QObject::tr("Failed to find service: ")+m_INIFile.m_strDescriptor);
		return;
	}
	int nExitCode=app.exitCode();
	QByteArray errContent=app.readAllStandardError();
	QByteArray fileContent=app.readAllStandardOutput();
	if (errContent.size()!=0)
	{
		pStatus.setError(1,QT_TR_NOOP("Failed to send command to service! Reason:"+errContent.left(4095)));
		return;
	}

	QFile::remove(m_strAppSrvPath+"/"+m_INIFile.m_strDescriptor+".exe");
}

void ApplicationServerConfigurator::SetAutoStart(bool bAuto)
{
	QString strAppService=QString(SERVER_DESCRIPTION_TAG)+QString(" - ")+m_INIFile.m_strDescriptor;
	QSettings settingsApp("HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentCOntrolSet\\Services\\"+strAppService,	QSettings::NativeFormat);

	if (bAuto)
		settingsApp.setValue("Start",2);
	else
		settingsApp.setValue("Start",3);

}

bool ApplicationServerConfigurator::CreateUnInstallBat(QString strFileName)
{
	QString strExec=m_INIFile.m_strDescriptor+ " -t "+"\n";		//stop
	strExec+=m_INIFile.m_strDescriptor+ " -u ";			//uninstall

	QFile file(m_strAppSrvPath+"/"+strFileName);
	if (file.exists())
		file.remove();

	//to include firebird uninstall?
	QFile fileUninstall(m_strAppSrvPath+"/firebird_local");
	if (fileUninstall.exists())
	{
		//change dir
		QString strCD=m_strAppSrvPath+"/firebird/bin";
		strCD=QDir::toNativeSeparators(strCD);
		strCD="CD "+strCD+"";
		strExec+="\n"+strCD;

		//exec firebird uninstall
		QString strFire=m_strAppSrvPath+"/firebird/bin/uninstall.bat";
		strFire=QDir::toNativeSeparators(strFire);
		strFire="CALL \""+strFire+"\"";
		strExec+="\n"+strFire;

		//when uninstalling FB remove firebird_local settings
		QString	strRemoveLocal="del firebird_local";
		strExec+="\n"+strRemoveLocal;
	}


	if(file.open(QIODevice::WriteOnly | QIODevice::Text))
	{
		file.write(strExec.toLatin1());
		file.close();
		return true;
	}
	else
	{
		return false;
	}


}


//return if service is running:
bool ApplicationServerConfigurator::IsServiceRunning(QString strService)
{
	//check if srv exists
	QSettings settingsApp("HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentCOntrolSet\\Services\\"+strService,	QSettings::NativeFormat);
	QString  strName=settingsApp.value("DisplayName","").toString().toUpper();
	if (strName.isEmpty())
		return false;

	QProcess app;
	app.start(" NET START");
	if (!app.waitForFinished(-1)) //wait forever
	{		
		return false;
	}
	int nExitCode=app.exitCode();
	QByteArray errContent=app.readAllStandardError();
	QByteArray fileContent=app.readAllStandardOutput();
	if (errContent.size()!=0)
	{
		return false;
	}

	//find: is installed and running:
	QString strOut(fileContent);
	QStringList lstSrv=strOut.split("\n");

	int nSize=lstSrv.size();
	for(int i=0;i<nSize;++i)
	{
		if (lstSrv.at(i).toUpper().indexOf(strName.toUpper())>=0)
		{
			return true;

		}
	}

	return false;
}

//return if service is isntalled:
bool ApplicationServerConfigurator::IsServiceInstalled(QString strService)
{
	//check if srv exists
	QSettings settingsApp("HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentCOntrolSet\\Services\\"+strService,	QSettings::NativeFormat);

	QString  strName=settingsApp.value("DisplayName","").toString();
	if (strName.isEmpty())
		return false;
	else
		return true;
}


void ApplicationServerConfigurator::InstallFirebirdService(Status &pStatus)
{
	pStatus.setError(0);

	QString strInstallBat=m_strAppSrvPath+"/firebird/bin/install_super.bat";
	strInstallBat=QDir::toNativeSeparators(strInstallBat);
	QString strApp="cmd /C";
	strInstallBat=strInstallBat.trimmed();
	strInstallBat.prepend("\"");
	strInstallBat.append("\"");
	strApp=strApp+" "+strInstallBat;


	QProcess app;
	app.setWorkingDirectory(m_strAppSrvPath+"/firebird/bin");
	app.start(strApp);
	if (!app.waitForFinished(-1)) //wait forever
	{		
		pStatus.setError(1,QObject::tr("Failed to execute: ")+strInstallBat);
		return;
	}
	int nExitCode=app.exitCode();
	QByteArray errContent=app.readAllStandardError();
	QByteArray fileContent=app.readAllStandardOutput();
	if (errContent.size()!=0)
	{
		pStatus.setError(1,QT_TR_NOOP("Failed to install FireBird Service! Reason:"+errContent.left(4095)));
		return;
	}


	//create file:
	QFile fileUninstall(m_strAppSrvPath+"/firebird_local");
	if (!fileUninstall.exists())
	{
		if(fileUninstall.open(QIODevice::WriteOnly | QIODevice::Text))
		{
			fileUninstall.write("local");
			fileUninstall.close();
		}
	}

}