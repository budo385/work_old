#include <QApplication>
#include "appconfigmain.h"
#include "multiappserversetup.h"
//#include "bus_client/bus_client/client_global_objects_create.h"
//#include "bus_client/bus_client/clientmanagerext.h"
//ClientManagerExt *g_pClientManager=NULL;


int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	Q_INIT_RESOURCE(gui_core);


	if (argc>6)
	{
		QString strPrefix=QString(argv[1]);
		int nStartPort=QString(argv[2]).toInt();
		int nStart=QString(argv[3]).toInt();
		int nEnd=QString(argv[4]).toInt();
		QString strSource=QString(argv[5]);
		QString strTarget=QString(argv[6]);
		QString strBackup;
		QString strDataDir;
			strBackup=QString(argv[7]);
		if (argc>8)
			strDataDir=QString(argv[8]);
		
		MultiAppServerSetup::Start(strPrefix,nStartPort,nStart,nEnd,strSource,strTarget,strBackup,strDataDir);
		return 1;
	}

	//g_pClientManager= new ClientManagerExt(&a); 
	//g_pClientManager->SetApplicationDisplayName("SOKRATES<sup>�</sup> <i>Application Server</i>");

	AppConfigMain w;
	w.show();
	a.connect(&a, SIGNAL(lastWindowClosed()), &a, SLOT(quit()));
	return a.exec();
}
