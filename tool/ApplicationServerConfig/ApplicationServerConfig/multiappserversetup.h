#ifndef MULTIAPPSERVERSETUP_H
#define MULTIAPPSERVERSETUP_H

#include <QObject>

class MultiAppServerSetup : public QObject
{
	Q_OBJECT

public:
	MultiAppServerSetup(QObject *parent);
	~MultiAppServerSetup();

	static void Start(QString strPrefix,int nStartPort,int nStartCount, int nEndCount,QString strSourceDir,QString strTargetDir,QString strBackupDir="",QString strDataDir="");

	static bool executeShellCommand(QString strCommand);

private:
	
};

#endif // MULTIAPPSERVERSETUP_H
