#include "multiappserversetup.h"
#include <QDir>
#include <QProcess>
#include <QVariant>
#include <QDebug>
#include "common/common/status.h"
#include "applicationserverconfigurator.h"
#include "db_core/db_core/dbconnsettings.h"
#include "common/common/datahelper.h"
#include "db_core/db_core/db_drivers.h"
#include "db/db/dbconnectionsetup.h"
//#include "common/common/config.h"
#include "common/common/config_version_sc.h"

MultiAppServerSetup::MultiAppServerSetup(QObject *parent)
	: QObject(parent)
{

}

MultiAppServerSetup::~MultiAppServerSetup()
{

}


void MultiAppServerSetup::Start(QString strPrefix,int nStartPort,int nStartCount, int nEndCount,QString strSourceDir,QString strTargetDir,QString strBackupDir,QString strDataDir)
{

	strSourceDir=QDir::toNativeSeparators(strSourceDir);
	strTargetDir=QDir::toNativeSeparators(strTargetDir);

	nStartPort--;
	for(int i=nStartCount;i<=nEndCount;++i)
	{
		nStartPort++;

		QString strAppName=strPrefix+QVariant(i).toString();
		QDir targetDir(strTargetDir);
		targetDir.mkdir(strAppName);
		QString strTargetDirApp=strTargetDir+"\\"+strAppName;

		QString strCommand="xcopy \""+strSourceDir+ "\" \""+strTargetDirApp+"\" /S /E /Y";
		if(!executeShellCommand(strCommand))
			continue;

		QString strTargetDataFile=strTargetDirApp+"\\data\\DATA.FDB";
		if (!strDataDir.isEmpty())
		{
			strTargetDataFile=strDataDir+"\\"+strAppName+".FDB";
			QFile file(strTargetDirApp+"\\data\\DATA.FDB");
			if(!file.copy(strTargetDataFile))
			{
				qDebug()<<tr("Failed to copy database");
				continue;

			}
		}

		QString strBackupDirApp=strTargetDirApp+"\\backup";
		if (!strBackupDir.isEmpty())
		{
			strBackupDirApp=strBackupDir+"\\"+strAppName;
			QDir dirBcp(strBackupDir);
			dirBcp.mkdir(strAppName);
		}

		//create database connection to strTargetDataFile, save it
		Status err;
		QString strSettingsPath=strTargetDirApp+"/settings/database_connections.cfg";
		DbConnectionSettings settingsFireBird;
		settingsFireBird.m_strCustomName=strAppName;
		settingsFireBird.m_strDbName=strTargetDataFile;
		settingsFireBird.m_strDbUserName="SYSDBA";
		settingsFireBird.m_strDbPassword="masterkey";
		settingsFireBird.m_strDbType=DBTYPE_FIREBIRD;
		DbConnectionSetup::CreateDatabaseConnection(err,strSettingsPath,settingsFireBird);
		if (!err.IsOK())
		{
			qDebug()<<tr("Failed to create database connection: ")+strTargetDataFile;
			continue;
		}

		//copy appserver:
		/*
		QFile fileApp(strTargetDirApp+"\\template_data\\appserver.exe");
		if(!fileApp.copy(strTargetDirApp+"\\"+strAppName+".exe"))
		{
			qDebug()<<tr("Failed to copy appserver");
			continue;
		}
		*/

		//modify ini: backup+connection+port
		Status errTmp;
		ApplicationServerConfigurator AppSrv;
		AppSrv.SetAppServerPath(strTargetDirApp);
		AppSrv.LoadINIFile(err);
		if (!err.IsOK())
		{
			qDebug()<<tr("Failed to load ini file");
			continue;
		}

		//AppSrv.UnInstallService(errTmp); //remove previous:
		if (AppSrv.IsRunning(errTmp))
			AppSrv.Stop(errTmp); //remove previous:

		//save all to ->INI/registry:
		AppSrv.GetINIFile()->m_strDescriptor=strAppName;
		AppSrv.GetINIFile()->m_nAppServerPort=nStartPort;
		AppSrv.GetINIFile()->m_strDBConnectionName=strAppName;
		AppSrv.GetINIFile()->m_strBackupPath=strBackupDirApp;
		if(!AppSrv.SaveINIFile())
		{
			qDebug()<<tr("Failed to save ini file");
			continue;
		}
/*
		strCommand=strTargetDirApp+"\\"+strAppName+".exe -i";
		if(!executeShellCommand(strCommand))
		{
			qDebug()<<tr("Failed to install service");
			continue;

		}

		QString strAppService=QString(SERVER_DESCRIPTION_TAG)+QString(" - ")+strAppName;
		DataHelper::Registry_SetKeyValue("HKEY_LOCAL_MACHINE","SYSTEM\\CurrentCOntrolSet\\Services\\"+strAppService,"DependOnService",2,"FirebirdServerDefaultInstance");
*/
	
		//AppSrv.InstallService(err,DBTYPE_FIREBIRD);
		AppSrv.StartOrInstall(err,DBTYPE_FIREBIRD);
		if (!err.IsOK())
		{
			qDebug()<<tr("Failed to install service");
			continue;
		}
	

		AppSrv.SetAutoStart(true);
		AppSrv.CreateUnInstallBat();
		AppSrv.Start(err);
		if(!err.IsOK())
		{
			qDebug()<<tr("Failed to start service");
			QString strLastError=AppSrv.LastError();
			if (!strLastError.isEmpty())
			{
				qDebug()<<tr("Error reported by application server: ")+strLastError;
			}
			continue;
		}

		qDebug()<<tr("Server ")+strAppName+tr(" successfully started at port: ")+QVariant(nStartPort).toString();
	}
}





bool MultiAppServerSetup::executeShellCommand(QString strCommand)
{
	QProcess app;

	app.start(strCommand);
	if (!app.waitForFinished(-1)) //wait forever
	{	
		qDebug()<<tr("Failed to execute command: ")+strCommand;
		return false;
	}
	int nExitCode=app.exitCode();
	QByteArray errContent=app.readAllStandardError();
	QByteArray fileContent=app.readAllStandardOutput();
	if (errContent.size()!=0)
	{

		qDebug()<<tr("Failed to execute command: ")+strCommand;
		qDebug()<<tr("Reason:")<<errContent;
		return false;
	}

	return true;
}