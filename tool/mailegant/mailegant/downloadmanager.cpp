/****************************************************************************
 **
 ** Copyright (C) 2012 Nokia Corporation and/or its subsidiary(-ies).
 ** All rights reserved.
 ** Contact: Nokia Corporation (qt-info@nokia.com)
 **
 ** This file is part of the examples of the Qt Toolkit.
 **
 ** $QT_BEGIN_LICENSE:BSD$
 ** You may use this file under the terms of the BSD license as follows:
 **
 ** "Redistribution and use in source and binary forms, with or without
 ** modification, are permitted provided that the following conditions are
 ** met:
 **   * Redistributions of source code must retain the above copyright
 **     notice, this list of conditions and the following disclaimer.
 **   * Redistributions in binary form must reproduce the above copyright
 **     notice, this list of conditions and the following disclaimer in
 **     the documentation and/or other materials provided with the
 **     distribution.
 **   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
 **     the names of its contributors may be used to endorse or promote
 **     products derived from this software without specific prior written
 **     permission.
 **
 ** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 ** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 ** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 ** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 ** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 ** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 ** $QT_END_LICENSE$
 **
 ****************************************************************************/

#include "downloadmanager.h"

#include <QFileInfo>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QString>
#include <QStringList>
#include <QTimer>
#include <QDir>
#include <stdio.h>

DownloadManager::DownloadManager(QObject *parent)
	 : QObject(parent), downloadedCount(0), totalCount(0), m_bToFile(false)
 {
 }

void DownloadManager::downloadFile(const QUrl &url, QString &strPathToDownloadTo)
{
	append(url);
	startDownloadFiles(strPathToDownloadTo);
}

void DownloadManager::downloadFiles(const QStringList &urlList, QString &strPathToDownloadTo)
{
	foreach (QString url, urlList)
		append(QUrl::fromEncoded(url.toLocal8Bit()));

	startDownloadFiles(strPathToDownloadTo);
}

void DownloadManager::downloadByteArray(const QUrl &url)
{
	append(url);
	startDownloadBytes();
}

void DownloadManager::downloadByteArrays(const QStringList &urlList)
{
	foreach (QString url, urlList)
		append(QUrl::fromEncoded(url.toLocal8Bit()));

	startDownloadBytes();
}

void DownloadManager::append(const QUrl &url)
 {
     downloadQueue.enqueue(url);
     ++totalCount;
 }

void DownloadManager::startDownloadFiles(QString &strPathToDownloadTo)
{
	m_bToFile=true;
	m_strPathToDownloadTo=strPathToDownloadTo;
	m_lstSavedFiles.clear();
	QTimer::singleShot(0, this, SLOT(startNextDownload()));
}

void DownloadManager::startDownloadBytes()
{
	m_bToFile=false;
	m_lstBytOutput.clear();
	QTimer::singleShot(0, this, SLOT(startNextDownload()));
}

QString DownloadManager::saveFileName(const QUrl &url)
 {
     QString path = url.path();
     QString basename = QFileInfo(path).fileName();

     if (basename.isEmpty())
         basename = "download";

	 QString strFullPathToSaveFile=QDir::toNativeSeparators(m_strPathToDownloadTo+QDir::separator()+basename);

     if (QFile::exists(strFullPathToSaveFile))
	 {
         // already exists, don't overwrite
         int i = 0;
         strFullPathToSaveFile += '.';
         while (QFile::exists(strFullPathToSaveFile + QString::number(i)))
             ++i;

         strFullPathToSaveFile += QString::number(i);
     }

     return strFullPathToSaveFile;
 }

 void DownloadManager::startNextDownload()
 {
	 if (downloadQueue.isEmpty()) {
		 printf("%d/%d files downloaded successfully\n", downloadedCount, totalCount);
		 emit finishedTotal(m_lstBytOutput, m_lstSavedFiles);
		 return;
	 }

	QUrl url = downloadQueue.dequeue();

	if (m_bToFile)
	{
		 QString filename = saveFileName(url);
		 output.setFileName(filename);
		 m_lstSavedFiles.append(filename);
		 if (!output.open(QIODevice::WriteOnly)) {
			 fprintf(stderr, "Problem opening save file '%s' for download '%s': %s\n",
					 qPrintable(filename), url.toEncoded().constData(),
					 qPrintable(output.errorString()));

			 startNextDownload();
			 return;                 // skip this download
		 }
	}

     QNetworkRequest request(url);
     currentDownload = manager.get(request);
     connect(currentDownload, SIGNAL(downloadProgress(qint64,qint64)),
             SLOT(downloadProgress(qint64,qint64)));
     connect(currentDownload, SIGNAL(finished()),
             SLOT(downloadFinished()));
     connect(currentDownload, SIGNAL(readyRead()),
             SLOT(downloadReadyRead()));

     // prepare the output
     printf("Downloading %s...\n", url.toEncoded().constData());
     downloadTime.start();
 }

 void DownloadManager::downloadProgress(qint64 bytesReceived, qint64 bytesTotal)
 {
     //progressBar.setStatus(bytesReceived, bytesTotal);

     // calculate the download speed
     double speed = bytesReceived * 1000.0 / downloadTime.elapsed();
     QString unit;
     if (speed < 1024) {
         unit = "bytes/sec";
     } else if (speed < 1024*1024) {
         speed /= 1024;
         unit = "kB/s";
     } else {
         speed /= 1024*1024;
         unit = "MB/s";
     }

     //progressBar.setMessage(QString::fromLatin1("%1 %2").arg(speed, 3, 'f', 1).arg(unit));
     //progressBar.update();
 }

 void DownloadManager::downloadFinished()
 {
     //progressBar.clear();
     output.close();

     if (currentDownload->error()) {
         // download failed
		 emit errorDownload(currentDownload->errorString());
         fprintf(stderr, "Failed: %s\n", qPrintable(currentDownload->errorString()));
     } else {
         printf("Succeeded.\n");
         ++downloadedCount;
     }

     currentDownload->deleteLater();
     startNextDownload();
 }

 void DownloadManager::downloadReadyRead()
 {
	 if (m_bToFile)
	 {
		output.write(currentDownload->readAll());
	 }
	 else
	 {
		m_lstBytOutput.append(currentDownload->readAll());
	 }
 }