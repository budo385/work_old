#include "about.h"

about::about(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);

	setWindowFlags(Qt::WindowCloseButtonHint | Qt::WindowMaximizeButtonHint);
	setStyleSheet("QWidget { background-color:black; color:white;}");

	ui.logo_label->setPixmap(QPixmap(":logo.png"));
	setWindowTitle("mailegant Writer");
}

about::~about()
{

}
