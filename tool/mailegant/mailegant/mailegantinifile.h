#ifndef MAILEGANTINIFILE_H
#define MAILEGANTINIFILE_H

#include <QObject>
#include "common/common/inifile.h"

class MailegantIniFile : public QObject
{
	Q_OBJECT

public:
	MailegantIniFile(QObject *parent=0);
	~MailegantIniFile();
	bool isDebug();
	QString getXMLLocation();
	int getVersion();
	QString getAppLocationURL();
	void setVersion(int nVersion);
	void SaveIni();

private:
	QString	m_strIniFilePath;
	IniFile	m_inifile;
};

#endif // MAILEGANTINIFILE_H
