#ifndef MAILEGANT_H
#define MAILEGANT_H

#include <QtWidgets/QMainWindow>
#include "ui_mailegant.h"
#include "downloadmanager.h"
#include "mailegantinifile.h"
#include "SMTPEmail/SMTPEmail/smtpclient.h"
#include "aboutwidget.h"

class WebPage : public QWebPage
{
	Q_OBJECT

public:
	WebPage(QObject* parent = 0);
	~WebPage();

private slots:
	void permissionRequested(QWebFrame* frame, QWebPage::Feature feature);
	void featurePermissionRequestCanceled(QWebFrame*, QWebPage::Feature);
};

class mailegant : public QMainWindow
{
	Q_OBJECT

public:
	mailegant(QWidget *parent = 0, Qt::WindowFlags flags = 0);
	~mailegant();

private:
	void openMainWin(QString strTmpAppLocation);
	void getAppXML();
	void setLoadLocation();
	int testConnection(SmtpClient &smtp);

	Ui::mailegantClass ui;
	DownloadManager m_xmldDownload;
	int				m_nTmpVersion;
	QString			m_strTmpAppLocation;
	MailegantIniFile m_iniFile;
	WebPage			*m_pPage;
	aboutWidget		*m_pAboutWidg;

	QAction*		m_pInstallAddIn;
	QAction*		m_pAbout;
	QAction*		m_pExit;
	QAction*		m_pReload;

public slots:
	void callWrapper(QString eventType, QString ScottyEventName, int numPar1, int numPar2, QString strPar1,  QString strPar2, QString strPar3, QString strPar4, QString strPar5, QString strPar6, QString strPar7, QString strPar8, QString strPar9, QString strPar10, QString strPar11, QString strPar12, QString strPar13);
	bool sendEmail(QVariantMap map);
	bool testEmailSettings(QVariantMap map);
	void writeFile(QString relativePath, QString data);
	void readFile(QString relativePath, int start, int end);

private slots:
	void on_reload_clicked();
	void on_menuInstall_Outlook_Add_In();
	void on_About();
	void finishedDownloadXML(QList<QByteArray> lstBytOutput, QStringList lstSavedFiles);
	void finishedZipDownload(QList<QByteArray> lstBytOutput, QStringList lstSavedFiles);
	void errorDownloadXML(QString error);
	void errorDownloadZIP(QString error);
	void populateJavaScriptWindowObject();
	void handleSslErrors(QNetworkReply * reply, const QList<QSslError> & errors);
};

#endif // MAILEGANT_H
