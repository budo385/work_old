#ifndef UNZIPINITVERSION_H
#define UNZIPINITVERSION_H

#include <QObject>

class UnzipInitVersion : public QObject
{
	Q_OBJECT

public:
	UnzipInitVersion(QObject *parent=0);
	~UnzipInitVersion();
	void Unzip(QStringList lstData);

private:
	
};

#endif // UNZIPINITVERSION_H
