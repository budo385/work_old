#include "mailegantinifile.h"
#include "const_definitions.h"
#include "unzipinitversion.h"
#include "common/common/datahelper.h"
#include <QDir>

MailegantIniFile::MailegantIniFile(QObject *parent)
	: QObject(parent)
{
	QString strAppHomePath = WEBKIT_PROTOCOL+DataHelper::GetApplicationHomeDir()+QString("/")+HTML_DIR+QString("/")+INIT_HTML_LOCATION;
	m_strIniFilePath=QDir::toNativeSeparators(DataHelper::GetApplicationHomeDir()+QDir::separator()+INI_FILE_NAME);

	m_inifile.SetPath(m_strIniFilePath);
	m_inifile.Load();
	if (!m_inifile.EntryExists("Main", "nVersion"))
	{
		m_inifile.SetValue("Main", "nVersion", INIT_VERSION);
		m_inifile.SetValue("Main", "strAppLocation", INIT_HTML_LOCATION);
		m_inifile.SetValue("Main", "strAppLocationURL", strAppHomePath);
		m_inifile.Save();
		
		QStringList lstData;
		UnzipInitVersion unzipInit;
		unzipInit.Unzip(lstData);
	}
	else //Add new things that need
	{
		//Set app location url.
		QString strAppLocationURL;
		m_inifile.GetValue("Main", "strAppLocationURL", strAppLocationURL);
		if(strAppLocationURL.isEmpty())
		{
			m_inifile.SetValue("Main", "strAppLocationURL", strAppHomePath);
			m_inifile.Save();
		}
	}
}

MailegantIniFile::~MailegantIniFile()
{

}

bool MailegantIniFile::isDebug()
{
	QString strXMLLocation;
	m_inifile.GetValue("Main", "XML_LOCATION", strXMLLocation);

	if(strXMLLocation.isEmpty())
	{
		return false;
	}

	return true;
}

QString MailegantIniFile::getXMLLocation()
{
	QString strXMLLocation;
	m_inifile.GetValue("Main", "XML_LOCATION", strXMLLocation);

	if(strXMLLocation.isEmpty())
	{
		strXMLLocation = XML_LOCATION;
	}
	return strXMLLocation;
}

int MailegantIniFile::getVersion()
{
	int nVersion;
	m_inifile.GetValue("Main", "nVersion", nVersion);
	return nVersion;
}

QString MailegantIniFile::getAppLocationURL()
{
	QString strAppLocationURL;
	m_inifile.GetValue("Main", "strAppLocationURL", strAppLocationURL);
	return strAppLocationURL;
}

void MailegantIniFile::setVersion(int nVersion)
{
	m_inifile.SetValue("Main", "nVersion", nVersion);
}

void MailegantIniFile::SaveIni()
{
	m_inifile.Save();
}
