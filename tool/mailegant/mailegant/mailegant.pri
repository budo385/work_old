# ----------------------------------------------------
# This file is generated by the Qt Visual Studio Add-in.
# ------------------------------------------------------

# This is a reminder that you are using a generated .pro file.
# Remove it when you are finished editing this file.
message("You are running qmake on a generated .pro file. This may not work!")


HEADERS += ./const_definitions.h \
    ./resource.h \
    ./about.h \
    ./downloadmanager.h \
    ./mailegant.h \
    ./mailegantinifile.h \
    ./unzipinitversion.h \
    ./aboutwidget.h
SOURCES += ./about.cpp \
    ./downloadmanager.cpp \
    ./mailegant.cpp \
    ./mailegantinifile.cpp \
    ./main.cpp \
    ./unzipinitversion.cpp \
    ./aboutwidget.cpp
FORMS += ./about.ui \
    ./mailegant.ui \
    ./aboutwidget.ui
RESOURCES += mailegant.qrc
