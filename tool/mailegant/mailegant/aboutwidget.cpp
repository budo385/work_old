#include "aboutwidget.h"

aboutWidget::aboutWidget(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);

	setWindowIcon(QIcon(":mailegant.ico"));
	setWindowFlags(Qt::Dialog);
	setAttribute(Qt::WA_DeleteOnClose);
	setWindowFlags(Qt::WindowCloseButtonHint);
	setStyleSheet("QWidget { background-color:black; color:white;}");

	ui.logo_label->setPixmap(QPixmap(":logo.png"));
	setWindowTitle("mailegant Writer");
}

aboutWidget::~aboutWidget()
{

}
