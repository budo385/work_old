#include "mailegant.h"
#include <QApplication>
#include "const_definitions.h"

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	QCoreApplication::setApplicationName(APP_NAME);
	mailegant w;
	w.show();
	return a.exec();
}
