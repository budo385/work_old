#include "mailegant.h"
#include "const_definitions.h"
#include "about.h"
#include "common/common/datahelper.h"
#include "common/common/zipcompression.h"
#include <QDomDocument>
#include <QDir>
#include <QWebFrame>
#include <QSslError>
#include <QNetworkReply>
#include "SMTPEmail/SMTPEmail/mimetext.h"
#include <QProcess>
#include <QMessageBox>
#include <QMenu>
#include <QMenuBar>

WebPage::WebPage(QObject* parent /*= 0*/) :
QWebPage(parent)
{
	connect(this, SIGNAL(featurePermissionRequested(QWebFrame*, QWebPage::Feature)), SLOT(permissionRequested(QWebFrame*, QWebPage::Feature)));
	connect(this, SIGNAL(featurePermissionRequestCanceled(QWebFrame*, QWebPage::Feature)), this, SLOT(featurePermissionRequestCanceled(QWebFrame*, QWebPage::Feature)));
}

WebPage::~WebPage()
{
}

void WebPage::permissionRequested(QWebFrame* frame, QWebPage::Feature feature)
{
	setFeaturePermission(frame, feature, PermissionGrantedByUser);
}
void WebPage::featurePermissionRequestCanceled(QWebFrame*, QWebPage::Feature)
{
}

mailegant::mailegant(QWidget *parent, Qt::WindowFlags flags)
	: QMainWindow(parent, flags)
{
	ui.setupUi(this);

	setWindowIcon(QIcon(":mailegant.ico"));
	setWindowTitle(QString("mailegant Writer") + QChar(169));

	QString strDirPath=QDir::toNativeSeparators(DataHelper::GetApplicationHomeDir()+QDir::separator()+TEMPLATES_DIR);
	QDir dir(strDirPath);
	if (!dir.exists())
	{
		dir.mkdir(strDirPath);
	}

	//Geolocation support.
	m_pPage=new WebPage();
	ui.webView->setPage(m_pPage);

	m_pPage->settings()->setAttribute(QWebSettings::JavaEnabled, true);
	m_pPage->settings()->setAttribute(QWebSettings::JavascriptEnabled, true);
	m_pPage->settings()->setAttribute(QWebSettings::LocalStorageEnabled, true);
	if (m_iniFile.isDebug())
	{
		m_pPage->settings()->setAttribute(QWebSettings::DeveloperExtrasEnabled, true);
	}

	ui.webView->settings()->setOfflineWebApplicationCachePath(strDirPath);
	ui.webView->settings()->setLocalStoragePath(strDirPath);
	ui.webView->settings()->setOfflineStoragePath(strDirPath);
	ui.webView->settings()->setAttribute(QWebSettings::LocalContentCanAccessRemoteUrls,true); 

	m_pInstallAddIn = new QAction(tr("Install Outlook Add-In"),this);
	connect(m_pInstallAddIn, SIGNAL(triggered()), this, SLOT(on_menuInstall_Outlook_Add_In()));

	m_pReload = new QAction(tr("Reload"),this);
	m_pReload->setMenuRole(QAction::ApplicationSpecificRole);
	connect(m_pReload, SIGNAL(triggered()), this, SLOT(on_reload_clicked()));

	m_pAbout = new QAction(tr("About"),this);
	connect(m_pAbout, SIGNAL(triggered()), this, SLOT(on_About()));

	m_pExit = new QAction(tr("Exit"),this);
	connect(m_pExit, SIGNAL(triggered()), this, SLOT(close()));

	QMenu *helpMenu = new QMenu(tr("&mailegant"));
	helpMenu->addAction(m_pInstallAddIn);
	helpMenu->addAction(m_pReload);
	helpMenu->addAction(m_pAbout);
	helpMenu->addAction(m_pExit);

#ifndef __APPLE__
	menuBar()->addMenu(helpMenu);
#endif

/*	menuBar()->addAction(m_pInstallAddIn);
	menuBar()->addAction(m_pReload);
	menuBar()->addAction(m_pAbout);
	menuBar()->addAction(m_pExit);*/
/*	ui.menuMailegant->addAction(m_pInstallAddIn);
	ui.menuMailegant->addAction(m_pReload);
	ui.menuMailegant->addAction(m_pAbout);
	ui.menuMailegant->addAction(m_pExit);*/

	connect(m_pPage->networkAccessManager(), SIGNAL(sslErrors(QNetworkReply*, const QList<QSslError> & )), this, SLOT(handleSslErrors(QNetworkReply*, const QList<QSslError> & )));  
	getAppXML();
}

mailegant::~mailegant()
{
	m_pAboutWidg=NULL;
}

void mailegant::openMainWin(QString strTmpAppLocation)
{
	setLoadLocation();

	connect(m_pPage->mainFrame(), SIGNAL(javaScriptWindowObjectCleared()), this, SLOT(populateJavaScriptWindowObject()));
}

void mailegant::getAppXML()
{
	QString strXML= m_iniFile.getXMLLocation();
	QUrl url(strXML);
	m_xmldDownload.downloadByteArray(url);
	QObject::connect(&m_xmldDownload, SIGNAL(finishedTotal(QList<QByteArray>,QStringList)), this, SLOT(finishedDownloadXML(QList<QByteArray>,QStringList)));
	QObject::connect(&m_xmldDownload, SIGNAL(errorDownload(QString)), this, SLOT(errorDownloadXML(QString)));
}

void mailegant::setLoadLocation()
{
	m_pPage->mainFrame()->load(QUrl(m_iniFile.getAppLocationURL()));
}

void mailegant::on_menuInstall_Outlook_Add_In()
{
	QProcess proc_setup;
	proc_setup.startDetached("setup");
}

void mailegant::on_About()
{
	m_pAboutWidg = new aboutWidget();
	m_pAboutWidg->show();
	//about dlg(this);
	//dlg.exec();
}

void mailegant::on_reload_clicked()
{
	setLoadLocation();
}

void mailegant::finishedDownloadXML(QList<QByteArray> lstBytOutput, QStringList lstSavedFiles)
{
	QObject::disconnect(&m_xmldDownload, SIGNAL(finishedTotal(QList<QByteArray>,QStringList)), this, SLOT(finishedDownloadXML(QList<QByteArray>,QStringList)));
	QObject::disconnect(&m_xmldDownload, SIGNAL(errorDownload(QString)), this, SLOT(errorDownloadXML(QString)));

	QDomDocument xml;
	xml.setContent(lstBytOutput.at(0));

	m_nTmpVersion = xml.elementsByTagName("version").item(0).firstChild().nodeValue().toInt();
	m_strTmpAppLocation = xml.elementsByTagName("apploc").item(0).firstChild().nodeValue();
	QString strAppDownloadLocation = xml.elementsByTagName("appdownload").item(0).firstChild().nodeValue();
	QUrl url(strAppDownloadLocation);

//	//QMessageBox::information(NULL, "strAppDownloadLocation", strAppDownloadLocation);
//	//QMessageBox::information(NULL, "m_nTmpVersion", QVariant(m_nTmpVersion).toString());
//	//QMessageBox::information(NULL, "m_iniFile.getVersion()", QVariant(m_iniFile.getVersion()).toString());

	if(m_nTmpVersion>m_iniFile.getVersion())
	{
//		//QMessageBox::information(NULL, "1", "1");

		QString strHtmlPath=QDir::toNativeSeparators(DataHelper::GetApplicationHomeDir()+QDir::separator());
		m_xmldDownload.downloadFile(url, strHtmlPath);

//		//QMessageBox::information(NULL, "strHtmlPath", strHtmlPath);
//		//QMessageBox::information(NULL, "2", "2");

		QObject::connect(&m_xmldDownload, SIGNAL(finishedTotal(QList<QByteArray>,QStringList)), this, SLOT(finishedZipDownload(QList<QByteArray>,QStringList)));
		QObject::connect(&m_xmldDownload, SIGNAL(errorDownload(QString)), this, SLOT(errorDownloadZIP(QString)));

		return;
	}

	openMainWin(INIT_HTML_LOCATION);
}

void mailegant::finishedZipDownload(QList<QByteArray> lstBytOutput, QStringList lstSavedFiles)
{
//	//QMessageBox::information(NULL, "3", "3");
	QObject::disconnect(&m_xmldDownload, SIGNAL(finishedTotal(QList<QByteArray>,QStringList)), this, SLOT(finishedZipDownload(QList<QByteArray>,QStringList)));
	QObject::disconnect(&m_xmldDownload, SIGNAL(errorDownload(QString)), this, SLOT(errorDownloadZIP(QString)));

	QStringList lstData;
	QString strHtmlPath=QDir::toNativeSeparators(DataHelper::GetApplicationHomeDir()+QDir::separator()+HTML_DIR);

	QString strSavedZip = lstSavedFiles.value(0);

//	//QMessageBox::information(NULL, "4", "4");
//	//QMessageBox::information(NULL, "zip path", strSavedZip);
//	//QMessageBox::information(NULL, "strHtmlPath path", strHtmlPath);

//	//QMessageBox::information(NULL, "strSavedZip", strSavedZip);

	if(ZipCompression::Unzip(strSavedZip ,strHtmlPath,lstData))
	{
//		//QMessageBox::information(NULL, "zip succedded", "zip succedded");
		/*for (int i=0; i< lstData.count(); i++)
		{
			//QMessageBox::information(NULL, "lst data", lstData.value(i));
		}*/

		m_iniFile.setVersion(m_nTmpVersion);
		m_iniFile.SaveIni();

		openMainWin(m_strTmpAppLocation);
		QFile::remove(lstSavedFiles.value(0));
		return;
	}
	else
	{
//		//QMessageBox::information(NULL, "zip not succedded", "zip not succedded");
		QFile::remove(lstSavedFiles.value(0));
	}

	/*for (int i=0; i< lstData.count(); i++)
	{
		//QMessageBox::information(NULL, "lst data", lstData.value(i));
	}*/

	QFile::remove(lstSavedFiles.value(0));

	openMainWin(INIT_HTML_LOCATION);
}

void mailegant::errorDownloadXML(QString error)
{
	QObject::disconnect(&m_xmldDownload, SIGNAL(finishedTotal(QList<QByteArray>,QStringList)), this, SLOT(finishedDownloadXML(QList<QByteArray>,QStringList)));
	QObject::disconnect(&m_xmldDownload, SIGNAL(errorDownload(QString)), this, SLOT(errorDownloadXML(QString)));

	openMainWin(INIT_HTML_LOCATION);
}

void mailegant::errorDownloadZIP(QString error)
{
	////QMessageBox::information(NULL, "5", "5");
	QObject::disconnect(&m_xmldDownload, SIGNAL(finishedTotal(QList<QByteArray>,QStringList)), this, SLOT(finishedZipDownload(QList<QByteArray>,QStringList)));
	QObject::disconnect(&m_xmldDownload, SIGNAL(errorDownload(QString)), this, SLOT(errorDownloadZIP(QString)));

	////QMessageBox::information(NULL, "errorDownloadZIP", "");

	openMainWin(INIT_HTML_LOCATION);
}

int mailegant::testConnection(SmtpClient &smtp)
{
	smtp.setConnectionType(SmtpClient::TcpConnection);
	if(smtp.connectToHost())
		return 1;

	smtp.setConnectionType(SmtpClient::TlsConnection);
	if(smtp.connectToHost())
		return 2;

	smtp.setConnectionType(SmtpClient::SslConnection);
	if(smtp.connectToHost())
		return 3;
}

bool mailegant::sendEmail(QVariantMap map)
{
	QString strServer	= map.value("server").toString();
	int nPort			= map.value("port").toInt();
	int nConnectionType	= map.value("connType").toInt();
	int useAuth			= map.value("useAuth").toInt();
	QString strUserName	= map.value("username").toString();
	QString strPassword	= map.value("pass").toString();
	QString strMime		= map.value("mime").toString();
	QString strFullFrom	= map.value("fullFrom").toString();
	QString strFrom		= map.value("from").toString();
	QString strTo		= map.value("to").toString();
	QString strCC		= map.value("cc").toString();
	QString strBCC		= map.value("bcc").toString();

	QStringList lstTo = strTo.split(",");
	QStringList lstCC = strCC.split(",");
	QStringList lstBCC = strBCC.split(",");

	MimeMessage message;
	message.setSender(new EmailAddress(strFrom));

	//Set to.
	for (int i=0; i<lstTo.length(); i++)
	{
		if(!lstTo.at(i).isEmpty())
		{
			message.addTo(new EmailAddress(lstTo.at(i)));
		}
	}
	//Set CC.
	for (int i=0; i<lstCC.length(); i++)
	{
		if(!lstCC.at(i).isEmpty())
		{
			message.addCc(new EmailAddress(lstCC.at(i)));
		}
	}
	//Set BCC.
	for (int i=0; i<lstBCC.length(); i++)
	{
		if(!lstBCC.at(i).isEmpty())
		{
			message.addBcc(new EmailAddress(lstBCC.at(i)));
		}
	}

	/////////////////////////////////////////////////////////////////////////
	SmtpClient smtp(strServer, nPort);
	smtp.setUser(strUserName);
	smtp.setPassword(strPassword);
	useAuth ? smtp.setAuthMethod(SmtpClient::AuthLogin) : smtp.setAuthMethod(SmtpClient::AuthPlain);

	bool bCanConnect = false;
	if(nConnectionType==0)
	{
		if (testConnection(smtp)>=0)
		{
			bCanConnect = true;
		}
		else
		{
			bCanConnect = false;
		}
	}
	else
	{
		if (nConnectionType==1)
		{
			smtp.setConnectionType(SmtpClient::TcpConnection);
		}
		else if (nConnectionType==2)
		{
			smtp.setConnectionType(SmtpClient::TlsConnection);
		}
		else if (nConnectionType==3)
		{
			smtp.setConnectionType(SmtpClient::SslConnection);
		}
		bCanConnect = smtp.connectToHost();
	}

	if (!bCanConnect)
	{
		smtp.quit();
		return false;
	}
		
	QString mimeMessage = strFullFrom +"\n"+strMime;
	//QString mimeMessage = QString("From: \"Marko\" <marko.perutovic@gmail.com>") +"\n"+strMime;

	if(!smtp.login())
	{
		smtp.quit();
		return false;
 	}
	if (!smtp.sendMailWithMime(message, mimeMessage))
	{
		smtp.quit();
		return false;
	}

	smtp.quit();
	return true;
}

bool mailegant::testEmailSettings(QVariantMap map)
{
	QString strServer	= map.value("server").toString();
	int nPort			= map.value("port").toInt();
	int nConnectionType	= map.value("connType").toInt();
	int useAuth			= map.value("useAuth").toInt();
	QString strUserName	= map.value("username").toString();
	QString strPassword	= map.value("pass").toString();

//	useAuth			= 1;

	SmtpClient smtp(strServer, nPort);
	smtp.setUser(strUserName);
	smtp.setPassword(strPassword);
	useAuth ? smtp.setAuthMethod(SmtpClient::AuthLogin) : smtp.setAuthMethod(SmtpClient::AuthPlain);

	bool bCanConnect = false;
	if(nConnectionType==0)
	{
		if (testConnection(smtp)>=0)
		{
			bCanConnect = true;
		}
		else
		{
			bCanConnect = false;
		}
	}
	else
	{
		if (nConnectionType==1)
		{
			smtp.setConnectionType(SmtpClient::TcpConnection);
		}
		else if (nConnectionType==2)
		{
			smtp.setConnectionType(SmtpClient::TlsConnection);
		}
		else if (nConnectionType==3)
		{
			smtp.setConnectionType(SmtpClient::SslConnection);
		}
		bCanConnect = smtp.connectToHost();
	}

	if (!bCanConnect)
	{
		smtp.quit();
		return false;
	}

	if(!smtp.login())
	{
		smtp.quit();
		return false;
	}

	smtp.quit();
	return true;
}

void mailegant::callWrapper(QString eventType, QString ScottyEventName, int numPar1, int numPar2, QString strPar1,  QString strPar2, QString strPar3, QString strPar4, 
							QString strPar5, QString strPar6, QString strPar7, QString strPar8, QString strPar9, QString strPar10, QString strPar11, QString strPar12, QString strPar13)
{
    if(eventType=="eventScotty")
    {
	    m_pPage->mainFrame()->evaluateJavaScript("eventDispatcher('"+ScottyEventName+"')");
    }
    else if(eventType=="selectPics")
    {
		/*var selectPics=require('selectPics');
    	selectPics.select(numPar1,numPar2,editor_webview,arrowDirection);*/
    } 
    else if(eventType=="sendemail")
    {


		/*var selectPics=require('selectPics');
    	selectPics.select(numPar1,numPar2,editor_webview,arrowDirection);*/
    }
    /*else if(eventType=='getScreenPic')
    {
		var xOffSet=parseInt(strPar1);
		var yOffSet=parseInt(strPar2);
		var cropWidth=parseInt(strPar3);
		var cropHeight=parseInt(strPar4);
		var resizeWidth=parseInt(strPar5);
	
		var scrShot=require('screenShot');
		scrShot.getScreenShot(xOffSet,yOffSet,cropWidth,cropHeight,resizeWidth);
    }
    else if(eventType=='conv2DataURI')
    {
		var conv2DataURI=require('conv2DataURI');
    	conv2DataURI.convURI(strPar1,win1);
    }
    else if(eventType=='scalePic')
    {
		var scalePic=require('scalePic');
		scalePic.scalePic(strPar1, numPar1, numPar2, win1);
    }
    else if(eventType=='openEmailDialog')
    {
		var strTo=strPar1;
		var strBcc=strPar2;
		var strCC=strPar3;
		var subject=strPar4;
		var body=strPar5;
		var strAttachments=strPar6;
	
		var mailDialog=require('openMailDialog');
		mailDialog.open(strTo,strBcc,strCC,subject,body,strAttachments);
    }
    else if(eventType=='setContentHeight')
    {
		editor_webview.height=numPar1;
    }
/*    else if(eventType=='setHeaderHeight')
    {
		setHeadHeight(numPar1);
    }
    else if(eventType=='setSidebarWidth')
    {
		setSidebarWidth(numPar1);
    }*/
    /*else if(eventType=='selectContact')
    {
		var selectContact=require('selectContact');
		selectContact.select(editor_webview);
    }
    else if(eventType=='getFeatures')
    {
		var getFeatures=require('getFeatures');
		var evalStr=getFeatures.getFeatures();
    	editor_webview.evalJS("registerFeatures('"+evalStr+"')");
    }
    else if(eventType=='inAppPurchase')
    {
		var inAppWindow = require('in_app_purchase');
		new inAppWindow().open({modal:true});
    }
    else if(eventType=='liftCurtain')
    {
		liftCourtainF();
    }
    else if(eventType=='takePics')
    {
		var fireUpCamera=require('fireUpCamera');
    	fireUpCamera.fireUpTheCamera(numPar1, numPar2);
    }*/

}

void mailegant::populateJavaScriptWindowObject()
{

	m_pPage->mainFrame()->addToJavaScriptWindowObject(QString("mailegant"), this);

}

void mailegant::handleSslErrors(QNetworkReply * reply, const QList<QSslError> & errors)
{
	qDebug() << "handleSslErrors: ";
	foreach (QSslError e, errors)
	{
		qDebug() << "ssl error: " << e;
	}

	reply->ignoreSslErrors();
}

void mailegant::writeFile(QString relativePath, QString data)
{
	QString strFilePath=QDir::toNativeSeparators(DataHelper::GetApplicationHomeDir()+QDir::separator()+relativePath/*+QDir::separator()*/);
	QString strDirPath=QDir::toNativeSeparators(DataHelper::GetApplicationHomeDir()+QDir::separator()+TEMPLATES_DIR);
	QDir dir(strDirPath);
	if (!dir.exists())
	{
		dir.mkdir(strDirPath);
	}

	QFile file(strFilePath);
	if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
		return;

	QTextStream out(&file);
	out << data;

	file.close();

	m_pPage->mainFrame()->evaluateJavaScript(QString("registerFileManagerWriteResult('%1',1)").arg(relativePath));
}

void mailegant::readFile(QString relativePath, int start, int end)
{
 	QString strFilePath=QDir::toNativeSeparators(DataHelper::GetApplicationHomeDir()+QDir::separator()+relativePath/*+QDir::separator()*/);
	QFile file(strFilePath);
	if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		m_pPage->mainFrame()->evaluateJavaScript("registerFileManagerSuccessResult(1,'')");
		return;
	}

	QTextStream out(&file);
	QString fileContent = out.readAll();
	fileContent = fileContent.replace("'", "&#39;");
	QString fileToSend = fileContent.mid(start, end-start);

	int i = fileContent.length();
	if (fileContent.length()>end)
	{
		m_pPage->mainFrame()->evaluateJavaScript(QString("registerFileManagerSuccessResult(0,'%1')").arg(fileToSend));
	}
	else
	{
		m_pPage->mainFrame()->evaluateJavaScript(QString("registerFileManagerSuccessResult(1,'%1')").arg(fileToSend));
	}
	
	file.close();
}
