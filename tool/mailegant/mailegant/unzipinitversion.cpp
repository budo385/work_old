#include "unzipinitversion.h"
#include "const_definitions.h"
#include "common/common/datahelper.h"
#include "common/common/zipcompression.h"
#include <QDir>

UnzipInitVersion::UnzipInitVersion(QObject *parent)
	: QObject(parent)
{

}

UnzipInitVersion::~UnzipInitVersion()
{

}

void UnzipInitVersion::Unzip(QStringList lstData)
{
	QString strHtmlPath=QDir::toNativeSeparators(DataHelper::GetApplicationHomeDir()+QDir::separator()+HTML_DIR+QDir::separator());
	bool result = ZipCompression::Unzip(INIT_ZIP_FILE,strHtmlPath,lstData);
}
