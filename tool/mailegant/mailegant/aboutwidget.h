#ifndef ABOUTWIDGET_H
#define ABOUTWIDGET_H

#include <QWidget>
#include "ui_aboutwidget.h"	

class aboutWidget : public QWidget
{
	Q_OBJECT

public:
	aboutWidget(QWidget *parent = 0);
	~aboutWidget();

private:
	Ui::aboutWidget ui;
};

#endif // ABOUTWIDGET_H
