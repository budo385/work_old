#define INIT_VERSION 100
#define INIT_ZIP_FILE "init_zip.zip"

#define APP_NAME "Mailegant"
#define INIT_HTML_LOCATION "index.html"
#define INI_FILE_NAME "mailegant.ini"
#define HTML_DIR "html"
#define TEMPLATES_DIR "mw"
#define WEBKIT_PROTOCOL "file:///"

//	#define XML_LOCATION "http://mailegant.com/med.xml"
	#define XML_LOCATION "http://mailegant.com/mep.xml"
