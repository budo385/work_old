#!/bin/bash

CERT_NAME="3rd Party Mac Developer Application: Helix Business-Soft AG"
CERT_INSTALLER="3rd Party Mac Developer Installer: Helix Business-Soft AG"
APP_NAME=Mailegant
APP_BUNDLE=$APP_NAME.app
SETUP_NAME=MailegantSetupAppStore
APP_BUNDLE_ID=com.mailegant.Mailegant
APP_VERSION=1.0.0

CLEAN_OBJS=1

echo "------------------------------------------------------------"
echo " Compiling $APP_NAME"
echo "------------------------------------------------------------"

#update all sources from svn
cd ../../
svn up
cd tool/mailegant
chmod +x ./compile.sh

#cleanup builds
echo "cleanup builds"
rm -rf ./debug

#compile common
echo "compile common lib"
if [ "$CLEAN_OBJS" == "1" ]; then
	rm -rf ../../lib/common/common/debug
fi
rm -f ../../lib/common/common/Makefile
cd ../../lib/common/common/
qmake -makefile -spec macx-g++ -Wall -o Makefile common.pro
if [ "$CLEAN_OBJS" == "1" ]; then
  make clean
fi
make all  || exit 1
cd ../../../tool/mailegant
cp ../../lib/debug/libcommon.a  ./ || exit 1

#compile SMTPEmail
echo "compile SMTPEmail lib"
rm -f ../../lib/SMTPEmail/SMTPEmail/Makefile
cd ../../lib/SMTPEmail/SMTPEmail/
qmake -makefile -spec macx-g++ -Wall -o Makefile SMTPEmail.pro
if [ "$CLEAN_OBJS" == "1" ]; then
  make clean
fi
make all || exit 1
cd ../../../tool/mailegant
cp ../../lib/debug/libSMTPEmail.a  ./ || exit 1

#final compilation
echo "compile mailegant"
rm -f ./Makefile
qmake -makefile -spec macx-g++ -Wall -o Makefile ./mailegant/mailegant.pro
if [ "$CLEAN_OBJS" == "1" ]; then
  make clean
fi
make all || { echo "mailegant compilation failed"; exit 1; }

#TOFIX copy all the required files to the mailegant.app bundle, create dirs too
#copy all the required files to the Sokrates-SPC.app bundle, create dirs too
echo "Copy files to bundle"
install -d -m755 ./debug/$APP_BUNDLE/Contents/MacOS/ || echo "Some files were not copied"
install -m644 ../../deploy/Setup/Mailegant2/*.icns ./Debug/$APP_BUNDLE/Contents/Resources/ || echo "Some files were not copied"
install -m644 ./mailegant/init_zip.zip ./Debug/$APP_BUNDLE/Contents/MacOS/ || echo "Some files were not copied"

#delete .svn folders
find ./debug/$APP_BUNDLE/Contents/MacOS/ -name ".svn" | xargs rm -rf \;

#copies all the Qt dependencies into the main bundle
macdeployqt ./debug/$APP_BUNDLE

mkdir -p ./debug/Resources
cp -R ../../deploy/Setup/Mailegant2/license.txt ./debug/Resources/License.txt

# use our own Info file to overwrite auto-generated one
cp -R ../../deploy/Setup/Mailegant2/Info.plist ./debug/$APP_BUNDLE/Contents/Info.plist

# NOTE: this non-signed version is for ordinary download (not for MacStore!!!)
# create flat package (.pkg)
/Applications/PackageMaker.app/Contents/MacOS/PackageMaker -AppleLanguages "(English)" -r ./debug/$APP_BUNDLE/ -o ./Debug/MailegantInstall.pkg -v $APP_VERSION -t "Mailegant Setup" -i $APP_BUNDLE_ID --no-relocate --verbose --target 10.5 --resources ./debug/Resources/

echo "signing the APP";
codesign --verbose -fs "3rd Party Mac Developer Application: Helix Business-Soft AG" ./debug/Mailegant.app

productbuild --component ./debug/Mailegant.app /Applications --sign "3rd Party Mac Developer Installer: Helix Business-Soft AG" ./debug/$SETUP_NAME.pkg

sudo installer -store -pkg ./debug/$SETUP_NAME.pkg  -target /