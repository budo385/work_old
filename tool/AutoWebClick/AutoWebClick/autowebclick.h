#ifndef AUTOWEBCLICK_H
#define AUTOWEBCLICK_H

#include <QtGui/QMainWindow>
#include "ui_autowebclick.h"
#include <QWebElement>
#include <QProgressBar>

#define LOAD_ABSOLUTE_URL 0
#define LOAD_RELATIVE_URL 1
#define FORM_FLD_FIND 2
#define FORM_FLD_SET_VALUE 3
#define FORM_FLD_EXE_JAVA_SCRIPT_WAIT_FOR_LOAD_URL 4
#define FORM_FLD_EXE_JAVA_SCRIPT 5
#define FORM_FLD_EXE_JAVA_SCRIPT_IGNORE_LOAD_URL_ERR 6
#define FORM_FLD_EXE_JAVA_SCRIPT_FORCE_EXIT 7
#define FORM_FLD_SET_ATTR 8
#define WAIT 9


#define STATE_STANDBY	0
#define STATE_EXECUTING 1



//helper data class defining each web action
class WebAction
{
	public:
	
	WebAction(int nAction, QString strValue){m_nAction=nAction;m_strValue=strValue;};

	int		m_nAction;
	QString m_strValue;
};


/*!
	\class AutoWebClick
	\brief Main GUI window & logic

	Parses script file and executed command by command in the QWebView control
*/


//main gui class
class AutoWebClick : public QMainWindow
{
	Q_OBJECT

public:
	AutoWebClick(QWidget *parent = 0, Qt::WFlags flags = 0);
	~AutoWebClick();
	void SetCmdLineMode(QString strScriptPath){m_bCmdLineMode=true;m_strScriptPath=strScriptPath;};

public slots:
	void ExecuteFromCmdLine();

private slots:
	void on_btnChoose_clicked();
	void on_btnStart_clicked();
	void On_LoadFinished (bool bOk);
	void ExecuteScript();
	void ShowErrMsg();
	void SetSuccess();
	
private:
	void SetError(const QString &strErr);
	void PrepareForExecute();
	bool ParseScript(QString strPath);
	QString GetCommandName(int nCmd);
	
	Ui::AutoWebClickClass ui;
	QString		m_strLastError;
	

	int							m_nCurrentPos;
	QList<WebAction>			m_hshActions;

	QWebElement					m_LastElement;
	QString						m_strLastAbsUrl;
	QString						m_strLastRelUrl;
	int							m_nState;
	bool						m_bIgnoreLoadErr;
	bool						m_bCmdLineMode;
	QString						m_strScriptPath;
	QProgressBar				*m_ProgresBar;

};

#endif // AUTOWEBCLICK_H
