#include "autowebclick.h"
#include <QSettings>
#include <QFileDialog>
#include <QMessageBox>
#include <QTimer>
#include <QUrl>
#include <QWebFrame>
#include <QDebug>



AutoWebClick::AutoWebClick(QWidget *parent, Qt::WFlags flags)
	: QMainWindow(parent, flags),m_nCurrentPos(-1),m_nState(STATE_STANDBY),m_bIgnoreLoadErr(false),m_bCmdLineMode(false)
{
	ui.setupUi(this);
	setAttribute(Qt::WA_QuitOnClose);

	setWindowTitle("Auto Web Click Version 1.0");

	QCoreApplication::setOrganizationName("Mandrilo");
	QCoreApplication::setOrganizationDomain("mandrilo.org");
	QCoreApplication::setApplicationName("Auto Web Click");

	//get last script:
	QSettings settings(QSettings::SystemScope,QCoreApplication::organizationName(),QCoreApplication::applicationName());
	QString strLastSelected=settings.value("LastScriptPath","").toString();
	if (!strLastSelected.isEmpty())
	{
		ui.lineEdit->setText(strLastSelected);
		m_strScriptPath=strLastSelected;
	}

	m_ProgresBar = new QProgressBar();
	m_ProgresBar->setMinimumWidth(200);
	m_ProgresBar->setMaximumWidth(200);
	m_ProgresBar->setMaximumHeight(20);
	m_ProgresBar->setRange(1,100);
	m_ProgresBar->setVisible(false);
	this->statusBar()->addPermanentWidget(m_ProgresBar);
	this->statusBar()->clearMessage();
}


AutoWebClick::~AutoWebClick()
{

}


void AutoWebClick::on_btnChoose_clicked()
{
	//get last dir:
	QString strDefaultPath=QCoreApplication::applicationDirPath();
	QSettings settings(QSettings::SystemScope,QCoreApplication::organizationName(),QCoreApplication::applicationName());
	QString strLastSelected=settings.value("LastScriptPath","").toString();
	
	if (!strLastSelected.isEmpty())
	{
		QFileInfo info(strLastSelected);
		strDefaultPath=info.absolutePath();
	}

	QString	strFile=QFileDialog::getOpenFileName ( this, "Select script for execution", strDefaultPath, "Script File (*.txt)");
	if (!strFile.isEmpty())
	{
		ui.lineEdit->setText(strFile);
		m_strScriptPath=strFile;
		settings.setValue("LastScriptPath",strFile);
	}
}

bool AutoWebClick::ParseScript(QString strPath)
{
	m_hshActions.clear();

	QDir::setCurrent(QCoreApplication::applicationDirPath());
	QFile file(strPath);
	if(file.open(QIODevice::ReadOnly))
	{
		QStringList lstCommands=QString(file.readAll()).split("\n",QString::SkipEmptyParts);
		file.close();

		int nSize=lstCommands.size();
		for (int i=0;i<nSize;i++)
		{
			QStringList lstParts=lstCommands.at(i).split("\t",QString::SkipEmptyParts);
			if (lstParts.size()==2)
			{
				int nAction=-1;
				QString strCmd=lstParts.at(0);

				if (strCmd=="LOAD_ABSOLUTE_URL")
					nAction=LOAD_ABSOLUTE_URL;
				else if (strCmd=="LOAD_RELATIVE_URL")
					nAction=LOAD_RELATIVE_URL;
				else if (strCmd=="FORM_FLD_FIND")
					nAction=FORM_FLD_FIND;
				else if (strCmd=="FORM_FLD_SET_VALUE")
					nAction=FORM_FLD_SET_VALUE;
				else if (strCmd=="FORM_FLD_EXE_JAVA_SCRIPT_WAIT_FOR_LOAD_URL")
					nAction=FORM_FLD_EXE_JAVA_SCRIPT_WAIT_FOR_LOAD_URL;
				else if (strCmd=="FORM_FLD_EXE_JAVA_SCRIPT")
					nAction=FORM_FLD_EXE_JAVA_SCRIPT;
				else if (strCmd=="FORM_FLD_EXE_JAVA_SCRIPT_IGNORE_LOAD_URL_ERR")
					nAction=FORM_FLD_EXE_JAVA_SCRIPT_IGNORE_LOAD_URL_ERR;
				else if (strCmd=="FORM_FLD_EXE_JAVA_SCRIPT_IGNORE_LOAD_URL_ERR")
					nAction=FORM_FLD_EXE_JAVA_SCRIPT_IGNORE_LOAD_URL_ERR;
				else if (strCmd=="FORM_FLD_EXE_JAVA_SCRIPT_FORCE_EXIT")
					nAction=FORM_FLD_EXE_JAVA_SCRIPT_FORCE_EXIT;
				else if (strCmd=="FORM_FLD_SET_ATTR")
					nAction=FORM_FLD_SET_ATTR;
				else if (strCmd=="WAIT")
					nAction=WAIT;

				if (nAction==-1)continue;
				WebAction action(nAction,lstParts.at(1).trimmed());
				m_hshActions.append(action);
			}
		}


		return true;
	}

	return false;

}

QString AutoWebClick::GetCommandName(int nCmd)
{
	QString strCmd="";

	if (nCmd==LOAD_ABSOLUTE_URL)
		strCmd="LOAD_ABSOLUTE_URL";
	else if (nCmd==LOAD_RELATIVE_URL)
		strCmd="LOAD_RELATIVE_URL";
	else if (nCmd==FORM_FLD_FIND)
		strCmd="FORM_FLD_FIND";
	else if (nCmd==FORM_FLD_SET_VALUE)
		strCmd="FORM_FLD_SET_VALUE";
	else if (nCmd==FORM_FLD_EXE_JAVA_SCRIPT_WAIT_FOR_LOAD_URL)
		strCmd="FORM_FLD_EXE_JAVA_SCRIPT_WAIT_FOR_LOAD_URL";
	else if (nCmd==FORM_FLD_EXE_JAVA_SCRIPT)
		strCmd="FORM_FLD_EXE_JAVA_SCRIPT";
	else if (nCmd==FORM_FLD_EXE_JAVA_SCRIPT_IGNORE_LOAD_URL_ERR)
		strCmd="FORM_FLD_EXE_JAVA_SCRIPT_IGNORE_LOAD_URL_ERR";
	else if (nCmd==FORM_FLD_EXE_JAVA_SCRIPT_IGNORE_LOAD_URL_ERR)
		strCmd="FORM_FLD_EXE_JAVA_SCRIPT_IGNORE_LOAD_URL_ERR";
	else if (nCmd==FORM_FLD_EXE_JAVA_SCRIPT_FORCE_EXIT)
		strCmd="FORM_FLD_EXE_JAVA_SCRIPT_FORCE_EXIT";
	else if (nCmd==FORM_FLD_SET_ATTR)
		strCmd="FORM_FLD_SET_ATTR";
	else if (nCmd==WAIT)
		strCmd="WAIT";

	return strCmd;
}

void AutoWebClick::ExecuteFromCmdLine()
{
	if(!ParseScript(m_strScriptPath))
	{
		QMessageBox::critical(this,tr("Error"),"Error while parsing script. Invalid format or path.");
		close();
		return;
	}

	PrepareForExecute();
	QTimer::singleShot(0,this,SLOT(ExecuteScript()));
}

void AutoWebClick::on_btnStart_clicked()
{
	//parse script:
	if(!ParseScript(ui.lineEdit->text()))
	{
		QMessageBox::critical(this,tr("Error"),"Error while parsing script. Invalid format or path.");
		return;
	}

	//prepare to start script
	PrepareForExecute();
	QTimer::singleShot(0,this,SLOT(ExecuteScript()));
}

//Note: as webview is async, QTimer::singleShot() is used to return execution to the loop
//On each url changed slot On_LoadFinished() is invoked and result is checked.
void AutoWebClick::ExecuteScript()
{
	if (m_nState!=STATE_EXECUTING)
		return;

	if (m_nCurrentPos>=m_hshActions.size())
	{
		SetError("Error while initializing script!");
		return;
	}

	while (m_nCurrentPos<(m_hshActions.size()-1)) 
	{
		m_nCurrentPos++;
		int nAction = m_hshActions.at(m_nCurrentPos).m_nAction;
		QString strValue=m_hshActions.at(m_nCurrentPos).m_strValue;
		this->statusBar()->showMessage(QString("Executing command %1 with value \"%2\"").arg(GetCommandName(nAction)).arg(strValue));
		int nVal=(float)m_nCurrentPos/m_hshActions.size()*100;
		if (nVal==0)nVal=1;
		m_ProgresBar->setValue(nVal);
		QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents);


		switch(nAction)
		{
		case LOAD_ABSOLUTE_URL:
			{
				m_strLastAbsUrl=strValue;
				ui.webView->load(strValue);
				return;
			}
			break;
		case LOAD_RELATIVE_URL:
			{
				QStringList lstParts=ui.webView->url().path().split("/",QString::SkipEmptyParts);
				if (lstParts.size()>0)
					lstParts.removeLast();
				QString strNewUrl="";
				if (lstParts.size()>0)
					strNewUrl=ui.webView->url().scheme()+"://"+ui.webView->url().host()+"/"+lstParts.join("/");
				else
					strNewUrl=ui.webView->url().scheme()+"://"+ui.webView->url().host();

				strNewUrl+="/"+strValue;

				ui.webView->load(strNewUrl);
				return;
			}
			break;
		case FORM_FLD_FIND:
			{
				m_LastElement = ui.webView->page()->currentFrame()->findFirstElement(strValue);
				if (m_LastElement.isNull())
				{
					SetError(QString("Can not find element with CSS selector: %1").arg(strValue));
					return;
				}
				else
					continue;
			}
		    break;
		case FORM_FLD_SET_VALUE:
			{
				if (m_LastElement.isNull())
				{
					SetError(QString("Can not find web element"));
					return;
				}
				else
				{
					m_LastElement.evaluateJavaScript("this.value='"+strValue+"'");
					QTimer::singleShot(0,this,SLOT(ExecuteScript()));
					return;
				}
			}
		    break;
		case FORM_FLD_SET_ATTR:
			{
				if (m_LastElement.isNull())
				{
					SetError(QString("Can not find web element"));
					return;
				}
				else
				{
					QStringList lstParts=strValue.split("=");
					if (lstParts.size()!=2)
					{
						SetError(QString("Error in attribute format %1. Must be in format: attribute_name=attribute_value").arg(strValue));
						return;
					}
					m_LastElement.setAttribute(lstParts.at(0),lstParts.at(1));
					continue;
				}
			}
			break;


			
		case FORM_FLD_EXE_JAVA_SCRIPT_WAIT_FOR_LOAD_URL: //exe script and in SLOT: onload page wait to trigger back to loop
			{
				if (m_LastElement.isNull())
				{
					SetError(QString("Can not find web element"));
					return;
				}
				else
				{
					m_LastElement.evaluateJavaScript(strValue);
					return;
				}
			}
			break;
		case FORM_FLD_EXE_JAVA_SCRIPT_IGNORE_LOAD_URL_ERR: //exe script and in SLOT: onload page wait to trigger back to loop
			{
				if (m_LastElement.isNull())
				{
					SetError(QString("Can not find web element"));
					return;
				}
				else
				{
					m_bIgnoreLoadErr=true;
					m_LastElement.evaluateJavaScript(strValue);
					return;
				}
			}
			break;

		case FORM_FLD_EXE_JAVA_SCRIPT_FORCE_EXIT: //exe script and in SLOT: onload page wait to trigger back to loop
			{
				if (m_LastElement.isNull())
				{
					SetError(QString("Can not find web element"));
					return;
				}
				else
				{
					m_bIgnoreLoadErr=true;
					disconnect(ui.webView,SIGNAL(loadFinished ( bool)),this,SLOT(On_LoadFinished (bool)));
					m_LastElement.evaluateJavaScript(strValue);
					QTimer::singleShot(2000,this,SLOT(SetSuccess())); //exit after 2sek
					return;
				}
			}
			break;

			

		case FORM_FLD_EXE_JAVA_SCRIPT: //exe script and return to loop
			{
				if (m_LastElement.isNull())
				{
					SetError(QString("Can not find web element"));
					return;
				}
				else
				{
					m_LastElement.evaluateJavaScript(strValue);
					QTimer::singleShot(0,this,SLOT(ExecuteScript())); //return to exe loop after 5sek
					return;
				}
			}
			break;

			

		case WAIT: //wait N ms
			{
				if (strValue.toInt()<0 || strValue.toInt()>60000)
				{
					strValue=1000;
				}

				if (strValue.toInt()==0) //just exe next command
					return;

				QTimer::singleShot(strValue.toInt(),this,SLOT(ExecuteScript())); //return to exe loop after 5sek
				return;
			}
			break;
	

			
		default:
			SetError("Unknown command. Aborting.");
			return;
		    break;
		}

	}

	SetSuccess();

}

void AutoWebClick::On_LoadFinished (bool bOk)
{
	if (bOk || m_bIgnoreLoadErr)
	{
		if (m_bIgnoreLoadErr) m_bIgnoreLoadErr=false; //reset flag
		QTimer::singleShot(0,this,SLOT(ExecuteScript())); //return to exe loop after 5sek
	}
	else
		SetError(QString("Error while loading url: %1").arg(ui.webView->url().toString()));
	
}




void AutoWebClick::SetError(const QString &strErr)
{
	m_nState=STATE_STANDBY;
	m_nCurrentPos = -1;
	m_strLastError=strErr;
	disconnect(ui.webView,SIGNAL(loadFinished ( bool)),this,SLOT(On_LoadFinished (bool)));
	ui.webView->stop();

	ui.btnChoose->setEnabled(true);
	ui.btnStart->setEnabled(true);

	this->statusBar()->clearMessage();
	m_ProgresBar->setVisible(false);

	QTimer::singleShot(0,this,SLOT(ShowErrMsg())); //return to exe loop

}

//taken from webView signal coz it cause crash
void AutoWebClick::ShowErrMsg()
{
	if (!m_strLastError.isEmpty())
	{
		QMessageBox::critical(this,tr("AutoWebClick"),m_strLastError);
		if (m_bCmdLineMode)
			close();
	}
}

void AutoWebClick::SetSuccess()
{
	m_nState=STATE_STANDBY;
	m_nCurrentPos = -1;
	disconnect(ui.webView,SIGNAL(loadFinished ( bool)),this,SLOT(On_LoadFinished (bool)));
	m_ProgresBar->setValue(100);
	QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents);

	QMessageBox::information(this,tr("AutoWebClick"),QString(tr("Script %1 successfully executed!")).arg(m_strScriptPath));
	if (m_bCmdLineMode)
		close();

	ui.btnChoose->setEnabled(true);
	ui.btnStart->setEnabled(true);

	this->statusBar()->clearMessage();
	m_ProgresBar->setVisible(false);

}
void AutoWebClick::PrepareForExecute()
{
	connect(ui.webView,SIGNAL(loadFinished ( bool)),this,SLOT(On_LoadFinished (bool)));
	
	m_nState=-1;
	m_LastElement=QWebElement();
	m_strLastAbsUrl="";
	m_nCurrentPos = -1;
	m_strLastError="";

	m_nState=STATE_EXECUTING;
	ui.btnChoose->setEnabled(false);
	ui.btnStart->setEnabled(false);

	this->statusBar()->clearMessage();
	m_ProgresBar->setValue(1);
	m_ProgresBar->setVisible(true);
	QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents);

}

