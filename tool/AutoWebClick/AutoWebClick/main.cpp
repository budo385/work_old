#include "autowebclick.h"
#include <QtGui/QApplication>
#include <QTimer>

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	AutoWebClick w;

	//with -execute parameter script will be executed from command line, but since application is compiled as GUI then GUI will be still opened
	//TODO: add option to go silent mode (no GUI, just execute)
	QStringList lstArgs=qApp->arguments();
	int nRow=lstArgs.indexOf("-execute");
	bool bCmdLineMode=false;
	if (nRow>0)
	{
		if ((nRow+1)<lstArgs.size())
		{
			QString strScriptPath=lstArgs.at(nRow+1);
			bCmdLineMode=true;
			w.SetCmdLineMode(strScriptPath);
			QTimer::singleShot(0,&w,SLOT(ExecuteFromCmdLine()));
		}
	}

	
	if (!bCmdLineMode)
		w.show();
	else
		w.showMinimized();

	return a.exec();
}
