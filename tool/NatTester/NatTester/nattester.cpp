#include "nattester.h"
#include <QFileDialog>

void Logger(int, const char *, unsigned long);

NatTester::NatTester(QWidget *parent, Qt::WFlags flags)
	: QDialog(parent, flags)
{
	ui.setupUi(this);
	ui.txtPort->setText("10000");
	
	m_objNAT.RegisterLogger(Logger, (unsigned long)this);
	m_objNAT.Initialize();
}

NatTester::~NatTester()
{
}

void NatTester::on_btnForward_clicked()
{
	int nPort = ui.txtPort->text().toInt();
	m_objNAT.StartPortForwardRequest(nPort);
}

void NatTester::on_btnStopForward_clicked()
{
	int nPort = ui.txtPort->text().toInt();
	m_objNAT.StopPortForwardRequest(nPort);
}

void NatTester::on_btnClose_clicked()
{
	done(0);
}

void NatTester::on_btnClear_clicked()
{
	ui.lstLog->clear();
}

void NatTester::AddLogEntry(QString strTxt)
{
	new QListWidgetItem(strTxt, ui.lstLog);
}

void Logger(int nMsg, const char *szText, unsigned long nData)
{
	NatTester *pObj = (NatTester *)nData;
	if(pObj){
		pObj->AddLogEntry(QString(szText));
	}
}

void NatTester::on_btnSaveLog_clicked()
{
	QString strFile = QFileDialog::getSaveFileName();
	if(!strFile.isEmpty()){
		QFile file(strFile);
		file.open(QIODevice::WriteOnly);
		int count = ui.lstLog->count();
		for(int i=0; i<count; i++){
			QString strMsg = ui.lstLog->item(i)->text();
			strMsg += "\r\n";
			file.write(strMsg.toLocal8Bit().constData(), strMsg.length());
		}

		file.close();
	}
}
