#include "nattester.h"
#include <QtGui/QApplication>
#ifdef _WIN32
 #include <windows.h>
#endif

int main(int argc, char *argv[])
{
#ifdef _WIN32
    //initiates use of socket DLL by a process
    WSADATA WsaData;
    if(0 != WSAStartup(0x0101,&WsaData))
    {
        //TRACE("ERROR: Winsock init failed.\n");
        return 1;
    }
#endif

	QApplication a(argc, argv);
	NatTester w;
	w.show();
	return a.exec();
}
