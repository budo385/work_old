#ifndef NATTESTER_H
#define NATTESTER_H

#include <QtGui/QDialog>
#include "ui_nattester.h"
#include "nattraversal.h"

class NatTester : public QDialog
{
	Q_OBJECT

public:
	NatTester(QWidget *parent = 0, Qt::WFlags flags = 0);
	~NatTester();

	NATTraversal m_objNAT;

	void AddLogEntry(QString strTxt);

private slots:
	void on_btnClose_clicked();
	void on_btnForward_clicked();
	void on_btnStopForward_clicked();
	void on_btnClear_clicked();
	void on_btnSaveLog_clicked();

private:
	Ui::NatTesterClass ui;
};

#endif // NATTESTER_H
