cd ../../
svn up
cd tool/NatTester
chmod +x ./compile.sh

make -C ../../lib/NATTraversal/NATTraversal/libnatpmp/ clean libnatpmp.a  || exit 1
make -C ../../lib/NATTraversal/NATTraversal/miniupnpc/ clean all  || exit 1
cp ../../lib/NATTraversal/NATTraversal/libnatpmp/libnatpmp.a ./ || exit 1
cp ../../lib/NATTraversal/NATTraversal/miniupnpc/libminiupnpc.a ./ || exit 1
qmake -makefile -macx -spec macx-g++ -Wall -o ../../lib/NATTraversal/Makefile ../../lib/NATTraversal/NATTraversal/NATTraversal.pro
make -C ../../lib/NATTraversal clean all  || exit 1
cp ../../lib/NATTraversal/libNATTraversal.a  ./ || exit 1
qmake -makefile -macx -spec macx-g++ -Wall -o Makefile ./NatTester/NatTester.pro
make
