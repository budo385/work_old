/*====================================================================
		rstring.c

		rFunc InterBase UDF library.
		String functions.

		Copyright 1998-2003 Polaris Software
		http://rfunc.sourceforge.net
		mailto: rFunc@mail.ru

	 This library is free software; you can redistribute it and/or
	 modify it under the terms of the GNU Lesser General Public
	 License as published by the Free Software Foundation; either
	 version 2.1 of the License, or (at your option) any later version.
	 See license.txt for more details.

====================================================================== */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "rfunc.h"
#include "rstring.h"
#include "rmath.h"

char *delims = " "; // empty delimiters

unsigned char* EXPORT fn_chr(ARG(short*, i))
ARGLIST(short *i)
{
	unsigned char *s = (unsigned char*) MALLOC (2);
	s[0] = (unsigned char) *i;
	s[1] = '\0';
	return s;
}

short EXPORT fn_ord(ARG(unsigned char*, s))
ARGLIST(unsigned char *s)
{
	return (short) s[0];
}

char* EXPORT fn_ltrim(ARG(char*, s))
ARGLIST(char *s)
{
	char *c;
	c = s;
	while (*c && strchr(delims, *c)) c++;
	return c;
}

char* EXPORT fn_rtrim(ARG(char*, s))
ARGLIST(char *s)
{
	long n;
	n = strlen(s);
	while (n && strchr(delims, s[n-1])) n--;
	s[n] = '\0';
	return s;
}

char* EXPORT fn_trim(ARG(char*, s))
ARGLIST(char *s)
{
	return fn_rtrim(fn_ltrim(s));
}

char*	EXPORT fn_repeattrim(ARG(char*, str1), ARG(char*, delim))
ARGLIST(char *str1)
ARGLIST(char *delim)
{
	char *s = str1, *d = str1;

//	while (*s == ' ') s++;
	if (delim[0])
		while (*s)
		{
			while (*s && (*s != delim[0])) *d++ = *s++;
			*d++ = *s++;
			while (*s == delim[0]) s++;
//			if (*s) *d++ = delim[0];
//			else *d++ = '\0';
		}
		*d = '\0';
	return str1;
}

char* EXPORT fn_substr(ARG(char*, s), ARG(long*, m), ARG(long*, n))
ARGLIST(char *s)
ARGLIST(long *m)
ARGLIST(long *n)
{
	long left, right;
	long len = strlen(s);

	left = (*m < 0) ? len + *m + 1 : *m;
	right = (*n < 0) ? left - 1 : left - 1 + *n - 1;
	left = (*n < 0) ? left + *n : left - 1;

	left = (left < 0) ? 0 : left;
	right = (right > len - 1) ? len - 1 : right;

	if (!*n || (left > len - 1) || (right < 0))
		return &s[len];

	s[right + 1] = '\0';
	return &s[left];
}

char* strnrepeat(ARG(char*, s), ARG(long*, c), ARG(long, maxlength))
ARGLIST(char *s)
ARGLIST(long *c)
ARGLIST(long maxlength)
{
	long i = 0;
	long j = 0;
	long l = strlen(s);
	long n = MIN(*c * l + 1L, maxlength);
	char *buffer = (char*) MALLOC (n);

	if (*s)
		while (i < n - 1L)
		{
			buffer[i++] = s[j++];
			if (!s[j]) j = 0;
		}
	buffer[i] = '\0';
	return buffer;
}

char* EXPORT fn_strrepeat(ARG(char*, s), ARG(long*, c))
ARGLIST(char *s)
ARGLIST(long *c)
{
	return strnrepeat(s, c, shortlen);
}

char* EXPORT fn_longstrrepeat(ARG(char*, s), ARG(long*, c))
ARGLIST(char *s)
ARGLIST(long *c)
{
	return strnrepeat(s, c, longlen);
}

char* strnstuff(ARG(char*, s), ARG(long*, spos), ARG(long*, dcount), ARG(char*, is), ARG(long, maxlength))
ARGLIST(char *s)
ARGLIST(long *spos)
ARGLIST(long *dcount)
ARGLIST(char *is)
ARGLIST(long maxlength)
{
	long i = 0;
	long j = 0;
	long l = 0;
	long slen = strlen(s), n, len;
	char *buffer;

	len = slen + strlen(is) + 1L;
	len = MIN(len, maxlength);
	buffer = (char*) MALLOC (len);

	n = (*spos > slen) ? slen : ((*spos > 0) ? *spos - 1 : 0);
	while (l < n) buffer[l++] = s[j++];
	while (is[i] && (l < len - 1L)) buffer[l++] = is[i++];
	j = MIN(j + *dcount, slen);
	while (s[j] && (l < len - 1L)) buffer[l++] = s[j++];

	buffer[l] = '\0';
	return buffer;
}

char* EXPORT fn_strstuff(ARG(char*, s), ARG(long*, spos), ARG(long*, dcount), ARG(char*, is))
ARGLIST(char *s)
ARGLIST(long *spos)
ARGLIST(long *dcount)
ARGLIST(char *is)
{
	return strnstuff(s, spos, dcount, is, shortlen);
}

char* EXPORT fn_longstrstuff(ARG(char*, s), ARG(long*, spos), ARG(long*, dcount), ARG(char*, is))
ARGLIST(char *s)
ARGLIST(long *spos)
ARGLIST(long *dcount)
ARGLIST(char *is)
{
	return strnstuff(s, spos, dcount, is, longlen);
}

char* strnreplace(ARG(char*, s), ARG(char*, froms), ARG(char*, tos), ARG(long, maxlength))
ARGLIST(char *s)
ARGLIST(char *froms)
ARGLIST(char *tos)
ARGLIST(long maxlength)
{
	long sn = strlen(froms);
	char *buffer = (char*) MALLOC (maxlength);
	char *sptr = s, *ptr = s, *bptr = buffer;

	while (ptr = strstr(ptr, froms))
	{
		while (sptr != ptr && (bptr-buffer<maxlength-1)) *bptr++ = *sptr++;
		sptr = tos;
		while (*sptr && (bptr-buffer<maxlength-1)) *bptr++ = *sptr++;
		ptr = ptr + sn;
		sptr = ptr;
	}
	while (*sptr && (bptr-buffer<maxlength-1))
		*bptr++ = *sptr++;
	*bptr = '\0';
	return buffer;
}

char* EXPORT fn_strreplace(ARG(char*, s), ARG(char*, froms), ARG(char*, tos))
ARGLIST(char *s)
ARGLIST(char *froms)
ARGLIST(char *tos)
{
	return strnreplace(s, froms, tos, shortlen);
}

char* EXPORT fn_longstrreplace(ARG(char*, s), ARG(char*, froms), ARG(char*, tos))
ARGLIST(char *s)
ARGLIST(char *froms)
ARGLIST(char *tos)
{
	return strnreplace(s, froms, tos, longlen);
}

long EXPORT fn_strpos(ARG(char*, str1), ARG(char*, str2))
ARGLIST(char *str1)
ARGLIST(char *str2)
{
	char *ptr;
	if (!*str1) return 0;
	ptr = strstr(str2, str1);
	return (ptr) ? (ptr-str2+1) : 0;
}

long EXPORT fn_strlen(ARG(char*, s))
ARGLIST(char *s)
{
	return strlen(s);
}

long EXPORT fn_strcount(ARG(char*, str1), ARG(char*, str2))
ARGLIST(char *str1)
ARGLIST(char *str2)
{
	char *ptr;
	long  r = 0;
	long  len = strlen(str1);

	if (!len || !*str2) return 0;
	ptr = str2;
	while (ptr = strstr(ptr, str1))
	{
		ptr += len;
		r++;
	}
	return r;
}

short EXPORT fn_strcmp(ARG(char*, str1), ARG(char*, str2))
ARGLIST(char *str1)
ARGLIST(char *str2)
{
	int r = strcmp(str1, str2);
	return (short) r;
}

long EXPORT fn_wordcount(ARG(char*, s), ARG(char*, delim), ARG(long*, flag))
ARGLIST(char *s)
ARGLIST(char *delim)
ARGLIST(long *flag)
{
	char *c = s;
	long r = 0;
	long d = 1;

	if (!*c) return r;
	while (*c)
		if (strchr(delim, *c++))
			d++; // counting words
		else {
			while (*c && !strchr(delim, *c)) c++;
			r++; // counting full words
		}
	if (*flag)
		return d;
	else
		return r;
}

char* EXPORT fn_wordnum(ARG(char*, s), ARG(long*, n), ARG(char*, delim), ARG(long*, flag))
ARGLIST(char *s)
ARGLIST(long *n)
ARGLIST(char *delim)
ARGLIST(long *flag)
{
	char *c = s, *ptr;
	long r = 1;

	if (!*c) return c;
	while (*c && r!=*n)
		if (strchr(delim, *c++)) {
			if (*flag)
				r++;
			else
				while (*c && strchr(delim, *c)) c++;
		} else {
			while (*c && !strchr(delim, *c)) c++;
			if (!*flag) r++;
		}
	if (!*flag)
		while (*c && strchr(delim, *c)) c++;
	ptr = c;
	while (*ptr && !strchr(delim, *ptr)) ptr++;
	*ptr = '\0';

	return c;
}

char* padnright(ARG(char*, s), ARG(short*, n), ARG(char*, c), ARG(long, maxlength))
ARGLIST(char *s)
ARGLIST(short *n)
ARGLIST(char *c)
ARGLIST(long maxlength)
{
	long i = 0;
	char *buffer;
	char *ptr = s;
	long len = MIN(*n + 1L, maxlength);

	buffer = (char*) MALLOC (len);
	while (*ptr && (i < len - 1L)) buffer[i++] = *ptr++;
	while ((i < *n) && (i < len - 1L)) buffer[i++] = *c;
	buffer[i] = '\0';
	return buffer;
}

char* EXPORT fn_padright(ARG(char*, s), ARG(short*, n), ARG(char*, c))
ARGLIST(char *s)
ARGLIST(short *n)
ARGLIST(char *c)
{ return padnright(s, n, c, shortlen); }

char* EXPORT fn_longpadright(ARG(char*, s), ARG(short*, n), ARG(char*, c))
ARGLIST(char *s)
ARGLIST(short *n)
ARGLIST(char *c)
{ return padnright(s, n, c, longlen); }

char* padnleft(ARG(char*, s), ARG(short*, n), ARG(char*, c), ARG(long, maxlength))
ARGLIST(char *s)
ARGLIST(short *n)
ARGLIST(char *c)
ARGLIST(long maxlength)
{
	long l = strlen(s), i = 0;
	char *buffer;
	char *ptr = s;
	long len = MIN(*n + 1L, maxlength);

	buffer = (char*) MALLOC (len);
	while ((i < *n - l) && (i < len - 1L)) buffer[i++] = *c;
	while (*ptr && (i < len - 1L)) buffer[i++] = *ptr++;
	buffer[i] = '\0';
	return buffer;
}

char* EXPORT fn_padleft(ARG(char*, s), ARG(short*, n), ARG(char*, c))
ARGLIST(char *s)
ARGLIST(short *n)
ARGLIST(char *c)
{ return padnleft(s, n, c, shortlen); }

char* EXPORT fn_longpadleft(ARG(char*, s), ARG(short*, n), ARG(char*, c))
ARGLIST(char *s)
ARGLIST(short *n)
ARGLIST(char *c)
{ return padnleft(s, n, c, longlen); }

char* EXPORT fn_c(ARG(char*, s))
ARGLIST(char* s)
{ return s; }

char* EXPORT fn_floattostr(ARG(double*, d), ARG(char*, fmt))
ARGLIST(double *d)
ARGLIST(char *fmt)
{
	char *buffer = (char *) MALLOC (shortlen);
	char	*s, slong[20];
	long	i;

	buffer[0] = '\0';
// ��������� ������ �������, ������� ��������� ������������� ��� ���������.
	// ������ ������������ �������������� ���������� �������� %
	if (s = strstr(fmt, "%"))
	{
		i = 1;
		while (strchr("+- #", s[i])) i++;
		while (strchr("0123456789", s[i])) i++;
		while (strchr(".", s[i])) i++;
		while (strchr("0123456789", s[i])) i++;
		if (strchr("diouxX", s[i]))
		{
			sprintf(slong, "%.0f", *d);
			i = atoi(slong);
			sprintf(buffer, fmt, i);
		}
		else
			sprintf(buffer, fmt, *d);
	}
	else
		sprintf(buffer, fmt, *d);
	return buffer;
}

char* EXPORT fn_inttostr(ARG(long*, l), ARG(char*, fmt))
ARGLIST(long *d)
ARGLIST(char *fmt)
{
	char *buffer = (char *) MALLOC (shortlen);

	sprintf(buffer, fmt, *l);
	return buffer;
}

char*	EXPORT fn_convertsymbols(ARG(char*, s), ARG(char*, source), ARG(char*, target))
ARGLIST(char *s)
ARGLIST(char *source)
ARGLIST(char *target)
{
	char	*c = s, *p;
	while (*c)
		if (p = strchr(source, *c))
			*c++ = target[p - source];
		else
			c++;
	return s;
}

/*
DECLARE EXTERNAL FUNCTION N_STR
  CSTRING(255) CHARACTER SET NONE
RETURNS CSTRING(255) CHARACTER SET NONE FREE_IT
ENTRY_POINT 'fn_nvl_string' MODULE_NAME 'rfunc';

test:
SELECT N_STR(Null) FROM rdb$database;
SELECT N_STR('ante') FROM rdb$database;
*/
//http://docs.oracle.com/cd/B19306_01/server.102/b14200/functions105.htm
//NVL version specialized for string param only (with added bonus that the return value is hardcoded to a single space character ' ')
char*	EXPORT fn_nvl_string(ARG(char*, expr1))
ARGLIST(char* expr1)
{
  if(!expr1 || *expr1 == '\0')
  {
	//NULL string is replaced with ' '
	char *szRes = MALLOC(2);
	strcpy(szRes, " ");
	return szRes;
  }
  else{
	char *szRes = MALLOC(strlen(expr1)+1);
	strcpy(szRes, expr1);
    return szRes;
  }
}

char *rtrim(char *s)
{
	char* back = s + strlen(s);
	while(isspace(*--back));
	*(back+1) = '\0';
	return s;
}

//BT: "vraca integer, prima integer, ako je input 0, onda je output NULL, a za sve ostalo je output = input"
char*	EXPORT fn_nvl_integer(ARG(char *expr1))
ARGLIST(char *expr1)
{
  expr1 = rtrim(expr1);
  if(!expr1 || 0 == strcmp(expr1, "0"))
  {
	return NULL;
  }
  else{
	char *szRes = MALLOC(strlen(expr1)+1);
	strcpy(szRes, expr1);
    return szRes;
  }
}






char* EXPORT fn_str_encodeToBase64(ARG(char *,expr1))
ARGLIST(char *expr1)
{
	long length = strlen(expr1);
	char *buffer =  (char*) MALLOC (length*2);
	
	base64_encode(expr1, length, buffer, 1024);
	return buffer;
}

char* EXPORT fn_str_decodeFromBase64(ARG(char *, expr1))
ARGLIST(char *expr1)
{
	long length = strlen(expr1);
	char *buffer =  (char*) MALLOC (length);

	long actual_length=base64_decode(expr1, buffer, length);

	if (actual_length>=0)
	{
		buffer[actual_length]=0;
	}

	return buffer;

}




//--------------------------------------
//		BASE 64 decode/encode
//--------------------------------------



/**
 * characters used for Base64 encoding
 */  
const char *BASE64_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

/**
 * encode three bytes using base64 (RFC 3548)
 *
 * @param triple three bytes that should be encoded
 * @param result buffer of four characters where the result is stored
 */  
void _base64_encode_triple(unsigned char triple[3], char result[4])
 {
    int tripleValue, i;

    tripleValue = triple[0];
    tripleValue *= 256;
    tripleValue += triple[1];
    tripleValue *= 256;
    tripleValue += triple[2];

    for (i=0; i<4; i++)
    {
 result[3-i] = BASE64_CHARS[tripleValue%64];
 tripleValue /= 64;
    }
} 

/**
 * encode an array of bytes using Base64 (RFC 3548)
 *
 * @param source the source buffer
 * @param sourcelen the length of the source buffer
 * @param target the target buffer
 * @param targetlen the length of the target buffer
 * @return 1 on success, 0 otherwise
 */  


int base64_encode(unsigned char *source, size_t sourcelen, char *target, size_t targetlen)
 {
	size_t tmp= sourcelen;
	unsigned char *source_orig=source;
	char *target_orig=target;

    /* check if the result will fit in the target buffer */
    //if ((sourcelen+2)/3*4 > targetlen-1)
	if ((sourcelen+2)/3*4 > targetlen)
	return 0;

    /* encode all full triples */
    while (sourcelen >= 3)
    {
 _base64_encode_triple(source, target);
 sourcelen -= 3;
 source += 3;
 target += 4;
    }

    /* encode the last one or two characters */
	
    if (sourcelen > 0)
    {
 unsigned char temp[3];
 memset(temp, 0, sizeof(temp));
 memcpy(temp, source, sourcelen);
 _base64_encode_triple(temp, target);
 target[3] = '=';
 if (sourcelen == 1)
     target[2] = '=';

 target += 4;
    }
	

    /* terminate the string */
	/* B.T.: do not terminate!!!! as this prevents correct decoding */

    //target[0] = 0;
/*
	if (tmp==91)
	{
		FILE *pIn2 = fopen("C:\\temp_in.file", "wb");
		if(pIn2)
		{
			fwrite(source_orig,tmp,1,pIn2);
			fclose(pIn2);
			//return 1;
		}

		FILE *pOut2 = fopen("C:\\temp_out.file", "wb");
		if(pOut2)
		{
			fwrite(target_orig,targetlen,1,pOut2);
			fclose(pOut2);
			//return 1;
		}

	}
*/

    return 1;
} 


/**
 * determine the value of a base64 encoding character
 *
 * @param base64char the character of which the value is searched
 * @return the value in case of success (0-63), -1 on failure
 */  
int _base64_char_value(char base64char)
 {
    if (base64char >= 'A' && base64char <= 'Z')
 return base64char-'A';
    if (base64char >= 'a' && base64char <= 'z')
 return base64char-'a'+26;
    if (base64char >= '0' && base64char <= '9')
 return base64char-'0'+2*26;
    if (base64char == '+')
 return 2*26+10;
    if (base64char == '/')
 return 2*26+11;
    return -1;
} 

/**
 * decode a 4 char base64 encoded byte triple
 *
 * @param quadruple the 4 characters that should be decoded
 * @param result the decoded data
 * @return lenth of the result (1, 2 or 3), 0 on failure
 */  
int _base64_decode_triple(char quadruple[4], unsigned char *result)
 {
    int i, triple_value, bytes_to_decode = 3, only_equals_yet = 1;
    int char_value[4];

    for (i=0; i<4; i++)
 char_value[i] = _base64_char_value(quadruple[i]);

    /* check if the characters are valid */
    for (i=3; i>=0; i--)
    {
 if (char_value[i]<0)
 {
     if (only_equals_yet && quadruple[i]=='=')
     {
  /* we will ignore this character anyway, make it something
   * that does not break our calculations */
  char_value[i]=0;
  bytes_to_decode--;
  continue;
     }
     return 0;
 }
 /* after we got a real character, no other '=' are allowed anymore */
 only_equals_yet = 0;
    }

    /* if we got "====" as input, bytes_to_decode is -1 */
    if (bytes_to_decode < 0)
 bytes_to_decode = 0;

    /* make one big value out of the partial values */
    triple_value = char_value[0];
    triple_value *= 64;
    triple_value += char_value[1];
    triple_value *= 64;
    triple_value += char_value[2];
    triple_value *= 64;
    triple_value += char_value[3];

    /* break the big value into bytes */
    for (i=bytes_to_decode; i<3; i++)
 triple_value /= 256;
    for (i=bytes_to_decode-1; i>=0; i--)
    {
 result[i] = triple_value%256;
 triple_value /= 256;
    }

    return bytes_to_decode;
} 

/**
 * decode base64 encoded data
 *
 * @param source the encoded data (zero terminated)
 * @param target pointer to the target buffer
 * @param targetlen length of the target buffer
 * @return length of converted data on success, -1 otherwise
 */  
size_t base64_decode(char *source, unsigned char *target, int targetlen)
 {
    char *src, *tmpptr;
    char quadruple[4], tmpresult[3];
    int i, tmplen = 3;
    size_t converted = 0;


    /* concatinate '===' to the source to handle unpadded base64 data */
    src = (char *)malloc(strlen(source)+5);
    if (src == NULL)
 return -1;
    strcpy(src, source);
    strcat(src, "====");
    tmpptr = src;

    /* convert as long as we get a full result */
    while (tmplen == 3)
    {
 /* get 4 characters to convert */
 for (i=0; i<4; i++)
 {
     /* skip invalid characters - we won't reach the end */
     while (*tmpptr != '=' && _base64_char_value(*tmpptr)<0)
  tmpptr++;

     quadruple[i] = *(tmpptr++);
 }

 /* convert the characters */
 tmplen = _base64_decode_triple(quadruple, (unsigned char *)tmpresult);

 /* check if the fit in the result buffer */
 if (targetlen < tmplen)
 {
     free(src);
     return -1;
 }

 /* put the partial result in the result buffer */
 memcpy(target, tmpresult, tmplen);
 target += tmplen;
 targetlen -= tmplen;
 converted += tmplen;
    }

    free(src);
    return converted;
} 

//--------------------------------------
//		BASE 64 decode/encode
//--------------------------------------

