2012-04-05:
-------------------------
Source is modified from _data/rfunc-2.1.3.1-RC1-win32.zip
This dll is neccesary for our SPC database on FB

Two new functions are added:
DECLARE EXTERNAL FUNCTION N_DATE
  DATE
RETURNS DATE FREE_IT
ENTRY_POINT 'fn_nvl_date' MODULE_NAME 'rfunc';

DECLARE EXTERNAL FUNCTION N_STR
  CSTRING(255)
RETURNS CSTRING(255) FREE_IT
ENTRY_POINT 'fn_nvl_string' MODULE_NAME 'rfunc';

//if input is 0 then return NULL, else return input (integer)
DECLARE EXTERNAL FUNCTION N_INT 
    CSTRING (1020)
RETURNS CSTRING (1020) FREE_IT
ENTRY_POINT 'fn_nvl_integer' MODULE_NAME 'rfunc';

DECLARE EXTERNAL FUNCTION F_ENCODE_BASE64
   CSTRING(255)
   RETURNS CSTRING(255) FREE_IT
  ENTRY_POINT 'fn_str_encodeToBase64'  MODULE_NAME 'rfunc';
  
DECLARE EXTERNAL FUNCTION F_DECODE_BASE64
   CSTRING(255)
   RETURNS CSTRING(255) FREE_IT
  ENTRY_POINT 'fn_str_decodeFromBase64'  MODULE_NAME 'rfunc';
