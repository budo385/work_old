#include "sokrates_progress.h"
#include <QDesktopWidget>
#include <QFile>
#include <qDebug>
#include <QMenu>
#include <QMouseEvent>
#include <QMediaPlaylist>
#include <QMessageBox>

extern QString g_strLabel;

Sokrates_Progress::Sokrates_Progress(QWidget *parent, Qt::WindowFlags flags)
	: QFrame(parent, Qt::SplashScreen|Qt::WindowStaysOnTopHint), m_player(NULL)
{
	ui.setupUi(this);
	
	//center on current screen
	QRect desktopRec=QApplication::desktop()->availableGeometry(QApplication::desktop()->screenNumber(this));
	int w = desktopRec.width();
	int h = desktopRec.height();
	int mw = width();
	int mh = height();
	int cw = (w/2) - (mw/2);
	int ch = (h/2) - (mh/2);
	move(cw,ch);

	//set label text
	ui.txtMessage->setText(g_strLabel);

	//set left banner
	ui.imgLeft->setPixmap(QPixmap(":SOKRATES_Vert_V2-0.png"));
	
	//set fallback background iamge
	ui.pixBkg->setPixmap(QPixmap(":splash.png"));

	QString strStyle = QString("QFrame#Sokrates_ProgressClass { border-width: 4px; border-image:url(%1) 4px 4px 4px 4px stretch stretch }").arg(":Theme_Blue_Temple_Sidebar_Chapter.png");
	this->setStyleSheet(strStyle);

	//ui.wndVideo->setFrameStyle(QFrame::Box|QFrame::Sunken);

	//set video scaling (no black bars at the sides)
	//ui.wndVideo->setScaleMode(Phonon::VideoWidget::ScaleAndCrop);

	m_bStaticImage = false;

	m_strAviPath = QCoreApplication::applicationDirPath() + "/sok.dat";
	if(QFile::exists(m_strAviPath))
	{
		//QMessageBox::information(this, "Info", QString("Found avi at: %1").arg(m_strAviPath));

		m_player = new QMediaPlayer(this, QMediaPlayer::VideoSurface);
		connect(m_player, SIGNAL(error(QMediaPlayer::Error)), this, SLOT(onVideoError(QMediaPlayer::Error)));
		connect(m_player, SIGNAL(mediaStatusChanged(QMediaPlayer::MediaStatus)), this, SLOT(onMediaStatusChanged(QMediaPlayer::MediaStatus)));
		connect(m_player, SIGNAL(videoAvailableChanged(bool)), this, SLOT(onVideoAvailableChanged(bool)));

		m_player->setMedia(QMediaContent(QUrl::fromLocalFile(m_strAviPath)));
		m_player->setVideoOutput(ui.wndVideo);

		ui.wndVideo->show();
		ui.pixBkg->hide();
		m_player->play();

		//play the video
		onVideoAboutToFinish();
	}
	else
	{
		//QMessageBox::information(this, "Info", QString("Avi not found at: %1").arg(m_strAviPath));
		FallbackToStaticImage();
	}

	//ensure app closes when window closes
	setAttribute(Qt::WA_QuitOnClose);
}

Sokrates_Progress::~Sokrates_Progress()
{
}

void Sokrates_Progress::onVideoAboutToFinish()
{
	//ensures eternal loop
	//m_mediaObject.setCurrentSource(Phonon::MediaSource(m_strAviPath));
	//m_mediaObject.play();
}

/*
void Sokrates_Progress::onVideoStateChanged(Phonon::State newstate, Phonon::State oldstate)
{
	if(Phonon::ErrorState == newstate)
		//if(Phonon::NoError != m_mediaObject.errorType())
			FallbackToStaticImage();
}
*/
void Sokrates_Progress::onTotalTimeChanged(qint64 newTotalTime)
{
	if(newTotalTime < 1000) //1 sec
		FallbackToStaticImage();
}

void Sokrates_Progress::FallbackToStaticImage()
{
	if(m_bStaticImage)
		return; //already here;
	m_bStaticImage = true;

	qDebug() << "Fallback to the static image progress";

	//m_mediaObject.clear();
	ui.wndVideo->hide();
	//ui.stackedWidget->setCurrentIndex(1);
	ui.pixBkg->show();
}

//make the window draggable
void Sokrates_Progress::mousePressEvent(QMouseEvent *event)
{
	QPoint p;
	p = event->pos();
	m_nDiffX = p.x();
	m_nDiffY = p.y();

    if (event->button() == Qt::RightButton) 
	{
		QAction *pActClose = new QAction(tr("Close"),this);
		connect(pActClose, SIGNAL(triggered()), this, SLOT(OnMenu_Close()));
		QMenu *menu = new QMenu();
		menu->addAction(pActClose);

		menu->popup(event->globalPos());

		event->accept();
    }
}

void Sokrates_Progress::mouseMoveEvent(QMouseEvent *e)
{
	QPoint p;
	p = e->globalPos();
	move(p.x() - m_nDiffX, p.y() - m_nDiffY);
}

void Sokrates_Progress::OnMenu_Close()
{
	close();
}

void Sokrates_Progress::onMediaStatusChanged(QMediaPlayer::MediaStatus status)
{
#ifdef _DEBUG
	qDebug() << "Metadata:" << m_player->availableMetaData();
	qDebug() << "Supported:" << QMediaPlayer::supportedMimeTypes(QMediaPlayer::VideoSurface);
	qDebug() << "Error:" << m_player->errorString();

	QMessageBox::information(this, "Info", QString("Media status changed to %1").arg(status));
#endif
}

void Sokrates_Progress::onVideoAvailableChanged(bool videoAvailable)
{
#ifdef _DEBUG
	QMessageBox::information(this, "Info", QString("Video available status changed to %1").arg(videoAvailable));
#endif
}

void Sokrates_Progress::onVideoError(QMediaPlayer::Error error) 
{
#ifdef _DEBUG
	QMessageBox::information(this, "Info", QString("Video error: %1 (%2)").arg(error).arg(m_player->errorString()));
#endif
}
