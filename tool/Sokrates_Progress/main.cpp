#include <QApplication>
#include "sokrates_progress.h"

QString g_strLabel;

int main(int argc, char *argv[])
{
	//do not start if started without params
	if(argc < 2)
		return -1;

	//store label text
	g_strLabel = argv[1];

	//QApplication::setGraphicsSystem("raster");

	QApplication a(argc, argv);
	Sokrates_Progress w;
	w.show();
	return a.exec();
}
