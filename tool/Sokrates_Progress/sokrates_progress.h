#ifndef SOKRATES_PROGRESS_H
#define SOKRATES_PROGRESS_H

#include <QWidget>
#include "ui_sokrates_progress.h"
#include <QMouseEvent>
#include <QMediaPlayer>

class Sokrates_Progress : public QFrame
{
	Q_OBJECT

public:
	Sokrates_Progress(QWidget *parent = 0, Qt::WindowFlags flags = 0);
	~Sokrates_Progress();

	QString m_strAviPath;

public slots:
	void onVideoAvailableChanged(bool videoAvailable);
	void onMediaStatusChanged(QMediaPlayer::MediaStatus status);
	void onVideoError(QMediaPlayer::Error error);
	void onVideoAboutToFinish();
	void onTotalTimeChanged(qint64 newTotalTime);
	void OnMenu_Close();

protected:
	void mousePressEvent(QMouseEvent *e);
	void mouseMoveEvent(QMouseEvent *e);
	void FallbackToStaticImage();

private:
	int m_nDiffX;
    int m_nDiffY;
	bool m_bStaticImage;
	Ui::Sokrates_ProgressClass ui;
	QMediaPlayer *m_player;
};

#endif // SOKRATES_PROGRESS_H
