#ifndef TABLE_APISTAT_H
#define TABLE_APISTAT_H

#include "universaltablewidgetex.h"

class Table_ApiStat : public UniversalTableWidgetEx
{
	Q_OBJECT

public:
	Table_ApiStat(QWidget *parent);
	~Table_ApiStat();

	static void DefineList(DbRecordSet *plstData);
	void Initialize(DbRecordSet *plstData);

private slots:

public slots:

protected:
	

private:

};

#endif // TABLE_TIMINIGS_H
