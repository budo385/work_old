#ifndef WORKTHREADES_H
#define WORKTHREADES_H

#include "workthread.h"
#include "common/common/dbrecordset.h"
#include "trans/trans/resthttpclient.h"

class WorkThreadExecutorES : public WorkThreadExecutor
{
	Q_OBJECT

public:
	WorkThreadExecutorES(WorkThread *pParent);
	~WorkThreadExecutorES();

public slots:
	void ExecuteTask();

private:
	int GetServerTime(Status &pStatus,QDateTime& Ret_datCurrentServerTime, QDateTime& Ret_datTimeNextTest, int& Ret_nActiveClientSocketCnt, RestHTTPClient *client);
	int ReadUserEmails(Status &pStatus, int &Ret_nTotalCount, DbRecordSet &Ret_Emails, int nFromN=-1, int nToN=-1, int nSortOrder=-1, QString strEmailAcc="",QString strEmailSender="", RestHTTPClient *client=NULL);
	int ReadEmailDetails(Status &pStatus, int nEmailID, DbRecordSet &Ret_Email, DbRecordSet &Ret_Attachments, RestHTTPClient *client);
	int ReadEmailAsHtmlBinary(Status &pStatus, int nEmailID,QByteArray &Ret_byteData, RestHTTPClient *client);
	int GetAttachmentBinary(Status &pStatus, int nAttachmentID, QByteArray &Ret_byteData, RestHTTPClient *client);
};


class WorkThreadES : public WorkThread
{
	Q_OBJECT

public:
	WorkThreadES(QObject *parent);
	~WorkThreadES();

protected:
	void run();
	WorkThreadExecutorES *m_pExecutor;
};




#endif // WORKTHREADES_H
