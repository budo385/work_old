#ifndef WORKTHREAD_H
#define WORKTHREAD_H

#include <QThread>
#include <QReadWriteLock>
#include <QDateTime>
#include <QList>
#include "common/common/status.h"
#include "common/common/threadsync.h"



class WorkThreadTiming
{
public:
	WorkThreadTiming(QString strApi="",QDateTime time=QDateTime::currentDateTime(),int nResponseTime=0)
		:m_strApi(strApi),m_datTime(time),m_nResponseTimeMsec(nResponseTime){};

	void clear(){m_strApi="",m_datTime=QDateTime::currentDateTime();m_nResponseTimeMsec=0;};

	QString		m_strApi;
	QDateTime	m_datTime;
	int			m_nResponseTimeMsec;
};

//class WorkThreadExecutor;
//class WorkThreadExecutorCS;
class WorkThread : public QThread
{
	Q_OBJECT

public:
	//friend class WorkThreadExecutor;
	//friend class WorkThreadExecutorCS;
	
	WorkThread(QObject *parent);
	virtual ~WorkThread();

	//control thread
	void	Start(QString strServerURL, QString strUsername, QString strPassword, bool bKeepHistory=false);
	void	Stop();
	Status	GetLastError(){QReadLocker lck(&m_lck);return m_LastError;};
	QDateTime GetLatestServerTime(){QReadLocker lck(&m_lck);return m_datLatestServerTime;};
	int		  GetServerSockets(){QReadLocker lck(&m_lck);return m_nActiveServerSockets;};

	//for slave thread comm:
	void	SetLastError(Status &pStatus){QWriteLocker lck(&m_lck); m_LastError=pStatus;};
	bool	IsStopCommandIssued(){QReadLocker lck(&m_lck);return m_bStopCommandIssued;};
	void	SetLatestServerTime(QDateTime datLatestServerTime){QWriteLocker lck(&m_lck);m_datLatestServerTime=datLatestServerTime;};
	void    SetServerSockets(int nActiveServerSockets){QWriteLocker lck(&m_lck);m_nActiveServerSockets=nActiveServerSockets;};
	void	StoreError(Status &err){m_lstErrors.append(err);};

	//get results
	QHash<QString,QList<WorkThreadTiming>>	GetAll(){QReadLocker lck(&m_lck);return m_lstAll;};
	QHash<QString,WorkThreadTiming>			GetMin(){QReadLocker lck(&m_lck);return m_minTime;};
	QHash<QString,WorkThreadTiming>			GetMax(){QReadLocker lck(&m_lck);return m_maxTime;};
	QHash<QString,WorkThreadTiming>			GetAvg(){QReadLocker lck(&m_lck);return m_avgTime;};
	QHash<QString,WorkThreadTiming>			GetLatest(){QReadLocker lck(&m_lck);return m_latestTime;};

	QHash<QString,qint64>					GetApiSuccessCnt(){QReadLocker lck(&m_lck);return m_ApiSuccessCnt;};
	QHash<QString,qint64>					GetApiFailCnt(){QReadLocker lck(&m_lck);return m_ApiFailCnt;};
	QList<Status>							GetErrorList(){return m_lstErrors;}

public slots:
	void OnStopThread(){Stop();};

public:
	virtual void run()=0;

	QReadWriteLock			m_lck;

	//inner vars for slave thread:
	bool					m_bKeepHistory; //if true then m_lstAll is preserved -> memory consumption, else it is cleared (only latest)
	QString					m_strServerURL;
	QString					m_strServerIP;
	int						m_nServerPort;
	int						m_nServerIsSSL;
	QString					m_strUser;
	QString					m_strPass;
		
	ThreadSynchronizer		m_ThreadSync;
	
	//vars for main thread comm:
	QDateTime				m_datLatestServerTime;
	int						m_nActiveServerSockets;
	Status					m_LastError;
	bool					m_bStopCommandIssued;

	QHash<QString,QList<WorkThreadTiming>> m_lstAll;
	QHash<QString,WorkThreadTiming>	m_minTime;
	QHash<QString,WorkThreadTiming>	m_maxTime;
	QHash<QString,WorkThreadTiming>	m_avgTime;
	QHash<QString,WorkThreadTiming>	m_latestTime;

	QHash<QString,qint64>	m_ApiSuccessCnt;
	QHash<QString,qint64>	m_ApiFailCnt;
	QHash<QString,qint64>	m_ApiElapsedTimeTotal;
	QHash<QString,qint64>	m_ApiElapsedTimeMin;
	QHash<QString,qint64>	m_ApiElapsedTimeMax;
	QList<Status>			m_lstErrors;	
};


class WorkThreadExecutor : public QObject
{
	Q_OBJECT

public:
	WorkThreadExecutor(WorkThread *pParent);
	virtual ~WorkThreadExecutor();

signals:
	void GoStopThread();

public slots:
	virtual void ExecuteTask()=0;

protected:
	WorkThread *m_Parent;
};


#endif // WORKTHREAD_H

