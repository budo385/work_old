#ifndef SERVERPERFORMANCETESTER_H
#define SERVERPERFORMANCETESTER_H

#include <QtWidgets/QMainWindow>
#include "ui_serverperformancetester.h"
#include "workthreadcs.h"
#include "workthreades.h"


class ServerPerformanceTester : public QMainWindow
{
	Q_OBJECT

public:
	ServerPerformanceTester(QWidget *parent = 0);
	~ServerPerformanceTester();
	
	
	enum TestProgressStatus
	{
		STATUS_IDLE =0,
		STATUS_WAITING=1,
		STATUS_TEST_IN_PROGRESS=2,
		STATUS_GO_FROM_SYNC_TO_TEST=3
	};
	

public slots:

	void on_btnStartTest_clicked();
	void on_btnSetTime_clicked();
	void on_btnGetTime_clicked();
	void on_btnRefreshServerStatus_clicked();
	void OnRowChanged();


private:
	Ui::ServerPerformanceTesterClass ui;
	
	void	InitFlds();
	void	timerEvent(QTimerEvent *event); 
	void	RefreshServerTime(Status &pStatus);
	int		CS_GetServerTime( Status &pStatus,QString strServerURL, QString strUser, QString strPass, QDateTime& Ret_datCurrentServerTime, QDateTime& Ret_datTimeNextTest, int& Ret_nActiveClientSocketCnt );
	int		ES_GetServerTime( Status &pStatus,QString strServerURL, QString strUser, QString strPass, QDateTime& Ret_datCurrentServerTime, QDateTime& Ret_datTimeNextTest, int& Ret_nActiveClientSocketCnt );
	int		CS_SetServerTime( Status &pStatus,QString strServerURL, QString strUser, QString strPass, QDateTime datTimeNextTest);
	int		ES_SetServerTime( Status &pStatus,QString strServerURL, QString strUser, QString strPass, QDateTime datTimeNextTest);;
	
	void	RefreshWorkThreads();
	void	StopWorkThreads();
	QString ConvertNumValue( int nValue );

	int m_nTimerID;
	//server times:
	QDateTime	m_datServerTime;
	qint64		m_nLocalTimeOffsetSeconds;
	QDateTime	m_datNextTestTime;

	//local times:
	QDateTime	m_datTestStartTime;
	QDateTime	m_datTestDurationEnd;

	int			m_nActiveServerSockets;
	int			m_nMaxThreads;

	int			m_nTestInProgress; //0-idle, 1-waiting to start, 2-started
	int			m_nThreadIncrement;
	int			m_nDurationIncrementSec;
	int			m_nThreadIncrementCounter;

	QList<WorkThread*>	m_lstThreads;
	DbRecordSet			m_lstTiming;
	DbRecordSet			m_lstTimingPerAPI;
	DbRecordSet			m_lstTimingSingleAPI;



};

#endif // SERVERPERFORMANCETESTER_H
