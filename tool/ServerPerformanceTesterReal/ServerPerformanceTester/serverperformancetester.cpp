#include "serverperformancetester.h"
#include "trans/trans/resthttpclient.h"
#include "common/common/sha256hash.h"
#include "common/common/datahelper.h"
#include <QMessageBox>
#include <QDebug>
#include "common/common/threadid.h"

#define SECRET_SPICE_PASSWORD "BANDIT"
#define HTML_START_F1_RED			"<html><head><meta name=\"qrichtext\" content=\"1\" /></head><body style=\" white-space: pre-wrap; font-family:Arial; font-size:10pt; font-weight:bold; font-style:normal; text-decoration:none;\"><p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:10pt; color: red; font-style:normal;\">"
#define HTML_START_F1_GREEN			"<html><head><meta name=\"qrichtext\" content=\"1\" /></head><body style=\" white-space: pre-wrap; font-family:Arial; font-size:10pt; font-weight:bold; font-style:normal; text-decoration:none;\"><p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:10pt; color: green; font-style:normal;\">"
#define HTML_START_F1_BLUE			"<html><head><meta name=\"qrichtext\" content=\"1\" /></head><body style=\" white-space: pre-wrap; font-family:Arial; font-size:10pt; font-weight:bold; font-style:normal; text-decoration:none;\"><p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:10pt; color: blue; font-style:normal;\">"
#define HTML_END_F1					"</span></p></body></html>"

#define THREAD_INCREASE_STEPS 5 //from 2 to N

ServerPerformanceTester::ServerPerformanceTester(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	m_nTimerID = startTimer(1000); //ever 1 second check server status and test status
	m_nTestInProgress=STATUS_IDLE;
	InitFlds();
	
	ui.tableAll->DefineTimingList(&m_lstTiming);
	ui.tableAll->Initialize(&m_lstTiming);
	ui.tablePerAPI->DefineList(&m_lstTimingPerAPI); 
	m_lstTimingSingleAPI.copyDefinition(m_lstTimingPerAPI);
	ui.tablePerAPI->Initialize(&m_lstTimingSingleAPI);
	
	connect(ui.tableAll,SIGNAL(SignalSelectionChanged()),this,SLOT(OnRowChanged()));
	
}

ServerPerformanceTester::~ServerPerformanceTester()
{

}

void ServerPerformanceTester::InitFlds()
{
	ui.txtThreads->setInputMask("00000");
	ui.txtTestTime->setInputMask("00:00");
	ui.txtDuration->setInputMask("000");

	//ui.txtServerURL->setText("http://everxconnect.com:43001");
	//ui.txtServerURL->setText("http://localhost:41566");
	//ui.txtServerURL->setText("http://192.168.200.10:43001");
	ui.txtServerURL->setText("http://localhost:9999");
	
	ui.txtUser->setText("user0000@everxconnect.com");
	ui.txtPass->setText("pw0000");
	ui.txtThreads->setText("30");
	ui.txtTestTime->setText("00:00");
	ui.txtDuration->setText("10");

	//ui.rbCS->setChecked(true);
	ui.rbES->setChecked(true);
	ui.labServerStatus->setText(HTML_START_F1_RED+QString("offline")+HTML_END_F1);
	ui.labTestStatus->setText(HTML_START_F1_BLUE+QString("Idle")+HTML_END_F1);
	m_nActiveServerSockets=0;	
}

void ServerPerformanceTester::timerEvent( QTimerEvent *event )
{
	if(event->timerId()==m_nTimerID)
	{
		killTimer(m_nTimerID);

		//Refresh this every now and then....
		if (m_nTestInProgress==STATUS_WAITING)
		{
			//calculate how many till we can start test:
			int nSecTo=QDateTime::currentDateTime().addSecs(m_nLocalTimeOffsetSeconds).secsTo(m_datNextTestTime);
			if (nSecTo<=0)
			{
				m_nTestInProgress=STATUS_GO_FROM_SYNC_TO_TEST;
				on_btnStartTest_clicked(); //start baby
				/*
				int nDurationMin=ui.txtDuration->text().toInt();
				m_datTestStartTime=QDateTime::currentDateTime();
				m_datTestDurationEnd = QDateTime::currentDateTime().addSecs(nDurationMin*60);
				ui.labTestStatus->setText(HTML_START_F1_BLUE+QString("Test in progress...")+HTML_END_F1);
				m_nTestInProgress=STATUS_TEST_IN_PROGRESS;
				RefreshWorkThreads();
				return;
				*/
			}
			else
			{
				QTime timeElapsed=DataHelper::GetTimeFromSeconds(nSecTo);
				QString strElapsed=timeElapsed.toString("HH:mm:ss");
				ui.labTestStatus->setText(HTML_START_F1_GREEN+QString("Waiting for other clients, test will start in: %1").arg(strElapsed)+HTML_START_F1_GREEN);
			}
		}
		else if (m_nTestInProgress==STATUS_TEST_IN_PROGRESS)
		{
			if (QDateTime::currentDateTime()>=m_datTestDurationEnd)
				on_btnStartTest_clicked(); //end if over
			else
				RefreshWorkThreads();
		}
		else
		{
			
		}

		m_nTimerID = startTimer(1000);
	}


}

void ServerPerformanceTester::on_btnStartTest_clicked()
{
	if (m_nTestInProgress==STATUS_TEST_IN_PROGRESS)
	{
		QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
		ui.labTestStatus->setText(HTML_START_F1_BLUE+QString("Stopping all threads, please wait.....")+HTML_END_F1);
		int nCntThreads=m_lstThreads.size();
		StopWorkThreads();
		//Status err;
		//RefreshServerTime(err);
		QApplication::restoreOverrideCursor();

		//calc total calls:+
		int nSize=m_lstTiming.getRowCount();
		int nTotalCalls=0;
		int nTotalFailed=0;
		for (int i=0;i<nSize;i++)
		{
			nTotalCalls+=m_lstTiming.getDataRef(i,"CNT_TOTAL").toInt();
			nTotalFailed+=m_lstTiming.getDataRef(i,"CNT_FAIL").toInt();
		}

		QDateTime datServerStartTestTime=m_datTestStartTime.addSecs(m_nLocalTimeOffsetSeconds);
		ui.labTestStatus->setText(HTML_START_F1_BLUE+QString("Test started on: %1 is finished. Client executed %2 server calls, %3 of them failed!").arg(datServerStartTestTime.toString("HH:mm:ss")).arg(nTotalCalls).arg(nTotalFailed)+HTML_END_F1);

		/*
		if (nCntThreads<err.IsOK())
		{
			ui.labTestStatus->setText(HTML_START_F1_BLUE+QString("Test started on: %1 is successfully finished. Client executed %2 server calls, %3 of them failed!").arg(datServerStartTestTime.toString("HH:mm:ss")).arg(nTotalCalls).arg(nTotalFailed)+HTML_END_F1);
		}
		else
		{
			ui.labTestStatus->setText(HTML_START_F1_BLUE+QString("Test started on: %1 finished, but server probably crashed. Check server logs!").arg(datServerStartTestTime.toString("HH:mm:ss"))+HTML_END_F1);
		}
		*/
		DbRecordSet lstTemp;
		lstTemp.copyDefinition(m_lstTimingPerAPI);
		lstTemp.merge(m_lstTiming);
		lstTemp.setColValue("THREADS",nCntThreads);
		m_lstTimingPerAPI.merge(lstTemp);


		ui.btnStartTest->setText("Start Test");
		m_nTestInProgress=STATUS_IDLE;
		ui.groupBox->setEnabled(true);
	}
	else if (m_nTestInProgress==STATUS_WAITING)
	{

		QApplication::restoreOverrideCursor();
		ui.labTestStatus->setText(HTML_START_F1_BLUE+QString("Idle")+HTML_END_F1);
		ui.btnStartTest->setText("Start Test");
		m_nTestInProgress=STATUS_IDLE;
		ui.groupBox->setEnabled(true);
	}
	else if (m_nTestInProgress==STATUS_GO_FROM_SYNC_TO_TEST)
	{
		int nDurationMin=ui.txtDuration->text().toInt();
		ui.btnStartTest->setText("Stop Test");
		ui.groupBox->setEnabled(false);
		m_datTestStartTime=QDateTime::currentDateTime();
		m_datTestDurationEnd = QDateTime::currentDateTime().addSecs(nDurationMin*60);

		//now start increasing threads from 1 to max by 5:
		m_nThreadIncrement=m_nMaxThreads/(THREAD_INCREASE_STEPS);
		if (m_nThreadIncrement==0)
			m_nThreadIncrement=1;

		m_nThreadIncrement=5000;//quick and dirty, give me all

		m_nDurationIncrementSec=nDurationMin*60/(THREAD_INCREASE_STEPS+1);
		if (m_nDurationIncrementSec>30)
			m_nDurationIncrementSec=30;

		m_nThreadIncrementCounter=1;

		ui.labTestStatus->setText(HTML_START_F1_BLUE+QString("Test in progress...")+HTML_END_F1);
		m_nTestInProgress=STATUS_TEST_IN_PROGRESS;
		RefreshWorkThreads();
	}
	else
	{
		m_lstTimingPerAPI.clear();
		m_lstTiming.clear();
		m_lstTimingSingleAPI.clear();
		ui.tableAll->RefreshDisplay();
		ui.tablePerAPI->RefreshDisplay();

		m_nMaxThreads=ui.txtThreads->text().toInt();
		if (m_nMaxThreads<=0 || m_nMaxThreads>20000)
		{
			QMessageBox::warning(this,tr("Warning"),tr("Maximum number of threads must be in range from 1 to 20000!"));
			return;
		}
		int nDurationMin=ui.txtDuration->text().toInt();
		if (nDurationMin<1 || nDurationMin>1440)
		{
			QMessageBox::warning(this,tr("Warning"),tr("Maximum duration must be in range from 1 to 1440 minutes!"));
			return;
		}
		
		
		Status err;
		RefreshServerTime(err);
		if (!err.IsOK())
		{
			QMessageBox::warning(this,tr("Warning"),QString(tr("Test can not be started as server is unreachable, check server URL, user and password. Error was: %1!")).arg(err.getErrorText()));
			return;
		}
		ui.btnStartTest->setText("Stop Test");
		ui.groupBox->setEnabled(false);
		
		m_datTestStartTime=QDateTime::currentDateTime();
		m_datTestDurationEnd = QDateTime::currentDateTime().addSecs(nDurationMin*60);

		//now start increasing threads from 1 to max by 5:
		//m_nThreadIncrement=m_nMaxThreads;
		m_nThreadIncrement=m_nMaxThreads/(THREAD_INCREASE_STEPS);
		if (m_nThreadIncrement==0)
			m_nThreadIncrement=1;

		m_nThreadIncrement=5000;//quick and dirty, give me all
		
		m_nDurationIncrementSec=nDurationMin*60/(THREAD_INCREASE_STEPS+1);
		if (m_nDurationIncrementSec>30)
			m_nDurationIncrementSec=30;

		m_nThreadIncrementCounter=1;

		//m_nDurationIncrementSec=;

		//qDebug()<<m_datServerTime.toString("yyyyMMdd-HH:mm");
		//qDebug()<<m_datNextTestTime.toString("yyyyMMdd-HH:mm");

		int nSec = m_datServerTime.secsTo(m_datNextTestTime);

		if (m_datServerTime.secsTo(m_datNextTestTime)>0)
		{
			//ask: wait for server sync time to synchronize with other test clients or proceed immediately with test
			int nResult = QMessageBox::question(this, "", tr("Wait for server sync time to synchronize with other test clients?"), tr("Yes"),tr("No"));
			if (nResult==0)
			{
				m_nTestInProgress=STATUS_WAITING;
				ui.labTestStatus->setText(HTML_START_F1_BLUE+QString("Waiting for other clients...")+HTML_END_F1);
				return;
			}
		}

		ui.labTestStatus->setText(HTML_START_F1_BLUE+QString("Test in progress...")+HTML_END_F1);
		m_nTestInProgress=STATUS_TEST_IN_PROGRESS;
		RefreshWorkThreads();

	}
}

void ServerPerformanceTester::on_btnSetTime_clicked()
{
	Status pStatus;
	QString strHr=ui.txtTestTime->text().left(ui.txtTestTime->text().indexOf(":"));
	QString strMin=ui.txtTestTime->text().mid(ui.txtTestTime->text().indexOf(":")+1);
	QDateTime datNextTestTime=QDateTime(QDate::currentDate(),QTime(strHr.toInt(),strMin.toInt()));
	if (datNextTestTime<QDateTime::currentDateTime())
		datNextTestTime.addDays(1);

	QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
	if (ui.rbCS->isChecked())
		CS_SetServerTime(pStatus,ui.txtServerURL->text(),ui.txtUser->text(),ui.txtPass->text(),datNextTestTime);
	else
		ES_SetServerTime(pStatus,ui.txtServerURL->text(),ui.txtUser->text(),ui.txtPass->text(),datNextTestTime);
	QApplication::restoreOverrideCursor();
	if (!pStatus.IsOK())
	{
		QMessageBox::critical(this,tr("Error"),QString(tr("Error while trying to reach server, error was: %1!")).arg(pStatus.getErrorText()));
		return;
	}

	m_datNextTestTime=datNextTestTime;
}


void ServerPerformanceTester::on_btnGetTime_clicked()
{
	Status err;
	RefreshServerTime(err);
	if (!err.IsOK())
	{
		QMessageBox::critical(this,tr("Warning"),QString(tr("Server is unreachable, error is: %1")).arg(err.getErrorText()));
		return;
	}
}



int ServerPerformanceTester::CS_GetServerTime( Status &pStatus,QString strServerURL, QString strUser, QString strPass, QDateTime& Ret_datCurrentServerTime, QDateTime& Ret_datTimeNextTest, int& Ret_nActiveClientSocketCnt )
{
	QUrl url(strServerURL);
	QString strServerIP=url.host();
	int nServerPort=url.port();
	QString strScheme=url.scheme();
	int nServerIsSSL=0;
	if (strScheme.toLower()=="https")
		nServerIsSSL=1;

	int nElapsedMsec=0;

	RestHTTPClient client;
	client.SetConnectionSettings(strServerIP,nServerPort,nServerIsSSL,5);
	client.Connect(pStatus);
	if(!pStatus.IsOK())
	{
		//pStatus.setError(1,"Server offline");
		return nElapsedMsec;
	}

	//generate conce & auth token
	Sha256Hash Hasher;
	QString strConce=Hasher.GetHash(Authenticator::GenerateRandomSequence("iu")).toHex();
	QString strAuthTokenServer=Hasher.GetHash(QString(strUser+strConce+SECRET_SPICE_PASSWORD+strPass).toUtf8()).toHex();

	client.msg_send.AddParameter(&strUser);
	client.msg_send.AddParameter(&strConce);
	client.msg_send.AddParameter(&strAuthTokenServer);

	QTime timer;
	timer.start();
	client.RestSend(pStatus,"POST","/rest/service/getservertime");
	int nElapsed=timer.elapsed();

	if(!pStatus.IsOK()) return nElapsedMsec;
	
	client.msg_recv.GetParameter(0,&Ret_datCurrentServerTime,pStatus);	
	if (!pStatus.IsOK())return nElapsedMsec;
	client.msg_recv.GetParameter(1,&Ret_datTimeNextTest,pStatus);	
	if (!pStatus.IsOK())return nElapsedMsec;
	client.msg_recv.GetParameter(2,&Ret_nActiveClientSocketCnt,pStatus);	
	if (!pStatus.IsOK())return nElapsedMsec;

	return nElapsedMsec;
}


int ServerPerformanceTester::ES_GetServerTime( Status &pStatus,QString strServerURL, QString strUser, QString strPass, QDateTime& Ret_datCurrentServerTime, QDateTime& Ret_datTimeNextTest, int& Ret_nActiveClientSocketCnt )
{
	
	QUrl url(strServerURL);
	QString strServerIP=url.host();
	int nServerPort=url.port();
	QString strScheme=url.scheme();
	int nServerIsSSL=0;
	if (strScheme.toLower()=="https")
		nServerIsSSL=1;

	int nElapsedMsec=0;

	RestHTTPClient client;
	client.SetConnectionSettings(strServerIP,nServerPort,nServerIsSSL,5);
	client.Connect(pStatus);
	if(!pStatus.IsOK()) return nElapsedMsec;

	//login:
	QString strSession;
	client.RestStartSession(pStatus,RestHTTPClient::REST_TYPE_COMMUNICATOR,strUser,strPass,strSession);
	if(!pStatus.IsOK()) return nElapsedMsec;
	int nContactID,nPersonID;
	client.RestGetCurrentSessionData(strSession,nPersonID,nContactID);


	QTime timer;
	timer.start();
	client.RestSend(pStatus,"GET","/rest/server/getservertime",strSession);
	int nElapsed=timer.elapsed();

	if(!pStatus.IsOK()) return nElapsedMsec;


	client.msg_recv.GetParameter(0,&Ret_datCurrentServerTime,pStatus);	
	if (!pStatus.IsOK())return nElapsedMsec;
	client.msg_recv.GetParameter(1,&Ret_datTimeNextTest,pStatus);	
	if (!pStatus.IsOK())return nElapsedMsec;
	client.msg_recv.GetParameter(2,&Ret_nActiveClientSocketCnt,pStatus);	
	if (!pStatus.IsOK())return nElapsedMsec;

	client.RestEndSession(pStatus,RestHTTPClient::REST_TYPE_COMMUNICATOR,strSession);

	return nElapsedMsec;
}

int ServerPerformanceTester::CS_SetServerTime( Status &pStatus,QString strServerURL, QString strUser, QString strPass, QDateTime datTimeNextTest )
{
	QUrl url(strServerURL);
	QString strServerIP=url.host();
	int nServerPort=url.port();
	QString strScheme=url.scheme();
	int nServerIsSSL=0;
	if (strScheme.toLower()=="https")
		nServerIsSSL=1;

	int nElapsedMsec=0;

	RestHTTPClient client;
	client.SetConnectionSettings(strServerIP,nServerPort,nServerIsSSL,5);
	client.Connect(pStatus);
	if(!pStatus.IsOK())
	{
		return nElapsedMsec;
	}

	//generate conce & auth token
	Sha256Hash Hasher;
	QString strConce=Hasher.GetHash(Authenticator::GenerateRandomSequence("iu")).toHex();
	QString strAuthTokenServer=Hasher.GetHash(QString(strUser+strConce+SECRET_SPICE_PASSWORD+strPass).toUtf8()).toHex();

	client.msg_send.AddParameter(&strUser);
	client.msg_send.AddParameter(&strConce);
	client.msg_send.AddParameter(&strAuthTokenServer);
	client.msg_send.AddParameter(&datTimeNextTest);

	QTime timer;
	timer.start();
	client.RestSend(pStatus,"POST","/rest/service/setnexttesttime");
	int nElapsed=timer.elapsed();
	if(!pStatus.IsOK()) return nElapsedMsec;
	
	return nElapsedMsec;
}

int ServerPerformanceTester::ES_SetServerTime( Status &pStatus,QString strServerURL, QString strUser, QString strPass, QDateTime datTimeNextTest )
{
	QUrl url(strServerURL);
	QString strServerIP=url.host();
	int nServerPort=url.port();
	QString strScheme=url.scheme();
	int nServerIsSSL=0;
	if (strScheme.toLower()=="https")
		nServerIsSSL=1;

	int nElapsedMsec=0;

	RestHTTPClient client;
	client.SetConnectionSettings(strServerIP,nServerPort,nServerIsSSL,5);
	client.Connect(pStatus);
	if(!pStatus.IsOK()) return nElapsedMsec;

	//login:
	QString strSession;
	client.RestStartSession(pStatus,RestHTTPClient::REST_TYPE_COMMUNICATOR,strUser,strPass,strSession);
	if(!pStatus.IsOK()) return nElapsedMsec;
	int nContactID,nPersonID;
	client.RestGetCurrentSessionData(strSession,nPersonID,nContactID);

	client.msg_send.AddParameter(&datTimeNextTest);

	QTime timer;
	timer.start();
	client.RestSend(pStatus,"POST","/rest/server/setnexttesttime",strSession);
	int nElapsed=timer.elapsed();

	if(!pStatus.IsOK()) return nElapsedMsec;

	client.RestEndSession(pStatus,RestHTTPClient::REST_TYPE_COMMUNICATOR,strSession);

	return nElapsedMsec;

}

void ServerPerformanceTester::RefreshServerTime(Status &pStatus)
{
	QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
	if (ui.rbCS->isChecked())
		CS_GetServerTime(pStatus,ui.txtServerURL->text(),ui.txtUser->text(),ui.txtPass->text(),m_datServerTime,m_datNextTestTime,m_nActiveServerSockets);
	else
		ES_GetServerTime(pStatus,ui.txtServerURL->text(),ui.txtUser->text(),ui.txtPass->text(),m_datServerTime,m_datNextTestTime,m_nActiveServerSockets);
	QApplication::restoreOverrideCursor();

	if (!pStatus.IsOK())
	{
		ui.labServerStatus->setText(HTML_START_F1_RED+QString("offline")+HTML_END_F1);
	}
	else
	{
		m_nLocalTimeOffsetSeconds =QDateTime::currentDateTime().secsTo(m_datServerTime);
		if (m_datNextTestTime.isValid())
			ui.txtTestTime->setText(m_datNextTestTime.toString("HH:mm"));
		else
			QMessageBox::information(this,"Warning","Server test time is not set!");
		ui.labServerStatus->setText(HTML_START_F1_GREEN+QString("online")+HTML_END_F1);
	}
	
}

void ServerPerformanceTester::StopWorkThreads()
{
	int nSize=m_lstThreads.size();
	for (int i=0;i<nSize;i++)
	{
		
		QList<Status> lstErrors=m_lstThreads.at(i)->GetErrorList();
		for (int k=0;k<lstErrors.size();k++)
		{
			Status err=lstErrors.at(k);
			QString strErr=err.getErrorText();
			qDebug()<<strErr;
		}
		
		m_lstThreads.at(i)->Stop();
		//m_lstThreads.at(i)->deleteLater();
		//qDebug()<<"Thread "<<i<<" stopped";
	}

	m_lstThreads.clear();
}


void ServerPerformanceTester::RefreshWorkThreads()
{
	//qDebug()<<"Main thread "<<ThreadIdentificator::GetCurrentThreadID()<<" checking slave threads!";

	//Check if all threads are alive: if some thread died...notify user and ask if it wants to abort testing...
	int nSize=m_lstThreads.size();
	for (int i=0;i<nSize;i++)
	{
		if (!m_lstThreads.at(i)->isRunning())
		{
			Status last_err=m_lstThreads.at(i)->GetLastError();
			if (!last_err.IsOK())
			{
				QMessageBox::warning(this,tr("Warning"),tr("Client thread terminated unexpectedly, server probably crashed. Test will be aborted!"));
				on_btnStartTest_clicked();
				return;
/*
				int nResult = QMessageBox::question(this, "", tr("Client thread terminated unexpectedly, server probably crashed. Test will be aborted!"), tr("Yes"),tr("No"));
				if (nResult==0)
				{
					on_btnStartTest_clicked();
					return;
				}
*/
			}
		}
	}
	bool bFirstTime=false;

	//Start from 1, check if we need to go further
	if (m_lstThreads.size()==0)
	{
		WorkThread *pThread;
		if (ui.rbCS->isChecked())
			pThread= new WorkThreadCS(NULL);
		else
			pThread= new WorkThreadES(NULL);
		m_lstThreads.append(pThread);
		pThread->Start(ui.txtServerURL->text(),ui.txtUser->text(),ui.txtPass->text());
		bFirstTime=true;
	}
	else
	{
		if (m_datTestStartTime.addSecs(m_nDurationIncrementSec*m_nThreadIncrementCounter)<=QDateTime::currentDateTime())
		{
			m_nThreadIncrementCounter++;
			int nAddThreads=0;
			//start new threads:
			if (m_lstThreads.size()==1)
				nAddThreads=m_nThreadIncrement-1;
			else
				nAddThreads=m_nThreadIncrement;

			if (nAddThreads==0)nAddThreads=1;

			if (nAddThreads+m_lstThreads.size()>m_nMaxThreads) //hit end? block this 
			{
				nAddThreads=m_nMaxThreads-m_lstThreads.size();
			}

			if (nAddThreads>0)
			{
				//first store actual data to other table:
				DbRecordSet lstTemp;
				lstTemp.copyDefinition(m_lstTimingPerAPI);
				lstTemp.merge(m_lstTiming);
				//m_lstTiming.Dump();
				//lstTemp.Dump();
				lstTemp.setColValue("THREADS",m_lstThreads.size());
				m_lstTimingPerAPI.merge(lstTemp);
				//m_lstTimingPerAPI.Dump();

								
				QString strEmailDomain;
				QString strUserStart=ui.txtUser->text();
				QString strPassStart=ui.txtPass->text();
				if (strUserStart.contains("@"))
				{
					strEmailDomain=strUserStart.mid(strUserStart.indexOf("@"));
					strUserStart=strUserStart.left(strUserStart.indexOf("@"));
				}
				int nUserFirst=strUserStart.right(4).toInt();
				strUserStart.chop(4);
				strPassStart.chop(4);


				for (int i=0;i<nAddThreads;i++)
				{
					WorkThread *pThread;
					if (ui.rbCS->isChecked())
						pThread= new WorkThreadCS(NULL);
					else
						pThread= new WorkThreadES(NULL);
					m_lstThreads.append(pThread);
					int nIdx=m_lstThreads.size()-1;
					//calc user/password:
					QString strUser=strUserStart+ConvertNumValue(nUserFirst+nIdx)+strEmailDomain;
					QString strPass=strPassStart+ConvertNumValue(nUserFirst+nIdx);

					pThread->Start(ui.txtServerURL->text(),strUser,strPass);
				}
			}
		}
	}

	//get newest data if not first time: get all data and assemble data list -> refresh diaplay and status
	if (!bFirstTime)
	{
		int nSize=m_lstThreads.size();
		m_lstTiming.setColValue("AVG_RESPONSE",0);
		m_lstTiming.setColValue("CNT_SUCCESS",0);
		m_lstTiming.setColValue("CNT_FAIL",0);
		m_lstTiming.setColValue("LATEST_RESPONSE",0);
		
		QHash<QString,QMap<QDateTime,int>> lstLatestRespTime; //always show most latest
		//api,<resp, time>

		for (int i=0;i<nSize;i++)
		{
			//min
			QHash<QString,WorkThreadTiming> dataTime = m_lstThreads.at(i)->GetMin();
			//qDebug()<<dataTime.size();
			{
				QHashIterator <QString,WorkThreadTiming> x(dataTime);
				while (x.hasNext()) 
				{
					x.next();
					QString strAPI=x.key();
					int nRow=m_lstTiming.find("API",strAPI,true);
					if (nRow<0)
					{
						m_lstTiming.addRow();
						nRow=m_lstTiming.getRowCount()-1;
						m_lstTiming.setData(nRow,"API",strAPI);
						m_lstTiming.setData(nRow,"MIN_RESPONSE",-1);
						m_lstTiming.setData(nRow,"MAX_RESPONSE",0);
						m_lstTiming.setData(nRow,"AVG_RESPONSE",0);
						m_lstTiming.setData(nRow,"LATEST_RESPONSE",0);
					}

					int nRespMsec = 	m_lstTiming.getDataRef(nRow,"MIN_RESPONSE").toInt();
					if (nRespMsec>x.value().m_nResponseTimeMsec || nRespMsec<0)
					{
						m_lstTiming.setData(nRow,"MIN_RESPONSE",x.value().m_nResponseTimeMsec);
						m_lstTiming.setData(nRow,"MIN_RESPONSE_TIME",x.value().m_datTime);
					}
				}
			}

			//max
			dataTime = m_lstThreads.at(i)->GetMax();
			{
				QHashIterator <QString,WorkThreadTiming> x(dataTime);
				while (x.hasNext()) 
				{
					x.next();
					QString strAPI=x.key();
					int nRow=m_lstTiming.find("API",strAPI,true);
					if (nRow<0)
					{
						m_lstTiming.addRow();
						nRow=m_lstTiming.getRowCount()-1;
						m_lstTiming.setData(nRow,"API",strAPI);
						m_lstTiming.setData(nRow,"MIN_RESPONSE",-1);
						m_lstTiming.setData(nRow,"MAX_RESPONSE",0);
						m_lstTiming.setData(nRow,"AVG_RESPONSE",0);
						m_lstTiming.setData(nRow,"LATEST_RESPONSE",0);
					}
					int nRespMsec = 	m_lstTiming.getDataRef(nRow,"MAX_RESPONSE").toInt();
					if (nRespMsec<x.value().m_nResponseTimeMsec)
					{
						m_lstTiming.setData(nRow,"MAX_RESPONSE",x.value().m_nResponseTimeMsec);
						m_lstTiming.setData(nRow,"MAX_RESPONSE_TIME",x.value().m_datTime);
					}
				}
			}

			//latest: keep max
			dataTime = m_lstThreads.at(i)->GetLatest();
			{
				QHashIterator <QString,WorkThreadTiming> x(dataTime);
				while (x.hasNext()) 
				{
					x.next();
					QString strAPI=x.key();
					int nRow=m_lstTiming.find("API",strAPI,true);
					//qDebug()<<x.value().m_nResponseTimeMsec;
					//qDebug()<<strAPI<<": "<<x.value().m_nResponseTimeMsec;
					if (nRow<0)
					{
						m_lstTiming.addRow();
						nRow=m_lstTiming.getRowCount()-1;
						m_lstTiming.setData(nRow,"API",strAPI);
						m_lstTiming.setData(nRow,"MIN_RESPONSE",-1);
						m_lstTiming.setData(nRow,"MAX_RESPONSE",0);
						m_lstTiming.setData(nRow,"AVG_RESPONSE",0);
						m_lstTiming.setData(nRow,"LATEST_RESPONSE",0);
					}

					QMap<QDateTime,int> lstExisted=	lstLatestRespTime.value(strAPI);
					lstExisted[x.value().m_datTime]=x.value().m_nResponseTimeMsec;
					lstLatestRespTime[strAPI] = lstExisted;
					/*
					int nRespMsec = 	m_lstTiming.getDataRef(nRow,"LATEST_RESPONSE").toInt();
					if ((x.value().m_nResponseTimeMsec<nRespMsec && nRespMsec>0) || nRespMsec==0)
					{
						m_lstTiming.setData(nRow,"LATEST_RESPONSE",x.value().m_nResponseTimeMsec); //store higher
					}
					*/
				}
			}
			
			//average: sum all then calc averge on /no threads
			dataTime = m_lstThreads.at(i)->GetAvg();
			{
				QHashIterator <QString,WorkThreadTiming> x(dataTime);
				while (x.hasNext()) 
				{
					x.next();
					QString strAPI=x.key();
					int nRow=m_lstTiming.find("API",strAPI,true);
					
					if (nRow<0)
					{
						m_lstTiming.addRow();
						nRow=m_lstTiming.getRowCount()-1;
						m_lstTiming.setData(nRow,"API",strAPI);
						m_lstTiming.setData(nRow,"MIN_RESPONSE",-1);
						m_lstTiming.setData(nRow,"MAX_RESPONSE",0);
						m_lstTiming.setData(nRow,"AVG_RESPONSE",0);
						m_lstTiming.setData(nRow,"LATEST_RESPONSE",0);
					}
					int nRespMsec = m_lstTiming.getDataRef(nRow,"AVG_RESPONSE").toInt()+x.value().m_nResponseTimeMsec;
					m_lstTiming.setData(nRow,"AVG_RESPONSE",nRespMsec);
				}
			}

			QHash<QString,qint64> dataCnt = m_lstThreads.at(i)->GetApiSuccessCnt();
			{
				QHashIterator <QString,qint64> x(dataCnt);
				while (x.hasNext()) 
				{
					x.next();
					QString strAPI=x.key();
					int nRow=m_lstTiming.find("API",strAPI,true);
					if (nRow<0)
					{
						m_lstTiming.addRow();
						nRow=m_lstTiming.getRowCount()-1;
						m_lstTiming.setData(nRow,"API",strAPI);
						m_lstTiming.setData(nRow,"MIN_RESPONSE",-1);
						m_lstTiming.setData(nRow,"MAX_RESPONSE",0);
						m_lstTiming.setData(nRow,"AVG_RESPONSE",0);
						m_lstTiming.setData(nRow,"LATEST_RESPONSE",0);
					}
					qint64 nCnt = m_lstTiming.getDataRef(nRow,"CNT_SUCCESS").toInt()+x.value();
					m_lstTiming.setData(nRow,"CNT_SUCCESS",(int)nCnt);
				}
			}

			dataCnt = m_lstThreads.at(i)->GetApiFailCnt();
			{
				QHashIterator <QString,qint64> x(dataCnt);
				while (x.hasNext()) 
				{
					x.next();
					QString strAPI=x.key();
					int nRow=m_lstTiming.find("API",strAPI,true);
					if (nRow<0)
					{
						m_lstTiming.addRow();
						nRow=m_lstTiming.getRowCount()-1;
						m_lstTiming.setData(nRow,"API",strAPI);
						m_lstTiming.setData(nRow,"MIN_RESPONSE",-1);
						m_lstTiming.setData(nRow,"MAX_RESPONSE",0);
						m_lstTiming.setData(nRow,"AVG_RESPONSE",0);
						m_lstTiming.setData(nRow,"LATEST_RESPONSE",0);
					}
					qint64 nCnt = m_lstTiming.getDataRef(nRow,"CNT_FAIL").toInt()+x.value();
					m_lstTiming.setData(nRow,"CNT_FAIL",(int)nCnt);
				}
			}

		}


		//recalc rest:
		nSize=m_lstTiming.getRowCount();
		for (int i=0;i<nSize;i++)
		{
			//calc avg:
			int nTotalAvgResponseMsec=m_lstTiming.getDataRef(i,"AVG_RESPONSE").toInt();
			if (m_lstThreads.size()>0)
			{
				nTotalAvgResponseMsec=nTotalAvgResponseMsec/m_lstThreads.size();
				m_lstTiming.setData(i,"AVG_RESPONSE",nTotalAvgResponseMsec);
			}

			//calc total cnt:
			int nCntTotalAPI=m_lstTiming.getDataRef(i,"CNT_FAIL").toInt()+m_lstTiming.getDataRef(i,"CNT_SUCCESS").toInt();
			m_lstTiming.setData(i,"CNT_TOTAL",nCntTotalAPI);
		}

		QHashIterator<QString,QMap<QDateTime,int>> x(lstLatestRespTime);
		while (x.hasNext()) 
		{
			x.next();
			//qDebug()<<"API: "<<x.key();
			QString strAPI=x.key();
			QMap<QDateTime,int> lstExisted=	x.value();
			QMapIterator<QDateTime,int> y(lstExisted);
			while (y.hasNext()) {
				y.next();
				int nRow=m_lstTiming.find("API",strAPI,true);
				if (nRow>=0)
				{
					int nVal=y.value();
					m_lstTiming.setData(nRow,"LATEST_RESPONSE",nVal);
				}
				//qDebug()<<y.key()<<"  "<<y.value();
				//break;
			}
		}
	}

	ui.tableAll->RefreshDisplay();

	//refresh server time and sockets
	if (m_lstThreads.size()>0)
	{
		m_datServerTime=m_lstThreads.at(0)->GetLatestServerTime();
		m_nActiveServerSockets=m_lstThreads.at(0)->GetServerSockets();
	}
	

	//Test in progress: working threads, current server time, active server sockets, elapsed time
	qint64 nSec=m_datTestStartTime.secsTo(QDateTime::currentDateTime());
	QTime timeElapsed=DataHelper::GetTimeFromSeconds(nSec);
	QString strElapsed=timeElapsed.toString("HH:mm:ss");

	QString strMsg=QString("Test in progress: threads = %1, server time = %2, active server sockets = %3, elapsed time = %4");
	strMsg=strMsg.arg(m_lstThreads.size()).arg(m_datServerTime.toString("HH:mm:ss")).arg(m_nActiveServerSockets).arg(strElapsed);
		
	ui.labTestStatus->setText(HTML_START_F1_BLUE+strMsg+HTML_END_F1);
}

void ServerPerformanceTester::OnRowChanged()
{
	DbRecordSet rowSelected;
	ui.tableAll->GetSelection(rowSelected);

	if (rowSelected.getRowCount()>0)
	{
		//m_lstTimingPerAPI.Dump();

		QString strAPI=rowSelected.getDataRef(0,"API").toString();
		m_lstTimingPerAPI.find("API",strAPI,false,false,true,true);
		m_lstTimingSingleAPI.clear();
		m_lstTimingSingleAPI.merge(m_lstTimingPerAPI,true);
		m_lstTimingPerAPI.sort("THREADS");
		ui.labTestResult->setText("Measured values per API: "+strAPI);
	}
	else
	{
		ui.labTestResult->setText("Measured values per API:  (click on row in upper table)");
		m_lstTimingSingleAPI.clear();
		ui.tablePerAPI->clear();
	}
	ui.tablePerAPI->RefreshDisplay();
}


QString ServerPerformanceTester::ConvertNumValue( int nValue )
{
	QString strVal=QString::number(nValue);
	if (strVal.size()==1)
		return "000"+strVal;
	if (strVal.size()==2)
		return "00"+strVal;
	if (strVal.size()==3)
		return "0"+strVal;
	return strVal;
}

void ServerPerformanceTester::on_btnRefreshServerStatus_clicked()
{
	Status err;
	RefreshServerTime(err);
	if (!err.IsOK())
	{
		QMessageBox::critical(this,tr("Warning"),QString(tr("Server is unreachable, error is: %1")).arg(err.getErrorText()));
		return;
	}

}