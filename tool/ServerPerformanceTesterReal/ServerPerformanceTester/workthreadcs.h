#ifndef WORKTHREADCS_H
#define WORKTHREADCS_H

#include "workthread.h"
#include "common/common/dbrecordset.h"
#include "trans/trans/resthttpclient.h"


class WorkThreadExecutorCS : public WorkThreadExecutor
{
	Q_OBJECT

public:
	WorkThreadExecutorCS(WorkThread *pParent);
	~WorkThreadExecutorCS();

public slots:
	void ExecuteTask();

private:
	int GetServerTime(Status &pStatus,QDateTime& Ret_datCurrentServerTime, QDateTime& Ret_datTimeNextTest, int& Ret_nActiveClientSocketCnt, RestHTTPClient *client);
	int IsRegistered(Status &pStatus,QString strEmail,int &Ret_IsRegistered, QString &Ret_strESHost, RestHTTPClient *client);
	int StoreData(Status &pStatus, QString strEmail, int nType, QString strName, QString strData, RestHTTPClient *client);
	int ReadDataList(Status &pStatus, QString strEmail, int nType, DbRecordSet &Ret_List, RestHTTPClient *client);
	int GetData(Status &pStatus, QString strEmail, int nType, QString strName, QString &Ret_strData, RestHTTPClient *client);
};


class WorkThreadCS : public WorkThread
{
	Q_OBJECT

public:
	WorkThreadCS(QObject *parent);
	~WorkThreadCS();

protected:
	void run();
	WorkThreadExecutorCS *m_pExecutor;
};



#endif // WORKTHREADCS_H
