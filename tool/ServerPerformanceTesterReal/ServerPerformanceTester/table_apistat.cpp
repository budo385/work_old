#include "table_apistat.h"
#include <QComboBox>
#include <QUrl>
#include <QFileDialog>
#include <QLabel>
#include <QListView>
#include <QTreeView>
#include <QMessageBox>
#include <QHeaderView>


#define COL_CURRENT		3

Table_ApiStat::Table_ApiStat(QWidget * parent)
:UniversalTableWidgetEx(parent)
{
	//connect(this,SIGNAL(SignalDataChanged(int,int)),this,SLOT(OnCellChanged(int,int)));
}


Table_ApiStat::~Table_ApiStat()
{

}

void Table_ApiStat::DefineList( DbRecordSet *plstData )
{
	plstData->destroy();
	plstData->addColumn(QVariant::String,"API");
	plstData->addColumn(QVariant::Int,"THREADS");
	plstData->addColumn(QVariant::Int,"AVG_RESPONSE");
	plstData->addColumn(QVariant::Int,"MIN_RESPONSE");
	plstData->addColumn(QVariant::Int,"MAX_RESPONSE");
}


void Table_ApiStat::Initialize(DbRecordSet *plstData)
{
	//set data source for table, enable drop
	DbRecordSet columns;
	AddColumnToSetup(columns,"THREADS",tr("Threads"),80,false,"" ,COL_TYPE_TEXT,"");
	AddColumnToSetup(columns,"AVG_RESPONSE",tr("Avg. Resp."),90,false,"" ,COL_TYPE_TEXT,"");
	AddColumnToSetup(columns,"MIN_RESPONSE",tr("Min. Resp."),90,false,"" ,COL_TYPE_TEXT,"");
	AddColumnToSetup(columns,"MAX_RESPONSE",tr("Max. Resp."),90,false,"" ,COL_TYPE_TEXT,"");
	
	UniversalTableWidgetEx::Initialize(plstData,&columns);
	horizontalHeader()->setStretchLastSection(true);

	//CreateContextMenu();
	/*
	QList<QAction*> lstActions;
	QAction *SeparatorAct;

	QAction* pAction = new QAction(tr("UnPublish"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnUnPublish()));
	lstActions.append(pAction);

	lstActions.append(pAction);
	SetContextMenuActions(lstActions);
	*/

	//connect(this,SIGNAL(cellDoubleClicked(int,int)),this,SLOT(OnCellDoubleClicked(int,int)));
}




