#ifndef TABLE_TIMINGS_H
#define TABLE_TIMINGS_H

#include "universaltablewidgetex.h"

class Table_Timings : public UniversalTableWidgetEx
{
	Q_OBJECT

public:
	Table_Timings(QWidget *parent);
	~Table_Timings();

	static void DefineTimingList(DbRecordSet *plstData);
	void Initialize(DbRecordSet *plstData);


private slots:

public slots:

protected:
	
	void Data2CustomWidget(int nRow, int nCol);
	void CustomWidget2Data(int nRow, int nCol);

private:

};

#endif // TABLE_TIMINIGS_H
