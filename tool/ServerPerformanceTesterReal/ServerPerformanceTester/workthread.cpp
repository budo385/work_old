#include "workthread.h"
#include <QUrl>
#include <QTimer>
#include <QDebug>
#include "common/common/threadid.h"


WorkThread::WorkThread(QObject *parent)
	: QThread(parent)
{
	m_bStopCommandIssued=false;
}

WorkThread::~WorkThread()
{
}

void WorkThread::Start(QString strServerURL, QString strUsername, QString strPassword, bool bKeepHistory)
{
	m_bStopCommandIssued=false;
	m_strServerURL=strServerURL;
	m_bKeepHistory=bKeepHistory;

	QUrl url(strServerURL);
	m_strServerIP=url.host();
	m_nServerPort=url.port();
	QString strScheme=url.scheme();
	m_nServerIsSSL=0;
	if (strScheme.toLower()=="https")
		m_nServerIsSSL=1;

	m_strUser=strUsername;
	m_strPass=strPassword;

	QWriteLocker lck(&m_lck);

	m_lstAll.clear();
	m_minTime.clear();
	m_maxTime.clear();
	m_avgTime.clear();
	m_latestTime.clear();

	start();
}

void WorkThread::Stop()
{
	m_lck.lockForWrite();
	m_bStopCommandIssued=true;
	m_lck.unlock();
	
	if(isRunning())
	{
		quit();
		bool bOK=wait(10);		//wait 100msec
		if (!bOK)
		{
			QTimer::singleShot(0,this,SLOT(OnStopThread())); 
		}
		else
			delete this;
	}
	else
		delete this;
}

/*
void WorkThread::run()
{
	qDebug()<<"Worker in thread "<<ThreadIdentificator::GetCurrentThreadID()<<" is crunching server!!";
	exec();
	QTimer::singleShot(0,this,SLOT(ExecuteTask()));
	
}
*/


WorkThreadExecutor::WorkThreadExecutor( WorkThread *pParent )
{
	m_Parent=pParent;
	connect(this,SIGNAL(GoStopThread()),m_Parent,SLOT(OnStopThread()));
}

WorkThreadExecutor::~WorkThreadExecutor()
{

}