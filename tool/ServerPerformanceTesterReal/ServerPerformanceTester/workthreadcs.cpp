#include "workthreadcs.h"
#include "trans/trans/resthttpclient.h"
#include "common/common/sha256hash.h"
#include "common/common/datahelper.h"
#include <QDebug>
#include "common/common/threadid.h"


#define SECRET_SPICE_PASSWORD "BANDIT"

#define STORE_TIME_SUCCESS(X) QWriteLocker lck(&m_Parent->m_lck);\
					  QDateTime currServerTime=QDateTime::currentDateTime().addSecs(nSecondsOffsetLocalTime);\
					  WorkThreadTiming wrkdata(X,currServerTime,nElapsed);\
					  m_Parent->m_latestTime[X]=wrkdata;\
					  if(m_Parent->m_bKeepHistory)\
					  {\
						QList<WorkThreadTiming> lstExisting=m_Parent->m_lstAll.value(X);\
						lstExisting.append(wrkdata);\
						m_Parent->m_lstAll[X]=lstExisting;\
					  }\
					  m_Parent->m_ApiSuccessCnt[X]=m_Parent->m_ApiSuccessCnt.value(X,0)+1;\
					  m_Parent->m_ApiElapsedTimeTotal[X]=m_Parent->m_ApiElapsedTimeTotal.value(X,0)+nElapsed;\
					  int nAvgMsec= m_Parent->m_ApiElapsedTimeTotal.value(X,0)/m_Parent->m_ApiSuccessCnt.value(X,1);\
					  m_Parent->m_avgTime[X]=WorkThreadTiming(X,currServerTime,nAvgMsec);\
					  if (m_Parent->m_ApiElapsedTimeMin.value(X,0)==0 || (m_Parent->m_ApiElapsedTimeMin.value(X,0)>0 && m_Parent->m_ApiElapsedTimeMin.value(X,0)>nElapsed))\
						{m_Parent->m_ApiElapsedTimeMin[X]=nElapsed;m_Parent->m_minTime[X]=WorkThreadTiming(X,currServerTime,nElapsed);}\
					  if (m_Parent->m_ApiElapsedTimeMax.value(X,0)==0 || (m_Parent->m_ApiElapsedTimeMax.value(X,0)>0 && m_Parent->m_ApiElapsedTimeMax.value(X,0)<nElapsed))\
						{m_Parent->m_ApiElapsedTimeMax[X]=nElapsed;m_Parent->m_maxTime[X]=WorkThreadTiming(X,currServerTime,nElapsed);}



WorkThreadExecutorCS::WorkThreadExecutorCS(WorkThread *pParent)
	:WorkThreadExecutor(pParent)
{
}

WorkThreadExecutorCS::~WorkThreadExecutorCS()
{

}

void WorkThreadExecutorCS::ExecuteTask()
{

	m_Parent->m_ApiSuccessCnt.clear();
	m_Parent->m_ApiFailCnt.clear();
	m_Parent->m_ApiElapsedTimeTotal.clear();

	m_Parent->m_lstAll.clear();
	m_Parent->m_minTime.clear();
	m_Parent->m_maxTime.clear();
	m_Parent->m_avgTime.clear();
	m_Parent->m_latestTime.clear();

	m_Parent->m_lstErrors.clear();
	
	do 
	{
		if (m_Parent->IsStopCommandIssued())
			return;

		RestHTTPClient client;
		int nSecondsOffsetLocalTime=0;
		Status err;

		qDebug()<<"Worker in thread "<<ThreadIdentificator::GetCurrentThreadID()<<" is crunching server!!";

		//try to connect to the server
		for (int i=0;i<10;i++)
		{
			if (m_Parent->IsStopCommandIssued())
				return;

			client.SetConnectionSettings(m_Parent->m_strServerIP,m_Parent->m_nServerPort,m_Parent->m_nServerIsSSL,10); //10 seconds
			client.Connect(err);
			if(err.IsOK())
				break;
		}

		if (!client.IsConnected())
			continue;

		//------------------------------------------------------------
		//First call GetServerTime!
		//------------------------------------------------------------


		for (int i=0;i<10;i++)
		{
			if (m_Parent->IsStopCommandIssued())
				return;

			int nSecondsOffsetLocalTime=0;
			QDateTime datNextTestTime;
			QDateTime datServerTime;
			int nActiveSockets;
			Status err;
			int nElapsed=GetServerTime(err,datServerTime,datNextTestTime,nActiveSockets,&client);
			m_Parent->SetLatestServerTime(datServerTime);
			m_Parent->SetServerSockets(nActiveSockets);
			nSecondsOffsetLocalTime=datServerTime.secsTo(QDateTime::currentDateTime()); 	//adjust offset from local to server time in seconds...
			if (err.IsOK())
			{
				if (nElapsed==0)
					nElapsed=1; //just to keep vars in ok cond
				STORE_TIME_SUCCESS("GetServerTime");
			}
			else
			{
				m_Parent->StoreError(err);
				QWriteLocker lck(&m_Parent->m_lck);
				m_Parent->m_ApiFailCnt["GetServerTime"]=m_Parent->m_ApiFailCnt.value("GetServerTime",0)+1;
			}
		}
	
		//------------------------------------------------------------
		//call IsRegistered 10x
		//------------------------------------------------------------
		
		for (int i=0;i<10;i++)
		{
			if (m_Parent->IsStopCommandIssued())
				return;

			int nIsRegistered=0;
			QString strESHost;
			int nElapsed=IsRegistered(err,m_Parent->m_strUser,nIsRegistered,strESHost,&client);
			if (err.IsOK())
			{
				if (nElapsed==0)
					nElapsed=1; //just to keep vars in ok cond
				STORE_TIME_SUCCESS("IsRegistered");
			}
			else
			{
				m_Parent->StoreError(err);
				QWriteLocker lck(&m_Parent->m_lck);
				m_Parent->m_ApiFailCnt["IsRegistered"]=m_Parent->m_ApiFailCnt.value("IsRegistered",0)+1;
			}
		}

		//------------------------------------------------------------
		//call StoreData 5x //add
		//------------------------------------------------------------
		QString strMime;
		DataHelper::LoadFile(QCoreApplication::applicationDirPath()+"/mime_example_1.txt",strMime);
		if (strMime.isEmpty())
		{
			err.setError(1,"mime_example_1.txt is not found in application path!");
			m_Parent->StoreError(err);
			//emit GoStopThread();
			//m_Parent->Stop();
			//return;
		}
		for (int i=0;i<10;i++)
		{
			if (m_Parent->IsStopCommandIssued())
				return;

			int nType=0;
			int nElapsed=StoreData(err,m_Parent->m_strUser,nType,"DummyTemplate"+QString::number(i),strMime,&client);
			if (err.IsOK())
			{
				if (nElapsed==0)
					nElapsed=1; //just to keep vars in ok cond
				STORE_TIME_SUCCESS("StoreData Adding");
			}
			else
			{
				m_Parent->StoreError(err);
				QWriteLocker lck(&m_Parent->m_lck);
				m_Parent->m_ApiFailCnt["StoreData Adding"]=m_Parent->m_ApiFailCnt.value("StoreData Adding",0)+1;
			}
		}
		
		//------------------------------------------------------------
		//call ReadDataList 5x
		//------------------------------------------------------------
		for (int i=0;i<10;i++)
		{
			if (m_Parent->IsStopCommandIssued())
				return;

			int nType=0;
			DbRecordSet lstData;
			int nElapsed=ReadDataList(err,m_Parent->m_strUser,nType,lstData,&client);
			if (err.IsOK())
			{
				if (nElapsed==0)
					nElapsed=1; //just to keep vars in ok cond
				STORE_TIME_SUCCESS("ReadDataList");
			}
			else
			{
				m_Parent->StoreError(err);
				QWriteLocker lck(&m_Parent->m_lck);
				m_Parent->m_ApiFailCnt["ReadDataList"]=m_Parent->m_ApiFailCnt.value("ReadDataList",0)+1;
			}
		}
		
		//------------------------------------------------------------
		//call GetData 5x 
		//------------------------------------------------------------
		for (int i=0;i<10;i++)
		{
			if (m_Parent->IsStopCommandIssued())
				return;

			int nType=0;
			QString strData;
			int nElapsed=GetData(err,m_Parent->m_strUser,nType,"DummyTemplate"+QString::number(i),strData,&client);
			if (err.IsOK())
			{
				if (nElapsed==0)
					nElapsed=1; //just to keep vars in ok cond
				STORE_TIME_SUCCESS("GetData");
			}
			else
			{
				m_Parent->StoreError(err);
				QWriteLocker lck(&m_Parent->m_lck);
				m_Parent->m_ApiFailCnt["GetData"]=m_Parent->m_ApiFailCnt.value("GetData",0)+1;
			}
		}
		
		//------------------------------------------------------------
		//call StoreData 5x  //delete
		//------------------------------------------------------------
		for (int i=0;i<10;i++)
		{
			if (m_Parent->IsStopCommandIssued())
				return;

			int nType=0;
			int nElapsed=StoreData(err,m_Parent->m_strUser,nType,"DummyTemplate"+QString::number(i),"",&client);
			if (err.IsOK())
			{
				if (nElapsed==0)
					nElapsed=1; //just to keep vars in ok cond
				STORE_TIME_SUCCESS("StoreData Delete");
			}
			else
			{
				m_Parent->StoreError(err);
				QWriteLocker lck(&m_Parent->m_lck);
				m_Parent->m_ApiFailCnt["StoreData Delete"]=m_Parent->m_ApiFailCnt.value("StoreData Delete",0)+1;
			}
		}

		client.Disconnect(err);
		
		//exit loop
		if (m_Parent->IsStopCommandIssued())
			return;

		ThreadSleeper::Sleep(1); //sleep 1 msecond

	} while (1==1);


}

int WorkThreadExecutorCS::GetServerTime( Status &pStatus,QDateTime& Ret_datCurrentServerTime, QDateTime& Ret_datTimeNextTest, int& Ret_nActiveClientSocketCnt, RestHTTPClient *client )
{
	client->RestClearBuffers();
	int nElapsedMsec=0;
	
	//generate conce & auth token
	Sha256Hash Hasher;
	QString strConce=Hasher.GetHash(Authenticator::GenerateRandomSequence("iu")).toHex();
	QString strAuthTokenServer=Hasher.GetHash(QString(m_Parent->m_strUser+strConce+SECRET_SPICE_PASSWORD+m_Parent->m_strPass).toUtf8()).toHex();

	client->msg_send.AddParameter(&m_Parent->m_strUser);
	client->msg_send.AddParameter(&strConce);
	client->msg_send.AddParameter(&strAuthTokenServer);
	
	QTime timer;
	timer.start();
		client->RestSend(pStatus,"POST","/rest/service/getservertime");
	nElapsedMsec=timer.elapsed();

	if(!pStatus.IsOK()) return nElapsedMsec;


	client->msg_recv.GetParameter(0,&Ret_datCurrentServerTime,pStatus);	
	if (!pStatus.IsOK())return nElapsedMsec;
	client->msg_recv.GetParameter(1,&Ret_datTimeNextTest,pStatus);	
	if (!pStatus.IsOK())return nElapsedMsec;
	client->msg_recv.GetParameter(2,&Ret_nActiveClientSocketCnt,pStatus);	
	if (!pStatus.IsOK())return nElapsedMsec;

	return nElapsedMsec;
}

int WorkThreadExecutorCS::IsRegistered( Status &pStatus,QString strEmail,int &Ret_IsRegistered, QString &Ret_strESHost, RestHTTPClient *client )
{
	client->RestClearBuffers();
	int nElapsedMsec=0;

	client->msg_send.AddParameter(&strEmail);

	QTime timer;
	timer.start();
	client->RestSend(pStatus,"POST","/rest/service/isregistered");
	nElapsedMsec=timer.elapsed();

	if(!pStatus.IsOK()) return nElapsedMsec;

	client->msg_recv.GetParameter(0,&Ret_IsRegistered,pStatus);	
	if (!pStatus.IsOK())return nElapsedMsec;
	client->msg_recv.GetParameter(1,&Ret_strESHost,pStatus);	
	if (!pStatus.IsOK())return nElapsedMsec;

	return nElapsedMsec;
}

int WorkThreadExecutorCS::StoreData( Status &pStatus, QString strEmail, int nType, QString strName, QString strData, RestHTTPClient *client )
{
	client->RestClearBuffers();
	int nElapsedMsec=0;

	//generate conce & auth token
	Sha256Hash Hasher;
	QString strConce=Hasher.GetHash(Authenticator::GenerateRandomSequence("iu")).toHex();
	QString strAuthTokenServer=Hasher.GetHash(QString(m_Parent->m_strUser+strConce+SECRET_SPICE_PASSWORD+m_Parent->m_strPass).toUtf8()).toHex();


	client->msg_send.AddParameter(&strEmail);
	client->msg_send.AddParameter(&strConce);
	client->msg_send.AddParameter(&strAuthTokenServer);
	client->msg_send.AddParameter(&nType);
	client->msg_send.AddParameter(&strName);
	client->msg_send.AddParameter(&strData);

	QTime timer;
	timer.start();
	client->RestSend(pStatus,"POST","/rest/service/storedata");
	nElapsedMsec=timer.elapsed();
	if(!pStatus.IsOK()) return nElapsedMsec;


	return nElapsedMsec;
}

int WorkThreadExecutorCS::ReadDataList( Status &pStatus, QString strEmail, int nType, DbRecordSet &Ret_List, RestHTTPClient *client )
{
	client->RestClearBuffers();
	int nElapsedMsec=0;

	//generate conce & auth token
	Sha256Hash Hasher;
	QString strConce=Hasher.GetHash(Authenticator::GenerateRandomSequence("iu")).toHex();
	QString strAuthTokenServer=Hasher.GetHash(QString(m_Parent->m_strUser+strConce+SECRET_SPICE_PASSWORD+m_Parent->m_strPass).toUtf8()).toHex();

	client->msg_send.AddParameter(&strEmail);
	client->msg_send.AddParameter(&strConce);
	client->msg_send.AddParameter(&strAuthTokenServer);
	client->msg_send.AddParameter(&nType);

	QTime timer;
	timer.start();
	client->RestSend(pStatus,"POST","/rest/service/readdatalist");
	nElapsedMsec=timer.elapsed();

	if(!pStatus.IsOK()) return nElapsedMsec;

	Ret_List.destroy();
	Ret_List.addColumn(QVariant::Int,"CT_TYPE");
	Ret_List.addColumn(QVariant::String,"CT_NAME");
	Ret_List.addColumn(QVariant::DateTime,"CT_DAT_LAST_MODIFIED");
	
	client->msg_recv.GetParameter(0,&Ret_List,pStatus);	
	if (!pStatus.IsOK())return nElapsedMsec;

	return nElapsedMsec;
}

int WorkThreadExecutorCS::GetData( Status &pStatus, QString strEmail, int nType, QString strName, QString &Ret_strData, RestHTTPClient *client )
{
	client->RestClearBuffers();
	int nElapsedMsec=0;

	//generate conce & auth token
	Sha256Hash Hasher;
	QString strConce=Hasher.GetHash(Authenticator::GenerateRandomSequence("iu")).toHex();
	QString strAuthTokenServer=Hasher.GetHash(QString(m_Parent->m_strUser+strConce+SECRET_SPICE_PASSWORD+m_Parent->m_strPass).toUtf8()).toHex();

	client->msg_send.AddParameter(&strEmail);
	client->msg_send.AddParameter(&strConce);
	client->msg_send.AddParameter(&strAuthTokenServer);
	client->msg_send.AddParameter(&nType);
	client->msg_send.AddParameter(&strName);

	QTime timer;
	timer.start();
	client->RestSend(pStatus,"POST","/rest/service/getdata");
	nElapsedMsec=timer.elapsed();

	if(!pStatus.IsOK()) return nElapsedMsec;

	client->msg_recv.GetParameter(0,&Ret_strData,pStatus);	
	if (!pStatus.IsOK())return nElapsedMsec;

	return nElapsedMsec;
}




WorkThreadCS::WorkThreadCS( QObject *parent )
	:WorkThread(parent)
{
	m_pExecutor=NULL;
}

WorkThreadCS::~WorkThreadCS()
{
	if (m_pExecutor)
		m_pExecutor->deleteLater();
}

void WorkThreadCS::run()
{
	m_pExecutor = new WorkThreadExecutorCS(this);
	qDebug()<<"Worker in thread "<<ThreadIdentificator::GetCurrentThreadID()<<" is crunching server!!";
	QTimer::singleShot(0,m_pExecutor,SLOT(ExecuteTask()));
	exec();
}