#include "workthreades.h"
#include "trans/trans/resthttpclient.h"
#include "common/common/sha256hash.h"
#include "common/common/datahelper.h"
#include "db_core/db_core/dbsqltableview.h"
#include <QDebug>
#include "common/common/threadid.h"


#define STORE_TIME_SUCCESS(X) QWriteLocker lck(&m_Parent->m_lck);\
	QDateTime currServerTime=QDateTime::currentDateTime().addSecs(nSecondsOffsetLocalTime);\
	WorkThreadTiming wrkdata(X,currServerTime,nElapsed);\
	m_Parent->m_latestTime[X]=wrkdata;\
	if(m_Parent->m_bKeepHistory)\
					  {\
					  QList<WorkThreadTiming> lstExisting=m_Parent->m_lstAll.value(X);\
					  lstExisting.append(wrkdata);\
					  m_Parent->m_lstAll[X]=lstExisting;\
					  }\
					  m_Parent->m_ApiSuccessCnt[X]=m_Parent->m_ApiSuccessCnt.value(X,0)+1;\
					  m_Parent->m_ApiElapsedTimeTotal[X]=m_Parent->m_ApiElapsedTimeTotal.value(X,0)+nElapsed;\
					  double dAvgMsec=(double)m_Parent->m_ApiElapsedTimeTotal.value(X,0)/m_Parent->m_ApiSuccessCnt.value(X,1);\
					  m_Parent->m_avgTime[X]=WorkThreadTiming(X,currServerTime,dAvgMsec);\
					  if (m_Parent->m_ApiElapsedTimeMin.value(X,0)==0 || (m_Parent->m_ApiElapsedTimeMin.value(X,0)>0 && m_Parent->m_ApiElapsedTimeMin.value(X,0)>nElapsed))\
						{m_Parent->m_ApiElapsedTimeMin[X]=nElapsed;m_Parent->m_minTime[X]=WorkThreadTiming(X,currServerTime,nElapsed);}\
						if (m_Parent->m_ApiElapsedTimeMax.value(X,0)==0 || (m_Parent->m_ApiElapsedTimeMax.value(X,0)>0 && m_Parent->m_ApiElapsedTimeMax.value(X,0)<nElapsed))\
						{m_Parent->m_ApiElapsedTimeMax[X]=nElapsed;m_Parent->m_maxTime[X]=WorkThreadTiming(X,currServerTime,nElapsed);}


WorkThreadExecutorES::WorkThreadExecutorES(WorkThread *pParent)
	:WorkThreadExecutor(pParent)
{
}

WorkThreadExecutorES::~WorkThreadExecutorES()
{

}



void WorkThreadExecutorES::ExecuteTask()
{
	m_Parent->m_ApiSuccessCnt.clear();
	m_Parent->m_ApiFailCnt.clear();
	m_Parent->m_ApiElapsedTimeTotal.clear();

	m_Parent->m_lstAll.clear();
	m_Parent->m_minTime.clear();
	m_Parent->m_maxTime.clear();
	m_Parent->m_avgTime.clear();
	m_Parent->m_latestTime.clear();

	m_Parent->m_lstErrors.clear();

	do 
	{
		if (m_Parent->IsStopCommandIssued())
			return;

		RestHTTPClient client;
		int nSecondsOffsetLocalTime=0;
		Status err;

		qDebug()<<"Worker in thread "<<ThreadIdentificator::GetCurrentThreadID()<<" is crunching server!!";

		//try to connect to the server
		for (int i=0;i<10;i++)
		{
			if (m_Parent->IsStopCommandIssued())
				return;

			client.SetConnectionSettings(m_Parent->m_strServerIP,m_Parent->m_nServerPort,m_Parent->m_nServerIsSSL,10); //10 seconds
			client.Connect(err);
			if(err.IsOK())
				break;
		}

		if (!client.IsConnected())
			continue;


		//login:
		QString strSession;
		client.RestStartSession(err,RestHTTPClient::REST_TYPE_COMMUNICATOR,m_Parent->m_strUser,m_Parent->m_strPass,strSession);
		if(!err.IsOK()) 
			continue;

		//------------------------------------------------------------
		//First call GetServerTime!
		//------------------------------------------------------------
		for (int i=0;i<10;i++)
		{
			int nSecondsOffsetLocalTime=0;
			QDateTime datNextTestTime;
			QDateTime datServerTime;
			int nActiveSockets;
			Status err;
			int nElapsed=GetServerTime(err,datServerTime,datNextTestTime,nActiveSockets,&client);
			m_Parent->SetLatestServerTime(datServerTime);
			m_Parent->SetServerSockets(nActiveSockets);

			nSecondsOffsetLocalTime=datNextTestTime.secsTo(QDateTime::currentDateTime()); 	//adjust offset from local to server time in seconds...
			if (err.IsOK())
			{
				if (nElapsed==0)
					nElapsed=1; //just to keep vars in ok cond
				STORE_TIME_SUCCESS("GetServerTime");
			}
			else
			{
				m_Parent->StoreError(err);
				QWriteLocker lck(&m_Parent->m_lck);
				m_Parent->m_ApiFailCnt["GetServerTime"]=m_Parent->m_ApiFailCnt.value("GetServerTime",0)+1;
			}
		}


		//------------------------------------------------------------
		//call ReadEmails 10x (100 by 100)
		//------------------------------------------------------------
		DbRecordSet lstEmails;
		for (int i=0;i<10;i++)
		{
			if (m_Parent->IsStopCommandIssued())
				break;

			int nRetCnt;
			int nElapsed=ReadUserEmails(err,nRetCnt,lstEmails,0,100,-1,"","",&client);
			//lstEmails.Dump();
			if (err.IsOK())
			{
				if (nElapsed==0)
					nElapsed=1; //just to keep vars in ok cond
				STORE_TIME_SUCCESS("ReadUserEmails");
			}
			else
			{
				m_Parent->StoreError(err);
				QWriteLocker lck(&m_Parent->m_lck);
				m_Parent->m_ApiFailCnt["ReadUserEmails"]=m_Parent->m_ApiFailCnt.value("ReadUserEmails",0)+1;
			}
		}

		//------------------------------------------------------------
		//call getbinary email 20x 
		//------------------------------------------------------------
		if (lstEmails.getRowCount()>0)
		{
			//------------------------------------------------------------
			//call getbinary email 20x 
			//------------------------------------------------------------
			int nEnd=lstEmails.getRowCount()>20?20:lstEmails.getRowCount();

			for (int i=0;i<nEnd;i++)
			{
				if (m_Parent->IsStopCommandIssued())
					break;

				QByteArray data;
				int nElapsed=ReadEmailAsHtmlBinary(err,lstEmails.getDataRef(i,"BEM_ID").toInt(),data,&client);
				if (err.IsOK())
				{
					if (nElapsed==0)
						nElapsed=1; //just to keep vars in ok cond
					STORE_TIME_SUCCESS("ReadEmailAsHtmlBinary");
				}
				else
				{
					m_Parent->StoreError(err);
					QWriteLocker lck(&m_Parent->m_lck);
					m_Parent->m_ApiFailCnt["ReadEmailAsHtmlBinary"]=m_Parent->m_ApiFailCnt.value("ReadEmailAsHtmlBinary",0)+1;
				}
			}


			//------------------------------------------------------------
			//call getemail data + attachments 10x
			//------------------------------------------------------------

			for (int i=0;i<nEnd;i++)
			{
				if (m_Parent->IsStopCommandIssued())
					break;

				DbRecordSet Ret_Email;
				DbRecordSet Ret_Attachments;
				int nElapsed=ReadEmailDetails(err,lstEmails.getDataRef(i,"BEM_ID").toInt(),Ret_Email,Ret_Attachments,&client);
				if (err.IsOK())
				{
					if (nElapsed==0)
						nElapsed=1; //just to keep vars in ok cond
					STORE_TIME_SUCCESS("ReadEmailDetails");
				}
				else
				{
					m_Parent->StoreError(err);
					QWriteLocker lck(&m_Parent->m_lck);
					m_Parent->m_ApiFailCnt["ReadEmailDetails"]=m_Parent->m_ApiFailCnt.value("ReadEmailDetails",0)+1;
				}

				if (Ret_Attachments.getRowCount()>0)
				{
					int nSize=Ret_Attachments.getRowCount();
					for (int k=0;k<nSize;k++)
					{
						if (m_Parent->IsStopCommandIssued())
							break;

						QByteArray data;
						int nAttachmentID=Ret_Attachments.getDataRef(k,"BEA_ID").toInt();
						int nElapsed=GetAttachmentBinary(err,nAttachmentID,data,&client);
						if (err.IsOK())
						{
							if (nElapsed==0)
								nElapsed=1; //just to keep vars in ok cond
							STORE_TIME_SUCCESS("GetAttachmentBinary");
						}
						else
						{
							m_Parent->StoreError(err);
							QWriteLocker lck(&m_Parent->m_lck);
							m_Parent->m_ApiFailCnt["GetAttachmentBinary"]=m_Parent->m_ApiFailCnt.value("GetAttachmentBinary",0)+1;
						}
					}
				}
			}
		}

		client.RestEndSession(err,RestHTTPClient::REST_TYPE_COMMUNICATOR,strSession);

		//exit loop
		if (m_Parent->IsStopCommandIssued())
			break;

		ThreadSleeper::Sleep(1); //sleep 1 second

	} while (1==1);


}


int WorkThreadExecutorES::GetServerTime( Status &pStatus,QDateTime& Ret_datCurrentServerTime, QDateTime& Ret_datTimeNextTest, int& Ret_nActiveClientSocketCnt, RestHTTPClient *client )
{
	client->RestClearBuffers();
	int nElapsedMsec=0;

	QTime timer;
	timer.start();
	client->RestSend(pStatus,"GET","/rest/server/getservertime");
	nElapsedMsec=timer.elapsed();

	if(!pStatus.IsOK()) return nElapsedMsec;

	client->msg_recv.GetParameter(0,&Ret_datCurrentServerTime,pStatus);	
	if (!pStatus.IsOK())return nElapsedMsec;
	client->msg_recv.GetParameter(1,&Ret_datTimeNextTest,pStatus);	
	if (!pStatus.IsOK())return nElapsedMsec;
	client->msg_recv.GetParameter(2,&Ret_nActiveClientSocketCnt,pStatus);	
	if (!pStatus.IsOK())return nElapsedMsec;

	return nElapsedMsec;
}

int WorkThreadExecutorES::ReadUserEmails( Status &pStatus, int &Ret_nTotalCount, DbRecordSet &Ret_Emails, int nFromN/*=-1*/, int nToN/*=-1*/, int nSortOrder/*=-1*/, QString strEmailAcc/*=""*/,QString strEmailSender/*=""*/, RestHTTPClient *client )
{
	client->RestClearBuffers();
	int nElapsedMsec=0;

	//get login data:
	QString strSession;
	int nContactID,nPersonID;
	client->RestGetCurrentSessionData(strSession,nPersonID,nContactID);

	//client->msg_send.AddParameter(&nPersonID);
	client->msg_send.AddParameter(&nFromN);
	client->msg_send.AddParameter(&nToN);
	client->msg_send.AddParameter(&nSortOrder);
	client->msg_send.AddParameter(&strEmailAcc);

	QTime timer;
	timer.start();
	client->RestSend(pStatus,"POST","/rest/communication/user/"+QString::number(nPersonID)+"/emails",strSession);
	nElapsedMsec=timer.elapsed();
	if(!pStatus.IsOK()) return nElapsedMsec;

	client->msg_recv.GetParameter(0,&Ret_nTotalCount,pStatus);	
	if (!pStatus.IsOK())return nElapsedMsec;

	Ret_Emails.destroy();
	Ret_Emails.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_WEBSERVICE_BUS_EMAIL_SELECT));

	client->msg_recv.GetParameter(1,&Ret_Emails,pStatus);	
	if (!pStatus.IsOK())return nElapsedMsec;

	return nElapsedMsec;
}

int WorkThreadExecutorES::ReadEmailAsHtmlBinary(Status &pStatus, int nEmailID,QByteArray &Ret_byteData, RestHTTPClient *client )
{
	client->RestClearBuffers();
	int nElapsedMsec=0;

	QTime timer;
	timer.start();
	client->RestSend(pStatus,"GET","/rest/email/"+QString::number(nEmailID)+"/html/binary");
	nElapsedMsec=timer.elapsed();
	if(!pStatus.IsOK()) return nElapsedMsec;

	client->msg_recv.GetParameter(0,&Ret_byteData,pStatus);	
	if (!pStatus.IsOK())return nElapsedMsec;
	
	return nElapsedMsec;
}

int WorkThreadExecutorES::GetAttachmentBinary( Status &pStatus, int nAttachmentID, QByteArray &Ret_byteData, RestHTTPClient *client  )
{
	client->RestClearBuffers();
	int nElapsedMsec=0;

	QTime timer;
	timer.start();
	client->RestSend(pStatus,"GET","/rest/email/attachment/"+QString::number(nAttachmentID)+"/binary");
	nElapsedMsec=timer.elapsed();
	if(!pStatus.IsOK()) return nElapsedMsec;

	client->msg_recv.GetParameter(0,&Ret_byteData,pStatus);	
	if (!pStatus.IsOK())return nElapsedMsec;

	return nElapsedMsec;
}

int WorkThreadExecutorES::ReadEmailDetails( Status &pStatus, int nEmailID, DbRecordSet &Ret_Email, DbRecordSet &Ret_Attachments , RestHTTPClient *client)
{
	client->RestClearBuffers();
	int nElapsedMsec=0;

	QTime timer;
	timer.start();
	client->RestSend(pStatus,"GET","/rest/email/"+QString::number(nEmailID));
	nElapsedMsec=timer.elapsed();
	if(!pStatus.IsOK()) return nElapsedMsec;

	Ret_Email.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_WEBSERVICE_BUS_EMAIL_SELECT));
	client->msg_recv.GetParameter(0,&Ret_Email,pStatus);	
	if (!pStatus.IsOK())return nElapsedMsec;

	Ret_Attachments.defineFromView(DbSqlTableView::getView(DbSqlTableView::TVIEW_WEBSERVICE_BUS_EMAIL_ATTACHMENT_LIST));
	client->msg_recv.GetParameter(1,&Ret_Attachments,pStatus);	
	if (!pStatus.IsOK())return nElapsedMsec;
	
	return nElapsedMsec;
}



WorkThreadES::WorkThreadES( QObject *parent )
	:WorkThread(parent)
{
	m_pExecutor=NULL;
}

WorkThreadES::~WorkThreadES()
{
	if (m_pExecutor)
		m_pExecutor->deleteLater();
}

void WorkThreadES::run()
{
	m_pExecutor = new WorkThreadExecutorES(this);
	qDebug()<<"Worker in thread "<<ThreadIdentificator::GetCurrentThreadID()<<" is crunching server!!";
	QTimer::singleShot(0,m_pExecutor,SLOT(ExecuteTask()));
	exec();
}