#include "table_timings.h"
#include <QComboBox>
#include <QUrl>
#include <QFileDialog>
#include <QLabel>
#include <QListView>
#include <QTreeView>
#include <QMessageBox>
#include <QHeaderView>
#include <QProgressBar>
#include <QStyleFactory>

#define COL_CURRENT		3

Table_Timings::Table_Timings(QWidget * parent)
:UniversalTableWidgetEx(parent)
{
	//connect(this,SIGNAL(SignalDataChanged(int,int)),this,SLOT(OnCellChanged(int,int)));
}


Table_Timings::~Table_Timings()
{

}

void Table_Timings::DefineTimingList( DbRecordSet *plstData )
{
	plstData->destroy();
	plstData->addColumn(QVariant::String,"API");
	plstData->addColumn(QVariant::Int,"CNT_FAIL");
	plstData->addColumn(QVariant::Int,"CNT_SUCCESS");
	plstData->addColumn(QVariant::Int,"CNT_TOTAL");
	plstData->addColumn(QVariant::Int,"LATEST_RESPONSE");
	plstData->addColumn(QVariant::Int,"AVG_RESPONSE");
	plstData->addColumn(QVariant::Int,"MIN_RESPONSE");
	plstData->addColumn(QVariant::DateTime,"MIN_RESPONSE_TIME");
	plstData->addColumn(QVariant::Int,"MAX_RESPONSE");
	plstData->addColumn(QVariant::DateTime,"MAX_RESPONSE_TIME");
}


void Table_Timings::Initialize(DbRecordSet *plstData)
{
	//set data source for table, enable drop
	DbRecordSet columns;
	AddColumnToSetup(columns,"API",tr("API Name"),120,false,"" ,COL_TYPE_TEXT,"");
	AddColumnToSetup(columns,"CNT_TOTAL",tr("Total Calls"),90,false,"" ,COL_TYPE_TEXT,"");
	AddColumnToSetup(columns,"CNT_FAIL",tr("Failed Calls"),90,false,"" ,COL_TYPE_TEXT,"");
	AddColumnToSetup(columns,"",tr("Current"),100,false,"" ,COL_TYPE_CUSTOM_WIDGET,"");
	//AddColumnToSetup(columns,"LATEST_RESPONSE",tr("Curr."),60,false,"" ,COL_TYPE_TEXT,"");
	AddColumnToSetup(columns,"AVG_RESPONSE",tr("Avg."),60,false,"" ,COL_TYPE_TEXT,"");
	AddColumnToSetup(columns,"MIN_RESPONSE",tr("Min."),60,false,"" ,COL_TYPE_TEXT,"");
	AddColumnToSetup(columns,"MAX_RESPONSE",tr("Max."),70,false,"" ,COL_TYPE_TEXT,"");
	AddColumnToSetup(columns,"MIN_RESPONSE_TIME",tr("Min. Resp. Time"),100,false,"" ,COL_TYPE_TEXT,"HH:mm");
	AddColumnToSetup(columns,"MAX_RESPONSE_TIME",tr("Max. Resp. Time"),100,false,"" ,COL_TYPE_TEXT,"HH:mm");
	

	
	UniversalTableWidgetEx::Initialize(plstData,&columns);

	horizontalHeader()->setStretchLastSection(true);

	//CreateContextMenu();
	/*
	QList<QAction*> lstActions;
	QAction *SeparatorAct;

	QAction* pAction = new QAction(tr("UnPublish"), this);
	connect(pAction, SIGNAL(triggered()), this, SLOT(OnUnPublish()));
	lstActions.append(pAction);

	lstActions.append(pAction);
	SetContextMenuActions(lstActions);
	*/

	//connect(this,SIGNAL(cellDoubleClicked(int,int)),this,SLOT(OnCellDoubleClicked(int,int)));
}


void Table_Timings::Data2CustomWidget(int nRow, int nCol)
{
	if (nCol==COL_CURRENT)
	{
		QProgressBar *pProg = dynamic_cast<QProgressBar *>(cellWidget(nRow,nCol));
		if (!pProg)
		{
			pProg = new QProgressBar;
			pProg->setFormat("%vms");
			pProg->setTextVisible(true);
			pProg->setOrientation(Qt::Horizontal);
			setCellWidget(nRow,nCol,pProg);
			QStyle *style = QStyleFactory::create(QLatin1String("fusion"));
			pProg->setStyle(style);

		}
		pProg->setRange(0,m_plstData->getDataRef(nRow,"MAX_RESPONSE").toInt());
		pProg->setValue(m_plstData->getDataRef(nRow,"LATEST_RESPONSE").toInt());
	}
}
void Table_Timings::CustomWidget2Data(int nRow, int nCol)
{
	if (nCol==COL_CURRENT)
	{
	}
}


