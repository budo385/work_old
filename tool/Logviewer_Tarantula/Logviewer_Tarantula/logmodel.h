#include <QAbstractTableModel>
#include <QString>
#include <QList>
#include <QSet>

class LogData
{
public:
	LogData(){};
	LogData(const LogData &other){ operator=(other); };
	LogData& operator =(const LogData &other){ 
		strDate = other.strDate; 
		strClientID = other.strClientID; 
		strServerID = other.strServerID; 
		
		// Program Codes
		str_DIGIT_APP = other.str_DIGIT_APP;
		str_DIGIT_PLATFORM = other.str_DIGIT_PLATFORM;
		str_DIGIT_STATUS = other.str_DIGIT_STATUS;
		str_BIT_ADDON = other.str_BIT_ADDON;
		
		strClientPlatform = other.strClientPlatform; 
		strAppVersion = other.strAppVersion; 
		strClientNatStat = other.strClientNatStat; 
		strClientIP = other.strClientIP; 
		strServerIP = other.strServerIP; 
		strProtocols = other.strProtocols; 
		strCallResult = other.strCallResult; 

	return *this;
	};

	QString strDate;
	QString strClientID;
	QString strServerID;
	
	// PROGRAM_CODES
	QString str_DIGIT_APP;
	QString str_DIGIT_PLATFORM;	
	QString str_DIGIT_STATUS;
	QString str_BIT_ADDON;

	QString strClientPlatform;
	QString strAppVersion;
	QString strClientNatStat;
	QString strClientIP;
	QString strServerIP;
	QString strProtocols;
	QString strCallResult;

};

class MyModel : public QAbstractTableModel
{
	Q_OBJECT

public:
	MyModel(QObject *parent, QList<LogData> &lstLogData);

	int rowCount(const QModelIndex &parent = QModelIndex()) const ;
	int columnCount(const QModelIndex &parent = QModelIndex()) const;
	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
	QVariant headerData(int section, Qt::Orientation orientation, int role) const;
	void ForceRefresh();

	QSet<int> m_filterSet;
	bool m_bUseFilter; 
	void filterList(QString strID);
	void RemoveFilter();
	int getSetSize();

	QString m_FilterID;
	

private:
	virtual void sort(int column, Qt::SortOrder order = Qt::AscendingOrder);

protected:
	QList<LogData> &m_lstLogData;
	
};
