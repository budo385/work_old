#ifndef OPENLOG_H
#define OPENLOG_H

#include <QDialog>
#include "ui_openlog.h"
#include <QString>

class OpenLogDlg : public QDialog
{
	Q_OBJECT

public:
	OpenLogDlg(QWidget *parent = 0);
	~OpenLogDlg();

	// return values
	QDateTime getDateFrom();
	QDateTime getDateTo();
	QString getFileName();
	bool getUseSettings();
	
private:
	Ui::OpenLogDlg ui;
	bool bUseSettings;
	QString strFileName;

private slots:
	void handleSettingsChange(int);
	void openLogFile();
	//void on_ButtonAccept_clicked();

};

#endif // OPENLOG_H
