#include "openlog.h"
#include<QDateTimeEdit>
#include<QDateTime>
#include<QCheckBox>
#include<QMessageBox>
#include<QFileDialog>
#include<QString>

OpenLogDlg::OpenLogDlg(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
	bUseSettings = false;

	// init dialog controls
	ui.dateTimeFrom->setDateTime(QDateTime::currentDateTime().addDays(-7));
	ui.dateTimeTo->setDateTime(QDateTime::currentDateTime());
	ui.groupBoxSettings->setEnabled(false);
	ui.checkBoxSettings->setChecked(false);
 
	// handlers
	connect(ui.checkBoxSettings, SIGNAL(stateChanged(int)), this, SLOT(handleSettingsChange(int)));
	connect(ui.ButtonAccept, SIGNAL(clicked()),this, SLOT(accept()));
    connect(ui.ButtonCancel, SIGNAL(clicked()),this, SLOT(reject()));
	connect(ui.ButtonOpenlog,SIGNAL(clicked()),this, SLOT(openLogFile()));
	
}
OpenLogDlg::~OpenLogDlg(){}

QDateTime OpenLogDlg::getDateFrom()
{
	return ui.dateTimeFrom->dateTime();
}
QDateTime OpenLogDlg::getDateTo()
{
	return ui.dateTimeTo->dateTime();
}
bool OpenLogDlg::getUseSettings()
{
	return bUseSettings;
}
QString OpenLogDlg::getFileName()
{
	return strFileName;
}
void OpenLogDlg::handleSettingsChange(int bState)
{
	//Qt::Unchecked	0
	//Qt::Checked	2
	if(bState == 0) {
		ui.groupBoxSettings->setEnabled(false);
		bUseSettings = !bUseSettings; //toggle
	}	
	if(bState == 2) {
		ui.groupBoxSettings->setEnabled(true);
		bUseSettings = !bUseSettings;
	}	
}
void OpenLogDlg::openLogFile()
{
	strFileName = QFileDialog::getOpenFileName(
		NULL,
		"Choose a file",
		QDir::currentPath(),
		"appserver log file (*.log)");

	if(!strFileName.isEmpty()) 
	{
		ui.lineEditOpenlog->setText(strFileName);
	}
}