#include "logviewer.h"
#include "openlog.h"
#include "filter.h"
#include "binarysearch.h"
#include <stdio.h>
#include <QFileInfo>
#include <QFileDialog>
#include <QInputDialog>
#include <QMessageBox>
#include <QStringList>
#include <QTextStream>
#include <QHeaderView>
#include <QDebug>

QDateTime startParse;
QDateTime endParse;
QDateTime startGui;
QDateTime endGui;

Logviewer::Logviewer(QWidget *parent, Qt::WFlags flags)  
: QMainWindow(parent, flags)
{
	ui.setupUi(this);
	
	m_model = new MyModel(this, m_lstLogData);
	ui.tableView->setModel(m_model);
	
	connect(ui.actionOpen_log, SIGNAL(triggered()), this, SLOT(OnMenu_OnOpen()));
	connect(ui.actionExit, SIGNAL(triggered()), this, SLOT(OnMenu_OnExit()));	
	connect(ui.actionExport, SIGNAL(triggered()), this, SLOT(OnMenu_OnExport()));
	connect(ui.action_Reload_log, SIGNAL(triggered()), this, SLOT(OnMenu_OnReload()));
	connect(ui.actionClear_log, SIGNAL(triggered()), this, SLOT(OnMenu_OnClear()));
	connect(ui.actionBy_ID, SIGNAL(triggered()), this, SLOT(OnMenu_OnFilter()));
}
Logviewer::~Logviewer() {}


bool Logviewer::ParseLog(QString strInputFile, qint64 nBeginParsePosition, qint64 nEndParsePosition)
{ 
	startParse = QDateTime::currentDateTime();
	ui.tableView->setModel(m_model);	
	int  nIdx = -1;						// line counter
	long int nPos;						// character pos
	char szBuffer[1024];				// line buffer	
	QString strLine;					// line in log
	QString strLineMsg;					// message to parse in line		
	
	m_lstLogData.clear();	
	ui.labelStatus->clear(); //setText("");

	// Open File
	FILE *pInFile = fopen(strInputFile.toLatin1().constData(), "rb");
	if(NULL == pInFile){
		QMessageBox::information(this, "Error", "Failed to open input file");
		return false;
	}

	// Verify log byte size
	QFileInfo fi(strInputFile);
	qint64 fileSize = fi.size();
	if(fileSize==0) {
		QMessageBox::information(this, "Warning", "This log is empty");
		ui.labelStatus->setText("");
		OnMenu_OnOpen();
	}

	/*qint64 nProgressMin = nBeginParsePosition;
	qint64 nProgressMax = nEndParsePosition;
	if(nBeginParsePosition){}
	if(nEndParsePosition){}*/


	// progress bar
	QProgressDialog progress("Loading log file..", "Abort", 0, fileSize, this); //fileSize
	progress.setWindowModality(Qt::WindowModal);
	
	// skip to pos (caused by bynary search)
	if(nBeginParsePosition > 0){
		int nS = fseek(	pInFile, nBeginParsePosition, SEEK_SET);
	}

	// read log and parse
	while(NULL != fgets(szBuffer, sizeof(szBuffer), pInFile))
	{
		nPos = ftell(pInFile);
		if(nEndParsePosition != -1 && nPos >= nEndParsePosition)
		{
			fclose(pInFile);
			return false;
		}

		nIdx++;
		szBuffer[sizeof(szBuffer)-1] = '\0';
		strLine = szBuffer;
		strLine = strLine.trimmed();
		
		if (progress.wasCanceled())
		{
			fclose(pInFile);
			return(false); 
		}

		// update progress bar once every 100
		if(nIdx % 100 == 0)
		{
			progress.setValue(nPos);
		}

		// parser
		nPos = strLine.indexOf("\t2000\t");
		if(nPos != -1) 
		{
			nPos = strLine.lastIndexOf("\t");	  
			strLineMsg = strLine.mid(nPos).trimmed();

			// parseMessage(strLineMsg);
			if( !parseMessage(strLineMsg) ) {
				// delete last row
				if ( !m_lstLogData.isEmpty() )
					m_lstLogData.removeLast();
			}
		}	
		
	} //end: while

	fclose(pInFile);
	progress.setValue(fileSize);

	endParse = QDateTime::currentDateTime();
	return(true);
}

void Logviewer::OnMenu_OnFilter()
{
	if (m_lstLogData.isEmpty())
	{
		QMessageBox::information(this, "Warning", "Required to read .log file first");
		OnMenu_OnOpen();
		return;
	}

	OpenFilterDlg fd;
	QString FilterByID;
	if(fd.exec() == QDialog::Accepted)
	{
		// retrieve ID
		FilterByID = fd.getFilterID();
		m_model->filterList(FilterByID);
	}
	ui.labelStatus->clear();
	int nRowSize = m_model->getSetSize();
	QString strTableRows = QString("\tRow Count: %1 ").arg(nRowSize);
	QString strStatusInfo = "File: "+m_lastLogFile+strTableRows;
	ui.labelStatus->setText(strStatusInfo);
}

void Logviewer::OnMenu_OnOpen()
{
	OpenLogDlg opl; 
	if (opl.exec()== QDialog::Accepted) 
	{
		m_lastLogFile = opl.getFileName();

		// Use interval settings (from & to)
		if(opl.getUseSettings())
		{
			if( !m_lastLogFile.isEmpty() )
			{	
				// get dates from open dialog
				m_SearchFrom = opl.getDateFrom();
				m_SearchTo = opl.getDateTo();

				// validate date range
				if(m_SearchFrom >= m_SearchTo){
					QMessageBox::information(this, "Warning", "\"Date from\" exceeds \"Date To\"");
					OnMenu_OnOpen();
				}

					qDebug() << "Start search" << QDateTime::currentDateTime();
				
				BinarySearch bin(m_lastLogFile,m_SearchFrom,m_SearchTo);

					qDebug() << "End search" << QDateTime::currentDateTime();

				int nBeginParsePosition = bin.getFromPosition();
				int nEndParsePosition = bin.getToPosition();

				// dates are identical "means that both from&to are out of file bound
				if(nBeginParsePosition == nEndParsePosition){
					QMessageBox::information(this, "Warning", "Selected dates are out of file bound");
					OnMenu_OnOpen();
				}

				if(ParseLog(m_lastLogFile, nBeginParsePosition, nEndParsePosition) == true){
					m_model->ForceRefresh();
					QString strParseTime = QString("\t -> Parse time: %1 sec").arg(startParse.secsTo(endParse));
					QString strGuiTime = QString(" | Gui build time: %1 sec").arg(startGui.secsTo(endGui));
					QString strTableRows = QString(" | %1 Table rows").arg(m_lstLogData.size());
					QString strStatusInfo = "File loaded: "+m_lastLogFile+strParseTime+strGuiTime+strTableRows;
					ui.labelStatus->setText(strStatusInfo);
					ui.tableView->horizontalHeader()->setResizeMode(QHeaderView::ResizeToContents);
				}
				else{
					ui.labelStatus->setText("");
				}
			}
		}
		else // parse entire log
		{	
			if( !m_lastLogFile.isEmpty() )
			{
				if(ParseLog(m_lastLogFile, -1, -1) == true)
				{
					m_model->ForceRefresh();
					QString strParseTime = QString("\t -> Parse time: %1 sec").arg(startParse.secsTo(endParse));
					QString strGuiTime = QString(" | Gui build time: %1 sec").arg(startGui.secsTo(endGui));
					QString strTableRows = QString(" | %1 Table rows").arg(m_lstLogData.size());
					QString strStatusInfo = "File loaded: "+m_lastLogFile+strParseTime+strGuiTime+strTableRows;
					ui.labelStatus->setText(strStatusInfo);

					ui.tableView->horizontalHeader()->setResizeMode(QHeaderView::ResizeToContents);
				}
				else
				{
					ui.labelStatus->setText("");
					//m_lstLogData.clear();
					//m_model->delete();
				}
			}
		}
	}	
}

// action: exit
void Logviewer::OnMenu_OnExit()
{
	switch( QMessageBox::information( this, "Exit",
		"Are you sure you want to quit?",
		"Yes", "No",
		0, 1 ) ) {
	case 0:
		QApplication::exit(0); 
		//a.connect(&a, SIGNAL(lastWindowClosed()), &a, SLOT(quit()));
		break;
	case 1:
		//QMessageBox::information(this, "Info", "Stayin");
		break;
	default:
		break;
	}
}

// action: csv export
void Logviewer::OnMenu_OnExport()
{
	if(m_lstLogData.isEmpty()){
		QMessageBox::information(this, "Warning", "No file is loaded for export");
		return;
	}
		
	// choose output file
	QString strOutputFile = QFileDialog::getSaveFileName(
		this, 
		tr("Save file"),
		"/",							
		tr("Delimited File (*.txt)")); //csv
	
	// open file for writing
	QFile f(strOutputFile);	
	f.open(QIODevice::WriteOnly | QIODevice::Text);	
	QTextStream out(&f);
	
	bool bAborted = false;
	bool bPrintHeaders = true;
	
	int MIN = 0;
	int MAX = m_lstLogData.size();
	QProgressDialog pro("Exporting", "Abort", MIN, MAX, this);
	pro.setWindowModality(Qt::WindowModal);

	for(int i=0; i<m_lstLogData.size(); i++)
 	{
 		if (pro.wasCanceled())
 		{
 			ui.labelStatus->setText("");
 			out.flush();
 			f.remove();	
 			bAborted = true;
 			break;
 		}	
		
		pro.setValue(i);

		// Column headers
		if(bPrintHeaders)
		{
			out << "Date" << "\t"				// 2011-08-30 10.19.15.520
				<< "Bridge ID" << "\t"			// qrdzz42118
				<< "App name" << "\t"			// everPICs, everDOCs
				<< "App type" << "\t"			// iPhone, Browser webapp, Mobile webapp
				<< "App Addon" << "\t"			// SSL, DOC,
				<< "App Status" << "\t"			// none, lite, pro
				<< "Client Platform" << "\t"	// iphone, windows, ..
				<< "Client IP" << "\t"			// 93.143.8.55
				<< "Server IP" << "\t"			// 93.143.8.55
				<< "Protocol" << "\t"			// http
				<< "Port forwarding" << "\t"	// upnp, natpmp,..
				<< "Call Result" << "\n";		// OK
		}
		
		// skip server messages - show only client messages
		if(m_lstLogData[i].str_DIGIT_PLATFORM != "Bridge")
		{
			out << m_lstLogData[i].strDate   << "\t"
				<< m_lstLogData[i].strServerID   << "\t"
				<< m_lstLogData[i].str_DIGIT_APP << "\t"
				<< m_lstLogData[i].str_DIGIT_PLATFORM << "\t"
				<< m_lstLogData[i].str_BIT_ADDON << "\t"
				<< m_lstLogData[i].str_DIGIT_STATUS << "\t"
				<< m_lstLogData[i].strClientPlatform << "\t"
				<< m_lstLogData[i].strClientIP << "\t" 
				<< m_lstLogData[i].strServerIP << "\t" 
				<< m_lstLogData[i].strProtocols << "\t" 
				<< m_lstLogData[i].strClientNatStat << "\t"
				<< m_lstLogData[i].strCallResult << "\n";
		}
		bPrintHeaders=false;
	}

	if(!bAborted) 
		pro.setValue(m_lstLogData.size());
	
	f.close();
}

void Logviewer::OnMenu_OnReload()
{
	if(ParseLog(m_lastLogFile, -1, -1)==true){
		
		m_model->ForceRefresh();
		
		// info
		QString strParseTime = QString("\t -> Parse time: %1 sec").arg(startParse.secsTo(endParse));
		QString strGuiTime = QString(" | Gui build time: %1 sec").arg(startGui.secsTo(endGui));
		QString strStatusInfo = "File loaded: "+m_lastLogFile+strParseTime+strGuiTime;
		ui.labelStatus->setText(strStatusInfo);
	}
	else{
		ui.labelStatus->setText("");
		m_lstLogData.clear();
	}
}

void Logviewer::OnMenu_OnClear(){
	
	ui.tableView->setModel(0);
	m_model->ForceRefresh();
	ui.labelStatus->setText("");
	//m_lstLogData.clear();
}


// Parse the Program Code
void Logviewer::getAppName(QString str_code, QString &str_DIGIT_APP, QString &str_DIGIT_PLATFORM, QString &str_DIGIT_STATUS, QString &str_BIT_ADDON)
{
	QString temp = str_code;

	if(str_code.size()== NULL)
	{
		str_DIGIT_APP = "";
		str_DIGIT_PLATFORM = "";
		str_DIGIT_STATUS = "";
		str_BIT_ADDON = "";
	}
	else if(str_code == "3")
	{
		// for old clients still sending PROGRAM_CODE = 3 -> this is: everPICs iphone (lucy)
		str_DIGIT_APP = "everPICs";
		str_DIGIT_PLATFORM = "iPhone";
		str_DIGIT_STATUS = "none";				
		str_BIT_ADDON = "unknown";	

	}
	else if(str_code == "4")
	{
		str_DIGIT_APP = "everPICs";
		str_DIGIT_PLATFORM = "Bridge";
		str_DIGIT_STATUS = "none";				
		str_BIT_ADDON = "\"   ,   \"";			
	}
	else if (str_code == "6")
	{
		str_DIGIT_APP = "everDOCs";
		str_DIGIT_PLATFORM = "Bridge";
		str_DIGIT_STATUS = "none";				
		str_BIT_ADDON = "\"   ,   \"";					
	}	
	else
	{
		str_DIGIT_APP = getDigitAppValue(str_code.left(2));
		str_DIGIT_PLATFORM = getDigitPlatformValue(str_code.mid(2, 2));
		str_DIGIT_STATUS = getDigitStatusValue(str_code.mid(4, 1));
		str_BIT_ADDON = getDigitAddonValue(str_code.right(3));
	}	
}

/*
// Application prefix: first two digits
// #define DIGIT_APP_EVERDOCS	"11"
// #define DIGIT_APP_EVERPICS	"10"
*/
QString Logviewer::getDigitAppValue(QString digit)
{
	if(digit == "10")
		return "everPICs";
	else if(digit == "11")
		return "everDOCs";
	else
		return digit;
}

/*
// Platform prefix: next two digits
// #define DIGIT_PLATFORM_IPHONE				"00"
// #define DIGIT_PLATFORM_ANDROID				"01"
// #define DIGIT_PLATFORM_BLACKBERRY			"02"
// #define DIGIT_PLATFORM_WINDOWS_CLIENT		"05"
// #define DIGIT_PLATFORM_MAC_CLIENT			"06"
// #define DIGIT_PLATFORM_LINUX_CLIENT			"07"
// #define DIGIT_PLATFORM_BROWSER_WEBAPP		"10"
// #define DIGIT_PLATFORM_MOBILE_IPHONE_WEBAPP	"11"
*/
QString Logviewer::getDigitPlatformValue(QString digit)
{
	if(digit == "00")
		return "iPhone";
	else if(digit == "01")
		return "Android";
	else if(digit == "02")
		return "Blackberry";
	else if(digit == "05")
		return "Windows";
	else if(digit == "06")
		return "Mac";
	else if(digit == "07")
		return "Linux";
	else if(digit == "10")
		return "Browser Webapp";
	else if(digit == "11")
		return "Mobile Webapp";
	else 
		return digit;
}

/*
// App status: lite, pro, none (next 1 digit)
// #define DIGIT_STATUS_NONE	"0"
// #define DIGIT_STATUS_LITE	"1"
// #define DIGIT_STATUS_PRO		"2"
*/

QString Logviewer::getDigitStatusValue(QString digit)
{
	if(digit == "0")
		return "none";
	else if(digit == "1")
		return "Lite";
	else if(digit == "2")
		return "Pro";
	else
		return digit;
}

/*
// Add ons (next 3 decimal digits: coded as binary, bit by bit and displayed as decimal, hehe)
// Here are represent decimal values that are to be added to the program code:
// e.g. if SSL=ON, then PROG_CODE += BIT_ADDON_SSL, etc....
// #define BIT_ADDON_SSL							"1"
// #define BIT_ADDON_DOCUMENT_PACKAGE				"2"
*/

QString Logviewer::getDigitAddonValue(QString digit)
{
	int nDecimal=digit.toInt();

	//test SSL bit:
	if (nDecimal & 1){
		// SSL=true
		return "\"SSL,   \"";
	} 
	else if (nDecimal & 2){
		// Document package =TRUE
		return "\"   ,DOC\"";
	}
	else if(nDecimal & 3){
		// SSL=true, Document package =TRUE
		return "\"SSL,DOC\"";
	}
	else
		return "\"   ,   \"";
}


QString Logviewer::getNatStats(QString code)
{
	if(code == "0")
		return "0";
	else if(code == "1")
		return "NATPMP";
	else if (code == "2")
		return "UPNP";
	else if (code == "3")
		return "MANUAL";
	else
	    return code;
}


QString Logviewer::getProtocols(QString code)
{
	if(code == "0")
		return "http";
	else if (code == "1")
		return "https";
	else 
		return code;
}

bool Logviewer::parseMessage(QString strMessage)
{
	//dumpLog();

	m_lstLogData.append(LogData());										// add one row (constructor of LogData class)
	LogData &newRecord = m_lstLogData[m_lstLogData.size()-1];			// reference to last imported row
	int  nCurrRowIdx = m_lstLogData.size();								// row index

	QStringList strLineMsgList = strMessage.split("|",QString::SkipEmptyParts);  // Split message into string list
	int nMsgListSize = strLineMsgList.size();

	// |Call=Unregister|ClientIP=46.126.214.9|ClientPort=34306|ClientID=marin00001|Date=2011-09-09 09.08.44.288|CallStatusMsg=OK|CallStatusCode=0

	// parser
	for(int i=0; i<nMsgListSize; ++i) 
	{
		QString strMsgIdentificator = strLineMsgList[i];
		QStringList lstMsg = strMsgIdentificator.split("=");
	
		 //Skip unregister calls 
		 if(lstMsg.startsWith("Call") && lstMsg.at(1)== "Unregister"){
			return false;
		 }

		 //Client IP
		 if(lstMsg.startsWith("ClientIP")){
			newRecord.strClientIP = lstMsg.at(1);
			continue;
		 }
		 		 
		 // Client Code (App Code: 4=james, 6=john, etc.. )
		 if(lstMsg.startsWith("ClientCode")){
			 
			 //newRecord.strAppCode = 
			 QString code = lstMsg.at(1); 
			 getAppName(code,newRecord.str_DIGIT_APP, newRecord.str_DIGIT_PLATFORM, newRecord.str_DIGIT_STATUS, newRecord.str_BIT_ADDON);
			 continue;
		 }

 		// Client Version (App Version: "1.0.9" means: Major.Minor.Revision)
 		if(lstMsg.startsWith("ClientVersion")){
 			newRecord.strAppVersion = lstMsg.at(1);
 			continue;
 		}

		// Server ID (Bridge ID)
		if(lstMsg.startsWith("ServerID")){
			newRecord.strServerID = lstMsg.at(1);
			continue;
		}
 
 		// Client NatStats
 		if(lstMsg.startsWith("ClientNatStats")){
 			QString code = lstMsg.at(1);
 			newRecord.strClientNatStat = getNatStats(code);
 			continue;
 		}

		// Date
		if(lstMsg.startsWith("Date")){
			newRecord.strDate = lstMsg.at(1);
			continue;
		}

		// Platform
		if(lstMsg.startsWith("ClientPlatform")){
			newRecord.strClientPlatform = lstMsg.at(1);
			continue;
		}

		// Client ID  (Unique Client ID)
		if(lstMsg.startsWith("ClientID")){
			newRecord.strClientID = lstMsg.at(1);
			continue;
		}

		// Server IP
		if(lstMsg.startsWith("ServerIP")){
			newRecord.strServerIP = lstMsg.at(1);
			continue;
		}
		
		// Protocols (http/https) - ServerIsSSL
		if(lstMsg.startsWith("ServerIsSSL")){
			QString code = lstMsg.at(1);
			newRecord.strProtocols = getProtocols(code);
			continue;
		}

		// CALL RESULT
		// save msg every time to temp
		QString strErrorMsg;
		if(lstMsg.startsWith("CallStatusMsg")){
			strErrorMsg = lstMsg.at(1); 
			continue;
		}
						
		// now find the code
		if(lstMsg.startsWith("CallStatusCode")){
			QString code = lstMsg.at(1);
			if(code!="0"){
				newRecord.strCallResult = strErrorMsg;
				continue;
			}
			else{
				newRecord.strCallResult = "OK";
				continue;
			}		
		}
				
	} //end: for

	return true;
}


// strClientPlatform
/*
// 		nPos = strMsgIdentificator.indexOf("ClientPlatform=Windows");
// 		if(nPos != -1 && nCurrRowIdx > 0) {
// 			nPos = strMsgIdentificator.indexOf("=");
// 			newRecord.strClientPlatform = strMsgIdentificator.mid(nPos+1).trimmed();
// 			QString a = newRecord.strClientPlatform;
// 			continue;
// 		}
*/

void Logviewer::dumpLog(void)
{
#ifdef _DEBUG
	int count = m_lstLogData.size();
	for (int i = 0; i<count; i++)
	{
		qDebug()<< "redak " << i << "platform " << m_lstLogData[i].strClientPlatform;
	}
#endif
}