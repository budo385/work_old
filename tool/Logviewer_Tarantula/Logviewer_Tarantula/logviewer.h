#ifndef LOGVIEWER_H
#define LOGVIEWER_H

#include <QtGui/QMainWindow>
#include <QProgressDialog>
#include <QDateTime>
#include "ui_logviewer.h"
#include "logmodel.h"

class Logviewer : public QMainWindow
{
	Q_OBJECT

public:
	Logviewer(QWidget *parent = 0, Qt::WFlags flags = 0);
	~Logviewer();
	QDateTime m_SearchFrom;
	QDateTime m_SearchTo;

private:
	Ui::LogviewerClass ui;
	bool ParseLog(QString strInputFile, qint64 nBeginParsePosition, qint64 nEndParsePosition);
	QList<LogData> m_lstLogData;
	MyModel *m_model;
	QString m_lastLogFile;

	void getAppName(QString str_code, QString &str_DIGIT_APP, QString &str_DIGIT_PLATFORM, QString &str_DIGIT_STATUS, QString &str_BIT_ADDON);
	QString getNatStats(QString code);
	QString getProtocols(QString code);
	QString getDigitAppValue(QString digit);
	QString getDigitPlatformValue(QString digit);
	QString getDigitStatusValue(QString digit);
	QString getDigitAddonValue(QString digit);
	bool parseMessage(QString strMessage);
	void dumpLog(void);

private slots:

	void OnMenu_OnOpen();
	void OnMenu_OnExit();
	void OnMenu_OnExport();
	void OnMenu_OnReload();
	void OnMenu_OnClear();
	void OnMenu_OnFilter();
	
};

#endif // LOGVIEWER_H

