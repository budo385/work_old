#include "binarysearch.h"
#include <QDebug>
#include <QIODevice>
#include <QMessageBox>
#include <QDateTime>


BinarySearch::BinarySearch(QString strFileName,QDateTime dtFrom_target,QDateTime dtTo_target)
{
	m_file.setFileName(strFileName);
	m_file.open(QIODevice::ReadOnly);
	qint64 nFileSize = m_file.size();
	
	// return positions (use public ret functions)
	m_RetFromPos=0;
	m_RetToPos=0;

	// 2 bin searches (from & to)
	qint64 nNewFromPos = Search(dtFrom_target, 0, nFileSize, true);
	if(nNewFromPos == -1){
		//nNewFromPos = 0;
	}

	qint64 nNewToPos = Search(dtTo_target, nNewFromPos, nFileSize, false);
	if(nNewToPos == -1){
		// nNewToPos = m_file.size(); //already done
	}
	
		qDebug() << "Search: from=" << showValueOnPos(nNewFromPos).toString();
		qDebug() << "Search: to=" << showValueOnPos(nNewToPos).toString();

	QDateTime dtTestFrom = showValueOnPos(nNewFromPos);
	QDateTime dtTestTo = showValueOnPos(nNewToPos);

	QString strTestFrom = dtTestFrom.toString();
	QString strTestTo = dtTestTo.toString();
	
	m_RetFromPos = nNewFromPos;
	m_RetToPos = nNewToPos;
	m_file.close();
}
BinarySearch::~BinarySearch(){}

// Binary Search: If no match then closest neighbour is found
qint64 BinarySearch::Search(QDateTime dtKey, qint64 nMinPos, qint64 nMaxPos, bool bLowerBound) 
{
	qint64 nMidPos;		
	qint64 nSpace;			
	qint64 nCount = 0;		
	QDateTime dtMid;
	QDateTime dtNext;
	QDateTime dtPrev;
	
   while (nMinPos <= nMaxPos) 
   {
	   nSpace = nMaxPos - nMinPos;
	   nCount++;
	   qDebug() << "Search: step=" << nCount;

	   nMidPos=(nMinPos+nMaxPos)/2;
	   m_file.seek(nMidPos);
 
	   // pos to middle
	   if(!nextLine()) {
		   
		   // EOF  detected
		   if(m_file.atEnd()){
				//thisLine();
		   }
		   
	   }

	   // assumption: file pointer is correct positioned at begin of line
	   dtMid = midDate();
	   dtNext = nextDate();
	   dtPrev = prevDate();
       	   
			qDebug() << "Search: key=" << dtKey.toString() << "Mid=" << dtMid.toString() << "Next=" << dtNext.toString() << "Prev=" << dtPrev.toString();

	   if(nSpace <= 10 || nCount >= 25){
			if( dtPrev <= dtKey && dtKey <= dtNext )
			{
				int bbb = 20;

				if(bLowerBound){
					if(prevLine()){
						getDuplicatesBefore(m_file.pos());
						return m_file.pos();	
					}	
				}
				else
				{
					int aaa = 20;

					if(nextLine()){
						qint64 temppos = m_file.pos();
						getDuplicatesAfter(m_file.pos());
						return m_file.pos();
					}
					else{
						// EOF detected: there is no nextLine()
						qint64 temppos = m_file.pos();
						qint64 temrpos = m_file.pos();
					}
				}
			}
	   }

	 if (dtKey < dtMid)	{
		  nMaxPos = nMidPos - 1; // search lower half	
	 }
	 else if (dtKey > dtMid) {
		  nMinPos = nMidPos + 1; // search upper half	
	 }
	 else {
		return nMidPos; // exact match :)	 		
	 }
  }
return -1; 
}

// Retrieve Mid Position Date
QDateTime BinarySearch::midDate()
{
	qint64 nCurrentPos = m_file.pos();
	char lineBuff[1024];

	qint64 lineLength = m_file.readLine(lineBuff, 24);
	QDateTime dt = QDateTime::fromString(QString(lineBuff),"yyyy-MM-dd HH.mm.ss.zzz");
	m_file.seek(nCurrentPos);
	return dt;
}

// retrieve next date
QDateTime BinarySearch::nextDate()
{
	qint64 nCurrentPos = m_file.pos();
	char lineBuff[1024];
	qint64 lineLength;
	QDateTime dt;

	lineLength=m_file.readLine(lineBuff,sizeof(lineBuff));
    lineLength=m_file.readLine(lineBuff, 24);	
    dt = QDateTime::fromString(QString(lineBuff),"yyyy-MM-dd HH.mm.ss.zzz");				
	m_file.seek(nCurrentPos);
	return dt;
}

// retrieve previous date
QDateTime BinarySearch::prevDate()   
{
	qint64 nCurrentPos = m_file.pos();
	qint64 nGotoPos= nCurrentPos - 1024;
	qint64 lineLength;

	char lineBuff[1024];
	int sizeBuff = sizeof(lineBuff);
	int nBytesToRead = sizeBuff;
	
	QDateTime dt;
	QByteArray baLine;
	QString strTemp;
	QString strPrevLine;

	if(nGotoPos <= 0){
		nGotoPos = 0; 
		nBytesToRead=nCurrentPos;
	}
	m_file.seek(nGotoPos); 
	baLine = m_file.read(nBytesToRead); 
	QString strPrev(baLine); 
	
	// calculate start & begin of the prev line
	int nEndLine = strPrev.lastIndexOf("\n");
	if(nEndLine >= 0){
		int nStartLine = strPrev.lastIndexOf("\n", -2);
		if(nStartLine >= 0){
		   strTemp = strPrev.mid(nStartLine,nEndLine);
		   strPrevLine = strTemp.left(24);
		   strPrevLine = strPrevLine.trimmed();
		}
		else{
			m_file.seek(0);
			baLine = m_file.read(24); 
			QString strPrev(baLine); 
			strPrevLine = strPrevLine.trimmed();
		}
	}
	else{
		// no previous date available
	}
  
	dt = QDateTime::fromString(strPrevLine,"yyyy-MM-dd HH.mm.ss.zzz");	
	if(dt.isValid()){
		m_file.seek(nCurrentPos);
		return dt;
	}
	else{
		m_file.seek(nCurrentPos);
		return dt;
	}
}

// pos file pointer to the next line
bool BinarySearch::nextLine()
{
	char lineBuff[1024];	
	if(m_file.atEnd())
		return false;
	if( m_file.readLine(lineBuff,sizeof(lineBuff)) >= 0)
		return true;
 
return false;
}

// pos file pointer to the previous line
bool BinarySearch::prevLine()
{
	qint64 nCurrentPos = m_file.pos();
	qint64 nGotoPos= nCurrentPos - 1024;

	char lineBuff[1024];
	int sizeBuff = sizeof(lineBuff);
	int nBytesToRead = sizeBuff;
	QByteArray baLine;
	
	if(nGotoPos <= 0){
		nGotoPos = 0; 
		nBytesToRead = nCurrentPos;
	}
	m_file.seek(nGotoPos);
	baLine = m_file.read(nBytesToRead);

	QString strPrev(baLine);
	int nEndLine = strPrev.lastIndexOf("\n");
	if(nEndLine >= 0){
		int nStartLine = strPrev.lastIndexOf("\n", -2);
		if(nStartLine >= 0){
			int nMyPosition = nGotoPos + nStartLine +1;
			m_file.seek(nMyPosition);
		}
		else{
			m_file.seek(0);
		}
	}
	else{
		m_file.seek(0);
	}
return true;
}

// show the DateTime value given nPos
QDateTime BinarySearch::showValueOnPos(qint64 nPos)
{
	m_file.seek(nPos);
	QDateTime dt;
	char lineBuff[1024];
	qint64 lineLength;

	lineLength = m_file.readLine(lineBuff, 24);
	dt = QDateTime::fromString(QString(lineBuff),"yyyy-MM-dd HH.mm.ss.zzz");
	m_file.seek(nPos);
	return dt;
}
	
// find identical dates before given nPos and position to the first one
void BinarySearch::getDuplicatesBefore(qint64 nPos)
{
	qint64 nNewPos;
	QDateTime dtCurrent = showValueOnPos(nPos);
	QDateTime dtBefore = prevDate();
	
	while(dtCurrent == dtBefore){
		if(prevLine()){
			dtBefore = prevDate();
			dtCurrent = showValueOnPos(m_file.pos());
		}
	}
}

// find identical dates after given nPos and position to the last one
void BinarySearch::getDuplicatesAfter(qint64 nPos)
{
	qint64 nNewPos;
	QDateTime dtCurrent = showValueOnPos(nPos);
	QDateTime dtAfter = nextDate();
	
	while(dtCurrent == dtAfter){
		if(nextLine()){
			dtAfter = nextDate();
			dtCurrent = showValueOnPos(m_file.pos());
		}
	}
}

// return new "from" pos
qint64 BinarySearch::getFromPosition(){
	return m_RetFromPos;
}

// return new "to" pos
qint64 BinarySearch::getToPosition(){
	return m_RetToPos;
}

// when nextLine() fails, EOF is detected, this kinda function looks for the last \n and pos the file pointer
void BinarySearch::thisLine()
{
	/*char lineBuff[1024];
	int nBytesToRead = sizeof(lineBuff);
	qint64 lineCurrent = m_file.pos();
	qint64 nGotoPos= lineCurrent - 1024;

	m_file.seek(nGotoPos);
	QByteArray baLine = m_file.read(nBytesToRead);
	QString strPrev(baLine);

	int nLastNewLine = strPrev.lastIndexOf("\n");
    int nMyPosition = nGotoPos + nLastNewLine +1;
	m_file.seek(nMyPosition);
	*/


	QDateTime tester3 = prevDate();
	QString tester4 = tester3.toString();
	int tester5 = 40;
}