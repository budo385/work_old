#include "logmodel.h"
#include <QAbstractItemModel>

// qSort comparator
class MyComparator 
{
public:
	int nColumn;
	bool bAsc;	

	bool operator ()(const LogData &a, const LogData &b)
	{
		bool bResult = true; 
		
		if(0 == nColumn)
		{
			int nCmpResult = a.strDate.compare(b.strDate);
			if(0 == nCmpResult)
			{
				//dates are equal, need to compare by some other fields too
				 nCmpResult = a.strClientIP.compare(b.strClientIP);
				 if(0 == nCmpResult)
				 {
					 //equal strClientIP too, compare by some other
					 nCmpResult = a.strServerIP.compare(b.strServerIP);
					 if(0 == nCmpResult)
					 {
						//strServerIP are equal, need to compare by some other fields too
						nCmpResult = a.strClientID.compare(b.strClientID);
						if(0 == nCmpResult)
						{
							//strClientID are equal, need to compare by some other fields too
							nCmpResult = a.strServerID.compare(b.strServerID);
							if(0 == nCmpResult)
							{
								//strServerID are equal, need to compare by some other fields too
								nCmpResult = a.str_DIGIT_APP.compare(b.str_DIGIT_APP);
								if(0 == nCmpResult)
								{
									//str_DIGIT_APP are equal, need to compare by some other fields too
									nCmpResult = a.str_DIGIT_PLATFORM.compare(b.str_DIGIT_PLATFORM);
									if(0 == nCmpResult)
									{
										//str_DIGIT_PLATFORM are equal, need to compare by some other fields too
										nCmpResult = a.str_DIGIT_STATUS.compare(b.str_DIGIT_STATUS);
										if(0 == nCmpResult)
										{
											//str_DIGIT_STATUS are equal, need to compare by some other fields too
											nCmpResult = a.str_BIT_ADDON.compare(b.str_BIT_ADDON);
											if(0 == nCmpResult)
											{
												//str_BIT_ADDON are equal, need to compare by some other fields too
												nCmpResult = a.strClientPlatform.compare(b.strClientPlatform);
												if(0 == nCmpResult)
												{
													//strClientPlatform are equal, need to compare by some other fields too
													nCmpResult = a.strAppVersion.compare(b.strAppVersion);
													if(0 == nCmpResult)
													{
														//strAppVersion are equal, need to compare by some other fields too
														nCmpResult = a.strClientNatStat.compare(b.strClientNatStat);
														if(0 == nCmpResult)
														{
															//strClientNatStat are equal, need to compare by some other fields too
															nCmpResult = a.strProtocols.compare(b.strProtocols);
															if(0 == nCmpResult)
															{
																//strProtocols are equal, need to compare by some other fields too
																nCmpResult = a.strCallResult.compare(b.strCallResult);
																if(0 == nCmpResult)
																{
																	bResult = true; //random anwser
																}
																else
																	bResult = (nCmpResult < 0);		//old comparison, non-equal strCallResult fields
															}
															else
																bResult = (nCmpResult < 0);		//old comparison, non-equal strProtocols fields
														}
														else
															bResult = (nCmpResult < 0);		//old comparison, non-equal strClientNatStat fields
													}
													else
														bResult = (nCmpResult < 0);		//old comparison, non-equal strAppVersion fields
												}
												else
													bResult = (nCmpResult < 0);		//old comparison, non-equal strClientPlatform fields
											}
											else
												bResult = (nCmpResult < 0);		//old comparison, non-equal str_BIT_ADDON fields
										}
										else
											bResult = (nCmpResult < 0);		//old comparison, non-equal str_DIGIT_STATUS fields
									}
									else
										bResult = (nCmpResult < 0);		//old comparison, non-equal str_DIGIT_PLATFORM fields
								}
								else
									bResult = (nCmpResult < 0);		//old comparison, non-equal str_DIGIT_APP fields
							}
							else
								bResult = (nCmpResult < 0);		//old comparison, non-equal strServerID fields
						}
						else
							 bResult = (nCmpResult < 0);	//old comparison, non-equal strClientID fields
					 }
					 else
						 bResult = (nCmpResult < 0);	//old comparison, non-equal strServerID fields
				 }
				 else
					bResult = (nCmpResult < 0);    //old comparison, non-equal strClientIP fields
			}
			else
				bResult = (nCmpResult < 0);		//old comparison, non-equal dates
		}
		else if(1 == nColumn) //str_DIGIT_APP
		{
			int nCmpResult = a.str_DIGIT_APP.compare(b.str_DIGIT_APP);	
			if(0 == nCmpResult)
			{
				//str_DIGIT_APP are equal, need to compare by some other fields too
				 nCmpResult = a.strClientIP.compare(b.strClientIP);
				 if(0 == nCmpResult)
				 {
					 //equal strClientIP too, compare by some other
					 nCmpResult = a.strServerIP.compare(b.strServerIP);
					 if(0 == nCmpResult)
					 {
						//strServerIP are equal, need to compare by some other fields too
						nCmpResult = a.strClientID.compare(b.strClientID);
						if(0 == nCmpResult)
						{
							//strClientID are equal, need to compare by some other fields too
							nCmpResult = a.strServerID.compare(b.strServerID);
							if(0 == nCmpResult)
							{
								//strServerID are equal, need to compare by some other fields too
								nCmpResult = a.strDate.compare(b.strDate);
								if(0 == nCmpResult)
								{
									//strDate are equal, need to compare by some other fields too
									nCmpResult = a.str_DIGIT_PLATFORM.compare(b.str_DIGIT_PLATFORM);
									if(0 == nCmpResult)
									{
										//str_DIGIT_PLATFORM are equal, need to compare by some other fields too
										nCmpResult = a.str_DIGIT_STATUS.compare(b.str_DIGIT_STATUS);
										if(0 == nCmpResult)
										{
											//str_DIGIT_STATUS are equal, need to compare by some other fields too
											nCmpResult = a.str_BIT_ADDON.compare(b.str_BIT_ADDON);
											if(0 == nCmpResult)
											{
												//str_BIT_ADDON are equal, need to compare by some other fields too
												nCmpResult = a.strClientPlatform.compare(b.strClientPlatform);
												if(0 == nCmpResult)
												{
													//strClientPlatform are equal, need to compare by some other fields too
													nCmpResult = a.strAppVersion.compare(b.strAppVersion);
													if(0 == nCmpResult)
													{
														//strAppVersion are equal, need to compare by some other fields too
														nCmpResult = a.strClientNatStat.compare(b.strClientNatStat);
														if(0 == nCmpResult)
														{
															//strClientNatStat are equal, need to compare by some other fields too
															nCmpResult = a.strProtocols.compare(b.strProtocols);
															if(0 == nCmpResult)
															{
																//strProtocols are equal, need to compare by some other fields too
																nCmpResult = a.strCallResult.compare(b.strCallResult);
																if(0 == nCmpResult)
																{
																	bResult = true;
																}
																else
																	bResult = (nCmpResult < 0);		//old comparison, non-equal strCallResult fields
															}
															else
																bResult = (nCmpResult < 0);		//old comparison, non-equal strProtocols fields
														}
														else
															bResult = (nCmpResult < 0);		//old comparison, non-equal strClientNatStat fields
													}
													else
														bResult = (nCmpResult < 0);		//old comparison, non-equal strAppVersion fields
												}
												else
													bResult = (nCmpResult < 0);		//old comparison, non-equal strClientPlatform fields
											}
											else
												bResult = (nCmpResult < 0);		//old comparison, non-equal str_BIT_ADDON fields
										}
										else
											bResult = (nCmpResult < 0);		//old comparison, non-equal str_DIGIT_STATUS fields
									}
									else
										bResult = (nCmpResult < 0);		//old comparison, non-equal str_DIGIT_PLATFORM fields
								}
								else
									bResult = (nCmpResult < 0);		//old comparison, non-equal strDate fields
							}
							else
								bResult = (nCmpResult < 0);		//old comparison, non-equal strServerID fields
						}
						else
							 bResult = (nCmpResult < 0);	//old comparison, non-equal strClientID fields
					 }
					 else
						 bResult = (nCmpResult < 0);	//old comparison, non-equal strServerID fields
				 }
				 else
					bResult = (nCmpResult < 0);    //old comparison, non-equal strClientIP fields
			}
			else
				bResult = (nCmpResult < 0);		//old comparison, non-equal str_DIGIT_APP
		}
		else if(2 == nColumn) //str_DIGIT_PLATFORM
		{
			int nCmpResult = a.str_DIGIT_PLATFORM.compare(b.str_DIGIT_PLATFORM);
			if(0 == nCmpResult)
			{
				//str_DIGIT_PLATFORM are equal, need to compare by some other fields too
				 nCmpResult = a.strClientIP.compare(b.strClientIP);
				 if(0 == nCmpResult)
				 {
					 //equal strClientIP too, compare by some other
					 nCmpResult = a.strServerIP.compare(b.strServerIP);
					 if(0 == nCmpResult)
					 {
						//strServerIP are equal, need to compare by some other fields too
						nCmpResult = a.strClientID.compare(b.strClientID);
						if(0 == nCmpResult)
						{
							//strClientID are equal, need to compare by some other fields too
							nCmpResult = a.strServerID.compare(b.strServerID);
							if(0 == nCmpResult)
							{
								//strServerID are equal, need to compare by some other fields too
								nCmpResult = a.str_DIGIT_APP.compare(b.str_DIGIT_APP);
								if(0 == nCmpResult)
								{
									//str_DIGIT_APP are equal, need to compare by some other fields too
									nCmpResult = a.strDate.compare(b.strDate);
									if(0 == nCmpResult)
									{
										//strDate are equal, need to compare by some other fields too
										nCmpResult = a.str_DIGIT_STATUS.compare(b.str_DIGIT_STATUS);
										if(0 == nCmpResult)
										{
											//str_DIGIT_STATUS are equal, need to compare by some other fields too
											nCmpResult = a.str_BIT_ADDON.compare(b.str_BIT_ADDON);
											if(0 == nCmpResult)
											{
												//str_BIT_ADDON are equal, need to compare by some other fields too
												nCmpResult = a.strClientPlatform.compare(b.strClientPlatform);
												if(0 == nCmpResult)
												{
													//strClientPlatform are equal, need to compare by some other fields too
													nCmpResult = a.strAppVersion.compare(b.strAppVersion);
													if(0 == nCmpResult)
													{
														//strAppVersion are equal, need to compare by some other fields too
														nCmpResult = a.strClientNatStat.compare(b.strClientNatStat);
														if(0 == nCmpResult)
														{
															//strClientNatStat are equal, need to compare by some other fields too
															nCmpResult = a.strProtocols.compare(b.strProtocols);
															if(0 == nCmpResult)
															{
																//strProtocols are equal, need to compare by some other fields too
																nCmpResult = a.strCallResult.compare(b.strCallResult);
																if(0 == nCmpResult)
																{
																	bResult = true;
																}
																else
																	bResult = (nCmpResult < 0);		//old comparison, non-equal strCallResult fields
															}
															else
																bResult = (nCmpResult < 0);		//old comparison, non-equal strProtocols fields
														}
														else
															bResult = (nCmpResult < 0);		//old comparison, non-equal strClientNatStat fields
													}
													else
														bResult = (nCmpResult < 0);		//old comparison, non-equal strAppVersion fields
												}
												else
													bResult = (nCmpResult < 0);		//old comparison, non-equal strClientPlatform fields
											}
											else
												bResult = (nCmpResult < 0);		//old comparison, non-equal str_BIT_ADDON fields
										}
										else
											bResult = (nCmpResult < 0);		//old comparison, non-equal str_DIGIT_STATUS fields
									}
									else
										bResult = (nCmpResult < 0);		//old comparison, non-equal strDate fields
								}
								else
									bResult = (nCmpResult < 0);		//old comparison, non-equal str_DIGIT_APP fields
							}
							else
								bResult = (nCmpResult < 0);		//old comparison, non-equal strServerID fields
						}
						else
							 bResult = (nCmpResult < 0);	//old comparison, non-equal strClientID fields
					 }
					 else
						 bResult = (nCmpResult < 0);	//old comparison, non-equal strServerID fields
				 }
				 else
					bResult = (nCmpResult < 0);    //old comparison, non-equal strClientIP fields
			}
			else
				bResult = (nCmpResult < 0);		//old comparison, non-equal str_DIGIT_PLATFORM
		}
		else if(3 == nColumn) //**str_BIT_ADDON
		{
			int nCmpResult = a.str_BIT_ADDON.compare(b.str_BIT_ADDON);
			if(0 == nCmpResult)
			{
				//str_BIT_ADDON are equal, need to compare by some other fields too
				 nCmpResult = a.strClientIP.compare(b.strClientIP);
				 if(0 == nCmpResult)
				 {
					 //equal strClientIP too, compare by some other
					 nCmpResult = a.strServerIP.compare(b.strServerIP);
					 if(0 == nCmpResult)
					 {
						//strServerIP are equal, need to compare by some other fields too
						nCmpResult = a.strClientID.compare(b.strClientID);
						if(0 == nCmpResult)
						{
							//strClientID are equal, need to compare by some other fields too
							nCmpResult = a.strServerID.compare(b.strServerID);
							if(0 == nCmpResult)
							{
								//strServerID are equal, need to compare by some other fields too
								nCmpResult = a.str_DIGIT_APP.compare(b.str_DIGIT_APP);
								if(0 == nCmpResult)
								{
									//str_DIGIT_APP are equal, need to compare by some other fields too
									nCmpResult = a.str_DIGIT_PLATFORM.compare(b.str_DIGIT_PLATFORM);
									if(0 == nCmpResult)
									{
										//str_DIGIT_PLATFORM are equal, need to compare by some other fields too
										nCmpResult = a.str_DIGIT_STATUS.compare(b.str_DIGIT_STATUS);
										if(0 == nCmpResult)
										{
											//str_DIGIT_STATUS are equal, need to compare by some other fields too
											nCmpResult = a.strDate.compare(b.strDate);
											if(0 == nCmpResult)
											{
												//strDate are equal, need to compare by some other fields too
												nCmpResult = a.strClientPlatform.compare(b.strClientPlatform);
												if(0 == nCmpResult)
												{
													//strClientPlatform are equal, need to compare by some other fields too
													nCmpResult = a.strAppVersion.compare(b.strAppVersion);
													if(0 == nCmpResult)
													{
														//strAppVersion are equal, need to compare by some other fields too
														nCmpResult = a.strClientNatStat.compare(b.strClientNatStat);
														if(0 == nCmpResult)
														{
															//strClientNatStat are equal, need to compare by some other fields too
															nCmpResult = a.strProtocols.compare(b.strProtocols);
															if(0 == nCmpResult)
															{
																//strProtocols are equal, need to compare by some other fields too
																nCmpResult = a.strCallResult.compare(b.strCallResult);
																if(0 == nCmpResult)
																{
																	bResult = true;
																}
																else
																	bResult = (nCmpResult < 0);		//old comparison, non-equal strCallResult fields
															}
															else
																bResult = (nCmpResult < 0);		//old comparison, non-equal strProtocols fields
														}
														else
															bResult = (nCmpResult < 0);		//old comparison, non-equal strClientNatStat fields
													}
													else
														bResult = (nCmpResult < 0);		//old comparison, non-equal strAppVersion fields
												}
												else
													bResult = (nCmpResult < 0);		//old comparison, non-equal strClientPlatform fields
											}
											else
												bResult = (nCmpResult < 0);		//old comparison, non-equal strDate fields
										}
										else
											bResult = (nCmpResult < 0);		//old comparison, non-equal str_DIGIT_STATUS fields
									}
									else
										bResult = (nCmpResult < 0);		//old comparison, non-equal str_DIGIT_PLATFORM fields
								}
								else
									bResult = (nCmpResult < 0);		//old comparison, non-equal str_DIGIT_APP fields
							}
							else
								bResult = (nCmpResult < 0);		//old comparison, non-equal strServerID fields
						}
						else
							 bResult = (nCmpResult < 0);	//old comparison, non-equal strClientID fields
					 }
					 else
						 bResult = (nCmpResult < 0);	//old comparison, non-equal strServerID fields
				 }
				 else
					bResult = (nCmpResult < 0);    //old comparison, non-equal strClientIP fields
			}
			else
				bResult = (nCmpResult < 0);		//old comparison, non-equal str_BIT_ADDON
		}
		else if(4 == nColumn) //**str_DIGIT_STATUS
		{
			int nCmpResult = a.str_DIGIT_STATUS.compare(b.str_DIGIT_STATUS);
			if(0 == nCmpResult)
			{
				//str_DIGIT_STATUS are equal, need to compare by some other fields too
				 nCmpResult = a.strClientIP.compare(b.strClientIP);
				 if(0 == nCmpResult)
				 {
					 //equal strClientIP too, compare by some other
					 nCmpResult = a.strServerIP.compare(b.strServerIP);
					 if(0 == nCmpResult)
					 {
						//strServerIP are equal, need to compare by some other fields too
						nCmpResult = a.strClientID.compare(b.strClientID);
						if(0 == nCmpResult)
						{
							//strClientID are equal, need to compare by some other fields too
							nCmpResult = a.strServerID.compare(b.strServerID);
							if(0 == nCmpResult)
							{
								//strServerID are equal, need to compare by some other fields too
								nCmpResult = a.str_DIGIT_APP.compare(b.str_DIGIT_APP);
								if(0 == nCmpResult)
								{
									//str_DIGIT_APP are equal, need to compare by some other fields too
									nCmpResult = a.str_DIGIT_PLATFORM.compare(b.str_DIGIT_PLATFORM);
									if(0 == nCmpResult)
									{
										//str_DIGIT_PLATFORM are equal, need to compare by some other fields too
										nCmpResult = a.strDate.compare(b.strDate);
										if(0 == nCmpResult)
										{
											//strDate are equal, need to compare by some other fields too
											nCmpResult = a.str_BIT_ADDON.compare(b.str_BIT_ADDON);
											if(0 == nCmpResult)
											{
												//str_BIT_ADDON are equal, need to compare by some other fields too
												nCmpResult = a.strClientPlatform.compare(b.strClientPlatform);
												if(0 == nCmpResult)
												{
													//strClientPlatform are equal, need to compare by some other fields too
													nCmpResult = a.strAppVersion.compare(b.strAppVersion);
													if(0 == nCmpResult)
													{
														//strAppVersion are equal, need to compare by some other fields too
														nCmpResult = a.strClientNatStat.compare(b.strClientNatStat);
														if(0 == nCmpResult)
														{
															//strClientNatStat are equal, need to compare by some other fields too
															nCmpResult = a.strProtocols.compare(b.strProtocols);
															if(0 == nCmpResult)
															{
																//strProtocols are equal, need to compare by some other fields too
																nCmpResult = a.strCallResult.compare(b.strCallResult);
																if(0 == nCmpResult)
																{
																	bResult = true;
																}
																else
																	bResult = (nCmpResult < 0);		//old comparison, non-equal strCallResult fields
															}
															else
																bResult = (nCmpResult < 0);		//old comparison, non-equal strProtocols fields
														}
														else
															bResult = (nCmpResult < 0);		//old comparison, non-equal strClientNatStat fields
													}
													else
														bResult = (nCmpResult < 0);		//old comparison, non-equal strAppVersion fields
												}
												else
													bResult = (nCmpResult < 0);		//old comparison, non-equal strClientPlatform fields
											}
											else
												bResult = (nCmpResult < 0);		//old comparison, non-equal str_BIT_ADDON fields
										}
										else
											bResult = (nCmpResult < 0);		//old comparison, non-equal strDate fields
									}
									else
										bResult = (nCmpResult < 0);		//old comparison, non-equal str_DIGIT_PLATFORM fields
								}
								else
									bResult = (nCmpResult < 0);		//old comparison, non-equal str_DIGIT_APP fields
							}
							else
								bResult = (nCmpResult < 0);		//old comparison, non-equal strServerID fields
						}
						else
							 bResult = (nCmpResult < 0);	//old comparison, non-equal strClientID fields
					 }
					 else
						 bResult = (nCmpResult < 0);	//old comparison, non-equal strServerID fields
				 }
				 else
					bResult = (nCmpResult < 0);    //old comparison, non-equal strClientIP fields
			}
			else
				bResult = (nCmpResult < 0);		//old comparison, non-equal str_DIGIT_STATUS
			
		}
		else if(5 == nColumn) //**strAppVersion
		{
			int nCmpResult = a.strAppVersion.compare(b.strAppVersion);
			if(0 == nCmpResult)
			{
				//strAppVersion are equal, need to compare by some other fields too
				 nCmpResult = a.strClientIP.compare(b.strClientIP);
				 if(0 == nCmpResult)
				 {
					 //equal strClientIP too, compare by some other
					 nCmpResult = a.strServerIP.compare(b.strServerIP);
					 if(0 == nCmpResult)
					 {
						//strServerIP are equal, need to compare by some other fields too
						nCmpResult = a.strClientID.compare(b.strClientID);
						if(0 == nCmpResult)
						{
							//strClientID are equal, need to compare by some other fields too
							nCmpResult = a.strServerID.compare(b.strServerID);
							if(0 == nCmpResult)
							{
								//strServerID are equal, need to compare by some other fields too
								nCmpResult = a.str_DIGIT_APP.compare(b.str_DIGIT_APP);
								if(0 == nCmpResult)
								{
									//str_DIGIT_APP are equal, need to compare by some other fields too
									nCmpResult = a.str_DIGIT_PLATFORM.compare(b.str_DIGIT_PLATFORM);
									if(0 == nCmpResult)
									{
										//str_DIGIT_PLATFORM are equal, need to compare by some other fields too
										nCmpResult = a.str_DIGIT_STATUS.compare(b.str_DIGIT_STATUS);
										if(0 == nCmpResult)
										{
											//str_DIGIT_STATUS are equal, need to compare by some other fields too
											nCmpResult = a.str_BIT_ADDON.compare(b.str_BIT_ADDON);
											if(0 == nCmpResult)
											{
												//str_BIT_ADDON are equal, need to compare by some other fields too
												nCmpResult = a.strClientPlatform.compare(b.strClientPlatform);
												if(0 == nCmpResult)
												{
													//strClientPlatform are equal, need to compare by some other fields too
													nCmpResult = a.strDate.compare(b.strDate);
													if(0 == nCmpResult)
													{
														//strDate are equal, need to compare by some other fields too
														nCmpResult = a.strClientNatStat.compare(b.strClientNatStat);
														if(0 == nCmpResult)
														{
															//strClientNatStat are equal, need to compare by some other fields too
															nCmpResult = a.strProtocols.compare(b.strProtocols);
															if(0 == nCmpResult)
															{
																//strProtocols are equal, need to compare by some other fields too
																nCmpResult = a.strCallResult.compare(b.strCallResult);
																if(0 == nCmpResult)
																{
																	bResult = true;
																}
																else
																	bResult = (nCmpResult < 0);		//old comparison, non-equal strCallResult fields
															}
															else
																bResult = (nCmpResult < 0);		//old comparison, non-equal strProtocols fields
														}
														else
															bResult = (nCmpResult < 0);		//old comparison, non-equal strClientNatStat fields
													}
													else
														bResult = (nCmpResult < 0);		//old comparison, non-equal strDate fields
												}
												else
													bResult = (nCmpResult < 0);		//old comparison, non-equal strClientPlatform fields
											}
											else
												bResult = (nCmpResult < 0);		//old comparison, non-equal str_BIT_ADDON fields
										}
										else
											bResult = (nCmpResult < 0);		//old comparison, non-equal str_DIGIT_STATUS fields
									}
									else
										bResult = (nCmpResult < 0);		//old comparison, non-equal str_DIGIT_PLATFORM fields
								}
								else
									bResult = (nCmpResult < 0);		//old comparison, non-equal str_DIGIT_APP fields
							}
							else
								bResult = (nCmpResult < 0);		//old comparison, non-equal strServerID fields
						}
						else
							 bResult = (nCmpResult < 0);	//old comparison, non-equal strClientID fields
					 }
					 else
						 bResult = (nCmpResult < 0);	//old comparison, non-equal strServerID fields
				 }
				 else
					bResult = (nCmpResult < 0);    //old comparison, non-equal strClientIP fields
			}
			else
				bResult = (nCmpResult < 0);		//old comparison, non-equal strAppVersion
		}
		else if(6 == nColumn) //strClientPlatform
		{
			int nCmpResult = a.strClientPlatform.compare(b.strClientPlatform);
			if(0 == nCmpResult)
			{
				//strClientPlatform are equal, need to compare by some other fields too
				 nCmpResult = a.strClientIP.compare(b.strClientIP);
				 if(0 == nCmpResult)
				 {
					 //equal strClientIP too, compare by some other
					 nCmpResult = a.strServerIP.compare(b.strServerIP);
					 if(0 == nCmpResult)
					 {
						//strServerIP are equal, need to compare by some other fields too
						nCmpResult = a.strClientID.compare(b.strClientID);
						if(0 == nCmpResult)
						{
							//strClientID are equal, need to compare by some other fields too
							nCmpResult = a.strServerID.compare(b.strServerID);
							if(0 == nCmpResult)
							{
								//strServerID are equal, need to compare by some other fields too
								nCmpResult = a.str_DIGIT_APP.compare(b.str_DIGIT_APP);
								if(0 == nCmpResult)
								{
									//str_DIGIT_APP are equal, need to compare by some other fields too
									nCmpResult = a.str_DIGIT_PLATFORM.compare(b.str_DIGIT_PLATFORM);
									if(0 == nCmpResult)
									{
										//str_DIGIT_PLATFORM are equal, need to compare by some other fields too
										nCmpResult = a.str_DIGIT_STATUS.compare(b.str_DIGIT_STATUS);
										if(0 == nCmpResult)
										{
											//str_DIGIT_STATUS are equal, need to compare by some other fields too
											nCmpResult = a.str_BIT_ADDON.compare(b.str_BIT_ADDON);
											if(0 == nCmpResult)
											{
												//str_BIT_ADDON are equal, need to compare by some other fields too
												nCmpResult = a.strDate.compare(b.strDate);
												if(0 == nCmpResult)
												{
													//strDate are equal, need to compare by some other fields too
													nCmpResult = a.strAppVersion.compare(b.strAppVersion);
													if(0 == nCmpResult)
													{
														//strAppVersion are equal, need to compare by some other fields too
														nCmpResult = a.strClientNatStat.compare(b.strClientNatStat);
														if(0 == nCmpResult)
														{
															//strClientNatStat are equal, need to compare by some other fields too
															nCmpResult = a.strProtocols.compare(b.strProtocols);
															if(0 == nCmpResult)
															{
																//strProtocols are equal, need to compare by some other fields too
																nCmpResult = a.strCallResult.compare(b.strCallResult);
																if(0 == nCmpResult)
																{
																	bResult = true;
																}
																else
																	bResult = (nCmpResult < 0);		//old comparison, non-equal strCallResult fields
															}
															else
																bResult = (nCmpResult < 0);		//old comparison, non-equal strProtocols fields
														}
														else
															bResult = (nCmpResult < 0);		//old comparison, non-equal strClientNatStat fields
													}
													else
														bResult = (nCmpResult < 0);		//old comparison, non-equal strAppVersion fields
												}
												else
													bResult = (nCmpResult < 0);		//old comparison, non-equal strDate fields
											}
											else
												bResult = (nCmpResult < 0);		//old comparison, non-equal str_BIT_ADDON fields
										}
										else
											bResult = (nCmpResult < 0);		//old comparison, non-equal str_DIGIT_STATUS fields
									}
									else
										bResult = (nCmpResult < 0);		//old comparison, non-equal str_DIGIT_PLATFORM fields
								}
								else
									bResult = (nCmpResult < 0);		//old comparison, non-equal str_DIGIT_APP fields
							}
							else
								bResult = (nCmpResult < 0);		//old comparison, non-equal strServerID fields
						}
						else
							 bResult = (nCmpResult < 0);	//old comparison, non-equal strClientID fields
					 }
					 else
						 bResult = (nCmpResult < 0);	//old comparison, non-equal strServerID fields
				 }
				 else
					bResult = (nCmpResult < 0);    //old comparison, non-equal strClientIP fields
			}
			else
				bResult = (nCmpResult < 0);		//old comparison, non-equal strClientPlatform
		}
		else if(7 == nColumn) //**strServerID
		{
			int nCmpResult = a.strServerID.compare(b.strServerID);
			if(0 == nCmpResult)
			{
				//strServerID are equal, need to compare by some other fields too
				 nCmpResult = a.strClientIP.compare(b.strClientIP);
				 if(0 == nCmpResult)
				 {
					 //equal strClientIP too, compare by some other
					 nCmpResult = a.strServerIP.compare(b.strServerIP);
					 if(0 == nCmpResult)
					 {
						//strServerIP are equal, need to compare by some other fields too
						nCmpResult = a.strClientID.compare(b.strClientID);
						if(0 == nCmpResult)
						{
							//strClientID are equal, need to compare by some other fields too
							nCmpResult = a.strDate.compare(b.strDate);
							if(0 == nCmpResult)
							{
								//strDate are equal, need to compare by some other fields too
								nCmpResult = a.str_DIGIT_APP.compare(b.str_DIGIT_APP);
								if(0 == nCmpResult)
								{
									//str_DIGIT_APP are equal, need to compare by some other fields too
									nCmpResult = a.str_DIGIT_PLATFORM.compare(b.str_DIGIT_PLATFORM);
									if(0 == nCmpResult)
									{
										//str_DIGIT_PLATFORM are equal, need to compare by some other fields too
										nCmpResult = a.str_DIGIT_STATUS.compare(b.str_DIGIT_STATUS);
										if(0 == nCmpResult)
										{
											//str_DIGIT_STATUS are equal, need to compare by some other fields too
											nCmpResult = a.str_BIT_ADDON.compare(b.str_BIT_ADDON);
											if(0 == nCmpResult)
											{
												//str_BIT_ADDON are equal, need to compare by some other fields too
												nCmpResult = a.strClientPlatform.compare(b.strClientPlatform);
												if(0 == nCmpResult)
												{
													//strClientPlatform are equal, need to compare by some other fields too
													nCmpResult = a.strAppVersion.compare(b.strAppVersion);
													if(0 == nCmpResult)
													{
														//strAppVersion are equal, need to compare by some other fields too
														nCmpResult = a.strClientNatStat.compare(b.strClientNatStat);
														if(0 == nCmpResult)
														{
															//strClientNatStat are equal, need to compare by some other fields too
															nCmpResult = a.strProtocols.compare(b.strProtocols);
															if(0 == nCmpResult)
															{
																//strProtocols are equal, need to compare by some other fields too
																nCmpResult = a.strCallResult.compare(b.strCallResult);
																if(0 == nCmpResult)
																{
																	bResult = true;
																}
																else
																	bResult = (nCmpResult < 0);		//old comparison, non-equal strCallResult fields
															}
															else
																bResult = (nCmpResult < 0);		//old comparison, non-equal strProtocols fields
														}
														else
															bResult = (nCmpResult < 0);		//old comparison, non-equal strClientNatStat fields
													}
													else
														bResult = (nCmpResult < 0);		//old comparison, non-equal strAppVersion fields
												}
												else
													bResult = (nCmpResult < 0);		//old comparison, non-equal strClientPlatform fields
											}
											else
												bResult = (nCmpResult < 0);		//old comparison, non-equal str_BIT_ADDON fields
										}
										else
											bResult = (nCmpResult < 0);		//old comparison, non-equal str_DIGIT_STATUS fields
									}
									else
										bResult = (nCmpResult < 0);		//old comparison, non-equal str_DIGIT_PLATFORM fields
								}
								else
									bResult = (nCmpResult < 0);		//old comparison, non-equal str_DIGIT_APP fields
							}
							else
								bResult = (nCmpResult < 0);		//old comparison, non-equal strDate fields
						}
						else
							 bResult = (nCmpResult < 0);	//old comparison, non-equal strClientID fields
					 }
					 else
						 bResult = (nCmpResult < 0);	//old comparison, non-equal strServerID fields
				 }
				 else
					bResult = (nCmpResult < 0);    //old comparison, non-equal strClientIP fields
			}
			else
				bResult = (nCmpResult < 0);		//old comparison, non-equal strServerID
		}
		else if(8 == nColumn)	//**strClientNatStat
		{
			int nCmpResult = a.strClientNatStat.compare(b.strClientNatStat);
			if(0 == nCmpResult)
			{
				//strClientNatStat are equal, need to compare by some other fields too
				 nCmpResult = a.strClientIP.compare(b.strClientIP);
				 if(0 == nCmpResult)
				 {
					 //equal strClientIP too, compare by some other
					 nCmpResult = a.strServerIP.compare(b.strServerIP);
					 if(0 == nCmpResult)
					 {
						//strServerIP are equal, need to compare by some other fields too
						nCmpResult = a.strClientID.compare(b.strClientID);
						if(0 == nCmpResult)
						{
							//strClientID are equal, need to compare by some other fields too
							nCmpResult = a.strServerID.compare(b.strServerID);
							if(0 == nCmpResult)
							{
								//strServerID are equal, need to compare by some other fields too
								nCmpResult = a.str_DIGIT_APP.compare(b.str_DIGIT_APP);
								if(0 == nCmpResult)
								{
									//str_DIGIT_APP are equal, need to compare by some other fields too
									nCmpResult = a.str_DIGIT_PLATFORM.compare(b.str_DIGIT_PLATFORM);
									if(0 == nCmpResult)
									{
										//str_DIGIT_PLATFORM are equal, need to compare by some other fields too
										nCmpResult = a.str_DIGIT_STATUS.compare(b.str_DIGIT_STATUS);
										if(0 == nCmpResult)
										{
											//str_DIGIT_STATUS are equal, need to compare by some other fields too
											nCmpResult = a.str_BIT_ADDON.compare(b.str_BIT_ADDON);
											if(0 == nCmpResult)
											{
												//str_BIT_ADDON are equal, need to compare by some other fields too
												nCmpResult = a.strClientPlatform.compare(b.strClientPlatform);
												if(0 == nCmpResult)
												{
													//strClientPlatform are equal, need to compare by some other fields too
													nCmpResult = a.strAppVersion.compare(b.strAppVersion);
													if(0 == nCmpResult)
													{
														//strAppVersion are equal, need to compare by some other fields too
														nCmpResult = a.strDate.compare(b.strDate);
														if(0 == nCmpResult)
														{
															//strDate are equal, need to compare by some other fields too
															nCmpResult = a.strProtocols.compare(b.strProtocols);
															if(0 == nCmpResult)
															{
																//strProtocols are equal, need to compare by some other fields too
																nCmpResult = a.strCallResult.compare(b.strCallResult);
																if(0 == nCmpResult)
																{
																	bResult = true;
																}
																else
																	bResult = (nCmpResult < 0);		//old comparison, non-equal strCallResult fields
															}
															else
																bResult = (nCmpResult < 0);		//old comparison, non-equal strProtocols fields
														}
														else
															bResult = (nCmpResult < 0);		//old comparison, non-equal strDate fields
													}
													else
														bResult = (nCmpResult < 0);		//old comparison, non-equal strAppVersion fields
												}
												else
													bResult = (nCmpResult < 0);		//old comparison, non-equal strClientPlatform fields
											}
											else
												bResult = (nCmpResult < 0);		//old comparison, non-equal str_BIT_ADDON fields
										}
										else
											bResult = (nCmpResult < 0);		//old comparison, non-equal str_DIGIT_STATUS fields
									}
									else
										bResult = (nCmpResult < 0);		//old comparison, non-equal str_DIGIT_PLATFORM fields
								}
								else
									bResult = (nCmpResult < 0);		//old comparison, non-equal str_DIGIT_APP fields
							}
							else
								bResult = (nCmpResult < 0);		//old comparison, non-equal strServerID fields
						}
						else
							 bResult = (nCmpResult < 0);	//old comparison, non-equal strClientID fields
					 }
					 else
						 bResult = (nCmpResult < 0);	//old comparison, non-equal strServerID fields
				 }
				 else
					bResult = (nCmpResult < 0);    //old comparison, non-equal strClientIP fields
			}
			else
				bResult = (nCmpResult < 0);		//old comparison, non-equal strClientNatStat
		}
		else if(9 == nColumn) //**strClientIP
		{
			int nCmpResult = a.strClientIP.compare(b.strClientIP);
			if(0 == nCmpResult)
			{
				//strClientIP are equal, need to compare by some other fields too
				 nCmpResult = a.strDate.compare(b.strDate);
				 if(0 == nCmpResult)
				 {
					 //equal strDate too, compare by some other
					 nCmpResult = a.strServerIP.compare(b.strServerIP);
					 if(0 == nCmpResult)
					 {
						//strServerIP are equal, need to compare by some other fields too
						nCmpResult = a.strClientID.compare(b.strClientID);
						if(0 == nCmpResult)
						{
							//strClientID are equal, need to compare by some other fields too
							nCmpResult = a.strServerID.compare(b.strServerID);
							if(0 == nCmpResult)
							{
								//strServerID are equal, need to compare by some other fields too
								nCmpResult = a.str_DIGIT_APP.compare(b.str_DIGIT_APP);
								if(0 == nCmpResult)
								{
									//str_DIGIT_APP are equal, need to compare by some other fields too
									nCmpResult = a.str_DIGIT_PLATFORM.compare(b.str_DIGIT_PLATFORM);
									if(0 == nCmpResult)
									{
										//str_DIGIT_PLATFORM are equal, need to compare by some other fields too
										nCmpResult = a.str_DIGIT_STATUS.compare(b.str_DIGIT_STATUS);
										if(0 == nCmpResult)
										{
											//str_DIGIT_STATUS are equal, need to compare by some other fields too
											nCmpResult = a.str_BIT_ADDON.compare(b.str_BIT_ADDON);
											if(0 == nCmpResult)
											{
												//str_BIT_ADDON are equal, need to compare by some other fields too
												nCmpResult = a.strClientPlatform.compare(b.strClientPlatform);
												if(0 == nCmpResult)
												{
													//strClientPlatform are equal, need to compare by some other fields too
													nCmpResult = a.strAppVersion.compare(b.strAppVersion);
													if(0 == nCmpResult)
													{
														//strAppVersion are equal, need to compare by some other fields too
														nCmpResult = a.strClientNatStat.compare(b.strClientNatStat);
														if(0 == nCmpResult)
														{
															//strClientNatStat are equal, need to compare by some other fields too
															nCmpResult = a.strProtocols.compare(b.strProtocols);
															if(0 == nCmpResult)
															{
																//strProtocols are equal, need to compare by some other fields too
																nCmpResult = a.strCallResult.compare(b.strCallResult);
																if(0 == nCmpResult)
																{
																	bResult = true;
																}
																else
																	bResult = (nCmpResult < 0);		//old comparison, non-equal strCallResult fields
															}
															else
																bResult = (nCmpResult < 0);		//old comparison, non-equal strProtocols fields
														}
														else
															bResult = (nCmpResult < 0);		//old comparison, non-equal strClientNatStat fields
													}
													else
														bResult = (nCmpResult < 0);		//old comparison, non-equal strAppVersion fields
												}
												else
													bResult = (nCmpResult < 0);		//old comparison, non-equal strClientPlatform fields
											}
											else
												bResult = (nCmpResult < 0);		//old comparison, non-equal str_BIT_ADDON fields
										}
										else
											bResult = (nCmpResult < 0);		//old comparison, non-equal str_DIGIT_STATUS fields
									}
									else
										bResult = (nCmpResult < 0);		//old comparison, non-equal str_DIGIT_PLATFORM fields
								}
								else
									bResult = (nCmpResult < 0);		//old comparison, non-equal str_DIGIT_APP fields
							}
							else
								bResult = (nCmpResult < 0);		//old comparison, non-equal strServerID fields
						}
						else
							 bResult = (nCmpResult < 0);	//old comparison, non-equal strClientID fields
					 }
					 else
						 bResult = (nCmpResult < 0);	//old comparison, non-equal strServerID fields
				 }
				 else
					bResult = (nCmpResult < 0);    //old comparison, non-equal strDate fields
			}
			else
				bResult = (nCmpResult < 0);		//old comparison, non-equal strClientIP
		}
		else if(10 == nColumn) //**strServerIP
		{
			int nCmpResult = a.strServerIP.compare(b.strServerIP);
			if(0 == nCmpResult)
			{
				//strServerIP are equal, need to compare by some other fields too
				 nCmpResult = a.strClientIP.compare(b.strClientIP);
				 if(0 == nCmpResult)
				 {
					 //equal strClientIP too, compare by some other
					 nCmpResult = a.strDate.compare(b.strDate);
					 if(0 == nCmpResult)
					 {
						//strDate are equal, need to compare by some other fields too
						nCmpResult = a.strClientID.compare(b.strClientID);
						if(0 == nCmpResult)
						{
							//strClientID are equal, need to compare by some other fields too
							nCmpResult = a.strServerID.compare(b.strServerID);
							if(0 == nCmpResult)
							{
								//strServerID are equal, need to compare by some other fields too
								nCmpResult = a.str_DIGIT_APP.compare(b.str_DIGIT_APP);
								if(0 == nCmpResult)
								{
									//str_DIGIT_APP are equal, need to compare by some other fields too
									nCmpResult = a.str_DIGIT_PLATFORM.compare(b.str_DIGIT_PLATFORM);
									if(0 == nCmpResult)
									{
										//str_DIGIT_PLATFORM are equal, need to compare by some other fields too
										nCmpResult = a.str_DIGIT_STATUS.compare(b.str_DIGIT_STATUS);
										if(0 == nCmpResult)
										{
											//str_DIGIT_STATUS are equal, need to compare by some other fields too
											nCmpResult = a.str_BIT_ADDON.compare(b.str_BIT_ADDON);
											if(0 == nCmpResult)
											{
												//str_BIT_ADDON are equal, need to compare by some other fields too
												nCmpResult = a.strClientPlatform.compare(b.strClientPlatform);
												if(0 == nCmpResult)
												{
													//strClientPlatform are equal, need to compare by some other fields too
													nCmpResult = a.strAppVersion.compare(b.strAppVersion);
													if(0 == nCmpResult)
													{
														//strAppVersion are equal, need to compare by some other fields too
														nCmpResult = a.strClientNatStat.compare(b.strClientNatStat);
														if(0 == nCmpResult)
														{
															//strClientNatStat are equal, need to compare by some other fields too
															nCmpResult = a.strProtocols.compare(b.strProtocols);
															if(0 == nCmpResult)
															{
																//strProtocols are equal, need to compare by some other fields too
																nCmpResult = a.strCallResult.compare(b.strCallResult);
																if(0 == nCmpResult)
																{
																	bResult = true;
																}
																else
																	bResult = (nCmpResult < 0);		//old comparison, non-equal strCallResult fields
															}
															else
																bResult = (nCmpResult < 0);		//old comparison, non-equal strProtocols fields
														}
														else
															bResult = (nCmpResult < 0);		//old comparison, non-equal strClientNatStat fields
													}
													else
														bResult = (nCmpResult < 0);		//old comparison, non-equal strAppVersion fields
												}
												else
													bResult = (nCmpResult < 0);		//old comparison, non-equal strClientPlatform fields
											}
											else
												bResult = (nCmpResult < 0);		//old comparison, non-equal str_BIT_ADDON fields
										}
										else
											bResult = (nCmpResult < 0);		//old comparison, non-equal str_DIGIT_STATUS fields
									}
									else
										bResult = (nCmpResult < 0);		//old comparison, non-equal str_DIGIT_PLATFORM fields
								}
								else
									bResult = (nCmpResult < 0);		//old comparison, non-equal str_DIGIT_APP fields
							}
							else
								bResult = (nCmpResult < 0);		//old comparison, non-equal strServerID fields
						}
						else
							 bResult = (nCmpResult < 0);	//old comparison, non-equal strClientID fields
					 }
					 else
						 bResult = (nCmpResult < 0);	//old comparison, non-equal strDate fields
				 }
				 else
					bResult = (nCmpResult < 0);    //old comparison, non-equal strClientIP fields
			}
			else
				bResult = (nCmpResult < 0);		//old comparison, non-equal strServerIP
		}
		else if(11 == nColumn) //**strProtocols
		{
			int nCmpResult = a.strProtocols.compare(b.strProtocols);
			if(0 == nCmpResult)
			{
				 //strProtocols are equal, need to compare by some other fields too
				 nCmpResult = a.strClientIP.compare(b.strClientIP);
				 if(0 == nCmpResult)
				 {
					 //equal strClientIP too, compare by some other
					 nCmpResult = a.strServerIP.compare(b.strServerIP);
					 if(0 == nCmpResult)
					 {
						//strServerIP are equal, need to compare by some other fields too
						nCmpResult = a.strClientID.compare(b.strClientID);
						if(0 == nCmpResult)
						{
							//strClientID are equal, need to compare by some other fields too
							nCmpResult = a.strServerID.compare(b.strServerID);
							if(0 == nCmpResult)
							{
								//strServerID are equal, need to compare by some other fields too
								nCmpResult = a.str_DIGIT_APP.compare(b.str_DIGIT_APP);
								if(0 == nCmpResult)
								{
									//str_DIGIT_APP are equal, need to compare by some other fields too
									nCmpResult = a.str_DIGIT_PLATFORM.compare(b.str_DIGIT_PLATFORM);
									if(0 == nCmpResult)
									{
										//str_DIGIT_PLATFORM are equal, need to compare by some other fields too
										nCmpResult = a.str_DIGIT_STATUS.compare(b.str_DIGIT_STATUS);
										if(0 == nCmpResult)
										{
											//str_DIGIT_STATUS are equal, need to compare by some other fields too
											nCmpResult = a.str_BIT_ADDON.compare(b.str_BIT_ADDON);
											if(0 == nCmpResult)
											{
												//str_BIT_ADDON are equal, need to compare by some other fields too
												nCmpResult = a.strClientPlatform.compare(b.strClientPlatform);
												if(0 == nCmpResult)
												{
													//strClientPlatform are equal, need to compare by some other fields too
													nCmpResult = a.strAppVersion.compare(b.strAppVersion);
													if(0 == nCmpResult)
													{
														//strAppVersion are equal, need to compare by some other fields too
														nCmpResult = a.strClientNatStat.compare(b.strClientNatStat);
														if(0 == nCmpResult)
														{
															//strClientNatStat are equal, need to compare by some other fields too
															nCmpResult = a.strDate.compare(b.strDate);
															if(0 == nCmpResult)
															{
																//strDate are equal, need to compare by some other fields too
																nCmpResult = a.strCallResult.compare(b.strCallResult);
																if(0 == nCmpResult)
																{
																	bResult = true;
																}
																else
																	bResult = (nCmpResult < 0);		//old comparison, non-equal strCallResult fields
															}
															else
																bResult = (nCmpResult < 0);		//old comparison, non-equal strDate fields
														}
														else
															bResult = (nCmpResult < 0);		//old comparison, non-equal strClientNatStat fields
													}
													else
														bResult = (nCmpResult < 0);		//old comparison, non-equal strAppVersion fields
												}
												else
													bResult = (nCmpResult < 0);		//old comparison, non-equal strClientPlatform fields
											}
											else
												bResult = (nCmpResult < 0);		//old comparison, non-equal str_BIT_ADDON fields
										}
										else
											bResult = (nCmpResult < 0);		//old comparison, non-equal str_DIGIT_STATUS fields
									}
									else
										bResult = (nCmpResult < 0);		//old comparison, non-equal str_DIGIT_PLATFORM fields
								}
								else
									bResult = (nCmpResult < 0);		//old comparison, non-equal str_DIGIT_APP fields
							}
							else
								bResult = (nCmpResult < 0);		//old comparison, non-equal strServerID fields
						}
						else
							 bResult = (nCmpResult < 0);	//old comparison, non-equal strClientID fields
					 }
					 else
						 bResult = (nCmpResult < 0);	//old comparison, non-equal strServerID fields
				 }
				 else
					bResult = (nCmpResult < 0);    //old comparison, non-equal strClientIP fields
			}
			else
				bResult = (nCmpResult < 0);		//old comparison, non-equal strProtocols
		}
		else if(12 == nColumn) //**strClientID
		{
			int nCmpResult = a.strClientID.compare(b.strClientID);
			if(0 == nCmpResult)
			{
				//strClientID are equal, need to compare by some other fields too
				 nCmpResult = a.strClientIP.compare(b.strClientIP);
				 if(0 == nCmpResult)
				 {
					 //equal strClientIP too, compare by some other
					 nCmpResult = a.strServerIP.compare(b.strServerIP);
					 if(0 == nCmpResult)
					 {
						//strServerIP are equal, need to compare by some other fields too
						nCmpResult = a.strDate.compare(b.strDate);
						if(0 == nCmpResult)
						{
							//strDate are equal, need to compare by some other fields too
							nCmpResult = a.strServerID.compare(b.strServerID);
							if(0 == nCmpResult)
							{
								//strServerID are equal, need to compare by some other fields too
								nCmpResult = a.str_DIGIT_APP.compare(b.str_DIGIT_APP);
								if(0 == nCmpResult)
								{
									//str_DIGIT_APP are equal, need to compare by some other fields too
									nCmpResult = a.str_DIGIT_PLATFORM.compare(b.str_DIGIT_PLATFORM);
									if(0 == nCmpResult)
									{
										//str_DIGIT_PLATFORM are equal, need to compare by some other fields too
										nCmpResult = a.str_DIGIT_STATUS.compare(b.str_DIGIT_STATUS);
										if(0 == nCmpResult)
										{
											//str_DIGIT_STATUS are equal, need to compare by some other fields too
											nCmpResult = a.str_BIT_ADDON.compare(b.str_BIT_ADDON);
											if(0 == nCmpResult)
											{
												//str_BIT_ADDON are equal, need to compare by some other fields too
												nCmpResult = a.strClientPlatform.compare(b.strClientPlatform);
												if(0 == nCmpResult)
												{
													//strClientPlatform are equal, need to compare by some other fields too
													nCmpResult = a.strAppVersion.compare(b.strAppVersion);
													if(0 == nCmpResult)
													{
														//strAppVersion are equal, need to compare by some other fields too
														nCmpResult = a.strClientNatStat.compare(b.strClientNatStat);
														if(0 == nCmpResult)
														{
															//strClientNatStat are equal, need to compare by some other fields too
															nCmpResult = a.strProtocols.compare(b.strProtocols);
															if(0 == nCmpResult)
															{
																//strProtocols are equal, need to compare by some other fields too
																nCmpResult = a.strCallResult.compare(b.strCallResult);
																if(0 == nCmpResult)
																{
																	bResult = true;
																}
																else
																	bResult = (nCmpResult < 0);		//old comparison, non-equal strCallResult fields
															}
															else
																bResult = (nCmpResult < 0);		//old comparison, non-equal strProtocols fields
														}
														else
															bResult = (nCmpResult < 0);		//old comparison, non-equal strClientNatStat fields
													}
													else
														bResult = (nCmpResult < 0);		//old comparison, non-equal strAppVersion fields
												}
												else
													bResult = (nCmpResult < 0);		//old comparison, non-equal strClientPlatform fields
											}
											else
												bResult = (nCmpResult < 0);		//old comparison, non-equal str_BIT_ADDON fields
										}
										else
											bResult = (nCmpResult < 0);		//old comparison, non-equal str_DIGIT_STATUS fields
									}
									else
										bResult = (nCmpResult < 0);		//old comparison, non-equal str_DIGIT_PLATFORM fields
								}
								else
									bResult = (nCmpResult < 0);		//old comparison, non-equal str_DIGIT_APP fields
							}
							else
								bResult = (nCmpResult < 0);		//old comparison, non-equal strServerID fields
						}
						else
							 bResult = (nCmpResult < 0);	//old comparison, non-equal strDate fields
					 }
					 else
						 bResult = (nCmpResult < 0);	//old comparison, non-equal strServerID fields
				 }
				 else
					bResult = (nCmpResult < 0);    //old comparison, non-equal strClientIP fields
			}
			else
				bResult = (nCmpResult < 0);		//old comparison, non-equal strClientID
		}
		else if(13 == nColumn) //**strCallResult
		{
			int nCmpResult = a.strCallResult.compare(b.strCallResult);
			if(0 == nCmpResult)
			{
				//strCallResult are equal, need to compare by some other fields too
				 nCmpResult = a.strClientIP.compare(b.strClientIP);
				 if(0 == nCmpResult)
				 {
					 //equal strClientIP too, compare by some other
					 nCmpResult = a.strServerIP.compare(b.strServerIP);
					 if(0 == nCmpResult)
					 {
						//strServerIP are equal, need to compare by some other fields too
						nCmpResult = a.strClientID.compare(b.strClientID);
						if(0 == nCmpResult)
						{
							//strClientID are equal, need to compare by some other fields too
							nCmpResult = a.strServerID.compare(b.strServerID);
							if(0 == nCmpResult)
							{
								//strServerID are equal, need to compare by some other fields too
								nCmpResult = a.str_DIGIT_APP.compare(b.str_DIGIT_APP);
								if(0 == nCmpResult)
								{
									//str_DIGIT_APP are equal, need to compare by some other fields too
									nCmpResult = a.str_DIGIT_PLATFORM.compare(b.str_DIGIT_PLATFORM);
									if(0 == nCmpResult)
									{
										//str_DIGIT_PLATFORM are equal, need to compare by some other fields too
										nCmpResult = a.str_DIGIT_STATUS.compare(b.str_DIGIT_STATUS);
										if(0 == nCmpResult)
										{
											//str_DIGIT_STATUS are equal, need to compare by some other fields too
											nCmpResult = a.str_BIT_ADDON.compare(b.str_BIT_ADDON);
											if(0 == nCmpResult)
											{
												//str_BIT_ADDON are equal, need to compare by some other fields too
												nCmpResult = a.strClientPlatform.compare(b.strClientPlatform);
												if(0 == nCmpResult)
												{
													//strClientPlatform are equal, need to compare by some other fields too
													nCmpResult = a.strAppVersion.compare(b.strAppVersion);
													if(0 == nCmpResult)
													{
														//strAppVersion are equal, need to compare by some other fields too
														nCmpResult = a.strClientNatStat.compare(b.strClientNatStat);
														if(0 == nCmpResult)
														{
															//strClientNatStat are equal, need to compare by some other fields too
															nCmpResult = a.strProtocols.compare(b.strProtocols);
															if(0 == nCmpResult)
															{
																//strProtocols are equal, need to compare by some other fields too
																nCmpResult = a.strDate.compare(b.strDate);
																if(0 == nCmpResult)
																{
																	bResult = true;
																}
																else
																	bResult = (nCmpResult < 0);		//old comparison, non-equal strDate fields
															}
															else
																bResult = (nCmpResult < 0);		//old comparison, non-equal strProtocols fields
														}
														else
															bResult = (nCmpResult < 0);		//old comparison, non-equal strClientNatStat fields
													}
													else
														bResult = (nCmpResult < 0);		//old comparison, non-equal strAppVersion fields
												}
												else
													bResult = (nCmpResult < 0);		//old comparison, non-equal strClientPlatform fields
											}
											else
												bResult = (nCmpResult < 0);		//old comparison, non-equal str_BIT_ADDON fields
										}
										else
											bResult = (nCmpResult < 0);		//old comparison, non-equal str_DIGIT_STATUS fields
									}
									else
										bResult = (nCmpResult < 0);		//old comparison, non-equal str_DIGIT_PLATFORM fields
								}
								else
									bResult = (nCmpResult < 0);		//old comparison, non-equal str_DIGIT_APP fields
							}
							else
								bResult = (nCmpResult < 0);		//old comparison, non-equal strServerID fields
						}
						else
							 bResult = (nCmpResult < 0);	//old comparison, non-equal strClientID fields
					 }
					 else
						 bResult = (nCmpResult < 0);	//old comparison, non-equal strServerID fields
				 }
				 else
					bResult = (nCmpResult < 0);    //old comparison, non-equal strClientIP fields
			}
			else
				bResult = (nCmpResult < 0);		//old comparison, non-equal strCallResult
		}
		else{}

		if(!bAsc){
			bResult = !bResult;
		}

	return bResult;
	}
};

MyModel::MyModel(QObject *parent, QList<LogData> &lstLogData)
:QAbstractTableModel(parent),m_lstLogData(lstLogData)
{
	m_bUseFilter = false;
}

// return row count
int MyModel::rowCount(const QModelIndex & /*parent*/) const
{
	if(m_lstLogData.isEmpty())
        return 0;

    int nRes = m_lstLogData.size(); 
	
	if(m_bUseFilter){
		nRes = m_filterSet.size();
	}
	return nRes;
}

// return column count
int MyModel::columnCount(const QModelIndex & /*parent*/) const
{
	if(m_lstLogData.isEmpty())
        return 0;

	return 14;
}

// return data 
QVariant MyModel::data(const QModelIndex &index, int role) const
{
	if(m_lstLogData.isEmpty())
        return QVariant();
    
	if (!index.isValid())
        return QVariant();

	if (role == Qt::DisplayRole)
	{
		int nRow = index.row();
		
		if(m_bUseFilter){
			QSet<int>::const_iterator It =	m_filterSet.constBegin();
			It += nRow;
			nRow = *It;
		}	

		LogData &object = m_lstLogData[nRow];
		switch(index.column())
		{
			case 0:
				return object.strDate;
			case 1:
				return object.str_DIGIT_APP;
			case 2:
				return object.str_DIGIT_PLATFORM;
			case 3:
				return object.str_BIT_ADDON;
			case 4:
				return object.str_DIGIT_STATUS;
			case 5:
				return object.strAppVersion;
			case 6:
				return object.strClientPlatform;
			case 7:
				return object.strServerID;
			case 8:
				return object.strClientNatStat;
			case 9:
				return object.strClientIP;
			case 10:
				return object.strServerIP;
			case 11:
				return object.strProtocols;
			case 12:
				return object.strClientID;
			case 13:
				return object.strCallResult;
		}
	}
	return QVariant();
}

// setup the headers
QVariant MyModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if (role == Qt::DisplayRole)
	{
		if (orientation == Qt::Horizontal) 
		{
			switch (section)
			{
				case 0:
					return QString("Date");
				case 1:
					return QString("App name");
				case 2:
					return QString("App type");
				case 3:
					return QString("App addon");
				case 4:
					return QString("App status");
				case 5:
					return QString("App version");
				case 6:
					return QString("Platform");
				case 7:
					return QString("Bridge ID");
				case 8:
					return QString("Port forward");
				case 9:
					return QString("Client IP");
				case 10:
					return QString("Server IP");
				case 11:
					return QString("Protocol");
				case 12:
					return QString("Client ID");
				case 13:
					return QString("Call Result");
			}
		}
	}
	if (role == Qt::ToolTipRole)
	{
		switch (section)
		{
			case 0:
				//return QString("");
			case 1:
				//return QString("");
			case 2:
				//return QString("");
			case 3:
				//return QString(" ");
			case 4:
				return QString("0|lite|pro");
			case 5:
				//return QString(" ");
			case 6:
				//return QString("");
			case 7:
				//return QString(" ");
			case 8:
				//return QString(" ");
			case 9:
				//return QString(" ");
			case 10:
				//return QString(" ");
			case 11:
				//return QString("");
			case 12:
				//return QString(" ");
			case 13:
				return QString(" ");
		}
	}
return QVariant();
}

void MyModel::ForceRefresh()
{
	emit layoutChanged();
}
void MyModel::sort(int column, Qt::SortOrder order)
{
	MyComparator cmp;
	
	cmp.nColumn = column;
	if(order == 1){
		cmp.bAsc = false;
	} 
	else{
		 cmp.bAsc = true;
	}
	qSort(m_lstLogData.begin(), m_lstLogData.end(), cmp);
	ForceRefresh();
}
void MyModel::filterList(QString strID)
{
	if(!strID.isEmpty())
	{
		m_filterSet.clear();
		m_FilterID = strID;
		int nSize = m_lstLogData.size(); 
		for (int i=0; i<nSize; i++) 
		{
			if( m_lstLogData[i].strServerID ==  strID  || m_lstLogData[i].strClientID ==  strID)
			{	
				m_filterSet.insert(i);
			}
		}
		m_bUseFilter = true;
		ForceRefresh();
	}
	else
		RemoveFilter();
}

void MyModel::RemoveFilter()
{
	m_bUseFilter = false;
	m_filterSet.clear();
	ForceRefresh();
}

int MyModel::getSetSize()
{
	int size = m_lstLogData.size();
	if(m_bUseFilter)
		size = m_filterSet.size();
	return size;
}