#ifndef BINARYSEARCH_H
#define BINARYSEARCH_H

#include <QString>
#include <QDateTime>
#include <QFile>

class BinarySearch
{

public:
	BinarySearch(QString strFileName,QDateTime dtFrom_target,QDateTime dtTo_target);
	~BinarySearch();

	// public getter functions
	qint64 getFromPosition();
	qint64 getToPosition();

private:	
	// member functions
	qint64 Search(QDateTime dtKey,qint64 nMinPos, qint64 nMaxPos, bool bLowerBound);
	QDateTime midDate();
	QDateTime nextDate();
	QDateTime prevDate();
	
	bool prevLine();
	bool nextLine();
	void thisLine();

	void getDuplicatesBefore(qint64 nPos);
	void getDuplicatesAfter(qint64 nPos);
	QDateTime showValueOnPos(qint64 nPos);
    
	// member variables
	QFile m_file;
	qint64 m_RetFromPos;
	qint64 m_RetToPos;
};

#endif // LOGVIEWER_H