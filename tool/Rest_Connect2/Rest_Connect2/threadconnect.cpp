#include "threadconnect.h"

ThreadConnect::ThreadConnect(QObject *parent)
	: QThread(parent)
{
	m_ThreadSync.ThreadSetForWait();	//prepare for wait
	start(); //start thread
	m_ThreadSync.ThreadWait();			//prepare for start
}

ThreadConnect::~ThreadConnect()
{
	m_ThreadSync.ThreadSetForWait();	//prepare for wait
	emit Destroy();
	m_ThreadSync.ThreadWait();			//prepare for start

	//stop thread gently:
	quit();
	wait();
}

void ThreadConnect::run()
{
	m_Rest_Connector = new Rest_Connect(&m_ThreadSync);

	connect(this,SIGNAL(SetConnection(QString,QString,QString)),m_Rest_Connector,SLOT(SetConnection(QString, QString, QString)));
	connect(this,SIGNAL(ExecuteCall(QString, QString)),m_Rest_Connector,SLOT(ExecuteCall(QString, QString)));
	connect(this,SIGNAL(Destroy()),m_Rest_Connector,SLOT(Destroy()));

	m_ThreadSync.ThreadRelease();
	exec();
}


void ThreadConnect::ExecuteCall(int &nErrorCode, QString &strErrText,QString strUserName, QString strPassword,QString strHost, QString strHttpMethod, QString strURLPath,const QByteArray &byteXMLRequest,QByteArray &byteXMLResponse)
{
	m_Rest_Connector->m_nLastErrorCode=0;
	m_Rest_Connector->m_nLastErrorText="";
	m_Rest_Connector->m_RequestBuffer=byteXMLRequest;
	
	m_ThreadSync.ThreadSetForWait();
	emit SetConnection(strUserName,strPassword,strHost);
	m_ThreadSync.ThreadWait();

	m_ThreadSync.ThreadSetForWait();
	emit ExecuteCall(strHttpMethod,strURLPath);
	m_ThreadSync.ThreadWait();

	nErrorCode=m_Rest_Connector->m_nLastErrorCode;
	strErrText=m_Rest_Connector->m_nLastErrorText;
	byteXMLResponse=m_Rest_Connector->m_RespBuffer;
}

