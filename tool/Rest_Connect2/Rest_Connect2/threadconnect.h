#ifndef THREADCONNECT_H
#define THREADCONNECT_H

#include <QThread>
#include "rest_connect_ext.h"
#include "threadsync.h"

class ThreadConnect : public QThread
{
	Q_OBJECT

public:
	ThreadConnect(QObject *parent=0);
	~ThreadConnect();
	
	void ExecuteCall(int &nErrorCode, QString &strErrText,QString strUserName, QString strPassword,QString strHost, QString strHttpMethod, QString strURLPath,const QByteArray &byteXMLRequest,QByteArray &byteXMLResponse);

signals:
	void Destroy();
	void SetConnection(QString,QString,QString);
	void ExecuteCall(QString, QString);

private:
	void run();

	Rest_Connect *m_Rest_Connector;
	ThreadSynchronizer m_ThreadSync;///< for syncing between mastr & slave thread
};

#endif // THREADCONNECT_H
