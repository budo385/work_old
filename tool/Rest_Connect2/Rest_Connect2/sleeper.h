#ifndef THREADSLEEPER_H 
#define THREADSLEEPER_H

#include <QMutex>
#include <QWaitCondition>

/*!
    \class ThreadSleeper
    \brief Provides cross platform sleep implementation
    \ingroup Thread

	Locks thread and puts in sleep for given time period.
	NOTE: Avoid use of this class, use QTimer, signal or other means, this is dirty!!

	Use:
	\code
	ThreadSleeper::Sleep(1);//1ms
	\endcode
*/
class ThreadSleeper 
{ 
public:

	/*!
	Thread is locked, and put in sleep
	\param miliseconds	- sleeps for given time

	Use static
	*/
	static void Sleep(unsigned long miliseconds)
	{
		QMutex mutex;
		mutex.lock();
		QWaitCondition holdthread;
		holdthread.wait(&mutex,miliseconds);
		mutex.unlock();
	}
};

#endif //THREADSLEEPER_H
