#include "interface.h"
#include "threadconnect.h"
#include "trans/trans/resthttpclient.h"
#include <windows.h>

/*
bool CloseDatabaseConnection(char *szErrorString)
{
	return false;
};


bool SetConnection()
{
	Rest.SetConnection(szUser,szPass,szHost);
	return true;
}
*/
bool ExecuteCall(int *pnErrorCode, char *szErrorString,const char *szUser,const char *szPass,const char *szHost,const char *szMethod,const char *szPath,const char *szRequest,char *szResponse, int *pnResponseBufferSize)
{

	/*
	//CONNECT:
	Status err;
	RestHTTPClient client;
	client.SetConnectionSettings("localhost",11,true);
	client.Connect(err);
	if (!err.IsOK())
		return false;

	//SEND:
	client.RestSend(err,"GET","/rest/service/settings/reload");
	if (!err.IsOK())return false;
	*/


	ThreadConnect rest;
	int nErrorCode=0;
	QString strError;
	QByteArray response;

	rest.ExecuteCall(nErrorCode,strError,szUser,szPass,szHost,szMethod,szPath,szRequest,response);

	::MessageBoxA(NULL, "aaa", "", MB_OK);
	strcpy(szErrorString,strError.toLocal8Bit().data());
	*pnErrorCode=nErrorCode;
	if (nErrorCode!=0)
		return false;
	else
	{
		int nSize=response.size();
		if (*pnResponseBufferSize<response.size())
		{
			nErrorCode=1;
			strError="Response Buffer overrun. Please increase size of buffer";
			return false;
		}
		memcpy(szResponse,response.data(),nSize);
		*pnResponseBufferSize=nSize;
		return true;
	}

}
/*
bool GetCallResult(int *pnErrorCode, char *szErrorString,char *szResponse,int nTimeOutMsec)
{
	
	int nErrorCode=0;
	QString strError;
	QByteArray response;
	Rest.GetCallResult(nErrorCode,strError,response,nTimeOutMsec);
	strcpy(szErrorString,strError.toLocal8Bit().data());
	memcpy(szResponse,response.data(),response.size());
	*pnErrorCode=nErrorCode;
	if (nErrorCode!=0)
		return false;
	else
		return true;
}
*/