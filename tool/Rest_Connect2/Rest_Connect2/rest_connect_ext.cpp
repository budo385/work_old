#include "rest_connect_ext.h"

#include <stdlib.h>
//#include <time.h>
#include "rijndael.h"
#include "sha2.h"
#include "sha256hash.h"
#include <QUrl>
#include <QDebug>
#include <QTimer>


Rest_Connect::Rest_Connect(ThreadSynchronizer *sync)
{
	m_nTimerID=-1;
	m_sync=sync;
	connect(&m_Http,SIGNAL(requestFinished ( int , bool  )),this,SLOT(ONrequestFinished ( int , bool  )));
	connect(&m_Http,SIGNAL(readyRead ( const QHttpResponseHeader &  )),this,SLOT(OnreadyRead ( const QHttpResponseHeader &  )));
	connect(&m_Http,SIGNAL(responseHeaderReceived ( const QHttpResponseHeader &  )),this,SLOT(OnresponseHeaderReceived ( const QHttpResponseHeader &  )));
	connect(&m_Http,SIGNAL(sslErrors ( const QList<QSslError> &  ) ),this,SLOT(OnsslErrors ( const QList<QSslError> & )));
}

Rest_Connect::~Rest_Connect()
{

}

void Rest_Connect::SetConnection(QString strUserName, QString strPassword,QString strHost)
{

	m_strUserName=strUserName;
	m_strPassword=strPassword;
	m_strHost=strHost;
	m_sync->ThreadRelease();
}

void Rest_Connect::ExecuteCall(QString strHttpMethod, QString strURLPath)
{

	m_nLastErrorCode=0;
	m_nLastErrorText="";
	m_strURL=m_strHost+strURLPath;

	if (strHttpMethod.toLower()=="get")
		m_bGetRequest=true;
	else
		m_bGetRequest=false;

	MakeGetRequest();
	m_nTimerID=startTimer(20000); //20 sekundi za connection establish
}

void Rest_Connect::Destroy()
{
	deleteLater();
	m_sync->ThreadRelease();
}

void Rest_Connect::MakeGetRequest(QString stAuthToken)
{
	QUrl url(m_strURL);
	int nPort=url.port();
	if (m_strURL.left(5).toLower()=="https")
		m_Http.setHost(url.host(),QHttp::ConnectionModeHttps,nPort);
	else
		m_Http.setHost(url.host(),QHttp::ConnectionModeHttp,nPort);


	//m_Http.setHost(url.host(),nPort);

	QHttpRequestHeader header_request;
	if (m_bGetRequest)
		header_request.setRequest("GET", url.path());
	else
		header_request.setRequest("POST", url.path());

	header_request.setValue("Host", url.host());

	if (!stAuthToken.isEmpty())
	{
		header_request.setValue("auth", stAuthToken);
	}

	nID=m_Http.request(header_request,m_RequestBuffer);

	//set request field (keep old):
	QString strHeaderValues=header_request.toString();
	strHeaderValues += "\r\n"; //header is separated from body by double \r\n
}

//token=base64(SHA256(username:SHA256(username:password):ServerNonce))
QString Rest_Connect::GenerateAuthToken(QString strUser, QString strPass, QString strServerNonce)
{
	Sha256Hash Hasher;
	QByteArray bytePassword=strUser.toAscii()+":"+strPass.toAscii();

	bytePassword=Hasher.GetHash(bytePassword).toHex(); //hasher:user+pass
	QByteArray byteAuthToken=strUser.toLatin1()+":"+bytePassword+":"+strServerNonce.toLatin1(); 
	QByteArray token=Hasher.GetHash(byteAuthToken);

	return QString(token.toHex()).toUtf8();
}


void Rest_Connect::ONrequestFinished ( int id, bool error )
{
	if (id=nID)
	{
		if (error)
		{
			if (m_Http.error()==QHttp::AuthenticationRequiredError)
			{
				QHttpResponseHeader	resp=m_Http.lastResponse();
				OnresponseHeaderReceived(resp);
				int nStatusCode=resp.statusCode();
				if (nStatusCode==401) //
				{
					//get server token:
					QString strWWWAuth=resp.value("WWW-Authenticate");
					QStringList lstTokens=strWWWAuth.split("\"",QString::SkipEmptyParts);
					if (lstTokens.size()>=2)
					{
						QString strServerNonce=lstTokens.at(1);
						strLastAuth=GenerateAuthToken(m_strUserName,m_strPassword,strServerNonce);
						strLastAuth="SokratesAuth user=\""+m_strUserName+"\" token=\""+strLastAuth+"\" nonce=\""+strServerNonce+"\"";
						//m_Http.clearPendingRequests();
						//m_Http.close();
						//QCoreApplication::processEvents();
						QTimer::singleShot(0,this,SLOT(GoExecute()));
						return;
					}
				}
			}
			m_nLastErrorCode=m_Http.error();
			m_nLastErrorText=m_Http.errorString();
			m_sync->ThreadRelease();
		}
		else
		{
			qDebug()<<m_Http.state();
			if(m_Http.state()==QHttp::Connected)
				m_sync->ThreadRelease();
		}
	}

}
void Rest_Connect::GoExecute()
{
	MakeGetRequest(strLastAuth);
}

void Rest_Connect::OnreadyRead ( const QHttpResponseHeader & resp )
{
	m_RespBuffer+=m_Http.readAll();
}


void Rest_Connect::OnresponseHeaderReceived ( const QHttpResponseHeader &  resp)
{
	QString strHeaderValues = resp.toString ();
	strHeaderValues += "\r\n"; //header is separated from body by double \r\n
}


// xml encodings (xml-encoded entities are preceded with '&')
static const char  AMP = '&';
static const QByteArray rawEntity[] = { "&",      "<",    ">",     "\'",     "\""};
static const QByteArray xmlEntity[] = { "&amp;", "&lt;", "&gt;",  "&apos;", "&quot;"};



/*!
Replace raw text with xml-encoded entities
\param xml	-external buffer with XMLRPC stream
\return binary stream (include copy op)
*/
QByteArray Rest_Connect::xmlDecode(const QByteArray& xml)
{

	// check if no AMP there is no need to decode
	int pos=xml.indexOf(AMP);
	if(pos<0) return xml;

	QByteArray byteDecoded=xml;

	// iterate through list backwards: its very important to pars &amp last
	for (int iEntity=4; iEntity>=0; iEntity--)
		byteDecoded.replace(xmlEntity[iEntity],rawEntity[iEntity]);

	return byteDecoded;
}



/*!
Replace xml-encoded back to raw text

\param raw		-external buffer with XMLRPC stream
\return string (include copy op)
*/

QString Rest_Connect::xmlEncode(const QString& raw)
{

	QString strEncoded=raw;

	// iterate through list forwad: its very important to encode &amp first
	if(strEncoded.indexOf("&")>=0)strEncoded.replace("&","&amp;");
	if(strEncoded.indexOf('<')>=0)strEncoded.replace("<","&lt;");
	if(strEncoded.indexOf(">")>=0)strEncoded.replace(">","&gt;");
	if(strEncoded.indexOf("\'")>=0)	strEncoded.replace("\'","&apos;");
	if(strEncoded.indexOf("\"")>=0)	strEncoded.replace("\"","&quot;");

	//	for (uint iEntity=2; iEntity<5; ++iEntity)
	//		strEncoded.replace(rawEntity[iEntity],xmlEntity[iEntity]);

	return strEncoded;
}

void Rest_Connect::OnsslErrors ( const QList<QSslError> & )
{
	m_Http.ignoreSslErrors();
}

void Rest_Connect::timerEvent(QTimerEvent *event)
{
	//out timer is here, then fire Time out error, save our asses!!!
	if(event->timerId()==m_nTimerID)
	{
		if(m_Http.state()==QHttp::Unconnected || m_Http.state()==QHttp::HostLookup)
		{
			m_nLastErrorCode=1;
			m_nLastErrorText="TimeOut";
			m_sync->ThreadRelease();
		}
		else
			killTimer(m_nTimerID);
	}
}