#ifndef REST_CONNECT_H
#define REST_CONNECT_H

#include <QString>
#include <QByteArray>
#include <QHttp>
#include <QSemaphore>
#include <QHttpResponseHeader>
#include <QTimerEvent>
#include "threadsync.h"

class Rest_Connect : public QObject
{
	Q_OBJECT

public:
	Rest_Connect(ThreadSynchronizer *sync);
	~Rest_Connect();

	QByteArray m_RespBuffer;
	QByteArray m_RequestBuffer;
	QString m_strHost;
	QString m_strUserName;
	QString m_strPassword;
	QString m_nLastErrorText;
	int m_nLastErrorCode;

public slots:
	void SetConnection(QString strUserName, QString strPassword,QString strHost);
	void ExecuteCall(QString strHttpMethod, QString strURLPath);
	void Destroy();
	//void GetCallResult(int &nErrorCode, QString &strErrText, QByteArray &byteXMLResponse, int nTimeOutMsec=-1);

private slots:
	void MakeGetRequest(QString stAuthToken="");
	void ONrequestFinished (  int id, bool error  );
	void OnreadyRead ( const QHttpResponseHeader & resp ); 
	void OnresponseHeaderReceived ( const QHttpResponseHeader &  );
	void GoExecute();
	void OnsslErrors ( const QList<QSslError> & );

private:
	QString GenerateAuthToken(QString strUser, QString strPass, QString strServerNonce);
	static QString xmlEncode(const QString& );
	static QByteArray xmlDecode(const QByteArray& );

protected:
	void timerEvent(QTimerEvent *event);
	int m_nTimerID;

	QString m_strURL;
	//QSemaphore *m_semaphore;
	QHttp m_Http;

	int nID;
	QString strLastAuth;
	bool m_bGetRequest;
	ThreadSynchronizer *m_sync;

};



/*
#include "rest_connect_global.h"

class REST_CONNECT_EXPORT Rest_Connect
{
public:
	Rest_Connect();
	~Rest_Connect();

private:
};
*/
#endif // REST_CONNECT_H
