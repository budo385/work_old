@ECHO OFF
REM 
REM Script to clean Atol project tree on windows (delete temporary files)
REM 	NOTE: requires some recent Windows (NT based)
REM

IF NOT "%OS%"=="Windows_NT" GOTO :End

rmdir /S /Q ..\Debug
rmdir /S /Q ..\Release
rmdir /S /Q ..\GeneratedFiles
rmdir /S /Q ..\Deleted
del /Q ..\*.ncb

rmdir /S /Q ..\VisioDDl2QtCode\Debug
rmdir /S /Q ..\VisioDDl2QtCode\Release
rmdir /S /Q ..\VisioDDl2QtCode\GeneratedFiles
rmdir /S /Q ..\VisioDDl2QtCode\Deleted

del /Q ..\VisioDDl2QtCode\*.user
del /Q ..\VisioDDl2QtCode\*.aps
del /F /Q /A H ..\*.suo


:End
