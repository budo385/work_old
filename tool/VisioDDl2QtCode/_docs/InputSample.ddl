-- Create new table BUS_PERSON.
-- BUS_PERSON : Person table.
-- 	Roles are assigned to persons. Persons are more like titles, not like individuals.
-- 	Connects user with his role.
-- 	BPER_FIRST_NAME : First name of person.\\\\\
-- 	BPER_LAST_NAME : Last name of person.\\\\\
-- 	BPER_USER_ID : FK -> CORE_USER\\\\\
-- 	BPER_ACTIVE_FLAG : Is person active 0-inactive, 1-active\\\\\
-- 	BPER_SEX : 0-Male.1-Female\Gender\Gender1\0\\
-- 	BPER_BIRTH_DATE : Birth Date\Birth Date\Birth Date1\\\
-- 	BPER_DESCRIPTION : Notes\Notes\Notes1\\\
-- 	BPER_DEPARTMENT_ID : FK -> BUS_DEPARTMENT\\\\\
-- 	BPER_COMPANY_ID : FK -> BUS_COMPANY\\\\\
-- 	BPER_DIRECT_PHONE : Direct Phone\Direct Phone\Direct Phone1\\\
-- 	BPER_INSURANCE_ID : Insurance ID\\\\\
-- 	BPER_MAIN_FUNCTION_ID : FK -> BUS_FUNCTION\\\\\
-- 	BPER_OCCUPATION : Occupation % (TIMELINE)\\\\\
-- 	BPER_DATE_ENTERED : Entered\\\\\
-- 	BPER_DATE_LEFT : Left\\\\\  
create table BUS_PERSON ( 
	BPER_ID INTEGER not null,
	BPER_GLOBAL_ID VARCHAR(15) null,
	BPER_DAT_LAST_MODIFIED TIMESTAMP not null,
	BPER_FIRST_NAME VARCHAR(50) not null,
	BPER_LAST_NAME VARCHAR(50) null,
	BPER_USER_ID INTEGER null,
	BPER_ACTIVE_FLAG INTEGER not null,
	BPER_CODE VARCHAR(12) not null,
	BPER_CONTACT_ID INTEGER null,
	BPER_SEX SMALLINT not null,
	BPER_BIRTH_DATE DATE null,
	BPER_DESCRIPTION LONGVARCHAR null,
	BPER_INITIALS VARCHAR(8) null,
	BPER_DEPARTMENT_ID INTEGER null,
	BPER_COMPANY_ID INTEGER null,
	BPER_DIRECT_PHONE VARCHAR(30) null,
	BPER_INSURANCE_ID VARCHAR(30) null,
	BPER_MAIN_FUNCTION_ID INTEGER null,
	BPER_OCCUPATION DECIMAL(10,2) null,
	BPER_DATE_ENTERED DATETIME null,
	BPER_DATE_LEFT DATETIME null,
	BPER_PICTURE BINARY(10) null, constraint BUS_PERSON_PK primary key (BPER_ID) )  