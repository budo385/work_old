#include <QtGui/QApplication>
#include "visioddl2qtcode.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    VisioDDl2QtCode w;
    w.show();
    a.connect(&a, SIGNAL(lastWindowClosed()), &a, SLOT(quit()));
    return a.exec();
}
