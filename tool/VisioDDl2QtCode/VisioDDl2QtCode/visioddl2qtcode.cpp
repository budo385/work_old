#include "visioddl2qtcode.h"
#include <QFileDialog>
#include <QMessageBox>

VisioDDl2QtCode::VisioDDl2QtCode(QWidget *parent, Qt::WFlags flags)
    : QMainWindow(parent, flags)
{
	ui.setupUi(this);

	// map supported types from SQL to Variant types
	m_lstTypeMap["INTEGER"]	 = "QVariant::Int";
	m_lstTypeMap["SMALLINT"] = "QVariant::Int";
	m_lstTypeMap["BIT"]		 = "QVariant::Int";
	m_lstTypeMap["DECIMAL"]	 = "QVariant::Double";
	m_lstTypeMap["NUMERIC"]	 = "QVariant::Double";
	m_lstTypeMap["FLOAT"]	 = "QVariant::Double";
	m_lstTypeMap["REAL"]	 = "QVariant::Double";
	m_lstTypeMap["DATETIME"] = "QVariant::DateTime";
	m_lstTypeMap["DATE"]	 = "QVariant::Date";
	m_lstTypeMap["TIME"]	 = "QVariant::Time";
	m_lstTypeMap["TIMESTAMP"]= "QVariant::DateTime";
	m_lstTypeMap["LONGVARBINARY"] = "QVariant::ByteArray";
	m_lstTypeMap["BINARY"] = "QVariant::ByteArray";
	m_lstTypeMap["VARCHAR"] = "QVariant::String";
	m_lstTypeMap["CHAR"] = "QVariant::String";
	m_lstTypeMap["LONGVARCHAR"] = "QVariant::String, true";
}

VisioDDl2QtCode::~VisioDDl2QtCode()
{
}

void VisioDDl2QtCode::on_btnPickSrc_clicked()
{
	QString	strFile = QFileDialog::getOpenFileName(
                    NULL,
                    "Choose a file",
                    "/",
                    "Visio DDL file (*.ddl)");

	if(!strFile.isEmpty())
		ui.editSrcFile->setText(strFile);
}

void VisioDDl2QtCode::on_btnPickDst_clicked()
{
	QString	strFile = QFileDialog::getOpenFileName(
                    NULL,
                    "Choose a file",
                    "/",
                    "Text file (*.txt)");

	if(!strFile.isEmpty())
		ui.editDstFile->setText(strFile);
}

void VisioDDl2QtCode::on_btnGenerate_clicked()
{
	//clear internal data
	m_lstFields.clear();
	m_strTableName = "";

	//
	// STEP 1: check input data
	//
	QString strInputFile  = ui.editSrcFile->text();
	QString strOutputFile = ui.editDstFile->text();
	if(strInputFile.isEmpty() || strOutputFile.isEmpty()){
		QMessageBox::information(this, "Warning", "Please define input/output parameters!");
		return;
	}

	//
	// STEP 2: parse input file
	//
	FILE *pInFile = fopen(strInputFile.toLatin1().constData(), "r");
	if(NULL == pInFile){
		QMessageBox::information(this, "Error", "Failed to open input file!");
		return;
	}

	//load input as a set of lines
	QList<QString> lstInput;
	char szBuffer[1024];
	while(NULL != fgets(szBuffer, sizeof(szBuffer), pInFile))
		lstInput.append(QString(szBuffer));
	fclose(pInFile);

	//filter out "junk" lines
	int nIdx = 0;
	while(nIdx < lstInput.size())
	{
		//trim lines
		lstInput[nIdx] = lstInput[nIdx].trimmed();

		//remove some unneeded lines
		if(	lstInput[nIdx].size() < 1 ||	//empty line
			(lstInput[nIdx].indexOf("--")==0 && lstInput[nIdx].indexOf("\\")<0) ) // comment line with no "\"
		{
			lstInput.removeAt(nIdx);
		}
		else
			nIdx ++;
	}

	//
	// parse available lines
	//

	//first parse only non-comment lines (to ensure fields are sorted as in the file)
	for(int i=0; i<lstInput.size(); i++)
	{
		if(lstInput[i].indexOf("--") == 0)	//comment line
		{
			// we'll parse these in the next pass
		}
		else if (lstInput[i].indexOf("create table") == 0)  // line with table name
		{
			//extract table name
			QString strName = lstInput[i];
			strName = strName.right(strName.length()-strlen(("create table")));
			int nPos = strName.indexOf("(");
			if(nPos >= 0)
				strName = strName.left(nPos);
			strName = strName.trimmed();	//trim the name

			m_strTableName = strName;
		}
		else	// line with field name
		{
			//extract field name
			QString strName = lstInput[i];
			int nPos = strName.indexOf(" ");
			if(nPos >= 0)
				strName = strName.left(nPos);
			strName = strName.trimmed();

			//extract field's sql type
			QString strSqlType = lstInput[i];
			if(nPos >= 0)
				strSqlType = strSqlType.right(strSqlType.size()-nPos);
			strSqlType = strSqlType.trimmed();
			nPos = strSqlType.indexOf(" ");
			if(nPos >= 0)
				strSqlType = strSqlType.left(nPos);
			nPos = strSqlType.indexOf("(");
			if(nPos >= 0)
				strSqlType = strSqlType.left(nPos);
			strSqlType = strSqlType.trimmed();

			//convert SQL to Variant type
			QString strVarType = m_lstTypeMap[strSqlType];

			FieldInfo info;
			info.strName = strName;

			nPos = m_lstFields.indexOf(info);	//does this field entry already exist
			if(nPos >= 0){
				Q_ASSERT(FALSE);
				m_lstFields[nPos].strType = strVarType;	// update existing entry
			}
			else
			{
				info.strType = strVarType;
				m_lstFields.push_back(info);	// add new entry
			}
		}
	}

	//next parse only comment lines !!!
	for(int i=0; i<lstInput.size(); i++)
	{
		if(lstInput[i].indexOf("--") == 0)	//comment line
		{
			//extract field name
			QString strName = lstInput[i];
			strName = strName.right(strName.length()-strlen(("--")));
			int nPos = strName.indexOf(":");
			if(nPos >= 0)
				strName = strName.left(nPos);
			strName = strName.trimmed();	//trim the name

			FieldInfo info;
			info.strName = strName;

			//extract label and tooltip
			QString strTooltip;
			QString strLabel = lstInput[i];
			nPos = strLabel.indexOf("\\");
			if(nPos >= 0){
				strLabel = strLabel.right(strLabel.size()-nPos-1);
				nPos = strLabel.indexOf("\\");
				if(nPos >= 0){
					strTooltip = strLabel.right(strLabel.size()-nPos-1);
					strLabel = strLabel.left(nPos);
					strLabel = strLabel.trimmed();	//trim the name

					info.strLabel = strLabel;
				}
			}
			nPos = strTooltip.indexOf("\\");
			if(nPos >= 0){
				strTooltip = strTooltip.left(nPos);
				strTooltip = strTooltip.trimmed();	//trim the name
			
				info.strTooltip = strTooltip;
			}

			//does this field entry already exist
			nPos = m_lstFields.indexOf(info);
			if(nPos >= 0){
				// update existing entry
				m_lstFields[nPos].strLabel = info.strLabel;
				m_lstFields[nPos].strTooltip = info.strTooltip;	
			}
		}
	}

	//
	// STEP 3: generate output file
	//
	FILE *pOutFile = fopen(strOutputFile.toLatin1().constData(), "w");
	if(pOutFile)
	{
		fprintf(pOutFile, "\tview.m_nSkipFirstColsInsert\t= 3;\n");
		fprintf(pOutFile, "\tview.m_nSkipFirstColsUpdate\t= 3;\n");
		fprintf(pOutFile, "\tview.m_strTables=\"%s\";\n\n", m_strTableName.toLatin1().constData());
		fprintf(pOutFile, "\t//Record set definition\n");

		int nCount = m_lstFields.size();
		for(int i=0; i<nCount; i++){
			fprintf(pOutFile, "\tview.append(DbColumnEx(\"%s\", %s));\n", 
								m_lstFields[i].strName.toLatin1().constData(),
								m_lstFields[i].strType.toLatin1().constData());
		}

		fprintf(pOutFile, "\n\t//Add extra column info\n");

		for(int i=0; i<nCount; i++){
			if( m_lstFields[i].strLabel.size()>0 ||
				m_lstFields[i].strTooltip.size()>0)
			{
				fprintf(pOutFile, "\tview.m_lstExtraColumnInfo[\"%s\"]=DbColumnExtraInfo(tr(\"%s\"),tr(\"%s\"));\n", 
									m_lstFields[i].strName.toLatin1().constData(),
									m_lstFields[i].strLabel.toLatin1().constData(),
									m_lstFields[i].strTooltip.toLatin1().constData());
			}
		}

		fclose(pOutFile);
	}

	QMessageBox::information(this, "", "Done!");
}
