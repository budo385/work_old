#ifndef VISIODDL2QTCODE_H
#define VISIODDL2QTCODE_H

#include <QtGui/QMainWindow>
#include "ui_visioddl2qtcode.h"

class FieldInfo {
public:
	FieldInfo(){};
	FieldInfo(const FieldInfo &other){ operator = (other); };
	void operator = (const FieldInfo &other) {
		strName		= other.strName;
		strType		= other.strType;
		strLabel	= other.strLabel;
		strTooltip	= other.strTooltip;
	}
	bool operator == (const FieldInfo &other) {
		return (strName	== other.strName);
	}

	QString strName;
	QString strType;	// "QVariant::Int"
	QString strLabel;
	QString strTooltip;
};


class VisioDDl2QtCode : public QMainWindow
{
    Q_OBJECT

public:
    VisioDDl2QtCode(QWidget *parent = 0, Qt::WFlags flags = 0);
    ~VisioDDl2QtCode();

protected:
	//parsed data
	QList<FieldInfo> m_lstFields;
	QString	m_strTableName;

	//map SQL to Variant data
	QMap<QString, QString>	m_lstTypeMap;

private:
    Ui::VisioDDl2QtCodeClass ui;

private slots:
	void on_btnGenerate_clicked();
	void on_btnPickDst_clicked();
	void on_btnPickSrc_clicked();
};

#endif // VISIODDL2QTCODE_H
