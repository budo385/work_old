#include "dlgimportprogress.h"
#include "common/common/datahelper.h"


DlgImportProgress::DlgImportProgress(QWidget *parent)
	: QDialog(parent),m_nTotalRecords(0)
{
	ui.setupUi(this);
	setWindowTitle("Importing data...");
	ui.btnCancel->setVisible(false);
	
}

DlgImportProgress::~DlgImportProgress()
{

}

void DlgImportProgress::Start(quint64 nTotalRecords, int nTableCnt)
{
	m_nTotalRecords=nTotalRecords;
	m_nTableCnt=nTableCnt;
	m_nCurrRecords=0;
	m_nCurrRecordsLastTable=0;
	m_nCurrTableID=0;
	dtStartTime=QDateTime::currentDateTime();
	ui.progressBarSingle->reset();
	ui.progressBarSingle->setRange(0,100);
	ui.progressBarTotal->reset();
	ui.progressBarTotal->setRange(0,100);
}



//reset progress and hides it
void DlgImportProgress::Stop()
{
	m_nTotalRecords=0;
	m_nCurrRecords=0;
	m_nCurrRecordsLastTable=0;
	m_nCurrTableID=0;
	m_nTableCnt=0;
	hide();
}

void DlgImportProgress::UpdateProgress(QString strStep, quint64 nTotalCurrRecord, int nTableCurrIdx, int nTableCurrRec, int nTableTotalRec)
{
	bool bSameStep=false;
	if (m_strStepLast==strStep)
	{
		m_nCurrentStepHits++;
		if (m_nCurrentStepHits>100)
			m_nCurrentStepHits=50;
		bSameStep=true;
	}
	else
	{
		m_nCurrentStepHits=0;
		m_strStepLast=strStep;
	}

	//Parse step:
	QString strStepNo=strStep.left(strStep.indexOf("/")).mid(strStep.indexOf(" ")+1).trimmed();
	int nStep=strStepNo.toInt();

	//time elapsed:
	QString strTitle="Importing Omnis data, elapsed time: %1, estimated time: %2";
	quint64 nElapsedTime = dtStartTime.msecsTo(QDateTime::currentDateTime());
	QString strElapsedTime =DataHelper::GetFormatedTime(nElapsedTime);

	quint64 nMsecRemain=0;
	if (nStep<4)
	{
		nMsecRemain+=3*60*1000; //before
		nMsecRemain+=(m_nTotalRecords/1000)*1*10*1000; //estimate 10sec for 1000 records
		nMsecRemain+=2*60*1000+(m_nTotalRecords/10000)*1*60*1000; //2 minutes afters + based on records some more
	}
	else if (nStep==4)
	{
		nMsecRemain = ((double)m_nTotalRecords/nTotalCurrRecord)*nElapsedTime-nElapsedTime;
		nMsecRemain+=2*60*1000+(m_nTotalRecords/10000)*1*10*1000; //2 minutes afters + based on records some more
		strStep+=", table "+QString::number(nTableCurrIdx+1)+"/"+QString::number(m_nTableCnt);
	}
	else if (nStep>4)
	{
		nMsecRemain+=2*60*1000+(m_nTotalRecords/10000)*1*60*1000; //2 minutes afters + based on records some more
	}

	//calc remaining time:
	QString strRemainTime = DataHelper::GetFormatedTime(nMsecRemain);
	ui.labelTitle->setText(strTitle.arg(strElapsedTime).arg(strRemainTime));

	//set current header:
	if (nStep==4 && nTableTotalRec>0)
	{
		strStep+=", processed table records %1/%2";
		strStep=strStep.arg(nTableCurrRec).arg(nTableTotalRec);
	}
	ui.labelFileInfo->setText(strStep);		

	//global progress:
	int nTotalProgress=m_nTableCnt+9;
	int nCurrTotalProgress=nStep+nTableCurrIdx;
	ui.progressBarTotal->setValue(((double)nCurrTotalProgress/nTotalProgress)*100);

	//single progress:
	if (bSameStep)
	{
		if (nStep==4)
			ui.progressBarSingle->setValue(((double)nTableCurrRec/nTableTotalRec)*100);
		else
		{
			double dDummyProgress=(double)m_nCurrentStepHits/100; //give it 100 ticks
			ui.progressBarSingle->setValue(dDummyProgress*100);
		}
		
	}
	else
	{
		ui.progressBarSingle->reset();
	}

	if(isHidden() && nElapsedTime>700)show(); //show only after half of second
}

