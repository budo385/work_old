#include <QApplication>
#include "admintool.h"


int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	//init resources for all linked libraries 
	Q_INIT_RESOURCE(gui_core);


	AdminTool w;


	w.show();
	a.connect(&a, SIGNAL(lastWindowClosed()), &a, SLOT(quit()));
	return a.exec();
	//QTimer::singleShot(0, App, SLOT(Start()));	//wait to start until QCoreApp enters event loop

	return a.exec();
}
