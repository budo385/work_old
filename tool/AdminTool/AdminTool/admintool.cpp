#include "admintool.h"
#include <QHeaderView>
#include "databasemanager.h"
#include "clientinifile.h"
ClientIniFile g_INIFile;

AdminTool::AdminTool(QWidget *parent, Qt::WindowFlags flags)
	: QMainWindow(parent, flags)
{
	ui.setupUi(this);

	g_INIFile.Load(QCoreApplication::applicationDirPath()+"/admintool.ini");

	SetupGUI();

}


AdminTool::~AdminTool()
{
	QWidget * pWidget=ui.stackedWidget->widget(0);
	DatabaseManager *pDbMan=dynamic_cast<DatabaseManager *>(pWidget);
	if (pDbMan)
	{
		pDbMan->SaveState();
	}
	g_INIFile.Save(QCoreApplication::applicationDirPath()+"/admintool.ini");

}

void AdminTool::SetupGUI()
{

	QList<QTreeWidgetItem*> itemsList;

	//Set tree look.
	ui.treeWidget->header()->hide();
	ui.treeWidget->setSelectionMode(QAbstractItemView::SingleSelection);

	//First row is application option set.
	QTreeWidgetItem *Item = new QTreeWidgetItem();
	Item->setIcon(0, QIcon(":edit_mode.png"));
	Item->setData(0, Qt::DisplayRole, "DataBase Actualization");
	Item->setData(0, Qt::UserRole, MENU_DB_ACTUALIZATION);
	itemsList << Item;

	/*
	Item = new QTreeWidgetItem();
	Item->setIcon(0, QIcon(":edit_mode.png"));
	Item->setData(0, Qt::DisplayRole, "DataBase Batch Act.");
	Item->setData(0, Qt::UserRole, MENU_DB_BATCH_ACTUALIZATION);
	itemsList << Item;
	*/


	ui.treeWidget->addTopLevelItems(itemsList);

	//insert widgets:
	QWidget *pWidget= new DatabaseManager;
	ui.stackedWidget->insertWidget(0,pWidget);

	//pWidget= new DatabaseBatchProcess;
	//ui.stackedWidget->insertWidget(1,pWidget);


	//Connect tree selection to options widget.
	connect(ui.treeWidget, SIGNAL(itemSelectionChanged()), this, SLOT(on_treeSelectionChanged()));

	//Select first item.
	if (ui.treeWidget->topLevelItemCount())
		ui.treeWidget->setCurrentItem(ui.treeWidget->topLevelItem(0));
}


void AdminTool::on_treeSelectionChanged()
{
	int nMenuID;
	QList<QTreeWidgetItem *> items=ui.treeWidget->selectedItems();
	if (items.count()!=1)
	{
		return;
	}
	else
	{
		nMenuID=items.at(0)->data(0,Qt::UserRole).toInt();
	}

	switch(nMenuID)
	{
	case MENU_DB_ACTUALIZATION:
		ui.stackedWidget->setCurrentIndex(0);
		break;

	}
}