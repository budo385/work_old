#ifndef ADMINTOOL_H
#define ADMINTOOL_H

#include <QMainWindow>
#include "ui_admintool.h"

class AdminTool : public QMainWindow
{
	Q_OBJECT

public:
	AdminTool(QWidget *parent = 0, Qt::WindowFlags flags = 0);
	~AdminTool();

	enum MENU_ITEMS
	{
		MENU_DB_ACTUALIZATION
		//MENU_DB_BATCH_ACTUALIZATION


	};

private slots:
		void on_treeSelectionChanged();

private:
	Ui::AdminToolClass ui;
	void AdminTool::SetupGUI();

};

#endif // ADMINTOOL_H
