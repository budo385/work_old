#ifndef DLGDOCUMENTTRANSFERPROGRESS_H
#define DLGDOCUMENTTRANSFERPROGRESS_H

#include <QDialog>
#include <QDateTime>
#include "ui_dlgimportprogress.h"

/*
	Usage: 
	- call Start()
	- call SetFileName()
	- call UpdateProgress()
	- call Stop()
*/

class DlgImportProgress : public QDialog
{
	Q_OBJECT

public:
	DlgImportProgress(QWidget *parent = 0);
	~DlgImportProgress();

	void Start(quint64 nTotalRecords, int nTableCnt);
	void Stop();
	void UpdateProgress(QString strStep, quint64 nTotalCurrRecord, int nTableCurrIdx, int nTableCurrRec, int nTableTotalRec);
	
public slots:
	void on_btnCancel_clicked(){emit CancelTransfer();};

signals:
	void CancelTransfer();
	
private:
	Ui::DlgImportProgress ui;

	QDateTime	dtStartTime;
	QString		m_strStepLast;
	quint64		m_nTotalRecords;
	quint64		m_nCurrRecords;
	quint64		m_nCurrRecordsLastTable;
	int			m_nCurrTableID;
	int			m_nTableCnt;
	int			m_nTableCurrIdx;
	quint64		m_nCurrentStepHits; //every 400msecs on same step, estimate 2 minutes for each: 300 total

};

#endif // DlgImportProgress_H
