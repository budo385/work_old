#include "databasemanager.h"
#include <QFileDialog>
#include <QMessageBox>
#include <QInputDialog>
#include <QStringList>
#include "db_core/db_core/dbconnectionslist.h"
#include "gui_core/gui_core/dbconnsettingsgui.h"
#include "common/common/config.h"
#include "common/common/config_version_sc.h"
#include "common/common/datahelper.h"

#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QSqlRecord>
#include <QLocale>
#include <QProcess>
#include <QTimer>




typedef bool (*tCopyDatabase)(char *,const char **,bool);

#include "clientinifile.h"
extern ClientIniFile g_INIFile;

#define HTML_START_F1	"<html><head><meta name=\"qrichtext\" content=\"1\" /></head><body style=\" white-space: pre-wrap; font-family:MS Shell Dlg 2; font-size:10pt; font-weight:bold; font-style:normal; text-decoration:none;\"><p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:10pt; font-weight:bold; font-style:italic;\">"
#define HTML_END_F1		"</span></p></body></html>"


//#include <windows.h>
//HINSTANCE g_hMAPIexDll;


DatabaseManager::DatabaseManager(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
	

	ui.btnSelectBackup->setIcon(QIcon(":SAP_Select.png"));
	ui.btnSelectSettings->setIcon(QIcon(":SAP_Select.png"));
	ui.btnEditSettings->setIcon(QIcon(":SAP_Modify.png"));
	ui.btnSelectStart->setIcon(QIcon(":SAP_Select.png"));
	ui.btnSelectEnd->setIcon(QIcon(":SAP_Select.png"));
	ui.btnEditSettingsTarget->setIcon(QIcon(":SAP_Modify.png"));

	//pm: 2.4.2012
	ui.btnInputOmnisDDL->setIcon(QIcon(":SAP_Select.png"));
	ui.btnOutputFromOmnisDDL->setIcon(QIcon(":SAP_Select.png"));

	//load from INI:
	m_strBackupDirPath=g_INIFile.m_strBackupDirPath;
	m_strDbConnectionsettingsPath=g_INIFile.m_strDbConnectionsettingsPath;
	ui.txtPathStartBat->setText(g_INIFile.m_strBatch_StartScript);
	ui.txtPathEndBat->setText(g_INIFile.m_strBatch_EndScript);

	ui.txtSettingsPath->setReadOnly(true);
	ui.txtPath->setReadOnly(true);

	ui.ckbSPCImportDropExisting->setChecked(true);

	QString strDatabaseString="SC: "+QString::number(DATABASE_VERSION)+" SPC: "+QString::number(DATABASE_VERSION_SPC);
	ui.labelVerActual->setText(strDatabaseString);

	ui.tableWidget->Initialize(&m_lstBackups,false,false,true,true,25,true);
	m_lstBackups.addColumn(QVariant::String,"FILE");
	m_lstBackups.addColumn(QVariant::String,"ORDER_STRING");
	DbRecordSet lstSetup;
	ui.tableWidget->AddColumnToSetup(lstSetup,"FILE","Backup File",400,false);
	ui.tableWidget->AddColumnToSetup(lstSetup,"ORDER_STRING","Date Created",150,false);
	ui.tableWidget->SetColumnSetup(lstSetup);

	//BATCH:
	m_lstConnectionsTable.addColumn(QVariant::String,"Connection");
	m_lstConnectionsTable.addColumn(QVariant::String,"LastError");

	lstSetup.destroy();
	ui.tableWidgetBatch->Initialize(&m_lstConnectionsTable,false,false,false,true,25,true);
	ui.tableWidgetBatch->AddColumnToSetup(lstSetup,"Connection","Connection",200,false);
	ui.tableWidgetBatch->AddColumnToSetup(lstSetup,"LastError","Last Error",400,false);
	ui.tableWidgetBatch->SetColumnSetup(lstSetup);

	ClearConnected();

	//reload settings, set to last connection:
	if (!m_strDbConnectionsettingsPath.isEmpty())
	{
		ui.txtSettingsPath->setText(m_strDbConnectionsettingsPath);
		ReloadSettings();
		if (!g_INIFile.m_strLastConnectionName.isEmpty())
		{
			int nSize=m_lstConnections.size();
			for(int i=0;i<nSize;++i)
			{
				if (m_lstConnections.at(i).m_strCustomName==g_INIFile.m_strLastConnectionName)
				{
					ui.cboConnection->setCurrentIndex(i);
					break;
				}
			}
		}
	}
	else
	{
		ui.cboConnection->setEnabled(false);
		ui.btnEditSettings->setEnabled(false);
	}

	
	if (m_strBackupDirPath.isEmpty())
	{
		m_strBackupDirPath=QCoreApplication::applicationDirPath();
	}
	ui.txtPath->setText(m_strBackupDirPath);


	ui.tabWidget->setCurrentIndex(0);
	//this->setAttribute(Qt::WA_DeleteOnClose, true);

#ifdef QT_NO_DEBUG
	ui.btnImportFromOracleSPC->setVisible(false);
	ui.groupDDLFromOmnis->setVisible(false);
	ui.ckbSPCImportDropExisting->setVisible(false);
#endif



	//B.T. 2014: get local days, months, short ones:
	/*
	QLocale loc_orig(QLocale::English,QLocale::AnyCountry);
	QLocale loc_target(QLocale::Turkish,QLocale::Turkey);
	/*
		for (int i=1;i<8;i++)
	{
		qDebug()<<loc_orig.dayName(i, QLocale::LongFormat) <<"\t"<<loc_target.dayName(i, QLocale::LongFormat); 
	}
	*/
	
	//qDebug()<<"/*SHORT DAYS*/";
	/*
	for (int i=1;i<8;i++)
	{
		qDebug()<<loc_orig.dayName(i, QLocale::ShortFormat) <<"\t"<<QString(loc_target.dayName(i, QLocale::ShortFormat).left(1).toUpper()+loc_target.dayName(i, QLocale::ShortFormat).mid(1)); 
	}

	qDebug()<<"";
	*/
	
	//qDebug()<<"/*VERY SHORT DAYS*/";
	/*
	for (int i=1;i<8;i++)
	{
		qDebug()<<loc_orig.dayName(i, QLocale::ShortFormat).left(2) <<"\t"<<QString(loc_target.dayName(i, QLocale::ShortFormat).left(1).toUpper()+loc_target.dayName(i, QLocale::ShortFormat).mid(1,1)); 
	}*/

	//qDebug()<<"";
	//qDebug()<<"/*MONTHS*/";
	/*
	for (int i=1;i<=12;i++)
	{
		qDebug()<<loc_orig.monthName(i, QLocale::LongFormat) <<"\t"<<QString(loc_target.monthName(i, QLocale::LongFormat).left(1).toUpper()+loc_target.monthName(i, QLocale::LongFormat).mid(1)); 
	}

	qDebug()<<"";
	*/
	//qDebug()<<"/*SHORT MONTHS*/";
	/*
	for (int i=1;i<=12;i++)
	{
		qDebug()<<loc_orig.monthName(i, QLocale::ShortFormat) <<"\t"<<QString(loc_target.monthName(i, QLocale::ShortFormat).left(1).toUpper()+loc_target.monthName(i, QLocale::ShortFormat).mid(1)); 
	}
	*/
}

DatabaseManager::~DatabaseManager()
{
	on_btnDisconnect_clicked();



}

void DatabaseManager::SaveState()
{
	//save too INI:
	g_INIFile.m_strBackupDirPath=m_strBackupDirPath;
	g_INIFile.m_strDbConnectionsettingsPath=m_strDbConnectionsettingsPath;
	g_INIFile.m_strLastConnectionName=ui.cboConnection->currentText();
	//save too INI:
	g_INIFile.m_strBatch_StartScript=ui.txtPathStartBat->toPlainText();
	g_INIFile.m_strBatch_EndScript=ui.txtPathEndBat->toPlainText();
}

void DatabaseManager::SetConnected(DbConnectionSettings connection)
{
	QString strDatabaseString="SC: "+QString::number(m_nDbVersion)+" SPC: "+QString::number(m_nDbVersionSPC);
	ui.labelVer->setText(strDatabaseString);
	if (m_nDbVersion>=DATABASE_VERSION && m_nDbVersionSPC>=DATABASE_VERSION_SPC)
		ui.btnDbOrganize->setEnabled(false);
	else
		ui.btnDbOrganize->setEnabled(true);

	QString strConnName=tr("Connected to: ")+connection.m_strDbName+tr(" at: ")+connection.m_strDbHostName+tr(", as: ")+connection.m_strDbUserName;
	ui.labelConn->setText(HTML_START_F1+strConnName+QString(HTML_END_F1));

	ui.btnSelectSettings->setEnabled(false);
	ui.btnEditSettings->setEnabled(false);
	ui.cboConnection->setEnabled(false);
	ui.btnConnect->setEnabled(false);
	ui.btnDisconnect->setEnabled(true);


	ui.btnSelectBackup->setEnabled(false);
	ReloadBackups();

	ui.btnBackup->setEnabled(true);
	ui.btnRestore->setEnabled(true);
	//ui.btnCreateODBC_User->setEnabled(true);
	ui.btnImportFromOracleSPC->setEnabled(true);
	ui.btnImportFromOmnis->setEnabled(true);
	ui.ckbSPCImportDropExisting->setEnabled(true);

}


void DatabaseManager::ClearConnected()
{
	m_lstBackups.clear();
	ui.tableWidget->RefreshDisplay();
	ui.btnSelectBackup->setEnabled(true);
	ui.btnBackup->setEnabled(false);
	ui.btnRestore->setEnabled(false);

	ui.btnDbOrganize->setEnabled(false);
	ui.labelVer->clear();

	ui.btnDisconnect->setEnabled(false);
	ui.btnSelectSettings->setEnabled(true);
	ui.btnEditSettings->setEnabled(true);
	ui.cboConnection->setEnabled(true);
	ui.btnConnect->setEnabled(true);
	ui.labelConn->setText(HTML_START_F1+tr("not connected")+QString(HTML_END_F1));
	//ui.btnCreateODBC_User->setEnabled(false);
	ui.btnImportFromOracleSPC->setEnabled(false);
	ui.btnImportFromOmnis->setEnabled(false);
	ui.ckbSPCImportDropExisting->setEnabled(false);

}

void DatabaseManager::on_btnSelectSettings_clicked()
{
	QString strStartDir=QDir::currentPath(); 
	QString strFilter="database_connections.cfg, *.cfg";


	if (!m_strDbConnectionsettingsPath.isEmpty())
	{
		QFileInfo info(m_strDbConnectionsettingsPath);
		strStartDir=info.absolutePath ();
	}

	//open file dialog:
	QString strFile = QFileDialog::getOpenFileName(
		NULL,
		QT_TR_NOOP("Select database connection settings file"),
		strStartDir,
		strFilter);

	if(!strFile.isEmpty())
	{
		ui.txtSettingsPath->setText(strFile);
		m_strDbConnectionsettingsPath=strFile;
		ReloadSettings();
	}

}




void DatabaseManager::ReloadSettings()
{
	//read all from file, put in combo:
	DbConnectionsList objData;
	if(!objData.Load(m_strDbConnectionsettingsPath, MASTER_PASSWORD))
	{
		QMessageBox::critical(this,tr("Error"),tr("Can not open database setting file!"));
		ui.cboConnection->setEnabled(false);
		ui.cboConnectionTarget->setEnabled(false);
		ui.btnEditSettings->setEnabled(false);
		return;
	}

	m_lstConnections=objData.m_lstConnections;
	ui.cboConnection->clear();
	ui.cboConnectionTarget->clear();
	int nCnt = m_lstConnections.size();
	for(int i=0; i<nCnt; i++)
	{
		ui.cboConnection->addItem(m_lstConnections[i].m_strCustomName);
		ui.cboConnectionTarget->addItem(m_lstConnections[i].m_strCustomName);
	}



	ui.cboConnection->setEnabled(true);
	ui.cboConnectionTarget->setEnabled(true);
	
	ui.btnEditSettings->setEnabled(true);

}

void DatabaseManager::on_btnEditSettings_clicked()
{
	DbConnectionsList objData;
	if(!objData.Load(m_strDbConnectionsettingsPath, MASTER_PASSWORD))
	{
		QMessageBox::critical(this,tr("Error"),tr("Can not open database setting file!"));
		return;
	}

	//ask for password if the one was set
	if(objData.IsUserPasswordSet())
	{
		bool ok;
		QString text = QInputDialog::getText(NULL, "User Settings Password",
			"Password:", QLineEdit::Password,
			"", &ok);
		if (ok)	//empty pass allowed
		{
			if(!objData.MatchUserPassword(text)){
				QMessageBox::information(NULL, "Error", "Password does not match!");
				return;
			}
		}
		else
			return;
	}

	// Now display the GUI dialog
	DbConnSettingsGUI dlg;
	dlg.SetConnections(objData.m_lstConnections, ui.cboConnection->currentIndex());
	dlg.SetPassword(objData.IsUserPasswordSet(), 
		objData.GetPasswordHash());

	//display dialog	
	int nRes = dlg.exec();
	if(nRes > 0)
	{
		//save changes from the dialog
		objData.m_lstConnections = dlg.GetConnections();
		int nCurrentSelection=dlg.GetCurSelection();

		bool bUsePass = false;
		QString strHash;
		dlg.GetPassword(bUsePass, strHash);

		if(bUsePass)
			objData.SetUserPasswordHash(strHash);
		else
			objData.ClearUserPassword();

		objData.Save(m_strDbConnectionsettingsPath, MASTER_PASSWORD);

		ReloadSettings();
		ui.cboConnection->setCurrentIndex(nCurrentSelection);

	}


}

void DatabaseManager::on_btnConnect_clicked()
{
/*
	DateTimeHandler::s_bUseUTCTimeZone=true;
	QDateTime t1=DateTimeHandler::currentDateTime();

	//QDateTime t1=QDateTime::currentDateTime();
	qDebug()<<t1;
	qDebug()<<t1.timeSpec();
	//QDateTime t5=t1.addSecs(-7200);
	//qDebug()<<t5;
	//t5.setTimeSpec(Qt::UTC);
	//qDebug()<<t1.toLocalTime();
	QDateTime t2=DateTimeHandler::fromUTC(t1);
	qDebug()<<t2;
	qDebug()<<t2.timeSpec();

	QDateTime t4=DateTimeHandler::toUTC(t2);
	qDebug()<<t4;
	qDebug()<<t4.timeSpec();

	if (t1==t4)
	{
		qDebug()<<"ok";
	}

	QDateTime t3(t2.date(),t2.time(),Qt::UTC);
	qDebug()<<t3.timeSpec();
	qDebug()<<t3;
	qDebug()<<t3.toLocalTime();
*/

	int nCurrentSelection=ui.cboConnection->currentIndex();
	if (nCurrentSelection==-1)
		return;
	DbConnectionSettings settings=m_lstConnections.at(nCurrentSelection);

	//load actualize dll:
	if(!m_DbActualizator.Initialize(settings)) 
	{
		QMessageBox::critical(this,tr("Error"),tr("Can not load db_actualize.dll!"));
		ClearConnected();
		return;
	}
	//bring up connection:
	Status err;
	m_DbActualizator.InitConnection(err,m_strBackupDirPath,false);
	if (!err.IsOK())
	{
		QMessageBox::critical(this,tr("Error"),err.getErrorText());
		return;
	}

	//read DB version:
	GetDbVersion();
	SetConnected(settings);


}

void DatabaseManager::on_btnDisconnect_clicked()
{

	//load actualize dll:
	Status pStatus;
	m_DbActualizator.DisConnect(pStatus);
	/*
	if (!pStatus.IsOK())
	{
		QMessageBox::critical(this,tr("Error"),pStatus.getErrorText());
		return;
	}
	*/

	ClearConnected();
}

void DatabaseManager::on_btnSelectBackup_clicked()
{
	QString strStartDir=ui.txtPath->toPlainText();

	if (strStartDir.isEmpty())
	{
		strStartDir=QCoreApplication::applicationDirPath();
	}

	//open file dialog:
	QString strFile = QFileDialog::getExistingDirectory(
		NULL,
		QT_TR_NOOP("Select backup directory"),
		strStartDir);

	if(!strFile.isEmpty())
	{
		ui.txtPath->setText(strFile);
		m_strBackupDirPath=strFile;
		ReloadBackups();
	}
}


void DatabaseManager::on_btnReloadBackups_clicked()
{
	ReloadBackups();
}

void DatabaseManager::ReloadBackups()
{
	
	m_lstBackups.clear();
	if (m_strBackupDirPath.isEmpty() || ui.cboConnection->currentIndex()<0) return;
	QString strDbName=m_lstConnections.at(ui.cboConnection->currentIndex()).m_strDbName;

	if (ui.ckbIgnoreVerision->isChecked())
	{
		strDbName="";
	}

	Status err;
	m_DbActualizator.LoadBackupList(err,m_strBackupDirPath,m_lstBackups,strDbName);
	if (!err.IsOK())
	{
		QMessageBox::critical(this,tr("Error"),err.getErrorText());
		return;
	}
	
	m_lstBackups.sort(1,1);
	ui.tableWidget->RefreshDisplay();

}


void DatabaseManager::on_btnBackup_clicked()
{
	if(QMessageBox::question(this,tr("Confirmation"),tr("Create new backup. Proceed?"),tr("Yes"),tr("No"))) return;

	Status err;
	QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
	QString strBcp=m_DbActualizator.Backup(err);
	QApplication::restoreOverrideCursor();
	if (!err.IsOK())
	{
		QMessageBox::critical(this,tr("Error"),err.getErrorText());
		return;
	}

	ReloadBackups();
}
void DatabaseManager::on_btnRestore_clicked()
{
	int nRow=m_lstBackups.getSelectedRow();
	if(nRow<0) return;

	QString strRestore=m_lstBackups.getDataRef(nRow,0).toString();
	if(QMessageBox::question(this,tr("Confirmation"),tr("Restore Database from: ")+strRestore+tr(". This will destroy current database data. Proceed?"),tr("Yes"),tr("No"))) return;

	Status err;
	QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
	strRestore=m_strBackupDirPath+"/"+strRestore;
	m_DbActualizator.Restore(err,strRestore);
	QApplication::restoreOverrideCursor();
	if (!err.IsOK())
	{
		QMessageBox::critical(this,tr("Error"),err.getErrorText());
		return;
	}

	GetDbVersion();
}


void DatabaseManager::on_btnDbOrganize_clicked()
{
	if(QMessageBox::question(this,tr("Confirmation"),tr("Organize Database to latest version. Proceed?"),tr("Yes"),tr("No"))) return;

	bool bDropData=ui.ckbTable->isChecked();

	Status err;
	QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
	m_DbActualizator.Organize(err,bDropData);
	QApplication::restoreOverrideCursor();
	if (!err.IsOK())
	{
		QMessageBox::critical(this,tr("Error"),err.getErrorText());
		return;
	}
	GetDbVersion();
	ReloadBackups();
}


int DatabaseManager::GetDbVersion()
{
	Status err;
	m_nDbVersion=m_DbActualizator.GetDbVersion(err);
	if (!err.IsOK())
	{
		QMessageBox::critical(this,tr("Error"),err.getErrorText());
		m_nDbVersion=-1;
		return -1;
	}

	m_nDbVersionSPC=m_DbActualizator.GetDbVersion_SPC(err);
	if (!err.IsOK())
	{
		QMessageBox::critical(this,tr("Error"),err.getErrorText());
		m_nDbVersionSPC=-1;
		return -1;
	}

	QString strDatabaseString="SC: "+QString::number(m_nDbVersion)+" SPC: "+QString::number(m_nDbVersionSPC);

	ui.labelVer->setText(strDatabaseString);

	if (m_nDbVersion>=DATABASE_VERSION && m_nDbVersionSPC>=DATABASE_VERSION_SPC)
		ui.btnDbOrganize->setEnabled(false);
	else
		ui.btnDbOrganize->setEnabled(true);
	return m_nDbVersion;
}




//--------------------------------------------------------------------------------
//								BATCH
//--------------------------------------------------------------------------------


void DatabaseManager::on_btnSelectStart_clicked()
{
	QString strStartDir=QDir::currentPath(); 
	QString strFilter="*.*";


	if (!ui.txtPathStartBat->toPlainText().isEmpty())
	{
		QFileInfo info(ui.txtPathStartBat->toPlainText());
		strStartDir=info.absolutePath ();
	}

	//open file dialog:
	QString strFile = QFileDialog::getOpenFileName(
		NULL,
		QT_TR_NOOP("Select Start script to execute"),
		strStartDir,
		strFilter);

	if(!strFile.isEmpty())
	{
		ui.txtPathStartBat->setText(strFile);
	}

}
void DatabaseManager::on_btnSelectEnd_clicked()
{
	QString strStartDir=QDir::currentPath(); 
	QString strFilter="*.*";


	if (!ui.txtPathEndBat->toPlainText().isEmpty())
	{
		QFileInfo info(ui.txtPathEndBat->toPlainText());
		strStartDir=info.absolutePath ();
	}

	//open file dialog:
	QString strFile = QFileDialog::getOpenFileName(
		NULL,
		QT_TR_NOOP("Select End script to execute"),
		strStartDir,
		strFilter);

	if(!strFile.isEmpty())
	{
		ui.txtPathEndBat->setText(strFile);
	}

}


void DatabaseManager::on_btnLoad_clicked()
{
	//read all from file, put in combo:
	DbConnectionsList objData;
	if(!objData.Load(ui.txtSettingsPath->toPlainText(), MASTER_PASSWORD))
	{
		QMessageBox::critical(this,tr("Error"),tr("Can not open database setting file!"));
		return;
	}

	m_lstConnections=objData.m_lstConnections;

	m_lstConnectionsTable.clear();

	int nCnt = m_lstConnections.size();
	for(int i=0; i<nCnt; i++)
	{
		m_lstConnectionsTable.addRow();
		int nRow=m_lstConnectionsTable.getRowCount()-1;
		m_lstConnectionsTable.setData(nRow,0,m_lstConnections[i].m_strCustomName);
	}

	ui.tableWidgetBatch->RefreshDisplay();
}


void DatabaseManager::on_btnDbOrganizeBatch_clicked()
{
	if(QMessageBox::question(this,tr("Confirmation"),tr("Organize Selected Database(s) to latest version. Proceed?"),tr("Yes"),tr("No"))) return;

	bool bDropData=ui.ckbTable->isChecked();

		QProcess script;
		if (!ui.txtPathStartBat->toPlainText().isEmpty())
		{
			QString strBat=ui.txtPathStartBat->toPlainText();
#ifdef _WIN32 //put in quotes for windows
			strBat.prepend("\"");
			strBat.append("\"");
#endif
			/*
			QFileInfo info(strBat);
			if(!info.exists());
			{
			QMessageBox::critical(this,tr("Error"),tr("Failed to find start batch script!"));
			return;
			}
			*/

			script.execute(strBat);
		}

		QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));

		int nSize=m_lstConnectionsTable.getRowCount();
		for(int i=0;i<nSize;++i)
		{
			ui.tableWidgetBatch->RefreshDisplay(); //refresh content every time
			QApplication::processEvents(QEventLoop::ExcludeUserInputEvents);

			if (!m_lstConnectionsTable.isRowSelected(i))
			{
				m_lstConnectionsTable.setData(i,1,"");
				continue;
			}

			QString strErr;
			if (!Connect(m_lstConnectionsTable.getDataRef(i,0).toString(),strErr))
			{
				m_lstConnectionsTable.setData(i,1,strErr);
				continue;
			}
			else
			{
				Status err;
				m_DbActualizator.Organize(err,bDropData);
				if (!err.IsOK())
				{
					strErr=err.getErrorText();
					m_lstConnectionsTable.setData(i,1,strErr);
				}
				if (!DisConnect(strErr))
				{
					m_lstConnectionsTable.setData(i,1,strErr);
					continue;
				}

			}


			if (strErr.isEmpty())
				m_lstConnectionsTable.setData(i,1,tr("OK"));

		}

		QApplication::restoreOverrideCursor();

		if (!ui.txtPathEndBat->toPlainText().isEmpty())
		{
			QString strBat=ui.txtPathEndBat->toPlainText();
			/*
			QFileInfo info(strBat);
			if(!info.exists());
			{
			QMessageBox::critical(this,tr("Error"),tr("Failed to find enf batch script!"));
			return;
			}
			*/
#ifdef _WIN32 //put in quotes for windows
			strBat.prepend("\"");
			strBat.append("\"");
#endif
			script.startDetached(strBat);		//let it go...
		}



	ui.tableWidgetBatch->RefreshDisplay();
}



bool DatabaseManager::Connect(QString strConnectionName,QString &strErr)
{
	int nCurrentSelection=m_lstConnectionsTable.find(0,strConnectionName,true);
	if (nCurrentSelection==-1)
	{
		strErr=tr("Error: Can not find connection inside connection list1");
		return false;
	}
	DbConnectionSettings settings=m_lstConnections.at(nCurrentSelection);

	//load actualize dll:
	if(!m_DbActualizator.Initialize(settings)) 
	{
		strErr=tr("Can not load db_actualize.dll!");
		QMessageBox::critical(this,tr("Error"),tr("Can not load db_actualize.dll!"));
		return false;
	}

	return true;

}

bool DatabaseManager::DisConnect(QString &strErr)
{
	//load actualize dll:
	Status pStatus;
	m_DbActualizator.DisConnect(pStatus); 
	if (!pStatus.IsOK())
	{
		strErr=pStatus.getErrorText();
		QMessageBox::critical(this,tr("Error"),pStatus.getErrorText());
		return false;
	}

	return true;
}


//COPY:
void DatabaseManager::on_btnCopyDatabase_clicked()
{

	if (ui.btnConnect->isEnabled())
	{
		QMessageBox::warning(this,QObject::tr("Warning"),QObject::tr("Please, first connect to source database!"));
		return;
	}

	QString strSource=m_lstConnections.at(ui.cboConnection->currentIndex()).m_strDbName;
	QString strTarget=m_lstConnections.at(ui.cboConnectionTarget->currentIndex()).m_strDbName;
	QString strMsg=tr("Copy all data from ")+strSource+tr(" to the ")+strTarget+tr("? Warning: all data in target database will be destroyed. Backup of target database is done before this operation.");

	if (strSource==strTarget)
	{
		QMessageBox::warning(this,QObject::tr("Warning"),QObject::tr("Can not copy database to itself!"));
		return;

	}
	//->connect but check if source is connected:
	if(QMessageBox::question(this,tr("Confirmation"),strMsg,tr("Yes"),tr("No"))) return;


	int nCurrentSelection=ui.cboConnectionTarget->currentIndex();
	DbConnectionSettings settings=m_lstConnections.at(nCurrentSelection);

	Status err;
	QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
	m_DbActualizator.CopyDatabase(err,settings,true);
	QApplication::restoreOverrideCursor();
	if (!err.IsOK())
	{
		QMessageBox::critical(this,tr("Error"),err.getErrorText());
	}
}


void DatabaseManager::on_btnEditSettingsTarget_clicked()
{
	on_btnEditSettings_clicked();
	ui.cboConnectionTarget->setCurrentIndex(ui.cboConnectionTarget->currentIndex());
}

/*
//create ODBC user
void DatabaseManager::on_btnCreateODBC_User_clicked()
{
	Status err;
	m_DbActualizator.CreateReadOnlyUserForODBCAccess(err);
	if (!err.IsOK())
	{
		QMessageBox::critical(this,tr("Error"),err.getErrorText());
	}
	else
	{
		QMessageBox::information(this,tr("Information"),tr("User successfully created!"));
	}
}*/

void DatabaseManager::on_btnConvertOracleData_clicked()
{
	QString strStartDir=QDir::currentPath(); 
	QString strFilter="*.sql";



	//open file dialog:
	QString strFile = QFileDialog::getOpenFileName(
		NULL,
		QT_TR_NOOP("Select database connection settings file"),
		strStartDir,
		strFilter);

	if(!strFile.isEmpty())
	{
		QByteArray buffer;
		QFile filek(strFile);
		if(filek.open(QIODevice::ReadOnly))
		{
			buffer = filek.readAll();
			filek.close();

			//purge REM's
			buffer.replace("REM ","--- ");

			int nSize=buffer.size();
			int nPos=0;
			int nOcc=0;
			do 
			{
				nPos=buffer.indexOf("'",nPos);
				if (nPos>=0)
				{
					int nPos2=buffer.indexOf("'",nPos+1);
					if (nPos2>nPos)
					{
						int nComma=buffer.indexOf(",",nPos);
						if (nComma>nPos && nComma<nPos2)
						{
							//test if numbeR:
							QString str_test(buffer.mid(nPos+1,nPos2-nPos-1));
							bool bOK=false;
							float x = str_test.toDouble(&bOK); 
							if (bOK)
							{
								buffer.replace(nComma,1,".");
								nOcc++;
							}
						}
						nPos=nPos2;
					}
					else
						nPos++;
				}
			} while (nPos>=0);


			QMessageBox::information(this,"Information",QString("Found and replaces %1 occurrences of '[0-9]+,[0-9]+").arg(nOcc));
		}
		

		if(filek.open(QIODevice::WriteOnly))
		{
			filek.write(buffer);
			filek.close();
		}
	}


}

void DatabaseManager::on_btnImportFromOracleSPC_clicked()
{
	QString strStartDir=QDir::currentPath(); 
	QString strFilter="*.sql";

	//open file dialog:
	QString strFile = QFileDialog::getOpenFileName(
		NULL,
		QT_TR_NOOP("Select ORACLE export data file"),
		strStartDir,
		strFilter);

	Status err;
	int nProcessInsert=0;
	int nProcessSEQ=0;
	QApplication::setOverrideCursor(Qt::WaitCursor);
	m_DbActualizator.ImportDataFromOracleExport(err,strFile,nProcessInsert,nProcessSEQ);
	QApplication::restoreOverrideCursor();
	if (!err.IsOK())
	{
		QMessageBox::critical(this,"Error",err.getErrorText());
	}
	else
	{
		QMessageBox::information(this,"Success",QString("All data successfully imported. Executed %1 inserts and %2 sequences updated.").arg(nProcessInsert).arg(nProcessSEQ));
	}
}

void DatabaseManager::on_btnImportFromOmnis_clicked()
{
	//process event then execute:
	QTimer::singleShot(1,this,SLOT(ImportFromOmnisExecute()));
}





void DatabaseManager::on_btnInputOmnisDDL_clicked()
{
	QString strInputDir = QCoreApplication::applicationDirPath();

	//open file dialog:
	QString strFile = QFileDialog::getExistingDirectory(
		NULL,
		QT_TR_NOOP("Select input directory"),
		strInputDir);

	if(!strFile.isEmpty())
	{
		ui.lineInputOmnisDDL->setText(strFile);
	}
}
void DatabaseManager::on_btnOutputFromOmnisDDL_clicked()
{
	QString strInputDir = QCoreApplication::applicationDirPath();

	//open file dialog:
	QString strFile = QFileDialog::getExistingDirectory(
		NULL,
		QT_TR_NOOP("Select output directory"),
		strInputDir);

	if(!strFile.isEmpty())
	{
		ui.lineOutpuFromOmnisDDL->setText(strFile);
	}

}
void DatabaseManager::on_btnConvertDDLFromOmnis_clicked()
{
	Status err;
	QString strInputDir = ui.lineInputOmnisDDL->text();
	QString strOutputDir = ui.lineOutpuFromOmnisDDL->text();

	if(strInputDir.isEmpty() || strOutputDir.isEmpty()){
		QMessageBox::information(this, "Warning", "Select input/output dir");
		return;
	}

	m_DbActualizator.OmnisStartConvert(err,strInputDir,strOutputDir);

	if (!err.IsOK()){
		QMessageBox::critical(this,tr("Error"),err.getErrorText());
	}
	else{
		QMessageBox::information(this,tr("Information"),tr("Conversion Completed!"));
	}

}

void DatabaseManager::TestODBCConnection()
{
	//hardcore: connect on the FB through ODBC and test UTF8 fields from designated table:
	QSqlDatabase *pDb = new QSqlDatabase(QSqlDatabase::addDatabase("QODBC3", "my_odbc_conn"));
	//QSqlDatabase *pDb = new QSqlDatabase(QSqlDatabase::addDatabase("QIBASE", "my_odbc_conn"));
	
	if(pDb)
	{	
		pDb->setDatabaseName("Test2"); //open session
		//pDb->setHostName("localhost"); //open session
		pDb->setUserName("SYSDBA"); //open session
		pDb->setPassword("masterkey"); //open session
		//pDb->setDatabaseName("C:\\DATA.FDB"); //open session
		//pDb->setHostName("Test"); //open session
		if(!pDb->open())
		{
			//fill error string
			QSqlError err = pDb->lastError();
			Status err_1;
			err_1.setError(StatusCodeSet::ERR_SQL_CONNECTION_RESERVATION_FAILED,QVariant(err.number()).toString()+";"+err.text());
			QSqlDatabase::removeDatabase ("my_odbc_conn");	
		}
		else
		{
			QTime x1;
			x1.start();
			
			QString strSQL="SELECT * FROM FASSIGN		where fb_datum > '2011.03.01'";

			//QString strSQL="SELECT * FROM VW_FPROJCT ORDER BY PR_CODE_X ASC, PR_SEQUENCE ASC";
			//QString strSQL="SELECT * FROM FPROJCT ORDER BY PR_CODE ASC, PR_SEQUENCE ASC";
			QSqlQuery query(*pDb);
			if(query.exec(strSQL))
			{
				qDebug()<<x1.elapsed();
				x1.restart();
				//QStringList lst1;
				int x=0;
				while (query.next())
				{
					qDebug()<<x1.elapsed();
					int record_count = query.record().count();
					for (int i=0;i<record_count;i++)
					{
						QVariant value=query.value(i);
						qDebug()<<value;
					}
					/*
					qDebug()<<query.value(0).toString();
					lst1.append(query.value(0).toString());
					qDebug()<<lst1.value(lst1.size()-1);
					*/

					x++;
					if (x==2)
						break;
					x1.restart();
					x1.start();
				}
			}


			/*
			//QString strSQL="SELECT FP_NACHNAME FROM FPERSON";
			QString strSQL="SELECT FAB_NAME FROM VWS_F_Abteilung";
			 
			QSqlQuery query(*pDb);
			if(query.exec(strSQL))
			{
				QStringList lst1;
				while (query.next())
				{
					qDebug()<<query.value(0).toString();
					lst1.append(query.value(0).toString());
					qDebug()<<lst1.value(lst1.size()-1);
				}
			}
			*/

		}
	}

}


void DatabaseManager::ImportFromOmnisExecute()
{

		/*
	QFile file("C:\\temp_out.file");
	if(!file.open(QIODevice::ReadOnly))
		return;
	QByteArray buffer = file.readAll();
	file.close();

	QByteArray out=QByteArray::fromBase64(buffer);
	qDebug()<<out;
	*/


	//TestODBCConnection();

	QString strStartDir=QDir::currentPath(); 
	if (!g_INIFile.m_strLastDirPath.isEmpty())
		strStartDir=g_INIFile.m_strLastDirPath;

	//open file dialog:
	QString strDir = QFileDialog::getExistingDirectory(
		NULL,
		QT_TR_NOOP("Select input directory"),
		strStartDir);

	if (!strDir.isEmpty())
	{
		g_INIFile.m_strLastDirPath = strDir;

		Status err;
		int nProcessInsert=0;
		//QApplication::setOverrideCursor(Qt::WaitCursor);
		//m_DbActualizator.ImportDataFromOmnisExport(err,strDir,nProcessInsert, ui.ckbSPCImportDropExisting->isChecked());
		
		/*
		QFile status_file(QCoreApplication::applicationDirPath()+"/status.log");
		QString strStatus =QString("\nDatabaseManager::on_btnImportFromOmnis_clicked - after ImportDataFromOmnisExport");
		if(status_file.open(QIODevice::Append))
		{
			status_file.write(strStatus.toAscii());
			status_file.close();
		}
		*/


		//QApplication::restoreOverrideCursor();

		
		ImporterController *importControl = new ImporterController();
		bool bUserCancel=false;
		int nCurrentSelection=ui.cboConnection->currentIndex();
		if (nCurrentSelection==-1)
			return;
		DbConnectionSettings settings=m_lstConnections.at(nCurrentSelection);

		importControl->RunImport(err,bUserCancel,settings,m_strBackupDirPath,strDir,ui.ckbSPCImportDropExisting->isChecked());
		if (!bUserCancel)
		{
			if (!err.IsOK())
			{
				QMessageBox::critical(this,"Error",err.getErrorText());
			}
			else
			{
				//QMessageBox::information(this,"Success",QString("All data successfully imported. Executed %1 inserts.").arg(nProcessInsert));
				QMessageBox::information(this,"Success",QString("All data successfully imported."));
			}
		}
		

/*
		strStatus =QString("\nDatabaseManager::on_btnImportFromOmnis_clicked - after QMessageBox");
		if(status_file.open(QIODevice::Append))
		{
			status_file.write(strStatus.toAscii());
			status_file.close();
		}
*/
	}

}





/*
	IMPORTER object for importing in separate thread:
	Communicate via status.log file:
*/


Importer::Importer( ThreadSynchronizer *sync,int nTimeOut/*=10000*/ )
{
	m_ThreadSync=sync;
	m_nStatusCode=0;
	m_strStatusText="";
	m_bCancel=false;
}

bool Importer::Initialize(DbConnectionSettings &settings, QString &strBcpPath)
{
	//load actualize dll:
	if(!m_DbActualizator.Initialize(settings)) 
	{
		QMessageBox::critical(NULL,tr("Error"),tr("Can not load db_actualize.dll!"));
		return false;
	}
	//bring up connection:
	Status err;
	m_DbActualizator.InitConnection(err,strBcpPath,false);
	if (!err.IsOK())
	{
		QMessageBox::critical(NULL,tr("Error"),err.getErrorText());
		return false;
	}

	return true;
}

void Importer::Close()
{
	Status err;
	m_DbActualizator.DisConnect(err);
}

void Importer::CancelOperation()
{
	QWriteLocker lock(&m_RWLock);
	m_bCancel=true;
}

bool Importer::IsCanceled()
{
	QReadLocker lock(&m_RWLock);
	return m_bCancel;
}


void Importer::ExecuteImportFromOmnis( QString strInputDir, bool bDropExistingDB)
{
	int nProcessInsert=0;
	Status err;
	m_DbActualizator.ImportDataFromOmnisExport(err,strInputDir,nProcessInsert, bDropExistingDB);
	if (!err.IsOK())
	{
		m_nStatusCode=err.getErrorCode();
		m_strStatusText=err.getErrorText();
	}
	m_ThreadSync->ThreadRelease();
}

void Importer::Destroy()
{
	Close();
	deleteLater();
	m_ThreadSync->ThreadRelease();
}







ImporterController::ImporterController(QString strLogPath)
{
	m_strLogPath=strLogPath;
	m_DlgProgress = NULL;

	m_Importer=NULL;
	m_ThreadSync.ThreadSetForWait();	//prepare for wait
	start(); //start thread
	m_ThreadSync.ThreadWait();			//prepare for start

	m_nTotalRec=0;
	m_nTableCurrentRec=0;
	m_nTableTotalRec=0;
	m_nTableCnt=0;
	m_nTableCurrIdx=0;

}

ImporterController::~ImporterController()
{
	m_ThreadSync.ThreadSetForWait();	//prepare for wait
	emit Destroy();
	m_ThreadSync.ThreadWait();			//prepare for start

	//stop thread gently:
	quit();
	wait();

	if (m_DlgProgress)
		m_DlgProgress->deleteLater();
}

void ImporterController::run()
{
	m_Importer = new Importer(&m_ThreadSync);

	connect(this,SIGNAL(ExecuteImportFromOmnis(QString , bool )),m_Importer,SLOT(ExecuteImportFromOmnis(QString , bool )));
	connect(this,SIGNAL(Destroy()),m_Importer,SLOT(Destroy()));

	m_ThreadSync.ThreadRelease();
	exec();
}

void ImporterController::RunImport(Status &err,bool &bUserCanceled, DbConnectionSettings &settings, QString &strBcpPath,QString strInputDir, bool bDropExistingDB)
{
	bUserCanceled=false;
	//create dlg id not already:
	if (!m_DlgProgress)
	{
		m_DlgProgress = new DlgImportProgress();
		connect(m_DlgProgress,SIGNAL(CancelTransfer()),this,SLOT(OnUserCancelTransfer()));
	}

	//init importer:
	if(!m_Importer->Initialize(settings,strBcpPath))
	{
		err.setError(1,"Failed to initialize database!");
		return;
	}

	//execute import:
	//m_DlgProgress->Start(nSize>1?true:false,nTotalBytes,true);
	//m_DlgProgress->SetFileName(tr("Uploading: ")+lstDocFiles.at(i).m_strFileName,lstDocFiles.at(i).m_nSize); //prepare progress

	m_strStep="";
	bool bDialogHidden=true;
	
	//erase log:
	QFile::remove(QCoreApplication::applicationDirPath()+"/status.log");
	//erase log:

	m_ThreadSync.ThreadSetForWait();
	emit ExecuteImportFromOmnis(strInputDir,bDropExistingDB);			
	//transfer in progress: two outcomes: user cancel or normal exit
	while (!m_ThreadSync.ThreadWaitTimeOut(400))
	{
		qApp->processEvents(QEventLoop::AllEvents);  //this will give time for GUI refresh/signals and user chance to abort

		//check if cancel, if one is canceled, then cancel all:
		if (m_Importer->IsCanceled())
		{
			m_Importer->Close();
			m_DlgProgress->Stop();
			bUserCanceled=true;
			return; 
		}

		//parse progress:
		ParseLog();
		if (m_nTableCnt>0)
		{
			if ((!m_strStep.isEmpty() && bDialogHidden)) //if table cnt is still empty then we are still on 1st step
			{
				bDialogHidden=false;
				m_DlgProgress->Start(m_nTotalRec,m_nTableCnt);
			}
			else
				m_DlgProgress->UpdateProgress(m_strStep,m_nTotalCurrentRec,m_nTableCurrIdx,m_nTableCurrentRec,m_nTableTotalRec);
		}
	}
	
	m_Importer->Close();

	//check for error, if one is errored, cancel all:
	if (m_Importer->GetStatusCode())
	{
		m_DlgProgress->Stop();
		err.setError(m_Importer->GetStatusCode(),m_Importer->GetStatusText());
		return;
	}


	m_DlgProgress->Stop();
}


void ImporterController::ParseLog()
{
	QFile status_file(m_strLogPath);
	if(status_file.open(QIODevice::ReadOnly))
	{
		QString strBuff=status_file.readAll();
		if (strBuff.size()>0)
		{
			QStringList lstLines=strBuff.split("\n",QString::SkipEmptyParts);
			if (lstLines.size()==6)
			{
				m_nTotalRec=lstLines.at(0).toULong();
				m_nTableCnt=lstLines.at(1).toInt();
				m_strStep=lstLines.at(2);
				m_nTableCurrIdx=lstLines.at(3).toInt();
				m_nTotalCurrentRec=lstLines.at(4).toInt();
				QString strCurrProgress=lstLines.at(5);
				if (strCurrProgress.indexOf("/")>0)
				{
					m_nTableCurrentRec=strCurrProgress.left(strCurrProgress.indexOf("/")).toInt();
					m_nTableTotalRec=strCurrProgress.mid(strCurrProgress.indexOf("/")+1).toInt();
				}
			}
		}
	}
}

void ImporterController::OnUserCancelTransfer()
{
	m_Importer->CancelOperation();
}