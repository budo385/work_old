#include "clientinifile.h"

ClientIniFile::ClientIniFile()
{
}

ClientIniFile::~ClientIniFile()
{
}

void ClientIniFile::Clear()
{
	//m_lstConnections.clear();
}

bool ClientIniFile::Load(QString strFile, bool bCreateIfNotExists)
{
	Clear();

	if(!QFileInfo(strFile).exists()&& bCreateIfNotExists)
	{return SaveDefaults(strFile);}

	m_objIni.SetPath(strFile);
	if(!m_objIni.Load())
		return false;

	// read INI values
	m_objIni.GetValue("DbOperations", "DbBackupPath",		m_strBackupDirPath);
	m_objIni.GetValue("DbOperations", "DbSettingsPath",		m_strDbConnectionsettingsPath);
	m_objIni.GetValue("DbOperations", "LastDbSession",		m_strLastConnectionName);
	m_objIni.GetValue("BatchDbOperations", "DbSettingsPath",		m_strBatch_DbConnectionsettingsPath);
	m_objIni.GetValue("BatchDbOperations", "StartBatchScript",		m_strBatch_StartScript);
	m_objIni.GetValue("BatchDbOperations", "EndBatchScript",		m_strBatch_EndScript);
	m_objIni.GetValue("Common", "LastDirPath",	m_strLastDirPath);
		
	return true;
}

bool ClientIniFile::Save(QString strFile)
{
	m_objIni.SetPath(strFile);

	//fill the data inside
	m_objIni.RemoveSection("DbOperations");	//cleanup existing data

	// write INI values
	m_objIni.SetValue("DbOperations", "DbBackupPath",		m_strBackupDirPath);
	m_objIni.SetValue("DbOperations", "DbSettingsPath",		m_strDbConnectionsettingsPath);
	m_objIni.SetValue("DbOperations", "LastDbSession",		m_strLastConnectionName);
	m_objIni.SetValue("BatchDbOperations", "DbSettingsPath",		m_strBatch_DbConnectionsettingsPath);
	m_objIni.SetValue("BatchDbOperations", "StartBatchScript",		m_strBatch_StartScript);
	m_objIni.SetValue("BatchDbOperations", "EndBatchScript",		m_strBatch_EndScript);
	m_objIni.SetValue("Common", "LastDirPath",	m_strLastDirPath);

	return m_objIni.Save();
}




bool ClientIniFile::SaveDefaults(QString strFile)
{
	m_strBackupDirPath="";
	m_strDbConnectionsettingsPath="";
	m_strLastConnectionName="";
	m_strBatch_DbConnectionsettingsPath="";
	m_strBatch_StartScript="";
	m_strBatch_EndScript="";
	m_strLastDirPath="";

	return Save(strFile);
}
