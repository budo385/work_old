#ifndef DATABASEMANAGER_H
#define DATABASEMANAGER_H

#include <QWidget>
#include "ui_databasemanager.h"
#include "bus_core/bus_core/dbactualizator.h"
#include "db_core/db_core/dbconnsettings.h"
#include "common/common/threadsync.h"
#include "dlgimportprogress.h"


class DatabaseManager : public QWidget
{
	Q_OBJECT

public:
	DatabaseManager(QWidget *parent = 0);
	~DatabaseManager();

	void SaveState();

private slots:
	void on_btnConnect_clicked();
	void on_btnDisconnect_clicked();
	void on_btnSelectBackup_clicked();
	void on_btnSelectSettings_clicked();
	void on_btnEditSettings_clicked();
	void on_btnBackup_clicked();
	void on_btnRestore_clicked();
	void on_btnDbOrganize_clicked();
	void on_btnReloadBackups_clicked();
	//void on_btnCreateODBC_User_clicked();
	void on_btnConvertOracleData_clicked();
	void on_btnImportFromOracleSPC_clicked();
	void on_btnImportFromOmnis_clicked();

	//pm: 2.4.2012
    void on_btnInputOmnisDDL_clicked();
	void on_btnOutputFromOmnisDDL_clicked();
    void on_btnConvertDDLFromOmnis_clicked();

	//BATCH:
	void on_btnSelectStart_clicked();
	void on_btnSelectEnd_clicked();
	void on_btnDbOrganizeBatch_clicked();
	void on_btnLoad_clicked();

	//COPY:
	void on_btnCopyDatabase_clicked();
	void on_btnEditSettingsTarget_clicked();

	//SILENT EXECUTE:
	void ImportFromOmnisExecute();
	
	

private:
	Ui::DatabaseManagerClass ui;
	void SetConnected(DbConnectionSettings connection);
	void ClearConnected();
	void ReloadSettings();
	void ReloadBackups();
	int GetDbVersion();
	void TestODBCConnection();

	//BATCH:
	bool Connect(QString strConnectionName,QString &strErr);
	bool DisConnect(QString &strErr);


	QString m_strDbConnectionsettingsPath;
	QString m_strBackupDirPath;
	DbRecordSet m_lstBackups;
	QList<DbConnectionSettings> m_lstConnections;
	int m_nDbVersion;
	int m_nDbVersionSPC;
	DbRecordSet m_lstConnectionsTable;

	DbActualizator m_DbActualizator;

	
};

//main class that will read log, show progress...
class Importer;
class ImporterController : public QThread
{
	Q_OBJECT

public:
	ImporterController(QString strLogPath=QCoreApplication::applicationDirPath()+"/status.log");
	~ImporterController();
	
	void RunImport(Status &err,bool &bUserCanceled, DbConnectionSettings &settings, QString &strBcpPath,QString strInputDir, bool bDropExistingDB);

public slots:
	void OnUserCancelTransfer();

signals:
	void ExecuteImportFromOmnis(QString strInputDir, bool bDropExistingDB);
	void Destroy();

private:
	void run();
	void ParseLog();

	DlgImportProgress	*m_DlgProgress;
	ThreadSynchronizer	m_ThreadSync;
	Importer			*m_Importer;

	//current status:
	QString m_strLogPath;
	quint64 m_nTotalRec;
	quint64 m_nTotalCurrentRec;
	QString m_strStep;
	int m_nTableCurrentRec;
	int m_nTableTotalRec;

	int m_nTableCnt;
	int m_nTableCurrIdx;

};


class Importer : public QObject
{
	Q_OBJECT

public:

	Importer(ThreadSynchronizer *sync,int nTimeOut=10000);
	bool	Initialize(DbConnectionSettings &settings, QString &strBcpPath);
	void	Close();
	void	CancelOperation();
	bool	IsCanceled();
	int		GetStatusCode(){return m_nStatusCode;};
	QString	GetStatusText(){return m_strStatusText;};

public slots:
	void ExecuteImportFromOmnis(QString strInputDir, bool bDropExistingDB);
	void Destroy();

private:
	DbActualizator		m_DbActualizator;
	int					m_nStatusCode;
	QString				m_strStatusText;
	bool				m_bCancel;
	QReadWriteLock		m_RWLock;
	ThreadSynchronizer	*m_ThreadSync;
};




#endif // DATABASEMANAGER_H
