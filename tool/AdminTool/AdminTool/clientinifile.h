#ifndef SPCINIFILE_H
#define SPCINIFILE_H

#include "common/common/inifile.h"
#include <QFileInfo>


/*!
	\class ClientIniFile
	\brief INI file 

	Client INI file

*/
class ClientIniFile
{
public:
	ClientIniFile();
	~ClientIniFile();


	void Clear();
	bool Load(QString strFile, bool bCreateIfNotExists=true);
	bool Save(QString strFile);

public:

	QString m_strDbConnectionsettingsPath;
	QString m_strBackupDirPath;
	QString m_strLastConnectionName;

	QString m_strBatch_DbConnectionsettingsPath;
	QString m_strBatch_StartScript;
	QString m_strBatch_EndScript;

	QString m_strLastDirPath;


protected:
	bool SaveDefaults(QString strFile);
	IniFile m_objIni;
};

#endif //SERVERINIFILE_H