#ifndef RPCGENERATOR_H
#define RPCGENERATOR_H

#include <QMainWindow>
#include "ui_rpcgenerator.h"


// save a parameters type & name (ex: int number;)
class Param
{
public:
	QString strName;
	QString strType;

	Param(){}
	Param(const Param &that){   //copy constructor
		operator =(that);
	}
	void operator =(const Param &that){
		strName = that.strName;
		strType = that.strType;
	}
};

// save a method
class Method
{
public:
	QString strName;
	QString strParams;
	QString strParamsDef;	//with "= NULL" stripped
	QString strReturn;		// methods return type
	int nAccess;
	QList<Param> lstParams; // methods can have a list of Parameters

	Method(){}
	Method(const Method &that){
		operator =(that);
	}
	void operator =(const Method &that){
		strName = that.strName;
		strParams    = that.strParams;
		strParamsDef = that.strParamsDef;
		strReturn = that.strReturn;
		nAccess = that.nAccess;
		lstParams = that.lstParams;
	}
};

// save rest meta data
class ReturnParameter
{
public:
	QString strName;
	QString strDisplayName;
	QString strRowName;
	QString strRowURLAttr;
	QString strRowURLAttr_IDField;
	QString strListDefintion; //<DEFINE_LIST> contain list defintion for incoming lists
};


// save rest meta data
class MetaData
{
public:
	QString strFunctionName;
	QString strUrlName;
	QString strHttpMethodName;
	QString strRequestXmlSchema;
	QString strResponseXmlSchema;
	QString strResourceIDParam;
	QString strResourceParentIDParam;
	QList<ReturnParameter> lstReturnParametarData;
};



class RpcGenerator : public QMainWindow
{
    Q_OBJECT

public:
    RpcGenerator(QWidget *parent = 0, Qt::WindowFlags flags = 0); //contructor
    ~RpcGenerator();	//destructor

	bool LoadFile(QString strInputFile, QList<QString> &lstInput); 
	void FilterInput(QList<QString> &lstInput); 

private:
    Ui::RpcGeneratorClass ui;

	void		GenerateOne(QString strInputFile, QString strOutputDir,bool bSkipInteract=false,QString strForceBaseName="");
	bool		GenerateOne_Rest(QString strInputFile, QString strOutputDir,bool bSkipInteract=false,QString strForceBaseName="",bool bGenerateForSingleServiceHandler=false);
	bool		ParseMetaData(QString strInputFile, QList<MetaData> &lstMetaData, QString &strNameSpace);
	bool		GetReturnParameterData(QList<MetaData> &MetaData, QString strFunction, QString strParameterName,ReturnParameter &data);
	MetaData	GetFunctionMetaData(QList<MetaData> &lstMetaData, QString strFunction);


private slots:
	void on_pushGenerate_clicked();
	void on_pushPickOutput_clicked();
	void on_pushPickInput_clicked();
	void on_btnGenerateAll_clicked();

	void on_pushGenerate_Rest_clicked();
	void on_btnGenerateAll_Rest_clicked();

	void on_pushGenerate_Rest_Simple_clicked();
	void on_btnGenerateAll_Rest_Simple_clicked();

};

#endif // RPCGENERATOR_H


/*
	on_pushGenerate_Rest_clicked() 
*/

