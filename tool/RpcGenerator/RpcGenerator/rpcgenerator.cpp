#include "rpcgenerator.h"
#include <QFileDialog>
#include <QMessageBox>
#include <algorithm>	//max
#include <QInputDialog>
#include <QDebug>
#include <QByteArray>
#include <QDomDocument>
#include <QString>

#define ACCESS_PRIVATE		1
#define ACCESS_PUBLIC		2
#define ACCESS_PROTECTED	3


// the constructor
RpcGenerator::RpcGenerator(QWidget *parent, Qt::WindowFlags flags)
    : QMainWindow(parent, flags)
{
	ui.setupUi(this); //initialize Gui


// debug mode detection
//obsolete: interface_testbusinessobject.h
#ifdef _DEBUG
	ui.lineInputHdr->setText("E:/RPC_TEST_ENV/in/interface_servicehandler.h"); // populate with semi-dummy text :)
#endif

	ui.lineOutputDir->setText("D:");
}

// destructor
RpcGenerator::~RpcGenerator()
{
}

// event fired when choosing input file
void RpcGenerator::on_pushPickInput_clicked()
{
	QString	strFile = QFileDialog::getOpenFileName(
                    NULL,
                    "Choose a file",
                    "/",
                    "C++ header file (*.h)");

	if(!strFile.isEmpty())
		ui.lineInputHdr->setText(strFile);
}

// event fired for choosing output directory
void RpcGenerator::on_pushPickOutput_clicked()
{
	QString	strFile = QFileDialog::getExistingDirectory(
                    NULL,
                    "Choose a directoy",
                    "/",
                    QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);

	if(!strFile.isEmpty()){
		//ensure dir is teminated
		if( strFile.right(1) != "\\" && 
			strFile.right(1) != "/")	
			strFile += "/";

		ui.lineOutputDir->setText(strFile);
	}

}

// SOKRATES RPC GENERATOR FOR SELECTED INPUT FILE (INTERFACE)
void RpcGenerator::on_pushGenerate_clicked()
{
	//
	// STEP 1: check input data
	//
	QString strInputFile = ui.lineInputHdr->text();
	QString strOutputDir = ui.lineOutputDir->text();
	if(strInputFile.isEmpty() || strOutputDir.isEmpty()){
		QMessageBox::information(this, "Warning", "Please define input/output parameters!");
		return;
	}

	//ensure dir is teminated
	if( strOutputDir.right(1) != "\\" && 
		strOutputDir.right(1) != "/")	
		strOutputDir += "/";

	GenerateOne(strInputFile,strOutputDir);
	QMessageBox::information(this, "Info", "Done!");
}


// REST RPC GENERATOR FOR SELECTED INPUT FILE (INTERFACE) (ex: webconnect, everpics,..)
void RpcGenerator::on_pushGenerate_Rest_clicked()
{
	//
	// STEP 1: check input data
	//
	QString strInputFile = ui.lineInputHdr->text();
	QString strOutputDir = ui.lineOutputDir->text();
	if(strInputFile.isEmpty() || strOutputDir.isEmpty()){
		QMessageBox::information(this, "Warning", "Please define input/output parameters!");
		return;
	}

	//ensure dir is teminated
	if( strOutputDir.right(1) != "\\" && 
		strOutputDir.right(1) != "/")	
		strOutputDir += "/";

	if (GenerateOne_Rest(strInputFile,strOutputDir))
		QMessageBox::information(this, "Info", "Done!");
}

// REST RPC GENERATOR FOR SELECTED INPUT FILE (INTERFACE) (ex: webconnect, everpics,..)
void RpcGenerator::on_pushGenerate_Rest_Simple_clicked()
{
	QString strInputFile = ui.lineInputHdr->text();
	QString strOutputDir = ui.lineOutputDir->text();
	if(strInputFile.isEmpty() || strOutputDir.isEmpty()){
		QMessageBox::information(this, "Warning", "Please define input/output parameters!");
		return;
	}

	//ensure dir is teminated
	if( strOutputDir.right(1) != "\\" && 
		strOutputDir.right(1) != "/")	
		strOutputDir += "/";

	if (GenerateOne_Rest(strInputFile,strOutputDir,false,"",true))
		QMessageBox::information(this, "Info", "Done!");


}


void RpcGenerator::on_btnGenerateAll_clicked()
{

	QString	strOutputDir = QFileDialog::getExistingDirectory(
		NULL,
		"Choose a directoy",
		"/",
		QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);

	if(!strOutputDir.isEmpty())
	{

		//ensure dir is teminated
		if( strOutputDir.right(1) != "\\" && 
			strOutputDir.right(1) != "/")	
			strOutputDir += "/";

		//get all files:
		QDir dir(strOutputDir);
		if (!dir.exists())
		{
			QMessageBox::information(this, "Error", "Directory does not exists!");
			return;
		}
		//Algorithm finds first free slot: it will iterate 9999 times (max) before exiting...
		//QFileInfo info(strDirPath+"/"+strNewDirectory);
		QFileInfoList	lstFiles=dir.entryInfoList();
		int nSize=lstFiles.size();
		int nCnt=0;
		for(int i=0;i<nSize;++i)
		{
			if (lstFiles.at(i).isFile())
			{
				QString fileName=lstFiles.at(i).fileName();
				if (fileName.indexOf("interface_")==0)
				{
					qDebug()<<fileName;
					if (fileName=="interface_businessserviceset.h")
						continue;
					if (fileName=="interface_collection.h")
						continue;
					if (fileName=="interface_userlogon.h")
						continue;
					if (fileName=="interface_boentity.h") //special: generate predefined:
					{
						GenerateOne(lstFiles.at(i).absoluteFilePath(),strOutputDir,true,"BoEntity_BusContact");nCnt++;
						GenerateOne(lstFiles.at(i).absoluteFilePath(),strOutputDir,true,"BoEntity_BusCostCenter");nCnt++;
						GenerateOne(lstFiles.at(i).absoluteFilePath(),strOutputDir,true,"BoEntity_BusDepartment");nCnt++;
						GenerateOne(lstFiles.at(i).absoluteFilePath(),strOutputDir,true,"BoEntity_BusOrganization");nCnt++;
						GenerateOne(lstFiles.at(i).absoluteFilePath(),strOutputDir,true,"BoEntity_BusPerson");nCnt++;
						GenerateOne(lstFiles.at(i).absoluteFilePath(),strOutputDir,true,"BoEntity_BusProject");nCnt++;
						GenerateOne(lstFiles.at(i).absoluteFilePath(),strOutputDir,true,"BoEntity_CeEventTypes");nCnt++;
						GenerateOne(lstFiles.at(i).absoluteFilePath(),strOutputDir,true,"BoEntity_ContactType");nCnt++;
						GenerateOne(lstFiles.at(i).absoluteFilePath(),strOutputDir,true,"BoEntity_CeTypes");nCnt++;
						GenerateOne(lstFiles.at(i).absoluteFilePath(),strOutputDir,true,"BoEntity_CoreUser");nCnt++;
						GenerateOne(lstFiles.at(i).absoluteFilePath(),strOutputDir,true,"BoEntity_TestTree");nCnt++;
					}
					else
					{
						GenerateOne(lstFiles.at(i).absoluteFilePath(),strOutputDir,true);
						nCnt++;
					}
				}
			}
		}

		QFile::remove(strOutputDir+"rpcstub_userlogon.h");
		QFile::remove(strOutputDir+"rpcstub_userlogon.cpp");

		QMessageBox::information(this, "Done", "Processed: "+QVariant(nCnt).toString());
		//ui.lineOutputDir->setText(strFile);
	}

}

void RpcGenerator::on_btnGenerateAll_Rest_Simple_clicked()
{

	QString	strOutputDir = QFileDialog::getExistingDirectory(
		NULL,
		"Choose a directory",
		"/",
		QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);

	if(!strOutputDir.isEmpty())
	{

		//ensure dir is terminated
		if( strOutputDir.right(1) != "\\" && 
			strOutputDir.right(1) != "/")	
			strOutputDir += "/";

		//get all files:
		QDir dir(strOutputDir);
		if (!dir.exists())
		{
			QMessageBox::information(this, "Error", "Directory does not exists!");
			return;
		}
		//Algorithm finds first free slot: it will iterate 9999 times (max) before exiting...
		//QFileInfo info(strDirPath+"/"+strNewDirectory);
		QFileInfoList	lstFiles=dir.entryInfoList();
		int nSize=lstFiles.size();
		int nCnt=0;
		for(int i=0;i<nSize;++i)
		{
			if (lstFiles.at(i).isFile())
			{
				QString fileName=lstFiles.at(i).fileName();
				if (fileName.indexOf("interface_web")==0)
				{
					qDebug()<<fileName;
					//if (fileName=="interface_webserver.h")
					//	continue;

					if(!GenerateOne_Rest(lstFiles.at(i).absoluteFilePath(),strOutputDir,true,"",true))
					{
						QMessageBox::critical(this,"Error","Error occurred while parsing file: "+fileName);

					}
					nCnt++;
				}
			}
		}

		QMessageBox::information(this, "Done", "Processed: "+QVariant(nCnt).toString());
	}
}



void RpcGenerator::on_btnGenerateAll_Rest_clicked()
{

	QString	strOutputDir = QFileDialog::getExistingDirectory(
		NULL,
		"Choose a directory",
		"/",
		QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);

	if(!strOutputDir.isEmpty())
	{

		//ensure dir is terminated
		if( strOutputDir.right(1) != "\\" && 
			strOutputDir.right(1) != "/")	
			strOutputDir += "/";

		//get all files:
		QDir dir(strOutputDir);
		if (!dir.exists())
		{
			QMessageBox::information(this, "Error", "Directory does not exists!");
			return;
		}
		//Algorithm finds first free slot: it will iterate 9999 times (max) before exiting...
		//QFileInfo info(strDirPath+"/"+strNewDirectory);
		QFileInfoList	lstFiles=dir.entryInfoList();
		int nSize=lstFiles.size();
		int nCnt=0;
		for(int i=0;i<nSize;++i)
		{
			if (lstFiles.at(i).isFile())
			{
				QString fileName=lstFiles.at(i).fileName();
				if (fileName.indexOf("interface_web")==0)
				{
					qDebug()<<fileName;
					//if (fileName=="interface_webserver.h")
					//	continue;

					if(!GenerateOne_Rest(lstFiles.at(i).absoluteFilePath(),strOutputDir,true))
					{
						QMessageBox::critical(this,"Error","Error occurred while parsing file: "+fileName);

					}
					nCnt++;
				}
			}
		}

		QMessageBox::information(this, "Done", "Processed: "+QVariant(nCnt).toString());
	}
}

void RpcGenerator::GenerateOne(QString strInputFile, QString strOutputDir,bool bSkipInteract,QString strForceBaseName)
{

	//
	// STEP 2: parse input file (class name, method names, method parameters)
	//
	QString strClassName;
	QList<Method> lstMethods;
	QList<QString> lstInput;

	if(!RpcGenerator::LoadFile(strInputFile, lstInput))
		return;
	
	// trim lines, cut comments, etc,..
	FilterInput(lstInput);

	//parse remaining contents
	int nAccess = ACCESS_PRIVATE;	//default access right
	int nIdx = 0;

	while(nIdx < lstInput.size())
	{
		if(lstInput[nIdx].indexOf("class") == 0)
		{
			strClassName = lstInput[nIdx].right(lstInput[nIdx].size()-5).trimmed();
		}
		else if(lstInput[nIdx].indexOf("public:") >= 0)
		{
			nAccess = ACCESS_PUBLIC;
		}
		else if(lstInput[nIdx].indexOf("private:") >= 0)
		{
			nAccess = ACCESS_PRIVATE;
		}
		else if(lstInput[nIdx].indexOf("protected:") >= 0)
		{
			nAccess = ACCESS_PROTECTED;
		}
		else
		{
			//extract method name
			int nParamsPos = lstInput[nIdx].indexOf("(");
			if(nParamsPos < 0){
				QString strMsg = QString("Invalid line (no '(' character): %1!").arg(lstInput[nIdx]);
				QMessageBox::information(this, "Error", strMsg);
				return;
			}

			QString strLeft = lstInput[nIdx].left(nParamsPos).trimmed();

			//calculate method name
			QString strName = strLeft;
			int nPos = strName.lastIndexOf(" ");
			if(nPos >= 0)
				strName = strName.right(strName.size()-nPos-1);
			nPos = strName.lastIndexOf("\t");
			if(nPos >= 0)
				strName = strName.right(strName.size()-nPos-1);

			//filter out constructor and destructor methods
			if( strName == strClassName || 
				strName.right(strName.size()-1) == strClassName)
			{
				nIdx ++;			
				continue;
			}

			//calculate return type
			QString strRet = strLeft;
			nPos = strRet.lastIndexOf(" ");
			if(nPos >= 0)
				strRet = strRet.left(nPos);
			//remove "virtual" keyword (not needed for the derived classes)
			nPos = strRet.indexOf("virtual");
			if(nPos >= 0)
				strRet = strRet.left(nPos) + strRet.right(strRet.size()-nPos-7);
			strRet = strRet.trimmed();

			//calculate method params
			QString strParm = lstInput[nIdx].right(lstInput[nIdx].size()-nParamsPos-1);
			int nParamsEndPos = strParm.lastIndexOf(")");
			if(nParamsEndPos < 0){
				QString strMsg = QString("Invalid line (no ')' character): %1!").arg(lstInput[nIdx]);
				QMessageBox::information(this, "Error", strMsg);
				return;
			}
			strParm = strParm.left(nParamsEndPos);

			//remove all default values (for example "= NULL")
			QString strParm1 = strParm;
			nPos = strParm1.indexOf("=");
			while(nPos>0){
				int nEnd = strParm1.indexOf(",", nPos);
				if(nEnd > 0)
					strParm1 = strParm1.left(nPos) + strParm1.right(strParm1.size()-nEnd);
				else
					strParm1 = strParm1.left(nPos);
				nPos = strParm1.indexOf("=");
			}

			//parse the params into the list
			QString strTmp = strParm1;
			QList<QString> lstStrParams;
			nPos = strTmp.indexOf(",");
			while(nPos >= 0){
				QString strPart = strTmp.left(nPos);
				lstStrParams.append(strPart.trimmed());
				//proceed forward
				strTmp = strTmp.right(strTmp.size()-nPos-1);
				nPos = strTmp.indexOf(",");
			}
			if(!strTmp.isEmpty())
				lstStrParams.append(strTmp.trimmed());
			//break parts into the type+name list
			QList<Param> lstParams;
			for(int i=0; i<lstStrParams.size(); i++)
			{
				Param param;
				int nPos1 = lstStrParams[i].lastIndexOf(" ");
				int nPos2 = lstStrParams[i].lastIndexOf("\t");
				int nPos  = std::max(nPos1, nPos2);
				if(nPos >= 0)
				{
					while( lstStrParams[i].at(nPos+1) == '&' ||
						lstStrParams[i].at(nPos+1) == '*')
						nPos ++;

					param.strName = lstStrParams[i].right(lstStrParams[i].size()-nPos-1);
					param.strType = lstStrParams[i].left(nPos+1);
					lstParams.append(param);
				}
			}

			Method method;
			method.nAccess   = nAccess;
			method.strName   = strName;
			method.strParams = strParm;
			method.strParamsDef = strParm1;	//params without initial values
			method.strReturn = strRet;
			method.lstParams = lstParams;
			lstMethods.append(method);
		}

		nIdx ++;
	}

	//
	// STEP 3: generate output files
	//

	//debug code
#if 0
#ifdef _DEBUG
	FILE *pOut = fopen("D:\\a.txt", "w");
	if(pOut){
		for(int i=0; i<lstInput.size(); i++)
			fprintf(pOut, "%s\n", lstInput[i].toLatin1().constData());
		fclose(pOut);
	}
	FILE *pOut1 = fopen("D:\\b.txt", "w");
	if(pOut1){
		fprintf(pOut1, "%s\n", strClassName.toLatin1().constData());
		for(int i=0; i<lstMethods.size(); i++)
			fprintf(pOut1, "\t%s\t%s(%s)\n", 
			lstMethods[i].strReturn.toLatin1().constData(),
			lstMethods[i].strName.toLatin1().constData(),
			lstMethods[i].strParams.toLatin1().constData());
		fclose(pOut1);
	}
#endif
#endif

	// calc class name without the prefix
	QString strBaseName = strClassName;
	if(0 == strBaseName.indexOf("Interface_"))
		strBaseName = strBaseName.right(strBaseName.size()-10);




	//Ask user to use same base name:
	bool ok;
	if (!bSkipInteract)
	{
		QString text = QInputDialog::getText(this, "Enter base class name",
			"Base Class Name (Modify if different, leave as is if OK):", QLineEdit::Normal,
			strBaseName, &ok);
		if (ok && !text.isEmpty())
			strBaseName=text;
	}
	//QFileInfo file(strInputFile);
	QString strOriginalName=strBaseName;
	if (!strForceBaseName.isEmpty())
		strBaseName=strForceBaseName;


	qDebug()<<strBaseName;

	//
	// generate rpcstub HEADER file
	//
	QString strOutFile = strOutputDir;
	strOutFile += "rpcstub_";
	strOutFile += strBaseName.toLower();
	strOutFile += ".h";

	FILE *pOutStubHdr = fopen(strOutFile.toLatin1().constData(), "w");
	if(pOutStubHdr)
	{
		QString strDefine = "rpcstub_";
		strDefine += strBaseName.toLower();
		strDefine += "_h__";
		strDefine = strDefine.toUpper();

		fprintf(pOutStubHdr, "#ifndef %s\n#define %s\n\n", 
			strDefine.toLatin1().constData(),
			strDefine.toLatin1().constData());
     
		//write includes
		fprintf(pOutStubHdr, "#include \"common/common/status.h\"\n");
		//fprintf(pOutStubHdr, "#include \"bus_interface/bus_interface/interface_%s.h\"\n",strBaseName.toLower().toLatin1().constData());
		fprintf(pOutStubHdr, "#include \"bus_interface/bus_interface/interface_%s.h\"\n",strOriginalName.toLower().toLatin1().constData());
		fprintf(pOutStubHdr, "#include \"bus_trans_client/bus_trans_client/connectionhandler.h\"\n");
		fprintf(pOutStubHdr, "#include \"trans/trans/rpcstubmessagehandler.h\"\n");

		QString strNewClass = "RpcStub_" + strBaseName;

		//write class declaration
		fprintf(pOutStubHdr, "\nclass %s: public %s, public RpcStub\n{\n",
			strNewClass.toLatin1().constData(),
			strClassName.toLatin1().constData());

		//write constructor
		fprintf(pOutStubHdr, "public:\n\t%s(ConnectionHandler *pConnectionHandler,int RPCType):\n\t\tRpcStub(RPCType){m_ConnectionHandler=pConnectionHandler;}\n\n",
			strNewClass.toLatin1().constData());

		//write all methods
		int nLastAccess = ACCESS_PUBLIC;	//constructor's acess
		for(int i=0; i<lstMethods.size(); i++)
		{
			//write access if changed
			if(nLastAccess != lstMethods[i].nAccess)
			{
				nLastAccess = lstMethods[i].nAccess;
				switch(nLastAccess){
					case ACCESS_PUBLIC:
						fprintf(pOutStubHdr, "\npublic:\n"); break;
					case ACCESS_PROTECTED:
						fprintf(pOutStubHdr, "\nprotected:\n"); break;
					case ACCESS_PRIVATE:
						fprintf(pOutStubHdr, "\nprivate:\n"); break;
				}
			}

			//now write method declaration
			fprintf(pOutStubHdr, "\t%s %s(%s);\n",
				lstMethods[i].strReturn.toLatin1().constData(),
				lstMethods[i].strName.toLatin1().constData(),
				lstMethods[i].strParams.toLatin1().constData());
		}

		//write data members
		fprintf(pOutStubHdr, "\nprivate:\n\tConnectionHandler *m_ConnectionHandler; ///< connection handler\n");

		fprintf(pOutStubHdr, "};\n\n#endif\t// %s\n", strDefine.toLatin1().constData());
		fclose(pOutStubHdr);
	}

	//
	// generate rpcstub CPP file
	//
	strOutFile = strOutputDir;
	strOutFile += "rpcstub_";
	strOutFile += strBaseName.toLower();
	strOutFile += ".cpp";

	FILE *pOutStubCpp = fopen(strOutFile.toLatin1().constData(), "w");
	if(pOutStubCpp)
	{
		//include header
		QString strHdrFile = "rpcstub_";
		strHdrFile += strBaseName.toLower();
		strHdrFile += ".h";

		fprintf(pOutStubCpp, "#include \"%s\"\n\n", strHdrFile.toLatin1().constData());

		QString strNewClass = "RpcStub_" + strBaseName;

		//for each method
		for(int i=0; i<lstMethods.size(); i++)
		{
			//write method definition
			fprintf(pOutStubCpp, "%s %s::%s(%s)\n{\n",
				lstMethods[i].strReturn.toLatin1().constData(),
				strNewClass.toLatin1().constData(),
				lstMethods[i].strName.toLatin1().constData(),
				lstMethods[i].strParamsDef.toLatin1().constData());

			//write fixed code part
			fprintf(pOutStubCpp, "\t//make an instance of rpc msg handler\n");
			fprintf(pOutStubCpp, "\tRpcStubMessageHandler msg(this);\n");
			fprintf(pOutStubCpp, "\n\t//create RPC parameter list\n");
			fprintf(pOutStubCpp, "\t//----------------------------\n");
			fprintf(pOutStubCpp, "\n\t//first parameter is SESSION\n");
			fprintf(pOutStubCpp, "\tQString strSessionID=m_ConnectionHandler->GetSessionID();\n");
			fprintf(pOutStubCpp, "\tmsg.msg_in->AddParameter(&strSessionID,\"strSessionID\");\n");
			fprintf(pOutStubCpp, "\n\t//add all other parameters\n");

			//code to fill call parameters
			for(int j=1; j<lstMethods[i].lstParams.size(); j++)
			{
				//clear some data before sending
				if(lstMethods[i].lstParams[j].strName.indexOf("Ret_") == 0)
					continue; //BT: 20.01.2010: no need to send puer RET parameters, only RetOut and others
				/*
				{
					if( lstMethods[i].lstParams[j].strType.indexOf("DbRecordSet") >= 0)
					{
						fprintf(pOutStubCpp, "\t%s.destroy();\n", lstMethods[i].lstParams[j].strName.toLatin1().constData());
					}
					else if( lstMethods[i].lstParams[j].strType.indexOf("QByteArray") >= 0)
					{
						fprintf(pOutStubCpp, "\t%s.clear();\n", lstMethods[i].lstParams[j].strName.toLatin1().constData());
					}
				}
				*/

				fprintf(pOutStubCpp, "\tmsg.msg_in->AddParameter(&%s,\"%s\");\n", lstMethods[i].lstParams[j].strName.toLatin1().constData(),lstMethods[i].lstParams[j].strName.toLatin1().constData());

				//clear some data before sending
				if(lstMethods[i].lstParams[j].strName.indexOf("Destroy_") >= 0)
				{
					if( lstMethods[i].lstParams[j].strType.indexOf("DbRecordSet") >= 0)
					{
						fprintf(pOutStubCpp, "\t%s.destroy();\n", lstMethods[i].lstParams[j].strName.toLatin1().constData());
					}
					else if( lstMethods[i].lstParams[j].strType.indexOf("QByteArray") >= 0)
					{
						fprintf(pOutStubCpp, "\t%s.clear();\n", lstMethods[i].lstParams[j].strName.toLatin1().constData());
					}
				}
			}

			//fixed code part to execute RPC call
			fprintf(pOutStubCpp, "\n\t//make a RPC call\n");
			fprintf(pOutStubCpp, "\t//----------------------------\n");
			fprintf(pOutStubCpp, "\n\t//generate rpc call:\n");
			fprintf(pOutStubCpp, "\tmsg.msg_in->GenerateRPC(\"%s.%s\",%s);\n", strBaseName.toLatin1().constData(), lstMethods[i].strName.toLatin1().constData(), lstMethods[i].lstParams[0].strName.toLatin1().constData());
			fprintf(pOutStubCpp, "\tif (!%s.IsOK())return;\n", lstMethods[i].lstParams[0].strName.toLatin1().constData());
			fprintf(pOutStubCpp, "\t//make SYNC call to server:\n");
			fprintf(pOutStubCpp, "\tm_ConnectionHandler->SendRPC(%s,msg);\n",lstMethods[i].lstParams[0].strName.toLatin1().constData());
			fprintf(pOutStubCpp, "\tif (!%s.IsOK())return;\n", lstMethods[i].lstParams[0].strName.toLatin1().constData());

			//fixed code part to process response
			fprintf(pOutStubCpp, "\n\t//compile response\n");
			fprintf(pOutStubCpp, "\t//----------------------------\n");
			fprintf(pOutStubCpp, "\n\t//get fault if there is one\n");
			fprintf(pOutStubCpp, "\tmsg.msg_out->CheckForFault(%s);\n",lstMethods[i].lstParams[0].strName.toLatin1().constData());
			fprintf(pOutStubCpp, "\n\t//if not OK, return\n");
			fprintf(pOutStubCpp, "\tif (!%s.IsOK()) return;\n", lstMethods[i].lstParams[0].strName.toLatin1().constData());

			//code to extract return data
			fprintf(pOutStubCpp, "\n\t//extract return parameters from msg_out:\n");
			int nCounter = 0;
			for(int j=1; j<lstMethods[i].lstParams.size(); j++)
			{
				if(lstMethods[i].lstParams[j].strName.indexOf("Ret") == 0)
				{
					fprintf(pOutStubCpp, "\tmsg.msg_out->GetParameter(%d,&%s,%s); if (!%s.IsOK()) return;\n", 
						nCounter, 
						lstMethods[i].lstParams[j].strName.toLatin1().constData(),
						lstMethods[i].lstParams[0].strName.toLatin1().constData(),
						lstMethods[i].lstParams[0].strName.toLatin1().constData());
					nCounter ++;
				}
			}

			fprintf(pOutStubCpp, "}\n\n");
		}

		fclose(pOutStubCpp);
	}

	//
	// generate rpcskeleton HEADER file
	//
	strOutFile = strOutputDir;
	strOutFile += "rpcskeleton_";
	strOutFile += strBaseName.toLower();
	strOutFile += ".h";

	FILE *pOutSkeletonHdr = fopen(strOutFile.toLatin1().constData(), "w");
	if(pOutSkeletonHdr)
	{
		QString strDefine = "rpcskeleton_";
		strDefine += strBaseName.toLower();
		strDefine += "_h__";
		strDefine = strDefine.toUpper();

		QString strNewClass = "RpcSkeleton_" + strBaseName;

		fprintf(pOutSkeletonHdr, "#ifndef %s\n#define %s\n\n", 
			strDefine.toLatin1().constData(),
			strDefine.toLatin1().constData());

		//write includes
		fprintf(pOutSkeletonHdr, "#include \"common/common/status.h\"\n");
		fprintf(pOutSkeletonHdr, "#include \"trans/trans/rpcskeleton.h\"\n");
		fprintf(pOutSkeletonHdr, "#include \"trans/trans/rpcskeletonmessagehandler.h\"\n");
		//fprintf(pOutSkeletonHdr, "#include \"bus_interface/bus_interface/interface_%s.h\"\n", strBaseName.toLower().toLatin1().constData());
		fprintf(pOutSkeletonHdr, "#include \"bus_interface/bus_interface/interface_%s.h\"\n", strOriginalName.toLower().toLatin1().constData());
		fprintf(pOutSkeletonHdr, "#include \"bus_server/bus_server/businessserviceset.h\"\n");

		fprintf(pOutSkeletonHdr, "\nclass %s : public RpcSkeleton\n", strNewClass.toLatin1().constData());
		fprintf(pOutSkeletonHdr, "{\n\ntypedef void (%s::* PFN)(Status &err,RpcSkeletonMessageHandler &rpc);\n\n", strNewClass.toLatin1().constData());
		fprintf(pOutSkeletonHdr, "public:\n\t%s(BusinessServiceSet *pHandler,int RPCType);\n\n", strNewClass.toLatin1().constData());
		fprintf(pOutSkeletonHdr, "\tbool HandleRPC(Status &err, QByteArray *pBufRequest,QByteArray *pBufResponse, QString strRPCMethod, int nTimeZoneOffsetMinutes);\n\n");
		fprintf(pOutSkeletonHdr, "private:\n\n\t//skeleton function for each method publicly accessible\n");

		//for each method, create skeleton method
		for(int i=0; i<lstMethods.size(); i++)
			if(lstMethods[i].nAccess == ACCESS_PUBLIC)
				fprintf(pOutSkeletonHdr, "\tvoid %s(Status &err,RpcSkeletonMessageHandler &rpc);\n", lstMethods[i].strName.toLatin1().constData());

		fprintf(pOutSkeletonHdr, "\n\tInterface_%s *m_Handler; //< pointer to your BO\n", strOriginalName.toLatin1().constData());
		fprintf(pOutSkeletonHdr, "\tQHash<QString,PFN> mFunctList;\n");

		fprintf(pOutSkeletonHdr, "};\n\n#endif\t// %s\n", strDefine.toLatin1().constData());
		fclose(pOutSkeletonHdr);
	}

	//
	// generate rpcskeleton HEADER file
	//
	strOutFile = strOutputDir;
	strOutFile += "rpcskeleton_";
	strOutFile += strBaseName.toLower();
	strOutFile += ".cpp";

	FILE *pOutSkeletonCpp = fopen(strOutFile.toLatin1().constData(), "w");
	if(pOutSkeletonCpp)
	{
		//include header
		QString strHdrFile = "rpcskeleton_";
		strHdrFile += strBaseName.toLower();
		strHdrFile += ".h";

		fprintf(pOutStubCpp, "#include \"%s\"\n\n", strHdrFile.toLatin1().constData());

		QString strNewClass = "RpcSkeleton_" + strBaseName;

		//write constructor method
		fprintf(pOutSkeletonCpp, "%s::%s(BusinessServiceSet *pHandler,int RPCType):\n",
			strNewClass.toLatin1().constData(),
			strNewClass.toLatin1().constData());
		fprintf(pOutSkeletonCpp, "\tRpcSkeleton(RPCType)\n{\n");
		fprintf(pOutSkeletonCpp, "\tm_Handler=pHandler->%s; //store handler to actual business object\n", strBaseName.toLatin1().constData());
		fprintf(pOutSkeletonCpp, "\tSetNameSpace(\"%s\"); //set namespace (business object name)\n", strBaseName.toLatin1().constData());
		fprintf(pOutSkeletonCpp, "\n\t//List all methods\n");
		for(int i=0; i<lstMethods.size(); i++)
		{
			fprintf(pOutSkeletonCpp, "\tmFunctList.insert(\"%s\",&%s::%s);\n", 
				lstMethods[i].strName.toLatin1().constData(),
				strNewClass.toLatin1().constData(),
				lstMethods[i].strName.toLatin1().constData());
		}
		fprintf(pOutSkeletonCpp, "}\n\n");

		//write HandleRPC method
		fprintf(pOutSkeletonCpp, "bool %s::HandleRPC(Status &err,QByteArray *pBufRequest,QByteArray *pBufResponse, QString strRPCMethod, int nTimeZoneOffsetMinutes)\n{\n\t//init locals:\n", strNewClass.toLatin1().constData());
		fprintf(pOutSkeletonCpp, "\tif(!TestNameSpace(strRPCMethod))return false;\n\n");
		fprintf(pOutSkeletonCpp, "\tRpcSkeletonMessageHandler rpc(this,pBufRequest,pBufResponse,nTimeZoneOffsetMinutes);\n\n");
		fprintf(pOutSkeletonCpp, "\t//get methodname, test for error\n\tQString strMethodName=rpc.GetSkeletonMethodName();\n");
		fprintf(pOutSkeletonCpp, "\tif (strMethodName.isEmpty()){rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_FUNCTION_NOT_EXISTS);return true;}\n\n");
		fprintf(pOutSkeletonCpp, "\t//METHOD DISPATCHER\n\tQHash<QString,PFN>::const_iterator i = mFunctList.find(strMethodName);\n");
		fprintf(pOutSkeletonCpp, "\tif(i != mFunctList.end() && i.key() == strMethodName)\n\t{\n\t\t//call skeleton method\n\t\t(this->*i.value())(err,rpc);\n\t\treturn true;\n\t}\n\telse\n\t{\n");
		fprintf(pOutSkeletonCpp, "\t\t//if not found return error:\n");
		fprintf(pOutSkeletonCpp, "\t\terr.setError(StatusCodeSet::ERR_RPC_FUNCTION_NOT_EXISTS);\n");
		fprintf(pOutSkeletonCpp, "\t\trpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_FUNCTION_NOT_EXISTS);\n\t\treturn true;\n\t}\n\treturn true;\n}\n\n");

		//generate per-method skeleton functions
		for(int i=0; i<lstMethods.size(); i++)
		{
			fprintf(pOutSkeletonCpp, "void %s::%s(Status &err,RpcSkeletonMessageHandler &rpc)\n{\n",
				strNewClass.toLatin1().constData(),
				lstMethods[i].strName.toLatin1().constData());

			//fprintf(pOutSkeletonCpp, "\tStatus err;\n\n");

			//calc only Ret_ params:
			int nActualParamSize=0;
			for(int j=0; j<lstMethods[i].lstParams.size(); j++)
				if(!(lstMethods[i].lstParams[j].strName.indexOf("Ret_") == 0))
					nActualParamSize++;
			nActualParamSize++;		 //add one for incoming session which is always 1st param

			fprintf(pOutSkeletonCpp, "\t//test parameter count\n");
			fprintf(pOutSkeletonCpp, "\tif(rpc.msg_in->GetParameterCount()!=%d)\n", nActualParamSize);
			fprintf(pOutSkeletonCpp, "\t{rpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}\n");

			//NOTE: ignore 1st params when generating local varables
			fprintf(pOutSkeletonCpp, "\n\t//extract parameters to local vars\n");
			for(int j=1; j<lstMethods[i].lstParams.size(); j++){
				QString strType = lstMethods[i].lstParams[j].strType;
				strType = strType.replace("&", "");
				fprintf(pOutSkeletonCpp, "\t%s %s;\n",
					strType.toLatin1().constData(),
					lstMethods[i].lstParams[j].strName.toLatin1().constData());
			}

			fprintf(pOutSkeletonCpp, "\n\t//getParam (Only incoming & RETURNED-OUTGOING) from client\n");
			int nParamCnt=1; //0 is status/session
			for(int j=1; j<lstMethods[i].lstParams.size(); j++)
			{
				if(!(lstMethods[i].lstParams[j].strName.indexOf("Ret_") == 0))
				{
					fprintf(pOutSkeletonCpp, "\trpc.msg_in->GetParameter(%d,&%s,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}\n",
						nParamCnt, lstMethods[i].lstParams[j].strName.toLatin1().constData());
					nParamCnt++;
				}
			}

			fprintf(pOutSkeletonCpp, "\n\t//invoke handler\n");
			fprintf(pOutSkeletonCpp, "\tm_Handler->%s(err", lstMethods[i].strName.toLatin1().constData());
			for(int j=1; j<lstMethods[i].lstParams.size(); j++){
				fprintf(pOutSkeletonCpp, ",%s", lstMethods[i].lstParams[j].strName.toLatin1().constData());
			}
			fprintf(pOutSkeletonCpp, ");\n");

			fprintf(pOutSkeletonCpp, "\n\t//clear outgoing buffer\n\trpc.msg_out->ClearData();\n");
			fprintf(pOutSkeletonCpp, "\t//test if error: then return only error:\n");
			fprintf(pOutSkeletonCpp, "\tif(!err.IsOK())\n\t{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}\n");
			fprintf(pOutSkeletonCpp, "\n\t//return RETURNED variables to the client: (skip pStatus)\n\t//start from first RETURNED var:\n");

			for(int j=1; j<lstMethods[i].lstParams.size(); j++)
			{
				if(lstMethods[i].lstParams[j].strName.indexOf("Ret") == 0)
				{
					fprintf(pOutSkeletonCpp, "\trpc.msg_out->AddParameter(&%s,\"%s\");\n",
						lstMethods[i].lstParams[j].strName.toLatin1().constData(),lstMethods[i].lstParams[j].strName.toLatin1().constData());
				}
			}

			fprintf(pOutSkeletonCpp, "\n\t//generate response\n");
			fprintf(pOutSkeletonCpp, "\trpc.msg_out->GenerateResponse(err);\n");
			fprintf(pOutSkeletonCpp, "\tif (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorTextRaw());return;}\n}\n\n");
		}

		fclose(pOutSkeletonCpp);
	}



}



bool RpcGenerator::GenerateOne_Rest(QString strInputFile, QString strOutputDir,bool bSkipInteract,QString strForceBaseName,bool bGenerateForSingleServiceHandler)
{

	//
	// STEP 2: parse input file (class name, method names, method parameters)
	//
	QString strClassName;
	QList<Method> lstMethods;
	QList<MetaData> lstMetaData;
	QList<QString> lstInput;
	QString strNameSpace;

	int nIdx = 0; // line counter of input file [0],[1], etc..

	if(!RpcGenerator::LoadFile(strInputFile, lstInput)) //LoadFile function
	{
		QMessageBox::critical(this,"Warning","Can not load input file");
		return false;
	}

	// trim lines, cut comments, etc,..
	FilterInput(lstInput);


	// parse meta data
	if(!ParseMetaData(strInputFile,lstMetaData,strNameSpace))
	{
		QMessageBox::critical(this,"Warning","Can not parse meta data from header file");
		return false;
	}

	
	
	//parse remaining contents
	int nAccess = ACCESS_PRIVATE;	//default access right
    nIdx = 0;
	while(nIdx < lstInput.size())  //loop trough the input file (collection of lines)
	{
		if(lstInput[nIdx].indexOf("class") == 0) 
		{
			strClassName = lstInput[nIdx].right(lstInput[nIdx].size()-5).trimmed();
		}
		else if(lstInput[nIdx].indexOf("public:") >= 0)
		{
			nAccess = ACCESS_PUBLIC;
		}
		else if(lstInput[nIdx].indexOf("private:") >= 0)
		{
			nAccess = ACCESS_PRIVATE;
		}
		else if(lstInput[nIdx].indexOf("protected:") >= 0)
		{
			nAccess = ACCESS_PROTECTED;
		}
		else
		{
			//extract method name
			int nParamsPos = lstInput[nIdx].indexOf("(");

			// if the "(" sign is missing
			if(nParamsPos < 0){
				QString strMsg = QString("Invalid line (no '(' character): %1!").arg(lstInput[nIdx]);
				QMessageBox::information(this, "Error", strMsg);
				return false;
			}
			
			// take left side of "(" ?
			QString strLeft = lstInput[nIdx].left(nParamsPos).trimmed();

			//calculate method name
			QString strName = strLeft;				
			int nPos = strName.lastIndexOf(" ");
			
			if(nPos >= 0)
				strName = strName.right(strName.size()-nPos-1);
			nPos = strName.lastIndexOf("\t"); 
			if(nPos >= 0)
				strName = strName.right(strName.size()-nPos-1);

			//filter out constructor and destructor methods   
			if( strName == strClassName || 
				strName.right(strName.size()-1) == strClassName)
			{
				nIdx ++;			
				continue;
			}

			//calculate return type
			QString strRet = strLeft;
			nPos = strRet.lastIndexOf(" ");
			if(nPos >= 0)
				strRet = strRet.left(nPos);

			//remove "virtual" keyword (not needed for the derived classes)
			nPos = strRet.indexOf("virtual");
			if(nPos >= 0)
				strRet = strRet.left(nPos) + strRet.right(strRet.size()-nPos-7);
			strRet = strRet.trimmed();

			//calculate method params
			QString strParm = lstInput[nIdx].right(lstInput[nIdx].size()-nParamsPos-1);
			int nParamsEndPos = strParm.lastIndexOf(")");
			if(nParamsEndPos < 0){
				QString strMsg = QString("Invalid line (no ')' character): %1!").arg(lstInput[nIdx]);
				QMessageBox::information(this, "Error", strMsg);
				return false;
			}
			strParm = strParm.left(nParamsEndPos);

			//remove all default values (for example "= NULL")
			QString strParm1 = strParm;
			nPos = strParm1.indexOf("=");
			while(nPos>0){
				int nEnd = strParm1.indexOf(",", nPos);
				if(nEnd > 0)
					strParm1 = strParm1.left(nPos) + strParm1.right(strParm1.size()-nEnd);
				else
					strParm1 = strParm1.left(nPos);
				nPos = strParm1.indexOf("=");
			}

			//parse the params into the list
			QString strTmp = strParm1;
			QList<QString> lstStrParams;
			nPos = strTmp.indexOf(",");
			while(nPos >= 0){
				QString strPart = strTmp.left(nPos);
				lstStrParams.append(strPart.trimmed());
				//proceed forward
				strTmp = strTmp.right(strTmp.size()-nPos-1);
				nPos = strTmp.indexOf(",");
			}
			if(!strTmp.isEmpty())
				lstStrParams.append(strTmp.trimmed());
			//break parts into the type+name list
			QList<Param> lstParams;
			for(int i=0; i<lstStrParams.size(); i++)
			{
				Param param;
				int nPos1 = lstStrParams[i].lastIndexOf(" ");
				int nPos2 = lstStrParams[i].lastIndexOf("\t");
				int nPos  = std::max(nPos1, nPos2);
				if(nPos >= 0)
				{
					while( lstStrParams[i].at(nPos+1) == '&' ||
						lstStrParams[i].at(nPos+1) == '*')
						nPos ++;

					param.strName = lstStrParams[i].right(lstStrParams[i].size()-nPos-1);
					param.strType = lstStrParams[i].left(nPos+1);
					lstParams.append(param);
				}
			}

			Method method;
			method.nAccess   = nAccess;
			method.strName   = strName;
			method.strParams = strParm;
			method.strParamsDef = strParm1;	//params without initial values
			method.strReturn = strRet;
			method.lstParams = lstParams;
			lstMethods.append(method);
		}

		nIdx ++;
	}

	//
	// STEP 3: generate output files
	//

	//debug code

#if 0
#ifdef _DEBUG
	FILE *pOut = fopen("D:\\a.txt", "w");
	if(pOut){
		for(int i=0; i<lstInput.size(); i++)
			fprintf(pOut, "%s\n", lstInput[i].toLatin1().constData());
		fclose(pOut);
	}
	FILE *pOut1 = fopen("D:\\b.txt", "w");
	if(pOut1){
		fprintf(pOut1, "%s\n", strClassName.toLatin1().constData());
		for(int i=0; i<lstMethods.size(); i++)
			fprintf(pOut1, "\t%s\t%s(%s)\n", 
			lstMethods[i].strReturn.toLatin1().constData(),
			lstMethods[i].strName.toLatin1().constData(),
			lstMethods[i].strParams.toLatin1().constData());
		fclose(pOut1);
	}
#endif
#endif

	// calc class name without the prefix
	QString strBaseName = strClassName;                               //ex: Interface_ServiceHandler
	if(0 == strBaseName.indexOf("Interface_"))					      
		strBaseName = strBaseName.right(strBaseName.size()-10);		  //ex: ServiceHandler

	strBaseName.replace("Handler","");

	//Ask user to use same base name:
	bool ok;
	if (!bSkipInteract)
	{
		QString text = QInputDialog::getText(this, "Enter base class name",
			"Base Class Name (Modify if different, leave as is if OK):", QLineEdit::Normal,
			strBaseName, &ok);
		if (ok && !text.isEmpty())
			strBaseName=text;
	}
	//QFileInfo file(strInputFile);
	QString strOriginalName=strBaseName;    // probably used if the optional name argument is passed to the generate function..??
	if (!strForceBaseName.isEmpty())
		strBaseName=strForceBaseName;


	qDebug()<<strBaseName;

	//FILE *pOutStubCpp;
	QString strOutFile;
	//
	// generate rpc skeleton HEADER file
	//
	strOutFile = strOutputDir;
	if (!bGenerateForSingleServiceHandler)
		strOutFile += "rpcskeleton_";
	else
		strOutFile += "restrpcskeleton_";

	strOutFile += strBaseName.toLower();
	strOutFile += ".h";

	FILE *pOutSkeletonHdr = fopen(strOutFile.toLatin1().constData(), "w");
	if(pOutSkeletonHdr)
	{
		QString strDefine;
		if (!bGenerateForSingleServiceHandler)
			strDefine = "rpcskeleton_";
		else
			strDefine = "restrpcskeleton_";

		strDefine += strBaseName.toLower();
		strDefine += "_h__";
		strDefine = strDefine.toUpper();

		QString strNewClass;
		if (!bGenerateForSingleServiceHandler)
			strNewClass = "RpcSkeleton_" + strBaseName;
		else
			strNewClass = "RestRpcSkeleton_" + strBaseName;


		//#ifndef RESTRPCSKELETON_SERVICE_H
		//#define RESTRPCSKELETON_SERVICE_H	
		fprintf(pOutSkeletonHdr, "#ifndef %s\n#define %s\n\n", 
			strDefine.toLatin1().constData(),
			strDefine.toLatin1().constData());

		//write includes
		fprintf(pOutSkeletonHdr, "#include \"common/common/status.h\"\n");
		fprintf(pOutSkeletonHdr, "#include \"trans/trans/rpcskeleton.h\"\n");
		fprintf(pOutSkeletonHdr, "#include \"trans/trans/rpcskeletonmessagehandler.h\"\n");
		if (!bGenerateForSingleServiceHandler)
		{
			fprintf(pOutSkeletonHdr, "#include \"bus_interface/bus_interface/interface_%s.h\"\n", strOriginalName.toLower().toLatin1().constData());
			fprintf(pOutSkeletonHdr, "#include \"bus_server/bus_server/businessserviceset.h\"\n");
		}
		fprintf(pOutSkeletonHdr, "\nclass %s : public RpcSkeleton\n", strNewClass.toLatin1().constData());

		fprintf(pOutSkeletonHdr, "{\n\ntypedef void (%s::* PFN)(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id );\n\n", strNewClass.toLatin1().constData());
		if (bGenerateForSingleServiceHandler)
		{
			fprintf(pOutSkeletonHdr, "public:\n\t%s(int RPCType);\n\n", strNewClass.toLatin1().constData());
		}
		else
		{
			fprintf(pOutSkeletonHdr, "public:\n\t%s(BusinessServiceSet *pHandler, int RPCType);\n\n", strNewClass.toLatin1().constData());
		}
		fprintf(pOutSkeletonHdr, "\tbool HandleRPC(Status &err, QByteArray *pBufRequest,QByteArray *pBufResponse, QString strRPCMethod, int nTimeZoneOffsetMinutes);\n\n");

		//ex: //skeleton function for each method publicly accessible
		// first PRIVATE
		fprintf(pOutSkeletonHdr, "private:\n\n\t//skeleton function for each method publicly accessible\n");

		//for each method, create skeleton method
		for(int i=0; i<lstMethods.size(); i++)
			if(lstMethods[i].nAccess == ACCESS_PUBLIC)
				fprintf(pOutSkeletonHdr, "\tvoid %s(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id);\n", lstMethods[i].strName.toLatin1().constData());

		if (!bGenerateForSingleServiceHandler)
		{
			fprintf(pOutSkeletonHdr, "\n\tInterface_%s *m_Handler; //< pointer to your BO\n", strOriginalName.toLatin1().constData());
		}
		fprintf(pOutSkeletonHdr, "\n\tQHash<QString,PFN> mFunctList;\n");

		fprintf(pOutSkeletonHdr, "};\n\n#endif\t// %s\n", strDefine.toLatin1().constData());
		fclose(pOutSkeletonHdr);
	}

	//
	// generate rpcskeleton .CPP file
	//

	strOutFile = strOutputDir;
	if (!bGenerateForSingleServiceHandler)
		strOutFile += "rpcskeleton_";
	else
		strOutFile += "restrpcskeleton_";

	strOutFile += strBaseName.toLower();
	strOutFile += ".cpp";

	FILE *pOutSkeletonCpp = fopen(strOutFile.toLatin1().constData(), "w");
	if(pOutSkeletonCpp)
	{
		//include header
		QString strHdrFile;
		if (!bGenerateForSingleServiceHandler)
			strHdrFile = "rpcskeleton_";
		else
			strHdrFile = "restrpcskeleton_";

		strHdrFile += strBaseName.toLower();
		strHdrFile += ".h";

		fprintf(pOutSkeletonCpp, "#include \"%s\"\n", strHdrFile.toLatin1().constData());

		if (bGenerateForSingleServiceHandler)
		{
			if (strBaseName.toLower()!="service")
			{
				QString strAddOn=strBaseName.mid(strBaseName.lastIndexOf("_"));
				fprintf(pOutSkeletonCpp, "#include \"servicehandler"+strAddOn.toLower().toLatin1()+".h\"\n");
				fprintf(pOutSkeletonCpp, "extern ServiceHandler"+strAddOn.toLatin1()+" *g_ServiceHandler"+strAddOn.toLatin1()+";\n\n");
			}
			else
			{
				fprintf(pOutSkeletonCpp, "#include \"servicehandler.h\"\n");
				fprintf(pOutSkeletonCpp, "extern ServiceHandler *g_ServiceHandler;\n\n");
			}
		}

		QString strNewClass;
		if (!bGenerateForSingleServiceHandler)
			strNewClass = "RpcSkeleton_" + strBaseName;
		else
			strNewClass = "RestRpcSkeleton_" + strBaseName;



		if (!bGenerateForSingleServiceHandler)
		{
			//write constructor method
			fprintf(pOutSkeletonCpp, "%s::%s(BusinessServiceSet *pHandler,int RPCType):\n",
				strNewClass.toLatin1().constData(),
				strNewClass.toLatin1().constData());
		}
		else
		{
			//write constructor method
			fprintf(pOutSkeletonCpp, "%s::%s(int RPCType):\n",
				strNewClass.toLatin1().constData(),
				strNewClass.toLatin1().constData());
		}


		fprintf(pOutSkeletonCpp, "RpcSkeleton(RPCType)\n{\n");

		
		if (!bGenerateForSingleServiceHandler)
		{
			fprintf(pOutSkeletonCpp, "\tm_Handler=pHandler->%s; //store handler to actual business object\n", strBaseName.toLatin1().constData());
		}
		
		// Namespace
		//fprintf(pOutSkeletonCpp, "\tSetNameSpace(\"%s\"); //set namespace (business object name)\n", strBaseName.toLatin1().constData());
		fprintf(pOutSkeletonCpp, "\tSetNameSpace(\"%s\"); //set namespace (business object name)\n", strNameSpace.toLatin1().constData());

		fprintf(pOutSkeletonCpp, "\n\t//List all methods\n");		

		if (lstMethods.size()!=lstMetaData.size())
		{
			QMessageBox::critical(this,"Warning","Meta data is inconsistent with number of functions. Probably not all functions are defined in meta data in the interface.");
			return false;
		}
		for(int i=0; i<lstMethods.size(); i++)
		{
			fprintf(pOutSkeletonCpp, "\tmFunctList.insert(\"%s%s\",&%s::%s);\n", 
				lstMetaData[i].strHttpMethodName.toLatin1().constData(),
				lstMetaData[i].strUrlName.toLatin1().constData(),
				strNewClass.toLatin1().constData(),
				lstMetaData[i].strFunctionName.toLatin1().constData());
		}
		fprintf(pOutSkeletonCpp, "}\n\n");

	
		//write HandleRPC method
		fprintf(pOutSkeletonCpp, "bool %s::HandleRPC(Status &err,QByteArray *pBufRequest,QByteArray *pBufResponse, QString strRPCMethod, int nTimeZoneOffsetMinutes)\n{\n\t//init locals:\n", strNewClass.toLatin1().constData());
		
		fprintf(pOutSkeletonCpp, "\tQString strMethodForCompare;\n\tQString strSchema;\n\tint nResID=-1;\n\tint nParentResID=-1;\n\tQString strHttpMethod;\n\n");
		fprintf(pOutSkeletonCpp, "\tif(!TestNameSpaceForRest(strRPCMethod,strMethodForCompare,nResID,nParentResID,strHttpMethod))\n\t\treturn false;\n\n");
		fprintf(pOutSkeletonCpp, "\tRpcSkeletonMessageHandler rpc(this,pBufRequest,pBufResponse,nTimeZoneOffsetMinutes);\n");

		fprintf(pOutSkeletonCpp, "\trpc.msg_in->StartResponseParsing(err);\n"); 
		fprintf(pOutSkeletonCpp, "\tif (!err.IsOK()) \n\t\treturn true;\n\n");	    
		
		fprintf(pOutSkeletonCpp, "\tif (strHttpMethod==\"OPTIONS\")\n");			
		fprintf(pOutSkeletonCpp, "\t{\n");
		fprintf(pOutSkeletonCpp, "\t\tstrMethodForCompare=strMethodForCompare.mid(strMethodForCompare.indexOf(\"/\")+1);\n");	// "''"
		fprintf(pOutSkeletonCpp, "\t\tQString strHttpMethodAllowed;\n");
	    fprintf(pOutSkeletonCpp, "\t\tQHashIterator<QString,PFN> i(mFunctList);\n\n");
		fprintf(pOutSkeletonCpp, "\t\twhile (i.hasNext()) \n");
		fprintf(pOutSkeletonCpp, "\t\t{\n");
		fprintf(pOutSkeletonCpp, "\t\t\ti.next();\n");
		fprintf(pOutSkeletonCpp, "\t\t\tQString strMethod=i.key();\n");
		fprintf(pOutSkeletonCpp, "\t\t\tstrMethod=strMethod.mid(strMethod.indexOf(\"/\")+1);\n\n");
		fprintf(pOutSkeletonCpp, "\t\t\tif (strMethodForCompare==strMethod)\n");
		fprintf(pOutSkeletonCpp, "\t\t\t{\n");
		fprintf(pOutSkeletonCpp, "\t\t\t\tstrHttpMethodAllowed+=i.key().left(i.key().indexOf(\"/\"))+\",\";\n"); //strHttpMethodAllowed+=i.key().left(i.key().indexOf("/"))+",";
		fprintf(pOutSkeletonCpp, "\t\t\t}\n");
		fprintf(pOutSkeletonCpp, "\t\t}\n\n");
		fprintf(pOutSkeletonCpp, "\t\tif (!strHttpMethodAllowed.isEmpty())\n");
		fprintf(pOutSkeletonCpp, "\t\t{\n");
		fprintf(pOutSkeletonCpp, "\t\t\tstrHttpMethodAllowed.chop(1);\n");
		fprintf(pOutSkeletonCpp, "\t\t\terr.setError(StatusCodeSet::ERR_RPC_OPTION_REQUEST_OK,strHttpMethodAllowed);\n");
		fprintf(pOutSkeletonCpp, "\t\t\treturn true;\n");
		fprintf(pOutSkeletonCpp, "\t\t}\n");
		fprintf(pOutSkeletonCpp, "\t\telse\n");
		fprintf(pOutSkeletonCpp, "\t\t{\n");
		fprintf(pOutSkeletonCpp, "\t\t\terr.setError(StatusCodeSet::ERR_RPC_FUNCTION_NOT_EXISTS);\n");
		fprintf(pOutSkeletonCpp, "\t\t\trpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_FUNCTION_NOT_EXISTS);\n");
		fprintf(pOutSkeletonCpp, "\t\t\treturn true;\n");
		fprintf(pOutSkeletonCpp, "\t\t}\n");
		fprintf(pOutSkeletonCpp, "\t}\n\n");
		

		fprintf(pOutSkeletonCpp, "\tQHash<QString,PFN>::const_iterator i = mFunctList.find(strMethodForCompare);\n");
		fprintf(pOutSkeletonCpp, "\tif(i != mFunctList.end() && i.key() == strMethodForCompare)\n");
		fprintf(pOutSkeletonCpp, "\t{\n");
		fprintf(pOutSkeletonCpp, "\t\t(this->*i.value())(err,rpc,nResID,nParentResID);\n");
		fprintf(pOutSkeletonCpp, "\t\treturn true;\n");
		fprintf(pOutSkeletonCpp, "\t}\n");
		fprintf(pOutSkeletonCpp, "\telse\n");
		fprintf(pOutSkeletonCpp, "\t{\n");
		fprintf(pOutSkeletonCpp, "\t\terr.setError(StatusCodeSet::ERR_RPC_FUNCTION_NOT_EXISTS);\n");
		fprintf(pOutSkeletonCpp, "\t\trpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_FUNCTION_NOT_EXISTS);\n");
		fprintf(pOutSkeletonCpp, "\t\treturn true;\n");
		fprintf(pOutSkeletonCpp, "\t}\n\n");
		fprintf(pOutSkeletonCpp, "return true;\n");
		fprintf(pOutSkeletonCpp, "}\n\n");
			
		
		//generate per-method skeleton functions
		for(int i=0; i<lstMethods.size(); i++)
		{
			fprintf(pOutSkeletonCpp, "void %s::%s(Status &err,RpcSkeletonMessageHandler &rpc,int nResource_id,int nResource_parent_id)\n{\n",
				strNewClass.toLatin1().constData(),
				lstMethods[i].strName.toLatin1().constData());
	

			//fprintf(pOutSkeletonCpp, "\tStatus err;\n\n");

			//find metadata:
			QString strFunctName=lstMethods.at(i).strName;
			MetaData metaFunct = GetFunctionMetaData(lstMetaData,strFunctName);

			//only incoming parameters are in xml incoming structure:
			int nCountInputParameter=0;
			for(int j=1; j<lstMethods[i].lstParams.size(); j++)
			{
				if(!(lstMethods[i].lstParams[j].strName.indexOf("Ret_") == 0))
				{
					if (metaFunct.strResourceIDParam==lstMethods[i].lstParams[j].strName)
						continue;
					else if(metaFunct.strResourceParentIDParam==lstMethods[i].lstParams[j].strName)
						continue;
					else
						nCountInputParameter++;
				}
			}

			if (nCountInputParameter<0) nCountInputParameter=0;

			//only test param count if we are gonna extract 'em
			if (nCountInputParameter != 0)
			{
				fprintf(pOutSkeletonCpp, "\t//test parameter count\n");
				fprintf(pOutSkeletonCpp, "\tif(rpc.msg_in->GetParameterCount()!=%d)\n", nCountInputParameter);
				fprintf(pOutSkeletonCpp, "\t{err.setError(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);\n\trpc.msg_out->GenerateFault(StatusCodeSet::ERR_RPC_PARAMETER_COUNT_MISMATCH);return;}\n");
			}


			//NOTE: ignore 1st params when generating local varables
			//NOTE: all parameters with <DEFINE_LIST> just append to the function body...
			fprintf(pOutSkeletonCpp, "\n\t//extract parameters to local vars\n");
			for(int j=1; j<lstMethods[i].lstParams.size(); j++){
				QString strType = lstMethods[i].lstParams[j].strType;
				strType = strType.replace("&", "");
				fprintf(pOutSkeletonCpp, "\t%s %s;\n",
					strType.toLatin1().constData(),
					lstMethods[i].lstParams[j].strName.toLatin1().constData());

				ReturnParameter retdata;
				if (GetReturnParameterData(lstMetaData, lstMethods[i].strName, lstMethods[i].lstParams[j].strName,retdata))
				{
					if (!retdata.strListDefintion.isEmpty())
					{
						fprintf(pOutSkeletonCpp, "\t%s\n",
							retdata.strListDefintion.toLatin1().constData());
					}
				}
			}


			//Take RES_ID and RES_PARENT_ID parameters and replace them as is:
			fprintf(pOutSkeletonCpp, "\n\t//getParam (Only incoming & RETURNED-OUTGOING) from client\n");
			int nOffsetInputParameter=0; /* can be different then actual pos of input parameter */
			for(int j=1; j<lstMethods[i].lstParams.size(); j++)
			{
				if(!(lstMethods[i].lstParams[j].strName.indexOf("Ret_") == 0))
				{
					if (metaFunct.strResourceIDParam==lstMethods[i].lstParams[j].strName)
					{
						fprintf(pOutSkeletonCpp, "\t%s=nResource_id;\n",lstMethods[i].lstParams[j].strName.toLatin1().constData());
						nOffsetInputParameter--;
					}
					else if(metaFunct.strResourceParentIDParam==lstMethods[i].lstParams[j].strName)
					{
						fprintf(pOutSkeletonCpp, "\t%s=nResource_parent_id;\n",lstMethods[i].lstParams[j].strName.toLatin1().constData());
						nOffsetInputParameter--;
					}
					else
					{
						fprintf(pOutSkeletonCpp, "\trpc.msg_in->GetParameter(%d,&%s,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}\n",
							nOffsetInputParameter, lstMethods[i].lstParams[j].strName.toLatin1().constData());
					}
					nOffsetInputParameter++;
				}
			}
//rpc.msg_in->GetParameter(0,&strAliasDirectory,err);if (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}
			fprintf(pOutSkeletonCpp, "\n\t//invoke handler\n");

			if (!bGenerateForSingleServiceHandler)
			{
				fprintf(pOutSkeletonCpp, "\tm_Handler->%s(err", lstMethods[i].strName.toLatin1().constData());
			}
			else
			{
				if (strBaseName.toLower()!="service")
				{
					QString strAddOn=strBaseName.mid(strBaseName.lastIndexOf("_"));
					fprintf(pOutSkeletonCpp, "\tg_ServiceHandler"+strAddOn.toLatin1()+"->%s(err", lstMethods[i].strName.toLatin1().constData());
				}
				else
					fprintf(pOutSkeletonCpp, "\tg_ServiceHandler->%s(err", lstMethods[i].strName.toLatin1().constData());
			}

			for(int j=1; j<lstMethods[i].lstParams.size(); j++){
				fprintf(pOutSkeletonCpp, ",%s", lstMethods[i].lstParams[j].strName.toLatin1().constData());
			}
			fprintf(pOutSkeletonCpp, ");\n");

			fprintf(pOutSkeletonCpp, "\n\t//clear outgoing buffer\n\trpc.msg_out->ClearData();\n");
			fprintf(pOutSkeletonCpp, "\t//test if error: then return only error:\n");
			fprintf(pOutSkeletonCpp, "\tif(!err.IsOK())\n\t{rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}\n");
			fprintf(pOutSkeletonCpp, "\n\t//return RETURNED variables to the client: (skip pStatus)\n\t//start from first RETURNED var:\n");

			for(int j=1; j<lstMethods[i].lstParams.size(); j++)
			{
				if(lstMethods[i].lstParams[j].strName.indexOf("Ret") == 0)
				{
					/* BT: add inteligent AddParameter: parse XML meta data and change output names: so xml output names are different then internaly used */
					ReturnParameter retdata;
					if (GetReturnParameterData(lstMetaData, lstMethods[i].strName, lstMethods[i].lstParams[j].strName,retdata))
					{
						if (retdata.strRowName!="")
						{
							if (retdata.strRowURLAttr.contains("\""))
							{
								//retdata.strRowURLAttr = retdata.strRowURLAttr.replace("\"","\\\"");
								QString strOut=QString("\trpc.msg_out->AddParameter(&%1,\"%2\",\"%3\",%4,\"%5\");\n").arg(retdata.strName).arg(retdata.strDisplayName).arg(retdata.strRowName).arg(retdata.strRowURLAttr).arg(retdata.strRowURLAttr_IDField);
								fprintf(pOutSkeletonCpp, "%s", strOut.toLatin1().constData());
								
								//fprintf(pOutSkeletonCpp, "\trpc.msg_out->AddParameter(&%s,\"%s\",\"%s\",%s,\"%s\");\n",
								//	retdata.strName.toLatin1().constData(),retdata.strDisplayName.toLatin1().constData(),retdata.strRowName.toLatin1().constData(),retdata.strRowURLAttr.toLatin1().constData(),retdata.strRowURLAttr_IDField.toLatin1().constData());
							}
							else
								fprintf(pOutSkeletonCpp, "\trpc.msg_out->AddParameter(&%s,\"%s\",\"%s\",\"%s\",\"%s\");\n",
									retdata.strName.toLatin1().constData(),retdata.strDisplayName.toLatin1().constData(),retdata.strRowName.toLatin1().constData(),retdata.strRowURLAttr.toLatin1().constData(),retdata.strRowURLAttr_IDField.toLatin1().constData());
						}
						else
							fprintf(pOutSkeletonCpp, "\trpc.msg_out->AddParameter(&%s,\"%s\");\n",
								retdata.strName.toLatin1().constData(),retdata.strDisplayName.toLatin1().constData());
					}
					else
					{
						fprintf(pOutSkeletonCpp, "\trpc.msg_out->AddParameter(&%s,\"%s\");\n",
							lstMethods[i].lstParams[j].strName.toLatin1().constData(),lstMethods[i].lstParams[j].strName.toLatin1().constData());
					}
				}
			}

			fprintf(pOutSkeletonCpp, "\n\t//generate response\n");
			fprintf(pOutSkeletonCpp, "\trpc.msg_out->GenerateResponse(err);\n");
			fprintf(pOutSkeletonCpp, "\tif (!err.IsOK()){rpc.msg_out->GenerateFault(err.getErrorCode(),err.getErrorText());return;}\n}\n\n");
		}

		fclose(pOutSkeletonCpp);
	}

	return true;
}

bool RpcGenerator::GetReturnParameterData(QList<MetaData> &MetaData, QString strFunction, QString strParameterName,ReturnParameter &data)
{
	int nSize=MetaData.size();
	for (int i=0;i<nSize;i++)
	{
		if (MetaData.at(i).strFunctionName==strFunction)
		{
			int nSize2=MetaData.at(i).lstReturnParametarData.size();
			for (int k=0;k<nSize2;k++)
			{
				if (strParameterName==MetaData.at(i).lstReturnParametarData.at(k).strName)
				{
					data=MetaData.at(i).lstReturnParametarData.at(k);
					return true;
				}
			}
		}
	}
	return false;
}

MetaData RpcGenerator::GetFunctionMetaData(QList<MetaData> &lstMetaData, QString strFunction)
{
	int nSize=lstMetaData.size();
	for (int i=0;i<nSize;i++)
	{
		if (lstMetaData.at(i).strFunctionName==strFunction)
		{
			return lstMetaData.at(i);
		}
	}

	//Q_ASSERT(false);
	MetaData emptyVar;
	return emptyVar;
}



bool RpcGenerator::LoadFile(QString strInputFile, QList<QString> &lstInput)
{
	FILE *pInFile = fopen(strInputFile.toLatin1().constData(), "r");
	if(NULL == pInFile){
		QMessageBox::information(this, "Error", "Failed to open input file!");
		return false;
	}

	//load input as a set of lines
	char szBuffer[1024];
	while(NULL != fgets(szBuffer, sizeof(szBuffer), pInFile))
		lstInput.append(QString(szBuffer));
	fclose(pInFile);

	return true;
}

void RpcGenerator::FilterInput(QList<QString> &lstInput)
{
	//filter the input lines
	int nIdx = 0;
	while(nIdx < lstInput.size())
	{
		//trim lines
		lstInput[nIdx] = lstInput[nIdx].trimmed();

		//cut C++ comments
		int nPos = lstInput[nIdx].indexOf("//");
		if(nPos >= 0)
			lstInput[nIdx] = lstInput[nIdx].left(nPos);

		//remove some unneeded lines
		if( lstInput[nIdx].indexOf("#include") >= 0 ||
			lstInput[nIdx].indexOf("#ifndef") >= 0  ||
			lstInput[nIdx].indexOf("#define") >= 0  ||
			lstInput[nIdx].indexOf("#endif") >= 0   ||
			lstInput[nIdx].size() < 1 ||	//empty line
			lstInput[nIdx] == "{" ||
			lstInput[nIdx] == "};")
		{
			lstInput.removeAt(nIdx);
		}
		else
			nIdx ++;
	}

	//TOFIX add support for multiple comment blocks in the same line
	// remove C comment blocks (/**/)
	
	bool bInsideBlock = false;

	nIdx = 0;
	while(nIdx < lstInput.size())
	{
		// if we are inside a comment then we search for the end of the comment */
		if(bInsideBlock)
		{
			//search block end
			int nPos = lstInput[nIdx].indexOf("*/");
			if(nPos >= 0)
			{
				bInsideBlock = false;
				lstInput[nIdx] = lstInput[nIdx].right(lstInput[nIdx].size()-nPos-2);
				if(lstInput[nIdx].size() < 1)
					lstInput.removeAt(nIdx);	//remove empty line
				else
					nIdx ++;
			}
			else
				lstInput.removeAt(nIdx);	//remove entire line - inside block
		}
		else
		{
			//search block start
			int nPos = lstInput[nIdx].indexOf("/*");

			if(nPos >= 0)
			{
				bInsideBlock = true;
				lstInput[nIdx] = lstInput[nIdx].left(nPos);
				if(lstInput[nIdx].size() < 1)
					lstInput.removeAt(nIdx);	//remove empty line
				else
				{
					//see if the comment ends on the same trimmed line
					nPos = lstInput[nIdx].indexOf("*/");
					if(nPos >= 0)
					{
						bInsideBlock = false;
						lstInput[nIdx] = lstInput[nIdx].right(lstInput[nIdx].size()-nPos-2);
						if(lstInput[nIdx].size() < 1)
							lstInput.removeAt(nIdx);	//remove empty line
						else
							nIdx ++;
					}
					else
						nIdx ++;
				}
			}
			else
				nIdx ++;
		}
	}
}


/*
	For parsing interface header with meta data tag <Web_service_meta_data>
	Returns all metadata for each functions
	Returns false if XML is not structured properly, or file not on path
*/

bool RpcGenerator::ParseMetaData(QString strInputFile, QList<MetaData> &lstMetaData, QString &strNameSpace)
{
	//Load File
	QByteArray buffer;
    QFile in_file(strInputFile);
    if(in_file.open(QIODevice::ReadOnly))
        buffer=in_file.readAll();
	else
		return false;


	//Extract xml data
	int nPos = buffer.indexOf("<Web_service_meta_data>");
	if(nPos<0)
		return false;

	int nPos2 = buffer.indexOf("</Web_service_meta_data>");
	if(nPos2<0)
		return false;
	nPos2 += QByteArray("</Web_service_meta_data>").length()+1;


	QByteArray xml_data=buffer.mid(nPos,nPos2-nPos).trimmed();

	QDomDocument xml_doc;
	xml_doc.setContent(xml_data);


	//Parse xml data -> lstMetaData
	//parse by name: set tree items:
	//-------------------------------------------------------------
	//				PARSE XML
	//-------------------------------------------------------------

	QDomNodeList ListOfElements	= xml_doc.elementsByTagName("Web_service_meta_data");
	if (ListOfElements.count()==0)
	{
		return false;
	}
	QDomNodeList allNodes=ListOfElements.at(0).childNodes();
	if (allNodes.count()==0)
	{		
		return false;
	}

	int nSize2=allNodes.count();
	for(int i=0;i<nSize2;i++)
	{
		MetaData functData;
		QList<ReturnParameter> lstRetData;

		QDomNode functNode=allNodes.at(i);

		functData.strFunctionName = functNode.nodeName();
		
		QDomNodeList ListOfElements	= functNode.childNodes(); 
		int nSize=ListOfElements.count();
		for(int k=0;k<nSize;k++)
		{
			QDomNode  childNode = ListOfElements.at(k);
			QDomElement Element =  childNode.toElement();
			QString name = Element.tagName();
			QString text = Element.text();
			if (name=="URL")
			{
				if(k==0)
				{
					strNameSpace = text.trimmed();
					int slashPos1 = strNameSpace.indexOf("/");
					int slashPos2   = strNameSpace.indexOf("/",slashPos1+1); 
					int strLength = slashPos2 - slashPos1-1;
					strNameSpace = strNameSpace.mid(slashPos1+1,strLength);
				} 
				functData.strUrlName=text;
			}
			else if (name=="HTTP_METHOD")
				functData.strHttpMethodName=text;
			else if (name=="REQUEST_XML_SCHEMA")
				functData.strRequestXmlSchema=text;
			else if (name=="RESPONSE_XML_SCHEMA")
				functData.strResponseXmlSchema=text;
			else if (name=="RES_ID")
				functData.strResourceIDParam=text;
			else if (name=="RES_PARENT_ID")
				functData.strResourceParentIDParam=text;
			else /* parameter definition detected */
			{
				//parameter name is tag:
				ReturnParameter RetParamData;
				RetParamData.strName=name;
				QDomNodeList ListOfRetDataElements	= childNode.childNodes();
				int nSize5=ListOfRetDataElements.count();
				for(int z=0;z<nSize5;z++)
				{
					QDomNode  childNode2 = ListOfRetDataElements.at(z);
					QDomElement Element2 =  childNode2.toElement();
					QString name2 = Element2.tagName();
					QString text2 = Element2.text();
					if (name2=="DISPLAY_NAME")
					{
						RetParamData.strDisplayName=text2;
					}
					else if (name2=="ROW_NAME")
					{
						RetParamData.strRowName=text2;
					}
					else if (name2=="ROW_URL_ATTR")
					{
						RetParamData.strRowURLAttr=text2;
					}
					else if (name2=="ROW_URL_ATTR_ID_FIELD")
					{
						RetParamData.strRowURLAttr_IDField=text2;
					}
					else if (name2=="DEFINE_LIST")
					{
						RetParamData.strListDefintion=text2;
					}
				}

				lstRetData.append(RetParamData);
			}

		}

		functData.lstReturnParametarData=lstRetData;
		lstMetaData.append(functData);
	}

	return true;

}

