#ifndef INTERFACE_WEBCONTACTS_H
#define INTERFACE_WEBCONTACTS_H

#include "common/common/status.h"
#include "common/common/dbrecordset.h"

class Interface_WebContacts
{
public:
	virtual ~Interface_WebContacts(){};

	virtual void ReadContacts(Status &Ret_pStatus, DbRecordSet &Ret_Contacts)=0; 
	virtual void ReadContactsWithFilter(Status &Ret_pStatus, int &Ret_nTotalCount,DbRecordSet &Ret_Contacts, int nFilter=-1,int nFromN=-1, int nToN=-1, QString strFromLetter="", QString strToLetter="")=0; 
	virtual void ReadContactDetails(Status &Ret_pStatus, int nContactID, DbRecordSet &Ret_ContactData,DbRecordSet &Ret_Emails,DbRecordSet &Ret_WebSites,DbRecordSet &Ret_Phones,DbRecordSet &Ret_Sms,DbRecordSet &Ret_Sykpe,DbRecordSet &Ret_Addresses) = 0;
	virtual void ModifyContact(Status &Ret_pStatus, DbRecordSet &Ret_ContactData) = 0;
	virtual void AddContact(Status &Ret_pStatus, DbRecordSet &ContactData, int &Ret_nNewContactID) = 0;
	virtual void DeleteContact(Status &Ret_pStatus, int nContactID) = 0;

	virtual void SearchContacts(Status &Ret_pStatus, int &Ret_nTotalCount, DbRecordSet &Ret_Contacts,int nSearchType, QString strField1_Name, QString strField1_Value, QString strField2_Name, QString strField2_Value, int nGroupID=-1,int nFromN=-1, int nToN=-1, QString strFromLetter="", QString strToLetter="") = 0;
	virtual void ReadFavorites(Status &Ret_pStatus, DbRecordSet &Ret_Favorites) = 0;
	virtual void ReadContactAddresses(Status &Ret_pStatus, int nContactID,DbRecordSet &Ret_Emails,DbRecordSet &Ret_WebSites,DbRecordSet &Ret_Phones,DbRecordSet &Ret_Sms,DbRecordSet &Ret_Sykpe,DbRecordSet &Ret_Addresses) = 0;

	virtual void ReadContactDocuments(Status &Ret_pStatus, int &Ret_nTotalCount,DbRecordSet &Ret_Documents,  int nContactID,int nDocType=-1, int nFromN=-1, int nToN=-1)=0;
	virtual void ReadContactEmails(Status &Ret_pStatus, int &Ret_nTotalCount, DbRecordSet &Ret_Emails, int nContactID, int nFromN=-1, int nToN=-1)=0;
	virtual void ReadCommEntityCount(Status &Ret_pStatus, int nContactID, int &Ret_nFileDocCount, int &Ret_nWebSiteCount, int &Ret_nNotesCount,int &Ret_nMailCount)=0;

};

/*
<Web_service_meta_data>
	
	<ReadContacts>
		<URL>/contacts</URL>
		<HTTP_METHOD>GET</HTTP_METHOD>
		<RESPONSE_XML_SCHEMA>contacts_response.xsd</RESPONSE_XML_SCHEMA>
		<SERVICE_DESC_XML>contacts_contacts.xml</SERVICE_DESC_XML>
		<Ret_Contacts>
			<SCHEMA>TVIEW_BUS_CONTACT_SELECTION</SCHEMA>
			<ROW_NAME>contact</ROW_NAME>
			<ROW_URL>contact/[BCNT_ID]</ROW_URL>
		</Ret_Contacts>
	</ReadContacts>

	<SearchContacts>
		<URL>/contacts/search</URL>
		<HTTP_METHOD>GET</HTTP_METHOD>
		<REQUEST_XML_SCHEMA>contacts_search_request.xsd</REQUEST_XML_SCHEMA>
		<RESPONSE_XML_SCHEMA>contacts_search_response.xsd</RESPONSE_XML_SCHEMA>
		<SERVICE_DESC_XML>contacts_contacts.xml</SERVICE_DESC_XML>
		<Ret_Contacts>
			<SCHEMA>TVIEW_BUS_CONTACT_SELECTION</SCHEMA>
			<ROW_NAME>contact</ROW_NAME>
			<ROW_URL>contact/[BCNT_ID]</ROW_URL>
		</Ret_Contacts>
	</SearchContacts>
	
	<ReadFavorites>
		<URL>/contacts/favorites</URL>
		<HTTP_METHOD>GET</HTTP_METHOD>
		<REQUEST_XML_SCHEMA>contacts_favorites_request.xsd</REQUEST_XML_SCHEMA>
		<RESPONSE_XML_SCHEMA>contacts_favorites_response.xsd</RESPONSE_XML_SCHEMA>
		<SERVICE_DESC_XML>contacts_contacts.xml</SERVICE_DESC_XML>
		<Ret_Favorites>
			<SCHEMA>TVIEW_BUS_CONTACT_SELECTION_WITH_PICS</SCHEMA>
			<ROW_NAME>contact</ROW_NAME>
			<ROW_URL>contact/[BCNT_ID]</ROW_URL>
		</Ret_Favorites>
	</ReadFavorites>
	
	<ReadContactAddresses>
		<URL>/contact/[RES_ID]/addresses</URL>
		<HTTP_METHOD>GET</HTTP_METHOD>
		<REQUEST_XML_SCHEMA>contacts_addresses_request.xsd</REQUEST_XML_SCHEMA>
		<RESPONSE_XML_SCHEMA>contacts_addresses_response.xsd</RESPONSE_XML_SCHEMA>
		<SERVICE_DESC_XML>contacts_contacts.xml</SERVICE_DESC_XML>
		<Ret_Emails>
			<SCHEMA>TVIEW_WEBSERVICE_BUS_CM_EMAIL_SELECT</SCHEMA>
			<ROW_NAME>email</ROW_NAME>
		</Ret_Emails>
		<Ret_WebSites>
			<SCHEMA>TVIEW_WEBSERVICE_BUS_CM_INTERNET_SELECT</SCHEMA>
			<ROW_NAME>website</ROW_NAME>
		</Ret_WebSites>
		<Ret_Phones>
			<SCHEMA>TVIEW_WEBSERVICE_BUS_CM_PHONE_SELECT</SCHEMA>
			<ROW_NAME>phone</ROW_NAME>
		</Ret_Phones>
		<Ret_Sms>
			<SCHEMA>TVIEW_WEBSERVICE_BUS_CM_PHONE_SELECT</SCHEMA>
			<ROW_NAME>sms</ROW_NAME>
		</Ret_Sms>
		<Ret_Addresses>
			<SCHEMA>TVIEW_WEBSERVICE_BUS_CM_ADDRESS_SELECT</SCHEMA>
			<ROW_NAME>address</ROW_NAME>
		</Ret_Addresses>
	</ReadContactAddresses>
	
</Web_service_meta_data>
*/

#endif // INTERFACE_WEBCONTACTS_H
