#ifndef INTERFACE_SERVICEHANDLER_H
#define INTERFACE_SERVICEHANDLER_H

#include "common/common/status.h"
#include "common/common/dbrecordset.h"

class Interface_ServiceHandler
{
public:

	virtual ~Interface_ServiceHandler(){};

	virtual void ReadCatalog(Status &pStatus, int &Ret_nTotalCount, DbRecordSet &Ret_Files, DbRecordSet &Ret_SubFolders, QString strAliasDirectory, int nFromN=-1,int nToN=-1, int nSortOrder=0, int nPicWidth=75,int nPicHeight=75, bool bKeepAspectRatio=false, bool bReturnThumbAsBinary=false)=0;
	virtual void GetPicture(Status &pStatus, int &Ret_nTotalCount, int &Ret_nCurrentPos, DbRecordSet &Ret_Files, QString strAliasDirectory, QString strFileName, int nHeading=0, int nSortOrder=0, int nPicWidth=75,int nPicHeight=75, bool bKeepAspectRatio=false, bool bReturnThumbAsBinary=false)=0;
	virtual void RotatePicture(Status &pStatus, DbRecordSet &Ret_Files, QString strAliasDirectory, QString strFileName, int nAngle=90,int nPicWidth=75,int nPicHeight=75, bool bKeepAspectRatio=false, bool bReturnThumbAsBinary=false)=0;
	virtual void DeleteDocument(Status &pStatus, QString strAliasDirectory,QString strFileName)=0;
	virtual void UploadDocument(Status &pStatus, QString strAliasDirectory,QString strFileName,QByteArray &byteFileData, bool bOverWrite=true,QString strSession="")=0;
	virtual void DownloadDocument(Status &pStatus, QString strAliasDirectory,QString strFileName,QByteArray &Ret_byteFileData,QString &strThumbLink,bool bReturnThumbAsBinary=false)=0;
	virtual void DownloadPicture(Status &pStatus, DbRecordSet &Ret_Files, QString strAliasDirectory,DbRecordSet &FileNames, int nFull, int nPicWidth=75,int nPicHeight=75, bool bReturnThumbAsBinary=false)=0;
	virtual void HidePicture(Status &pStatus, QString strAliasDirectory, QString strFileName,int nHide=0)=0;
	virtual void GetHidePictureList(Status &pStatus, QString strAliasDirectory, DbRecordSet &Ret_Files, int nPicWidth,int nPicHeight, bool bReturnThumbAsBinary)=0;
	virtual void RenamePicture(Status &pStatus, QString strAliasDirectory, QString strFileName, QString strDisplayName)=0;
	
	virtual void GetCatalogs(Status &pStatus, DbRecordSet &Ret_Catalogs)=0;
	virtual void GetCatalogs_iPad(Status &pStatus, DbRecordSet &Ret_Catalogs)=0;
	virtual void GetCatalogPicture(Status &pStatus, QString strCatalogName, QByteArray &Ret_byteFileData, QByteArray &Ret_byteFileDataLarge, int &Ret_nTotalCount, QString &Ret_strName)=0;
	virtual void RenameCatalog(Status &pStatus, QString strOldName, QString strNewName)=0;
	virtual void RenameCatalogClient(Status &pStatus, QString strOldName, QString strNewName)=0;
	virtual void CreateCatalogClient(Status &pStatus, QString strCatalogName)=0;
	virtual void UnpublishCatalogClient(Status &pStatus, QString strAliasDirectory)=0;

	virtual void Login(Status& pStatus, QString& RetOut_strSessionID,QString strUserName, QString strAuthToken,QString strClientNonce,int nProgCode=0, QString strProgVer="", QString strClientID="", QString strPlatform="")=0;
	virtual void Logout(Status &pStatus,QString strSessionID)=0;
	virtual void GetFolderStatus(Status &pStatus, DbRecordSet &Ret_Folders,int &Ret_nSettingsChanged)=0;
	virtual void MakeInvitation(Status &pStatus, QString &Ret_strBody, int nMessageType, QString strCatalogName, QString strFirstName, QString strLastName,QString strEmail, QString strPhone,int nIsHtml,QString strEmailSubject,QString strEmailBody)=0;

	virtual void GetServerData(Status &pStatus, QString &Ret_strName,QByteArray &Ret_SrvPic,QString &Ret_strEmailSignature,int &nMajor,int &nMinor, int &nRev)=0;
	virtual void ReloadSettings(Status &pStatus, bool bSkipReloadCatalogs=false)=0;


};

/*
<Web_service_meta_data>
	
	<ReadCatalog>
		<URL>/service/catalog/read</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
		<REQUEST_XML_SCHEMA></REQUEST_XML_SCHEMA>
		<RESPONSE_XML_SCHEMA></RESPONSE_XML_SCHEMA>
	</ReadCatalog>

	<GetPicture>   
		<URL>/service/document/picture</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
		<REQUEST_XML_SCHEMA></REQUEST_XML_SCHEMA>
		<RESPONSE_XML_SCHEMA></RESPONSE_XML_SCHEMA>
	</GetPicture>

	<RotatePicture>
		<URL>/service/catalog/picture/rotate</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
		<REQUEST_XML_SCHEMA></REQUEST_XML_SCHEMA>
		<RESPONSE_XML_SCHEMA></RESPONSE_XML_SCHEMA>
	</RotatePicture>

	<DeleteDocument>
		<URL>/service/document/delete</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
		<REQUEST_XML_SCHEMA></REQUEST_XML_SCHEMA>
		<RESPONSE_XML_SCHEMA></RESPONSE_XML_SCHEMA>
	</DeleteDocument>

	<UploadDocument>
		<URL>/service/document/upload</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
		<REQUEST_XML_SCHEMA></REQUEST_XML_SCHEMA>
		<RESPONSE_XML_SCHEMA></RESPONSE_XML_SCHEMA>
	</UploadDocument>

	<DownloadDocument>
		<URL>/service/document/download</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
		<REQUEST_XML_SCHEMA></REQUEST_XML_SCHEMA>
		<RESPONSE_XML_SCHEMA></RESPONSE_XML_SCHEMA>
	</DownloadDocument>

	<DownloadPicture>
		<URL>/service/catalog/picture/download</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
		<REQUEST_XML_SCHEMA></REQUEST_XML_SCHEMA>
		<RESPONSE_XML_SCHEMA></RESPONSE_XML_SCHEMA>
	</DownloadPicture>

	<HidePicture>
		<URL>/service/catalog/picture/hide</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
		<REQUEST_XML_SCHEMA></REQUEST_XML_SCHEMA>
		<RESPONSE_XML_SCHEMA></RESPONSE_XML_SCHEMA>
	</HidePicture>

	<GetHidePictureList>
		<URL>/service/catalog/picture/hidelist</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
		<REQUEST_XML_SCHEMA></REQUEST_XML_SCHEMA>
		<RESPONSE_XML_SCHEMA></RESPONSE_XML_SCHEMA>
	</GetHidePictureList>

	<RenamePicture>
		<URL>/service/catalog/picture/rename</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
		<REQUEST_XML_SCHEMA></REQUEST_XML_SCHEMA>
		<RESPONSE_XML_SCHEMA></RESPONSE_XML_SCHEMA>
	</RenamePicture>

	<GetCatalogs>
		<URL>/service/catalogs</URL>
		<HTTP_METHOD>GET</HTTP_METHOD>
		<REQUEST_XML_SCHEMA></REQUEST_XML_SCHEMA>
		<RESPONSE_XML_SCHEMA></RESPONSE_XML_SCHEMA>
	</GetCatalogs>

	<GetCatalogs_iPad>
		<URL>/service/catalogs_ipad</URL>
		<HTTP_METHOD>GET</HTTP_METHOD>
		<REQUEST_XML_SCHEMA></REQUEST_XML_SCHEMA>
		<RESPONSE_XML_SCHEMA></RESPONSE_XML_SCHEMA>
	</GetCatalogs_iPad>

	<GetCatalogPicture>
		<URL>/service/catalog/picture</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
		<REQUEST_XML_SCHEMA></REQUEST_XML_SCHEMA>
		<RESPONSE_XML_SCHEMA></RESPONSE_XML_SCHEMA>
	</GetCatalogPicture>

	<RenameCatalog>
		<URL>/service/catalog/rename</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
		<REQUEST_XML_SCHEMA></REQUEST_XML_SCHEMA>
		<RESPONSE_XML_SCHEMA></RESPONSE_XML_SCHEMA>
	</RenameCatalog>

	<RenameCatalogClient>
		<URL>/service/catalog/rename_client</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
		<REQUEST_XML_SCHEMA></REQUEST_XML_SCHEMA>
		<RESPONSE_XML_SCHEMA></RESPONSE_XML_SCHEMA>
	</RenameCatalogClient>

	<CreateCatalogClient>
		<URL>/service/catalog/create_client</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
		<REQUEST_XML_SCHEMA></REQUEST_XML_SCHEMA>
		<RESPONSE_XML_SCHEMA></RESPONSE_XML_SCHEMA>
	</CreateCatalogClient>

	<UnpublishCatalogClient>
		<URL>/service/catalog/unpublish_client</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
		<REQUEST_XML_SCHEMA></REQUEST_XML_SCHEMA>
		<RESPONSE_XML_SCHEMA></RESPONSE_XML_SCHEMA>
	</UnpublishCatalogClient>

	<Login>
		<URL>/service/login</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
		<REQUEST_XML_SCHEMA></REQUEST_XML_SCHEMA>
		<RESPONSE_XML_SCHEMA></RESPONSE_XML_SCHEMA>
	</Login>

	<Logout>
		<URL>/service/Logout</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
		<REQUEST_XML_SCHEMA></REQUEST_XML_SCHEMA>
		<RESPONSE_XML_SCHEMA></RESPONSE_XML_SCHEMA>
	</Logout>

	<GetFolderStatus>
		<URL>/service/catalogs/status</URL>
		<HTTP_METHOD>GET</HTTP_METHOD>
		<REQUEST_XML_SCHEMA></REQUEST_XML_SCHEMA>
		<RESPONSE_XML_SCHEMA></RESPONSE_XML_SCHEMA>
	</GetFolderStatus>

	<MakeInvitation>
		<URL>/service/catalog/invite</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
		<REQUEST_XML_SCHEMA></REQUEST_XML_SCHEMA>
		<RESPONSE_XML_SCHEMA></RESPONSE_XML_SCHEMA>
	</MakeInvitation>

	<GetServerData>
		<URL>/service/settings/server_data</URL>
		<HTTP_METHOD>GET</HTTP_METHOD>
		<REQUEST_XML_SCHEMA></REQUEST_XML_SCHEMA>
		<RESPONSE_XML_SCHEMA></RESPONSE_XML_SCHEMA>
	</GetServerData>

	<ReloadSettings>
		<URL>/service/settings/reload</URL>
		<HTTP_METHOD>POST</HTTP_METHOD>
		<REQUEST_XML_SCHEMA></REQUEST_XML_SCHEMA>
		<RESPONSE_XML_SCHEMA></RESPONSE_XML_SCHEMA>
	</ReloadSettings>
	
</Web_service_meta_data>
*/

#endif // INTERFACE_SERVICEHANDLER_H

	
	
	