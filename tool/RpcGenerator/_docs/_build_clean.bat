@ECHO OFF
REM 
REM Script to clean Atol project tree on windows (delete temporary files)
REM 	NOTE: requires some recent Windows (NT based)
REM

IF NOT "%OS%"=="Windows_NT" GOTO :End

rmdir /S /Q ..\Debug
rmdir /S /Q ..\Release
rmdir /S /Q ..\GeneratedFiles
rmdir /S /Q ..\Deleted
del /Q ..\*.ncb

rmdir /S /Q ..\RpcGenerator\Debug
rmdir /S /Q ..\RpcGenerator\Release
rmdir /S /Q ..\RpcGenerator\GeneratedFiles
rmdir /S /Q ..\RpcGenerator\Deleted

del /Q ..\RpcGenerator\*.user
del /Q ..\RpcGenerator\*.aps
del /F /Q /A H ..\*.suo


:End
