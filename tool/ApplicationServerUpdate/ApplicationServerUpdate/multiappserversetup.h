#ifndef MULTIAPPSERVERSETUP_H
#define MULTIAPPSERVERSETUP_H

#include <QObject>

class MultiAppServerSetup : public QObject
{
	Q_OBJECT

public:
	MultiAppServerSetup(QObject *parent);
	~MultiAppServerSetup();

	static void Start(QString strPrefix,int nStartPort,int nStartCount, int nEndCount,QString strSourceDir,QString strTargetDir,QString strBackupDir="",QString strDataDir="");
	static bool executeShellCommand(QString strCommand);
	static int SetUpdateTimes(QString strPrefix,int nStartCount, int nEndCount,QString strTargetDir,QString strUpdateTime,QString strWarningTime, int nMinuteIncrement);
	static int ActivateUpdateDate(QString strPrefix,int nStartCount, int nEndCount,QString strTargetDir,QString strUpdateDate);
	static int UpdateServer(int nWaitMinutesBeforeUpdate=0);

private:
	
};

#endif // MULTIAPPSERVERSETUP_H
