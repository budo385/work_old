#include <QtCore/QCoreApplication>
#include "multiappserversetup.h"
#include <QDebug>
#include <QTimer>

int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);

	if (argc>=2)
	{
		QString strMode=QString(argv[1]);

		if (strMode=="-setdate")
		{
			QString strPrefix=QString(argv[2]);
			int nStart=QString(argv[3]).toInt();
			int nEnd=QString(argv[4]).toInt();
			QString strTarget=QString(argv[5]);
			QString strDate=QString(argv[6]);
			return MultiAppServerSetup::ActivateUpdateDate(strPrefix,nStart,nEnd,strTarget,strDate);
		}

		if (strMode=="-settime")
		{
			QString strPrefix=QString(argv[2]);
			int nStart=QString(argv[3]).toInt();
			int nEnd=QString(argv[4]).toInt();
			QString strTarget=QString(argv[5]);
			QString strTime=QString(argv[6]);
			QString strWarning=QString(argv[7]);
			int nMin=QString(argv[8]).toInt();
			return MultiAppServerSetup::SetUpdateTimes(strPrefix,nStart,nEnd,strTarget,strTime,strWarning,nMin);
		}
		if (strMode=="-update")
		{
			int nWaitMinutesBeforeUpdate=0;
			if (argc==3)
				nWaitMinutesBeforeUpdate=QString(argv[2]).toInt();

			return MultiAppServerSetup::UpdateServer(nWaitMinutesBeforeUpdate);
		}
	}
		qDebug()<<"Usage:";
		qDebug()<<"-setdate strPrefix nStartCount nEndCount strTargetDir strUpdateDate";
		qDebug()<<"-settime strPrefix nStartCount nEndCount strTargetDir strUpdateTime strWarningTime nMinuteIncrement";
		qDebug()<<"-update nWaitMinutesBeforeUpdate";
		

	QTimer::singleShot(0,&a,SLOT(quit()));
	return a.exec();
}
