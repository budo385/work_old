#ifndef APPLICATIONSERVERCONFIGURATOR_H
#define APPLICATIONSERVERCONFIGURATOR_H

#include "bus_core/bus_core/serverinifile.h"
#include "common/common/status.h"

class ApplicationServerConfigurator 
{

public:
	ApplicationServerConfigurator();
	~ApplicationServerConfigurator();

	void SetAppServerPath(QString strPath){m_strAppSrvPath=strPath.replace("\\","/");}

	void LoadINIFile(Status &pStatus);
	ServerIniFile * GetINIFile(){return &m_INIFile;};
	bool SaveINIFile(){return m_INIFile.Save(m_strIniFilePath);}

	bool IsRunning(Status &pStatus);
	void Stop(Status &pStatus);
	void Start(Status &pStatus);

	bool IsAutoStart();
	QString LastError();

	void InstallService(Status &pStatus,QString strDbType);
	void UnInstallService(Status &pStatus);
	void SetAutoStart(bool bAuto);

	bool CreateUnInstallBat(QString strFileName="uninstall.bat");

	//DB
	bool IsServiceRunning(QString strService);
	bool IsServiceInstalled(QString strService);
	void InstallFirebirdService(Status &pStatus);

	QString m_strAppSrvPath;
private:
	ServerIniFile m_INIFile;
	QString m_strIniFilePath;
	
};

#endif // APPLICATIONSERVERCONFIGURATOR_H
