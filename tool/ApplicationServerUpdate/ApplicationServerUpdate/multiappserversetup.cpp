#include "multiappserversetup.h"
#include <QDir>
#include <QProcess>
#include <QVariant>
#include <QDebug>
#include <QCoreApplication>
#include "common/common/status.h"
#include "applicationserverconfigurator.h"
#include "db_core/db_core/dbconnsettings.h"
#include "common/common/datahelper.h"
#include "db_core/db_core/db_drivers.h"
#include "db/db/dbconnectionsetup.h"
#include "common/common/config.h"
#include "db/db/dbsqlmanager.h"
#include "db/db/dbsqlquery.h"
#include "common/common/logger.h"
#include "common/common/threadid.h"
#include "common/common/sleeper.h"

MultiAppServerSetup::MultiAppServerSetup(QObject *parent)
	: QObject(parent)
{

}

MultiAppServerSetup::~MultiAppServerSetup()
{

}


void MultiAppServerSetup::Start(QString strPrefix,int nStartPort,int nStartCount, int nEndCount,QString strSourceDir,QString strTargetDir,QString strBackupDir,QString strDataDir)
{

	strSourceDir=QDir::toNativeSeparators(strSourceDir);
	strTargetDir=QDir::toNativeSeparators(strTargetDir);

	nStartPort--;
	for(int i=nStartCount;i<=nEndCount;++i)
	{
		nStartPort++;

		QString strAppName=strPrefix+QVariant(i).toString();
		QDir targetDir(strTargetDir);
		targetDir.mkdir(strAppName);
		QString strTargetDirApp=strTargetDir+"\\"+strAppName;

		QString strCommand="xcopy \""+strSourceDir+ "\" \""+strTargetDirApp+"\" /S /E /Y";
		if(!executeShellCommand(strCommand))
			continue;

		QString strTargetDataFile=strTargetDirApp+"\\data\\DATA.FDB";
		if (!strDataDir.isEmpty())
		{
			strTargetDataFile=strDataDir+"\\"+strAppName+".FDB";
			QFile file(strTargetDirApp+"\\data\\DATA.FDB");
			if(!file.copy(strTargetDataFile))
			{
				qDebug()<<tr("Failed to copy database");
				continue;

			}
		}

		QString strBackupDirApp=strTargetDirApp+"\\backup";
		if (!strBackupDir.isEmpty())
		{
			strBackupDirApp=strBackupDir+"\\"+strAppName;
			QDir dirBcp(strBackupDir);
			dirBcp.mkdir(strAppName);
		}

		//create database connection to strTargetDataFile, save it
		Status err;
		QString strSettingsPath=strTargetDirApp+"/settings/database_connections.cfg";
		DbConnectionSettings settingsFireBird;
		settingsFireBird.m_strCustomName=strAppName;
		settingsFireBird.m_strDbName=strTargetDataFile;
		settingsFireBird.m_strDbUserName="SYSDBA";
		settingsFireBird.m_strDbPassword="masterkey";
		settingsFireBird.m_strDbType=DBTYPE_FIREBIRD;
		DbConnectionSetup::CreateDatabaseConnection(err,strSettingsPath,settingsFireBird);
		if (!err.IsOK())
		{
			qDebug()<<tr("Failed to create database connection: ")+strTargetDataFile;
			continue;
		}

		//copy appserver:
		/*
		QFile fileApp(strTargetDirApp+"\\template_data\\appserver.exe");
		if(!fileApp.copy(strTargetDirApp+"\\"+strAppName+".exe"))
		{
			qDebug()<<tr("Failed to copy appserver");
			continue;
		}
		*/

		//modify ini: backup+connection+port
		Status errTmp;
		ApplicationServerConfigurator AppSrv;
		AppSrv.SetAppServerPath(strTargetDirApp);
		AppSrv.LoadINIFile(err);
		if (!err.IsOK())
		{
			qDebug()<<tr("Failed to load ini file");
			continue;
		}

		AppSrv.UnInstallService(errTmp); //remove previous:

		//save all to ->INI/registry:
		AppSrv.GetINIFile()->m_strDescriptor=strAppName;
		AppSrv.GetINIFile()->m_nAppServerPort=nStartPort;
		AppSrv.GetINIFile()->m_strDBConnectionName=strAppName;
		AppSrv.GetINIFile()->m_strBackupPath=strBackupDirApp;
		if(!AppSrv.SaveINIFile())
		{
			qDebug()<<tr("Failed to save ini file");
			continue;
		}
/*
		strCommand=strTargetDirApp+"\\"+strAppName+".exe -i";
		if(!executeShellCommand(strCommand))
		{
			qDebug()<<tr("Failed to install service");
			continue;

		}

		QString strAppService=QString(SERVER_DESCRIPTION_TAG)+QString(" - ")+strAppName;
		DataHelper::Registry_SetKeyValue("HKEY_LOCAL_MACHINE","SYSTEM\\CurrentCOntrolSet\\Services\\"+strAppService,"DependOnService",2,"FirebirdServerDefaultInstance");
*/
	
		AppSrv.InstallService(err,DBTYPE_FIREBIRD);
		if (!err.IsOK())
		{
			qDebug()<<tr("Failed to install service");
			continue;
		}
	

		AppSrv.SetAutoStart(true);
		AppSrv.CreateUnInstallBat();
		AppSrv.Start(err);
		if(!err.IsOK())
		{
			qDebug()<<tr("Failed to start service");
			QString strLastError=AppSrv.LastError();
			if (!strLastError.isEmpty())
			{
				qDebug()<<tr("Error reported by application server: ")+strLastError;
			}
			continue;
		}

		qDebug()<<tr("Server ")+strAppName+tr(" successfully started at port: ")+QVariant(nStartPort).toString();
	}
}





bool MultiAppServerSetup::executeShellCommand(QString strCommand)
{
	QProcess app;

	app.start(strCommand);
	if (!app.waitForFinished(-1)) //wait forever
	{	
		qDebug()<<tr("Failed to execute command: ")+strCommand;
		return false;
	}
	int nExitCode=app.exitCode();
	QByteArray errContent=app.readAllStandardError();
	QByteArray fileContent=app.readAllStandardOutput();
	if (errContent.size()!=0)
	{

		qDebug()<<tr("Failed to execute command: ")+strCommand;
		qDebug()<<tr("Reason:")<<errContent;
		return false;
	}

	return true;
}

//servers must be shutdown before this:
int MultiAppServerSetup::SetUpdateTimes(QString strPrefix,int nStartCount, int nEndCount,QString strTargetDir,QString strUpdateTime,QString strWarningTime, int nMinuteIncrement)
{
	//read appserver.ini
	//set fields:
	QTime timeUpdate;
	timeUpdate=QTime::fromString(strUpdateTime,"hh:mm");

	bool bErrorOccured=false;

	strTargetDir=QDir::toNativeSeparators(strTargetDir);

	for(int i=nStartCount;i<=nEndCount;++i)
	{
		QString strAppName=strPrefix+QVariant(i).toString();
		if (nStartCount==nEndCount)
		{
			strAppName=strPrefix;
		}
		
		//QString strSettingsPath=strTargetDir+"/"+strAppName+"/settings/app_server.ini";

		//modify ini: backup+connection+port
		Status err;
		ApplicationServerConfigurator AppSrv;
		AppSrv.SetAppServerPath(strTargetDir+"\\"+strAppName);
		AppSrv.LoadINIFile(err);
		if (!err.IsOK())
		{
			qDebug()<<tr("Failed to load ini file for server: ")<<i;
			bErrorOccured=true;
			continue;
		}

		QString strTimeUpdate=timeUpdate.toString("hh:mm");
		
		//save all to ->INI/registry:
		AppSrv.GetINIFile()->m_strUpdateTime=strTimeUpdate;
		AppSrv.GetINIFile()->m_strWarningUpdateTime=strWarningTime;


		if(!AppSrv.SaveINIFile())
		{
			qDebug()<<tr("Failed to load ini file for server: ")<<i;
			bErrorOccured=true;
			continue;
		}

		qDebug()<<tr("Server ")+AppSrv.m_strAppSrvPath+tr(" successfully set update times");

		timeUpdate=timeUpdate.addSecs(nMinuteIncrement*60);
	}

	if (bErrorOccured)
		return 1;
	else
		return 0;
}

//server set date for update-> CORE_DATAABSE_INFO
int MultiAppServerSetup::ActivateUpdateDate(QString strPrefix,int nStartCount, int nEndCount,QString strTargetDir,QString strUpdateDate)
{
	QDate date=QDate::fromString(strUpdateDate,"dd.MM.yyyy");
	if(!date.isValid())
	{
		qDebug()<<tr("Invalid date");
		return 1;
	}

	bool bErrorOccured=false;

	strTargetDir=QDir::toNativeSeparators(strTargetDir);

	for(int i=nStartCount;i<=nEndCount;++i)
	{
		QString strAppName=strPrefix+QVariant(i).toString();
		if (nStartCount==nEndCount)
		{
			strAppName=strPrefix;
		}

		QString strTargetDirApp=strTargetDir+"\\"+strAppName;

		QString strConnectionName;
		Status err;
		ApplicationServerConfigurator AppSrv;
		AppSrv.SetAppServerPath(strTargetDirApp);
		AppSrv.LoadINIFile(err);
		if (!err.IsOK())
		{
			qDebug()<<tr("Failed to load ini file for server: ")<<i;
			bErrorOccured=true;
			continue;
		}
		strConnectionName=AppSrv.GetINIFile()->m_strDBConnectionName;
		QString strSettingsPath=strTargetDirApp+"\\settings\\database_connections.cfg";
		DbConnectionSettings settingsDB;
		if(!DbConnectionSetup::GetDatabaseConnection(err,strSettingsPath,strConnectionName,settingsDB))
		{
			qDebug()<<tr("Failed to load DB connection ")<<strConnectionName <<tr(" for server: ")<<strTargetDirApp;
			bErrorOccured=true;
			continue;
		}
		

		DbSqlManager pDbManager;
		pDbManager.Initialize(err,settingsDB,10);
		if (!err.IsOK())
		{
			qDebug()<<err.getErrorText();
			qDebug()<<tr("Failed to connect to DB file for server: ")<<strTargetDirApp;
			bErrorOccured=true;
			continue;
		}

		//update:
		QString strSQL="UPDATE CORE_DATABASE_INFO SET CDI_SERVER_UPDATE_DATE=?";
		DbSqlQuery query(err,&pDbManager);
		if (!err.IsOK())
		{
			qDebug()<<err.getErrorText();
			qDebug()<<tr("Failed to set up query to DB file for server: ")<<i<<" "<<err.getErrorText();
			bErrorOccured=true;
			continue;
		}
		query.Prepare(err,strSQL);
		if (!err.IsOK())
		{
			qDebug()<<err.getErrorText();
			qDebug()<<tr("Failed to prepare SQL to DB file for server: ")<<i<<" "<<err.getErrorText();
			bErrorOccured=true;
			continue;
		}
		query.bindValue(0,date);
		query.ExecutePrepared(err);
		if (!err.IsOK())
		{
			qDebug()<<err.getErrorText();
			qDebug()<<tr("Failed to prepare SQL to DB file for server: ")<<i<<" "<<err.getErrorText();
			bErrorOccured=true;
			continue;
		}

		qDebug()<<tr("Server ")+strAppName+tr(" successfully set update date:")<<strUpdateDate;
	}

	if (bErrorOccured)
		return 1;
	else
		return 0;

}

int MultiAppServerSetup::UpdateServer(int nWaitMinutesBeforeUpdate)
{

	if (nWaitMinutesBeforeUpdate>0)
	{
		qDebug()<< QDateTime::currentDateTime().toString()<<": waiting for "<<nWaitMinutesBeforeUpdate<<" minutes before starting update.";
		ThreadSleeper::Sleep(1000*nWaitMinutesBeforeUpdate*60);
	}

	qDebug()<< QDateTime::currentDateTime().toString()<<": starting update!";

	QString strSourceDir=QCoreApplication::applicationDirPath();
	QDir dir (QCoreApplication::applicationDirPath());
	dir.cdUp();
	QString strTargetDirApp=dir.absolutePath();



	/*
	if (!QFile::exists(strSourceDir+"/appserver.exe") || !QFile::exists(strTargetDirApp+"/db_actualize.dll"))
	{
		Logger log;
		log.EnableFileLogging(QCoreApplication::applicationDirPath()+"/debug_emergency.log");
		log.EnableConsoleLogging();
		log.SetLogLevel(4);
		int nThread=ThreadIdentificator::GetCurrentThreadID();
		log.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Paths are invalid!");
		return 1;
	}
	*/

	QString strConnectionName;
	Status err;
	ApplicationServerConfigurator AppSrv;
	AppSrv.SetAppServerPath(strTargetDirApp);
	AppSrv.LoadINIFile(err);
	if (!err.IsOK())
	{
		Logger log;
		log.EnableFileLogging(QCoreApplication::applicationDirPath()+"/debug_emergency.log");
		log.EnableConsoleLogging();
		log.SetLogLevel(4);
		int nThread=ThreadIdentificator::GetCurrentThreadID();
		log.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Failed to load INI file");
		return 1;
	}

	qDebug()<< "Current Application Server directory: " << strTargetDirApp;
	qDebug()<< "Application Server Descriptor: " << AppSrv.GetINIFile()->m_strDescriptor;

	qDebug()<<"stopping server in"<<strTargetDirApp;

	//wait 1st time 10sec:
	bool brunning=AppSrv.IsRunning(err);
	if (err.IsOK() && brunning)
	{
		qDebug()<<"Server was running, now stopping";
		ThreadSleeper::Sleep(1000*60); //1 min wait before firing
		AppSrv.Stop(err);
		if (!err.IsOK())
		{
			qDebug()<<err.getErrorText();
			Logger log;
			log.EnableFileLogging(QCoreApplication::applicationDirPath()+"/update_emergency.log");
			log.EnableConsoleLogging();
			log.SetLogLevel(4);
			int nThread=ThreadIdentificator::GetCurrentThreadID();
			log.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Failed to stop application server");
			return 1;
		}
		ThreadSleeper::Sleep(1000*20);
	}
	else
		qDebug()<<"Server was not running";

	while(AppSrv.IsRunning(err))
	{
		if (!err.IsOK())
		{
			Logger log;
			log.EnableFileLogging(QCoreApplication::applicationDirPath()+"/update_emergency.log");
			log.EnableConsoleLogging();
			log.SetLogLevel(4);
			int nThread=ThreadIdentificator::GetCurrentThreadID();
			log.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Failed to check if application server is running");
			return 1;
		}
		qDebug()<<"server is running";
		AppSrv.Stop(err);
		if (!err.IsOK())
		{
			Logger log;
			log.EnableFileLogging(QCoreApplication::applicationDirPath()+"/update_emergency.log");
			log.EnableConsoleLogging();
			log.SetLogLevel(4);
			int nThread=ThreadIdentificator::GetCurrentThreadID();
			log.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Failed to stop application server");
			return 1;
		}
		ThreadSleeper::Sleep(1000*20);
		qDebug()<<"server stopped";
	}

	//server stoped: xcopy all from /update to current!
	QString strCommand="xcopy \""+strSourceDir+ "\" \""+strTargetDirApp+"\" /S /E /Y";
	if(!executeShellCommand(strCommand))
	{
		Logger log;
		log.EnableFileLogging(QCoreApplication::applicationDirPath()+"/update_emergency.log");
		log.EnableConsoleLogging();
		log.SetLogLevel(4);
		int nThread=ThreadIdentificator::GetCurrentThreadID();
		log.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Failed to copy update files");
		return 1;
	}

	//delete old & set new:
	if(!AppSrv.GetINIFile()->m_strDescriptor.isEmpty())
	{
		QString strAppServerExe=AppSrv.GetINIFile()->m_strDescriptor+".exe";
		QFile::remove(strTargetDirApp+"/"+strAppServerExe);
		if(!QFile::copy(strSourceDir+"/appserver.exe",strTargetDirApp+"/"+strAppServerExe))
		{
			Logger log;
			log.EnableFileLogging(QCoreApplication::applicationDirPath()+"/update_emergency.log");
			log.EnableConsoleLogging();
			log.SetLogLevel(4);
			int nThread=ThreadIdentificator::GetCurrentThreadID();
			log.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Failed to copy new appserver.exe file");
			return 1;
		}

		//install service if not installed: new on Bugatti: 25.01.2011
		if (!AppSrv.IsServiceInstalled("SOKRATES AppServer - "+AppSrv.GetINIFile()->m_strDescriptor))
		{
			AppSrv.InstallService(err,DBTYPE_FIREBIRD);
			if (!err.IsOK())
			{
				Logger log;
				log.EnableFileLogging(QCoreApplication::applicationDirPath()+"/update_emergency.log");
				log.EnableConsoleLogging();
				log.SetLogLevel(4);
				int nThread=ThreadIdentificator::GetCurrentThreadID();
				log.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,QString("Failed to install service: ") +QString("SOKRATES AppServer - ")+AppSrv.GetINIFile()->m_strDescriptor);
				return 1;
			}
		}
	}




	AppSrv.Start(err);
	if (!err.IsOK())
	{
		Logger log;
		log.EnableFileLogging(QCoreApplication::applicationDirPath()+"/update_emergency.log");
		log.EnableConsoleLogging();
		log.SetLogLevel(4);
		int nThread=ThreadIdentificator::GetCurrentThreadID();
		log.logMessage(StatusCodeSet::TYPE_ERROR,StatusCodeSet::ERR_SYSTEM_GENERAL_MSG,"Failed to start application server");
		return 1;
	}

	return 0;
}