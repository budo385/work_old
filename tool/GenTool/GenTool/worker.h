#ifndef WORKER_H
#define WORKER_H

#include <QObject>
#include <QTimer>
#include <QFile>
#include <QPixmap>
#include <QFileInfo>
#include <QPrinter>
#include <QDebug>
#include <QTextDocument>


#define MAX_THUMB_PIC_WIDTH_LARGE 1280
#define MAX_THUMB_PIC_HEIGHT_LARGE 1024

#define FULL_VIEW_PIC_WIDTH_LARGE 2048
#define FULL_VIEW_PIC_HEIGHT_LARGE 2048



class Worker : public QObject
{
	Q_OBJECT

public:
	Worker(QObject *parent);
	~Worker();

	bool ParseCommandLine(QStringList lstArgs);

public slots:
	void OnWork();

private:
	
	bool GetFilesFromFolder(QString strFolderPath,QMap<QString,QDateTime> &lstFilesOut,  QString strAllowedExtension,QString strFileSuffixFilter, QStringList lstSuffixes);
	bool GetFilesFromFolder_iPhoto(QString strAlbumIPhotoID,QMap<QString,QDateTime> &lstFilesOut,  QString strAllowedExtension,QString strFileSuffixFilter, QStringList lstSuffixes);
	void WriteConsoleHelp();
	bool CreateThumb(QString &strErr, QString strIn,QString strOut,QFileInfo &infoSourceFile);
	bool CreateThumbFromRaw(QString &strErr, QString strIn,QString strOut);
	bool ExtractOriginalJPEGFromRaw(QString &strErr, QString strIn, QString strOut);
	bool ConvertToJPG(QString &strErr, QString strIn, QString strOut, bool bForceConvert=false);
	void PreprocessFixRtfEmail(QString &strEmail);
	void UnembedHtmlPix(QString &strHtmlData, QStringList &lstResTempFiles, QString strTargetTempDir);
	bool RotateThumb(QString &strErr, int nAngle, QString strIn,QString strOut);

	bool ResizeDimensionsWithAspect(int nTargetWidth, int nTargetHeight, int nActualWidth, int nActualHeight, int &nCalcWidth, int &nCalcHeight, Qt::AspectRatioMode mode=Qt::KeepAspectRatio);

	bool RotateImageToExifOrientation(QString &strErr, QString strFile);
	bool RotateImageToExifOrientationPix(QString &strErr, QString strFile,QPixmap &pixmap,QByteArray &buffer);
	bool ReadOrientationFromExifData(QString strFile,int &nOrientation);


	QString m_strIn;
	QString m_strOut;
	bool m_boolInDir_iPhotoAlbum;
	QString m_strInDir;
	QString m_strOutDir;
	QString m_strSuffix;
	QString m_strExtensions;
	QStringList m_lstSizes;
	QStringList m_lstSuffix;

	QString m_strTempDirForPdfConversion;
	bool m_bPicMode;
	int m_nWidth;
	int m_nHeight;
	bool m_bKeepAspectRatio;
	bool m_bAutoPotrait;
	int m_nAngle;
	int m_bRotateBasedOnExif;
	int m_nJPEGQuality;
	bool m_bExtractRaw;
	bool m_bToJpg;


};
/*

#include <QMutex>
#include <QWaitCondition>


class ThreadSleeper 
{ 
public:
	static void Sleep(unsigned long miliseconds)
	{
		QMutex mutex;
		mutex.lock();
		QWaitCondition holdthread;
		holdthread.wait(&mutex,miliseconds);
		mutex.unlock();
	}
};
*/
#endif // WORKER_H
