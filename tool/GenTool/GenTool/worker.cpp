#include "worker.h"
#include <QCoreApplication>
#include <QFileInfo>
#include <QDateTime>
#include <QDir>
#include <QProcess>
#include <QSettings>
#include <QBuffer>
#include <QImageReader>
#include <QPainter>
#include "iphotohandler.h"
#include "exif/QImageMetaData.h"

#define  RAW_EXTENSIONS "cr2;crw;pef;dng;raw;raf;3fr;dcr;dcs;kdc;mef;nef;orf;rw2;x3f;srf;sr2;arw"

Worker::Worker(QObject *parent)
	: QObject(parent)
{
	m_nWidth=64;;
	m_nHeight=64;
	m_bKeepAspectRatio=false;
	m_bPicMode=false;
	m_bExtractRaw=false;
	m_bToJpg=false;
	m_bAutoPotrait=false;
	m_boolInDir_iPhotoAlbum=false;
	m_bRotateBasedOnExif=false;

	QCoreApplication::setOrganizationName("Helix Business Soft");
	QCoreApplication::setOrganizationDomain("everpics.com");
	QCoreApplication::setApplicationName("EverPICs Bridge");

}

Worker::~Worker()
{

}

void Worker::OnWork()
{
	//int nAnte;
	//QString err;
	//QString strOut;
	//ReadOrientationFromExifData("D:/iPhone_portrait.jpg",nAnte);
	//qDebug()<<nAnte;
	//qDebug()<<strOut;


	QStringList lstArgs=qApp->arguments();
/*
	//log to file:
	QFile in_file("D:/log_gentool.exe");
	if(in_file.open(QIODevice::Append))
	{
		QString strText=lstArgs.join(" ")+"\r\n";
		in_file.write(strText.toLatin1());
		in_file.close();
	}
*/

	if(!ParseCommandLine(lstArgs))
	{
		WriteConsoleHelp();
		qApp->exit(1);
		return;
	}
	else
	{
		if (m_strExtensions.isEmpty())
		{
			m_strExtensions="bmp;gif;ico;jpeg;jpg;mng;pbm;pgm;png;ppm;svg;tif;tiff;xbm;xpm";
			m_strExtensions +=RAW_EXTENSIONS;
		}

		//

		if (m_bPicMode)
		{
			if (!m_strIn.isEmpty() && !m_strOut.isEmpty()) 
			{
				QString strErr;

				if(m_bExtractRaw)
				{
					if(!ExtractOriginalJPEGFromRaw(strErr,m_strIn,m_strOut))
					{
						qCritical()<<strErr;
						qApp->exit(1);
						return;
					}

				}
				else if (m_bToJpg)
				{
					if(!ConvertToJPG(strErr,m_strIn,m_strOut,true))
					{
						qCritical()<<strErr;
						qApp->exit(1);
						return;
					}
				}
				
				else if (m_nAngle==0)
				{
					QFileInfo info(m_strIn);

					if(!CreateThumb(strErr,m_strIn,m_strOut,info))
					{
						qCritical()<<strErr;
						qApp->exit(1);
						return;
					}
				}
				else
				{
					if(!RotateThumb(strErr,m_nAngle,m_strIn,m_strOut))
					{
						qCritical()<<strErr;
						qApp->exit(1);
						return;
					}

				}
			}
			else if (!m_strInDir.isEmpty() && !m_strOutDir.isEmpty()) //BT: only files that not exist will be created or thumbs with last modify < original
			{
				//read all source files by last modify sort 'em
				//read all target files by last modify,
				QMap<QString,QDateTime> lstInFiles;
				QMap<QString,QDateTime> lstOutFiles;

			
				
				QStringList lstEmpty;
				if(!m_boolInDir_iPhotoAlbum)
				{
					if(!Worker::GetFilesFromFolder(m_strInDir,lstInFiles, m_strExtensions,"",lstEmpty))
					{
						qCritical()<<"Error: can not open directory: "<<m_strInDir;
						qApp->exit(1);
						return;
					}
				}
				else
				{
					if(!Worker::GetFilesFromFolder_iPhoto(m_strInDir,lstInFiles, m_strExtensions,"",lstEmpty))
					{
						qCritical()<<"Error: can not open directory: "<<m_strInDir;
						qApp->exit(1);
						return;
					}
				}

				if(!Worker::GetFilesFromFolder(m_strOutDir,lstOutFiles, m_strExtensions,m_strSuffix,m_lstSuffix))
				{
					qCritical()<<"Error: can not open directory: "<<m_strOutDir;
					qApp->exit(1);
					return;
				}
				
				int nCnt=0;
				int nTotal=lstInFiles.size();

				/*
				QSettings settings(QSettings::SystemScope,QCoreApplication::organizationName(),QCoreApplication::applicationName());
				
				settings.setValue("ProgressTotal",nTotal);
				settings.setValue("ProgressCurrent",1);
				settings.setValue("ProgressThumbSize",QString::number(m_nWidth)+"x"+QString::number(m_nHeight));
				settings.setValue("ProgressPath",m_strInDir);
				*/

				//compare source-target modify if <> generate new thumb, else break
				QMapIterator <QString,QDateTime> i(lstInFiles);
				while (i.hasNext()) 
				{
					i.next();
					QFileInfo info(i.key());

					QString strTargetThumbFileName=info.completeBaseName()+m_strSuffix+".jpg"; //only valid for 1

					bool bNeed2CreateThumb=false;

					//test if thumb alredy exists. if so compare date last modify with original
					if (m_lstSizes.count()>0)
					{
						for (int k=0;k<m_lstSizes.count();k++)
						{
							strTargetThumbFileName=info.completeBaseName()+"_"+m_lstSizes.at(k)+".jpg";
							QDateTime datLastModify=lstOutFiles.value(strTargetThumbFileName,QDateTime());
							if (!datLastModify.isValid() || (datLastModify.isValid() && datLastModify<i.value()))
							{
								bNeed2CreateThumb=true;
								break;
							}
						}
					}
					else
					{
						QDateTime datLastModify=lstOutFiles.value(strTargetThumbFileName,QDateTime());
						if (!datLastModify.isValid() || (datLastModify.isValid() && datLastModify<i.value()))
							bNeed2CreateThumb=true;
					}


					if (bNeed2CreateThumb)
					{
						QString strInFile;
						
						if(m_boolInDir_iPhotoAlbum)
							strInFile=i.key();
						else
							strInFile=m_strInDir+"/"+i.key();

						QString strOutFile=m_strOutDir+"/"+strTargetThumbFileName;

						QString strErr;
						QFileInfo infoIn(strInFile);
						//qDebug()<<" ........."<<strInFile;
						if (QString(RAW_EXTENSIONS).indexOf(infoIn.completeSuffix().toLower())>=0) //test if raw picture
						{
							//qDebug()<<" ......... raw com";
							if(!CreateThumbFromRaw(strErr,strInFile,strOutFile))
							{
								qCritical()<<strErr;
							}
						}
						else
						{
							//qDebug()<<" ......... QT com";
							if(!CreateThumb(strErr,strInFile,strOutFile,info))
							{
								qCritical()<<strErr;
							}
						}


					}
					nCnt++;

					//if (nCnt%10==0)
					//{
					//		settings.setValue("ProgressCurrent",nCnt);
					//	}
				}

				//settings.setValue("ProgressPath","");

			}
			else if (!m_strIn.isEmpty() && m_bRotateBasedOnExif)
			{
				QString strErr;
				if (!RotateImageToExifOrientation(strErr,m_strIn))
				{
					qCritical()<<strErr;
					qApp->exit(1);
					return;
				}
			}
			else
			{
				WriteConsoleHelp();
				qApp->exit(1);
				return;
			}
		}
		else
		{
			QFile in_file(m_strIn);
			if(!in_file.open(QIODevice::ReadOnly))
			{
				qCritical()<<"Error: failed to open input file: "<<m_strIn;
				qApp->exit(1);
				return;
			}

			QByteArray buffer=in_file.readAll();
			if (buffer.size()==0)
			{
				in_file.close();
				qCritical()<<"Error: failed to read from input file: "<<m_strIn;
				qApp->exit(1);
				return;
			}

			QString strData=QString(buffer);
			PreprocessFixRtfEmail(strData);
			QStringList lstTempFiles;
			if (m_strTempDirForPdfConversion.isEmpty())
				m_strTempDirForPdfConversion=QCoreApplication::applicationDirPath();
			UnembedHtmlPix(strData,lstTempFiles,m_strTempDirForPdfConversion);
			in_file.close();

			//PRINT TO PDF.
			QPrinter *printer = new QPrinter();
			printer->setOutputFormat(QPrinter::PdfFormat);
			printer->setPageSize(QPrinter::B6);
			printer->setOrientation(QPrinter::Portrait);
			//printer->setFullPage(true);
			printer->setPageMargins(3,3,3,3, QPrinter::Millimeter);
			
			printer->setOutputFileName(m_strOut);	
			QTextDocument text;
			text.setHtml(strData);
			//text.setDocumentMargin(0.1);
			text.print(printer);

			for (int i=0;i<lstTempFiles.size();i++)
			{
				QFile::remove(lstTempFiles.at(i));
			}
			delete printer;
			
			/*
			QFileInfo info(m_strOut);
			int n=0; //10 seconds
			while (!info.exists() && info.size()==0 && n<100)
			{
				ThreadSleeper::Sleep(100);
				n++;
			}
			*/
		}

		//fire exit:
		QTimer::singleShot(0,qApp,SLOT(quit()));
	}

}

//strOut is ignored if >1
/*
bool Worker::CreateThumb(QString &strErr, QString strIn,QString strOut,QFileInfo &info)
{
	if (m_lstSizes.count()>0)
	{
		for (int i=0; i<m_lstSizes.count(); i++)
		{
			QImageReader image_reader(strIn);
			int image_width = image_reader.size().width();
			int image_height = image_reader.size().height();

			bool bFlipWidthAndHeight=false;

			//test auto potrait: 
			if (m_bAutoPotrait && (image_height>image_width))
				bFlipWidthAndHeight=true;

			QString strSize=m_lstSizes.at(i);
			int nWidth=strSize.left(strSize.indexOf("x")).toInt();
			int nHeight=strSize.mid(strSize.indexOf("x")+1).toInt();
			if (bFlipWidthAndHeight)
			{
				int nTmp=nHeight;
				nHeight=nWidth;
				nWidth=nTmp;
			}
			if (nWidth==0 || nHeight ==0)
			{
				strErr="Error: failed to parse size list";
				return false;
			}
			QString strOutFile=m_strOutDir+"/"+info.completeBaseName()+"_"+strSize+".jpg";


			//->TRY TO RESCALE TO GIVEN WIDTH OR HEIGHT IF SMALLER THEN JUST COPY..

			int nNewWidth,nNewHeight;
			bool bScale=false;
			if (m_bKeepAspectRatio) 
				bScale=ResizeDimensionsWithAspect(nWidth,nHeight,image_width,image_height,nNewWidth,nNewHeight,Qt::KeepAspectRatio);
			else
				bScale=ResizeDimensionsWithAspect(nWidth,nHeight,image_width,image_height,nNewWidth,nNewHeight,Qt::KeepAspectRatio);

			if (bScale)
			{
				image_reader.setScaledSize(QSize(nNewWidth, nNewHeight));
				QImage thumbnail = image_reader.read();
				if(!thumbnail.save(strOutFile, 0, m_nJPEGQuality))
				{
					strErr="Error: failed to save to the output file: "+strOut;
					return false;
				}
			}
			else
			{
				//copy:
				if(!QFile::copy(strIn,strOut))
				{
					strErr="Error: failed to save to the output file: "+strOut;
					return false;
				}
			}
		}

		return true;
	}
	else
	{
		QImageReader image_reader(strIn);
		int image_width = image_reader.size().width();
		int image_height = image_reader.size().height();

		//limit max w/h, chek values
		if (m_nHeight<=0)m_nHeight=75;
		if (m_nWidth<=0)m_nWidth=75;

		int nNewWidth,nNewHeight;
		bool bScale=false;
		if (m_bKeepAspectRatio) 
			bScale=ResizeDimensionsWithAspect(m_nWidth,m_nHeight,image_width,image_height,nNewWidth,nNewHeight,Qt::KeepAspectRatio);
		else
			bScale=ResizeDimensionsWithAspect(m_nWidth,m_nHeight,image_width,image_height,nNewWidth,nNewHeight,Qt::KeepAspectRatio);

		if (bScale)
		{
			image_reader.setScaledSize(QSize(nNewWidth, nNewHeight));
			QImage thumbnail = image_reader.read();
			if(!thumbnail.save(strOut, 0, m_nJPEGQuality))
			{
				strErr="Error: failed to save to the output file: "+strOut;
				return false;
			}
		}
		else
		{
			//copy:
			if(!QFile::copy(strIn,strOut))
			{
				strErr="Error: failed to save to the output file: "+strOut;
				return false;
			}
		}


		return true;
	}
}

*/
bool Worker::ResizeDimensionsWithAspect(int nTargetWidth, int nTargetHeight, int nActualWidth, int nActualHeight, int &nCalcWidth, int &nCalcHeight, Qt::AspectRatioMode mode )
{
	//no need to resizee
	if (nActualHeight<nTargetHeight && nActualWidth<nTargetWidth)
	{
		return false;
	}
	QSize newSize(nActualWidth,nActualHeight);
	newSize.scale(nTargetWidth,nTargetHeight,mode);
	nCalcHeight=newSize.height();
	nCalcWidth=newSize.width();

	return true;
}


//strOut is ignored if >1

bool Worker::CreateThumb(QString &strErr, QString strIn,QString strOut,QFileInfo &info)
{

	QPixmap pix;
	QByteArray buffer;
	if(!RotateImageToExifOrientationPix(strErr,strIn,pix,buffer))
	{
		return false;
	}

	/*

	QFile in_file(strIn);
	if(!in_file.open(QIODevice::ReadOnly))
	{
		strErr="Error: failed to open input file: "+strIn;
		return false;
	}

	QByteArray buffer=in_file.readAll();
	if (buffer.size()==0)
	{
		in_file.close();
		strErr="Error: failed to read from input file: "+strIn;
		return false;
	}
	*/

	QFileInfo infoInput(strIn);
	bool bSourceIsPNG=false;
	if (infoInput.suffix().toLower()=="png")
		bSourceIsPNG=true;

	/*
	QPixmap pix;
	if (!pix.loadFromData(buffer))
	{
		strErr="Error: failed to decode picture: "+strIn;
		return false;
	}
	*/

	if (m_lstSizes.count()>0)
	{
		bool bFlipWidthAndHeight=false;
		
		//test auto potrait: 
		if (m_bAutoPotrait && (pix.height()>pix.width()))
				bFlipWidthAndHeight=true;

		//scale first to largest whatever:
		if (pix.height()>MAX_THUMB_PIC_HEIGHT_LARGE || pix.width()>MAX_THUMB_PIC_WIDTH_LARGE)
			pix=pix.scaled(MAX_THUMB_PIC_WIDTH_LARGE,MAX_THUMB_PIC_HEIGHT_LARGE,Qt::KeepAspectRatio,Qt::FastTransformation);

		for (int i=0; i<m_lstSizes.count(); i++)
		{
			QString strSize=m_lstSizes.at(i);
			int nWidth=strSize.left(strSize.indexOf("x")).toInt();
			int nHeight=strSize.mid(strSize.indexOf("x")+1).toInt();
			if (bFlipWidthAndHeight)
			{
				int nTmp=nHeight;
				nHeight=nWidth;
				nWidth=nTmp;
			}
			if (nWidth==0 || nHeight ==0)
			{
				strErr="Error: failed to parse size list";
				return false;
			}
			QString strOutFile=m_strOutDir+"/"+info.completeBaseName()+"_"+strSize+".jpg";

			QPixmap pix_thumb;
			if (m_bKeepAspectRatio) 
				pix_thumb=pix.scaled(nWidth,nHeight,Qt::KeepAspectRatio,Qt::SmoothTransformation);
			else
				pix_thumb=pix.scaled(nWidth,nHeight,Qt::IgnoreAspectRatio,Qt::SmoothTransformation);


			//issue convert transparent png to jpg with trans
			if (bSourceIsPNG)
			{
				if(pix_thumb.hasAlphaChannel()){
					//replace transparency with white
					QPixmap image2(pix_thumb.size());
					image2.fill(QColor(Qt::white).rgb());
					QPainter painter(&image2);
					painter.drawPixmap(0, 0, pix_thumb);
					pix_thumb = image2;
				}
			}

			if(!pix_thumb.save(strOutFile, 0, m_nJPEGQuality))
			{
				strErr="Error: failed to save to the output file: "+strOut;
				return false;
			}
		}

		return true;
	}
	else
	{
		bool bFlipWidthAndHeight=false;

		//test auto potrait: 
		if (m_bAutoPotrait && (pix.height()>pix.width()))
			bFlipWidthAndHeight=true;

		//scale first to largest whatever:
		if (pix.height()>MAX_THUMB_PIC_HEIGHT_LARGE || pix.width()>MAX_THUMB_PIC_WIDTH_LARGE)
			pix=pix.scaled(MAX_THUMB_PIC_WIDTH_LARGE,MAX_THUMB_PIC_HEIGHT_LARGE,Qt::KeepAspectRatio,Qt::FastTransformation);

		//limit max w/h, chek values
		if (m_nHeight<=0)m_nHeight=75;
		if (m_nWidth<=0)m_nWidth=75;

		//QString strSize=m_lstSizes.at(i);
		int nWidth=m_nWidth;
		int nHeight=m_nHeight;
		if (bFlipWidthAndHeight)
		{
			int nTmp=nHeight;
			nHeight=nWidth;
			nWidth=nTmp;
		}
		if (nWidth==0 || nHeight ==0)
		{
			strErr="Error: failed to parse size list";
			return false;
		}

		QString strOutFile=strOut;

		QPixmap pix_thumb;
		if (m_bKeepAspectRatio) 
			pix_thumb=pix.scaled(nWidth,nHeight,Qt::KeepAspectRatio,Qt::SmoothTransformation);
		else
			pix_thumb=pix.scaled(nWidth,nHeight,Qt::IgnoreAspectRatio,Qt::SmoothTransformation);

		if(!pix_thumb.save(strOutFile, 0, m_nJPEGQuality))
		{
			strErr="Error: failed to save to the output file: "+strOut;
			return false;
		}

		return true;
	}

}


//new: take original pic then scale to 1280x1024 by width or height then rotate
bool Worker::RotateThumb(QString &strErr, int nAngle, QString strIn,QString strOut)
{

	QFile in_file(strIn);
	if(!in_file.open(QIODevice::ReadOnly))
	{
		strErr="Error: failed to open input file: "+strIn;
		return false;
	}

	QByteArray buffer=in_file.readAll();
	if (buffer.size()==0)
	{
		in_file.close();
		strErr="Error: failed to read from input file: "+strIn;
		return false;
	}

	QPixmap pix;
	pix.loadFromData(buffer);

	if (m_nWidth == -1 && m_nHeight == -1)
	{
		QTransform trans;
		trans=trans.rotate(nAngle);
		pix=pix.transformed(trans);
	}
	else //just rotate
	{
		//for faster transform use fast to 1280/1024 then smooth: http://labs.trolltech.com/blogs/2009/01/26/creating-thumbnail-preview/
		if (pix.height()>MAX_THUMB_PIC_WIDTH_LARGE || pix.width()>MAX_THUMB_PIC_HEIGHT_LARGE)
		{
			if (pix.width()>pix.height())
				pix=pix.scaledToWidth(MAX_THUMB_PIC_WIDTH_LARGE,Qt::FastTransformation);
			else
				pix=pix.scaledToHeight(MAX_THUMB_PIC_HEIGHT_LARGE,Qt::FastTransformation);
		}

		QTransform trans;
		trans=trans.rotate(nAngle);
		pix=pix.transformed(trans);

		if (m_bKeepAspectRatio) 
		{
			//test auto potrait: 
			if (m_bAutoPotrait && (pix.height()>pix.width()))
				pix=pix.scaled(m_nHeight,m_nWidth,Qt::KeepAspectRatio,Qt::SmoothTransformation);
			else
				pix=pix.scaled(m_nWidth,m_nHeight,Qt::KeepAspectRatio,Qt::SmoothTransformation);
		}
		else
			pix=pix.scaled(m_nWidth,m_nHeight,Qt::IgnoreAspectRatio,Qt::SmoothTransformation);
	}


	if(!pix.save(strOut, 0, m_nJPEGQuality))
	{
		in_file.close();
		strErr="Error: failed to save to the output file: "+strOut;
		return false;
	}

	in_file.close();
	return true;
}



//extract thumbs if exists inside:
//if not then convert to tiff
//->input file tiff or jpg convert as usual
//->clear intermediate output...

bool Worker::CreateThumbFromRaw(QString &strErr, QString strIn,QString strOut)
{
	//qDebug()<<"Creating thumb from "<<strIn;

	QFileInfo info(strIn);

	QProcess dcraw;
	QStringList lstArgs;
	lstArgs<<"-e";
	lstArgs<<strIn;
	dcraw.start(QCoreApplication::applicationDirPath()+"/RawLib",lstArgs);
	if (!dcraw.waitForFinished(-1)) //wait forever
	{	
		strErr="Failed to execute RawLib";
		return false;
	}
	int nExitCode=dcraw.exitCode();
	QByteArray errContent=dcraw.readAllStandardError();
	if (errContent.size()!=0)
	{
		strErr="Thumb from RAW format process returned errors: "+QString(errContent.left(4095));
		return false;
	}

	//if succeed then try to find name+".thumbxx" +jpg??:
	QFileInfo infoIn(strIn);
	QString strThumbName=infoIn.absolutePath()+"/"+infoIn.completeBaseName()+".thumb.jpg";
	QString strThumbName2=infoIn.absolutePath()+"/"+infoIn.completeBaseName()+".thumb.ppm";
	if (QFile::exists(strThumbName))
	{
		//qDebug()<<"thumb extracted "<<strThumbName;

		
		bool bOK=CreateThumb(strErr,strThumbName,strOut,info);
		QFile::remove(strThumbName);
		QFile::remove(strThumbName2);
		return bOK;
	}
	else if (QFile::exists(strThumbName2))
	{
		//qDebug()<<"thumb extracted "<<strThumbName2;

		bool bOK=CreateThumb(strErr,strThumbName2,strOut,info);
		QFile::remove(strThumbName);
		QFile::remove(strThumbName2);
		return bOK;
	}

	//else generate tiff:
	lstArgs.clear();
	lstArgs<<"-T";
	lstArgs<<strIn;
	dcraw.start(QCoreApplication::applicationDirPath()+"/RawLib",lstArgs);
	if (!dcraw.waitForFinished(-1)) //wait forever
	{	
		strErr="Failed to execute RawLib for TIFF conversion";
		return false;
	}
	nExitCode=dcraw.exitCode();
	errContent=dcraw.readAllStandardError();
	if (errContent.size()!=0)
	{
		strErr="Thumb from RAW format (TIFF conversion) process returned errors: "+QString(errContent.left(4095));
		return false;
	}

	//if succeed then try to find name+tiff?:
	QString strTiffName=infoIn.absolutePath()+"/"+infoIn.completeBaseName()+".tiff";
	if (QFile::exists(strTiffName))
	{
		//qDebug()<<"tiff extracted "<<strTiffName;

		bool bOK=CreateThumb(strErr,strTiffName,strOut,info);
		QFile::remove(strThumbName);
		return bOK;
	}

	strErr="Failed to create thumb from picture: "+infoIn.completeBaseName();
	return false;
}

bool Worker::ExtractOriginalJPEGFromRaw(QString &strErr, QString strIn, QString strOut)
{
	//qDebug()<<"Creating thumb from "<<strIn;
	//qDebug()<<"out: "<<strOut;

	QProcess dcraw;
	QStringList lstArgs;
	lstArgs<<"-e";
	lstArgs<<strIn;
	dcraw.start(QCoreApplication::applicationDirPath()+"/RawLib",lstArgs);
	if (!dcraw.waitForFinished(-1)) //wait forever
	{	
		strErr="Failed to execute RawLib";
		return false;
	}
	int nExitCode=dcraw.exitCode();
	QByteArray errContent=dcraw.readAllStandardError();
	if (errContent.size()!=0)
	{
		strErr="Thumb from RAW format process returned errors: "+QString(errContent.left(4095));
		return false;
	}


	//if succeed then try to find name+".thumbxx" +jpg??:
	QFileInfo infoIn(strIn);
	QString strThumbName=infoIn.absolutePath()+"/"+infoIn.completeBaseName()+".thumb.jpg";
	QString strThumbName2=infoIn.absolutePath()+"/"+infoIn.completeBaseName()+".thumb.ppm";
	if (QFile::exists(strThumbName))
	{
		//qDebug()<<"thumb extracted "<<strThumbName;

		bool bOK=ConvertToJPG(strErr,strThumbName,strOut,true);
		QFile::remove(strThumbName);
		QFile::remove(strThumbName2);
		return bOK;
	}
	else if (QFile::exists(strThumbName2))
	{
		//qDebug()<<"thumb extracted "<<strThumbName2;

		bool bOK=ConvertToJPG(strErr,strThumbName2,strOut,true);
		QFile::remove(strThumbName);
		QFile::remove(strThumbName2);
		return bOK;
	}


	//else generate tiff:
	lstArgs.clear();
	lstArgs<<"-T";
	lstArgs<<strIn;
	dcraw.start(QCoreApplication::applicationDirPath()+"/RawLib",lstArgs);
	if (!dcraw.waitForFinished(-1)) //wait forever
	{	
		strErr="Failed to execute RawLib for TIFF conversion";
		return false;
	}
	nExitCode=dcraw.exitCode();
	errContent=dcraw.readAllStandardError();
	if (errContent.size()!=0)
	{
		strErr="Thumb from RAW format (TIFF conversion) process returned errors: "+QString(errContent.left(4095));
		return false;
	}

	//if succeed then try to find name+tiff?:
	QString strTiffName=infoIn.absolutePath()+"/"+infoIn.completeBaseName()+".tiff";
	if (QFile::exists(strTiffName))
	{
		//qDebug()<<"tiff extracted "<<strTiffName;

		bool bOK=ConvertToJPG(strErr,strTiffName,strOut,true);
		QFile::remove(strThumbName);
		return bOK;
	}

	strErr="Failed to create thumb from picture: "+infoIn.completeBaseName();
	return false;
}

bool Worker::ConvertToJPG(QString &strErr, QString strIn, QString strOut, bool bForceConvert)
{
	bool bConvert=true;
	QFileInfo info(strIn);
	if (!bForceConvert)
	{
		if (info.suffix().toLower()=="jpg" || info.suffix().toLower()=="jpeg")
		{
			bConvert=false;
		}
	}

	QPixmap pix;
	QByteArray buffer;
	if(!RotateImageToExifOrientationPix(strErr,strIn,pix,buffer))
	{
		return false;
	}


	//scale first to largest whatever:
	if (pix.height()>FULL_VIEW_PIC_WIDTH_LARGE || pix.width()>FULL_VIEW_PIC_WIDTH_LARGE)
	{
		pix=pix.scaled(FULL_VIEW_PIC_WIDTH_LARGE,FULL_VIEW_PIC_WIDTH_LARGE,Qt::KeepAspectRatio,Qt::FastTransformation);
		bConvert=true;
	}
	
	
	if (bConvert)
	{
		if(!pix.save(strOut, 0, m_nJPEGQuality))
		{
			strErr="Error: failed to save to the output file: "+strOut;
			return false;
		}
	}
	else
	{
		QFile out_file(strOut);
		if(!out_file.open(QIODevice::WriteOnly))
		{
			strErr="Error: failed to open output file: "+strOut;
			return false;
		}
		out_file.write(buffer);
		out_file.close();
	}

	return true;
}




bool Worker::ParseCommandLine(QStringList lstArgs)
{
	m_nAngle=0;
	int nRow=lstArgs.indexOf("-f");
	if (nRow<0)return false;
	if (nRow+2>lstArgs.size())
		return false;

	if (lstArgs.at(nRow+1)=="pic")
		m_bPicMode=true;
	else if (lstArgs.at(nRow+1)=="pdf")
		m_bPicMode=false;
	else
		return false;

	nRow=lstArgs.indexOf("-i");
	if (nRow>=0)
		if (nRow+1<lstArgs.size())
			m_strIn=lstArgs.at(nRow+1);

	nRow=lstArgs.indexOf("-o");
	if (nRow>=0)
		if (nRow+1<lstArgs.size())
			m_strOut=lstArgs.at(nRow+1);

	nRow=lstArgs.indexOf("-r");
	if (nRow>=0)
		if (nRow+1<lstArgs.size())
			m_nAngle=lstArgs.at(nRow+1).toInt();

	nRow=lstArgs.indexOf("-rexif");
	if (nRow>=0)
		m_bRotateBasedOnExif=true;

	nRow=lstArgs.indexOf("-id");
	if (nRow>=0)
		if (nRow+1<lstArgs.size())
			m_strInDir=lstArgs.at(nRow+1);

	nRow=lstArgs.indexOf("-od");
	if (nRow>=0)
		if (nRow+1<lstArgs.size())
			m_strOutDir=lstArgs.at(nRow+1);

	nRow=lstArgs.indexOf("-su");
	if (nRow>=0)
		if (nRow+1<lstArgs.size())
			m_strSuffix=lstArgs.at(nRow+1);

	nRow=lstArgs.indexOf("-ext");
	if (nRow>=0)
		if (nRow+1<lstArgs.size())
			m_strExtensions=lstArgs.at(nRow+1);

	

	if (m_bPicMode)
	{

		nRow=lstArgs.indexOf("-iphoto_album_id");
		if (nRow>=0)
			if (nRow+1<lstArgs.size())
			{
				m_boolInDir_iPhotoAlbum=true;
				m_strInDir=lstArgs.at(nRow+1);
			}

		nRow=lstArgs.indexOf("-auto_potrait");
		if (nRow>0)
			m_bAutoPotrait=true;


		nRow=lstArgs.indexOf("-w");
		if (nRow<0)
		{
			m_nWidth=64;
		}
		else
		{
			if (nRow+2>lstArgs.size())
				return false;
			m_nWidth=lstArgs.at(nRow+1).toInt();
		}

		nRow=lstArgs.indexOf("-h");
		if (nRow<0)
		{
			m_nHeight=64;
		}
		else
		{
			if (nRow+2>lstArgs.size())
				return false;
			m_nHeight=lstArgs.at(nRow+1).toInt();
		}

		nRow=lstArgs.indexOf("-thumb-sizes");
		if (nRow>=0)
		{
			if (nRow+2>lstArgs.size())
				return false;
			m_lstSizes = lstArgs.at(nRow+1).split(",",QString::SkipEmptyParts);
			for (int i = 0; i<m_lstSizes.count();i++)
			{
				m_lstSuffix.append("_"+m_lstSizes.at(i));
			}
		}


		nRow=lstArgs.indexOf("-a");
		if (nRow>0)
			m_bKeepAspectRatio=true;

		nRow=lstArgs.indexOf("-raw_thumb");
		if (nRow>0)
			m_bExtractRaw=true;

		nRow=lstArgs.indexOf("-to_jpg");
		if (nRow>0)
			m_bToJpg=true;
	

		nRow=lstArgs.indexOf("-q");
		if (nRow<0)
		{
			m_nJPEGQuality=40;
		}
		else
		{
			if (nRow+2>lstArgs.size())
				return false;
			m_nJPEGQuality=lstArgs.at(nRow+1).toInt();
			if (m_nJPEGQuality<30 || m_nJPEGQuality>100)
			{
				m_nJPEGQuality=40;
			}
		}


		
	}
	else
	{
		nRow=lstArgs.indexOf("-t");
		if (nRow>0)
		{
			if (nRow+2>lstArgs.size())
				return false;
			m_strTempDirForPdfConversion=lstArgs.at(nRow+1);
		}

	}

	return true;
}


void Worker::PreprocessFixRtfEmail(QString &strEmail)
{
	//when streaming HTML back to QTextBrowser widget, modify HTML to use Qt's HTML dialect
	strEmail.replace("<br />", "<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; text-indent:0px;\"><span style=\" font-size:small; color:#000000;\"> </span></p>");
}


void Worker::UnembedHtmlPix(QString &strHtmlData, QStringList &lstResTempFiles, QString strTargetTempDir)
{
	lstResTempFiles.clear();

	//find and embed each picture in the HTML
	int nPos = strHtmlData.indexOf("<img", 0, Qt::CaseInsensitive);
	while(nPos >= 0)
	{
		//extract file path
		int nSrcStart = strHtmlData.indexOf("src=\"", nPos, Qt::CaseInsensitive);
		if(nSrcStart > 0)
		{
			int nSrcStart1 = strHtmlData.indexOf(";base64,", nSrcStart);
			if(nSrcStart1 < 0){
				nPos = nSrcStart + 1;

				//search next
				nPos = strHtmlData.indexOf("<img", nPos+1, Qt::CaseInsensitive);
				continue;
			}
			nSrcStart1 += 8; //strlen(";base64,")
			int nSrcEnd = strHtmlData.indexOf("\"", nSrcStart1);
			if(nSrcEnd > 0)
			{
				QString strBase64 = strHtmlData.mid(nSrcStart1, nSrcEnd-nSrcStart1);
				//convert from base64
				QByteArray arData = QByteArray::fromBase64(strBase64.toLatin1());

				//extract extension from mime type
				const static int nHdrLen = strlen("src=\"data:image/");
				QString strExt = strHtmlData.mid(nSrcStart+nHdrLen, nSrcStart1-nSrcStart-nHdrLen-8);	//8 -> strlen(";base64,")

				// save the image contents into the temp file
				QString strName = "SokratesImage." + strExt;
				QFileInfo attInfo(strName);
				QString strTmpPath;
				//generate unique temp name
				int nTry = 0; bool bOK = false;
				while(nTry < 1000 && !bOK){
					strTmpPath = strTargetTempDir;
					strTmpPath += "/";
					strTmpPath += attInfo.completeBaseName();
					strTmpPath += QString::number(nTry);
					strTmpPath += ".";
					strTmpPath += attInfo.completeSuffix();
					nTry ++;
					bOK = !QFile::exists(strTmpPath);
				}
				if(!bOK){
					Q_ASSERT(false);	//could not create unused temp path
					//search next
					nPos = strHtmlData.indexOf("<img", nPos+1, Qt::CaseInsensitive);
					continue;
				}

				//save to file
				QFile file(strTmpPath);
				if (file.open(QIODevice::WriteOnly|QIODevice::Truncate))
				{
					file.write(arData);
					file.close();

					//write back into the string (replace old "src" value with file url)
					strHtmlData.remove(nSrcStart + strlen("src=\""), nSrcEnd-nSrcStart-strlen("src=\""));
					strHtmlData.insert(nSrcStart + strlen("src=\""), strTmpPath);

					lstResTempFiles.append(strTmpPath);
				}
			}
		}

		//search next
		nPos = strHtmlData.indexOf("<img", nPos+1, Qt::CaseInsensitive);
	}
}

void Worker::WriteConsoleHelp()
{
	qCritical()<<"Error: failed to parse command line arguments";
	qCritical()<<"--------------";
	qCritical()<<"Commands for generating pdf or thumbs based on input file:";	
	qCritical()<<"-f pic for thumbs / pdf for pdf";
	qCritical()<<"-i input file abs path";
	qCritical()<<"-o output abs path (overwrite by default)";
	qCritical()<<"-r rotate angle, if defined input picture will be rotated and saved into output";

	qCritical()<<"---------------pic options for dir: compare by last modify and create thumbs jpg----------------";
	qCritical()<<"-id input dir abs path";
	qCritical()<<"-od output dir abs path";
	qCritical()<<"-ext allowed extension for pics semicolon delimited (if not defined then all supported pic formats)";
	qCritical()<<"-su thumb filename = original filename+suffix (if not defined then its same)";

	qCritical()<<"-w width of thumb (only valid for pic mode)";
	qCritical()<<"-h width of thumb (only valid for pic mode)";
	qCritical()<<"-thumb-sizes sizes (WxH) for all thumbs in comma separated list, e.g. 75x75,64x64,100x200. Note: -su is ignored, sufixes are automatic: _WxH";

	qCritical()<<"-a optional for pic mode: if defined aspect ratio will be preserved";
	qCritical()<<"-q JPEG quality: default is 50 (valid values:30-100)";
	qCritical()<<"-raw_thumb extract raw thumb as is and save it in output file as jpg";
	qCritical()<<"-to_jpg convert always to jpg (when from raw";
	qCritical()<<"-iphoto_album_id iPhoto Album ID for MAC only";
	qCritical()<<"-auto_potrait if given then original picture is tested: if h>w then thumb size is reversed and produced image is in potrait";
	qCritical()<<"-rexif reads exif information and rotates picture to OrientationTopLeft orientation";
	qCritical()<<"---------------pdf options----------------";
	qCritical()<<"-t optional for pdf mode: defines temp directory for pdf file conversion, else same directory where is application";
	
	qCritical()<<"--------------";
	qCritical()<<"-iphoto_album_id iPhoto Album ID for MAC only";
}


//get files from folder into map: sorted by date time (first is oldest)
//or get 1 suffic or list: test if list then one
bool Worker::GetFilesFromFolder(QString strFolderPath,QMap<QString,QDateTime> &lstFilesOut,  QString strAllowedExtension,QString strFileSuffixFilter, QStringList lstSuffixes)
{
	QDir dir(strFolderPath);
	if (!dir.exists())
		return false;

	QFileInfoList lstFiles=dir.entryInfoList(QDir::Files |QDir::Readable);
	int nSize=lstFiles.size();
	for(int i=0;i<nSize;++i)
	{
		//Get all files from folder if list is empty or filter 'em
		if (!strAllowedExtension.isEmpty())
		{
			if(!strAllowedExtension.contains(lstFiles.at(i).suffix().toLower()))
				continue;
		}
		if (lstSuffixes.count()>0)
		{
			bool bFound =false;
			for (int k=0;k<lstSuffixes.count();k++)
			{
				if (lstFiles.at(i).completeBaseName().right(lstSuffixes.at(k).length())!=lstSuffixes.at(k))
				{
					bFound=true;
					break;
				}
			}

			if (!bFound) //not found then skip file
				continue;
		}
		else if (!strFileSuffixFilter.isEmpty())
		{
			if (lstFiles.at(i).completeBaseName().right(strFileSuffixFilter.length())!=strFileSuffixFilter)
				continue;
		}


		lstFilesOut[lstFiles.at(i).fileName()]=lstFiles.at(i).created();
	}

	return true;
}



//get files from folder into map: sorted by date time (first is oldest)
//or get 1 suffic or list: test if list then one
bool Worker::GetFilesFromFolder_iPhoto(QString strAlbumIPhotoID,QMap<QString,QDateTime> &lstFilesOut,  QString strAllowedExtension,QString strFileSuffixFilter, QStringList lstSuffixes)
{

	QFileInfoList lstFiles;  
	if(!iPhotoHandler::GetAlbumPicturesExt(strAlbumIPhotoID,lstFiles)) 
		return false;


	int nSize=lstFiles.size();
	for(int i=0;i<nSize;++i)
	{
		//Get all files from folder if list is empty or filter 'em
		if (!strAllowedExtension.isEmpty())
		{
			if(!strAllowedExtension.contains(lstFiles.at(i).suffix().toLower()))
				continue;
		}
		if (lstSuffixes.count()>0)
		{
			bool bFound =false;
			for (int k=0;k<lstSuffixes.count();k++)
			{
				if (lstFiles.at(i).completeBaseName().right(lstSuffixes.at(k).length())!=lstSuffixes.at(k))
				{
					bFound=true;
					break;
				}
			}

			if (!bFound) //not found then skip file
				continue;
		}
		else if (!strFileSuffixFilter.isEmpty())
		{
			if (lstFiles.at(i).completeBaseName().right(strFileSuffixFilter.length())!=strFileSuffixFilter)
				continue;
		}

		lstFilesOut[lstFiles.at(i).absoluteFilePath()]=lstFiles.at(i).created();
	}
	return true;
}


	//read exif if !=OrientationTopLeft and unknown then rotate based on this:
	/*
		1) transform="";;
		2) transform="-flip horizontal";;
		3) transform="-rotate 180";;
		4) transform="-flip vertical";;
		5) transform="-transpose";;
		6) transform="-rotate 90";;
		7) transform="-transverse";;
		8) transform="-rotate 270";;
	*/

bool Worker::RotateImageToExifOrientationPix(QString &strErr, QString strFile,QPixmap &pixmap,QByteArray &buffer)
{
	int nOrient=0;
	bool bLoadPixmapAndExit=false;
	if(!ReadOrientationFromExifData(QDir::cleanPath(strFile),nOrient))
		bLoadPixmapAndExit=true; //if exif data does not exist do not return err;

	QFile in_file(strFile);
	if(!in_file.open(QIODevice::ReadOnly))
	{
		strErr="Error: failed to open input file: "+strFile;
		return false;
	}

	buffer=in_file.readAll();
	if (buffer.size()==0)
	{
		in_file.close();
		strErr="Error: failed to read from input file: "+strFile;
		return false;
	}

	in_file.close();

	if(nOrient==QImageMetaData::OrientationTopLeft || nOrient ==QImageMetaData::OrientationUndefined || nOrient==QImageMetaData::OrientationUnknown)
		bLoadPixmapAndExit=true;

	if (bLoadPixmapAndExit)
	{
		if (!pixmap.loadFromData(buffer))
		{
			strErr="Error: failed to decode picture: "+strFile;
			return false;
		}
		return true;
	}


	switch(nOrient)
	{
	case QImageMetaData::OrientationTopRight:
		{
			QImage pix;
			if (!pix.loadFromData(buffer))
			{
				strErr="Error: failed to decode picture: "+strFile;
				return false;
			}

			pix=pix.mirrored(true,false);
			pixmap=QPixmap::fromImage(pix);
			return true;
		}
		break;
	case QImageMetaData::OrientationBottomRight:
		{
			QPixmap pix;
			if (!pix.loadFromData(buffer))
			{
				strErr="Error: failed to decode picture: "+strFile;
				return false;
			}

			QTransform trans;
			trans=trans.rotate(180);
			pixmap=pix.transformed(trans);
			return true;
		}
		break;
	case QImageMetaData::OrientationBottomLeft:
		{
			QImage pix;
			if (!pix.loadFromData(buffer))
			{
				strErr="Error: failed to decode picture: "+strFile;
				return false;
			}

			pix=pix.mirrored(false,true);
			pixmap=QPixmap::fromImage(pix);
			return true;
		}
		break;
	case QImageMetaData::OrientationLeftTop: //flip horizontal then rotate
		{
			QImage pix;
			if (!pix.loadFromData(buffer))
			{
				strErr="Error: failed to decode picture: "+strFile;
				return false;
			}

			pix=pix.mirrored(true,false);
			QPixmap pix2=QPixmap::fromImage(pix);
			//pix2.load(strFile);
			QTransform trans;
			trans=trans.rotate(270);
			pixmap=pix2.transformed(trans);
			return true;
		}

		break;
	case QImageMetaData::OrientationRightTop:
		{
			QPixmap pix;
			if (!pix.loadFromData(buffer))
			{
				strErr="Error: failed to decode picture: "+strFile;
				return false;
			}

			QTransform trans;
			trans=trans.rotate(90);
			pixmap=pix.transformed(trans);
			return true;
		}
		break;
	case QImageMetaData::OrientationRightBottom: //flip horizontal then rotate
		{
			QImage pix;
			if (!pix.loadFromData(buffer))
			{
				strErr="Error: failed to decode picture: "+strFile;
				return false;
			}

			pix=pix.mirrored(true,false);
			QPixmap pix2=QPixmap::fromImage(pix);
			QTransform trans;
			trans=trans.rotate(90);
			pixmap=pix2.transformed(trans);
			return true;
		}
		break;
	case QImageMetaData::OrientationLeftBottom:
		{
			QPixmap pix;
			if (!pix.loadFromData(buffer))
			{
				strErr="Error: failed to decode picture: "+strFile;
				return false;
			}

			QTransform trans;
			trans=trans.rotate(270);
			pixmap=pix.transformed(trans);
			return true;
		}
		break;
	default:
		strErr="Error: can not parse EXIF data from  the file: "+strFile;
		return false;
		break;
	}

	strErr="Error: can not parse EXIF data from  the file: "+strFile;
	return false;

}

	//read exif if !=OrientationTopLeft and unknown then rotate based on this:
	/*
	1) transform="";;
	2) transform="-flip horizontal";;
	3) transform="-rotate 180";;
	4) transform="-flip vertical";;
	5) transform="-transpose";;
	6) transform="-rotate 90";;
	7) transform="-transverse";;
	8) transform="-rotate 270";;
	*/

bool Worker::RotateImageToExifOrientation(QString &strErr, QString strFile)
{
	int nOrient=0;
	if(!ReadOrientationFromExifData(QDir::cleanPath(strFile),nOrient))
		return false;

	if(nOrient==QImageMetaData::OrientationTopLeft || nOrient ==QImageMetaData::OrientationUndefined || nOrient==QImageMetaData::OrientationUnknown)
		return true;

	//nOrient=QImageMetaData::OrientationRightBottom;
	//load into

	QFile in_file(strFile);
	if(!in_file.open(QIODevice::ReadOnly))
	{
		strErr="Error: failed to open input file: "+strFile;
		return false;
	}

	QByteArray buffer=in_file.readAll();
	if (buffer.size()==0)
	{
		in_file.close();
		strErr="Error: failed to read from input file: "+strFile;
		return false;
	}

	in_file.close();



	switch(nOrient)
	{
	case QImageMetaData::OrientationTopRight:
		{
			QImage pix;
			pix.loadFromData(buffer);
			pix=pix.mirrored(true,false);
			if(!pix.save(strFile, 0, m_nJPEGQuality))
			{
				strErr="Error: failed to save to the file: "+strFile;
				return false;
			}
			return true;
		}
		break;
	case QImageMetaData::OrientationBottomRight:
		{
			QPixmap pix;
			pix.loadFromData(buffer);
			QTransform trans;
			trans=trans.rotate(180);
			pix=pix.transformed(trans);
			if(!pix.save(strFile, 0, m_nJPEGQuality))
			{
				strErr="Error: failed to save to the file: "+strFile;
				return false;
			}
			return true;
		}
		break;
	case QImageMetaData::OrientationBottomLeft:
		{
			QImage pix;
			pix.loadFromData(buffer);
			pix=pix.mirrored(false,true);
			if(!pix.save(strFile, 0, m_nJPEGQuality))
			{
				strErr="Error: failed to save to the file: "+strFile;
				return false;
			}
			return true;
		}
	    break;
	case QImageMetaData::OrientationLeftTop: //flip horizontal then rotate
		{
			QImage pix;
			pix.loadFromData(buffer);
			pix=pix.mirrored(true,false);
			//QByteArray buffer2;
			//QBuffer buff1(&buffer2);
			if(!pix.save(strFile, 0, m_nJPEGQuality))
			{
				strErr="Error: failed to save to the file: "+strFile;
				return false;
			}

			QPixmap pix2;
			pix2.load(strFile);
			QTransform trans;
			trans=trans.rotate(270);
			pix2=pix2.transformed(trans);
			if(!pix2.save(strFile, 0, m_nJPEGQuality))
			{
				strErr="Error: failed to save to the file: "+strFile;
				return false;
			}
			return true;
		}

	    break;
	case QImageMetaData::OrientationRightTop:
		{
			QPixmap pix;
			pix.loadFromData(buffer);
			QTransform trans;
			trans=trans.rotate(90);
			pix=pix.transformed(trans);
			if(!pix.save(strFile, 0, m_nJPEGQuality))
			{
				strErr="Error: failed to save to the file: "+strFile;
				return false;
			}
			return true;
		}
		break;
	case QImageMetaData::OrientationRightBottom: //flip horizontal then rotate
		{
			QImage pix;
			pix.loadFromData(buffer);
			pix=pix.mirrored(true,false);
			if(!pix.save(strFile, 0, m_nJPEGQuality))
			{
				strErr="Error: failed to save to the file: "+strFile;
				return false;
			}

			QPixmap pix2;
			pix2.load(strFile);
			QTransform trans;
			trans=trans.rotate(90);
			pix2=pix2.transformed(trans);
			if(!pix2.save(strFile, 0, m_nJPEGQuality))
			{
				strErr="Error: failed to save to the file: "+strFile;
				return false;
			}
			return true;
		}
		break;
	case QImageMetaData::OrientationLeftBottom:
		{
			QPixmap pix;
			pix.loadFromData(buffer);
			QTransform trans;
			trans=trans.rotate(270);
			pix=pix.transformed(trans);
			if(!pix.save(strFile, 0, m_nJPEGQuality))
			{
				strErr="Error: failed to save to the file: "+strFile;
				return false;
			}
			return true;
		}
	    break;
	default:
		strErr="Error: can not parse EXIF data from  the file: "+strFile;
		return false;
	    break;
	}

	strErr="Error: can not parse EXIF data from  the file: "+strFile;
	return false;
}

bool Worker::ReadOrientationFromExifData(QString strFile, int &nOrientation)
{
	bool bOK = false;
	QImageMetaData* metadata = new QImageMetaData();
	if (!metadata->read(strFile)) 
	{
		//error
		//qDebug() << metadata->errorMessage();
	} 
	else 
	{
		//success
		nOrientation = metadata->orientation();
		bOK = true;
	}
	delete(metadata);
	return bOK;
}