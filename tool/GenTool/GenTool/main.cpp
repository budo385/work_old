#include <QApplication>
#include "worker.h"


int main(int argc, char *argv[])
{
	QApplication a(argc, argv);

	QCoreApplication::setOrganizationName("Helix Business Soft");
	QCoreApplication::setOrganizationDomain("everpics.com");
	QCoreApplication::setApplicationName("EverPICs Bridge");

	Worker work(&a);
	QTimer::singleShot(0,&work,SLOT(OnWork()));
	return a.exec();
}


