#include "interface.h"
//#include "threadconnect.h"
#include "trans/trans/resthttpclient.h"
#include <windows.h>


#define _ERR_TEST(X) 	\
	if (!X.IsOK())\
	{	*pnErrorCode=err.getErrorCode();;\
		QString strError=err.getErrorText();\
		strcpy(szErrorString,strError.toLocal8Bit().data());\
		*pnResponseBufferSize=0;\
		return;}\


//nLoginType=1 communicator
//nLoginType=0 james
void ExecuteCall(int *pnErrorCode, char *szErrorString,int nLoginType, const char *szUser,const char *szPass,const char *szHost,const char *szMethod,const char *szPath,const char *szRequest,char *szResponse, int *pnResponseBufferSize)
{
	//connect:
	QUrl url(szHost);
	int nPort=url.port();
	bool bUseSSL=false;
	if (url.scheme()=="https")
		bUseSSL=true;

	//CONNECT:
	Status err;
	RestHTTPClient client;
	client.SetConnectionSettings(url.host(),url.port(),bUseSSL);
	client.Connect(err);
	_ERR_TEST(err);

	//if username is defined then login:
	QString strSession;
	QString strUserName(szUser);
	if (!strUserName.isEmpty())
	{
		client.RestStartSession(err,nLoginType,strUserName,szPass,strSession);
		_ERR_TEST(err);
	}

	//SEND:
	*(client.msg_send.GetBuffer())=QByteArray(szRequest);
	client.RestSend(err,szMethod,szPath,strSession,true);
	if (!err.IsOK())
	{
		Status err_temp;
		client.RestEndSession(err_temp,nLoginType,strSession);
		_ERR_TEST(err);
	}
	else
	{
		int nSize=client.msg_recv.GetBuffer()->size();
		if (*pnResponseBufferSize<nSize)
		{
			*pnErrorCode=1;
			QString strError="Response Buffer overrun. Please increase size of buffer";
			strcpy(szErrorString,strError.toLocal8Bit().data());\

			Status err_temp;
			client.RestEndSession(err_temp,nLoginType,strSession);

			return;
		}
		memcpy(szResponse,client.msg_recv.GetBuffer()->data(),nSize);
		*pnResponseBufferSize=nSize;
	}

	if (!strUserName.isEmpty())
	{
		Status err_temp;
		client.RestEndSession(err_temp,nLoginType,strSession);
	}

	return;
}


void TestCall(char *szErrorString)
{
	FILE *pOut = fopen("C:\\tu_sam.txt", "w");
	if(pOut){
		fprintf(pOut, "Input: %s\nOutput: bbbbb", szErrorString);
		fclose(pOut);
	}
	strcpy(szErrorString, "bbbbb");

	/*
	QByteArray strResponse;
	int nSize;
	::MessageBoxA(NULL, "aaa", "", MB_OK);
	*pnErrorCode=123;

	//QString strError="Response Buffer overrun. Please increase size of buffer";
	//strcpy(szErrorString,strError.toLocal8Bit().data());\
	*/

	return;
	///return ExecuteCall(pnErrorCode, szErrorString,nLoginType, "Je","","http://192.168.200.18:9999","GET","/rest/contacts","",strResponse.data(), &nSize);

}