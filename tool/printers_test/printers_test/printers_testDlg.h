
// printers_testDlg.h : header file
//

#pragma once


// Cprinters_testDlg dialog
class Cprinters_testDlg : public CDialog
{
// Construction
public:
	Cprinters_testDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_PRINTERS_TEST_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedList();
	CString m_strResult;
	afx_msg void OnStnClickedResult();
};
