// TapiCall.h: interface for the CTapiCall class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TAPILINE_H__64E4FCD7_09C8_419D_ABF4_6D864361094B__INCLUDED_)
#define AFX_TAPILINE_H__64E4FCD7_09C8_419D_ABF4_6D864361094B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//#define TAPI_CURRENT_VERSION 0x00010004
#define TAPI_CURRENT_VERSION 0x00020000
#include <tapi.h>
#include <QString>
 
class CTapiLine  
{
public:
	CTapiLine();
	virtual ~CTapiLine();

	static bool InitTAPI();
	static void ShutdownTAPI();

	int Open(DWORD dwDeviceID, int nMode = LINEMEDIAMODE_AUTOMATEDVOICE);
	int Close();

	//TAPI device enumeration
	int  GetDeviceCount();
	bool GetDeviceInfo(DWORD dwDeviceID, QString &strDeviceName, bool &bVoiceCalls);

	int MakeOutgoingCall(const char *szAddress);
	int GetIncomingCall();
	int PickupIncomingCall();

	HANDLE GetHandle(const char *szClassType, long *lError);
	void GetErrorString(int nError, char *&szErrText);
	long GetThreadResult(){ return m_lEventThreadResult; }
	bool IsConnected(){ return m_bConnected; }
	bool IsConnecting(){ return m_bConnecting; }

protected:
	static HLINEAPP	m_hLineApp; // application's usage handle for TAPI
	static DWORD	m_dwNumDevs;
	static HANDLE	m_hLineEvent; // line change events

	HLINE		m_hLine;
	HCALL		m_hCall;
	int			m_nDevID;

	DWORD		m_dwLineMsg;

	bool		m_bConnected;
	bool		m_bConnecting;

	HANDLE	m_hEventFromThread;

	HANDLE	m_hLineMsgThread;   // TAPI Event monitoring thread.
	bool	m_bStopLineEventThread;
	static DWORD WINAPI LineEventThread(LPVOID lpVoid);
	long	m_lEventThreadResult;
};

#ifdef WINCE
	extern "C" void SendSMS(bool bSendConfirmation, bool bUseDefaultSMSC, LPCTSTR lpszSMSC, LPCTSTR lpszRecipient, LPCTSTR lpszMessage);
	extern "C" BOOL ConnectToNetwork(int timeoutsecs);
#endif

#endif // !defined(AFX_TAPILINE_H__64E4FCD7_09C8_419D_ABF4_6D864361094B__INCLUDED_)
