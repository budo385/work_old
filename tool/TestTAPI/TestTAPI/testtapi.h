#ifndef TESTTAPI_H
#define TESTTAPI_H

#include <QtGui/QMainWindow>
#include "ui_testtapi.h"
#include "tapiline.h"

class TestTAPI : public QMainWindow
{
	Q_OBJECT

public:
	TestTAPI(QWidget *parent = 0, Qt::WFlags flags = 0);
	~TestTAPI();

private:
	Ui::TestTAPIClass ui;

private slots:
	void on_btnCallAction1_clicked();
	void OnTAPICheckTimer();

protected:
	void timerEvent ( QTimerEvent * event ); 

private:
	QString GetTapiDeviceName(int nTapiDevID);
	void ClearCall();
	void OnError();

	CTapiLine m_lineTAPI;
	int m_nTapiDeviceID;
	QString m_strPhone;
	bool m_bCallInProgress;
	int m_nTimerID;
};



#endif // TESTTAPI_H
