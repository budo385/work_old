// TapiLine.cpp: implementation of the CTapiLine class.
// Author: T.Yogaramanan
// Include this header if you use any part of the code from this file 
/////////////////////////////////////////////////////////////////////////////

#include "TapiLine.h"
#include <QDebug>
#include <QMessageBox>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#ifdef WINCE
 #pragma comment(lib, "Coredll")
#else
 #pragma comment(lib, "tapi32")
#endif

HLINEAPP	CTapiLine::m_hLineApp = NULL;
DWORD		CTapiLine::m_dwNumDevs = 0;
HANDLE		CTapiLine::m_hLineEvent = NULL; // line change events

#ifdef WINCE
 //SMS sending
 #include <sms.h>
 #pragma comment(lib, "sms")
 //ConnMgrEstablishConnectionSync
 #include <initguid.h>
 #include <connmgr.h>
 #pragma comment(lib, "cellcore")
#endif

//#include "common/common/logger.h"
//extern Logger g_Logger;					//global logger

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CTapiLine::CTapiLine()
{
	m_hEventFromThread = NULL;
	m_hLineMsgThread = NULL;   // TAPI Event monitoring thread.
	m_bStopLineEventThread = true; 
	m_lEventThreadResult = 0; 
	m_dwLineMsg= 0;
	m_nDevID = 0;
	m_hLine = NULL;
	m_hCall = NULL;
	m_bConnected = false;
	m_bConnecting = false;
}

CTapiLine::~CTapiLine()
{
	Close();
}

bool CTapiLine::InitTAPI()
{
	LINEINITIALIZEEXPARAMS stInitParams;
	memset(&stInitParams, 0, sizeof(LINEINITIALIZEEXPARAMS));
	stInitParams.dwTotalSize = sizeof(LINEINITIALIZEEXPARAMS);
	stInitParams.dwOptions = LINEINITIALIZEEXOPTION_USEEVENT;

	// Initialize TAPI.
	DWORD dwTAPIVer = TAPI_CURRENT_VERSION;
	LONG lRet = lineInitializeEx(&m_hLineApp, NULL, NULL, L"Sokrates", &m_dwNumDevs, &dwTAPIVer, &stInitParams);
	if(lRet)  //error
		return false;

	// Got the event handle...
	m_hLineEvent = stInitParams.Handles.hEvent;
	return true;
}

void CTapiLine::ShutdownTAPI()
{
	if(m_hLineApp)
		lineShutdown(m_hLineApp);
	m_hLineApp = NULL;
}

// if nMode = 0 then it is data/fax calls
//    nMode = 1 then it is data/fax/voice calls
int CTapiLine::Open(DWORD dwDeviceID, int nMode)
{
	LINEDEVCAPS *lpDevCaps =NULL;
	LONG lRet;
	
	DWORD dwMediaMode;
	DWORD dwTAPIVer =TAPI_CURRENT_VERSION;
	DWORD dwTmpVer =0;
	LINEEXTENSIONID stExtID;

	//if(!nMode)
	//	dwMediaMode = LINEMEDIAMODE_DATAMODEM; // data/fax this should be
	//else
	dwMediaMode = LINEMEDIAMODE_INTERACTIVEVOICE;// for voice this should be


	//ensure TAPI inited
	if(!m_hLineApp)
	{
		QMessageBox::information(NULL,"","TAPI not init");
		return LINEERR_BADDEVICEID; //TOFIX
	}

	m_nDevID = -1;
	if(-1 == dwDeviceID)
	{
		// go through the device list and select the appropriate device to transfer voice
		for(int i=0;i<m_dwNumDevs;i++)
		{
			lRet = lineNegotiateAPIVersion(m_hLineApp, i, dwTAPIVer, dwTAPIVer, &dwTmpVer, &stExtID);
			if(lRet != 0)
			{
				continue;
			}
					
			lpDevCaps = (LINEDEVCAPS *)malloc(sizeof(LINEDEVCAPS)+1024);// Allocate a little extra memory...
			memset(lpDevCaps, 0, sizeof(LINEDEVCAPS)+1024);
			lpDevCaps->dwTotalSize = sizeof(LINEDEVCAPS)+1024;

			////g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("TAPI call lineGetDevCaps"));

			lRet = lineGetDevCaps(m_hLineApp, i, dwTmpVer, 0, lpDevCaps);
			if(lRet)  //error
			{
				free(lpDevCaps);
				lpDevCaps=NULL;
				continue;
			}
			
			char *szText = ((char*)lpDevCaps) + lpDevCaps->dwLineNameOffset;// FOR DEBUGGING
			// for more refer LINEMEDIAMODE_ Constants in MSDN
	
			if(lpDevCaps->dwMediaModes & dwMediaMode) 
			{
				free(lpDevCaps);
				m_nDevID = i;
				break;
			}
		}
		free(lpDevCaps);
	}
	else{
		// go through the device list and select the appropriate device to transfer voice
		//for(int i=0;i<dwNumDevs;i++)
		{
			lRet = lineNegotiateAPIVersion(m_hLineApp, dwDeviceID, dwTAPIVer, dwTAPIVer, &dwTmpVer, &stExtID);
			if(lRet != 0)
			{
				QMessageBox::information(NULL,"Failed","lineNegotiateAPIVersion: ");
				return LINEERR_BADDEVICEID;//continue;
			}
					
			lpDevCaps = (LINEDEVCAPS *)malloc(sizeof(LINEDEVCAPS)+1024);// Allocate a little extra memory...

			memset(lpDevCaps, 0, sizeof(LINEDEVCAPS)+1024);
			lpDevCaps->dwTotalSize = sizeof(LINEDEVCAPS)+1024;

			//g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("TAPI call lineGetDevCaps"));

			lRet = lineGetDevCaps(m_hLineApp, dwDeviceID, dwTmpVer, 0, lpDevCaps);
			if(lRet)  //error
			{
				QMessageBox::information(NULL,"Failed","lineGetDevCaps: ");
				free(lpDevCaps);
				lpDevCaps=NULL;
				return LINEERR_BADDEVICEID;//continue;
			}
			
			char *szText = ((char*)lpDevCaps) + lpDevCaps->dwLineNameOffset;// FOR DEBUGGING
			// for more refer LINEMEDIAMODE_ Constants in MSDN
	/*
			if(lpDevCaps->dwMediaModes & dwMediaMode) 
			{
				free(lpDevCaps);
				m_nDevID = dwDeviceID;
				return LINEERR_BADDEVICEID; //break;
			}
	*/
			m_nDevID = dwDeviceID;
			free(lpDevCaps);
		}
	}
	
	if(m_nDevID < 0)
	{
		QMessageBox::information(NULL,"Failed","no device available: ");
		return LINEERR_BADDEVICEID; // no device available
	}

	//g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("TAPI call lineOpen (deviceID %1, Line %2, tapi ver %3, media mode %4, app %5)").arg(m_nDevID).arg(m_hLine).arg(dwTAPIVer).arg(dwMediaMode).arg(m_hLineApp));

	// Open the line...
	lRet = lineOpen(m_hLineApp, m_nDevID, &m_hLine, dwTAPIVer, 0x00000000, 1,LINECALLPRIVILEGE_OWNER,dwMediaMode,NULL);
	if(lRet)
	{
		char *szBuffer;
		GetErrorString(lRet, szBuffer);
		QMessageBox::information(NULL,"Failed",QString("lineOpen: %1").arg(szBuffer));
		return lRet;
	}
	
	//g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("TAPI call lineSetStatusMessages"));

	// We want to be notified for everything
	lRet = lineSetStatusMessages(m_hLine, 0x1ffffff, 0);
	if(lRet)
	{	
		QMessageBox::information(NULL,"Failed",QString("lineSetStatusMessages: %1").arg(lRet));
		return lRet;	
	}
	m_bStopLineEventThread = false;
	m_hLineMsgThread = CreateThread(NULL,NULL,LineEventThread,this,NULL,0);
	m_hEventFromThread = CreateEvent(NULL,0,0,NULL);
	return 0;
}

int CTapiLine::Close()
{
	LINECALLSTATUS stLineStatus;
	LONG lRet;

	qDebug() << "CTapiLine::Close";
	//g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("TAPI line close"));

	if(m_hCall)  // Call might be in progress...
	{
		qDebug() << "m_hCall != 0";
		//memset(&stLineStatus, 0, sizeof(LINECALLSTATUS));

		//lRet = lineGetCallStatus(m_hCall, &stLineStatus); 
			// Technically, lineGetCallStatus returns more info than 
			// there is in the structure.  Since we don't care about it,
			// We just go on our merry way...
		//if(!lRet) // If it didn't fail, there's at least a call that needs to be droped.
		{
			//g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("TAPI drop call: lineDrop"));

			qDebug() << "Drop Call";
			lineDrop(m_hCall, NULL, 0);
		}
	}
	else
		//g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("TAPI call is NULL"));
	m_hCall = NULL;

	if(m_hLine){
		LINECALLLIST *pList = (LINECALLLIST *)malloc(sizeof(LINECALLLIST) + 1000);
		ZeroMemory(pList, sizeof(LINECALLLIST));
		pList->dwTotalSize = sizeof(LINECALLLIST) + 1000;
		//call lineGetNewCalls to see if someone other uses this line as we speak!
		if(SUCCEEDED(lineGetNewCalls(m_hLine, 0, LINECALLSELECT_LINE, pList)))
		{
			//g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("TAPI call lineClose"));
			qDebug() << "Close Line";

			if(0 == pList->dwCallsNumEntries)
				lineClose(m_hLine); //it's safe to close the line
		}
		else
		{
			//g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("TAPI call lineGetNewCalls failed"));
			int i=0; //TOFIX handle error here
		}
		free(pList);
	}
	m_hLine = NULL;

	if(m_hLineMsgThread)
	{
		qDebug() << "Wait tapi thread to end";
		//g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("Wait tapi thread to end"));
		m_bStopLineEventThread = true;  // stop the event waiting thread
		WaitForSingleObject(m_hLineMsgThread, INFINITE); // Wait for it to comit suicide..
		CloseHandle(m_hLineMsgThread);
		CloseHandle(m_hEventFromThread);
		//g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("Wait tapi thread to end, done!"));
	}
	
	m_hLineMsgThread = NULL;
	m_hEventFromThread = NULL;
	m_hLineMsgThread = NULL;

	m_lEventThreadResult = 0; 
	m_dwLineMsg= 0;
	m_nDevID = -1;
	m_bConnected= false;
	m_bConnecting=false;
	//g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("TAPI line close, end"));
	return 0;
}


int CTapiLine::GetIncomingCall()
{
	long lRet;
	
	// set the ring b4 receiving the call
	lRet = lineSetNumRings(m_hLine,0,5);
	if(lRet)
		return lRet;
	
	// Now we wait for notification.
	switch(WaitForSingleObject(m_hEventFromThread, INFINITE))
	{
	case WAIT_OBJECT_0:
		if(m_dwLineMsg == LINECALLSTATE_OFFERING)
		{
			lRet = lineAnswer(m_hCall, NULL, 0);
			lRet = (lRet>0)?0:lRet;

			if(lRet)
				Close();
			return lRet;
		}
		break;
	case WAIT_TIMEOUT:
		return ERROR_TIMEOUT;
	};
	return 0xffffffff; // unknown error
}

int CTapiLine::PickupIncomingCall()
{
	long lRet;

	lRet = linePickup(m_hLine,0,&m_hCall,NULL,NULL);
	return (lRet>=0)?0:lRet;
}

int CTapiLine::MakeOutgoingCall(const char *szAddress)
{
	long lRet;

	LPLINECALLPARAMS lpCallParams;

	lpCallParams = (LPLINECALLPARAMS)malloc(sizeof(LINECALLPARAMS)+1024);
	memset(lpCallParams,0,sizeof(LINECALLPARAMS)+1024);
	lpCallParams->dwTotalSize = sizeof(LINECALLPARAMS)+1024;

    // This is where we configure the line for DATAMODEM usage.
    lpCallParams->dwBearerMode = LINEBEARERMODE_VOICE;
    lpCallParams->dwMediaMode  = LINEMEDIAMODE_INTERACTIVEVOICE;

    // This specifies that we want to use only IDLE calls and
    // don't want to cut into a call that might not be IDLE (ie, in use).
    lpCallParams->dwCallParamFlags = LINECALLPARAMFLAGS_IDLE;
                                    
    // if there are multiple addresses on line, use first anyway.
    // It will take a more complex application than a simple tty app
    // to use multiple addresses on a line anyway.
    lpCallParams->dwAddressMode = LINEADDRESSMODE_ADDRESSID;
    lpCallParams->dwAddressID = 0;

    // Address we are dialing.
    lpCallParams->dwDisplayableAddressOffset = sizeof(LINECALLPARAMS);
    lpCallParams->dwDisplayableAddressSize = strlen(szAddress);
    strcpy((LPSTR)lpCallParams+sizeof(LINECALLPARAMS), szAddress);
	
	m_bConnecting = true;

	//g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("TAPI call lineMakeCall(%1)").arg(szAddress));

	QString strNum(szAddress);
	lRet = lineMakeCall(m_hLine, &m_hCall, (LPCWSTR)strNum.utf16(), 0, lpCallParams);
	qDebug() << "lineMakeCall result (0 is OK):" << lRet << "Call:" << m_hCall;
	/*
	if(lRet)
	{
		char *szBuffer;
		GetErrorString(lRet, szBuffer);
		QMessageBox::information(NULL,"Failed",QString("lineMakeCall: %1").arg(szBuffer));
	}
	*/
	return (lRet>=0)?0:lRet;
}

//Author: Ramanan.T
HANDLE CTapiLine::GetHandle(const char *szClassType, long *lError)
{
	if(!m_bConnected)
	{
		*lError = ERROR_DEVICE_NOT_CONNECTED;
		return NULL;
	}

	VARSTRING *pvarStrDevID = (VARSTRING *)malloc(sizeof(VARSTRING)+255);

	memset(pvarStrDevID,0,sizeof(VARSTRING)+255);
	pvarStrDevID->dwTotalSize = sizeof(VARSTRING)+255;
	
	long lRet = lineGetID(m_hLine,0,m_hCall,LINECALLSELECT_LINE,pvarStrDevID, (LPWSTR)szClassType);
	if(lRet)
	{
		*lError = lRet;
		return NULL;
	}

	*lError = 0;

	return *((LPHANDLE)((char *)pvarStrDevID + pvarStrDevID->dwStringOffset));
}

//Author: Ramanan.T
DWORD WINAPI CTapiLine::LineEventThread(LPVOID lpVoid)
{
	CTapiLine *pcTapiLine = (CTapiLine *)lpVoid;
	long lRet;
	LINEMESSAGE stLineMsg;
	LINECALLINFO *lpCallInfo;

	//g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("LineEventThread start"));

	while(!pcTapiLine->m_bStopLineEventThread)
	{
		// Get a TAPI event if available, wait for 1s just enough to give up quontum
		switch(WaitForSingleObject(pcTapiLine->m_hLineEvent, 1000))
		{
		case WAIT_OBJECT_0:
			//g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("calling lineGetMessage"));

			//TAPI's got something
			if ((lRet = lineGetMessage(pcTapiLine->m_hLineApp, &stLineMsg, 0)) != 0)
			{
				pcTapiLine->m_lEventThreadResult = lRet;
				qDebug() << "TapiLineThread exit!";
				//g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("TapiLineThread exit1! Last thread result: %1").arg(lRet));
				return lRet;
			}
			// Process the retruned msg
			switch (stLineMsg.dwMessageID)
			{
			case LINE_REPLY: // Sent after lineMakeCall or lineDrop				
				{
				char *szErrTxt = NULL;
				pcTapiLine->GetErrorString((LONG)stLineMsg.dwParam2, szErrTxt);
				pcTapiLine->m_lEventThreadResult = (LONG)stLineMsg.dwParam2;
				qDebug() << "LINE_REPLY:" << szErrTxt;
				//g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("LINE_REPLY: %1 (thread res: %2)!").arg(szErrTxt).arg(pcTapiLine->m_lEventThreadResult));
				free(szErrTxt);
				}
				break;

			case LINE_CALLSTATE:  // Sent after change of call state
				qDebug() << "LINE_CALLSTATE:";
				//g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("LINE_CALLSTATE"));
				switch (stLineMsg.dwParam1)
				{
					case LINECALLSTATE_OFFERING:	//Incoming call is offering.
						qDebug() << "LINECALLSTATE_OFFERING";
						//g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("LINECALLSTATE_OFFERING"));
						//  Get the call handle
						pcTapiLine->m_hCall = (HCALL)stLineMsg.hDevice;
						////g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("LINECALLSTATE_OFFERING: call handle: %1!").arg(pcTapiLine->m_hCall));

						pcTapiLine->m_dwLineMsg = LINECALLSTATE_OFFERING;
						SetEvent(pcTapiLine->m_hEventFromThread);
						break;

					case LINECALLSTATE_IDLE:
						qDebug() << "LINECALLSTATE_IDLE";
						//g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("LINECALLSTATE_IDLE"));
						//lineDrop(pcTapiLine->m_hCall, NULL, 0);
						//pcTapiLine->m_hCall = NULL;
						////g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("LINECALLSTATE_OFFERING: call handle: %1!").arg(pcTapiLine->m_hCall));
						//pcTapiLine->m_lEventThreadResult = 0;
						//pcTapiLine->m_bConnected = false;
						//pcTapiLine->m_bConnecting = false;
						break;

					case LINECALLSTATE_CONNECTED:
						qDebug() << "LINECALLSTATE_CONNECTED";
						//g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("LINECALLSTATE_CONNECTED"));
						if(stLineMsg.dwCallbackInstance == 1)
						{
							pcTapiLine->m_dwLineMsg = LINECALLSTATE_CONNECTED;
							SetEvent(pcTapiLine->m_hEventFromThread);
							pcTapiLine->m_bConnected = true;
							pcTapiLine->m_bConnecting = false;
						}
						break;

					case LINECALLSTATE_DISCONNECTED:
						qDebug() << "LINECALLSTATE_DISCONNECTED";
						//g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("LINECALLSTATE_DISCONNECTED"));
						switch (stLineMsg.dwParam2) 
						{// currently these r not handled separately
							case LINEDISCONNECTMODE_NORMAL:
								qDebug() << "LINEDISCONNECTMODE_NORMAL"; break;
							case LINEDISCONNECTMODE_UNKNOWN:
								qDebug() << "LINEDISCONNECTMODE_UNKNOWN"; break;
							case LINEDISCONNECTMODE_REJECT:
								qDebug() << "LINEDISCONNECTMODE_REJECT"; break;
							case LINEDISCONNECTMODE_PICKUP:
								qDebug() << "LINEDISCONNECTMODE_PICKUP"; break;
							case LINEDISCONNECTMODE_FORWARDED:
								qDebug() << "LINEDISCONNECTMODE_FORWARDED"; break;
							case LINEDISCONNECTMODE_BUSY:
								qDebug() << "LINEDISCONNECTMODE_BUSY"; break;
							case LINEDISCONNECTMODE_NOANSWER:
								qDebug() << "LINEDISCONNECTMODE_NOANSWER"; break;
							case LINEDISCONNECTMODE_BADADDRESS:
								qDebug() << "LINEDISCONNECTMODE_BADADDRESS"; break;
							case LINEDISCONNECTMODE_UNREACHABLE:
								qDebug() << "LINEDISCONNECTMODE_UNREACHABLE"; break;
							case LINEDISCONNECTMODE_CONGESTION:
								qDebug() << "LINEDISCONNECTMODE_CONGESTION"; break;
							case LINEDISCONNECTMODE_INCOMPATIBLE:
								qDebug() << "LINEDISCONNECTMODE_INCOMPATIBLE"; break;
							case LINEDISCONNECTMODE_UNAVAIL:
								qDebug() << "LINEDISCONNECTMODE_UNAVAIL"; break;
							case LINEDISCONNECTMODE_NODIALTONE:
								qDebug() << "LINEDISCONNECTMODE_NODIALTONE"; break;
							default:
								break;
						}

						//g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("TAPI call #2 lineDrop"));

						pcTapiLine->m_bConnected = false;
						pcTapiLine->m_bConnecting = false;

						// Got disconnected, so drop the call
						lineDrop((HCALL)stLineMsg.hDevice, NULL, 0);
						////g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("drop handle: %1!").arg((HCALL)stLineMsg.hDevice));
						pcTapiLine->m_hCall = NULL;

						qDebug() << "TapiLineThread exit!";
						//g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("TapiLineThread exit!"));
						return 0;

					default:
						break;
				}
				break;

			case LINE_CALLINFO: //  Call Info is available
				qDebug() << "LINE_CALLINFO";
				//g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("LINE_CALLINFO"));
				if(stLineMsg.dwParam1 == LINECALLINFOSTATE_CALLID)
				{
					//g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("LINECALLINFOSTATE_CALLID"));

					//Caller ID became available.
					lpCallInfo = (LINECALLINFO *)malloc(sizeof(LINECALLINFO)+1024);
					memset(lpCallInfo, 0, sizeof(LINECALLINFO)+1024);
					lpCallInfo->dwTotalSize = sizeof(LINECALLINFO)+1024;
					lineGetCallInfo(pcTapiLine->m_hCall, lpCallInfo);
					if (lpCallInfo->dwTotalSize < lpCallInfo->dwNeededSize)
					{
						lpCallInfo = (LINECALLINFO *)realloc(lpCallInfo, lpCallInfo->dwNeededSize);
						lineGetCallInfo(pcTapiLine->m_hCall, lpCallInfo);
					}
				}
				else if(stLineMsg.dwParam1 == LINECALLINFOSTATE_APPSPECIFIC)
					qDebug() << "LINECALLINFOSTATE_APPSPECIFIC";
				else if(stLineMsg.dwParam1 == LINECALLINFOSTATE_BEARERMODE)
					qDebug() << "LINECALLINFOSTATE_BEARERMODE";
				else if(stLineMsg.dwParam1 == LINECALLINFOSTATE_CALLDATA)
					qDebug() << "LINECALLINFOSTATE_CALLDATA";
				else if(stLineMsg.dwParam1 == LINECALLINFOSTATE_CALLERID)
					qDebug() << "LINECALLINFOSTATE_CALLERID";
				else if(stLineMsg.dwParam1 == LINECALLINFOSTATE_CALLID)
					qDebug() << "LINECALLINFOSTATE_CALLID";
				else if(stLineMsg.dwParam1 == LINECALLINFOSTATE_CHARGINGINFO)
					qDebug() << "LINECALLINFOSTATE_CHARGINGINFO";
				else if(stLineMsg.dwParam1 == LINECALLINFOSTATE_COMPLETIONID)
					qDebug() << "LINECALLINFOSTATE_COMPLETIONID";
				else if(stLineMsg.dwParam1 == LINECALLINFOSTATE_CONNECTEDID)
					qDebug() << "LINECALLINFOSTATE_CONNECTEDID";
				else if(stLineMsg.dwParam1 == LINECALLINFOSTATE_DEVSPECIFIC)
					qDebug() << "LINECALLINFOSTATE_DEVSPECIFIC";
				else if(stLineMsg.dwParam1 == LINECALLINFOSTATE_DIALPARAMS)
					qDebug() << "LINECALLINFOSTATE_DIALPARAMS";
				else if(stLineMsg.dwParam1 == LINECALLINFOSTATE_DISPLAY)
					qDebug() << "LINECALLINFOSTATE_DISPLAY";
				else if(stLineMsg.dwParam1 == LINECALLINFOSTATE_HIGHLEVELCOMP)
					qDebug() << "LINECALLINFOSTATE_HIGHLEVELCOMP";
				else if(stLineMsg.dwParam1 == LINECALLINFOSTATE_LOWLEVELCOMP)
					qDebug() << "LINECALLINFOSTATE_LOWLEVELCOMP";
				else if(stLineMsg.dwParam1 == LINECALLINFOSTATE_MEDIAMODE)
					qDebug() << "LINECALLINFOSTATE_MEDIAMODE";
				else if(stLineMsg.dwParam1 == LINECALLINFOSTATE_MONITORMODES)
					qDebug() << "LINECALLINFOSTATE_MONITORMODES";
				else if(stLineMsg.dwParam1 == LINECALLINFOSTATE_NUMMONITORS)
					qDebug() << "LINECALLINFOSTATE_NUMMONITORS";
				else if(stLineMsg.dwParam1 == LINECALLINFOSTATE_NUMOWNERDECR)
					qDebug() << "LINECALLINFOSTATE_NUMOWNERDECR";
				else if(stLineMsg.dwParam1 == LINECALLINFOSTATE_NUMOWNERINCR)
					qDebug() << "LINECALLINFOSTATE_NUMOWNERINCR";
				else if(stLineMsg.dwParam1 == LINECALLINFOSTATE_ORIGIN)
					qDebug() << "LINECALLINFOSTATE_ORIGIN";
				else if(stLineMsg.dwParam1 == LINECALLINFOSTATE_OTHER)
					qDebug() << "LINECALLINFOSTATE_OTHER";
				else if(stLineMsg.dwParam1 == LINECALLINFOSTATE_QOS)
					qDebug() << "LINECALLINFOSTATE_QOS";
				else if(stLineMsg.dwParam1 == LINECALLINFOSTATE_RATE)
					qDebug() << "LINECALLINFOSTATE_RATE";
				else if(stLineMsg.dwParam1 == LINECALLINFOSTATE_REASON)
					qDebug() << "LINECALLINFOSTATE_REASON";
				else if(stLineMsg.dwParam1 == LINECALLINFOSTATE_REDIRECTINGID)
					qDebug() << "LINECALLINFOSTATE_REDIRECTINGID";
				else if(stLineMsg.dwParam1 == LINECALLINFOSTATE_REDIRECTIONID)
					qDebug() << "LINECALLINFOSTATE_REDIRECTIONID";
				else if(stLineMsg.dwParam1 == LINECALLINFOSTATE_RELATEDCALLID)
					qDebug() << "LINECALLINFOSTATE_RELATEDCALLID";
				else if(stLineMsg.dwParam1 == LINECALLINFOSTATE_TERMINAL)
					qDebug() << "LINECALLINFOSTATE_TERMINAL";
				else if(stLineMsg.dwParam1 == LINECALLINFOSTATE_TREATMENT)
					qDebug() << "LINECALLINFOSTATE_TREATMENT";
				else if(stLineMsg.dwParam1 == LINECALLINFOSTATE_TRUNK)
					qDebug() << "LINECALLINFOSTATE_TRUNK";
				else if(stLineMsg.dwParam1 == LINECALLINFOSTATE_USERUSERINFO)
					qDebug() << "LINECALLINFOSTATE_USERUSERINFO";
				else
					qDebug() << "Unknown LINE_CALLINFO state" << stLineMsg.dwParam1;
				break;

			default:
				break;
			};
			break;

		case WAIT_TIMEOUT:
			Sleep(1);
			//g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("WAIT_TIMEOUT"));
			break;

		default:
			Sleep(3);
			//g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("WaitForSingleObject default branch"));
			continue;
		}
	}

	pcTapiLine->m_bConnecting = false;
	qDebug() << "TapiLineThread exit!";
	//g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("TapiLineThread exit!"));
	return 0;
}

//Author: Ramanan.T
void CTapiLine::GetErrorString(int nError, char *&szErrText)
{
	//g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("TAPI line GetErrorString"));

	const int nBufSize = 1255;
	szErrText = (char*)malloc(nBufSize);
	switch(nError)
	{
	case LINEERR_ALLOCATED:
		strcpy(szErrText,"LINEERR_ALLOCATED");
		break;
	case LINEERR_BADDEVICEID:
		strcpy(szErrText,"LINEERR_BADDEVICEID");
		break;
	case LINEERR_BEARERMODEUNAVAIL:
		strcpy(szErrText,"LINEERR_BEARERMODEUNAVAIL");
		break;
	case LINEERR_CALLUNAVAIL:
		strcpy(szErrText,"LINEERR_CALLUNAVAIL");
		break;
	case LINEERR_COMPLETIONOVERRUN:
		strcpy(szErrText,"LINEERR_COMPLETIONOVERRUN");
		break;
	case LINEERR_CONFERENCEFULL:
		strcpy(szErrText,"LINEERR_CONFERENCEFULL");
		break;
	case LINEERR_DIALBILLING:
		strcpy(szErrText,"LINEERR_DIALBILLING");
		break;
	case LINEERR_DIALDIALTONE:
		strcpy(szErrText,"LINEERR_DIALDIALTONE");
		break;
	case LINEERR_DIALPROMPT:
		strcpy(szErrText,"LINEERR_DIALPROMPT");
		break;
	case LINEERR_DIALQUIET:
		strcpy(szErrText,"LINEERR_DIALQUIET");
		break;
	case LINEERR_INCOMPATIBLEAPIVERSION:
		strcpy(szErrText,"LINEERR_INCOMPATIBLEAPIVERSION");
		break;
	case LINEERR_INCOMPATIBLEEXTVERSION:
		strcpy(szErrText,"LINEERR_INCOMPATIBLEEXTVERSION");
		break;          
	case LINEERR_INIFILECORRUPT:
		strcpy(szErrText,"LINEERR_INIFILECORRUPT");
		break;                  
	case LINEERR_INUSE:
		strcpy(szErrText,"LINEERR_INUSE");
		break;                           
	case LINEERR_INVALADDRESS:
		strcpy(szErrText,"LINEERR_INVALADDRESS");
		break;                    
	case LINEERR_INVALADDRESSID:
		strcpy(szErrText,"LINEERR_INVALADDRESSID");
		break;                  
	case LINEERR_INVALADDRESSMODE:
		strcpy(szErrText,"LINEERR_INVALADDRESSMODE");
		break;                
	case LINEERR_INVALADDRESSSTATE:
		strcpy(szErrText,"LINEERR_INVALADDRESSSTATE");
		break;               
	case LINEERR_INVALAPPHANDLE:
		strcpy(szErrText,"LINEERR_INVALAPPHANDLE");
		break;                  
	case LINEERR_INVALAPPNAME:
		strcpy(szErrText,"LINEERR_INVALAPPNAME");
		break;                    
	case LINEERR_INVALBEARERMODE:
		strcpy(szErrText,"LINEERR_INVALBEARERMODE");
		break;                 
	case LINEERR_INVALCALLCOMPLMODE:
		strcpy(szErrText,"LINEERR_INVALCALLCOMPLMODE");
		break;              
	case LINEERR_INVALCALLHANDLE:
		strcpy(szErrText,"LINEERR_INVALCALLHANDLE");
		break;                 
	case LINEERR_INVALCALLPARAMS:
		strcpy(szErrText,"LINEERR_INVALCALLPARAMS");
		break;                 
	case LINEERR_INVALCALLPRIVILEGE:
		strcpy(szErrText,"LINEERR_INVALCALLPRIVILEGE");
		break;              
	case LINEERR_INVALCALLSELECT:
		strcpy(szErrText,"LINEERR_INVALCALLSELECT");
		break;                 
	case LINEERR_INVALCALLSTATE:
		strcpy(szErrText,"LINEERR_INVALCALLSTATE");
		break;                  
	case LINEERR_INVALCALLSTATELIST:
		strcpy(szErrText,"LINEERR_INVALCALLSTATELIST");
		break;              
	case LINEERR_INVALCARD:
		strcpy(szErrText,"LINEERR_INVALCARD");
		break;                       
	case LINEERR_INVALCOMPLETIONID:
		strcpy(szErrText,"LINEERR_INVALCOMPLETIONID");
		break;               
	case LINEERR_INVALCONFCALLHANDLE:
		strcpy(szErrText,"LINEERR_INVALCONFCALLHANDLE");
		break;             
	case LINEERR_INVALCONSULTCALLHANDLE:
		strcpy(szErrText,"LINEERR_INVALCONSULTCALLHANDLE");
		break;          
	case LINEERR_INVALCOUNTRYCODE:
		strcpy(szErrText,"LINEERR_INVALCOUNTRYCODE");
		break;                
	case LINEERR_INVALDEVICECLASS:
		strcpy(szErrText,"LINEERR_INVALDEVICECLASS");
		break;                
	case LINEERR_INVALDEVICEHANDLE:
		strcpy(szErrText,"LINEERR_INVALDEVICEHANDLE");
		break;               
	case LINEERR_INVALDIALPARAMS:
		strcpy(szErrText,"LINEERR_INVALDIALPARAMS");
		break;                 
	case LINEERR_INVALDIGITLIST:
		strcpy(szErrText,"LINEERR_INVALDIGITLIST");
		break;                  
	case LINEERR_INVALDIGITMODE:
		strcpy(szErrText,"LINEERR_INVALDIGITMODE");
		break;                  
	case LINEERR_INVALDIGITS:
		strcpy(szErrText,"LINEERR_INVALDIGITS");
		break;                    
	case LINEERR_INVALEXTVERSION:
		strcpy(szErrText,"LINEERR_INVALEXTVERSION");
		break;                 
	case LINEERR_INVALGROUPID:
		strcpy(szErrText,"LINEERR_INVALGROUPID");
		break;                    
	case LINEERR_INVALLINEHANDLE:
		strcpy(szErrText,"LINEERR_INVALLINEHANDLE");
		break;                
	case LINEERR_INVALLINESTATE:
		strcpy(szErrText,"LINEERR_INVALLINESTATE");
		break;                  
	case LINEERR_INVALLOCATION:
		strcpy(szErrText,"LINEERR_INVALLOCATION");
		break;                   
	case LINEERR_INVALMEDIALIST:
		strcpy(szErrText,"LINEERR_INVALMEDIALIST");
		break;                  
	case LINEERR_INVALMEDIAMODE:
		strcpy(szErrText,"LINEERR_INVALMEDIAMODE");
		break;                  
	case LINEERR_INVALMESSAGEID:
		strcpy(szErrText,"LINEERR_INVALMESSAGEID");
		break;                  
	case LINEERR_INVALPARAM:
		strcpy(szErrText,"LINEERR_INVALPARAM");
		break;                      
	case LINEERR_INVALPARKID:
		strcpy(szErrText,"LINEERR_INVALPARKID");
		break;                     
	case LINEERR_INVALPARKMODE:
		strcpy(szErrText,"LINEERR_INVALPARKMODE");
		break;                   
	case LINEERR_INVALPOINTER:
		strcpy(szErrText,"LINEERR_INVALPOINTER");
		break;                    
	case LINEERR_INVALPRIVSELECT:
		strcpy(szErrText,"LINEERR_INVALPRIVSELECT");
		break;                 
	case LINEERR_INVALRATE:
		strcpy(szErrText,"LINEERR_INVALRATE");
		break;                       
	case LINEERR_INVALREQUESTMODE:
		strcpy(szErrText,"LINEERR_INVALREQUESTMODE");
		break;                
	case LINEERR_INVALTERMINALID:
		strcpy(szErrText,"LINEERR_INVALTERMINALID");
		break;                 
	case LINEERR_INVALTERMINALMODE:
		strcpy(szErrText,"LINEERR_INVALTERMINALMODE");
		break;               
	case LINEERR_INVALTIMEOUT:
		strcpy(szErrText,"LINEERR_INVALTIMEOUT");
		break;                    
	case LINEERR_INVALTONE:
		strcpy(szErrText,"LINEERR_INVALTONE");
		break;                       
	case LINEERR_INVALTONELIST:
		strcpy(szErrText,"LINEERR_INVALTONELIST");
		break;                   
	case LINEERR_INVALTONEMODE:
		strcpy(szErrText,"LINEERR_INVALTONEMODE");
		break;                   
	case LINEERR_INVALTRANSFERMODE:
		strcpy(szErrText,"LINEERR_INVALTRANSFERMODE");
		break;               
	case LINEERR_LINEMAPPERFAILED:
		strcpy(szErrText,"LINEERR_LINEMAPPERFAILED");
		break;                
	case LINEERR_NOCONFERENCE:
		strcpy(szErrText,"LINEERR_NOCONFERENCE");
		break;                    
	case LINEERR_NODEVICE:
		strcpy(szErrText,"LINEERR_NODEVICE");
		break;                        
	case LINEERR_NODRIVER:
		strcpy(szErrText,"LINEERR_NODRIVER");
		break;                        
	case LINEERR_NOMEM:
		strcpy(szErrText,"LINEERR_NOMEM");
		break;                           
	case LINEERR_NOREQUEST:
		strcpy(szErrText,"LINEERR_NOREQUEST");
		break;                       
	case LINEERR_NOTOWNER:
		strcpy(szErrText,"LINEERR_NOTOWNER");
		break;                        
	case LINEERR_NOTREGISTERED:
		strcpy(szErrText,"LINEERR_NOTREGISTERED");
		break;                   
	case LINEERR_OPERATIONFAILED:
		strcpy(szErrText,"LINEERR_OPERATIONFAILED");
		break;                 
	case LINEERR_OPERATIONUNAVAIL:
		strcpy(szErrText,"LINEERR_OPERATIONUNAVAIL");
		break;                
	case LINEERR_RATEUNAVAIL:
		strcpy(szErrText,"LINEERR_RATEUNAVAIL");
		break;                     
	case LINEERR_RESOURCEUNAVAIL:
		strcpy(szErrText,"LINEERR_RESOURCEUNAVAIL");
		break;                 
	case LINEERR_REQUESTOVERRUN:
		strcpy(szErrText,"LINEERR_REQUESTOVERRUN");
		break;                  
	case LINEERR_STRUCTURETOOSMALL:
		strcpy(szErrText,"LINEERR_STRUCTURETOOSMALL");
		break;               
	case LINEERR_TARGETNOTFOUND:
		strcpy(szErrText,"LINEERR_TARGETNOTFOUND");
		break;                  
	case LINEERR_TARGETSELF:
		strcpy(szErrText,"LINEERR_TARGETSELF");
		break;                      
	case LINEERR_UNINITIALIZED:
		strcpy(szErrText,"LINEERR_UNINITIALIZED");
		break;                   
	case LINEERR_USERUSERINFOTOOBIG:
		strcpy(szErrText,"LINEERR_USERUSERINFOTOOBIG");
		break;           
	case LINEERR_REINIT:
		strcpy(szErrText,"LINEERR_REINIT");
		break;
	case LINEERR_ADDRESSBLOCKED:
		strcpy(szErrText,"LINEERR_ADDRESSBLOCKED");
		break;
	case LINEERR_BILLINGREJECTED:
		strcpy(szErrText,"LINEERR_BILLINGREJECTED");
		break;
	case LINEERR_INVALFEATURE:
		strcpy(szErrText,"LINEERR_INVALFEATURE");
		break;
	case LINEERR_NOMULTIPLEINSTANCE:
		strcpy(szErrText,"LINEERR_NOMULTIPLEINSTANCE");
		break;
	case LINEERR_INVALAGENTID:
		strcpy(szErrText,"LINEERR_INVALAGENTID");
		break;
	case LINEERR_INVALAGENTGROUP:
		strcpy(szErrText,"LINEERR_INVALAGENTGROUP");
		break;
	case LINEERR_INVALPASSWORD:
		strcpy(szErrText,"LINEERR_INVALPASSWORD");
		break;
	case LINEERR_INVALAGENTSTATE:
		strcpy(szErrText,"LINEERR_INVALAGENTSTATE");
		break;
	case LINEERR_INVALAGENTACTIVITY:
		strcpy(szErrText,"LINEERR_INVALAGENTACTIVITY");
		break;
	case LINEERR_DIALVOICEDETECT:
		strcpy(szErrText,"LINEERR_DIALVOICEDETECT");
		break;
	default:
		sprintf(szErrText,"Error: %d", nError);
		//FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM|FORMAT_MESSAGE_IGNORE_INSERTS,NULL,nError,0,(LPTSTR)&szErrText,nBufSize,NULL);
	};
}

int CTapiLine::GetDeviceCount()
{
	return m_dwNumDevs;
}
	
bool CTapiLine::GetDeviceInfo(DWORD dwDeviceID, QString &strDeviceName, bool &bVoiceCalls)
{
	//g_Logger.logMessage(StatusCodeSet::TYPE_ERROR,0, QString("TAPI line GetDeviceInfo ID=%1").arg(dwDeviceID));

	int nBuffExtSize = 4024;
	LINEDEVCAPS *lpDevCaps = (LINEDEVCAPS *)malloc(sizeof(LINEDEVCAPS)+nBuffExtSize);// Allocate a little extra memory...
	memset(lpDevCaps, 0, sizeof(LINEDEVCAPS)+nBuffExtSize);
	lpDevCaps->dwTotalSize = sizeof(LINEDEVCAPS)+nBuffExtSize;

	DWORD dwTAPIVer = TAPI_CURRENT_VERSION;
	DWORD dwTmpVer = 0;
	LINEEXTENSIONID stExtID;
	LONG lRet = lineNegotiateAPIVersion(m_hLineApp, dwDeviceID, dwTAPIVer, dwTAPIVer, &dwTmpVer, &stExtID);
	if(lRet != 0){
		free(lpDevCaps);
		return false;
	}

	lRet = lineGetDevCaps(m_hLineApp, dwDeviceID, dwTmpVer, 0, lpDevCaps);
	if(lRet)  //error
	{
		free(lpDevCaps);
		return false;
	}
	
	char *szText = ((char*)lpDevCaps) + lpDevCaps->dwLineNameOffset;// FOR DEBUGGING
	// for more refer LINEMEDIAMODE_ Constants in MSDN
	strDeviceName = QString::fromUtf16((const ushort *)szText);

	// Check to see if basic data/voice capabilities are available.
	if (!(lpDevCaps->dwBearerModes & LINEBEARERMODE_VOICE))
		bVoiceCalls = false;
	else
		bVoiceCalls = true;

	free(lpDevCaps);
	return true;
}

#ifdef WINCE

// ***************************************************************************
// Function Name: SendSMS
// 
// Purpose: Send an SMS Message
//
// Arguments: none
//
// Return Values: none
//
// Description:
//	Called after everything has been set up, this function merely opens an
//	SMS_HANDLE and tries to send the SMS Message.
 
void SendSMS(bool bSendConfirmation, bool bUseDefaultSMSC, LPCTSTR lpszSMSC, LPCTSTR lpszRecipient, LPCTSTR lpszMessage)
{
	SMS_HANDLE smshHandle;
	SMS_ADDRESS smsaSource;
	SMS_ADDRESS smsaDestination;
	TEXT_PROVIDER_SPECIFIC_DATA tpsd;
	SMS_MESSAGE_ID smsmidMessageID;

	// try to open an SMS Handle
	if(FAILED(SmsOpen(SMS_MSGTYPE_TEXT, SMS_MODE_SEND, &smshHandle, NULL)))
	{
		QMessageBox::warning(NULL, "", QObject::tr("Can not open a SMS handle!"));
		return;
	}

	// Create the source address
	if(!bUseDefaultSMSC)
	{
		smsaSource.smsatAddressType = SMSAT_INTERNATIONAL;
		_tcsncpy(smsaSource.ptsAddress, lpszSMSC, SMS_MAX_ADDRESS_LENGTH);
	}

	// Create the destination address
	smsaDestination.smsatAddressType = SMSAT_INTERNATIONAL;
	_tcsncpy(smsaDestination.ptsAddress, lpszRecipient, SMS_MAX_ADDRESS_LENGTH);

	// Set up provider specific data
    memset(&tpsd, 0, sizeof(tpsd));
	tpsd.dwMessageOptions = bSendConfirmation ? PS_MESSAGE_OPTION_STATUSREPORT : PS_MESSAGE_OPTION_NONE;
	tpsd.psMessageClass = PS_MESSAGE_CLASS1;
	tpsd.psReplaceOption = PSRO_NONE;
	tpsd.dwHeaderDataSize = 0;

	// Send the message, indicating success or failure
	HRESULT hRes = SmsSendMessage(smshHandle, ((bUseDefaultSMSC) ? NULL : &smsaSource), 
		&smsaDestination, NULL, (PBYTE) lpszMessage, 
		_tcslen(lpszMessage) * sizeof(TCHAR), (PBYTE) &tpsd, 
		sizeof(TEXT_PROVIDER_SPECIFIC_DATA), SMSDE_OPTIMAL, 
		SMS_OPTION_DELIVERY_NONE, &smsmidMessageID);

	if(SUCCEEDED(hRes))
	{
		QMessageBox::information(NULL, "", QObject::tr("Your message has been sent successfully!"));
	}
	else
	{
		QString strErr;
		switch(hRes){
		case E_INVALIDARG:
			strErr = "E_INVALIDARG"; break;
		case E_OUTOFMEMORY:
			strErr = "E_OUTOFMEMORY"; break;
		case E_UNEXPECTED:
			strErr = "E_UNEXPECTED"; break;
		case E_FAIL:
			strErr = "E_FAIL"; break;
		default:
			strErr = "Unknown";
		}
		QMessageBox::warning(NULL, "", QObject::tr("There was a problem sending your message (%1)!").arg(strErr));
	}

	// clean up
	VERIFY(SUCCEEDED(SmsClose(smshHandle)));
}

BOOL ConnectToNetwork(int timeoutsecs)
{
  // handle to connection to start
  HANDLE hConnection = NULL;

  // stores return value identifying status of connection attempt
  DWORD dwStatus;

  // initialise connection info structure
  CONNMGR_CONNECTIONINFO pConnectionInfo;
  ZeroMemory(&pConnectionInfo, sizeof(CONNMGR_CONNECTIONINFO));

  // set structure size
  pConnectionInfo.cbSize = sizeof(CONNMGR_CONNECTIONINFO);

  // set priority to identify that a user initiated this request
  // and the GUI is waiting on the creation of the connection
  pConnectionInfo.dwPriority = CONNMGR_PRIORITY_USERINTERACTIVE;

  // identify the network to connect to
  pConnectionInfo.dwParams = CONNMGR_PARAM_GUIDDESTNET;
  pConnectionInfo.guidDestNet = IID_DestNetInternet;

  // specify that other applications can use this connection
  pConnectionInfo.bExclusive = FALSE;

  // specify that a connection should be made
  pConnectionInfo.bDisabled = FALSE;

  // request connection
  HRESULT hr = ConnMgrEstablishConnectionSync(&pConnectionInfo,
                                              &hConnection,
                                              timeoutsecs * 1000,
                                              &dwStatus);

  if (hr == S_OK)
  {
    return TRUE;
  }
  else
  {
    switch (dwStatus)
    {
    case CONNMGR_STATUS_DISCONNECTED:
      MessageBox(NULL,TEXT("The connection has been disconnected"),TEXT("Connection Manager"),MB_ICONERROR);
      break;
    case CONNMGR_STATUS_WAITINGFORPATH:
      MessageBox(NULL,TEXT("A path to the destination exists but is not presently available"),TEXT("Connection Manager"),MB_ICONERROR);
      break;
    case CONNMGR_STATUS_WAITINGFORRESOURCE:
      MessageBox(NULL,TEXT("Another client is using resources that this connection requires"),TEXT("Connection Manager"),MB_ICONERROR);
      break;
    case CONNMGR_STATUS_WAITINGFORPHONE:
      MessageBox(NULL,TEXT("Connection cannot be made while call in progress"),TEXT("Connection Manager"),MB_ICONERROR);
      break;
    case CONNMGR_STATUS_NOPATHTODESTINATION:
      MessageBox(NULL,TEXT("No path to the destination could be found"),TEXT("Connection Manager"),MB_ICONERROR);
      break;
    case CONNMGR_STATUS_CONNECTIONFAILED:
      MessageBox(NULL,TEXT("The connection failed and cannot be reestablished"),TEXT("Connection Manager"),MB_ICONERROR);
      break;
    case CONNMGR_STATUS_CONNECTIONCANCELED:
      MessageBox(NULL,TEXT("The user aborted the connection"),TEXT("Connection Manager"),MB_ICONERROR);
      break;
    case CONNMGR_STATUS_WAITINGCONNECTION:
      MessageBox(NULL,TEXT("The device is attempting to connect"),TEXT("Connection Manager"),MB_ICONERROR);
      break;
    case CONNMGR_STATUS_WAITINGCONNECTIONABORT:
      MessageBox(NULL,TEXT("The device is aborting the connection attempt"),TEXT("Connection Manager"),MB_ICONERROR);
      break;
    case CONNMGR_STATUS_WAITINGDISCONNECTION:
      MessageBox(NULL,TEXT("The connection is being brought down"),TEXT("Connection Manager"),MB_ICONERROR);
      break;
    default:
      MessageBox(NULL,TEXT("The connection attempt failed"),TEXT("Connection Manager"),MB_ICONERROR);
      break;
    }
    return FALSE;
  }
}
#endif //WINCE
