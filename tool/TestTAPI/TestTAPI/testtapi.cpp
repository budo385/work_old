#include "testtapi.h"
//#include "mobilehelper.h"
//#include "bus_client/bus_client/clientoptionsandsettingsmanager.h"
//extern ClientOptionsAndSettingsManager* g_pSettings;
//#include "gui_core/gui_core/thememanager.h"
//#include "gui_core/gui_core/gui_helper.h"
//#include <QHeaderView>
#include <QTimerEvent>
#include <QMessageBox>
#include <QDebug>

TestTAPI::TestTAPI(QWidget *parent, Qt::WFlags flags)
	: QMainWindow(parent, flags)
{
	ui.setupUi(this);

	//this->setWindowFlags(Qt::Window);
	m_bCallInProgress=false;
	m_nTimerID=-1;

	
	if(!m_lineTAPI.InitTAPI())
	{
		QMessageBox::critical(NULL,"Error","TAPI can not be init");

	}

	int nCnt = m_lineTAPI.GetDeviceCount();
	for(int i=0; i<nCnt; i++)
	{
		QString strDeviceName;
		bool bVoiceCalls = false;
		if(m_lineTAPI.GetDeviceInfo(i, strDeviceName, bVoiceCalls))
			if(bVoiceCalls){
				ui.cboTAPI->addItem(strDeviceName,i);
			}
	}

	//m_nTapiDeviceID=0;

}

TestTAPI::~TestTAPI()
{
	ClearCall();
	m_lineTAPI.ShutdownTAPI();
}

void TestTAPI::on_btnCallAction1_clicked()
{
	qDebug() << "TestTAPI::on_btnCallAction1_clicked";

	if (m_bCallInProgress)
	{
		qDebug() << "end call";
		ClearCall();
	}
	else
	{
		qDebug() << "start call";

		QMessageBox::information(NULL,"","opening device");
		int nDeviceID=ui.cboTAPI->itemData(ui.cboTAPI->currentIndex()).toInt();
		if(0 != m_lineTAPI.Open(nDeviceID))
		{
			QMessageBox::information(NULL, "", tr("Could not open requested TAPI device: ")+GetTapiDeviceName(ui.cboTAPI->currentIndex()));
			OnError();
			return;
		}
		QMessageBox::information(NULL,"","opening ok, calling");
		QString strNumToCall = ui.lineEdit->text();

		int nRet = m_lineTAPI.MakeOutgoingCall(strNumToCall.toLocal8Bit().constData());
		if(0 == nRet)
		{
			//QMessageBox::information(NULL,"","calling ok, calling");
			ui.btnCallAction1->setText("Hangup");
			//qDebug()<<ui.lineTimer->text().toInt();
			m_nTimerID=startTimer(1000 /*ui.lineTimer->text().toInt()*/);
			m_bCallInProgress=true;
		}
		else
		{
			OnError();
		}
	}

}
QString TestTAPI::GetTapiDeviceName(int nTapiDevID)
{
	return ui.cboTAPI->itemText(nTapiDevID);

}


void TestTAPI::OnTAPICheckTimer()
{
	int nResult = m_lineTAPI.GetThreadResult();
	if(nResult != 0)
	{
		char *szErrTxt = NULL;
		m_lineTAPI.GetErrorString(nResult, szErrTxt);
		if(0 == strcmp("LINEERR_OPERATIONUNAVAIL", szErrTxt))
			QMessageBox::information(NULL, "", tr("Phone Line Occupied!"));
		else if(0 == strcmp("LINEERR_OPERATIONFAILED", szErrTxt))
			QMessageBox::information(NULL, "", tr("Phone Operation Failed!"));
		else
			QMessageBox::information(NULL, "", tr("TAPI Error (TimerCheck): %1!").arg(szErrTxt));
		free(szErrTxt);
		ClearCall();
		OnError();
		return;
	}

	//terminated?
	if( m_bCallInProgress && 
		!m_lineTAPI.IsConnecting() &&
		!m_lineTAPI.IsConnected())
	{
		ClearCall();
	}
}



void TestTAPI::timerEvent ( QTimerEvent * event )
{
	//find message:
	int nTimerID=event->timerId();
	if (nTimerID==m_nTimerID)
	{
		OnTAPICheckTimer();
	}

}

void TestTAPI::ClearCall()
{
	if (m_nTimerID!=-1)
		killTimer(m_nTimerID);

	m_lineTAPI.Close();
	ui.btnCallAction1->setText("Call Now!");
	m_nTimerID=-1;
	m_bCallInProgress=false;
}


void TestTAPI::OnError()
{
	//asked: change tapi device or exit...
	int nResult=QMessageBox::question(NULL,tr("Error"),tr("Error occurred while trying to use TAPI device: ")+GetTapiDeviceName(ui.cboTAPI->currentIndex())+tr(" Do you want to select another TAPI device or to exit?"),tr("Select another TAPI device"),tr("Exit"));
	if (nResult==0)
	{
		
	}
	else
	{
		close();  //exit fui
	} 
}

