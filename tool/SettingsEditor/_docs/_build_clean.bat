@ECHO OFF
REM 
REM Script to clean Atol project tree on windows (delete temporary files)
REM 	NOTE: requires some recent Windows (NT based)
REM

IF NOT "%OS%"=="Windows_NT" GOTO :End

rmdir /S /Q ..\Debug
rmdir /S /Q ..\Release
rmdir /S /Q ..\GeneratedFiles
rmdir /S /Q ..\Deleted
del /Q ..\*.ncb

rmdir /S /Q ..\SettingsEditor\Debug
rmdir /S /Q ..\SettingsEditor\Release
rmdir /S /Q ..\SettingsEditor\GeneratedFiles
rmdir /S /Q ..\SettingsEditor\Deleted

del /Q ..\SettingsEditor\*.user
del /Q ..\SettingsEditor\*.aps
del /F /Q /A H ..\*.suo


:End
