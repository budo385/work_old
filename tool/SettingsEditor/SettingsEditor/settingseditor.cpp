#include "settingseditor.h"
#include <QFileDialog>
#include <QMessageBox>
#include <QInputDialog>

#include "common/common/config.h"
#include "trans/trans/networkconnections.h"
#include "db_core/db_core/dbconnectionslist.h"
#include "gui_core/gui_core/dbconnsettingsgui.h"
#include "gui_core/gui_core/networkconnectionsdlg.h"

void EditDatabaseSettings(QString strFile);
void EditNetworkSettings(QString strFile);

extern QString g_strCmdLineDocument;
extern bool	g_bDbSettingsType;

SettingsEditor::SettingsEditor(QWidget *parent, Qt::WFlags flags)
    : QMainWindow(parent, flags)
{
	ui.setupUi(this);
	ui.radDb->setChecked(1);

	//QMessageBox::information(NULL, "", g_strCmdLineDocument);

	if(!g_strCmdLineDocument.isEmpty()){
		ui.editFilePath->setText(g_strCmdLineDocument);
		if(!g_bDbSettingsType)
			ui.radNet->setChecked(1);

		on_btnEdit_clicked();
	}
}

SettingsEditor::~SettingsEditor()
{
}

void SettingsEditor::on_btnPickFile_clicked()
{
	QString	strFile = QFileDialog::getOpenFileName(
                    NULL,
                    "Choose a file",
					QCoreApplication::applicationDirPath(),
                    "All files (*.*)");

	if(!strFile.isEmpty())
		ui.editFilePath->setText(strFile);
}

void SettingsEditor::on_btnEdit_clicked()
{
	QString strFile = ui.editFilePath->text();
	if(strFile.isEmpty()){
		QMessageBox::information(this, "Warning", "Please select a file to be edited!");
		return;
	}

	if(ui.radDb->isChecked())
	{
		EditDatabaseSettings(strFile);
	}
	else
	{
		EditNetworkSettings(strFile);
	}
}

void EditDatabaseSettings(QString strFile)
{
	DbConnectionsList objData;
	objData.Load(strFile, MASTER_PASSWORD);

	//ask for password if the one was set
	if(objData.IsUserPasswordSet())
	{
		bool ok;
		QString text = QInputDialog::getText(NULL, "User Settings Password",
											 "Password:", QLineEdit::Password,
											 "", &ok);
		if (ok)	//empty pass allowed
		{
			if(!objData.MatchUserPassword(text)){
				QMessageBox::information(NULL, "Error", "Password does not match!");
				return;
			}
		}
		else
			return;
	}

	// Now display the GUI dialog
	DbConnSettingsGUI dlg;
	dlg.SetConnections(objData.m_lstConnections, -1);
	dlg.SetPassword(objData.IsUserPasswordSet(), 
					objData.GetPasswordHash());
	
	//display dialog	
	int nRes = dlg.exec();
	if(nRes > 0)
	{
		//save changes from the dialog
		objData.m_lstConnections = dlg.GetConnections();

		bool bUsePass = false;
		QString strHash;
		dlg.GetPassword(bUsePass, strHash);

		if(bUsePass)
			objData.SetUserPasswordHash(strHash);
		else
			objData.ClearUserPassword();

		objData.Save(strFile, MASTER_PASSWORD);
	}
}

void EditNetworkSettings(QString strFile)
{
	NetworkConnections objData;
	objData.Load(strFile, MASTER_PASSWORD);

	//ask for password if the one was set
	if(objData.IsUserPasswordSet())
	{
		bool ok;
        QString text = QInputDialog::getText(NULL, "User Settings Password",
                                             "Password:", QLineEdit::Password,
                                             "", &ok);
        if (ok)	//empty pass allowed
		{
			if(!objData.MatchUserPassword(text)){
				QMessageBox::information(NULL, "Error", "Password does not match!");
				return;
			}
		}
		else
			return;
	}

	// Now display the GUI dialog
	NetworkConnectionsDlg dlg;
	dlg.SetConnections(objData.m_lstConnections, -1);
	dlg.SetPassword(objData.IsUserPasswordSet(), 
					objData.GetPasswordHash());
	
	//display dialog	
	int nRes = dlg.exec();
	if(nRes > 0)
	{
		//save changes from the dialog
		objData.m_lstConnections = dlg.GetConnections();

		bool bUsePass = false;
		QString strHash;
		dlg.GetPassword(bUsePass, strHash);

		if(bUsePass)
			objData.SetUserPasswordHash(strHash);
		else
			objData.ClearUserPassword();

		objData.Save(strFile, MASTER_PASSWORD);
	}
}