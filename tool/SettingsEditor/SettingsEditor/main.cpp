#include <QtGui/QApplication>
#include "settingseditor.h"

bool	g_bDbSettingsType = false;	//true - DB, false - Network
QString g_strCmdLineDocument;

int main(int argc, char *argv[])
{
	if(argc >= 2){
		if("-db" == QString(argv[1])){
			g_bDbSettingsType = true;
			g_strCmdLineDocument = argv[2];
		}
		else
			g_strCmdLineDocument = argv[1];
	}

    QApplication a(argc, argv);
    SettingsEditor w;
    w.show();
    a.connect(&a, SIGNAL(lastWindowClosed()), &a, SLOT(quit()));
    return a.exec();
}
