#ifndef SETTINGSEDITOR_H
#define SETTINGSEDITOR_H

#include <QtGui/QMainWindow>
#include "ui_settingseditor.h"

class SettingsEditor : public QMainWindow
{
    Q_OBJECT

public:
    SettingsEditor(QWidget *parent = 0, Qt::WFlags flags = 0);
    ~SettingsEditor();

private:
    Ui::SettingsEditorClass ui;

private slots:
	void on_btnEdit_clicked();
	void on_btnPickFile_clicked();
};

#endif // SETTINGSEDITOR_H
