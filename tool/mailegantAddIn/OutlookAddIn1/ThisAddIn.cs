﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Outlook = Microsoft.Office.Interop.Outlook;
using Office = Microsoft.Office.Core;

namespace mailegantAddIn
{
    public partial class ThisAddIn
    {
        public System.IO.StreamWriter logFile = null;

        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
            //System.Windows.Forms.MessageBox.Show(DateTime.Now.ToLongTimeString() + ": ThisAddIn_Startup()", "Log");
            ////if (null != logFile)
            ////    logFile.WriteLine(DateTime.Now.ToLongTimeString() + ": ThisAddIn_Startup");
        }

        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
            //System.Windows.Forms.MessageBox.Show(DateTime.Now.ToLongTimeString() + ": ThisAddIn_Shudown()", "Log");
            ////if (null != logFile){
            ////    logFile.WriteLine(DateTime.Now.ToLongTimeString() + ": ThisAddIn_Shudown");
            ////    logFile = null;
            ////}
        }

        protected override Microsoft.Office.Core.IRibbonExtensibility CreateRibbonExtensibilityObject()
        {
            //System.Windows.Forms.MessageBox.Show(DateTime.Now.ToLongTimeString() + ": CreateRibbonExtensibilityObject()", "Log");
            ////if (null != logFile)
            ////    logFile.WriteLine(DateTime.Now.ToLongTimeString() + ": CreateRibbonExtensibilityObject");
            return new Ribbon();
        }

        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            //System.Windows.Forms.MessageBox.Show(DateTime.Now.ToLongTimeString() + ": InternalStartup()", "Log");
            ////logFile = new System.IO.StreamWriter("C:\\Users\\Public\\mailegantWriter_log.txt");
            ////if (null != logFile)
            ////    logFile.WriteLine(DateTime.Now.ToLongTimeString() + ": InternalStartup");

            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }
        
        #endregion
    }
}
