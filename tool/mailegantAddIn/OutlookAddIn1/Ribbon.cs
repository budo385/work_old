﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using Office = Microsoft.Office.Core;

// TODO:  Follow these steps to enable the Ribbon (XML) item:

// 1: Copy the following code block into the ThisAddin, ThisWorkbook, or ThisDocument class.

//  protected override Microsoft.Office.Core.IRibbonExtensibility CreateRibbonExtensibilityObject()
//  {
//      return new Ribbon();
//  }

// 2. Create callback methods in the "Ribbon Callbacks" region of this class to handle user
//    actions, such as clicking a button. Note: if you have exported this Ribbon from the Ribbon designer,
//    move your code from the event handlers to the callback methods and modify the code to work with the
//    Ribbon extensibility (RibbonX) programming model.

// 3. Assign attributes to the control tags in the Ribbon XML file to identify the appropriate callback methods in your code.  

// For more information, see the Ribbon XML documentation in the Visual Studio Tools for Office Help.

using Outlook = Microsoft.Office.Interop.Outlook;

namespace mailegantAddIn
{
    [ComVisible(false)]
    public class AutoClosingMessageBox
    {
        System.Threading.Timer _timeoutTimer;
        string _caption;
        AutoClosingMessageBox(string text, string caption, int timeout)
        {
            _caption = caption;
            _timeoutTimer = new System.Threading.Timer(OnTimerElapsed, null, timeout, System.Threading.Timeout.Infinite);
            System.Windows.Forms.MessageBox.Show(text, caption);
        }
        public static void Show(string text, string caption, int timeout)
        {
            new AutoClosingMessageBox(text, caption, timeout);
        }
        void OnTimerElapsed(object state)
        {
            IntPtr mbWnd = FindWindow(null, _caption);
            if (mbWnd != IntPtr.Zero)
                SendMessage(mbWnd, WM_CLOSE, IntPtr.Zero, IntPtr.Zero);
            _timeoutTimer.Dispose();
        }
        const int WM_CLOSE = 0x0010;
        [System.Runtime.InteropServices.DllImport("user32.dll", SetLastError = true)]
        static extern IntPtr FindWindow(string lpClassName, string lpWindowName);
        [System.Runtime.InteropServices.DllImport("user32.dll", CharSet = System.Runtime.InteropServices.CharSet.Auto)]
        static extern IntPtr SendMessage(IntPtr hWnd, UInt32 Msg, IntPtr wParam, IntPtr lParam);
    }

    [ComVisible(true)]
    public class Ribbon : Office.IRibbonExtensibility
    {
        private Office.IRibbonUI ribbon;

        public Ribbon()
        {
            //System.Windows.Forms.MessageBox.Show(DateTime.Now.ToLongTimeString() + ": Ribbon()", "Log");
        }

        #region IRibbonExtensibility Members

        public string GetCustomUI(string ribbonID)
        {
            //System.Windows.Forms.MessageBox.Show(DateTime.Now.ToLongTimeString() + ": GetCustomUI(" + ribbonID + ")", "Log");

            //this ribbon should be added only in the main/Explorer window
            if (ribbonID == "Microsoft.Outlook.Explorer")
            {
                //detect what Office is starting this addin
                string strMajorVersion = Globals.ThisAddIn.Application.Version.Split(new char[] { '.' })[0];
                Int32 majorVersion = 0;
                int.TryParse(strMajorVersion, out majorVersion);
                bool bOffice2007 = (majorVersion == 12);

                //System.Windows.Forms.MessageBox.Show(DateTime.Now.ToLongTimeString() + ": GetCustomUI() - office version=" + strMajorVersion, "Log");

                //support both Outlook 2007 and 2010
                if (bOffice2007)
                    return GetResourceText("mailegantAddIn.Ribbon2007.xml");
                else
                    return GetResourceText("mailegantAddIn.Ribbon2010.xml");
            }
            return null;    //for all other Outlook windows
        }

        #endregion

        #region Ribbon Callbacks
        //Create callback methods here. For more information about adding callback methods, select the Ribbon XML item in Solution Explorer and then press F1

        public void Ribbon_Load(Office.IRibbonUI ribbonUI)
        {
            //System.Windows.Forms.MessageBox.Show(DateTime.Now.ToLongTimeString() + ": Ribbon_Load()", "Log");
            ////if (null != Globals.ThisAddIn.logFile)
            ////    Globals.ThisAddIn.logFile.WriteLine(DateTime.Now.ToLongTimeString() + ": Ribbon_Load()");

            this.ribbon = ribbonUI;
        }

        public void btnReply_Click(Office.IRibbonControl control)
        {
            //http://msdn.microsoft.com/en-us/library/vstudio/ms268994.aspx
            //http://www.codeproject.com/Articles/21342/Programmatically-forwarding-email-message-with-inl
            //http://www.codeproject.com/Articles/59270/Forwarding-Outlook-emails-to-another-account-in-an
            //http://stackoverflow.com/questions/11223462/how-to-send-a-mail-using-microsoft-office-interop-outlook-mailitem-by-specifying
            //http://social.msdn.microsoft.com/Forums/en-US/vsto/thread/6c063b27-7e8a-4963-ad5f-ce7e5ffb2c64/
            try
            {
                if (Globals.ThisAddIn.Application.ActiveExplorer().Selection.Count > 0)
                {
                    //get selected mail object
                    Object selObject = Globals.ThisAddIn.Application.ActiveExplorer().Selection[1];
                    if (selObject is Outlook.MailItem)
                    {
                        Outlook.MailItem mailItem = (selObject as Outlook.MailItem);
                        if (null != mailItem)
                        {
                            //create a copy of the mail object to be forwarded
                            Outlook.MailItem newItem = ((Outlook._MailItem)mailItem).Forward();   //use casting because there is Forward() ambiguity
                            if (null != newItem)
                            {
                                //
                                //prepare custom email settings
                                //
                                
                                //replace current recipients with our own
                                int nCnt = newItem.Recipients.Count;
                                for (int i = 0; i < nCnt; i++)
                                {
                                    newItem.Recipients.Remove(0);
                                }
                                newItem.Recipients.Add("todo@mailegant.com");

                                //NOTE: no need to rewrite email body to include original "From" information, Forward call does that to us
                                /*
                                if( Outlook.OlBodyFormat.olFormatPlain == newItem.BodyFormat )
                                {
                                    //modify plain text body
                                    string strBody = "----- Original message -----\r\n";
                                    strBody += "From: " + mailItem.SenderEmailAddress + "\r\n";
                                    strBody += "To: " + mailItem.To + "\r\n";
                                    strBody += "CC: " + mailItem.CC + "\r\n";
                                    strBody += "\r\n";
                                    strBody += newItem.Body;

                                    newItem.Body = strBody;
                                }
                                else if (Outlook.OlBodyFormat.olFormatHTML == newItem.BodyFormat)
                                {
                                    //modify HTML body
                                    string strHdr = "----- Original message -----<br>";
                                    strHdr += "From: " + mailItem.SenderEmailAddress + "<br>";
                                    strHdr += "To: " + mailItem.To + "<br>";
                                    strHdr += "CC: " + mailItem.CC + "<br><br>";

                                    string strBody = newItem.HTMLBody;
                                    int nPos = strBody.IndexOf("<body", 0, StringComparison.InvariantCultureIgnoreCase); 
                                    if (nPos >= 0) { 
                                        nPos = strBody.IndexOf(">", nPos);    //end of tag
                                        if (nPos >= 0) {
                                            strBody = strBody.Insert(nPos + 1, strHdr);
                                        }
                                    }

                                    newItem.HTMLBody = strBody;
                                }
                                else if (Outlook.OlBodyFormat.olFormatRichText == newItem.BodyFormat)
                                {
                                    //modify HTML body
                                    string strHdr = "----- Original message -----<br>";
                                    strHdr += "From: " + mailItem.SenderEmailAddress + "<br>";
                                    strHdr += "To: " + mailItem.To + "<br>";
                                    strHdr += "CC: " + mailItem.CC + "<br><br>";

                                    string strBody = newItem.HTMLBody;
                                    int nPos = strBody.IndexOf("<body", 0, StringComparison.InvariantCultureIgnoreCase);
                                    if (nPos >= 0)
                                    {
                                        nPos = strBody.IndexOf(">", nPos);    //end of tag
                                        if (nPos >= 0){
                                            strBody = strBody.Insert(nPos + 1, strHdr);
                                        }
                                    }

                                    newItem.HTMLBody = strBody;
                                }
                                else if (Outlook.OlBodyFormat.olFormatUnspecified == newItem.BodyFormat)
                                {
                                    //modify HTML body
                                    string strHdr = "----- Original message -----<br>";
                                    strHdr += "From: " + mailItem.SenderEmailAddress + "<br>";
                                    strHdr += "To: " + mailItem.To + "<br>";
                                    strHdr += "CC: " + mailItem.CC + "<br><br>";

                                    string strBody = newItem.HTMLBody;
                                    int nPos = strBody.IndexOf("<body", 0, StringComparison.InvariantCultureIgnoreCase);
                                    if (nPos >= 0)
                                    {
                                        nPos = strBody.IndexOf(">", nPos);    //end of tag
                                        if (nPos >= 0){
                                            strBody = strBody.Insert(nPos + 1, strHdr);
                                        }
                                    }

                                    newItem.HTMLBody = strBody;
                                }*/

                                //find matching Outlook account (to be used for "From:" field) based on the settings or current user email
                                bool bSent = false;
                                string strFromEmailSetting = (string)Microsoft.Win32.Registry.GetValue(@"HKEY_CURRENT_USER\SOFTWARE\iNimated\MailegantAddIn\Settings", "FromEmail", null);
                                if (string.IsNullOrEmpty(strFromEmailSetting))
                                {
                                    /*
                                     *  MB on Skype:
                                     *  - When the user clicks on "Reply in mW" for the first time, a dialog appears, where he can select one of his current accounts to be used. This will be remembered, and the dialog will not appear automatically again.
                                        - If the user has only one email account, this will become the mW account without dialog.
                                     */
                                    if (Globals.ThisAddIn.Application.Session.Accounts.Count > 1)
                                    {
                                        //ask user to select
                                        EmailSettingsDialog dlg = new EmailSettingsDialog();
                                        dlg.ShowDialog();

                                        //now retry fetching the settings
                                        strFromEmailSetting = (string)Microsoft.Win32.Registry.GetValue(@"HKEY_CURRENT_USER\SOFTWARE\iNimated\MailegantAddIn\Settings", "FromEmail", null);
                                        if (string.IsNullOrEmpty(strFromEmailSetting))
                                            strFromEmailSetting = Globals.ThisAddIn.Application.ActiveExplorer().Session.CurrentUser.Address;   //fallback to current user email
                                    }
                                    else {
                                        //automatically use email from the only account available
                                        strFromEmailSetting = Globals.ThisAddIn.Application.ActiveExplorer().Session.CurrentUser.Address;   //fallback to current user email
                                    }
                                }

                                Outlook.Accounts accounts = Globals.ThisAddIn.Application.Session.Accounts;
                                foreach (Outlook.Account account in accounts)
                                {
                                    // When the e-mail address matches, send the mail.
                                    if (account.SmtpAddress == strFromEmailSetting)
                                    {
                                        newItem.SendUsingAccount = account;
                                        ((Outlook._MailItem)newItem).Send();    //use casting because there is Send() ambiguity
                                        bSent = true;
                                        break;
                                    }
                                }
                                if (!bSent)
                                {
                                    System.Windows.Forms.MessageBox.Show("Failed to find matching account for sending!", "Error");
                                }
                                else {
                                    //TOFIX mark original item as being replied to
                                    //mailItem.

                                    AutoClosingMessageBox.Show("Email added to the mailegant Writer's ToDo-list!", "Info", 5000);
                                    //System.Windows.Forms.MessageBox.Show("Email added to the mailegant Writer's ToDo-list!", "Info");
                                }

                                newItem = null;
                            }
                            else
                            {
                                System.Windows.Forms.MessageBox.Show("Error forwarding selected mail!", "Error");
                            }

                            mailItem = null;
                        }
                        else {
                            System.Windows.Forms.MessageBox.Show("Error accessing selected mail!", "Error");
                        }
                    }
                    else
                    {
                        System.Windows.Forms.MessageBox.Show("Selected item is not a mail!", "Error");
                    }
                }
                else
                {
                    System.Windows.Forms.MessageBox.Show("No item selected!", "Error");
                }
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message, "Error");
            }
        }

        public void btnNew_Click(Office.IRibbonControl control)
        {
            //fetch mailegant .exe path from registry
            string strInstallPath = (string)Microsoft.Win32.Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\iNimated\Mailegant\Settings", "InstallPath", null);
            if (strInstallPath != null)
            {
                strInstallPath += "\\mailegant.exe";
                System.Diagnostics.Process.Start(strInstallPath);   //start mailegant program
            }
            else {
                System.Windows.Forms.MessageBox.Show("Could not find mailegant installation!", "Error");
            }
        }

        public void btnSettings_Click(Office.IRibbonControl control)
        {
            EmailSettingsDialog dlg = new EmailSettingsDialog();
            dlg.ShowDialog();
        }

        #endregion
     
        #region Helpers

        private static string GetResourceText(string resourceName)
        {
            Assembly asm = Assembly.GetExecutingAssembly();
            string[] resourceNames = asm.GetManifestResourceNames();
            for (int i = 0; i < resourceNames.Length; ++i)
            {
                if (string.Compare(resourceName, resourceNames[i], StringComparison.OrdinalIgnoreCase) == 0)
                {
                    using (StreamReader resourceReader = new StreamReader(asm.GetManifestResourceStream(resourceNames[i])))
                    {
                        if (resourceReader != null)
                        {
                            return resourceReader.ReadToEnd();
                        }
                    }
                }
            }
            return null;
        }

        public System.Drawing.Bitmap GetImage(Microsoft.Office.Core.IRibbonControl control)
        {
            switch (control.Id)
            {
                case "btnNew":
                    return new System.Drawing.Bitmap(Properties.Resources.mw_Icon_48);
                case "btnReply":
                    return new System.Drawing.Bitmap(Properties.Resources.mw_Reply_48);
            }
            return null;
        }

        #endregion
    }
}
