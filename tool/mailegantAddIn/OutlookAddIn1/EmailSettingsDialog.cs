﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace mailegantAddIn
{
    public partial class EmailSettingsDialog : Form
    {
        public class EmailEntry
        {
            public string Address { get; set; }
        }

        List<EmailEntry> _lstEmails = new List<EmailEntry>();

        public EmailSettingsDialog()
        {
            InitializeComponent();

            //list all emails
            Outlook.Accounts accounts = Globals.ThisAddIn.Application.Session.Accounts;
            foreach (Outlook.Account account in accounts){
                _lstEmails.Add(new EmailEntry() { Address = account.SmtpAddress });
            }
            this.cboEmails.DataSource = _lstEmails;
            this.cboEmails.DisplayMember = "Address";
            this.cboEmails.ValueMember = "Address";

            //restore email selection setting
            string strEmail = (string)Microsoft.Win32.Registry.GetValue(@"HKEY_CURRENT_USER\SOFTWARE\iNimated\MailegantAddIn\Settings", "FromEmail", null);
            if (strEmail != null) {
                int nCnt = _lstEmails.Count();
                for (int i = 0; i < nCnt; i++) {
                    if (_lstEmails[i].Address == strEmail) {
                        this.cboEmails.SelectedItem = _lstEmails[i];
                        break;
                    }
                }
            }

            // make combo readonly
            this.cboEmails.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            //store email selection setting
            string strSelectedEmail = ((EmailEntry)this.cboEmails.SelectedItem).Address;
            //System.Windows.Forms.MessageBox.Show(strSelectedEmail, "Info");
            Microsoft.Win32.Registry.SetValue(@"HKEY_CURRENT_USER\SOFTWARE\iNimated\MailegantAddIn\Settings", "FromEmail", strSelectedEmail);

            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
