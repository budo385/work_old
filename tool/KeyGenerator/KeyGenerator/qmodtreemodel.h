/****************************************************************************
**
** Copyright (C) 2005-2006 Trolltech AS. All rights reserved.
**
** This file is part of the example classes of the Qt Toolkit.
**
** Licensees holding a valid Qt License Agreement may use this file in
** accordance with the rights, responsibilities and obligations
** contained therein.  Please consult your licensing agreement or
** contact sales@trolltech.com if any conditions of this licensing
** agreement are not clear to you.
**
** Further information about Qt licensing is available at:
** http://www.trolltech.com/products/qt/licensing.html or by
** contacting info@trolltech.com.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#ifndef QModTreeModel_H
#define QModTreeModel_H

#include <QAbstractItemModel>
#include <QModelIndex>
#include <QVariant>
#include "bus_core/bus_core/fpdf.h"

class TreeItem;

class QModTreeModel : public QAbstractItemModel
{
    Q_OBJECT

public:
    QModTreeModel(QObject *parent = 0);
    ~QModTreeModel();

    QVariant data(const QModelIndex &index, int role) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const;
    QModelIndex index(int row, int column,
                      const QModelIndex &parent = QModelIndex()) const;
    QModelIndex parent(const QModelIndex &index) const;
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;

	bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex());
	bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex());
	bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole );
	Qt::DropActions supportedDropActions() const;

	//my APIs
	void Clear();
	void IndexToList(QModelIndex index, QList<int> &lstPos) const;
	QModelIndex ListToIndex(QList<int> lstPos);
	bool AddNewModule(QString strName, bool bVisible = true, int nStatus = ITEM_STATUS_ON);
	bool AddNewLeaf(TreeItem *parent, QString strName, bool bVisible = true, int nStatus = ITEM_STATUS_ON, bool bRedColor = false);
	void ShowRow(int nRow, bool bShow);
	void ShowHidden(bool bShow = true);
	void RebuildGuiBranch(QModelIndex parent);
	QModelIndex GetRoot();
	void RedrawBranch(QModelIndex index);

protected:
    TreeItem *rootItem;

public:
    FPDF *m_pData;
	int m_nCurModSibling;
};

#endif	// QModTreeModel_H
