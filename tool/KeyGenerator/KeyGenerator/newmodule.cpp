#include "newmodule.h"
#include <QMessageBox>

newmodule::newmodule(QWidget *parent)
    : QDialog(parent)
{
	m_nSelectedModule = -1;
	ui.setupUi(this);
}

newmodule::~newmodule()
{
}

void newmodule::FillData(FPDF *pData)
{
	Q_ASSERT(NULL != pData);

	//fill combo box with the list of all available modules
	ui.comboModules->clear();

	int nSibling = 0;
	int nNode = pData->m_lstModSets.FindChild(-1, nSibling);
	while(nNode >= 0)
	{
		QString strTitle = pData->m_lstModSets[nNode].strName;
		strTitle += " (";
		strTitle += pData->m_lstModSets[nNode].strCode;
		strTitle += ")";

		ui.comboModules->addItem(strTitle);

		nSibling ++;
		nNode = pData->m_lstModSets.FindChild(-1, nSibling);
	}

}

void newmodule::on_pushOK_clicked()
{
	m_nSelectedModule = ui.comboModules->currentIndex();
	if(m_nSelectedModule < 0)
	{
		QMessageBox::information(this, "FpAdmin", "Please select a module from drop down list.");
		return;
	}

	accept();
}

void newmodule::on_pushCancel_clicked()
{
	reject();
}

