#ifndef NEWMODULE_H
#define NEWMODULE_H

#include <QDialog>
#include <QString>
#include "ui_newmoduledlg.h"
#include "bus_core/bus_core/fpdf.h"

class newmodule : public QDialog
{
    Q_OBJECT

public:
    newmodule(QWidget *parent = 0);
    ~newmodule();

	void FillData(FPDF *pData);

public:
	int m_nSelectedModule;
	
private:
    Ui::newmoduleClass ui;

private slots:
	void on_pushOK_clicked();
	void on_pushCancel_clicked();
};

#endif // NEWMODULE_H
