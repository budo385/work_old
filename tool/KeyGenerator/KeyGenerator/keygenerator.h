#ifndef KEYGENERATOR_H
#define KEYGENERATOR_H

#include <QMainWindow>
#include "ui_keygenerator.h"
#include "qmodtreemodel.h"
#include "bus_core/bus_core/licenseinfo.h"

class KeyGenerator : public QMainWindow
{
    Q_OBJECT

public:
    KeyGenerator(QWidget *parent = 0, Qt::WindowFlags flags = 0);
    ~KeyGenerator();

protected:
	void ShowFPDFData();
	void BuildModelFFSTree();
	void ClearModuleInfo();
	void EnableModuleInfo(bool bEnable = true);
	void EnableModuleList(bool bEnable = true);
	void EnableLicenseHeader(bool bEnable = true);
	bool LoadFpdfFile(QString strFile = QString());
	void LoadLicenseKey(QString strFile);
	void FixKeyfileNewFPs();

protected:
	QModTreeModel	*m_pModModel;
	LicenseInfo	m_lic;
	QString m_strKeyFile;

private:
    Ui::KeyGeneratorClass ui;

private slots:
	void on_checkMod_UseLicenseExpDate_stateChanged(int);
	void on_lineMod_MLIID_textChanged(const QString &);
	void on_lineMod_NumLicenses_textChanged(const QString &);
	void on_treeModules_currentItemChanged(QTreeWidgetItem*,QTreeWidgetItem*);
	void OnLicenseOpen();
	void OnLicenseSave();
	void OnLicenseSaveAs();
	void OnFPDFOpen();
	void on_lineFP_Value_textChanged(const QString &);
	void on_pushDeleteModuleLicense_clicked();
	void on_pushNewModuleLicense_clicked();
	void OnModFpsSelectionChange(const QModelIndex &to, const QModelIndex &from);
	void OnDumpLicense();
};

#endif // KEYGENERATOR_H
