TEMPLATE	= app
LANGUAGE	= C++
CONFIG		+= qt warn_on release
HEADERS		= keygenerator.h \
		  newmodule.h \
		  qmodtreemodel.h \
		  treeitem.h
SOURCES		= keygenerator.cpp \
		  main.cpp \
		  newmodule.cpp \
		  qmodtreemodel.cpp \
		  treeitem.cpp
INTERFACES	= keygenerator.ui \
		  newmoduledlg.ui
TARGET		= KeyGenerator
INCLUDEPATH	+= ../../../lib
