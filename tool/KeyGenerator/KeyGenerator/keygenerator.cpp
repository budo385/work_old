#include "keygenerator.h"
#include <QListWidgetItem>
#include <QHeaderView>
#include <QFileDialog>
#include <QMessageBox>
#include "bus_core/bus_core/fpsets.h"
#include "newmodule.h"
#include <qDebug>

extern QString g_strCmdLineDocument;

KeyGenerator::KeyGenerator(QWidget *parent, Qt::WindowFlags flags)
    : QMainWindow(parent, flags)
{
	ui.setupUi(this);

	//set build info in the 
	QString strMsg;
	strMsg.sprintf(tr("KeyGenerator (Build: %s %s)").toLatin1().constData(), __DATE__, __TIME__);
	setWindowTitle(strMsg);

	//insert columns
	ui.treeModules->setColumnCount(3);
	ui.treeModules->model()->setHeaderData(0, Qt::Horizontal, QObject::tr("Module"));
	ui.treeModules->model()->setHeaderData(1, Qt::Horizontal, QObject::tr("Users"));
	ui.treeModules->model()->setHeaderData(2, Qt::Horizontal, QObject::tr("MLIID"));
	
	//set column sizes
	QHeaderView *pHdr = ui.treeModules->header();
	pHdr->resizeSection(0, 40);
	pHdr->resizeSection(1, 40);
	pHdr->resizeSection(2, 40);

	//initialize tree widget
	m_pModModel = new QModTreeModel;
	m_pModModel->m_pData = &(m_lic.m_fpdf);
	ui.treeFPS->setModel(m_pModModel);
	ui.treeFPS->header()->hide();

	//build a default FPS list
	//InitAvailableSets();

	//by default select "Is company"
	ui.checkHdr_IsCompany->setCheckState(Qt::Checked);

	QObject::connect(ui.actionOpen_License, SIGNAL(triggered()), this, SLOT(OnLicenseOpen()));
    QObject::connect(ui.actionSave_License, SIGNAL(triggered()), this, SLOT(OnLicenseSave()));
	QObject::connect(ui.actionSave_As_License, SIGNAL(triggered()), this, SLOT(OnLicenseSaveAs()));
	QObject::connect(ui.actionLoadFPDF, SIGNAL(triggered()), this, SLOT(OnFPDFOpen()));
	QObject::connect(ui.actionQuit, SIGNAL(triggered()), this, SLOT(close()));
	QObject::connect(ui.actionDump_to_File, SIGNAL(triggered()), this, SLOT(OnDumpLicense()));
	QObject::connect(ui.treeFPS->selectionModel(), SIGNAL(currentChanged(const QModelIndex&, const QModelIndex&)), this, SLOT(OnModFpsSelectionChange(const QModelIndex&, const QModelIndex&)));
	
	ui.actionOpen_License->setShortcut(QKeySequence(tr("Ctrl+O")));
	ui.actionSave_License->setShortcut(QKeySequence(tr("Ctrl+S")));
	ui.actionQuit->setShortcut(QKeySequence(tr("Ctrl+Q")));
	ui.actionLoadFPDF->setShortcut(QKeySequence(tr("Ctrl+T")));

	EnableLicenseHeader(false);
	EnableModuleList(false);
	EnableModuleInfo(false);
	ui.lineFP_Value->setEnabled(false);

	//automatically search and load first .fpdf file found in "data" subdirectory (if found)
	QString strAppDir = QCoreApplication::applicationDirPath();
	QDir dir(strAppDir, QString("*.fpdf"));
	if (dir.cd("data"))
	{
		//get first .fpdf file in a directory
		QFileInfoList list = dir.entryInfoList();
        if(list.size() > 0) {
			if(LoadFpdfFile(list.at(0).absoluteFilePath()))
			{
				//load .key file given through the commandl ine
				if(!g_strCmdLineDocument.isEmpty())
					LoadLicenseKey(g_strCmdLineDocument);
			}
        }
	}
}

KeyGenerator::~KeyGenerator()
{
}

void KeyGenerator::on_pushNewModuleLicense_clicked()
{
	if(m_lic.m_fpdf.IsEmpty()){
		QMessageBox::information(this, "Warning", "Please Load FPDF file first!");
		return;
	}

	newmodule dlg;
	dlg.FillData(&m_lic.m_fpdf);
	if(dlg.exec())
	{
		int nNode = m_lic.m_fpdf.m_lstModSets.FindChild(-1, dlg.m_nSelectedModule);
		Q_ASSERT(nNode >= 0);

		//add new module into the license
		LicenseModInfo info;
		info.m_strCode			= m_lic.m_fpdf.m_lstModSets[nNode].strCode;
		info.m_strModuleName	= m_lic.m_fpdf.m_lstModSets[nNode].strName;
		info.m_dateCreated		= QDateTime::currentDateTime ();
		info.m_dateModified		= info.m_dateCreated;
		info.m_treeAR			= m_lic.m_fpdf;					//copy the default tree
		info.m_nModTreeSibling	= dlg.m_nSelectedModule;
		info.m_nLicensingType   = 0; // concurrent users
		info.m_bUseExpireDate	= false;
		
		//issue #1955: initialize default values - copy from main header into the module
		info.m_strReportLine1 = ui.lineHdr_Text1->text();
		info.m_strReportLine2 = ui.lineHdr_Text2->text();
		info.m_nCustomSolutionID = ui.lineHdr_CustomSolutionID->text().toInt();

		m_lic.m_lstModInfo.append(info);

		QTreeWidgetItem *pItem = new QTreeWidgetItem(ui.treeModules);
		pItem->setText(0, info.m_strCode);
		pItem->setText(1, "0");	// 0 licenses by default
		pItem->setText(2, "");	// empty MLIID

		//refresh module count indicator
		ui.lineHdr_NumModules->setText(QString().sprintf("%d",m_lic.m_lstModInfo.size()));

		EnableModuleInfo(true);

		//switch selection to the new item
		ui.treeModules->setCurrentItem(pItem, 0);
	}
}

void KeyGenerator::on_pushDeleteModuleLicense_clicked()
{
	QTreeWidgetItem *pItem = ui.treeModules->currentItem();
	int nIdx = ui.treeModules->indexOfTopLevelItem (pItem);
	ui.treeModules->takeTopLevelItem(nIdx);

	//remove module data
	m_lic.m_lstModInfo.removeAt(nIdx);

	//clear module display
	ClearModuleInfo();
	EnableModuleInfo(false);
	
	//refresh module count indicator
	int nSize = m_lic.m_lstModInfo.size();
	ui.lineHdr_NumModules->setText(QString().sprintf("%d", nSize));
}

void KeyGenerator::OnLicenseOpen()
{
	if(m_lic.m_fpdf.IsEmpty()){
		QMessageBox::information(this, "Warning", "Please load FPDF file first!");
		return;
	}

	static QString strDir;

	//calculate default directory for the file open dialog
	QString strStartDir = QCoreApplication::applicationDirPath();
	if(strDir.isEmpty()){
		QDir dir(strStartDir);
		if (dir.cd("data"))
			strStartDir = dir.absolutePath();
	}
	else
		strStartDir = strDir;

	QString str = QFileDialog::getOpenFileName(
                    NULL,
                    "Choose a file",
                    strStartDir,
                    "License Key (*.key)");
	if(!str.isEmpty())
	{
		LoadLicenseKey(str);

		//refresh last used directory
		QDir dir(str);
		dir.cdUp();
		strDir = dir.path();
	}
}

void KeyGenerator::OnLicenseSave()
{
	if(m_lic.m_fpdf.IsEmpty()){
		QMessageBox::information(this, "Warning", "Please load FPDF file first!");
		return;
	}

	if(m_strKeyFile.isEmpty()){
		OnLicenseSaveAs();
		return;
	}

	//update license data from GUI
	m_lic.m_strCustomerID   = ui.lineHdr_ClientID->text();
	m_lic.m_nLicenseVersion = ui.comboHdr_LicenseVersion->currentText().toInt();
	m_lic.m_nCustomSolutionID = ui.lineHdr_CustomSolutionID->text().toInt();
	m_lic.m_nLicenseID      = ui.lineHdr_LicenseID->text().toInt();
	m_lic.m_strClientName = ui.lineHdr_ClientName->text();
	m_lic.m_bIsCompany    = (ui.checkHdr_IsCompany->checkState() == Qt::Checked)? true : false; 
	m_lic.m_dateCreated	  = QDateTime::fromString(ui.lineHdr_CreatedDate->text(), Qt::ISODate);
	m_lic.m_dateModified  = QDateTime::fromString(ui.lineHdr_ModifiedDate->text(), Qt::ISODate);
	m_lic.m_strReportLine1 = ui.lineHdr_Text1->text();
	m_lic.m_strReportLine2 = ui.lineHdr_Text2->text();

	//update module data from GUI
	on_treeModules_currentItemChanged(NULL, ui.treeModules->currentItem());
	FixKeyfileNewFPs();

	if(!m_lic.Save(m_strKeyFile))
		QMessageBox::information(this, "Warning", "Failed to save License Key file!");
}

void KeyGenerator::OnLicenseSaveAs()
{
	if(m_lic.m_fpdf.IsEmpty()){
		QMessageBox::information(this, "Warning", "Please load FPDF file first!");
		return;
	}

	//calculate default directory for the file open dialog
	QString strStartDir = QCoreApplication::applicationDirPath();
	QDir dir(strStartDir);
	if (dir.cd("data"))
		strStartDir = dir.absolutePath();

	QString str = QFileDialog::getSaveFileName(
                    NULL,
                    "Choose a file",
                    strStartDir,
                    "License Key (*.key)");
	if(!str.isEmpty())
	{
		//ensure ".key" extension
		if(!str.endsWith(".key", Qt::CaseInsensitive))
			str.append(".key");

		//update license data from GUI
		m_lic.m_strCustomerID   = ui.lineHdr_ClientID->text();
		m_lic.m_nLicenseVersion = ui.comboHdr_LicenseVersion->currentText().toInt();
		m_lic.m_nCustomSolutionID = ui.lineHdr_CustomSolutionID->text().toInt();
		m_lic.m_nLicenseID      = ui.lineHdr_LicenseID->text().toInt();
		m_lic.m_strClientName = ui.lineHdr_ClientName->text();
		m_lic.m_bIsCompany    = (ui.checkHdr_IsCompany->checkState() == Qt::Checked)? true : false; 
		m_lic.m_dateCreated	  = QDateTime::fromString(ui.lineHdr_CreatedDate->text(), Qt::ISODate);
		m_lic.m_dateModified  = QDateTime::fromString(ui.lineHdr_ModifiedDate->text(), Qt::ISODate);
		m_lic.m_strReportLine1 = ui.lineHdr_Text1->text();
		m_lic.m_strReportLine2 = ui.lineHdr_Text2->text();

		//update module data from GUI
		on_treeModules_currentItemChanged(NULL, ui.treeModules->currentItem());
		FixKeyfileNewFPs();

		if(!m_lic.Save(str))
			QMessageBox::information(this, "Warning", "Failed to save License Key file!");
		else{
			m_strKeyFile = str;

			QString strMsgStatus = "Saved keyfile: " + m_strKeyFile;
			ui.statusBar->showMessage(strMsgStatus);	//show file name in the status bar

			QString strMsg;
			strMsg.sprintf(tr("KeyGenerator (Build: %s %s) %s").toLatin1().constData(), __DATE__, __TIME__, strMsgStatus.toLatin1().constData());
			setWindowTitle(strMsg);
		}
	}
}

void KeyGenerator::OnFPDFOpen()
{
	LoadFpdfFile();
}

void KeyGenerator::BuildModelFFSTree()
{
	//rebuild module tree for currently selected module
	m_pModModel->Clear();

	//get currently selected module
	QTreeWidgetItem *pItem = ui.treeModules->currentItem();
	int nCurSection = ui.treeModules->indexOfTopLevelItem (pItem);
	if(nCurSection < 0)
		return;
	TreeList &lstData(m_lic.m_lstModInfo[nCurSection].m_treeAR.m_lstModSets);

	//find what is the current tree type for this section
	if(m_lic.m_lstModInfo[nCurSection].m_nModTreeSibling < 0)
	{
		//calc sibling from node index
		int nRootNode = m_lic.m_lstModInfo[nCurSection].FindRootNode();
		m_lic.m_lstModInfo[nCurSection].m_nModTreeSibling = 0;
		for(int i=0; i<nRootNode; i++)
			if(lstData[i].nLevel == 0)
				m_lic.m_lstModInfo[nCurSection].m_nModTreeSibling ++;
	}
	int nModSibling = m_lic.m_lstModInfo[nCurSection].m_nModTreeSibling;

	//store sibling (needed for tree-data mapping)
	m_pModModel->m_nCurModSibling = nModSibling;
	m_pModModel->m_pData = &(m_lic.m_lstModInfo[nCurSection].m_treeAR);

	int nModIdx = lstData.FindChild(-1, nModSibling);
	if(nModIdx < 0)
		return;	// module not found

	//build a FPS tree for a single module
	int nIdx = nModIdx + 1;
	int nCount = lstData.size();
	while(nIdx < nCount)
	{
		if(lstData[nIdx].nLevel < 1)
			break;	//end of branch

		//calculate node title
		QString strTitle = lstData[nIdx].strName;
		if(lstData[nIdx].nValue != 0){
			QString strTmp;
			strTmp.sprintf(" (%d)",	lstData[nIdx].nValue);
			strTitle += strTmp;
		}

		//update model (GUI)
		int nParent = lstData.FindParent(nIdx);
		if(nParent < 0)
			m_pModModel->AddNewModule(strTitle, lstData[nIdx].bActive, lstData[nIdx].nStatus);
		else{
			QList<int> lstPos;
			lstData.GetNodePos(nParent, lstPos);
			lstPos.pop_front();	// mapping from a tree of all modules to a single module child trees
			QModelIndex index = m_pModModel->ListToIndex(lstPos);
			TreeItem *parent = static_cast<TreeItem*>(index.internalPointer());
			Q_ASSERT(parent != NULL);

			//module FPs that could not be found in the keyfile (newer than the keyfile)
			//are displayed in the red color
			//IGNORE non-leaf items from this (groups)
			bool bRedColor = true;
			if(ITEM_TYPE_FP == lstData[nIdx].nType)
			{
				int nMax = m_lic.m_lstModInfo[nCurSection].m_lstAR.count();
				if(nMax > 0){
					for(int i=0; i<nMax; i++){
						if(m_lic.m_lstModInfo[nCurSection].m_lstAR[i].nCode == lstData[nIdx].nCode){
							bRedColor = false;	//found the AR existign in the keyfile (FPDF file is not newer then the keyfile, just looking at this single access right)
							break;
						}
					}

					if(bRedColor){
						//add missing AR into the keyfile
						FP_ITEM1 item;
						item.nCode	= lstData[nIdx].nCode;
						item.bCanUse= (lstData[nIdx].nStatus == ITEM_STATUS_ON || lstData[nIdx].nStatus == ITEM_STATUS_INHERITED); //TOFIX support for inherited status too
						item.nValue	= lstData[nIdx].nValue;
						m_lic.m_lstModInfo[nCurSection].m_lstAR.push_back(item);
					}
				}
				else
					bRedColor = false;	//new keyfile
			}
			else
				bRedColor = false;	// sets are not painted red

			m_pModModel->AddNewLeaf(parent, strTitle, lstData[nIdx].bActive, lstData[nIdx].nStatus, bRedColor);
		}

		nIdx ++;	//next node
	}
}

void KeyGenerator::on_lineFP_Value_textChanged(const QString &data)
{
	QModelIndex index = ui.treeFPS->selectionModel()->currentIndex();
	if(index.isValid())
	{
		//get currently selected module
		QTreeWidgetItem *pItem = ui.treeModules->currentItem();
		int nCurSection = ui.treeModules->indexOfTopLevelItem (pItem);
		if(nCurSection < 0)
			return;
		TreeList &lstData(m_lic.m_lstModInfo[nCurSection].m_treeAR.m_lstModSets);

		//convert to node index
		QList<int> lstPos;
		m_pModModel->IndexToList(index, lstPos);
		lstPos.push_front(m_pModModel->m_nCurModSibling);	//map from tree pos to data position
		int nNode = lstData.FindNode(lstPos);
		Q_ASSERT(nNode >= 0);

		//prepare title
		QString strTitle = lstData[nNode].strName;
		if(data.toInt() != 0){
			QString strTmp;
			strTmp.sprintf(" (%s)",	data.toLatin1().constBegin());
			strTitle += strTmp;
		}
		m_pModModel->setData(index, strTitle);

		//store changed value
		lstData[nNode].nValue = data.toInt();
	}
}

void KeyGenerator::OnModFpsSelectionChange(const QModelIndex &to, const QModelIndex &from)
{
	//get currently selected module
	QTreeWidgetItem *pItem = ui.treeModules->currentItem();
	int nCurSection = ui.treeModules->indexOfTopLevelItem (pItem);
	if(nCurSection < 0)
		return;
	TreeList &lstData(m_lic.m_lstModInfo[nCurSection].m_treeAR.m_lstModSets);

	//store current value into the old current item
	int nValue = ui.lineFP_Value->text().toInt();
	if(from.isValid())
	{
		//convert to node index
		QList<int> lstPos;
		m_pModModel->IndexToList(from, lstPos);
		lstPos.push_front(m_pModModel->m_nCurModSibling);	//map from tree pos to data position
		int nNode = lstData.FindNode(lstPos);
		Q_ASSERT(nNode >= 0);

		lstData[nNode].nValue = nValue;
	}

	//set value and check state for the new current index
	nValue  = 0;
	if(to.isValid())
	{
		//convert to node index
		QList<int> lstPos;
		m_pModModel->IndexToList(to, lstPos);
		lstPos.push_front(m_pModModel->m_nCurModSibling);	//map from tree pos to data position
		int nNode = lstData.FindNode(lstPos);
		Q_ASSERT(nNode >= 0);
	
		//enable value box only for FP nodes
		if(lstData[nNode].nType == ITEM_TYPE_FP) {
			nValue = lstData[nNode].nValue;
			ui.lineFP_Value->setEnabled(true);
			ui.lineFP_Value->setText(QString().sprintf("%d", nValue));
		}
		else {
			ui.lineFP_Value->setEnabled(false);
			ui.lineFP_Value->setText("");
		}
	}
	else {
		ui.lineFP_Value->setEnabled(false);
		ui.lineFP_Value->setText("");
	}

	qDebug("Module tree: Current item changed");
}

void KeyGenerator::on_treeModules_currentItemChanged(QTreeWidgetItem*to,QTreeWidgetItem*from)
{
	if(NULL != from)
	{
		//store all values from GUI to previous module selection
		int nIdx = ui.treeModules->indexOfTopLevelItem(from);
		Q_ASSERT(nIdx >= 0);

		m_lic.m_lstModInfo[nIdx].m_strModuleName = ui.editMod_ModuleCode->text();
		m_lic.m_lstModInfo[nIdx].m_strSubCode    = ui.editMod_ModuleSubtype->text();
		m_lic.m_lstModInfo[nIdx].m_strName       = ui.lineMod_ClientName->text();
		m_lic.m_lstModInfo[nIdx].m_strMLIID		 = ui.lineMod_MLIID->text();
		m_lic.m_lstModInfo[nIdx].m_strSerialNumber = ui.lineMod_SerialNumber->text();
		m_lic.m_lstModInfo[nIdx].m_nLicensePlatforms = ui.comboMod_Platform->currentIndex();
		m_lic.m_lstModInfo[nIdx].m_nLicensePlatformsSubcode = ui.lineMod_PlatformSubcode->text().toInt();
		m_lic.m_lstModInfo[nIdx].m_nCustomSolutionID = ui.lineMod_CustomSolID->text().toInt();
		m_lic.m_lstModInfo[nIdx].m_strReportLine1 = ui.lineMod_Text1->text();
		m_lic.m_lstModInfo[nIdx].m_strReportLine2 = ui.lineMod_Text2->text();
		m_lic.m_lstModInfo[nIdx].m_bUseExpireDate = (ui.checkMod_UseLicenseExpDate->checkState() == Qt::Checked)? true : false; 
		m_lic.m_lstModInfo[nIdx].m_dateExpiration = QDateTime::fromString(ui.lineMod_ExpireDate->text(), Qt::ISODate);
		m_lic.m_lstModInfo[nIdx].m_dateWarning	  = QDateTime::fromString(ui.lineMod_WarningDate->text(), Qt::ISODate);
		m_lic.m_lstModInfo[nIdx].m_dateCreated	  = QDateTime::fromString(ui.lineMod_CreatedDate->text(), Qt::ISODate);
		m_lic.m_lstModInfo[nIdx].m_dateModified	  = QDateTime::fromString(ui.lineMod_ModifiedDate->text(), Qt::ISODate);
		m_lic.m_lstModInfo[nIdx].m_nLicensingType = ui.comboMod_LicensingType->currentIndex();
		m_lic.m_lstModInfo[nIdx].m_nNumLicenses = ui.lineMod_NumLicenses->text().toInt();
		m_lic.m_lstModInfo[nIdx].m_strModifiedBy = ui.lineMod_ModifiedBy->text();
	}

	if(NULL != to && from != to)
	{
		//initialize all values from new module data to the GUI
		int nIdx = ui.treeModules->indexOfTopLevelItem(to);
		Q_ASSERT(nIdx >= 0);

		ui.editMod_ModuleCode->setText(m_lic.m_lstModInfo[nIdx].m_strModuleName);
		ui.editMod_ModuleSubtype->setText(m_lic.m_lstModInfo[nIdx].m_strSubCode);
		ui.lineMod_ClientName->setText(m_lic.m_lstModInfo[nIdx].m_strName);
		ui.lineMod_MLIID->setText(m_lic.m_lstModInfo[nIdx].m_strMLIID);
		ui.lineMod_SerialNumber->setText(m_lic.m_lstModInfo[nIdx].m_strSerialNumber);
		ui.comboMod_Platform->setCurrentIndex(m_lic.m_lstModInfo[nIdx].m_nLicensePlatforms);
		ui.lineMod_PlatformSubcode->setText(QString().sprintf("%d",m_lic.m_lstModInfo[nIdx].m_nLicensePlatformsSubcode));
		ui.lineMod_CustomSolID->setText(QString().sprintf("%d",m_lic.m_lstModInfo[nIdx].m_nCustomSolutionID));
		ui.lineMod_Text1->setText(m_lic.m_lstModInfo[nIdx].m_strReportLine1);
		ui.lineMod_Text2->setText(m_lic.m_lstModInfo[nIdx].m_strReportLine2);
		ui.checkMod_UseLicenseExpDate->setCheckState((m_lic.m_lstModInfo[nIdx].m_bUseExpireDate)? Qt::Checked : Qt::Unchecked ); 
		ui.lineMod_ExpireDate->setText(m_lic.m_lstModInfo[nIdx].m_dateExpiration.toString(Qt::ISODate));
		ui.lineMod_WarningDate->setText(m_lic.m_lstModInfo[nIdx].m_dateWarning.toString(Qt::ISODate));
		ui.lineMod_CreatedDate->setText(m_lic.m_lstModInfo[nIdx].m_dateCreated.toString(Qt::ISODate));
		ui.lineMod_ModifiedDate->setText(m_lic.m_lstModInfo[nIdx].m_dateModified.toString(Qt::ISODate));
		ui.comboMod_LicensingType->setCurrentIndex(m_lic.m_lstModInfo[nIdx].m_nLicensingType);
		ui.lineMod_NumLicenses->setText(QString().sprintf("%d",m_lic.m_lstModInfo[nIdx].m_nNumLicenses));
		ui.lineMod_ModifiedBy->setText(m_lic.m_lstModInfo[nIdx].m_strModifiedBy);

		//TOFIX refresh tree 
		//TOFIX map from module entry index to the module type index!!!!
		//changing combo will rebuild function point sets tree for given module selection
		//TOFIX ui.comboMod_ModuleCode->setCurrentIndex(nModIdx);
		BuildModelFFSTree();
		EnableModuleInfo(true);
	}
	else{
		//ClearModuleInfo();
	}

	//reset tree value box
	ui.lineFP_Value->setText("");
}

void KeyGenerator::on_lineMod_NumLicenses_textChanged(const QString &data)
{
	//refresh module list GUI current item when edit box changes
	QTreeWidgetItem *pItem = ui.treeModules->currentItem();
	if(pItem)
		pItem->setText(1, data);
}

void KeyGenerator::on_lineMod_MLIID_textChanged(const QString &data)
{
	//refresh module list GUI current item when edit box changes
	QTreeWidgetItem *pItem = ui.treeModules->currentItem();
	if(pItem)
		pItem->setText(2, data);
}

void KeyGenerator::ClearModuleInfo()
{
	ui.treeFPS->blockSignals(true);
	m_pModModel->Clear();
	ui.treeFPS->blockSignals(false);

	ui.editMod_ModuleCode->setText("");
	ui.editMod_ModuleSubtype->setText("");
	ui.lineMod_ClientName->setText("");
	ui.lineMod_MLIID->setText("");
	ui.lineMod_SerialNumber->setText("");
	ui.comboMod_Platform->setCurrentIndex(-1);
	ui.lineMod_PlatformSubcode->setText("0");
	ui.lineMod_CustomSolID->setText("0");
	ui.lineMod_Text1->setText("");
	ui.lineMod_Text2->setText("");
	ui.checkMod_UseLicenseExpDate->setCheckState(Qt::Unchecked); 
	ui.lineMod_ExpireDate->setText("");
	ui.lineMod_WarningDate->setText("");
	ui.lineMod_CreatedDate->setText("");
	ui.lineMod_ModifiedDate->setText("");
	ui.comboMod_LicensingType->setCurrentIndex(0);
	ui.lineMod_NumLicenses->setText("");
	ui.lineMod_ModifiedBy->setText("");
}

void KeyGenerator::EnableModuleInfo(bool bEnable)
{
	ui.editMod_ModuleSubtype->setEnabled(bEnable);
	ui.lineMod_ClientName->setEnabled(bEnable);
	ui.lineMod_MLIID->setEnabled(bEnable);
	ui.lineMod_SerialNumber->setEnabled(bEnable);
	ui.comboMod_Platform->setEnabled(bEnable);
	ui.lineMod_PlatformSubcode->setEnabled(bEnable);
	ui.lineMod_CustomSolID->setEnabled(bEnable);
	ui.lineMod_Text1->setEnabled(bEnable);
	ui.lineMod_Text2->setEnabled(bEnable);
	ui.checkMod_UseLicenseExpDate->setEnabled(bEnable);
	ui.comboMod_LicensingType->setEnabled(bEnable);
	ui.lineMod_NumLicenses->setEnabled(bEnable);
	ui.lineMod_ModifiedBy->setEnabled(bEnable);
	
	ui.lineFP_Value->setEnabled(bEnable);
	ui.treeFPS->setEnabled(bEnable);

	//these two fields are sometimes disabled when everything is enabled
	if(bEnable)
	{
		int nState = ui.checkMod_UseLicenseExpDate->checkState();
		ui.lineMod_ExpireDate->setEnabled(nState > 0);
		ui.lineMod_WarningDate->setEnabled(nState > 0);
	}
	else
	{
		ui.lineMod_ExpireDate->setEnabled(false);
		ui.lineMod_WarningDate->setEnabled(false);
	}
}

void KeyGenerator::EnableModuleList(bool bEnable)
{
	ui.treeModules->setEnabled(bEnable);
	ui.pushNewModuleLicense->setEnabled(bEnable);
	ui.pushDeleteModuleLicense->setEnabled(bEnable);
}

void KeyGenerator::EnableLicenseHeader(bool bEnable)
{
	ui.lineHdr_ClientID->setEnabled(bEnable);
	ui.comboHdr_LicenseVersion->setEnabled(bEnable);
	ui.lineHdr_CustomSolutionID->setEnabled(bEnable);
	ui.lineHdr_LicenseID->setEnabled(bEnable);
	ui.lineHdr_ClientName->setEnabled(bEnable);
	ui.lineHdr_Text1->setEnabled(bEnable);
	ui.lineHdr_Text2->setEnabled(bEnable);
	ui.checkHdr_IsCompany->setEnabled(bEnable);
}

void KeyGenerator::on_checkMod_UseLicenseExpDate_stateChanged(int nState)
{
	ui.lineMod_ExpireDate->setEnabled(nState > 0);
	ui.lineMod_WarningDate->setEnabled(nState > 0);
}

bool KeyGenerator::LoadFpdfFile(QString strFile)
{
	//FPFD can be loaded only once in app lifetime (at the start of work)
	static bool bFpdfLoaded = false;
	if(bFpdfLoaded){
		QMessageBox::information(this, "Warning", "You can load FPDF only once while application is running!\nThis prevents data loss problems when merging FP list to another FP tree.");
		return false;
	}

	if(strFile.isEmpty())
	{
		//calculate default directory for the file open dialog
		QString strStartDir = QCoreApplication::applicationDirPath();
		QDir dir(strStartDir);
		if (dir.cd("data"))
			strStartDir = dir.absolutePath();

		strFile = QFileDialog::getOpenFileName(
					NULL,
					"Choose a file",
					strStartDir,
					"FPDF (*.fpdf)");
	}

	if(!strFile.isEmpty())
	{
		//TOFIX if license is not empty, issue the warning that it will kill existing contents!

		if(!m_lic.m_fpdf.Load(strFile.toLatin1().constData()))
			QMessageBox::information(this, "Warning", "Failed to load FPDF file!");
		else
		{
			//TOFIX: resolve all ITEM_STATUS_INHERITED flags to on or off ?
			#ifdef _DEBUG
				m_lic.AssertValid();
			#endif
			bFpdfLoaded = true;
			ui.actionLoadFPDF->setEnabled(false);	//FPDF can be loeaded only once

			//license can be created only after FPDF tree is loaded 
			EnableLicenseHeader(true);
			EnableModuleList(true);
	
			QString strMsgStatus = "Loaded tree file: " + strFile;
			ui.statusBar->showMessage(strMsgStatus);	//show file name in the status bar

			QString strMsg;
			strMsg.sprintf(tr("KeyGenerator (Build: %s %s) %s").toLatin1().constData(), __DATE__, __TIME__, strMsgStatus.toLatin1().constData());
			setWindowTitle(strMsg);

			return true;
		}
	}

	return false;
}

void KeyGenerator::OnDumpLicense()
{
	/*
	//calculate default directory for the file open dialog
	QString strStartDir = QCoreApplication::applicationDirPath();
	QDir dir(strStartDir);
	if (dir.cd("data"))
		strStartDir = dir.absolutePath();

	QString strFile = QFileDialog::getSaveFileName(
                    NULL,
                    "Choose a file",
                    strStartDir,
                    "Text file (*.txt)");
	if(!strFile.isEmpty())
	{
		//ensure ".txt" extension
		if(!strFile.endsWith(".txt"))
			strFile.append(".txt");

		//if(!m_lic.Save(strFile.toLatin1().constData()))
		//	QMessageBox::information(this, "Warning", "Failed to dump license file!");
	}
	*/

#ifdef _DEBUG
	m_lic.Dump();
	m_lic.AssertValid();
#endif
}

void KeyGenerator::LoadLicenseKey(QString strFile)
{
	if(m_lic.Load(strFile))
	{
	#ifdef _DEBUG
		m_lic.AssertValid();
	#endif

		//update license header GUI from license data
		ui.lineHdr_ClientID->setText(m_lic.m_strCustomerID);
		int nPos = ui.comboHdr_LicenseVersion->findText(QString().sprintf("%d", m_lic.m_nLicenseVersion));
		if(nPos >= 0)
			ui.comboHdr_LicenseVersion->setCurrentIndex(nPos);
		ui.lineHdr_CustomSolutionID->setText(QString().sprintf("%d", m_lic.m_nCustomSolutionID));
		ui.lineHdr_LicenseID->setText(QString().sprintf("%d", m_lic.m_nLicenseID));
		ui.lineHdr_ClientName->setText(m_lic.m_strClientName);
		ui.checkHdr_IsCompany->setCheckState(m_lic.m_bIsCompany? Qt::Checked : Qt::Unchecked); 
		ui.lineHdr_CreatedDate->setText(m_lic.m_dateCreated.toString(Qt::ISODate));
		ui.lineHdr_ModifiedDate->setText(m_lic.m_dateModified.toString(Qt::ISODate));
		ui.lineHdr_Text1->setText(m_lic.m_strReportLine1);
		ui.lineHdr_Text2->setText(m_lic.m_strReportLine2);

		//refresh module count indicator
		ui.lineHdr_NumModules->setText(QString().sprintf("%d",m_lic.m_lstModInfo.size()));

		//fill the list of available modules
		ui.treeModules->blockSignals(true);
		ui.treeModules->clear();
		int nCount = m_lic.m_lstModInfo.size();
		for(int i=0; i<nCount; i++)
		{
			QTreeWidgetItem *pItem = new QTreeWidgetItem(ui.treeModules);
			pItem->setText(0, m_lic.m_lstModInfo[i].m_strCode);
			pItem->setText(1, QString().sprintf("%d", m_lic.m_lstModInfo[i].m_nNumLicenses));
			pItem->setText(2, m_lic.m_lstModInfo[i].m_strMLIID);
		}
		ui.treeModules->blockSignals(false);

		ClearModuleInfo();

		m_strKeyFile = strFile;

		QString strMsgStatus = "Loaded keyfile: " + strFile;
		ui.statusBar->showMessage(strMsgStatus);	//show file name in the status bar

		QString strMsg;
		strMsg.sprintf(tr("KeyGenerator (Build: %s %s) %s").toLatin1().constData(), __DATE__, __TIME__, strMsgStatus.toLatin1().constData());
		setWindowTitle(strMsg);
	}
	else
		QMessageBox::information(this, "Warning", "Failed to load License Key file!");
}

void KeyGenerator::FixKeyfileNewFPs()
{
	//fix the keyfile to add the new items from the FPDF (FPs not in keyfile)
	//module FPs that could not be found in the keyfile (newer than the keyfile)
	int nCount = m_lic.m_lstModInfo.size();
	for(int nCurSection=0; nCurSection<nCount; nCurSection++)
	{
		TreeList &lstData(m_lic.m_lstModInfo[nCurSection].m_treeAR.m_lstModSets);

		//for each item in the single module tree
		int nModSibling = m_lic.m_lstModInfo[nCurSection].m_nModTreeSibling;
		int nModIdx = lstData.FindChild(-1, nModSibling);
		if(nModIdx < 0)
			break;	// module data not found
		int nItemIdx = nModIdx + 1;
		int nItemsCount = lstData.size();
		while(nItemIdx < nItemsCount)
		{
			if(lstData[nItemIdx].nLevel < 1)
				break;	//end of branch

			//IGNORE non-leaf items from this (groups)
			if(ITEM_TYPE_FP == lstData[nItemIdx].nType)
			{
				bool bRedColor = true;
				int nFoundIdx = -1;
				int nMax = m_lic.m_lstModInfo[nCurSection].m_lstAR.count();
				if(nMax > 0){
					for(int i=0; i<nMax; i++){
						if(m_lic.m_lstModInfo[nCurSection].m_lstAR[i].nCode == lstData[nItemIdx].nCode){
							bRedColor = false;	//found the AR existign in the keyfile (FPDF file is not newer then the keyfile, just looking at this single access right)
							nFoundIdx = i;
							break;
						}
					}
				}
				else
					bRedColor = false;	//new key file
				if(bRedColor){
					//item was not found in the m_lstAR list, add the defaults
					FP_ITEM1 item;
					item.nCode = lstData[nItemIdx].nCode;
					item.nValue = lstData[nItemIdx].nValue;
					item.bCanUse = (lstData[nItemIdx].nStatus == ITEM_STATUS_ON || lstData[nItemIdx].nStatus == ITEM_STATUS_INHERITED); //TOFIX support for inherited status too
/*
					if(lstData[nItemIdx].nStatus == ITEM_STATUS_INHERITED)
					{
						//if necessary, resolve inherited status (search the same item in the FP list)
						int nIdxFP = m_lic.m_fpdf.m_lstFP.FindByCode(item.nCode);
						if(nIdxFP >= 0)
							item.bCanUse = (m_lic.m_fpdf.m_lstFP[nIdxFP].nStatus == ITEM_STATUS_ON);
						else
							item.bCanUse = true;
					}
//					else
//						qDebug() << QString("Fixed: Module section: %3, FP: %1, can use: %2").arg(lstData[nItemIdx].strName).arg(item.bCanUse).arg(nCurSection);
*/					
					qDebug() << QString("Fixed: Module section: %3, FP: %1, can use: %2").arg(lstData[nItemIdx].strName).arg(item.bCanUse).arg(nCurSection);
					m_lic.m_lstModInfo[nCurSection].m_lstAR.append(item);
				}
			}
			nItemIdx ++;
		}
	}
}
