#include <QApplication>
#include "keygenerator.h"

QString g_strCmdLineDocument;

int main(int argc, char *argv[])
{
	if(argc > 1)
		g_strCmdLineDocument = argv[1];

    QApplication a(argc, argv);
    KeyGenerator w;
    w.show();
    a.connect(&a, SIGNAL(lastWindowClosed()), &a, SLOT(quit()));
    return a.exec();
}
