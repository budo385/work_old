/****************************************************************************
**
** Copyright (C) 2005-2006 Trolltech AS. All rights reserved.
**
** This file is part of the example classes of the Qt Toolkit.
**
** Licensees holding a valid Qt License Agreement may use this file in
** accordance with the rights, responsibilities and obligations
** contained therein.  Please consult your licensing agreement or
** contact sales@trolltech.com if any conditions of this licensing
** agreement are not clear to you.
**
** Further information about Qt licensing is available at:
** http://www.trolltech.com/products/qt/licensing.html or by
** contacting info@trolltech.com.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

/*
    QModTreeModel.cpp

    Provides a simple tree model to show how to create and use hierarchical
    models.
*/

#include <QtGui>
#include <QTreeView>

#include "treeitem.h"
#include "qmodtreemodel.h"

QModTreeModel::QModTreeModel(QObject *parent)
    : QAbstractItemModel(parent)
{
	m_pData = NULL;
	m_pWidget = NULL;
	m_pFPWidget = NULL;
	m_pFPSWidget = NULL;

	m_bFPDrop = false;
	m_bFPSDrop = false;

	QList<QVariant> rootData;
    rootData << "Name";
    rootItem = new TreeItem(rootData);
}

QModTreeModel::~QModTreeModel()
{
    delete rootItem;
}

int QModTreeModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return static_cast<TreeItem*>(parent.internalPointer())->columnCount();
    else
        return rootItem->columnCount();
}

QVariant QModTreeModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
		return QVariant();

	QList<int> lstPos;
	IndexToList(index, lstPos);
	int nNode = m_pData->m_lstModSets.FindNode(lstPos);
	if(nNode < 0)
		return QVariant();	//TOFIX
	//Q_ASSERT(nNode >= 0);

    TreeItem *item = static_cast<TreeItem*>(index.internalPointer());
	if (role == Qt::CheckStateRole)
	{
		//update internal data
		//only FP nodes are checkable
		if(ITEM_TYPE_FP == m_pData->m_lstModSets[nNode].nType)
			return QVariant(item->nCheckState);	//return node checked state	
			
		return QVariant();	//no checkbox for this one
	}
	else if (role == Qt::DecorationRole)
	{
		//calculate icon based on the type (FP/PFS)
		//is node hidden
		bool bHidden = false;
		int nIdx = nNode;
		while(nIdx >= 0)
		{
			//check if the node is hidden
			if(!m_pData->m_lstModSets[nIdx].bActive){
				bHidden = true;
				break;
			}
			//proceed to parent level (check if the parent is hidden)
			nIdx = m_pData->m_lstModSets.FindParent(nIdx);
		}

		QIcon icon;
		if(bHidden)
			icon.addFile(":inactive.png");
		else if(ITEM_TYPE_MOD == m_pData->m_lstModSets[nNode].nType)
			icon.addFile(":mod.png");
		else if(ITEM_TYPE_FPS == m_pData->m_lstModSets[nNode].nType)
			icon.addFile(":fps.png");
		else
			icon.addFile(":fp.png");
		return QVariant(icon);	//return node's icon
	}
	else if (role == Qt::ToolTipRole || role == Qt::StatusTipRole)
	{
		//support to view long entries as tooltips and status bar entries
		return QVariant(m_pData->m_lstModSets[nNode].strDesc);
	}

	if (role != Qt::DisplayRole){
		return QVariant();
	}

	//return node text
    return item->data(index.column());
}

Qt::ItemFlags QModTreeModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::ItemIsEnabled;

	Qt::ItemFlags flags = Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsUserCheckable | Qt::ItemIsTristate; 

	//only top level items can be drop targets
	if(!index.parent().isValid())
		flags |= Qt::ItemIsDropEnabled;

	return flags;
}

QVariant QModTreeModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
        return rootItem->data(section);

    return QVariant();
}

QModelIndex QModTreeModel::index(int row, int column, const QModelIndex &parent)
            const
{
    TreeItem *parentItem;

    if (!parent.isValid())
        parentItem = rootItem;
    else
        parentItem = static_cast<TreeItem*>(parent.internalPointer());

    TreeItem *childItem = parentItem->child(row);
    if (childItem)
        return createIndex(row, column, childItem);
    else
        return QModelIndex();
}

QModelIndex QModTreeModel::parent(const QModelIndex &index) const
{
    if (!index.isValid())
        return QModelIndex();

    TreeItem *childItem = static_cast<TreeItem*>(index.internalPointer());
    TreeItem *parentItem = childItem->parent();

    if (parentItem == rootItem)
        return QModelIndex();

    return createIndex(parentItem->row(), 0, parentItem);
}

int QModTreeModel::rowCount(const QModelIndex &parent) const
{
    TreeItem *parentItem;

    if (!parent.isValid())
        parentItem = rootItem;
    else
        parentItem = static_cast<TreeItem*>(parent.internalPointer());

    return parentItem->childCount();
}

bool QModTreeModel::insertRows(int row, int count, const QModelIndex &parent)
{
	TreeItem *item = static_cast<TreeItem*>(parent.internalPointer());
	if(NULL == item)
		item = rootItem;
	
	int first = item->childCount();
	beginInsertRows(parent, first, first+count-1);

	//add X children into the parent node
	QList<QVariant> columnData;
	columnData << "New item";	//temporary name for all new child nodes
	for(int i=0; i<count; i++)
	{
		item->appendChild(new TreeItem(columnData, item));
	}

	endInsertRows();

	//
	// fix our internal FPDF storage
	//

	//TOFIX: below we assume only one node/branch was dropped
	//		 (will need fixing if we require multiple node drag and drop)
	if(m_bFPSDrop && NULL != m_pFPSWidget)
	{
		m_bFPSDrop = false;
		qDebug("FP Set dropped on to Model tree");

		//find parent node in the list (this is always top level node - the only DnD target allowed)
		QList<int> lstPos;
		IndexToList(parent, lstPos);
		int nModule = m_pData->m_lstModSets.FindNode(lstPos);
		Q_ASSERT(nModule >= 0);

		//get selected FPS (the one being dropped to this widget - only top level branch can be source)
		QModelIndex idxSrc = ((QTreeView *)m_pFPSWidget)->selectionModel()->currentIndex();
		IndexToList(idxSrc, lstPos);
		int nFpsIdx = m_pData->m_lstFpSets.FindNode(lstPos);
		Q_ASSERT(nFpsIdx >= 0);

		//copy entire branch into the new tree
		m_pData->m_lstModSets.CopyNode(m_pData->m_lstFpSets, nFpsIdx, nModule);

		//generate new model indexes recursively / refresh tree layout
		RebuildGuiBranch(parent);
	}
	else if (m_bFPDrop && NULL != m_pFPWidget)
	{
		//single FP dropped
		m_bFPDrop = false;
		qDebug("FP dropped on to Model tree");

		//find parent node in the list (this is always top level node - the only DnD target allowed)
		QList<int> lstPos;
		IndexToList(parent, lstPos);
		m_pData->m_lstModSets.DumpPos(lstPos);
		int nModule = m_pData->m_lstModSets.FindNode(lstPos);
		Q_ASSERT(nModule >= 0);

		//get selected FP (the one being dropped to this widget)
		QString strName;
		bool bActive = true;
		int nValue = 0;
		int nState = ITEM_STATUS_ON;

		//try to copy original internal name (without "(11)" suffix)
		QModelIndex idxSrc = ((QTreeView *)m_pFPWidget)->selectionModel()->currentIndex();
		if(idxSrc.isValid()){
			strName = m_pData->m_lstFP[idxSrc.row()].strName;
			nValue  = m_pData->m_lstFP[idxSrc.row()].nValue;
			nState  = m_pData->m_lstFP[idxSrc.row()].nStatus;
			bActive  = m_pData->m_lstFP[idxSrc.row()].bActive;
		}

		//case when adding new point
		FP_ITEM item;
		item.strName  = strName;
		item.bActive  = bActive;			//copy active state from src
		item.nType	  = ITEM_TYPE_FP;
		item.nValue	  = nValue;				//copy value from source
		item.nStatus  = nState;				//copy status from original
		item.nLevel	  = 0;					//will be filled by AddChild
		item.nCode	  = idxSrc.row() + 1;	//calculate code from index

		m_pData->m_lstModSets.AddChild(nModule, item);
	}

	return true;
}

bool QModTreeModel::removeRows(int row, int count, const QModelIndex &parent)
{
	TreeItem *item = static_cast<TreeItem*>(parent.internalPointer());
	if(NULL == item)
		item = rootItem;

	//remove X children from the parent node
	beginRemoveRows(parent, row, row+count-1);
	item->RemoveChildren(row, count);
	endRemoveRows();

	//refresh internal model (assume only top level can be deleted)
	QList<int> lstPos;
	IndexToList(parent, lstPos);
	int nParent = m_pData->m_lstModSets.FindNode(lstPos);	// can be -1

	for(int i=0; i<count; i++)
	{
		int nChild = m_pData->m_lstModSets.FindChild(nParent, row);
		m_pData->m_lstModSets.RemoveNode(nChild);
	}

	return true;
}

bool QModTreeModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
	TreeItem *item = static_cast<TreeItem*>(index.internalPointer());
	if(NULL != item)
	{
		Q_ASSERT(0 == index.column());
		if(Qt::EditRole == role || Qt::DisplayRole == role)
			item->SetData(index.column(), value);	//store title
		else if (Qt::CheckStateRole == role)
		{
			//add 3state support
			if(item->nCheckState == 2)
				item->nCheckState = 1;		//store check state
			else
				item->nCheckState = value.toInt();		//store check state
		
			//update internal data
			QList<int> lstPos;
			IndexToList(index, lstPos);
			int nNode = m_pData->m_lstModSets.FindNode(lstPos);
			Q_ASSERT(nNode >= 0);
			m_pData->m_lstModSets[nNode].nStatus = (item->nCheckState == 0)? ITEM_STATUS_OFF : (item->nCheckState == 2)? ITEM_STATUS_ON : ITEM_STATUS_INHERITED;
		}
		emit dataChanged(index, index);	//force item redraw
		return true;
	}

	Q_ASSERT(false);
	return false;
}

Qt::DropActions QModTreeModel::supportedDropActions() const
{
    return Qt::CopyAction;
}

void QModTreeModel::Clear()
{
	if(rootItem->childCount() > 0)
	{
		QModelIndex parent;
		removeRows(0, rootItem->childCount(), parent);
		emit layoutChanged();
	}
}

//store parent branch info (list of indexes)
void QModTreeModel::IndexToList(QModelIndex index, QList<int> &lstPos) const
{
	lstPos.clear();

	QModelIndex tmp = index;
	while(tmp.isValid()){
		lstPos.push_front(tmp.row());
		tmp = tmp.parent();
	}
}

QModelIndex QModTreeModel::ListToIndex(QList<int> lstPos)
{
	QModelIndex index = createIndex(rootItem->row(), 0, rootItem);	//get root

	int nCount = lstPos.size();
	for(int i=0; i<nCount; i++)
		index = index.child(lstPos[i], 0);

	return index;
}

bool QModTreeModel::AddNewModule(QString strName, bool bVisible, int nStatus)
{
	return AddNewLeaf(rootItem, strName, bVisible, nStatus);
}

bool QModTreeModel::AddNewLeaf(TreeItem *parent, QString strName, bool bVisible, int nStatus)
{
	QList<QVariant> columnData;
	columnData << strName;
	TreeItem *child = new TreeItem(columnData, parent);
	//support 3state check box
	child->nCheckState = (nStatus == ITEM_STATUS_ON)? 2 : (nStatus == ITEM_STATUS_OFF)? 0 : 1;

	parent->appendChild(child);
	emit layoutChanged();

	//if needed, hide row in the QTreeView
	if(!bVisible){
		if(m_pWidget){
			QModelIndex index;
			int row = parent->childCount()-1;
			((QTreeView *)m_pWidget)->setRowHidden(row, index, !bVisible);
		}
	}

	return true;
}

void QModTreeModel::ShowRow(int nRow, bool bShow)
{
	((QTreeView *)m_pWidget)->clearSelection();//TOFIX this is temporary fix for setRowHidden bug
	((QTreeView *)m_pWidget)->setRowHidden(nRow, QModelIndex(), !bShow);
}

void QModTreeModel::ShowHidden(bool bShow)
{
	//unhide all rows
	QModelIndex root;
	int nCount = m_pData->m_lstModSets.size();
	for(int i=0; i<nCount; i++)
	{
		if(!m_pData->m_lstModSets[i].bActive)
			((QTreeView *)m_pWidget)->setRowHidden(i, root, !bShow);
	}
	emit layoutChanged();
}

//only fill missing pieces in the model (available from storage)
void QModTreeModel::RebuildGuiBranch(QModelIndex parent)
{
	TreeList &lstData(m_pData->m_lstModSets);

	//find parent node in the data storage list
	QList<int> lstPos;
	IndexToList(parent, lstPos);
	int nParent = lstData.FindNode(lstPos);

	TreeItem *item = rootItem;
	if(parent.isValid())
		item = static_cast<TreeItem*>(parent.internalPointer());

	//compare child counts in the model and data storage
	int nExistingCount = item->childCount();
	int nTotalCount	   = lstData.GetChildCount(nParent);

	int nDiff = nTotalCount - nExistingCount;
	if(nDiff > 0)
	{
		beginInsertRows(parent, nExistingCount, nTotalCount-1);
		int nChildIdx = lstData.FindChild(nParent, nExistingCount);
		while(nChildIdx >= 0)
		{
			//prepare column data for the new item
			QString strTitle;

			//append optional code into the title
			if(lstData[nChildIdx].strCode.isEmpty())
				strTitle = lstData[nChildIdx].strName;
			else
				strTitle.sprintf("%s (%s)", lstData[nChildIdx].strName.toLatin1().constData(), lstData[nChildIdx].strCode.toLatin1().constData());

			//append optional value into the title
			if(lstData[nChildIdx].nValue != 0)
			{
				QString strTmp;
				strTmp.sprintf(" (%d)",	lstData[nChildIdx].nValue);
				strTitle += strTmp;
			}

			//create new child item
			QList<QVariant> columnData;
			columnData << strTitle;

			TreeItem *child = new TreeItem(columnData, item);

			int nStatus = lstData[nChildIdx].nStatus;
			child->nCheckState = (nStatus == ITEM_STATUS_ON)? 2 : (nStatus == ITEM_STATUS_OFF)? 0 : 1;
			item->appendChild(child);

			nChildIdx = lstData.FindNextSibling(nChildIdx);	// go to next child
		}
		endInsertRows();
	}

	//recurse to the deeper level
	for(int i=0; i<nTotalCount; i++)
		RebuildGuiBranch(index(i, 0, parent));
}

QModelIndex QModTreeModel::GetRoot()
{
	return createIndex(rootItem->row(), 0, rootItem);
}

void QModTreeModel::RedrawBranch(QModelIndex index)
{
	//calculate last descendant
	QModelIndex last = index;
	TreeItem *item = static_cast<TreeItem*>(last.internalPointer());
	int nCnt = item->childCount();
	while(nCnt > 0){
		last = last.child(nCnt-1, 0);
		item = static_cast<TreeItem*>(last.internalPointer());
		nCnt = item->childCount();
	}

	emit dataChanged(index, last);	//force item redraw
}
