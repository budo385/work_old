/****************************************************************************
**
** Copyright (C) 2005-2006 Trolltech AS. All rights reserved.
**
** This file is part of the example classes of the Qt Toolkit.
**
** Licensees holding a valid Qt License Agreement may use this file in
** accordance with the rights, responsibilities and obligations
** contained therein.  Please consult your licensing agreement or
** contact sales@trolltech.com if any conditions of this licensing
** agreement are not clear to you.
**
** Further information about Qt licensing is available at:
** http://www.trolltech.com/products/qt/licensing.html or by
** contacting info@trolltech.com.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#ifndef QFPTREEMODEL_H
#define QFPTREEMODEL_H

#include <QAbstractItemModel>
#include <QModelIndex>
#include <QVariant>
#include "bus_core/bus_core/fpdf.h"

class TreeItem;

class QFPTreeModel : public QAbstractItemModel
{
    Q_OBJECT

public:
    QFPTreeModel(QObject *parent = 0);
    ~QFPTreeModel();

    QVariant data(const QModelIndex &index, int role) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const;
    QModelIndex index(int row, int column,
                      const QModelIndex &parent = QModelIndex()) const;
    QModelIndex parent(const QModelIndex &index) const;
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;
	bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole );

	//my APIs
	bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex());
	bool AddNewFP(QString strName, bool bVisible = true, bool bChecked = true);
	void Clear();
	void ShowHidden(bool bShow = true);
	void ShowRow(int nRow, bool bShow = true);
	void IndexToList(QModelIndex index, QList<int> &lstPos) const;
	void RedrawBranch(QModelIndex index);

private:
    TreeItem *rootItem;

public:
    FPDF	*m_pData;
	QWidget	*m_pWidget;
};

#endif
