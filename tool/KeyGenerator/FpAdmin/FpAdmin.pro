TEMPLATE	= app
LANGUAGE	= C++
CONFIG		+= qt warn_on release ordered common bus_core
HEADERS		= fpadmin.h \
		  newmodule.h \
		  qfpstreemodel.h \
		  qfpstreewidget.h \
		  qfptreemodel.h \
		  qfptreewidget.h \
		  qmodtreemodel.h \
		  qmodtreewidget.h \
		  treeitem.h
SOURCES		= fpadmin.cpp \
		  main.cpp \
		  newmodule.cpp \
		  qfpstreemodel.cpp \
		  qfpstreewidget.cpp \
		  qfptreemodel.cpp \
		  qfptreewidget.cpp \
		  qmodtreemodel.cpp \
		  qmodtreewidget.cpp \
		  treeitem.cpp
INTERFACES	= fpadmin.ui \
		  newmoduledlg.ui
TARGET		= FpAdmin
INCLUDEPATH	+= ../../../lib
LIBS 		+= -L../../../lib/bus_core -lbus_core -L../../../lib/common/ -lcommon 
