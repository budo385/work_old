#include "qfptreewidget.h"
#include <QHeaderView>

QFPTreeWidget::QFPTreeWidget(QWidget *parent)
	: QTreeView(parent)
{
	setSelectionMode(QAbstractItemView::SingleSelection);
    setDragEnabled(true);
    setDropIndicatorShown(true);
}

QFPTreeWidget::~QFPTreeWidget()
{
}

void QFPTreeWidget::dragEnterEvent(QDragEnterEvent *event)
{
	//we are drag source, but NOT drop target
	event->ignore();
}

void QFPTreeWidget::dropEvent(QDropEvent *event)
{
	//we are drag source, but NOT drop target
	event->ignore();
}
