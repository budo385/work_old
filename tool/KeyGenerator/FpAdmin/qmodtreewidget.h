#ifndef QModTreeWidget_H
#define QModTreeWidget_H

#include <QTreeView>
#include <QDragEnterEvent>
#include <QDropEvent>
#include "bus_core/bus_core/fpdf.h"

class QModTreeWidget : public QTreeView
{
	Q_OBJECT

public:
    QModTreeWidget(QWidget *parent);
    ~QModTreeWidget();

private:
    void dragEnterEvent(QDragEnterEvent *event);
    void dropEvent(QDropEvent *event);

public:
    FPDF *m_pData;
	
	QWidget *m_pFPWidget;
	QWidget *m_pFPSWidget;
};

#endif // QModTreeWidget_H
