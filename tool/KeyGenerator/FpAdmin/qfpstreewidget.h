#ifndef QFPSTREEWIDGET_H
#define QFPSTREEWIDGET_H

#include <QTreeView>
#include <QDragEnterEvent>
#include <QDropEvent>
#include "bus_core/bus_core/fpdf.h"

class QFPSTreeWidget : public QTreeView
{
	Q_OBJECT

public:
    QFPSTreeWidget(QWidget *parent);
    ~QFPSTreeWidget();

private:
    void dragEnterEvent(QDragEnterEvent *event);
    void dropEvent(QDropEvent *event);

public:
	QWidget *m_pFPWidget;
};

#endif // QFPSTREEWIDGET_H
