#include "newmodule.h"
#include <QMessageBox>

newmodule::newmodule(QWidget *parent)
    : QDialog(parent)
{
	ui.setupUi(this);
}

newmodule::~newmodule()
{
}

void newmodule::on_pushOK_clicked()
{
	m_strCode = ui.lineCode->text();
	m_strName = ui.lineName->text();

	//validate input 
	if(m_strCode.isEmpty() || m_strName.isEmpty())
	{
		QMessageBox::information(this, "FpAdmin",
			"Please fill both the code and the name field.");
		return;
	}

	//name must not contain some junk characters
	if( m_strName.indexOf('.') >= 0 ||
		m_strName.indexOf('(') >= 0 ||
		m_strName.indexOf(')') >= 0 ||
		m_strName.indexOf('=') >= 0 ||
		m_strName.indexOf(',') >= 0 ||
		m_strName.indexOf('&') >= 0)
	{
		QMessageBox::information(this, "FpAdmin",
			"Please use only basic alphanumeric characters for the name\n(no ',.()=,&' etc.).");
		return;
	}


	accept();
}

void newmodule::on_pushCancel_clicked()
{
	reject();
}

