#include "fpadmin.h"
#include "newmodule.h"
#include <QFileDialog>
#include <QTreeWidgetItem>
#include <QHeaderView>
#include <QInputDialog>
#include <QMessageBox>
#include <QDateTime>
#include <QMenu>
#include "treeitem.h"

QString CreateSymbolName(QString strInput);

FpAdmin::FpAdmin(QWidget *parent, Qt::WindowFlags flags)
    : QMainWindow(parent, flags)
{
	ui.setupUi(this);
	ui.mainToolBar->hide();
	ui.statusBar->hide();

	//initialze FP view
	m_pFPModel = new QFPTreeModel;
	m_pFPModel->m_pData = &m_def;
	m_pFPModel->m_pWidget = ui.treeFP;
	ui.treeFP->setModel(m_pFPModel);
	ui.treeFP->header()->hide();

	//initialze FPS list
	m_pFPSModel = new QFPSTreeModel;
	m_pFPSModel->m_pData = &m_def;
	m_pFPSModel->m_pWidget = ui.treeFPSets;
	m_pFPSModel->m_pFPWidget = ui.treeFP;
	ui.treeFPSets->m_pFPWidget = ui.treeFP;
	ui.treeFPSets->setModel(m_pFPSModel);
	ui.treeFPSets->header()->hide();

	//initialze Modules list
	m_pModModel = new QModTreeModel;
	m_pModModel->m_pData = &m_def;
	m_pModModel->m_pWidget = ui.treeModules;
	m_pModModel->m_pFPWidget = ui.treeFP;
	m_pModModel->m_pFPSWidget = ui.treeFPSets;
	ui.treeModules->m_pFPWidget = ui.treeFP;
	ui.treeModules->m_pFPSWidget = ui.treeFPSets;
	ui.treeModules->setModel(m_pModModel);
	ui.treeModules->header()->hide();

	//cross link the model objects
	m_pFPSModel->m_pModModel = m_pModModel;

    QObject::connect(ui.actionLoadCodeFile, SIGNAL(triggered()), this, SLOT(OnFileOpen()));
    QObject::connect(ui.actionSaveCodeFile, SIGNAL(triggered()), this, SLOT(OnFileSave()));
	QObject::connect(ui.actionGenerateModuleHeaders, SIGNAL(triggered()), this, SLOT(OnGenerateHeaders()));
	QObject::connect(ui.treeFP->selectionModel(), SIGNAL(currentChanged(const QModelIndex&, const QModelIndex&)), this, SLOT(OnFPSelectionChange(const QModelIndex&, const QModelIndex&)));
	QObject::connect(ui.treeModules->selectionModel(), SIGNAL(currentChanged(const QModelIndex&, const QModelIndex&)), this, SLOT(OnModuleFPSelectionChange(const QModelIndex&, const QModelIndex&)));
	QObject::connect(ui.treeFPSets->selectionModel(), SIGNAL(currentChanged(const QModelIndex&, const QModelIndex&)), this, SLOT(OnFPSetSelectionChange(const QModelIndex&, const QModelIndex&)));

	ui.actionLoadCodeFile->setShortcut(QKeySequence(tr("Ctrl+O")));
	ui.actionSaveCodeFile->setShortcut(QKeySequence(tr("Ctrl+S")));
	ui.actionQuit->setShortcut(QKeySequence(tr("Ctrl+Q")));
	ui.actionGenerateModuleHeaders->setShortcut(QKeySequence(tr("Ctrl+H")));

	ui.lineFPSValue->setEnabled(false);
	ui.lineMFPValue->setEnabled(false);

	//automatically search and load first .fpdf file found in "data" subdirectory (if found)
	QString strAppDir = QCoreApplication::applicationDirPath();
	QDir dir(strAppDir, QString("*.fpdf"));
	if (dir.cd("data"))
	{
		//get first .fpdf file in a directory
		QFileInfoList list = dir.entryInfoList();
        if(list.size() > 0) {
			LoadFpdfFile(list.at(0).absoluteFilePath());
        }
	}
}

FpAdmin::~FpAdmin()
{
	if(m_def.IsModified())
	{
		int nRes = QMessageBox::question(this, "Warning", 
			"FPDF document has been modified! Save?",tr("&Yes"), tr("&No"),
			QString(), 0, 1);
		if(0 == nRes){
			OnFileSave();
		}
	}
}

void FpAdmin::OnFileOpen()
{
	LoadFpdfFile();
}

void FpAdmin::LoadFpdfFile(QString strFile)
{
	if(strFile.isEmpty())
	{
		//calculate default directory for the file open dialog
		QString strStartDir;
		if(m_strDir.isEmpty())
		{
			strStartDir = QCoreApplication::applicationDirPath();
			QDir dir(strStartDir);
			if (dir.cd("data"))
				strStartDir = dir.absolutePath();
		}
		else
			strStartDir = m_strDir;

		strFile = QFileDialog::getOpenFileName(
                    NULL,
                    "Choose a file",
                    strStartDir,
                    "FPDF (*.fpdf)");

	}

	if(!strFile.isEmpty())
	{
		//remember last used directory
		m_strDir = QFileInfo(strFile).absolutePath();

		//clear entire contents
		m_pModModel->Clear();
		m_pFPModel->Clear();
		m_pFPSModel->Clear();

		if(!m_def.Load(strFile.toLatin1().constData()))
			QMessageBox::information(this, "Warning", "Failed to load FPDF file!");
		else{
			ui.statusBar->showMessage(strFile);	//show file name in the status bar
			setWindowTitle ("FpAdmin - " + strFile);
		}

		ShowData();	//refresh window contents

#ifdef _DEBUG
		m_def.Dump();
		m_def.AssertValid();
#endif
	}
}

void FpAdmin::OnFileSave()
{
	//calculate default directory for the file open dialog
	QString strStartDir;
	if(m_strDir.isEmpty()){
		strStartDir = QCoreApplication::applicationDirPath();
		QDir dir(strStartDir);
		if (dir.cd("data"))
			strStartDir = dir.absolutePath();
	}
	else
		strStartDir = m_strDir;

	QString strFile = QFileDialog::getSaveFileName(
                    NULL,
                    "Choose a file",
                    strStartDir,
                    "FPDF (*.fpdf)");
	if(!strFile.isEmpty())
	{
		//remember last used directory
		m_strDir = QFileInfo(strFile).absolutePath();

		//ensure ".fpdf" extension
		if(!strFile.endsWith(".fpdf"))
			strFile.append(".fpdf");

		if(!m_def.Save(strFile.toLatin1().constData()))
			QMessageBox::information(this, "Warning", "Failed to save FPDF file!");
		else
		{
			ui.statusBar->showMessage(strFile);	//show file name in the status bar
			setWindowTitle ("FpAdmin - " + strFile);
		}
	}
}

void FpAdmin::ShowData()
{
	//clear all trees
	m_pFPModel->Clear();
	m_pFPSModel->Clear();
	m_pModModel->Clear();

	//show all function points
	bool bShowAllFP = ui.cbShowAllFP->isChecked();
	int nCount = m_def.m_lstFP.size();
	for(int i=0; i<nCount; i++)
	{
		//prepare title
		QString strTitle = m_def.m_lstFP[i].strName;
		if(m_def.m_lstFP[i].nValue != 0){
			QString strTmp;
			strTmp.sprintf(" (%d)",	m_def.m_lstFP[i].nValue);
			strTitle += strTmp;
		}

		//update model (GUI)
		m_pFPModel->AddNewFP(strTitle, bShowAllFP || m_def.m_lstFP[i].bActive, (m_def.m_lstFP[i].nStatus == ITEM_STATUS_ON)? 1 : 0);
	}

	//build FPSets tree
	bool bShowAllFPS = ui.checkShowAllFPS->isChecked();
	nCount = m_def.m_lstFpSets.size();
	for(int i=0; i<nCount; i++)
	{
		//prepare title
		QString strTitle = m_def.m_lstFpSets[i].strName;
		if(m_def.m_lstFpSets[i].nValue != 0){
			QString strTmp;
			strTmp.sprintf(" (%d)",	m_def.m_lstFpSets[i].nValue);
			strTitle += strTmp;
		}

		//update model (GUI)
		int nParent = m_def.m_lstFpSets.FindParent(i);
		if(nParent < 0)
			m_pFPSModel->AddNewFPSet(strTitle, bShowAllFPS || m_def.m_lstFpSets[i].bActive, m_def.m_lstFpSets[i].nStatus);
		else{
			QList<int> lstPos;
			m_def.m_lstFpSets.GetNodePos(nParent, lstPos);
			//m_def.m_lstFpSets.DumpPos(lstPos);
			QModelIndex index = m_pFPSModel->ListToIndex(lstPos);
			TreeItem *parent = static_cast<TreeItem*>(index.internalPointer());
			Q_ASSERT(parent != NULL);

			m_pFPSModel->AddNewLeaf(parent, strTitle, bShowAllFPS || m_def.m_lstFpSets[i].bActive, m_def.m_lstFpSets[i].nStatus);
		}
	}

	//build modules tree
	bool bShowAllMod = ui.cbShowAllMod->isChecked();
	nCount = m_def.m_lstModSets.size();
	for(int i=0; i<nCount; i++)
	{
		QString strTitle;
		if(m_def.m_lstModSets[i].strCode.isEmpty())
			strTitle  = m_def.m_lstModSets[i].strName;
		else
			strTitle.sprintf("%s (%s)", m_def.m_lstModSets[i].strName.toLatin1().constData(), m_def.m_lstModSets[i].strCode.toLatin1().constData());
		if(m_def.m_lstModSets[i].nValue != 0){
			QString strTmp;
			strTmp.sprintf(" (%d)",	m_def.m_lstModSets[i].nValue);
			strTitle += strTmp;
		}

		//update model (GUI)
		int nParent = m_def.m_lstModSets.FindParent(i);
		if(nParent < 0)
			m_pModModel->AddNewModule(strTitle, bShowAllMod || m_def.m_lstModSets[i].bActive, m_def.m_lstModSets[i].nStatus);
		else{
			QList<int> lstPos;
			m_def.m_lstModSets.GetNodePos(nParent, lstPos);
			//m_def.m_lstModSets.DumpPos(lstPos);
			QModelIndex index = m_pModModel->ListToIndex(lstPos);
			TreeItem *parent = static_cast<TreeItem*>(index.internalPointer());
			Q_ASSERT(parent != NULL);

			m_pModModel->AddNewLeaf(parent, strTitle, bShowAllMod || m_def.m_lstModSets[i].bActive, m_def.m_lstModSets[i].nStatus);
		}
	}
}

void FpAdmin::on_pushNewFP_clicked()
{
	bool ok;
    QString strName = QInputDialog::getText(this, tr("Input"),
                                         tr("FP Name:"), QLineEdit::Normal, "", &ok);
    if (ok && !strName.isEmpty())
	{
		//name must not contain some junk characters
		if(	strName.indexOf('.') >= 0 ||
			strName.indexOf('(') >= 0 ||
			strName.indexOf(')') >= 0 ||
			strName.indexOf('=') >= 0 ||
			strName.indexOf(',') >= 0 ||
			strName.indexOf('&') >= 0)
		{
			QMessageBox::information(this, "FpAdmin",
				"Please use only basic alphanumeric characters for the name\n(no ',.()=,&' etc.).");
			return;
		}

		//calculate new FP code
		int nCode = 1;
		int nCount = m_def.m_lstFP.size();
		for(int i=0; i<nCount; i++)
			if(m_def.m_lstFP[i].nCode >= nCode)
				nCode = m_def.m_lstFP[i].nCode + 1;

		if(m_def.AddFunctionPoint(strName, QString(), nCode))
		{
			#ifdef _DEBUG
			m_def.AssertValid();
			#endif
			//update model (GUI)
			m_pFPModel->AddNewFP(strName);
		}
		else
			QMessageBox::warning(this, "Error", "FP is already in the list!");
	}
}

void FpAdmin::on_pushNewFPS_clicked()
{
	bool ok;
    QString strName = QInputDialog::getText(this, tr("Input"),
                                         tr("FPS Name:"), QLineEdit::Normal,
                                         "", &ok);
    if (ok && !strName.isEmpty())
	{
		//name must not contain some junk characters
		if( strName.indexOf('.') >= 0 ||
			strName.indexOf('(') >= 0 ||
			strName.indexOf(')') >= 0 ||
			strName.indexOf('=') >= 0 ||
			strName.indexOf(',') >= 0 ||
			strName.indexOf('&') >= 0)
		{
			QMessageBox::information(this, "FpAdmin",
				"Please use only basic alphanumeric characters for the name\n(no ',.()=,&' etc.).");
			return;
		}

		//calculate new FPS code
		int nMax  = 1;
		int nCount = m_def.m_lstFpSets.size();
		for(int i=0; i<nCount; i++)
			if(m_def.m_lstFpSets[i].nCode > nMax)
				nMax = m_def.m_lstFpSets[i].nCode;
		int nCode = nMax+1;

		if(m_def.AddFunctionPointSet(strName, QString(), nCode))
		{
			#ifdef _DEBUG
			m_def.AssertValid();
			#endif

			//update model (GUI)
			m_pFPSModel->AddNewFPSet(strName);
		}
	}
}

void FpAdmin::on_pushDeleteFPS_clicked()
{
	//delete selected FPS or one of its FP leafs
	QModelIndex index = ui.treeFPSets->selectionModel()->currentIndex();
	if(index.isValid()){
		//allow deleting on 1st and 2nd tree level only
		if(index.parent().isValid() && index.parent().parent().isValid()){
			QMessageBox::information(this, "Warning", "You can delete only 1st and 2nd level items!");
			return;
		}

		m_pFPSModel->m_bPropagate = true;
		m_pFPSModel->removeRow(index.row(), index.parent());
		m_def.m_bModified = true;
	}
}

int FpAdmin::GetCurrentFP()
{
	//get selected function point
	QModelIndex index = ui.treeFP->selectionModel()->currentIndex();
	if(!index.isValid())
		return -1;
	return index.row();
}

int FpAdmin::GetCurrentModule()
{
	int nNode = -1;

	//get selected module tree node
	QModelIndex index = ui.treeModules->selectionModel()->currentIndex();
	if(index.isValid())
	{
		//get to the oldest parent node (level 0 being the actual module) 
		while(index.parent().isValid())
			index = index.parent();

		//convert to node index
		QList<int> lstToPos;
		m_pModModel->IndexToList(index, lstToPos);
		nNode = m_def.m_lstModSets.FindNode(lstToPos);
		Q_ASSERT(nNode >= 0);
	}

	return nNode;
}

void FpAdmin::on_cbShowAllFP_stateChanged(int nState)
{
	m_pFPModel->ShowHidden(nState);
}

void FpAdmin::on_pushDeleteFP_clicked()
{
	int nRow = GetCurrentFP();
	if(nRow >= 0)
	{
		//last warning
		int nRes = QMessageBox::question(this, "Warning", 
					"Deleting FP can be dangerous! Proceed?", tr("&Yes"), tr("&No"),
					QString(), 0, 1);
		if(0 == nRes){
			//delete FP
			m_pFPModel->removeRow(nRow);
			m_def.m_lstFP.erase(m_def.m_lstFP.begin() + nRow);	//TOFIX move this into the model?
			m_def.m_bModified = true;
		}
	}
}

void FpAdmin::on_pushActivateFP_clicked()
{
	int nRow = GetCurrentFP();
	if(nRow >= 0){
		m_def.m_lstFP[nRow].bActive = true;	//just save the state
		m_def.m_bModified = true;

		//redraw branch icons
		if(ui.cbShowAllFP->isChecked())
			m_pFPModel->RedrawBranch(ui.treeFP->selectionModel()->currentIndex());
	}
}

void FpAdmin::on_pushDeactivateFP_clicked()
{
	int nRow = GetCurrentFP();
	if(nRow >= 0)
	{
		m_def.m_lstFP[nRow].bActive = false;	//just save the state
		m_def.m_bModified = true;

		if(!ui.cbShowAllFP->isChecked())
			m_pFPModel->ShowRow(nRow, false);	//hide if not "Show all" mode
		else
			m_pFPModel->RedrawBranch(ui.treeFP->selectionModel()->currentIndex());
	}
}

void FpAdmin::OnFPSelectionChange(const QModelIndex &to, const QModelIndex &from)
{
	//store current value into the old current item
	int nValue = ui.lineFPValue->text().toInt();
	int nOldRow = from.row();
	if(nOldRow >= 0)
		m_def.m_lstFP[nOldRow].nValue = nValue;

	//store current description
	QString strDesc = ui.lineFPDescription->toPlainText();
	//convert from multiline to single line - in order to store it in INI file
	strDesc.replace("\r\n", "�");
	strDesc.replace("\n", "�");
	if(nOldRow >= 0)
		m_def.m_lstFP[nOldRow].strDesc = strDesc;

	//set value and check state for the new current index
	nValue  = 0;
	int nNewRow = to.row();
	if(nNewRow >= 0)
		nValue = m_def.m_lstFP[nNewRow].nValue;
	ui.lineFPValue->setText(QString().sprintf("%d", nValue));

	strDesc = "";
	if(nNewRow >= 0){
		strDesc = m_def.m_lstFP[nNewRow].strDesc;

		//convert from single line back to multiline
		strDesc.replace("�", "\r\n");	//TOFIX unix?
	}
	ui.lineFPDescription->setPlainText(strDesc);

	qDebug("FP: Current item changed from %d to %d", nOldRow, nNewRow);
}

void FpAdmin::on_pushActivateFPS_clicked()
{
	QModelIndex index = ui.treeFPSets->selectionModel()->currentIndex();
	if(index.isValid())
	{
		if(index.parent().isValid()){
			QMessageBox::information(this, "Warning", "You can (de)activate only 1st level items!");
			return;
		}

		//row from index
		QList<int> lstPos;
		m_pFPSModel->IndexToList(index, lstPos);
		int nRow = m_def.m_lstFpSets.FindNode(lstPos);
		Q_ASSERT(nRow >= 0);

		m_def.m_lstFpSets[nRow].bActive = true;	//just save the state
		m_def.m_bModified = true;
	
		//redraw branch icons
		if(ui.checkShowAllFPS->isChecked())
			m_pFPSModel->RedrawBranch(index);
	}
}

void FpAdmin::on_pushDeactivateFPS_clicked()
{
	QModelIndex index = ui.treeFPSets->selectionModel()->currentIndex();
	if(index.isValid())
	{
		if(index.parent().isValid()){
			QMessageBox::information(this, "Warning", "You can (de)activate only 1st level items!");
			return;
		}

		//row from index
		QList<int> lstPos;
		m_pFPSModel->IndexToList(index, lstPos);
		int nRow = m_def.m_lstFpSets.FindNode(lstPos);
		Q_ASSERT(nRow >= 0);

		m_def.m_lstFpSets[nRow].bActive = false;	//just save the state
		m_def.m_bModified = true;

		if(!ui.checkShowAllFPS->isChecked())
			m_pFPSModel->ShowRow(index, false);		// hide if not in "show all" mode
		else
			m_pFPSModel->RedrawBranch(index);
	}
}

void FpAdmin::on_checkShowAllFPS_stateChanged(int nState)
{
	m_pFPSModel->ShowHidden(nState);
}

void FpAdmin::OnFPSetSelectionChange(const QModelIndex &to, const QModelIndex &from)
{
	//store current value and description into the old current item
	int nValue = ui.lineFPSValue->text().toInt();
	QString strDesc = ui.lineFPSDescription->toPlainText();
	//convert from multiline to single line - in order to store it in INI file
	strDesc.replace("\r\n", "�");
	strDesc.replace("\n", "�");

	if(from.isValid())
	{
		//convert to node index
		QList<int> lstFromPos;
		m_pFPSModel->IndexToList(from, lstFromPos);
		int nNode = m_def.m_lstFpSets.FindNode(lstFromPos);
		Q_ASSERT(nNode >= 0);

		m_def.m_lstFpSets[nNode].nValue = nValue;
		m_def.m_lstFpSets[nNode].strDesc = strDesc;
	}

	//set value and check state for the new current index
	nValue  = 0;
	strDesc = "";
	if(to.isValid())
	{
		//convert to node index
		QList<int> lstToPos;
		m_pFPSModel->IndexToList(to, lstToPos);
		int nNode = m_def.m_lstFpSets.FindNode(lstToPos);
		Q_ASSERT(nNode >= 0);
	
		//enable value box only for FP nodes
		if(m_def.m_lstFpSets[nNode].nType == ITEM_TYPE_FP) {
			nValue = m_def.m_lstFpSets[nNode].nValue;
			ui.lineFPSValue->setEnabled(true);
			ui.lineFPSValue->setText(QString().sprintf("%d", nValue));
		}
		else {
			ui.lineFPSValue->setEnabled(false);
			ui.lineFPSValue->setText("");
		}

		strDesc = m_def.m_lstFpSets[nNode].strDesc;
		//convert from single line back to multiline
		strDesc.replace("�", "\r\n");	//TOFIX unix?

		ui.lineFPSDescription->setPlainText(strDesc);
	}
	else {
		ui.lineFPSValue->setEnabled(false);
		ui.lineFPSValue->setText("");
		ui.lineFPSDescription->setPlainText("");
	}

	qDebug("FP Set: Current item changed");
}

void FpAdmin::on_pushNewModule_clicked()
{
	newmodule dlg;
	if(dlg.exec())
	{
		int nCode = 0;	//numeric code not relevant for top level module tree

		//add new module into the definition
		if(m_def.AddModule(dlg.m_strCode, dlg.m_strName, QString(), nCode, ITEM_STATUS_ON))
		{
			//refresh GUI
			QString strTitle;
			strTitle.sprintf("%s (%s)", dlg.m_strName.toLatin1().constData(), dlg.m_strCode.toLatin1().constData());

			m_pModModel->AddNewModule(strTitle);
		}
	}
}

void FpAdmin::on_pushDeleteModule_clicked()
{
	//delete selected FPS or one of its FP leafs
	QModelIndex index = ui.treeModules->selectionModel()->currentIndex();
	if(index.isValid())
	{
		//allow deleting on 1st and 2nd tree level only
		if(index.parent().isValid() && index.parent().parent().isValid()){
			QMessageBox::information(this, "Warning", "You can delete only 1st and 2nd level items!");
			return;
		}

		//perform the operation
		m_pModModel->removeRow(index.row(), index.parent());
		m_def.m_bModified = true;
	}
}

void FpAdmin::on_pushActivateModule_clicked()
{
	QModelIndex index = ui.treeModules->selectionModel()->currentIndex();
	if(index.isValid())
	{
		if(index.parent().isValid()){
			QMessageBox::information(this, "Warning", "You can (de)activate only 1st level items!");
			return;
		}

		//row from index
		QList<int> lstPos;
		m_pFPSModel->IndexToList(index, lstPos);
		int nRow = m_def.m_lstModSets.FindNode(lstPos);
		Q_ASSERT(nRow >= 0);

		m_def.m_lstModSets[nRow].bActive = true;	//just save the state
		m_def.m_bModified = true;

		//redraw branch icons
		if(ui.cbShowAllMod->isChecked())
			m_pModModel->RedrawBranch(index);
	}
}

void FpAdmin::on_pushDeactivateModule_clicked()
{
	QModelIndex index = ui.treeModules->selectionModel()->currentIndex();
	if(index.isValid())
	{
		if(index.parent().isValid()){
			QMessageBox::information(this, "Warning", "You can (de)activate only 1st level items!");
			return;
		}

		//row from index
		QList<int> lstPos;
		m_pFPSModel->IndexToList(index, lstPos);
		int nRow = m_def.m_lstFpSets.FindNode(lstPos);
		Q_ASSERT(nRow >= 0);

		m_def.m_lstModSets[nRow].bActive = false;	//just save the state
		if(!ui.cbShowAllMod->isChecked())
			m_pModModel->ShowRow(nRow, false);		//hide if not in "show all" mode
		else
			m_pModModel->RedrawBranch(index);

		m_def.m_bModified = true;
	}
}

void FpAdmin::on_cbShowAllMod_stateChanged(int nState)
{
	m_pModModel->ShowHidden(nState);
}

void FpAdmin::OnModuleFPSelectionChange(const QModelIndex &to, const QModelIndex &from)
{
	//store current value into the old current item
	int nValue = ui.lineMFPValue->text().toInt();
	QString strDesc = ui.lineMFPDescription->toPlainText();
	//convert from multiline to single line - in order to store it in INI file
	strDesc.replace("\r\n", "�");
	strDesc.replace("\n", "�");

	if(from.isValid())
	{
		//convert to node index
		QList<int> lstFromPos;
		m_pModModel->IndexToList(from, lstFromPos);
		//m_def.m_lstModSets.DumpPos(lstFromPos);
		//m_def.m_lstModSets.DumpTree();
		int nNode = m_def.m_lstModSets.FindNode(lstFromPos);
		Q_ASSERT(nNode >= 0);

		m_def.m_lstModSets[nNode].nValue = nValue;
		m_def.m_lstModSets[nNode].strDesc = strDesc;
	}

	//set value and check state for the new current index
	nValue  = 0;
	strDesc	= "";
	if(to.isValid())
	{
		//convert to node index
		QList<int> lstToPos;
		m_pModModel->IndexToList(to, lstToPos);
		int nNode = m_def.m_lstModSets.FindNode(lstToPos);
		Q_ASSERT(nNode >= 0);

		//enable value box only for FP nodes
		if(m_def.m_lstModSets[nNode].nType == ITEM_TYPE_FP)	{
			nValue = m_def.m_lstModSets[nNode].nValue;
			ui.lineMFPValue->setEnabled(true);
			ui.lineMFPValue->setText(QString().sprintf("%d", nValue));
		}
		else {
			ui.lineMFPValue->setEnabled(false);
			ui.lineMFPValue->setText("");
		}

		strDesc = m_def.m_lstModSets[nNode].strDesc;
		//convert from single line back to multiline
		strDesc.replace("�", "\r\n");	//TOFIX unix?

		ui.lineMFPDescription->setPlainText(strDesc);
	}
	else {
		ui.lineMFPValue->setEnabled(false);
		ui.lineMFPValue->setText("");
		ui.lineMFPDescription->setPlainText("");
	}
	
	qDebug("Module tree: Current item changed");
}

void FpAdmin::on_pushCopyModule_clicked()
{
	int nNode = GetCurrentModule();
	if(nNode >= 0)
	{
		//ask for new module name
		newmodule dlg;
		if(dlg.exec())
		{
			//add new module into the definition
			if(m_def.CopyModule(nNode, dlg.m_strCode, dlg.m_strName))
			{
				//refresh GUI
				m_pModModel->RebuildGuiBranch(QModelIndex()/*m_pModModel->GetRoot()*/);
			}
		}
	}
}

void FpAdmin::on_lineFPValue_textChanged(const QString &data)
{
	QModelIndex index = ui.treeFP->selectionModel()->currentIndex();
	if(index.isValid())
	{
		//convert to node index
		QList<int> lstPos;
		m_pFPModel->IndexToList(index, lstPos);
		int nNode = m_def.m_lstFP.FindNode(lstPos);
		Q_ASSERT(nNode >= 0);

		//prepare title
		QString strTitle = m_def.m_lstFP[nNode].strName;
		if(data.toInt() != 0){
			QString strTmp;
			strTmp.sprintf(" (%s)",	data.toLatin1().constBegin());
			strTitle += strTmp;
		}
		m_pFPModel->setData(index, strTitle);

		//store changed value
		m_def.m_lstFP[nNode].nValue = data.toInt();
	}
}

void FpAdmin::on_lineFPSValue_textChanged(const QString &data)
{
	QModelIndex index = ui.treeFPSets->selectionModel()->currentIndex();
	if(index.isValid())
	{
		//convert to node index
		QList<int> lstPos;
		m_pFPSModel->IndexToList(index, lstPos);
		int nNode = m_def.m_lstFpSets.FindNode(lstPos);
		Q_ASSERT(nNode >= 0);

		//prepare title
		QString strTitle = m_def.m_lstFpSets[nNode].strName;
		if(data.toInt() != 0){
			QString strTmp;
			strTmp.sprintf(" (%s)",	data.toLatin1().constBegin());
			strTitle += strTmp;
		}
		m_pFPSModel->setData(index, strTitle);

		//store changed value
		m_def.m_lstFpSets[nNode].nValue = data.toInt();
	}
}

void FpAdmin::on_lineMFPValue_textChanged(const QString &data)
{
	QModelIndex index = ui.treeModules->selectionModel()->currentIndex();
	if(index.isValid())
	{
		//convert to node index
		QList<int> lstPos;
		m_pModModel->IndexToList(index, lstPos);
		int nNode = m_def.m_lstModSets.FindNode(lstPos);
		Q_ASSERT(nNode >= 0);

		//prepare title
		QString strTitle = m_def.m_lstModSets[nNode].strName;
		if(!m_def.m_lstModSets[nNode].strCode.isEmpty()){
			strTitle += " (";
			strTitle += m_def.m_lstModSets[nNode].strCode;
			strTitle += ")";
		}
		if(data.toInt() != 0){
			QString strTmp;
			strTmp.sprintf(" (%s)",	data.toLatin1().constBegin());
			strTitle += strTmp;
		}
		m_pModModel->setData(index, strTitle);

		//store changed value
		m_def.m_lstModSets[nNode].nValue = data.toInt();
	}
}

//generate C++ headers for all modules in a single file
void FpAdmin::OnGenerateHeaders()
{
	//calculate default directory for the file open dialog
	QString strStartDir;
	if(m_strDir.isEmpty()){
		strStartDir = QCoreApplication::applicationDirPath();
		QDir dir(strStartDir);
		if (dir.cd("data"))
			strStartDir = dir.absolutePath();
	}
	else
		strStartDir = m_strDir;

	//ask output file name (*.h)
	QString str = QFileDialog::getSaveFileName(
            NULL,
            "Choose a file",
            strStartDir + "fp_default.h",
            "C++ Header File (*.h)");
	if(str.isEmpty())
		return;

	//
	// generate header file with all FP codes defined
	//
	int nModCount;

	//ensure file extension
	if(!str.endsWith(".h"))
		str += ".h";

	QString strHdr = str;

	FILE *pOutHdr = fopen(str.toLatin1().constData(), "w");
	if(pOutHdr)
	{
		//write info on the generated file
		QString strTime = QDateTime::currentDateTime().toString("yyyy.MM.dd. hh:mm:ss");
		fprintf(pOutHdr, "//\n// WARNING: This is a computer generated file. Do not edit!\n//\tGenerated from: %s\n//\tCreated on:     %s\n//\n\n", m_def.m_strFile.toLatin1().constBegin(), strTime.toLatin1().constBegin());

		fprintf(pOutHdr, "#ifndef FP_CODES_H__\n#define FP_CODES_H__\n\n");

		//
		// write Modules code definition list
		//
		fprintf(pOutHdr, "//\n// Global list of all Modules (Mod)\n//\n\n");
		int nIdx = 0;
		int nCount = m_def.m_lstModSets.size();
		for(int i=0; i<nCount; i++)
		{
			if(0 == m_def.m_lstModSets[i].nLevel)
			{
				nIdx ++;

				//write module rights as a C++ array
				QString strModule = CreateSymbolName(m_def.m_lstModSets[i].strName);

				QString strLine;
				strLine.sprintf("#define APP_MODULE_%s", strModule.toLatin1().constData());
				strLine += "\t"; //= strLine.leftJustified(50, ' ');
				strLine += QString().sprintf("%d",nIdx);

				fprintf(pOutHdr, "%s\n", strLine.toLatin1().constData());
			}
		}

		fprintf(pOutHdr, "\n//\n// Global list of all Module codes (Mod)\n//\n\n");

		for(int i=0; i<nCount; i++)
		{
			if(0 == m_def.m_lstModSets[i].nLevel)
			{
				//write module rights as a C++ array
				QString strModule = CreateSymbolName(m_def.m_lstModSets[i].strName);

				QString strLine;
				strLine.sprintf("#define APP_MODULE_NAME_%s", strModule.toLatin1().constData());
				strLine  += "\t"; // = strLine.leftJustified(60, ' ');
				//strLine += QString().sprintf("%d",nIdx);
				strLine += "\"";
				strLine += m_def.m_lstModSets[i].strCode;
				strLine += "\"";

				fprintf(pOutHdr, "%s\n", strLine.toLatin1().constData());
			}
		}

		nModCount = nIdx;	//store module count
		fprintf(pOutHdr, "\n#define COUNT_APP_MODULES\t%d\n\n", nModCount);

		//
		// write FP code definition list
		//
		int nMaxID = 0;

		fprintf(pOutHdr, "\n//\n// Global list of all Function Points (FP)\n//\n\n");
		nCount = m_def.m_lstFP.size();
		for(int i=0; i<nCount; i++)
		{
			QString strName = CreateSymbolName(m_def.m_lstFP[i].strName);

			QString strLine;
			strLine.sprintf("#define FP_%s", strName.toLatin1().constData());
			strLine += "\t"; //= strLine.leftJustified(50, ' ');
			strLine += QString().sprintf("%d", m_def.m_lstFP[i].nCode /*i+1*/);

			fprintf(pOutHdr, "%s\n", strLine.toLatin1().constData());

			if(m_def.m_lstFP[i].nCode > nMaxID)
				nMaxID = m_def.m_lstFP[i].nCode;
		}

		fprintf(pOutHdr, "\n#define COUNT_FPS\t%d\n", nCount);
		fprintf(pOutHdr, "#define MAX_FPS_ID\t%d\n", nMaxID);

		//
		// write function declaration
		//
		fprintf(pOutHdr, "\n\n//methods to help getting default Access Rights for a FP inside some Module\n");
		fprintf(pOutHdr, "int  ARDefault_GetModuleCode(const char *);\n");
		fprintf(pOutHdr, "bool ARDefault_CanUseFP(int nMod, int nFP, int &nValue);\n\n");
		
		fprintf(pOutHdr, "\n#endif //FP_CODES_H__\n");

		fclose(pOutHdr);
	}
	else{
		QMessageBox::information(this, "Warning", "Failed to open file for writing!");
		return;
	}

	//
	// generate .cpp file with default access rights for all modules defined
	//

	//ensure file extension
	str.remove(str.size()-2,2);	//remove ".h"
	str += ".cpp";

	FILE *pOutCpp = fopen(str.toLatin1().constData(), "w");
	if(pOutCpp)
	{
		//write info on the generated file
		QString strTime = QDateTime::currentDateTime().toString("yyyy.MM.dd. hh:mm:ss");
		fprintf(pOutCpp, "//\n// WARNING: This is a computer generated file. Do not edit!\n//\tGenerated from: %s\n//\tCreated on:     %s\n//\n\n", m_def.m_strFile.toLatin1().constBegin(), strTime.toLatin1().constBegin());

		//write header part
		QFileInfo info(strHdr);
		fprintf(pOutCpp, "#include \"%s\"\n",info.fileName().toLatin1().constData());
		fprintf(pOutCpp, "#include <string.h>\n\n");

		fprintf(pOutCpp, "\nint FindEntry(int nFP);\n\n");

		fprintf(pOutCpp, "//\n// Array with module info\n//\ntypedef struct {\n\tint nCode;\n\tconst char *szCode;\n} MOD_INFO;\n\nMOD_INFO g_arModInfo[COUNT_APP_MODULES] =\n{\n\n");

		//for each module defined
		int nSibling = 0;
		int nNode = m_def.m_lstModSets.FindChild(-1, nSibling);
		while(nNode >= 0)
		{
			//write module rights as a C++ array
			QString strModule = CreateSymbolName(m_def.m_lstModSets[nNode].strName);
			//strModule = strModule.leftJustified(50, ' ');

			fprintf(pOutCpp, "{APP_MODULE_%s, \"%s\"},\n", strModule.toLatin1().constData(), m_def.m_lstModSets[nNode].strCode.toLatin1().constData());

			//proceed to next module
			nSibling ++;
			nNode = m_def.m_lstModSets.FindChild(-1, nSibling);
		}
		
		fprintf(pOutCpp, "\n};\n\n");
		
		fprintf(pOutCpp, "//\n// Array with Default Access Rights (per FP)\n//\n");
		fprintf(pOutCpp, "//\n// NOTE: Array fields schema:\n//\t{code, can_use, value}\n//\n\n");
		fprintf(pOutCpp, "typedef struct {\n\tbool bCanUse;\n\tint nValue;\n} FP_MOD_STATE;\n\n");

		fprintf(pOutCpp, "typedef struct {\n\tint nCode;\n\tFP_MOD_STATE arState[COUNT_APP_MODULES];\n} FP_ITEM1;\n\nFP_ITEM1 g_arAccessRights[]=\n{\n\n");

		//for each module defined, generate flat list
		TreeList *pFlatLists = new TreeList[nModCount];
		nSibling = 0;
		nNode = m_def.m_lstModSets.FindChild(-1, nSibling);
		while(nNode >= 0)
		{
			//generate flat list from module access rights 
			//(push only FP lines, not FPS lines)
			int nLevel = m_def.m_lstModSets[nNode].nLevel;
			int nIdx = nNode;
			do{
				if(m_def.m_lstModSets[nIdx].nType == ITEM_TYPE_FP)
				{
					FP_ITEM item = m_def.m_lstModSets[nIdx];

					//if necessary, resolve inherited status by looking into the FP list in FPDF
					if(item.nStatus == ITEM_STATUS_INHERITED){
						//search the same item in the FP list
						int nIdxFP = m_def.m_lstFP.FindByCode(item.nCode);
						//item.nStatus = m_def.m_lstFP[item.nCode-1].nStatus;
						item.nStatus = m_def.m_lstFP[nIdxFP].nStatus;
					}

					//push into list
					pFlatLists[nSibling].push_back(item);
				}
				nIdx ++;
			}while(nIdx<m_def.m_lstModSets.size() && m_def.m_lstModSets[nIdx].nLevel > nLevel);

			//remove duplicates (prefer active FP instances)
			pFlatLists[nSibling].Sort();
			nIdx = 0;
			while(nIdx < pFlatLists[nSibling].size()-1)
			{
				if( pFlatLists[nSibling][nIdx].strName == pFlatLists[nSibling][nIdx+1].strName &&
					pFlatLists[nSibling][nIdx].nType == pFlatLists[nSibling][nIdx+1].nType)
				{
					pFlatLists[nSibling].removeAt(nIdx);
				}
				else
					nIdx++;
			}

			//proceed to next module
			nSibling ++;
			nNode = m_def.m_lstModSets.FindChild(-1, nSibling);
		}

		//
		// for each known FP in the system
		//
		int nCount = m_def.m_lstFP.size();
		for(int i=0; i<nCount; i++)
		{
			//calculate code's #define name
			QString strFpCode = "FP_";
			strFpCode += m_def.m_lstFP[i].strName;
			strFpCode  = CreateSymbolName(strFpCode);
			//strFpCode = strFpCode.leftJustified(30, ' ');

			fprintf(pOutCpp, "{%s,{", strFpCode.toLatin1().constData());

			// write sub-array with AR for each module
			for(int k=0; k<nModCount; k++)
			{
				int nFlatCnt = pFlatLists[k].size();

				//search if FP is in the flat list
				int nPos = -1;
				for(int j=0; j<nFlatCnt; j++)
				{
					if(pFlatLists[k][j].nCode == i+1){
						nPos = j;
						break;
					}
				}

				if(k > 0)
					fprintf(pOutCpp, ",");

				if(nPos < 0)	//not found in flat FP list
				{
					fprintf(pOutCpp, "{0, 0}");
				}
				else
				{
					fprintf(pOutCpp, "{%d, %d}", 
						(pFlatLists[k][nPos].nStatus == ITEM_STATUS_ON)? 1 : 0,
						(pFlatLists[k][nPos].nStatus == ITEM_STATUS_ON)? pFlatLists[k][nPos].nValue : 0);
				}
			}

			fprintf(pOutCpp, "}},\n");	//terminate the line
		}

		fprintf(pOutCpp, "\n};\n");

		//write some static code
		fprintf(pOutCpp, "\n\nint ARDefault_GetModuleCode(const char *szCode)\n{\n");
		fprintf(pOutCpp, "\tstatic int nCnt = sizeof(g_arModInfo)/sizeof(g_arModInfo[0]);\n");
		fprintf(pOutCpp, "\tfor(int i=0; i<nCnt; i++)\n");
		fprintf(pOutCpp, "\t\tif(0 == strcmp(g_arModInfo[0].szCode, szCode))\n");
		fprintf(pOutCpp, "\t\t\treturn i+1;\n\treturn -1;\n}\n");

		fprintf(pOutCpp, "\n\nbool ARDefault_CanUseFP(int nMod, int nFP, int &nValue)\n{\n");
		fprintf(pOutCpp, "\tint nFPIdx = FindEntry(nFP);\n\tif(nFPIdx >= 0){\n");
		fprintf(pOutCpp, "\t\tnValue = g_arAccessRights[nFPIdx].arState[nMod-1].nValue;\n");
		fprintf(pOutCpp, "\t\treturn g_arAccessRights[nFPIdx].arState[nMod-1].bCanUse;\n\t}\n");
		fprintf(pOutCpp, "\treturn false;\n}\n");

		fprintf(pOutCpp, "\n\nint FindEntry(int nFP)\n{\n");
		fprintf(pOutCpp, "\tint nMax = sizeof(g_arAccessRights)/sizeof(g_arAccessRights[0]);\n");
		fprintf(pOutCpp, "\tint nIdx = -1;\n");
		fprintf(pOutCpp, "\tfor(int i=0; i<nMax; i++){\n");
		fprintf(pOutCpp, "\t\tif(g_arAccessRights[i].nCode == nFP){\n");
		fprintf(pOutCpp, "\t\t\tnIdx = i;\n");
		fprintf(pOutCpp, "\t\t\tbreak;\n");
		fprintf(pOutCpp, "\t\t}\n\t}\n");
		fprintf(pOutCpp, "\treturn nIdx;\n}\n");

		fclose(pOutCpp);

		delete [] pFlatLists;
	}
	else
		QMessageBox::information(this, "Warning", "Failed to open .cpp file for writing!");
}

void FpAdmin::on_lineFPDescription_textChanged()
{
	QModelIndex index = ui.treeFP->selectionModel()->currentIndex();
	if(index.isValid())
	{
		//convert to node index
		QList<int> lstPos;
		m_pFPModel->IndexToList(index, lstPos);
		int nNode = m_def.m_lstFP.FindNode(lstPos);
		Q_ASSERT(nNode >= 0);

		//convert from multiline to single line - in order to store it in INI file
		QString data = ui.lineFPDescription->toPlainText();
		QString strDesc(data);
		strDesc.replace("\r\n", "�");
		strDesc.replace("\n", "�");

		//store changed value
		m_def.m_lstFP[nNode].strDesc = strDesc;
	}
}

void FpAdmin::on_lineFPSDescription_textChanged()
{
	QModelIndex index = ui.treeFPSets->selectionModel()->currentIndex();
	if(index.isValid())
	{
		//convert to node index
		QList<int> lstPos;
		m_pFPSModel->IndexToList(index, lstPos);
		int nNode = m_def.m_lstFpSets.FindNode(lstPos);
		Q_ASSERT(nNode >= 0);

		//convert from multiline to single line - in order to store it in INI file
		QString data = ui.lineFPSDescription->toPlainText();
		QString strDesc(data);
		strDesc.replace("\r\n", "�");
		strDesc.replace("\n", "�");

		//store changed value
		m_def.m_lstFpSets[nNode].strDesc = strDesc;
	}
}

void FpAdmin::on_lineMFPDescription_textChanged()
{
	QModelIndex index = ui.treeModules->selectionModel()->currentIndex();
	if(index.isValid())
	{
		//convert to node index
		QList<int> lstPos;
		m_pModModel->IndexToList(index, lstPos);
		int nNode = m_def.m_lstModSets.FindNode(lstPos);
		Q_ASSERT(nNode >= 0);

		//convert from multiline to single line - in order to store it in INI file
		QString data = ui.lineMFPDescription->toPlainText();
		QString strDesc(data);
		strDesc.replace("\r\n", "�");
		strDesc.replace("\n", "�");

		//store changed value
		m_def.m_lstModSets[nNode].strDesc = strDesc;
	}
}

QString CreateSymbolName(QString strInput)
{
	strInput = strInput.toUpper();
	strInput.replace(" - ", "_");
	strInput.replace(' ', '_');
	strInput.replace(':', '_');
	strInput.replace('-', '_');
	strInput.replace('\'', '_');

	return strInput;
}
