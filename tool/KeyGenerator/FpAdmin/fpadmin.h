#ifndef FPADMIN_H
#define FPADMIN_H

#include <QMainWindow>
#include "ui_fpadmin.h"
#include "bus_core/bus_core/fpdf.h"
#include "qfptreemodel.h"
#include "qfpstreemodel.h"
#include "qmodtreemodel.h"
#include <QTreeWidgetItem>

class FpAdmin : public QMainWindow
{
    Q_OBJECT

public:
    FpAdmin(QWidget *parent = 0, Qt::WindowFlags flags = 0);
    ~FpAdmin();

	void ShowData();
	int  GetCurrentFP();
	int  GetCurrentModule();
	void LoadFpdfFile(QString strFile = QString());

protected:
	FPDF m_def;						//data definition storage	
	QFPTreeModel	*m_pFPModel;	//tree model storing FP list
	QFPSTreeModel	*m_pFPSModel;	//tree model storing FP sets list
	QModTreeModel	*m_pModModel;	//tree model storing FP sets for a single model
	QString			m_strDir;		//remember last opened directory

public slots:
	void OnFileOpen();
	void OnFileSave();
	void OnGenerateHeaders();

private:
    Ui::FpAdminClass ui;

private slots:
	void on_lineMFPDescription_textChanged();
	void on_lineFPSDescription_textChanged();
	void on_lineFPDescription_textChanged();
	void on_lineMFPValue_textChanged(const QString &);
	void on_lineFPSValue_textChanged(const QString &);
	void on_lineFPValue_textChanged(const QString &);
	void on_pushCopyModule_clicked();
	void on_checkShowAllFPS_stateChanged(int);
	void on_pushDeactivateFPS_clicked();
	void on_pushActivateFPS_clicked();
	void on_cbShowAllMod_stateChanged(int);
	void on_pushDeactivateModule_clicked();
	void on_pushActivateModule_clicked();
	void on_pushDeactivateFP_clicked();
	void on_pushActivateFP_clicked();
	void on_pushDeleteFP_clicked();
	void on_cbShowAllFP_stateChanged(int);
	void on_pushDeleteFPS_clicked();
	void on_pushNewFPS_clicked();
	void on_pushNewFP_clicked();
	void on_pushDeleteModule_clicked();
	void on_pushNewModule_clicked();
	void OnFPSelectionChange(const QModelIndex&, const QModelIndex&);
	void OnModuleFPSelectionChange(const QModelIndex&, const QModelIndex&);
	void OnFPSetSelectionChange(const QModelIndex &, const QModelIndex &);
};

#endif // FPADMIN_H
