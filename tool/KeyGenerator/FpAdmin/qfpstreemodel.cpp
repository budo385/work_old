/****************************************************************************
**
** Copyright (C) 2005-2006 Trolltech AS. All rights reserved.
**
** This file is part of the example classes of the Qt Toolkit.
**
** Licensees holding a valid Qt License Agreement may use this file in
** accordance with the rights, responsibilities and obligations
** contained therein.  Please consult your licensing agreement or
** contact sales@trolltech.com if any conditions of this licensing
** agreement are not clear to you.
**
** Further information about Qt licensing is available at:
** http://www.trolltech.com/products/qt/licensing.html or by
** contacting info@trolltech.com.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

/*
    QFPSTreeModel.cpp

    Provides a simple tree model to show how to create and use hierarchical
    models.
*/

#include <QtGui>
#include "treeitem.h"
#include "qfpstreemodel.h"
#include "bus_core/bus_core/fpsets.h"
#include <QTreeView>

QFPSTreeModel::QFPSTreeModel(QObject *parent)
    : QAbstractItemModel(parent)
{
	m_pData = NULL;
	m_pWidget = NULL;
	m_pFPWidget = NULL;
	m_bSelfDrop = false;
	m_pModModel = NULL;

	m_bNodeAdded = false;
	m_bPropagate = false;

    QList<QVariant> rootData;
    rootData << "Name";
    rootItem = new TreeItem(rootData);
}

QFPSTreeModel::~QFPSTreeModel()
{
    delete rootItem;
}

int QFPSTreeModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return static_cast<TreeItem*>(parent.internalPointer())->columnCount();
    else
        return rootItem->columnCount();
}

QVariant QFPSTreeModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

	//find node data
	QList<int> lstPos;
	IndexToList(index, lstPos);
	int nNode = m_pData->m_lstFpSets.FindNode(lstPos);
	//Q_ASSERT(nNode >= 0);
	if(nNode < 0)
		return QVariant();

	TreeItem *item = static_cast<TreeItem*>(index.internalPointer());
	if (role == Qt::CheckStateRole)
	{
		//all nodes except FPS are checkable
		if(ITEM_TYPE_FPS == m_pData->m_lstFpSets[nNode].nType)
			return QVariant();

		return QVariant(item->nCheckState);	//return node checked state
	}
	else if (role == Qt::DecorationRole)
	{
		//calculate icon based on the type (FP/PFS)
		//is node hidden
		bool bHidden = false;
		int nIdx = nNode;
		while(nIdx >= 0)
		{
			//check if the node is hidden
			if(!m_pData->m_lstFpSets[nIdx].bActive){
				bHidden = true;
				break;
			}
			//proceed to parent level (check if the parent is hidden)
			nIdx = m_pData->m_lstFpSets.FindParent(nIdx);
		}

		QIcon icon;
		if(bHidden)
			icon.addFile(":inactive.png");
		else if(ITEM_TYPE_FPS == m_pData->m_lstFpSets[nNode].nType)
			icon.addFile(":fps.png");
		else
			icon.addFile(":fp.png");
		return QVariant(icon);	//return node's icon
	}
	else if (role == Qt::ToolTipRole || role == Qt::StatusTipRole)
	{
		//support to view long entries as tooltips and status bar entries
		return QVariant(m_pData->m_lstFpSets[nNode].strDesc);
	}
    if (role != Qt::DisplayRole)
        return QVariant();

    return item->data(index.column());
}

Qt::ItemFlags QFPSTreeModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::ItemIsEnabled;

	Qt::ItemFlags flags = Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsUserCheckable | Qt::ItemIsTristate; 

	//only top level items can be drag sources and drop targets
	if(!index.parent().isValid())
		flags |= Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled;

	return flags;
}

QVariant QFPSTreeModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
        return rootItem->data(section);

    return QVariant();
}

QModelIndex QFPSTreeModel::index(int row, int column, const QModelIndex &parent) const
{
    TreeItem *parentItem;

    if (!parent.isValid())
        parentItem = rootItem;
    else
        parentItem = static_cast<TreeItem*>(parent.internalPointer());

    TreeItem *childItem = parentItem->child(row);
    if (childItem)
        return createIndex(row, column, childItem);
    else
        return QModelIndex();
}

QModelIndex QFPSTreeModel::parent(const QModelIndex &index) const
{
    if (!index.isValid())
        return QModelIndex();

    TreeItem *childItem = static_cast<TreeItem*>(index.internalPointer());
    TreeItem *parentItem = childItem->parent();

    if (parentItem == rootItem)
        return QModelIndex();

    return createIndex(parentItem->row(), 0, parentItem);
}

int QFPSTreeModel::rowCount(const QModelIndex &parent) const
{
    TreeItem *parentItem;

    if (!parent.isValid())
        parentItem = rootItem;
    else
        parentItem = static_cast<TreeItem*>(parent.internalPointer());

    return parentItem->childCount();
}

bool QFPSTreeModel::insertRows(int row, int count, const QModelIndex &parent)
{
	TreeItem *item = static_cast<TreeItem*>(parent.internalPointer());
	if(NULL == item)
		item = rootItem;
	
	int first = item->childCount();
	beginInsertRows(parent, first, first+count-1);

	//add X children into the parent node
	QList<QVariant> columnData;
	columnData << "New item";	//temporary name for all new child nodes
	for(int i=0; i<count; i++)
		item->appendChild(new TreeItem(columnData, item));

	endInsertRows();

	m_bNodeAdded = true;
	return true;
}

bool QFPSTreeModel::removeRows(int row, int count, const QModelIndex &parent)
{
	TreeItem *item = static_cast<TreeItem*>(parent.internalPointer());
	if(NULL == item)
		item = rootItem;

	//store parent branch info (list of indexes)
	QList<int> lstPos;
	IndexToList(parent, lstPos);

	//remove X children from the parent node
	beginRemoveRows(parent, row, row+count-1);
	item->RemoveChildren(row, count);
	endRemoveRows();
	
	//
	// refresh internal model
	//

	//parent node can be -1 (root)
	int nNode = m_pData->m_lstFpSets.FindNode(lstPos);

	int nChildren = m_pData->m_lstFpSets.GetChildCount(nNode);
	Q_ASSERT(row+count-1 < nChildren);

	for(int i=row+count-1; i>=row; i--){
		int nChild = m_pData->m_lstFpSets.FindChild(nNode, i);
		m_pData->m_lstFpSets.RemoveNode(nChild);
		if(nChild < nNode)
			nNode --;
	}

	//
	// propagate FPS change (rows deleted) to all clones of this FPS  (including the ones in Module tree)
	//
	int nParent = nNode;
	if( m_bPropagate &&
		nParent >= 0 && 
		m_pData->m_lstFpSets[nParent].nType  == ITEM_TYPE_FPS &&
		m_pData->m_lstFpSets[nParent].nLevel == 0) //root FPS only
	{
		m_bPropagate = false;

		int nCode = m_pData->m_lstFpSets[nParent].nCode;
		int nCount = m_pData->m_lstFpSets.size();
		for(int i=0; i<nCount; i++)
		{
			//if FPS clone found in the tree
			if( i != nParent && 
				m_pData->m_lstFpSets[i].nType == ITEM_TYPE_FPS &&
				m_pData->m_lstFpSets[i].nCode == nCode)
			{
				//remove same rows in a cloned FPS
				QList<int> lstPos;
				m_pData->m_lstFpSets.GetNodePos(i, lstPos);
				QModelIndex index = ListToIndex(lstPos);
				
				nChildren = m_pData->m_lstFpSets.GetChildCount(i);
				if(row+count-1 < nChildren){
					removeRows(row, count, index);
					
					//refresh parent index after each delete
					IndexToList(parent, lstPos);
					nParent = m_pData->m_lstFpSets.FindNode(lstPos);
				}

				//adapt to changes
				nCount = m_pData->m_lstFpSets.size();
			}
		}

		//propagate the same change into the modules tree
		nCount = m_pData->m_lstModSets.size();
		for(int i=0; i<nCount; i++)
		{
			//if FPS clone found in the tree
			if( m_pData->m_lstModSets[i].nType == ITEM_TYPE_FPS &&
				m_pData->m_lstModSets[i].nCode == nCode)
			{
				//remove same rows in a cloned FPS
				QList<int> lstPos;
				m_pData->m_lstModSets.GetNodePos(i, lstPos);
				QModelIndex index = m_pModModel->ListToIndex(lstPos);
				
				nChildren = m_pData->m_lstModSets.GetChildCount(i);
				if(row+count-1 < nChildren)
					m_pModModel->removeRows(row, count, index);

				//adapt to changes
				nCount = m_pData->m_lstModSets.size();
			}
		}
	}

	m_bPropagate = false;
	//
	// End of propagation code
	//

	return true;
}

bool QFPSTreeModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
	TreeItem *item = static_cast<TreeItem*>(index.internalPointer());
	if(NULL != item)
	{
		Q_ASSERT(0 == index.column());

		if(Qt::EditRole == role || Qt::DisplayRole == role)
		{
			qDebug("Set new node title:%s\n", value.toString().toLatin1().constData());
			item->SetData(index.column(), value);	//store title
		}
		else if (Qt::CheckStateRole == role)
		{
			//add 3state support
			if(item->nCheckState == 2)
				item->nCheckState = 1;		//store check state
			else
				item->nCheckState = value.toInt();		//store check state
			
			//update internal data
			QList<int> lstPos;
			IndexToList(index, lstPos);
			int nNode = m_pData->m_lstFpSets.FindNode(lstPos);
			Q_ASSERT(nNode >= 0);
			m_pData->m_lstFpSets[nNode].nStatus = (item->nCheckState == 0)? ITEM_STATUS_OFF : (item->nCheckState == 2)? ITEM_STATUS_ON : ITEM_STATUS_INHERITED;

			emit dataChanged(index, index);	//force item redraw
			return true;
		}

		emit dataChanged(index, index);	//force item redraw

		//fix our internal FPDF storage
		if(m_bNodeAdded)
		{
			m_bNodeAdded = false;
			m_bPropagate = true;

			//TOFIX not always the root
			if(!index.parent().isValid())
			{
				//calculate new FPS code
				int nCode = 1;
				int nCount = m_pData->m_lstFpSets.size();
				for(int i=0; i<nCount; i++)
					if(m_pData->m_lstFpSets[i].nLevel == 0)
						nCode ++;

				//case when adding new point set
				m_pData->AddFunctionPointSet(value.toString(), QString(), nCode);
			}
			else 
			{
				//case when dropping new FP to the FPS
				QList<int> lstPos;
				IndexToList(index, lstPos);
				//m_pData->m_lstFpSets.DumpPos(lstPos);
				lstPos.pop_back();	//calc parent
				int nParent = m_pData->m_lstFpSets.FindNode(lstPos);

				if(m_bSelfDrop)
				{
					//dropped from our own tree
					//FPS droped onto another FPS - copy entire branch

					QModelIndex idxSrc = ((QTreeView *)m_pWidget)->selectionModel()->currentIndex();
					if(idxSrc.isValid())
					{
						QList<int> lstPos;
						IndexToList(idxSrc, lstPos);
						int nNodeSrc = m_pData->m_lstFpSets.FindNode(lstPos);
						int nNodeDst = nParent;
						if(!m_pData->m_lstFpSets.CopyNode(nNodeSrc, nNodeDst))
							return false;

						//generate new model indexes recursively / refresh tree layout
						RebuildGuiBranch(index);

						//
						// propagate FPS change (new FPS sub-branch) to all clones of this FPS  (including the ones in Module tree)
						//
						if(m_bPropagate)
						{
							m_bPropagate = false;

							int nChildren  = m_pData->m_lstFpSets.GetChildCount(nParent);
							int nSrcNode   = m_pData->m_lstFpSets.FindChild(nParent, nChildren-1);
							int nCopyTotal = m_pData->m_lstFpSets.GetChildCountRecursive(nSrcNode);

							int nCode = m_pData->m_lstFpSets[nParent].nCode;
							int nCount = m_pData->m_lstFpSets.size();
							for(int i=0; i<nCount; i++)
							{
								//if FPS clone found in the tree
								if( i != nParent && 
									m_pData->m_lstFpSets[i].nType == ITEM_TYPE_FPS &&
									m_pData->m_lstFpSets[i].nCode == nCode)
								{
									//copy the node data
									bool bRes = m_pData->m_lstFpSets.CopyNode(nSrcNode, i);
									Q_ASSERT(bRes);

									//refresh model
									QList<int> lstPos;
									m_pData->m_lstFpSets.GetNodePos(i, lstPos);
									QModelIndex parent = ListToIndex(lstPos);
									RebuildGuiBranch(parent);
									m_bNodeAdded = false;
									
									//adapt to changes
									nCount = m_pData->m_lstFpSets.size();
									if(i < nParent)
										nParent += nCopyTotal;
									if(i < nSrcNode)
										nSrcNode += nCopyTotal;
								}
							}

							//propagate the same change into the modules tree
							nCount = m_pData->m_lstModSets.size();
							for(int i=0; i<nCount; i++)
							{
								//if FPS clone found in the tree
								if( m_pData->m_lstModSets[i].nType == ITEM_TYPE_FPS &&
									m_pData->m_lstModSets[i].nCode == nCode)
								{
									//copy the node data
									if(!m_pData->m_lstModSets.CopyNode(m_pData->m_lstFpSets, nSrcNode, i))
										return false;

									//refresh model
									QList<int> lstPos;
									m_pData->m_lstModSets.GetNodePos(i, lstPos);
									QModelIndex parent = m_pModModel->ListToIndex(lstPos);
									m_pModModel->RebuildGuiBranch(parent);
									m_bNodeAdded = false;

									//adapt to changes
									nCount += nCopyTotal;
									if(i <= nParent)
										i += nCopyTotal;
								}
							}
						}
						//
						// End of propagation code
						//

					}

					m_bSelfDrop = false;
				}
				else
				{
					//dropped from FP tree (list)

					QString strName;
					QString strDesc;
					bool bActive = true;
					int nValue = 0;
					int nState = ITEM_STATUS_ON;
					int nCode = 0;

					//copy original internal name (without "(11)" suffix)
					QModelIndex idxSrc = ((QTreeView *)m_pFPWidget)->selectionModel()->currentIndex();
					if(idxSrc.isValid()){
						strName = m_pData->m_lstFP[idxSrc.row()].strName;
						nValue  = m_pData->m_lstFP[idxSrc.row()].nValue;
						nState  = m_pData->m_lstFP[idxSrc.row()].nStatus;
						bActive  = m_pData->m_lstFP[idxSrc.row()].bActive;
						strDesc  = m_pData->m_lstFP[idxSrc.row()].strDesc;
						nCode  = m_pData->m_lstFP[idxSrc.row()].nCode;
					}
					else{
						strName = value.toString();
						nCode	= idxSrc.row() + 1;
					}

					//fix title
					item->SetData(index.column(), QVariant(strName));	//store title

					FP_ITEM item;
					item.strName  = strName;
					item.bActive  = bActive ;			//copy active state from src
					item.nType	  = ITEM_TYPE_FP;
					item.nValue	  = nValue;				//copy value from source
					item.nStatus  = nState;				//copy state from source
					item.nLevel	  = 0;					//will be filled by AddChild
					item.nCode	  = nCode;				//calculate code from index
					item.strDesc  = strDesc;

					m_pData->m_lstFpSets.AddChild(nParent, item);

					//
					// propagate FPS change (new FP node) to all clones of this FPS  (including the ones in Module tree)
					//
					if(m_bPropagate)
					{
						m_bPropagate = false;

						int nChildren = m_pData->m_lstFpSets.GetChildCount(nParent);
						int nSrcNode  = m_pData->m_lstFpSets.FindChild(nParent, nChildren-1);

						int nCode = m_pData->m_lstFpSets[nParent].nCode;
						int nCount = m_pData->m_lstFpSets.size();
						for(int i=0; i<nCount; i++)
						{
							//if FPS clone found in the tree
							if( i != nParent && 
								m_pData->m_lstFpSets[i].nType == ITEM_TYPE_FPS &&
								m_pData->m_lstFpSets[i].nCode == nCode)
							{
								//copy the node data
								if(!m_pData->m_lstFpSets.CopyNode(nSrcNode, i))
									return false;

								//refresh model
								QList<int> lstPos;
								m_pData->m_lstFpSets.GetNodePos(i, lstPos);
								QModelIndex parent = ListToIndex(lstPos);
								RebuildGuiBranch(parent);

								//adapt to changes
								nCount ++;
								if(i < nSrcNode)
									nSrcNode ++;
								if(i < nParent)
									nParent ++;
							}
						}

						//propagate the same change into the modules tree
						nCount = m_pData->m_lstModSets.size();
						for(int i=0; i<nCount; i++)
						{
							//if FPS clone found in the tree
							if( m_pData->m_lstModSets[i].nType == ITEM_TYPE_FPS &&
								m_pData->m_lstModSets[i].nCode == nCode)
							{
								//copy the node data
								if(!m_pData->m_lstModSets.CopyNode(m_pData->m_lstFpSets, nSrcNode, i))
									return false;

								//refresh model
								QList<int> lstPos;
								m_pData->m_lstModSets.GetNodePos(i, lstPos);
								QModelIndex parent = m_pModModel->ListToIndex(lstPos);
								m_pModModel->RebuildGuiBranch(parent);
								m_bNodeAdded = false;

								//adapt to changes
								nCount ++;
							}
						} //mod tree propagation
					}
					//
					// End of propagation code
					//
				}
			}
		}
		return true;
	}
	Q_ASSERT(false);
	return false;
}

Qt::DropActions QFPSTreeModel::supportedDropActions() const
{
    return Qt::CopyAction;
}

bool QFPSTreeModel::AddNewFPSet(QString strName, bool bVisible, int nStatus)
{
	return AddNewLeaf(rootItem, strName, bVisible, nStatus);
}

bool QFPSTreeModel::AddNewLeaf(TreeItem *parent, QString strName, bool bVisible, int nStatus)
{
	QList<QVariant> columnData;
	columnData << strName;
	TreeItem *child = new TreeItem(columnData, parent);
	parent->appendChild(child);
	//support 3state check box
	child->nCheckState = (nStatus == ITEM_STATUS_ON)? 2 : (nStatus == ITEM_STATUS_OFF)? 0 : 1;

	emit layoutChanged();

	//if needed, hide row in the QTreeView
	if(!bVisible){
		if(m_pWidget){
			QModelIndex parentIdx = createIndex(parent->row(), 0, parent);
			int row = parent->childCount()-1;
			ShowRow(parentIdx.child(row, 0), bVisible);
		}
	}

	return true;
}

void QFPSTreeModel::Clear()
{
	if(rootItem->childCount() > 0)
	{
		QModelIndex parent;
		removeRows(0, rootItem->childCount(), parent);
		emit layoutChanged();
	}
}

//store parent branch info (list of indexes)
void QFPSTreeModel::IndexToList(QModelIndex index, QList<int> &lstPos) const
{
	lstPos.clear();

	QModelIndex tmp = index;
	while(tmp.isValid()){
		lstPos.push_front(tmp.row());
		tmp = tmp.parent();
	}
}

QModelIndex QFPSTreeModel::ListToIndex(QList<int> lstPos)
{
	QModelIndex index = createIndex(rootItem->row(), 0, rootItem);	//get root

	int nCount = lstPos.size();
	for(int i=0; i<nCount; i++)
		index = index.child(lstPos[i], 0);

	return index;
}

void QFPSTreeModel::ShowRow(QModelIndex index, bool bShow)
{
	((QTreeView *)m_pWidget)->clearSelection();//TOFIX this is temporary fix for setRowHidden bug
	((QTreeView *)m_pWidget)->setRowHidden(index.row(), index.parent(), !bShow);
}

void QFPSTreeModel::ShowHidden(bool bShow)
{
	//hide/unhide all hidden rows
	QModelIndex root;
	int nCount = m_pData->m_lstFpSets.size();
	for(int i=0; i<nCount; i++)
	{
		if(!m_pData->m_lstFpSets[i].bActive){
			QList<int> lstPos;
			m_pData->m_lstFpSets.GetNodePos(i, lstPos);
			QModelIndex index = ListToIndex(lstPos);
			ShowRow(index, bShow);
		}
	}
	emit layoutChanged();
}

void QFPSTreeModel::RedrawBranch(QModelIndex index)
{
	//calculate last descendant
	QModelIndex last = index;
	TreeItem *item = static_cast<TreeItem*>(last.internalPointer());
	int nCnt = item->childCount();
	while(nCnt > 0){
		last = last.child(nCnt-1, 0);
		item = static_cast<TreeItem*>(last.internalPointer());
		nCnt = item->childCount();
	}

	emit dataChanged(index, last);	//force item redraw
}

//only fill missing pieces in the model (available from storage)
void QFPSTreeModel::RebuildGuiBranch(QModelIndex parent)
{
	TreeList &lstData(m_pData->m_lstFpSets);

	//find parent node in the data storage list
	QList<int> lstPos;
	IndexToList(parent, lstPos);
	int nParent = lstData.FindNode(lstPos);

	TreeItem *item = rootItem;
	if(parent.isValid())
		item = static_cast<TreeItem*>(parent.internalPointer());

	//compare child counts in the model and data storage
	int nExistingCount = item->childCount();
	int nTotalCount	   = lstData.GetChildCount(nParent);

	int nDiff = nTotalCount - nExistingCount;
	if(nDiff > 0)
	{
		beginInsertRows(parent, nExistingCount, nTotalCount-1);
		int nChildIdx = lstData.FindChild(nParent, nExistingCount);
		while(nChildIdx >= 0)
		{
			//prepare column data for the new item
			QString strTitle;

			//append optional code into the title
			if(lstData[nChildIdx].strCode.isEmpty())
				strTitle = lstData[nChildIdx].strName;
			else
				strTitle.sprintf("%s (%s)", lstData[nChildIdx].strName.toLatin1().constData(), lstData[nChildIdx].strCode.toLatin1().constData());

			//append optional value into the title
			if(lstData[nChildIdx].nValue != 0)
			{
				QString strTmp;
				strTmp.sprintf(" (%d)",	lstData[nChildIdx].nValue);
				strTitle += strTmp;
			}

			//create new child item
			QList<QVariant> columnData;
			columnData << strTitle;

			TreeItem *child = new TreeItem(columnData, item);

			int nStatus = lstData[nChildIdx].nStatus;
			child->nCheckState = (nStatus == ITEM_STATUS_ON)? 2 : (nStatus == ITEM_STATUS_OFF)? 0 : 1;
			item->appendChild(child);

			nChildIdx = lstData.FindNextSibling(nChildIdx);	// go to next child
		}
		endInsertRows();
	}

	//recurse to the deeper level
	for(int i=0; i<nTotalCount; i++)
		RebuildGuiBranch(index(i, 0, parent));
}
