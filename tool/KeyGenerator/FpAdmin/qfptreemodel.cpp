/****************************************************************************
**
** Copyright (C) 2005-2006 Trolltech AS. All rights reserved.
**
** This file is part of the example classes of the Qt Toolkit.
**
** Licensees holding a valid Qt License Agreement may use this file in
** accordance with the rights, responsibilities and obligations
** contained therein.  Please consult your licensing agreement or
** contact sales@trolltech.com if any conditions of this licensing
** agreement are not clear to you.
**
** Further information about Qt licensing is available at:
** http://www.trolltech.com/products/qt/licensing.html or by
** contacting info@trolltech.com.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

/*
    QFPTreeModel.cpp

    Provides a simple tree model to show how to create and use hierarchical
    models.
*/

#include <QtGui>
#include <QTreeView>
#include "treeitem.h"
#include "qfptreemodel.h"

QFPTreeModel::QFPTreeModel(QObject *parent)
    : QAbstractItemModel(parent)
{
	m_pData		= NULL;
	m_pWidget	= NULL;

	QList<QVariant> rootData;
    rootData << "Name";
    rootItem = new TreeItem(rootData);
}

QFPTreeModel::~QFPTreeModel()
{
    delete rootItem;
}

int QFPTreeModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return static_cast<TreeItem*>(parent.internalPointer())->columnCount();
    else
        return rootItem->columnCount();
}

QVariant QFPTreeModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

	QList<int> lstPos;
	IndexToList(index, lstPos);
	int nNode = m_pData->m_lstFP.FindNode(lstPos);
	Q_ASSERT(nNode >= 0);

    TreeItem *item = static_cast<TreeItem*>(index.internalPointer());
	if (role == Qt::CheckStateRole)
	{
		return QVariant(item->nCheckState);	//return node checked state
	}
	else if (role == Qt::DecorationRole)
	{
		//calculate icon based on the type (FP/PFS)

		//is node hidden
		bool bHidden = false;
		if(!m_pData->m_lstFP[nNode].bActive)
			bHidden = true;

		QIcon icon;
		if(bHidden)
			icon.addFile(":inactive.png");
		else
			icon.addFile(":fp.png");
		return QVariant(icon);	//return node's icon
	}
	else if (role == Qt::ToolTipRole || role == Qt::StatusTipRole)
	{
		//support to view long entries as tooltips and status bar entries
		return QVariant(m_pData->m_lstFP[nNode].strDesc);
	}

    if (role != Qt::DisplayRole)
        return QVariant();

    return item->data(index.column());
}

Qt::ItemFlags QFPTreeModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::ItemIsEnabled;

    return Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsDragEnabled | Qt::ItemIsSelectable | Qt::ItemIsUserCheckable;
}

QVariant QFPTreeModel::headerData(int section, Qt::Orientation orientation,
                               int role) const
{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
        return rootItem->data(section);

    return QVariant();
}

QModelIndex QFPTreeModel::index(int row, int column, const QModelIndex &parent)
            const
{
    TreeItem *parentItem;

    if (!parent.isValid())
        parentItem = rootItem;
    else
        parentItem = static_cast<TreeItem*>(parent.internalPointer());

    TreeItem *childItem = parentItem->child(row);
    if (childItem)
        return createIndex(row, column, childItem);
    else
        return QModelIndex();
}

QModelIndex QFPTreeModel::parent(const QModelIndex &index) const
{
    if (!index.isValid())
        return QModelIndex();

    TreeItem *childItem = static_cast<TreeItem*>(index.internalPointer());
    TreeItem *parentItem = childItem->parent();

    if (parentItem == rootItem)
        return QModelIndex();

    return createIndex(parentItem->row(), 0, parentItem);
}

int QFPTreeModel::rowCount(const QModelIndex &parent) const
{
    TreeItem *parentItem;

    if (!parent.isValid())
        parentItem = rootItem;
    else
        parentItem = static_cast<TreeItem*>(parent.internalPointer());

    return parentItem->childCount();
}

bool QFPTreeModel::removeRows(int row, int count, const QModelIndex &parent)
{
	TreeItem *item = static_cast<TreeItem*>(parent.internalPointer());
	if(NULL == item)
		item = rootItem;

	//remove X children from the parent node
	beginRemoveRows(parent, row, row+count-1);
	item->RemoveChildren(row, count);
	endRemoveRows();
	
	return true;
}

bool QFPTreeModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
	TreeItem *item = static_cast<TreeItem*>(index.internalPointer());
	if(NULL != item){
		Q_ASSERT(0 == index.column());
		if(Qt::EditRole == role || Qt::DisplayRole == role){
			item->SetData(index.column(), value);	//store title
		}
		else if (Qt::CheckStateRole == role){
			item->nCheckState = value.toInt();		//store check state
			//update data
			m_pData->m_lstFP[index.row()].nStatus = (item->nCheckState > 0)? ITEM_STATUS_ON : ITEM_STATUS_OFF;
		}
		emit dataChanged(index, index);	//force item redraw
		return true;
	}
	Q_ASSERT(false);
	return false;
}

bool QFPTreeModel::AddNewFP(QString strName, bool bVisible, bool bChecked)
{
	QList<QVariant> columnData;
	columnData << strName;

	TreeItem *item = new TreeItem(columnData, rootItem);
	item->nCheckState = (bChecked)? 2 : 0;

	rootItem->appendChild(item);
	emit layoutChanged();

	//if needed, hide row in the QTreeView
	if(!bVisible){
		if(m_pWidget){
			QModelIndex parent;
			int row = rootItem->childCount()-1;
			((QTreeView *)m_pWidget)->setRowHidden(row, parent, !bVisible);
		}
	}

	return true;
}

void QFPTreeModel::Clear()
{
	if(rootItem->childCount() > 0)
	{
		QModelIndex parent;
		removeRows(0, rootItem->childCount(), parent);
		emit layoutChanged();
	}
}

void QFPTreeModel::ShowRow(int nRow, bool bShow)
{
	((QTreeView *)m_pWidget)->clearSelection();//TOFIX this is temporary fix for setRowHidden bug
	((QTreeView *)m_pWidget)->setRowHidden(nRow, QModelIndex(), !bShow);
}

void QFPTreeModel::ShowHidden(bool bShow)
{
	//unhide all rows
	QModelIndex root;
	int nCount = m_pData->m_lstFP.size();
	for(int i=0; i<nCount; i++)
	{
		if(!m_pData->m_lstFP[i].bActive)
			((QTreeView *)m_pWidget)->setRowHidden(i, root, !bShow);
	}
	emit layoutChanged();
}

//store parent branch info (list of indexes)
void QFPTreeModel::IndexToList(QModelIndex index, QList<int> &lstPos) const
{
	lstPos.clear();

	QModelIndex tmp = index;
	while(tmp.isValid()){
		lstPos.push_front(tmp.row());
		tmp = tmp.parent();
	}
}

void QFPTreeModel::RedrawBranch(QModelIndex index)
{
	emit dataChanged(index, index);	//force item redraw
}
