#ifndef QFPTREEWIDGET_H
#define QFPTREEWIDGET_H

#include <QTreeView>
#include <QDragEnterEvent>
#include <QDropEvent>

class QFPTreeWidget : public QTreeView
{
	Q_OBJECT

public:
    QFPTreeWidget(QWidget *parent);
    ~QFPTreeWidget();

private:
    void dragEnterEvent(QDragEnterEvent *event);
    void dropEvent(QDropEvent *event);
};

#endif // QFPTREEWIDGET_H
