#include "qfpstreewidget.h"
#include <QTreeWidgetItem>
#include <QMimeData>
#include "qfpstreemodel.h"

QFPSTreeWidget::QFPSTreeWidget(QWidget *parent)
	: QTreeView(parent)
{
	setSelectionMode(QAbstractItemView::SingleSelection);
    setDragEnabled(true);
    setAcceptDrops(true);
    setDropIndicatorShown(true);
}

QFPSTreeWidget::~QFPSTreeWidget()
{
}

void QFPSTreeWidget::dragEnterEvent(QDragEnterEvent *event)
{
	if (event->mimeData()->hasFormat("application/x-qabstractitemmodeldatalist"))
        event->accept();
    else
        event->ignore();
}

void QFPSTreeWidget::dropEvent(QDropEvent *event)
{
	if (event->mimeData()->hasFormat("application/x-qabstractitemmodeldatalist")) 
	{
		if(event->source() == m_pFPWidget)	//drop from FP list
		{
			//allow dropping only to top level items
			QModelIndex parentIdx = indexAt(event->pos());
			if(parentIdx.isValid() && !parentIdx.parent().isValid())
			{
				int row		= model()->rowCount(parentIdx);	//drop as last child
				int column	= 0; 
			
				//insert new FP's into the FPS tree
				model()->dropMimeData(event->mimeData(), event->dropAction(), row, column, parentIdx);
		
				//TOFIX refresh my FPS data storage in the model object?
				event->accept();
				return;
			}
		}
		else if(event->source() == this)	//allow from own list
		{
			//allow dropping only to top level items
			QModelIndex parentIdx = indexAt(event->pos());
			if(parentIdx.isValid() && !parentIdx.parent().isValid())
			{
				// droping the node onto itself is not allowed
				if(parentIdx == selectionModel()->currentIndex()){
					event->ignore();
					return;
				}

				((QFPSTreeModel *)model())->m_bSelfDrop = true;

				int row		= model()->rowCount(parentIdx);	//drop as last child
				int column	= 0; 
			
				//insert new FP's into the FPS tree
				model()->dropMimeData(event->mimeData(), event->dropAction(), row, column, parentIdx);
		
				//TOFIX refresh my FPS data storage in the model object?
				event->accept();
				return;
			}
		}
    }
     
	event->ignore();
}
