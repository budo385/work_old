#include "qmodtreewidget.h"
#include <QTreeWidgetItem>
#include "qmodtreemodel.h"
#include <QMimeData>

QModTreeWidget::QModTreeWidget(QWidget *parent)
	: QTreeView(parent)
{
	m_pData = NULL;
	m_pFPWidget = NULL;
	m_pFPSWidget = NULL;

	setSelectionMode(QAbstractItemView::SingleSelection);
    setAcceptDrops(true);
    setDropIndicatorShown(true);
}

QModTreeWidget::~QModTreeWidget()
{
}

void QModTreeWidget::dragEnterEvent(QDragEnterEvent *event)
{
    if (event->mimeData()->hasFormat("application/x-qabstractitemmodeldatalist"))
        event->accept();
    else
        event->ignore();
}

void QModTreeWidget::dropEvent(QDropEvent *event)
{
	if (event->mimeData()->hasFormat("application/x-qabstractitemmodeldatalist")) 
	{
		if(event->source() == m_pFPSWidget)
		{
			//allow dropping only to top level items
			QModelIndex parentIdx = indexAt(event->pos());
			if(parentIdx.isValid() && !parentIdx.parent().isValid())
			{
				//calculate row/col from position
				int row, column; 
				row		= model()->rowCount(parentIdx);
				column	= 0;
				
				//insert new FPS' into the FPS tree
				((QModTreeModel *)model())->m_bFPSDrop = true;
				model()->dropMimeData(event->mimeData(), event->dropAction(), row, column, parentIdx);
				event->accept();
				return;
			}
		}
		else if(event->source() == m_pFPWidget)
		{
			//allow dropping only to top level items
			QModelIndex parentIdx = indexAt(event->pos());
			if(parentIdx.isValid() && !parentIdx.parent().isValid())
			{
				//calculate row/col from position
				int row, column; 
				row		= model()->rowCount(parentIdx);
				column	= 0;
				
				//insert new FP's into the FPS tree
				((QModTreeModel *)model())->m_bFPDrop = true;
				model()->dropMimeData(event->mimeData(), event->dropAction(), row, column, parentIdx);
				event->accept();
				return;
			}
		}
    }

	event->ignore();
}
