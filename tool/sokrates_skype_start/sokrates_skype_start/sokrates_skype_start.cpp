#include "sokrates_skype_start.h"

#include <QSettings>
#include <QString>
#include <QProcess>
#include <QDir>
#include <QCloseEvent>
#include <QLibrary>
#include <QAxObject>
#include <QMessageBox>

sokrates_skype_start::sokrates_skype_start(QWidget *parent, Qt::WFlags flags)
	: QMainWindow(parent, flags)
{
	QAxObject DRMClient("spmServices.DRMClientV2");
	QList<QVariant> vars;

	QStringList lstVerbs = DRMClient.verbs();

	foreach(QString verb, lstVerbs)
		verb;

/*	int vers = DRMClient.property("Version").toInt();

	QString sessionID = QApplication::arguments().last();
	QString sessionID1 = QApplication::arguments().first();

	QMessageBox::information(NULL, "naslov last", sessionID);
	QMessageBox::information(NULL, "naslov first", sessionID1);

	bool b = DRMClient.dynamicCall("ConnectService(const QString&)", sessionID).toBool();
	const char *sessID = sessionID.toLocal8Bit().data();

	int nLicenceType = DRMClient.dynamicCall("LicenseType(\"932.7.7\")").toInt();
	//int nLicenceType = DRMClient.dynamicCall(sessID).toInt();*/

//	QMessageBox::information(NULL, "naslov", QVariant(nLicenceType).toString());
	QSettings settingsPE("HKEY_LOCAL_MACHINE\\Software\\Helix Business Soft\\SOKRATES Communicator Personal Edition\\Settings",	QSettings::NativeFormat);
	QString strPath = settingsPE.value("InstallPath",QString("")).toString();
	strPath = "\"" + strPath + "\\sokrates.exe\"";
	strPath = QDir::toNativeSeparators(strPath);
	QProcess::startDetached(strPath);

	close();
}

sokrates_skype_start::~sokrates_skype_start()
{

}

void sokrates_skype_start::closeEvent(QCloseEvent *event)
{
	event->accept();
} 