#ifndef SOKRATES_SKYPE_START_H
#define SOKRATES_SKYPE_START_H

#include <QtGui/QMainWindow>
#include "ui_sokrates_skype_start.h"

class sokrates_skype_start : public QMainWindow
{
	Q_OBJECT

public:
	sokrates_skype_start(QWidget *parent = 0, Qt::WFlags flags = 0);
	~sokrates_skype_start();

	void closeEvent(QCloseEvent *event);
private:
	Ui::sokrates_skype_startClass ui;
};

#endif // SOKRATES_SKYPE_START_H
