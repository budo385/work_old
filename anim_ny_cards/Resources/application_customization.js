var backgroundColor='#740A0A';
var backgroundColorLighter='#E33535';
var application_xml_url="http://inimated.com/acc/res/template";
var application_xml='/acc2_templates.xml';
var application_update_url="http://inimated.com/acc/res/template/acc2_version.txt";
var image_download_url="http://download.sokrates.ch/img/ACC/";

if(Titanium.App.name=='anim_ny_cards')
{
    backgroundColor='#080859';
    backgroundColorLighter='#3C3CE6';
}

if(Titanium.App.name=='emails_plus_fun')
{
    backgroundColor='#003231';
    backgroundColorLighter='#00bebc';

    application_xml_url="http://inimated.com/mpf/res/template";
    application_xml="/mpf_templates.xml";
    application_update_url="http://inimated.com/mpf/res/template/mpf_version.txt";
    image_download_url="http://download.sokrates.ch/img/ACC/";
}
