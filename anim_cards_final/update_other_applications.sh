#!/bin/bash

#Christmas cards
CHRISTMAS_CARD_IPHONE_DIR=("/Users/helix/Documents/anim_cards_final/")
CHRISTMAS_CARD_IPHONE_RESOURCES_DIR=("/Users/helix/Documents/anim_cards_final/Resources/")
CHRISTMAS_CARD_IPAD_DIR=("/Users/helix/Documents/animated_cards_ipad/")

echo "Removing Resources from Ipad christmas cards app"
rm -r $CHRISTMAS_CARD_IPAD_DIR"Resources"
echo "Copying Resources to Ipad christmas cards app"
cp -r $CHRISTMAS_CARD_IPHONE_DIR"Resources" $CHRISTMAS_CARD_IPAD_DIR
echo "Removing i18n from Ipad christmas cards app"
rm -r $CHRISTMAS_CARD_IPAD_DIR"i18n"
echo "Copying i18n to Ipad christmas cards app"
cp -r $CHRISTMAS_CARD_IPHONE_DIR"i18n" $CHRISTMAS_CARD_IPAD_DIR
echo "Copying Info.plist to Ipad christmas cards app"
cp -r $CHRISTMAS_CARD_IPHONE_DIR"Info.plist" $CHRISTMAS_CARD_IPAD_DIR

#New year cards
CHRISTMAS_CARD_NY_DIR=("/Users/helix/Documents/anim_ny_cards/")
CHRISTMAS_CARD_NY_RESOURCES_DIR=("/Users/helix/Documents/anim_ny_cards/Resources/")
FILES_APPLICATION_RESOURCES_FILES=("app.js" "application_customization.js" "body_replace.js" "images" "info.js" "language_selector.js"
"message_input.js" "preview_card.js" "title_input.js" "languages.xml")

echo "Copying i18n to new year app"
cp -r $CHRISTMAS_CARD_IPHONE_DIR"i18n" $CHRISTMAS_CARD_NY_DIR
echo "Copying Info.plist to new year app"
cp -r $CHRISTMAS_CARD_IPHONE_DIR"Info.plist" $CHRISTMAS_CARD_NY_DIR

echo "Copying files from resources to new year app resources"
for (( i = 0; i < ${#FILES_APPLICATION_RESOURCES_FILES[@]}; i++ ))
do
    cp -r $CHRISTMAS_CARD_IPHONE_RESOURCES_DIR${FILES_APPLICATION_RESOURCES_FILES[$i]} $CHRISTMAS_CARD_NY_RESOURCES_DIR
    echo "Copying ${FILES_APPLICATION_RESOURCES_FILES[$i]}"
done

#Emails plus fun.
EMAILS_PLUS_FUN_DIR=("/Users/helix/Documents/emails_plus_fun/")
EMAILS_PLUS_FUN_RESOURCES_DIR=("/Users/helix/Documents/emails_plus_fun/Resources/")
EMAILS_PLUS_FUN_FILES_APPLICATION_RESOURCES_FILES=("app.js" "application_customization.js" "body_replace.js" "images" "info.js" "language_selector.js"
"message_input.js" "preview_card.js" "title_input.js" "languages.xml")

#echo "Copying i18n to new year app"
#cp -r $CHRISTMAS_CARD_IPHONE_DIR"i18n" $EMAILS_PLUS_FUN_DIR
#echo "Copying Info.plist to new year app"
#cp -r $CHRISTMAS_CARD_IPHONE_DIR"Info.plist" $EMAILS_PLUS_FUN_DIR

echo "Copying files from resources to new year app resources"
for (( i = 0; i < ${#EMAILS_PLUS_FUN_FILES_APPLICATION_RESOURCES_FILES[@]}; i++ ))
do
    cp -r $CHRISTMAS_CARD_IPHONE_RESOURCES_DIR${EMAILS_PLUS_FUN_FILES_APPLICATION_RESOURCES_FILES[$i]} $EMAILS_PLUS_FUN_RESOURCES_DIR
    echo "Copying ${EMAILS_PLUS_FUN_FILES_APPLICATION_RESOURCES_FILES[$i]}"
done