Titanium.include('./body_replace.js','./application_customization.js');

var inputWin = Titanium.UI.currentWindow;
inputWin.barColor = backgroundColor;
inputWin.title=L("message");
inputWin.backgroundGradient={type:'linear',colors:[{color:backgroundColor,position:0.0}, {color:backgroundColorLighter,position:0.50},{color:backgroundColor,position:1.0}]};
inputWin.templateName=Ti.App.Properties.getString('TEMPLATE_NAME');

var messageTextBottom = 225;

if(Titanium.Platform.osname=='ipad')
{
    inputWin.orientationModes=[Titanium.UI.PORTRAIT,Titanium.UI.UPSIDE_PORTRAIT,Titanium.UI.LANDSCAPE_LEFT,Titanium.UI.LANDSCAPE_RIGHT];
    messageTextBottom = 270;
}
else
{
    inputWin.orientationModes=[Titanium.UI.PORTRAIT];
}

var templatesPath = Titanium.Filesystem.resourcesDirectory+'/templates';

var buttonObjects = [
    {image:'./images/Preview16.png',width:50},
    {title:L("ok"), width:50}
];

var buttonBar = Titanium.UI.createButtonBar({
    labels:buttonObjects,
    backgroundColor:backgroundColor,
    style:Titanium.UI.iPhone.SystemButtonStyle.BAR
});

inputWin.rightNavButton=buttonBar;

var messageTextArea = Titanium.UI.createTextArea({
    top:10,
    bottom:messageTextBottom,
    left:10,
    right:10,
    borderWidth:2,
    zIndex:2,
    opacity:0.85,
    font:{fontSize:17},
    borderColor:'#bbb',
    borderRadius:8,
    suppressReturn:false
});
if(Ti.App.Properties.getString('MESSAGE_TEXT')!=null)
{
    messageTextArea.value=Ti.App.Properties.getString('MESSAGE_TEXT');
}
messageTextArea.addEventListener('change', function(e)
{
    Ti.App.Properties.setString('MESSAGE_TEXT',e.value);
});

inputWin.add(messageTextArea);

if(Titanium.App.name!='emails_plus_fun')
{
    var htmlPath= Titanium.Filesystem.resourcesDirectory+'/html';
    var f = Titanium.Filesystem.getFile(htmlPath, 'index.html');
    var contents = f.read();
    var html=contents.text;
    
    var webview = Titanium.UI.createWebView({
	html:html
    });
    
    inputWin.add(webview);
}

function onCreateCard()
{
    var emailDialog = Titanium.UI.createEmailDialog();
    emailDialog.barColor=backgroundColor;
    emailDialog.setSubject(Ti.App.Properties.getString('TITLE_TEXT'));
    var html=getTemplateHTML(inputWin.templateName);
    
    emailDialog.setMessageBody(html);
    emailDialog.setHtml(true);
    emailDialog.addEventListener('complete',function(e)
    {
	if (e.result == emailDialog.SENT)
	{
	    alert("Message Sent!");
	}
	else if (e.result == emailDialog.FAILED)
	{
	    alert("Message Not Sent.");
	}
	else if (e.result == emailDialog.SAVED)
	{
	    alert("Message Saved.");
	}
    });
    emailDialog.open();
}

function onPreview()
{
    var w = Titanium.UI.createWindow({
	url:'preview_card.js'
    });
    var b = Titanium.UI.createButton({
	    title:L("close"),
	    style:Titanium.UI.iPhone.SystemButtonStyle.PLAIN
    });
    w.setLeftNavButton(b);
    b.addEventListener('click',function()
    {
	    w.close();
    });
    w.open({modal:true});
}

buttonBar.addEventListener('click', function(e)
{
    if(e.index)
    {
	onCreateCard();
    }
    else
    {
        onPreview();
    }
});

inputWin.addEventListener('open', function()
{
    messageTextArea.focus();
});
