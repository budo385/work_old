//Titanium.include('./hashes.js','./device_preferences.js');

//English.
var enLetterIDToPicture=[];
enLetterIDToPicture.push("a.png"); 	//0
enLetterIDToPicture.push("b.png");	//1 
enLetterIDToPicture.push("c.png");	//2
enLetterIDToPicture.push("d.png");	//3
enLetterIDToPicture.push("e.png");	//4
enLetterIDToPicture.push("f.png");	//5
enLetterIDToPicture.push("g.png");	//6
enLetterIDToPicture.push("h.png");	//7
enLetterIDToPicture.push("i.png");	//8
enLetterIDToPicture.push("j.png");	//9
enLetterIDToPicture.push("k.png");	//10
enLetterIDToPicture.push("l.png");	//11
enLetterIDToPicture.push("m.png");	//12
enLetterIDToPicture.push("n.png");	//13
enLetterIDToPicture.push("o.png");	//14
enLetterIDToPicture.push("p.png");	//15
enLetterIDToPicture.push("q.png");	//16
enLetterIDToPicture.push("r.png");	//17
enLetterIDToPicture.push("s.png");	//18
enLetterIDToPicture.push("t.png");	//19
enLetterIDToPicture.push("u.png");	//20
enLetterIDToPicture.push("v.png");	//21
enLetterIDToPicture.push("w.png");	//22
enLetterIDToPicture.push("x.png");	//23
enLetterIDToPicture.push("y.png");	//24
enLetterIDToPicture.push("z.png");	//25

//Croatian.
var enBlankLetterIDToPicture=[];
enBlankLetterIDToPicture.push("a_blank.png"); 	//0
enBlankLetterIDToPicture.push("b_blank.png");	//1 
enBlankLetterIDToPicture.push("c_blank.png");	//2
enBlankLetterIDToPicture.push("d_blank.png");	//3
enBlankLetterIDToPicture.push("e_blank.png");	//4
enBlankLetterIDToPicture.push("f_blank.png");	//5
enBlankLetterIDToPicture.push("g_blank.png");	//6
enBlankLetterIDToPicture.push("h_blank.png");	//7
enBlankLetterIDToPicture.push("i_blank.png");	//8
enBlankLetterIDToPicture.push("j_blank.png");	//9
enBlankLetterIDToPicture.push("k_blank.png");	//10
enBlankLetterIDToPicture.push("l_blank.png");	//11
enBlankLetterIDToPicture.push("m_blank.png");	//12
enBlankLetterIDToPicture.push("n_blank.png");	//13
enBlankLetterIDToPicture.push("o_blank.png");	//14
enBlankLetterIDToPicture.push("p_blank.png");	//15
enBlankLetterIDToPicture.push("q_blank.png");	//16
enBlankLetterIDToPicture.push("r_blank.png");	//17
enBlankLetterIDToPicture.push("s_blank.png");	//18
enBlankLetterIDToPicture.push("t_blank.png");	//19
enBlankLetterIDToPicture.push("u_blank.png");	//20
enBlankLetterIDToPicture.push("v_blank.png");	//21
enBlankLetterIDToPicture.push("w_blank.png");	//22
enBlankLetterIDToPicture.push("x_blank.png");	//23
enBlankLetterIDToPicture.push("y_blank.png");	//24
enBlankLetterIDToPicture.push("z_blank.png");	//25

var enHighlightedLetterIDToPicture=[];
enHighlightedLetterIDToPicture.push("a_highlighted.png"); 	//0
enHighlightedLetterIDToPicture.push("b_highlighted.png");	//1 
enHighlightedLetterIDToPicture.push("c_highlighted.png");	//2
enHighlightedLetterIDToPicture.push("d_highlighted.png");	//3
enHighlightedLetterIDToPicture.push("e_highlighted.png");	//4
enHighlightedLetterIDToPicture.push("f_highlighted.png");	//5
enHighlightedLetterIDToPicture.push("g_highlighted.png");	//6
enHighlightedLetterIDToPicture.push("h_highlighted.png");	//7
enHighlightedLetterIDToPicture.push("i_highlighted.png");	//8
enHighlightedLetterIDToPicture.push("j_highlighted.png");	//9
enHighlightedLetterIDToPicture.push("k_highlighted.png");	//10
enHighlightedLetterIDToPicture.push("l_highlighted.png");	//11
enHighlightedLetterIDToPicture.push("m_highlighted.png");	//12
enHighlightedLetterIDToPicture.push("n_highlighted.png");	//13
enHighlightedLetterIDToPicture.push("o_highlighted.png");	//14
enHighlightedLetterIDToPicture.push("p_highlighted.png");	//15
enHighlightedLetterIDToPicture.push("q_highlighted.png");	//16
enHighlightedLetterIDToPicture.push("r_highlighted.png");	//17
enHighlightedLetterIDToPicture.push("s_highlighted.png");	//18
enHighlightedLetterIDToPicture.push("t_highlighted.png");	//19
enHighlightedLetterIDToPicture.push("u_highlighted.png");	//20
enHighlightedLetterIDToPicture.push("v_highlighted.png");	//21
enHighlightedLetterIDToPicture.push("w_highlighted.png");	//22
enHighlightedLetterIDToPicture.push("x_highlighted.png");	//23
enHighlightedLetterIDToPicture.push("y_highlighted.png");	//24
enHighlightedLetterIDToPicture.push("z_highlighted.png");	//25

var hrLetterIDToPicture=[];
hrLetterIDToPicture.push("a.png"); 	//0
hrLetterIDToPicture.push("b.png");	//1 
hrLetterIDToPicture.push("c.png");	//2
hrLetterIDToPicture.push("cch.png");	//3
hrLetterIDToPicture.push("ch.png");	//4
hrLetterIDToPicture.push("d.png");	//5
hrLetterIDToPicture.push("ddz.png");	//6
hrLetterIDToPicture.push("dz.png");	//7
hrLetterIDToPicture.push("e.png");	//8
hrLetterIDToPicture.push("f.png");	//9
hrLetterIDToPicture.push("g.png");	//10
hrLetterIDToPicture.push("h.png");	//11
hrLetterIDToPicture.push("i.png");	//12
hrLetterIDToPicture.push("j.png");	//13
hrLetterIDToPicture.push("k.png");	//14
hrLetterIDToPicture.push("l.png");	//15
hrLetterIDToPicture.push("lj.png");	//16
hrLetterIDToPicture.push("m.png");	//17
hrLetterIDToPicture.push("n.png");	//18
hrLetterIDToPicture.push("nj.png");	//19
hrLetterIDToPicture.push("o.png");	//20
hrLetterIDToPicture.push("p.png");	//21
hrLetterIDToPicture.push("r.png");	//22
hrLetterIDToPicture.push("s.png");	//23
hrLetterIDToPicture.push("sh.png");	//24
hrLetterIDToPicture.push("t.png");	//25
hrLetterIDToPicture.push("u.png");	//26
hrLetterIDToPicture.push("v.png");	//27
hrLetterIDToPicture.push("z.png");	//28
hrLetterIDToPicture.push("zh.png");	//29

var hrBlankLetterIDToPicture=[];
hrBlankLetterIDToPicture.push("a_blank.png"); 	//0
hrBlankLetterIDToPicture.push("b_blank.png");	//1 
hrBlankLetterIDToPicture.push("c_blank.png");	//2
hrBlankLetterIDToPicture.push("cch_blank.png");	//3
hrBlankLetterIDToPicture.push("ch_blank.png");	//4
hrBlankLetterIDToPicture.push("d_blank.png");	//5
hrBlankLetterIDToPicture.push("ddz_blank.png");	//6
hrBlankLetterIDToPicture.push("dz_blank.png");	//7
hrBlankLetterIDToPicture.push("e_blank.png");	//8
hrBlankLetterIDToPicture.push("f_blank.png");	//9
hrBlankLetterIDToPicture.push("g_blank.png");	//10
hrBlankLetterIDToPicture.push("h_blank.png");	//11
hrBlankLetterIDToPicture.push("i_blank.png");	//12
hrBlankLetterIDToPicture.push("j_blank.png");	//13
hrBlankLetterIDToPicture.push("k_blank.png");	//14
hrBlankLetterIDToPicture.push("l_blank.png");	//15
hrBlankLetterIDToPicture.push("lj_blank.png");	//16
hrBlankLetterIDToPicture.push("m_blank.png");	//17
hrBlankLetterIDToPicture.push("n_blank.png");	//18
hrBlankLetterIDToPicture.push("nj_blank.png");	//19
hrBlankLetterIDToPicture.push("o_blank.png");	//20
hrBlankLetterIDToPicture.push("p_blank.png");	//21
hrBlankLetterIDToPicture.push("r_blank.png");	//22
hrBlankLetterIDToPicture.push("s_blank.png");	//23
hrBlankLetterIDToPicture.push("sh_blank.png");	//24
hrBlankLetterIDToPicture.push("t_blank.png");	//25
hrBlankLetterIDToPicture.push("u_blank.png");	//26
hrBlankLetterIDToPicture.push("v_blank.png");	//27
hrBlankLetterIDToPicture.push("z_blank.png");	//28
hrBlankLetterIDToPicture.push("zh_blank.png");	//29

var hrHighlightedLetterIDToPicture=[];
hrHighlightedLetterIDToPicture.push("a_highlighted.png"); 	//0
hrHighlightedLetterIDToPicture.push("b_highlighted.png");		//1 
hrHighlightedLetterIDToPicture.push("c_highlighted.png");		//2
hrHighlightedLetterIDToPicture.push("cch_highlighted.png");	//3
hrHighlightedLetterIDToPicture.push("ch_highlighted.png");	//4
hrHighlightedLetterIDToPicture.push("d_highlighted.png");		//5
hrHighlightedLetterIDToPicture.push("ddz_highlighted.png");	//6
hrHighlightedLetterIDToPicture.push("dz_highlighted.png");	//7
hrHighlightedLetterIDToPicture.push("e_highlighted.png");		//8
hrHighlightedLetterIDToPicture.push("f_highlighted.png");		//9
hrHighlightedLetterIDToPicture.push("g_highlighted.png");		//10
hrHighlightedLetterIDToPicture.push("h_highlighted.png");		//11
hrHighlightedLetterIDToPicture.push("i_highlighted.png");		//12
hrHighlightedLetterIDToPicture.push("j_highlighted.png");		//13
hrHighlightedLetterIDToPicture.push("k_highlighted.png");		//14
hrHighlightedLetterIDToPicture.push("l_highlighted.png");		//15
hrHighlightedLetterIDToPicture.push("lj_highlighted.png");	//16
hrHighlightedLetterIDToPicture.push("m_highlighted.png");		//17
hrHighlightedLetterIDToPicture.push("n_highlighted.png");		//18
hrHighlightedLetterIDToPicture.push("nj_highlighted.png");	//19
hrHighlightedLetterIDToPicture.push("o_highlighted.png");		//20
hrHighlightedLetterIDToPicture.push("p_highlighted.png");		//21
hrHighlightedLetterIDToPicture.push("r_highlighted.png");		//22
hrHighlightedLetterIDToPicture.push("s_highlighted.png");		//23
hrHighlightedLetterIDToPicture.push("sh_highlighted.png");	//24
hrHighlightedLetterIDToPicture.push("t_highlighted.png");		//25
hrHighlightedLetterIDToPicture.push("u_highlighted.png");		//26
hrHighlightedLetterIDToPicture.push("v_highlighted.png");		//27
hrHighlightedLetterIDToPicture.push("z_highlighted.png");		//28
hrHighlightedLetterIDToPicture.push("zh_highlighted.png");	//29

function getEnWord(id)
{
    var lstWord=[];
    switch(id)
    {
    case 0:
	    lstWord=["15","11","0","13","4"];			//Plane.
    break;
    case 1:
	    lstWord=["15","11","0","13","4"];			//Plane.
    break;
    case 2:
	    lstWord=["15","11","0","13","4"];			//Plane.
    break;
    case 3:
	    lstWord=["15","11","0","13","4"];			//Plane.
    break;
    case 4:
	    lstWord=["15","11","0","13","4"];			//Plane.
    break;
    case 5:
	    lstWord=["15","11","0","13","4"];			//Plane.
    break;
    case 6:
	    lstWord=["15","11","0","13","4"];			//Plane.
    break;
    }

    return lstWord;
}

function getHrWord(id)
{
    var lstWord=[];
    switch(id)
    {
    case 0:
	    lstWord=["0","27","12","20","18"];			//Avion.
    break;
    case 1:
	    lstWord=["0","26","25","20"];				//Auto
    break;
    case 2:
	    lstWord=["11","15","0","3","8"];			//Hlace
    break;
    case 3:
	    lstWord=["18","0","20","3","0","15","8"];		//Naocale
    break;
    case 4:
	    lstWord=["27","15","0","14"];				//Vlak
    break;
    case 5:
	    lstWord=["18","0","20","3","0","15","8"];		//Cesalj
    break;
    }

    return lstWord;
}

function getEnWordPicture(id)
{
    var picture='./images/';
    switch(id)
    {
    case 0:
	    picture+='avion.png';			//Avion.
    break;
    case 1:
	    picture+='avion.png';			//Avion.
    break;
    case 2:
	    picture+='avion.png';			//Avion.
    break;
    case 3:
	    picture+='avion.png';			//Avion.
    break;
    case 4:
	    picture+='avion.png';			//Avion.
    break;
    case 5:
	    picture+='avion.png';			//Avion.
    break;
    case 6:
	    picture+='avion.png';			//Avion.
    break;
    }

    return picture;
}

function getHrWordPicture(id)
{
    var picture='./images/';
    switch(id)
    {
    case 0:
	    picture+='avion.png';			//Avion.
    break;
    case 1:
	    picture+='avion.png';			//Avion.
    break;
    case 2:
	    picture+='avion.png';			//Avion.
    break;
    case 3:
	    picture+='avion.png';			//Avion.
    break;
    case 4:
	    picture+='avion.png';			//Avion.
    break;
    case 5:
	    picture+='avion.png';			//Avion.
    break;
    }

    return picture;
}

function getWord(id)
{
    var lstWord=[];
    switch(Ti.App.Properties.getInt('LANGUAGE'))
    {
    case 0/*'en'*/:				//English.
	lstWord=getEnWord(id);
    break;
    case 1/*'hr'*/:				//Croatian.
	lstWord=getHrWord(id);
    break;
    }
	
    return lstWord;
}

function getWordCount()
{
    var i=0;
    var lstWord=[];
    do
    {
	switch(Ti.App.Properties.getInt('LANGUAGE'))
	{
	case 0/*'en'*/:				//English.
	    lstWord=getEnWord(i);
	break;
	case 1/*'hr'*/:				//Croatian.
	    lstWord=getHrWord(i);
	break;
	}
	i++;
    }
    while (lstWord.length>0);

	
    return (i-1);
}

function getWordPicture(id)
{
    var picture;
    switch(Ti.App.Properties.getInt('LANGUAGE'))
    {
    case 0/*'en'*/:				//English.
	picture=getEnWordPicture(id);
    break;
    case 1/*'hr'*/:				//Croatian.
	picture=getHrWordPicture(id);
    break;
    }
	
    return picture;
}

function getLetterIDToPicture(id)
{
    
    var letterPicture;
    switch(Ti.App.Properties.getInt('LANGUAGE'))
    {
    case 0/*'en'*/:				//English.
	letterPicture=enLetterIDToPicture[id];
    break;
    case 1/*'hr'*/:				//Croatian.
	letterPicture=hrLetterIDToPicture[id];
    break;
    }
	
    return letterPicture;
}

function getBlankLetterIDToPicture(id)
{
    var letterPicture;
    switch(Ti.App.Properties.getInt('LANGUAGE'))
    {
    case 0/*'en'*/:				//English.
	letterPicture=enBlankLetterIDToPicture[id];
    break;
    case 1/*'hr'*/:				//Croatian.
	letterPicture=hrBlankLetterIDToPicture[id];
    break;
    }
	
    return letterPicture;
}

function getHighlightedLetterIDToPicture(id)
{
    var letterPicture;
    switch(Ti.App.Properties.getInt('LANGUAGE'))
    {
    case 0/*'en'*/:				//English.
	letterPicture=enHighlightedLetterIDToPicture[id];
    break;
    case 1/*'hr'*/:				//Croatian.
	letterPicture=hrHighlightedLetterIDToPicture[id];
    break;
    }
	
    return letterPicture;
}

/*
void LetterMap::FillWordIDToPictureNameHash()
{
	m_hshWordIDToPictureName.insert(0, ":avion.png");
	m_hshWordIDToPictureName.insert(1, ":auto.png");
	m_hshWordIDToPictureName.insert(2, ":hlace.png");
	m_hshWordIDToPictureName.insert(3, ":naocale.png");
	m_hshWordIDToPictureName.insert(4, ":vlak.png");
	m_hshWordIDToPictureName.insert(5, ":cesalj.png");
	m_hshWordIDToPictureName.insert(6, ":kuca.png");
	m_hshWordIDToPictureName.insert(7, ":brod.png");
	m_hshWordIDToPictureName.insert(8, ":leptir.png");
	m_hshWordIDToPictureName.insert(9, ":torba.png");
	m_hshWordIDToPictureName.insert(10, ":zvijezda.png");
	m_hshWordIDToPictureName.insert(11, ":riba.png");
	m_hshWordIDToPictureName.insert(12, ":usta.png");
	m_hshWordIDToPictureName.insert(13, ":oko.png");
	m_hshWordIDToPictureName.insert(14, ":cvijet.png");
	m_hshWordIDToPictureName.insert(15, ":sunce.png");
}
*/