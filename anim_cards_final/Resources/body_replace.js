Titanium.include('./application_customization.js');

var templatesPath = Titanium.Filesystem.resourcesDirectory+'/templates';
var templatesDir = Titanium.Filesystem.getFile(templatesPath);
var templatesXMLPath = templatesPath+application_xml;

function replaceBodyText(html,templateName)
{
    var color=templateName.charAt(0);
    var prefix=image_download_url;
    var image;

    switch(Ti.App.Properties.getInt('LANG_PICKER_ROW'))
    {
    case 0:
	image='enmc';
	break;
    case 1:
	image='frmc';
      break;
    case 2:
	image='gemc';
      break;
    case 3:
	image='spmc';
      break;
    case 4:
	image='itmc';
      break;
    case 5:
	image='nlmc';
      break;
    case 6:
	image='hrmc';
      break;
    case 7:
	image='pomc';
      break;
    case 8:
	image='irmc';
      break;
    case 9:
	image='czmc';
      break;
    case 10:
	image='inmc';
      break;
    case 11:
	image='nomc';
      break;
    case 12:
	image='slmc';
      break;
    case 13:
	image='svmc';
      break;
    default:
	image='enmc';
    }

    image=image+color+'.png';
    image=prefix+image;
    
    html=html.replace("[MC_Pic]", image);

    if(Ti.App.Properties.getString('MESSAGE_TEXT')==null ||Ti.App.Properties.getString('MESSAGE_TEXT')=='')
    {
	html=html.replace("[Message]", 'Body Message');
    }
    else
    {
	var text = Ti.App.Properties.getString('MESSAGE_TEXT');
	text = text.replace(/\n/gi,'<br />');
        html=html.replace("[Message]", text);
    }
    
    return html;
}
function setTitle(language_id)
{
    var languagesXMLPath = Titanium.Filesystem.resourcesDirectory+'/languages.xml';
    var f = Titanium.Filesystem.getFile(languagesXMLPath);
    var contents = f.read();
    var xml=Titanium.XML.parseString(contents.text);
    var languages=xml.getElementsByTagName("languages").item(0).getElementsByTagName("language");
    var language_string=languages.item(language_id).getElementsByTagName("name").item(0).text;
    language_string=language_string+'_title_text';

    var titleText=L(language_string);
    return titleText;
}

function getTemplatesNames()
{
    //Ti.API.info(Ti.App.Properties.getString('TEMPLATES_XML'));
    var xml=Titanium.XML.parseString(Ti.App.Properties.getString('TEMPLATES_XML'));
    var templates=xml.getElementsByTagName("ACC_Templates").item(0).getElementsByTagName("ACC_Template");
    var templateNames=[];
    for(var i=0;i<templates.length;i++)
    {
	templateNames.push(templates.item(i).getElementsByTagName("ACC_FileName").item(0).text);
    }
    
    return templateNames;
}

function getTemplateHTML(templateName)
{
    var xml=Titanium.XML.parseString(Ti.App.Properties.getString('TEMPLATES_XML'));
    var templates=xml.getElementsByTagName("ACC_Templates").item(0).getElementsByTagName("ACC_Template");
    var html;
    for(var i=0;i<templates.length;i++)
    {
	if(templateName==templates.item(i).getElementsByTagName("ACC_FileName").item(0).text)
	{
	    html=templates.item(i).getElementsByTagName("ACC_Data").item(0).text;
	    html=replaceBodyText(html,templateName);
	}
    }

    return html;
}