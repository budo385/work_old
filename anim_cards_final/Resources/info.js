Titanium.include('./application_customization.js');

var infoWin = Titanium.UI.currentWindow;
infoWin.barColor = backgroundColor;
infoWin.backgroundGradient={type:'linear',colors:[{color:backgroundColor,position:0.0}, {color:backgroundColorLighter,position:0.50},{color:backgroundColor,position:1.0}]};
infoWin.orientationModes=[Titanium.UI.PORTRAIT,Titanium.UI.LANDSCAPE_LEFT,Titanium.UI.LANDSCAPE_RIGHT];

var infoPath= Titanium.Filesystem.resourcesDirectory;

var f = Titanium.Filesystem.getFile(infoPath, 'ACC_Info.html');
var contents = f.read();
var html=contents.text;

var webviewInfo = Titanium.UI.createWebView({
	html:html,
	scalesPageToFit:true
});

infoWin.add(webviewInfo);