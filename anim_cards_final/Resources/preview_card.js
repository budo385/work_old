Titanium.include('./body_replace.js','./application_customization.js');

var previewCard = Titanium.UI.currentWindow;
previewCard.barColor = backgroundColor;
previewCard.backgroundGradient={type:'linear',colors:[{color:backgroundColor,position:0.0}, {color:backgroundColorLighter,position:0.50},{color:backgroundColor,position:1.0}]};
previewCard.templateName=Ti.App.Properties.getString('TEMPLATE_NAME');

var html=getTemplateHTML(previewCard.templateName);

var webview = Titanium.UI.createWebView({
    html:html
});

previewCard.add(webview);