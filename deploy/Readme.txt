Deploy Server (DS) Design
-------------------------------

- deploy server is used to automate all steps needed for making setup: svn-update, build, inno-setup make, ftp-upload.
- through web interface, admin can schedule task for making setup
- task is enabled if there is filename named as task in same directory where DS server is
- central DS server (at 192.168.200.10) can invoke all other slave DS servers to make setup (on different platforms) or additional tasks
- setup is deploy by ftp upload, admin is notified about error or sucess by email

Deploy server installation & setup
-------------------------------

- install Visual, QT, inno, openSSL
- set env vars: QTDIR,SOKRATES_XP and Path=qt\bin
- install certificates (dbl click on pfx file)
- once installed, run service as administrator (not local system), as certificates will not be recognized
- rewrite QT dll files from U:\Qtdlls
- install svn client (console), ankh, visual X, qt addin.
- enable/disable task by creating filename of same name as task (batchmanager.h). This is only needed for central DS
- in ini file, specify mail server, from, to, etc...


Deploy server adding new task
-------------------------------

- every new task is hardcoded, see batchmanager.h/cpp
- task can invoke batch scripts, ftp upload and send mail


ren dump.bat "dump_%time:~0,2%_%time:~3,2%_%time:~6,2%.bat"


Current Config
-------------------------------

Central Deploy server

- web: https://192.168.200.10:6666/tasks.html (only LAN)
- u:root, p:Konj1234
- sc_private, public i ffhserver tasks: svn_update, build, make setup, upload to akamai and sokrates.hr ftp's. 
  Invoke ferrari DS to deploy new files (newcode.zip) as Ferrari needs to update all server files
- Ferrari DS only accepts: deploy new files, restart private servers and schedule for public update.
- scenarios:
a) Marin wants private sc build: 
  - increase version in common/common/config_version_sc.h/commit to svn
  - login to DS server, execute private build
  - ask marin when to update private servers on ferrari then press: Send Ferrari command to update PRIVATE servers
  - as result in ftp.sokrates.hr/public_html there should be two new setup files (client and server) and in root ftp dir a newcode.zip (for ferrari)

b) Marin wants public sc build: 
  - increase version in common/common/config_version_sc.h/commit to svn
  - login to DS server, execute public build
  - ask marin when to update private servers on ferrari then press: Send Ferrari command to update PUBLIC servers
  - as result in akamai.ftp there should be two new setup files (client and server) and in root ftp dir a newcode.zip (for ferrari)


c) Marin wants FFH server
  - increase version in common/common/config_version_ffh.h/commit to svn
  - login to DS server, execute FFH Server: Deploy
  - as result in ftp.filesfromhome there should be new ffh setup

admin should receive mail if all went ok: /settings.ini/admin_mail= trumbic@sokrates.hr



Tasks
-------------------------------

0. Think if it is possible to create default tasks for remote DS server (no need to hardcode), configure new task through ini or something else.
1. Deploy DS server on MAC: create custom steps to make setup
2. use x64 compiler and make 64bit build
2. Install VMWare with Linux, deploy DS server and make linux build








