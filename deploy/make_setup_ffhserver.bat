@ECHO OFF

CALL D:\Deploy\Setup\certificate\sign_exe_ffhserver.bat
REM IF %ERRORLEVEL% NEQ 0 GOTO ERROR_HANDLER

D:\Deploy\Setup\FFHServer\everPICs-Bridge -convert "D:\Deploy\Setup\FFHServer\webservices\html""
IF %ERRORLEVEL% NEQ 0 GOTO ERROR_HANDLER

"C:\Program Files\Inno Setup 5\Compil32.exe" /cc D:\Deploy\Setup\FFHServer\install.iss
IF %ERRORLEVEL% NEQ 0 GOTO ERROR_HANDLER

copy /Y D:\Deploy\Setup\FFHServer\Output\EverPICsBridgeSetup.exe D:\Deploy\%1
IF %ERRORLEVEL% NEQ 0 GOTO ERROR_HANDLER

copy /Y D:\Deploy\Setup\FFHServer\webdata.bin D:\Deploy\webdata.bin
IF %ERRORLEVEL% NEQ 0 GOTO ERROR_HANDLER

CALL D:\Deploy\Setup\certificate\sign.bat %1
REM IF %ERRORLEVEL% NEQ 0 GOTO ERROR_HANDLER


echo.
echo Sucess!!!
exit 0

:ERROR_HANDLER
echo.
echo Error occured!!!
exit 1

