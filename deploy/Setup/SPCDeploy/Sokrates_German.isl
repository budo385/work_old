; This file contains custom messages used in the Sokrates Installation Program

[CustomMessages]
; These are Inno-specific

; These are Sokrates-specific
Sokrates_CreateStartMenuIcon=Verkn�pfung im &Startmen� erzeugen
Sokrates_Register=Registrieren
Sokrates_LaunchApp=Anwendung starten
SkeletonCaption=SOKRATES� Communicator Editionen
SkeletonDescription=W�hlen Sie die zu installierende Edition!
Warning_SameSettings=Es besteht schon eine Installation mit dem selben Unterverzeichnisnamen (letzter Teil des Installationspfades), bitte �ndern Sie diesen!
Warning_NoRemovableDrive=Kein externes Speicherger�t gefunden
Msg_RemovableInstall=Installation auf externen Speicherger�ten (USB-Stick, -Platte)
Msg_RemovableInstallLabel=Laufwerk:
