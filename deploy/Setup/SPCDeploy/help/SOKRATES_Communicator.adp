<!DOCTYPE DCF>

<assistantconfig version="3.2.0">

<profile>
    <property name="name">sokratescommunicator</property>
    <property name="title">SOKRATES Communicator</property>
    <property name="applicationicon">images/sokrates_help.png</property>
    <property name="startpage">index.html</property>
    <property name="aboutmenutext">About SOKRATES Communicator</property>
    <property name="abouturl">about.txt</property>
    <property name="assistantdocs">.</property>
</profile>

<DCF ref="index.html" icon="images/sokrates_help.png" title="SOKRATES Communicator">
        <section ref="./Contacts_Main.html" title="Contacts">
            <section ref="./Contacts_Workbench.html" title="The Contacts Workbench" />
            <section ref="./Contacts_How_To.html" title="Contacts: How To...">
                 <section ref="./Contacts_Projects.html" title="Assign Projects to a Contact" />            	
            </section>
        </section>

        <section ref="./Projects_Main.html" title="Projects">
            <section ref="./Projects_Workbench.html" title="The Project Workbench" />
            <section ref="./Projects_How_To.html" title="Projects: How To..." />
        </section>
</DCF>

</assistantconfig>

