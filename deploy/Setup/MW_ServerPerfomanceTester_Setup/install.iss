;
; SokratesXP installation script file (Inno Setup)
;
; SEE THE DOCUMENTATION FOR DETAILS ON CREATING .ISS SCRIPT FILES!

[Setup]
AppName=Sokrates Server Perfomance Tester
AppVerName=Sokrates Server Perfomance Tester
AppendDefaultDirName=false
AppCopyright=Copyright � 2014 Helix Business Soft development team
DefaultDirName={pf}\Sokrates Server Perfomance Tester
;it is possible to skip creating program group
AllowNoIcons=yes
PrivilegesRequired=none
UsePreviousAppDir=no
SetupIconFile=Sokrates.ico
LicenseFile=license.txt
OutputBaseFilename=ServerPerfomanceTester_Setup



[Files]
;Core:
Source: "ServerPerformanceTester.exe"; DestDir: "{app}"; Flags: promptifolder; 
Source: "*.dll"; DestDir: "{app}"; Flags: ignoreversion;
Source: "unzip.exe"; DestDir: "{app}"; Flags: ignoreversion;
Source: "zip.exe"; DestDir: "{app}"; Flags: ignoreversion;
Source: "mime_example_1.txt"; DestDir: "{app}"; Flags: ignoreversion;

Source: "platforms\*.*"; DestDir: "{app}\platforms"; Flags: recursesubdirs ignoreversion;
Source: "imageformats\*.*"; DestDir: "{app}\imageformats"; Flags: recursesubdirs ignoreversion;
;Source: "playlistformats\*.*"; DestDir: "{app}\playlistformats"; Flags: recursesubdirs ignoreversion;
Source: "printsupport\*.*"; DestDir: "{app}\printsupport"; Flags: recursesubdirs ignoreversion;

;Side by Side, private assembly by MS guidelines (this will fail to work on Win98,Me)
;Do not use vcredist_x86.exe, because we do not want to make our installation to modify system files
;All manifests & dll's must be copied into plugins subfolders (coz they can not use it from there)
Source: "Microsoft.CRT\*.*"; DestDir: "{app}"; Flags: recursesubdirs;
Source: "Microsoft.CRT\*.*"; DestDir: "{app}\imageformats"; Flags: recursesubdirs;
Source: "Microsoft.CRT\*.*"; DestDir: "{app}\platforms"; Flags: recursesubdirs;
Source: "Microsoft.CRT\*.*"; DestDir: "{app}\printsupport"; Flags: recursesubdirs;



;issue 1412: delete old templates:
[InstallDelete]
Type: filesandordirs; Name: "{app}\platforms";
Type: filesandordirs; Name: "{app}\imageformats";
Type: filesandordirs; Name: "{app}\printsupport";


[UninstallDelete]
Type: filesandordirs; Name: "{app}";


[Run]
Filename: "{app}\ServerPerformanceTester.exe";  Description: "Launch application"; WorkingDir: "{app}"; Flags: postinstall nowait skipifsilent





