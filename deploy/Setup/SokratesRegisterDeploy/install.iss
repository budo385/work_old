;
; SokratesXP installation script file (Inno Setup)
;
; SEE THE DOCUMENTATION FOR DETAILS ON CREATING .ISS SCRIPT FILES!
; 01.07.2010: Only one Sokrates_register.exe + network.cfg + sokrates.key will be copied from same dir



[Setup]
OutputBaseFilename=sokrates_register
AppName=SOKRATES Register
AppVerName=SOKRATES Register
AppCopyright=Copyright � 2013 Helix Business Soft development team
DefaultDirName={tmp}
DefaultGroupName=SOKRATES Register
;it is possible to skip creating program group
AllowNoIcons=yes
PrivilegesRequired=none
Uninstallable=yes
DisableDirPage=yes
DisableFinishedPage=yes
DisableProgramGroupPage=yes
DisableReadyPage=yes
DisableReadyMemo=yes
SetupIconFile=sokrates.ico

[Messages]
WelcomeLabel1=Register your SOKRATES� software
WelcomeLabel2=
ClickNext=Click Next to continue, or Cancel to exit the Registration Utility
ExitSetupMessage= Are you sure you want to cancel the registration process?


[Files]
;Core:
Source: "Sokrates_Register.exe"; DestDir: "{app}"; Flags: promptifolder;
;//Source: "QtCore4.dll"; DestDir: "{app}"; Flags: ignoreversion;
;//Source: "QtGui4.dll"; DestDir: "{app}"; Flags: ignoreversion;
;//Source: "sokrates.key"; DestDir: "{app}"; Flags: ignoreversion skipifsourcedoesntexist;
;//Source: "network_connections.cfg"; DestDir: "{app}"; Flags: ignoreversion skipifsourcedoesntexist;


;Side by Side, private assembly by MS guidelines (this will fail to work on Win98,Me)
;Do not use vcredist_x86.exe, because we do not want to make our installation to modify system files
;All manifests & dll's must be copied into plugins subfolders (coz they can not use it from there)
;//Source: "Microsoft.VC80.CRT\*.*"; DestDir: "{app}"; Flags: recursesubdirs;


[Run]
;Filename: "{app}\uninstall.bat";  WorkingDir: "{app}"; Flags: waituntilterminated
Filename: "{app}\Sokrates_Register.exe";  WorkingDir: "{app}"; Flags: waituntilterminated  hidewizard; Check: CopyDLL


[Code]
function CopyFiles(regValue: String): Boolean;
var
	installDir: String;
	sourceDir: String;
begin

      installDir:= ExpandConstant('{app}');
      
     if FileCopy(regValue+'\Qt5Core.dll', installDir++'\Qt5Core.dll', False)=False then
     begin
			Result := False;
			Exit;
     end
     if FileCopy(regValue+'\Qt5Gui.dll', installDir++'\Qt5Gui.dll', False)=False then
     begin
			Result := False;
			Exit;
     end
	 if FileCopy(regValue+'\Qt5Widgets.dll', installDir++'\Qt5Widgets.dll', False)=False then
     begin
			Result := False;
			Exit;
     end
     if FileCopy(regValue+'\msvcp100.dll', installDir++'\msvcp100.dll', False)=False then
     begin
			Result := False;
			Exit;
     end
     if FileCopy(regValue+'\msvcr100.dll', installDir++'\msvcr100.dll', False)=False then
     begin
			Result := False;
			Exit;
     end
     if FileCopy(regValue+'\icudt51.dll', installDir++'\icudt51.dll', False)=False then
     begin
			Result := False;
			Exit;
     end
     if FileCopy(regValue+'\icuin51.dll', installDir++'\icuin51.dll', False)=False then
     begin
			Result := False;
			Exit;
     end
     if FileCopy(regValue+'\icuuc51.dll', installDir++'\icuuc51.dll', False)=False then
     begin
			Result := False;
			Exit;
     end
    if FileCopy(regValue+'\libGLESv2.dll', installDir++'\libGLESv2.dll', False)=False then
     begin
			Result := False;
			Exit;
     end
    if FileCopy(regValue+'\libEGL.dll', installDir++'\libEGL.dll', False)=False then
     begin
			Result := False;
			Exit;
     end
     

     //create dir and copy
     CreateDir(installDir++'\platforms');
     FileCopy(regValue+'\platforms\qminimal.dll', installDir++'\platforms\qminimal.dll', False);
     FileCopy(regValue+'\platforms\qoffscreen.dll', installDir++'\platforms\qoffscreen.dll', False);
     FileCopy(regValue+'\platforms\qwindows.dll', installDir++'\platforms\qwindows.dll', False);
     
     ;//BT: 01.07.2010: use local files from SRC:
     sourceDir :=ExpandConstant('{src}');
     FileCopy(sourceDir+'\network_connections.cfg', installDir+'\network_connections.cfg', False);
     FileCopy(sourceDir+'\sokrates.key', installDir+'\sokrates.key', False);

     Result := True;
end;


function CopyDLL(): Boolean;
var
	regValue: String;
begin

  //find SOKRATES dir installation
  if RegQueryStringValue(HKEY_LOCAL_MACHINE, 'Software\Helix Business Soft\SOKRATES Communicator Team Edition\Settings','InstallPath', regValue) then
  begin
			Result := CopyFiles(regValue);
			if Result = False then
        MsgBox('Failed to find SOKRATES installation. Aborting registration!', mbInformation, MB_OK);
			Exit;
  end
  if RegQueryStringValue(HKEY_LOCAL_MACHINE, 'Software\Helix Business Soft\SOKRATES Communicator Personal Edition\Settings','InstallPath', regValue) then
  begin
			Result := CopyFiles(regValue);
			if Result = False then
        MsgBox('Failed to find SOKRATES installation. Aborting registration!', mbInformation, MB_OK);
			Exit;
  end
  if RegQueryStringValue(HKEY_LOCAL_MACHINE, 'Software\Helix Business Soft\SOKRATES Communicator Business Edition\Settings','InstallPath', regValue) then
  begin
			Result := CopyFiles(regValue);
			if Result = False then
        MsgBox('Failed to copy files from SOKRATES installation directory. Aborting registration!', mbInformation, MB_OK);
			Exit;
  end

  MsgBox('Failed to find SOKRATES installation. No registry entry found. Aborting registration!', mbInformation, MB_OK);
  Result := False;
end;
  







