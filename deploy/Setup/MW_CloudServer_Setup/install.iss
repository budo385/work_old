;
; SokratesXP installation script file (Inno Setup)
;
; SEE THE DOCUMENTATION FOR DETAILS ON CREATING .ISS SCRIPT FILES!

[Setup]
AppName=Mailegant Cloud Server
AppVerName=Mailegant Cloud Server
AppendDefaultDirName=false
AppCopyright=Copyright � 2014 Helix Business Soft development team
DefaultDirName={pf}\Mailegant Cloud Server
;it is possible to skip creating program group
AllowNoIcons=yes
PrivilegesRequired=none
UsePreviousAppDir=no
SetupIconFile=mailegant.ico
LicenseFile=license.txt
OutputBaseFilename=mw_cloudserver_setup



[Files]
;Core:
Source: "mailegant_connector.exe"; DestDir: "{app}"; Flags: promptifolder; BeforeInstall: StopServer;
Source: "*.dll"; DestDir: "{app}"; Flags: ignoreversion;
Source: "unzip.exe"; DestDir: "{app}"; Flags: ignoreversion;
Source: "zip.exe"; DestDir: "{app}"; Flags: ignoreversion;

Source: "sqldrivers\*.*"; DestDir: "{app}\sqldrivers"; Flags: recursesubdirs ignoreversion;
Source: "platforms\*.*"; DestDir: "{app}\platforms"; Flags: recursesubdirs ignoreversion;
Source: "webservices\*.*"; DestDir: "{app}\webservices"; Flags: recursesubdirs;

Source: "firebird\*.*"; DestDir: "{app}\firebird"; Flags: recursesubdirs ignoreversion;
Source: "firebird\bin\isql.exe"; DestDir: "{app}"; Flags: ignoreversion;
Source: "firebird\bin\gbak.exe"; DestDir: "{app}"; Flags: ignoreversion;
Source: "firebird\bin\nbackup.exe"; DestDir: "{app}"; Flags: ignoreversion;
Source: "firebird\bin\fbclient.dll"; DestDir: "{app}"; Flags: ignoreversion;
Source: "firebird\firebird.msg"; DestDir: "{app}"; Flags: ignoreversion;
Source: "firebird\bin\msvcp80.dll"; DestDir: "{app}"; Flags: ignoreversion;
Source: "firebird\bin\msvcr80.dll"; DestDir: "{app}"; Flags: ignoreversion;
Source: "firebird\bin\Microsoft.VC80.CRT.manifest"; DestDir: "{app}"; Flags: ignoreversion;


;Side by Side, private assembly by MS guidelines (this will fail to work on Win98,Me)
;Do not use vcredist_x86.exe, because we do not want to make our installation to modify system files
;All manifests & dll's must be copied into plugins subfolders (coz they can not use it from there)
Source: "Microsoft.CRT\*.*"; DestDir: "{app}"; Flags: recursesubdirs;
Source: "Microsoft.CRT\*.*"; DestDir: "{app}\sqldrivers"; Flags: recursesubdirs;
Source: "Microsoft.CRT\*.*"; DestDir: "{app}\imageformats"; Flags: recursesubdirs;
Source: "Microsoft.CRT\*.*"; DestDir: "{app}\platforms"; Flags: recursesubdirs;
Source: "Microsoft.CRT\*.*"; DestDir: "{app}\firebird\udf"; Flags: recursesubdirs;

;Reports:

;Configuration   B.T. 2014_03_27: new SSL config files:
Source: "settings\server.ca-bundle"; DestDir: "{app}\settings"; Flags: ignoreversion;
Source: "settings\server.cert"; DestDir: "{app}\settings"; Flags: ignoreversion;
Source: "settings\server.pkey"; DestDir: "{app}\settings"; Flags: ignoreversion;
Source: "settings\app_server.ini"; DestDir: "{app}\settings"; Flags: onlyifdoesntexist;

;issue 1412: delete old templates:
[InstallDelete]
Type: filesandordirs; Name: "{app}\sqldrivers";
Type: filesandordirs; Name: "{app}\platforms";

[UninstallDelete]
Type: filesandordirs; Name: "{app}";


[Run]
Filename: "{app}\start_server.bat";  Description: "Launch application"; WorkingDir: "{app}"; Flags: postinstall nowait skipifsilent

[UninstallRun]
Filename: "{app}\stop_server.bat";  WorkingDir: "{app}"; Flags: waituntilterminated






[Code]






procedure StopServer();
var
  ErrorCode: Integer;
begin

  if not FileExists(ExpandConstant('{app}\stop_server.bat')) then
  begin
    Exit;
  end;

  if not ShellExec('', ExpandConstant('{app}\stop_server.bat'),'', ExpandConstant('{app}'), SW_HIDE, ewWaitUntilTerminated, ErrorCode) then
  begin
      MsgBox('Failed to stop application server!', mbInformation, MB_OK);
  end;

end;




//----------------------------
// Custom  pages
//----------------------------

 var
    txtPort: TEdit;
    txtServiceName: TEdit;
    txtURL: TEdit; 
    labURL: TLabel; 
    nIsSSL: Integer;
    chkSSL: TCheckBox; 

    txtSMTP_Server: TEdit; 
    txtSMTP_Port: TEdit;
    txtSMTP_EmailAddress: TEdit;
    txtSMTP_User: TEdit;
    txtSMTP_Password: TEdit;

            

//create isntall, uninstall.bat
procedure CurStepChanged(CurStep: TSetupStep);
var
	strInstallPath: String;
  strCmdString: String;
  strIniFile: String;
	
begin
 
  if CurStep = ssPostInstall  then
  begin
	
    strInstallPath := ExpandConstant('{app}');
    strCmdString := 'mailegant_connector.exe -i '+  txtServiceName.text + #13#10;
    strCmdString := strCmdString + 'NET START "Mailegant Connector - '+  txtServiceName.text+'"';
    SaveStringToFile(strInstallPath+'\start_server.bat', strCmdString, False);

    strCmdString := 'mailegant_connector.exe -t '+  txtServiceName.text + #13#10;
    strCmdString := strCmdString + 'mailegant_connector.exe -u '+  txtServiceName.text + #13#10;
    SaveStringToFile(strInstallPath+'\stop_server.bat', strCmdString, False);

    //save defaults to ini file:
    strIniFile  := ExpandConstant('{app}')+'\settings\app_server.ini';
    SetIniString('Main', 'AppServerPort', txtPort.text, strIniFile);
    SetIniString('Main', 'Service Descriptor', txtServiceName.text, strIniFile);
    SetIniString('Main', 'Host domain', txtURL.text, strIniFile);
    if(chkSSL.checked) then
      begin
        SetIniString('SSL', 'SSLMode', '1', strIniFile);
      end
    else
      begin
        SetIniString('SSL', 'SSLMode', '0', strIniFile);
      end

     //save smtp:
     SetIniString('SMTP', 'Mail SMTP Server', txtSMTP_Server.text, strIniFile);
     SetIniString('SMTP', 'Server Port', txtSMTP_Port.text, strIniFile);
     SetIniString('SMTP', 'Server Mail Address', txtSMTP_EmailAddress.text, strIniFile);
     SetIniString('SMTP', 'Server Mail User', txtSMTP_User.text, strIniFile);
     SetIniString('SMTP', 'Server Mail Password', txtSMTP_Password.text, strIniFile);
  end


  
end;

// read from INI defaults:
procedure Skeleton_Activate(Page: TWizardPage);
var
  strIniFile: String;

 begin
  strIniFile  := ExpandConstant('{app}')+'\settings\app_server.ini';
  txtPort.text :=GetIniString('Main', 'AppServerPort', '12000', strIniFile);
  txtServiceName.text :=GetIniString('Main', 'Service Descriptor', 'default', strIniFile);
  txtURL.text :=GetIniString('Main', 'Host domain', 'everxconnect.com', strIniFile);

  if(GetIniString('SSL', 'SSLMode', '0', strIniFile)='0') then
    begin
      nIsSSL :=0;
    end
  else
    begin
      nIsSSL :=1;
    end
 
 if( nIsSSL = 1) then
   begin
      labURL.Caption := 'Admin URL: https://'+txtURL.text+':'+txtPort.text+'/stat/index.html';
      chkSSL.checked := true;
   end
 else
   begin
      labURL.Caption := 'Admin URL: http://'+txtURL.text+':'+txtPort.text+'/stat/index.html';
      chkSSL.checked := false;
   end
end;


// read from INI defaults:
procedure Skeleton_Activate_Page2(Page: TWizardPage);
var
  strIniFile: String;
begin
  strIniFile  := ExpandConstant('{app}')+'\settings\app_server.ini';
  txtSMTP_Server.text :=GetIniString('SMTP', 'Mail SMTP Server', '', strIniFile);
  txtSMTP_Port.text :=GetIniString('SMTP', 'Server Port', '25', strIniFile);
  txtSMTP_EmailAddress.text :=GetIniString('SMTP', 'Server Mail Address', '', strIniFile);
  txtSMTP_User.text :=GetIniString('SMTP', 'Server Mail User', '', strIniFile);
  txtSMTP_Password.text :=GetIniString('SMTP', 'Server Mail Password', '', strIniFile);
end;



procedure OnURLChanged(Sender: TObject);
begin

 if( chkSSL.checked = true)  then
   begin
      labURL.Caption := 'Admin URL: https://'+txtURL.text+':'+txtPort.text+'/stat/index.html';
      nIsSSL:=1;
   end
 else
   begin
      labURL.Caption := 'Admin URL: http://'+txtURL.text+':'+txtPort.text+'/stat/index.html';
      nIsSSL:=0;
   end
end;


        

function CreatePage_Page1(PreviousPageId: Integer): Integer;
var
    Page: TWizardPage;
    
    cLabel0: TLabel;
    cLabel1: TLabel;
    cLabel2: TLabel;
    cLabel3: TLabel;
    
begin
    Page := CreateCustomPage(PreviousPageId, 'Setup port and service name', 'Define port for server to listen on, service name that is unique on local host and host domain');

    // add controls to the page here
    
  cLabel0 := TLabel.Create(Page);
	with cLabel0 do
	begin
		Caption := 'SSL:';
		Width	:= Page.SurfaceWidth;
		Parent 	:= Page.Surface; 
		Left 	:= ScaleX(10);
		Top 	:= ScaleY(20);
		Width 	:= ScaleX(160);
		Height 	:= ScaleY(17);
    end;

  chkSSL := TCheckBox.Create(Page);
	with chkSSL do
	begin
		Caption := '';
		Width	:= Page.SurfaceWidth;
		Parent 	:= Page.Surface; 
		Left 	:= ScaleX(180);
		Top 	:= ScaleY(20);
		Width 	:= ScaleX(60);
		Height 	:= ScaleY(17);
    OnClick := @OnURLChanged;
    end;

  cLabel1 := TLabel.Create(Page);
	with cLabel1 do
	begin
		Caption := 'Port:';
		Width	:= Page.SurfaceWidth;
		Parent 	:= Page.Surface; 
		Left 	:= ScaleX(10);
		Top 	:= ScaleY(44);
		Width 	:= ScaleX(160);
		Height 	:= ScaleY(17);
    end;
   
	txtPort := TEdit.Create(Page);
	with txtPort do
	begin
		Width	:= Page.SurfaceWidth;
		Parent 	:= Page.Surface; 
		Left 	:= ScaleX(180);
		Top 	:= ScaleY(44);
		Width 	:= ScaleX(200);
		Height 	:= ScaleY(17);
    OnChange:= @OnURLChanged;
    end;

  cLabel2 := TLabel.Create(Page);
	with cLabel2 do
	begin
		Caption := 'Service name:';
		Width	:= Page.SurfaceWidth;
		Parent 	:= Page.Surface; 
		Left 	:= ScaleX(10);
		Top 	:= ScaleY(68);
		Width 	:= ScaleX(160);
		Height 	:= ScaleY(17);
    end;

	txtServiceName := TEdit.Create(Page);
	with txtServiceName do
	begin
		Width	:= Page.SurfaceWidth;
		Parent 	:= Page.Surface; 
		Left 	:= ScaleX(180);
		Top 	:= ScaleY(68);
		Width 	:= ScaleX(200);
		Height 	:= ScaleY(17);
  end;

  cLabel3 := TLabel.Create(Page);
	with cLabel3 do
	begin
		Caption := 'Domain (e.g. everxconnect.com):';
		Width	:= Page.SurfaceWidth;
		Parent 	:= Page.Surface; 
		Left 	:= ScaleX(10);
		Top 	:= ScaleY(92);
		Width 	:= ScaleX(160);
		Height 	:= ScaleY(17);
    end;

	txtURL := TEdit.Create(Page);
	with txtURL do
	begin
		Width	:= Page.SurfaceWidth;
		Parent 	:= Page.Surface; 
		Left 	:= ScaleX(180);
		Top 	:= ScaleY(92);
		Width 	:= ScaleX(200);
		Height 	:= ScaleY(17);
    OnChange:= @OnURLChanged;
  end;

  labURL := TLabel.Create(Page);
	with labURL do
	begin
		Caption := 'Admin URL: everxconnect.com:12000/registration';
		Width	:= Page.SurfaceWidth;
		Parent 	:= Page.Surface; 
		Left 	:= ScaleX(10);
		Top 	:= ScaleY(140);
		Width 	:= ScaleX(400);
		Height 	:= ScaleY(17);
    end;

  

    Page.OnActivate := @Skeleton_Activate;
    //Page.OnShouldSkipPage := @Skeleton_ShouldSkipPage;
    //Page.OnBackButtonClick := @Skeleton_BackButtonClick;
    //Page.OnNextButtonClick := @Skeleton_NextButtonClick;
    //Page.OnCancelButtonClick := @Skeleton_CancelButtonClick;
    Result := Page.ID;
   
  txtPort.text        :=  '12000';
  txtServiceName.text :=  'default';
  txtURL.text         :=  'everxconnect.com';
    
end;



function CreatePage_Page2(PreviousPageId: Integer): Integer;
var
    Page: TWizardPage;
    
    cLabel0: TLabel;
    cLabel1: TLabel;
    cLabel2: TLabel;
    cLabel3: TLabel;
    cLabel4: TLabel;
    
begin
    Page := CreateCustomPage(PreviousPageId, 'Setup server SMTP account', 'Define SMTP email settings for sending confirmation emails to the clients');

    // add controls to the page here
    
  cLabel0 := TLabel.Create(Page);
	with cLabel0 do
	begin
		Caption := 'SMTP mail server:';
		Width	:= Page.SurfaceWidth;
		Parent 	:= Page.Surface; 
		Left 	:= ScaleX(10);
		Top 	:= ScaleY(20);
		Width 	:= ScaleX(160);
		Height 	:= ScaleY(17);
    end;

	txtSMTP_Server := TEdit.Create(Page);
	with txtSMTP_Server do
	begin
		Width	:= Page.SurfaceWidth;
		Parent 	:= Page.Surface; 
		Left 	:= ScaleX(180);
		Top 	:= ScaleY(20);
		Width 	:= ScaleX(200);
		Height 	:= ScaleY(17);
    end;

  cLabel1 := TLabel.Create(Page);
	with cLabel1 do
	begin
		Caption := 'SMTP Port:';
		Width	:= Page.SurfaceWidth;
		Parent 	:= Page.Surface; 
		Left 	:= ScaleX(10);
		Top 	:= ScaleY(44);
		Width 	:= ScaleX(160);
		Height 	:= ScaleY(17);
    end;
   
	txtSMTP_Port := TEdit.Create(Page);
	with txtSMTP_Port do
	begin
		Width	:= Page.SurfaceWidth;
		Parent 	:= Page.Surface; 
		Left 	:= ScaleX(180);
		Top 	:= ScaleY(44);
		Width 	:= ScaleX(200);
		Height 	:= ScaleY(17);
    end;

  cLabel2 := TLabel.Create(Page);
	with cLabel2 do
	begin
		Caption := 'Email Address:';
		Width	:= Page.SurfaceWidth;
		Parent 	:= Page.Surface; 
		Left 	:= ScaleX(10);
		Top 	:= ScaleY(68);
		Width 	:= ScaleX(160);
		Height 	:= ScaleY(17);
    end;

	txtSMTP_EmailAddress := TEdit.Create(Page);
	with txtSMTP_EmailAddress do
	begin
		Width	:= Page.SurfaceWidth;
		Parent 	:= Page.Surface; 
		Left 	:= ScaleX(180);
		Top 	:= ScaleY(68);
		Width 	:= ScaleX(200);
		Height 	:= ScaleY(17);
  end;

  cLabel3 := TLabel.Create(Page);
	with cLabel3 do
	begin
		Caption := 'SMTP User:';
		Width	:= Page.SurfaceWidth;
		Parent 	:= Page.Surface; 
		Left 	:= ScaleX(10);
		Top 	:= ScaleY(92);
		Width 	:= ScaleX(160);
		Height 	:= ScaleY(17);
    end;

	txtSMTP_User := TEdit.Create(Page);
	with txtSMTP_User do
	begin
		Width	:= Page.SurfaceWidth;
		Parent 	:= Page.Surface; 
		Left 	:= ScaleX(180);
		Top 	:= ScaleY(92);
		Width 	:= ScaleX(200);
		Height 	:= ScaleY(17);
  end;

  cLabel4 := TLabel.Create(Page);
	with cLabel4 do
	begin
		Caption := 'SMTP Password:';
		Width	:= Page.SurfaceWidth;
		Parent 	:= Page.Surface; 
		Left 	:= ScaleX(10);
		Top 	:= ScaleY(116);
		Width 	:= ScaleX(160);
		Height 	:= ScaleY(17);
    end;

	txtSMTP_Password := TEdit.Create(Page);
	with txtSMTP_Password do
	begin
		Width	:= Page.SurfaceWidth;
		Parent 	:= Page.Surface; 
		Left 	:= ScaleX(180);
		Top 	:= ScaleY(116);
		Width 	:= ScaleX(200);
		Height 	:= ScaleY(17);
  end;

  

    Page.OnActivate := @Skeleton_Activate_Page2;
    //Page.OnShouldSkipPage := @Skeleton_ShouldSkipPage;
    //Page.OnBackButtonClick := @Skeleton_BackButtonClick;
    //Page.OnNextButtonClick := @Skeleton_NextButtonClick;
    //Page.OnCancelButtonClick := @Skeleton_CancelButtonClick;
    Result := Page.ID;
   
  txtPort.text        :=  '25';
    
end;


procedure InitializeWizard();
var
 LastId: Integer;
 
begin

  LastId := wpSelectDir;
	LastId := CreatePage_Page1(LastId); // add custom page after the select dir
  LastId := CreatePage_Page2(LastId); // add another custom page
       
end;


