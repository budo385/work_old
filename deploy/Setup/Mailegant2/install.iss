;
; SokratesXP installation script file (Inno Setup)
;
; SEE THE DOCUMENTATION FOR DETAILS ON CREATING .ISS SCRIPT FILES!

[Setup]
AppName=Mailegant
AppVerName=Mailegant
AppCopyright=Copyright � 2013 Inimated
DefaultDirName={pf}\Mailegant
DefaultGroupName=Mailegant
UninstallDisplayIcon={app}\mailegant.exe
;it is possible to skip creating program group
AllowNoIcons=yes
PrivilegesRequired=admin
UsePreviousAppDir=no
SetupIconFile=mailegant.ico
OutputBaseFilename=mailegant_setup

;AppMutex=SOKRATES_AppServer

[Messages]
SelectDirBrowseLabel=To continue, click Next. If you would like to select a different folder, click Browse.%nYou'll need admin rights if you want to install the program within "Program Files" folder.
WelcomeLabel2=This will install [name/ver] on your computer.%n%nWARNING: Stop application server instances before before continuing.

[Tasks]
Name: desktopicon; Description: "Create a &desktop icon"; GroupDescription: "Additional icons:";
Name: quicklaunchicon; Description: "Create a &Quick Launch icon"; GroupDescription: "Additional icons:"; Flags: unchecked
Name: startmenuicon; Description: "Create a &start menu icon"; GroupDescription: "Additional icons:"; Flags: unchecked

[Files]
;Core:
Source: "*.exe"; DestDir: "{app}"; Flags: recursesubdirs ignoreversion;
Source: "*.dll"; DestDir: "{app}"; Flags: ignoreversion;

Source: "imageformats\*.*"; DestDir: "{app}\imageformats"; Flags: recursesubdirs ignoreversion;
Source: "platforms\*.*"; DestDir: "{app}\platforms"; Flags: recursesubdirs ignoreversion;
Source: "playlistformats\*.*"; DestDir: "{app}\playlistformats"; Flags: recursesubdirs ignoreversion;
Source: "printsupport\*.*"; DestDir: "{app}\printsupport"; Flags: recursesubdirs ignoreversion;
Source: "qmltooling\*.*"; DestDir: "{app}\qmltooling"; Flags: recursesubdirs ignoreversion;
Source: "sqldrivers\*.*"; DestDir: "{app}\sqldrivers"; Flags: recursesubdirs ignoreversion;
Source: "sensors\*.*"; DestDir: "{app}\sensors"; Flags: recursesubdirs ignoreversion;


Source: "Microsoft.CRT\*.*"; DestDir: "{app}"; Flags: recursesubdirs;
Source: "Microsoft.CRT\*.*"; DestDir: "{app}\sqldrivers"; Flags: recursesubdirs;
Source: "Microsoft.CRT\*.*"; DestDir: "{app}\imageformats"; Flags: recursesubdirs;
Source: "Microsoft.CRT\*.*"; DestDir: "{app}\platforms"; Flags: recursesubdirs;
Source: "Microsoft.CRT\*.*"; DestDir: "{app}\playlistformats"; Flags: recursesubdirs;
Source: "Microsoft.CRT\*.*"; DestDir: "{app}\printsupport"; Flags: recursesubdirs;
Source: "Microsoft.CRT\*.*"; DestDir: "{app}\qmltooling"; Flags: recursesubdirs;
Source: "Microsoft.CRT\*.*"; DestDir: "{app}\sensors"; Flags: recursesubdirs;


[Registry]
;register installation path->remove on deinstall
Root: HKLM; Subkey: "Software\iNimated\Mailegant\Settings"; ValueType: string; ValueName: "InstallPath"; ValueData: "{app}"; Flags: uninsdeletekey;

[UninstallDelete]
Type: filesandordirs; Name: "{app}";

[Icons]
Name: "{group}\Mailegant"; Filename: "{app}\mailegant.exe"; WorkingDir: "{app}";
Name: "{group}\Uninstall Mailegant"; Filename: "{uninstallexe}";
Name: "{userappdata}\Microsoft\Internet Explorer\Quick Launch\Mailegant"; Filename: "{app}\mailegant.exe"; Tasks: quicklaunchicon;  WorkingDir: "{app}";
Name: "{userdesktop}\Mailegant"; Filename: "{app}\mailegant.exe"; Tasks: desktopicon; WorkingDir: "{app}";
Name: "{userstartmenu}\Mailegant"; Filename: "{app}\mailegant.exe"; Tasks: startmenuicon; WorkingDir: "{app}";

[Run]
Filename: "{app}\mailegant.exe";  Description: "Launch application"; WorkingDir: "{app}"; Flags: postinstall nowait skipifsilent


