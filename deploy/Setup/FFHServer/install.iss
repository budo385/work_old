[Setup]
AppName=EverPICs Bridge
AppVerName=EverPICs Bridge
AppendDefaultDirName=false
AppCopyright=Copyright � 2014 Helix Business Soft development team
DefaultDirName={code:DefaultInstallPath}
DefaultGroupName=EverPICs Bridge
UninstallDisplayIcon={app}\everPICs-Bridge.exe
;it is possible to skip creating program group
AllowNoIcons=yes
PrivilegesRequired=admin
UsePreviousAppDir=no
SetupIconFile=Sokrates.ico
LicenseFile=license.txt
OutputBaseFilename=EverPICsBridgeSetup

;AppMutex=SOKRATES_AppServer

[Messages]
SelectDirBrowseLabel=To continue, click Next. If you would like to select a different folder, click Browse.%nYou'll need admin rights if you want to install the program within "Program Files" folder.
WelcomeLabel2=This will install [name/ver] on your computer.%n%nWARNING: Stop application server instances before before continuing.

[Tasks]
Name: desktopicon; Description: "Create a &desktop icon"; GroupDescription: "Additional icons:";
Name: quicklaunchicon; Description: "Create a &Quick Launch icon"; GroupDescription: "Additional icons:"; Flags: unchecked
Name: startmenuicon; Description: "Create a &start menu icon"; GroupDescription: "Additional icons:"; Flags: unchecked

[Files]
;Core:
Source: "everPICs-Service.exe"; DestDir: "{app}"; Flags: promptifolder; BeforeInstall: StopServer;
Source: "everPICs-Bridge.exe"; DestDir: "{app}"; Flags: ignoreversion;
Source: "everPICs-Bridge_no_admin.exe"; DestDir: "{app}"; Flags: ignoreversion;
Source: "GenTool.exe"; DestDir: "{app}"; Flags: ignoreversion;
Source: "Grant.exe"; DestDir: "{app}"; Flags: ignoreversion;
Source: "RawLib.exe"; DestDir: "{app}"; Flags: ignoreversion;
Source: "uninstall.bat"; DestDir: "{app}"; Flags: ignoreversion;
Source: "unregister.bat"; DestDir: "{app}"; Flags: ignoreversion;
Source: "*.dll"; DestDir: "{app}"; Flags: ignoreversion;
;Source: "webservices\*.*"; DestDir: "{app}\webservices"; Flags: recursesubdirs;
Source: "webdata.bin"; DestDir: "{app}"; Flags: ignoreversion;
Source: "imageformats\*.*"; DestDir: "{app}\imageformats"; Flags: recursesubdirs;
Source: "sqldrivers\*.*"; DestDir: "{app}\sqldrivers"; Flags: recursesubdirs ignoreversion;
Source: "platforms\*.*"; DestDir: "{app}\platforms"; Flags: recursesubdirs ignoreversion;
Source: "playlistformats\*.*"; DestDir: "{app}\playlistformats"; Flags: recursesubdirs ignoreversion;
Source: "printsupport\*.*"; DestDir: "{app}\printsupport"; Flags: recursesubdirs ignoreversion;
Source: "qmltooling\*.*"; DestDir: "{app}\qmltooling"; Flags: recursesubdirs ignoreversion;

;Side by Side, private assembly by MS guidelines (this will fail to work on Win98,Me)
;Do not use vcredist_x86.exe, because we do not want to make our installation to modify system files
;All manifests & dll's must be copied into plugins subfolders (coz they can not use it from there)
Source: "Microsoft.CRT\*.*"; DestDir: "{app}"; Flags: recursesubdirs;
Source: "Microsoft.CRT\*.*"; DestDir: "{app}\sqldrivers"; Flags: recursesubdirs;
Source: "Microsoft.CRT\*.*"; DestDir: "{app}\imageformats"; Flags: recursesubdirs;
Source: "Microsoft.CRT\*.*"; DestDir: "{app}\platforms"; Flags: recursesubdirs;
Source: "Microsoft.CRT\*.*"; DestDir: "{app}\playlistformats"; Flags: recursesubdirs;
Source: "Microsoft.CRT\*.*"; DestDir: "{app}\printsupport"; Flags: recursesubdirs;
Source: "Microsoft.CRT\*.*"; DestDir: "{app}\qmltooling"; Flags: recursesubdirs;

Source: "template_settings\*.*"; DestDir: "{app}\template_settings"; Flags: ignoreversion recursesubdirs;
Source: "EXB_Manager_de.qm"; DestDir: "{app}"; Flags: ignoreversion; AfterInstall: SetLanguage();
Source: "zip.exe"; DestDir: "{app}"; Flags: ignoreversion;
Source: "unzip.exe"; DestDir: "{app}"; Flags: ignoreversion;


[UninstallDelete]
Type: filesandordirs; Name: "{app}";

[Icons]
Name: "{group}\EverPICs Bridge"; Filename: "{app}\everPICs-Bridge.exe"; WorkingDir: "{app}";
Name: "{group}\Uninstall EverPICs Bridge"; Filename: "{uninstallexe}";
Name: "{userappdata}\Microsoft\Internet Explorer\Quick Launch\EverPICs Bridge"; Filename: "{app}\everPICs-Bridge.exe"; Tasks: quicklaunchicon;  WorkingDir: "{app}";
Name: "{userdesktop}\EverPICs Bridge"; Filename: "{app}\everPICs-Bridge.exe"; Tasks: desktopicon; WorkingDir: "{app}";
Name: "{userstartmenu}\EverPICs Bridge"; Filename: "{app}\everPICs-Bridge.exe"; Tasks: startmenuicon; WorkingDir: "{app}";


[Run]
Filename: "{app}\everPICs-Bridge.exe";  Description: "Launch application"; WorkingDir: "{app}"; Flags: runascurrentuser postinstall nowait skipifsilent

[Registry]
Root: HKLM; Subkey: "Software\Helix Business Soft\EverPICs Bridge\Settings"; ValueType: string; ValueName: "InstallPath"; ValueData: "{app}"; Flags: uninsdeletekey;

[UninstallRun]
Filename: "{app}\uninstall.bat";  WorkingDir: "{app}"; Flags: waituntilterminated
Filename: "{app}\unregister.bat";  WorkingDir: "{app}"; Flags: waituntilterminated
;Filename: "{app}\firebird\bin\uninstall.bat";  Flags: waituntilterminated

[Code]
//if Vista or non admin user set to C:\
function GetInstallDirectory(Param: String): String;
var
	Version: TWindowsVersion;
begin
  GetWindowsVersionEx(Version);
	// On Windows Vista, must be placed out of user control:
	if Version.NTPlatform and (Version.Major > 6) then
	 begin
				Result :='C:';
	 end
	 else
	 begin
    	 Result := ExpandConstant('{pf}');
	 end
end;


//issue by MB: 1786
function DefaultInstallPath(Param: String): String;
var
	regValue: String;
begin

  //find SOKRATES dir installation
  if RegQueryStringValue(HKEY_LOCAL_MACHINE, 'Software\Helix Business Soft\EverPICs Bridge\Settings','InstallPath', regValue) then
  begin
			Result := regValue;
	end
	else
	begin
    	Result := ExpandConstant('{pf}\EverPICs Bridge');
	end

end;


procedure StopServer();
var
  ErrorCode: Integer;
begin

  if not FileExists(ExpandConstant('{app}\uninstall.bat')) then
  begin
    Exit;
  end;

  if not ShellExec('', ExpandConstant('{app}\uninstall.bat'),'', ExpandConstant('{app}'), SW_HIDE, ewWaitUntilTerminated, ErrorCode) then
  begin
      MsgBox('Failed to stop application server!', mbInformation, MB_OK);
  end;

end;



// Utility functions for Inno Setup
//   used to add/remove programs from the windows firewall rules
// Code originally from http://news.jrsoftware.org/news/innosetup/msg43799.html

const
  NET_FW_SCOPE_ALL = 0;
  NET_FW_IP_VERSION_ANY = 2;

procedure SetFirewallException(AppName,FileName:string);
var
  FirewallObject: Variant;
  FirewallManager: Variant;
  FirewallProfile: Variant;
begin
  try
    FirewallObject := CreateOleObject('HNetCfg.FwAuthorizedApplication');
    FirewallObject.ProcessImageFileName := FileName;
    FirewallObject.Name := AppName;
    FirewallObject.Scope := NET_FW_SCOPE_ALL;
    FirewallObject.IpVersion := NET_FW_IP_VERSION_ANY;
    FirewallObject.Enabled := True;
    FirewallManager := CreateOleObject('HNetCfg.FwMgr');
    FirewallProfile := FirewallManager.LocalPolicy.CurrentProfile;
    FirewallProfile.AuthorizedApplications.Add(FirewallObject);
  except
  end;
end;

procedure RemoveFirewallException( FileName:string );
var
  FirewallManager: Variant;
  FirewallProfile: Variant;
begin
  try
    FirewallManager := CreateOleObject('HNetCfg.FwMgr');
    FirewallProfile := FirewallManager.LocalPolicy.CurrentProfile;
    FireWallProfile.AuthorizedApplications.Remove(FileName);
  except
  end;
end;

procedure CurStepChanged(CurStep: TSetupStep);
begin
  if CurStep=ssPostInstall then
     SetFirewallException('EverPICs Bridge', ExpandConstant('{app}')+'\everPICs-Service.exe');
end;

procedure CurUninstallStepChanged(CurUninstallStep: TUninstallStep);
begin
  if CurUninstallStep=usPostUninstall then
     RemoveFirewallException(ExpandConstant('{app}')+'\everPICs-Service.exe');
end;


// set language
procedure SetLanguage();
var
	S: String;
	T: String;
	L: String;
           	
begin

	S := ExpandConstant('{src}');
	L := ExpandConstant('{language}');
	
	T := ExpandConstant('{app}');
	T := T+'\template_settings\client.ini';
		
	if L = 'de' then
			begin
				SetIniString('Main','LanguageCode','de',T);
				Exit;
			end	
	
end;
