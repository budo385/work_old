;
; SokratesXP installation script file (Inno Setup)
;
; SEE THE DOCUMENTATION FOR DETAILS ON CREATING .ISS SCRIPT FILES!

[Setup]
AppName=SOKRATES Application Server
AppVerName=SOKRATES Application Server
AppendDefaultDirName=false
AppCopyright=Copyright � 2014 Helix Business Soft development team
DefaultDirName={code:DefaultInstallPath}
DefaultGroupName=SOKRATES Application Server
UninstallDisplayIcon={app}\ApplicationServerConfig.exe
;it is possible to skip creating program group
AllowNoIcons=yes
PrivilegesRequired=none
UsePreviousAppDir=no
SetupIconFile=sokrates.ico
LicenseFile=license.txt
OutputBaseFilename=appserversetup

;AppMutex=SOKRATES_AppServer

[Messages]
SelectDirBrowseLabel=To continue, click Next. If you would like to select a different folder, click Browse.%nYou'll need admin rights if you want to install the program within "Program Files" folder.
WelcomeLabel2=This will install [name/ver] on your computer.%n%nWARNING: Stop application server instances before before continuing.

[Tasks]
Name: desktopicon; Description: "Create a &desktop icon"; GroupDescription: "Additional icons:";
Name: quicklaunchicon; Description: "Create a &Quick Launch icon"; GroupDescription: "Additional icons:"; Flags: unchecked
Name: startmenuicon; Description: "Create a &start menu icon"; GroupDescription: "Additional icons:"; Flags: unchecked

[Files]
;Core:
Source: "appserver.exe"; DestDir: "{app}"; Flags: promptifolder; BeforeInstall: StopServer;
Source: "appserver.exe"; DestDir: "{app}\template_data"; Flags: promptifolder;
Source: "template_data\*.*"; DestDir: "{app}\template_data"; Flags: recursesubdirs;
Source: "*.dll"; DestDir: "{app}"; Flags: ignoreversion;
Source: "AdminTool.exe"; DestDir: "{app}"; Flags: ignoreversion;
Source: "GenTool.exe"; DestDir: "{app}"; Flags: ignoreversion;
Source: "ApplicationServerConfig.exe"; DestDir: "{app}"; Flags: ignoreversion;
Source: "uninstall.bat"; DestDir: "{app}"; Flags: onlyifdoesntexist;
Source: "phonet.dll"; DestDir: "{app}"; Flags: ignoreversion;
Source: "ssleay32.dll"; DestDir: "{app}"; Flags: ignoreversion;
Source: "libeay32.dll"; DestDir: "{app}"; Flags: ignoreversion;
Source: "*.hed"; DestDir: "{app}"; Flags: ignoreversion;
Source: "unzip.exe"; DestDir: "{app}"; Flags: ignoreversion;
Source: "zip.exe"; DestDir: "{app}"; Flags: ignoreversion;
Source: "mimepp.dll"; DestDir: "{app}"; Flags: ignoreversion;
Source: "smailpp.dll"; DestDir: "{app}"; Flags: ignoreversion;

Source: "imageformats\*.*"; DestDir: "{app}\imageformats"; Flags: recursesubdirs ignoreversion;
Source: "sqldrivers\*.*"; DestDir: "{app}\sqldrivers"; Flags: recursesubdirs ignoreversion;
Source: "platforms\*.*"; DestDir: "{app}\platforms"; Flags: recursesubdirs ignoreversion;
Source: "webservices\*.*"; DestDir: "{app}\webservices"; Flags: recursesubdirs;
Source: "printsupport\*.*"; DestDir: "{app}\printsupport"; Flags: recursesubdirs;

Source: "firebird\*.*"; DestDir: "{app}\firebird"; Flags: recursesubdirs ignoreversion;
Source: "firebird\bin\isql.exe"; DestDir: "{app}"; Flags: ignoreversion;
Source: "firebird\bin\gbak.exe"; DestDir: "{app}"; Flags: ignoreversion;
Source: "firebird\bin\nbackup.exe"; DestDir: "{app}"; Flags: ignoreversion;
Source: "firebird\bin\fbclient.dll"; DestDir: "{app}"; Flags: ignoreversion;
Source: "firebird\firebird.msg"; DestDir: "{app}"; Flags: ignoreversion;
Source: "firebird\bin\msvcp80.dll"; DestDir: "{app}"; Flags: ignoreversion;
Source: "firebird\bin\msvcr80.dll"; DestDir: "{app}"; Flags: ignoreversion;
Source: "firebird\bin\Microsoft.VC80.CRT.manifest"; DestDir: "{app}"; Flags: ignoreversion;

;Side by Side, private assembly by MS guidelines (this will fail to work on Win98,Me)
;Do not use vcredist_x86.exe, because we do not want to make our installation to modify system files
;All manifests & dll's must be copied into plugins subfolders (coz they can not use it from there)
Source: "Microsoft.CRT\*.*"; DestDir: "{app}"; Flags: recursesubdirs;
Source: "Microsoft.CRT\*.*"; DestDir: "{app}\sqldrivers"; Flags: recursesubdirs;
Source: "Microsoft.CRT\*.*"; DestDir: "{app}\imageformats"; Flags: recursesubdirs;
Source: "Microsoft.CRT\*.*"; DestDir: "{app}\platforms"; Flags: recursesubdirs;
Source: "Microsoft.CRT\*.*"; DestDir: "{app}\firebird\udf"; Flags: recursesubdirs;

;Special Rest:
;Source: "SHW32.DLL"; DestDir: "{app}"; Flags: ignoreversion;

;Reports:

;Configuration   B.T. 2014_03_27: new SSL config files:
Source: "settings\sokrates.key"; DestDir: "{app}\settings"; Flags: onlyifdoesntexist;
Source: "settings\app_server.ini"; DestDir: "{app}\settings"; Flags: onlyifdoesntexist;
Source: "settings\server.ca-bundle"; DestDir: "{app}\settings"; Flags: ignoreversion;
Source: "settings\server.cert"; DestDir: "{app}\settings"; Flags: ignoreversion;
Source: "settings\server.pkey"; DestDir: "{app}\settings"; Flags: ignoreversion;

;for push:
Source: "settings\aps_prod_key_cert.pem"; DestDir: "{app}\settings"; Flags: ignoreversion;
Source: "settings\aps_prod_key_noenc.pem"; DestDir: "{app}\settings"; Flags: ignoreversion;
Source: "settings\AppleWWDRCA.cer"; DestDir: "{app}\settings"; Flags: ignoreversion;
Source: "settings\AppleIncRootCertificate.cer"; DestDir: "{app}\settings"; Flags: ignoreversion;



;issue 1412: delete old templates:
[InstallDelete]
Type: filesandordirs; Name: "{app}\template_data";
Type: filesandordirs; Name: "{app}\sqldrivers";
Type: filesandordirs; Name: "{app}\imageformats";
Type: filesandordirs; Name: "{app}\platforms";


[UninstallDelete]
Type: filesandordirs; Name: "{app}";


[Registry]
;register installation path->remove on deinstall
Root: HKLM; Subkey: "Software\Helix Business Soft\SOKRATES Application Server\Settings"; ValueType: string; ValueName: "InstallPath"; ValueData: "{app}"; Flags: uninsdeletekey;


[Icons]
Name: "{group}\SOKRATES Application Server"; Filename: "{app}\ApplicationServerConfig.exe"; WorkingDir: "{app}";
Name: "{group}\Uninstall SOKRATES Application Server"; Filename: "{uninstallexe}";
Name: "{userappdata}\Microsoft\Internet Explorer\Quick Launch\SOKRATES Application Server"; Filename: "{app}\ApplicationServerConfig.exe"; Tasks: quicklaunchicon;  WorkingDir: "{app}";
Name: "{userdesktop}\SOKRATES Application Server"; Filename: "{app}\ApplicationServerConfig.exe"; Tasks: desktopicon; WorkingDir: "{app}";
Name: "{userstartmenu}\SOKRATES Application Server"; Filename: "{app}\ApplicationServerConfig.exe"; Tasks: startmenuicon; WorkingDir: "{app}";



[Run]
Filename: "{app}\ApplicationServerConfig.exe";  Description: "Launch application"; WorkingDir: "{app}"; Flags: postinstall nowait skipifsilent

[UninstallRun]
Filename: "{app}\uninstall.bat";  WorkingDir: "{app}"; Flags: waituntilterminated
;Filename: "{app}\firebird\bin\uninstall.bat";  Flags: waituntilterminated

[Code]
//if Vista or non admin user set to C:\
function GetInstallDirectory(Param: String): String;
var
	Version: TWindowsVersion;
begin
  GetWindowsVersionEx(Version);
	// On Windows Vista, must be placed out of user control:
	if Version.NTPlatform and (Version.Major > 6) then
	 begin
				Result :='C:';
	 end
	 else
	 begin
    	 Result := ExpandConstant('{pf}');
	 end
end;


//issue by MB: 1786
function DefaultInstallPath(Param: String): String;
var
	regValue: String;
begin

  //find SOKRATES dir installation
  if RegQueryStringValue(HKEY_LOCAL_MACHINE, 'Software\Helix Business Soft\SOKRATES Application Server\Settings','InstallPath', regValue) then
  begin
			Result := regValue;
	end
	else
	begin
    	Result := ExpandConstant('{pf}\SOKRATES Application Server');
	end

end;


procedure StopServer();
var
  ErrorCode: Integer;
begin

  if not FileExists(ExpandConstant('{app}\uninstall.bat')) then
  begin
    Exit;
  end;

  if not ShellExec('', ExpandConstant('{app}\uninstall.bat'),'', ExpandConstant('{app}'), SW_HIDE, ewWaitUntilTerminated, ErrorCode) then
  begin
      MsgBox('Failed to stop application server!', mbInformation, MB_OK);
  end;

end;

