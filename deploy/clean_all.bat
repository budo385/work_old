@ECHO OFF
REM set BUILD_SOURCE=%SOKRATES_XP%

CALL "C:\Program Files\Microsoft Visual Studio 9.0\Common7\Tools\vsvars32.bat"
IF %ERRORLEVEL% NEQ 0 GOTO ERROR_HANDLER

REM clean all
devenv "%BUILD_SOURCE%\exe\sokrates_spc\sokrates_spc.sln" /clean release
devenv "%BUILD_SOURCE%\exe\appserver\appserver.sln" /clean release
devenv "%BUILD_SOURCE%\exe\appserver_james\appserver_james.sln" /clean release
devenv "%BUILD_SOURCE%\exe\appserver_john\appserver_john.sln" /clean release
devenv "%BUILD_SOURCE%\tool\ApplicationServerConfig\ApplicationServerConfig.sln" /clean release
devenv "%BUILD_SOURCE%\tool\AdminTool\AdminTool.sln" /clean release
devenv "%BUILD_SOURCE%\tool\Sokrates_Register\Sokrates_Register.sln" /clean release
devenv "%BUILD_SOURCE%\lib\phonet\phonet.sln" /clean release
devenv "%BUILD_SOURCE%\lib\mapi_ex\MAPIEx.sln" /clean release
devenv "%BUILD_SOURCE%\tool\KeyGenerator\KeyGenerator.sln" /clean release


echo.
echo Sucess!!!
GOTO QUIT

:ERROR_HANDLER
echo.
echo Error occured!!!
exit 1

:QUIT
echo.



